﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [], data = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Inventory');
    simsController.controller('DirectOrder_VATCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.req_no = [];
            $scope.selected_reqtype = '';
            $scope.flag_iforderdiscount = "false";
            $scope.temp={}
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {;
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);
            $timeout(function () {
                $("#fixTable2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.lcshow = false;
            $scope.show_vehicle = true;

            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.pagesize_ld = "5";
            $scope.pageindex_ld = "1";
            $scope.filteredTodos_ld = [], $scope.currentPage_ld = 1, $scope.numPerPage_ld = 5, $scope.maxSize_ld = 5;

            $scope.cdetails_data = [];
            $scope.Suppliers = [];
            $scope.CurMaster = [];
            var date = new Date();
            //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');

            $scope.temp2 = {
                im_inv_no: '',
                im_desc: '',
                dept_name: '',
            }

            //setTimeout(function () {
            //    $("#cmb_sup").select2();
            //}, 100);
            //setTimeout(function () {
            //    $("#cmb_currency").select2();
            //}, 100);

            $scope.temp =
                {
                    order_date: $scope.ddMMyyyy,
                    expected_date: $scope.ddMMyyyy,
                    invs058_order_apply_aexp: false
                }

            $scope.total_netamt = 0;
            $scope.total_grossamt = 0;
            $scope.totAmt = 0;
            $scope.itemsearch_true = true;
            $scope.itemset_true = false;
            //*********************All Combo*********************

            $http.get(ENV.apiUrl + "api/CreateOrder/GetDepartment").then(function (res1) {
                $scope.Department = res1.data;
                $scope.temp.dep_code = '10';
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetSuppliers").then(function (res11) {
                $scope.Suppliers = res11.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetServiceTypes").then(function (res2) {
                $scope.ServiceTypes = res2.data;

                $scope.temp.servicevalue = 'I';
                $scope.selected_reqtype = 'I';
                $scope.getOrders($scope.temp.servicevalue);
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetSupplierDepartment").then(function (res1) {
                $scope.supplier = res1.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetDeliveryMOde").then(function (res1) {
                $scope.Deliverymode = res1.data;

                //if ($scope.Deliverymode.length > 0)
                //    $scope.temp['dm_code'] = $scope.Deliverymode[0].dm_code;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/Getpaymentmodes").then(function (res1) {
                $scope.paymentmode = res1.data;

                //if ($scope.paymentmode.length > 0)
                //    $scope.temp['pm_code'] = $scope.paymentmode[0].pm_code;

            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetTradeterms").then(function (res1) {
                $scope.Trades = res1.data;

                //if ($scope.Trades.length > 0)
                //    $scope.temp['trt_code'] = $scope.Trades[0].trt_code;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetLetterOfCredit").then(function (res1) {
                $scope.LOC = res1.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetForwardAgent").then(function (res1) {
                $scope.Agent = res1.data;

                //if ($scope.Agent.length > 0)
                //    $scope.temp['fa_code'] = $scope.Agent[0].fa_code;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetCurrencyMaster").then(function (res1) {
                debugger;
                $scope.CurMaster = res1.data;

                $http.get(ENV.apiUrl + "api/CreateOrder/GetCompanyCurrency").then(function (res_cur) {
                    console.log(res_cur);
                    if ($scope.CurMaster.length > 0)

                         $scope.temp['excg_curcy_code'] = res_cur.data;
                        //$("#cmb_currency").select2("val", res_cur.data);
                });
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/Getvehiclergnos").then(function (res_veh1) {
                console.log(res_veh1);
                $scope.res_veh = res_veh1.data;
                if ($scope.res_veh.length > 0)

                    $scope.temp['sims_transport_vehicle_code'] = $scope.res_veh[0].sims_transport_vehicle_code;
            });
            //$scope.temp = {};
            $scope.temp.invs058_order_apply_aexp = true;

            $scope.order_report_name = '';

            $http.get(ENV.apiUrl + "api/CreateOrder/Get_Order_report").then(function (res) {
                $scope.shipment_report_name = res.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetOrderAttributes").then(function (res2) {
                $scope.OrderAttrs = res2.data;
            });

            $scope.getOrders = function (req_type) {
                $http.get(ENV.apiUrl + "api/CreateOrder/GetPurchaseOrder?dept_code=" + $scope.temp.dep_code + "&req_type=" + req_type).then(function (res1) {
                    $scope.Orders = res1.data.table;
                    console.log("Orders ", $scope.Orders);
                    $scope.OrderDetails = res1.data.table1;
                    console.log("OrderDetails ", $scope.OrderDetails);
                });
            }
           

            $scope.getOrderDetails = function (ord_obj) {
                debugger
                console.log("OrderDetails 2", $scope.OrderDetails);                
                $scope.testALL = [];

                $scope.temp.sup_code = ord_obj.sup_code;
                $scope.temp.excg_curcy_code = ord_obj.cur_code;
                $scope.temp.dm_code = ord_obj.dm_code;
                $scope.temp.sup_code = ord_obj.sup_code;
                $scope.temp.pm_code = ord_obj.pm_code;
                $scope.temp.trt_code = ord_obj.trt_code;
                $scope.temp.fa_code = ord_obj.fa_code;
                $scope.temp.supplier_qty_ref = ord_obj.ord_supl_quot_ref;
                $scope.temp.sims_transport_vehicle_code = ord_obj.od_vehicle_no;
                $scope.temp.invs058_heading = ord_obj.order_heading;
                $scope.temp.invs058_notes = ord_obj.order_notes;
                $scope.temp.ifapplyexp = ord_obj.order_apply_aexp;


                //$('#cmb_sup').select2('destroy');
                //$('#cmb_sup').append();
                //$('#cmb_sup').val(ord_obj.sup_code)
                //$('#cmb_sup').select2();
                //$("#cmb_sup").select2("val", ord_obj.sup_code);
                //$('#cmb_sup').select2().select2('val', ord_obj.sup_code);
                //$('#cmb_sup').val(ord_obj.sup_code).trigger('change');
                //$('#cmb_sup').select2('destroy').select2();
                //$('#cmb_sup').val(ord_obj.sup_code)
                //$('#cmb_sup').select2();
              
                //$("#cmb_currency").select2("val", ord_obj.cur_code);
                //$('#cmb_currency').select2('destroy').select2(ord_obj.cur_code);

                $scope.ord_addexps = ord_obj.ord_addexps;
                $scope.ord_disamt = ord_obj.ord_disamt;
                $scope.ord_dispct = ord_obj.ord_dispct;
             

               
                $scope.order_createdby = ord_obj.order_createdby;
                $scope.ord_date = ord_obj.ord_date;
                $scope.total_grossamt = ord_obj.totalGrossAmt;
                $scope.total_netamt = ord_obj.totalNetAmt;
                $scope.vatTotalAmt = ord_obj.ord_vat_total_amount;
                $scope.totAmt = ord_obj.orderAmount;


                angular.forEach($scope.OrderDetails, function (value, key) {
                    if(value.ord_no == ord_obj.ord_no) {
                        var ob = {                                
                            invs058_im_inv_no:value.im_inv_no,
                            invs058_rd_item_desc:value.rd_item_desc,
                            invs058_uom_code:value.uom_name,                           
                            invs058_ordered_quantity: value.od_order_qty,
                            invs058_sup_price: value.od_supplier_price,
                            invs058_discount:value.od_line_discount_pct,
                            invs058_discount_amt:value.od_line_discount_amt,
                            im_item_vat_percentage:value.od_item_vat_per,
                            dd_item_vat_amount:value.od_item_vat_amount,
                            order_d_od_remarks: value.od_remarks,
                            invs058_gross_amt:value.grossamt,
                            invs058_rqvalue:value.netamt                          
                        };
                        $scope.testALL.push(ob);
                    }
                });


                for (var i = 0; i < $scope.OrderAttrs.length; i++) {                    
                    if (ord_obj.ord_addexps != undefined || ord_obj.ord_addexps != "" || ord_obj.ord_addexps != null) {
                        if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                            $scope.OrderAttrs[i].or_atamount = ord_obj.ord_addexps;
                        }
                    }
                    if (ord_obj.ord_dispct != undefined || ord_obj.ord_dispct != "" || ord_obj.ord_dispct != null) {
                        if ($scope.OrderAttrs[i].or_atstatus == 'P') {
                            $scope.OrderAttrs[i].or_atamount = ord_obj.ord_dispct;
                        }
                    }
                    if (ord_obj.ord_disamt != undefined || ord_obj.ord_disamt != "" || ord_obj.ord_disamt != null) {
                        if ($scope.OrderAttrs[i].or_atstatus == 'S') {
                            $scope.OrderAttrs[i].or_atamount = ord_obj.ord_disamt;
                        }
                    }                          
                }
                console.log("testAll", $scope.testALL);             
            }


            $scope.okbuttonclick1 = function () {
                debugger;
                var item_per = 0;
                for (var i = 0; i < $scope.GetItems.length; i++) {
                    var t = $scope.GetItems[i].im_inv_no;
                    var v = document.getElementById(t + i);
                    if ($scope.GetItems[i].ischecked == true) {
                        //$scope.inv_no = $scope.GetItems[i].im_inv_no
                        //$scope.inv_desc = $scope.GetItems[i].im_desc
                        //$scope.sup_code = $scope.GetItems[i].sup_code
                        //invno = $scope.inv_no;

                        if ($scope.GetItems[i].im_item_vat_percentage == undefined || $scope.GetItems[i].im_item_vat_percentage == "" || $scope.GetItems[i].im_item_vat_percentage == null) {
                            item_per = 0;
                        }
                        else {
                            item_per = parseFloat($scope.GetItems[i].im_item_vat_percentage).toFixed(3);
                        }

                        var data =
                        {
                            //sg_name: $scope.GetItems[i].sg_name,
                            //sg_code: '',
                            invs058_im_inv_no: $scope.GetItems[i].im_inv_no,
                            //im_item_code: $scope.GetItems[i].im_item_code,
                            invs058_rd_item_desc: $scope.GetItems[i].im_desc,
                            invs058_uom_code: $scope.GetItems[i].uom_code,
                            im_item_vat_percentage: item_per,
                            dd_item_vat_amount: item_per
                            //loc_code: '',
                            //original_qty: '',
                            //dd_qty: '',
                            //dd_sell_price: '',
                            //dd_sell_value_final: '',
                            //doc_remark: '',
                            //doc_require_date: $scope.tdate,
                        }
                        //$scope.itemList.push(data)
                        $scope.testALL.push(data)
                    }
                }
            }

            $scope.getfetch = function () {
                debugger;
                if ($scope.itemsetflg) {
                    $scope.temp1['im_assembly_ind'] = true;
                }
                else {
                    $scope.temp1['im_assembly_ind'] = false;


                }
                if($scope.temp.dep_code == undefined || $scope.temp.dep_code == "") {
                    $scope.temp.dep_code = '10';
                }
                $scope.temp1.invs021_dep_code = $scope.temp.dep_code;

                debugger;
                $http.post(ENV.apiUrl + "api/ItemRequests/postgetItemSerch", $scope.temp1).then(function (res) {
                    if (res.data.length > 0)
                        $scope.GetItems = res.data;
                    else
                        swal({ text: "No Record Found.", imageUrl: "assets/img/notification-alert.png", });


                });
            }

            $http.get(ENV.apiUrl + "api/ItemRequests/get_supplier_code").then(function (res) {
                debugger;
                $scope.cmb_supplier = res.data;
            });

            $http.get(ENV.apiUrl + "api/ItemRequests/GetAllSupplierGroupName").then(function (res) {
                debugger;
                $scope.rcbGroup = res.data;

            });

            $http.get(ENV.apiUrl + "api/ItemRequests/get_Categories").then(function (res) {
                debugger;
                $scope.rcbCategories = res.data;

            });

            $scope.getCategories = function (str) {

                if ($scope.temp.subcategory_code == undefined)
                    $scope.rcbItems = [];
                if ($scope.temp.subcategory_code == '')
                    $scope.rcbItems = [];

                $http.get(ENV.apiUrl + "api/ItemRequests/get_SubCategories?pc_parentcode=" + str).then(function (res) {

                    $scope.rcbSubCategories = res.data;

                });

            }

            //***************************************************

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.cdetails_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.cdetails_data;
                }
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            function searchUtil(item, toSearch) {
                /* Search Text in all 2 fields */
                return (item.invs058_im_inv_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.invs058_rd_item_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.chk_click_item = function (obj, flg) {
                debugger;
                if ($scope.itemsetflg) {

                    $http.post(ENV.apiUrl + "api/ItemRequests/Fetch_ItemDetails_ItemSet?im_inv_no=" + obj.im_inv_no).then(function (res) {

                        console.log(res.data);
                        // $scope.rcbSubCategories = res.data;

                        for (var i = 0; i < res.data.length; i++) {
                            if ($scope.temp.doc_type == '02') {
                                $scope.internal_im_sell_price = 0;
                            }
                            else {
                                $scope.internal_im_sell_price = parseFloat(res.data[i].im_sell_price);
                            }

                            //    var data =
                            //{
                            //    sg_name: res.data[i].sg_name,
                            //    sg_code: res.data[i].sg_name,
                            //    im_inv_no: res.data[i].im_inv_no,
                            //    im_item_code: res.data[i].im_item_code,
                            //    im_desc: res.data[i].im_desc,
                            //    item_location_name: res.data[i].loc_code,
                            //    loc_code: res.data[i].loc_code,
                            //    original_qty: res.data[i].id_cur_qty,
                            //    dd_qty: res.data[i].ia_component_qty,
                            //    dd_sell_price: $scope.internal_im_sell_price,
                            //    dd_sell_value_final: $scope.internal_im_sell_price,
                            //}
                            //    $scope.itemList.push(data)

                            debugger;
                            var data =
                            {
                                //sg_name: res.data[i].sg_name,
                                //sg_code: res.data[i].sg_name,
                                invs058_im_inv_no: obj.im_inv_no,
                                //im_item_code: res.data[i].im_item_code,
                                invs058_rd_item_desc: obj.im_desc,
                                invs058_uom_code: obj.uom_name,
                                //item_location_name: res.data[i].loc_code,
                                //loc_code: res.data[i].loc_code,
                                //original_qty: res.data[i].id_cur_qty,
                                //dd_qty: res.data[i].ia_component_qty,
                                //dd_sell_price: $scope.internal_im_sell_price,
                                //dd_sell_value_final: $scope.internal_im_sell_price,
                            }
                            $scope.testALL.push(data);

                        }
                        $scope.totalAmtClick();
                    });

                    $('#itemSearch').modal('hide');
                }
                else {
                    $scope.Exflg = false;

                    var data =
                            {
                                invs058_im_inv_no: obj.im_inv_no,
                                invs058_rd_item_desc: obj.im_desc,
                                invs058_uom_code: obj.uom_name,
                            }
                    debugger;
                    if (flg) {

                        if ($scope.Exflg == false)
                            $scope.testALL.push(data);
                    }
                    else {

                        for (var i = 0; i < $scope.testALL.length; i++) {
                            if ($scope.testALL[i].im_inv_no == obj.im_inv_no) {
                                if ($scope.testALL[i].dd_qty > 1) {
                                    $scope.testALL[i].dd_qty -= 1;
                                    $scope.testALL[i].dd_sell_value_final -= parseFloat(obj.im_sell_price)

                                }
                                else
                                    $scope.testALL.splice(i, 1);
                            }
                        }
                        $scope.totalAmtClick();
                    }
                }
            }

            $scope.totalAmtClick = function () {
                $scope.totalFinal = 0;
                for (var i = 0; i < $scope.itemList.length; i++) {

                    $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                }
                $scope.temp['totalFinal'] = $scope.totalFinal;
                if ($scope.temp.otherCharges == undefined || $scope.temp.otherCharges == '') {
                }
                else {
                    $scope.temp.totalFinal += parseFloat($scope.temp.otherCharges);
                }

                if ($scope.temp.disamt == undefined || $scope.temp.disamt == '' || isNaN(parseFloat($scope.temp.disamt))) {
                }
                else {
                    $scope.temp.totalFinal -= parseFloat($scope.temp.disamt);
                }

                //   $scope.temp['totalFinal'] = $scope.totalFinal + (parseFloat($scope.temp.otherCharges) - parseFloat($scope.temp.disamt));



            }

            $scope.SearchRequest = function () {
                debugger;
                $scope.itemsetflg = false;
                $scope.temp1 = {};
                if ($scope.selected_reqtype != undefined && $scope.selected_reqtype != '') {
                    if ($scope.temp.servicevalue != $scope.selected_reqtype && $scope.testALL.length > 0) {
                        swal({ title: "Alert", text: "You have selected different Request Type than Previous one.", width: 300, height: 200 });

                        return;
                    }
                }

                $scope.selected_reqtype = $scope.temp.servicevalue;

                console.log($scope.temp.dep_code);
                console.log($scope.temp.servicevalue);
                if ($scope.temp.dep_code != undefined && $scope.temp.servicevalue != undefined) {
                    $scope.totAmt = 0;
                    $scope.itemsetflg = false;
                    // $('#MyModal1').modal('show');
                    $('#MyModal1').modal({ backdrop: 'static', keyboard: true });
                    $scope.srchsupplier = '';
                    $scope.srchitemcode = '';
                    $scope.srch_req_no = '';
                    var im_inv_no = '';
                    var im_item_code = '';
                    var im_desc = '';
                    $scope.GetItems = [];
                    $("#itemSearchChk1").attr('checked', false);

                    //$http.get(ENV.apiUrl + "api/CreateOrder/Get_ALLRequestDetails?dep_code=" + $scope.temp.dep_code + "&req_type=" + $scope.temp.servicevalue + "&srch_supcode=" + $scope.srchsupplier + "&srch_itemdesc=" + im_desc + "&srch_itemcode=" + im_item_code + "&srch_req_no=" + $scope.srch_req_no).then(function (res12) {
                    //    debugger;
                    //    $scope.search_Req_Data = res12.data;
                    //    $scope.search_Req_Data1 = angular.copy(res12.data);
                    //    $scope.cdetails_data = $scope.search_Req_Data1;
                    //    $scope.totalItems = $scope.cdetails_data.length;
                    //    $scope.todos = $scope.cdetails_data;
                    //    $scope.makeTodos();

                    //    //console.log($scope.search_Req_Data);
                    //});
                    $scope.itemsetflg = false;
                }
                else {
                    swal({ title: "Alert", text: "Please Select Department and Request Type", width: 300, height: 200 });
                }
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].req_no1 = false;
                }

            };

            $scope.RemoveEnrollMentNo = function ($event, index, str) {
                str.splice(index, 1);
            }

            $scope.search_Req_Data_all = [];
            $scope.testALL = [];

            $scope.okbuttonclick = function (j) {
                debugger;
                //if (item search)
                var item_per = 0;
                if ($scope.itemsetflg == false) {
                    //old Code
                    for (var i = 0; i < $scope.GetItems.length; i++) {
                        var t = $scope.GetItems[i].im_inv_no + i;
                        var v = document.getElementById(t);
                        $scope.flg_chk = true;                       
                        if (v.checked == true) {

                            //check if this item is already added
                            for (var j = 0; j < $scope.itemList.length; j++) {

                                debugger;

                                if ($scope.itemList[j].im_item_code == $scope.GetItems[i].im_item_code) {
                                    $scope.flg_chk = false;
                                }
                            }

                            if ($scope.GetItems[i].im_item_vat_percentage == undefined || $scope.GetItems[i].im_item_vat_percentage == "" || $scope.GetItems[i].im_item_vat_percentage == null) {
                                item_per = 0;
                            }
                            else {
                                item_per = parseFloat($scope.GetItems[i].im_item_vat_percentage).toFixed(3);
                            }
                            //if ($scope.flg_chk) {
                            var data =
                            {
                                sg_name: $scope.GetItems[i].sg_desc,
                                //  sg_code: $scope.GetItems[i].gcode,
                                im_inv_no: $scope.GetItems[i].im_inv_no,
                                im_item_code: $scope.GetItems[i].im_item_code,
                                im_desc: $scope.GetItems[i].im_desc,
                                item_location_name: $scope.GetItems[i].loc_code,
                                loc_code: $scope.GetItems[i].loc_code,
                                original_qty: $scope.GetItems[i].id_cur_qty,
                                dd_qty: 1,
                                dd_sell_price: $scope.GetItems[i].im_sell_price,
                                dd_sell_value_final: 0,
                                im_item_vat_percentage: item_per,
                                dd_item_vat_amount: item_per
                            }

                            $scope.itemList.push(data)
                            //}
                        }
                    }
                }
                else if ($scope.itemsetflg == true) {
                    debugger;
                    var imnno;
                    for (var i = 0; i < $scope.GetItems.length; i++) {
                        var t = $scope.GetItems[i].im_inv_no + i;
                        var v = document.getElementById(t);
                        $scope.flg_chk = true;
                        if (v.checked == true) {
                            imnno = $scope.GetItems[i].im_inv_no;
                        }
                    }
                    $http.post(ENV.apiUrl + "api/ItemRequests/Fetch_ItemDetails_ItemSet?im_inv_no=" + imnno).then(function (res) {

                        console.log(res.data);
                        // $scope.rcbSubCategories = res.data;

                       // for (var i = 0; i < res.data.length; i++) {                         
                            debugger;
                            for (var i = 0; i < res.data.length; i++) {

                                if ($scope.temp.doc_type == '02') {
                                    $scope.internal_im_sell_price = 0;
                                }
                                else {
                                    $scope.internal_im_sell_price = parseFloat(res.data[i].im_sell_price);
                                }

                                if (res.data[i].im_item_vat_percentage == undefined || res.data[i].im_item_vat_percentage == "" || res.data[i].im_item_vat_percentage == null) {
                                    item_per = 0;
                                }
                                else {
                                    item_per = parseFloat(res.data[i].im_item_vat_percentage).toFixed(3);
                                }
                                var data =
                                    {
                                        sg_name: res.data[i].sg_desc,
                                        //  sg_code: $scope.GetItems[i].gcode,
                                        im_inv_no: res.data[i].im_inv_no,
                                        im_item_code: res.data[i].im_item_code,
                                        im_desc: res.data[i].im_desc,
                                        item_location_name: res.data[i].loc_code,
                                        loc_code: res.data[i].loc_code,
                                        original_qty: res.data[i].id_cur_qty,
                                        dd_qty: 1,
                                        dd_sell_price: res.data[i].im_sell_price,
                                        dd_sell_value_final: 0,
                                        im_item_vat_percentage: item_per,
                                        dd_item_vat_amount: item_per
                                    }

                                $scope.itemList.push(data)
                            }

                        //}
                    });
                }
            }

            $scope.okbuttonclick = function () {
                debugger;

                var item_per = 0;

                if ($scope.itemsetflg == false) {

                    //old Code
                    for (var i = 0; i < $scope.GetItems.length; i++) {
                        var t = $scope.GetItems[i].im_inv_no + i;
                        var v = document.getElementById(t);
                        $scope.flg_chk = true;
                        debugger;
                        if (v.checked == true) {

                            //check if this item is already added
                            for (var j = 0; j < $scope.testALL.length; j++) {

                                debugger;

                                if ($scope.testALL[j].invs058_im_inv_no == $scope.GetItems[i].im_inv_no) {
                                    $scope.flg_chk = false;
                                }
                            }

                            if ($scope.GetItems[i].im_item_vat_percentage == undefined || $scope.GetItems[i].im_item_vat_percentage == "" || $scope.GetItems[i].im_item_vat_percentage == null) {
                                item_per = 0;
                            }
                            else {
                                item_per = parseFloat($scope.GetItems[i].im_item_vat_percentage).toFixed(3);
                            }
                            //if ($scope.flg_chk) {
                            var data =
                           {
                               invs058_im_inv_no: $scope.GetItems[i].im_inv_no,
                               invs058_rd_item_desc: $scope.GetItems[i].im_desc,
                               invs058_uom_code: $scope.GetItems[i].uom_name,
                               im_item_vat_percentage: item_per,
                               dd_item_vat_amount: item_per
                           }

                            $scope.testALL.push(data);
                            // }
                        }
                    }
                }

                else if ($scope.itemsetflg == true) {
                    debugger;
                    var imnno;
                    for (var i = 0; i < $scope.GetItems.length; i++) {
                        var t = $scope.GetItems[i].im_inv_no + i;
                        var v = document.getElementById(t);
                        $scope.flg_chk = true;
                        if (v.checked == true) {
                            imnno = $scope.GetItems[i].im_inv_no;
                        }
                    }
                    $http.post(ENV.apiUrl + "api/ItemRequests/Fetch_ItemDetails_ItemSet?im_inv_no=" + imnno).then(function (res) {

                        console.log(res.data);
                        // $scope.rcbSubCategories = res.data;

                        //for (var i = 0; i < res.data.length; i++) {

                            debugger;
                            for (var i = 0; i < res.data.length; i++) {

                                if (res.data[i].im_item_vat_percentage == undefined || res.data[i].im_item_vat_percentage == "" || res.data[i].im_item_vat_percentage == null) {
                                    item_per = 0;
                                }
                                else {
                                    item_per = parseFloat(res.data[i].im_item_vat_percentage).toFixed(3);
                                }

                                var data =
                                    {

                                        invs058_im_inv_no: res.data[i].im_inv_no,
                                        invs058_rd_item_desc: res.data[i].im_desc,
                                        invs058_uom_code: res.data[i].uom_name,
                                        im_item_vat_percentage: item_per,
                                        dd_item_vat_amount: item_per

                                        //sg_name: res.data[i].sg_desc,
                                        ////  sg_code: $scope.GetItems[i].gcode,
                                        //im_inv_no: res.data[i].im_inv_no,
                                        //im_item_code: res.data[i].im_item_code,
                                        //im_desc: res.data[i].im_desc,
                                        //item_location_name: res.data[i].loc_code,
                                        //loc_code: res.data[i].loc_code,
                                        //original_qty: res.data[i].id_cur_qty,
                                        //dd_qty: 1,
                                        //dd_sell_price: res.data[i].im_sell_price,
                                        //dd_sell_value_final: 0,
                                    }

                                $scope.testALL.push(data)
                            }

                        //}
                    });
                }
            }

            $scope.checkreqtype = function () {
                debugger;

                if ($scope.selected_reqtype != undefined && $scope.selected_reqtype != '') {
                    debugger;
                    if ($scope.temp.servicevalue != $scope.selected_reqtype && $scope.testALL.length > 0) {

                        if ($scope.temp.servicevalue == 'I')
                            $scope.temp.servicevalue = 'S'
                        else if ($scope.temp.servicevalue == 'S')
                            $scope.temp.servicevalue = 'I'

                        swal({ title: "Alert", text: "You have selected different Request Type than Previous one.", width: 300, height: 200 });

                        return;
                    }

                    if ($scope.temp.servicevalue == 'I') {
                        $scope.itemsearch_true = true;
                        $scope.itemset_true = false;
                    }
                    else if ($scope.temp.servicevalue == 'S') {
                        $scope.itemsearch_true = false;
                        $scope.itemset_true = true;
                    }

                    if ($scope.temp.dep_code != undefined) {
                        if ($scope.temp.dep_code == '30') {
                            if ($scope.temp.servicevalue == 'S') {
                                $scope.show_vehicle = true;
                            }
                        }
                    }
                }
            }

            $scope.checkreqtype_dep = function () {
                debugger;

                if ($scope.temp.dep_code != undefined) {
                    if ($scope.temp.dep_code == '30') {
                        if ($scope.temp.servicevalue != undefined) {
                            if ($scope.temp.servicevalue == 'S') {
                                $scope.show_vehicle = true;
                            }
                            else {
                                $scope.show_vehicle = false;
                            }
                        }
                    }
                    else {
                        $scope.show_vehicle = true;
                    }
                }
            }

            $scope.SearchRequests = function () {
                debugger;
                var im_inv_no = $scope.srchitemcode.im_inv_no;
                var im_item_code = $scope.srchitemcode.im_item_code;
                var im_desc = $("#rcbItem").find("option:selected").text();

                if (im_item_code == undefined)
                    im_item_code = '';

                $http.get(ENV.apiUrl + "api/CreateOrder/Get_ALLRequestDetails?dep_code=" + $scope.temp.dep_code + "&req_type=" + $scope.temp.servicevalue + "&srch_supcode=" + $scope.srchsupplier + "&srch_itemdesc=" + im_desc + "&srch_itemcode=" + im_item_code + "&srch_req_no=" + $scope.srch_req_no).then(function (res12) {
                    debugger;
                    $scope.search_Req_Data = res12.data;
                    $scope.search_Req_Data1 = angular.copy(res12.data);
                    $scope.cdetails_data = $scope.search_Req_Data1;
                    $scope.totalItems = $scope.cdetails_data.length;
                    $scope.todos = $scope.cdetails_data;
                    $scope.makeTodos();

                    //console.log($scope.search_Req_Data);
                });
            }

            $scope.ItemSetSearch = function () {

                $scope.GetItems = []
                $scope.temp1 = {}

                $('#MyModal1').modal('show');
                $scope.itemsetflg = true;
                $scope.temp1['im_assembly_ind'] = true;
                $http.post(ENV.apiUrl + "api/ItemRequests/postgetItemSerch", $scope.temp1).then(function (res) {
                    if (res.data.length > 0)
                        $scope.GetItems = res.data;
                    else
                        swal({ text: "No Record Found.", imageUrl: "assets/img/notification-alert.png", });
                });
            }

            $scope.SearchService = function (str) {
                $scope.GetItems = [];
                $('#MyModal2').modal('show');
            }

            $scope.ResetRequests = function () {
                $scope.srchsupplier = '';
                $scope.srchitemcode = '';
                $scope.srch_req_no = '';
                $("#rcbItem").select2("val", "");
                var im_inv_no = '';
                var im_item_code = '';
                var im_desc = '';
                $scope.testALL = [];
                datasend = [];
                datasend1 = [];
                datasend2 = [];
                $scope.totAmt = 0;
                $scope.total_netamt = 0;
                $scope.total_grossamt = 0;

                $scope.flag_iforderdiscount = "false";
                $http.get(ENV.apiUrl + "api/CreateOrder/Get_ALLRequestDetails?dep_code=" + $scope.temp.dep_code + "&req_type=" + $scope.temp.servicevalue + "&srch_supcode=" + $scope.srchsupplier + "&srch_itemdesc=" + im_desc + "&srch_itemcode=" + im_item_code + "&srch_req_no=" + $scope.srch_req_no).then(function (res12) {
                    debugger;
                    $scope.search_Req_Data = res12.data;
                    $scope.search_Req_Data1 = angular.copy(res12.data);
                    $scope.cdetails_data = $scope.search_Req_Data1;
                    $scope.totalItems = $scope.cdetails_data.length;
                    $scope.todos = $scope.cdetails_data;
                    $scope.makeTodos();

                    //console.log($scope.search_Req_Data);
                });
            }

            $scope.Cancel1 = function () {
                debugger;
                $scope.temp = {};
                $scope.search_Req_Data = [];
                $scope.testALL = [];
                $scope.OrderAttrs = [];
                $scope.show_vehicle = true;
                $scope.selected_reqtype = '';
                //$("#cmb_sup").select2("val", "");
                //$("#cmb_currency").select2("val", "");
                $scope.temp =
                {
                    order_date: $scope.ddMMyyyy,
                    expected_date: $scope.ddMMyyyy,
                }
                $scope.totAmt = 0;
                $http.get(ENV.apiUrl + "api/CreateOrder/GetOrderAttributes").then(function (res2) {
                    $scope.OrderAttrs = res2.data;
                });

                $scope.table = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.total_grossamt = '';
                $scope.total_netamt = '';
                $scope.total_grossamt = '';
                $scope.testALL = [];
                datasend = [];
                datasend1 = [];
                datasend2 = [];
                $scope.total_netamt = 0;
                $scope.total_grossamt = 0;
                $scope.totAmt = 0;
                $scope.flag_iforderdiscount = "false";
                $scope.itemsearch_true = true;
                $scope.itemset_true = false;

                $http.get(ENV.apiUrl + "api/CreateOrder/GetDepartment").then(function (res1) {
                    $scope.Department = res1.data;
                    $scope.temp.dep_code = '10';
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/GetSuppliers").then(function (res11) {
                    $scope.Suppliers = res11.data;
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/GetServiceTypes").then(function (res2) {
                    $scope.ServiceTypes = res2.data;

                    $scope.temp.servicevalue = 'I';
                    $scope.selected_reqtype = 'I';
                    $scope.getOrders($scope.temp.servicevalue);
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/GetSupplierDepartment").then(function (res1) {
                    $scope.supplier = res1.data;
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/GetDeliveryMOde").then(function (res1) {
                    $scope.Deliverymode = res1.data;

                    if ($scope.Deliverymode.length > 0)
                        $scope.temp['dm_code'] = $scope.Deliverymode[0].dm_code;
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/Getpaymentmodes").then(function (res1) {
                    $scope.paymentmode = res1.data;

                    if ($scope.paymentmode.length > 0)
                        $scope.temp['pm_code'] = $scope.paymentmode[0].pm_code;

                });

                $http.get(ENV.apiUrl + "api/CreateOrder/GetTradeterms").then(function (res1) {
                    $scope.Trades = res1.data;

                    if ($scope.Trades.length > 0)
                        $scope.temp['trt_code'] = $scope.Trades[0].trt_code;
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/GetLetterOfCredit").then(function (res1) {
                    $scope.LOC = res1.data;
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/GetForwardAgent").then(function (res1) {
                    $scope.Agent = res1.data;

                    if ($scope.Agent.length > 0)
                        $scope.temp['fa_code'] = $scope.Agent[0].fa_code;
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/GetCurrencyMaster").then(function (res1) {
                    debugger;
                    $scope.CurMaster = res1.data;

                    $http.get(ENV.apiUrl + "api/CreateOrder/GetCompanyCurrency").then(function (res_cur) {
                        console.log(res_cur);
                        if ($scope.CurMaster.length > 0)

                            $scope.temp['excg_curcy_code'] = res_cur.data;//$scope.CurMaster[0].excg_curcy_code;
                            //$("#cmb_currency").select2("val", res_cur.data);
                    });
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/Getvehiclergnos").then(function (res_veh1) {
                    console.log(res_veh1);
                    $scope.res_veh = res_veh1.data;
                    if ($scope.res_veh.length > 0)

                        $scope.temp['sims_transport_vehicle_code'] = $scope.res_veh[0].sims_transport_vehicle_code;
                });

                $scope.temp.invs058_order_apply_aexp = false;

                $state.go('main.dorvat');
                //swal({ title: "Alert", text: "In Cancel", width: 300, height: 200 });    
            }

            $scope.ServiceFetch = function () {

                debugger;
                $http.get(ENV.apiUrl + "api/ItemRequests/getServiceSearch?im_no=" + $scope.temp2.im_inv_no + "&des_ser=" + $scope.temp2.im_desc + "&dep_name=" + $scope.temp2.dept_name).then(function (res1) {
                    $scope.GetItems = res1.data;
                    $scope.totalItems = $scope.GetItems.length;
                    $scope.todos = $scope.GetItems;
                    $scope.makeTodos();
                    $scope.hide_suc = true;
                });
            }

            $scope.ServiceReset = function () {
                $scope.temp2 = {
                    im_inv_no: '',
                    im_desc: '',
                    dept_name: '',
                }
                $scope.GetItems = [];
            }

            $scope.Reset = function () {
                $scope.temp1 = {
                    im_inv_no: '',
                    im_desc: '',
                    im_item_code: '',
                    dept_code: '',
                    sec_code: '',
                    code: '',
                    category_code: '',
                    subcategory_code: '',
                    im_assembly_ind: '',
                    im_assembly_ind_s: '',
                    ifconsumable: '',
                    onetimefla: '',
                    //sup_sblgr_acno:''
                }
                $scope.GetItems = [];
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                debugger;
                $scope.temp = "";
                $scope.search_Req_Data = null;
                $scope.testALL = null;
                $state.go('main.dorvat');
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            //DATA SAVE INSERT
            var datasend = [];
            var datasend1 = [];
            var datasend2 = [];
            var gofurther = true;
            var data = {};
            $scope.removeItem = function (obj, index) {
                $scope.testALL.splice(index, 1);
                $scope.orcalculateamt12();
            }

            $scope.getlastpurchaseinfo = function (obj, index) {
                debugger;
                $('#Modal_LastPurchaseReport').modal('show');
                $scope.ItemDesc = obj.invs058_rd_item_desc;
                $scope.CurrentQty = "100";
                $http.get(ENV.apiUrl + "api/CreateOrder/GetLastPurchaseDetails?im_inv_no=" + obj.invs058_im_inv_no).then(function (res2) {
                    $scope.LastPDetails = res2.data;
                    $scope.CurrentQty = $scope.LastPDetails[0].currentqty;

                });
            }

            //$scope.savedata = function () {
            //    debugger;

            //    for (var i = 0; i < $scope.testALL.length; i++) {
            //        //if ($scope.testALL[i].invs058_rd_line_no1 == true) {
            //        if ($scope.temp.servicevalue == 'I' || $scope.temp.servicevalue == 'S') {
            //            if ($scope.testALL[i].invs058_ordered_quantity == undefined || $scope.testALL[i].invs058_sup_price == undefined) {
            //                gofurther = false;
            //                swal({ title: "Alert", text: "Please enter Ordered Quantity/Supplier Price", width: 300, height: 200 });
            //                gofurther = true;
            //                return;
            //            }
            //        }
            //        //}
            //    }                

            //    if (gofurther == true) {
            //        if ($scope.temp.order_date != undefined && $scope.temp.sup_code != undefined && $scope.temp.dep_code != undefined && $scope.temp.servicevalue != undefined &&
            //            $scope.temp.pm_code != undefined && $scope.temp.trt_code != undefined) {
            //            var rand = Math.floor((Math.random() * 100) + 1);
            //            for (var i = 0; i < $scope.testALL.length; i++) {
            //                //for (var j = 0; j < $scope.testALL[i].req_details.length; j++) {
            //                // if ($scope.testALL[i].invs058_rd_line_no1 == true) {
            //                data = {
            //                    'opr': 'D',
            //                    'order_d_req_no': $scope.testALL[i].invs058_req_no,
            //                    'order_d_rd_line_no': $scope.testALL[i].invs058_rd_line_no,
            //                    'order_d_im_inv_no': $scope.testALL[i].invs058_im_inv_no,
            //                    'order_d_uom_code': $scope.testALL[i].invs058_uom_code,
            //                    'order_d_od_order_qty': $scope.testALL[i].invs058_ordered_quantity,
            //                    'order_d_od_supplier_price': $scope.testALL[i].invs058_sup_price,
            //                    'order_d_od_discount_pct': $scope.testALL[i].invs058_discount,
            //                    'order_d_od_shipped_qty': $scope.testALL[i].order_d_od_shipped_qty,
            //                    'order_d_od_received_qty': $scope.testALL[i].order_d_od_received_qty,
            //                    'order_d_od_orig_line_no': $scope.testALL[i].order_d_od_orig_line_no,
            //                    'order_d_od_status': $scope.testALL[i].order_d_od_status,
            //                    'order_d_od_remarks': $scope.testALL[i].order_d_od_remarks,
            //                    'invs058_discount_amt': $scope.testALL[i].invs058_discount_amt,

            //                    'order_ord_date': $scope.temp.order_date,
            //                    'order_dep_code': $scope.temp.dep_code,
            //                    'order_sup_code': $scope.temp.sup_code,
            //                    'order_cur_code': $scope.temp.excg_curcy_code,
            //                    'order_dm_code': $scope.temp.dm_code,
            //                    'order_ord_shipment_date': $scope.temp.expected_date,
            //                    'order_pm_code': $scope.temp.pm_code,
            //                    'order_trt_code': $scope.temp.trt_code,
            //                    'order_fa_code': $scope.temp.fa_code,
            //                    'order_lc_no': $scope.temp.lc_no,
            //                    'order_ord_supl_quot_ref': $scope.temp.supplier_qty_ref,
            //                    'invs058_req_type': $scope.temp.servicevalue,
            //                    'randomnumber': rand,
            //                    'invs058_order_createdby': $rootScope.globals.currentUser.username,
            //                    'invs058_heading': $scope.temp.invs058_heading,
            //                    'invs058_notes': $scope.temp.invs058_notes,
            //                    'invs058_order_apply_aexp': ifapplyexp,

            //                    'invs058_ord_addexps': ord_addexp,
            //                    'invs058_ord_dispct': ord_discount_pct,
            //                    'invs058_ord_disamt': ord_discount_amt,
            //                    'invs058_od_total_discount': '',
            //                    'invs058_od_total_expense': '',
            //                    'sims_transport_vehicle_code': $scope.temp.sims_transport_vehicle_code

            //                }
            //                datasend.push(data);
            //                //}
            //                //}
            //            }

            //            var ifapplyexp = 0;
            //            if ($scope.temp.invs058_order_apply_aexp == true)
            //                ifapplyexp = 1;

            //            var ord_addexp = 0;
            //            var ord_discount_pct = 0;
            //            var ord_discount_amt = 0;

            //            for (var i = 0; i < $scope.OrderAttrs.length; i++) {
            //                if ($scope.OrderAttrs[i].or_atamount != 0 && $scope.OrderAttrs[i].or_atamount != undefined && $scope.OrderAttrs[i].or_atamount != '') {
            //                    if ($scope.OrderAttrs[i].or_atstatus == 'A')
            //                        ord_addexp = $scope.OrderAttrs[i].or_atamount;
            //                    if ($scope.OrderAttrs[i].or_atstatus == 'P')
            //                        ord_discount_pct = $scope.OrderAttrs[i].or_atamount;
            //                    if ($scope.OrderAttrs[i].or_atstatus == 'S')
            //                        ord_discount_amt = $scope.OrderAttrs[i].or_atamount;
            //                }
            //            }

            //            //$scope.temp.invs058_notes = $scope.temp.invs058_notes.replace('&', 'amp;');

            //            $scope.data1 = {
            //                'opr': 'O',
            //                'order_ord_date': $scope.temp.order_date,
            //                'order_dep_code': $scope.temp.dep_code,
            //                'order_sup_code': $scope.temp.sup_code,
            //                'order_cur_code': $scope.temp.excg_curcy_code,
            //                'order_dm_code': $scope.temp.dm_code,
            //                'order_ord_shipment_date': $scope.temp.expected_date,
            //                'order_pm_code': $scope.temp.pm_code,
            //                'order_trt_code': $scope.temp.trt_code,
            //                'order_fa_code': $scope.temp.fa_code,
            //                'order_lc_no': $scope.temp.lc_no,
            //                'order_ord_supl_quot_ref': $scope.temp.supplier_qty_ref,
            //                'invs058_req_type': $scope.temp.servicevalue,
            //                'randomnumber': Math.floor((Math.random() * 100) + 1),
            //                'invs058_order_createdby': $rootScope.globals.currentUser.username,
            //                'invs058_heading': $scope.temp.invs058_heading,
            //                'invs058_notes': $scope.temp.invs058_notes,
            //                'invs058_order_apply_aexp': ifapplyexp,

            //                'invs058_ord_addexps': ord_addexp,
            //                'invs058_ord_dispct': ord_discount_pct,
            //                'invs058_ord_disamt': ord_discount_amt,
            //                'invs058_od_total_discount': '',
            //                'invs058_od_total_expense': '',
            //                'sims_transport_vehicle_code': $scope.temp.sims_transport_vehicle_code
            //            }

            //            //datasend1.push($scope.data1);

            //            if (datasend.length > 0) {
            //                $http.post(ENV.apiUrl + "api/CreateOrder/SaveOrderDetails1", datasend).then(function (msg) {
            //                    //$http.post(ENV.apiUrl + "api/CreateOrder/SaveOrderDetails?data1=" + JSON.stringify($scope.data1), datasend).then(function (msg) {

            //                    $scope.msg1 = msg.data;

            //                    //$http.post(ENV.apiUrl + "api/CreateOrder/COrderDetails?ordno=" + $scope.msg1, datasend2).then(function (msg) {


            //                    swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 });
            //                    if ($scope.msg1 != null && $scope.msg1 != '') {
            //                        //swal({ title: "Alert", text: "Order Number " + $scope.msg1 + "  inserted successfully", width: 300, height: 200 });

            //                        swal({
            //                            title: '',
            //                            text: "Purchase Order No " + $scope.msg1 + "  successfully created.",
            //                            showCloseButton: true,
            //                            showCancelButton: true,
            //                            confirmButtonText: 'Yes',
            //                            width: 380,
            //                            cancelButtonText: 'No',
            //                            allowOutsideClick: false,
            //                        }).then(function (isConfirm) {
            //                            //if (isConfirm) {
            //                            //    var data = {
            //                            //                location: 'Invs.invr21',
            //                            //                parameter: {
            //                            //                    orderno: $scope.msg1,
            //                            //                },
            //                            //                state: 'main.Inv044',
            //                            //                ready: function () {
            //                            //                    this.refreshReport();
            //                            //                },
            //                            //            }
            //                            //            console.log(data);

            //                            //            window.localStorage["ReportDetails"] = JSON.stringify(data);
            //                            //            $state.go('main.ReportCardParameter');
            //                            //}
            //                        });

            //                        $scope.Cancel1();
            //                    }
            //                    else {
            //                        swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
            //                    }
            //                    $scope.grid_Display();
            //                    //});
            //                    datasend = [];
            //                    $scope.selected_reqtype = '';
            //                    $scope.testALL = [];
            //                    $scope.temp.servicevalue = 'I'
            //                    $scope.flag_iforderdiscount = "false";
            //                    datasend1 = [];
            //                    datasend2 = [];
            //                    $scope.totAmt = 0;
            //                    $scope.table = true;
            //                    $scope.display = false;
            //                    //}
            //                });
            //            }
            //            else {
            //                swal({ title: "Alert", text: "Please Select Items to Create Order.", width: 300, height: 200 });
            //            }
            //        }
            //    }

            //    else {
            //        swal({ title: "Alert", text: "Please Select Mandatory Fields", width: 300, height: 200 });
            //    }
            //}



            //My Change

            $scope.savedata = function () {
                debugger;

                for (var i = 0; i < $scope.testALL.length; i++) {
                    //if ($scope.testALL[i].invs058_rd_line_no1 == true) {
                    if ($scope.temp.servicevalue == 'I' || $scope.temp.servicevalue == 'S') {
                        if ($scope.testALL[i].invs058_ordered_quantity == undefined || $scope.testALL[i].invs058_sup_price == undefined) {
                            gofurther = false;
                            swal({ title: "Alert", text: "Please enter Ordered Quantity/Supplier Price", width: 300, height: 200 });
                            gofurther = true;
                            return;
                        }
                    }
                    //}
                }

                if (gofurther == true) {
                    if ($scope.temp.order_date != undefined && $scope.temp.sup_code != undefined && $scope.temp.dep_code != undefined && $scope.temp.servicevalue != undefined &&
                        $scope.temp.pm_code != undefined && $scope.temp.trt_code != undefined) {

                        var ifapplyexp = 0;
                        if ($scope.temp.invs058_order_apply_aexp == true)
                            ifapplyexp = 1;

                        var ord_addexp = 0;
                        var ord_discount_pct = 0;
                        var ord_discount_amt = 0;

                        for (var i = 0; i < $scope.OrderAttrs.length; i++) {
                            if ($scope.OrderAttrs[i].or_atamount != 0 && $scope.OrderAttrs[i].or_atamount != undefined && $scope.OrderAttrs[i].or_atamount != '') {
                                if ($scope.OrderAttrs[i].or_atstatus == 'A')
                                    ord_addexp = $scope.OrderAttrs[i].or_atamount;
                                if ($scope.OrderAttrs[i].or_atstatus == 'P')
                                    ord_discount_pct = $scope.OrderAttrs[i].or_atamount;
                                if ($scope.OrderAttrs[i].or_atstatus == 'S')
                                    ord_discount_amt = $scope.OrderAttrs[i].or_atamount;
                            }
                        }

                        var rand = Math.floor((Math.random() * 100) + 1);

                        for (var i = 0; i < $scope.testALL.length; i++) {
                            //for (var j = 0; j < $scope.testALL[i].req_details.length; j++) {
                            // if ($scope.testALL[i].invs058_rd_line_no1 == true) {
                            data = {
                                'opr': 'D',
                                'order_d_req_no': $scope.testALL[i].invs058_req_no,
                                'order_d_rd_line_no': $scope.testALL[i].invs058_rd_line_no,
                                'order_d_im_inv_no': $scope.testALL[i].invs058_im_inv_no,
                                'order_d_uom_code': $scope.testALL[i].invs058_uom_code,
                                'order_d_od_order_qty': $scope.testALL[i].invs058_ordered_quantity,
                                'order_d_od_supplier_price': $scope.testALL[i].invs058_sup_price,
                                'order_d_od_discount_pct': $scope.testALL[i].invs058_discount,
                                'order_d_od_shipped_qty': $scope.testALL[i].order_d_od_shipped_qty,
                                'order_d_od_received_qty': $scope.testALL[i].order_d_od_received_qty,
                                'order_d_od_orig_line_no': $scope.testALL[i].order_d_od_orig_line_no,
                                'order_d_od_status': $scope.testALL[i].order_d_od_status,
                                'order_d_od_remarks': $scope.testALL[i].order_d_od_remarks,
                                'invs058_discount_amt': $scope.testALL[i].invs058_discount_amt,

                                'order_ord_date': $scope.temp.order_date,
                                'order_dep_code': $scope.temp.dep_code,
                                'order_sup_code': $("#cmb_sup1 option:selected").text(), //$("#cmb_sup").find("option:selected").text(),//$scope.temp.sup_code,
                                'order_cur_code': $("#cmb_currency1 option:selected").text(), //$("#cmb_currency").find("option:selected").text(),//$scope.temp.excg_curcy_code,
                                'order_dm_code': $scope.temp.dm_code,
                                'order_ord_shipment_date': $scope.temp.expected_date,
                                'order_pm_code': $scope.temp.pm_code,
                                'order_trt_code': $scope.temp.trt_code,
                                'order_fa_code': $scope.temp.fa_code,
                                'order_lc_no': $scope.temp.lc_no,
                                'order_ord_supl_quot_ref': $scope.temp.supplier_qty_ref,
                                'invs058_req_type': $scope.temp.servicevalue,
                                'randomnumber': rand,
                                'invs058_order_createdby': $rootScope.globals.currentUser.username,
                                'invs058_heading': $scope.temp.invs058_heading,
                                'invs058_notes': $scope.temp.invs058_notes,
                                'invs058_order_apply_aexp': ifapplyexp,

                                'invs058_ord_addexps': ord_addexp,
                                'invs058_ord_dispct': ord_discount_pct,
                                'invs058_ord_disamt': ord_discount_amt,
                                'invs058_od_total_discount': '',
                                'invs058_od_total_expense': '',
                                'sims_transport_vehicle_code': $scope.temp.sims_transport_vehicle_code,

                                'od_item_vat_per': $scope.testALL[i].im_item_vat_percentage,
                                'od_item_vat_amount': $scope.testALL[i].dd_item_vat_amount,
                                'ord_vat_total_amount': $scope.vatTotalAmt

                            }
                            datasend.push(data);
                            //}
                            //}
                        }

                        //var ifapplyexp = 0;
                        //if ($scope.temp.invs058_order_apply_aexp == true)
                        //    ifapplyexp = 1;

                        //var ord_addexp = 0;
                        //var ord_discount_pct = 0;
                        //var ord_discount_amt = 0;

                        //for (var i = 0; i < $scope.OrderAttrs.length; i++) {
                        //    if ($scope.OrderAttrs[i].or_atamount != 0 && $scope.OrderAttrs[i].or_atamount != undefined && $scope.OrderAttrs[i].or_atamount != '') {
                        //        if ($scope.OrderAttrs[i].or_atstatus == 'A')
                        //            ord_addexp = $scope.OrderAttrs[i].or_atamount;
                        //        if ($scope.OrderAttrs[i].or_atstatus == 'P')
                        //            ord_discount_pct = $scope.OrderAttrs[i].or_atamount;
                        //        if ($scope.OrderAttrs[i].or_atstatus == 'S')
                        //            ord_discount_amt = $scope.OrderAttrs[i].or_atamount;
                        //    }
                        //}

                        //$scope.temp.invs058_notes = $scope.temp.invs058_notes.replace('&', 'amp;');
                        $scope.data1 = {
                            'opr': 'O',
                            'order_ord_date': $scope.temp.order_date,
                            'order_dep_code': $scope.temp.dep_code,
                            'order_sup_code': $("#cmb_sup1 option:selected").text(), //$("#cmb_sup").find("option:selected").text(),//$scope.temp.sup_code,
                            'order_cur_code': $("#cmb_currency1 option:selected").text(), //$("#cmb_currency").find("option:selected").text(),//$scope.temp.excg_curcy_code,
                            'order_dm_code': $scope.temp.dm_code,
                            'order_ord_shipment_date': $scope.temp.expected_date,
                            'order_pm_code': $scope.temp.pm_code,
                            'order_trt_code': $scope.temp.trt_code,
                            'order_fa_code': $scope.temp.fa_code,
                            'order_lc_no': $scope.temp.lc_no,
                            'order_ord_supl_quot_ref': $scope.temp.supplier_qty_ref,
                            'invs058_req_type': $scope.temp.servicevalue,
                            'randomnumber': Math.floor((Math.random() * 100) + 1),
                            'invs058_order_createdby': $rootScope.globals.currentUser.username,
                            'invs058_heading': $scope.temp.invs058_heading,
                            'invs058_notes': $scope.temp.invs058_notes,
                            'invs058_order_apply_aexp': ifapplyexp,

                            'invs058_ord_addexps': ord_addexp,
                            'invs058_ord_dispct': ord_discount_pct,
                            'invs058_ord_disamt': ord_discount_amt,
                            'invs058_od_total_discount': '',
                            'invs058_od_total_expense': '',
                            'sims_transport_vehicle_code': $scope.temp.sims_transport_vehicle_code
                        }

                        console.log("datasend",datasend);
                        swal({
                            title: '',
                            text: "Do you want to Save Purchase Order?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',
                            allowOutsideClick: false,
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                if (datasend.length > 0) {

                                    $http.post(ENV.apiUrl + "api/CreateOrder/SaveOrderDetails1_VAT", datasend).then(function (msg) {
                                        $scope.msg1 = msg.data;

                                        if ($scope.msg1 != null && $scope.msg1 != '') {
                                            swal({ text: "Purchase Order No " + $scope.msg1 + "  successfully created.", imageUrl: "assets/img/check.png", width: 300, height: 200 });

                                            $scope.Cancel1();
                                        }
                                        else {
                                            swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                        }
                                        $scope.grid_Display();
                                        $scope.getOrders($scope.temp.servicevalue);
                                        datasend = [];
                                        $scope.selected_reqtype = '';
                                        $scope.testALL = [];
                                        $scope.temp.servicevalue = 'I'
                                        $scope.flag_iforderdiscount = "false";
                                        datasend1 = [];
                                        datasend2 = [];
                                        $scope.totAmt = 0;
                                        $scope.table = true;
                                        $scope.display = false;
                                    });
                                }
                                else {
                                    swal({ text: "Please Select Items to Create Order.", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                                }
                            }
                            else {
                                debugger;
                                $scope.Cancel1();
                                $scope.grid_Display();
                                datasend = [];
                                $scope.selected_reqtype = '';
                                $scope.testALL = [];
                                $scope.temp.servicevalue = 'I'
                                $scope.flag_iforderdiscount = "false";
                                datasend1 = [];
                                datasend2 = [];
                                $scope.totAmt = 0;
                                $scope.table = true;
                                $scope.display = false;
                            }
                        }
                        );
                    }
                }
                else {
                    swal({ title: "Alert", text: "Please Select Mandatory Fields", width: 300, height: 200 });
                }
            }

            $scope.onlyNumbers = function (event, str) {

                if (str > 100) {
                    swal({ title: "Alert", text: "Invalid Discount", width: 300, height: 200 });
                }

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $scope.Report = function (shipment_no) {
                console.log(str);
                var data = {
                    location: $scope.order_report_name,
                    parameter: {
                        ship: ord_no,
                    },
                    state: 'main.dorvat',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
            }

            $scope.printConfrimDetails = function (str) {
                var data = {
                    location: 'Invs.INVR17',
                    parameter: { ord_no: str },
                    state: 'main.dorvat',
                    reg: param,
                    email: email
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)

                // printDetails
                $state.go('Report');
            }

            var dom;
            $scope.flag = true;
            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                         "<tr>   <td class='semi-bold'>" + "" + "</td>  <td class='semi-bold'>" + "Request No." + "</td> <td class='semi-bold'>" + "Request Line No." + " </td><td class='semi-bold'>" + "Item Desc" + "</td>" +
                        "<td class='semi-bold'>" + "UOM" + "</td> <td class='semi-bold'>" + "Quantity" + " </td>" + "<td class='semi-bold'>" + "Ordered Quantity" + " </td>" + "<td class='semi-bold'>" + "Supplier Price" + " </td>" + "<td class='semi-bold'>" + "Discount" + " </td>" + "<td class='semi-bold'>" + "Remark" + " </td>" + "</tr>" +

                          "<tr> <td>" + '<input type="checkbox" value="1" ng-click="SingleCheckSave()">' +
                         "<td>" + (info.invs058_req_no) + "</td> <td>" + (info.invs058_rd_line_no) + " </td><td>" + (info.rd_item_desc) + "</td>" +
                         "<td>" + (info.uom_code) + "</td> <td>" + (info.rd_quantity) + "</td>" + "<td>" + '<input style="width: 90px; height: 25px" aria-controls="example" ng-model="info.pays_appl_form_field_value2" class="form-control input-sm">' + "<td>" + '<input style="width: 90px; height: 25px" aria-controls="example" ng-model="info.pays_appl_form_field_value2" class="form-control input-sm">' + "<td>" + '<input style="width: 90px; height: 25px" aria-controls="example" ng-model="info.pays_appl_form_field_value2" class="form-control input-sm">' + "<td>" + (info.rd_remarks) + " </td>" + "</tr>" +

                        " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.req_no.length; i++) {
                        $scope.req_no[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }

            };

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.adddata = function (chkvalue, info) {
                debugger;
                if ($scope.temp.servicevalue == 'I') {
                    if (chkvalue == true) {
                        info.servicevalue = false;
                    }
                    else if (chkvalue == false) {
                        info.servicevalue = true;
                    }
                }
                else { info.servicevalue = true; }

                $scope.totAmt = 0;
                var totalamt = 0;
                for (var a = 0; a < $scope.testALL.length; a++) {
                    if ($scope.testALL[a].invs058_rd_line_no1 == true) {
                        if ($scope.testALL[a].invs058_ordered_quantity != undefined && $scope.testALL[a].invs058_sup_price != undefined) {
                            console.log($scope.testALL[a].invs058_rqvalue);
                            totalamt = totalamt + $scope.testALL[a].invs058_rqvalue;
                        }
                    }
                }

                if (totalamt != "NaN") {
                    $scope.totAmt = totalamt;
                } else { $scope.totAmt = 0; }
            }

            //$scope.calamount = function (info, va) {
            //    debugger;
            //    $scope.totAmt = 0;
            //    if (va == 'amt') {
            //        if (info.invs058_discount_amt != '' && info.invs058_discount_amt != undefined) {
            //            info.invs058_discount = '';
            //        }
            //    }
            //    if (va == 'dis') {
            //        if (info.invs058_discount != '' && info.invs058_discount != undefined) {
            //            if (info.invs058_discount > 100) {
            //                info.invs058_discount = '';
            //                swal({ title: "Alert", text: "Invalid Discount", width: 300, height: 200 });
            //            }
            //            info.invs058_discount_amt = '';
            //        }
            //    }

            //    var totalamt = 0;
            //    var totalgrossamt = 0;
            //    var totalnetamt = 0;

            //    var totalamt1 = 0;
            //    var totalamt_gross1 = 0;
            //    for (var i = 0; i < $scope.testALL.length; i++) {
            //        //if ($scope.testALL[i].invs058_rd_line_no1 == true) {
            //        if ($scope.temp.servicevalue == 'I' || $scope.temp.servicevalue == 'S') {

            //            if ($scope.testALL[i].invs058_ordered_quantity != undefined && $scope.testALL[i].invs058_ordered_quantity != '') {
            //                if ($scope.testALL[i].invs058_sup_price != undefined && $scope.testALL[i].invs058_sup_price != '') {

            //                    totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
            //                    $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
            //                    $scope.testALL[i].invs058_gross_amt = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;

            //                    totalamt_gross1 = $scope.testALL[i].invs058_gross_amt;

            //                    if ($scope.flag_iforderdiscount == "false") {
            //                        if ($scope.testALL[i].invs058_discount != undefined && $scope.testALL[i].invs058_discount != '') {
            //                            var disamt = ((totalamt1 * $scope.testALL[i].invs058_discount) / 100);
            //                            $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - disamt;
            //                            totalamt1 = totalamt1 - disamt;
            //                        }
            //                        if ($scope.testALL[i].invs058_discount_amt != undefined && $scope.testALL[i].invs058_discount_amt != '') {
            //                            var disamt = $scope.testALL[i].invs058_discount_amt;
            //                            $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - $scope.testALL[i].invs058_discount_amt;
            //                            totalamt1 = totalamt1 - disamt;
            //                        }
            //                    }
            //                    else {
            //                        $scope.testALL[i].invs058_discount = '';
            //                        $scope.testALL[i].invs058_discount_amt = '';
            //                    }
            //                    totalnetamt = totalnetamt + totalamt1;
            //                    totalgrossamt = totalgrossamt + totalamt_gross1;
            //                }
            //                else {
            //                    $scope.testALL[i].invs058_rqvalue = 0;
            //                    $scope.testALL[i].invs058_gross_amt = 0;
            //                }
            //            }
            //        }

            //        totalamt = totalamt + totalamt1;
            //        $scope.total_netamt = totalnetamt;
            //        $scope.total_grossamt = totalgrossamt;
            //        totalamt1 = 0;
            //        totalamt_gross1 = 0;
            //        // }
            //    }

            //    if (totalamt != NaN) {
            //        $scope.totAmt = totalamt;
            //        $scope.total_grossamt = totalgrossamt;
            //        $scope.total_netamt = totalnetamt;
            //    }
            //    else { $scope.totAmt = 0; }


            //}

            $scope.calamount = function (info, va) {
                debugger;
                $scope.totAmt = 0;
                if (va == 'amt') {
                    if (info.invs058_discount_amt != '' && info.invs058_discount_amt != undefined) {
                        info.invs058_discount = '';
                    }
                }
                if (va == 'dis') {
                    if (info.invs058_discount != '' && info.invs058_discount != undefined) {
                        if (info.invs058_discount > 100) {
                            info.invs058_discount = '';
                            swal({ title: "Alert", text: "Invalid Discount", width: 300, height: 200 });
                        }
                        info.invs058_discount_amt = '';
                    }
                }

                var totalamt = 0;
                var totalgrossamt = 0;
                var totalnetamt = 0;
                $scope.vatTotalAmt = '';
                var totalamt1 = 0;
                var totalamt_gross1 = 0;
                var dis_vat = 0;
                var vat_total_amt = 0;
                var vatAMT = 0;
                for (var i = 0; i < $scope.testALL.length; i++) {
                    //if ($scope.testALL[i].invs058_rd_line_no1 == true) {
                    if ($scope.temp.servicevalue == 'I' || $scope.temp.servicevalue == 'S') {

                        if ($scope.testALL[i].invs058_ordered_quantity != undefined && $scope.testALL[i].invs058_ordered_quantity != '') {
                            if ($scope.testALL[i].invs058_sup_price != undefined && $scope.testALL[i].invs058_sup_price != '') {


                                totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                $scope.testALL[i].invs058_gross_amt = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                $scope.testALL[i].invs058_gross_amt_vat = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;

                                totalamt_gross1 = $scope.testALL[i].invs058_gross_amt;

                                //if ($scope.testALL[i].im_item_vat_percentage != undefined || $scope.testALL[i].im_item_vat_percentage != "" || $scope.testALL[i].im_item_vat_percentage != null) {
                                //    //var finalPer = parseFloat(info.invs058_gross_amt_vat) * parseFloat(info.im_item_vat_percentage);
                                //    //var totalvat = parseFloat(finalPer) / 100;
                                //    //info.dd_item_vat_amount = parseFloat(totalvat.toFixed(3)); //parseFloat($scope.testALL[i].invs058_gross_amt_vat) + parseFloat(totalvat);
                                //    //info.invs058_gross_amt = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                //    //info.invs058_rqvalue = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                //    //totalamt_gross1 = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                //    //totalamt1 = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                //    //vat_total_amt = vat_total_amt + $scope.testALL[i].dd_item_vat_amount;

                                //    var sell_val_final = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                //    var totalvat = (parseFloat(sell_val_final) * $scope.testALL[i].im_item_vat_percentage) / 100;
                                //    $scope.testALL[i].dd_item_vat_amount = parseFloat(totalvat).toFixed(3);
                                //    $scope.testALL[i].invs058_gross_amt = parseFloat(sell_val_final) + parseFloat(totalvat);
                                //    $scope.testALL[i].invs058_rqvalue = parseFloat(sell_val_final) + parseFloat(totalvat);
                                //    totalamt_gross1 = parseFloat(sell_val_final) + parseFloat(totalvat);
                                //    totalamt1 = parseFloat(sell_val_final) + parseFloat(totalvat);
                                //    vatAMT = $scope.testALL[i].dd_item_vat_amount;
                                //}
                                //else {
                                //    totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                //    $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                //    $scope.testALL[i].invs058_gross_amt = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                //    $scope.testALL[i].invs058_gross_amt_vat = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;

                                //    totalamt_gross1 = $scope.testALL[i].invs058_gross_amt;
                                //}

                                if ($scope.flag_iforderdiscount == "false") {
                                    if ($scope.testALL[i].invs058_discount != undefined && $scope.testALL[i].invs058_discount != '') {
                                        var disamt = ((totalamt1 * $scope.testALL[i].invs058_discount) / 100);
                                        $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - disamt;
                                        totalamt1 = totalamt1 - disamt;
                                        //if (info.im_item_vat_percentage != undefined || info.im_item_vat_percentage != "" || info.im_item_vat_percentage != null) {
                                        //    dis_vat = (((totalamt1) * info.im_item_vat_percentage) / 100);
                                        //    info.dd_item_vat_amount = parseFloat(dis_vat).toFixed(3);

                                        //    totalamt1 = parseFloat(parseFloat(totalamt1) + parseFloat(info.dd_item_vat_amount)).toFixed(3);
                                        //    totalamt1 = totalamt1 - parseFloat(disamt).toFixed(3);
                                        //}
                                    }
                                    if ($scope.testALL[i].invs058_discount_amt != undefined && $scope.testALL[i].invs058_discount_amt != '') {
                                        var disamt = $scope.testALL[i].invs058_discount_amt;
                                        $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - $scope.testALL[i].invs058_discount_amt;
                                        totalamt1 = totalamt1 - disamt;

                                        //if (info.im_item_vat_percentage != undefined || info.im_item_vat_percentage != "" || info.im_item_vat_percentage != null) {
                                        //    dis_vat = (((totalamt1) * info.im_item_vat_percentage) / 100);
                                        //    info.dd_item_vat_amount = parseFloat(dis_vat).toFixed(3);

                                        //    totalamt1 = parseFloat(parseFloat(totalamt1) + parseFloat(info.dd_item_vat_amount)).toFixed(3);
                                        //    totalamt1 = totalamt1 - parseFloat(disamt).toFixed(3);
                                        //}

                                    }
                                }
                                else {
                                    $scope.testALL[i].invs058_discount = '';
                                    $scope.testALL[i].invs058_discount_amt = '';
                                }

                                if ($scope.testALL[i].im_item_vat_percentage != undefined || $scope.testALL[i].im_item_vat_percentage != "" || $scope.testALL[i].im_item_vat_percentage != null) {
                                    //var finalPer = parseFloat(info.invs058_gross_amt_vat) * parseFloat(info.im_item_vat_percentage);
                                    //var totalvat = parseFloat(finalPer) / 100;
                                    //info.dd_item_vat_amount = parseFloat(totalvat.toFixed(3)); //parseFloat($scope.testALL[i].invs058_gross_amt_vat) + parseFloat(totalvat);
                                    //info.invs058_gross_amt = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                    //info.invs058_rqvalue = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                    //totalamt_gross1 = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                    //totalamt1 = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                    //vat_total_amt = vat_total_amt + $scope.testALL[i].dd_item_vat_amount;

                                    var sell_val_final = $scope.testALL[i].invs058_rqvalue; //parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                    var totalvat = (parseFloat(sell_val_final) * $scope.testALL[i].im_item_vat_percentage) / 100;

                                    var sell_val_final_gross = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                    var totalvatgross = (parseFloat(sell_val_final_gross) * $scope.testALL[i].im_item_vat_percentage) / 100;

                                    $scope.testALL[i].dd_item_vat_amount = parseFloat(totalvat).toFixed(3);
                                    $scope.testALL[i].invs058_gross_amt = parseFloat($scope.testALL[i].invs058_gross_amt); //+ parseFloat(totalvatgross);
                                    $scope.testALL[i].invs058_rqvalue = parseFloat( $scope.testALL[i].invs058_rqvalue) + parseFloat(totalvat);
                                    totalamt_gross1 = parseFloat(sell_val_final_gross); //+ parseFloat(totalvatgross);
                                    totalamt1 = $scope.testALL[i].invs058_rqvalue;//parseFloat(sell_val_final) + parseFloat(totalvat);
                                    vatAMT = $scope.testALL[i].dd_item_vat_amount;
                                }

                                totalnetamt = totalnetamt + totalamt1;
                                totalgrossamt = totalgrossamt + totalamt_gross1;
                                vat_total_amt = vat_total_amt + parseFloat(vatAMT);

                            }
                            else {
                                $scope.testALL[i].invs058_rqvalue = 0;
                                $scope.testALL[i].invs058_gross_amt = 0;
                            }
                        }
                    }

                    totalamt = totalamt + totalamt1;
                    $scope.total_netamt = totalnetamt.toFixed(2);
                    $scope.total_grossamt = totalgrossamt;
                    $scope.vatTotalAmt = vat_total_amt;
                    totalamt1 = 0;
                    totalamt_gross1 = 0;
                    // }
                }

                if (totalamt != NaN) {
                    $scope.totAmt = totalamt;
                    $scope.total_grossamt = totalgrossamt;
                    $scope.total_netamt = totalnetamt.toFixed(2);
                    $scope.vatTotalAmt = vat_total_amt;
                }
                else { $scope.totAmt = 0; }


            }

            $scope.orcalculateamt = function (str) {
                debugger;

                var ttval1 = str.or_atstatus;
                if (ttval1 == 'P') {
                    if (str.or_atamount != 0 && str.or_atamount != '' && str.or_atamount != undefined) {
                        $scope.flag_iforderdiscount = "true";
                        for (var j = 0; j < $scope.OrderAttrs.length; j++) {
                            if ($scope.OrderAttrs[j].or_atstatus == 'S') {
                                $scope.OrderAttrs[j].or_atamount = 0;
                            }
                        }
                        if (str.or_atamount > 100) {
                            str.or_atamount = '';
                            swal({ title: "Alert", text: "Invalid Discount", width: 300, height: 200 });
                        }
                    }
                    else {
                        for (var j1 = 0; j1 < $scope.OrderAttrs.length; j1++) {
                            if ($scope.OrderAttrs[j1].or_atstatus == 'S') {
                                if ($scope.OrderAttrs[j1].or_atamount != 0 && $scope.OrderAttrs[j1].or_atamount != '' && $scope.OrderAttrs[j1].or_atamount != undefined) {

                                }
                                else {
                                    $scope.flag_iforderdiscount = "false";
                                }
                            }
                        }
                    }
                }

                if (ttval1 == 'S') {
                    if (str.or_atamount != 0 && str.or_atamount != '' && str.or_atamount != undefined) {
                        $scope.flag_iforderdiscount = "true";
                        for (var j = 0; j < $scope.OrderAttrs.length; j++) {
                            if ($scope.OrderAttrs[j].or_atstatus == 'P') {
                                $scope.OrderAttrs[j].or_atamount = 0;
                            }
                        }
                    }
                    else {
                        for (var j1 = 0; j1 < $scope.OrderAttrs.length; j1++) {
                            if ($scope.OrderAttrs[j1].or_atstatus == 'P') {
                                if ($scope.OrderAttrs[j1].or_atamount != 0 && $scope.OrderAttrs[j1].or_atamount != '' && $scope.OrderAttrs[j1].or_atamount != undefined) {

                                }
                                else {
                                    $scope.flag_iforderdiscount = "false";
                                }
                            }
                        }
                    }
                }

                var totalamt = 0;
                var totalgrossamt = 0;
                var totalnetamt = 0;

                var totalamt1 = 0;
                var totalamt_gross1 = 0;
                var vat_total_amt = 0;
                var vatAMT = 0;


                for (var i = 0; i < $scope.testALL.length; i++) {

                    //if ($scope.testALL[i].invs058_rd_line_no1 == true) {
                    if ($scope.temp.servicevalue == 'I' || $scope.temp.servicevalue == 'S') {

                        if ($scope.testALL[i].invs058_ordered_quantity != undefined && $scope.testALL[i].invs058_ordered_quantity != '') {
                            if ($scope.testALL[i].invs058_sup_price != undefined && $scope.testALL[i].invs058_sup_price != '') {

                                totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                $scope.testALL[i].invs058_gross_amt = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                $scope.testALL[i].invs058_gross_amt_vat = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;

                                totalamt_gross1 = $scope.testALL[i].invs058_gross_amt;


                                if ($scope.flag_iforderdiscount == "false") {
                                    if ($scope.testALL[i].invs058_discount != undefined && $scope.testALL[i].invs058_discount != '') {
                                        var disamt = ((totalamt1 * $scope.testALL[i].invs058_discount) / 100);
                                        $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - disamt;
                                        totalamt1 = totalamt1 - disamt;
                                    }
                                    if ($scope.testALL[i].invs058_discount_amt != undefined && $scope.testALL[i].invs058_discount_amt != '') {
                                        var disamt = $scope.testALL[i].invs058_discount_amt;
                                        $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - $scope.testALL[i].invs058_discount_amt;
                                        totalamt1 = totalamt1 - disamt;
                                    }
                                }
                                else {
                                    $scope.testALL[i].invs058_discount = '';
                                    $scope.testALL[i].invs058_discount_amt = '';
                                }


                                if ($scope.testALL[i].im_item_vat_percentage != undefined || $scope.testALL[i].im_item_vat_percentage != "" || $scope.testALL[i].im_item_vat_percentage != null) {
                                    //var finalPer = parseFloat(info.invs058_gross_amt_vat) * parseFloat(info.im_item_vat_percentage);
                                    //var totalvat = parseFloat(finalPer) / 100;
                                    //info.dd_item_vat_amount = parseFloat(totalvat.toFixed(3)); //parseFloat($scope.testALL[i].invs058_gross_amt_vat) + parseFloat(totalvat);
                                    //info.invs058_gross_amt = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                    //info.invs058_rqvalue = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                    //totalamt_gross1 = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                    //totalamt1 = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                    //vat_total_amt = vat_total_amt + $scope.testALL[i].dd_item_vat_amount;

                                    var sell_val_final = $scope.testALL[i].invs058_rqvalue; //parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                    var totalvat = (parseFloat(sell_val_final) * $scope.testALL[i].im_item_vat_percentage) / 100;

                                    var sell_val_final_gross = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                    var totalvatgross = (parseFloat(sell_val_final_gross) * $scope.testALL[i].im_item_vat_percentage) / 100;

                                    $scope.testALL[i].dd_item_vat_amount = parseFloat(totalvat).toFixed(3);
                                    $scope.testALL[i].invs058_gross_amt = parseFloat($scope.testALL[i].invs058_gross_amt); //+ parseFloat(totalvatgross);
                                    $scope.testALL[i].invs058_rqvalue = parseFloat($scope.testALL[i].invs058_rqvalue) + parseFloat(totalvat);
                                    totalamt_gross1 = parseFloat(sell_val_final_gross); //+ parseFloat(totalvatgross);
                                    totalamt1 = $scope.testALL[i].invs058_rqvalue;//parseFloat(sell_val_final) + parseFloat(totalvat);
                                    vatAMT = $scope.testALL[i].dd_item_vat_amount;
                                }

                                totalnetamt = totalnetamt + totalamt1;
                                totalgrossamt = totalgrossamt + totalamt_gross1;
                                vat_total_amt = vat_total_amt + parseFloat(vatAMT);
                            }
                            else {
                                $scope.testALL[i].invs058_rqvalue = 0;
                                $scope.testALL[i].invs058_gross_amt = 0;
                            }
                        }
                    }

                    totalamt = totalamt + totalamt1;
                    $scope.total_netamt = totalnetamt;
                    $scope.vatTotalAmt = vat_total_amt;
                    // totalamt = totalnetamt;
                    $scope.total_grossamt = totalgrossamt;
                    totalamt1 = 0;
                    totalamt_gross1 = 0;
                }

                debugger;
                if ($scope.temp['invs058_order_apply_aexp'] == true) {//if checkbox is clicked

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var add = 0;

                            totalamt += (parseFloat(add));
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'P') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - ((parseFloat(totalamt) * parseFloat(percentage)) / 100);
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'S') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - $scope.OrderAttrs[i].or_atamount;
                        }
                    }
                }

                if ($scope.temp['invs058_order_apply_aexp'] == false) {//if checkbox is not clicked

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'P') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - ((parseFloat(totalamt) * parseFloat(percentage)) / 100);
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'S') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - $scope.OrderAttrs[i].or_atamount;
                        }


                        //if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                        //    if ($scope.OrderAttrs[i].or_atamount != '')
                        //        var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                        //    else
                        //        var add = 0;

                        //    totalamt += (parseFloat(add));
                        //}

                    }

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var add = 0;

                            totalamt += (parseFloat(add));
                        }
                    }
                }


                $scope.totAmt = totalamt;


            }

            $scope.orcalculateamt12 = function () {
                debugger;
                var totalamt = 0;
                var totalgrossamt = 0;
                var totalnetamt = 0;

                var totalamt1 = 0;
                var totalamt_gross1 = 0;
                $scope.total_grossamt = '';
                $scope.total_netamt = '';
                $scope.vatTotalAmt = '';
                var dis_vat = 0;
                var vat_total_amt = 0;
                var vatAMT = 0;

                for (var i = 0; i < $scope.testALL.length; i++) {
                    //if ($scope.testALL[i].invs058_rd_line_no1 == true) {
                    if ($scope.temp.servicevalue == 'I' || $scope.temp.servicevalue == 'S') {
                        if ($scope.testALL[i].invs058_ordered_quantity != undefined && $scope.testALL[i].invs058_ordered_quantity != '') {
                            if ($scope.testALL[i].invs058_sup_price != undefined && $scope.testALL[i].invs058_sup_price != '') {

                                //totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                //$scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                //$scope.testALL[i].invs058_gross_amt = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;

                                //totalamt_gross1 = $scope.testALL[i].invs058_gross_amt;

                                totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                $scope.testALL[i].invs058_gross_amt = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                $scope.testALL[i].invs058_gross_amt_vat = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;

                                totalamt_gross1 = $scope.testALL[i].invs058_gross_amt;

                                //if ($scope.testALL[i].im_item_vat_percentage != undefined || $scope.testALL[i].im_item_vat_percentage != "" || $scope.testALL[i].im_item_vat_percentage != null) {
                                //    //var finalPer = parseFloat(info.invs058_gross_amt_vat) * parseFloat(info.im_item_vat_percentage);
                                //    //var totalvat = parseFloat(finalPer) / 100;
                                //    //info.dd_item_vat_amount = parseFloat(totalvat.toFixed(3)); //parseFloat($scope.testALL[i].invs058_gross_amt_vat) + parseFloat(totalvat);
                                //    //info.invs058_gross_amt = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                //    //info.invs058_rqvalue = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                //    //totalamt_gross1 = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                //    //totalamt1 = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                //    //vat_total_amt = vat_total_amt + $scope.testALL[i].dd_item_vat_amount;

                                //    var sell_val_final = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                //    var totalvat = (parseFloat(sell_val_final) * $scope.testALL[i].im_item_vat_percentage) / 100;
                                //    $scope.testALL[i].dd_item_vat_amount = parseFloat(totalvat).toFixed(3);
                                //    $scope.testALL[i].invs058_gross_amt = parseFloat(sell_val_final) + parseFloat(totalvat);
                                //    $scope.testALL[i].invs058_rqvalue = parseFloat(sell_val_final) + parseFloat(totalvat);
                                //    totalamt_gross1 = parseFloat(sell_val_final) + parseFloat(totalvat);
                                //    totalamt1 = parseFloat(sell_val_final) + parseFloat(totalvat);
                                //    vatAMT = $scope.testALL[i].dd_item_vat_amount;
                                //}
                                //else {
                                //    totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                //    $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                //    $scope.testALL[i].invs058_gross_amt = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                //    $scope.testALL[i].invs058_gross_amt_vat = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;

                                //    totalamt_gross1 = $scope.testALL[i].invs058_gross_amt;
                                //}

                                if ($scope.flag_iforderdiscount == "false") {
                                    if ($scope.testALL[i].invs058_discount != undefined && $scope.testALL[i].invs058_discount != '') {
                                        var disamt = ((totalamt1 * $scope.testALL[i].invs058_discount) / 100);
                                        $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - disamt;
                                        totalamt1 = totalamt1 - disamt;
                                    }
                                    if ($scope.testALL[i].invs058_discount_amt != undefined && $scope.testALL[i].invs058_discount_amt != '') {
                                        var disamt = $scope.testALL[i].invs058_discount_amt;
                                        $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - $scope.testALL[i].invs058_discount_amt;
                                        totalamt1 = totalamt1 - disamt;
                                    }
                                }
                                else {
                                    $scope.testALL[i].invs058_discount = '';
                                    $scope.testALL[i].invs058_discount_amt = '';
                                }

                                if($scope.testALL[i].im_item_vat_percentage != undefined || $scope.testALL[i].im_item_vat_percentage != "" || $scope.testALL[i].im_item_vat_percentage != null) {
                                    //var finalPer = parseFloat(info.invs058_gross_amt_vat) * parseFloat(info.im_item_vat_percentage);
                                    //var totalvat = parseFloat(finalPer) / 100;
                                    //info.dd_item_vat_amount = parseFloat(totalvat.toFixed(3)); //parseFloat($scope.testALL[i].invs058_gross_amt_vat) + parseFloat(totalvat);
                                    //info.invs058_gross_amt = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                    //info.invs058_rqvalue = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                    //totalamt_gross1 = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                    //totalamt1 = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                                    //vat_total_amt = vat_total_amt + $scope.testALL[i].dd_item_vat_amount;

                                    var sell_val_final = $scope.testALL[i].invs058_rqvalue; //parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                    var totalvat = (parseFloat(sell_val_final) * $scope.testALL[i].im_item_vat_percentage) / 100;

                                    var sell_val_final_gross = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                    var totalvatgross = (parseFloat(sell_val_final_gross) * $scope.testALL[i].im_item_vat_percentage) / 100;

                                    $scope.testALL[i].dd_item_vat_amount = parseFloat(totalvat).toFixed(3);
                                    $scope.testALL[i].invs058_gross_amt = parseFloat($scope.testALL[i].invs058_gross_amt); //+ parseFloat(totalvatgross);
                                    $scope.testALL[i].invs058_rqvalue = parseFloat($scope.testALL[i].invs058_rqvalue) + parseFloat(totalvat);
                                    totalamt_gross1 = parseFloat(sell_val_final_gross); //+ parseFloat(totalvatgross);
                                    totalamt1 = $scope.testALL[i].invs058_rqvalue;//parseFloat(sell_val_final) + parseFloat(totalvat);
                                    vatAMT = $scope.testALL[i].dd_item_vat_amount;
                                }


                                totalnetamt = totalnetamt + totalamt1;
                                totalgrossamt = totalgrossamt + totalamt_gross1;
                                vat_total_amt = vat_total_amt + parseFloat(vatAMT);
                            }
                            else {
                                $scope.testALL[i].invs058_rqvalue = 0;
                                $scope.testALL[i].invs058_gross_amt = 0;
                            }
                        }
                    }

                    totalamt = totalamt + totalamt1;
                    $scope.total_netamt = totalnetamt.toFixed(2);
                    $scope.total_grossamt = totalgrossamt;
                    $scope.vatTotalAmt = vat_total_amt;
                    totalamt1 = 0;
                    totalamt_gross1 = 0;
                    // }
                }



                debugger;
                if ($scope.temp['invs058_order_apply_aexp'] == true) {//if checkbox is clicked

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var add = 0;

                            totalamt += (parseFloat(add));
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'P') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - ((parseFloat(totalamt) * parseFloat(percentage)) / 100);
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'S') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - $scope.OrderAttrs[i].or_atamount;
                        }
                    }
                }

                if ($scope.temp['invs058_order_apply_aexp'] == false) {//if checkbox is not clicked

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'P') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - ((parseFloat(totalamt) * parseFloat(percentage)) / 100);
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'S') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - $scope.OrderAttrs[i].or_atamount;
                        }


                        //if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                        //    if ($scope.OrderAttrs[i].or_atamount != '')
                        //        var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                        //    else
                        //        var add = 0;

                        //    totalamt += (parseFloat(add));
                        //}

                    }

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var add = 0;

                            totalamt += (parseFloat(add));
                        }
                    }
                }

                $scope.totAmt = totalamt;
            }

            $scope.orcalculateamt12_old = function () {
                debugger;
                var totalamt = 0;
                var totalgrossamt = 0;
                var totalnetamt = 0;

                var totalamt1 = 0;
                var totalamt_gross1 = 0;
                $scope.total_grossamt = '';
                $scope.total_netamt = '';

                for (var i = 0; i < $scope.testALL.length; i++) {
                    //if ($scope.testALL[i].invs058_rd_line_no1 == true) {
                    if ($scope.temp.servicevalue == 'I' || $scope.temp.servicevalue == 'S') {
                        if ($scope.testALL[i].invs058_ordered_quantity != undefined && $scope.testALL[i].invs058_ordered_quantity != '') {
                            if ($scope.testALL[i].invs058_sup_price != undefined && $scope.testALL[i].invs058_sup_price != '') {

                                totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                $scope.testALL[i].invs058_gross_amt = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;

                                totalamt_gross1 = $scope.testALL[i].invs058_gross_amt;

                                if ($scope.flag_iforderdiscount == "false") {
                                    if ($scope.testALL[i].invs058_discount != undefined && $scope.testALL[i].invs058_discount != '') {
                                        var disamt = ((totalamt1 * $scope.testALL[i].invs058_discount) / 100);
                                        $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - disamt;
                                        totalamt1 = totalamt1 - disamt;
                                    }
                                    if ($scope.testALL[i].invs058_discount_amt != undefined && $scope.testALL[i].invs058_discount_amt != '') {
                                        var disamt = $scope.testALL[i].invs058_discount_amt;
                                        $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - $scope.testALL[i].invs058_discount_amt;
                                        totalamt1 = totalamt1 - disamt;
                                    }
                                }
                                else {
                                    $scope.testALL[i].invs058_discount = '';
                                    $scope.testALL[i].invs058_discount_amt = '';
                                }

                                totalnetamt = totalnetamt + totalamt1;
                                totalgrossamt = totalgrossamt + totalamt_gross1;
                            }
                            else {
                                $scope.testALL[i].invs058_rqvalue = 0;
                                $scope.testALL[i].invs058_gross_amt = 0;
                            }
                        }
                    }

                    totalamt = totalamt + totalamt1;
                    $scope.total_netamt = totalnetamt;
                    $scope.total_grossamt = totalgrossamt;
                    totalamt1 = 0;
                    totalamt_gross1 = 0;
                    // }
                }



                debugger;
                if ($scope.temp['invs058_order_apply_aexp'] == true) {//if checkbox is clicked

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var add = 0;

                            totalamt += (parseFloat(add));
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'P') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - ((parseFloat(totalamt) * parseFloat(percentage)) / 100);
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'S') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - $scope.OrderAttrs[i].or_atamount;
                        }
                    }
                }

                if ($scope.temp['invs058_order_apply_aexp'] == false) {//if checkbox is not clicked

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'P') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - ((parseFloat(totalamt) * parseFloat(percentage)) / 100);
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'S') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - $scope.OrderAttrs[i].or_atamount;
                        }


                        //if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                        //    if ($scope.OrderAttrs[i].or_atamount != '')
                        //        var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                        //    else
                        //        var add = 0;

                        //    totalamt += (parseFloat(add));
                        //}

                    }

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var add = 0;

                            totalamt += (parseFloat(add));
                        }
                    }
                }


                $scope.totAmt = totalamt;

            }

            $scope.orcalculateamt_old = function (str) {
                debugger;
                console.log(str);
                $scope.totAmt = 0;
                var totalamt = 0;
                var totalamt1 = 0;
                for (var i = 0; i < $scope.testALL.length; i++) {
                    // if ($scope.testALL[i].invs058_rd_line_no1 == true) {
                    if ($scope.temp.servicevalue == 'I' || $scope.temp.servicevalue == 'S') {
                        if ($scope.testALL[i].invs058_ordered_quantity != undefined) {
                            if ($scope.testALL[i].invs058_sup_price != undefined) {

                                totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);

                                if ($scope.testALL[i].invs058_discount != undefined) {
                                    var disamt = ((totalamt1 * $scope.testALL[i].invs058_discount) / 100);
                                    totalamt1 = totalamt1 - disamt;
                                }
                            }
                        }
                    }

                    totalamt = totalamt + totalamt1;
                    totalamt1 = 0;
                    //}
                }

                if (totalamt != "NaN") {
                    $scope.totAmt = parseFloat(totalamt);
                }
                else { $scope.totAmt = 0; }
                console.log(str.or_atamount);
                var o_amt;
                if (str.or_atamount != '' && str.or_atamount != undefined) {
                    o_amt = str.or_atamount;
                    //Percentage
                    if (str.or_atstatus == 'P') {
                        if (str.or_atamount != '') {
                            var disamt = ((parseFloat($scope.totAmt) * parseFloat(str.or_atamount)) / 100);
                            var totalamount = $scope.totAmt - disamt;

                            if (totalamount != "NaN") {
                                $scope.totAmt = parseFloat(totalamount);
                            }
                        }
                        str.or_atamount = o_amt;
                    }

                    //Amount
                    if (str.or_atstatus == 'A') {
                        if (str.or_atamount != undefined) {
                            o_amt = str.or_atamount;
                            var totalamount = parseFloat($scope.totAmt) + parseFloat(str.or_atamount);
                            if (totalamount != "NaN") {
                                $scope.totAmt = parseFloat(totalamount);
                            }
                            str.or_atamount = o_amt;
                        }
                    }
                }
                else
                    str.or_atamount = 0;
            }

            $scope.considerVat = function (obj) {
                debugger;
              
                if (obj.consider_vat == true) {
                    $scope.cloneObj = angular.copy(obj);
                    console.log("clone obj", $scope.cloneObj);
                    obj.im_item_vat_percentage = 0;
                    obj.dd_item_vat_amount = 0;                    
                    //obj.invs058_ordered_quantity = "";
                    //obj.invs058_sup_price = "";
                    //obj.invs058_discount = "";
                    //obj.invs058_discount_amt = "";
                }
                else {
                    obj.im_item_vat_percentage = $scope.cloneObj.im_item_vat_percentage;
                    obj.dd_item_vat_amount = $scope.cloneObj.dd_item_vat_amount;
                }
                $scope.orcalculateamt12();
                console.log("after obj", obj);
                console.log("after clone obj", $scope.cloneObj);
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $scope.CheckAllService = function () {
                var main1 = document.getElementById('mainchk1');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if (main1.checked == true) {
                        //var chkitem = document.getElementById('test-'+i);
                        //chkitem.checked = true;

                        $scope.filteredTodos[i]['req_no1'] = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                    else {
                        //var chkitem = document.getElementById('test-'+i);
                        //chkitem.checked = false;
                        main1.checked = false;
                        $scope.filteredTodos[i]['req_no1'] = false;;

                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }
            }

            $scope.checkonebyoneselectServices = function () {

                $("input[type='checkbox']").change(function (e) {

                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main1 = document.getElementById('mainchk1');
                if (main1.checked == true) {
                    main1.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.CheckAllItems = function () {
                debugger;
                var main1 = document.getElementById('itemSearchChk1');
                for (var i = 0; i < $scope.GetItems.length; i++) {
                    if (main1.checked == true) {
                        //var chkitem = document.getElementById('test-'+i);
                        //chkitem.checked = true;

                        $scope.GetItems[i]['im_inv_chk'] = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                    else {
                        //var chkitem = document.getElementById('test-'+i);
                        //chkitem.checked = false;
                        main1.checked = false;
                        $scope.GetItems[i]['im_inv_chk'] = false;;

                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }
            }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


        }])

})();
