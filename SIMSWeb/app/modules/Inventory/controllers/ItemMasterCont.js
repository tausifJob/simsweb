﻿(function () {
    'use strict';
    var opr = '';
    var itemcode = [];
    var main;
    var data1 = [];
    var data = [];
    var itemset;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ItemMasterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {
            itemset = document.getElementById("chk_itemset");
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.table1 = false;
            $scope.div_update = false;
            $scope.operation = false;
            $scope.editmode = false;
            $scope.global_search = true;
            $scope.showmain_chk = true;
            $scope.CheckItemSet = function () {
               
                itemset = document.getElementById("chk_itemset");
                if (itemset.checked == true) {
                    $scope.supdis = true;
                    $scope.edt = {};
                    $scope.edt =
                        {
                            sg_name: $scope.SupplierGroupNamewithouitem_data[0].sg_name,
                            im_model_name: 'NA'
                        }

                    $scope.edt['im_assembly_ind'] = true;
                }
                else {
                    $scope.supdis = false;
                    $scope.edt = {};
                    $scope.edt['im_assembly_ind'] = false;

                }
            }

            $scope.percentage = function (str) {
                debugger
                if (str > 100) {

                    swal({ title: "Alert", text: "VAT% less than 100", showCloseButton: true, width: 380, })
                    $scope.edt.im_Vat_Percentage = '';
                }
                else {
                    $scope.edt.im_Vat_Percentage = str;
                }
            }
            $scope.chk_item = function (str) {
                $http.get(ENV.apiUrl + "api/ItemMasterDetails/getChkitemPresent?item_code=" + str).then(function (res) {
                    try {
                        if (res.data) {
                            swal({ text: 'Item code is already exist..', imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                            $scope.edt['im_item_code'] = '';
                        }
                    }
                    catch (ex) { }

                });
            }
            //$scope.refreshingSellPrice() = function () {
            //    if (str=='including') {
            //        $scope.edt.im_including_vat = '';
            //    }
            //    else if (str == 'sell') {
            //        $scope.edt.im_sellprice_special = '';
            //    }
            //}
            $scope.refreshingSellPrice = function (str) {
                debugger;   
                if (str == 1) {
                    if ($scope.edt.im_sell_price_including_vat == '' || parseFloat($scope.edt.im_sell_price_including_vat) == 0 || $scope.edt.im_sell_price_including_vat == undefined) {
                    }
                    else {
                        $scope.edt.im_sell_price = '';
                    }                    
                }
                else if (str == 2) {
                    if ($scope.edt.im_sell_price == '' || parseFloat($scope.edt.im_sell_price) == 0 || $scope.edt.im_sell_price == undefined) {
                    }
                    else {
                        $scope.edt.im_sell_price_including_vat = '';
                    }                    
                }
            }
            $scope.Show_Data = function (im_inv_no, im_desc, im_item_code, dep_code, sec_code, sup_code, sg_name, pc_code) {
                debugger;
                if (im_inv_no != null) {
                    $scope.temp1.im_inv_no = im_inv_no;
                }
                var data = $scope.temp1;           

                $http.get(ENV.apiUrl + "api/ItemMasterDetails/getItemMasterDetails?data=" + JSON.stringify(data)).then(function (getItemMasterDetails_Data) {
                    $scope.ItemMasterDetail = getItemMasterDetails_Data.data;
                    $scope.totalItems = $scope.ItemMasterDetail.length;
                    $scope.todos = $scope.ItemMasterDetail;
                    $scope.makeTodos();
                    if ($scope.ItemMasterDetail < 1) {
                        swal({ text: "Sorry, There Is No Data Found..!", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });

                        $scope.global_search = true;
                        $scope.table1 = false;
                    }
                    else {
                        $scope.global_search = true;
                        $scope.table1 = true;
                    }
                });
            }
          

            $http.get(ENV.apiUrl + "api/ItemMasterDetails/getSupplierGroupName").then(function (getSupplierGroupName_Data) {
                $scope.SupplierGroupName = getSupplierGroupName_Data.data;
            });

            $http.get(ENV.apiUrl + "api/ItemMasterDetails/getSupplierGroupNamewithouitem").then(function (getSupplierGroupNamewithouitem_data) {
                $scope.SupplierGroupNamewithouitem_data = getSupplierGroupNamewithouitem_data.data;
            });

            $http.get(ENV.apiUrl + "api/CreateCostingSheet/getDepartments").then(function (getDepartments_Data) {
                $scope.Departments_Data = getDepartments_Data.data;
            });

            $scope.getSectionNames = function () {

                $http.get(ENV.apiUrl + "api/ItemMasterDetails/getSectionName?dept_code=" + $scope.edt.dep_code).then(function (getSectionName_Data) {
                    $scope.SectionName = getSectionName_Data.data;
                });
            }

            $scope.getSectionNamesbydeptcode = function () {

                $http.get(ENV.apiUrl + "api/ItemMasterDetails/getSectionName?dept_code=" + $scope.temp1.dep_code).then(function (getSectionName_Data) {
                    $scope.SectionName = getSectionName_Data.data;
                });
            }

            $http.get(ENV.apiUrl + "api/ItemMasterDetails/getProductDesc").then(function (getProductDesc_Data) {
                $scope.ProductDesc = getProductDesc_Data.data;
            });

            $http.get(ENV.apiUrl + "api/CreateCostingSheet/getSupplier").then(function (getSupplier_Data) {
                $scope.Supplier_Data = getSupplier_Data.data;
            });

            $http.get(ENV.apiUrl + "api/ItemMasterDetails/getUnitOfMeasurement").then(function (getUnitOfMeasurement_Data) {
                $scope.UnitOfMeasurement = getUnitOfMeasurement_Data.data;
            });

            $http.get(ENV.apiUrl + "api/ItemMasterDetails/getTradeCategory").then(function (getTradeCategory_Data) {
                $scope.TradeCategory = getTradeCategory_Data.data;
            });

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage);
                //$scope.makeTodos();

                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['check_all'] = false;
                $scope.row1 = '';
            }

            $scope.New = function () {
                $scope.global_search = false;
                $scope.check = true;
                $scope.editmode = false;
                $scope.newmode = true;
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.div_update = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.temp = {}
                $scope.temp['enteritemset'] = false;

                $scope.supdis = false;

                $scope.edt = {

                    sg_name: "",
                    im_item_code: "",
                    im_desc: "",
                    im_model_name: "",
                    dep_code: "",
                    sec_code: "",
                    pc_code: "",
                    sup_code: "",
                    uom_code: "",
                    uom_code_has: "",
                    im_supl_catalog_price: "",
                    im_supl_price_date: "",
                    im_supl_min_qty: "",
                    im_supl_buying_price: "",
                    im_supl_buying_price_old: "",
                    im_sell_min_qty: "",
                    im_min_level: "",
                    im_max_level: "",
                    im_reorder_level: "",
                    im_economic_order_qty: "",
                    im_rso_qty: "",
                    im_average_month_demand: "",
                    im_malc_rate: "",
                    im_malc_rate_old: "",
                    im_mark_up: "",
                    im_sell_price: "",
                    im_sell_price_old: "",
                    im_sellprice_special: "",
                    im_sellprice_freeze_ind: "",
                    im_approximate_price: "",
                    im_estimated_price: "",
                    im_creation_date: "",
                    im_stock_check_date: "",
                    im_no_of_packing: "",
                    im_last_receipt_date: "",
                    im_last_issue_date: "",
                    im_last_supp_code: "",
                    im_approximate_lead_time: "",
                    im_manufact_serial_no: "",
                    im_last_supp_cur: "",
                    im_trade_cat: "",
                    im_one_time_flag: "",
                    im_agency_flag: "",
                    im_assembly_ind: "",
                    im_proprietary_flag: "",
                    im_reusable_flag: "",
                    im_supersed_ind: "",
                    im_obsolete_excess_ind: "",
                    im_invest_flag: ""
                };

                var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');
                debugger;
                $scope.edt = {
                    im_creation_date: dateyear,
                }

            }

            $scope.up = function (str) {
                debugger
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.readonly = true;
                $scope.global_search = false;
                $scope.div_update = true;
                $scope.edt = {
                    im_inv_no: str.im_inv_no,
                    sg_name: str.sg_name,
                    sg_desc: str.sg_desc,
                    im_item_code: str.im_item_code,
                    im_desc: str.im_desc,
                    im_model_name: str.im_model_name,
                    dep_code: str.dep_code,
                    dep_name: str.dep_name,
                    sec_code: str.sec_code,
                    sec_name: str.sec_name,
                    pc_code: str.pc_code,
                    pc_desc: str.pc_desc,
                    sup_code: str.sup_code,
                    sup_name: str.sup_name,
                    uom_code: str.uom_code,
                    uom_name: str.uom_name,
                    uom_code_has: str.uom_code_has,
                    uom_name_has: str.uom_name_has,
                    im_supl_catalog_price: str.im_supl_catalog_price,
                    im_supl_price_date: str.im_supl_price_date,
                    im_supl_min_qty: str.im_supl_min_qty,

                    im_supl_buying_price: str.im_supl_buying_price,
                    im_supl_buying_price_old: str.im_supl_buying_price,
                    im_min_level: str.im_min_level,
                    im_max_level: str.im_max_level,
                    im_reorder_level: str.im_reorder_level,
                    im_economic_order_qty: str.im_economic_order_qty,
                    im_rso_qty: str.im_rso_qty,
                    im_average_month_demand: str.im_average_month_demand,
                    im_malc_rate: str.im_malc_rate,
                    im_malc_rate_old: str.im_malc_rate,

                    im_mark_up: str.im_mark_up,
                    im_sell_min_qty: str.im_sell_min_qty,

                    im_sell_price: str.im_sell_price,
                    im_sell_price_old: str.im_sell_price,

                    im_sellprice_special: str.im_sellprice_special,
                    im_sellprice_freeze_ind: str.im_sellprice_freeze_ind,
                    im_approximate_price: str.im_approximate_price,
                    im_estimated_price: str.im_estimated_price,
                    im_creation_date: str.im_creation_date,
                    im_stock_check_date: str.im_stock_check_date,
                    im_no_of_packing: str.im_no_of_packing,
                    im_last_receipt_date: str.im_last_receipt_date,
                    im_last_issue_date: str.im_last_issue_date,
                    im_last_supp_code: str.im_last_supp_code,
                    im_approximate_lead_time: str.im_approximate_lead_time,
                    im_manufact_serial_no: str.im_manufact_serial_no,
                    im_last_supp_cur: str.im_last_supp_cur,
                    im_trade_cat: str.im_trade_cat,
                    trade_cat_name: str.trade_cat_name,
                    im_one_time_flag: str.im_one_time_flag,
                    im_agency_flag: str.im_agency_flag,
                    im_assembly_ind: str.im_assembly_ind,
                    im_proprietary_flag: str.im_proprietary_flag,
                    im_reusable_flag: str.im_reusable_flag,
                    im_supersed_ind: str.im_supersed_ind,
                    im_obsolete_excess_ind: str.im_obsolete_excess_ind,
                    im_invest_flag: str.im_invest_flag,
                    im_img: str.im_img,
                    im_status_ind: str.im_status_ind,
                    im_status_date: str.im_status_date,
                    im_Vat_Percentage: str.im_Vat_Percentage,
                    im_sell_price_including_vat: str.im_sell_price_including_vat,                    
                }
                $scope.temp = {}
                if ($scope.edt.sg_name == null || $scope.edt.sg_name == "" || $scope.edt.sg_name == '00') {

                    $scope.temp['enteritemset'] = true;
                    $scope.supdis = true;
                }
                else {

                    $scope.temp['enteritemset'] = false;
                    $scope.supdis = false;
                }
                $scope.getSectionNames();
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.global_search = true;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

                $scope.edt = {

                    sg_name: "",
                    im_item_code: "",
                    im_desc: "",
                    im_model_name: "",
                    dep_code: "",
                    sec_code: "",
                    pc_code: "",
                    sup_code: "",
                    uom_code: "",
                    uom_code_has: "",
                    im_supl_catalog_price: "",
                    im_supl_price_date: "",
                    im_supl_min_qty: "",
                    im_supl_buying_price: "",
                    im_supl_buying_price_old: "",
                    im_sell_min_qty: "",
                    im_min_level: "",
                    im_max_level: "",
                    im_reorder_level: "",
                    im_economic_order_qty: "",
                    im_rso_qty: "",
                    im_average_month_demand: "",
                    im_malc_rate: "",
                    im_malc_rate_old: "",
                    im_mark_up: "",
                    im_sell_price: "",
                    im_sell_price_old: "",
                    im_sellprice_special: "",
                    im_sellprice_freeze_ind: "",
                    im_approximate_price: "",
                    im_estimated_price: "",
                    im_creation_date: "",
                    im_stock_check_date: "",
                    im_no_of_packing: "",
                    im_last_receipt_date: "",
                    im_last_issue_date: "",
                    im_last_supp_code: "",
                    im_approximate_lead_time: "",
                    im_manufact_serial_no: "",
                    im_last_supp_cur: "",
                    im_trade_cat: "",
                    im_one_time_flag: "",
                    im_agency_flag: "",
                    im_assembly_ind: "",
                    im_proprietary_flag: "",
                    im_reusable_flag: "",
                    im_supersed_ind: "",
                    im_obsolete_excess_ind: "",
                    im_invest_flag: ""
                };
            }

            $scope.Reset = function () {
                $scope.table1 = false;
                $scope.temp1 = {
                    im_inv_no: '',
                    im_desc: '',
                    im_item_code: '',
                    dep_code: '',
                    sec_code: '',
                    sup_code: '',
                    sg_name: '',
                    pc_code: ''
                }

                $scope.filteredTodos = [];
                $scope.SectionName = [];
            }

            $scope.Save = function (myForm) {
                debugger;   
                var vat_calculation = [];
                debugger
                itemset = document.getElementById("chk_itemset");
                if (itemset.checked == true) {
                    if (myForm) {
                        data1 = [];
                        var data = [];
                        if ($scope.edt.im_Vat_Percentage != null) {
                            vat_calculation = 'true';
                        }
                        else if ($scope.edt.im_Vat_Percentage != undefined) {
                            vat_calculation = 'true';
                        }
                        else {
                            vat_calculation = 'false';
                        }
                        
                        data = $scope.edt;
                        data.opr = 'I';
                        data1.push(data);
                        var temp_im_inv_no = data.im_inv_no;
                        if (vat_calculation == 'true') {
                            $http.post(ENV.apiUrl + "api/ItemMasterDetails/ItemMasterDetailsCUD_VAT", data1).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                    $scope.Show_Data(temp_im_inv_no);                                
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                                else {
                                    swal({ text: $scope.msg1, imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                                
                            });
                        }
                        else {
                            $http.post(ENV.apiUrl + "api/ItemMasterDetails/ItemMasterDetailsCUD", data1).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                    $scope.Show_Data(temp_im_inv_no);
                                }
                                else if($scope.msg1 == false){
                                    swal({ text: "Record not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                                else {
                                       swal({ text: $scope.msg1, imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                            });
                        }                        
                        $scope.table1 = true;
                        $scope.global_search = true;
                        $scope.operation = false;
                    }
                }
                else {
                    if ($scope.edt == undefined) {
                        $scope.edt = '';
                    }
                    if ($scope.edt.sg_name == null || $scope.edt.sg_name == '') {
                        swal({ title: "Alert", text: "Please select Supplier Group name", width: 300, height: 200 });
                    }
                    else {
                        if (myForm) {
                            data1 = [];
                            var data = [];
                            if ($scope.edt.im_Vat_Percentage != null) {
                                vat_calculation = 'true';
                            }
                            else if ($scope.edt.im_Vat_Percentage != undefined) {
                                vat_calculation = 'true';
                            }
                            else {
                                vat_calculation = 'false';
                            }
                            data = $scope.edt;
                            data.opr = 'I';
                            data1.push(data);
                            var temp_im_inv_no = data.im_inv_no;
                            if (vat_calculation == 'true') {
                                $http.post(ENV.apiUrl + "api/ItemMasterDetails/ItemMasterDetailsCUD_VAT", data1).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if($scope.msg1 == true) {
                                        swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                        $scope.Show_Data(temp_im_inv_no);
                                    }
                                    else if($scope.msg1 == false) {
                                        swal({ text: "Record not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                    }
                                    else {
                                        swal({ text: $scope.msg1, imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                    }
                                });
                            }
                            else {
                                $http.post(ENV.apiUrl + "api/ItemMasterDetails/ItemMasterDetailsCUD", data1).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                        $scope.Show_Data(temp_im_inv_no);
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Record not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                    }
                                    else {
                                        swal({ text: $scope.msg1, imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                    }
                                });
                            }                          
                            $scope.table1 = true;
                            $scope.global_search = true;
                            $scope.operation = false;
                        }

                    }
                }

            }
           
            $scope.update = function (myForm) {
                debugger
                var vat_calculation = [];
                if (myForm) {

                    data1 = [];
                    var data = [];
                    if ($scope.edt.im_Vat_Percentage != null) {
                        vat_calculation = 'true';
                    }
                    else if ($scope.edt.im_Vat_Percentage != 'undefined') {
                        vat_calculation = 'true';
                    }
                    else {
                        vat_calculation = 'false';
                    }
                    data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    var temp_im_inv_no = data.im_inv_no;
                    if (vat_calculation == 'true') {
                        $http.post(ENV.apiUrl + "api/ItemMasterDetails/ItemMasterDetailsCUD_VAT", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            $scope.Show_Data(temp_im_inv_no);
                        });
                    }
                    else {
                        $http.post(ENV.apiUrl + "api/ItemMasterDetails/ItemMasterDetailsCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                $scope.Show_Data(temp_im_inv_no);
                            }
                            else {
                                swal({ text: "Record not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                        });
                    }
                    $scope.operation = false;
                    $scope.table1 = true;
                    $scope.global_search = true;
                }

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].im_inv_no + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].im_inv_no + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function (str) {
                debugger;
                if (str.ischecked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        if ($scope.filteredTodos[i].im_inv_no == str.im_inv_no) {
                            str.ischecked = true;
                            str.row_selected = '#ffffcc';

                        }
                        else {
                            $scope.filteredTodos[i].ischecked = false;
                            $scope.filteredTodos[i].row_selected = '#fff';
                        }
                    }
                }
                else {
                    str.ischecked = false;
                    str.row_selected = '#fff';
                }

                //$("input[type='checkbox']").change(function (e) {
                //    if ($(this).is(":checked")) {
                //        $(this).closest('tr').addClass("row_selected");
                //        $scope.color = '#edefef';
                //    } else {
                //        $(this).closest('tr').removeClass("row_selected");
                //        $scope.color = '#edefef';
                //    }
                //});

                //main = document.getElementById('mainchk');
                //if (main.checked == true) {
                //    main.checked = false;
                //    $scope.color = '#edefef';
                //    $scope.row1 = '';
                //}               
            }

            $scope.delrec = function () {
                itemcode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById('test-' + $scope.filteredTodos[i].im_inv_no + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletecode = ({
                            'im_inv_no': $scope.filteredTodos[i].im_inv_no,
                            opr: 'D'
                        });
                        itemcode.push(deletecode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/ItemMasterDetails/ItemMasterDetailsCUD", itemcode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                    $scope.Show_Data();
                                }
                                else {
                                    swal({ text: "Record not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                                //if ($scope.msg1 == true) {
                                //swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                //swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                //    if (isConfirm) {

                                //        $scope.Show_Data();
                                //        //$http.get(ENV.apiUrl + "api/ItemMasterDetails/getItemMasterDetails").then(function (getItemMasterDetails_Data) {
                                //        //    $scope.ItemMasterDetail = getItemMasterDetails_Data.data;
                                //        //    $scope.totalItems = $scope.ItemMasterDetail.length;
                                //        //    $scope.todos = $scope.ItemMasterDetail;
                                //        //    $scope.makeTodos();
                                //        //});
                                //    }
                                //});
                                //}
                                //else {
                                //    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                //        if (isConfirm) {

                                //            $scope.Show_Data();
                                //            //$http.get(ENV.apiUrl + "api/ItemMasterDetails/getItemMasterDetails").then(function (getItemMasterDetails_Data) {
                                //            //    $scope.ItemMasterDetail = getItemMasterDetails_Data.data;
                                //            //    $scope.totalItems = $scope.ItemMasterDetail.length;
                                //            //    $scope.todos = $scope.ItemMasterDetail;
                                //            //    $scope.makeTodos();
                                //            //});
                                //        }
                                //    });
                                //}

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].im_inv_no + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.Show_Data();
                //$scope.currentPage = true;
                $scope.global_search = true;
                $scope.table1 = true;
                main.checked = false;

            }

            $scope.deleterecord1 = function () {
                itemcode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].im_inv_no + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletecode = ({
                            'im_inv_no': $scope.filteredTodos[i].im_inv_no,
                            opr: 'D'
                        });
                        itemcode.push(deletecode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/ItemMasterDetails/ItemMasterDetailsCUD", itemcode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                    $scope.Show_Data();
                                }
                                else {
                                    swal({ text: "Record not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                                //if ($scope.msg1 == true) {
                                //swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                //swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                //    if (isConfirm) {

                                //        $scope.Show_Data();
                                //        //$http.get(ENV.apiUrl + "api/ItemMasterDetails/getItemMasterDetails").then(function (getItemMasterDetails_Data) {
                                //        //    $scope.ItemMasterDetail = getItemMasterDetails_Data.data;
                                //        //    $scope.totalItems = $scope.ItemMasterDetail.length;
                                //        //    $scope.todos = $scope.ItemMasterDetail;
                                //        //    $scope.makeTodos();
                                //        //});
                                //    }
                                //});
                                //}
                                //else {
                                //    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                //        if (isConfirm) {

                                //            $scope.Show_Data();
                                //            //$http.get(ENV.apiUrl + "api/ItemMasterDetails/getItemMasterDetails").then(function (getItemMasterDetails_Data) {
                                //            //    $scope.ItemMasterDetail = getItemMasterDetails_Data.data;
                                //            //    $scope.totalItems = $scope.ItemMasterDetail.length;
                                //            //    $scope.todos = $scope.ItemMasterDetail;
                                //            //    $scope.makeTodos();
                                //            //});
                                //        }
                                //    });
                                //}

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].im_inv_no + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.Show_Data();
                //$scope.currentPage = true;
                $scope.global_search = true;
                $scope.table1 = true;
                main.checked = false;

            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.ItemMasterDetail, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.ItemMasterDetail;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                return (
                     item.sg_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.im_item_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.im_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.dep_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                     item.sec_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.pc_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sup_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.uom_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.im_min_level.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                     item.im_inv_no == toSearch) ? true : false;
            }

            $scope.View = function (obj) {
                debugger
                $('#MyModal').modal('show');
                //for (var i = 0; i < $scope.filteredTodos.length; i++) {
                //    if(obj.im_inv_no==$scope.filteredTodos[i].im_inv_no)
                //    $scope.im_inv_no = $scope.filteredTodos[i].im_inv_no;
                //}
                $scope.im_model_name = obj.im_model_name;
                $scope.uom_name = obj.uom_name;
                $scope.uom_name_has = obj.uom_name_has;
                $scope.im_supl_catalog_price = obj.im_supl_catalog_price;
                $scope.im_supl_price_date = obj.im_supl_price_date;
                $scope.im_supl_min_qty = obj.im_supl_min_qty;
                $scope.im_supl_buying_price = obj.im_supl_buying_price;

                $scope.im_sell_min_qty = obj.im_sell_min_qty;
                $scope.im_min_level = obj.im_min_level;
                $scope.im_max_level = obj.im_max_level;
                $scope.im_reorder_level = obj.im_reorder_level;
                $scope.im_economic_order_qty = obj.im_economic_order_qty;
                $scope.im_rso_qty = obj.im_rso_qty;
                $scope.im_average_month_demand = obj.im_average_month_demand;
                $scope.im_malc_rate = obj.im_malc_rate;

                $scope.im_mark_up = obj.im_mark_up;
                $scope.im_sell_price = obj.im_sell_price;

                $scope.im_sellprice_special = obj.im_sellprice_special;
                $scope.im_sellprice_freeze_ind = obj.im_sellprice_freeze_ind;
                $scope.im_approximate_price = obj.im_approximate_price;
                $scope.im_estimated_price = obj.im_estimated_price;

                $scope.im_creation_date = obj.im_creation_date;
                $scope.im_stock_check_date = obj.im_stock_check_date;
                $scope.im_no_of_packing = obj.im_no_of_packing;
                $scope.im_approximate_lead_time = obj.im_approximate_lead_time;
                $scope.im_manufact_serial_no = obj.im_manufact_serial_no;
                $scope.trade_cat_name = obj.trade_cat_name;
                $scope.im_one_time_flag = obj.im_one_time_flag;
                $scope.im_agency_flag = obj.im_agency_flag;
                $scope.im_assembly_ind = obj.im_assembly_ind;
                $scope.im_proprietary_flag = obj.im_proprietary_flag;

                $scope.im_reusable_flag = obj.im_reusable_flag;
                $scope.im_supersed_ind = obj.im_supersed_ind;
                $scope.im_obsolete_excess_ind = obj.im_obsolete_excess_ind;
                $scope.im_invest_flag = obj.im_invest_flag;
                $scope.im_status_ind = obj.im_status_ind;
                $scope.im_Vat_Percentage = obj.im_Vat_Percentage;
                $scope.im_sell_price_including_vat = obj.im_sell_price_including_vat;
                $scope.ItemDetail = true;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


        }])
})();





