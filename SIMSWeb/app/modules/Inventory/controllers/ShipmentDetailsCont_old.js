﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Inventory');
    simsController.controller('ShipmentDetailsCont',
        ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.display = true;
            $scope.grid = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            var str, cnt;
            var data1 = [];
            var data = [];
            $scope.edit_code = false;
            $scope.head_teacher_mapping = [];
            var main, autoid;
            $scope.btn_save = true;
            $scope.btn_delete = false;
            var head_teacher_code = [];
            $scope.Order_lists = [];
            $scope.test_shift = [];
            $scope.orderdetails_data = [];
            $scope.shipment_lists = [];
            var date = new Date();
            $scope.pager = false;
            // $scope.btn_order = false;
            $scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');
            console.log($scope.ddMMyyyy);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100)

            $timeout(function () {
                $("#fixTable2").tableHeadFixer({ 'top': 1 });
            }, 100)

            $scope.countData = [
                    { val: 5, data: 5 },
                    { val: 10, data: 10 },
                    { val: 15, data: 15 },
            ]


            $http.get(ENV.apiUrl + "api/common/ShipmentDetails/GetShipmentId").then(function (res) {
                autoid = res.data;
                // $scope.edt['Invs051_sr_shipment_no']=autoid
                $scope.edt =
                    {
                        invs051_sr_shipment_no: autoid,
                        invs051_sr_bill_of_lading_date: $scope.ddMMyyyy,
                    }
            });

            $http.get(ENV.apiUrl + "api/common/ShipmentDetails/Get_DeliveryMode").then(function (res) {
                $scope.deliveryMode_data = res.data;
                console.log($scope.deliveryMode_data);
            });

            $http.get(ENV.apiUrl + "api/common/ShipmentDetails/Get_SupplierCode").then(function (res) {
                $scope.supplier_data = res.data;
                console.log($scope.supplier_data);
            });

            $scope.Order_details = function () {
                $('#OrderModal').modal('show');

                $http.get(ENV.apiUrl + "api/common/ShipmentDetails/Get_Search_Order").then(function (res) {
                    $scope.order_data = res.data;
                    console.log($scope.order_data);
                });
            }

            $scope.search_shipdetails = function () {
                // $scope.btn_order = true;
                $scope.edit_code = true;
                $('#SearchShipmentModal').modal('show');

                $http.get(ENV.apiUrl + "api/common/ShipmentDetails/Get_Search_Shipment").then(function (res) {
                    $scope.shipment_data = res.data;
                    console.log($scope.shipment_data);
                });
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.getOrderdetails = function () {
                var v = '';
                var unique = [];
                $scope.orderdetails_data = [];
                $scope.pager = true;
                for (var i = 0; i < $scope.order_data.length; i++) {
                    if ($scope.order_data[i].invs051_ord_no1 == true) {
                        var found = false;
                        for (var j = 0; j < $scope.filteredTodos.length; j++) {
                            if ($scope.order_data[i].invs051_ord_no == $scope.filteredTodos[j].ord_no) {
                                found = true;
                                break;
                            }
                        }

                        if (found == false) {
                            unique.push($scope.order_data[i].invs051_ord_no);
                        }
                    }
                }

                console.log(unique);
                for (var i = 0; i < unique.length; i++) {
                    v += unique[i] + ',';
                }

                if (v != "") {
                    $http.get(ENV.apiUrl + "api/common/ShipmentDetails/getSearch_Order_Details?ob=" + v).then(function (res) {
                        $scope.order_Details = res.data.orderDetails;
                        $scope.totalItems = $scope.order_Details.length;
                        $scope.todos = $scope.order_Details;

                        for (var i = 0; i < $scope.totalItems; i++) {
                            $scope.orderdetails_data.push($scope.order_Details[i]);
                        }
                        $scope.makeTodos();
                        console.log($scope.orderdetails_data);
                    });
                }
            }

            $scope.getAllShipmentDetails = function () {
                debugger;
                $scope.orderdetails_data = [];
                $scope.pager = true;
                //$scope.filteredTodos = [];
                for (var i = 0; i < $scope.shipment_data.length; i++) {
                    if ($scope.shipment_data[i].invs051_sr_shipment_no1 == true) {
                        $scope.shipment_lists.push($scope.shipment_data[i]);
                        $scope.test_shift = $scope.shipment_data[i];
                        console.log($scope.test_shift);
                    }

                }

                $http.post(ENV.apiUrl + "api/common/ShipmentDetails/Search_Shipment_Details", $scope.shipment_lists).then(function (res) {
                    debugger;
                    $scope.shipmentdetails_data = res.data;
                    $scope.orderdetails_data = $scope.shipmentdetails_data.shipmentOrder;
                    $scope.totalItems = $scope.orderdetails_data.length;
                    $scope.todos = $scope.orderdetails_data;
                    $scope.makeTodos();
                    console.log($scope.orderdetails_data);

                    $scope.edt =
                        {
                            invs051_sr_shipment_no: $scope.test_shift.invs051_sr_shipment_no,
                            invs051_sr_party_ref: $scope.test_shift.invs051_sr_party_ref,
                            invs051_sr_bill_of_lading_date: $scope.test_shift.invs051_sr_bill_of_lading_date,
                            invs051_sup_code: $scope.test_shift.invs051_sup_code,
                            invs051_dm_code: $scope.test_shift.invs051_dm_code,
                            invs051_sr_remarks: $scope.test_shift.invs051_sr_remarks
                        }
                    // console.log($scope.test_shift.push($scope.orderdetails_data));
                });
            }


            $scope.get_ischecked = function (shift) {
                shift.ischecked = true;
            }

            $scope.Save = function () {
                data1 = [];
                data = [];
                debugger

                if (!jQuery.isEmptyObject($scope.orderdetails_data)) {
                    var data = ({
                        invs051_sr_shipment_no: $scope.edt.invs051_sr_shipment_no,
                        invs051_sr_party_ref: $scope.edt.invs051_sr_party_ref,
                        invs051_sr_remarks: $scope.edt.invs051_sr_remarks,
                        invs051_sr_bill_of_lading_date: $scope.edt.invs051_sr_bill_of_lading_date,
                        invs051_sup_code: $scope.edt.invs051_sup_code,
                        invs051_dm_code: $scope.edt.invs051_dm_code,
                        ShipmentOrder: $scope.orderdetails_data,
                    });

                    data1.push(data);
                    // }

                    $http.post(ENV.apiUrl + "api/common/ShipmentDetails/CUDInsertShipment_regiser", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == '5') {
                            swal({ text: "Shipment Details Added Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                            $scope.Cancel();
                        }
                        else if ($scope.msg1 == '2') {
                            swal({ title: "Alert", text: "Sorry!!Please Check All The Mandatory Fields", showCloseButton: true, width: 380, });
                            $scope.Cancel();
                        }
                        else if ($scope.msg1 == '4') {
                            swal({ text: "Shipment Register Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                            $scope.Cancel();
                        }
                        else if ($scope.msg1 == '6') {
                            swal({ text: "Shipment Details Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                            $scope.Cancel();
                        }
                    });
                }
                else {
                    swal({ text: "Select Atleast One Order", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
            }

            $scope.Delete = function () {
                swal({
                    title: '',
                    text: "Are you sure you want to Delete?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        $http.post(ENV.apiUrl + "api/common/ShipmentDetails/CUDeleteShipment_details?shift_no=" + $scope.edt.invs051_sr_shipment_no).then(function (res) {
                            debugger
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Shipment Details Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                                $scope.Cancel();
                            }
                            else {
                                swal({ text: "Shipment Details Not Deleted Successfully", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                                $scope.Cancel();
                            }
                        });
                    }
                });
            }

            $scope.Cancel = function () {
                debugger
                $scope.edit_code = false;
                $scope.filteredTodos = [];
                $scope.orderdetails_data = [];
                $scope.order_details = '';
                $scope.edt = '';
                //$scope.makeTodos();
                $scope.todos = [];
                $scope.pager = false;
                $http.get(ENV.apiUrl + "api/common/ShipmentDetails/GetShipmentId").then(function (res) {
                    autoid = res.data;
                    $scope.edt =
                        {
                            invs051_sr_shipment_no: autoid,
                            invs051_sr_bill_of_lading_date: $scope.ddMMyyyy,
                        }
                });
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage);
                $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.orderdetails_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.orderdetails_data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.im_item_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.im_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }])


})();