﻿/// <reference path="../../HRMS/controller/AttendanceMachine.js" />

(function () {

    'use strict';
    var temp = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StoreIssueCont',
      ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
          $scope.temp = [];
          $scope.edt = [];
          var insertDetailData = [];
          var uname = $rootScope.globals.currentUser.username;
          $scope.expand_object = false;

          var today = new Date();
          today = ('0' + today.getDate()).slice(-2) + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + today.getFullYear();
          $scope.temp["to_date"] = today
          $scope.temp["from_date"] = today
          $scope.temp["issue_creation_date"] = today
          $scope.hide_submit = false;
          $http.get(ENV.apiUrl + "api/StoreIssueController/GetCur").then(function (GetCur) {
              $scope.GetCur = GetCur.data;
              $scope.edt = { sims_cur_code: $scope.GetCur[0].sims_cur_code }
              $scope.select_cur($scope.GetCur[0].sims_cur_code);
          });

          $scope.select_cur = function (cur) {
              $http.get(ENV.apiUrl + "api/StoreIssueController/GetAca?cur=" + cur).then(function (GetAca) {
                  $scope.GetAca = GetAca.data;
                  $scope.edt['sims_acaedmic_year'] = $scope.GetAca[0].sims_acaedmic_year;
                  $scope.select_aca($scope.GetAca[0].sims_acaedmic_year)
              });
          }
          $scope.select_aca = function (aca) {
              $http.get(ENV.apiUrl + "api/StoreIssueController/Getgrd?cur=" + $scope.edt.sims_cur_code + "&aca=" + aca + "&user=" + uname).then(function (Getgrd) {
                  $scope.Getgrd = Getgrd.data;

                  setTimeout(function () {
                      $('#cmb_grade').change(function () {
                          console.log($(this).val());
                      }).multipleSelect({
                          width: '100%'
                      });
                  }, 1000);

              });
          }
          $scope.select_grd = function (grd) {
              $http.get(ENV.apiUrl + "api/StoreIssueController/Getsec?cur=" + $scope.edt.sims_cur_code + "&aca=" + $scope.edt.sims_acaedmic_year + "&grd=" + grd + "&user=" + uname).then(function (Getsec) {
                  $scope.Getsec = Getsec.data;
              });

          }
          $scope.get_Details = function (data) {
              $scope.hide_submit = true;
              debugger;
              $http.get(ENV.apiUrl + "api/StoreIssueController/getDetailsNew?cur=" + data.sims_cur_code + "&aca=" + data.sims_acaedmic_year + "&grd=" + data.sims_grade_code + "&section=" + data.sims_section_code + "&fromdate=" + $scope.temp["from_date"] + "&todate=" + $scope.temp["to_date"]).then(function (getFullData) {
                  $scope.store_issue_details = getFullData.data;
                  if ($scope.store_issue_details.length > 0) {
                      for (var i = 0; i < $scope.store_issue_details.length; i++) {
                          for (var j = 0; j < $scope.store_issue_details[i].sublist1.length; j++) {
                              $scope.store_issue_details[i].sublist1[j].creation_user = $rootScope.globals.currentUser.username;
                              var invoice_qty = $scope.store_issue_details[i].sublist1[j].dd_qty;
                              var current_qty = $scope.store_issue_details[i].sublist1[j].id_cur_qty;
                              var deleverd_qty = $scope.store_issue_details[i].sublist1[j].issued_dd_issue_qty;
                              if (deleverd_qty == '') {
                                  deleverd_qty = 0;
                              }
                              var total_minus_value = 0;
                              if (parseFloat(invoice_qty) < parseFloat(current_qty)) {
                                  $scope.store_issue_details[i].sublist1[j].inner_checked = true;

                                  total_minus_value = parseFloat(invoice_qty) - parseFloat(deleverd_qty)
                                  total_minus_value = parseInt(total_minus_value)
                                  if (parseInt(current_qty) > total_minus_value) {
                                      $scope.store_issue_details[i].sublist1[j].issue_quantity = total_minus_value;
                                  } else {
                                      $scope.store_issue_details[i].sublist1[j].issue_quantity_dis = true;
                                  }
                                  if (total_minus_value == 0) {
                                      $scope.store_issue_details[i].sublist1[j].inner_disabled = true;
                                      $scope.store_issue_details[i].sublist1[j].inner_checked = false;
                                  }
                              }
                              else {
                                  $scope.store_issue_details[i].sublist1[j].inner_disabled = true;
                                  $scope.store_issue_details[i].sublist1[j].issue_quantity_dis = true;
                                  $scope.store_issue_details[i].sublist1[j].remarks_dis = true;
                              }
                          }

                      }
                      $scope.totalItems = $scope.store_issue_details.length;
                      $scope.todos = $scope.store_issue_details;
                      for (var i = 0; i < $scope.totalItems; i++) {
                          $scope.store_issue_details[i].icon = "fa fa-plus-circle";
                      }
                  }
                  else {
                      swal({ text: "Record not found. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                  }
              });

          }
          var dom;
          var count = 0;
          $scope.flag = true;
          var innerTable;

          $scope.expand = function (info, $event) {
              if (info['isexpanded']) {
                  info['isexpanded'] = false;
                  info['sims_icon'] = "fa fa-plus-circle";
              }
              else {
                  info['isexpanded'] = true;
                  info['sims_icon'] = "fa fa-minus-circle";

              }


          }
          $scope.onlyNumbers = function (obj, event) {
              debugger;
              var keys = {

                  'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                  '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
              };


              for (var index in keys) {
                  if (!keys.hasOwnProperty(index)) continue;
                  if (parseInt(event.target.value) < 0) {
                      event.target.value = "";
                  }
                  if (/^\d*$/.test(parseFloat(event.target.value))) {

                  }
                  else {
                      event.target.value = "";
                  }
                  if (parseInt(event.target.value) > parseInt(obj.id_cur_qty)) {
                      event.target.value = "";
                  }
                  if (parseInt(obj.dd_qty) < parseInt(event.target.value)) {
                      event.target.value = "";
                  }
                  if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                      return; //default event
                  }

              }

              event.preventDefault();
          };
          var stroe_report_name = [];
          $http.get(ENV.apiUrl + "api/StoreIssueController/getreport_store_1").then(function (getFullData) {
              $scope.stroe_report_name = getFullData.data;
          });
          var invoice_report_name = [];
          $http.get(ENV.apiUrl + "api/StoreIssueController/getreport_invoice_1").then(function (getFullData) {
              debugger;
              $scope.invoice_report_name = getFullData.data;
          });
          $scope.stor_report = function (row) {
              debugger;

              $http.get(ENV.apiUrl + "api/StoreIssueController/getreport_store?doc_prov_no=" + row.invoice_no + "&enroll_no=" + row.enroll_no + "&sims_cur_code=" + $scope.edt.sims_cur_code + "&sims_acaedmic_year=" + $scope.edt.sims_acaedmic_year + "&doc_prov_date=" + row.doc_prov_date).then(function (report_data) {
                  var doc_sr_code_user = report_data.data;
                  console.log(doc_sr_code_user);
                  console.log($scope.stroe_report_name);
                  console.log(stroe_report_name);

                  var data = {
                      location: $scope.stroe_report_name,
                      parameter: {
                          doc_sr_code: doc_sr_code_user,
                      },
                      state: 'main.InvSto',
                      ready: function () {
                          this.refreshReport();
                      },
                  }
                  console.log(data);
                  window.localStorage["ReportDetails"] = JSON.stringify(data);
                  $state.go('main.ReportCardParameter');
              });
          }
          $scope.stor_report_after_submit = function (row) {
              debugger;          
              $http.get(ENV.apiUrl + "api/StoreIssueController/get_afterSubmitgetreport_single?doc_prov_no=" + row.invoice_no ).then(function (report_data) {
                  var doc_sr_code_user = report_data.data;
                  console.log(doc_sr_code_user);
                  console.log($scope.stroe_report_name);
                  console.log(stroe_report_name);
                  var data = {
                      location: $scope.stroe_report_name,
                      parameter: {
                          doc_sr_code: doc_sr_code_user,
                      },
                      state: 'main.InvSto',
                      ready: function () {
                          this.refreshReport();
                      },
                  }
                  console.log(data);
                  window.localStorage["ReportDetails"] = JSON.stringify(data);
                  $state.go('main.ReportCardParameter');
              });
          }



          $scope.invoice_report = function (str) {
              debugger;
              console.log($scope.invoice_report_name);
              var data = {
                  location: $scope.invoice_report_name,
                  parameter: {
                      doc_prov_no: str,
                  },
                  state: 'main.InvSto',
                  ready: function () {
                      this.refreshReport();
                  },
              }
              console.log(data);

              window.localStorage["ReportDetails"] = JSON.stringify(data);
              $state.go('main.ReportCardParameter');
          }
          $scope.resetData = function () {
              $scope.temp["to_date"] = today
              $scope.temp["from_date"] = today
              $scope.temp["issue_creation_date"] = today
              $scope.hide_submit = false;
              $scope.Getgrd = [];
              $scope.Getsec = [];
              $scope.store_issue_details = [];
          }

          $scope.insertion_data = function () {
              debugger;
              var data_temp = [];
             
              if ($scope.store_issue_details != "undefined") {
                  var sending_Data = [];
                  for (var i = 0; i < $scope.store_issue_details.length; i++) {
                      var value = 0;
                      for (var j = 0; j < $scope.store_issue_details[i].sublist1.length; j++) {

                          if ($scope.store_issue_details[i].checked_outer == true) {                              
                              if ($scope.store_issue_details[i].sublist1[j].inner_checked == true) {
                                  $scope.store_issue_details[i].sublist1[j].creation_date = $scope.temp.issue_creation_date;
                                  console.log($scope.temp.issue_creation_date);
                                  sending_Data.push($scope.store_issue_details[i].sublist1[j])
                                  if (value == 0) {
                                      if (data_temp.length == 0) {
                                          data_temp = {
                                              invoice_no: $scope.store_issue_details[i].invoice_no + $scope.store_issue_details[i].enroll_no,
                                              doc_prov_date: $scope.store_issue_details[i].doc_prov_date,
                                          }
                                      } else {
                                          data_temp = {
                                              invoice_no: data_temp.invoice_no + "," + $scope.store_issue_details[i].invoice_no  + $scope.store_issue_details[i].enroll_no,
                                              doc_prov_date: data_temp.doc_prov_date + "," + $scope.store_issue_details[i].doc_prov_date,
                                          }
                                      }
                                  }
                                  value++;
                              }
                          }
                      }

                  }
                  console.log(sending_Data);                  
                  if (sending_Data.length > 0) {
                      $http.post(ENV.apiUrl + "api/StoreIssueController/insertStoreIssueData", sending_Data).then(function (insertedFlag) {
                          debugger;
                          var flag = insertedFlag.data;
                          console.log(flag);
                          if (flag) {
                              swal({ text: "Record Insert Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });                           
                              //$scope.store_issue_details = [];
                              //get_Details($scope.edt);
                              $scope.stor_report_after_submit(data_temp);
                              
                          }
                          else {
                              swal({ text: "Record not inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                          }
                      });
                  }
              }
          }


          $scope.selectedData = function () {
              debugger;
              $scope.tempArray = [];
              var id = event.target.id;
              for (var i = 0; i < $scope.store_issue_details.length; i++) {
                  for (var j = 0; j < $scope.store_issue_details[i].sublist1.length; j++) {
                      if ($scope.store_issue_details[i].checked_outer == true) {
                          if ($scope.store_issue_details[i].sublist1[j].inner_disabled != true) {
                              $scope.store_issue_details[i].sublist1[j].inner_checked = true;
                          }
                      }
                      //else {
                      //    if ($scope.store_issue_details[i].sublist1[j].inner_disabled != true) {
                      //        $scope.store_issue_details[i].sublist1[j].inner_checked = false;
                      //    }
                      //}
                  }
              }

              //for (var i = 0; i < $scope.store_issue_details.length; i++) {
              //    for (var j = 0; j < $scope.store_issue_details[i].sublist1.length; j++) {
              //        if ($scope.store_issue_details[i].checked_outer == true) {
              //            if ($scope.store_issue_details[i].sublist1[j].inner_disabled == false) {
              //                $scope.store_issue_details[i].sublist1[j].inner_checked = true;
              //            }
              //        }
              //    }
              //}
              console.log("tempArray", $scope.tempArray);
          }
          //documnet.getElementById('remark'+i).value;
      }])


})();