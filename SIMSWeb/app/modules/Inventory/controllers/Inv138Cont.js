﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Inv138');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('Inv138Controller1',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $http.get(ENV.apiUrl + "api/common/LetterofCredits/getLetterOfCreditByIndex?pageIndex=1&PageSize=5").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.obj = res.data;

            });
            $http.get(ENV.apiUrl + "api/common/LetterofCredits/getSupplierName").then(function (getSupplierName) {
                $scope.Supplier_Name = getSupplierName.data;

            });
            $http.get(ENV.apiUrl + "api/common/LetterofCredits/getBankName").then(function (getBankName) {
                $scope.Bank_Name = getBankName.data;

            });

            $http.get(ENV.apiUrl + "api/common/LetterofCredits/getCurrency").then(function (getCurrency) {
                $scope.Currency = getCurrency.data;

            });

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.btn_save = false;
                $scope.btn_update = true;
                $scope.btn_delete = false;
                $scope.edt = str;

                if (main.checked == true) {

                    for (var i = 0; i < $scope.obj.length; i++) {
                        var t = $scope.obj[i].lc_no1;
                        var v = document.getElementById(t);
                        v.checked = false;

                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.btn_save = true;
                $scope.btn_update = false;
                $scope.btn_delete = false;
                $scope.edt = "";

                $http.get(ENV.apiUrl + "api/common/LetterofCredits/getLCNO").then(function (getLCNO) {
                    $scope.LC_NO = getLCNO.data;
                    $scope.edt = { lc_no1: $scope.LC_NO[0].lc_no1 };
                });
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;

            }

            $scope.SaveData = function (isvalid) {

                if (isvalid) {

                    $http.post(ENV.apiUrl + "api/common/LetterofCredits/CUDLetterOfCreditsDetails?data=" + JSON.stringify($scope.edt) + "&opr=I").then(function (msg) {
                        $scope.msg1 = msg.data;
                        $('#message').modal('show');
                        $scope.display = false;

                        $scope.grid = true;

                    });
                }
            }

            $scope.Update = function () {

                $http.post(ENV.apiUrl + "api/common/LetterofCredits/CUDLetterOfCreditsDetails?data=" + JSON.stringify($scope.edt) + "&opr=U").then(function (msg) {

                    $scope.msg1 = msg.data;
                    $('#message').modal('show');
                    $scope.display = false;
                    $scope.grid = true;

                });
            }

            $scope.Delete = function () {

                for (var i = 0; i < $scope.obj.length; i++) {
                    for (var j = 0; j < del.length; j++) {
                        if ($scope.obj[i].lc_no1 == del[j]) {

                            $http.post(ENV.apiUrl + "api/common/LetterofCredits/CUDLetterOfCreditsDetails?data=" + JSON.stringify($scope.obj[i]) + "&opr=D").then(function (msg) {

                                $scope.msg1 = msg.data;
                            })



                        }
                    }
                }
                $('#message').modal('show');
            }

            $scope.multipledelete = function () {
                del = [];
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.obj.length; i++) {
                        var t = $scope.obj[i].lc_no1;
                        var v = document.getElementById(t);
                        v.checked = true;
                        del.push(t);
                        $scope.row1 = 'row_selected';
                    }

                }
                else {
                    for (var i = 0; i < $scope.obj.length; i++) {
                        var t = $scope.obj[i].lc_no1;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        del.pop(t);
                        $scope.row1 = '';
                    }

                }

            }

            $scope.delete_onebyone = function (str) {

                var v = document.getElementById(str);
                if (v.checked == true) {

                    del.push(str);

                    $scope.row1 = '';
                }

                else {

                    v.checked = false;
                    var index = del.indexOf(str);
                    del.splice(index, 1);

                }

                main.checked = false;
                $scope.row1 = '';
            }

            $scope.size = function (str) {

                $scope.pagesize = str;
                $http.get(ENV.apiUrl + "api/common/LetterofCredits/getLetterOfCreditByIndex?pageIndex=" + $scope.pageindex + "&PageSize=" + $scope.pagesize).then(function (res) {
                    $scope.obj = res.data;


                    $scope.check();
                    $scope.row1 = '';
                });
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $http.get(ENV.apiUrl + "api/common/LetterofCredits/getLetterOfCreditByIndex?pageIndex=" + $scope.pageindex + "&PageSize=" + $scope.pagesize).then(function (res) {
                    $scope.obj = res.data;

                    $scope.check();
                    $scope.row1 = '';
                });
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            var dom;
            var count = 0;

            $scope.expand = function (info, $event) {

                $(dom).remove();

                dom = $("<tr><td class='details' colspan='11'>" +
                    "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                    "<tbody>" +
                     "<tr> <td class='semi-bold'>" + "LC BENEFICIARY" + "</td> <td class='semi-bold'>" + "LC AMOUNT" + " </td><td class='semi-bold'>" + "LC REVISED AMOUNT" + "</td><td class='semi-bold'>" + "LC UTILIZED AMOUNT" + "</td><td class='semi-bold'>" + "LC CANCELLED AMOUNT" + " </td>" +
                    "<td class='semi-bold'>" + "LC BANK CHARGES" + "</td><td class='semi-bold'>" + "LC VALID DATE" + "</td></tr>" +

                      "<tr><td>" + (info.lc_beneficiary) + "</td> <td>" + (info.lc_amount) + " </td><td>" + (info.lc_revised_amount) + "</td><td>" + (info.lc_utilized_amount) + "</td><td>" + (info.lc_cancelled_amount) + "</td>" +
                    "<td>" + (info.lc_bank_charges) + "</td><td>" + (info.lc_valid_date) + "</td></tr>" +

                    "<tr> <td class='semi-bold'>" + "LC TERMS" + "</td> <td class='semi-bold'>" + "LC REMARKS" + " </td><td class='semi-bold'>" + "LC CLOSE DATE" + "</td></tr>" +
                    "<tr><td>" + (info.lc_terms) + "</td> <td>" + (info.lc_remarks) + " </td><td>" + (info.lc_close_date) + "</td></tr>" +

                    " </table></td></tr>")

                $($event.currentTarget).parents("tr").after(dom);


            };

        }])
})();