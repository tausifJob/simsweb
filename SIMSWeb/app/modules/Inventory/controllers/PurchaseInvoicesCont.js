﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PurchaseInvoicesCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.grid = true;

            $http.get(ENV.apiUrl + "api/purchaseinvices/getCurrency").then(function (getCurrency) {
                $scope.getCurrency = getCurrency.data;
            });

            $http.get(ENV.apiUrl + "api/purchaseinvices/Get_Adj_Suppliername").then(function (Get_Adj_Suppliername) {
                $scope.Get_Adj_Suppliername = Get_Adj_Suppliername.data;
            });

            $http.get(ENV.apiUrl + "api/purchaseinvices/GetAll_Payment_Modes").then(function (GetAll_Payment_Modes) {
                $scope.GetAll_Payment_Modes = GetAll_Payment_Modes.data;
            });

            $http.get(ENV.apiUrl + "api/purchaseinvices/GetAll_Forward_Agents").then(function (GetAll_Forward_Agents) {
                $scope.GetAll_Forward_Agents = GetAll_Forward_Agents.data;
            });

            $http.get(ENV.apiUrl + "api/purchaseinvices/GetAll_final_payments").then(function (GetAll_final_payments) {
                $scope.GetAll_final_payments = GetAll_final_payments.data;
            });

            $http.get(ENV.apiUrl + "api/purchaseinvices/GetAll_Purchase_Invoices").then(function (GetAll_Purchase_Invoices) {
                $scope.GetAll_Purchase_Invoices = GetAll_Purchase_Invoices.data;
                $scope.totalItems = $scope.GetAll_Purchase_Invoices.length;
                $scope.todos = $scope.GetAll_Purchase_Invoices;
                $scope.makeTodos();
            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.CheckMultiple();
                }
            };

            $scope.edit = function (str) {
                $scope.grid = false;
                $scope.btn_save = false;
                $scope.btn_update = true;
                $scope.temp = str;

            }

            $scope.New = function () {
                $scope.grid = false;
                $scope.btn_save = true;
                $scope.temp = "";
                $scope.MyForm.$setPristine();
            }

            $scope.Cancel = function () {
                $scope.grid = true;
            }
            var datasend = [];
            var data = {};

            $scope.SaveData = function (isvalid) {


                datasend = [];
                data = {};
                if (isvalid) {
                    data = $scope.temp;
                    data.opr = 'I';
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/purchaseinvices/CUD_Purchase_Invoices", datasend).then(function (msg) {

                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: 'Record Updated Successfully', imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                            $scope.grid = true;
                            $http.get(ENV.apiUrl + "api/purchaseinvices/GetAll_Purchase_Invoices").then(function (GetAll_Purchase_Invoices) {
                                $scope.GetAll_Purchase_Invoices = GetAll_Purchase_Invoices.data;
                                $scope.totalItems = $scope.GetAll_Purchase_Invoices.length;
                                $scope.todos = $scope.GetAll_Purchase_Invoices;
                                $scope.makeTodos();
                            });
                        }
                        else if ($scope.msg1 == false) {

                            swal({ text: 'Record Not Updated. ', imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                            $http.get(ENV.apiUrl + "api/purchaseinvices/GetAll_Purchase_Invoices").then(function (GetAll_Purchase_Invoices) {
                                $scope.GetAll_Purchase_Invoices = GetAll_Purchase_Invoices.data;
                                $scope.totalItems = $scope.GetAll_Purchase_Invoices.length;
                                $scope.todos = $scope.GetAll_Purchase_Invoices;
                                $scope.makeTodos();
                            });

                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });

                }
            }

            var dataupdate = [];
            var data1 = {};
            $scope.Update = function () {

                dataupdate = [];
                data1 = {};
                data1 = $scope.temp;
                data1.opr = 'U';
                dataupdate.push(data1);
                $http.post(ENV.apiUrl + "api/purchaseinvices/CUD_Purchase_Invoices", dataupdate).then(function (msg) {

                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {

                        swal({ text: 'Record Updated Successfully', imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                        $scope.grid = true;
                        $http.get(ENV.apiUrl + "api/purchaseinvices/GetAll_Purchase_Invoices").then(function (GetAll_Purchase_Invoices) {
                            $scope.GetAll_Purchase_Invoices = GetAll_Purchase_Invoices.data;
                            $scope.totalItems = $scope.GetAll_Purchase_Invoices.length;
                            $scope.todos = $scope.GetAll_Purchase_Invoices;
                            $scope.makeTodos();
                        });
                    }
                    else if ($scope.msg1 == false) {

                        swal({ text: 'Record Not Updated. ', imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                        $http.get(ENV.apiUrl + "api/purchaseinvices/GetAll_Purchase_Invoices").then(function (GetAll_Purchase_Invoices) {
                            $scope.GetAll_Purchase_Invoices = GetAll_Purchase_Invoices.data;
                            $scope.totalItems = $scope.GetAll_Purchase_Invoices.length;
                            $scope.todos = $scope.GetAll_Purchase_Invoices;
                            $scope.makeTodos();
                        });

                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }

                });
            }

            $scope.Delete = function () {

                var pi_no = '';
                var datasend = [];
                var flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].pi_no);
                    if (v.checked == true) {
                        flag = true;
                        var data = {
                            'pi_no': $scope.filteredTodos[i].pi_no,
                            'opr': 'D'
                        };
                        datasend.push(data);
                    }
                }

                if (flag == true) {
                    swal({

                        text: "Are You Sure To Delete Record",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ok',
                        width: 380,
                        showCloseButton: true
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/purchaseinvices/CUD_Purchase_Invoices", datasend).then(function (msg) {

                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: 'Record Deleted Successfully', imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                                    $http.get(ENV.apiUrl + "api/purchaseinvices/GetAll_Purchase_Invoices").then(function (GetAll_Purchase_Invoices) {
                                        $scope.GetAll_Purchase_Invoices = GetAll_Purchase_Invoices.data;
                                        $scope.totalItems = $scope.GetAll_Purchase_Invoices.length;
                                        $scope.todos = $scope.GetAll_Purchase_Invoices;
                                        $scope.makeTodos();
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: 'Recoed Not Deleted. ', imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                                    $http.get(ENV.apiUrl + "api/purchaseinvices/GetAll_Purchase_Invoices").then(function (GetAll_Purchase_Invoices) {
                                        $scope.GetAll_Purchase_Invoices = GetAll_Purchase_Invoices.data;
                                        $scope.totalItems = $scope.GetAll_Purchase_Invoices.length;
                                        $scope.todos = $scope.GetAll_Purchase_Invoices;
                                        $scope.makeTodos();
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });

                        }
                        else {
                            main = document.getElementById('mainchk');
                            main.checked = false;
                            $scope.multipledelete();
                        }
                    });
                }
                else {
                    swal({ text: 'Select At Least One Record To Delete', imageUrl: "assets/img/notification-alert.png", width: 380, showCloseButton: true });
                }
            }

            $scope.multipledelete = function () {

                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].pi_no);
                    if (main.checked == true) {
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                    else {
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.delete_onebyone = function (str) {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            var dom;
            $scope.flag = true;
            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='12'>" +
                    "<table class='inner-table' width='100%' cellpadding='5' cellspacing='0' style='border-color:red;border:solid;border-width:02px'>" +
                    "<tbody>" +
                    "<tr style='text-align:center;height:20px'><td class='semi-bold'>" + "EXCHANE RATE" + "</td> <td class='semi-bold'>" + "INVOICE DATE" + " </td><td class='semi-bold'>" + "AMOUNT" + "</td><td class='semi-bold''>" + "DISCOUNT AMOUNT" + "</td><td class='semi-bold'>" + "FREIGHT" + "</td><td class='semi-bold''>" + "INSURANCE AMOUNT" + "</td></tr>" +
                    "<tr style='text-align:center;height:20px'><td >" + (info.pi_exchange_rate) + "</td> <td>" + (info.pi_invoice_date) + " </td><td>" + (info.pi_amount) + "</td><td>" + (info.pi_discount_amount) + "</td><td>" + (info.pi_freight) + "</td><td>" + (info.pi_insurance) + "</td></tr>" +
                     "<tr style='text-align:center;height:20px'><td class='semi-bold'>" + "OTHER CHARGES" + "</td> <td class='semi-bold'>" + "INDICATOR" + " </td><td class='semi-bold'>" + "PAYMENT DUE DATE" + "</td><td class='semi-bold''>" + "FP_NO" + "</td><td class='semi-bold'>" + "PAYMENT MODE" + "</td><td class='semi-bold''>" + "REMARK" + "</td></tr>" +
                    "<tr style='text-align:center;height:20px'><td >" + (info.pi_other_charge) + "</td> <td>" + (info.pi_indicator) + " </td><td >" + (info.pi_payment_due_date) + "</td><td>" + (info.fp_no) + "</td><td>" + (info.pm_desc) + "</td><td>" + (info.pi_remarks) + "</td></tr>" +
                      "<tr style='text-align:center;height:20px'><td class='semi-bold'>" + "DEBIT NOTE NO" + "</td> <td class='semi-bold'>" + "DEBIT NOTE DATE" + " </td><td class='semi-bold'>" + "INVOICE RECEIVED DATE" + "</td><td class='semi-bold''>" + "INVOICE CREATION DATE" + "</td><td></td><td></td></tr>" +
                    "<tr style='text-align:center;height:20px'><td >" + (info.pi_debit_note_no) + "</td> <td>" + (info.pi_debit_note_date) + " </td><td>" + (info.pi_invoice_received_date) + "</td><td>" + (info.pi_creation_date) + "</td><td></td><td></td></tr>" +
                    " </table1></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.GetAll_Purchase_Invoices, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.GetAll_Purchase_Invoices;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.pi_invoice_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sup_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

        }])
})();