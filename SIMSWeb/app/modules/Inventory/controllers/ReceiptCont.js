﻿(function () {
    'use strict';
    var opr = '';
    var data1 = [];
    var main, edt;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ReceiptCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            //$rootScope.provno = '';

            $scope.rpt = JSON.parse(window.localStorage["ReportProvNumber"]);

            $scope.prov_no = $scope.rpt.doc_prov_no;
            $scope.prov_status = $scope.rpt.doc_status;
            $scope.back_state = $scope.rpt.back_state;


            if ($scope.prov_status == 'Cancel Sale Receipt') {
                $scope.flg_staus = true
            }


            $(window).bind('keydown', function (event) {

                //if (event.keyCode == '8') {
                //    $state.go($scope.back_state)
                //}
                if (event.ctrlKey || event.metaKey) {
                    switch (String.fromCharCode(event.which).toLowerCase()) {
                        case 'p':
                            event.preventDefault();
                            var docHead = document.head.outerHTML;
                            var printContents = document.getElementById('printd').outerHTML;
                            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

                            var newWin = window.open("", "_blank", winAttr);
                            var writeDoc = newWin.document;
                            writeDoc.open();
                            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                            writeDoc.close();
                            newWin.focus();


                            break;


                    }
                }
            });

            $scope.back = function () {

                $state.go($scope.back_state)


            }
            $http.get(ENV.apiUrl + "api/Sales_Doc/GetReceiptDetails?prov_no=" + $scope.prov_no).then(function (res) {

                $scope.data = res.data;

                //var printContents = document.getElementById('ddd').innerHTML;
                //var originalContents = document.body.innerHTML;

                //document.body.innerHTML = printContents;

                //window.print();
                $('body').addClass('grey condense-menu');
                $('#main-menu').addClass('mini');
                $('.page-content').addClass('condensed');
                $rootScope.isCondensed = true;

                //document.body.innerHTML = originalContents;
                setTimeout(function () {
                    try {
                        //var headstr = "<html><head><title>Booking Details</title></head><body>";
                        //var footstr = "</body>";
                        //var newstr = document.getElementById('printd').innerHTML;
                        //var oldstr = document.body.innerHTML;
                        //document.body.innerHTML = headstr + newstr + footstr;
                        //window.print();
                        //document.body.innerHTML = oldstr;


                        //return false;


                        var docHead = document.head.outerHTML;
                        var printContents = document.getElementById('printd').outerHTML;
                        var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

                        var newWin = window.open("", "_blank", winAttr);
                        var writeDoc = newWin.document;
                        writeDoc.open();
                        writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                        writeDoc.close();
                        newWin.focus();





                    }
                    catch (er) { }
                }, 100);

            });

            //for collapse main menu

        }]);

})();



