﻿(function () {

    'use strict';
    var temp = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SMFCStoreIssueCont',
      ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

          $scope.temp = [];
          $scope.edt = [];
          var uname = $rootScope.globals.currentUser.username;
          var today = new Date();
          today = ('0' + today.getDate()).slice(-2) + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + today.getFullYear();
          $scope.temp["to_date"] = today
          $scope.temp["from_date"] = today
          
          $http.get(ENV.apiUrl + "api/StoreIssueController/GetCur").then(function (GetCur) {
              
              $scope.GetCur = GetCur.data;
              $scope.edt = { sims_cur_code: $scope.GetCur[0].sims_cur_code }
              $scope.select_cur($scope.GetCur[0].sims_cur_code);
          });

          $scope.select_cur = function (cur) {
              $http.get(ENV.apiUrl + "api/StoreIssueController/GetAca?cur=" + cur).then(function (GetAca) {
                  $scope.GetAca = GetAca.data;
                  $scope.edt['sims_acaedmic_year'] = $scope.GetAca[0].sims_acaedmic_year;
                  $scope.select_aca($scope.GetAca[0].sims_acaedmic_year)
              });
          }
          $scope.select_aca = function (aca) {
              $http.get(ENV.apiUrl + "api/StoreIssueController/Getgrd?cur=" + $scope.edt.sims_cur_code + "&aca=" + aca + "&user=" + uname).then(function (Getgrd) {
                  $scope.Getgrd = Getgrd.data;

                  setTimeout(function () {
                      $('#cmb_grade').change(function () {
                          console.log($(this).val());
                      }).multipleSelect({
                          width: '100%'
                      });
                  }, 1000);

              });
          }
      
          $scope.disabledPropert = function (str) {
              debugger;
              if (str==true) {
                  $scope.edt.searchText_return = "";
              }
              else {
                  $scope.edt.searchText = "";
              }
          }
          $scope.reSet = function () {
              $scope.temp["to_date"] = today;
              $scope.temp["from_date"] = today;
              $scope.edt = [];
              $scope.GetAca = [];
              $scope.Getgrd = [];
              $scope.Getsec = [];
              $scope.edt = { sims_cur_code: $scope.GetCur[0].sims_cur_code }
              $scope.store_issue_details = [];
              $scope.select_cur($scope.GetCur[0].sims_cur_code);
          }
          $scope.select_grd = function (grd) {
              $http.get(ENV.apiUrl + "api/StoreIssueController/Getsec?cur=" + $scope.edt.sims_cur_code + "&aca=" + $scope.edt.sims_acaedmic_year + "&grd=" + grd + "&user=" + uname).then(function (Getsec) {
                  $scope.Getsec = Getsec.data;
              });

          }

          $scope.get_Details = function (data) {
              debugger;
              var search_data = '';
              if (data.searchText_return!='') {
                  search_data = data.searchText_return;
              } else if (data.searchText!='') {
                  search_data = data.searchText;
              }
              $http.get(ENV.apiUrl + "api/StoreIssueController/getDetailsSMFC?cur=" + data.sims_cur_code + "&aca=" + data.sims_acaedmic_year + "&grd=" + data.sims_grade_code + "&section=" + data.sims_section_code + "&fromdate=" + $scope.temp["from_date"] + "&todate=" + $scope.temp["to_date"] + "&searchText=" + search_data).then(function (getFullData) {
                  $scope.store_issue_details = getFullData.data;
                  console.log($scope.store_issue_details);
                  if ($scope.store_issue_details.length <= 0) {
                      swal({ text: "Record not found. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                  }
              });

          }
          $scope.openDetails = function (row) {
              debugger
              $state.go('main.detail', { 'IP': { 'enroll_No': row.enroll_no, 'doc_prov_no': row.invoice_no, 'doc_prov_date': row.doc_prov_date, 'creation_user': row.creation_user, 'student_name': row.student_name, 'sims_cur_code': $scope.edt.sims_cur_code, 'sims_acaedmic_year': $scope.edt.sims_acaedmic_year, 'sims_grade_code': $scope.edt.sims_grade_code, 'sims_section_code': $scope.edt.sims_section_code, 'dt_code': row.dt_code} });
          }
          $scope.openReport = function (row) {
              
              $state.go('main.report', { 'IP': { 'enroll_No': row.enroll_no, 'student_name': row.student_name, 'doc_prov_no': row.invoice_no, 'doc_prov_date': row.doc_prov_date} });
          }

      }])


})();