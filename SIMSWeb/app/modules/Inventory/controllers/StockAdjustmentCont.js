﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, main1, deletefin = [];
    var simsController = angular.module('sims.module.Inventory');
    simsController.controller('StockAdjustmentCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.pagesize = '';
            $scope.pageindex = 1;
            $scope.table = true;
            $scope.Main_table = false;
            $scope.searchitems = [];
            $scope.searchitemsremove = [];
            $scope.GetItems = [];
            var str1 = '';
            var dco_no_712 = '';
            $scope.Quntity_show_txt = true;
            var old_cur_qty, old_malc_rate, search_item_des, n_v, m_v, n_vr, m_vr;
            var flagsa = false;
            var flagrs = false;
            var flagai = false;
            var flagri = false;
            //console.clear();
            //var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
            var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');
            $scope.dt = {
                sims_from_date: dateyear,
                sims_to_date: dateyear,
            }

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.getallcomboBox = function () {
                $http.get(ENV.apiUrl + "api/StockAdjustment/getAllLocation").then(function (AllLoc) {
                    $scope.location = AllLoc.data;
                    $scope.isa = { loc_code: $scope.location[0].loc_code };
                });

                $http.get(ENV.apiUrl + "api/StockAdjustment/getAllSupplierName").then(function (AllSna) {
                    $scope.SupplierName = AllSna.data;
                    $scope.isa1 = { sup_sblgr_acno: $scope.SupplierName[0].sup_sblgr_acno };
                });

                $http.get(ENV.apiUrl + "api/StockAdjustment/getAllReason").then(function (AllRea) {
                    $scope.Reason = AllRea.data;
                    $scope.isa2 = { ar_code: $scope.Reason[0].ar_code };
                });

                $http.get(ENV.apiUrl + "api/StockAdjustment/getAllType").then(function (Alltype) {
                    $scope.Type = Alltype.data;
                    $scope.isa3 = { dco_code: $scope.Type[0].dco_code };
                });

                $http.get(ENV.apiUrl + "api/StockAdjustment/getsuppliergroupname").then(function (Allsupgname) {
                    $scope.supgname = Allsupgname.data;
                });

                $http.get(ENV.apiUrl + "api/StockAdjustment/getAllDepartment").then(function (resd) {
                    $scope.department = resd.data;
                    $scope.isa4 = { dep_code: $scope.department[0].dep_code };
                });

            }

            $scope.getallcomboBox();

            $scope.getCategories = function () {
                $http.get(ENV.apiUrl + "api/StockAdjustment/getSubCategories?pc_pcode=" + $scope.temp1.category_code).then(function (SubCategories) {
                    $scope.allSubCategories = SubCategories.data;

                });
            }

            $scope.Show_item_detais = function (itemcode) {
                search_item_des = itemcode;
                if (itemcode != undefined || itemcode != null) {
                    $http.get(ENV.apiUrl + "api/StockAdjustment/getsearchItemss?itemcode=" + itemcode).then(function (itemdetail) {
                        $scope.itemdetailrecord = itemdetail.data;
                        if ($scope.itemdetailrecord.length > 0) {
                            $scope.Quntity_show_txt = false;
                            $scope.edt = {
                                'im_desc': $scope.itemdetailrecord[0].im_desc,
                                'item_code': itemcode,
                                //'id_cur_qty': $scope.itemdetailrecord[0].id_cur_qty,
                                //'im_malc_rate': $scope.itemdetailrecord[0].im_malc_rate,
                                'old_cur_qty': $scope.itemdetailrecord[0].id_cur_qty,
                                'old_malc_rate': $scope.itemdetailrecord[0].im_malc_rate,
                                'im_inv_no': $scope.itemdetailrecord[0].im_inv_no,
                                'im_malc_rate': $scope.itemdetailrecord[0].im_malc_rate,
                                'im_sell_price': $scope.itemdetailrecord[0].im_sell_price
                            }

                        }
                        else {
                            $scope.Quntity_show_txt = true;
                            swal({ title: 'Alert', text: "Item Not Found...", showCloseButton: true, width: 450, height: 200 });
                        }
                    });
                }

            }

            //$scope.Val_Cal = function (quty, rate) {
            //    
            //    var cla_value_new = parseFloat($scope.edt.id_cur_qty) * parseFloat($scope.edt.im_malc_rate);
            //    $scope.value = cla_value_new;
            //}

            //$scope.Val_Cal_new = function (o) {

            //    
            //    n_v = o.new_qty;
            //    m_v = o.new_rate;
            //    var cla_value_new1 = parseFloat(o.new_qty) * parseFloat(o.new_rate);
            //    o.new_value = cla_value_new1;
            //    if ($scope.isa3.dco_code == "ADJC") {
            //        o.new_quntity = parseFloat(o.id_cur_qty) + parseFloat(o.new_qty);
            //        var quntity_old = parseFloat(o.id_cur_qty) * parseFloat(o.im_malc_rate);
            //        var quntity_new = parseFloat(o.new_qty) * parseFloat(o.new_rate);
            //        var quntity_on = parseFloat(o.id_cur_qty) + parseFloat(o.new_qty);
            //        var q_on = parseFloat(quntity_old) + parseFloat(quntity_new);
            //        var cal_q_no = (parseFloat(q_on) / parseFloat(quntity_on));
            //        o.new_malc = parseFloat(cal_q_no);
            //    }
            //    else if ($scope.isa3.dco_code == "ADJS") {
            //        o.new_quntity = parseFloat(o.id_cur_qty) + parseFloat(o.new_qty);
            //        o.new_malc = parseFloat(o.new_qty) * parseFloat(mackrate);
            //    }
            //}

            //$scope.Val_Cal_new_Remove = function (o) {

            //    
            //    n_vr = o.new_qty;
            //    m_vr = o.new_rate;
            //    var cla_value_new1r = parseFloat(o.new_qty) * parseFloat(o.new_rate);
            //    o.new_value = cla_value_new1r;
            //    //for (var j = 0; j <= $scope.Type.length; j++) {
            //    if ($scope.isa3.dco_code == "ADJC") {
            //        o.new_quntity = parseFloat(o.id_cur_qty) - parseFloat(o.new_qty);
            //        var quntity_old = parseFloat(o.id_cur_qty) * parseFloat(o.im_malc_rate);
            //        var quntity_new = parseFloat(o.new_qty) * parseFloat(o.new_rate);
            //        var quntity_on = parseFloat(o.id_cur_qty) - parseFloat(o.new_qty);
            //        var q_on = parseFloat(quntity_old) - parseFloat(quntity_new);
            //        var cal_q_no = (parseFloat(q_on) / parseFloat(quntity_on));
            //        o.new_malc = parseFloat(cal_q_no);
            //    }
            //    else if ($scope.isa3.dco_code == "ADJS") {
            //        o.new_quntity = parseFloat(o.id_cur_qty) - parseFloat(o.new_qty);
            //        o.new_malc = parseFloat(o.new_qty) * parseFloat(mackrate);
            //    }
            //    //}
            //}


            $scope.Val_Cal = function (quty, rate) {
               
                //var cla_value_new = parseFloat($scope.edt.id_cur_qty) * parseFloat($scope.edt.im_malc_rate);
                //$scope.value = cla_value_new;
                var cla_value_new1 = parseFloat($scope.edt.id_cur_qty) * parseFloat($scope.edt.im_malc_rat);
                if (str1 == "AI") {
                    if ($scope.isa3.dco_code == "ADJC") {
                        /////////o.new_quntity = parseFloat($scope.edt.old_cur_qty) + parseFloat($scope.edt.id_cur_qty);
                        $scope.value = parseFloat($scope.edt.id_cur_qty) * parseFloat(mackrate);
                    }
                    else if ($scope.isa3.dco_code == "ADJS") {
                        /////////o.new_quntity = parseFloat($scope.edt.old_cur_qty) + parseFloat($scope.edt.id_cur_qty);
                        var quntity_old = parseFloat($scope.edt.old_cur_qty) * parseFloat($scope.edt.old_malc_rate);
                        var quntity_new = parseFloat($scope.edt.id_cur_qty) * parseFloat($scope.edt.im_malc_rate);
                        var quntity_on = parseFloat($scope.edt.old_cur_qty) + parseFloat($scope.edt.id_cur_qty);
                        var q_on = parseFloat(quntity_old) + parseFloat(quntity_new);
                        var cal_q_no = (parseFloat(q_on) / parseFloat(quntity_on));
                        $scope.value = parseFloat(cal_q_no);
                    }
                }
                else if (str1 == "RI") {
                    if ($scope.isa3.dco_code == "ADJC") {
                        /////////o.new_quntity = parseFloat($scope.edt.old_cur_qty) + parseFloat($scope.edt.id_cur_qty);
                        $scope.value = parseFloat($scope.edt.id_cur_qty) * parseFloat(mackrate);
                    }
                    else if ($scope.isa3.dco_code == "ADJS") {
                        /////////o.new_quntity = parseFloat($scope.edt.old_cur_qty) + parseFloat($scope.edt.id_cur_qty);
                        var quntity_old = parseFloat($scope.edt.old_cur_qty) * parseFloat($scope.edt.old_malc_rate);
                        var quntity_new = parseFloat($scope.edt.id_cur_qty) * parseFloat($scope.edt.im_malc_rate);
                        var quntity_on = parseFloat($scope.edt.old_cur_qty) - parseFloat($scope.edt.id_cur_qty);
                        var q_on = parseFloat(quntity_old) - parseFloat(quntity_new);
                        var cal_q_no = (parseFloat(q_on) / parseFloat(quntity_on));
                        $scope.value = parseFloat(cal_q_no);
                    }
                }
                
            }

            $scope.Val_Cal_new = function (o) {

               
                n_v = o.new_qty;
                m_v = o.new_rate;
                var cla_value_new1 = parseFloat(o.new_qty) * parseFloat(o.new_rate);
                o.new_value = cla_value_new1;
                if ($scope.isa3.dco_code == "ADJC") {
                    o.new_quntity = parseFloat(o.id_cur_qty) + parseFloat(o.new_qty);
                    //o.new_malc = parseFloat(o.new_qty) * parseFloat(mackrate);
                    o.new_malc = parseFloat(o.new_rate);
                }
                else if ($scope.isa3.dco_code == "ADJS") {
                    o.new_quntity = parseFloat(o.id_cur_qty) + parseFloat(o.new_qty);
                    var quntity_old = parseFloat(o.id_cur_qty) * parseFloat(o.im_malc_rate);
                    var quntity_new = parseFloat(o.new_qty) * parseFloat(o.new_rate);
                    var quntity_on = parseFloat(o.id_cur_qty) + parseFloat(o.new_qty);
                    var q_on = parseFloat(quntity_old) + parseFloat(quntity_new);
                    var cal_q_no = (parseFloat(q_on) / parseFloat(quntity_on));
                    o.new_malc = parseFloat(cal_q_no);
                }
            }

            $scope.Val_Cal_new_Remove = function (o) {

              
                n_vr = o.new_qty;
                m_vr = o.new_rate;
                var cla_value_new1r = parseFloat(o.new_qty) * parseFloat(o.new_rate);
                o.new_value = cla_value_new1r;
                //for (var j = 0; j <= $scope.Type.length; j++) {
                if ($scope.isa3.dco_code == "ADJC") {
                    o.new_quntity = parseFloat(o.id_cur_qty) - parseFloat(o.new_qty);
                    o.new_malc = parseFloat(o.new_qty) * parseFloat(mackrate);
                }
                else if ($scope.isa3.dco_code == "ADJS") {
                    o.new_quntity = parseFloat(o.id_cur_qty) - parseFloat(o.new_qty);
                    var quntity_old = parseFloat(o.id_cur_qty) * parseFloat(o.im_malc_rate);
                    var quntity_new = parseFloat(o.new_qty) * parseFloat(o.new_rate);
                    var quntity_on = parseFloat(o.id_cur_qty) - parseFloat(o.new_qty);
                    var q_on = parseFloat(quntity_old) - parseFloat(quntity_new);
                    var cal_q_no = (parseFloat(q_on) / parseFloat(quntity_on));
                    o.new_malc = parseFloat(cal_q_no);
                }
                //}
            }

            $scope.ItemSearch = function () {
                if ($scope.isa.loc_code != undefined && $scope.isa1.sup_sblgr_acno != undefined && $scope.isa2.ar_code != undefined && $scope.isa3.dco_code != undefined) {
                    $scope.GetItems = [];
                    $('#itemSearch').modal('show');
                    $http.get(ENV.apiUrl + "api/StockAdjustment/getCategories").then(function (Allcategorycode) {
                        $scope.categorycode = Allcategorycode.data;
                        flagai = true;
                    });
                }

                else {
                    swal({ title: 'Alert', text: "Please Select Location ,Supplier, Reason, Doc...", showCloseButton: true, width: 450, height: 200 });
                }
            }

            $scope.ItemSearch1 = function () {
                if ($scope.isa.loc_code != undefined && $scope.isa1.sup_sblgr_acno != undefined && $scope.isa2.ar_code != undefined && $scope.isa3.dco_code != undefined) {
                    $scope.GetItems = [];
                    $('#itemSearch').modal('show');
                    $http.get(ENV.apiUrl + "api/StockAdjustment/getCategories").then(function (Allcategorycode) {
                        $scope.categorycode = Allcategorycode.data;
                        flagri = true;
                    });
                }

                else {
                    swal({ title: 'Alert', text: "Please Select Location ,Supplier, Reason, Doc...", showCloseButton: true, width: 450, height: 200 });
                }
            }

            $scope.getfetch = function () {

                $http.post(ENV.apiUrl + "api/StockAdjustment/postgetItemSerch", $scope.temp1).then(function (res) {
                    if (res.data.length > 0)
                        $scope.GetItems = res.data;
                    else
                        swal({ text: "No Record Found Select Othere Record Then Try Again...", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 450, height: 200 });
                });
            }

            $scope.okbuttonclick = function () {

                if ($scope.searchitems.length == 0) {
                    $scope.Removestock_show = false;
                    $scope.Addstock_show = false;
                    $scope.Save_hide = false;
                    $scope.Remove_hide = false;
                    $scope.flagsa = false;
                    $scope.searchitems = [];
                }

                if ($scope.searchitemsremove.length == 0) {
                    $scope.flagrs = false;
                    $scope.Removestock_show = false;
                    $scope.Addstock_show = false;
                    $scope.Save_hide = false;
                    $scope.Remove_hide = false;
                    $scope.searchitemsremove = [];
                }

                if (flagai == true) {
                    for (var i = 0; i < $scope.GetItems.length; i++) {
                        var v = document.getElementById($scope.GetItems[i].im_inv_no + i);
                        if (v.checked == true) {
                           
                            $scope.GetItems[i].new_qty = '0';
                            $scope.GetItems[i].new_rate = '0';
                            $scope.GetItems[i].new_value = '0';
                            $scope.GetItems[i].new_quntity = '0';
                            $scope.GetItems[i].new_malc = '0';
                            $scope.edt = {
                                item_code: $scope.GetItems[i].im_item_code,
                            }
                            $scope.searchitems.push($scope.GetItems[i]);
                        }
                    }
                    flagai = false;
                }
                if (flagri == true) {
                    for (var i = 0; i < $scope.GetItems.length; i++) {
                        var v = document.getElementById($scope.GetItems[i].im_inv_no + i);
                        if (v.checked == true) {
                           
                            $scope.GetItems[i].new_qty = '0';
                            $scope.GetItems[i].new_rate = '0';
                            $scope.GetItems[i].new_value = '0';
                            $scope.GetItems[i].new_quntity = '0';
                            $scope.GetItems[i].new_malc = '0';
                            $scope.edt = {
                                item_code: $scope.GetItems[i].im_item_code,
                            }
                            $scope.searchitemsremove.push($scope.GetItems[i]);
                        }
                    }
                    flagri = false;
                }
                $scope.Main_table = true;
               
                if ($scope.searchitems.length >= 1) {
                    $scope.Removestock_show = false;
                    $scope.Addstock_show = true;
                    $scope.Save_hide = true;
                    $scope.Remove_hide = false;
                    $scope.flagsa = true;
                }
                else {
                    $scope.clear_txt();
                }

                if ($scope.searchitemsremove.length >= 1) {
                    $scope.flagrs = true;
                    $scope.Removestock_show = true;
                    $scope.Addstock_show = false;
                    $scope.Save_hide = false;
                    $scope.Remove_hide = true;
                }
                else {
                    $scope.clear_txt();
                }
            }

            $scope.Add = function () {
                
                $scope.Removestock_show = false;
                $scope.Addstock_show = true;
                $scope.Save_hide = true;
                $scope.Remove_hide = false;
                $scope.flagsa = true;

                if ($scope.flagrs = true) {
                    if ($scope.searchitems.length == 0) {
                        $scope.searchitems = [];
                    }
                }

                if ($scope.isa.loc_code != undefined && $scope.isa1.sup_sblgr_acno != undefined && $scope.isa2.ar_code != undefined && $scope.isa3.dco_code != undefined) {
                    if (search_item_des != undefined || search_item_des != null) {
                        if (($scope.edt.id_cur_qty != undefined || $scope.edt.search_item_des != null) && ($scope.edt.im_malc_rate != undefined || $scope.edt.im_malc_rate != null)) {
                            var val = parseFloat($scope.edt.old_cur_qty) * parseFloat($scope.edt.old_malc_rate);
                            if ($scope.isa3.dco_code == "ADJC") {
                                var row_newqty1 = parseFloat($scope.edt.old_cur_qty) + parseFloat($scope.edt.id_cur_qty);
                                var quntity_old = parseFloat($scope.edt.old_cur_qty) * parseFloat($scope.edt.old_malc_rate);
                                var quntity_new = parseFloat($scope.edt.id_cur_qty) * parseFloat($scope.edt.im_malc_rate);
                                var quntity_on = parseFloat($scope.edt.old_cur_qty) + parseFloat($scope.edt.id_cur_qty);
                                var q_on = parseFloat(quntity_old) + parseFloat(quntity_new);
                                var cal_q_no = (parseFloat(q_on) / parseFloat(quntity_on));
                                var row_newrate1 = parseFloat(cal_q_no);

                                var GetItemsadd = ({
                                    'im_inv_no': $scope.edt.im_inv_no,
                                    //'id_cur_qty': $scope.edt.id_cur_qty,
                                    'id_cur_qty': $scope.edt.old_cur_qty,
                                    'new_qty': $scope.edt.id_cur_qty,
                                    'new_rate': $scope.edt.im_malc_rate,
                                    'im_item_code': $scope.edt.item_code,
                                    'im_desc': $scope.edt.im_desc,
                                    'im_sell_price': $scope.edt.im_sell_price,
                                    'im_malc_rate': $scope.edt.old_malc_rate,
                                    'loc_code': $scope.isa.loc_code,
                                    'new_value': $scope.value,
                                    'new_quntity': row_newqty1,
                                    'new_malc': row_newrate1,
                                });
                                $scope.searchitems.push(GetItemsadd);
                                $scope.Main_table = true;
                            }

                            else if ($scope.isa3.dco_code == "ADJS") {
                                var row_newqty1 = parseFloat($scope.edt.old_cur_qty) + parseFloat($scope.edt.id_cur_qty);
                                var GetItemsadd = ({
                                    'im_inv_no': $scope.edt.im_inv_no,
                                    'id_cur_qty': $scope.edt.old_cur_qty,
                                    'new_qty': $scope.edt.id_cur_qty,
                                    'new_rate': $scope.edt.im_malc_rate,
                                    'new_value': $scope.value,
                                    'im_item_code': $scope.edt.item_code,
                                    'im_desc': $scope.edt.im_desc,
                                    'im_sell_price': $scope.edt.im_sell_price,
                                    'im_malc_rate': $scope.edt.old_malc_rate,
                                    'loc_code': $scope.isa.loc_code,
                                    'new_quntity': row_newqty1,
                                    'new_malc': val,
                                });
                                $scope.searchitems.push(GetItemsadd);
                                $scope.Main_table = true;
                            }
                        }

                        else {
                            swal({ title: 'Alert', text: "Please Enter New Quntity, New Rate Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                        }
                    }

                    else {
                        swal({ title: 'Alert', text: "Please Enter Item Code Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                    }
                }

                else {
                    swal({ title: 'Alert', text: "Please Select Location ,Supplier, Reason, Doc...", showCloseButton: true, width: 450, height: 200 });
                }
            }

            $scope.Remove_stock = function () {
                
                $scope.flagrs = true;
                $scope.Removestock_show = true;
                $scope.Addstock_show = false;
                $scope.Save_hide = false;
                $scope.Remove_hide = true;

                if ($scope.flagsa = true) {
                    if ($scope.searchitemsremove.length == 0) {
                        $scope.searchitemsremove = [];
                    }
                }

                if ($scope.isa.loc_code != undefined && $scope.isa1.sup_sblgr_acno != undefined && $scope.isa2.ar_code != undefined && $scope.isa3.dco_code != undefined) {
                    if (search_item_des != undefined || search_item_des != null) {
                        if (($scope.edt.id_cur_qty != undefined || $scope.edt.search_item_des != null) && ($scope.edt.im_malc_rate != undefined || $scope.edt.im_malc_rate != null)) {
                            var valr = parseFloat($scope.edt.old_cur_qty) * parseFloat($scope.edt.old_malc_rate);
                            if ($scope.isa3.dco_code == "ADJC") {
                                var row_newqty1r = parseFloat($scope.edt.old_cur_qty) - parseFloat($scope.edt.id_cur_qty);
                                var quntity_oldr = parseFloat($scope.edt.old_cur_qty) * parseFloat($scope.edt.old_malc_rate);
                                var quntity_newr = parseFloat($scope.edt.id_cur_qty) * parseFloat($scope.edt.im_malc_rate);
                                var quntity_onr = parseFloat($scope.edt.old_cur_qty) - parseFloat($scope.edt.id_cur_qty);
                                var q_onr = parseFloat(quntity_oldr) - parseFloat(quntity_newr);
                                var cal_q_nor = (parseFloat(q_onr) / parseFloat(quntity_onr));
                                var row_newrate1r = parseFloat(cal_q_nor);

                                var GetItemsadd = ({
                                    'im_inv_no': $scope.edt.im_inv_no,
                                    //'id_cur_qty': $scope.edt.id_cur_qty,
                                    'id_cur_qty': $scope.edt.old_cur_qty,
                                    'new_qty': $scope.edt.id_cur_qty,
                                    'new_rate': $scope.edt.im_malc_rate,
                                    'im_item_code': $scope.edt.item_code,
                                    'im_desc': $scope.edt.im_desc,
                                    'im_sell_price': $scope.edt.im_sell_price,
                                    'im_malc_rate': $scope.edt.old_malc_rate,
                                    'loc_code': $scope.isa.loc_code,
                                    'new_value': $scope.value,
                                    'new_quntity': row_newqty1r,
                                    'new_malc': row_newrate1r,
                                });
                                $scope.searchitemsremove.push(GetItemsadd);
                                $scope.Main_table = true;
                            }

                            else if ($scope.isa3.dco_code == "ADJS") {
                                var row_newqty1r = parseFloat($scope.edt.old_cur_qty) - parseFloat($scope.edt.id_cur_qty);
                                var GetItemsadd = ({
                                    'im_inv_no': $scope.edt.im_inv_no,
                                    'id_cur_qty': $scope.edt.old_cur_qty,
                                    'new_qty': $scope.edt.id_cur_qty,
                                    'new_rate': $scope.edt.im_malc_rate,
                                    'new_value': $scope.value,
                                    'im_item_code': $scope.edt.item_code,
                                    'im_desc': $scope.edt.im_desc,
                                    'im_sell_price': $scope.edt.im_sell_price,
                                    'im_malc_rate': $scope.edt.old_malc_rate,
                                    'loc_code': $scope.isa.loc_code,
                                    'new_quntity': row_newqty1r,
                                    'new_malc': valr,
                                });
                                $scope.searchitemsremove.push(GetItemsadd);
                                $scope.Main_table = true;
                            }
                        }

                        else {
                            swal({ title: 'Alert', text: "Please Enter New Quntity, New Rate Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                        }
                    }

                    else {
                        swal({ title: 'Alert', text: "Please Enter Item Code Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                    }
                }

                else {
                    swal({ title: 'Alert', text: "Please Select Location ,Supplier, Reason, Doc...", showCloseButton: true, width: 450, height: 200 });
                }
            }

            $scope.RemoveItemdetails = function ($event, index, str) {
                str.splice(index, 1);
                if ($scope.searchitems.length == 0) {
                    $scope.Main_table = false;
                }
                if ($scope.searchitemsremove.length == 0) {
                    $scope.Main_table = false;
                }
            }

            $scope.Save = function () {
                if (($scope.remark == null || $scope.remark == undefined)) {
                    swal({ title: "Alert", text: "Please Enter Remark", showCloseButton: true, width: 380, });
                }

                else {
                    var Savedata = [];
                    var Save_sa = ({
                        'tdate': $scope.dt.sims_from_date,
                        'loc_code': $scope.isa.loc_code,
                        'sup_sblgr_acno': $scope.isa1.sup_sblgr_acno,
                        'ar_code': $scope.isa2.ar_code,
                        'dco_code': $scope.isa3.dco_code,
                        'dep_code': $scope.isa4.dep_code,
                        'adj_grv_no': $scope.grvno,
                        'remark': $scope.remark
                    });
                    Savedata.push(Save_sa);

                    $http.post(ENV.apiUrl + "api/StockAdjustment/Insertadjustments", Savedata).then(function (msg) {

                        $scope.msg1 = msg.data;
                        dco_no_712 = '';
                        dco_no_712 = $scope.msg1.strMessage;
                        if ($scope.msg1.strMessage != undefined) {
                            if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                var Savedata1 = [];
                                for (var i = 0 ; i < $scope.searchitems.length; i++) {
                                    var Save_sa1 = ({
                                        'tdate': $scope.dt.sims_from_date,
                                        'loc_code': $scope.isa.loc_code,
                                        'sup_sblgr_acno': $scope.isa1.sup_sblgr_acno,
                                        'ar_code': $scope.isa2.ar_code,
                                        'dco_code': $scope.isa3.dco_code,
                                        'dep_code': $scope.isa4.dep_code,
                                        'ad_line_no': $scope.msg1.strMessage,
                                        'newQty': $scope.searchitems[i].new_qty,
                                        'newValue': $scope.searchitems[i].new_malc,
                                        'oldQty': $scope.searchitems[i].new_quntity,                                        
                                        'new_malc': $scope.searchitems[i].new_malc,
                                        'adj_inv_no': $scope.searchitems[i].im_inv_no,
                                        'adj_grv_no': $scope.grvno,
                                        'remark': $scope.remark
                                    });
                                    Savedata1.push(Save_sa1);

                                }

                                $http.post(ENV.apiUrl + "api/StockAdjustment/Insertadjustmentsdetails", Savedata1).then(function (msg) {
                                    $scope.msg1 = '';
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1.strMessage != undefined) {
                                        if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                            swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });

                                            
                                            var arr = [];
                                            var ob = {};
                                            for (var i = 0; i < $scope.searchitems.length; i++) {
                                                ob.im_item_code = $scope.searchitems[i].im_item_code;
                                                arr.push(ob.im_item_code);
                                            }

                                            if (arr.length > 0) {
                                                $scope.t = 'Invs.INVR36';
                                                var data = {
                                                    location: $scope.t,
                                                    parameter: {
                                                        //item_code: arr + "",
                                                        item_code: dco_no_712,
                                                        stock_status: '+',
                                                        dep_code: $scope.isa4.dep_code,
                                                        anotation: $scope.remark,
                                                    },
                                                    state: 'main.Inv047'
                                                }
                                                window.localStorage["ReportDetails"] = JSON.stringify(data)
                                                $state.go('main.ReportCardParameter')
                                            }

                                            $scope.Clear();
                                            $scope.Cancel();
                                            $scope.Main_table = false;
                                            $scope.searchitems = [];
                                        }
                                    }
                                    else {
                                        swal({ text: "Record Not Inserted...", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                                    }
                                });

                            }

                        }

                        else {
                            swal({ text: "Record Not Inserted...", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                        }
                    });

                }
            }

            $scope.Remove = function () {

                if (($scope.remark == null || $scope.remark == undefined)) {
                    swal({ title: "Alert", text: "Please Enter Remark", showCloseButton: true, width: 380, });
                }

                else {

                    var Removedata = [];
                    var Remove_sa = ({
                        'tdate': $scope.dt.sims_from_date,
                        'loc_code': $scope.isa.loc_code,
                        'sup_sblgr_acno': $scope.isa1.sup_sblgr_acno,
                        'ar_code': $scope.isa2.ar_code,
                        'dco_code': $scope.isa3.dco_code,
                        'dep_code': $scope.isa4.dep_code,
                        'adj_grv_no': $scope.grvno,
                        'remark': $scope.remark
                    });
                    Removedata.push(Remove_sa);

                    $http.post(ENV.apiUrl + "api/StockAdjustment/Removeadjustments", Removedata).then(function (msg) {

                        $scope.msg1 = msg.data;
                        if ($scope.msg1.strMessage != undefined) {
                            if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                var Removedata1 = [];



                                for (var i = 0 ; i < $scope.searchitemsremove.length; i++) {
                                    var Remove_sa1 = ({
                                        'tdate': $scope.dt.sims_from_date,
                                        'loc_code': $scope.isa.loc_code,
                                        'sup_sblgr_acno': $scope.isa1.sup_sblgr_acno,
                                        'ar_code': $scope.isa2.ar_code,
                                        'dco_code': $scope.isa3.dco_code,
                                        'dep_code': $scope.isa4.dep_code,
                                        'ad_line_no': $scope.msg1.strMessage,
                                        'newQty': $scope.searchitemsremove[i].new_qty,
                                        'newValue': $scope.searchitemsremove[i].new_malc,
                                        'oldQty': $scope.searchitemsremove[i].new_quntity,
                                        'oldVal': $scope.searchitemsremove[i].new_malc,
                                        'adj_inv_no': $scope.searchitemsremove[i].im_inv_no,
                                        'adj_grv_no': $scope.grvno,
                                        'remark': $scope.remark
                                    });
                                    Removedata1.push(Remove_sa1);
                                }


                                $http.post(ENV.apiUrl + "api/StockAdjustment/Removeadjustmentsdetails", Removedata1).then(function (msg) {

                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1.strMessage != undefined) {
                                        if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                            swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });


                                            
                                            var arr = [];
                                            var ob = {};
                                            for (var i = 0; i < $scope.searchitemsremove.length; i++) {
                                                ob.im_item_code = $scope.searchitemsremove[i].im_item_code;
                                                arr.push(ob.im_item_code);
                                            }

                                            if (arr.length > 0) {
                                                $scope.t = 'Invs.INVR36';
                                                var data = {
                                                    location: $scope.t,
                                                    parameter: {
                                                        item_code: arr + "",
                                                        stock_status: '-',
                                                        dep_code: $scope.isa4.dep_code,
                                                        anotation: $scope.remark,
                                                    },
                                                    state: 'main.Inv047'
                                                }
                                                window.localStorage["ReportDetails"] = JSON.stringify(data)
                                                $state.go('main.ReportCardParameter')
                                            }

                                            $scope.Clear();
                                            $scope.Cancel();
                                            $scope.Main_table = false;
                                            $scope.searchitemsremove = [];
                                        }
                                    }
                                    else {
                                        swal({ title: "Alert", text: "Stock Remove...", showCloseButton: true, width: 380, });
                                    }
                                });

                            }

                        }

                        else {
                            swal({ title: "Alert", text: "Stock Not Remove..." + $scope.msg1, showCloseButton: true, width: 380, });
                        }
                    });

                }
            }

            $scope.Clear = function () {
                $scope.value = '';
                $scope.edt = {
                    im_desc: '',
                    item_code: '',
                    id_cur_qty: '',
                    im_malc_rate: '',
                    old_cur_qty: '',
                    old_malc_rate: '',
                }

                $scope.searchitems = [];
                $scope.searchitemsremove = [];
                $scope.GetItems = [];
                $scope.isa.loc_code = '';
                $scope.isa1.sup_sblgr_acno = '';
                $scope.isa2.ar_code = '';
                $scope.isa3.dco_code = '';
                $scope.grvno = '';
                $scope.remark = '';
                $scope.isa4.dep_code = '';
                $scope.Quntity_show_txt = true;
                $scope.Main_table = false;
                $scope.getallcomboBox();
                $scope.Removestock_show = false;
                $scope.Addstock_show = false;
                $scope.Save_hide = false;
                $scope.Remove_hide = false;
            }

            $scope.Cancel = function () {
                $scope.value = '';
                $scope.edt = {
                    im_desc: '',
                    item_code: '',
                    id_cur_qty: '',
                    im_malc_rate: '',
                    old_cur_qty: '',
                    old_malc_rate: '',
                }
                $scope.searchitems = [];
                $scope.searchitemsremove = [];
                $scope.GetItems = [];
                $scope.grvno = '';
                $scope.remark = '';
                $scope.getallcomboBox();
                $scope.Removestock_show = false;
                $scope.Addstock_show = false;
                $scope.Save_hide = false;
                $scope.Remove_hide = false;
            }

            $scope.clear_txt = function () {
                $scope.value = '';
                $scope.edt.im_desc = '';
                $scope.edt.item_code = '';
            }

            $scope.Reset = function () {
                $scope.temp1 = {};
            }


            $scope.get_ischecked = function (str) {
             
                str1 = '';
                if (str == "AI") {
                    $scope.Addscis_show = true;
                    $scope.Remscis_show = false;
                }
                else if (str == "RI") {
                    $scope.Addscis_show = false;
                    $scope.Remscis_show = true;
                }
                str1 = str;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });
        }
        ])
})();