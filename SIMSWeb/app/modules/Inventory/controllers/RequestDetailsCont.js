﻿(function () {
    'use strict';
    var obj1, temp, opr, CurrentDate, comp;
    var main, deletefin = [], data = [], data1 = [], ItemDetails = [], main1;
    var finanacecode = [];
    var check;
    var reqno;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('RequestDetailsCont',
         ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {
             $scope.priview_table = false;
             $scope.pagesize = "5";
             $scope.pageindex = 0;
             $scope.hide_itemne = false;
             $scope.hide_suc = false;

             //$scope.hide_uncode_service = false;
             $scope.hide_uncode_item = true;

             $scope.hide_SelectIetm = false;
             $scope.hide_SelectService = false;
             $scope.Main_table = false;
             $scope.Main_table_Service = false;
             $scope.hide_status_code = true;
             var dataforSave = [];
             $scope.save_btn = true;
             $scope.Update_btn = true;
             $scope.Delete_btn = true;
             $scope.savedisabled = false;
             $scope.updisabled = true;
             $scope.deldisabled = true;
             $scope.display = false;
             $scope.table = true;
             $scope.Grid = true;
             $scope.hide_itemne_total = true;
             $scope.temp = [];
             $scope.temp2 = [];
             $scope.im_inv_no = [];
             $scope.search_Req_Data = [];
             console.clear();

             var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');
             $scope.dt = {
                 req_date: dateyear,
                 rd_date_required: dateyear
             }

             $('#loader0712').modal({ backdrop: 'static', keyboard: false });

             $timeout(function () {
                 $("#fixTable").tableHeadFixer({ 'top': 1 });
             }, 100);

             $scope.getallcomboBox = function () {

                 $http.get(ENV.apiUrl + "api/StockAdjustment/getAllLocation").then(function (AllLoc) {
                     $scope.location = AllLoc.data;
                     $scope.temp = { loc_code: $scope.location[0].loc_code };
                 });

                 $http.get(ENV.apiUrl + "api/StockAdjustment/getAllSupplierName").then(function (AllSna) {
                     $scope.SupplierName = AllSna.data;
                 });

                 $http.get(ENV.apiUrl + "api/StockAdjustment/getsuppliergroupname").then(function (Allsupgname) {
                     $scope.supgname = Allsupgname.data;
                 });

                 $http.get(ENV.apiUrl + "api/StockAdjustment/getAllDepartment").then(function (resd) {
                     $scope.department = resd.data;
                     $scope.temp = { dep_code: $scope.department[0].dep_code };
                 });

                 $http.get(ENV.apiUrl + "api/StockAdjustment/getCategories").then(function (Allcategorycode) {
                     $scope.categorycode = Allcategorycode.data;
                 });

             }

             $scope.getallcomboBox();

             $scope.getCategories = function () {
                 $http.get(ENV.apiUrl + "api/StockAdjustment/getSubCategories?pc_pcode=" + $scope.temp1.category_code).then(function (SubCategories) {
                     $scope.allSubCategories = SubCategories.data;

                 });
             }

             $http.get(ENV.apiUrl + "api/ItemAssembly/GetAssembly").then(function (docstatus1) {
                 $scope.fillcombo = docstatus1.data;
                 console.log($scope.fillcombo);
             });

             $http.get(ENV.apiUrl + "api/RequestDetail/getRequestTypeNew").then(function (reqtypedetailsNew) {
                 $scope.req_type_detailsNew = reqtypedetailsNew.data;
                 $scope.temp['request_mode_code'] = $scope.req_type_detailsNew[0].request_mode_code;
                 if ($scope.temp.request_mode_code == 'S') {
                     $scope.hide_itemne = true;
                     $scope.temp.reqQty = 1;
                     $scope.temp.reqQtyesprice = 0;
                     $scope.reqQtyespricetotal = 0;
                 }
                 else {
                     $scope.temp.reqQty = 1;
                     $scope.temp.reqQtyesprice = 1;
                     $scope.reqQtyespricetotal = 1;
                     $scope.hide_itemne = false;
                 }
                 console.log($scope.req_type_detailsNew);
                 $scope.delivery_mode();
             });

             $http.get(ENV.apiUrl + "api/RequestDetail/getRequestType").then(function (reqtypedetails) {
                 $scope.req_type_details = reqtypedetails.data;
                 $scope.temp['request_type_code'] = $scope.req_type_details[0].request_type_code;
                 console.log($scope.req_type_details);
             });

             $http.get(ENV.apiUrl + "api/RequestDetail/getDeparmentsUsercode?user_code=" + $rootScope.globals.currentUser.username).then(function (deptdata) {
                 $scope.dept_data = deptdata.data;
                 $scope.temp['dept_code'] = $scope.dept_data[0].dept_code;
                 $scope.delivery_mode();
                 console.log($scope.dept_data);
             });

             $scope.delivery_mode = function () {

                 $scope.dt = {
                     req_date: dateyear,
                     rd_date_required: dateyear
                 }

                 if ($scope.temp.request_mode_code == 'S') {
                     $scope.hide_itemne = true;

                     $scope.temp.reqQty = 1;
                     $scope.temp.reqQtyesprice = 0;
                     $scope.reqQtyespricetotal = 0;

                     //$scope.hide_uncode_service = false;
                     $scope.hide_uncode_item = true;
                     $scope.temp.im_remark = '';
                     $scope.hide_SelectIetm = false;
                     $scope.hide_SelectService = true;
                     $scope.hide_suc = false;

                     $scope.im_item_codel = '';
                     $scope.items_Data = [];
                     $http.get(ENV.apiUrl + "api/RequestDetail/getServiceSerch?dep_code", $scope.temp).then(function (res1) {
                         $scope.items_Data = res1.data;
                     });
                 }
                 else {
                     $scope.hide_itemne = false;
                     $scope.hide_SelectIetm = true;
                     $scope.hide_SelectService = false;
                     $scope.temp.reqQty = 1;
                     $scope.temp.reqQtyesprice = 1;
                     $scope.reqQtyespricetotal = 1;
                     $scope.hide_suc = false;

                     //$scope.hide_uncode_service = false;
                     $scope.hide_uncode_item = true;

                     $scope.im_item_codel = '';
                     $scope.temp.im_remark = '';
                     $scope.items_Data = [];
                     $http.get(ENV.apiUrl + "api/RequestDetail/getItemSerch?dep_code", $scope.temp).then(function (res1) {
                         $scope.items_Data = res1.data;
                     });
                 }

                 if ($scope.im_inv_no.length > 0) {
                     $scope.Main_table = false;
                     $scope.Main_table_Service = false;
                     $scope.display = false;
                     $scope.savedisabled = false;
                     $scope.updisabled = true;
                     $scope.im_inv_no = [];
                     $scope.temp.req_no = "";
                     $scope.temp.req_remarks = "";
                     $scope.dt.rd_date_required = "";

                     $scope.Myform.$setPristine();
                     $scope.Myform.$setUntouched();
                 }
                 $('#loader0712').modal('hide');
             }

             $http.get(ENV.apiUrl + "api/RequestDetail/getCurrency").then(function (currencydata) {
                 $scope.currency_data = currencydata.data;
                 $scope.temp['excg_curcy_code'] = $scope.currency_data[0].excg_curcy_code;
             });

             setTimeout(function () {
                 $("#cmb_uom_code").select2();
             }, 100);

             setTimeout(function () {
                 $("#cmb_seritm_code").select2();
             }, 100);

             $scope.getUOM = function (str) {
                 debugger;
                 if ($scope.temp.request_mode_code == 'S') {
                     $scope.hide_itemne = true;
                     $scope.temp.reqQty = 1;
                     $scope.temp.reqQtyesprice = 0;
                     $scope.reqQtyespricetotal = 0;
                     // $scope.temp.uom_code = 0;

                     //Code added for UOM to be displayed in case of service request
                     for (var i = 0; i < $scope.items_Data.length; i++) {
                         if (str == $scope.items_Data[i].im_desc) {
                             $scope.umcode = $scope.items_Data[i].uom_code;
                             break;
                         }
                     }

                     setTimeout(function () {
                         $("#cmb_uom_code").select2();
                         $scope.temp['uom_code'] = $scope.umcode;
                     }, 1000);

                     $http.get(ENV.apiUrl + "api/RequestDetail/getuomdetails").then(function (uom_details) {
                         $scope.uomdetails = uom_details.data;
                         $scope.temp['uom_code'] = $scope.umcode;//.uom_code;
                     });
                     //////////////////////////
                 }
                 else {
                     $scope.temp.reqQty = 1;
                     $scope.temp.reqQtyesprice = 1;
                     $scope.reqQtyespricetotal = 1;

                     for (var i = 0; i < $scope.items_Data.length; i++) {
                         if (str == $scope.items_Data[i].im_desc) {
                             $scope.umcode = $scope.items_Data[i].uom_code;
                             break;
                         }
                     }

                     setTimeout(function () {
                         $("#cmb_uom_code").select2();
                         $scope.temp['uom_code'] = $scope.umcode;
                     }, 1000);

                     //setTimeout(function () {
                     //    $("#cmb_uom_code").select2();
                     //    $scope.temp['uom_code'] = str.uom_code;

                     //}, 100);

                     $http.get(ENV.apiUrl + "api/RequestDetail/getuomdetails").then(function (uom_details) {
                         $scope.uomdetails = uom_details.data;
                         $scope.temp['uom_code'] = $scope.umcode;//.uom_code;
                     });
                 }
             }

             $scope.edt = {};

             $scope.SearchRequest = function (sr) {
                 $('#MyModal1').modal('show');
                 //$http.get(ENV.apiUrl + "api/RequestDetail/getSearchRequest?sr=" + $scope.temp.request_mode_code + "&user_code=" + $rootScope.globals.currentUser.username).then(function (searchReqData) {
                 $http.get(ENV.apiUrl + "api/RequestDetail/getRequestItemSerch?sr=" + $scope.temp.request_mode_code + "&user_code=" + $rootScope.globals.currentUser.username).then(function (searchReqData) {
                     //$scope.search_Req_Data = searchReqData.data;
                     $scope.search_Req_SIData = searchReqData.data;
                     //console.log($scope.search_Req_Data);
                 });
                 $scope.Grid = true;
                 $scope.savedisabled = true;
                 $scope.updisabled = false;
             }

             $scope.SelectService = function () {

                 $scope.GetItems = [];
                 $scope.ServiceSearch = true;
                 $('#MyModal2').modal('show');
                 $scope.display = true;
                 $scope.updisabled = true;
                 $scope.deldisabled = true;
                 $scope.savedisabled = false;
             }

             $scope.SelectIetm = function () {

                 $scope.GetItems = [];
                 $scope.itemSearch = true;
                 $('#MyModal').modal('show');
                 $scope.display = true;
                 $scope.updisabled = true;
                 $scope.deldisabled = true;
                 $scope.savedisabled = false;
             }

             $scope.geteditData = function (req_no) {
                 $scope.im_inv_no = [];
                 $scope.Grid = true;
                 $scope.Main_table = true;
                 $scope.Main_table_Service = false;

                 $http.get(ENV.apiUrl + "api/RequestDetail/getGridDataforEdit?req_no=" + req_no).then(function (getGridEditData) {
                     $scope.getGrid_Edit_Data_lst = getGridEditData.data;
                     for (var i = 0; i < $scope.getGrid_Edit_Data_lst.length; i++) {
                         $scope.im_inv_no.push($scope.getGrid_Edit_Data_lst[i])
                     }
                     console.log($scope.getGrid_Edit_Data);
                 });
             }

             $scope.geteditDataService = function (req_no) {
                 $scope.im_inv_no = [];
                 $scope.Grid = true;
                 $scope.Main_table = false;
                 $scope.Main_table_Service = true;

                 $http.get(ENV.apiUrl + "api/RequestDetail/getGridDataforEdit?req_no=" + req_no).then(function (getGridEditData) {
                     $scope.getGrid_Edit_Data_lst = getGridEditData.data;
                     for (var i = 0; i < $scope.getGrid_Edit_Data_lst.length; i++) {
                         $scope.im_inv_no.push($scope.getGrid_Edit_Data_lst[i])
                     }
                     console.log($scope.getGrid_Edit_Data);
                 });
             }

             //Update Selection Window Code For Item/Services...

             $scope.okbuttonSerclick = function (oi) {
                 debugger;
                 $scope.lencheck = [];
                 var data = {};
                 $scope.im_inv_no = [];

                 //var req_no = null;
                 //for (var i = 0; i < $scope.search_Req_SIData.length; i++) {
                 //    var v = document.getElementById($scope.search_Req_SIData[i].req_no + i);
                 //    if (v.checked == true) {
                 //        $scope.flag = true;
                 //        req_no = req_no + $scope.search_Req_SIData[i].req_no + ',';
                 //    }
                 //}
                 $('#MyModal1').modal('hide');

                 data.req_no = oi.req_no;
                 data.req_type = $scope.temp.request_mode_code;
                 data.sm_code = $rootScope.globals.currentUser.username;

                 $http.post(ENV.apiUrl + "api/RequestDetail/RequestDetailsItemSearch", data).then(function (searchReqISRecord) {
                     $scope.search_Req_Data = searchReqISRecord.data;

                     if ($scope.search_Req_Data.length > 0) {
                         if ($scope.temp.request_mode_code == 'I') {
                             for (var i = 0; i < $scope.search_Req_Data.length; i++) {
                                 var t = $scope.search_Req_Data[i].req_no;
                                 //var v = document.getElementById(t + i);
                                 var v = document.getElementById(t);
                                 //if (v.checked == true) {
                                 //$scope.inv_no = $scope.search_Req_Data[i].im_inv_no
                                 //$scope.inv_desc = $scope.search_Req_Data[i].im_desc
                                 //$scope.sup_code = $scope.search_Req_Data[i].sup_code
                                 //invno = $scope.inv_no;
                                 $scope.im_inv_no.push($scope.search_Req_Data[i]);
                                 $scope.lencheck.push($scope.search_Req_Data[i]);

                                 $('#MyModal1').modal('hide');
                                 $scope.temp = {

                                     'req_no': $scope.search_Req_Data[i].req_no,
                                     'request_mode_code': $scope.search_Req_Data[i].req_type,
                                     'request_mode_name': $scope.search_Req_Data[i].req_type_name,
                                     'request_type_code': $scope.search_Req_Data[i].req_mode,
                                     'request_type_name': $scope.search_Req_Data[i].req_mode_name,
                                     'dept_code': $scope.search_Req_Data[i].dep_code,
                                     'dm_code': $scope.search_Req_Data[i].dm_code,
                                     'request_status_code': $scope.search_Req_Data[i].req_status,
                                     'salesman_code': $scope.search_Req_Data[i].sm_code,
                                     'req_remarks': $scope.search_Req_Data[i].req_remarks

                                 }
                                 //$scope.geteditDataService($scope.search_Req_Data[i].req_no);
                                 //}
                             }

                             $scope.reqQtyespricetotal = 0;
                             $scope.temp.reqQtyesprice = 0;
                             $scope.temp.reqQty = 0;

                             if ($scope.lencheck.length > 0) {
                                 $scope.Main_table = true;
                                 $scope.Main_table_Service = false;
                                 $scope.hide_suc = true;
                                 $scope.lencheck = [];
                             }
                         }

                         else if ($scope.temp.request_mode_code == 'S') {

                             for (var i = 0; i < $scope.search_Req_Data.length; i++) {
                                 var t = $scope.search_Req_Data[i].req_no;
                                 //var v = document.getElementById(t + i);
                                 //var v = document.getElementById(t);
                                 //if (v.checked == true) {
                                 //$scope.inv_no = $scope.search_Req_Data[i].im_inv_no
                                 //$scope.inv_desc = $scope.search_Req_Data[i].im_desc
                                 //$scope.sup_code = $scope.search_Req_Data[i].sup_code
                                 //invno = $scope.inv_no;

                                 $scope.im_inv_no.push($scope.search_Req_Data[i]);
                                 $scope.lencheck.push($scope.search_Req_Data[i]);

                                 $('#MyModal1').modal('hide');
                                 $scope.temp = {
                                     'req_no': $scope.search_Req_Data[i].req_no,
                                     'request_mode_code': $scope.search_Req_Data[i].req_type,
                                     'request_mode_name': $scope.search_Req_Data[i].req_type_name,
                                     'request_type_code': $scope.search_Req_Data[i].req_mode,
                                     'request_type_name': $scope.search_Req_Data[i].req_mode_name,
                                     'dept_code': $scope.search_Req_Data[i].dep_code,
                                     'dm_code': $scope.search_Req_Data[i].dm_code,
                                     'request_status_code': $scope.search_Req_Data[i].req_status,
                                     'salesman_code': $scope.search_Req_Data[i].sm_code,
                                     'req_remarks': $scope.search_Req_Data[i].req_remarks
                                 }
                                 //$scope.geteditDataService($scope.search_Req_Data[i].req_no);
                             }


                             $scope.reqQtyespricetotal = 0;
                             $scope.temp.reqQtyesprice = 0;
                             $scope.temp.reqQty = 0;

                             if ($scope.lencheck.length > 0) {
                                 $scope.Main_table = false;
                                 $scope.Main_table_Service = true;
                                 $scope.hide_suc = true;
                                 $scope.lencheck = [];
                             }
                             //}
                         }
                     }
                 });
             }

             $scope.editData = function (info) {
                 $scope.hide_suc = true;
                 if ($scope.temp.request_mode_code == 'I') {
                     $('#MyModal1').modal('hide');
                     $scope.temp = {
                         'req_no': info.req_no,
                         'request_mode_code': info.req_type,
                         'request_mode_name': info.req_type_name,
                         'request_type_code': info.req_mode,
                         'request_type_name': info.req_mode_name,
                         'dept_code': info.dep_code,
                         'dm_code': info.dm_code,
                         'request_status_code': info.req_status,
                         'salesman_code': info.sm_code,
                         'req_remarks': info.req_remarks
                     }
                     $scope.geteditData(info.req_no);
                 }

                 else if ($scope.temp.request_mode_code == 'S') {
                     $('#MyModal1').modal('hide');
                     $scope.temp = {
                         'req_no': info.req_no,
                         'request_mode_code': info.req_type,
                         'request_mode_name': info.req_type_name,
                         'request_type_code': info.req_mode,
                         'request_type_name': info.req_mode_name,
                         'dept_code': info.dep_code,
                         'dm_code': info.dm_code,
                         'request_status_code': info.req_status,
                         'salesman_code': info.sm_code,
                         'req_remarks': info.req_remarks
                     }
                     $scope.geteditDataService(info.req_no);
                 }
             }

             //End...

             $scope.CurrentDate = new Date();
             var today = new Date();
             var dd = today.getDate();
             var mm = today.getMonth() + 1;
             var yyyy = today.getFullYear();

             //PreviewButton
             $scope.Preview = function (assembly) {

                 if ($scope.temp.assembly == 'undefined' || $scope.temp.assembly == null) {
                     swal({ title: "Alert", text: "Select Assembly No", width: 300, height: 200 });
                     exit;
                 }
                 else {

                     $http.post(ENV.apiUrl + "api/ItemAssembly/SelectPreview?assembly=" + $scope.temp.assembly).then(function (docstatus1) {
                         $scope.PriviewData = docstatus1.data;
                         console.log($scope.PriviewData);
                     });
                 }

                 $scope.priview_table = true;
             }

             $scope.checkvalue = function () {
                 if ($scope.temp.check == true) {
                     $scope.check = "Y";
                 }
             }

             $scope.SelectAssembly = function () {
                 $scope.itemSearch = false;
                 $scope.GetItems = [];
                 $('#MyModal').modal('show');
                 $scope.display = true;
             }

             $scope.RemoveEnrollMentNo = function ($event, index, str) {
                 str.splice(index, 1);
             }

             //Fetch Data For Item...

             $scope.Fetch = function () {
                 debugger;//
                 if ($scope.itemSearch) {//api/StockAdjustment/postgetItemSerch
                     $http.post(ENV.apiUrl + "api/RequestDetail/postItemSerch", $scope.temp1).then(function (res1) {
                         $scope.GetItems = res1.data;
                         $scope.totalItems = $scope.GetItems.length;
                         $scope.todos = $scope.GetItems;
                         $scope.makeTodos();
                         $scope.hide_suc = true;
                     });
                 }
             }

             //End

             //Fetch Data For Service...

             $scope.ServiceFetch = function () {
                 if ($scope.ServiceSearch) {
                     debugger;
                     $http.get(ENV.apiUrl + "api/RequestDetail/getServiceSearch?im_no=" + $scope.temp2.im_inv_no + "&des_ser=" + $scope.temp2.im_desc + "&dep_name=" + $scope.temp2.dept_name).then(function (res1) {
                         $scope.GetItems = res1.data;
                         $scope.totalItems = $scope.GetItems.length;
                         $scope.todos = $scope.GetItems;
                         $scope.makeTodos();
                         $scope.hide_suc = true;
                     });
                 }
             }

             //End



             //Table Assign For Insert Request Detail...
             var invno;

             $scope.okbuttonclick = function () {

                 if ($scope.temp.request_mode_code == 'I') {

                     if ($scope.itemSearch) {
                         for (var i = 0; i < $scope.GetItems.length; i++) {
                             var t = $scope.GetItems[i].im_inv_no;
                             var v = document.getElementById(t + i);
                             if (v.checked == true) {
                                 $scope.GetItems[i]['excg_curcy_code'] = $scope.temp.excg_curcy_code;
                                 $scope.GetItems[i]['rd_remarks'] = $scope.temp.im_remark;
                                 $scope.GetItems[i]['rd_date_required'] = $scope.dt.rd_date_required;
                                 $scope.GetItems[i]['rd_quantity'] = $scope.temp.reqQty;
                                 $scope.GetItems[i]['rd_estimate_price'] = $scope.temp.reqQtyesprice;
                                 $scope.GetItems[i]['reqQtyespricetotal'] = $scope.reqQtyespricetotal;
                                 $scope.im_inv_no.push($scope.GetItems[i]);
                             }
                         }
                         if ($scope.im_inv_no.length > 0) {
                             $scope.Main_table = true;
                             $scope.Main_table_Service = false;
                             $scope.Grid = true;
                             $scope.hide_suc = true;
                             main.checked = false;
                         }
                     }
                     else {
                         for (var i = 0; i < $scope.GetItems.length; i++) {
                             var t = $scope.GetItems[i].im_inv_no;
                             var v = document.getElementById(t + i);
                             if (v.checked == true) {
                                 $scope.inv_no = $scope.GetItems[i].im_inv_no
                                 $scope.inv_desc = $scope.GetItems[i].im_desc
                                 $scope.sup_code = $scope.GetItems[i].sup_code
                                 //$scope.GetItems['excg_curcy_code'] = $scope.currency_data[0].excg_curcy_code;
                                 invno = $scope.inv_no;
                             }
                         }
                         $scope.Grid = true;
                         $scope.hide_suc = true;
                     }
                     $scope.Reset();
                 }

                 if ($scope.temp.request_mode_code == 'S') {

                     if ($scope.ServiceSearch) {
                         for (var i = 0; i < $scope.GetItems.length; i++) {
                             //var t = $scope.GetItems[i].im_inv_no;
                             //var v = document.getElementById(t + i + 1);
                             if ($scope.GetItems[i].ischecked == true) {
                                 $scope.GetItems[i]['excg_curcy_code'] = $scope.temp.excg_curcy_code;
                                 $scope.GetItems[i]['rd_remarks'] = $scope.temp.im_remark;
                                 $scope.GetItems[i]['rd_date_required'] = $scope.dt.rd_date_required;

                                 $scope.GetItems[i]['rd_quantity'] = $scope.temp.reqQty;
                                 $scope.GetItems[i]['rd_estimate_price'] = $scope.temp.reqQtyesprice;
                                 $scope.GetItems[i]['reqQtyespricetotal'] = $scope.reqQtyespricetotal;


                                 $scope.im_inv_no.push($scope.GetItems[i]);
                             }
                         }
                         if ($scope.im_inv_no.length > 0) {
                             $scope.Main_table = false;
                             $scope.Main_table_Service = true;
                             $scope.Grid = true;
                             $scope.hide_suc = true;
                             main1.checked = false;
                         }
                     }
                     else {
                         for (var i = 0; i < $scope.GetItems.length; i++) {
                             if ($scope.GetItems[i].ischecked == true) {
                                 $scope.inv_no = $scope.GetItems[i].im_inv_no
                                 $scope.inv_desc = $scope.GetItems[i].im_desc
                                 $scope.sup_code = $scope.GetItems[i].sup_code
                                 invno = $scope.inv_no;
                             }

                         }
                         $scope.Grid = true;
                         $scope.hide_suc = true;
                     }
                     $scope.ServiceReset();
                 }

                 $scope.GetItems = [];
             }

             var datasend = [];
             var datasend1 = [];

             $scope.IsChangeFunction = function (info) {
                 info.ischange = true;
             }

             $scope.Add = function (myForm) {
                 debugger;

                 if ($scope.temp.request_mode_code == 'I') {
                     if ($scope.im_item_codel == undefined || $scope.im_item_codel == '') {
                         swal({ title: "Alert", text: "Please Select Item", width: 300, height: 200 });
                         return;
                     }
                     if ($scope.temp.reqQty == undefined) {
                         swal({ title: "Alert", text: "Please Enter Quantity", width: 300, height: 200 });
                         return;
                     }

                     if ($scope.temp.reqQty == '') {
                         swal({ title: "Alert", text: "Please Enter Quantity", width: 300, height: 200 });
                         return;
                     }

                     if ($scope.temp.reqQty == 0) {
                         swal({ title: "Alert", text: "Please Enter Quantity More Than Zero", width: 300, height: 200 });
                         return;
                     }

                     if ($scope.dt.rd_date_required == undefined || $scope.dt.rd_date_required == '') {
                         swal({ title: "Alert", text: "Please Select Require Date", width: 300, height: 200 });
                         return;
                     }

                     if ($scope.updisabled) {
                         $scope.Main_table = true;
                         $scope.Main_table_Service = false;
                         $scope.Grid = true;

                         for (var i = 0; i < $scope.items_Data.length; i++) {
                             if ($scope.im_item_codel == $scope.items_Data[i].im_desc) {
                                 $scope.items_Data[i].rd_quantity = $scope.temp.reqQty;
                                 $scope.items_Data[i].rd_remarks = $scope.temp.im_remark;
                                 $scope.items_Data[i].rd_date_required = $scope.dt.rd_date_required;
                                 $scope.items_Data[i].rd_estimate_price = $scope.temp.reqQtyesprice;
                                 $scope.items_Data[i].reqQtyespricetotal = $scope.reqQtyespricetotal;
                                 $scope.items_Data[i].excg_curcy_code = $scope.temp.excg_curcy_code;
                                 $scope.im_inv_no.push($scope.items_Data[i]);
                                 break;
                             }
                         }

                         $scope.deldisabled = false;
                     }
                     else {
                         $scope.updisabled = true;
                         $scope.savedisabled = false;
                         //$scope.im_inv_no = [];
                         $scope.Add();
                     }
                     if ($scope.search_Req_Data.length > 0) {
                         $scope.updisabled = false;
                         $scope.savedisabled = true;
                     }
                     else {
                         $scope.updisabled = true;
                         $scope.savedisabled = false;
                         $scope.hide_suc = true;
                     }
                 }

                 if ($scope.temp.request_mode_code == 'S') {
                     if ($scope.im_item_codel == undefined || $scope.im_item_codel == '') {
                         swal({ title: "Alert", text: "Please Select Item", width: 300, height: 200 });
                         return;
                     }
                     if (($scope.temp.reqQty == undefined || $scope.temp.reqQty == '') && $scope.temp.reqQty != 0) {
                         swal({ title: "Alert", text: "Please Enter Quantity", width: 300, height: 200 });
                         return;
                     }
                     if ($scope.dt.rd_date_required == undefined || $scope.dt.rd_date_required == '') {
                         swal({ title: "Alert", text: "Please Select Require Date", width: 300, height: 200 });
                         return;
                     }

                     if ($scope.updisabled) {
                         $scope.Main_table = false;
                         $scope.Main_table_Service = true;
                         $scope.Grid = true;
                         for (var i = 0; i < $scope.items_Data.length; i++) {
                             if ($scope.im_item_codel == $scope.items_Data[i].im_desc) {
                                 $scope.items_Data[i].rd_date_required = $scope.dt.rd_date_required;
                                 $scope.items_Data[i].rd_quantity = $scope.temp.reqQty;
                                 $scope.items_Data[i].rd_estimate_price = $scope.temp.reqQtyesprice;
                                 $scope.items_Data[i].reqQtyespricetotal = $scope.reqQtyespricetotal;
                                 $scope.items_Data[i].rd_remarks = $scope.temp.im_remark;
                                 $scope.items_Data[i].excg_curcy_code = $scope.temp.excg_curcy_code;
                                 $scope.im_inv_no.push($scope.items_Data[i]);
                                 break;
                             }
                         }
                         $scope.deldisabled = false;
                     }
                     else {
                         $scope.updisabled = true;
                         $scope.savedisabled = false;
                         //$scope.im_inv_no = [];
                         $scope.Add();
                     }

                     if ($scope.search_Req_Data.length > 0) {
                         $scope.updisabled = false;
                         $scope.savedisabled = true;
                     }
                     else {
                         $scope.updisabled = true;
                         $scope.savedisabled = false;
                         $scope.hide_suc = true;
                     }

                     //$scope.hide_suc = true;
                 }
                 $scope.im_item_codel = '';
                 $('#rcbItem').focus();
             };

             $scope.savedata = function (myform) {
                 debugger;
                 if (myform) {
                     //if ($scope.im_inv_no[i].ischange == true) {
                     if ($scope.im_inv_no.length > 0) {
                         for (var i = 0; i < $scope.im_inv_no.length; i++) {
                             if ($scope.im_inv_no[i].rd_date_required == undefined)
                                 $scope.im_inv_no[i].rd_date_required = $scope.dt.rd_date_required;

                             data = {
                                 'opr': 'R',
                                 'req_no': $scope.im_inv_no[i].req_no,
                                 'im_inv_no': $scope.im_inv_no[i].im_inv_no,
                                 'rd_status': 0,
                                 'rd_date_required': $scope.im_inv_no[i].rd_date_required,
                                 'rd_quantity': $scope.im_inv_no[i].rd_quantity,
                                 'rd_remarks': $scope.im_inv_no[i].rd_remarks,
                                 'sup_code': $scope.im_inv_no[i].sup_code,
                                 'uom_code': $scope.im_inv_no[i].uom_code,
                                 'excg_curcy_code': $scope.im_inv_no[i].excg_curcy_code,
                                 'rd_estimate_price': $scope.im_inv_no[i].rd_estimate_price,
                                 'im_desc': $scope.im_inv_no[i].im_desc

                             }
                             datasend.push(data);

                         }

                         if ($scope.temp.req_remarks == undefined)
                             $scope.temp.req_remarks = null;
                         $scope.data1 = {
                             'opr': 'I',
                             'request_mode_code': $scope.temp.request_mode_code,
                             'request_type_code': $scope.temp.request_type_code,
                             'dept_code': $scope.temp.dept_code,
                             'req_date': $scope.dt.req_date,
                             'dm_code': $scope.temp.dm_code,
                             'request_status_code': $scope.temp.request_status_code,
                             'salesman_code': $rootScope.globals.currentUser.username,//$scope.temp.salesman_code,
                             'req_remarks': $scope.temp.req_remarks
                         }

                         $http.post(ENV.apiUrl + "api/RequestDetail/CUDRequestDetails?data1=" + JSON.stringify($scope.data1), datasend).then(function (msg) {
                             $scope.msg1 = msg.data;
                             if ($scope.msg1 != null || $scope.msg1 != undefined) {
                                 //'Purchase Reuquisition No:{Request No} has been created successfully
                                 var m = 'Purchase Requisition No: ' + $scope.msg1.strMessage + ' has been created successfully';
                                 swal({ title: "Alert", text: m, width: 450, height: 200 });

                                 $http.get(ENV.apiUrl + "api/RequestDetail/getApproveReport?Request_Type=" + $scope.temp.request_mode_code).then(function (approvereno) {
                                     var re_no_new = $scope.msg1.strMessage;
                                     debugger;
                                     //var re_nonew = [];
                                     //re_nonew = re_no_new.split('-');
                                     //var f = re_nonew[0];

                                     //if (f == undefined) {
                                     //    f = null;
                                     //}

                                     //var re_nonew = [];
                                     //re_nonew = re_no_new.split('-');

                                     var f = $scope.msg1.strMessage;
                                     //var f = re_no_new;

                                     if (re_no_new == undefined) {
                                         f = 0;
                                     }

                                     else {
                                         if ($scope.temp.request_mode_code == 'I') {
                                             $scope.t = approvereno.data;
                                             var data = {
                                                 location: $scope.t,
                                                 parameter: {
                                                     //req_from: f,
                                                     //req_to: f,

                                                     Reqno: f,
                                                     Reqtype: $scope.temp.request_mode_code,
                                                     Subject: $scope.temp.subject_matter,

                                                 },
                                                 state: 'main.Inv058'
                                             }
                                             window.localStorage["ReportDetails"] = JSON.stringify(data)
                                             $state.go('main.ReportCardParameter')
                                         }

                                         else if ($scope.temp.request_mode_code == 'S') {
                                             $scope.t = approvereno.data;
                                             var data = {
                                                 location: $scope.t,
                                                 parameter: {
                                                     Reqno: f,
                                                     Reqtype: $scope.temp.request_mode_code,
                                                     Subject: $scope.temp.subject_matter,

                                                 },
                                                 state: 'main.Inv058'
                                             }
                                             window.localStorage["ReportDetails"] = JSON.stringify(data)
                                             $state.go('main.ReportCardParameter')
                                         }
                                     }
                                 });

                             }
                             else {
                                 swal({ title: "Alert", text: "Record Not Inserted. ", width: 300, height: 200 });
                             }

                         });
                         datasend = [];
                         datasend1 = [];
                         $scope.table = true;
                         $scope.display = false;
                         $scope.Cancel();
                     }

                     else {
                         swal({ title: "Alert", text: "Please Add Record", width: 300, height: 200 });
                     }
                     //}
                     //else {
                     //    swal({ title: "Alert", text: "Please Enter Require Date", width: 300, height: 200 });
                     //}
                 }
             }


             //End

             //Update
             var datau = [];

             $scope.Update = function (j) {

                 for (var i = 0; i < $scope.im_inv_no.length; i++) {
                     //if ($scope.im_inv_no[i].ischange == true)
                     debugger;
                     {
                         var datal = {
                             'opr': 'Q',
                             'req_no': $scope.im_inv_no[i].req_no,
                             'im_inv_no': $scope.im_inv_no[i].im_inv_no,
                             'req_status': $scope.im_inv_no[i].req_status,
                             'rd_date_required': $scope.im_inv_no[i].rd_date_required,
                             'rd_quantity': $scope.im_inv_no[i].rd_quantity,
                             'rd_remarks': $scope.im_inv_no[i].rd_remarks,
                             'sup_code': $scope.im_inv_no[i].sup_code,
                             'uom_code': $scope.im_inv_no[i].uom_code,
                             'excg_curcy_code': $scope.im_inv_no[i].excg_curcy_code,
                             'im_sell_price': $scope.im_inv_no[i].rd_estimate_price,
                             //'request_mode_code': $scope.temp.request_mode_code,   req_type,
                             //'req_type': $scope.$scope.im_inv_no[i].req_type,
                             //'req_mode': $scope.$scope.im_inv_no[i].req_mode,
                             'im_desc': $scope.im_inv_no[i].im_desc
                         }
                         datau.push(datal);
                     }
                 }

                 $scope.datap = {
                     'opr': 'N',
                     'req_no': $scope.temp.req_no,
                     //'request_type_code': $scope.temp.request_type_code,
                     //'dept_code': $scope.temp.dept_code,
                     //'dm_code': $scope.temp.dm_code,
                     //'request_status_code': $scope.temp.request_status_code,
                     //'salesman_code': $scope.temp.salesman_code,
                     'req_remarks': $scope.temp.req_remarks,
                     //'request_mode_code': $scope.temp.request_mode_code
                 }

                 $http.post(ENV.apiUrl + "api/RequestDetail/CUDRequestDetailsUpdate?data2=" + JSON.stringify($scope.datap), datau).then(function (msg) {
                     $scope.msg1 = msg.data;
                     if ($scope.msg1 == true) {
                         swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                     }
                     else if ($scope.msg1 == false) {
                         swal({ title: "Alert", text: "Record Not Updated. " , width: 300, height: 200 });
                     }
                     else {
                         swal("Error-" + $scope.msg1)
                     }
                     $scope.Clear();
                     $scope.Cancel();
                 });
                 datasend = [];
                 datasend1 = [];
                 datau = [];
                 $scope.table = true;
                 $scope.display = false;
                 $state.go('main.Inv058');
             }

             //Delete
             $scope.Delete = function () {

                 $scope.RemoveEnrollMentNo();
             }

             //Select Data SHOW
             $http.get(ENV.apiUrl + "api/InvsParameter/GetCompanyName").then(function (docstatus1) {
                 $scope.Compname = docstatus1.data;
                 console.log($scope.Compname);
             });

             $scope.size = function (str) {
                 console.log(str);
                 $scope.pagesize = str;
                 $scope.currentPage = 1;
                 $scope.numPerPage = str;
                 console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
             }

             $scope.index = function (str) {
                 $scope.pageindex = str;
                 $scope.currentPage = str;
                 console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                 main.checked = false;
             }

             $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

             $scope.makeTodos = function () {
                 var rem = parseInt($scope.totalItems % $scope.numPerPage);
                 if (rem == '0') {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                 }
                 else {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                 }

                 var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                 var end = parseInt(begin) + parseInt($scope.numPerPage);

                 console.log("begin=" + begin); console.log("end=" + end);

                 $scope.filteredTodos = $scope.todos.slice(begin, end);
             };

             $scope.searched = function (valLists, toSearch) {
                 return _.filter(valLists,

                 function (i) {
                     /* Search Text in all  fields */
                     return searchUtil(i, toSearch);
                 });
             };

             //Search
             $scope.search = function () {
                 $scope.todos = $scope.searched($scope.getParameter, $scope.searchText);
                 $scope.totalItems = $scope.todos.length;
                 $scope.currentPage = '1';
                 if ($scope.searchText == '') {
                     $scope.todos = $scope.getParameter;
                 }
                 $scope.makeTodos();
                 main.checked = false;
                 $scope.CheckAllChecked();
             }

             function searchUtil(item, toSearch) {
                 /* Search Text in all 3 fields */
                 return (item.invs_appl_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.invs_appl_form_field.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.invs_appl_parameter.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.invs_appl_form_field_value1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.acno == toSearch) ? true : false;
             }

             //NEW BUTTON
             $scope.New = function () {
                 $scope.disabled = false;
                 $scope.gdisabled = false;
                 $scope.aydisabled = false;
                 $scope.table = false;
                 $scope.display = true;
                 $scope.save_btn = true;
                 $scope.Update_btn = false;
                 $scope.divcode_readonly = false;
                 $scope.temp = "";
                 $scope.temp = {};
                 $scope.temp['sims_library_catalogue_status'] = true;
                 $scope.temp["sims_library_catalogue_name"] = "";
             }

             //DATA CANCEL

             $scope.Clear = function () {
                 $scope.im_item_codel = "";
                 $scope.temp.uom_code = "";
                 $scope.temp.im_remark = "";
                 $scope.temp.req_remarks = "";
                 $scope.im_item_codel = '';
                 //$scope.dt.rd_date_required = '';
                 $scope.hide_suc = false;
                 $state.go('main.Inv058');
                 $("#cmb_uom_code").select2("val", "");
                 $scope.temp.reqQty = 1;
                 $scope.temp.reqQtyesprice = 0;
                 $scope.reqQtyespricetotal = 0;

                 $scope.delivery_mode();

                 $scope.dt = {
                     req_date: dateyear,
                     rd_date_required: dateyear
                 }
                 //$state.go('main.Inv058');


                 //$("#Select4").select2("val", "");
                 //$scope.dt.req_date = "";
                 //$scope.temp.dept_code = "";
                 //$scope.temp.dm_code = "";
                 //$scope.temp.salesman_code = "";

             }

             $scope.Cancel = function () {
                 $scope.Clear();
                 $scope.im_inv_no = [];
                 $scope.search_Req_Data = [];
                 $scope.table = true;
                 $scope.Main_table = false;
                 $scope.Main_table_Service = false;
                 $scope.display = false;
                 $scope.savedisabled = false;
                 $scope.updisabled = true;

                 $scope.temp.req_no = "";
                 $scope.temp.salesman_code = "";
                 $scope.temp.req_remarks = "";
                 $scope.dt.rd_date_required = "";
                 $scope.Myform.$setPristine();
                 $scope.Myform.$setUntouched();
                 $scope.hide_suc = false;
                 $scope.im_item_codel = "";
                 $("#cmb_uom_code").select2("val", "");
                 $scope.temp.reqQty = 1;
                 $scope.temp.reqQtyesprice = 0;
                 $scope.reqQtyespricetotal = 0;
                 $state.go('main.Inv058');
                 $scope.dt = {
                     req_date: dateyear,
                     rd_date_required: dateyear
                 }

                 //$scope.temp.dept_code = "";
                 //$scope.temp.dm_code = "";
                 //$("#Select4").select2("val", "");
             }

             $scope.clearonadd = function () {

                 $scope.temp.req_no = "";
                 //$scope.temp.dept_code = "";
                 //$scope.temp.dm_code = "";
                 $scope.temp.salesman_code = "";
                 $scope.temp.req_remarks = "";
             }

             //DATA EDIT
             $scope.edit = function (str) {

                 $scope.gdisabled = false;
                 $scope.aydisabled = true;
                 $scope.table = false;
                 $scope.display = true;
                 $scope.save_btn = false;
                 $scope.Update_btn = true;
                 $scope.divcode_readonly = true;
                 //  $scope.temp = str;

                 $scope.temp = {

                     invs_comp_code: str.invs_comp_code,
                     invs_appl_code: str.invs_appl_code,
                     invs_appl_form_field: str.invs_appl_form_field,
                     invs_appl_parameter: str.invs_appl_parameter,
                     invs_appl_form_field_value1: str.invs_appl_form_field_value1,
                     invs_appl_form_field_value2: str.invs_appl_form_field_value2,
                     invs_appl_form_field_value3: str.invs_appl_form_field_value3,
                     invs_appl_form_field_value4: str.invs_appl_form_field_value4,

                     invs_comp_code_old: str.invs_comp_code,
                     invs_appl_code_old: str.invs_appl_code,
                     invs_appl_form_field_old: str.invs_appl_form_field,
                     invs_appl_parameter_old: str.invs_appl_parameter,
                     invs_appl_form_field_value1_old: str.invs_appl_form_field_value1,
                     invs_appl_form_field_value2_old: str.invs_appl_form_field_value2,
                     invs_appl_form_field_value3_old: str.invs_appl_form_field_value3,
                     invs_appl_form_field_value4_old: str.invs_appl_form_field_value4,
                 };
             }

             $timeout(function () {
                 $("#fixTable").tableHeadFixer({ 'top': 1 });
             }, 100);

             $timeout(function () {
                 $("#fixTable1").tableHeadFixer({ 'top': 1 });
             }, 100);

             $scope.Reset = function () {
                 $scope.temp1 = {
                     im_inv_no: '',
                     im_desc: '',
                     im_item_code: '',
                     dept_code: '',
                     sec_code: '',
                     code: '',
                     category_code: '',
                     subcategory_code: '',
                     im_assembly_ind: '',
                     //sup_sblgr_acno:''
                 }
                 $scope.GetItems = [];
             }

             $scope.ServiceReset = function () {
                 $scope.temp2 = {
                     im_inv_no: '',
                     im_desc: '',
                     dept_name: '',
                 }
                 $scope.GetItems = [];
             }

             $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                 $('input[type="text"]', $(this).parent()).focus();
             });

             $('*[data-datepicker="true"] input[type="text"]').datepicker({
                 todayBtn: true,
                 orientation: "top left",
                 autoclose: true,
                 todayHighlight: true,
                 format: 'dd-mm-yyyy'
             });

             $scope.calculate_cost = function (o) {
                 var cla_value_new1 = parseFloat(o.rd_quantity) * parseFloat(o.rd_estimate_price);
                 o.reqQtyespricetotal = cla_value_new1;
             }

             $scope.Val_Cal = function (quty, rate) {
                 debugger;
                 var cla_value_new = parseFloat($scope.temp.reqQty) * parseFloat($scope.temp.reqQtyesprice);
                 $scope.reqQtyespricetotal = cla_value_new;
             }

             $scope.CheckAllItems = function () {
                 main = document.getElementById('mainchk');
                 if (main.checked == true) {
                     for (var i = 0; i < $scope.GetItems.length; i++) {
                         var v = document.getElementById($scope.GetItems[i].im_inv_no + i);
                         v.checked = true;
                         $scope.row1 = 'row_selected';
                         $('tr').addClass("row_selected");
                     }
                 }
                 else {

                     for (var i = 0; i < $scope.GetItems.length; i++) {
                         var v = document.getElementById($scope.GetItems[i].im_inv_no + i);
                         v.checked = false;
                         main.checked = false;
                         $scope.row1 = '';
                         $('tr').removeClass("row_selected");
                     }
                 }
             }

             $scope.checkonebyoneselectItem = function () {

                 $("input[type='checkbox']").change(function (e) {

                     if ($(this).is(":checked")) {
                         $(this).closest('tr').addClass("row_selected");
                         $scope.color = '#edefef';
                     }
                     else {
                         $(this).closest('tr').removeClass("row_selected");
                         $scope.color = '#edefef';
                     }
                 });

                 main = document.getElementById('mainchk');
                 if (main.checked == true) {
                     main.checked = false;
                     $("input[type='checkbox']").change(function (e) {
                         if ($(this).is(":checked")) {
                             $(this).closest('tr').addClass("row_selected");
                         }
                         else {
                             $(this).closest('tr').removeClass("row_selected");
                         }
                     });
                 }
             }

             $scope.CheckAllService = function () {
                 main1 = document.getElementById('mainchk1');
                 for (var i = 0; i < $scope.GetItems.length; i++) {
                     if (main1.checked == true) {
                         $scope.GetItems[i].ischecked = true;
                         $scope.row1 = 'row_selected';
                         $('tr').addClass("row_selected");
                     }
                     else {


                         $scope.GetItems[i].ischecked = false;
                         main1.checked = false;
                         $scope.row1 = '';
                         $('tr').removeClass("row_selected");
                     }
                 }


             }

             $scope.checkonebyoneselectServices = function () {

                 $("input[type='checkbox']").change(function (e) {

                     if ($(this).is(":checked")) {
                         $(this).closest('tr').addClass("row_selected");
                         $scope.color = '#edefef';
                     }
                     else {
                         $(this).closest('tr').removeClass("row_selected");
                         $scope.color = '#edefef';
                     }
                 });

                 main1 = document.getElementById('mainchk1');
                 if (main1.checked == true) {
                     main1.checked = false;
                     $("input[type='checkbox']").change(function (e) {
                         if ($(this).is(":checked")) {
                             $(this).closest('tr').addClass("row_selected");
                         }
                         else {
                             $(this).closest('tr').removeClass("row_selected");
                         }
                     });
                 }
             }

         }])
})();
