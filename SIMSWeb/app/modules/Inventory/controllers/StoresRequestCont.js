﻿(function () {
    'use strict';
    var del = [];
    var main;
    var date1;
    var date3;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StoresRequestCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

           
            $scope.disable_btn = false;
            $scope.save_btn = true;

            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.temp1 = {
                im_inv_no: undefined, invs021_sg_name: undefined, im_item_code: undefined, im_desc: undefined, invs021_dep_code: undefined, sec_code: undefined, invs021_sup_code: undefined, im_assembly_ind_s: undefined, category_code: undefined, subcategory_code: undefined
            };

            $scope.temp2 = {
                im_inv_no: '',
                im_desc: '',
                dept_name: '',
            }

            //$scope.pagesize = "5";
            // $scope.pageindex = "1";
            $scope.itemsPerPage = '5';
            $scope.currentPage = 0;
            $scope.obj = [];
            $scope.itemList = [];
            $scope.temp = {};

            $scope.reqval = '';

            setTimeout(function () {
                $("#rcbItem").select2();

            }, 100);
            setTimeout(function () {
                $("#rcbItem1").select2();

            }, 100);

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd
            }

            if (mm < 10) {
                mm = '0' + mm
            }

            //today = yyyy + '-' + mm + '-' + dd;
            today = dd + '-' + mm + '-' + yyyy;

            $scope.tdate = today;

            $scope.temp['provDate'] = today
            $scope.paymodeList = [
                { name: 'Cash', value: 'CS' },
                 { name: 'Cheques', value: 'CH' },
                  { name: 'Bank Transfer', value: 'BT' },
                    { name: 'Card Payment', value: 'CP' },
            ]

            $scope.temp['paymentMode'] = 'CS';

            $scope.GetItemServiceMethods = function () {
              
                $scope.reqval = $scope.temp.servicevalue;

                if ($scope.temp.servicevalue == 'I') {
                    $scope.show_div_search_item = true;
                    $scope.show_div_search_service = false;
                    if ($scope.ServiceTypes.length > 0)
                        $scope.temp['servicevalue'] = $scope.ServiceTypes[1].servicevalue;
                }
                if ($scope.temp.servicevalue == 'S') {
                    $scope.show_div_search_item = false;
                    $scope.show_div_search_service = true;
                    if ($scope.ServiceTypes.length > 0)
                        $scope.temp['servicevalue'] = $scope.ServiceTypes[0].servicevalue;
                }
            }

            $scope.GetItemServiceMethods1 = function (str) {
           

                if ($scope.reqval == str) {
                    if (str == 'I') {
                        $scope.show_div_search_item = true;
                        $scope.show_div_search_service = false;
                        if ($scope.ServiceTypes.length > 0)
                            $scope.temp['servicevalue'] = $scope.ServiceTypes[1].servicevalue;
                    }
                    if (str == 'S') {
                        $scope.show_div_search_item = false;
                        $scope.show_div_search_service = true;
                        if ($scope.ServiceTypes.length > 0)
                            $scope.temp['servicevalue'] = $scope.ServiceTypes[0].servicevalue;
                    }
                }
                else if ($scope.reqval != str && $scope.itemList.length <= 0) {
                    if (str == 'I') {
                        $scope.show_div_search_item = true;
                        $scope.show_div_search_service = false;
                        if ($scope.ServiceTypes.length > 0)
                            $scope.temp['servicevalue'] = $scope.ServiceTypes[1].servicevalue;
                    }
                    if (str == 'S') {
                        $scope.show_div_search_item = false;
                        $scope.show_div_search_service = true;
                        if ($scope.ServiceTypes.length > 0)
                            $scope.temp['servicevalue'] = $scope.ServiceTypes[0].servicevalue;
                    }
                }
                else {
                    $scope.temp.servicevalue = $scope.reqval;
                    swal({ title: "Alert", text: "Please select valid Request Type.", width: 300, height: 200 });
                    return;
                }
            }

            $http.get(ENV.apiUrl + "api/CreateOrder/GetServiceTypes").then(function (res2) {
              
                $scope.ServiceTypes = res2.data;
                if ($scope.ServiceTypes.length > 0)
                    $scope.temp['servicevalue'] = $scope.ServiceTypes[1].servicevalue;
                $scope.GetItemServiceMethods();
            });

            $http.get(ENV.apiUrl + "api/RequestDetail/getDeparmentsUsercode?user_code=" + $rootScope.globals.currentUser.username).then(function (deptdata) {
                $scope.dept_data = deptdata.data;
                $scope.temp['dept_code'] = $scope.dept_data[0].dept_code;              
                console.log("dept_data",$scope.dept_data);
            });

            $scope.SearchService = function (str) {
                $scope.GetItems = [];
                $('#MyModal2').modal('show');
            }

            $scope.size1 = function (pagesize) {
                $scope.itemsPerPage = pagesize;

            }

            $scope.enroll_change = function (str) {
                if ($scope.temp.sal_type == '05') {

                    $scope.inCustFlg = true;

                    for (var i = 0; i < $scope.rcbInternal.length; i++) {
                        if (str == $scope.rcbInternal[i].up_name) {
                            $scope.internalUser = $scope.rcbInternal[i].up_num;
                            $scope.inCustFlg = false;
                            break;
                        }
                    }

                    if ($scope.inCustFlg) {
                        $http.post(ENV.apiUrl + "api/ItemRequests/Insert_User_profiles?name=" + str).then(function (res) {

                            $scope.internalUser = res.data;
                           
                            //  $scope.rcbCategories = res.data;
                            //  if(res.data.length>0)
                            //   $scope.temp['name'] = res.data.studentname;

                        });
                    }

                }
                else if ($scope.temp.sal_type == '04') {
                    $http.get(ENV.apiUrl + "api/ItemRequests/GetStudentInfo?enrollnum=" + str).then(function (res) {
                       
                        //  $scope.rcbCategories = res.data;
                        if (res.data.studentname != undefined)
                            $scope.temp['name'] = res.data.studentname + ' Class:' + res.data.grade_name + '-' + res.data.section_name;

                    });

                }

                else if ($scope.temp.sal_type == '03') {
                    $http.get(ENV.apiUrl + "api/ItemRequests/GetEmployeeInfo?enrollnum=" + str).then(function (res) {
                     
                        //  $scope.rcbCategories = res.data;
                        if (res.data.studentname != undefined)
                            $scope.temp['name'] = res.data.studentname;

                    });

                }
                if (str == '' || str == undefined) {
                }
                else {
                    $scope.disable_btn = false;

                }
            }

            $http.get(ENV.apiUrl + "api/ItemRequests/GetAllItemsInSubCategoryNew?pc_code=" + '').then(function (res) {

                $scope.rcbItems = res.data;

                $scope.rcbitemOld = angular.copy(res.data);
            });

            $http.get(ENV.apiUrl + "api/ItemRequests/GetServiceCodeDetails").then(function (res) {

                $scope.resitem1 = res.data;
                $scope.rcbitem1Old = angular.copy(res.data);

            });
            /////////////////////////Service Search
            $scope.size = function (str) {
             
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
               $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

               
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };
            /////////////////////////

            //$scope.temp['doc_type'] = $scope.obj2[0].sims_cur_code;

            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;


            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.obj.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;
            };

            $scope.searchstudent = function () {
                $scope.busy = true;
                $scope.searchtable = false;
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.temp)).then(function (Allstudent) {

                    $scope.student = Allstudent.data;

                    $scope.busy = false;

                    $scope.searchtable = true;
                });
            }

            $scope.GetSalesTypeChange = function (str) {
                $scope.searchDisable = false;
                $scope.rcbInternal = [];
                if (str == '03') {   //staff

                    $rootScope.visible_stud = false;
                    $rootScope.visible_parent = false;
                    $rootScope.visible_search_parent = false;
                    $rootScope.visible_teacher = false;
                    $rootScope.visible_User = false;
                    $rootScope.visible_Employee = true;

                    $rootScope.chkMulti = false;


                    $scope.chk_internal = false;
                    for (var i = 0; i < $scope.rcbDocType.length; i++) {

                        if ($scope.rcbDocType[i].dt_code == '02') {
                            $scope.chk_internal = true;
                        }
                    }
                    if (!$scope.chk_internal)
                        $scope.rcbDocType.push($scope.internal_tars);


                }
                else if (str == '04') { //student

                    $rootScope.visible_stud = true;
                    $rootScope.visible_parent = false;
                    $rootScope.visible_search_parent = false;
                    $rootScope.visible_teacher = false;
                    $rootScope.visible_User = false;
                    $rootScope.visible_Employee = false;

                    $rootScope.chkMulti = false;

                    for (var i = 0; i < $scope.rcbDocType.length; i++) {

                        if ($scope.rcbDocType[i].dt_code == '02') {
                            $scope.internal_tars = angular.copy($scope.rcbDocType[i]);
                            $scope.rcbDocType.splice(i, 1);
                        }
                    }

                }
                else if (str == '05') {
                    $scope.searchDisable = true

                    $http.get(ENV.apiUrl + "api/ItemRequests/GetUserProfiles").then(function (res) {

                        $scope.rcbInternal = res.data;
                        // de.log(res.data[0])
                    });

                    for (var i = 0; i < $scope.rcbDocType.length; i++) {

                        if ($scope.rcbDocType[i].dt_code == '02') {
                            $scope.internal_tars = angular.copy($scope.rcbDocType[i]);
                            $scope.rcbDocType.splice(i, 1);
                        }
                    }
                }
            }

            $scope.$on('global_cancel', function (str) {

              
                if ($scope.SelectedUserLst.length > 0) {
                  
                    if ($scope.temp.sal_type == '03') {
                        $scope.temp['enroll'] = $scope.SelectedUserLst[0].em_number;
                        $scope.temp['name'] = $scope.SelectedUserLst[0].empName;

                    }
                    else if ($scope.temp.sal_type == '04') {
                        $scope.temp['enroll'] = $scope.SelectedUserLst[0].s_enroll_no;
                        $scope.temp['name'] = $scope.SelectedUserLst[0].name + ' Class: ' + $scope.SelectedUserLst[0].s_class;

                    }
                }

                if ($scope.temp.enroll == '' || $scope.temp.enroll == undefined) {
                }
                else {
                    $scope.disable_btn = false;

                }
                // $scope.getstudentList();
            });

            $(window).bind('keydown', function (event) {

                if (event.ctrlKey || event.metaKey) {
                    switch (String.fromCharCode(event.which).toLowerCase()) {
                        case 'p':
                            event.preventDefault();
                            $scope.print('tableIDDiv');

                            break;


                    }
                }
            });

            $scope.edit = function (info) {
                $scope.temp['enroll'] = info.s_enroll_no;
                //  $scope.SearchStudentData();
            }

            $http.get(ENV.apiUrl + "api/ItemRequests/get_supplier_code").then(function (res) {
               
                $scope.cmb_supplier = res.data;
            });

            $http.get(ENV.apiUrl + "api/ItemRequests/GetAllSupplierGroupName").then(function (res) {
               
                $scope.rcbGroup = res.data;

            });

            $http.get(ENV.apiUrl + "api/ItemRequests/get_Categories").then(function (res) {
           
                $scope.rcbCategories = res.data;

            });

            $scope.print = function (div) {

                $("#tableIDDiv").css({ display: "block" });


                var docHead = document.head.outerHTML;
                var printContents = document.getElementById(div).outerHTML;
                var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

                var newWin = window.open("", "_blank", winAttr);
                var writeDoc = newWin.document;
                writeDoc.open();
                writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                writeDoc.close();
                newWin.focus();

                // $scope.shds = false;
                $timeout(function () {
                    $("#tableIDDiv").css({ display: "none" });

                }, 200);



            }

            $scope.cancel = function () {
                $scope.temp = {}
                $scope.temp1 = {}
                $scope.GetItems = [];
                $scope.itemList = [];
                $scope.rcbInternal = [];
                $scope.im_item_code1 = '';
                $scope.temp['paymentMode'] = 'CS';
                $scope.temp['provDate'] = today
                $scope.cardpay = false;
                $scope.temp['dept_code'] = $scope.dept_data[0].dept_code;
                $scope.temp['servicevalue'] = $scope.ServiceTypes[1].servicevalue;

                if ($scope.rcbSalesType.length > 0)
                    $scope.temp['sal_type'] = $scope.rcbSalesType[1].sal_type;

                if ($scope.rcbDocType.length > 0)
                    $scope.temp['doc_type'] = $scope.rcbDocType[0].dt_code;
                $scope.ServiceTypes = [];

                $http.get(ENV.apiUrl + "api/CreateOrder/GetServiceTypes").then(function (res2) {
                    $scope.ServiceTypes = res2.data;
                    if ($scope.ServiceTypes.length > 0)
                        $scope.temp['servicevalue'] = $scope.ServiceTypes[1].servicevalue;
                    $scope.GetItemServiceMethods();
                });
            }

            $scope.getCategories = function (str) {

                if ($scope.temp.subcategory_code == undefined)
                    $scope.rcbItems = [];
                if ($scope.temp.subcategory_code == '')
                    $scope.rcbItems = [];
                //$scope.temp.gcode = '';
                $scope.temp.loc_code = '';
                $scope.temp.subcategory_code = '';

                $http.get(ENV.apiUrl + "api/ItemRequests/get_SubCategories?pc_parentcode=" + str).then(function (res) {

                    $scope.rcbSubCategories = res.data;
                    $http.get(ENV.apiUrl + "api/InterDepartmentTransfer_internal/get_SubCategoriesidt?pc_parentcode=" + str).then(function (res) {
                        $scope.rcbSubCategories = res.data;

                        $http.get(ENV.apiUrl + "api/InterDepartmentTransfer_internal/GetAllItemsInCategoryNewidt?pc_code=" + str).then(function (res) {
                            $scope.rcbItems = res.data;

                        });
                    });

                });

            }

            $scope.getSubCategories = function (str) {
                $scope.rcbItems = [];                
                $scope.temp.loc_code = '';
                if ($scope.temp.dept_code == undefined || $scope.temp.dept_code == "" || $scope.temp.dept_code==null){
                    $scope.temp.dept_code='10';
                }
                $http.get(ENV.apiUrl + "api/ItemRequests/GetAllItemsInSubCategoryNew?pc_code=" + str + "&dep_code="+ $scope.temp.dept_code).then(function (res) {
                    $scope.rcbItems = res.data;
                    console.log("rcbItems", $scope.rcbItems);
                });
            }

            $scope.getsupgrpitems = function (str) {
                $scope.rcbItems = [];
                $scope.temp.loc_code = '';
                $scope.temp.category_code = '';
                $scope.temp.subcategory_code = '';
                $http.get(ENV.apiUrl + "api/InterDepartmentTransfer_internal/GetAllItemsInSupgroupidt?sg_name=" + str).then(function (res) {
                    $scope.rcbItems = res.data;
                });

            }

            $scope.qtychange = function (str) {
                $scope.temp.finalValue = ($scope.temp.qty * parseFloat($scope.temp.im_sell_price));
                //$scope.totalAmtClick()
                $scope.totalAmtClick();
            }

            $scope.qtychangeby = function (str) {
                str.dd_sell_value_final = (str.dd_qty * parseFloat(str.dd_sell_price));
                // $scope.totalAmtClick()
                $scope.totalAmtClick();
            }

            $scope.getitemCode = function (str) {
                
                $http.get(ENV.apiUrl + "api/ItemRequests/GetItemCodeDetails?item_code=" + str.im_item_code + '&sg_name=' + $scope.temp.gcode).then(function (res) {

                    $scope.resitem = res.data[0];

                    if ($scope.resitem.original_qty > 0) {

                        if ($scope.temp.doc_type == '02') {
                            $scope.temp.qty = 1;
                            $scope.temp.im_sell_price = $scope.resitem.im_sell_price;
                            $scope.temp.finalValue = 0;
                            $scope.temp.gcode = $scope.resitem.sg_name;
                        }
                        else {
                            $scope.temp.qty = 1;
                            $scope.temp.im_sell_price = $scope.resitem.im_sell_price;
                            $scope.temp.finalValue = (1 * parseFloat($scope.resitem.im_sell_price));
                            $scope.temp.gcode = $scope.resitem.sg_name;
                        }

                    }
                    else {
                        swal({  text: "Item quantity is 0.Please Update Selected Item Quantity.", imageUrl: "assets/img/notification-alert.png", });

                    }


                    $http.get(ENV.apiUrl + "api/ItemRequests/GetItemCodeLocations?im_inv_no=" + str.im_inv_no).then(function (res) {

                        $scope.resloc = res.data;
                        if ($scope.resloc.length > 0)
                            $scope.temp.loc_code = $scope.resloc[0].loc_code;

                        $scope.temp.gcode = str.sg_name
                        $scope.temp.loc_code
                    });

                });

            }

            $scope.getServiceCode = function (str) {
               
                $http.get(ENV.apiUrl + "api/ItemRequests/GetServiceDetails?item_code=" + str.im_inv_no).then(function (res) {

                    $scope.serdata = res.data;

                });
            }

            $scope.addservice = function () {

                if ($scope.im_service_code1.im_inv_no == '' || $scope.im_service_code1.im_inv_no == undefined) {
                }

                else {

                    var data =
                        {
                            //   sg_name: $("#rcbGroup").find("option:selected").text(),

                            sg_name: $scope.im_service_code1.sg_name,
                            sg_code: '',
                            im_inv_no: $scope.im_service_code1.im_inv_no,
                            im_item_code: $scope.im_service_code1.im_item_code,
                            im_desc: $("#rcbItem1").find("option:selected").text(),
                            // item_location_name: $("#rcbLocation").find("option:selected").text(),
                            item_location_name: '',
                            loc_code: '',
                            original_qty: '',
                            dd_qty: '',
                            dd_sell_price: '',
                            dd_sell_value_final: 0,
                            doc_remark: $scope.temp.ser_doc_remark,
                            doc_require_date: $scope.tdate,
                        }
                    $scope.itemList.push(data)
                }
                $scope.CancelService();
                document.getElementById("rcbItem1").focus();
            }

            $scope.add = function () {


                if ($scope.im_item_code1.im_inv_no == '' || $scope.im_item_code1.im_inv_no == undefined) {
                }

                else {
                    $scope.totalFinal = 0;
                    $scope.flg = false;
                    //for (var i = 0; i < $scope.itemList.length; i++) {
                    //    if ($scope.itemList[i].im_inv_no == $scope.im_item_code1.im_inv_no) {

                    //        $scope.itemList[i].dd_qty += 1;
                    //        $scope.itemList[i].dd_sell_value_final += $scope.temp.im_sell_price
                    //        // $scope.itemList[i].im_inv_no
                    //        $scope.flg = true;
                    //        i = $scope.itemList.length;
                    //    }
                    //}

                    //= false;
                    if ($scope.flg == false) {

                        if ($scope.temp.doc_type == '02') {
                            var data =
                                {
                                    sg_name: $scope.temp.gcode,
                                    sg_code: $scope.temp.gcode,
                                    im_inv_no: $scope.im_item_code1.im_inv_no,
                                    im_item_code: $scope.im_item_code1.im_item_code,
                                    im_desc: $("#rcbItem").find("option:selected").text(),
                                    // item_location_name: $("#rcbLocation").find("option:selected").text(),
                                    item_location_name: $scope.temp.loc_code,
                                    loc_code: $scope.temp.loc_code,
                                    original_qty: $scope.resitem.original_qty,
                                    dd_qty: $scope.temp.qty,
                                    dd_sell_price: $scope.temp.im_sell_price,
                                    dd_sell_value_final: 0,
                                    doc_remark: $scope.temp.doc_remark,
                                    doc_require_date: $scope.tdate,
                                    im_UOM: $scope.im_item_code1.uom_name
                                }
                            $scope.itemList.push(data)
                        }
                        else {

                            var data =
                               {
                                   sg_name: $scope.temp.gcode,
                                   sg_code: $scope.temp.gcode,
                                   im_inv_no: $scope.im_item_code1.im_inv_no,
                                   im_item_code: $scope.im_item_code1.im_item_code,
                                   im_desc: $("#rcbItem").find("option:selected").text(),
                                   // item_location_name: $("#rcbLocation").find("option:selected").text(),
                                   item_location_name: $scope.temp.loc_code,
                                   loc_code: $scope.temp.loc_code,
                                   original_qty: $scope.resitem.original_qty,
                                   dd_qty: $scope.temp.qty,
                                   dd_sell_price: $scope.temp.im_sell_price,
                                   dd_sell_value_final: $scope.temp.finalValue,
                                   doc_remark: $scope.temp.doc_remark,
                                   doc_require_date: $scope.tdate,
                                   im_UOM: $scope.im_item_code1.uom_name
                               }
                            $scope.itemList.push(data)

                        }

                    }
                }
                $scope.totalAmtClick();


               // $scope.CancelItem();

                document.getElementById("rcbItem").focus();

            }

            $scope.finalize = function () {

                $scope.disable_btn = false;

                //if (!string.IsNullOrEmpty(salesman_cd))
                //    invsObj.sm_code = salesman_cd;
                //else
                //    invsObj.sm_code = txt_sm_code.Text;
             

                //if ($scope.temp.sal_type == '05') {
                //    $scope.temp['enroll'] = $scope.internalUser;
                //}

                var data = {
                    //dep_code: '10',
                    dep_code:$scope.temp['dept_code'],
                    doc_prov_no: '',
                    doc_prov_date: $scope.temp.provDate,
                    dt_code: $scope.temp.doc_type,
                    //sm_code: $scope.salesman.sm_code,//101
                    up_name: 'admin',
                    creation_user: $rootScope.globals.currentUser.username,
                    sal_type: $scope.temp.sal_type,
                    doc_total_amount: $scope.temp.totalFinal,
                    doc_status: '0',
                    cus_account_no: $scope.temp.enroll,
                    doc_special_name: $scope.temp.special_nm,
                    //dep_code_caused_by: '10',
                    dep_code_caused_by: $scope.temp['dept_code'],
                    doc_order_ref_no: '',
                    doc_other_charge_amount: $scope.temp.otherCharges,
                    doc_discount_pct: $scope.temp.overalldis,
                    doc_discount_amount: $scope.temp.disamt,
                    creation_date: today,
                    doc_date: $scope.temp.provDate,
                    doc_narration: $scope.temp.doc_narration,
                    doc_delivery_remarks: $scope.temp.doc_delivery_remarks,
                    servicevalue: $scope.temp.servicevalue,
                }

                $http.post(ENV.apiUrl + "api/ItemRequests/Insert_Sale_Documents", data).then(function (msg) {
                    var d = msg.data;

                    $scope.salesDetails = [];
                    for (var i = 0; i < $scope.itemList.length; i++) {

                        var paymode = $scope.temp.paymentMode;
                        //if (paymode=='CH')

                        var data = {
                            //dep_code: '10',
                            dep_code: $scope.temp['dept_code'],
                            doc_prov_no: msg.data,
                            dd_line_no: i + 1,
                            dd_status: '0',
                            dd_payment_mode: paymode,
                            im_inv_no: $scope.itemList[i].im_inv_no,
                            loc_code: $scope.itemList[i].loc_code,
                            dd_sell_price: $scope.itemList[i].dd_sell_price,
                            dd_sell_price_discounted: 0,
                            dd_sell_price_final: 0,
                            dd_sell_value_final: $scope.itemList[i].dd_sell_value_final,
                            dd_sell_cost_total: 0,
                            dd_qty: $scope.itemList[i].dd_qty,
                            dd_outstanding_qty: 0,
                            dd_line_discount_pct: 0,
                            dd_line_discount_amount: 0,
                            dd_line_total: 0,
                            dd_physical_qty: 0,
                            dd_cheque_number: $scope.temp.chequeNo,
                            dd_cheque_date: $scope.temp.chequeDate,
                            dd_cheque_bank_code: $scope.temp.bankCode,
                            doc_remark: $scope.itemList[i].doc_remark,
                            doc_require_date: $scope.itemList[i].doc_require_date,
                        }
                        $scope.salesDetails.push(data);
                    }
                    $http.post(ENV.apiUrl + "api/ItemRequests/Insert_Sale_Documents_Details1", $scope.salesDetails).then(function (msg) {
                        if (msg.data==true) {
                            swal({ title: "Alert", text: "Request No " + d + " Inserted Successfully." });
                            $scope.cancel();
                            //swal({
                            //    title: '',
                            //    text: "Do you want print receipt",
                            //    showCloseButton: true,
                            //    showCancelButton: true,
                            //    confirmButtonText: 'Yes',
                            //    width: 380,
                            //    cancelButtonText: 'No',
                            //    allowOutsideClick:false,
                            //}).then(function (isConfirm) {
                            //    if (isConfirm) {
                            //        //  $scope.OkRejectadm();
                            //        if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                            //            //var data = {
                            //            //    location: 'Invs.INVR02DPSMIS',
                            //            //    parameter: { doc_prov_no: d },
                            //            //    state: 'main.Inv035'
                            //            //}

                            //            var data = {
                            //                doc_prov_no: d,
                            //                sdate: '',
                            //                edate:'',
                            //                state: false,
                            //                doc_status: '',
                            //                back_state: 'main.Inv035'
                            //            }

                            //        }

                            //      else  if ($http.defaults.headers.common['schoolId'] == 'brs') {
                            //            var data = {
                            //                location: 'Invs.INVR02BRS',
                            //                parameter: { doc_prov_no: d },
                            //                state: 'main.Inv035'
                            //            }
                            //        }
                            //        else {
                            //            var data = {
                            //                location: 'Invs.INVR02',
                            //                parameter: { doc_prov_no: d },
                            //                state: 'main.Inv035'
                            //            }
                            //        }
                            //        //$scope.disable_btn = false;
                            //        if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {

                            //            window.localStorage["ReportProvNumber"] = JSON.stringify(data)
                            //            $state.go('main.rpts')
                            //        }
                            //        else {

                            //            window.localStorage["ReportDetails"] = JSON.stringify(data)
                            //            $state.go('main.ReportCardParameter')
                            //        }
                            //    }
                            //});                          

                        }
                        else if (msg.data == false) {
                            swal({  text: "Record Not Inserted ." , imageUrl: "assets/img/notification-alert.png" });
                            $scope.disable_btn = false;

                        }
                        else {
                            swal("Error-" + msg.data)
                        }


                    });



                });
            }

            $scope.ServiceFetch = function () {

               
                $http.get(ENV.apiUrl + "api/ItemRequests/getServiceSearch?im_no=" + $scope.temp2.im_inv_no + "&des_ser=" + $scope.temp2.im_desc + "&dep_name=" + $scope.temp2.dept_name).then(function (res1) {
                    $scope.GetItems = res1.data;
                    $scope.totalItems = $scope.GetItems.length;
                    $scope.todos = $scope.GetItems;
                    $scope.makeTodos();
                    $scope.hide_suc = true;
                });

            }

            $scope.ServiceReset = function () {
                $scope.temp2 = {
                    im_inv_no: '',
                    im_desc: '',
                    dept_name: '',
                }
                $scope.GetItems = [];
            }

            $scope.okbuttonclick1 = function () {
                
                for (var i = 0; i < $scope.GetItems.length; i++) {
                    var t = $scope.GetItems[i].im_inv_no;
                    var v = document.getElementById(t + i);
                    if ($scope.GetItems[i].ischecked == true) {
                        //$scope.inv_no = $scope.GetItems[i].im_inv_no
                        //$scope.inv_desc = $scope.GetItems[i].im_desc
                        //$scope.sup_code = $scope.GetItems[i].sup_code
                        //invno = $scope.inv_no;

                        var data =
                        {
                            sg_name: $scope.GetItems[i].sg_name,
                            sg_code: '',
                            im_inv_no: $scope.GetItems[i].im_inv_no,
                            im_item_code: $scope.GetItems[i].im_item_code,
                            im_desc: $scope.GetItems[i].im_desc,
                            item_location_name: '',
                            loc_code: '',
                            original_qty: '',
                            dd_qty: '',
                            dd_sell_price: '',
                            dd_sell_value_final: '',
                            doc_remark: '',
                            doc_require_date: $scope.tdate,
                        }
                        $scope.itemList.push(data)
                    }
                }
            }

            $scope.itemSearch = function () {
                $scope.GetItems = []
                $scope.temp1 = {}
                $('#itemSearch').modal('show');
                $scope.itemsetflg = false;
            }

            $scope.ItemSetSearch = function () {
                $scope.GetItems = []
                $scope.temp1 = {}

                $('#itemSearch').modal('show');
                $scope.itemsetflg = true;
                $scope.temp1['im_assembly_ind'] = true;
                $http.post(ENV.apiUrl + "api/ItemRequests/postgetItemSerch", $scope.temp1).then(function (res) {
                    if (res.data.length > 0)
                        $scope.GetItems = res.data;
                    else
                        swal({  text: "No Record Found.", imageUrl: "assets/img/notification-alert.png", });


                });

            }

            $scope.getfetch = function () {

                //var data = $scope.temp1;
                if ($scope.itemsetflg) {
                    $scope.temp1['im_assembly_ind'] = true;
                }
                else {
                    $scope.temp1['im_assembly_ind'] = false;


                }
                //$scope.temp1['im_assembly_ind'] = true;
                //$scope.temp1['im_inv_no'] = '';

                
                $http.post(ENV.apiUrl + "api/ItemRequests/postgetItemSerch", $scope.temp1).then(function (res) {
                    if (res.data.length > 0)
                        $scope.GetItems = res.data;
                    else
                        swal({  text: "No Record Found.", imageUrl: "assets/img/notification-alert.png", });


                });
            }

            $scope.okbuttonclick = function () {



                //old Code
                //for (var i = 0; i < $scope.GetItems.length; i++) {
                //    var t = $scope.GetItems[i].im_inv_no + i;
                //    var v = document.getElementById(t);

                //    if (v.checked == true) {
                //        var data =
                //   {
                //       sg_name: $scope.GetItems[i].sg_name,
                //     //  sg_code: $scope.GetItems[i].gcode,
                //       im_item_code: $scope.GetItems[i].im_item_code,
                //       im_desc: $scope.GetItems[i].im_desc,
                //       item_location_name: $scope.GetItems[i].loc_code,
                //       loc_code: $scope.GetItems[i].loc_code,
                //       original_qty: $scope.GetItems[i].id_cur_qty,
                //       dd_qty:0,
                //       dd_sell_price: $scope.GetItems[i].im_sell_price,
                //       dd_sell_value_final: 0,

                //   }

                //        $scope.itemList.push(data)

                //    }
                //}
            }

            $scope.calculateTotal = function () {
                $scope.totalFinal = 0;
                for (var i = 0; i < $scope.itemList.length; i++) {

                    $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                }
                $scope.temp['totalFinal'] = $scope.totalFinal;

            }

            $scope.chk_click_item = function (obj, flg) {
                debugger
                if ($scope.itemsetflg) {

                    //    obj.im_inv_no

                    $http.post(ENV.apiUrl + "api/ItemRequests/Fetch_ItemDetails_ItemSet?im_inv_no=" + obj.im_inv_no).then(function (res) {

                       
                        // $scope.rcbSubCategories = res.data;

                        for (var i = 0; i < res.data.length; i++) {
                            if ($scope.temp.doc_type == '02') {
                                $scope.internal_im_sell_price = 0;
                            }
                            else {
                                $scope.internal_im_sell_price = parseFloat(res.data[i].im_sell_price);
                            }

                            var data =
                        {
                            sg_name: res.data[i].sg_name,
                            sg_code: res.data[i].sg_name,
                            im_inv_no: res.data[i].im_inv_no,
                            im_item_code: res.data[i].im_item_code,
                            im_desc: res.data[i].im_desc,
                            item_location_name: res.data[i].loc_code,
                            loc_code: res.data[i].loc_code,
                            original_qty: res.data[i].id_cur_qty,
                            dd_qty: res.data[i].ia_component_qty,
                            dd_sell_price: $scope.internal_im_sell_price,
                            dd_sell_value_final: $scope.internal_im_sell_price,
                            doc_require_date: $scope.tdate
                        }
                            $scope.itemList.push(data)

                        }
                        $scope.totalAmtClick();



                    });

                    $('#itemSearch').modal('hide');
                }
                else {
                    $scope.Exflg = false;

                    if ($scope.temp.doc_type == '02') {
                        $scope.internal_im_sell_price = parseFloat(obj.im_sell_price);
                        $scope.internal_im_sell_price_final = 0;

                    }
                    else {
                        $scope.internal_im_sell_price = parseFloat(obj.im_sell_price);
                        $scope.internal_im_sell_price_final = parseFloat(obj.im_sell_price);

                    }
                    var data =
                    {
                        sg_name: obj.sg_name,
                        //  sg_code: $scope.GetItems[i].gcode,
                        im_inv_no: obj.im_inv_no,

                        im_item_code: obj.im_item_code,
                        im_desc: obj.im_desc,
                        item_location_name: obj.loc_code,
                        loc_code: obj.loc_code,
                        original_qty: obj.id_cur_qty,
                        im_UOM:obj.uom_name,
                        dd_qty: 1,
                        dd_sell_price: $scope.internal_im_sell_price,
                        dd_sell_value_final: $scope.internal_im_sell_price_final,
                        doc_require_date: $scope.tdate
                    }

                    if (flg) {

                        //for (var i = 0; i < $scope.itemList.length; i++) {
                        //    if ($scope.itemList[i].im_inv_no == obj.im_inv_no) {

                        //        $scope.itemList[i].dd_qty += 1;
                        //        $scope.itemList[i].dd_sell_value_final += parseFloat(obj.im_sell_price)
                        //        // $scope.itemList[i].im_inv_no
                        //        $scope.Exflg = true;
                        //        i = $scope.itemList.length;
                        //    }
                        //}

                        if ($scope.Exflg == false)
                            $scope.itemList.push(data)

                        $scope.totalAmtClick();


                    }
                    else {
                        //   $scope.itemList.pop(data)

                        for (var i = 0; i < $scope.itemList.length; i++) {
                            if ($scope.itemList[i].im_inv_no == obj.im_inv_no) {
                                if ($scope.itemList[i].dd_qty > 1) {
                                    $scope.itemList[i].dd_qty -= 1;
                                    $scope.itemList[i].dd_sell_value_final -= parseFloat(obj.im_sell_price)

                                }
                                else
                                    $scope.itemList.splice(i, 1);

                            }
                        }
                        $scope.totalAmtClick();

                    }
                }

            }

            $scope.CancelItem = function () {
                $scope.temp['finalValue'] = ''
                $scope.temp['im_sell_price'] = ''
                $scope.temp['qty'] = ''
                $scope.temp['loc_code'] = ''
                $scope.temp['subcategory_code'] = ''
                $scope.temp['category_code'] = ''
                $scope.temp['gcode'] = ''
                $scope.temp['doc_remark'] = ''
                $scope.rcbItems = [];
                //  $scope.rcbCategories = [];
                $scope.rcbSubCategories = [];
                $scope.im_item_code1 = '';
                $("#rcbItem").select2("val", "");

                $scope.rcbItems = $scope.rcbitemOld;

                //$http.get(ENV.apiUrl + "api/ItemRequests/GetAllItemsInSubCategoryNew?pc_code=" + '').then(function (res) {

                //    $scope.rcbItems = res.data;

                //});
            }

            $scope.CancelService = function () {
                $scope.temp['finalValue'] = ''
                $scope.temp['im_sell_price'] = ''
                $scope.temp['ser_qty'] = ''
                $scope.temp['loc_code'] = ''
                $scope.temp['subcategory_code'] = ''
                $scope.temp['category_code'] = ''
                $scope.temp['gcode'] = ''
                $scope.temp['ser_doc_remark'] = ''
                $scope.rcbItems1 = [];
                //  $scope.rcbCategories = [];
                $scope.rcbSubCategories = [];
                $scope.im_service_code1 = '';
                $("#rcbItem1").select2("val", "");

                $scope.rcbItems1 = $scope.rcbitem1Old;

                //$http.get(ENV.apiUrl + "api/ItemRequests/GetAllItemsInSubCategoryNew?pc_code=" + '').then(function (res) {

                //    $scope.rcbItems = res.data;

                //});
            }

            $scope.studentSearch = function () {
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                //$('#stdSearch').modal('show');

            }

            $scope.totalAmtClick = function () {
                $scope.totalFinal = 0;
                for (var i = 0; i < $scope.itemList.length; i++) {

                    $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                }
                $scope.temp['totalFinal'] = $scope.totalFinal;
                if ($scope.temp.otherCharges == undefined || $scope.temp.otherCharges == '') {
                }
                else {
                    $scope.temp.totalFinal += parseFloat($scope.temp.otherCharges);
                }

                if ($scope.temp.disamt == undefined || $scope.temp.disamt == '' || isNaN(parseFloat($scope.temp.disamt))) {
                }
                else {
                    $scope.temp.totalFinal -= parseFloat($scope.temp.disamt);
                }

                //   $scope.temp['totalFinal'] = $scope.totalFinal + (parseFloat($scope.temp.otherCharges) - parseFloat($scope.temp.disamt));



            }

            $scope.otherChargesChange = function () {
                $scope.totalFinal = 0;
                for (var i = 0; i < $scope.itemList.length; i++) {

                    $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                }
                $scope.temp['totalFinal'] = $scope.totalFinal;
                if ($scope.temp.otherCharges == undefined || $scope.temp.otherCharges == '') {
                }
                else {
                    $scope.temp.totalFinal += parseFloat($scope.temp.otherCharges);
                }

                if ($scope.temp.disamt == undefined || $scope.temp.disamt == '') {
                }
                else {
                    $scope.temp.totalFinal -= parseFloat($scope.temp.disamt);
                }

            }

            $scope.txt_discounted_price_TextChanged = function (str) {
                if (str <= 100 || str <= '100') {

                    $scope.totalFinal = 0;
                    for (var i = 0; i < $scope.itemList.length; i++) {

                        $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                    }
                    if (str == undefined || str == '') {
                        $scope.temp['disamt'] = 0;
                    }
                    else {
                        $scope.temp['disamt'] = (parseFloat($scope.totalFinal)) * (parseFloat(str) / 100);
                    }
                    $scope.df = ($scope.totalFinal) - $scope.temp['disamt'];

                    //$scope.temp['totalFinal'] = $scope.df;


                    $scope.temp['totalFinal'] = $scope.totalFinal;
                    if ($scope.temp.otherCharges == undefined || $scope.temp.otherCharges == '') {
                    }
                    else {
                        $scope.temp.totalFinal += parseFloat($scope.temp.otherCharges);
                    }

                    if ($scope.temp.disamt == undefined || $scope.temp.disamt == '') {
                    }
                    else {
                        $scope.temp.totalFinal -= parseFloat($scope.temp.disamt);
                    }

                }
                else {
                    $scope.totalFinal = 0;
                    for (var i = 0; i < $scope.itemList.length; i++) {

                        $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                    }

                    $scope.temp['totalFinal'] = $scope.totalFinal;
                    //  str = 0;
                    $scope.temp['overalldis'] = 0;
                    $scope.temp['disamt'] = 0;
                    if ($scope.temp.otherCharges == undefined || $scope.temp.otherCharges == '') {
                    }
                    else {
                        $scope.temp.totalFinal += parseFloat($scope.temp.otherCharges);
                    }

                    if ($scope.temp.disamt == undefined || $scope.temp.disamt == '') {
                    }
                    else {
                        $scope.temp.totalFinal -= parseFloat($scope.temp.disamt);
                    }

                    swal({  text: "discount Cant greater than 100%.", imageUrl: "assets/img/notification-alert.png", });

                }



            }

            $scope.paymentChange = function (str) {
                $scope.cardpay = false;
                if (str == 'CS') {
                    $scope.chequeShow = false;
                    $scope.bankShow = false;
                }
                else if (str == 'CH') {
                    $scope.chequeShow = true;
                    $scope.bankShow = false;
                }
                else if (str == 'BT') {
                    $scope.chequeShow = false;
                    $scope.bankShow = true;
                }

                else if (str == 'CP') {
                    $scope.chequeShow = false;
                    $scope.bankShow = false;
                    $scope.cardpay = true;

                }

            }

            $scope.removeItem = function (obj, index) {

                $scope.itemList.splice(index, 1);
                $scope.totalAmtClick();
            }

            $scope.onlyNumbers = function (event) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
        }])
})();