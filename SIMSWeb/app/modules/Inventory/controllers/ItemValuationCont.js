﻿(function () {
    'use strict';
    var obj1, temp, opr, CurrentDate, comp;
    var main, deletefin = [], data = [], data1 = [], ItemDetails = [];
    var finanacecode = [];
    var check;
    var reqno;

    var simsController = angular.module('sims.module.Inventory');
    simsController.controller('ItemValuationCont',
         ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {
             $scope.priview_table = false;
             $scope.pagesize = "5";
             $scope.pageindex = 0;
             $scope.search_Req_Data = [];
             $scope.Main_table = false;
             $scope.temp = {};
             $scope.temp2 = [];
             var subopr = null, ret, rem, res, red, res, ree;
             console.clear();

             $timeout(function () {
                 $("#fixTable").tableHeadFixer({ 'top': 1 });
             }, 100);

             $scope.getallvalues = function () {

                 $http.get(ENV.apiUrl + "api/RequestDetail/getDeparments").then(function (deptdata) {
                     debugger;
                     $scope.dept_data = deptdata.data;
                     $scope.temp['dept_code'] = $scope.dept_data[0].dept_code;
                     red = $scope.dept_data[0].dept_code;
                 });

                 $http.get(ENV.apiUrl + "api/StockAdjustment/getCategories").then(function (Allcategorycode) {
                     $scope.categorycode = Allcategorycode.data;
                 });
             };

             $scope.getallvalues();

             $http.get(ENV.apiUrl + "api/RequestDetail/getitemsdetails?data=" + JSON.stringify($scope.temp)).then(function (res1) {

                 $scope.items_Data = res1.data;

                 setTimeout(function () {
                     $('#cmb_seritm_code').change(function () {
                         console.log($(this).val());
                     }).multipleSelect({
                         width: '100%'
                     });
                 }, 1000);

             });


             $scope.getCategories = function () {

                 $http.get(ENV.apiUrl + "api/StockAdjustment/getSubCategories?pc_pcode=" + $scope.temp.category_code).then(function (SubCategories) {
                     $scope.allSubCategories = SubCategories.data;

                 });
                 debugger;
                 $http.get(ENV.apiUrl + "api/RequestDetail/getitemsdetails?data=" + JSON.stringify($scope.temp)).then(function (res1) {
                     $scope.items_Data = res1.data;
                     setTimeout(function () {
                         $('#cmb_seritm_code').change(function () {
                             console.log($(this).val());
                         }).multipleSelect({
                             width: '100%'
                         });
                     }, 1000);
                 });
             }

             //setTimeout(function () {
             //    $("#cmb_seritm_code").select2();
             //}, 100);

             $(function () {
                 $('#cmb_seritm_code').multipleSelect({
                     width: '100%'
                 });
             });

             $scope.SearchRequest = function () {
                 debugger
                 //arr + "",
                 // $scope.temp1.im_item_code +""
                 $http.get(ENV.apiUrl + "api/RequestDetail/getitemsvaluationdetails?&data=" + JSON.stringify($scope.temp) + "&data1=" +$scope.temp1.im_item_code +"").then(function (searchReqData) {

                     $scope.search_Req_Data = searchReqData.data.table;
                     $scope.Main_table = true;
                     $scope.Grid = true;
                 });
             }

             $scope.Clear = function () {
                 $scope.im_item_codel = "";
                 $scope.temp.uom_code = "";
                 $scope.temp.reqQty = "";
                 $scope.temp.im_remark = "";
                 $scope.temp.req_remarks = "";
                 $scope.hide_suc = false;
                 $scope.delivery_mode();

                 //$scope.dt.req_date = "";
                 //$scope.temp.dept_code = "";
                 //$scope.temp.dm_code = "";
                 //$scope.temp.salesman_code = "";

             }

             $scope.Cancel = function () {
                 $scope.Clear();
                 $scope.im_inv_no = [];
                 $scope.table = true;
                 $scope.Main_table = false;
                 $scope.display = false;
                 $scope.savedisabled = false;
                 $scope.updisabled = true;
                 $scope.temp.req_no = "";
                 $scope.temp.salesman_code = "";
                 $scope.temp.req_remarks = "";
                 $scope.Myform.$setPristine();
                 $scope.Myform.$setUntouched();
                 $scope.hide_suc = false;
                 $state.go('main.Inv058');

             }

             $scope.PrintServiceItemRe = function (str) {
                 debugger
                 var f = str.req_no;
                 $scope.t = 'Invs.INVR10DPSD';
                 var data = {
                     location: $scope.t,
                     parameter: {
                         subcat: str.subCategory,
                         item_code: str.im_item_code,
                     },
                     state: 'main.InvsIV'
                 }
                 window.localStorage["ReportDetails"] = JSON.stringify(data)
                 $state.go('main.ReportCardParameter')
             }

             $scope.Reset = function () {
                 $scope.temp = {
                     dept_code: '',
                     category_code: '',
                     subcategory_code: '',
                     im_item_code: ''
                 }
                 $scope.getallvalues();
                 $scope.Main_table = false;
                 $scope.Grid = false;
                 $scope.dis_forgc = false;
             }

             $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                 $('input[type="text"]', $(this).parent()).focus();
             });

             $('*[data-datepicker="true"] input[type="text"]').datepicker({
                 todayBtn: true,
                 orientation: "top left",
                 autoclose: true,
                 todayHighlight: true,
                 format: 'yyyy-mm-dd'
             });

         }])

})();
