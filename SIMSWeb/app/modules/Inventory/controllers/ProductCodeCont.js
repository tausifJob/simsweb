﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ProductCodeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.pagesize1 = '10';
            $scope.pageindex1 = "0";
            $scope.pager1 = true;

            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;

            $scope.Accounts = [];
            $scope.finyr_data = '';

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
            $scope.temp = {};
            $scope.edt = {};
            $scope.ifedit = false;

            //Select Data SHOW

            setTimeout(function () {
                $("#cmb_accno").select2();
            }, 100);
            setTimeout(function () {
                $("#cmb_stockaccno").select2();
            }, 100);
            setTimeout(function () {
                $("#cmb_costaccno").select2();
            }, 100);
            setTimeout(function () {
                $("#cmb_disaccno").select2();
            }, 100);

            setTimeout(function () {
                $("#cmb_accno_sub").select2();
            }, 100);
            setTimeout(function () {
                $("#cmb_stockaccno_sub").select2();
            }, 100);
            setTimeout(function () {
                $("#cmb_costaccno_sub").select2();
            }, 100);
            setTimeout(function () {
                $("#cmb_disaccno_sub").select2();
            }, 100);

            $http.get(ENV.apiUrl + "api/ProductCode/getProductCode").then(function (res1) {
                $scope.getProduct = res1.data;
                $scope.totalItems = $scope.getProduct.length;
                $scope.todos = $scope.getProduct;
                $scope.makeTodos();
            });

            $http.get(ENV.apiUrl + "api/InvsParameter/GetCompanyName").then(function (docstatus1) {
                $scope.Compname = docstatus1.data;
                console.log($scope.Compname);
            });

            $http.get(ENV.apiUrl + "api/InvsParameter/GetApplicationCode").then(function (docstatus1) {
                $scope.app_code = docstatus1.data;
                console.log($scope.app_code);
            });

            $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                debugger;
                $scope.comp_data = res.data;
                console.log($scope.comp_data);
                if ($scope.comp_data.length > 0) {
                    $scope.temp.compcode = $scope.comp_data[0].dept_comp_code;
                    $scope.edt.compcode_sub = $scope.comp_data[0].dept_comp_code;
                }
            });

            $http.get(ENV.apiUrl + "api/ProductCode/GetFinancialyears").then(function (res) {

                $scope.fin_data = res.data;

                if ($scope.fin_data.length > 0) {
                    $scope.temp.finyear = $scope.fin_data[0].fin_year;
                    $scope.edt.finyear_sub = $scope.fin_data[0].fin_year;
                }

                $scope.getglaccs();
            });

            $http.get(ENV.apiUrl + "api/ProductCode/GetDepartments").then(function (res) {
                $scope.dep_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/ProductCode/GetCategoryGroup").then(function (res1) {
                $scope.getCategoryGroups = res1.data;               
            });

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.getProduct, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.getProduct;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.search1 = function () {

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.pc_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.pc_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
                $scope.disabled = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.divcode_readonly = false;
                //$scope.temp = "";
                $scope.temp = {};

            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.table = true;
                $scope.display = false;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp.pc_desc = '';
                $scope.temp.depcode = '';
                $scope.temp.compcode = '';
                $scope.temp.finyear = '';
                $("#cmb_accno").select2("val", "");
                $("#cmb_stockaccno").select2("val", "");
                $("#cmb_costaccno").select2("val", "");
                $("#cmb_disaccno").select2("val", "");
                $scope.temp.pc_status = '';
                $scope.temp.pc_category_group = "";

                $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                    debugger;
                    $scope.comp_data = res.data;
                    console.log($scope.comp_data);
                    if ($scope.comp_data.length > 0) {
                        $scope.temp.compcode = $scope.comp_data[0].dept_comp_code;
                        $scope.edt.compcode_sub = $scope.comp_data[0].dept_comp_code;
                    }
                });

                $http.get(ENV.apiUrl + "api/ProductCode/GetFinancialyears").then(function (res) {

                    $scope.fin_data = res.data;

                    if ($scope.fin_data.length > 0) {
                        $scope.temp.finyear = $scope.fin_data[1].fin_year;
                        $scope.edt.finyear_sub = $scope.fin_data[1].fin_year;
                    }

                    $scope.getglaccs();
                });

                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = true;
                $scope.display = false;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.divcode_readonly = false;
                $scope.ifedit = true;
                $scope.temp = {
                    pc_code: str.pc_code,
                    pc_desc: str.pc_desc,
                    compcode: str.compcode,
                    finyear: str.finyear,
                    pc_category_group: str.pc_category_group
                };

                $scope.getglaccs(str);

            }


            //MAin table insert
           
            $scope.save = function () {
                debugger;              
                //if (Myform) {
                var data1 = [];
                $scope.temp.pc_parent_code = '0';
                var data = $scope.temp;
                data.opr = 'I';

                debugger;
                data1.push(data);

                $http.post(ENV.apiUrl + "api/ProductCode/CUDProductCode", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        $scope.temp.pc_desc = '';
                        $scope.temp.depcode = '';
                        $scope.temp.compcode = '';
                        $scope.temp.finyear = '';
                        $scope.temp.pc_category_group = '';
                        $("#cmb_accno").select2("val", "");
                        $("#cmb_stockaccno").select2("val", "");
                        $("#cmb_costaccno").select2("val", "");
                        $("#cmb_disaccno").select2("val", "");

                        $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                            debugger;
                            $scope.comp_data = res.data;
                            console.log($scope.comp_data);
                            if ($scope.comp_data.length > 0) {
                                $scope.temp.compcode = $scope.comp_data[0].dept_comp_code;
                                $scope.edt.compcode_sub = $scope.comp_data[0].dept_comp_code;
                            }
                        });

                        $http.get(ENV.apiUrl + "api/ProductCode/GetFinancialyears").then(function (res) {

                            $scope.fin_data = res.data;

                            if ($scope.fin_data.length > 0) {
                                $scope.temp.finyear = $scope.fin_data[0].fin_year;
                                $scope.edt.finyear_sub = $scope.fin_data[0].fin_year;
                            }

                            $scope.getglaccs();
                        });

                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                    $http.get(ENV.apiUrl + "api/ProductCode/getProductCode").then(function (res1) {
                        $scope.getProduct = res1.data;
                        $scope.totalItems = $scope.getProduct.length;
                        $scope.todos = $scope.getProduct;
                        $scope.makeTodos();
                    });
                });
                $scope.table = true;
                $scope.display = false;
            }

            $scope.update = function () {
                debugger
                data1 = [];
                var data = {
                    pc_category_group:$scope.temp.pc_category_group,
                    pc_code: $scope.temp.pc_code,
                    pc_desc: $scope.temp.pc_desc,
                    compcode: $scope.temp.compcode,
                    finyear: $scope.temp.finyear,
                    revenue_acc_no: $scope.temp.revenue_acc_no,
                    stock_acc_no: $scope.temp.stock_acc_no,
                    cost_acc_no: $scope.temp.cost_acc_no,
                    dis_acc_no: $scope.temp.dis_acc_no,
                };
                data.opr = 'U';

                data1.push(data);
                $http.post(ENV.apiUrl + "api/ProductCode/CUDProductCode", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                    $http.get(ENV.apiUrl + "api/ProductCode/getProductCode").then(function (res1) {
                        $scope.getProduct = res1.data;
                        $scope.totalItems = $scope.getProduct.length;
                        $scope.todos = $scope.getProduct;
                        $scope.makeTodos();
                    });

                });
                $scope.table = true;
                $scope.ifedit = false;
                $scope.display = false;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp.pc_desc = '';
                $scope.temp.depcode = '';
                $scope.temp.compcode = '';
                $scope.temp.finyear = '';
                $scope.temp.pc_category_group = '';
                $("#cmb_accno").select2("val", "");
                $("#cmb_stockaccno").select2("val", "");
                $("#cmb_costaccno").select2("val", "");
                $("#cmb_disaccno").select2("val", "");

                $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                    debugger;
                    $scope.comp_data = res.data;
                    console.log($scope.comp_data);
                    if ($scope.comp_data.length > 0) {
                        $scope.temp.compcode = $scope.comp_data[0].dept_comp_code;
                        $scope.edt.compcode_sub = $scope.comp_data[0].dept_comp_code;
                    }
                });

                $http.get(ENV.apiUrl + "api/ProductCode/GetFinancialyears").then(function (res) {

                    $scope.fin_data = res.data;

                    if ($scope.fin_data.length > 0) {
                        $scope.temp.finyear = $scope.fin_data[0].fin_year;
                        $scope.edt.finyear_sub = $scope.fin_data[0].fin_year;
                    }

                    $scope.getglaccs();
                });

            }


            // Data DELETE RECORD

            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].pc_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].pc_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.OkDelete = function () {
                debugger;
                deletefin = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].pc_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'pc_code': $scope.filteredTodos[i].pc_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/ProductCode/CUDProductCode", deletefin).then(function (msg) {
                                debugger;
                                $scope.msg1 = msg.data;
                                $scope.msg1 = true;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/ProductCode/getProductCode").then(function (res1) {
                                                $scope.getProduct = res1.data;
                                                $scope.totalItems = $scope.getProduct.length;
                                                $scope.todos = $scope.getProduct;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Cannot be Deleted as it is mapped with items. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/ProductCode/getProductCode").then(function (res1) {
                                                $scope.getProduct = res1.data;
                                                $scope.totalItems = $scope.getProduct.length;
                                                $scope.todos = $scope.getProduct;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].pc_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $scope.catagories = function () {
                $http.get(ENV.apiUrl + "api/ProductCode/GetCategoryNames").then(function (GetCategoryNames_Data) {
                    $scope.Category_Data = GetCategoryNames_Data.data;
                    console.log($scope.Category_Data);
                });
            }

            $scope.getglaccs = function () {
                debugger;
                if ($scope.temp.compcode != undefined && $scope.temp.finyear != undefined) {

                    $http.get(ENV.apiUrl + "api/ProductCode/GetGlAccounts?compcode=" + $scope.temp.compcode + "&fin_year=" + $scope.temp.finyear).then(function (res) {
                        $scope.Accounts = res.data;
                        $scope.StockAccounts = res.data;
                        $scope.CostAccounts = res.data;
                        $scope.DisAccounts = res.data;
                    });
                }
            }

            $scope.getglaccs = function (str) {
                debugger;
                if ($scope.temp.compcode != undefined && $scope.temp.finyear != undefined) {

                    $http.get(ENV.apiUrl + "api/ProductCode/GetGlAccounts?compcode=" + $scope.temp.compcode + "&fin_year=" + $scope.temp.finyear).then(function (res) {
                        $scope.Accounts = res.data;
                        $scope.StockAccounts = res.data;
                        $scope.CostAccounts = res.data;
                        $scope.DisAccounts = res.data;
                    });

                    $scope.temp = {
                        pc_category_group:str.pc_category_group,
                        pc_code: str.pc_code,
                        pc_desc: str.pc_desc,
                        compcode: str.compcode,
                        finyear: str.finyear,
                        revenue_acc_no: str.revenue_acc_no,
                        stock_acc_no: str.stock_acc_no,
                        cost_acc_no: str.cost_acc_no,
                        dis_acc_no: str.dis_acc_no,
                    };

                    $("#cmb_accno").select2("val", $scope.temp.revenue_acc_no);
                    $("#cmb_stockaccno").select2("val", $scope.temp.stock_acc_no);
                    $("#cmb_costaccno").select2("val", $scope.temp.cost_acc_no);
                    $("#cmb_disaccno").select2("val", $scope.temp.dis_acc_no);

                    //$scope.temp['revenue_acc_no']= str.revenue_acc_no;
                    //$scope.temp['stock_acc_no ']= str.stock_acc_no;
                    //$scope.temp['cost_acc_no']= str.cost_acc_no;
                    //$scope.temp['dis_acc_no'] = str.dis_acc_no;
                }
            }

            //******************************MODEL POPUP****************************************************************

            $scope.AddSubcatagory = function () {
                $('#MyModal').modal('show');
                $scope.display = true;
            }
            $scope.txt_cat = true;

            // $scope.Modaltable = true;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size1 = function (str1) {
                //console.log(str1);
                //$scope.pagesize1 = str1;
                //$scope.currentPage1 = 1;
                //$scope.numPerPage1 = str1; console.log("numPerPage1=" + $scope.numPerPage1); $scope.makeTodos1();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos1;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index1 = function (str1) {
                $scope.pageindex1 = str1;
                $scope.currentPage1 = str1; console.log("currentPage1=" + $scope.currentPage1); $scope.makeTodos1();
            }

            $scope.filteredTodos1 = [], $scope.currentPage1 = 1, $scope.numPerPage1 = 10, $scope.maxSize1 = 10;

            $scope.makeTodos1 = function () {
                var rem1 = parseInt($scope.totalItems1 % $scope.numPerPage1);
                if (rem1 == '0') {
                    $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1);
                }
                else {
                    $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1) + 1;
                }
                var begin1 = (($scope.currentPage1 - 1) * $scope.numPerPage1);
                var end1 = parseInt(begin1) + parseInt($scope.numPerPage1);

                console.log("begin1=" + begin1); console.log("end1=" + end1);

                $scope.filteredTodos1 = $scope.todos1.slice(begin1, end1);
            };
            $scope.modaloperation = false;
            $scope.Modaltable = true;


            $http.get(ENV.apiUrl + "api/ProductCode/getSubcategoryData").then(function (subcode) {
                $scope.AccountCode_Data = subcode.data;
                console.log($scope.AccountCode_Data);
            });

            $http.get(ENV.apiUrl + "api/ProductCode/GetCategoryNames").then(function (GetCategoryNames_Data) {

                $scope.Category_Data = GetCategoryNames_Data.data;
                console.log($scope.Category_Data);
            });

            $scope.CheckAllChecked1 = function () {
                debugger;
                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                        var v = document.getElementById("-test" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                        var v = document.getElementById("-test" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }
            $scope.New1 = function () {
                $scope.temp = {};
                $scope.edt = {};
                opr = 'S';
                $scope.txt_cat = false;
                $scope.savebtn1 = true;
                $scope.updatebtn1 = false;
                $scope.modaloperation = true;
                $scope.Modaltable = false;
                $scope.tes_ing = false;
                $scope.combocat = true;
                $scope.catagories();

                $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                    debugger;
                    $scope.comp_data = res.data;
                    console.log($scope.comp_data);
                    if ($scope.comp_data.length > 0) {
                        $scope.temp.compcode = $scope.comp_data[0].dept_comp_code;
                        $scope.edt.compcode_sub = $scope.comp_data[0].dept_comp_code;
                    }
                });

                $http.get(ENV.apiUrl + "api/ProductCode/GetFinancialyears").then(function (res) {

                    $scope.fin_data = res.data;

                    if ($scope.fin_data.length > 0) {
                        $scope.temp.finyear = $scope.fin_data[1].fin_year;
                        $scope.edt.finyear_sub = $scope.fin_data[1].fin_year;
                    }

                    $scope.getglaccs();
                });

            }

            //Editcode
            $scope.up1 = function (str1) {
                debugger;
                $scope.modaloperation = true;
                $scope.Modaltable = false;
                // opr = 'U';
                $scope.savebtn1 = false;
                $scope.updatebtn1 = true;
                $scope.combocat = false;
                $scope.readonly = true;
                $scope.tes_ing = true;
                $scope.tester = true;
                $scope.txt_cat = true;

                $scope.edt =
                {
                    pc_code: str1.pc_code,
                    pc_desc: str1.catname,
                    pc_parent_code: str1.pc_parent_code,
                    subcatname: str1.subcatname,
                    compcode_sub: str1.compcode_sub,
                    finyear_sub: str1.finyear_sub,
                }

                $scope.getglaccs_sub(str1);
            }

            $scope.getglaccs_sub = function () {
                debugger;
                if ($scope.edt.compcode_sub != undefined && $scope.edt.finyear_sub != undefined) {

                    $http.get(ENV.apiUrl + "api/ProductCode/GetGlAccounts?compcode=" + $scope.edt.compcode_sub + "&fin_year=" + $scope.edt.finyear_sub).then(function (res) {
                        $scope.Accounts = res.data;
                        $scope.StockAccounts = res.data;
                        $scope.CostAccounts = res.data;
                        $scope.DisAccounts = res.data;
                    });
                }
            }

            $scope.getglaccs_sub = function (str) {
                debugger;
                if ($scope.edt.compcode_sub != undefined && $scope.edt.finyear_sub != undefined) {

                    $http.get(ENV.apiUrl + "api/ProductCode/GetGlAccounts?compcode=" + $scope.edt.compcode_sub + "&fin_year=" + $scope.edt.finyear_sub).then(function (res) {
                        debugger;
                        $scope.Accounts = res.data;
                        $scope.StockAccounts = res.data;
                        $scope.CostAccounts = res.data;
                        $scope.DisAccounts = res.data;
                    });
                    debugger;
                    $scope.edt = {
                        pc_code: str.pc_code,
                        pc_desc: str.catname,
                        pc_parent_code: str.pc_parent_code,
                        subcatname: str.subcatname,
                        compcode_sub: str.compcode_sub,
                        finyear_sub: str.finyear_sub,
                        revenue_acc_no: str.revenue_acc_no_sub,
                        stock_acc_no: str.stock_acc_no_sub,
                        cost_acc_no: str.cost_acc_no_sub,
                        dis_acc_no: str.dis_acc_no_sub,
                    };
                    debugger;
                    console.log($scope.edt.compcode_sub);
                    console.log($scope.edt.finyear_sub);
                    console.log($scope.edt.revenue_acc_no);
                    console.log($scope.edt.stock_acc_no);
                    console.log($scope.edt.cost_acc_no);
                    console.log($scope.edt.dis_acc_no);
                    debugger;
                    $("#cmb_accno_sub").select2("val", $scope.edt.revenue_acc_no);
                    $("#cmb_stockaccno_sub").select2("val", $scope.edt.stock_acc_no);
                    $("#cmb_costaccno_sub").select2("val", $scope.edt.cost_acc_no);
                    $("#cmb_disaccno_sub").select2("val", $scope.edt.dis_acc_no);

                }
            }

            $scope.cancel1 = function () {
                $scope.modaloperation = false;
                $scope.Modaltable = true;
                $scope.temp = "";

                $scope.pc_desc = "";

                $scope.edt.compcode_sub = '';
                $scope.edt.finyear_sub = '';
                $("#cmb_accno_sub").select2("val", "");
                $("#cmb_stockaccno_sub").select2("val", "");
                $("#cmb_costaccno_sub").select2("val", "");
                $("#cmb_disaccno_sub").select2("val", "");

                $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                    debugger;
                    $scope.comp_data = res.data;
                    console.log($scope.comp_data);
                    if ($scope.comp_data.length > 0) {
                        $scope.temp.compcode = $scope.comp_data[0].dept_comp_code;
                        $scope.edt.compcode_sub = $scope.comp_data[0].dept_comp_code;
                    }
                });

                $http.get(ENV.apiUrl + "api/ProductCode/GetFinancialyears").then(function (res) {

                    $scope.fin_data = res.data;

                    if ($scope.fin_data.length > 0) {
                        $scope.temp.finyear = $scope.fin_data[1].fin_year;
                        $scope.edt.finyear_sub = $scope.fin_data[1].fin_year;
                    }

                    $scope.getglaccs();
                });

                $scope.edt["pc_code"] = '';
                $scope.edt["subcatname"] = '';

                $scope.myFormmodal.$setPristine();
            }


            ///Save Insert data for subCatagory
            var data1 = [];
            $scope.Save1 = function () {
                debugger;

                data1 = [];
                for (var i = 0; i < $scope.AccountCode_Data.length; i++) {
                    var pc_parent_code = $scope.AccountCode_Data[0].pc_parent_code;
                }
                var data =
                    {
                        pc_desc: $scope.edt.subcatname,
                        pc_parent_code: $scope.edt.pc_code,
                        pc_code: $scope.temp.pc_code,
                        compcode: $scope.edt.compcode_sub,
                        finyear: $scope.edt.finyear_sub,
                        revenue_acc_no: $scope.edt.revenue_acc_no,
                        stock_acc_no: $scope.edt.stock_acc_no,
                        cost_acc_no: $scope.edt.cost_acc_no,
                        dis_acc_no: $scope.edt.dis_acc_no,
                    }
                // var data = $scope.temp;
                data.opr = 'J';
                debugger;
                data1.push(data);
                $http.post(ENV.apiUrl + "api/ProductCode/CUDProductCode", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                    $http.get(ENV.apiUrl + "api/ProductCode/getSubcategoryData").then(function (subcode) {
                        $scope.AccountCode_Data = subcode.data;
                        console.log($scope.AccountCode_Data);
                    });
                });
                $scope.Modaltable = true;
                $scope.modaloperation = false;
                $scope.pc_desc = "";

                $scope.edt.compcode_sub = '';
                $scope.edt.finyear_sub = '';
                $("#cmb_accno_sub").select2("val", "");
                $("#cmb_stockaccno_sub").select2("val", "");
                $("#cmb_costaccno_sub").select2("val", "");
                $("#cmb_disaccno_sub").select2("val", "");

                $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                    debugger;
                    $scope.comp_data = res.data;
                    console.log($scope.comp_data);
                    if ($scope.comp_data.length > 0) {
                        $scope.temp.compcode = $scope.comp_data[0].dept_comp_code;
                        $scope.edt.compcode_sub = $scope.comp_data[0].dept_comp_code;
                    }
                });

                $http.get(ENV.apiUrl + "api/ProductCode/GetFinancialyears").then(function (res) {

                    $scope.fin_data = res.data;

                    if ($scope.fin_data.length > 0) {
                        $scope.temp.finyear = $scope.fin_data[1].fin_year;
                        $scope.edt.finyear_sub = $scope.fin_data[1].fin_year;
                    }

                    $scope.getglaccs();
                });

                $scope.edt["pc_code"] = '';
                $scope.edt["subcatname"] = '';

            }


            $scope.Update1 = function () {
                data1 = [];
                debugger
                for (var i = 0; i < $scope.AccountCode_Data.length; i++) {
                    var pc_parent_code = $scope.AccountCode_Data[i].pc_parent_code;
                }

                var data = {
                    //'pc_code': $scope.edt.pc_code,
                    //'pc_desc': $scope.edt.subcatname,
                    //'pc_parent_code': pc_parent_code,

                    pc_desc: $scope.edt.subcatname,
                    pc_parent_code: pc_parent_code,
                    pc_code: $scope.edt.pc_code,
                    compcode: $scope.edt.compcode_sub,
                    finyear: $scope.edt.finyear_sub,
                    revenue_acc_no: $scope.edt.revenue_acc_no,
                    stock_acc_no: $scope.edt.stock_acc_no,
                    cost_acc_no: $scope.edt.cost_acc_no,
                    dis_acc_no: $scope.edt.dis_acc_no,
                }
                data.opr = 'U';
                data1.push(data);
                $http.post(ENV.apiUrl + "api/ProductCode/CUDProductCode", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updates Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Not Updated present. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                    $http.get(ENV.apiUrl + "api/ProductCode/getSubcategoryData").then(function (subcode) {
                        $scope.AccountCode_Data = subcode.data;
                        console.log($scope.AccountCode_Data);
                    });

                });
                $scope.edt["pc_code"] = '';
                $scope.edt["pc_desc"] = '';
                $scope.edt["subcatname"] = '';
                $scope.edt.compcode_sub = '';
                $scope.edt.finyear_sub = '';
                $("#cmb_accno_sub").select2("val", "");
                $("#cmb_stockaccno_sub").select2("val", "");
                $("#cmb_costaccno_sub").select2("val", "");
                $("#cmb_disaccno_sub").select2("val", "");

                $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                    debugger;
                    $scope.comp_data = res.data;
                    console.log($scope.comp_data);
                    if ($scope.comp_data.length > 0) {
                        $scope.temp.compcode = $scope.comp_data[0].dept_comp_code;
                        $scope.edt.compcode_sub = $scope.comp_data[0].dept_comp_code;
                    }
                });

                $http.get(ENV.apiUrl + "api/ProductCode/GetFinancialyears").then(function (res) {

                    $scope.fin_data = res.data;

                    if ($scope.fin_data.length > 0) {
                        $scope.temp.finyear = $scope.fin_data[1].fin_year;
                        $scope.edt.finyear_sub = $scope.fin_data[1].fin_year;
                    }

                    $scope.getglaccs();
                });

                $scope.Modaltable = true;
                $scope.modaloperation = false;

            }


            $scope.deleterecord1 = function () {
                debugger;
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.AccountCode_Data.length; i++) {
                    var v = document.getElementById($scope.AccountCode_Data[i].pc_code + $scope.AccountCode_Data[i].pc_parent_code);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'pc_code': $scope.AccountCode_Data[i].pc_code,
                            opr: 'D'

                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/ProductCode/CUDProductCode", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/ProductCode/getSubcategoryData").then(function (subcode) {
                                                $scope.AccountCode_Data = subcode.data;
                                                console.log($scope.AccountCode_Data);
                                            });
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Cannot be Deleted as it is mapped with items. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/ProductCode/getSubcategoryData").then(function (subcode) {
                                                $scope.AccountCode_Data = subcode.data;
                                                console.log($scope.AccountCode_Data);
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }

            }

        }])

})();
