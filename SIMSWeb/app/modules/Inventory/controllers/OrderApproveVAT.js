﻿(function () {
    'use strict';
    var obj1, temp, opr, CurrentDate, comp;
    var main, deletefin = [], data = [], data1 = [], ItemDetails = [];
    var finanacecode = [];
    var check;
    var reqno;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('OrderApproveVATCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {
            $scope.table = true;

            var date = new Date();
            //$scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss');
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');
            //$scope.IP = {
            //    rd_from_date: dd + '-' + mm + '-' + yyyy,
            //    rd_up_date: dd + '-' + mm + '-' + yyyy
            //}

            $http.get(ENV.apiUrl + "api/CreateOrder/getRequestTypeNew").then(function (reqtypedetailsNew) {
                $scope.req_type_detailsNew = reqtypedetailsNew.data;
                $scope.temp['request_mode_code'] = $scope.req_type_detailsNew[0].request_mode_code;

            });

            $scope.hide_save = false;
            $scope.hide_saves = false;
            $scope.flag_iforderdiscount = "false";

            $scope.IP = {};
            $scope.sel = {};

            $http.get(ENV.apiUrl + "api/CreateOrder/getDeparments_approve").then(function (deptdata) {
                $scope.dept_data = deptdata.data;
                // $scope.IP['dept_code'] = $scope.dept_data[0].dept_code;
                console.log($scope.dept_data);
            });

            //var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
            //$scope.IP = {
            //    rd_from_required: dateyear,
            //    rd_up_required: dateyear,
            //}

            $http.get(ENV.apiUrl + "api/CreateOrder/GetOrderAttributes").then(function (res2) {
                $scope.OrderAttrs = res2.data;
                console.log("OrderAttrs", $scope.OrderAttrs);
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetSuppliers").then(function (res11) {
                $scope.Suppliers = res11.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetCurrencyMaster").then(function (res1) {
               $scope.CurMaster = res1.data;
                $http.get(ENV.apiUrl + "api/CreateOrder/GetCompanyCurrency").then(function (res_cur) {
                    console.log(res_cur);
                    if ($scope.CurMaster.length > 0)
                        $scope.temp['excg_curcy_code'] = res_cur.data;
                    //$("#cmb_currency").select2("val", res_cur.data);
                });
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetDeliveryMOde").then(function (res1) {
                $scope.Deliverymode = res1.data;

                if ($scope.Deliverymode.length > 0)
                    $scope.temp['dm_code'] = $scope.Deliverymode[0].dm_code;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/Getpaymentmodes").then(function (res1) {
                $scope.paymentmode = res1.data;

                if ($scope.paymentmode.length > 0)
                    $scope.temp['pm_code'] = $scope.paymentmode[0].pm_code;

            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetTradeterms").then(function (res1) {
                $scope.Trades = res1.data;

                if ($scope.Trades.length > 0)
                    $scope.temp['trt_code'] = $scope.Trades[0].trt_code;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetForwardAgent").then(function (res1) {
                $scope.Agent = res1.data;

                if ($scope.Agent.length > 0)
                    $scope.temp['fa_code'] = $scope.Agent[0].fa_code;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/Getvehiclergnos").then(function (res_veh1) {
                console.log(res_veh1);
                $scope.res_veh = res_veh1.data;
                if ($scope.res_veh.length > 0)

                    $scope.temp['sims_transport_vehicle_code'] = $scope.res_veh[0].sims_transport_vehicle_code;
            });


            $scope.SelectIetm = function () {
                debugger;

                $scope.IP.loginuser = $rootScope.globals.currentUser.username;
                $http.get(ENV.apiUrl + "api/CreateOrder/getOrderDetailsforapprove_VAT?data=" + JSON.stringify($scope.IP)).then(function (res) {
                    $scope.rows = res.data.table;
                    $scope.Allrows = res.data.table1;
                    console.log("rows", $scope.rows);
                    console.log("Allrows", $scope.Allrows);
                    if ($scope.Allrows.length <= 0) {
                        //$scope.remarks = '';
                        //$scope.hide_save = true;
                        //$scope.hide_saves = false;
                        swal({ text: "Records not found", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    else {
                        for (var r in $scope.rows) {
                            $scope.rows[r]['sims_icon'] = "fa fa-minus-circle";
                            $scope.rows[r]['isexpanded'] = "grid";
                            $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].ord_no);
                            $scope.remarks = $scope.rows[r].ord_approval_comment;
                            console.log("r remarks", $scope.remarks);
                        }
                        console.log("remarks", $scope.remarks);


                        $scope.hide_save = false;
                        $scope.hide_saves = true;

                    }

                    console.log(res.data.table);
                    console.log(res.data.table1);
                });
            }

            $scope.SelectIetm();

            $scope.SelectItem_1 = function () {
                debugger;
                $scope.remarks = '';
                $scope.hide_save = false;
                $scope.hide_saves = false;

                $http.get(ENV.apiUrl + "api/CreateOrder/getOrderDetailsforapprove_VAT?data=" + JSON.stringify($scope.IP)).then(function (res) {
                    $scope.rows = res.data.table;
                    $scope.Allrows = res.data.table1;
                    if ($scope.Allrows.length <= 0) {
                        //swal({ title: "Alert", text: "Records not found", showCloseButton: true, width: 380, });
                        $scope.remarks = '';
                        $scope.hide_save = false;
                        $scope.hide_saves = false;
                    }
                    else {
                        for (var r in $scope.rows) {
                            $scope.rows[r]['sims_icon'] = "fa fa-minus-circle";
                            $scope.rows[r]['isexpanded'] = "grid";
                            $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].ord_no);
                        }
                        $scope.hide_save = false;
                        $scope.hide_saves = true;

                    }

                    console.log(res.data.table);
                    console.log(res.data.table1);
                });
            }

            function getSubitems(dno) {
                var arr = [];
                for (var i = 0; i < $scope.Allrows.length; i++) {
                    if ($scope.Allrows[i].ord_no == dno) {
                        arr.push($scope.Allrows[i]);
                    }
                }
                return arr;
            }

            $scope.expand = function (j, $event) {
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";
                    console.log(j);
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none";
                }
            }

            $scope.reset = function () {
                $scope.IP = {};
                $scope.rows = {};
                $scope.Allrows = {};
                $scope.IP.rd_from_date = '';
                $scope.IP.rd_up_date = '';
            }

            $scope.checkRange = function (s) {
                debugger
                var rq = parseFloat(s.final_qnt);
                //if (rq == NaN)
                //    rq = 0;
                if (isNaN(rq)) {
                    rq = 0;
                }
                s.final_qnt = rq;
            }

            $scope.ordclick = function (gh) {
                var l = gh.subItems.length;
                for (var i = 0; i < l; i++) {
                    gh.subItems[i]['isChecked'] = gh.isChecked;
                }
            }

            $scope.ordlineclick = function (r, m) {
                var l = r.subItems.length;
                var f = false;
                if (m) {
                    f = true;
                    for (var i = 0; i < l; i++) {
                        f = f && r.subItems[i]['isChecked'];
                    }
                }
                r.isChecked = f;
            }

            $http.get(ENV.apiUrl + "api/AdjustmentReason/getDept").then(function (res) {
                $scope.sel['Dept'] = res.data.table;
            });

            $scope.save = function (s) {
                $scope.hide_save = true;
                $scope.hide_saves = true;
                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    debugger;
                    for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                        if ($scope.rows[i].subItems[j].isChecked) {
                            var ob = {};
                            ob.od_order_qty = $scope.rows[i].subItems[j].od_order_qty;
                            ob.final_qnt = $scope.rows[i].subItems[j].final_qnt;
                            ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.od_line_no = $scope.rows[i].subItems[j].od_line_no;
                            ob.ord_no = $scope.rows[i].subItems[j].ord_no;
                            ob.req_type = $scope.rows[i].subItems[j].req_type;
                            ob.req_no = $scope.rows[i].subItems[j].req_no;
                            ob.req_remarks = $scope.rows[i].ord_approval_comment;//$scope.remarks;
                            ob.ord_approved_by = $rootScope.globals.currentUser.username;
                            //ob.adj_status = $scope.rows[i].subItems[j].adj_status;
                            ob.sims_workflow_no = $scope.rows[i].subItems[j].sims_workflow_no;
                            ob.val = r;
                            ar.push(ob);
                        }
                    }
                }
                if (ar.length == 0) {
                    alert("Nothing Selected");
                    return;
                }

                $http.post(ENV.apiUrl + "api/CreateOrder/UpdateOrderDetailsforapprove_VAT", ar).then(function (res) {
                    console.log("res", res);
                    console.log("res.data", res.data);

                    //swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
                    //$scope.SelectIetm();

                    if (res.data != '0') {
                        //res.data != null && res.data != '' &&
                        swal({ text: 'Purchase Order No ' + res.data + ' Approved.', imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.SelectItem_1();

                                if ($http.defaults.headers.common['schoolId'] == 'siso') {
                                    var data = {
                                        location: 'Invs.invr21Siso',
                                        parameter: {
                                            //orderno: $scope.msg1,
                                            orderno: res.data,
                                        },
                                        state: 'main.apoasd',
                                        ready: function () {
                                            this.refreshReport();
                                        },
                                    }
                                }
                                else if ($http.defaults.headers.common['schoolId'] == 'rakmps' || $http.defaults.headers.common['schoolId'] == 'rmps') {
                                    var data = {
                                        location: 'Invs.invr21RAKMPS',
                                        parameter: {
                                            //orderno: $scope.msg1,
                                            orderno: res.data,
                                        },
                                        state: 'main.apoasd',
                                        ready: function () {
                                            this.refreshReport();
                                        },
                                    }
                                }
                                else if ($http.defaults.headers.common['schoolId'] == 'tosdxb' || $http.defaults.headers.common['schoolId'] == 'tiadxb' || $http.defaults.headers.common['schoolId'] == 'tiashj') {                             
                                    var data = {
                                        location: 'Invs.invr21LEAMS',//'Invs.invr21',
                                        parameter: {
                                            //orderno: $scope.msg1,
                                            orderno: res.data,
                                        },
                                        state: 'main.apoasd',
                                        ready: function () {
                                            this.refreshReport();
                                        },
                                    }
                                }
                                else {
                                    var data = {
                                        location: 'Invs.invr21RAKMPS',//'Invs.invr21',
                                        parameter: {
                                            //orderno: $scope.msg1,
                                            orderno: res.data,
                                        },
                                        state: 'main.apoasd',
                                        ready: function () {
                                            this.refreshReport();
                                        },
                                    }
                                }


                                console.log(data);

                                window.localStorage["ReportDetails"] = JSON.stringify(data);
                                $state.go('main.ReportCardParameter');
                            }
                        });
                    }
                    else if (res.data == '0') {
                        swal({ text: "Purchase order approved pending for next approval.", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        setTimeout(function () {
                            $scope.SelectIetm();
                        }, 3000);
                    }
                    else {
                        swal({ text: "Purchase order not approved.", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }

                });

            }

            $scope.reject = function (s) {
                $scope.hide_save = true;
                $scope.hide_saves = true;
                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    debugger;
                    for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                        if ($scope.rows[i].subItems[j].isChecked) {
                            var ob = {};
                            ob.od_order_qty = $scope.rows[i].subItems[j].od_order_qty;
                            ob.final_qnt = $scope.rows[i].subItems[j].final_qnt;
                            ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.od_line_no = $scope.rows[i].subItems[j].od_line_no;
                            ob.ord_no = $scope.rows[i].subItems[j].ord_no;
                            ob.req_type = $scope.rows[i].subItems[j].req_type;
                            ob.req_no = $scope.rows[i].subItems[j].req_no;
                            ob.req_remarks = $scope.rows[i].ord_approval_comment; //$scope.remarks;
                            ob.ord_approved_by = $rootScope.globals.currentUser.username;
                            //ob.adj_status = $scope.rows[i].subItems[j].adj_status;
                            ob.sims_workflow_no = $scope.rows[i].subItems[j].sims_workflow_no;
                            ob.val = r;
                            ar.push(ob);
                        }
                    }
                }
                if (ar.length == 0) {
                    alert("Nothing Selected");
                    return;
                }

                $http.post(ENV.apiUrl + "api/CreateOrder/UpdateOrderDetailsforreject_vat", ar).then(function (res) {
                    console.log(res);

                    //swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
                    //$scope.SelectIetm();

                    swal({ text: 'Purchase Order No ' + res.data + ' Rejected.',showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.SelectItem_1();

                        }
                    });
                });
            }

            //$scope.Reset = function () {
            //    $scope.IP = {
            //        rd_from_required: '',
            //        rd_up_required: '',
            //        req_no: '',
            //        req_range_no: '',
            //        request_mode_code: '',
            //        rd_item_desc: '',
            //    }
            //}

            //$scope.save = function (s) {
            //    $scope.hide_save = true;
            //    $scope.hide_saves = true;
            //    var ar = [];
            //    var r = Math.random() * 10;
            //    for (var i = 0; i < $scope.rows.length; i++) {
            //        for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
            //            if ($scope.rows[i].subItems[j].isChecked) {
            //                var ob = {};
            //                ob.od_order_qty = $scope.rows[i].subItems[j].od_order_qty;
            //                ob.final_qnt = $scope.rows[i].subItems[j].final_qnt;
            //                ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
            //                ob.od_line_no = $scope.rows[i].subItems[j].od_line_no;
            //                ob.ord_no = $scope.rows[i].subItems[j].ord_no;
            //                //ob.t_qty = $scope.rows[i].subItems[j].ad_qty;
            //                //ob.adj_status = $scope.rows[i].subItems[j].adj_status;
            //                ob.val = r;
            //                ar.push(ob);
            //            }
            //        }
            //    }
            //    if (ar.length == 0) {
            //        alert("Nothing Selected");
            //        return;
            //    }

            //    $http.post(ENV.apiUrl + "api/CreateOrder/UpdateOrderDetailsforapprove", ar).then(function (res) {
            //        console.log(res);

            //        swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
            //        $scope.SelectIetm();
            //    });

            //}

            $scope.Reset = function () {
                $scope.IP = {
                    rd_from_required: '',
                    rd_up_required: '',
                    req_no: '',
                    req_range_no: '',
                    request_mode_code: '',
                    rd_item_desc: '',
                    loginuser: '',
                }
            }

            //Upload Documents Functionality
            $scope.UploadDocuments = function (ordno) {
                debugger;
                $scope.Upload_doc = true;
                $scope.images_newfiles = [];
                $scope.images = [];
                $scope.requestdocs = '';

                $('#UploadDocModal').modal({ backdrop: 'static', keyboard: true });
                var doccnt = $scope.images_newfiles.length;
                $scope.doccount = doccnt + ' file(s) uploaded.';
                $scope.ordno = ordno;
                var vouchertype = '2';

                $http.get(ENV.apiUrl + "api/CreateOrder/GetOrderDocsApprove?vtype=" + vouchertype + "&vouchernum=" + ordno).then(function (res) {
                    $scope.requestdocs = res.data;
                    console.log($scope.requestdocs);

                    //setTimeout(function () {
                    debugger;
                    for (var k = 0; k < $scope.requestdocs.length; k++) {
                        var or_filename = $scope.requestdocs[k].filename;
                        var new_filename = $scope.requestdocs[k].filename_en;

                        $scope.images_newfiles.push({
                            new_filename: new_filename,
                            or_filename: or_filename
                        });

                        var doccnt = $scope.images_newfiles.length;
                        $scope.doccount = doccnt + ' file(s) uploaded.';
                    }
                    //}, 1000);
                });
            }

            /*start_attached Files*/

            var formdata = new FormData();
            $scope.images = [];
            $scope.images_att = [];
            var under_file = 1;

            $scope.getTheFiles = function ($files) {
                debugger;
                $scope.filesize = true;
                var i = 0, exists = "no";

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);

                    var i = 0;
                    if ($files[i].size > 2000000) {
                        $scope.filesize = false;
                        // $scope.edt.photoStatus = false;
                        swal({ title: "Alert", text: "File Should Not Exceed 2MB.", showCloseButton: true, width: 380, });
                    }
                    else {
                        //arr_files.push($files[i].name);
                        if ($scope.images.length > 0) {
                            for (var j = 0 ; j < $scope.images.length; j++) {
                                if ($scope.images[j].name == $files[i].name) {
                                    exists = "yes";
                                    return;
                                }
                            }

                            if (exists == "no") {

                                $scope.images.push({
                                    name: $files[i].name,
                                    file: $files[i].size
                                });
                                if ($scope.images.length > 10) {
                                    $scope.images.splice(10, $scope.images.length);
                                    swal({ title: "Alert", text: "You cannot upload more than 10 files.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                        }
                                    });
                                    return;
                                }
                            }
                            exists = "no";
                        }
                        else {
                            $scope.images.push({
                                name: $files[i].name,
                                file: $files[i].size

                            });
                        }
                        // $scope.$apply();
                    }
                    i++;
                });
            };

            $scope.file_changed = function (element, str) {
                debugger;
                var photofile = element.files[0];
                var v = photofile.name.indexOf('.');
                $scope.name = photofile.name.substr(0, v);
                $scope.fileext = photofile.name.substr(v + 1);
                $scope.photo_filename = (photofile.type);
                var files = new FormData();
                var filesData = $('#file1')[0].files;
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;

                    });
                };
                reader.readAsDataURL(photofile);

                if (element.files[0].size < 2000000) {
                    var filenm = '';
                    //var cnumber = document.getElementById('txt_number').value;
                    var cnumber = '1';
                    var ind = 0;

                    filenm = cnumber + '_' + '(' + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss') + ')';
                    var filenm_images = cnumber + '_' + '(' + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss') + ')' + '.' + $scope.fileext;
                    $scope.images_newfiles.push({
                        new_filename: filenm_images,
                        or_filename: element.files[0].name
                    });
                    files.append(filesData[0].name, filesData[0], filesData[0].name.replace(/\s/g, ""));
                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + 'api/file/uploadOrderDocument?filename=' + filenm + "&location=" + "OrderDocuments",
                        data: files,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    debugger
                    var doccnt = $scope.images_newfiles.length;
                    $scope.doccount = doccnt + ' file(s) uploaded.';
                    $http(request).success(function (d) {
                        under_file = under_file + 1;
                    });

                    console.log($scope.name.type);
                    console.log($scope.name);
                }
            };

            $scope.uploadClick = function (str) {
                $scope.filesize = true;
                $scope.edt1 = str;
                $scope.ins = false;
                formdata = new FormData();
            }

            $scope.delpush = [];

            $scope.CancelFileUpload = function (idx, Upload_doc) {
                debugger;
                $scope.delpush.push(idx);

                var comcode = '1';
                var vouchertype = '2';
                var filepath = 'content/images/OrderDocuments';
                var data_doc = {};
                var datasend_doc = [];
                if($scope.images_newfiles.length > 0) {

                    for (var j = 0 ; j < $scope.images_newfiles.length; j++) {

                        var original_filename = $scope.images_newfiles[j].or_filename;
                        var new_filename = $scope.images_newfiles[j].new_filename;
                        var doclinenum = j + 1;
                        if (Upload_doc.new_filename == new_filename) {
                            data_doc = {
                                'comcode': comcode,
                                'vouchernum': $scope.ordno,
                                'vouchertype': vouchertype,
                                'doclineno': doclinenum,
                                'filename': original_filename,
                                'filename_en': new_filename,
                                'orddate': $scope.ddMMyyyy,
                                'filepath': filepath,
                            }
                            datasend_doc.push(data_doc);
                        }
                    }
                }

                $http.post(ENV.apiUrl + "api/CreateOrder/DelOrderDocs", datasend_doc).then(function (msg) {
                    if (msg.data == true) {
                        $scope.images_newfiles.splice(idx, 1);
                        swal({ title: "Alert", text: "Documents deleted.", width: 300, height: 200 });
                        $('#UploadDocModal').modal('hide');
                        // $scope.images.splice(idx, 1);


                    }
                    else {
                        swal({ title: "Alert", text: "Documents not delete.", width: 300, height: 200 });
                        $('#UploadDocModal').modal('hide');
                    }
                });

                var doccnt = $scope.images_newfiles.length;
                $scope.doccount = doccnt + ' file(s) uploaded.';              
            };

            /*end_attached Files*/

            //File Download
            $scope.download = function (str) {
                window.open(ENV.apiUrl + '/content/' + $http.defaults.headers.common['schoolId'] + "/Images/OrderDocuments/" + str, "_new");
            };

            $scope.saveorderdocs = function () {
                debugger;
                //Save Order Docs

                var comcode = '1';
                var vouchertype = '2';
                var filepath = 'content/images/OrderDocuments';
                var data_doc = {};
                var datasend_doc = [];
                if ($scope.images_newfiles.length > 0) {

                    for (var j = 0 ; j < $scope.images_newfiles.length; j++) {

                        var original_filename = $scope.images_newfiles[j].or_filename;
                        var new_filename = $scope.images_newfiles[j].new_filename;
                        var doclinenum = j + 1;

                        data_doc = {
                            'comcode': comcode,
                            'vouchernum': $scope.ordno,
                            'vouchertype': vouchertype,
                            'doclineno': doclinenum,
                            'filename': original_filename,
                            'filename_en': new_filename,
                            'orddate': $scope.ddMMyyyy,
                            'filepath': filepath,
                        }
                        datasend_doc.push(data_doc);
                    }
                }

                $http.post(ENV.apiUrl + "api/CreateOrder/DelOrderDocs", datasend_doc).then(function (msg) {

                    $http.post(ENV.apiUrl + "api/CreateOrder/SaveOrderDocs", datasend_doc).then(function (msg) {

                        if(msg.data == true) {
                            swal({ title: "Alert", text: "Documents Uploaded.", width: 300, height: 200 });
                            $('#UploadDocModal').modal('hide');
                        }
                        else {
                            swal({ title: "Alert", text: "Documents Not Uploaded.", width: 300, height: 200 });
                            $('#UploadDocModal').modal('hide');
                        }

                    });
                });
            };

            $scope.cancelorderdocs = function () {
                $('#UploadDocModal').modal('hide');
            }
            //End Documents Funtionality//


            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            })

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            //$scope.IP = {
            //    rd_from_date: dd + '-' + mm + '-' + yyyy,
            //    rd_up_date: dd + '-' + mm + '-' + yyyy
            //}

            $scope.orderData = [];

            $scope.editOrderModal = function (obj) {
                console.log("obj", obj);
                debugger;
                $scope.testALL = [];
                $scope.temp = {};
                $scope.total_netamt = obj.totalNetAmt;
                $scope.vatTotalAmt = obj.ord_vat_total_amount;
                $scope.orderdisamt = obj.ord_disamt;
                $scope.totalExpense = obj.ord_addexps;
                $scope.addDiscountper = obj.ord_dispct;
                $scope.adddisAmt = obj.ord_discount_amt;                
                $scope.ord_no = obj.ord_no;
                $scope.total_grossamt = obj.totalGrossAmt;
                $scope.totAmt = obj.orderAmount;
                $scope.totAmtCopy = obj.orderAmount;
                $scope.copyOrderObject = angular.copy(obj);

                $scope.temp.sup_code = obj.sup_code;
                $scope.temp.excg_curcy_code = obj.cur_code;
                $scope.temp.dm_code = obj.dm_code;          
                $scope.temp.pm_code = obj.pm_code;
                $scope.temp.trt_code = obj.trt_code;
                $scope.temp.fa_code = obj.fa_code;
                $scope.temp.supplier_qty_ref = obj.ord_supl_quot_ref;
                $scope.temp.sims_transport_vehicle_code = obj.od_vehicle_no;
                $scope.temp.invs058_heading = obj.order_heading;
                $scope.temp.invs058_notes = obj.order_notes;
                $scope.temp.expected_date = obj.ord_shipment_date;
                
                angular.forEach(obj.subItems, function (value, key) {

                    var ob = {
                        cur_code: obj.cur_code,
                        fagentname: obj.fagentname,
                        ord_addexps: obj.ord_addexps,
                        ord_date: obj.ord_date,
                        ord_no: obj.ord_no,
                        ord_vat_total_amount: obj.ord_vat_total_amount,
                        orderAmount: obj.orderAmount,
                        order_createdby: obj.order_createdby,
                        orderdisamt: obj.ord_disamt,
                        totalDiscount: obj.totalDiscount,
                        totalExpense: obj.totalExpense,
                        total_netamt: obj.totalNetAmt,
                        tradeterms: obj.tradeterms,
                        vehicle_name: obj.vehicle_name,
                        ord_dispct: obj.ord_dispct,
                        ord_discount_amt: obj.ord_discount_amt,

                        dep_code: value.dep_code,
                        dep_name: value.dep_name,
                        final_qnt: value.final_qnt,
                        id_cur_qty: value.id_cur_qty,
                        im_inv_no: value.im_inv_no,
                        im_malc_rate: value.im_malc_rate,
                        line_discount_amount: value.line_discount_amount,
                        invs058_rqvalue: value.netamt,
                        invs058_gross_amt: value.grossamt,
                        od_discount_pct: value.od_discount_pct,
                        dd_item_vat_amount: value.od_item_vat_amount,
                        im_item_vat_percentage: value.od_item_vat_per,
                        od_line_no: value.od_line_no,
                        invs058_ordered_quantity: value.od_order_qty,
                        invs058_sup_price: value.od_supplier_price,
                        od_total_discount: value.od_total_discount,
                        od_total_expense: value.od_total_expense,
                        rd_item_desc: value.rd_item_desc,
                        req_no: value.req_no,
                        req_type: value.req_type,
                        sup_name: value.sup_name,
                        uom_name: value.uom_name,
                        invs058_discount: value.od_line_discount_pct,
                        invs058_discount_amt: value.od_line_discount_amt,
                        od_remarks: value.od_remarks
                    };
                    $scope.testALL.push(ob);
                });

                for (var i = 0; i < $scope.OrderAttrs.length; i++) {
                    if (obj.ord_addexps != undefined || obj.ord_addexps != "" || obj.ord_addexps != null) {
                        if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                            $scope.OrderAttrs[i].or_atamount = obj.ord_addexps;
                        }
                    }
                    if (obj.ord_dispct != undefined || obj.ord_dispct != "" || obj.ord_dispct != null) {
                        if ($scope.OrderAttrs[i].or_atstatus == 'P') {
                            $scope.OrderAttrs[i].or_atamount = obj.ord_dispct;
                        }
                    }
                    if (obj.ord_disamt != undefined || obj.ord_disamt != "" || obj.ord_disamt != null) {
                        if ($scope.OrderAttrs[i].or_atstatus == 'S') {
                            $scope.OrderAttrs[i].or_atamount = obj.ord_disamt;
                        }
                    }
                }

                console.log("testAll", $scope.testALL);

                $('#EditOrderModal').modal({ backdrop: 'static', keyboard: true });
            };


            $scope.calamount121 = function (info, va) {
                debugger;
                $scope.totAmt = 0;
                if (va == 'amt') {
                    if (info.od_line_discount_amt != '' && info.od_line_discount_amt != undefined) {
                        info.od_line_discount_pct = '';
                    }
                }
                if (va == 'dis') {
                    if (info.od_line_discount_pct != '' && info.od_line_discount_pct != undefined) {
                        if (parseFloat(info.od_line_discount_pct) > 100) {
                            info.od_line_discount_pct = '';
                            swal({ title: "Alert", text: "Invalid Discount", width: 300, height: 200 });
                        }
                        info.od_line_discount_pct_amt = '';
                    }
                }

                var totalamt = 0;
                var totalgrossamt = 0;
                var totalnetamt = 0;
                $scope.ord_vat_total_amount = '';
                var totalamt1 = 0;
                var totalamt_gross1 = 0;
                var dis_vat = 0;
                var vat_total_amt = 0;
                var vatAMT = 0;
                for (var i = 0; i < $scope.orderData.length; i++) {
                    //if ($scope.orderData[i].invs058_rd_line_no1 == true) {

                    if ($scope.orderData[i].od_order_qty != undefined && $scope.orderData[i].od_order_qty != '') {
                        if ($scope.orderData[i].od_supplier_price != undefined && $scope.orderData[i].od_supplier_price != '') {


                            totalamt1 = parseFloat($scope.orderData[i].od_order_qty) * parseFloat($scope.orderData[i].od_supplier_price);
                            $scope.orderData[i].netamt = $scope.orderData[i].od_order_qty * $scope.orderData[i].od_supplier_price;
                            $scope.orderData[i].grossAmt = $scope.orderData[i].od_order_qty * $scope.orderData[i].od_supplier_price;
                            $scope.orderData[i].invs058_gross_amt_vat = $scope.orderData[i].od_order_qty * $scope.orderData[i].od_supplier_price;

                            totalamt_gross1 = $scope.orderData[i].grossAmt;


                            //if ($scope.orderData[i].od_item_vat_per != undefined || $scope.orderData[i].od_item_vat_per != "" || $scope.orderData[i].od_item_vat_per != null) {
                            //    //var finalPer = parseFloat(info.invs058_gross_amt_vat) * parseFloat(info.od_item_vat_per);
                            //    //var totalvat = parseFloat(finalPer) / 100;
                            //    //info.od_item_vat_amount = parseFloat(totalvat.toFixed(3)); //parseFloat($scope.orderData[i].invs058_gross_amt_vat) + parseFloat(totalvat);
                            //    //info.grossAmt = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.od_item_vat_amount);
                            //    //info.totalNetAmt = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.od_item_vat_amount);
                            //    //totalamt_gross1 = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.od_item_vat_amount);
                            //    //totalamt1 = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.od_item_vat_amount);
                            //    //vat_total_amt = vat_total_amt + $scope.orderData[i].od_item_vat_amount;

                            //    var sell_val_final = parseFloat($scope.orderData[i].od_order_qty) * parseFloat($scope.orderData[i].od_supplier_price);
                            //    var totalvat = (parseFloat(sell_val_final) * $scope.orderData[i].od_item_vat_per) / 100;
                            //    $scope.orderData[i].od_item_vat_amount = parseFloat(totalvat).toFixed(3);
                            //    $scope.orderData[i].grossAmt = parseFloat(sell_val_final) + parseFloat(totalvat);
                            //    $scope.orderData[i].netamt = parseFloat(sell_val_final) + parseFloat(totalvat);
                            //    totalamt_gross1 = parseFloat(sell_val_final) + parseFloat(totalvat);
                            //    totalamt1 = parseFloat(sell_val_final) + parseFloat(totalvat);
                            //    vatAMT = $scope.orderData[i].od_item_vat_amount;
                            //}
                            //else {
                            //    totalamt1 = parseFloat($scope.orderData[i].od_order_qty) * parseFloat($scope.orderData[i].od_supplier_price);
                            //    $scope.orderData[i].netamt = $scope.orderData[i].od_order_qty * $scope.orderData[i].od_supplier_price;
                            //    $scope.orderData[i].grossAmt = $scope.orderData[i].od_order_qty * $scope.orderData[i].od_supplier_price;
                            //    $scope.orderData[i].invs058_gross_amt_vat = $scope.orderData[i].od_order_qty * $scope.orderData[i].od_supplier_price;

                            //    totalamt_gross1 = $scope.orderData[i].grossAmt;
                            //}

                            if ($scope.flag_iforderdiscount == "false") {
                                //if (va == 'dis'){
                                if ($scope.orderData[i].od_line_discount_pct != undefined && $scope.orderData[i].od_line_discount_pct != '') {
                                    var disamt = ((totalamt1 * parseFloat($scope.orderData[i].od_line_discount_pct)) / 100);
                                    $scope.orderData[i].netamt = $scope.orderData[i].netamt - disamt;
                                    totalamt1 = totalamt1 - disamt;
                                    //}

                                    //if (info.od_item_vat_per != undefined || info.od_item_vat_per != "" || info.od_item_vat_per != null) {
                                    //    dis_vat = (((totalamt1) * info.od_item_vat_per) / 100);
                                    //    info.od_item_vat_amount = parseFloat(dis_vat).toFixed(3);

                                    //    totalamt1 = parseFloat(parseFloat(totalamt1) + parseFloat(info.od_item_vat_amount)).toFixed(3);
                                    //    totalamt1 = totalamt1 - parseFloat(disamt).toFixed(3);
                                    //}
                                }
                                if ($scope.orderData[i].od_line_discount_amt != undefined && $scope.orderData[i].od_line_discount_amt != '') {
                                    var disamt = parseFloat($scope.orderData[i].od_line_discount_amt);
                                    $scope.orderData[i].netamt = $scope.orderData[i].netamt - $scope.orderData[i].od_line_discount_amt;
                                    totalamt1 = totalamt1 - disamt;

                                    //if (info.od_item_vat_per != undefined || info.od_item_vat_per != "" || info.od_item_vat_per != null) {
                                    //    dis_vat = (((totalamt1) * info.od_item_vat_per) / 100);
                                    //    info.od_item_vat_amount = parseFloat(dis_vat).toFixed(3);

                                    //    totalamt1 = parseFloat(parseFloat(totalamt1) + parseFloat(info.od_item_vat_amount)).toFixed(3);
                                    //    totalamt1 = totalamt1 - parseFloat(disamt).toFixed(3);
                                    //}

                                }
                            }
                            else {
                                $scope.orderData[i].od_line_discount_pct = '';
                                $scope.orderData[i].od_line_discount_amt = '';
                            }

                            if ($scope.orderData[i].od_item_vat_per != undefined || $scope.orderData[i].od_item_vat_per != "" || $scope.orderData[i].od_item_vat_per != null) {                               

                                var sell_val_final = parseFloat($scope.orderData[i].od_order_qty) * parseFloat($scope.orderData[i].od_supplier_price);
                                var totalvat = (parseFloat(sell_val_final) * $scope.orderData[i].od_item_vat_per) / 100;
                                $scope.orderData[i].od_item_vat_amount = parseFloat(totalvat).toFixed(3);
                                $scope.orderData[i].grossAmt = parseFloat($scope.orderData[i].grossAmt) + parseFloat(totalvat);
                                $scope.orderData[i].netamt = parseFloat($scope.orderData[i].netamt) + parseFloat(totalvat);
                                totalamt_gross1 = parseFloat(sell_val_final) + parseFloat(totalvat);
                                totalamt1 = $scope.orderData[i].netamt;//parseFloat(sell_val_final) + parseFloat(totalvat);
                                vatAMT = $scope.orderData[i].od_item_vat_amount;
                            }
                            totalnetamt = totalnetamt + totalamt1;
                            totalgrossamt = totalgrossamt + totalamt_gross1;
                            vat_total_amt = vat_total_amt + parseFloat(vatAMT);

                        }
                        else {
                            $scope.orderData[i].netamt = 0;
                            $scope.orderData[i].grossAmt = 0;
                        }
                    }


                    totalamt = totalamt + totalamt1;
                    $scope.totalNetAmt = totalnetamt.toFixed(2);
                    $scope.total_grossamt = totalgrossamt;
                    $scope.ord_vat_total_amount = vat_total_amt;
                    totalamt1 = 0;
                    totalamt_gross1 = 0;
                    // }
                }

                if (totalamt != NaN) {
                    $scope.totAmt = totalamt;
                    $scope.total_grossamt = totalgrossamt;
                    $scope.totalNetAmt = totalnetamt.toFixed(2);
                    $scope.ord_vat_total_amount = vat_total_amt;
                    $scope.totAmtCopy = $scope.totAmt;
                }
                else { $scope.totAmt = 0; $scope.totAmtCopy = $scope.totAmt; }

            }

            $scope.calamount = function (info, va) {
                debugger;
                $scope.totAmt = 0;
                if (va == 'amt') {
                    if (info.invs058_discount_amt != '' && info.invs058_discount_amt != undefined) {
                        info.invs058_discount = '';
                    }
                }
                if (va == 'dis') {
                    if (info.invs058_discount != '' && info.invs058_discount != undefined) {
                        if (info.invs058_discount > 100) {
                            info.invs058_discount = '';
                            swal({ title: "Alert", text: "Invalid Discount", width: 300, height: 200 });
                        }
                        info.invs058_discount_amt = '';
                    }
                }

                var totalamt = 0;
                var totalgrossamt = 0;
                var totalnetamt = 0;
                $scope.vatTotalAmt = '';
                var totalamt1 = 0;
                var totalamt_gross1 = 0;
                var dis_vat = 0;
                var vat_total_amt = 0;
                var vatAMT = 0;
                for (var i = 0; i < $scope.testALL.length; i++) {
                    //if ($scope.testALL[i].invs058_rd_line_no1 == true) {
                    // if ($scope.temp.servicevalue == 'I' || $scope.temp.servicevalue == 'S') {

                    if ($scope.testALL[i].invs058_ordered_quantity != undefined && $scope.testALL[i].invs058_ordered_quantity != '') {
                        if ($scope.testALL[i].invs058_sup_price != undefined && $scope.testALL[i].invs058_sup_price != '') {


                            totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                            $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                            $scope.testALL[i].invs058_gross_amt = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                            $scope.testALL[i].invs058_gross_amt_vat = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;

                            totalamt_gross1 = $scope.testALL[i].invs058_gross_amt;

                            //if ($scope.testALL[i].im_item_vat_percentage != undefined || $scope.testALL[i].im_item_vat_percentage != "" || $scope.testALL[i].im_item_vat_percentage != null) {
                            //    //var finalPer = parseFloat(info.invs058_gross_amt_vat) * parseFloat(info.im_item_vat_percentage);
                            //    //var totalvat = parseFloat(finalPer) / 100;
                            //    //info.dd_item_vat_amount = parseFloat(totalvat.toFixed(3)); //parseFloat($scope.testALL[i].invs058_gross_amt_vat) + parseFloat(totalvat);
                            //    //info.invs058_gross_amt = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                            //    //info.invs058_rqvalue = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                            //    //totalamt_gross1 = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                            //    //totalamt1 = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.dd_item_vat_amount);
                            //    //vat_total_amt = vat_total_amt + $scope.testALL[i].dd_item_vat_amount;

                            //    var sell_val_final = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                            //    var totalvat = (parseFloat(sell_val_final) * $scope.testALL[i].im_item_vat_percentage) / 100;
                            //    $scope.testALL[i].dd_item_vat_amount = parseFloat(totalvat).toFixed(3);
                            //    $scope.testALL[i].invs058_gross_amt = parseFloat(sell_val_final) + parseFloat(totalvat);
                            //    $scope.testALL[i].invs058_rqvalue = parseFloat(sell_val_final) + parseFloat(totalvat);
                            //    totalamt_gross1 = parseFloat(sell_val_final) + parseFloat(totalvat);
                            //    totalamt1 = parseFloat(sell_val_final) + parseFloat(totalvat);
                            //    vatAMT = $scope.testALL[i].dd_item_vat_amount;
                            //}
                            //else {
                            //    totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                            //    $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                            //    $scope.testALL[i].invs058_gross_amt = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                            //    $scope.testALL[i].invs058_gross_amt_vat = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;

                            //    totalamt_gross1 = $scope.testALL[i].invs058_gross_amt;
                            //}

                            if ($scope.flag_iforderdiscount == "false") {
                                if ($scope.testALL[i].invs058_discount != undefined && $scope.testALL[i].invs058_discount != '') {
                                    var disamt = ((totalamt1 * $scope.testALL[i].invs058_discount) / 100);
                                    $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - disamt;
                                    totalamt1 = totalamt1 - disamt;
                                    //if (info.im_item_vat_percentage != undefined || info.im_item_vat_percentage != "" || info.im_item_vat_percentage != null) {
                                    //    dis_vat = (((totalamt1) * info.im_item_vat_percentage) / 100);
                                    //    info.dd_item_vat_amount = parseFloat(dis_vat).toFixed(3);

                                    //    totalamt1 = parseFloat(parseFloat(totalamt1) + parseFloat(info.dd_item_vat_amount)).toFixed(3);
                                    //    totalamt1 = totalamt1 - parseFloat(disamt).toFixed(3);
                                    //}
                                }
                                if ($scope.testALL[i].invs058_discount_amt != undefined && $scope.testALL[i].invs058_discount_amt != '') {
                                    var disamt = $scope.testALL[i].invs058_discount_amt;
                                    $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - $scope.testALL[i].invs058_discount_amt;
                                    totalamt1 = totalamt1 - disamt;

                                    //if (info.im_item_vat_percentage != undefined || info.im_item_vat_percentage != "" || info.im_item_vat_percentage != null) {
                                    //    dis_vat = (((totalamt1) * info.im_item_vat_percentage) / 100);
                                    //    info.dd_item_vat_amount = parseFloat(dis_vat).toFixed(3);

                                    //    totalamt1 = parseFloat(parseFloat(totalamt1) + parseFloat(info.dd_item_vat_amount)).toFixed(3);
                                    //    totalamt1 = totalamt1 - parseFloat(disamt).toFixed(3);
                                    //}

                                }
                            }
                            else {
                                $scope.testALL[i].invs058_discount = '';
                                $scope.testALL[i].invs058_discount_amt = '';
                            }

                            if (angular.isUndefined($scope.testALL[i].im_item_vat_percentage) || $scope.testALL[i].im_item_vat_percentage == "" || $scope.testALL[i].im_item_vat_percentage == null) {                            
                            }
                            else {
                                //var sell_val_final = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                var sell_val_final = $scope.testALL[i].invs058_rqvalue;
                                var totalvat = (parseFloat(sell_val_final) * $scope.testALL[i].im_item_vat_percentage) / 100;
                                var sell_val_final_gross = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                var totalvatgross = (parseFloat(sell_val_final_gross) * $scope.testALL[i].im_item_vat_percentage) / 100;


                                $scope.testALL[i].dd_item_vat_amount = parseFloat(totalvat).toFixed(3);
                                $scope.testALL[i].invs058_gross_amt = parseFloat($scope.testALL[i].invs058_gross_amt); //+ parseFloat(totalvat);
                                $scope.testALL[i].invs058_rqvalue = parseFloat($scope.testALL[i].invs058_rqvalue) + parseFloat(totalvat);
                                totalamt_gross1 = parseFloat(sell_val_final_gross); //+ parseFloat(totalvat);
                                totalamt1 = $scope.testALL[i].invs058_rqvalue;//parseFloat(sell_val_final) + parseFloat(totalvat);
                                vatAMT = $scope.testALL[i].dd_item_vat_amount;
                            }

                            totalnetamt = totalnetamt + totalamt1;
                            totalgrossamt = totalgrossamt + totalamt_gross1;
                            vat_total_amt = vat_total_amt + parseFloat(vatAMT);

                        }
                        else {
                            $scope.testALL[i].invs058_rqvalue = 0;
                            $scope.testALL[i].invs058_gross_amt = 0;
                        }
                    }
                    /// }

                    totalamt = totalamt + totalamt1;
                    $scope.total_netamt = totalnetamt.toFixed(2);
                    $scope.total_grossamt = totalgrossamt;
                    $scope.vatTotalAmt = vat_total_amt;
                    totalamt1 = 0;
                    totalamt_gross1 = 0;
                    // }
                }

                if (totalamt != NaN) {
                    $scope.totAmt = totalamt;
                    $scope.total_grossamt = totalgrossamt;
                    $scope.total_netamt = totalnetamt.toFixed(2);
                    $scope.vatTotalAmt = vat_total_amt;

                }
                else { $scope.totAmt = 0; }


            }

            $scope.orcalculateamt121 = function (str) {
                debugger;
         
                if (str == 'P') {
                    if ($scope.addDiscountper != 0 && $scope.addDiscountper != '' && $scope.addDiscountper != undefined) {
                        //$scope.flag_iforderdiscount = "true";
                        $scope.flag_iforderdiscount = "false";
                        //for (var j = 0; j < $scope.OrderAttrs.length; j++) {
                        //    if ($scope.OrderAttrs[j].or_atstatus == 'S') {
                        //        $scope.OrderAttrs[j].or_atamount = 0;
                        //    }
                        //}
                        $scope.adddisAmt = 0;
                        //if ($scope.adddisAmt != 0 && $scope.adddisAmt != '' && $scope.adddisAmt != undefined) {
                        //    $scope.adddisAmt = 0;
                        //}

                        if ($scope.addDiscountper > 100) {
                            $scope.addDiscountper = '';
                            swal({ title: "Alert", text: "Invalid Discount", width: 300, height: 200 });
                        }
                    }
                    else {
                        // $scope.adddisAmt = 0;
                        if (str == 'S') {
                            $scope.flag_iforderdiscount = "false";
                        }
                        //for (var j1 = 0; j1 < $scope.OrderAttrs.length; j1++) {
                        //    if ($scope.OrderAttrs[j1].or_atstatus == 'S') {
                        //        if ($scope.OrderAttrs[j1].or_atamount != 0 && $scope.OrderAttrs[j1].or_atamount != '' && $scope.OrderAttrs[j1].or_atamount != undefined) {

                        //        }
                        //        else {
                        //            $scope.flag_iforderdiscount = "false";
                        //        }
                        //    }
                        //}
                    }
                }

                if (str == 'S') {
                    if ($scope.adddisAmt != 0 && $scope.adddisAmt != '' && $scope.adddisAmt != undefined) {
                        //$scope.flag_iforderdiscount = "true";
                        $scope.flag_iforderdiscount = "false";
                        $scope.addDiscountper = 0;
                        //for (var j = 0; j < $scope.OrderAttrs.length; j++) {
                        //    if ($scope.OrderAttrs[j].or_atstatus == 'P') {
                        //        $scope.OrderAttrs[j].or_atamount = 0;
                        //    }
                        //}
                        //if (str == 'P') {
                        //    $scope.addDiscountper = 0;
                        //}
                    }
                    else {
                        //  $scope.addDiscountper = 0;
                        if (str == 'P') {
                            $scope.flag_iforderdiscount = "false";
                        }
                        //for (var j1 = 0; j1 < $scope.OrderAttrs.length; j1++) {
                        //    if ($scope.OrderAttrs[j1].or_atstatus == 'P') {
                        //        if ($scope.OrderAttrs[j1].or_atamount != 0 && $scope.OrderAttrs[j1].or_atamount != '' && $scope.OrderAttrs[j1].or_atamount != undefined) {

                        //        }
                        //        else {
                        //            $scope.flag_iforderdiscount = "false";
                        //        }
                        //    }
                        //}
                    }
                }


                debugger;
                //var totalamt = $scope.totAmt;
               //$scope.calamount();
                var totalamt = 0;
                var totalgrossamt = 0;
                var totalnetamt = 0;
                $scope.ord_vat_total_amount = '';
                var totalamt1 = 0;
                var totalamt_gross1 = 0;
                var dis_vat = 0;
                var vat_total_amt = 0;
                var vatAMT = 0;
                for (var i = 0; i < $scope.orderData.length; i++) {
                    //if ($scope.orderData[i].invs058_rd_line_no1 == true) {

                    if ($scope.orderData[i].od_order_qty != undefined && $scope.orderData[i].od_order_qty != '') {
                        if ($scope.orderData[i].od_supplier_price != undefined && $scope.orderData[i].od_supplier_price != '') {


                            totalamt1 = parseFloat($scope.orderData[i].od_order_qty) * parseFloat($scope.orderData[i].od_supplier_price);
                            $scope.orderData[i].netamt = $scope.orderData[i].od_order_qty * $scope.orderData[i].od_supplier_price;
                            $scope.orderData[i].grossAmt = $scope.orderData[i].od_order_qty * $scope.orderData[i].od_supplier_price;
                            $scope.orderData[i].invs058_gross_amt_vat = $scope.orderData[i].od_order_qty * $scope.orderData[i].od_supplier_price;

                            totalamt_gross1 = $scope.orderData[i].grossAmt;


                            //if ($scope.orderData[i].od_item_vat_per != undefined || $scope.orderData[i].od_item_vat_per != "" || $scope.orderData[i].od_item_vat_per != null) {
                            //    //var finalPer = parseFloat(info.invs058_gross_amt_vat) * parseFloat(info.od_item_vat_per);
                            //    //var totalvat = parseFloat(finalPer) / 100;
                            //    //info.od_item_vat_amount = parseFloat(totalvat.toFixed(3)); //parseFloat($scope.orderData[i].invs058_gross_amt_vat) + parseFloat(totalvat);
                            //    //info.grossAmt = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.od_item_vat_amount);
                            //    //info.totalNetAmt = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.od_item_vat_amount);
                            //    //totalamt_gross1 = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.od_item_vat_amount);
                            //    //totalamt1 = parseFloat(info.invs058_gross_amt_vat) + parseFloat(info.od_item_vat_amount);
                            //    //vat_total_amt = vat_total_amt + $scope.orderData[i].od_item_vat_amount;

                            //    var sell_val_final = parseFloat($scope.orderData[i].od_order_qty) * parseFloat($scope.orderData[i].od_supplier_price);
                            //    var totalvat = (parseFloat(sell_val_final) * $scope.orderData[i].od_item_vat_per) / 100;
                            //    $scope.orderData[i].od_item_vat_amount = parseFloat(totalvat).toFixed(3);
                            //    $scope.orderData[i].grossAmt = parseFloat(sell_val_final) + parseFloat(totalvat);
                            //    $scope.orderData[i].netamt = parseFloat(sell_val_final) + parseFloat(totalvat);
                            //    totalamt_gross1 = parseFloat(sell_val_final) + parseFloat(totalvat);
                            //    totalamt1 = parseFloat(sell_val_final) + parseFloat(totalvat);
                            //    vatAMT = $scope.orderData[i].od_item_vat_amount;
                            //}
                            //else {
                            //    totalamt1 = parseFloat($scope.orderData[i].od_order_qty) * parseFloat($scope.orderData[i].od_supplier_price);
                            //    $scope.orderData[i].netamt = $scope.orderData[i].od_order_qty * $scope.orderData[i].od_supplier_price;
                            //    $scope.orderData[i].grossAmt = $scope.orderData[i].od_order_qty * $scope.orderData[i].od_supplier_price;
                            //    $scope.orderData[i].invs058_gross_amt_vat = $scope.orderData[i].od_order_qty * $scope.orderData[i].od_supplier_price;

                            //    totalamt_gross1 = $scope.orderData[i].grossAmt;
                            //}

                            if ($scope.flag_iforderdiscount == "false") {
                                //if (va == 'dis'){
                                if ($scope.orderData[i].od_line_discount_pct != undefined && $scope.orderData[i].od_line_discount_pct != '') {
                                    var disamt = ((totalamt1 * parseFloat($scope.orderData[i].od_line_discount_pct)) / 100);
                                    $scope.orderData[i].netamt = $scope.orderData[i].netamt - disamt;
                                    totalamt1 = totalamt1 - disamt;
                                    //}

                                    //if (info.od_item_vat_per != undefined || info.od_item_vat_per != "" || info.od_item_vat_per != null) {
                                    //    dis_vat = (((totalamt1) * info.od_item_vat_per) / 100);
                                    //    info.od_item_vat_amount = parseFloat(dis_vat).toFixed(3);

                                    //    totalamt1 = parseFloat(parseFloat(totalamt1) + parseFloat(info.od_item_vat_amount)).toFixed(3);
                                    //    totalamt1 = totalamt1 - parseFloat(disamt).toFixed(3);
                                    //}
                                }
                                if ($scope.orderData[i].od_line_discount_amt != undefined && $scope.orderData[i].od_line_discount_amt != '') {
                                    var disamt = parseFloat($scope.orderData[i].od_line_discount_amt);
                                    $scope.orderData[i].netamt = $scope.orderData[i].netamt - $scope.orderData[i].od_line_discount_amt;
                                    totalamt1 = totalamt1 - disamt;

                                    //if (info.od_item_vat_per != undefined || info.od_item_vat_per != "" || info.od_item_vat_per != null) {
                                    //    dis_vat = (((totalamt1) * info.od_item_vat_per) / 100);
                                    //    info.od_item_vat_amount = parseFloat(dis_vat).toFixed(3);

                                    //    totalamt1 = parseFloat(parseFloat(totalamt1) + parseFloat(info.od_item_vat_amount)).toFixed(3);
                                    //    totalamt1 = totalamt1 - parseFloat(disamt).toFixed(3);
                                    //}

                                }
                            }
                            else {
                                $scope.orderData[i].od_line_discount_pct = '';
                                $scope.orderData[i].od_line_discount_amt = '';
                            }

                            if ($scope.orderData[i].od_item_vat_per != undefined || $scope.orderData[i].od_item_vat_per != "" || $scope.orderData[i].od_item_vat_per != null) {

                                var sell_val_final = parseFloat($scope.orderData[i].od_order_qty) * parseFloat($scope.orderData[i].od_supplier_price);
                                var totalvat = (parseFloat(sell_val_final) * $scope.orderData[i].od_item_vat_per) / 100;
                                $scope.orderData[i].od_item_vat_amount = parseFloat(totalvat).toFixed(3);
                                $scope.orderData[i].grossAmt = parseFloat($scope.orderData[i].grossAmt) + parseFloat(totalvat);
                                $scope.orderData[i].netamt = parseFloat($scope.orderData[i].netamt) + parseFloat(totalvat);
                                totalamt_gross1 = parseFloat(sell_val_final) + parseFloat(totalvat);
                                totalamt1 = $scope.orderData[i].netamt;//parseFloat(sell_val_final) + parseFloat(totalvat);
                                vatAMT = $scope.orderData[i].od_item_vat_amount;
                            }
                            totalnetamt = totalnetamt + totalamt1;
                            totalgrossamt = totalgrossamt + totalamt_gross1;
                            vat_total_amt = vat_total_amt + parseFloat(vatAMT);

                        }
                        else {
                            $scope.orderData[i].netamt = 0;
                            $scope.orderData[i].grossAmt = 0;
                        }
                    }


                    totalamt = totalamt + totalamt1;
                    $scope.totalNetAmt = totalnetamt.toFixed(2);
                    $scope.total_grossamt = totalgrossamt;
                    $scope.ord_vat_total_amount = vat_total_amt;
                    totalamt1 = 0;
                    totalamt_gross1 = 0;
                    // }
                }

                //if ($scope.temp['invs058_order_apply_aexp'] == true) {//if checkbox is clicked

                // for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                if (str == 'A') {
                    if ($scope.totalExpense != '' && $scope.copyOrderObject.totalExpense != $scope.totalExpense)
                        var add = angular.copy($scope.totalExpense);
                    else
                        var add = 0;

                    totalamt += (parseFloat(add));
                }

                if (str == 'P') {
                    if ($scope.addDiscountper != '' && $scope.copyOrderObject.ord_dispct != $scope.addDiscountper)
                        var percentage = angular.copy($scope.addDiscountper);
                    else
                        var percentage = 0;

                    if (parseInt(percentage) != 0)
                        totalamt = totalamt - ((parseFloat(totalamt) * parseFloat(percentage)) / 100);
                }

                if (str == 'S') {
                    if ($scope.adddisAmt != '' && $scope.copyOrderObject.ord_discount_amt != $scope.adddisAmt)
                        var percentage = angular.copy($scope.adddisAmt);
                    else
                        var percentage = 0;

                    if (parseInt(percentage) != 0)
                        totalamt = totalamt - $scope.adddisAmt;
                }
                //}
                // }

               
                $scope.totAmt = totalamt;

                //if ($scope.totAmtCopy != $scope.totAmt && (($scope.adddisAmt == "" || $scope.adddisAmt == undefined || $scope.adddisAmt == "0") && ($scope.addDiscountper == "" || $scope.addDiscountper == undefined || $scope.addDiscountper == "0") && ($scope.totalExpense == "" || $scope.totalExpense == undefined || $scope.totalExpense == "0"))) {
                //    $scope.totAmt = $scope.totAmtCopy;
                //}


            }

            $scope.orcalculateamt = function (str) {
                debugger;

                var ttval1 = str.or_atstatus;
                if (ttval1 == 'P') {
                    if (str.or_atamount != 0 && str.or_atamount != '' && str.or_atamount != undefined) {
                        $scope.flag_iforderdiscount = "true";
                        $scope.flag_iforderdiscount = "false";
                        for (var j = 0; j < $scope.OrderAttrs.length; j++) {
                            if ($scope.OrderAttrs[j].or_atstatus == 'S') {
                                $scope.OrderAttrs[j].or_atamount = 0;
                            }
                        }
                        if (str.or_atamount > 100) {
                            str.or_atamount = '';
                            swal({ title: "Alert", text: "Invalid Discount", width: 300, height: 200 });
                        }
                    }
                    else {
                        for (var j1 = 0; j1 < $scope.OrderAttrs.length; j1++) {
                            if ($scope.OrderAttrs[j1].or_atstatus == 'S') {
                                if ($scope.OrderAttrs[j1].or_atamount != 0 && $scope.OrderAttrs[j1].or_atamount != '' && $scope.OrderAttrs[j1].or_atamount != undefined) {

                                }
                                else {
                                    $scope.flag_iforderdiscount = "false";
                                    $scope.flag_iforderdiscount = "true";
                                }
                            }
                        }
                    }
                }

                if (ttval1 == 'S') {
                    if (str.or_atamount != 0 && str.or_atamount != '' && str.or_atamount != undefined) {
                        $scope.flag_iforderdiscount = "true";
                        $scope.flag_iforderdiscount = "false";
                        for (var j = 0; j < $scope.OrderAttrs.length; j++) {
                            if ($scope.OrderAttrs[j].or_atstatus == 'P') {
                                $scope.OrderAttrs[j].or_atamount = 0;
                            }
                        }
                    }
                    else {
                        for (var j1 = 0; j1 < $scope.OrderAttrs.length; j1++) {
                            if ($scope.OrderAttrs[j1].or_atstatus == 'P') {
                                if ($scope.OrderAttrs[j1].or_atamount != 0 && $scope.OrderAttrs[j1].or_atamount != '' && $scope.OrderAttrs[j1].or_atamount != undefined) {

                                }
                                else {
                                    $scope.flag_iforderdiscount = "false";
                                    $scope.flag_iforderdiscount = "true";
                                }
                            }
                        }
                    }
                }

                var totalamt = 0;
               // totalamt =  $scope.totAmt;
                var totalgrossamt = 0;
                var totalnetamt = 0;

                var totalamt1 = 0;
                var totalamt_gross1 = 0;
                var vat_total_amt = 0;
                var vatAMT = 0;
                var addExpAmt = 0;


                for (var i = 0; i < $scope.testALL.length; i++) {

                        if ($scope.testALL[i].invs058_ordered_quantity != undefined && $scope.testALL[i].invs058_ordered_quantity != '') {
                            if ($scope.testALL[i].invs058_sup_price != undefined && $scope.testALL[i].invs058_sup_price != '') {

                                totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                $scope.testALL[i].invs058_gross_amt = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                $scope.testALL[i].invs058_gross_amt_vat = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;

                                totalamt_gross1 = $scope.testALL[i].invs058_gross_amt;


                                if ($scope.flag_iforderdiscount == "false") {
                                    if ($scope.testALL[i].invs058_discount != undefined && $scope.testALL[i].invs058_discount != '') {
                                        var disamt = ((totalamt1 * $scope.testALL[i].invs058_discount) / 100);
                                        $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - disamt;
                                        totalamt1 = totalamt1 - disamt;
                                    }
                                    if ($scope.testALL[i].invs058_discount_amt != undefined && $scope.testALL[i].invs058_discount_amt != '') {
                                        var disamt = $scope.testALL[i].invs058_discount_amt;
                                        $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - $scope.testALL[i].invs058_discount_amt;
                                        totalamt1 = totalamt1 - disamt;
                                    }
                                }
                                //else {
                                //    $scope.testALL[i].invs058_discount = '';
                                //    $scope.testALL[i].invs058_discount_amt = '';
                                //}


                                if ($scope.testALL[i].im_item_vat_percentage != undefined || $scope.testALL[i].im_item_vat_percentage != "" || $scope.testALL[i].im_item_vat_percentage != null) {
                                  
                                    var sell_val_final = $scope.testALL[i].invs058_rqvalue; //parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                    var totalvat = (parseFloat(sell_val_final) * $scope.testALL[i].im_item_vat_percentage) / 100;

                                    var sell_val_final_gross = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                    var totalvatgross = (parseFloat(sell_val_final_gross) * $scope.testALL[i].im_item_vat_percentage) / 100;

                                    $scope.testALL[i].dd_item_vat_amount = parseFloat(totalvat).toFixed(3);
                                    $scope.testALL[i].invs058_gross_amt = parseFloat($scope.testALL[i].invs058_gross_amt); //+ parseFloat(totalvatgross);
                                    $scope.testALL[i].invs058_rqvalue = parseFloat($scope.testALL[i].invs058_rqvalue) + parseFloat(totalvat);
                                    totalamt_gross1 = parseFloat(sell_val_final_gross); //+ parseFloat(totalvatgross);
                                    totalamt1 = $scope.testALL[i].invs058_rqvalue;//parseFloat(sell_val_final) + parseFloat(totalvat);
                                    vatAMT = $scope.testALL[i].dd_item_vat_amount;
                                }

                                totalnetamt = totalnetamt + totalamt1;
                                totalgrossamt = totalgrossamt + totalamt_gross1;
                                vat_total_amt = vat_total_amt + parseFloat(vatAMT);
                            }
                            else {
                                $scope.testALL[i].invs058_rqvalue = 0;
                                $scope.testALL[i].invs058_gross_amt = 0;
                            }
                        }
                    
                    totalamt = totalamt + totalamt1;
                    $scope.total_netamt = totalnetamt;
                    $scope.vatTotalAmt = vat_total_amt;
                    // totalamt = totalnetamt;
                    $scope.total_grossamt = totalgrossamt;
                    totalamt1 = 0;
                    totalamt_gross1 = 0;

                }

                //for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                //    if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                //        if ($scope.OrderAttrs[i].or_atamount == '' || $scope.OrderAttrs[i].or_atamount == undefined || $scope.OrderAttrs[i].or_atamount == 0)
                //            var add = 0;
                //        else
                //            var add = angular.copy($scope.OrderAttrs[i].or_atamount);

                //        totalamt += (parseFloat(add));
                //    }

                //    if ($scope.OrderAttrs[i].or_atstatus == 'P') {

                //        if ($scope.OrderAttrs[i].or_atamount == '' || $scope.OrderAttrs[i].or_atamount == undefined || $scope.OrderAttrs[i].or_atamount == 0)
                //            var percentage = 0;
                //        else
                //            var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);

                //        if (parseInt(percentage) != 0)
                //            totalamt = totalamt - ((parseFloat(totalamt) * parseFloat(percentage)) / 100);
                //    }

                //    if ($scope.OrderAttrs[i].or_atstatus == 'S') {
                //        if ($scope.OrderAttrs[i].or_atamount == '' || $scope.OrderAttrs[i].or_atamount == undefined || $scope.OrderAttrs[i].or_atamount == 0)
                //            var percentage = 0;
                //        else
                //            var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);

                //        if (parseInt(percentage) != 0)
                //            totalamt = totalamt - $scope.OrderAttrs[i].or_atamount;
                //    }
                //}

                debugger;
                //if ($scope.temp['invs058_order_apply_aexp'] == true) {//if checkbox is clicked

                    //for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if (str.or_atstatus == 'A') {
                            if (str.or_atamount == '' || str.or_atamount == undefined || str.or_atamount == 0)
                                var add = 0;                                
                            else
                                var add = angular.copy(str.or_atamount);

                            totalamt += (parseFloat(add));
                        }

                        if (str.or_atstatus == 'P') {

                            if (str.or_atamount == '' || str.or_atamount == undefined || str.or_atamount == 0)
                                var percentage = 0;                               
                            else
                                var percentage = angular.copy(str.or_atamount);

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - ((parseFloat(totalamt) * parseFloat(percentage)) / 100);
                        }

                        if (str.or_atstatus == 'S') {
                            if (str.or_atamount == '' || str.or_atamount == undefined || str.or_atamount == 0)
                                var percentage = 0;                                
                            else
                                var percentage = angular.copy(str.or_atamount);
                                
                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - str.or_atamount;
                        }
                    //}
                //}

                //if ($scope.temp['invs058_order_apply_aexp'] == false) {//if checkbox is not clicked

                //    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                //        if ($scope.OrderAttrs[i].or_atstatus == 'P') {
                //            if ($scope.OrderAttrs[i].or_atamount != '')
                //                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                //            else
                //                var percentage = 0;

                //            if (parseInt(percentage) != 0)
                //                totalamt = totalamt - ((parseFloat(totalamt) * parseFloat(percentage)) / 100);
                //        }

                //        if ($scope.OrderAttrs[i].or_atstatus == 'S') {
                //            if ($scope.OrderAttrs[i].or_atamount != '')
                //                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                //            else
                //                var percentage = 0;

                //            if (parseInt(percentage) != 0)
                //                totalamt = totalamt - $scope.OrderAttrs[i].or_atamount;
                //        }


                //        //if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                //        //    if ($scope.OrderAttrs[i].or_atamount != '')
                //        //        var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                //        //    else
                //        //        var add = 0;

                //        //    totalamt += (parseFloat(add));
                //        //}

                //    }

                //    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                //        if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                //            if ($scope.OrderAttrs[i].or_atamount != '')
                //                var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                //            else
                //                var add = 0;

                //            totalamt += (parseFloat(add));
                //        }
                //    }
                //}


                $scope.totAmt = totalamt;


            }

            $scope.cancel_modal = function () {
                $scope.testALL = [];

                $scope.totalNetAmt = '';
                $scope.ord_vat_total_amount = '';
                $scope.orderdisamt = '';
                $scope.totalExpense = '';
                $scope.addDiscountper = '';
                $scope.adddisAmt = '';
                $scope.totalGrossAmt = '';
                $scope.ord_no = '';
                $scope.total_grossamt = '';
                $scope.totAmt = '';
                $('#EditOrderModal').modal('hide');
            };

            $scope.removeItem = function (obj, index) {
                debugger;
                $scope.orderData.splice(index, 1);
                $scope.orcalculateamt12();
            }

        

            //DATA SAVE INSERT
            var datasend = [];
            var datasend1 = [];
            var datasend2 = [];
            var gofurther = true;
            var data = {};
            $scope.savedata = function () {
                debugger;
                for (var i = 0; i < $scope.testALL.length; i++) {
                    if ($scope.testALL[i].invs058_ordered_quantity == undefined || $scope.testALL[i].invs058_sup_price == undefined) {
                        gofurther = false;
                        swal({ title: "Alert", text: "Please enter Ordered Quantity/Supplier Price", width: 300, height: 200 });
                        gofurther = true;
                        return;
                    }
                }

                if (gofurther == true) {

                    for (var i = 0; i < $scope.testALL.length; i++) {
                        data = {
                            'opr': 'U1',
                            'order_d_req_no': $scope.testALL[i].req_no,
                            'order_d_rd_line_no': $scope.testALL[i].od_line_no,
                            'order_d_im_inv_no': $scope.testALL[i].im_inv_no,
                            'order_d_od_order_qty': $scope.testALL[i].invs058_ordered_quantity,
                            'order_d_od_supplier_price': $scope.testALL[i].invs058_sup_price,
                            'order_d_od_discount_pct': $scope.testALL[i].invs058_discount,
                            //'order_d_od_shipped_qty': $scope.testALL[i].order_d_od_shipped_qty,
                            //'order_d_od_received_qty': $scope.testALL[i].order_d_od_received_qty,
                            //'order_d_od_orig_line_no': $scope.testALL[i].order_d_od_orig_line_no,
                            //'order_d_od_status': $scope.testALL[i].order_d_od_status,
                            'order_d_od_remarks': $scope.testALL[i].od_remarks,
                            'invs058_discount_amt': $scope.testALL[i].invs058_discount_amt,
                            'od_item_vat_per': $scope.testALL[i].im_item_vat_percentage,
                            'od_item_vat_amount': $scope.testALL[i].dd_item_vat_amount,
                            'ord_no': $scope.testALL[i].ord_no,
                        }
                        datasend.push(data);
                    }


                    var ord_addexp = 0;
                    var ord_discount_pct = 0;
                    var ord_discount_amt = 0;

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {
                        if ($scope.OrderAttrs[i].or_atamount != 0 && $scope.OrderAttrs[i].or_atamount != undefined && $scope.OrderAttrs[i].or_atamount != '') {
                            if ($scope.OrderAttrs[i].or_atstatus == 'A')
                                ord_addexp = $scope.OrderAttrs[i].or_atamount;
                            if ($scope.OrderAttrs[i].or_atstatus == 'P')
                                ord_discount_pct = $scope.OrderAttrs[i].or_atamount;
                            if ($scope.OrderAttrs[i].or_atstatus == 'S')
                                ord_discount_amt = $scope.OrderAttrs[i].or_atamount;
                        }
                    }


                    $scope.data1 = {
                        'opr': 'U2',
                        //'order_ord_date': $scope.temp.order_date,
                        //'order_dep_code': $scope.temp.dep_code,
                        'order_sup_code': $scope.temp.sup_code,
                        'order_cur_code': $scope.temp.excg_curcy_code,
                        'order_dm_code': $scope.temp.dm_code,
                        'order_ord_shipment_date': $scope.temp.expected_date,
                        'order_pm_code': $scope.temp.pm_code,
                        'order_trt_code': $scope.temp.trt_code,
                        'order_fa_code': $scope.temp.fa_code,
                        //'order_lc_no': $scope.temp.lc_no,
                        'order_ord_supl_quot_ref': $scope.temp.supplier_qty_ref,
                        'sims_transport_vehicle_code': $scope.temp.sims_transport_vehicle_code,
                        //'invs058_req_type': $scope.temp.servicevalue,
                        //'randomnumber': Math.floor((Math.random() * 100) + 1),
                        //'invs058_order_createdby': $rootScope.globals.currentUser.username,
                        'invs058_heading': $scope.temp.invs058_heading,
                        'invs058_notes': $scope.temp.invs058_notes,
                        //'invs058_order_apply_aexp': ifapplyexp,


                        'invs058_ord_addexps': ord_addexp,
                        'invs058_ord_dispct': ord_discount_pct,
                        'invs058_ord_disamt': ord_discount_amt,
                        'ord_vat_total_amount': $scope.vatTotalAmt, //$scope.ord_vat_total_amount,
                        'ord_no': $scope.ord_no
                    }

                    swal({
                        title: '',
                        text: "Do you want to Edit Purchase Order?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                        allowOutsideClick: false,
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            if (datasend.length > 0) {

                                $http.post(ENV.apiUrl + "api/CreateOrder/UpdateOrderDetails_VAT?data1=" + JSON.stringify($scope.data1), datasend).then(function (msg) {
                                    $scope.msg1 = msg.data;

                                    if ($scope.msg1 == true) {
                                        swal({ text: "Purchase order updated successfully.", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Purchase order not updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                    }
                                    else {
                                        swal({ text: $scope.msg1, imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                    }
                                    
                                    $scope.SelectIetm();
                                    datasend = [];
                                    $scope.selected_reqtype = '';
                                    $scope.testALL = [];
                                    //$scope.temp.servicevalue = 'I'
                                    $scope.flag_iforderdiscount = "false";
                                    datasend1 = [];
                                    datasend2 = [];
                                    $scope.totAmt = 0;
                                    $scope.table = true;
                                    $scope.display = false;
                                    $scope.cancel_modal();
                                });
                            }
                            else {
                                swal({ text: "Please Select Items to Create Order.", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                            }
                        }
                        else {
                            debugger;
                            $scope.cancel_modal();
                            $scope.SelectIetm();
                            datasend = [];
                            $scope.selected_reqtype = '';
                            $scope.testALL = [];
                            //$scope.temp.servicevalue = 'I'
                            $scope.flag_iforderdiscount = "false";
                            datasend1 = [];
                            datasend2 = [];
                            $scope.totAmt = 0;
                            $scope.table = true;
                            $scope.display = false;
                        }
                    }
               );

                }

                else {
                    swal({ title: "Alert", text: "Please Select Mandatory Fields", width: 300, height: 200 });
                }
            }

        }])

})();
