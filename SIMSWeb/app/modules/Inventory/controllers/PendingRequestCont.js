﻿(function () {
    'use strict';
    var obj1, temp, opr, CurrentDate, comp;
    var main, deletefin = [], data = [], data1 = [], ItemDetails = [];
    var finanacecode = [];
    var check;
    var reqno;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PendingRequestCont',
         ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {
             $scope.priview_table = false;
             $scope.pagesize = "5";
             $scope.pageindex = 0;
             $scope.search_Req_Data = [];
             $scope.Main_table = false;
             $scope.Main_table_Service = false;
             $scope.temp = [];
             $scope.temp2 = [];

             $timeout(function () {
                 $("#fixTable").tableHeadFixer({ 'top': 1 });
             }, 100);


             $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

             $scope.makeTodos = function () {
                 var rem = parseInt($scope.totalItems % $scope.numPerPage);
                 if (rem == '0') {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                 }
                 else {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                 }

                 var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                 var end = parseInt(begin) + parseInt($scope.numPerPage);


                 $scope.filteredTodos = $scope.todos.slice(begin, end);
             };

             $http.get(ENV.apiUrl + "api/RequestDetail/getRequestTypeNew").then(function (reqtypedetailsNew) {
                 $scope.req_type_detailsNew = reqtypedetailsNew.data;
                 $scope.temp['request_mode_code'] = $scope.req_type_detailsNew[0].request_mode_code;
                 if ($scope.temp.request_mode_code == 'S') {
                     $scope.hide_itemne = true;
                     $scope.temp.reqQty = 0;
                 }
                 else {
                     $scope.hide_itemne = false;
                 }
             });

             $http.get(ENV.apiUrl + "api/RequestDetail/getDeparments").then(function (deptdata) {
                 $scope.dept_data = deptdata.data;
                 //$scope.temp['dept_code'] = $scope.dept_data[0].dept_code;
             });

             $http.get(ENV.apiUrl + "api/RequestDetail/getSalesmanType?user_code=" + $rootScope.globals.currentUser.username).then(function (salesmantype) {

                 $scope.salesman_type = salesmantype.data;
                 $scope.temp['salesman_code'] = $scope.salesman_type[0].salesman_code;

             });

             var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');
             $scope.dt = { req_date: dateyear }

             $scope.SearchRequest = function () {
                 $http.get(ENV.apiUrl + "api/RequestDetail/getPendingRequest?sr=" + $scope.temp.request_mode_code + "&user_code=" + $scope.temp.salesman_code + "&dept_code=" + $scope.temp.dept_code).then(function (searchReqData) {

                     $scope.search_Req_Data = searchReqData.data;
                     $scope.totalItems = $scope.search_Req_Data.length;
                     $scope.todos = $scope.search_Req_Data;
                     $scope.makeTodos();

                     if ($scope.temp.request_mode_code == 'I') {
                         if ($scope.search_Req_Data.length > 0) {
                             $scope.Main_table = true;
                             $scope.Main_table_Service = false;
                             $scope.Grid = true;
                         }
                         else {
                             swal({ text: "There are no Pending Requests", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                         }
                     }

                     if ($scope.temp.request_mode_code == 'S') {
                         if ($scope.search_Req_Data.length > 0) {
                             $scope.Main_table_Service = true;
                             $scope.Main_table = false;
                             $scope.Grid = true;
                         }
                         else {
                             swal({ text: "There are no Pending Requests", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                         }
                     }

                 });
             }


             $scope.searched = function (valLists, toSearch) {
                 return _.filter(valLists,

                 function (i) {
                     /* Search Text in all  fields */
                     return searchUtil(i, toSearch);
                 });
             };

             //Search
             $scope.search = function () {
                 $scope.todos = $scope.searched($scope.getParameter, $scope.searchText);
                 $scope.totalItems = $scope.todos.length;
                 $scope.currentPage = '1';
                 if ($scope.searchText == '') {
                     $scope.todos = $scope.getParameter;
                 }
                 $scope.makeTodos();
                 main.checked = false;
                 $scope.CheckAllChecked();
             }

             function searchUtil(item, toSearch) {
                 /* Search Text in all 3 fields */
                 return (item.invs_appl_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.invs_appl_form_field.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.invs_appl_parameter.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.invs_appl_form_field_value1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.acno == toSearch) ? true : false;
             }

             $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                 $('input[type="text"]', $(this).parent()).focus();
             });

             $('*[data-datepicker="true"] input[type="text"]').datepicker({
                 todayBtn: true,
                 orientation: "top left",
                 autoclose: true,
                 todayHighlight: true,
                 format: 'dd-mm-yyyy'
             });

         }])

})();
