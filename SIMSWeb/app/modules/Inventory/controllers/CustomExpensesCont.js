﻿(function () {
    'use strict';
    var opr = '';
    var customcode = [];
    var main;
    var data1 = [];
    var data = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CustomExpensesCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.table1 = true;
            $scope.operation = false;
            $scope.editmode = false;
            $scope.showShipmentDetails = false;

            $http.get(ENV.apiUrl + "api/CustomExpenses/getCustomExpensesDetails").then(function (getCustomExpensesDetails_Data) {
                $scope.CustomExpenses_Data = getCustomExpensesDetails_Data.data;
                $scope.totalItems = $scope.CustomExpenses_Data.length;
                $scope.todos = $scope.CustomExpenses_Data;
                $scope.makeTodos();

            });

            $http.get(ENV.apiUrl + "api/CustomExpenses/getReg_noFromClearance").then(function (getCCl_Reg_no) {
                $scope.CCl_Reg_no_data = getCCl_Reg_no.data;
            });

            $http.get(ENV.apiUrl + "api/common/getCCNoFromCCheques").then(function (getCCNoFromCCheques_Data) {
                $scope.CCNoFromCCheques = getCCNoFromCCheques_Data.data;
            });

            $http.get(ENV.apiUrl + "api/common/getPetDesc").then(function (getPetDesc_Data) {
                $scope.PetDesc_Data = getPetDesc_Data.data;
            });

            $scope.getShipmentDetails = function (shipment_id) {
                $scope.shipmentDetails = [];
                $http.get(ENV.apiUrl + "api/CustomExpenses/getShipmentDetails?shipment_id=" + shipment_id).then(function (res) {                    
                    if (res.data.length > 0) {
                        $scope.shipmentDetails = res.data;
                        $scope.showShipmentDetails = true;
                    }
                    console.log("shipmentDetails", $scope.shipmentDetails);                    
                });
            }
            
            $scope.size = function (str) {

                if (str == "All" || str == "all") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.CustomExpenses_Data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
                // $scope.CheckAllChecked();
            }

            $scope.New = function () {
                $scope.edt = "";
                $scope.edt = {};
                $scope.check = true;
                $scope.editmode = false;
                $scope.newmode = true;
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.edt = {
                    ce_ref_date: dd + '-' + mm + '-' + yyyy
                }
                $scope.showShipmentDetails = false;
            }

            $scope.up = function (str) {

                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.readonly = true;
                $scope.showShipmentDetails = false;

                $scope.edt = {

                    ccl_reg_no: str.ccl_reg_no,
                    pet_code: str.pet_code,
                    pet_desc: str.pet_desc,
                    ce_ref_no: str.ce_ref_no,
                    ce_amount: str.ce_amount,
                    ce_ref_date: str.ce_ref_date,
                    old_pet_code: str.pet_code,
                    cc_no: str.cc_no

                }
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.showShipmentDetails = false;
            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    data = [];
                    data1 = [];
                    data = $scope.edt;
                    data.opr = 'I';
                    $scope.exist = false;
                    for (var i = 0; i < $scope.CustomExpenses_Data.length; i++) {
                        if ($scope.CustomExpenses_Data[i].ccl_reg_no == data.ccl_reg_no &&
                            $scope.CustomExpenses_Data[i].pet_code == data.pet_code) {
                            $scope.exist = true;
                        }

                    }
                    if ($scope.exist) {
                        swal({ text: "Record Already Exists", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }
                    else {
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/CustomExpenses/CustomExpensesCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Inserted. " + $scope.msg1, imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                            $http.get(ENV.apiUrl + "api/CustomExpenses/getCustomExpensesDetails").then(function (getCustomExpensesDetails_Data) {
                                $scope.CustomExpenses_Data = getCustomExpensesDetails_Data.data;
                                $scope.totalItems = $scope.CustomExpenses_Data.length;
                                $scope.todos = $scope.CustomExpenses_Data;
                                $scope.makeTodos();
                            });
                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                }

            }

            $scope.Update = function (myForm) {
                if (myForm) {
                    data = [];
                    data1 = [];
                    data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/CustomExpenses/CustomExpensesCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $http.get(ENV.apiUrl + "api/CustomExpenses/getCustomExpensesDetails").then(function (getCustomExpensesDetails_Data) {
                            $scope.CustomExpenses_Data = getCustomExpensesDetails_Data.data;
                            $scope.totalItems = $scope.CustomExpenses_Data.length;
                            $scope.todos = $scope.CustomExpenses_Data;
                            $scope.makeTodos();
                        });
                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                }

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].ccl_reg_no + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].ccl_reg_no + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.deleterecord = function () {
                customcode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].ccl_reg_no + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletecode = ({
                            'ccl_reg_no': $scope.filteredTodos[i].ccl_reg_no,
                            'pet_code': $scope.filteredTodos[i].pet_code,
                            opr: 'D'
                        });
                        customcode.push(deletecode);
                    }
                }
                debugger
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/CustomExpenses/CustomExpensesCUD", customcode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/CustomExpenses/getCustomExpensesDetails").then(function (getCustomExpensesDetails_Data) {
                                                $scope.CustomExpenses_Data = getCustomExpensesDetails_Data.data;
                                                $scope.totalItems = $scope.CustomExpenses_Data.length;
                                                $scope.todos = $scope.CustomExpenses_Data;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/CustomExpenses/getCustomExpensesDetails").then(function (getCustomExpensesDetails_Data) {
                                                $scope.CustomExpenses_Data = getCustomExpensesDetails_Data.data;
                                                $scope.totalItems = $scope.CustomExpenses_Data.length;
                                                $scope.todos = $scope.CustomExpenses_Data;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].ccl_reg_no + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }

                $scope.currentPage = true;
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.CustomExpenses_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CustomExpenses_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                return (
                     item.pet_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.pet_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.cc_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.ce_ref_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.ce_amount.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.ccl_reg_no == toSearch) ? true : false;


            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.edt = {
                ce_ref_date: dd + '-' + mm + '-' + yyyy
            }


        }])
})();





