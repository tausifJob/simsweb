﻿(function () {
    'use strict';
    var del = [];
    var main;
    var date1;
    var date3;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SalesDocumentVATCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.disable_btn = true;

            $scope.disable_btn1 = true;
            $scope.save_btn = true;
            $scope.grade_code = '';
            $scope.academic_year = '';
            $scope.rcbDocType = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.showPickInvoice = false;
            $scope.old_doc_prov_no_show = false;
            var round_diff_val = 0;
            $scope.temp1 = {
                im_inv_no: undefined, invs021_sg_name: undefined, im_item_code: undefined, im_desc: undefined, invs021_dep_code: undefined, sec_code: undefined, invs021_sup_code: undefined, im_assembly_ind_s: undefined, category_code: undefined, subcategory_code: undefined

            };
            //$scope.pagesize = "5";
            // $scope.pageindex = "1";
            $scope.itemsPerPage = '5';
            $scope.currentPage = 0;
            $scope.obj = [];
            $scope.itemList = [];
            $scope.temp = {};
            $scope.paymodeList = [];
            $scope.disable_total_dis_per = false;
            $scope.edt = {};
            $scope.disableOnInvoiceReturn = false;
            $scope.showBtnOnInvoiceReturn = false;
            $scope.disableReturnBtn = true;
            $scope.disableSaveReturnBtn = true;

            $("#prov_date").kendoDatePicker({
                format: "dd-MM-yyyy",
                min: new Date(1900, 1, 1),
                max: new Date()
            });


            if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                $scope.disable_sale = true;
            }

            setTimeout(function () {
                $("#rcbItem").select2();

            }, 100);

            var today = new Date();
            //var dd = today.getDate();
            //var mm = today.getMonth() + 1; //January is 0!
            //var yyyy = today.getFullYear();

            //if (dd < 10) {
            //    dd = '0' + dd
            //}

            //if (mm < 10) {
            //    mm = '0' + mm
            //}

            ////today = yyyy + '-' + mm + '-' + dd;
            //today = dd + '-' + mm + '-' + yyyy;
            today = ('0' + today.getDate()).slice(-2) + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + today.getFullYear();
            $scope.tdate = today;

            $scope.temp['provDate'] = today
            //$scope.paymodeList = [
            //    { name: 'Cash', value: 'CS' },
            //     { name: 'Cheque', value: 'CH' },
            //      { name: 'Bank Transfer', value: 'BT' },
            //        { name: 'Card Payment', value: 'CP' },
            //]

            $scope.temp['paymentMode'] = 'CA';

            document.getElementById("rcb_saletype").focus();

            $scope.size = function (pagesize) {
                $scope.itemsPerPage = pagesize;

            }
            $scope.Scroll = function () {
                var element = document.getElementById('table_scroll');
                element.scrollTop = element.scrollHeight - element.clientHeight;
            }

            $http.get(ENV.apiUrl + "api/StudentFee/GetPaymentMode").then(function (res) {
                $scope.paymodeList = res.data;
                $scope.temp.paymentMode = res.data[0].pmDescShort;
                console.log($scope.paymodeList);
            });

            $scope.enroll_change = function (str) {
                debugger;
                if (str != '') {
                    if ($scope.temp.sal_type == '05') {

                        $scope.inCustFlg = true;

                        for (var i = 0; i < $scope.rcbInternal.length; i++) {
                            if (str == $scope.rcbInternal[i].up_name) {
                                $scope.internalUser = $scope.rcbInternal[i].up_num;
                                $scope.inCustFlg = false;
                                break;
                            }
                        }

                        if ($scope.inCustFlg) {
                            $http.post(ENV.apiUrl + "api/Sales_Doc/Insert_User_profiles?name=" + str).then(function (res) {

                                $scope.internalUser = res.data;
                                console.log("internalUser", $scope.internalUser);

                                //  $scope.rcbCategories = res.data;
                                //  if(res.data.length>0)
                                //   $scope.temp['name'] = res.data.studentname;

                            });
                        }

                    }
                    else if ($scope.temp.sal_type == '04') {
                        $http.get(ENV.apiUrl + "api/Sales_Doc/GetStudentInfo?enrollnum=" + str).then(function (res) {

                            //  $scope.rcbCategories = res.data;
                            if (res.data.studentname != undefined) {
                                $scope.temp['name'] = res.data.studentname + ' Class:' + res.data.grade_name + '-' + res.data.section_name;
                                $scope.grade_code = res.data.grade_code;
                            }
                            else {
                                swal({ title: "Alert", text: "Please Enter Correct Student ID.", });
                                $scope.disable_btn = true;
                                $scope.disable_btn1 = true;
                                $scope.showBtnOnInvoiceReturn = false;
                                $scope.grade_code = '';

                            }

                        });

                    }

                    else if ($scope.temp.sal_type == '03') {
                        $http.get(ENV.apiUrl + "api/Sales_Doc/GetEmployeeInfo?enrollnum=" + str).then(function (res) {

                            //  $scope.rcbCategories = res.data;
                            if (res.data.studentname != undefined)
                                $scope.temp['name'] = res.data.studentname;
                            else {
                                swal({ title: "Alert", text: "Please Enter Correct Employee ID.", });
                                $scope.disable_btn = true;
                                $scope.disable_btn1 = true;
                                $scope.showBtnOnInvoiceReturn = false;

                            }
                        });

                    }

                    if (str == '' || str == undefined) {
                    }
                    else {
                        $scope.disable_btn = false;
                        $scope.disable_btn1 = false;
                        //$scope.showBtnOnInvoiceReturn = false;

                    }
                }
            }

            //$http.get(ENV.apiUrl + "api/Sales_Doc/GetAllItemsInSubCategoryNew?pc_code=" + '').then(function (res) {

            //    $scope.rcbItems = res.data;

            //    $scope.rcbitemOld = angular.copy(res.data);

            //});

            $http.get(ENV.apiUrl + "api/Sales_Doc/GetSalesmanName?sales_code=" + $rootScope.globals.currentUser.username).then(function (res) {

                $scope.salesman = res.data[0];
                $scope.salesmanDeptCode = res.data[0].dep_code;
                console.log("salesman", $scope.salesman, "salesmanDeptCode", $scope.salesmanDeptCode);

                $http.get(ENV.apiUrl + "api/Sales_Doc/GetAllItemsInSubCategoryByDepartment?pc_code=" + '' + "&dept_code=" + $scope.salesmanDeptCode).then(function (res) {
                    $scope.rcbItems = res.data;
                    $scope.rcbitemOld = angular.copy(res.data);
                });

            });

            
            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;


            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.obj.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;
            };

            //$http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
            //    $scope.ComboBoxValues = AllComboBoxValues.data;
            //});

            $scope.searchstudent = function () {
                $scope.busy = true;
                $scope.searchtable = false;
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.temp)).then(function (Allstudent) {

                    $scope.student = Allstudent.data;

                    $scope.busy = false;

                    $scope.searchtable = true;
                });
            }

            $scope.GetSalesTypeChange = function (str) {
                $scope.itemList = [];
                //$scope.temp = {};
                $scope.searchDisable = false;
                $scope.rcbInternal = [];
                $scope.temp['enroll'] = '';
                $scope.temp['name'] = '';
                $scope.disable_btn = true;
                $scope.disable_btn1 = true;
                $scope.showBtnOnInvoiceReturn = false;
                $scope.grade_code = '';

                if (str == '03') {   //staff

                    $rootScope.visible_stud = false;
                    $rootScope.visible_parent = false;
                    $rootScope.visible_search_parent = false;
                    $rootScope.visible_teacher = false;
                    $rootScope.visible_User = false;
                    $rootScope.visible_Employee = true;
                    $rootScope.chkMulti = false;
                    $scope.chk_internal = false;
                    //for (var i = 0; i < $scope.rcbDocType.length; i++) {

                    //    //if ($scope.rcbDocType[i].dt_code == '02') {
                    //    //    $scope.chk_internal = true;
                    //    //}
                    //}
                    //if (!$scope.chk_internal)
                    //    $scope.rcbDocType.push($scope.internal_tars);

                }
                else if (str == '04') { //student

                    $rootScope.visible_stud = true;
                    $rootScope.visible_parent = false;
                    $rootScope.visible_search_parent = false;
                    $rootScope.visible_teacher = false;
                    $rootScope.visible_User = false;
                    $rootScope.visible_Employee = false;

                    $rootScope.chkMulti = false;

                    //for (var i = 0; i < $scope.rcbDocType.length; i++) {

                    //    if ($scope.rcbDocType[i].dt_code == '02') {
                    //        $scope.internal_tars = angular.copy($scope.rcbDocType[i]);
                    //        $scope.rcbDocType.splice(i, 1);
                    //    }
                    //}
                }
                else if (str == '05') {
                    //$scope.searchDisable = true

                    $http.get(ENV.apiUrl + "api/Sales_Doc/GetUserProfiles").then(function (res) {

                        $scope.rcbInternal = res.data;
                        // console.log(res.data[0])
                    });


                    $http.get(ENV.apiUrl + "api/Sales_Doc/Get_Subledger_Master").then(function (res) {

                        $scope.sublegerData = res.data;
                        console.log("sublegerData", $scope.sublegerData);
                    });
                    //for (var i = 0; i < $scope.rcbDocType.length; i++) {

                    //    if ($scope.rcbDocType[i].dt_code == '02') {
                    //        $scope.internal_tars = angular.copy($scope.rcbDocType[i]);
                    //        $scope.rcbDocType.splice(i, 1);
                    //    }
                    //}
                }
            }

            $scope.$on('global_cancel', function (str) {
                $scope.grade_code='';

                if ($scope.SelectedUserLst.length > 0) {

                    if ($scope.temp.sal_type == '03') {
                        $scope.temp['enroll'] = $scope.SelectedUserLst[0].em_login_code;
                        $scope.temp['name'] = $scope.SelectedUserLst[0].empName;

                    }
                    else if ($scope.temp.sal_type == '04') {
                        $scope.temp['enroll'] = $scope.SelectedUserLst[0].s_enroll_no;
                        $scope.temp['name'] = $scope.SelectedUserLst[0].name + ' Class: ' + $scope.SelectedUserLst[0].s_class;
                        $scope.grade_code = $scope.SelectedUserLst[0].grade_code;
                    }
                }

                if ($scope.temp.enroll == '' || $scope.temp.enroll == undefined) {
                }
                else {
                    $scope.disable_btn = false;
                    $scope.disable_btn1 = false;

                }
                // $scope.getstudentList();
            });

            $(window).bind('keydown', function (event) {

                if (event.ctrlKey || event.metaKey) {
                    switch (String.fromCharCode(event.which).toLowerCase()) {
                        case 'p':
                            event.preventDefault();
                            $scope.print('tableIDDiv');

                            break;


                    }
                }
            });

            $scope.getCheckedLeger = function (obj) {
                debugger;
                if (obj.leger_chk == true) {
                    $scope.temp['enroll'] = obj.slma_acno;
                    $scope.temp['name'] = obj.coad_pty_full_name;
                    $scope.internalUser = $scope.temp['enroll'];
                    obj.leger_chk = false;
                    $('#ExternalCustomerSearch').modal('hide');
                }
                if ($scope.temp.enroll == '' || $scope.temp.enroll == undefined) {
                }
                else {
                    $scope.disable_btn = false;
                    $scope.disable_btn1 = false;

                }
            };


            $scope.edit = function (info) {
                $scope.temp['enroll'] = info.s_enroll_no;
                //  $scope.SearchStudentData();
            }

            $http.get(ENV.apiUrl + "api/Sales_Doc/get_supplier_code").then(function (res) {

                $scope.cmb_supplier = res.data;
            });

            $http.get(ENV.apiUrl + "api/Sales_Doc/GetAllSalesType").then(function (res) {

                $scope.rcbSalesType = res.data;
                if ($scope.rcbSalesType.length > 0) {
                    $scope.temp['sal_type'] = '04';   //res.data[2].sal_type; for student;
                    $scope.GetSalesTypeChange($scope.temp['sal_type']);
                }

            });

            $http.get(ENV.apiUrl + "api/Sales_Doc/GetAllDocumentType").then(function (res) {


                $scope.rcbDocType = res.data;
                if ($scope.rcbDocType.length > 0) {
                  //  $scope.temp['doc_type'] = res.data[0].dt_code
                    
                    $scope.temp.doc_type = "01";

                    //for (var i = 0; i < $scope.rcbDocType.length; i++) {

                    //    if ($scope.rcbDocType[i].dt_code == '02') {
                    //        $scope.internal_tars = angular.copy($scope.rcbDocType[i]);
                    //        $scope.rcbDocType.splice(i, 1);
                    //    }
                    //}
                }
            });

            $scope.showInvoice = function () {
                // for Sales invoice return
               // $scope.cancel();
                $scope.showPickInvoice = false;
                if ($scope.temp.doc_type == "02") {
                    $scope.showPickInvoice = true;
                }
                else {
                    $scope.cancel();
                }
            }

            $http.get(ENV.apiUrl + "api/Sales_Doc/GetAllSupplierGroupName").then(function (res) {

                $scope.rcbGroup = res.data;

            });

            $http.get(ENV.apiUrl + "api/Sales_Doc/get_Categories").then(function (res) {

                $scope.rcbCategories = res.data;

               


            });

            //$http.get(ENV.apiUrl + "api/Sales_Doc/GetAllBanks").then(function (res) {

            $http.get(ENV.apiUrl + "api/StudentFee/GetBanks").then(function (res) {

                $scope.bankList = res.data;

            });

            $scope.print = function (div) {

                $("#tableIDDiv").css({ display: "block" });



                var docHead = document.head.outerHTML;
                var printContents = document.getElementById(div).outerHTML;
                var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

                var newWin = window.open("", "_blank", winAttr);
                var writeDoc = newWin.document;
                writeDoc.open();
                writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                writeDoc.close();
                newWin.focus();

                // $scope.shds = false;
                $timeout(function () {
                    $("#tableIDDiv").css({ display: "none" });

                }, 200);

            }

            $scope.cancel = function () {
                $scope.temp = {}
                $scope.temp1 = {}
                $scope.GetItems = [];
                $scope.itemList = [];
                $scope.rcbInternal = [];
                $scope.im_item_code1 = '';
                $scope.temp['paymentMode'] = 'CA';
                $scope.temp['provDate'] = today
                $scope.cardpay = false;
                $scope.bankShow = false;
                $scope.chequeShow = false;
                $scope.doc_vat_total_amount = 0;
                if ($scope.rcbSalesType.length > 0)
                    $scope.temp['sal_type'] = $scope.rcbSalesType[2].sal_type;

                if ($scope.rcbDocType.length > 0)
                    $scope.temp['doc_type'] = "01";  //$scope.rcbDocType[0].dt_code

                $scope.disableOnInvoiceReturn = false;
                $scope.showPickInvoice = false;
                $scope.showBtnOnInvoiceReturn = false;
                $scope.disableReturnBtn = false;
                $scope.disableSaveReturnBtn = false;
                $scope.old_doc_prov_no_show = false;

            }

            $scope.getCategories = function (str) {

                if ($scope.temp.subcategory_code == undefined)
                    $scope.rcbItems = [];
                if ($scope.temp.subcategory_code == '')
                    $scope.rcbItems = [];

                $http.get(ENV.apiUrl + "api/Sales_Doc/get_SubCategories?pc_parentcode=" + str).then(function (res) {

                    $scope.rcbSubCategories = res.data;

                });

            }
            /* ----------- This code is worinkg----------------- 
                $scope.getSubCategories = function (str) {
                    $http.get(ENV.apiUrl + "api/Sales_Doc/GetAllItemsInSubCategoryNew?pc_code=" + str).then(function (res) {

                        $scope.rcbItems = res.data;

                    });

                }
            */

            $scope.getSubCategories = function (str) {
                if($scope.salesmanDeptCode == undefined || $scope.salesmanDeptCode == "") {
                    $scope.salesmanDeptCode = '10';
                }

                $http.get(ENV.apiUrl + "api/Sales_Doc/GetAllItemsInSubCategoryByDepartment?pc_code=" + str + "&dept_code=" + $scope.salesmanDeptCode).then(function (res) {
                    $scope.rcbItems = res.data;
                });
            }

            $scope.qtychange = function (str) {
                debugger
                //if (parseInt($scope.resitem.original_qty) >= parseInt($scope.temp.qty) || $scope.temp.qty == '') {
                if (parseInt($scope.temp.qty) > 0 || $scope.temp.qty == '') {
                    $scope.temp.finalValue = ($scope.temp.qty * parseFloat($scope.temp.im_sell_price));
                    //$scope.totalAmtClick()
                    $scope.totalAmtClick();
                }
                else {
                    swal({
                        title: "Alert", text: " Item Quantity should be more than Zero.",
                    });

                    $scope.temp['qty'] = 1;
                    $scope.temp.finalValue = ($scope.temp.qty * parseFloat($scope.temp.im_sell_price));
                    $scope.totalAmtClick();

                }

            }
            
            $scope.qtychangeby = function (str) {
                debugger
                //if (parseInt(str.original_qty) >= parseInt(str.dd_qty) || str.dd_qty == '') {

                if (parseInt(str.dd_qty) > 0 || str.dd_qty == '') {

                    str.dd_sell_value_final = (str.dd_qty * parseFloat(str.dd_sell_price));


                    // $scope.totalAmtClick()

                    if (str.im_item_vat_percentage != undefined || str.im_item_vat_percentage != "" || str.im_item_vat_percentage != null) {
                        str.dd_item_vat_per = str.im_item_vat_percentage;
                        var sell_val_final = (str.dd_qty * parseFloat(str.dd_sell_price));

                        //var finalPer = (parseFloat(sell_val_final) * str.im_item_vat_percentage);

                        var totalvat = (parseFloat(sell_val_final) * str.im_item_vat_percentage) / 100;
                        str.dd_item_vat_amount = parseFloat(totalvat).toFixed(3);
                        str.dd_sell_value_final = parseFloat(sell_val_final) + parseFloat(totalvat);
                        str.dd_sell_value_final = parseFloat(str.dd_sell_value_final).toFixed(3);
                    }
                  
                    $scope.totalAmtClick();
                }

                else {
                    swal({ title: "Alert", text: " Item Quantity should be more than Zero.", });

                    str['dd_qty'] = 1;
                    str.dd_sell_value_final = (str.dd_qty * parseFloat(str.dd_sell_price));
                    $scope.totalAmtClick();

                }

            }

            //$scope.calculateVATTotal = function () {
            //    debugger
            //    for (var i = 0; i < $scope.itemList.length; i++) {
            //        $scope.doc_vat_total_amount = parseFloat($scope.doc_vat_total_amount) + parseFloat($scope.itemList[i].dd_item_vat_amount);
            //    }
            //}

            $scope.qtychangeby1 = function (str) {
                debugger;

                if (str.dd_sell_price == undefined) {

                    $http.get(ENV.apiUrl + "api/Sales_Doc/GetItemCodeDetails?item_code=" + str.im_item_code + '&sg_name=' + $scope.temp.gcode).then(function (res) {



                        $scope.resitem = res.data[0];

                        //if ($scope.resitem.original_qty > 0) {
                        if (str.dd_qty > 0) {

                            //  $scope.temp.qty = 1;
                            str.dd_sell_price = parseFloat($scope.resitem.im_sell_price);

                            str.dd_sell_value_final = (parseInt(str.dd_qty) * parseFloat($scope.resitem.im_sell_price));

                            var sell_val_final = (parseInt(str.dd_qty) * parseFloat($scope.resitem.im_sell_price));
                            
                            if (str.im_item_vat_percentage != undefined || str.im_item_vat_percentage != "" || str.im_item_vat_percentage != null) {
                                var finalPer = parseFloat(sell_val_final) * parseFloat(str.im_item_vat_percentage);
                                var totalvat = parseFloat(finalPer) / 100;
                                str.dd_item_vat_amount = parseFloat(sell_val_final) + parseFloat(totalvat);
                                str.dd_sell_value_final = parseFloat(sell_val_final) + parseFloat(totalvat);
                            }
                            

                        }
                        else {
                            swal({ title: "Alert", text: " Item Quantity should be more than Zero.", imageUrl: "assets/img/notification-alert.png", });

                        }


                        $http.get(ENV.apiUrl + "api/Sales_Doc/GetItemCodeLocations?im_inv_no=" + str.im_inv_no).then(function (res) {

                            $scope.resloc = res.data;
                            if ($scope.resloc.length > 0)
                                str.loc_codes = $scope.resloc[0].loc_code;



                        });

                    });
                }

                if (str.dd_sell_price == '') {
                    $http.get(ENV.apiUrl + "api/Sales_Doc/GetItemCodeDetails?item_code=" + str.im_item_code + '&sg_name=' + $scope.temp.gcode).then(function (res) {



                        $scope.resitem = res.data[0];

                        //if ($scope.resitem.original_qty > 0) {
                        if (str.dd_qty > 0) {

                            //  $scope.temp.qty = 1;
                            str.dd_sell_price = parseFloat($scope.resitem.im_sell_price);

                            str.dd_sell_value_final = (parseInt(str.dd_qty) * parseFloat($scope.resitem.im_sell_price));

                            var sell_val_final = (parseInt(str.dd_qty) * parseFloat($scope.resitem.im_sell_price));

                            if (str.im_item_vat_percentage != undefined || str.im_item_vat_percentage != "" || str.im_item_vat_percentage != null) {
                                var finalPer = parseFloat(sell_val_final) * parseFloat(str.im_item_vat_percentage);
                                var totalvat = parseFloat(finalPer) / 100;
                                str.dd_item_vat_amount = parseFloat(sell_val_final) + parseFloat(totalvat);
                                str.dd_sell_value_final = parseFloat(sell_val_final) + parseFloat(totalvat);
                            }


                        }
                        else {
                            swal({ title: "Alert", text: " Item Quantity should be more than Zero.", imageUrl: "assets/img/notification-alert.png", });

                        }


                        $http.get(ENV.apiUrl + "api/Sales_Doc/GetItemCodeLocations?im_inv_no=" + str.im_inv_no).then(function (res) {

                            $scope.resloc = res.data;
                            if ($scope.resloc.length > 0)
                                str.loc_codes = $scope.resloc[0].loc_code;

                        });

                    });
                }



                //if (parseInt(str.original_qty) >= parseInt(str.dd_qty) || str.dd_qty == '') {

                if (parseInt(str.dd_qty)>0 || str.dd_qty == '') {
                    str.dd_sell_value_final = (str.dd_qty * parseFloat(str.dd_sell_price));

                    // $scope.totalAmtClick()

                    var sell_val_final = (parseInt(str.dd_qty) * parseFloat(str.dd_sell_price));

                    if (str.im_item_vat_percentage != undefined || str.im_item_vat_percentage != "" || str.im_item_vat_percentage != null) {
                        var finalPer = parseFloat(sell_val_final) * parseFloat(str.im_item_vat_percentage);
                        var totalvat = parseFloat(finalPer) / 100;
                        //str.dd_item_vat_amount = parseFloat(sell_val_final) + parseFloat(totalvat);
                        //str.dd_sell_value_final = parseFloat(sell_val_final) + parseFloat(totalvat);
                        str.dd_sell_value_final = parseFloat(sell_val_final).toFixed(3);
                        str.dd_item_vat_amount = parseFloat(totalvat).toFixed(3);
                        str.dd_sell_value_line_total = parseFloat(sell_val_final)+parseFloat(totalvat);
                        str.dd_sell_value_line_total = parseFloat(str.dd_sell_value_line_total).toFixed(3);
                    }

                    $scope.lineDiscountPer(str);
                    //$scope.totalAmtClick();
                }

                else {
                    swal({ title: "Alert", text: " Item Quantity should be more than Zero.", });

                    str['dd_qty'] = 1;
                    str.dd_sell_value_final = (str.dd_qty * parseFloat(str.dd_sell_price));

                    var sell_val_final = (parseInt(str.dd_qty) * parseFloat(str.dd_sell_price));

                    if (str.im_item_vat_percentage != undefined || str.im_item_vat_percentage != "" || str.im_item_vat_percentage != null) {
                        var finalPer = parseFloat(sell_val_final) * parseFloat(str.im_item_vat_percentage);
                        var totalvat = parseFloat(finalPer) / 100;
                        str.dd_item_vat_amount = parseFloat(totalvat).toFixed(3);
                        str.dd_sell_value_final = parseFloat(sell_val_final).toFixed(3);
                        str.dd_sell_value_line_total = parseFloat(parseFloat(sell_val_final) + parseFloat(totalvat)).toFixed(3);;
                    }

                    $scope.lineDiscountPer(str);
                    //$scope.totalAmtClick();

                }

            }

            $scope.check_qty_value = function (str) {
                debugger
                if (parseInt(str.dd_qty) > parseInt(str.invoice_qty)) {
                    swal({ title: "Alert", text: "Item Quantity should not be more than invoice qty.", imageUrl: "assets/img/notification-alert.png", });
                    str.dd_qty = str.invoice_qty;
                }

                if (str.dd_qty == '') {
                    str['dd_qty'] = 1;

                    str.dd_sell_value_final = (str.dd_qty * parseFloat(str.dd_sell_price));

                    var sell_val_final = (parseInt(str.dd_qty) * parseFloat(str.dd_sell_price));

                    if (str.im_item_vat_percentage != undefined || str.im_item_vat_percentage != "" || str.im_item_vat_percentage != null) {
                        var finalPer = parseFloat(sell_val_final) * parseFloat(str.im_item_vat_percentage);
                        var totalvat = parseFloat(finalPer) / 100;
                        str.dd_item_vat_amount = parseFloat(totalvat).toFixed(3);
                        str.dd_sell_value_final = parseFloat(sell_val_final).toFixed(3);
                        str.dd_sell_value_line_total = parseFloat(parseFloat(sell_val_final) + parseFloat(totalvat)).toFixed(3);;
                    }

                    $scope.totalAmtClick();
                }
            }

            $scope.lineDiscountPer = function (item_data) {
                debugger;
                var dis_vat = 0;
                if (isNaN(parseFloat(item_data.dd_line_discount_pct))){
                    item_data.dd_line_discount_pct = 0;
                }
                if (parseFloat(item_data.dd_line_discount_pct) <= 100 ) {

                    
                    if (item_data.dd_line_discount_pct == undefined || item_data.dd_line_discount_pct == '' || item_data.dd_line_discount_pct == 0) {
                        item_data.dd_line_discount_amount = 0;
                        item_data.dd_line_discount_pct = 0;

                        if (item_data.im_item_vat_percentage != undefined || item_data.im_item_vat_percentage != "" || item_data.im_item_vat_percentage != null) {
                            dis_vat = (((item_data.dd_sell_value_final - item_data.dd_line_discount_amount) * item_data.im_item_vat_percentage) / 100);
                            item_data.dd_item_vat_amount = parseFloat(dis_vat).toFixed(3);
                        }

                        item_data.dd_sell_value_line_total = parseFloat(parseFloat(item_data.dd_sell_value_final) + parseFloat(item_data.dd_item_vat_amount)).toFixed(3);
                    }
                    else {
                        item_data.dd_line_discount_amount = parseFloat((parseFloat(item_data.dd_sell_value_final) * parseFloat(item_data.dd_line_discount_pct)) / 100).toFixed(3);

                        if (item_data.im_item_vat_percentage != undefined || item_data.im_item_vat_percentage != "" || item_data.im_item_vat_percentage != null) {
                            dis_vat = (((item_data.dd_sell_value_final - item_data.dd_line_discount_amount) * item_data.im_item_vat_percentage) / 100);
                            item_data.dd_item_vat_amount = parseFloat(dis_vat).toFixed(3);
                        }

                        item_data.dd_sell_value_line_total = parseFloat(item_data.dd_sell_value_final) + parseFloat(item_data.dd_item_vat_amount);
                        item_data.dd_sell_value_line_total = parseFloat(parseFloat(item_data.dd_sell_value_line_total) - parseFloat(item_data.dd_line_discount_amount)).toFixed(3);
                    }

                    $scope.totalAmtClick();
                }
                else {
                    item_data.dd_line_discount_amount = 0;
                    item_data.dd_line_discount_pct = 0;

                    if (item_data.im_item_vat_percentage != undefined || item_data.im_item_vat_percentage != "" || item_data.im_item_vat_percentage != null) {
                        dis_vat = (((item_data.dd_sell_value_final - item_data.dd_line_discount_amount) * item_data.im_item_vat_percentage) / 100);
                        item_data.dd_item_vat_amount = parseFloat(dis_vat).toFixed(3);
                    }

                    item_data.dd_sell_value_line_total = parseFloat(parseFloat(item_data.dd_sell_value_final) + parseFloat(item_data.dd_item_vat_amount)).toFixed(3);
                    swal({ title: "Alert", text: "Discount Should not be more than 100%.", imageUrl: "assets/img/notification-alert.png", });

                    $scope.totalAmtClick();

                }


            };

            $scope.getitemCode = function (str) {
                debugger
                console.log("str", str);
                //$scope.im_item_vat_percentag = 0;
                $scope.im_item_vat_percentage = str.im_item_vat_percentage;

                //$scope.im_item_vat_percentage = //$scope.im_item_vat_percentage1.toFixed(2);

                $http.get(ENV.apiUrl + "api/Sales_Doc/GetItemCodeDetails?item_code=" + str.im_item_code + '&sg_name=' + $scope.temp.gcode).then(function (res) {



                    $scope.resitem = res.data[0];

                    //if ($scope.resitem.original_qty > 0) {

                        if ($scope.temp.doc_type == '02') {
                            $scope.temp.qty = 1;
                            $scope.temp.im_sell_price = $scope.resitem.im_sell_price;
                            $scope.temp.finalValue = 0;
                            $scope.temp.im_item_vat_percentage = $scope.resitem.im_item_vat_percentage;
                        }
                        else {
                            $scope.temp.qty = 1;
                            $scope.temp.im_sell_price = $scope.resitem.im_sell_price;
                            $scope.temp.finalValue = (1 * parseFloat($scope.resitem.im_sell_price));
                            $scope.temp.im_item_vat_percentage = $scope.resitem.im_item_vat_percentage;
                        }

                    //}
                    //else {
                    //    swal({ title: "Alert", text: "Item quantity is 0.Please Update Selected Item Quantity.", imageUrl: "assets/img/notification-alert.png", });

                    //}


                    $http.get(ENV.apiUrl + "api/Sales_Doc/GetItemCodeLocations?im_inv_no=" + str.im_inv_no).then(function (res) {

                        $scope.resloc = res.data;
                        if ($scope.resloc.length > 0)
                            $scope.temp.loc_code = $scope.resloc[0].loc_code;

                        $scope.temp['gcode'] = str.sg_name
                        if ($scope.temp.enroll != undefined)
                            $scope.disable_btn1 = false;
                    });

                });

            }


            $scope.add = function () {
                debugger;
                var single_vat_cal = 0;

                setTimeout(function () {
                    $scope.Scroll();
                }, 700);
                if ($scope.im_item_code1.im_inv_no == '' || $scope.im_item_code1.im_inv_no == undefined) {
                }

                else {
                    $scope.totalFinal = 0;
                    $scope.flg = false;
                    //for (var i = 0; i < $scope.itemList.length; i++) {
                    //    if ($scope.itemList[i].im_inv_no == $scope.im_item_code1.im_inv_no) {

                    //        $scope.itemList[i].dd_qty += 1;
                    //        $scope.itemList[i].dd_sell_value_final += $scope.temp.im_sell_price
                    //        // $scope.itemList[i].im_inv_no
                    //        $scope.flg = true;
                    //        i = $scope.itemList.length;
                    //    }
                    //}

                    //= false;
                    if ($scope.flg == false) {

                        if ($scope.temp.doc_type == '02') {

                            single_vat_cal = parseFloat(($scope.temp.finalValue * $scope.temp.im_item_vat_percentage) / 100);

                            var data =
                                {
                                    //   sg_name: $("#rcbGroup").find("option:selected").text(),
                                    //sg_name: $scope.gropuName,
                                    sg_name: $("#rcbGroup").find("option:selected").text(),
                                    sg_desc: $("#rcbGroup").find("option:selected").text(),
                                    sg_code: $scope.temp.gcode,
                                    im_inv_no: $scope.im_item_code1.im_inv_no,
                                    im_item_code: $scope.im_item_code1.im_item_code,
                                    im_desc: $("#rcbItem").find("option:selected").text(),
                                    item_location_name: $("#rcbLocation").find("option:selected").text(),
                                    loc_name: $("#rcbLocation").find("option:selected").text(),
                                    //item_location_name: $scope.temp.loc_code,
                                    loc_code: $scope.temp.loc_code,
                                    original_qty: $scope.resitem.original_qty,
                                    dd_qty: $scope.temp.qty,
                                    dd_sell_price: parseFloat($scope.temp.im_sell_price).toFixed(3),
                                    dd_sell_value_final: parseFloat($scope.temp.finalValue).toFixed(3),
                                    doc_remark: $scope.temp.doc_remark,
                                    im_item_vat_percentage: $scope.temp.im_item_vat_percentage,
                                    dd_item_vat_amount: parseFloat(single_vat_cal).toFixed(3),
                                    dd_sell_value_line_total: parseFloat(parseFloat($scope.temp.finalValue) + parseFloat(single_vat_cal)).toFixed(3),
                                }
                            $scope.itemList.push(data)
                            //$scope.qtychangeby(data);
                        }
                        else {

                            single_vat_cal=parseFloat(($scope.temp.finalValue*$scope.temp.im_item_vat_percentage)/100);

                            var data =
                               {
                                   //   sg_name: $("#rcbGroup").find("option:selected").text(),

                                   //sg_name: $scope.gropuName,
                                   sg_name: $("#rcbGroup").find("option:selected").text(),
                                   sg_desc: $("#rcbGroup").find("option:selected").text(),
                                   sg_code: $scope.temp.gcode,
                                   im_inv_no: $scope.im_item_code1.im_inv_no,
                                   im_item_code: $scope.im_item_code1.im_item_code,
                                   im_desc: $("#rcbItem").find("option:selected").text(),
                                   item_location_name: $("#rcbLocation").find("option:selected").text(),
                                   loc_name: $("#rcbLocation").find("option:selected").text(),
                                   //item_location_name: $scope.temp.loc_code,
                                   loc_code: $scope.temp.loc_code,
                                   original_qty: $scope.resitem.original_qty,
                                   dd_qty: $scope.temp.qty,
                                   dd_sell_price: parseFloat($scope.temp.im_sell_price).toFixed(3),
                                   dd_sell_value_final: parseFloat($scope.temp.finalValue).toFixed(3),
                                   doc_remark: $scope.temp.doc_remark,
                                   im_item_vat_percentage: $scope.temp.im_item_vat_percentage,
                                   dd_item_vat_amount:parseFloat(single_vat_cal).toFixed(3),
                                   dd_sell_value_line_total: parseFloat(parseFloat($scope.temp.finalValue) + parseFloat(single_vat_cal)).toFixed(3),
                                   dd_line_discount_pct: 0,
                                   dd_line_discount_amount:0
                               }
                            $scope.itemList.push(data);
                            //$scope.qtychangeby(data);
                                
                        }

                    }
                }
                $scope.totalAmtClick();


                $scope.CancelItem();

                document.getElementById("rcbItem").focus();
            }

            $scope.finalize = function () {
                $scope.disable_btn = true;
                $scope.disable_btn1 = true;


                //if (!string.IsNullOrEmpty(salesman_cd))
                //    invsObj.sm_code = salesman_cd;
                //else
                //    invsObj.sm_code = txt_sm_code.Text;


                if ($scope.temp.sal_type == '05') {
                    $scope.temp['enroll'] = $scope.internalUser;
                }

                if ($scope.salesmanDeptCode == undefined || $scope.salesmanDeptCode == "") {
                    $scope.salesmanDeptCode = '10';
                }

                var data = {
                    dep_code: $scope.salesmanDeptCode,//'10',
                    doc_prov_no: '',
                    doc_prov_date: $scope.temp.provDate,
                    dt_code: $scope.temp.doc_type,
                    sm_code: $scope.salesman.sm_code,//101
                    up_name: $rootScope.globals.currentUser.username,
                    creation_user: $rootScope.globals.currentUser.username,
                    sal_type: $scope.temp.sal_type,
                    doc_total_amount: $scope.temp.totalFinal,
                    doc_status: '2',
                    cus_account_no: $scope.temp.enroll,
                    doc_special_name: $scope.temp.special_nm,
                    dep_code_caused_by: $scope.salesmanDeptCode,//'10',
                    doc_order_ref_no: $scope.temp.oderRefNo,
                    doc_other_charge_amount: $scope.temp.otherCharges,
                    doc_discount_pct: $scope.temp.overalldis,
                    doc_discount_amount: $scope.temp.disamt,
                    creation_date: today,
                    doc_date: $scope.temp.provDate,
                    doc_narration: $scope.temp.doc_narration,                
                    doc_vat_total_amount: $scope.doc_vat_total_amount,
                    doc_round_value:round_diff_val
                }

                $http.post(ENV.apiUrl + "api/Sales_Doc/Insert_Sale_Documents_VAT", data).then(function (msg) {
                    var d = msg.data;

                    $scope.salesDetails = [];
                    for (var i = 0; i < $scope.itemList.length; i++) {

                        var paymode = $scope.temp.paymentMode;
                        //if (paymode=='CH')

                        var data = {
                            dep_code: $scope.salesmanDeptCode,//'10',
                            doc_prov_no: msg.data,
                            dd_line_no: i,
                            dd_status: '2',
                            dd_payment_mode: paymode,
                            im_inv_no: $scope.itemList[i].im_inv_no,
                            loc_code: $scope.itemList[i].loc_code,
                            dd_sell_price: $scope.itemList[i].dd_sell_price,
                            dd_sell_price_discounted: $scope.itemList[i].dd_line_discount_amount,
                            dd_sell_price_final: 0,
                            dd_sell_value_final: $scope.itemList[i].dd_sell_value_final,
                            dd_sell_cost_total: 0,
                            dd_qty: $scope.itemList[i].dd_qty,
                            dd_outstanding_qty: 0,
                            dd_line_discount_pct: $scope.itemList[i].dd_line_discount_pct,
                            dd_line_discount_amount: $scope.itemList[i].dd_line_discount_amount,
                            dd_line_total: $scope.itemList[i].dd_sell_value_line_total,
                            dd_physical_qty: $scope.itemList[i].original_qty,
                            dd_cheque_number: $scope.temp.chequeNo,
                            dd_cheque_date: $scope.temp.chequeDate,
                            dd_cheque_bank_code: $scope.temp.bankCode,
                            doc_remark: $scope.itemList[i].doc_remark,
                            vat_flag: false,
                            dd_item_vat_per: $scope.itemList[i].im_item_vat_percentage,
                            dd_item_vat_amount: $scope.itemList[i].dd_item_vat_amount
                            
                            //dd_physical_qty:$scope.itemList[i].original_qty,

                        }
                        $scope.salesDetails.push(data)
                    }
                    $http.post(ENV.apiUrl + "api/Sales_Doc/Insert_Sale_Documents_Details1_VAT", $scope.salesDetails).then(function (msg) {
                        if (msg.data == true) {
                            //swal({ title: "Alert", text: d + " Record Inserted Success.", imageUrl: "assets/img/notification-alert.png", });
                            swal({
                                title: '',
                                text: "Do you want print receipt",
                                showCloseButton: true,
                                showCancelButton: true,
                                confirmButtonText: 'Yes',
                                width: 380,
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                            }).then(function (isConfirm) {
                                if (isConfirm) {
                                    //  $scope.OkRejectadm();
                                    if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                                        //var data = {
                                        //    location: 'Invs.INVR02DPSMIS',
                                        //    parameter: { doc_prov_no: d },
                                        //    state: 'main.salvat'
                                        //}

                                        var data = {
                                            doc_prov_no: d,
                                            sdate: '',
                                            edate: '',
                                            state: false,
                                            doc_status: '',
                                            back_state: 'main.salvat'
                                        }

                                    }

                                    else if ($http.defaults.headers.common['schoolId'] == 'brs') {
                                        var data = {
                                            location: 'Invs.INVR02BRS',
                                            parameter: { doc_prov_no: d },
                                            state: 'main.salvat'
                                        }
                                    }

                                    else if ($http.defaults.headers.common['schoolId'] == 'sisqatar') {
                                        var data = {
                                            location: 'Invs.INVR02SISQATAR',
                                            parameter: { doc_prov_no: d },
                                            state: 'main.salvat'
                                        }
                                    }

                                    else if ($http.defaults.headers.common['schoolId'] == 'tsis') {
                                        var data = {
                                            location: 'Invs.INVR02TSIS',
                                            parameter: { doc_prov_no: d },
                                            state: 'main.salvat'
                                        }
                                    }
                                    else if ($http.defaults.headers.common['schoolId'] == 'sms') {
                                        var data = {
                                            location: 'Invs.INVR02NIS',
                                            parameter: { doc_prov_no: d },
                                            state: 'main.salvat'
                                        }
                                    }

                                    else if ($http.defaults.headers.common['schoolId'] == 'smfc') {
                                        var data = {
                                            location: 'Invs.INVR02VatSMFC',
                                            parameter: { doc_prov_no: d },
                                            state: 'main.salvat'
                                        }
                                    }

                                        
                                    else {
                                        var data = {
                                            location: 'Invs.INVR02Vat', //INVR02
                                            parameter: { doc_prov_no: d },
                                            state: 'main.salvat'
                                        }
                                    }
                                    //$scope.disable_btn = false;
                                    if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {

                                        window.localStorage["ReportProvNumber"] = JSON.stringify(data)
                                        $state.go('main.rpts')
                                    }
                                    else {

                                        window.localStorage["ReportDetails"] = JSON.stringify(data)
                                        $state.go('main.ReportCardParameter')
                                    }
                                }
                            });
                            $scope.cancel();

                        }
                        else if (msg.data == false) {
                            swal({ title: "Alert", text: "Record Not Inserted .", imageUrl: "assets/img/notification-alert.png", });
                            $scope.disable_btn = false;
                            $scope.disable_btn1 = false;

                        }
                        else {
                            swal("Error-" + msg.data)
                        }

                        $scope.doc_vat_total_amount = 0;
                    });



                });
            }

            $scope.returnInvoice = function () {
                $scope.disableReturnBtn = true;
                $scope.disableSaveReturnBtn = true;

                var obj = {
                    dep_code: $scope.depCode,//'10',
                    doc_prov_no: '',
                    doc_prov_date: $scope.temp.provDate,
                    dt_code: $scope.temp.doc_type,
                    sm_code: $scope.salesman.sm_code, //101
                    up_name: $rootScope.globals.currentUser.username,
                    creation_user: $rootScope.globals.currentUser.username,
                    sal_type: $scope.temp.sal_type,
                    doc_total_amount: $scope.temp.totalFinal,
                    doc_status: '2',
                    cus_account_no: $scope.temp.enroll,
                    doc_special_name: $scope.temp.special_nm,
                    dep_code_caused_by: $scope.depCode, //'10',
                    doc_order_ref_no: $scope.temp.oderRefNo,
                    doc_other_charge_amount: $scope.temp.otherCharges,
                    doc_discount_pct: $scope.temp.overalldis,
                    doc_discount_amount: $scope.temp.disamt,
                    creation_date: today,
                    doc_date: $scope.temp.provDate,
                    doc_narration: $scope.temp.doc_narration,
                    doc_vat_total_amount: $scope.doc_vat_total_amount,
                    doc_round_value: round_diff_val,
                    old_doc_prov_no: $scope.old_doc_prov_no,
                    doc_reference_prov_no: $scope.old_doc_prov_no
                }

                console.log("obj", obj);
                console.log("itemList",$scope.itemList);

                $http.post(ENV.apiUrl + "api/Sales_Doc/Insert_Sale_Documents_Return", obj).then(function (msg) {
                   var d = msg.data;
                debugger
                    $scope.salesDetails1 = [];
                    for (var i = 0; i < $scope.itemList.length; i++) {

                        var paymode = $scope.temp.paymentMode;
                        //if ($scope.itemList[i].dd_status == '8') // check first time partially approved
                        //{
                        //    $scope.doc_prov_no_have_previous = $scope.old_doc_prov_no;
                        //    var dd_line_no_have_previous = $scope.doc_prov_no_have_previous;
                        //}
                        //else {
                        //    $scope.doc_prov_no_have_previous = '';
                        //    var dd_line_no_have_previous = $scope.old_doc_prov_no;  
                        //}
                        
                        if (parseInt($scope.itemList[i].invoice_qty) == parseInt($scope.itemList[i].dd_qty)) {
                           var dd_status = '9'; // dd_status 9 for fully return
                        }
                        else {
                            var dd_status = '8'; // dd_status 8 for Partially return
                        }
                        
                        var data = {
                            dep_code: $scope.itemList[i].doc_dept,      //'10',
                            doc_prov_no: msg.data,
                            dd_line_no: i,
                            dd_status: dd_status,  //'2',
                            dd_payment_mode: paymode,
                            im_inv_no: $scope.itemList[i].im_inv_no,
                            loc_code: $scope.itemList[i].loc_code,
                            dd_sell_price: $scope.itemList[i].dd_sell_price,
                            dd_sell_price_discounted: $scope.itemList[i].dd_line_discount_amount,
                            dd_sell_price_final: $scope.itemList[i].dd_sell_price_final,
                            dd_sell_value_final: $scope.itemList[i].dd_sell_value_final,
                            dd_sell_cost_total: 0,
                            dd_qty: $scope.itemList[i].dd_qty,
                            dd_outstanding_qty:0,
                            dd_line_discount_pct: $scope.itemList[i].dd_line_discount_pct,
                            dd_line_discount_amount: $scope.itemList[i].dd_line_discount_amount,
                            dd_line_total: $scope.itemList[i].dd_sell_value_line_total,
                            dd_physical_qty: $scope.itemList[i].original_qty,
                            dd_cheque_number: $scope.temp.chequeNo,
                            dd_cheque_date: $scope.temp.chequeDate,
                            dd_cheque_bank_code: $scope.temp.bankCode,
                            doc_remark: $scope.itemList[i].doc_remark,                            
                            dd_item_vat_per: $scope.itemList[i].im_item_vat_percentage,
                            dd_item_vat_amount: $scope.itemList[i].dd_item_vat_amount,
                            //dd_line_no_have_previous: dd_line_no_have_previous,
                            doc_prov_no_have_previous: $scope.old_doc_prov_no //$scope.doc_prov_no_have_previous
                        }
                        $scope.salesDetails1.push(data)
                    }

                    console.log("salesDetails1", $scope.salesDetails1);

                    $http.post(ENV.apiUrl + "api/Sales_Doc/Insert_Sale_Documents_Details1_Return", $scope.salesDetails1).then(function (msg) {
                        if (msg.data == true) {
                            
                            swal({
                                title: '',
                                text: "Do you want print receipt",
                                showCloseButton: true,
                                showCancelButton: true,
                                confirmButtonText: 'Yes',
                                width: 380,
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                            }).then(function (isConfirm) {
                                if (isConfirm) {
                                     //  $scope.OkRejectadm();
                                    if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                                            //var data = {
                                            //    location: 'Invs.INVR02DPSMIS',
                                            //    parameter: { doc_prov_no: d },
                                            //    state: 'main.salvat'
                                            //}

                                        var data = {
                                            doc_prov_no: d,
                                            sdate: '',
                                            edate: '',
                                            state: false,
                                            doc_status: '',
                                            back_state: 'main.salvat'
                                        }

                                    }

                                    else if ($http.defaults.headers.common['schoolId'] == 'brs') {
                                        var data = {
                                            location: 'Invs.INVR02BRS',
                                            parameter: { doc_prov_no: d },
                                            state: 'main.salvat'
                                        }
                                    }

                                    else if ($http.defaults.headers.common['schoolId'] == 'sisqatar') {
                                        var data = {
                                            location: 'Invs.INVR02SISQATAR',
                                            parameter: { doc_prov_no: d },
                                            state: 'main.salvat'
                                        }
                                    }

                                    else if ($http.defaults.headers.common['schoolId'] == 'tsis') {
                                        var data = {
                                            location: 'Invs.INVR02TSIS',
                                            parameter: { doc_prov_no: d },
                                            state: 'main.salvat'
                                        }
                                    }
                                    else if ($http.defaults.headers.common['schoolId'] == 'sms') {
                                        var data = {
                                            location: 'Invs.INVR02NIS',
                                            parameter: { doc_prov_no: d },
                                            state: 'main.salvat'
                                        }
                                    }

                                    else if ($http.defaults.headers.common['schoolId'] == 'smfc') {
                                        var data = {
                                            location: 'Invs.INVR02Vat',
                                            parameter: { doc_prov_no: d },
                                            state: 'main.salvat'
                                        }
                                    }


                                    else {
                                        var data = {
                                            location: 'Invs.INVR02',
                                            parameter: { doc_prov_no: d },
                                            state: 'main.salvat'
                                        }
                                    }
                                       //$scope.disable_btn = false;
                                    if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {

                                        window.localStorage["ReportProvNumber"] = JSON.stringify(data)
                                        $state.go('main.rpts')
                                    }
                                    else {

                                        window.localStorage["ReportDetails"] = JSON.stringify(data)
                                        $state.go('main.ReportCardParameter')
                                    }
                                }
                            });
                            $scope.cancel();

                        }
                        else if (msg.data == false) {
                            swal({ title: "Alert", text: "Record Not Inserted .", imageUrl: "assets/img/notification-alert.png", });
                            $scope.disableReturnBtn = false;
                            $scope.disableSaveReturnBtn = true;                           
                        }
                        else {
                            swal("Error-" + msg.data)
                        }

                        $scope.doc_vat_total_amount = 0;
                    });

                });
                

            };

            $scope.saveReturnInvoice = function () {                
                debugger
                $scope.disableSaveReturnBtn = true;
                $scope.disableReturnBtn = true;

                var obj = {
                    dep_code: $scope.depCode,//'10',
                    doc_prov_no: '',
                    doc_prov_date: $scope.temp.provDate,
                    dt_code: $scope.temp.doc_type,
                    sm_code: $scope.salesman.sm_code, //101
                    up_name: $rootScope.globals.currentUser.username,
                    creation_user: $rootScope.globals.currentUser.username,
                    sal_type: $scope.temp.sal_type,
                    doc_total_amount: $scope.temp.totalFinal,
                    doc_status: '0',
                    cus_account_no: $scope.temp.enroll,
                    doc_special_name: $scope.temp.special_nm,
                    dep_code_caused_by: $scope.depCode, //'10',
                    doc_order_ref_no: $scope.temp.oderRefNo,
                    doc_other_charge_amount: $scope.temp.otherCharges,
                    doc_discount_pct: $scope.temp.overalldis,
                    doc_discount_amount: $scope.temp.disamt,
                    creation_date: today,
                    doc_date: $scope.temp.provDate,
                    doc_narration: $scope.temp.doc_narration,
                    doc_vat_total_amount: $scope.doc_vat_total_amount,
                    doc_round_value: round_diff_val,
                    old_doc_prov_no: $scope.old_doc_prov_no,
                    doc_reference_prov_no: $scope.old_doc_prov_no,
                    save_finalize_flag: "S"
                }

                console.log("obj", obj);
                console.log("itemList", $scope.itemList);

                $http.post(ENV.apiUrl + "api/Sales_Doc/Insert_Sale_Documents_Return", obj).then(function (msg) {
                    var d = msg.data;
                    debugger
                    $scope.salesDetails1 = [];
                    for (var i = 0; i < $scope.itemList.length; i++) {

                        var paymode = $scope.temp.paymentMode;
                        //if ($scope.itemList[i].dd_status == '8') // check first time partially approved
                        //{
                        //    $scope.doc_prov_no_have_previous = $scope.old_doc_prov_no;
                        //    var dd_line_no_have_previous = $scope.doc_prov_no_have_previous;
                        //}
                        //else {
                        //    $scope.doc_prov_no_have_previous = '';
                        //    var dd_line_no_have_previous = $scope.old_doc_prov_no;  
                        //}

                        if (parseInt($scope.itemList[i].invoice_qty) == parseInt($scope.itemList[i].dd_qty)) {
                            var dd_status = '9'; // dd_status 9 for fully return
                        }
                        else {
                            var dd_status = '8'; // dd_status 8 for Partially return
                        }

                        var data = {
                            dep_code: $scope.itemList[i].doc_dept,      //'10',
                            doc_prov_no: msg.data,
                            dd_line_no: i,
                            dd_status: dd_status,  //'2',
                            dd_payment_mode: paymode,
                            im_inv_no: $scope.itemList[i].im_inv_no,
                            loc_code: $scope.itemList[i].loc_code,
                            dd_sell_price: $scope.itemList[i].dd_sell_price,
                            dd_sell_price_discounted: $scope.itemList[i].dd_line_discount_amount,
                            dd_sell_price_final: $scope.itemList[i].dd_sell_price_final,
                            dd_sell_value_final: $scope.itemList[i].dd_sell_value_final,
                            dd_sell_cost_total: 0,
                            dd_qty: $scope.itemList[i].dd_qty,
                            dd_outstanding_qty: 0,
                            dd_line_discount_pct: $scope.itemList[i].dd_line_discount_pct,
                            dd_line_discount_amount: $scope.itemList[i].dd_line_discount_amount,
                            dd_line_total: $scope.itemList[i].dd_sell_value_line_total,
                            dd_physical_qty: $scope.itemList[i].original_qty,
                            dd_cheque_number: $scope.temp.chequeNo,
                            dd_cheque_date: $scope.temp.chequeDate,
                            dd_cheque_bank_code: $scope.temp.bankCode,
                            doc_remark: $scope.itemList[i].doc_remark,
                            dd_item_vat_per: $scope.itemList[i].im_item_vat_percentage,
                            dd_item_vat_amount: $scope.itemList[i].dd_item_vat_amount,
                            //dd_line_no_have_previous: dd_line_no_have_previous,
                            doc_prov_no_have_previous: $scope.old_doc_prov_no //$scope.doc_prov_no_have_previous
                        }
                        $scope.salesDetails1.push(data)
                    }

                    console.log("salesDetails1", $scope.salesDetails1);

                    $http.post(ENV.apiUrl + "api/Sales_Doc/Insert_Sale_Documents_Details1_Return", $scope.salesDetails1).then(function (msg) {
                        if (msg.data == true) {

                            swal({
                                title: '',
                                text: "Do you want print receipt",
                                showCloseButton: true,
                                showCancelButton: true,
                                confirmButtonText: 'Yes',
                                width: 380,
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                            }).then(function (isConfirm) {
                                if (isConfirm) {
                                    //  $scope.OkRejectadm();
                                    if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                                        //var data = {
                                        //    location: 'Invs.INVR02DPSMIS',
                                        //    parameter: { doc_prov_no: d },
                                        //    state: 'main.salvat'
                                        //}

                                        var data = {
                                            doc_prov_no: d,
                                            sdate: '',
                                            edate: '',
                                            state: false,
                                            doc_status: '',
                                            back_state: 'main.salvat'
                                        }

                                    }

                                    else if ($http.defaults.headers.common['schoolId'] == 'brs') {
                                        var data = {
                                            location: 'Invs.INVR02BRS',
                                            parameter: { doc_prov_no: d },
                                            state: 'main.salvat'
                                        }
                                    }

                                    else if ($http.defaults.headers.common['schoolId'] == 'sisqatar') {
                                        var data = {
                                            location: 'Invs.INVR02SISQATAR',
                                            parameter: { doc_prov_no: d },
                                            state: 'main.salvat'
                                        }
                                    }

                                    else if ($http.defaults.headers.common['schoolId'] == 'tsis') {
                                        var data = {
                                            location: 'Invs.INVR02TSIS',
                                            parameter: { doc_prov_no: d },
                                            state: 'main.salvat'
                                        }
                                    }
                                    else if ($http.defaults.headers.common['schoolId'] == 'sms') {
                                        var data = {
                                            location: 'Invs.INVR02NIS',
                                            parameter: { doc_prov_no: d },
                                            state: 'main.salvat'
                                        }
                                    }

                                    else if ($http.defaults.headers.common['schoolId'] == 'smfc') {
                                        var data = {
                                            location: 'Invs.INVR02Vat',
                                            parameter: { doc_prov_no: d },
                                            state: 'main.salvat'
                                        }
                                    }


                                    else {
                                        var data = {
                                            location: 'Invs.INVR02Vat', //INVR02
                                            parameter: { doc_prov_no: d },
                                            state: 'main.salvat'
                                        }
                                    }
                                    //$scope.disable_btn = false;
                                    if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {

                                        window.localStorage["ReportProvNumber"] = JSON.stringify(data)
                                        $state.go('main.rpts')
                                    }
                                    else {

                                        window.localStorage["ReportDetails"] = JSON.stringify(data)
                                        $state.go('main.ReportCardParameter')
                                    }
                                }
                            });
                            $scope.cancel();

                        }
                        else if (msg.data == false) {
                            swal({ title: "Alert", text: "Record Not Inserted .", imageUrl: "assets/img/notification-alert.png", });
                            $scope.disableSaveReturnBtn = false;
                            $scope.disableReturnBtn = true;

                        }
                        else {
                            swal("Error-" + msg.data)
                        }

                        $scope.doc_vat_total_amount = 0;
                    });

                });


            };

            $scope.itemSearch = function () {
                $scope.GetItems = []
                $scope.temp1 = {}
                $('#itemSearch').modal('show');
                $scope.itemsetflg = false;
            }

            $scope.SearchInvoice = function () {
                $scope.GetItems = []
                $scope.temp1 = {}
                $('#invoiceSearch').modal('show');
                $scope.itemsetflg = false;
                $scope.store_issue_details = [];
            }
            $http.get(ENV.apiUrl + "api/StoreIssueController/GetCur").then(function (GetCur) {
                $scope.GetCur = GetCur.data;
                $scope.edt = { sims_cur_code: $scope.GetCur[0].sims_cur_code }
                $scope.select_cur($scope.GetCur[0].sims_cur_code);
            });

            $scope.select_cur = function (cur) {
                $http.get(ENV.apiUrl + "api/StoreIssueController/GetAca?cur=" + cur).then(function (GetAca) {
                    $scope.GetAca = GetAca.data;
                    $scope.edt['sims_acaedmic_year'] = $scope.GetAca[0].sims_acaedmic_year;
                    $scope.select_aca($scope.GetAca[0].sims_acaedmic_year)
                });
            }
            $scope.select_aca = function (aca) {
                $http.get(ENV.apiUrl + "api/StoreIssueController/Getgrd?cur=" + $scope.edt.sims_cur_code + "&aca=" + aca + "&user=" + $rootScope.globals.currentUser.username).then(function (Getgrd) {
                    $scope.Getgrd = Getgrd.data;

                    setTimeout(function () {
                        $('#cmb_grade').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }
            $scope.select_grd = function (grd) {
                $http.get(ENV.apiUrl + "api/StoreIssueController/Getsec?cur=" + $scope.edt.sims_cur_code + "&aca=" + $scope.edt.sims_acaedmic_year + "&grd=" + grd + "&user=" + $rootScope.globals.currentUser.username).then(function (Getsec) {
                    $scope.Getsec = Getsec.data;
                });

            }

            $scope.get_Details = function (data) {
                $scope.hide_submit = true;
                debugger;
                //"&doc_no=" + data.doc_no
                $http.get(ENV.apiUrl + "api/Sales_Doc/getInvoiceDetails?cur=" + data.sims_cur_code + "&aca=" + data.sims_acaedmic_year + "&grd=" + data.sims_grade_code + "&section=" + data.sims_section_code + "&fromdate=" + data.from_date + "&todate=" + data.to_date + "&doc_no=" + data.doc_no ).then(function (getFullData) {
                    $scope.store_issue_details = getFullData.data;
                    if ($scope.store_issue_details.length > 0) {
                        for (var i = 0; i < $scope.store_issue_details.length; i++) {
                            for (var j = 0; j < $scope.store_issue_details[i].sublist1.length; j++) {
                                $scope.store_issue_details[i].sublist1[j].creation_user = $rootScope.globals.currentUser.username;
                                var invoice_qty = $scope.store_issue_details[i].sublist1[j].dd_qty;
                                var current_qty = $scope.store_issue_details[i].sublist1[j].id_cur_qty;
                                var deleverd_qty = $scope.store_issue_details[i].sublist1[j].issued_dd_issue_qty;
                                if (deleverd_qty == '') {
                                    deleverd_qty = 0;
                                }
                                var total_minus_value = 0;
                                if (parseFloat(invoice_qty) < parseFloat(current_qty)) {
                                    $scope.store_issue_details[i].sublist1[j].inner_checked = true;

                                    total_minus_value = parseFloat(invoice_qty) - parseFloat(deleverd_qty)
                                    total_minus_value = parseInt(total_minus_value)
                                    if (parseInt(current_qty) > total_minus_value) {
                                        $scope.store_issue_details[i].sublist1[j].issue_quantity = total_minus_value;
                                    } else {
                                        $scope.store_issue_details[i].sublist1[j].issue_quantity_dis = true;
                                    }
                                }
                                else {
                                    $scope.store_issue_details[i].sublist1[j].inner_disabled = true;
                                    $scope.store_issue_details[i].sublist1[j].issue_quantity_dis = true;
                                    $scope.store_issue_details[i].sublist1[j].remarks_dis = true;
                                }
                            }

                        }
                        $scope.totalItems = $scope.store_issue_details.length;
                        $scope.todos = $scope.store_issue_details;
                        for (var i = 0; i < $scope.totalItems; i++) {
                            $scope.store_issue_details[i].icon = "fa fa-plus-circle";
                        }
                    }
                    else {
                        swal({ text: "Record not found. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                });

            }
            $scope.ItemSetSearch = function () {
                $scope.GetItems = []
                $scope.temp1 = {}
                //$scope.itemList = [];
                $('#itemSearch').modal('show');
                $scope.itemsetflg = true;
                $scope.temp1['im_assembly_ind'] = true;
                $scope.temp1['category_code'] = $scope.rcbCategories[0].category_code;

                if ($scope.salesmanDeptCode == undefined || $scope.salesmanDeptCode == "") {
                    $scope.salesmanDeptCode = '10';
                }
                $scope.temp1.invs021_dep_code = $scope.salesmanDeptCode;

                //$scope.grade_code;
                if ($scope.rcbCategories.length > 0) {

                    $http.get(ENV.apiUrl + "api/Sales_Doc/get_SubCategories?pc_parentcode=" + $scope.rcbCategories[0].category_code).then(function (res) {
                     
                        $scope.rcbSubCategories = res.data;
                        if ($scope.temp.sal_type == '04') {
                            for (var i = 0; i < $scope.rcbSubCategories.length; i++) {
                                if ($scope.rcbSubCategories[i].pc_grade_code == $scope.grade_code) {
                                    $scope.temp1['subcategory_code'] = $scope.rcbSubCategories[i].subcategory_code;

                                }
                            }
                        }
                        else {
                            $scope.temp1['subcategory_code'] = '';
                        }

                        $http.post(ENV.apiUrl + "api/Sales_Doc/postgetItemSerch", $scope.temp1).then(function (res) {
                            if (res.data.length > 0)
                                $scope.GetItems = res.data;
                            else
                                swal({ title: "Alert", text: "No Record Found.", imageUrl: "assets/img/notification-alert.png", });


                        });

                    });
                }


                

            }

            $scope.getfetch = function () {

                //var data = $scope.temp1;
                if ($scope.itemsetflg) {
                    $scope.temp1['im_assembly_ind'] = true;
                }
                else {
                    $scope.temp1['im_assembly_ind'] = false;


                }
                //$scope.temp1['im_assembly_ind'] = true;
                //$scope.temp1['im_inv_no'] = '';
                if($scope.salesmanDeptCode == undefined || $scope.salesmanDeptCode == "") {
                    $scope.salesmanDeptCode = '10';
                }
                $scope.temp1.invs021_dep_code = $scope.salesmanDeptCode;

                $http.post(ENV.apiUrl + "api/Sales_Doc/postgetItemSerch", $scope.temp1).then(function (res) {
                    if (res.data.length > 0)
                        $scope.GetItems = res.data;
                    else
                        swal({ title: "Alert", text: "No Record Found.", imageUrl: "assets/img/notification-alert.png", });


                });
            }

            $scope.okbuttonclick = function () {



                //old Code
                //for (var i = 0; i < $scope.GetItems.length; i++) {
                //    var t = $scope.GetItems[i].im_inv_no + i;
                //    var v = document.getElementById(t);

                //    if (v.checked == true) {
                //        var data =
                //   {
                //       sg_name: $scope.GetItems[i].sg_name,
                //     //  sg_code: $scope.GetItems[i].gcode,
                //       im_item_code: $scope.GetItems[i].im_item_code,
                //       im_desc: $scope.GetItems[i].im_desc,
                //       item_location_name: $scope.GetItems[i].loc_code,
                //       loc_code: $scope.GetItems[i].loc_code,
                //       original_qty: $scope.GetItems[i].id_cur_qty,
                //       dd_qty:0,
                //       dd_sell_price: $scope.GetItems[i].im_sell_price,
                //       dd_sell_value_final: 0,

                //   }

                //        $scope.itemList.push(data)

                //    }
                //}
            }

            $scope.calculateTotal = function () {
                $scope.totalFinal = 0;
                for (var i = 0; i < $scope.itemList.length; i++) {

                    $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                }
                $scope.temp['totalFinal'] = $scope.totalFinal;

                //$scope.temp.totalFinal = Math.round(parseFloat($scope.temp.totalFinal));
                $scope.temp.totalFinal = parseFloat($scope.temp.totalFinal);


            }

            $scope.Scroll = function () {
                var element = document.getElementById('table_scroll');
                element.scrollTop = element.scrollHeight - element.clientHeight;
            }


            $scope.add_selected_data=function(){
               debugger
                var vat_total = 0, line_total=0,cal_sale_value = 0;
                var inv_no = '';

                if ($scope.itemsetflg) {
                   

                    for (var i = 0; i < $scope.GetItems.length; i++) {
                        if ($scope.GetItems[i].im_inv_chk) {
                            inv_no = inv_no + $scope.GetItems[i].im_inv_no + ',';
                        }

                    }

                    $http.post(ENV.apiUrl + "api/Sales_Doc/Fetch_ItemDetails_ItemSet?im_inv_no=" + inv_no).then(function (res) {
                        setTimeout(function () {
                            $scope.Scroll();
                        }, 1000);

                        // $scope.rcbSubCategories = res.data;

                        for (var i = 0; i < res.data.length; i++) {
                 
                                if ($scope.temp.doc_type == '02') {
                                    $scope.internal_im_sell_price = 0;
                                }
                                else {
                                    $scope.internal_im_sell_price = parseFloat(res.data[i].im_sell_price).toFixed(3);
                                }

                                cal_sale_value = parseFloat($scope.internal_im_sell_price).toFixed(3) * parseFloat(res.data[i].ia_component_qty).toFixed(3);
                                vat_total = parseFloat((cal_sale_value * res.data[i].im_item_vat_percentage) / 100).toFixed(3);
                                line_total = parseFloat(cal_sale_value) + parseFloat(vat_total);

                                var data =
                                {
                                    sg_name: res.data[i].sg_name,
                                    sg_code: res.data[i].sg_name,
                                    sg_desc: res.data[i].sg_desc,
                                    im_inv_no: res.data[i].im_inv_no,
                                    im_item_code: res.data[i].im_item_code,
                                    im_desc: res.data[i].im_desc,
                                    item_location_name: res.data[i].loc_code,
                                    loc_code: res.data[i].loc_code,
                                    loc_name: res.data[i].loc_name,
                                    original_qty: res.data[i].id_cur_qty,
                                    dd_qty: res.data[i].ia_component_qty,
                                    dd_sell_price: parseFloat($scope.internal_im_sell_price).toFixed(3),
                                    dd_line_discount_pct:0,
                                    dd_line_discount_amount:0,
                                    dd_sell_value_final: parseFloat(cal_sale_value).toFixed(3),//parseFloat($scope.internal_im_sell_price).toFixed(3) * parseFloat(res.data[i].ia_component_qty),
                                    im_item_vat_percentage: res.data[i].im_item_vat_percentage,
                                    dd_item_vat_amount: parseFloat(vat_total).toFixed(3),//parseFloat((cal_sale_value * obj.im_item_vat_percentage) / 100).toFixed(3),
                                    dd_sell_value_line_total: parseFloat(line_total).toFixed(3),
                                }
                                //if (res.data[i].id_cur_qty > 0)
                                $scope.itemList.push(data)
                                                       
                        }
                        $scope.totalAmtClick();

                    });
                }

            }


            //SEARCH
            $scope.chk_click_item = function (obj, flg) {
                debugger;
                var vat_total = 0, line_total=0,cal_sale_value = 0;
                
                if ($scope.itemsetflg) {
                                   
                }
                else {

                    $scope.Exflg = false;

                    if ($scope.temp.doc_type == '02') {
                        $scope.internal_im_sell_price = parseFloat(obj.im_sell_price).toFixed(3);
                        $scope.internal_im_sell_price_final = 0;

                    }
                    else {
                        $scope.internal_im_sell_price = parseFloat(obj.im_sell_price).toFixed(3);
                        $scope.internal_im_sell_price_final = parseFloat(obj.im_sell_price).toFixed(3);

                    }
                    cal_sale_value = parseFloat($scope.internal_im_sell_price).toFixed(3) * 1;
                    vat_total = parseFloat((cal_sale_value * obj.im_item_vat_percentage) / 100).toFixed(3);

                  
                            var data =
                            {
                                sg_name: obj.sg_name,
                                //  sg_code: $scope.GetItems[i].gcode,
                                im_inv_no: obj.im_inv_no,

                                im_item_code: obj.im_item_code,
                                im_desc: obj.im_desc,
                                item_location_name: obj.loc_code,
                                loc_code: obj.loc_code,
                                original_qty: obj.id_cur_qty,
                                dd_qty: 1,
                                dd_sell_price: parseFloat($scope.internal_im_sell_price).toFixed(3),
                                dd_sell_value_final: parseFloat($scope.internal_im_sell_price_final).toFixed(3),
                                im_item_vat_percentage: obj.im_item_vat_percentage,
                                dd_item_vat_amount: parseFloat(vat_total).toFixed(3),
                                dd_sell_value_line_total: parseFloat(parseFloat(cal_sale_value) + parseFloat(vat_total)).toFixed(3),
                                dd_line_discount_pct: 0,
                                dd_line_discount_amount: 0
                            }
                   
                    if (flg) {

                        //for (var i = 0; i < $scope.itemList.length; i++) {
                        //    if ($scope.itemList[i].im_inv_no == obj.im_inv_no) {

                        //        $scope.itemList[i].dd_qty += 1;
                        //        $scope.itemList[i].dd_sell_value_final += parseFloat(obj.im_sell_price)
                        //        // $scope.itemList[i].im_inv_no
                        //        $scope.Exflg = true;
                        //        i = $scope.itemList.length;
                        //    }
                        //}

                        if ($scope.itemList.length == 0) {
                            if ($scope.Exflg == false) {                             
                                $scope.itemList.push(data);
                            }
                        }
                        else {
                            for (var j = 0; j < $scope.itemList.length; j++) {
                                if (obj.im_inv_no == $scope.itemList[j].im_inv_no) {

                                    $scope.Exflg = true;
                                    console.log("already exists");
                                    //if ($scope.Exflg == false) {
                                        //if (obj.id_cur_qty > 0) {
                                      
                                        //}
                                        //else {
                                        //  swal({ title: "Alert", text: "Current Qty is 0 so can not add item ", imageUrl: "assets/img/notification-alert.png", });
                                        //  obj['im_inv_chk'] = false;
                                        //}

                                    //}
                                }
                              
                            }
                            if($scope.Exflg == false) {
                               $scope.itemList.push(data)
                            }
                            
                        }
                        

                        $scope.totalAmtClick();
                    }
                    else {
                        //   $scope.itemList.pop(data)

                        //for (var i = 0; i < $scope.itemList.length; i++) {
                        //    if ($scope.itemList[i].im_inv_no == obj.im_inv_no) {
                        //        if ($scope.itemList[i].dd_qty > 1) {
                        //            $scope.itemList[i].dd_qty -= 1;
                        //            $scope.itemList[i].dd_sell_value_final -= parseFloat(obj.im_sell_price)
                        //        }
                        //        else
                        //            $scope.itemList.splice(i, 1);
                        //    }
                        //}

                        for (var i = 0; i < $scope.itemList.length; i++) {
                            if ($scope.itemList[i].im_inv_no == obj.im_inv_no) {                                
                               
                            }
                            else {
                                $scope.itemList.splice(i, 1);
                            }
                        }
                        $scope.totalAmtClick();

                    }
                }

            }

            $scope.addItemFromInvoice = function (obj,flag) {
                debugger
                console.log("obj", obj);
                $scope.disableOnInvoiceReturn = true;
                $scope.disable_total_dis_per = true;
                $scope.searchDisable = true;
                $scope.showBtnOnInvoiceReturn = true;
                $scope.disableReturnBtn = false;
                $scope.old_doc_prov_no_show = true;
                $scope.disableSaveReturnBtn = false;
                $scope.selectedInvoieList = [];
                $scope.itemList = [];
                if (flag) {

                    if ($scope.old_obj != undefined) {
                        $scope.selectedInvoieList.pop($scope.old_obj);
                        
                        if ($scope.old_obj != obj)
                            $scope.old_obj.invoice_no_chk = false;
                        $scope.selectedInvoieList.push(obj);
                    }
                    else {
                        $scope.selectedInvoieList.push(obj);
                    }

                    for (var i in obj.sublist1) {

                        console.log(obj.sublist1[i]);


                        var data =
                        {
                            sg_desc: obj.sublist1[i].sg_name,
                            //  sg_code: $scope.GetItems[i].gcode,
                            im_inv_no: obj.sublist1[i].im_inv_no,

                            im_item_code: obj.sublist1[i].im_item_code,
                            im_desc: obj.sublist1[i].im_desc,
                            loc_name: obj.sublist1[i].loc_code,
                            loc_code: obj.sublist1[i].loc_code,
                            original_qty: obj.sublist1[i].id_cur_qty,
                            dd_qty: obj.sublist1[i].dd_qty,
                            dd_sell_price: parseFloat(obj.sublist1[i].dd_sell_price).toFixed(3),  //parseFloat($scope.internal_im_sell_price).toFixed(3),
                            dd_sell_value_final: parseFloat(obj.sublist1[i].dd_sell_value_final).toFixed(3),       //parseFloat($scope.internal_im_sell_price_final).toFixed(3),
                            im_item_vat_percentage: parseFloat(obj.sublist1[i].dd_item_vat_per).toFixed(3),  // obj.im_item_vat_percentage,
                            dd_item_vat_amount: parseFloat(obj.sublist1[i].dd_item_vat_amount).toFixed(3),
                            dd_sell_value_line_total: parseFloat(obj.sublist1[i].dd_line_total).toFixed(3), //parseFloat(parseFloat(cal_sale_value) + parseFloat(vat_total)).toFixed(3),
                            dd_line_discount_pct: parseFloat(obj.sublist1[i].dd_line_discount_pct).toFixed(3),
                            dd_line_discount_amount: parseFloat(obj.sublist1[i].dd_line_discount_amount).toFixed(3),
                            doc_dept: obj.sublist1[i].doc_dept,
                            doc_remark: obj.sublist1[i].doc_remark, 
                            dd_physical_qty: obj.sublist1[i].dd_physical_qty,
                            invoice_qty: obj.sublist1[i].dd_qty,
                            doc_status: obj.sublist1[i].doc_status,
                            dd_status: obj.sublist1[i].dd_status
                        }

                        $scope.itemList.push(data);
                    }

                    $scope.temp.overalldis = obj.sublist1[0].doc_discount_pct;
                    $scope.temp.disamt = obj.sublist1[0].doc_discount_amount;
                    $scope.temp.otherCharges = obj.sublist1[0].doc_other_charge_amount;
                    $scope.temp.totalFinal = obj.sublist1[0].doc_total_amount;
                    $scope.doc_vat_total_amount = obj.sublist1[0].doc_vat_total_amount;
                    $scope.depCode = obj.sublist1[i].doc_dept;

                    $scope.temp.paymentMode = obj.sublist1[0].dd_payment_mode;
                    console.log("paymentMode", $scope.temp.paymentMode);

                    if ($scope.temp.paymentMode == 'CH' || $scope.temp.paymentMode == 'DD' || $scope.temp.paymentMode == 'CC' || $scope.temp.paymentMode == 'BT') {
                        $scope.temp.chequeNo = obj.sublist1[0].chequeNo;
                        $scope.temp.chequeDate = obj.sublist1[0].chequeDate;
                        $scope.temp.bankCode = obj.sublist1[0].bankCode;
                    }
                    if ($scope.temp.paymentMode == 'CP') {
                        $scope.temp.chequeNo = obj.sublist1[0].chequeNo;
                    }

                    $scope.temp.enroll = obj.enroll_no;
                    $scope.temp.name = obj.student_name;
                    $scope.temp.oderRefNo = obj.sublist1[0].doc_order_ref_no;
                    $scope.old_doc_prov_no = obj.invoice_no;
                    $scope.doc_reference_prov_no = obj.doc_reference_prov_no;

                    $scope.old_obj = obj;
                }
                else {
                   $scope.selectedInvoieList.pop(obj);
                }

                console.log("itemList", $scope.itemList);
            }



            $scope.CancelItem = function () {
                $scope.temp['finalValue'] = ''
                $scope.temp['im_sell_price'] = ''
                $scope.temp['qty'] = ''
                $scope.temp['loc_code'] = ''
                $scope.temp['subcategory_code'] = ''
                $scope.temp['category_code'] = ''
                $scope.temp['gcode'] = ''
                $scope.temp['doc_remark'] = ''
                $scope.rcbItems = [];
                //  $scope.rcbCategories = [];
                $scope.rcbSubCategories = [];
                $scope.im_item_code1 = '';
                $("#rcbItem").select2("val", "");


                $scope.rcbItems = $scope.rcbitemOld;

                $scope.disable_btn1 = true;

                $scope.showPickInvoice = false;
                $scope.old_doc_prov_no_show = false;
                

                //$http.get(ENV.apiUrl + "api/Sales_Doc/GetAllItemsInSubCategoryNew?pc_code=" + '').then(function (res) {

                //    $scope.rcbItems = res.data;

                //});


            }

            $scope.studentSearch = function () {
                debugger;
                if ($scope.temp.sal_type == '05') {
                    $('#ExternalCustomerSearch').modal('show');
                }
                else {
                    $scope.global_Search_click();
                    $('#Global_Search_Modal').modal({ backdrop: "static" });
                    //$('#stdSearch').modal('show');
                }
            }

            $scope.totalAmtClick = function () {
                debugger;
                $scope.totalFinal = 0;
                $scope.totalVAT = 0;
                $scope.total_dis = 0;
                for (var i = 0; i < $scope.itemList.length; i++) {

                    $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_line_total);
                    $scope.total_dis += parseFloat($scope.itemList[i].dd_line_discount_amount);
                    $scope.totalVAT += parseFloat($scope.itemList[i].dd_item_vat_amount);
                }

                $scope.temp['totalFinal'] = parseFloat($scope.totalFinal).toFixed(3);
                $scope.doc_vat_total_amount = parseFloat($scope.totalVAT).toFixed(3);

                if ($scope.total_dis != 0) {
                    $scope.temp.overalldis = '';
                    $scope.temp.disamt = parseFloat($scope.total_dis).toFixed(3);
                    $scope.disable_total_dis_per = true;
                }
                else if ($scope.temp.overalldis == undefined || $scope.temp.overalldis == '') {
                    $scope.temp.disamt = '';
                    $scope.temp.overalldis = '';
                    $scope.disable_total_dis_per = false;
                }
                //else {
                //    $scope.temp.disamt = '';
                //    $scope.temp.overalldis = '';
                //    $scope.disable_total_dis_per = false;
                //}
                
                if ($scope.temp.otherCharges == undefined || $scope.temp.otherCharges == '') {
                }
                else {
                    $scope.temp.totalFinal = parseFloat($scope.temp.totalFinal) + parseFloat($scope.temp.otherCharges);
                }

                if ($scope.temp.disamt == undefined || $scope.temp.disamt == '') {
                }
                else if ($scope.total_dis == 0) {
                    $scope.temp.totalFinal = parseFloat($scope.temp.totalFinal) - parseFloat($scope.temp.disamt);
                }


                //$scope.temp.totalFinal = Math.round(parseFloat($scope.temp.totalFinal));

                $scope.temp.totalFinal = parseFloat($scope.temp.totalFinal).toFixed(2);

                //if ($scope.temp.totalFinal > 0) {
                //    var f_value = String($scope.temp.totalFinal);
                //    var dec_list = f_value.split(".");
                //    var rnd_val = 0, t_amt = 0;
                //    round_diff_val = 0;
                //    // if (dec_list.length > 0) {
                //    if (dec_list[1].length > 0) {
                //        t_amt = dec_list[0];
                //        rnd_val = dec_list[1];

                //        console.log("rnd_val",rnd_val);
                //        if (rnd_val > 0 && rnd_val <= 25) {
                //            round_diff_val = 25 - rnd_val;
                //            rnd_val = 25;
                //            t_amt = t_amt + '.' + rnd_val;
                //        }
                //        else if (rnd_val > 25 && rnd_val <= 50) {
                //            round_diff_val = 50 - rnd_val;
                //            rnd_val = 50;
                //            t_amt = t_amt + '.' + rnd_val;
                //        }
                //        else if (rnd_val > 50 && rnd_val <= 75) {
                //            round_diff_val = 75 - rnd_val;
                //            rnd_val = 75;
                //            t_amt = t_amt + '.' + rnd_val;
                //        }
                //        else if (rnd_val > 75) {
                //            round_diff_val = 100 - rnd_val;
                //            rnd_val = 0;
                //            t_amt = parseFloat(t_amt) + 1;
                //        }
                //        $scope.temp.totalFinal = parseFloat(t_amt).toFixed(3);
                //    }
                //    //}
                //}


                $scope.temp.totalFinal = parseFloat($scope.temp.totalFinal).toFixed(3);
                

            }


            $scope.otherChargesChange = function () {
                $scope.totalAmtClick();

            }

            $scope.txt_discounted_price_TextChanged = function (str) {
                debugger;
                if (str <= 100 || str <= '100') {

                    $scope.totalFinal = 0;                
                    for (var i = 0; i < $scope.itemList.length; i++) {

                        $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_line_total);
                    }
                    if (str == undefined || str == '') {
                        $scope.temp['disamt'] = 0;
                    }
                    else {
                        $scope.temp['disamt'] = parseFloat((parseFloat($scope.totalFinal) * parseFloat(str)) / 100).toFixed(3);
                    }
                    $scope.df = parseFloat(parseFloat($scope.totalFinal) - parseFloat($scope.temp['disamt'])).toFixed(3);

                    $scope.totalAmtClick();
                }
                else {
                   
                    $scope.temp['overalldis'] = 0;
                    $scope.temp['disamt'] = 0;
                   
                    swal({ title: "Alert", text: "Discount Should not be more than 100%.", imageUrl: "assets/img/notification-alert.png", });

                    $scope.totalAmtClick();

                }

                
            }

            $scope.paymentChange = function (str) {
                $scope.cardpay = false;
                if (str == 'CA') {
                    $scope.chequeShow = false;
                    $scope.bankShow = false;
                }
                else if (str == 'CH' || str == 'DD' || str == 'CC') {
                    $scope.chequeShow = true;
                    $scope.bankShow = false;
                }
                else if (str == 'BT') {
                    $scope.chequeShow = false;
                    $scope.bankShow = true;
                }

                else if (str == 'CP') {
                    $scope.chequeShow = false;
                    $scope.bankShow = false;
                    $scope.cardpay = true;

                }

            }

            $scope.removeItem = function (obj, index) {

                $scope.itemList.splice(index, 1);
                $scope.totalAmtClick();
            }

            $scope.onlyNumbers = function (event) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
        }])
})();