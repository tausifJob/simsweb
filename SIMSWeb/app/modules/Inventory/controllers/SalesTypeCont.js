﻿(function () {
    'use strict';
    var opr = '';
    var data1 = [];
    var main, edt;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SalesTypeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.edt = "";
            $scope.edt1 = "";
            $scope.SalesTypeDetail = true;
            $scope.editmode = false;
            var formdata = new FormData();
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });



            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.SalesData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.cancel = function () {
                $scope.SalesTypeDetail = true;
                $scope.SalesTypeData = false;
                //$scope.bin_desc = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    opr = 'S';
                    $scope.editmode = false;
                    $scope.edt = "";
                    $scope.edt1 = "";
                    $scope.newmode = true;
                    $scope.readonly = false;
                    $scope.SalesTypeDetail = false;
                    $scope.SalesTypeData = true;
                    $scope.savebtn = true;
                    $scope.updatebtn = false;
                    $scope.myForm.$setPristine();
                    $scope.myForm.$setUntouched();
                    $scope.edt = {
                        salesind: 'S',
                    }


                }
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                console.log("begin=" + begin); console.log("end=" + end);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.SalesData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.SalesData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.check_all();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                    item.sal_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sal_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sal_indicator.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sale_acno.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.cost_acno.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.cost_acno == toSearch) ? true : false;

            }

            $http.get(ENV.apiUrl + "api/SalesTypeDetail/getSalesTypeDetail").then(function (Sales_Data) {
                $scope.SalesData = Sales_Data.data;
                $scope.totalItems = $scope.SalesData.length;
                $scope.todos = $scope.SalesData;
                $scope.makeTodos();
                console.log($scope.SalesData);
            });

            //$http.get(ENV.apiUrl + "api/SalesTypeDetail/getAccountNo").then(function (Account_Data) {

            //    $scope.AccountData = Account_Data.data;
            //    console.log($scope.AccountData);
            //});

            $http.get(ENV.apiUrl + "api/SalesTypeDetail/GetGLAccountNumber").then(function (docstatus1) {
                $scope.GlACNO = docstatus1.data;
                console.log($scope.GlACNO);
            });

            setTimeout(function () {
                $("#cmb_sales_account_no").select2();
            }, 100);

            setTimeout(function () {
                $("#cmb_cost_account_no").select2();
            }, 100);

            $scope.Save = function (myForm) {
                if (myForm) {
                    debugger
                    var data = {
                        sal_type: $scope.edt.sal_type,
                        sal_name: $scope.edt.sal_name,
                        salesind: $scope.edt.salesind,
                        acno: $scope.edt.sale_acno,
                        acno1: $scope.edt1.cost_acno,
                        opr: 'I',

                    }



                    $scope.exist = false;
                    data1.push(data);
                    for (var i = 0; i < $scope.SalesData.length; i++) {
                        if ($scope.SalesData[i].sal_type == data.sal_type) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ text: "Record Already exists", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                    else {
                        $http.post(ENV.apiUrl + "api/SalesTypeDetail/SalesTypeCUD?simsobj=", data1).then(function (msg) {
                            $scope.BinssData = false;
                            $http.get(ENV.apiUrl + "api/SalesTypeDetail/getSalesTypeDetail").then(function (Sales_Data) {
                                $scope.SalesData = Sales_Data.data;
                                $scope.totalItems = $scope.SalesData.length;
                                $scope.todos = $scope.SalesData;
                                $scope.makeTodos();
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        });
                        $scope.SalesTypeDetail = true;
                        $scope.SalesTypeData = false;
                    }
                    data1 = [];
                }
            }

            $scope.up = function (str) {
                debugger
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    opr = 'U';
                    $scope.editmode = true;
                    $scope.newmode = false;
                    $scope.savebtn = false;
                    $scope.updatebtn = true;
                    $scope.SalesTypeDetail = false;
                    $scope.SalesTypeData = true;
                    $scope.readonly = true;
                    $scope.edt = {
                        sal_type: str.sal_type
                                  , sal_name: str.sal_name
                                  , salesind: str.sal_indicator
                                  , sale_acno: str.sale_acno


                    };
                    $scope.edt1 = {
                        cost_acno: str.cost_acno
                    };

                }
            }

            $scope.Update = function (myForm) {
                debugger
                if (myForm) {

                    var data = {
                        sal_type: $scope.edt.sal_type,
                        sal_name: $scope.edt.sal_name,
                        salesind: $scope.edt.salesind,
                        acno: $scope.edt.sale_acno,
                        acno1: $scope.edt1.cost_acno,
                        opr: 'U',

                    }

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/SalesTypeDetail/SalesTypeCUD?simsobj=", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.BoardData1 = false;
                        $http.get(ENV.apiUrl + "api/SalesTypeDetail/getSalesTypeDetail").then(function (Sales_Data) {
                            $scope.SalesData = Sales_Data.data;
                            $scope.totalItems = $scope.SalesData.length;
                            $scope.todos = $scope.SalesData;
                            formdata = new FormData();
                            $scope.makeTodos();

                            if ($scope.msg1 == true) {
                                swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + scope.msg1)
                            }
                        });

                    })
                    $scope.SalesTypeData = false;
                    $scope.SalesTypeDetail = true;
                    data1 = [];

                }
            }

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }
            $scope.check_once = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }



            $scope.deleterecord = function () {
                debugger;
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.flag = false;
                    var deleteleave = [];
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodulecode = ({
                                'sal_type': $scope.filteredTodos[i].sal_type,
                                opr: 'D'
                            });
                            deleteleave.push(deletemodulecode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            confirmButtonText: 'Yes',
                            showCancelButton: true,
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                $http.post(ENV.apiUrl + "api/SalesTypeDetail/SalesTypeCUD", deleteleave).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/SalesTypeDetail/getSalesTypeDetail").then(function (Sales_Data) {
                                                    $scope.SalesData = Sales_Data.data;
                                                    $scope.totalItems = $scope.SalesData.length;
                                                    $scope.todos = $scope.SalesData;
                                                    $scope.makeTodos();
                                                });
                                                main = document.getElementById('all_chk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    $scope.row1 = '';
                                                }
                                            }

                                            $scope.currentPage = true;
                                        });
                                    }
                                    else {
                                        swal({ text: "Record Already Mapped, can't Delete. " + scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/SalesTypeDetail/getSalesTypeDetail").then(function (Sales_Data) {
                                                    $scope.SalesData = Sales_Data.data;
                                                    $scope.totalItems = $scope.SalesData.length;
                                                    $scope.todos = $scope.SalesData;
                                                    $scope.makeTodos();
                                                });
                                                main = document.getElementById('all_chk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    $scope.row1 = '';
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                            else {
                                main = document.getElementById('all_chk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById(i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    $scope.currentPage = str;
                }
            }


        }]);

})();



