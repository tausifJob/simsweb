﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [], data = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Inventory');
    simsController.controller('ReceiveRfqCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.req_no = [];
            $scope.selected_reqtype = '';
            var rfq = '10170003';
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);
            $timeout(function () {
                $("#fixTable2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.lcshow = false;
            $scope.chkMulti = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;
            $scope.cdetails_data = [];

            var date = new Date();
            $scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');

            $scope.temp =
                {
                    rfq_date: $scope.ddMMyyyy,
                    expected_date: $scope.ddMMyyyy,
                    dep_code: '',
                    servicevalue: '',
                }

            //*********************All Combo*********************

            $http.get(ENV.apiUrl + "api/CreateOrder/GetDepartment").then(function (res1) {
                $scope.Department = res1.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetServiceTypes").then(function (res2) {
                $scope.ServiceTypes = res2.data;
            });

            $scope.order_report_name = '';

            //***************************************************

            $scope.size = function (str) {

                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.cdetails_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.cdetails_data;
                }
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            function searchUtil(item, toSearch) {
                /* Search Text in all 2 fields */
                return (item.invs058_im_inv_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.invs058_rd_item_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.SearchRrq = function () {


                $http.get(ENV.apiUrl + "api/CreateOrder/GetSuppliers").then(function (res11_sup) {

                    $scope.Suppliers_srch = res11_sup.data;
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/GetAllItemsInSubCategoryNew?dep_code=" + $scope.temp.dep_code + "&req_type=" + $scope.temp.servicevalue).then(function (res_item) {
                    $scope.rcbItems = res_item.data;
                });

                setTimeout(function () {
                    $("#rcbItem").select2();
                    $("#rcbItem1").select2();

                }, 100);


                if ($scope.temp.dep_code != undefined && $scope.temp.servicevalue != undefined) {
                    $scope.totAmt = 0;
                    // $('#MyModal1').modal('show');
                    $('#MyModal1').modal({ backdrop: 'static', keyboard: true });
                    $scope.srchsupplier = '';
                    $scope.srchitemcode = '';
                    $scope.srch_req_no = '';
                    var im_inv_no = '';
                    var im_item_code = '';
                    var im_desc = '';

                    $http.get(ENV.apiUrl + "api/CreateOrder/Get_ALLRFQDetails?dep_code=" + $scope.temp.dep_code + "&req_type=" + $scope.temp.servicevalue + "&srch_supcode=" + $scope.srchsupplier + "&srch_itemdesc=" + im_desc + "&srch_itemcode=" + im_item_code).then(function (res12) {

                        $scope.search_Req_Data = res12.data;
                        $scope.search_Req_Data1 = angular.copy(res12.data);
                        $scope.cdetails_data = $scope.search_Req_Data1;
                        $scope.totalItems = $scope.cdetails_data.length;
                        $scope.todos = $scope.cdetails_data;
                        $scope.makeTodos();

                        //console.log($scope.search_Req_Data);
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Department and Request Type", width: 300, height: 200 });
                }
            }

            $scope.makeTodos = function () {

                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].req_no1 = false;
                }

            };

            $scope.RemoveEnrollMentNo = function ($event, index, str) {
                str.splice(index, 1);
            }

            $scope.search_Req_Data_all = [];
            $scope.testALL = [];

            $scope.okbuttonclick = function () {


                var depc = $scope.temp.dep_code;
                var serv = $scope.temp.servicevalue;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].req_no1 == true) {



                        $scope.temp = $scope.filteredTodos[i];
                        $scope.temp.dep_code = depc;
                        $scope.temp.servicevalue = serv;
                        $scope.testALL = $scope.filteredTodos[i].lst_rfq_details;

                        $scope.Main_table = true;
                    }
                }

                $scope.srchsupplier = null;
                $scope.srchitemcode = null;
                $scope.srch_req_no = null;
            }

            $scope.SearchRequests = function () {

                var im_inv_no = $scope.srchitemcode.im_inv_no;
                var im_item_code = $scope.srchitemcode.im_item_code;
                var im_desc = $("#rcbItem").find("option:selected").text();

                if (im_item_code == undefined)
                    im_item_code = '';

                $http.get(ENV.apiUrl + "api/CreateOrder/Get_ALLRFQDetails?dep_code=" + $scope.temp.dep_code + "&req_type=" + $scope.temp.servicevalue + "&srch_supcode=" + $scope.srchsupplier + "&srch_itemdesc=" + im_desc + "&srch_itemcode=" + im_item_code).then(function (res12) {

                    $scope.search_Req_Data = res12.data;
                    $scope.search_Req_Data1 = angular.copy(res12.data);
                    $scope.cdetails_data = $scope.search_Req_Data1;
                    $scope.totalItems = $scope.cdetails_data.length;
                    $scope.todos = $scope.cdetails_data;
                    $scope.makeTodos();

                    //console.log($scope.search_Req_Data);
                });
            }

            $scope.ResetRequests = function () {
                $scope.srchsupplier = '';
                $scope.srchitemcode = '';
                $scope.srch_req_no = '';
                $("#rcbItem").select2("val", "");
                var im_inv_no = '';
                var im_item_code = '';
                var im_desc = '';

                $http.get(ENV.apiUrl + "api/CreateOrder/Get_ALLRequestDetails?dep_code=" + $scope.temp.dep_code + "&req_type=" + $scope.temp.servicevalue + "&srch_supcode=" + $scope.srchsupplier + "&srch_itemdesc=" + im_desc + "&srch_itemcode=" + im_item_code + "&srch_req_no=" + $scope.srch_req_no).then(function (res12) {

                    $scope.search_Req_Data = res12.data;
                    $scope.search_Req_Data1 = angular.copy(res12.data);
                    $scope.cdetails_data = $scope.search_Req_Data1;
                    $scope.totalItems = $scope.cdetails_data.length;
                    $scope.todos = $scope.cdetails_data;
                    $scope.makeTodos();

                    //console.log($scope.search_Req_Data);
                });
            }

            $scope.Cancel1 = function () {

                $scope.grid_Display();
                datasend = [];
                $scope.selected_reqtype = '';

                $scope.filteredTodos = [];
                $scope.testALL = [];

                $scope.temp =
                {
                    rfq_date: $scope.ddMMyyyy,
                    expected_date: $scope.ddMMyyyy,
                    dep_code: '',
                    servicevalue: '',
                }

                $scope.table = true;
                $scope.display = false;
                $state.go('main.RcvRFQ');
            }

            //DATA CANCEL
            $scope.Cancel = function () {

                $scope.temp = "";
                $scope.search_Req_Data = null;
                $scope.testALL = null;
                $state.go('main.CrtRFQ');
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            //DATA SAVE INSERT
            var datasend = [];
            var datasend1 = [];
            var datasend2 = [];
            var gofurther = true;

            $scope.removeItem = function (obj, index) {

                $scope.testALL.splice(index, 1);
                $scope.orcalculateamt12();
            }

            $scope.savedata = function () {

                for (var i = 0; i < $scope.testALL.length; i++) {
                    var data = {
                        'rfq_no': $scope.testALL[i].rfq_no,
                        'req_no': $scope.testALL[i].req_no,
                        'rfq_line_no': $scope.testALL[i].rfq_line_no,
                        'rfq_rd_quantity': $scope.testALL[i].rfq_rd_quantity,
                        'pqd_quoted_qty': $scope.testALL[i].pqd_quoted_qty,
                        'pqd_quoted_price': $scope.testALL[i].pqd_quoted_price,
                        'rfq_supply_wanted_date': $scope.testALL[i].rfq_supply_wanted_date,
                        'pqd_quoted_supply_date': $scope.testALL[i].pqd_quoted_supply_date,
                        'rfq_remarks_details': $scope.testALL[i].rfq_remarks_details,
                        'terms': $scope.temp.terms,
                        'depcode': $scope.temp.dep_code,
                        'im_inv_no': $scope.testALL[i].im_inv_no,
                        'rfq_type': $scope.temp.servicevalue,
                    }
                    datasend.push(data);
                }

                if (datasend.length > 0) {
                    $http.post(ENV.apiUrl + "api/CreateOrder/SaveQuotationDetails", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        swal({ text: "Purchase Quotation Created Successfully.", imageUrl: "assets/img/check.png", width: 300, height: 200 });


                        datasend = [];
                        $scope.filteredTodos = [];
                        $scope.testALL = [];

                        $scope.temp =
                        {
                            rfq_date: $scope.ddMMyyyy,
                            expected_date: $scope.ddMMyyyy,
                            dep_code: '',
                            servicevalue: '',
                        }

                        $scope.table = true;
                        $scope.display = false;
                        $state.go('main.RcvRFQ');
                    });
                }
            }

            $scope.onlyNumbers = function (event, str) {

                if (str > 100) {
                    swal({ title: "Alert", text: "Invalid Discount", width: 300, height: 200 });
                }

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $scope.Report = function (shipment_no) {

                var data = {
                    location: $scope.order_report_name,
                    parameter: {
                        ship: ord_no,
                    },
                    state: 'main.Inv044',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
            }

            $scope.printConfrimDetails = function (str) {
                var data = {
                    location: 'Invs.INVR17',
                    parameter: { ord_no: str },
                    state: 'main.Inv044',
                    reg: param,
                    email: email
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)

                // printDetails
                $state.go('Report');
            }

            var dom;
            $scope.flag = true;
            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                         "<tr>   <td class='semi-bold'>" + "" + "</td>  <td class='semi-bold'>" + "Request No." + "</td> <td class='semi-bold'>" + "Request Line No." + " </td><td class='semi-bold'>" + "Item Desc" + "</td>" +
                        "<td class='semi-bold'>" + "UOM" + "</td> <td class='semi-bold'>" + "Quantity" + " </td>" + "<td class='semi-bold'>" + "Ordered Quantity" + " </td>" + "<td class='semi-bold'>" + "Supplier Price" + " </td>" + "<td class='semi-bold'>" + "Discount" + " </td>" + "<td class='semi-bold'>" + "Remark" + " </td>" + "</tr>" +

                          "<tr> <td>" + '<input type="checkbox" value="1" ng-click="SingleCheckSave()">' +
                         "<td>" + (info.invs058_req_no) + "</td> <td>" + (info.invs058_rd_line_no) + " </td><td>" + (info.rd_item_desc) + "</td>" +
                         "<td>" + (info.uom_code) + "</td> <td>" + (info.rd_quantity) + "</td>" + "<td>" + '<input style="width: 90px; height: 25px" aria-controls="example" ng-model="info.pays_appl_form_field_value2" class="form-control input-sm">' + "<td>" + '<input style="width: 90px; height: 25px" aria-controls="example" ng-model="info.pays_appl_form_field_value2" class="form-control input-sm">' + "<td>" + '<input style="width: 90px; height: 25px" aria-controls="example" ng-model="info.pays_appl_form_field_value2" class="form-control input-sm">' + "<td>" + (info.rd_remarks) + " </td>" + "</tr>" +

                        " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.req_no.length; i++) {
                        $scope.req_no[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }

            };

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.adddata = function (chkvalue, info) {

                if ($scope.temp.servicevalue == 'I') {
                    if (chkvalue == true) {
                        info.servicevalue = false;
                    }
                    else if (chkvalue == false) {
                        info.servicevalue = true;
                    }
                }
                else { info.servicevalue = true; }

                $scope.totAmt = 0;
                var totalamt = 0;
                for (var a = 0; a < $scope.testALL.length; a++) {
                    if ($scope.testALL[a].invs058_rd_line_no1 == true) {
                        if ($scope.testALL[a].invs058_ordered_quantity != undefined && $scope.testALL[a].invs058_sup_price != undefined) {

                            totalamt = totalamt + $scope.testALL[a].invs058_rqvalue;
                        }
                    }
                }

                if (totalamt != "NaN") {
                    $scope.totAmt = totalamt;
                } else { $scope.totAmt = 0; }
            }

            $scope.calamount = function (info, va) {

                $scope.totAmt = 0;
                if (va == 'amt') {
                    if (info.invs058_discount_amt != '' && info.invs058_discount_amt != undefined) {
                        info.invs058_discount = '';
                    }
                }
                if (va == 'dis') {
                    if (info.invs058_discount != '' && info.invs058_discount != undefined) {
                        if (info.invs058_discount > 100) {
                            info.invs058_discount = '';
                            swal({ title: "Alert", text: "Invalid Discount", width: 300, height: 200 });
                        }
                        info.invs058_discount_amt = '';
                    }
                }

                var totalamt = 0;
                var totalgrossamt = 0;
                var totalnetamt = 0;

                var totalamt1 = 0;
                var totalamt_gross1 = 0;
                for (var i = 0; i < $scope.testALL.length; i++) {
                    //if ($scope.testALL[i].invs058_rd_line_no1 == true) {
                    if ($scope.temp.servicevalue == 'I') {


                        if ($scope.testALL[i].invs058_ordered_quantity != undefined && $scope.testALL[i].invs058_ordered_quantity != '') {
                            if ($scope.testALL[i].invs058_sup_price != undefined && $scope.testALL[i].invs058_sup_price != '') {

                                totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                $scope.testALL[i].invs058_gross_amt = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;

                                totalamt_gross1 = $scope.testALL[i].invs058_gross_amt;

                                if ($scope.testALL[i].invs058_discount != undefined && $scope.testALL[i].invs058_discount != '') {
                                    var disamt = ((totalamt1 * $scope.testALL[i].invs058_discount) / 100);
                                    $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - disamt;
                                    totalamt1 = totalamt1 - disamt;
                                }
                                if ($scope.testALL[i].invs058_discount_amt != undefined && $scope.testALL[i].invs058_discount_amt != '') {
                                    var disamt = $scope.testALL[i].invs058_discount_amt;
                                    $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - $scope.testALL[i].invs058_discount_amt;
                                    totalamt1 = totalamt1 - disamt;
                                }
                                totalnetamt = totalnetamt + totalamt1;
                                totalgrossamt = totalgrossamt + totalamt_gross1;
                            }
                            else {
                                $scope.testALL[i].invs058_rqvalue = 0;
                                $scope.testALL[i].invs058_gross_amt = 0;
                            }
                        }
                    }

                    totalamt = totalamt + totalamt1;
                    $scope.total_netamt = totalamt1;
                    $scope.total_grossamt = totalgrossamt;
                    totalamt1 = 0;
                    totalamt_gross1 = 0;
                    // }
                }

                if (totalamt != NaN) {
                    $scope.totAmt = totalamt;
                    $scope.total_grossamt = totalgrossamt;
                    $scope.total_netamt = totalnetamt;
                }
                else { $scope.totAmt = 0; }


            }

            $scope.orcalculateamt = function (str) {


                var ttval1 = str.or_atstatus;
                if (ttval1 == 'P') {
                    if (str.or_atamount != 0 && str.or_atamount != '' && str.or_atamount != undefined) {
                        for (var j = 0; j < $scope.OrderAttrs.length; j++) {
                            if ($scope.OrderAttrs[j].or_atstatus == 'S') {
                                $scope.OrderAttrs[j].or_atamount = 0;
                            }
                        }
                        if (str.or_atamount > 100) {
                            str.or_atamount = '';
                            swal({ title: "Alert", text: "Invalid Discount", width: 300, height: 200 });
                        }
                    }
                }

                if (ttval1 == 'S') {
                    if (str.or_atamount != 0 && str.or_atamount != '' && str.or_atamount != undefined) {
                        for (var j = 0; j < $scope.OrderAttrs.length; j++) {
                            if ($scope.OrderAttrs[j].or_atstatus == 'P') {
                                $scope.OrderAttrs[j].or_atamount = 0;
                            }
                        }
                    }
                }
                var totalamt = 0;
                var totalgrossamt = 0;
                var totalnetamt = 0;

                var totalamt1 = 0;
                var totalamt_gross1 = 0;

                for (var i = 0; i < $scope.testALL.length; i++) {

                    //if ($scope.testALL[i].invs058_rd_line_no1 == true) {
                    if ($scope.temp.servicevalue == 'I') {


                        if ($scope.testALL[i].invs058_ordered_quantity != undefined && $scope.testALL[i].invs058_ordered_quantity != '') {
                            if ($scope.testALL[i].invs058_sup_price != undefined && $scope.testALL[i].invs058_sup_price != '') {

                                totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                $scope.testALL[i].invs058_gross_amt = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;

                                totalamt_gross1 = $scope.testALL[i].invs058_gross_amt;

                                if ($scope.testALL[i].invs058_discount != undefined && $scope.testALL[i].invs058_discount != '') {
                                    var disamt = ((totalamt1 * $scope.testALL[i].invs058_discount) / 100);
                                    $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - disamt;
                                    totalamt1 = totalamt1 - disamt;
                                }
                                if ($scope.testALL[i].invs058_discount_amt != undefined && $scope.testALL[i].invs058_discount_amt != '') {
                                    var disamt = $scope.testALL[i].invs058_discount_amt;
                                    $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - $scope.testALL[i].invs058_discount_amt;
                                    totalamt1 = totalamt1 - disamt;
                                }
                                totalnetamt = totalnetamt + totalamt1;
                                totalgrossamt = totalgrossamt + totalamt_gross1;
                            }
                            else {
                                $scope.testALL[i].invs058_rqvalue = 0;
                                $scope.testALL[i].invs058_gross_amt = 0;
                            }
                        }
                    }

                    totalamt = totalamt + totalamt1;
                    $scope.total_netamt = totalamt1;
                    $scope.total_grossamt = totalgrossamt;
                    totalamt1 = 0;
                    totalamt_gross1 = 0;
                }

                if ($scope.temp['invs058_order_apply_aexp'] == true) {//if checkbox is clicked

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var add = 0;

                            totalamt += (parseFloat(add));
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'P') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - ((parseFloat(totalamt) * parseFloat(percentage)) / 100);
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'S') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - $scope.OrderAttrs[i].or_atamount;
                        }
                    }
                }

                if ($scope.temp['invs058_order_apply_aexp'] == false) {//if checkbox is not clicked

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'P') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - ((parseFloat(totalamt) * parseFloat(percentage)) / 100);
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'S') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - $scope.OrderAttrs[i].or_atamount;
                        }


                        //if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                        //    if ($scope.OrderAttrs[i].or_atamount != '')
                        //        var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                        //    else
                        //        var add = 0;

                        //    totalamt += (parseFloat(add));
                        //}

                    }

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var add = 0;

                            totalamt += (parseFloat(add));
                        }
                    }
                }


                $scope.totAmt = totalamt;


            }

            $scope.orcalculateamt12 = function () {

                var totalamt = 0;
                var totalgrossamt = 0;
                var totalnetamt = 0;

                var totalamt1 = 0;
                var totalamt_gross1 = 0;
                $scope.total_grossamt = '';
                $scope.total_netamt = '';

                for (var i = 0; i < $scope.testALL.length; i++) {
                    //if ($scope.testALL[i].invs058_rd_line_no1 == true) {
                    if ($scope.temp.servicevalue == 'I') {
                        if ($scope.testALL[i].invs058_ordered_quantity != undefined && $scope.testALL[i].invs058_ordered_quantity != '') {
                            if ($scope.testALL[i].invs058_sup_price != undefined && $scope.testALL[i].invs058_sup_price != '') {

                                totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                $scope.testALL[i].invs058_gross_amt = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;

                                totalamt_gross1 = $scope.testALL[i].invs058_gross_amt;

                                if ($scope.testALL[i].invs058_discount != undefined && $scope.testALL[i].invs058_discount != '') {
                                    var disamt = ((totalamt1 * $scope.testALL[i].invs058_discount) / 100);
                                    $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - disamt;
                                    totalamt1 = totalamt1 - disamt;
                                }
                                if ($scope.testALL[i].invs058_discount_amt != undefined && $scope.testALL[i].invs058_discount_amt != '') {
                                    var disamt = $scope.testALL[i].invs058_discount_amt;
                                    $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - $scope.testALL[i].invs058_discount_amt;
                                    totalamt1 = totalamt1 - disamt;
                                }
                                totalnetamt = totalnetamt + totalamt1;
                                totalgrossamt = totalgrossamt + totalamt_gross1;
                            }
                            else {
                                $scope.testALL[i].invs058_rqvalue = 0;
                                $scope.testALL[i].invs058_gross_amt = 0;
                            }
                        }
                    }

                    totalamt = totalamt + totalamt1;
                    $scope.total_netamt = totalamt1;
                    $scope.total_grossamt = totalgrossamt;
                    totalamt1 = 0;
                    totalamt_gross1 = 0;
                    // }
                }




                if ($scope.temp['invs058_order_apply_aexp'] == true) {//if checkbox is clicked

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var add = 0;

                            totalamt += (parseFloat(add));
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'P') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - ((parseFloat(totalamt) * parseFloat(percentage)) / 100);
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'S') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - $scope.OrderAttrs[i].or_atamount;
                        }
                    }
                }

                if ($scope.temp['invs058_order_apply_aexp'] == false) {//if checkbox is not clicked

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'P') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - ((parseFloat(totalamt) * parseFloat(percentage)) / 100);
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'S') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - $scope.OrderAttrs[i].or_atamount;
                        }


                        //if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                        //    if ($scope.OrderAttrs[i].or_atamount != '')
                        //        var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                        //    else
                        //        var add = 0;

                        //    totalamt += (parseFloat(add));
                        //}

                    }

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var add = 0;

                            totalamt += (parseFloat(add));
                        }
                    }
                }


                $scope.totAmt = totalamt;

            }

            $scope.orcalculateamt_old = function (str) {

                $scope.totAmt = 0;
                var totalamt = 0;
                var totalamt1 = 0;
                for (var i = 0; i < $scope.testALL.length; i++) {
                    // if ($scope.testALL[i].invs058_rd_line_no1 == true) {
                    if ($scope.temp.servicevalue == 'I') {
                        if ($scope.testALL[i].invs058_ordered_quantity != undefined) {
                            if ($scope.testALL[i].invs058_sup_price != undefined) {

                                totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);

                                if ($scope.testALL[i].invs058_discount != undefined) {
                                    var disamt = ((totalamt1 * $scope.testALL[i].invs058_discount) / 100);
                                    totalamt1 = totalamt1 - disamt;
                                }
                            }
                        }
                    }

                    totalamt = totalamt + totalamt1;
                    totalamt1 = 0;
                    //}
                }

                if (totalamt != "NaN") {
                    $scope.totAmt = parseFloat(totalamt);
                }
                else { $scope.totAmt = 0; }

                var o_amt;
                if (str.or_atamount != '' && str.or_atamount != undefined) {
                    o_amt = str.or_atamount;
                    //Percentage
                    if (str.or_atstatus == 'P') {
                        if (str.or_atamount != '') {
                            var disamt = ((parseFloat($scope.totAmt) * parseFloat(str.or_atamount)) / 100);
                            var totalamount = $scope.totAmt - disamt;

                            if (totalamount != "NaN") {
                                $scope.totAmt = parseFloat(totalamount);
                            }
                        }
                        str.or_atamount = o_amt;
                    }

                    //Amount
                    if (str.or_atstatus == 'A') {
                        if (str.or_atamount != undefined) {
                            o_amt = str.or_atamount;
                            var totalamount = parseFloat($scope.totAmt) + parseFloat(str.or_atamount);
                            if (totalamount != "NaN") {
                                $scope.totAmt = parseFloat(totalamount);
                            }
                            str.or_atamount = o_amt;
                        }
                    }
                }
                else
                    str.or_atamount = 0;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $scope.CheckAllService = function () {
                var main1 = document.getElementById('mainchk1');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if (main1.checked == true) {
                        //var chkitem = document.getElementById('test-'+i);
                        //chkitem.checked = true;

                        $scope.filteredTodos[i]['req_no1'] = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                    else {
                        //var chkitem = document.getElementById('test-'+i);
                        //chkitem.checked = false;
                        main1.checked = false;
                        $scope.filteredTodos[i]['req_no1'] = false;;

                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }
            }

            $scope.checkonebyoneselectServices = function () {



                //$("input[type='checkbox']").change(function (e) {

                //    if ($(this).is(":checked")) {
                //        $(this).closest('tr').addClass("row_selected");
                //        $scope.color = '#edefef';
                //    }
                //    else {
                //        $(this).closest('tr').removeClass("row_selected");
                //        $scope.color = '#edefef';
                //    }
                //});

                //main1 = document.getElementById('mainchk1');
                //if (main1.checked == true) {
                //    main1.checked = false;
                //    $("input[type='checkbox']").change(function (e) {
                //        if ($(this).is(":checked")) {
                //            $(this).closest('tr').addClass("row_selected");
                //        }
                //        else {
                //            $(this).closest('tr').removeClass("row_selected");
                //        }
                //    });
                //}
            }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }])

})();
