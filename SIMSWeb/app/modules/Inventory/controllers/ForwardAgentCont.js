﻿(function () {
    'use strict';
    var modulecode = [];
    var del = [];
    var main;
    var deletefin = [];
    var flag;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ForwardAgentCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.div_hide = false;
            $scope.display = false;
            $scope.grid = true;
            $scope.save1 = false;//
            $scope.value = false;
            $scope.itemsPerPage = "10";
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/ItemAssembly/GetForwardAgent").then(function (res) {
                $scope.obj = res.data;
                for (var i = 0; i < $scope.obj.length; i++) {
                    $scope.obj[i].icon = "fa fa-plus-circle";
                }
                console.log($scope.obj);
                $scope.totalItems = $scope.obj.length;
                $scope.todos = $scope.obj;
                $scope.makeTodos();
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            });

            $scope.getdata = function () {
                $http.get(ENV.apiUrl + "api/ItemAssembly/GetForwardAgent").then(function (res) {
                    $scope.obj = res.data;
                    for (var i = 0; i < $scope.obj.length; i++) {
                        $scope.obj[i].icon = "fa fa-plus-circle";
                    }
                    console.log($scope.obj);
                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.makeTodos();
                    $scope.myForm.$setPristine();
                    $scope.myForm.$setUntouched();
                });
            }


            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                console.log($scope.filteredTodos);
            };

            $scope.size = function (str) {
                console.log(str);
                $scope.itemsPerPage = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }


            $http.get(ENV.apiUrl + "api/finance/getComCode").then(function (res) {
                $scope.compcode = res.data;
            });

            $scope.currency = function (contrycode) {

                $http.get(ENV.apiUrl + "api/finance/getCurrencydesc?contrycode=" + contrycode).then(function (res) {
                    $scope.currencyDesc = res.data;
                });
            }

            $scope.edit = function (str) {
                debugger;
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.value = true;
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = false;
                    $scope.update1 = true;
                    $scope.delete1 = false;



                    $scope.edt = {
                        fa_code: str.fa_code,
                        fa_name: str.fa_name,
                        excg_curcy_code: str.cur_code,
                        fa_location_ind: str.fa_location_ind,
                        fa_orig_code: str.fa_orig_code,
                        fa_address1: str.fa_address1,
                        fa_address2: str.fa_address2,
                        fa_address3: str.fa_address3,
                        fa_city: str.fa_city,
                        con_code: str.con_code,
                        fa_telephone_no: str.fa_telephone_no,
                        fa_fax_no: str.fa_fax_no,
                        fa_telex_no: str.fa_telex_no,
                        fa_email_address: str.fa_email_address,
                        fa_contact_name: str.fa_contact_name,
                        fa_contact_designation: str.fa_contact_designation,
                        fa_discount_pct: str.fa_discount_pct,
                        fa_credit_period: str.fa_credit_period,
                        fa_remarks: str.fa_remarks
                    }


                    console.log($scope.edt);
                    $scope.currency(str.comp_cntry_code);
                }
            }


            $http.get(ENV.apiUrl + "api/finance/getAllCountryName").then(function (res) {
                $scope.countryNames = res.data;

            });

            $http.get(ENV.apiUrl + "api/ItemAssembly/getCurrencyCode").then(function (res) {
                $scope.currencyNames = res.data;
            });

            $http.get(ENV.apiUrl + "api/Bank/getCountry").then(function (cont) {
                $scope.country_code = cont.data;
            });

            var data1 = [];
            $scope.Update = function (isvalidate) {
                debugger
                // if (isvalidate) {
                $scope.data = {
                    fa_code: $scope.edt.fa_code,
                    fa_name: $scope.edt.fa_name,
                    excg_curcy_code: $scope.edt.excg_curcy_code,
                    fa_location_ind: $scope.edt.fa_location_ind,
                    fa_orig_code: $scope.edt.fa_orig_code,
                    fa_address1: $scope.edt.fa_address1,
                    fa_address2: $scope.edt.fa_address2,
                    fa_address3: $scope.edt.fa_address3,
                    fa_city: $scope.edt.fa_city,
                    con_code: $scope.edt.con_code,
                    fa_telephone_no: $scope.edt.fa_telephone_no,
                    fa_fax_no: $scope.edt.fa_fax_no,
                    fa_telex_no: $scope.edt.fa_telex_no,
                    fa_email_address: $scope.edt.fa_email_address,
                    fa_contact_name: $scope.edt.fa_contact_name,
                    fa_contact_designation: $scope.edt.fa_contact_designation,
                    fa_discount_pct: $scope.edt.fa_discount_pct,
                    fa_credit_period: $scope.edt.fa_credit_period,
                    fa_remarks: $scope.edt.fa_remarks,
                    opr: 'U'
                }



                data1.push($scope.data)
                console.log(data);
                $http.post(ENV.apiUrl + "api/ItemAssembly/CUDForwardAgent", data1).then(function (msg) {
                    if (msg.data) {
                        $scope.grid = true;
                        $scope.display = false;
                        data1 = [];
                        swal('', 'Record Updated Successfully');

                        $scope.getdata();
                    } else {

                        swal('', "Record Not Updated");
                        $scope.getdata();
                    }
                    $scope.display = false;
                    $scope.grid = true;
                });
                //  }
            }
            var data = [];

            $scope.Save = function (isvalidate) {
                debugger
                if (isvalidate) {
                    $scope.edt.opr = "I"
                    data.push($scope.edt);
                    console.log(data);
                    $http.post(ENV.apiUrl + "api/ItemAssembly/CUDForwardAgent", data).then(function (msg) {
                        if (msg.data) {
                            data = [];
                            swal('', 'Record Inserted Successfully');

                            $scope.getdata();
                        } else {
                            data = [];
                            swal('', "Agent Code Already Present");
                            $scope.getdata();
                        }
                        $scope.display = false;
                        $scope.grid = true;
                    });
                }

            }

            $scope.New = function () {
                debugger
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = true;
                    $scope.value = false;
                    $scope.update1 = false;
                    $scope.delete1 = false;
                    $scope.edt = "";

                    //$http.get(ENV.apiUrl + "api/ItemAssembly/getAgentCode").then(function (res) {
                    //    $scope.fa_codes = res.data;
                    //    $scope.edt.fa_code = $scope.fa_codes[0].agent_code;
                    //    console.log($scope.edt.fa_code);
                    //}); 


                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.getdata();
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }


            $scope.multipledelete = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("code-" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("code-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.delete_onebyone = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.Delete = function () {
                debugger;
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    deletefin = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("code-" + i);
                        if (v.checked == true) {
                            var deletemodulecode = ({
                                'fa_code': $scope.filteredTodos[i].fa_code,
                                opr: 'D'
                            });
                            deletefin.push(deletemodulecode);
                            $scope.flag = true;
                        }
                    }

                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {

                            if (isConfirm) {
                                debugger;
                                $http.post(ENV.apiUrl + "api/ItemAssembly/CUDForwardAgent", deletefin).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getdata();
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                            }

                                        });
                                    }
                                    else {
                                        swal({ text: "Record Not Deleted", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.maindata();
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                            }
                                        });
                                        $scope.getdata();
                                    }

                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById("code-" + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        //$scope.row1 = '';
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    $scope.currentPage = true;
                }
            }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            var dom;
            var count = 0;


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                debugger
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            //function searchUtil(item, toSearch) {
            //    /* Search Text in all 3 fields */
            //    return (item.fa_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
            //        item.fa_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
            //        item.fa_short_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            //}

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.fa_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.fa_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }


            $scope.flag = true;

            $scope.expand = function (info, $event) {
                console.log(info)
                console.log($event);
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table table table-bordered' cellpadding='5' cellspacing='0'>" +
                        "<thead>" + "<tr><th class='semi-bold'>" + "Address 1" + " </th><th class='semi-bold'>" + "Address 2" + "</th> <th class='semi-bold'>" + "Address 3" + " </th><th class='semi-bold'>" + "Telephone No" + "</th><th class='semi-bold'>" + "Fax No" + "</th><th class='semi-bold'>" + "Email" + "</th></tr></thead>" +
                         "<tbody><tr><td class='v-align-middle'>" + (info.fa_address1) + " </td><td>" + (info.fa_address2) + "</td><td class='v-align-middle'>" + (info.fa_address3) + " </td><td>" + (info.fa_telephone_no) + "</td><td>" + (info.fa_fax_no) + "</td><td>" + (info.fa_email_address) + "</td> </tr>" +

                         "<thead>" + "<tr> <th class='semi-bold'>" + "Contact Name" + " </th><th class='semi-bold'>" + "Designation" + "</th> <th class='semi-bold'>" + "Discount" + "</th><th class='semi-bold'>" + "Credit Period" + " </th>" +
                        "<th class='semi-bold'>" + "Remark" + "</th><th class='semi-bold'>" + "Telex No" + " </th></tr>" + "<thead>" +

                          "<tr><td>" + (info.fa_contact_name) + " </td><td>" + (info.fa_contact_designation) + "</td><td>" + (info.fa_discount_pct) + "</td><td>" + (info.fa_credit_period) + "</td>" +
                        "<td>" + (info.fa_remarks) + "</td><td>" + (info.fa_telex_no) + " </td></tr>" +

                        " </tbody></table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);

                    console.log(dom);
                    $scope.flag = false;
                } else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.obj.length; i++) {

                        $scope.obj[i].icon = "fa fa-plus-circle";

                    }
                    $scope.flag = true;
                }
            };

        }])
})();