﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('InventoryFeePostingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.grid = true;
            $scope.edit_data = false;
            $scope.feeposting_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.dept_comp_code;
            $scope.slma_acno_dept_no;
            $scope.finyr;
            $scope.valstatus = false;
            $scope.onesec = true;
            $scope.tt = true;

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;


            $http.get(ENV.apiUrl + "api/DeliveryReasons/getProductAccountDetail").then(function (res) {
                $scope.getFeeData = res.data;
                $scope.totalItems = $scope.getFeeData.length;
                $scope.todos = $scope.getFeeData;
                $scope.makeTodos();

            });


            debugger;
            $http.get(ENV.apiUrl + "api/DeliveryReasons/GetAllGLAccountNos?glma_accountcode=" + '' + "&cmpnycode=" + '').then(function (res) {
                debugger
                $scope.getFeeaccouncodes = res.data;
                console.log($scope.getFeeaccouncodes);
            });





            $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                $scope.comp_data = res.data;
                console.log($scope.comp_data);
                $scope.dept_comp_code = $scope.comp_data[0].dept_comp_code;

                $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetCurrentFinancialYear").then(function (res) {
                    $scope.finyr_data = res.data;
                    $scope.finyr = $scope.finyr_data;

                    $http.get(ENV.apiUrl + "api/common/FeePosting/GetAllAccount_Name?comp_code=" + $scope.dept_comp_code + "&financialyr=" + $scope.finyr).then(function (res) {
                        $scope.AccountNo_data = res.data;
                        console.log($scope.AccountNo_data);
                    });
                });
            });

            $http.get(ENV.apiUrl + "api/common/FeePosting/GetAllCurName").then(function (res) {
                $scope.cur_data = res.data;
            });

            $scope.getacadyr = function (cur_code) {
                $http.get(ENV.apiUrl + "api/common/FeePosting/GetAllAcademicYear?cur_code=" + cur_code).then(function (res) {
                    $scope.acadyr_data = res.data;
                });
            }

            $(function () {
                $('#cmb_grade,#cmb_section').multipleSelect({
                    width: '100%'
                });
            });

            $scope.getgrade = function (cur, acad_yr) {
                if (cur != null && acad_yr != null) {
                    $http.get(ENV.apiUrl + "api/common/getAllGradesFeePost?cur_code=" + cur + "&ac_year=" + acad_yr).then(function (res) {
                        $scope.grade = res.data;
                        console.log($scope.grade);
                        setTimeout(function () {
                            $('#cmb_grade').change(function () {
                                console.log($(this).val());
                            }).multipleSelect({
                                width: '100%'
                            });
                        }, 1000);
                    });


                }
            }

            $scope.getsection = function (cur, acad_yr, grade) {
                if (cur != null && acad_yr != null) {
                    if (grade != "null") {
                        $http.get(ENV.apiUrl + "api/common/SectionScreening/getAllSection?cur_code=" + cur + "&ac_year=" + acad_yr + "&grade=" + grade).then(function (res) {
                            $scope.section = res.data;
                            console.log($scope.section);
                            setTimeout(function () {
                                $('#cmb_section').change(function () {
                                    console.log($(this).val());
                                }).multipleSelect({
                                    width: '100%'
                                });
                            }, 1000);
                        });
                    }
                }
            }

            $scope.getfeeposting_accrualdata = function (cur_code, acad_yr, grade_code, sec_code) {
                $scope.pagershow = false;

                $http.get(ENV.apiUrl + "api/common/FeePosting/GetAllfins_fees_posting_accrual?cur_code=" + cur_code + "&ac_year=" + acad_yr + "&g_code=" + grade_code + "&sec_code=" + sec_code).then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.feeposting_data = res.data;
                    console.log($scope.feeposting_data);
                    $scope.totalItems = $scope.feeposting_data.length;
                    $scope.todos = $scope.feeposting_data;
                    $scope.makeTodos();
                    if ($scope.feeposting_data.length > 0) {
                        $scope.tt = false;
                    }
                });
            }

            $scope.getAllAccountData = function (accNo, mod) {
                if (accNo != null) {
                    //swal({ title: "Alert", text: "Do You Want To Copy For All Months", imageUrl: "assets/img/notification-alert.png",},
                    //function ()
                    //{
                    //    debugger;
                    mod.fins_advance_received_acno_feb = accNo;
                    mod.fins_advance_received_acno_mar = accNo;
                    mod.fins_advance_received_acno_apr = accNo;
                    mod.fins_advance_received_acno_may = accNo;
                    mod.fins_advance_received_acno_jun = accNo;
                    mod.fins_advance_received_acno_jul = accNo;
                    mod.fins_advance_received_acno_aug = accNo;
                    mod.fins_advance_received_acno_sep = accNo;
                    mod.fins_advance_received_acno_oct = accNo;
                    mod.fins_advance_received_acno_nov = accNo;
                    mod.fins_advance_received_acno_dec = accNo;
                    // });

                }
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edt =
                {
                    sims_singlesec: 'true'
                }

            $scope.Save = function (isvalidate) {
                debugger
                var data1 = [];
                var data = [];
                $scope.insert = false;

                if (isvalidate) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        if ($scope.filteredTodos[i].fins_fee_number1 == true) {
                            if ($scope.filteredTodos[i].revenue_accno != "" || $scope.filteredTodos[i].stock_accno != "" ||
                                $scope.filteredTodos[i].cost_accno != "" || $scope.filteredTodos[i].discount_accno != ""
                               ) {
                                var data = {
                                    pc_code: $scope.filteredTodos[i].pc_code,
                                    pc_fin_year: $scope.filteredTodos[i].pc_fin_year,
                                    pc_revenue_acno: $scope.filteredTodos[i].pc_revenue_acno,
                                    pc_stock_acno: $scope.filteredTodos[i].pc_stock_acno,
                                    pc_cost_acno: $scope.filteredTodos[i].pc_cost_acno,
                                    pc_discont_acno: $scope.filteredTodos[i].pc_discont_acno,

                                    opr: 'U'
                                };

                                $scope.insert = true;
                                data1.push(data);
                                console.log(data1);
                            }
                        }

                    }

                    if ($scope.insert) {
                        $http.post(ENV.apiUrl + "api/DeliveryReasons/updateFeePosting", data1).then(function (res) {
                            $scope.feepost = res.data;

                            if ($scope.feepost == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully..." });
                                $http.get(ENV.apiUrl + "api/DeliveryReasons/getProductAccountDetail").then(function (res) {
                                    $scope.getFeeData = res.data;
                                    $scope.totalItems = $scope.getFeeData.length;
                                    $scope.todos = $scope.getFeeData;
                                    $scope.makeTodos();

                                });

                            }
                            else if ($scope.feepost == false)
                                swal({ title: "Alert", text: "Record Not Inserted ..." });
                            else
                                swal("Error-" + $scope.feepost)
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                }
            }

            //$scope.cancel = function (str) {

            //    str.status = false;

            //}

            $scope.mainCancel = function () {

                $http.get(ENV.apiUrl + "api/DeliveryReasons/getProductAccountDetail").then(function (res) {
                    $scope.getFeeData = res.data;
                    $scope.totalItems = $scope.getFeeData.length;
                    $scope.todos = $scope.getFeeData;
                    $scope.makeTodos();

                });

            }

            //$scope.edit = function (str) {
            //    debugger;
            //    var mod = [];
            //    str.status = true;

            //}

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i$scope.filteredTodos[i].pc_code);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i$scope.filteredTodos[i].pc_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function (str) {
                //debugger
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                        //  str.status = true;
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                        //str.status = false;
                    }
                });



                str.status = str.fins_fee_number1;


            }

            $scope.size = function (str) {

                if (str == "All" || str == "all") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.getFeeData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.getFeeData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.getFeeData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.pc_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1);
            }

        }])
})();