﻿(function () {
    'use strict';
    var opr = '';
    var costingcode = [];
    var main;
    var data1 = [];
    var data = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });



    simsController.controller('CreateCostingSheetCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.table1 = true;
            $scope.operation = false;
            $scope.editmode = false;

            $scope.size = function (str) {

                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
                main.checked = false;
                $scope.chk = {}
                $scope.chk['check_all'] = false;
                $scope.row1 = '';
            }

            $http.get(ENV.apiUrl + "api/CreateCostingSheet/getCreateCustomSheet").then(function (getCreateCustomSheet_Data) {
                $scope.CreateCustomSheet = getCreateCustomSheet_Data.data;
                $scope.totalItems = $scope.CreateCustomSheet.length;
                $scope.todos = $scope.CreateCustomSheet;
                $scope.makeTodos();


            });

            $http.get(ENV.apiUrl + "api/CreateCostingSheet/getDepartments").then(function (getDepartments_Data) {
                $scope.Departments_Data = getDepartments_Data.data;
            });

            $http.get(ENV.apiUrl + "api/CreateCostingSheet/getForwardAgent").then(function (getForwardAgent_Data) {
                $scope.FAgent_Data = getForwardAgent_Data.data;
            });

            $http.get(ENV.apiUrl + "api/CreateCostingSheet/getSupplier").then(function (getSupplier_Data) {
                $scope.Supplier_Data = getSupplier_Data.data;

            });

            $http.get(ENV.apiUrl + "api/CreateCostingSheet/getLocation").then(function (getLocation_Data) {
                $scope.Location_Data = getLocation_Data.data;
            });

            $http.get(ENV.apiUrl + "api/CustomExpenses/getReg_noFromClearance").then(function (getCCl_Reg_no) {
                $scope.CCl_Reg_no_data = getCCl_Reg_no.data;
            });

            $http.get(ENV.apiUrl + "api/CreateCostingSheet/getDeliveryMode").then(function (getDeliveryMode_Data) {
                $scope.DeliveryMode_Data = getDeliveryMode_Data.data;
            });

            $http.get(ENV.apiUrl + "api/CreateCostingSheet/getTradeTerm").then(function (getTradeTerm_Data) {
                $scope.TradeTerm_Data = getTradeTerm_Data.data;
            });

            $scope.New = function () {
                $scope.edt = "";
                $scope.check = true;
                $scope.editmode = false;
                $scope.newmode = true;
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.up = function (str) {

                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.readonly = true;

                $scope.edt = {
                    cs_prov_no: str.cs_prov_no,
                    cs_no: str.cs_no,
                    cs_date: str.cs_date,
                    cs_desc: str.cs_desc,
                    cs_prov_date: str.cs_prov_date,
                    cs_costaccount: str.cs_costaccount,

                    dep_code: str.dep_code,
                    dep_name: str.dep_name,
                    sup_code: str.sup_code,
                    sup_name: str.sup_name,
                    fa_code: str.fa_code,
                    fa_name: str.fa_name,
                    loc_code: str.loc_code,
                    loc_name: str.loc_name,
                    dm_code: str.dm_code,
                    dm_name: str.dm_name,
                    trt_code: str.trt_code,
                    trt_desc: str.trt_desc,

                    cs_supl_other_charge: str.cs_supl_other_charge,
                    cs_forward_agent_charge: str.cs_forward_agent_charge,
                    cs_remarks: str.cs_remarks,
                    ccl_reg_no: str.ccl_reg_no,
                    lc_no: str.lc_no,

                    cs_loading_note_no: str.cs_loading_note_no,
                    cs_loading_note_date: str.cs_loading_note_date,
                    cs_received_location: str.cs_received_location,
                    cs_received_by: str.cs_received_by

                }
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Save = function (myForm) {

                if (myForm) {
                    for (var i = 0; i < $scope.Supplier_Data.length; i++) {
                        if ($scope.Supplier_Data[i].sup_code == $scope.edt.sup_code)
                            var cur_code = $scope.Supplier_Data[i].cur_code;
                    }
                    data = [];
                    data1 = [];

                    data = {

                        'cs_prov_no': $scope.edt.cs_prov_no,
                        'cs_no': $scope.edt.cs_no,
                        'cs_date': $scope.edt.cs_date,
                        'cs_prov_date': $scope.edt.cs_prov_date,
                        'dep_code': $scope.edt.dep_code,
                        'sup_code': $scope.edt.sup_code,
                        'cur_code': cur_code,
                        'fa_code': $scope.edt.fa_code,
                        'loc_code': $scope.edt.loc_code,
                        'cs_costaccount': $scope.edt.cs_costaccount,
                        'cs_desc': $scope.edt.cs_desc,
                        'cs_supl_other_charge': $scope.edt.cs_supl_other_charge,
                        'cs_forward_agent_charge': $scope.edt.cs_forward_agent_charge,
                        'cs_remarks': $scope.edt.cs_remarks,
                        'lc_no': $scope.edt.lc_no,

                        'ccl_reg_no': $scope.edt.ccl_reg_no,
                        'cs_payment_due_date': $scope.edt.cs_payment_due_date,
                        'trt_code': $scope.edt.trt_code,
                        'dm_code': $scope.edt.dm_code,

                        'cs_loading_note_no': $scope.edt.cs_loading_note_no,
                        'cs_loading_note_date': $scope.edt.cs_loading_note_date,
                        'cs_received_location': $scope.edt.cs_received_location,
                        'cs_received_by': $scope.edt.cs_received_by,

                        opr: 'I'
                    };

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/CreateCostingSheet/CreateCustomSheetCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        $http.get(ENV.apiUrl + "api/CreateCostingSheet/getCreateCustomSheet").then(function (getCreateCustomSheet_Data) {
                            $scope.CreateCustomSheet = getCreateCustomSheet_Data.data;
                            $scope.totalItems = $scope.CreateCustomSheet.length;
                            $scope.todos = $scope.CreateCustomSheet;
                            $scope.makeTodos();
                        });
                    });

                    $scope.table1 = true;
                    $scope.operation = false;
                }

            }

            $scope.Update = function (myForm) {
                if (myForm) {
                    data = [];
                    data1 = [];
                    data = {

                        'cs_prov_no': $scope.edt.cs_prov_no,
                        'cs_no': $scope.edt.cs_no,
                        'cs_date': $scope.edt.cs_date,
                        'cs_prov_date': $scope.edt.cs_prov_date,
                        'dep_code': $scope.edt.dep_code,
                        'sup_code': $scope.edt.sup_code,

                        'fa_code': $scope.edt.fa_code,
                        'loc_code': $scope.edt.loc_code,
                        'cs_costaccount': $scope.edt.cs_costaccount,
                        'cs_desc': $scope.edt.cs_desc,
                        'cs_supl_other_charge': $scope.edt.cs_supl_other_charge,
                        'cs_forward_agent_charge': $scope.edt.cs_forward_agent_charge,
                        'cs_remarks': $scope.edt.cs_remarks,
                        'lc_no': $scope.edt.lc_no,

                        'ccl_reg_no': $scope.edt.ccl_reg_no,
                        'cs_payment_due_date': $scope.edt.cs_payment_due_date,
                        'trt_code': $scope.edt.trt_code,
                        'dm_code': $scope.edt.dm_code,

                        'cs_loading_note_no': $scope.edt.cs_loading_note_no,
                        'cs_loading_note_date': $scope.edt.cs_loading_note_date,
                        'cs_received_location': $scope.edt.cs_received_location,
                        'cs_received_by': $scope.edt.cs_received_by,

                        opr: 'U'
                    };

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/CreateCostingSheet/CreateCustomSheetCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        $http.get(ENV.apiUrl + "api/CreateCostingSheet/getCreateCustomSheet").then(function (getCreateCustomSheet_Data) {
                            $scope.CreateCustomSheet = getCreateCustomSheet_Data.data;
                            $scope.totalItems = $scope.CreateCustomSheet.length;
                            $scope.todos = $scope.CreateCustomSheet;
                            $scope.makeTodos();
                        });
                    });

                    $scope.table1 = true;
                    $scope.operation = false;
                }

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].cs_prov_no + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].cs_prov_no + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.deleterecord = function () {
                costingcode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].cs_prov_no + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletecostingcode = ({
                            'cs_prov_no': $scope.filteredTodos[i].cs_prov_no,

                            opr: 'D'
                        });
                        costingcode.push(deletecostingcode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/CreateCostingSheet/CreateCustomSheetCUD", costingcode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/CreateCostingSheet/getCreateCustomSheet").then(function (getCreateCustomSheet_Data) {
                                                $scope.CreateCustomSheet = getCreateCustomSheet_Data.data;
                                                $scope.totalItems = $scope.CreateCustomSheet.length;
                                                $scope.todos = $scope.CreateCustomSheet;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/CreateCostingSheet/getCreateCustomSheet").then(function (getCreateCustomSheet_Data) {
                                                $scope.CreateCustomSheet = getCreateCustomSheet_Data.data;
                                                $scope.totalItems = $scope.CreateCustomSheet.length;
                                                $scope.todos = $scope.CreateCustomSheet;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].cs_prov_no + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }

                $scope.currentPage = true;
                main.checked = false;

            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.CreateCustomSheet, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreateCustomSheet;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                return (
                     item.sup_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.cs_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.dep_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.fa_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.cur_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                     item.loc_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.cs_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.cs_costaccount.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.cs_supl_other_charge.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.cs_forward_agent_charge.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                     item.cs_remarks.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.ccl_reg_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.dm_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.trt_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.lc_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                     item.cs_received_by.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                     item.cs_prov_no == toSearch) ? true : false;


            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

        }])
})();





