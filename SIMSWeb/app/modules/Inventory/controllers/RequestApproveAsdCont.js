﻿(function () {
    'use strict';
    var obj1, temp, opr, CurrentDate, comp;
    var main, deletefin = [], data = [], data1 = [], ItemDetails = [];
    var finanacecode = [];
    var check;
    var reqno;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('RequestApproveAsdCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {
            $scope.table = true;

            $http.get(ENV.apiUrl + "api/RequestDetail/getRequestTypeNew?req_type=" + '1').then(function (reqtypedetailsNew) {
                $scope.req_type_detailsNew = reqtypedetailsNew.data;
                $scope.temp['request_mode_code'] = $scope.req_type_detailsNew[0].request_mode_code;

            });

            $scope.hide_save = false;
            $scope.hide_saves = false;
            $scope.hide_table = false;

            $scope.IP = {};
            $scope.sel = {};


            if ($http.defaults.headers.common['schoolId'] == 'SISERP' || $http.defaults.headers.common['schoolId'] == 'siserp' || $http.defaults.headers.common['schoolId'] == 'asd' || $http.defaults.headers.common['schoolId'] == 'ASD') {
                $scope.asd_show = true;
            }
            else {
                $scope.asd_show = false;
            }

            $http.get(ENV.apiUrl + "api/RequestDetail/getDeparments").then(function (deptdata) {
                $scope.dept_data = deptdata.data;
                console.log($scope.dept_data);
            });

            $scope.SelectIetm = function () {
                debugger;
                $scope.IP.username = $rootScope.globals.currentUser.username;
                $http.get(ENV.apiUrl + "api/RequestDetail/getRequestDetailsforapprove?data=" + JSON.stringify($scope.IP)).then(function (res) {
                    $scope.rows = res.data.table;
                    $scope.Allrows = res.data.table1;
                    console.log("Allrows", $scope.Allrows);
                    console.log("rows", $scope.rows);

                    $scope.reportReqNo = "";

                    if ($scope.Allrows.length <= 0) {
                        swal({ title: "Alert", text: "Records not found", showCloseButton: true, width: 380, });
                        $scope.hide_save = true;
                        $scope.hide_saves = false;
                        $scope.hide_table = false;
                    }
                    else {
                        for (var r in $scope.rows) {
                            //$scope.rows[r]['sims_icon'] = "fa fa-minus-circle";
                            // $scope.rows[r]['isexpanded'] = "grid";
                            $scope.rows[r]['sims_icon'] = "fa fa-plus-circle";
                            $scope.rows[r]['isexpanded'] = "none";
                            $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].req_no);
                            $scope.reportReqNo = $scope.reportReqNo + $scope.rows[r]['req_no'] + ',';
                            $scope.hide_save = false;
                            $scope.hide_saves = true;
                            $scope.hide_table = true;
                        }
                        console.log("reportReqNo", $scope.reportReqNo);
                    }


                    console.log(res.data.table);
                    console.log(res.data.table1);
                });
            }

            function getSubitems(dno) {
                var arr = [];
                for (var i = 0; i < $scope.Allrows.length; i++) {
                    if ($scope.Allrows[i].req_no == dno) {
                        arr.push($scope.Allrows[i]);
                    }
                }
                console.log('arr', arr);
                return arr;
            }

            $scope.expand = function (j, $event) {
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";
                    console.log(j);
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none";
                }
            }

            $scope.reset = function () {
                $scope.IP = {};
                $scope.rows = {};
                $scope.Allrows = [];

            }

            $scope.checkRange = function (s) {
                var rq = parseInt(s.final_qnt);
                if (rq == NaN)
                    rq = 0;
                s.final_qnt = rq;
            }

            $scope.ordclick = function (gh) {
                var l = gh.subItems.length;
                for (var i = 0; i < l; i++) {
                    gh.subItems[i]['isChecked'] = gh.isChecked;
                }
            }

            $scope.ordlineclick = function (r, m) {
                var l = r.subItems.length;
                var f = false;
                if (m) {
                    f = true;
                    for (var i = 0; i < l; i++) {
                        f = f && r.subItems[i]['isChecked'];
                    }
                }
                r.isChecked = f;
            }

            $scope.save = function (s) {

                $scope.hide_save = true;
                $scope.hide_saves = true;

                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {

                    for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                        if ($scope.rows[i].subItems[j].isChecked) {
                            var ob = {};
                            ob.rd_quantity = $scope.rows[i].subItems[j].rd_quantity;
                            ob.final_qnt = $scope.rows[i].subItems[j].final_qnt;
                            ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.rd_line_no = $scope.rows[i].subItems[j].rd_line_no;
                            ob.req_no = $scope.rows[i].subItems[j].req_no;
                            ob.req_type = $scope.rows[i].subItems[j].req_type;
                            ob.sims_workflow_no = $scope.rows[i].subItems[j].sims_workflow_no;
                            ob.user_code = $rootScope.globals.currentUser.username;
                            ob.rd_remarks = $scope.rows[i].subItems[j].rd_remarks;
                            ob.req_remarks = $scope.IP.rd_remarks;
                            ob.em_email_id = $scope.IP.em_email_id;
                            ob.em_login_code = $scope.IP.em_login_code;
                            ob.val = r;
                            ar.push(ob);
                        }
                    }
                }
                if (ar.length == 0) {
                    swal({ text: "Please Select Atleast One Request", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    $scope.hide_save = false;
                    $scope.hide_saves = true;

                }
                else {
                    $http.post(ENV.apiUrl + "api/RequestDetail/UpdateRequestDetailsforapprove", ar).then(function (res) {

                        var arr = [];
                        var ob = {};
                        //for (var i = 0; i < $scope.rows.length; i++) {
                        //    if ($scope.rows[i].isChecked) {
                        //        ob.req_no = $scope.rows[i].req_no;
                        //        arr.push(ob.req_no);
                        //    }
                        //}

                        for (var i = 0; i < $scope.rows.length; i++) {
                            for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                                if ($scope.rows[i].subItems[j].isChecked) {
                                    ob.req_no = $scope.rows[i].req_no;
                                    arr.push(ob.req_no);
                                }
                            }
                        }

                        debugger;
                        //swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
                        swal({ text: 'Purchase Requisition Number:- ' + arr + "," + "" + ' Approved', imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                        var rtype = $scope.IP.request_mode_code;

                        $scope.IP = {
                            rd_remarks: '',
                        }

                        if ($scope.IP.req_no == undefined && $scope.IP.req_range_no == undefined && arr.length == 0) {
                            swal({ text: 'Please Enter Request No From and Request No To OR Select Atleast One Request', imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                        }

                        else {

                            var check = true;

                            if (rtype == 'L') {

                                $scope.IP.username = $rootScope.globals.currentUser.username;
                                $scope.IP.request_mode_code = rtype;
                                $scope.IP = { request_mode_code: rtype }
                                $http.get(ENV.apiUrl + "api/RequestDetail/getRequestDetailsforapprove?data=" + JSON.stringify($scope.IP)).then(function (res) {
                                    $scope.rows = res.data.table;
                                    $scope.Allrows = res.data.table1;
                                    if ($scope.Allrows.length <= 0) {
                                        swal({ title: "Alert", text: "Records not found", showCloseButton: true, width: 380, });
                                        $scope.hide_save = true;
                                        $scope.hide_saves = false;
                                        $scope.hide_table = false;
                                    }
                                    else {
                                        for (var r in $scope.rows) {
                                            //$scope.rows[r]['sims_icon'] = "fa fa-minus-circle";
                                            // $scope.rows[r]['isexpanded'] = "grid";
                                            $scope.rows[r]['sims_icon'] = "fa fa-plus-circle";
                                            $scope.rows[r]['isexpanded'] = "none";
                                            $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].req_no);
                                            $scope.hide_save = false;
                                            $scope.hide_saves = true;
                                            $scope.hide_table = true;
                                        }
                                    }

                                    $scope.remarks = '';
                                    console.log(res.data.table);
                                    console.log(res.data.table1);
                                });
                            }

                            else {

                                if ($scope.IP.req_no == undefined) {
                                    $scope.IP.req_no = 0
                                }
                                if ($scope.IP.req_range_no == undefined) {
                                    $scope.IP.req_range_no = 0
                                }

                                if (check == true) {
                                    swal({
                                        title: '',
                                        text: "Do you want to print?",
                                        showCloseButton: true,
                                        showCancelButton: true,
                                        confirmButtonText: 'Yes',
                                        width: 380,
                                        cancelButtonText: 'No',

                                    }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            debugger;
                                            $scope.t = 'Invs.INVR16';
                                            if (arr.length == 0) {
                                                var data = {
                                                    location: $scope.t,
                                                    parameter: {
                                                        req_from: null,
                                                        req_to: null,
                                                        req_list: $scope.reportReqNo,
                                                    },
                                                    state: 'main.InvPRA'
                                                }
                                            }
                                            else {
                                                var data = {
                                                    location: $scope.t,
                                                    parameter: {
                                                        //req_from: $scope.IP.req_no,
                                                        //req_to: $scope.IP.req_range_no,
                                                        req_from: '0',
                                                        req_to: '0',
                                                        req_list: arr + "",
                                                    },
                                                    state: 'main.InvPRA'
                                                }
                                            }

                                            window.localStorage["ReportDetails"] = JSON.stringify(data)
                                            $state.go('main.ReportCardParameter')
                                        }
                                    });
                                }

                                else {
                                    //$state.go('main.InvPRA');
                                    $scope.SelectIetm();
                                }
                            }
                        }
                        //$scope.SelectIetm();
                    });
                }
            }

            $scope.Reset = function () {
                $scope.IP = {
                    rd_from_required: '',
                    rd_up_required: '',
                    req_no: '',
                    req_range_no: '',
                    request_mode_code: '',
                    rd_item_desc: '',
                }
                $scope.remarks = '';
                $scope.hide_save = false;
                $scope.hide_saves = false;
                $scope.hide_table = false;
            }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            })

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd-mm-yyyy'
            });

            $scope.Reject = function () {
                debugger
                $scope.hide_save = true;
                $scope.hide_saves = true;
                var ar = [];
                //ar = $scope.rows.length;
                var r = Math.random() * 10;

                for (var i = 0; i < $scope.rows.length; i++) {

                    for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                        if ($scope.rows[i].subItems[j].isChecked) {
                            var ob = {};
                            ob.rd_quantity = $scope.rows[i].subItems[j].rd_quantity;
                            ob.final_qnt = $scope.rows[i].subItems[j].final_qnt;
                            ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.rd_line_no = $scope.rows[i].subItems[j].rd_line_no;
                            ob.req_no = $scope.rows[i].subItems[j].req_no;
                            ob.req_type = $scope.rows[i].subItems[j].req_type;
                            ob.rd_remarks = $scope.rows[i].subItems[j].rd_remarks;
                            ob.req_remarks = $scope.IP.rd_remarks;
                            ob.sims_workflow_no = $scope.rows[i].subItems[j].sims_workflow_no;
                            ob.user_code = $rootScope.globals.currentUser.username;
                            ob.em_email_id = $scope.IP.em_email_id;
                            ob.em_login_code = $scope.IP.em_login_code;
                            ob.val = r;
                            ar.push(ob);
                        }
                    }
                }
                var rtype = $scope.IP.request_mode_code;
                $scope.IP = { request_mode_code: rtype }

                if (ar.length == 0) {
                    swal({ text: "Please Select Atleast One Request", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    $scope.hide_save = false;
                    $scope.hide_saves = true;
                }
                else {
                    $http.post(ENV.apiUrl + "api/RequestDetail/UpdateRequestDetailsforrejection", ar).then(function (res) {

                        swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });

                        $scope.IP = {
                            rd_remarks: '',
                        }
                        if (rtype == 'L') {
                            $scope.IP.username = $rootScope.globals.currentUser.username;
                            $scope.IP.request_mode_code = rtype;
                            $http.get(ENV.apiUrl + "api/RequestDetail/getRequestDetailsforapprove?data=" + JSON.stringify($scope.IP)).then(function (res) {
                                $scope.rows = res.data.table;
                                $scope.Allrows = res.data.table1;
                                if ($scope.Allrows.length <= 0) {
                                    swal({ title: "Alert", text: "Records not found", showCloseButton: true, width: 380, });
                                    $scope.hide_save = true;
                                    $scope.hide_saves = false;
                                    $scope.hide_table = false;
                                }
                                else {
                                    for (var r in $scope.rows) {
                                        //$scope.rows[r]['sims_icon'] = "fa fa-minus-circle";
                                        // $scope.rows[r]['isexpanded'] = "grid";
                                        $scope.rows[r]['sims_icon'] = "fa fa-plus-circle";
                                        $scope.rows[r]['isexpanded'] = "none";
                                        $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].req_no);
                                        $scope.hide_save = false;
                                        $scope.hide_saves = true;
                                        $scope.hide_table = true;
                                    }
                                }


                                console.log(res.data.table);
                                console.log(res.data.table1);
                            });
                        }
                        else {
                            $scope.IP.username = $rootScope.globals.currentUser.username;
                            $http.get(ENV.apiUrl + "api/RequestDetail/getRequestDetailsforapprove?data=" + JSON.stringify($scope.IP)).then(function (res) {
                                $scope.rows = res.data.table;
                                $scope.Allrows = res.data.table1;
                                if ($scope.Allrows.length <= 0) {
                                    swal({ title: "Alert", text: "Records not found", showCloseButton: true, width: 380, });
                                    $scope.hide_save = true;
                                    $scope.hide_saves = false;
                                    $scope.hide_table = false;
                                }
                                else {
                                    for (var r in $scope.rows) {
                                        //$scope.rows[r]['sims_icon'] = "fa fa-minus-circle";
                                        // $scope.rows[r]['isexpanded'] = "grid";
                                        $scope.rows[r]['sims_icon'] = "fa fa-plus-circle";
                                        $scope.rows[r]['isexpanded'] = "none";
                                        $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].req_no);
                                        $scope.hide_save = false;
                                        $scope.hide_saves = true;
                                        $scope.hide_table = true;
                                    }
                                }
                                $scope.remarks = '';

                                console.log(res.data.table);
                                console.log(res.data.table1);
                            });
                        }
                    });
                }
            }

            //$scope.Viewdoc = function (str712) {
            //    $http.get(ENV.apiUrl + "api/RequestDetail/getDocDetails?req_no=" + str712.req_no).then(function (res) {
            //        $scope.viewdoc_details = res.data;
            //        if ($scope.viewdoc_details.length > 0) {
            //            $scope.Upload_doc = true;
            //            $('#UploadDocModal').modal({ backdrop: 'static', keyboard: true });
            //        }
            //        else {
            //            swal({ title: "Alert", text: "Document Not Uploaded", showCloseButton: true, width: 380, });
            //        }
            //    });

            //}

            //$scope.downloaddoc = function (str) {
            //    $scope.url = ENV.apiUrl + 'Content/images/RequestDocuments/' + str;
            //    window.open($scope.url);
            //    //content / images / RequestDocuments
            //}


            //Upload Documents Functionality
            $scope.Viewdoc = function (str712) {
                debugger;
                $scope.images_newfiles = [];
                $scope.images = [];
                $scope.requestdocs = '';
                $scope.req_no_712 = str712.req_no;
                $http.get(ENV.apiUrl + "api/RequestDetail/getDocDetails?req_no=" + str712.req_no).then(function (res) {
                    $scope.requestdocs = res.data;
                    if ($scope.requestdocs.length > 0) {

                        //setTimeout(function () {
                        for (var k = 0; k < $scope.requestdocs.length; k++) {
                            var or_filename = $scope.requestdocs[k].salesman_name;
                            var new_filename = $scope.requestdocs[k].salesman_code;

                            $scope.images_newfiles.push({
                                new_filename: new_filename,
                                or_filename: or_filename
                            });

                            var doccnt = $scope.images_newfiles.length;
                            $scope.doccount = doccnt + ' file(s) uploaded.';
                        }
                        //}, 1000);
                    }
                    else {
                        swal({ text: "Document Not Uploaded", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                    }
                    $scope.Upload_doc = true;
                    $('#UploadDocModal').modal({ backdrop: 'static', keyboard: true });
                });
            }

            /*start_attached Files*/
            var formdata = new FormData();
            $scope.images = [];
            $scope.images_att = [];
            var datasend2 = [];
            var under_file = 1;

            $scope.getTheFiles = function ($files) {
                debugger;
                $scope.filesize = true;
                var i = 0, exists = "no";

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);

                    var i = 0;
                    if ($files[i].size > 2000000) {
                        $scope.filesize = false;
                        // $scope.edt.photoStatus = false;
                        swal({ title: "Alert", text: "File Should Not Exceed 2MB.", showCloseButton: true, width: 380, });
                    }
                    else {
                        //arr_files.push($files[i].name);
                        if ($scope.images.length > 0) {
                            for (var j = 0 ; j < $scope.images.length; j++) {
                                if ($scope.images[j].name == $files[i].name) {
                                    exists = "yes";
                                    return;
                                }
                            }

                            if (exists == "no") {

                                $scope.images.push({
                                    name: $files[i].name,
                                    file: $files[i].size
                                });
                                if ($scope.images.length > 10) {
                                    $scope.images.splice(10, $scope.images.length);
                                    swal({ title: "Alert", text: "You cannot upload more than 10 files.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                        }
                                    });
                                    return;
                                }
                            }
                            exists = "no";
                        }
                        else {
                            $scope.images.push({
                                name: $files[i].name,
                                file: $files[i].size

                            });
                        }
                        // $scope.$apply();
                    }
                    i++;
                });
            };

            $scope.file_changed = function (element, str) {
                debugger;
                var photofile = element.files[0];
                var v = photofile.name.indexOf('.');
                $scope.name = photofile.name.substr(0, v);
                $scope.fileext = photofile.name.substr(v + 1);
                $scope.photo_filename = (photofile.type);

                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;

                    });
                };
                reader.readAsDataURL(photofile);

                if (element.files[0].size < 2000000) {
                    var filenm = '';
                    //var cnumber = document.getElementById('txt_number').value;
                    var cnumber = '1';
                    var ind = 0;

                    filenm = cnumber + '_' + '(' + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss') + ')';
                    var filenm_images = cnumber + '_' + '(' + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss') + ')' + '.' + $scope.fileext;
                    $scope.images_newfiles.push({
                        new_filename: filenm_images,
                        or_filename: element.files[0].name
                    });

                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + 'api/file/uploadOrderDocument?filename=' + filenm + "&location=" + "OrderDocuments",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    debugger
                    var doccnt = $scope.images_newfiles.length;
                    $scope.doccount = doccnt + ' file(s) uploaded.';
                    $http(request).success(function (d) {
                        under_file = under_file + 1;
                    });

                    console.log($scope.name.type);
                    console.log($scope.name);
                }
            };

            $scope.uploadClick = function (str) {
                $scope.filesize = true;
                $scope.edt1 = str;
                $scope.ins = false;
                formdata = new FormData();
            }

            $scope.delpush = [];

            $scope.CancelFileUpload = function (idx, strname) {
                debugger;
                $scope.delpush.push(idx);

                $scope.images.splice(idx, 1);
                $scope.images_newfiles.splice(idx, 1);

                var doccnt = $scope.images_newfiles.length;
                $scope.doccount = doccnt + ' file(s) uploaded.';

                var data_doc = {
                    'opr': '3',
                    'req_no': $scope.req_no_712,
                    'request_type_code': strname,
                }
                datasend2.push(data_doc);

                $http.post(ENV.apiUrl + "api/RequestDetail/CUDSaveRequestDocs", datasend2).then(function (msg) { });
            };

            /*end_attached Files*/

            //File Download
            $scope.download = function (str) {
                window.open($scope.url + "/Images/OrderDocuments/" + str, "_new");
            };

            $scope.savereqdocs = function () {

                if ($scope.images_newfiles.length > 0) {

                    for (var j = 0 ; j < $scope.images_newfiles.length; j++) {
                        //var filepath = 'Content/images/OrderDocuments';
                        //var original_filename = $scope.images_newfiles[j].or_filename;
                        //var new_filename = $scope.images_newfiles[j].new_filename;
                        //var doclinenum = j + 1;

                        //var data_doc = {
                        //     'vouchernum': $scope.req_no_712,
                        //     'doclineno': doclinenum,
                        //     'filename': original_filename,
                        //     'filename_en': new_filename,
                        //     'filepath': filepath,
                        // }

                        var data_doc = {
                            'opr': '2',
                            'req_no': $scope.req_no_712,
                            'request_type_code': $scope.images_newfiles[j].or_filename,
                            'request_mode_code': $scope.images_newfiles[j].new_filename,
                            'im_inv_no': j + 1,
                            'req_remarks': 'Content/images/RequestDocuments'
                        }
                        datasend2.push(data_doc);
                    }
                }

                $http.post(ENV.apiUrl + "api/RequestDetail/CUDSaveRequestDocs", datasend2).then(function (msg) {
                    datasend2 = [];
                    $scope.images = [];
                    $scope.images_att = [];
                    $scope.Upload_doc = false;
                    $('#UploadDocModal').modal('hide');
                    swal({ text: "Document Uploaded Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                });

            };

            $scope.downloaddoc = function (str) {
                $scope.url = ENV.apiUrl + 'Content/images/RequestDocuments/' + str;
                window.open($scope.url);
                //content / images / RequestDocuments
            }

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.IP = {
                rd_from_required: dd + '-' + mm + '-' + yyyy,
                rd_up_required: dd + '-' + mm + '-' + yyyy
            }

            //End Documents Funtionality//

        }])

})();
