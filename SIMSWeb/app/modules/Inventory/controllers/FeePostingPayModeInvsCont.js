﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main;
    var finanacecode = [];
    var deletefin = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('FeePostingPayModeInvsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.acdyear1 = false;
            $scope.comp = false;
            $scope.Ptype1 = false;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/INVSFeePosting/getFeePosting").then(function (res1) {
                $scope.feepost = res1.data;
                $scope.totalItems = $scope.feepost.length;
                $scope.todos = $scope.feepost;
                $scope.makeTodos();
            });

            //Fill Combo PaymentMode
            $http.get(ENV.apiUrl + "api/FeePosting/GetPaymentMode").then(function (paymode) {

                $scope.pay_mode = paymode.data;

            });



            //Fill Combo GLAccountNo
            $http.get(ENV.apiUrl + "api/FeePosting/GetGLAccountNumber").then(function (docstatus1) {
                $scope.GlACNO = docstatus1.data;

            });


            //Company
            $http.get(ENV.apiUrl + "api/INVSFeePosting/getCompanyName").then(function (compdata) {
                $scope.comp_data = compdata.data;

                $scope.temp = {
                    'invs_company_code': $scope.comp_data[0].invs_company_code,
                }
            });

            $http.get(ENV.apiUrl + "api/INVSFeePosting/getAccYear").then(function (Academicyear) {
                $scope.Academic_year = Academicyear.data;

                $scope.temp = {
                    'invs_company_code': $scope.comp_data[0].invs_company_code,
                    'sims_academic_year': $scope.Academic_year[0].sims_academic_year
                }
            });


            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;

                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.feepost;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.feepost, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.feepost;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sd_payment_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sd_payment_scno.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "/" + month + "/" + day;
            }


            //NEW BUTTON
            $scope.New = function () {

                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

                $scope.acdyear1 = false;
                $scope.comp = false;
                $scope.Ptype1 = false;


                $scope.disabled = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = "";
                $scope.temp = {};
                $scope.temp = {
                    'invs_company_code': $scope.comp_data[0].invs_company_code,
                    'sims_academic_year': $scope.Academic_year[0].sims_academic_year,
                    'invs_fee_posting_status': true
                }
                //$scope.temp['fins_fee_posting_status'] = true;

            }

            //EnableDesable

            $scope.setvisible = function () {

                $scope.ldgno = true;
                $scope.slno = true;
            }


            //DATA SAVE INSERT
            var dataforSave = [];
            $scope.savedata = function (Myform) {

                dataforSave = [];
                data = [];

                if (Myform) {
                    var data = $scope.temp;
                    data.opr = 'I';

                    dataforSave.push(data);
                    $http.post(ENV.apiUrl + "api/INVSFeePosting/CUDFeePostingPaymentModeINVS", dataforSave).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 300, height: 200 });
                        }

                        $http.get(ENV.apiUrl + "api/INVSFeePosting/getFeePosting").then(function (res1) {
                            $scope.feepost = res1.data;
                            $scope.totalItems = $scope.feepost.length;
                            $scope.todos = $scope.feepost;
                            $scope.makeTodos();
                            $scope.table = true;
                            $scope.display = false;
                        });

                    });
                    dataforSave = [];

                }
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {

                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;

                $scope.acdyear1 = true;
                $scope.comp = true;
                $scope.Ptype1 = true;

                // $scope.temp = str;
                // var invs_fee_acno_old = 'invs_fee_acno_old';

                $scope.temp = {
                    invs_company_code: str.invs_company_code

                  , sims_academic_year: str.sd_fin_year

                  , sims_appl_parameter: str.sd_payment_type

                  , acno: str.sd_payment_scno

                 , invs_fee_posting_status: str.sd_status

                   , invs_fee_acno_old: str.sd_payment_scno
                };
                $scope.getacyr();
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

                $scope.currcode1 = true;
                $scope.acdyear1 = true;
                $scope.Ptype1 = true;

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function () {
                var data = $scope.temp;


                data = {
                    opr: "U",
                    invs_company_code: $scope.temp.invs_company_code,
                    sims_academic_year: $scope.temp.sims_academic_year,
                    sims_appl_parameter: $scope.temp.sims_appl_parameter,
                    acno: $scope.temp.acno,
                    invs_fee_acno_old: $scope.temp.invs_fee_acno_old,
                    invs_fee_posting_status: $scope.temp.invs_fee_posting_status


                };
                dataforUpdate.push(data);
                //dataupdate.push(data);

                $http.post(ENV.apiUrl + "api/INVSFeePosting/CUDFeePostingPaymentModeINVS", dataforUpdate).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 300, height: 200 });
                    }
                    else {
                        swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 300, height: 200 });
                    }
                    $http.get(ENV.apiUrl + "api/INVSFeePosting/getFeePosting").then(function (res1) {
                        $scope.feepost = res1.data;
                        $scope.totalItems = $scope.feepost.length;
                        $scope.todos = $scope.feepost;
                        $scope.makeTodos();
                        $scope.table = true;
                        $scope.display = false;
                    });

                });
                dataforUpdate = [];


            }

            //DELETE RECORD
            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sd_payment_scno + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sd_payment_scno + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.OkDelete = function () {
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sd_payment_scno + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'invs_company_code': $scope.filteredTodos[i].invs_company_code,
                            'sims_academic_year': $scope.filteredTodos[i].sd_fin_year,
                            'sims_appl_parameter': $scope.filteredTodos[i].sd_payment_type,
                            'acno': $scope.filteredTodos[i].sd_payment_scno,
                            'invs_fee_posting_status': $scope.filteredTodos[i].sd_status,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/INVSFeePosting/CUDFeePostingPaymentModeINVS", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 300, height: 200 }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/INVSFeePosting/getFeePosting").then(function (res1) {
                                                $scope.feepost = res1.data;
                                                $scope.totalItems = $scope.feepost.length;
                                                $scope.todos = $scope.feepost;
                                                $scope.makeTodos();
                                                $scope.table = true;
                                                $scope.display = false;
                                            });

                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 300, height: 200 }).then(function (isConfirm) {
                                        if (isConfirm) {

                                            $http.get(ENV.apiUrl + "api/INVSFeePosting/getFeePosting").then(function (res1) {
                                                $scope.feepost = res1.data;
                                                $scope.totalItems = $scope.feepost.length;
                                                $scope.todos = $scope.feepost;
                                                $scope.makeTodos();
                                                $scope.table = true;
                                                $scope.display = false;
                                            });

                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sd_payment_scno + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });


        }])

})();
