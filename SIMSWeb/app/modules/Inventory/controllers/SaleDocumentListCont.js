﻿(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [];

    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SaleDocumentListCont',
          ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {
              $scope.pagesize = '5';
              $scope.pager = true;
              var user = $rootScope.globals.currentUser.username;
              //var today = new Date();
              //var dd = today.getDate();
              //var mm = today.getMonth() + 1; //January is 0!
              //var yyyy = today.getFullYear();

              //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');
              $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');
              $scope.edt =
                            {
                                from_date: $scope.ddMMyyyy,
                                to_date: $scope.ddMMyyyy,
                            }




              $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;
              $scope.size = function (str) {

                  if (str == "All") {
                      $scope.currentPage = '1';
                      $scope.filteredTodos = $scope.saledocumentlistdata;
                      $scope.pager = false;
                  }
                  else {
                      $scope.pager = true;
                      $scope.pagesize = str;
                      $scope.currentPage = 1;
                      $scope.numPerPage = str;
                      $scope.makeTodos();
                  }

              }

              $scope.index = function (str) {
                  $scope.pageindex = str;
                  $scope.currentPage = str;

                  $scope.makeTodos();
                  main.checked = false;
                  $scope.row1 = '';
              }

              $scope.makeTodos = function () {
                  var rem = parseInt($scope.totalItems % $scope.numPerPage);
                  if (rem == '0') {
                      $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                  }
                  else {
                      $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                  }
                  var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                  var end = parseInt(begin) + parseInt($scope.numPerPage);



                  $scope.filteredTodos = $scope.todos.slice(begin, end);
              };

              $('*[data-datepicker="true"] input[type="text"]').datepicker({
                  todayBtn: true,
                  orientation: "top left",
                  autoclose: true,
                  todayHighlight: true,
                  format: "yyyy-mm-dd"
              });

              $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                  $('input[type="text"]', $(this).parent()).focus();
              });

              //var today = new Date();
              //var dd = today.getDate();
              //var mm = today.getMonth() + 1; //January is 0!
              //if (mm < 10) {
              //    mm = '0' + mm;
              //}
              //var yyyy = today.getFullYear();
              //$scope.sdate = yyyy + '-' + mm + '-' + dd;
              //$scope.edate = yyyy + '-' + mm + '-' + dd;
              //$scope.edt = {
              //    from_date: yyyy + '-' + mm + '-' + dd,
              //    to_date: yyyy + '-' + mm + '-' + dd,

              //}

              $scope.createdate = function (end_date, start_date, name) {
                  //var month1 = end_date.split("/")[0];
                  //var day1 = end_date.split("/")[1];
                  //var year1 = end_date.split("/")[2];
                  //var new_end_date = year1 + "/" + month1 + "/" + day1;
                  //var new_end_date = day1 + "/" + month1 + "/" + year1;

                  //var year = start_date.split("/")[0];
                  //var month = start_date.split("/")[1];
                  //var day = start_date.split("/")[2];
                  //var new_start_date = year + "/" + month + "/" + day;
                  // var new_start_date = day + "/" + month + "/" + year;

                  if ($scope.edt.to_date < $scope.edt.from_date) {
                      swal({ title: 'Please Select Next Date From', width: 380, height: 100 });
                      $scope.edt.to_date = '';
                  }
                  //else {
                  //    $scope.edt[name] = new_end_date;
                  //}
              }

              //$scope.showdate = function (date, name) {
              //    
              //    var month = date.split("/")[0];
              //    var day = date.split("/")[1];
              //    var year = date.split("/")[2];
              //    $scope.edt[name] = year + "/" + month + "/" + day;
              //}

              $timeout(function () {
                  $("#fixTable").tableHeadFixer({ 'top': 1 });
              }, 100);

              $timeout(function () {
                  $("#fixTable1").tableHeadFixer({ 'top': 1 });
              }, 100);

              $scope.Reset = function () {

                  $scope.edt.dep_code = '';
                  $scope.edt.sm_code = '';
                  $scope.edt.search = '';
                  $scope.edt.invs_appl_parameter = '';

              }

              $scope.Show = function () {
                  debugger;
                  //if ($scope.edt.from_date == undefined || $scope.edt.from_date == "") {
                  //    $scope.flag1 = true;
                  //    swal({ title: "Alert", text: "Please select From Date", showCloseButton: true, width: 380, });

                  //}
                  //else if ($scope.edt.to_date == undefined || $scope.edt.to_date == "") {
                  //    $scope.flag1 = true;
                  //    swal({ title: "Alert", text: "Please select To Date", showCloseButton: true, width: 380, });
                  //}
                  //else {
                  //  if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {

                  if ($scope.edt.doc_prov_no == undefined || $scope.edt.doc_prov_no == "") {
                      $scope.edt.doc_prov_no = '';
                  }
                  $scope.table1 = true;
                  $scope.ImageView = false;
                  $http.get(ENV.apiUrl + "api/documentlist/getAllRecords_dps?from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date + "&doc_prov_no=" + $scope.edt.doc_prov_no + "&dep_code=" + $scope.edt.dep_code + "&sm_code=" + $scope.edt.sm_code + "&doc_status=" + $scope.edt.invs_appl_parameter + "&name=" + $scope.edt.name + "&enroll=" + $scope.edt.enroll).then(function (saledocumentlist_data) {
                      $scope.saledocumentlistdata = saledocumentlist_data.data;
                      $scope.totalItems = $scope.saledocumentlistdata.length;
                      $scope.todos = $scope.saledocumentlistdata;
                      $scope.makeTodos();

                      if (saledocumentlist_data.data.length > 0) { }
                      else {
                          $scope.ImageView = true;
                      }

                  });

                  //    }
                  // else {
                  //     if ($scope.edt.doc_prov_no == undefined || $scope.edt.doc_prov_no == "") {
                  //         $scope.edt.doc_prov_no = '';
                  //     }
                  //     $http.get(ENV.apiUrl + "api/documentlist/getAllRecords?from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date + "&search=" + $scope.edt.search + "&dep_code=" + $scope.edt.dep_code + "&sm_code=" + $scope.edt.sm_code + "&doc_status=" + $scope.edt.invs_appl_parameter).then(function (saledocumentlist_data) {
                  //         $scope.saledocumentlistdata = saledocumentlist_data.data;
                  //         $scope.totalItems = $scope.saledocumentlistdata.length;
                  //         $scope.todos = $scope.saledocumentlistdata;
                  //         $scope.makeTodos();
                  //         console.log($scope.saledocumentlistdata);
                  //         if (saledocumentlist_data.data.length > 0) { }
                  //         else {
                  //             $scope.ImageView = true;
                  //         }
                  //     });
                  //}


              }

              $scope.Report = function (str) {
                  if ($http.defaults.headers.common['schoolId'] == 'brs') {
                      var data = {
                          location: 'Invs.INVR02BRS',
                          parameter: { doc_prov_no: str.doc_prov_no },
                          state: 'main.InvSlD',
                          ready: function () {
                              this.refreshReport();
                          },

                      }
                  }

                  else if ($http.defaults.headers.common['schoolId'] == 'sisqatar') {
                      var data = {
                          location: 'Invs.INVR02SISQATAR',
                          parameter: { doc_prov_no: str.doc_prov_no },
                          state: 'main.InvSlD',
                          ready: function () {
                              this.refreshReport();
                          },

                      }
                  }

                  else if ($http.defaults.headers.common['schoolId'] == 'smfc') {
                      var data = {
                          location: 'Invs.INVR02Vat',
                          parameter: { doc_prov_no: str.doc_prov_no },
                          state: 'main.InvSlD',
                          ready: function () {
                              this.refreshReport();
                          },
                           show_pdf: 'y'

                      }
                  }

                  else if ($http.defaults.headers.common['schoolId'] == 'sms') {
                      var data = {
                          location: 'Invs.INVR02NIS',
                          parameter: { doc_prov_no: str.doc_prov_no },
                          state: 'main.InvSlD',
                          ready: function () {
                              this.refreshReport();
                          },

                      }
                  }

                  else if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                      //var data = {
                      //    location: 'Invs.INVR02DPSMIS',
                      //    parameter: { doc_prov_no: str.doc_prov_no },
                      //    state: 'main.InvSlD',
                      //    ready: function () {
                      //        this.refreshReport();
                      //    },

                      //}
                      var data = {
                          doc_prov_no: str.doc_prov_no,
                          sdate: $scope.edt.from_date,
                          edate: $scope.edt.to_date,
                          state: true,
                          doc_status: str.doc_status,
                          back_state: 'main.InvSlD'
                      }
                  }
                  else if ($http.defaults.headers.common['schoolId'] == 'siso') {
                      var data = {
                          location: 'Invs.INVR02SISO',
                          parameter: { doc_prov_no: str.doc_prov_no },
                          state: 'main.InvSlD',
                          ready: function () {
                              this.refreshReport();
                          },
                      }
                  }
                  else {
                      var data = {
                          location: 'Invs.INVR02',
                          parameter: {
                              doc_prov_no: str.doc_prov_no,
                          },
                          state: 'main.InvSlD',
                          ready: function () {
                              this.refreshReport();
                          },
                      }
                  }

                  if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {

                      window.localStorage["ReportProvNumber"] = JSON.stringify(data)
                      $state.go('main.rpts')
                  }
                  else {
                      window.localStorage["ReportDetails"] = JSON.stringify(data)
                      $state.go('main.ReportCardParameter')
                  }
              }

              $scope.stor_report_after_submit = function (row) {
                  debugger;
                  $http.get(ENV.apiUrl + "api/StoreIssueController/get_afterSubmitgetreport_single?doc_prov_no=" + row.doc_prov_no + row.cus_account_no).then(function (report_data) {
                      var doc_sr_code_user = report_data.data;
                      console.log(doc_sr_code_user);
                      console.log($scope.stroe_report_name);                      
                      var data = {
                          location: $scope.stroe_report_name,
                          parameter: {
                              doc_sr_code: doc_sr_code_user,
                          },
                          state: 'main.InvSlD',
                          ready: function () {
                              this.refreshReport();
                          },
                      }
                      console.log(data);
                      window.localStorage["ReportDetails"] = JSON.stringify(data);
                      $state.go('main.ReportCardParameter');
                  });
              }

              try {

                  $scope.rpt = JSON.parse(window.localStorage["ReportProvNumber"]);
                  if ($scope.rpt.state) {
                      $scope.edt =
                          {
                              from_date: $scope.rpt.sdate,
                              to_date: $scope.rpt.edate,
                              dep_code: '',
                              sm_code: '',
                              invs_appl_parameter: '',
                              search: ''
                          }
                      // $scope.edt.from_date
                      $scope.Show()
                  }
              }
              catch (ex)
              { }


              $scope.searched = function (valLists, toSearch) {
                  return _.filter(valLists,

                  function (i) {
                      /* Search Text in all  fields */
                      return searchUtil(i, toSearch);
                  });
              };

              $scope.search = function () {
                  $scope.todos = $scope.searched($scope.saledocumentlistdata, $scope.searchText);
                  $scope.totalItems = $scope.todos.length;
                  $scope.currentPage = '1';
                  if ($scope.searchText == '') {
                      $scope.todos = $scope.saledocumentlistdata;
                  }
                  $scope.makeTodos();
                  main.checked = false;
                  $scope.CheckAllChecked();
              }

              function searchUtil(item, toSearch) {
                  /* Search Text in all 3 fields */
                  return (item.student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sm_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.doc_prov_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.dep_name == toSearch) ? true : false;
              }


              $http.get(ENV.apiUrl + "api/documentlist/getDocStatus").then(function (saleDocStatus) {
                  $scope.saleDocStat = saleDocStatus.data;
                  
              });

              $http.get(ENV.apiUrl + "api/documentlist/getDepartment").then(function (dept_name) {
                  $scope.deptname = dept_name.data;

              });

              $scope.getSMName = function () {

                  $http.get(ENV.apiUrl + "api/documentlist/getSmCode?dept_code=" + $scope.edt.dep_code).then(function (sm_code) {

                      $scope.smcode = sm_code.data;

                  });
              }

          }]
        )
})();
