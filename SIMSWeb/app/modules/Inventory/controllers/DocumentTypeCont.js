﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('DocumentTypeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/doctype/getAllDocumentTypes").then(function (res1) {
                $scope.doc_types = res1.data;
                $scope.totalItems = $scope.doc_types.length;
                $scope.todos = $scope.doc_types;
                $scope.makeTodos();
            });

            $scope.grid_Display = function () {
                $http.get(ENV.apiUrl + "api/doctype/getAllDocumentTypes").then(function (res1) {
                    $scope.doc_types = res1.data;
                    $scope.totalItems = $scope.doc_types.length;
                    $scope.todos = $scope.doc_types;
                    $scope.makeTodos();
                });
            }

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.doc_types;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.doc_types, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.doc_types;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.doc_type_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.doc_type_code == toSearch) ? true : false;
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    $scope.disabled = false;
                    $scope.readonly = true;
                    $scope.table = false;
                    $scope.display = true;
                    $scope.save_btn = true;
                    $scope.Update_btn = false;
                    $scope.temp = "";
                    $scope.Myform.$setPristine();
                    $scope.Myform.$setUntouched();
                    $scope.temp = {};
                    $scope.temp.doc_type_status = true;

                }
            }

            var datasend = [];
            $scope.savedata = function (Myform) {
                if (Myform) {
                    var flag = false;
                    if ($scope.filteredTodos == "") {
                        flag = true;
                    }
                    var data = $scope.temp;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        if ($scope.filteredTodos[i].doc_type_desc == $scope.temp.doc_type_desc) {
                            swal({ text: "Record Already Exist", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                            flag = false;
                        }

                        else {
                            flag = true;
                        }
                    }
                    if (flag == true) {
                        var data = $scope.temp;
                        data.opr = "I";
                        datasend.push(data);
                        $http.post(ENV.apiUrl + "api/doctype/CUDDocumentTypes", datasend).then(function (msg) {
                            $scope.msg1 = msg.data;

                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                            $scope.grid_Display();
                        });
                        datasend = [];
                        $scope.table = true;
                        $scope.display = false;
                    }
                }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.doc_type_code = "";
                $scope.temp.doc_type_desc = "";
                $scope.temp.doc_type_status = "";

            }

            $scope.edit = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.readonly = true;
                    $scope.disabled = true;
                    $scope.table = false;
                    $scope.display = true;
                    $scope.save_btn = false;
                    $scope.Update_btn = true;
                    $scope.temp = {
                        doc_type_code: str.doc_type_code,
                        doc_type_desc: str.doc_type_desc,
                        doc_type_status: str.doc_type_status
                    };
                }
            }
            var dataupdate = [];
            $scope.update = function () {//Myform
                //if (Myform) {
                var Udata = $scope.temp;
                Udata.opr = "U";
                dataupdate.push(Udata);

                $http.post(ENV.apiUrl + "api/doctype/CUDDocumentTypes", dataupdate).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                    $scope.grid_Display();
                });
                dataupdate = [];
                $scope.table = true;
                $scope.display = false;
                //}
            }

            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    var deletefin = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodulecode = ({
                                'doc_type_code': $scope.filteredTodos[i].doc_type_code,
                                opr: 'D'
                            });
                            deletefin.push(deletemodulecode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $http.post(ENV.apiUrl + "api/doctype/CUDDocumentTypes?data=", deletefin).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.grid_Display();
                                            }
                                        });
                                        main = document.getElementById('mainchk');
                                        if (main.checked == true) {
                                            main.checked = false;
                                            $scope.row1 = '';
                                        }
                                        $scope.currentPage = true;
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.grid_Display();
                                            }
                                        });
                                        main = document.getElementById('mainchk');
                                        if (main.checked == true) {
                                            main.checked = false;
                                            $scope.row1 = '';
                                        }
                                        $scope.currentPage = true;
                                    }
                                    e
                                    {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                            else {
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById(i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    $scope.currentPage = true;
                    $scope.checked = false;
                    $scope.CheckAllChecked();
                }
            }
        }])

})();
