﻿(function () {
    'use strict';
    var obj1, temp, opr, CurrentDate, comp;
    var main, deletefin = [], data = [], data1 = [], ItemDetails = [];
    var finanacecode = [];
    var check;
    var reqno;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ItemMovementsCont',
       ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

           $scope.pagesize = "10";
           $scope.pageindex = 0;
           $scope.hide_suc = false;
           $scope.hide_itemne = false;
           $scope.desc_hide = true;
           $scope.desc_hide1 = false;
           $scope.hide_SelectIetm = false;
           $scope.hide_SelectService = false;
           $scope.Main_table = false;
           //var itemcode_data;
           var dataforSave = [];
           $scope.display = false;
           $scope.table = true;
           $scope.table1 = false;
           $scope.Grid = true;
           $scope.disdata = true;
           $scope.temp = [];
           $scope.temp2 = [];
           $scope.im_inv_no = [];
           $scope.search_Req_Data = [];
           console.clear();
           var item_code;
           var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');
           var day = dateyear.split("-")[0];
           var month = dateyear.split("-")[1];
           var year = dateyear.split("-")[2];
           var expMonth = parseInt(month) - parseInt(1);
           if (expMonth < 10)
               expMonth = '0' + expMonth;

           $scope.dt = {
               req_date: dateyear,
               rd_date_required: day + "-" + expMonth + "-" + year,
           }

           //$('#loader0712').modal({ backdrop: 'static', keyboard: false });

           $timeout(function () {
               $("#fixTable").tableHeadFixer({ 'top': 1 });
           }, 100);

           $scope.getallcomboBox = function () {

               $http.get(ENV.apiUrl + "api/StockAdjustment/getAllLocation").then(function (AllLoc) {
                   $scope.location = AllLoc.data;
                   $scope.temp = { loc_code: $scope.location[0].loc_code };
               });

               $http.get(ENV.apiUrl + "api/StockAdjustment/getAllSupplierName").then(function (AllSna) {
                   $scope.SupplierName = AllSna.data;
               });

               $http.get(ENV.apiUrl + "api/StockAdjustment/getsuppliergroupname").then(function (Allsupgname) {
                   $scope.supgname = Allsupgname.data;
               });

               $http.get(ENV.apiUrl + "api/StockAdjustment/getAllDepartment").then(function (resd) {
                   $scope.department = resd.data;
                   $scope.temp = { dep_code: $scope.department[0].dep_code };
               });

               $http.get(ENV.apiUrl + "api/StockAdjustment/getCategories").then(function (Allcategorycode) {
                   $scope.categorycode = Allcategorycode.data;
               });

           }

           $scope.getallcomboBox();

           $scope.getCategories = function () {
               $http.get(ENV.apiUrl + "api/StockAdjustment/getSubCategories?pc_pcode=" + $scope.temp1.category_code).then(function (SubCategories) {
                   $scope.allSubCategories = SubCategories.data;
               });
           }

           $http.get(ENV.apiUrl + "api/ItemAssembly/GetAssembly").then(function (docstatus1) {
               $scope.fillcombo = docstatus1.data;
           });

           $http.get(ENV.apiUrl + "api/RequestDetail/getRequestTypeNew?req_type=" + '1').then(function (reqtypedetailsNew) {
               $scope.req_type_detailsNew = reqtypedetailsNew.data;
               $scope.temp['request_mode_code'] = $scope.req_type_detailsNew[0].request_mode_code;
               if ($scope.temp.request_mode_code == 'S') {
                   $scope.hide_itemne = true;
                   $scope.temp.reqQty = 1;
                   $scope.temp.reqQtyesprice = 0;
                   $scope.reqQtyespricetotal = 0;
               }
               else {
                   $scope.temp.reqQty = 1;
                   $scope.temp.reqQtyesprice = 1;
                   $scope.reqQtyespricetotal = 1;
                   $scope.hide_itemne = false;
               }
               console.log($scope.req_type_detailsNew);
               $scope.delivery_mode();
           });

           $http.get(ENV.apiUrl + "api/RequestDetail/getRequestType").then(function (reqtypedetails) {
               $scope.req_type_details = reqtypedetails.data;
               $scope.temp['request_type_code'] = $scope.req_type_details[0].request_type_code;
               console.log($scope.req_type_details);
           });

           $http.get(ENV.apiUrl + "api/RequestDetail/getDeparmentsUsercode?user_code=" + $rootScope.globals.currentUser.username).then(function (deptdata) {
               $scope.dept_data = deptdata.data;
               $scope.temp['dept_code'] = $scope.dept_data[0].dept_code;
               $scope.delivery_mode();
               console.log($scope.dept_data);
           });

           $scope.delivery_mode = function () {

               $scope.dt = {
                   req_date: dateyear,
                   rd_date_required: day + "-" + expMonth + "-" + year,
               }

               if ($scope.temp.request_mode_code == 'S') {
                   $scope.hide_itemne = true;

                   $scope.temp.reqQty = 1;
                   $scope.temp.reqQtyesprice = 0;
                   $scope.reqQtyespricetotal = 0;

                   //$scope.hide_uncode_service = false;
                   $scope.hide_uncode_item = true;
                   $scope.temp.im_remark = '';
                   $scope.hide_SelectIetm = false;
                   $scope.hide_SelectService = true;
                   $scope.hide_suc = false;

                   $scope.im_item_codel = '';
                   $scope.items_Data = [];
                   $http.get(ENV.apiUrl + "api/DeliveryMode/getServiceSerch?dep_code", $scope.temp).then(function (res1) {
                       $scope.items_Data = res1.data;
                   });
               }
               else {
                   $scope.hide_itemne = false;
                   $scope.hide_SelectIetm = true;
                   $scope.hide_SelectService = false;
                   $scope.temp.reqQty = 1;
                   $scope.temp.reqQtyesprice = 1;
                   $scope.reqQtyespricetotal = 1;
                   $scope.hide_suc = false;

                   //$scope.hide_uncode_service = false;
                   $scope.hide_uncode_item = true;

                   $scope.im_item_codel = '';
                   $scope.temp.im_remark = '';
                   $scope.items_Data = [];
                   $scope.DI = {};
                   $scope.DI.req_type = $scope.temp.request_mode_code
                   //data=" + JSON.stringify($scope.PI)
                   $http.get(ENV.apiUrl + "api/DeliveryMode/getItemSerch?data=" + JSON.stringify($scope.DI)).then(function (res1) {
                       $scope.items_Data = res1.data.table;
                   });
               }

               if ($scope.im_inv_no.length > 0) {
                   $scope.Main_table = false;
                   $scope.Main_table_Service = false;
                   $scope.display = false;
                   $scope.savedisabled = false;
                   $scope.updisabled = true;
                   $scope.im_inv_no = [];
                   $scope.temp.req_no = "";
                   $scope.temp.req_remarks = "";
                   $scope.dt.rd_date_required = "";

                   $scope.Myform.$setPristine();
                   $scope.Myform.$setUntouched();
               }
               //$('#loader0712').modal('hide');
           }

           setTimeout(function () {
               $("#cmb_seritm_code").select2();
           }, 100);

           $scope.getUOM = function (str) {
               $scope.disdata = true;
               debugger;
               item_code = '';
               if ($scope.temp.request_mode_code == 'S') {
                   $scope.hide_itemne = true;
                   for (var i = 0; i < $scope.items_Data.length; i++) {
                       if (str == $scope.items_Data[i].im_desc) {
                           $scope.temp['uom_code'] = $scope.items_Data[i].uom_code;
                           //$scope.temp['sup_name'] = $scope.items_Data[i].sup_name;//sup_code
                           $scope.temp['sup_code'] = $scope.items_Data[i].sup_code;
                           $scope.temp['sg_name'] = $scope.items_Data[i].sg_name;
                           $scope.temp["im_inv_no"] = $scope.items_Data[i].im_inv_no;
                           item_code = $scope.items_Data[i].im_item_code;
                           break;
                       }
                   }
               }
               else {
                   for (var i = 0; i < $scope.items_Data.length; i++) {
                       if (str == $scope.items_Data[i].im_desc) {
                           $scope.temp['uom_code'] = $scope.items_Data[i].uom_code;
                           //$scope.temp['sup_name'] = $scope.items_Data[i].sup_name;//sup_code
                           $scope.temp['sup_code'] = $scope.items_Data[i].sup_code;
                           $scope.temp['sg_name'] = $scope.items_Data[i].sg_name;
                           $scope.temp["im_inv_no"] = $scope.items_Data[i].im_inv_no;
                           item_code = $scope.items_Data[i].im_item_code;
                           break;
                       }
                   }
               }
           }

           $scope.edt = {};

           $scope.SelectService = function () {

               $scope.GetItems = [];
               $scope.ServiceSearch = true;
               $('#MyModal2').modal('show');
               $scope.display = true;
               $scope.updisabled = true;
               $scope.deldisabled = true;
               $scope.savedisabled = false;
           }

           $scope.SelectIetm = function () {

               $scope.GetItems = [];
               $scope.itemSearch = true;
               $('#MyModal').modal('show');
               $scope.display = true;
               $scope.updisabled = true;
               $scope.deldisabled = true;
               $scope.savedisabled = false;
           }

           $scope.okbuttonSerclick = function (oi) {
               debugger;
               $scope.lencheck = [];
               var data = {};
               $scope.im_inv_no = [];
               $('#MyModal1').modal('hide');
               data.req_no = oi.req_no;
               data.req_type = $scope.temp.request_mode_code;
               data.sm_code = $rootScope.globals.currentUser.username;
               $http.post(ENV.apiUrl + "api/RequestDetail/RequestDetailsItemSearch", data).then(function (searchReqISRecord) {
                   $scope.search_Req_Data = searchReqISRecord.data;

                   if ($scope.search_Req_Data.length > 0) {
                       if ($scope.temp.request_mode_code == 'I') {
                           for (var i = 0; i < $scope.search_Req_Data.length; i++) {
                               var t = $scope.search_Req_Data[i].req_no;
                               //var v = document.getElementById(t + i);
                               var v = document.getElementById(t);
                               //if (v.checked == true) {
                               //$scope.inv_no = $scope.search_Req_Data[i].im_inv_no
                               //$scope.inv_desc = $scope.search_Req_Data[i].im_desc
                               //$scope.sup_code = $scope.search_Req_Data[i].sup_code
                               //invno = $scope.inv_no;
                               $scope.im_inv_no.push($scope.search_Req_Data[i]);
                               $scope.lencheck.push($scope.search_Req_Data[i]);

                               $('#MyModal1').modal('hide');
                               $scope.temp = {

                                   'req_no': $scope.search_Req_Data[i].req_no,
                                   'request_mode_code': $scope.search_Req_Data[i].req_type,
                                   'request_mode_name': $scope.search_Req_Data[i].req_type_name,
                                   'request_type_code': $scope.search_Req_Data[i].req_mode,
                                   'request_type_name': $scope.search_Req_Data[i].req_mode_name,
                                   'dept_code': $scope.search_Req_Data[i].dep_code,
                                   'dm_code': $scope.search_Req_Data[i].dm_code,
                                   'request_status_code': $scope.search_Req_Data[i].req_status,
                                   'salesman_code': $scope.search_Req_Data[i].sm_code,
                                   'req_remarks': $scope.search_Req_Data[i].req_remarks

                               }
                               //$scope.geteditDataService($scope.search_Req_Data[i].req_no);
                               //}
                           }

                           $scope.reqQtyespricetotal = 0;
                           $scope.temp.reqQtyesprice = 0;
                           $scope.temp.reqQty = 0;

                           if ($scope.lencheck.length > 0) {
                               $scope.Main_table = true;
                               $scope.Main_table_Service = false;
                               $scope.hide_suc = true;
                               $scope.lencheck = [];
                           }
                       }

                       else if ($scope.temp.request_mode_code == 'S') {

                           for (var i = 0; i < $scope.search_Req_Data.length; i++) {
                               var t = $scope.search_Req_Data[i].req_no;
                               //var v = document.getElementById(t + i);
                               //var v = document.getElementById(t);
                               //if (v.checked == true) {
                               //$scope.inv_no = $scope.search_Req_Data[i].im_inv_no
                               //$scope.inv_desc = $scope.search_Req_Data[i].im_desc
                               //$scope.sup_code = $scope.search_Req_Data[i].sup_code
                               //invno = $scope.inv_no;

                               $scope.im_inv_no.push($scope.search_Req_Data[i]);
                               $scope.lencheck.push($scope.search_Req_Data[i]);

                               $('#MyModal1').modal('hide');
                               $scope.temp = {
                                   'req_no': $scope.search_Req_Data[i].req_no,
                                   'request_mode_code': $scope.search_Req_Data[i].req_type,
                                   'request_mode_name': $scope.search_Req_Data[i].req_type_name,
                                   'request_type_code': $scope.search_Req_Data[i].req_mode,
                                   'request_type_name': $scope.search_Req_Data[i].req_mode_name,
                                   'dept_code': $scope.search_Req_Data[i].dep_code,
                                   'dm_code': $scope.search_Req_Data[i].dm_code,
                                   'request_status_code': $scope.search_Req_Data[i].req_status,
                                   'salesman_code': $scope.search_Req_Data[i].sm_code,
                                   'req_remarks': $scope.search_Req_Data[i].req_remarks
                               }
                               //$scope.geteditDataService($scope.search_Req_Data[i].req_no);
                           }


                           $scope.reqQtyespricetotal = 0;
                           $scope.temp.reqQtyesprice = 0;
                           $scope.temp.reqQty = 0;

                           if ($scope.lencheck.length > 0) {
                               $scope.Main_table = false;
                               $scope.Main_table_Service = true;
                               $scope.hide_suc = true;
                               $scope.lencheck = [];
                           }
                           //}
                       }
                   }
               });
           }

           $scope.editData = function (info) {
               $scope.hide_suc = true;
               if ($scope.temp.request_mode_code == 'I') {
                   $('#MyModal1').modal('hide');
                   $scope.temp = {
                       'req_no': info.req_no,
                       'request_mode_code': info.req_type,
                       'request_mode_name': info.req_type_name,
                       'request_type_code': info.req_mode,
                       'request_type_name': info.req_mode_name,
                       'dept_code': info.dep_code,
                       'dm_code': info.dm_code,
                       'request_status_code': info.req_status,
                       'salesman_code': info.sm_code,
                       'req_remarks': info.req_remarks
                   }
                   $scope.geteditData(info.req_no);
               }

               else if ($scope.temp.request_mode_code == 'S') {
                   $('#MyModal1').modal('hide');
                   $scope.temp = {
                       'req_no': info.req_no,
                       'request_mode_code': info.req_type,
                       'request_mode_name': info.req_type_name,
                       'request_type_code': info.req_mode,
                       'request_type_name': info.req_mode_name,
                       'dept_code': info.dep_code,
                       'dm_code': info.dm_code,
                       'request_status_code': info.req_status,
                       'salesman_code': info.sm_code,
                       'req_remarks': info.req_remarks
                   }
                   $scope.geteditDataService(info.req_no);
               }
           }

           $scope.CurrentDate = new Date();
           var today = new Date();
           var dd = today.getDate();
           var mm = today.getMonth() + 1;
           var yyyy = today.getFullYear();

           $scope.RemoveEnrollMentNo = function ($event, index, str) {
               str.splice(index, 1);
           }

           //Fetch Data For Item...

           $scope.Fetch = function () {
               debugger;//
               $scope.table1 = false;
               $scope.PI = {};
               $scope.PI.req_type = $scope.temp.request_mode_code
               if ($scope.temp1 != undefined) {
                   if ($scope.temp1.sup_sblgr_acno != undefined && $scope.temp1.sup_sblgr_acno != '')
                       $scope.PI.rd_line_no = $scope.temp1.sup_sblgr_acno
                   if ($scope.temp1.code != undefined && $scope.temp1.code != '')
                       $scope.PI.sm_code = $scope.temp1.code
                   if ($scope.temp1.category_code != undefined && $scope.temp1.category_code != '')
                       $scope.PI.final_qnt = $scope.temp1.category_code
                   if ($scope.temp1.subcategory_code != undefined && $scope.temp1.subcategory_code != '')
                       $scope.PI.rd_quantity = $scope.temp1.subcategory_code
                   if ($scope.temp1.im_inv_no != undefined && $scope.temp1.im_inv_no != '')
                       $scope.PI.im_inv_no = $scope.temp1.im_inv_no
                   if ($scope.temp1.im_desc != undefined && $scope.temp1.im_desc != '')
                       $scope.PI.rd_item_desc = $scope.temp1.im_desc
                   if ($scope.temp1.im_item_code != undefined && $scope.temp1.im_item_code != '')
                       $scope.PI.req_no = $scope.temp1.im_item_code
                   if ($scope.temp1.sec_code != undefined && $scope.temp1.sec_code != '')
                       $scope.PI.dept_code = $scope.temp1.sec_code
               }

               //if ($scope.itemSearch) {
               $http.get(ENV.apiUrl + "api/DeliveryMode/getItemSerch?data=" + JSON.stringify($scope.PI)).then(function (res1) {
                   $scope.GetItems = res1.data.table;
                   $scope.totalItems = $scope.GetItems.length;
                   $scope.todos = $scope.GetItems;
                   $scope.makeTodos();
                   $scope.hide_suc = true;
               });
               //}
           }

           //End

           //Fetch Data For Service...

           $scope.ServiceFetch = function () {
               if ($scope.ServiceSearch) {
                   debugger;
                   $http.get(ENV.apiUrl + "api/RequestDetail/getServiceSearch?im_no=" + $scope.temp2.im_inv_no + "&des_ser=" + $scope.temp2.im_desc + "&dep_name=" + $scope.temp2.dept_name).then(function (res1) {
                       $scope.GetItems = res1.data;
                       $scope.totalItems = $scope.GetItems.length;
                       $scope.todos = $scope.GetItems;
                       $scope.makeTodos();
                       $scope.hide_suc = true;
                   });
               }
           }

           //End

           //Table Assign For Insert Request Detail...
           var invno;

           $scope.okbuttonclick = function () {

               if ($scope.temp.request_mode_code == 'I') {
                   debugger;
                   if ($scope.itemSearch) {

                       for (var i = 0; i < $scope.GetItems.length; i++) {
                           var t = $scope.GetItems[i].im_inv_no;
                           var v = document.getElementById(t + i);
                           if (v.checked == true) {
                               if ($scope.itemcodedata == undefined)
                                   $scope.itemcodedata = $scope.GetItems[i].im_item_code;
                               else
                                   $scope.itemcodedata = $scope.GetItems[i].im_item_code + ',' + $scope.itemcodedata;
                           }
                       }
                       if ($scope.itemcodedata != undefined) {
                           $scope.desc_hide = false;
                           $scope.desc_hide1 = true;
                       }


                   }
                   else {
                       $scope.itemcodedata = '';
                       for (var i = 0; i < $scope.GetItems.length; i++) {
                           var t = $scope.GetItems[i].im_inv_no;
                           var v = document.getElementById(t + i);
                           if (v.checked == true) {
                               if ($scope.itemcodedata == undefined)
                                   $scope.itemcodedata = $scope.GetItems[i].im_item_code;
                               else
                                   $scope.itemcodedata = $scope.GetItems[i].im_item_code + ',' + $scope.itemcodedata;
                           }
                       }
                       if ($scope.itemcodedata != undefined) {
                           $scope.desc_hide = false;
                           $scope.desc_hide1 = true;
                       }
                   }
                   $scope.Reset();
               }

               if ($scope.temp.request_mode_code == 'S') {
                   $scope.itemcodedata = '';
                   if ($scope.ServiceSearch) {
                       for (var i = 0; i < $scope.GetItems.length; i++) {
                           if ($scope.GetItems[i].ischecked == true) {
                               if ($scope.itemcodedata == undefined)
                                   $scope.itemcodedata = $scope.GetItems[i].im_item_code;
                               else
                                   $scope.itemcodedata = $scope.GetItems[i].im_item_code + ',' + $scope.itemcodedata;
                           }
                       }
                       if ($scope.itemcodedata != undefined) {
                           $scope.desc_hide = false;
                           $scope.desc_hide1 = true;
                       }
                   }
                   else {
                       $scope.itemcodedata = '';
                       for (var i = 0; i < $scope.GetItems.length; i++) {
                           if ($scope.GetItems[i].ischecked == true) {
                               if ($scope.itemcodedata == undefined)
                                   $scope.itemcodedata = $scope.GetItems[i].im_item_code;
                               else
                                   $scope.itemcodedata = $scope.GetItems[i].im_item_code + ',' + $scope.itemcodedata;
                           }

                       }
                       if ($scope.itemcodedata != undefined) {
                           $scope.desc_hide = false;
                           $scope.desc_hide1 = true;
                       }
                   }
                   $scope.ServiceReset();
               }

               $scope.GetItems = [];
           }

           var datasend = [];
           var datasend1 = [];
           var datasend2 = [];

           $scope.IsChangeFunction = function (info) {
               info.ischange = true;
           }

           //Select Data SHOW
           $http.get(ENV.apiUrl + "api/InvsParameter/GetCompanyName").then(function (docstatus1) {
               $scope.Compname = docstatus1.data;
               console.log($scope.Compname);
           });

           $scope.size = function (str) {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
                if (str == "All") {
                    $scope.currentPage = 1;
                    $scope.numPerPage = $scope.rows.length;
                    $scope.makeTodos();

                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
           }

           $scope.index = function (str) {
               $scope.pageindex = str;
               $scope.currentPage = str;
               $scope.makeTodos();

               main = document.getElementById('mainchk');
               if (main.checked == true) {
                   main.checked = false;
                   $scope.row1 = '';
                   $scope.color = '#edefef';
               }
           }

           $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

           $scope.makeTodos = function () {
               var rem = parseInt($scope.totalItems % $scope.numPerPage);
               if (rem == '0') {
                   $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
               }
               else {
                   $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
               }

               var begin = (($scope.currentPage - 1) * $scope.numPerPage);
               var end = parseInt(begin) + parseInt($scope.numPerPage);

               console.log("begin=" + begin); console.log("end=" + end);

               $scope.filteredTodos = $scope.todos.slice(begin, end);
           };

           $scope.searched = function (valLists, toSearch) {
               return _.filter(valLists,

               function (i) {
                   /* Search Text in all  fields */
                   return searchUtil(i, toSearch);
               });
           };

           //Search
           $scope.search = function () {
               $scope.todos = $scope.searched($scope.getParameter, $scope.searchText);
               $scope.totalItems = $scope.todos.length;
               $scope.currentPage = '1';
               if ($scope.searchText == '') {
                   $scope.todos = $scope.getParameter;
               }
               $scope.makeTodos();
               main.checked = false;
               $scope.CheckAllChecked();
           }

           function searchUtil(item, toSearch) {
               /* Search Text in all 3 fields */
               return (item.invs_appl_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                       item.invs_appl_form_field.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                       item.invs_appl_parameter.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                       item.invs_appl_form_field_value1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                       item.acno == toSearch) ? true : false;
           }

           //DATA CANCEL

           $scope.Clear = function () {
               $scope.im_item_codel = "";
               $scope.temp.uom_code = "";
               $scope.temp.sg_name = "";
               $scope.temp.sup_code = "";
               $scope.temp.im_inv_no = "";
               item_code = "";
               $scope.hide_suc = false;
               $scope.table = true;
               $scope.table1 = false;
               //$state.go('main.InvIMS');
               $("#cmb_seritm_code").select2("val", "");
               $scope.desc_hide = true;
               $scope.desc_hide1 = false;
               $scope.dt = {
                   req_date: dateyear,
                   rd_date_required: day + "-" + expMonth + "-" + year,
               }
               $scope.itemcodedata = '';
               $scope.delivery_mode();
           }

           $scope.Cancel = function () {
               $scope.Clear();
               $scope.im_inv_no = [];
               $scope.search_Req_Data = [];
               $scope.table = true;
               $scope.Main_table = false;
               $scope.Main_table_Service = false;
               $scope.display = false;
               $scope.savedisabled = false;
               $scope.updisabled = true;

               $scope.temp.req_no = "";
               $scope.temp.salesman_code = "";
               $scope.temp.req_remarks = "";
               $scope.dt.rd_date_required = "";
               $scope.Myform.$setPristine();
               $scope.Myform.$setUntouched();
               $scope.hide_suc = false;
               $scope.im_item_codel = "";
               $("#cmb_seritm_code").select2("val", "");
               $scope.dt = {
                   req_date: dateyear,
                   rd_date_required: day + "-" + expMonth + "-" + year,
               }
           }

           $scope.clearonadd = function () {

               $scope.temp.req_no = "";
               //$scope.temp.dept_code = "";
               //$scope.temp.dm_code = "";
               $scope.temp.salesman_code = "";
               $scope.temp.req_remarks = "";
           }

           $timeout(function () {
               $("#fixTable").tableHeadFixer({ 'top': 1 });
           }, 100);

           $timeout(function () {
               $("#fixTable1").tableHeadFixer({ 'top': 1 });
           }, 100);

           $scope.Reset = function () {
               $scope.temp1 = {
                   im_inv_no: '',
                   im_desc: '',
                   im_item_code: '',
                   dept_code: '',
                   sec_code: '',
                   code: '',
                   category_code: '',
                   subcategory_code: '',
                   im_assembly_ind: '',
                   //sup_sblgr_acno:''
               }
               $scope.GetItems = [];
           }

           $scope.ServiceReset = function () {
               $scope.temp2 = {
                   im_inv_no: '',
                   im_desc: '',
                   dept_name: '',
               }
               $scope.GetItems = [];
           }

           $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
               $('input[type="text"]', $(this).parent()).focus();
           });

           $('*[data-datepicker="true"] input[type="text"]').datepicker({
               todayBtn: true,
               orientation: "top left",
               autoclose: true,
               todayHighlight: true,
               format: 'dd-mm-yyyy'
           });

           $scope.CheckAllItems = function () {
               main = document.getElementById('mainchk');
               if (main.checked == true) {
                   for (var i = 0; i < $scope.GetItems.length; i++) {
                       var v = document.getElementById($scope.GetItems[i].im_inv_no + i);
                       v.checked = true;
                       $scope.row1 = 'row_selected';
                       $('tr').addClass("row_selected");
                   }
               }
               else {

                   for (var i = 0; i < $scope.GetItems.length; i++) {
                       var v = document.getElementById($scope.GetItems[i].im_inv_no + i);
                       v.checked = false;
                       main.checked = false;
                       $scope.row1 = '';
                       $('tr').removeClass("row_selected");
                   }
               }
           }

           $scope.checkonebyoneselectItem = function () {

               $("input[type='checkbox']").change(function (e) {

                   if ($(this).is(":checked")) {
                       $(this).closest('tr').addClass("row_selected");
                       $scope.color = '#edefef';
                   }
                   else {
                       $(this).closest('tr').removeClass("row_selected");
                       $scope.color = '#edefef';
                   }
               });

               main = document.getElementById('mainchk');
               if (main.checked == true) {
                   main.checked = false;
                   $("input[type='checkbox']").change(function (e) {
                       if ($(this).is(":checked")) {
                           $(this).closest('tr').addClass("row_selected");
                       }
                       else {
                           $(this).closest('tr').removeClass("row_selected");
                       }
                   });
               }
           }

           $scope.CheckAllService = function () {
               main1 = document.getElementById('mainchk1');
               for (var i = 0; i < $scope.GetItems.length; i++) {
                   if (main1.checked == true) {
                       $scope.GetItems[i].ischecked = true;
                       $scope.row1 = 'row_selected';
                       $('tr').addClass("row_selected");
                   }
                   else {


                       $scope.GetItems[i].ischecked = false;
                       main1.checked = false;
                       $scope.row1 = '';
                       $('tr').removeClass("row_selected");
                   }
               }


           }

           $scope.checkonebyoneselectServices = function () {

               $("input[type='checkbox']").change(function (e) {

                   if ($(this).is(":checked")) {
                       $(this).closest('tr').addClass("row_selected");
                       $scope.color = '#edefef';
                   }
                   else {
                       $(this).closest('tr').removeClass("row_selected");
                       $scope.color = '#edefef';
                   }
               });

               main1 = document.getElementById('mainchk1');
               if (main1.checked == true) {
                   main1.checked = false;
                   $("input[type='checkbox']").change(function (e) {
                       if ($(this).is(":checked")) {
                           $(this).closest('tr').addClass("row_selected");
                       }
                       else {
                           $(this).closest('tr').removeClass("row_selected");
                       }
                   });
               }
           }

           $scope.showdetails = function () {
               debugger;
               $scope.IP = {};

               $scope.IP.req_type = $scope.temp.request_mode_code
               if ($scope.itemcodedata != undefined && $scope.itemcodedata != '')
                   $scope.IP.im_inv_no = $scope.itemcodedata
               else
                   $scope.IP.im_inv_no = item_code
               $scope.IP.rd_from_required = $scope.dt.rd_date_required,
               $scope.IP.rd_up_required = $scope.dt.req_date,

               $http.get(ENV.apiUrl + "api/DeliveryMode/getItemmovementdetails?data=" + JSON.stringify($scope.IP)).then(function (res) {
                   $scope.rows = res.data.table;
                   $scope.totalItems = $scope.rows.length;
                   $scope.todos = $scope.rows;
                   $scope.makeTodos();
                   $scope.table1 = true;
               });

               //datasend = [];
               //datasend1 = [];
               //$scope.table = true;
               //$scope.display = false;
               //$scope.Cancel();
           }

       }])

})();