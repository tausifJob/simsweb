﻿(function () {
    'use strict';
    var obj1, temp, opr, edt, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var check;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('ItemAssemblyCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.priview_table = false;
            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Main_table = false;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.temp = [];
            $scope.im_inv_no = [];
            $scope.edt = {};
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //Fill combo Assembly
            $http.get(ENV.apiUrl + "api/ItemAssembly/GetAssembly").then(function (docstatus1) {

                $scope.fillcombo = docstatus1.data;

            });


            //MOdel2 fetch button
            $scope.btn_Fetch_ItemDetails = function (im_inv_no, im_item_code, im_desc, old_dep_code, sec_code, invs021_sup_code, invs021_sup_group_code, category_code, subcategory_code) {

                $scope.temp1 = {
                    im_assembly_ind: true,
                    im_inv_no: im_inv_no,
                    im_item_code: im_item_code,
                    im_desc: im_desc,
                    old_dep_code: old_dep_code,
                    sec_code: sec_code,
                    invs021_sup_code: invs021_sup_code,
                    invs021_sup_group_code: invs021_sup_group_code,
                    category_code: category_code,
                    subcategory_code: subcategory_code
                };
                $scope.busy = true;
                $scope.searchtable = false;
                $http.post(ENV.apiUrl + "api/ItemAssembly/Fetch_ItemDetails", $scope.temp1).then(function (Fetch_ItemDetails) {
                    $scope.Fetch_ItemDetails = Fetch_ItemDetails.data;
                    if ($scope.Fetch_ItemDetails.length <= 0) {
                        $scope.searchtable = false;
                        swal({ text: 'Sorry Data Not Found', width: 380, showCloseButton: true });
                    }
                    else {
                        $scope.searchtable = true;
                    }

                    $scope.busy = false;
                });
            }


            //Edit button in small window
            $scope.getselected_items = function (str) {

                $scope.inv_no = str.im_inv_no;
                $scope.inv_desc = str.im_desc;
            }



            //getitemdetals_search(SET)
            $scope.getitemdetals_search = function (im_inv_no, im_item_code, im_desc, old_dep_code, sec_code, invs021_sup_code, invs021_sup_group_code, category_code, subcategory_code)//
            {
                $scope.searchtable = false;
                $('#MyModal2').modal('show');
                //$scope.btn_Fetch_ItemDetails(im_inv_no, im_item_code, im_desc, old_dep_code, sec_code, invs021_sup_code, invs021_sup_group_code, category_code, subcategory_code)
                $scope.btn_Fetch_ItemDetails(im_inv_no, im_item_code, im_desc, old_dep_code, sec_code, invs021_sup_code, invs021_sup_group_code, category_code, subcategory_code)
            }


            //PreviewButton
            $scope.Preview = function (assembly) {


                if ($scope.temp.assembly == 'undefined' || $scope.temp.assembly == null) {
                    swal({ title: "Alert", text: "Select Assembly No", width: 300, height: 200 });
                    exit;
                }
                else {

                    $http.post(ENV.apiUrl + "api/ItemAssembly/SelectPreview?assembly=" + $scope.temp.assembly).then(function (docstatus1) {

                        $scope.PriviewData = docstatus1.data;

                    });
                }

                $scope.priview_table = true;
            }

            $scope.Reset = function () {
                $scope.edt = {};
                $scope.inv_no = '';
                $scope.inv_desc = '';
                $scope.temp = { assembly: '' };
                $scope.temp = '';
                $scope.priview_table = false;
            }

            $scope.SelectIetm = function () {
                $scope.GetItems = [];
                $scope.temp = {};
                $scope.itemSearch = true;
                $scope.temp.check = true;
                $('#MyModal').modal('show');
                $scope.display = true;
            }

            $scope.checkvalue = function () {
                if ($scope.temp.check == true) {
                    $scope.check = "Y";
                }
            }


            $scope.SelectAssembly = function () {
                $scope.itemSearch = false;
                $scope.GetItems = [];
                $('#MyModal').modal('show');
                $scope.display = true;
            }

            $scope.RemoveEnrollMentNo = function ($event, index, str) {

                str.splice(index, 1);

            }
            //Fetch data
            $scope.Fetch = function (im_inv_no, im_desc, im_item_code, invs021_dep_code, sec_code, invs021_sup_code, category_code, subcategory_code) {

                //$scope.temp = {
                //  im_assembly_ind: true,
                //  im_inv_no: im_inv_no,
                //  im_item_code: im_item_code,
                //  im_desc: im_desc,
                //  old_dep_code: old_dep_code,
                //  sec_code: sec_code,
                //  invs021_sup_code: invs021_sup_code,
                //  invs021_sup_group_code: invs021_sup_group_code,
                //  category_code: category_code,
                //  subcategory_code: subcategory_code
                //};
                if ($scope.edt == undefined) {
                    $scope.temp1 = {
                        category_code: '',
                        subcategory_code: ''
                    }

                }

                $http.get(ENV.apiUrl + "api/ItemAssembly/getItemSerch?im_inv_no=" + $scope.edt.im_inv_no + "&im_desc=" + $scope.edt.im_desc + "&im_item_code=" + $scope.edt.im_item_code + "&invs021_dep_code=" + $scope.edt.invs021_dep_code + "&sec_code=" + $scope.edt.sec_code + "&invs021_sup_code=" + $scope.edt.invs021_sup_code + "&category_code=" + $scope.edt.category_code + "&subcategory_code=" + $scope.edt.subcategory_code).then(function (res1) {
                    $scope.GetItems = res1.data;
                    $scope.totalItems = $scope.GetItems.length;
                    $scope.todos = $scope.GetItems;
                    $scope.makeTodos();
                });


            }

            //Table assign
            var invno;
            $scope.okbuttonclick = function () {

                $scope.edt = '';
                //  $scope.temp["curr_qty"] = '0';
                if ($scope.itemSearch) {

                    for (var i = 0; i < $scope.GetItems.length; i++) {
                        var t = $scope.GetItems[i].im_inv_no;
                        var v = document.getElementById(t + i);
                        if (v.checked == true) {
                            $scope.GetItems[i].curr_qty = '1';
                            $scope.im_inv_no.push($scope.GetItems[i]);
                        }
                    }
                    $scope.Main_table = true;
                }
                else {
                    for (var i = 0; i < $scope.GetItems.length; i++) {
                        var t = $scope.GetItems[i].im_inv_no;
                        var v = document.getElementById(t + i);
                        if (v.checked == true) {
                            $scope.inv_no = $scope.GetItems[i].im_inv_no
                            $scope.inv_desc = $scope.GetItems[i].im_desc
                            invno = $scope.inv_no;
                        }

                    }
                }
                $scope.GetItems = [];
            }

            //INsert
            $scope.Done = function () {

                var datasend = [];
                for (var i = 0; i < $scope.im_inv_no.length; i++) {
                    var data = {
                        'im_inv_no': $scope.im_inv_no[i].im_inv_no,
                        'im_inv_no_for': $scope.inv_no,//invno,
                        'ia_component_qty': $scope.im_inv_no[i].curr_qty,
                        opr: 'I'
                    }
                    datasend.push(data);
                }
                $http.post(ENV.apiUrl + "api/ItemAssembly/CUDInvsItemSearch", datasend).then(function (msg) {
                    datasend = [];
                    $scope.msg1 = msg.data;
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    }
                    else {
                        swal({ text: "Record already present. " + $scope.msg1, imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                });
                datasend = [];
                $scope.Main_table = false;
                $scope.inv_no = '';
                $scope.inv_desc = '';
            }

            //Update
            $scope.Update = function (j) {
                var datasend = [];

                //  for (var i = 0; i < $scope.PriviewData.length; i++) {
                var data = {
                    'im_inv_no': j.im_inv_no,
                    'im_inv_no_for': $scope.temp.assembly,
                    'ia_component_qty': j.ia_component_qty,
                }

                //  }
                data.opr = "U";
                datasend.push(data);

                $http.post(ENV.apiUrl + "api/ItemAssembly/CUDInvsItemSearch", datasend).then(function (msg) {
                    datasend = [];
                    $scope.msg1 = msg.data;
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                });
                datasend = [];
            }


            //Delete
            $scope.Delete = function (j) {

                var datasend = [];
                var data = {
                    'im_inv_no': j.im_inv_no,
                    'im_inv_no_for': $scope.temp.assembly,
                }
                data.opr = "D";
                datasend.push(data);

                $http.post(ENV.apiUrl + "api/ItemAssembly/CUDInvsItemSearch", datasend).then(function (msg) {
                    datasend = [];
                    $scope.msg1 = msg.data;
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        $scope.Preview(data.im_inv_no_for);
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                });
                datasend = [];
            }


            //Data Field small tebale
            $http.get(ENV.apiUrl + "api/InvLPO/get_Invs021_get_supplier_group_name").then(function (Invs021_get_supplier_group_name) {
                $scope.Invs021_get_supplier_group_name = Invs021_get_supplier_group_name.data;

            });

            $http.get(ENV.apiUrl + "api/InvLPO/get_Invs021_get_Categories").then(function (Invs021_get_Categories) {
                $scope.Invs021_get_Categories = Invs021_get_Categories.data;

            });

            $scope.getsubcategory_code_set = function () {

                $http.get(ENV.apiUrl + "api/InvLPO/get_Invs021_get_SubCategories?pc_parentcode=" + $scope.temp1.category_code).then(function (Invs021_get_SubCategories_set) {
                    $scope.Invs021_get_SubCategories_set_data = Invs021_get_SubCategories_set.data;

                });
            }


            $scope.getsubcategory_code = function () {

                $http.get(ENV.apiUrl + "api/InvLPO/get_Invs021_get_SubCategories?pc_parentcode=" + $scope.edt.category_code).then(function (Invs021_get_SubCategories) {
                    $scope.Invs021_get_SubCategories = Invs021_get_SubCategories.data;

                });
            }

            $http.get(ENV.apiUrl + "api/InvLPO/Get_Adj_Suppliername").then(function (Get_Adj_Suppliername) {
                $scope.Get_Adj_Suppliername = Get_Adj_Suppliername.data;

            });



            //Select Data SHOW



            $http.get(ENV.apiUrl + "api/InvsParameter/GetCompanyName").then(function (docstatus1) {
                $scope.Compname = docstatus1.data;

            });


            $scope.size = function (str) {

                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);



                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.getParameter, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.getParameter;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.invs_appl_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.invs_appl_form_field.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.invs_appl_parameter.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.invs_appl_form_field_value1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }


            $scope.Reset1 = function () {

                $scope.edt = {};
                //  $scope.temp = '';

                $scope.edt = { im_assembly_ind: true };
            }

            $scope.Reset2 = function () {
                $scope.temp1 = '';
                $scope.temp1 = { im_assembly_ind: true };
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });


        }])

})();
