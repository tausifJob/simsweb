﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LPOProcessCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            var dt = new Date();
            $scope.IP = {
                //dateFrom: ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear(),
                //dateUpto: ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear(),
            };
            $scope.IP = {
                dateFrom: dd + '-' + mm + '-' + yyyy,
                dateUpto: dd + '-' + mm + '-' + yyyy
            }
            $scope.sel = {};
            $scope.rows = [];
            $scope.ex = {
                inv_date: ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear(),
            };
            $scope.show = function () {
                debugger;
                if ($scope.IP['sup_code']) {
                    $http.get(ENV.apiUrl + "api/LPOGRM/getOrderApprovedDetails?data=" + JSON.stringify($scope.IP)).then(function (res) {
                        $scope.rows = res.data.table;
                        $scope.Allrows = res.data.table1;
                        if ($scope.Allrows.length <= 0) {
                            swal({ title: "Alert", text: "No order pending for  approval/ Release in given period", showCloseButton: true, width: 380, });
                        }
                        else {
                            for (var r in $scope.rows) {
                                $scope.rows[r]['sims_icon'] = "fa fa-plus-circle";
                                $scope.rows[r]['isexpanded'] = "none";
                                $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].adj_doc_no);
                                if ($scope.rows[r].order_approval_status == '2' && $scope.rows[r].od_status == '2') {
                                    $scope.rows[r]['ord_status_info'] = 'Approval pending'
                                }
                                else {
                                    $scope.rows[r]['ord_status_info'] = 'Approved'
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Supplier", showCloseButton: true, width: 380, });
                }
            }
            $scope.show2 = function () {
                $http.get(ENV.apiUrl + "api/LPOGRM/getOrderApprovedDetails?data=" + JSON.stringify($scope.IP)).then(function (res) {
                    $scope.rows = res.data.table;
                    $scope.Allrows = res.data.table1;
                    if ($scope.Allrows.length <= 0) {                        
                    }
                    else {
                        for (var r in $scope.rows) {
                            $scope.rows[r]['sims_icon'] = "fa fa-plus-circle";
                            $scope.rows[r]['isexpanded'] = "none";
                            $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].adj_doc_no);
                            if ($scope.rows[r].order_approval_status == '2' && $scope.rows[r].od_status == '2') {
                                $scope.rows[r]['ord_status_info'] = 'Approval pending'
                            }
                            else {
                                $scope.rows[r]['ord_status_info'] = 'Approved'
                            }
                        }
                    }
                });
            }

            function getSubitems(dno) {
                var arr = [];
                for (var i = 0; i < $scope.Allrows.length; i++) {
                    if ($scope.Allrows[i].adj_doc_no == dno) {
                        arr.push($scope.Allrows[i]);
                    }
                }
                return arr;
            }

            $scope.expand = function (j, $event) {
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";

                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none";
                }
            }

            $scope.reset = function () {
                $scope.IP = {};
                $scope.rows = {};
                $scope.Allrows = {};
                $scope.IP.sup_code = $scope.sel['SUP'][0].sup_code;
            }

            $scope.checkRange = function (s) {
                var rq = parseInt(s.remqty);
                if (rq == NaN)
                    rq = 0;
                s.remqty = rq;
            }

            $http.get(ENV.apiUrl + "api/AdjustmentReason/getDept").then(function (res) {
                $scope.sel['Dept'] = res.data.table;
            });

            $http.get(ENV.apiUrl + "api/AdjustmentReason/getDSSup").then(function (res) {
                $scope.sel['SUP'] = res.data.table;
                $scope.IP.sup_code = $scope.sel['SUP'][0].sup_code;
            });

            $scope.pendingReuest = function () {
                $('#loader').modal({ backdrop: 'static', keyboard: false });
                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    if ($scope.rows[i].isChecked) {
                        if ($scope.rows[i].ord_status_info == 'Approval pending') {
                            for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                                var ob = {};
                                ob.doc_no = $scope.rows[i].subItems[j].adj_doc_no;
                                ob.doc_line_no = $scope.rows[i].subItems[j].ad_line_no;
                                ob.item_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                                ob.r_qty = $scope.rows[i].subItems[j].remqty;
                                ob.grv_no = $scope.rows[i].subItems[j].adj_grv_no;
                                ob.t_qty = $scope.rows[i].subItems[j].ad_qty;
                                ob.adj_status = $scope.rows[i].subItems[j].adj_status;
                                ob.r_Type = $scope.rows[i].subItems[j]['r_Type'];
                                ob.req_no = $scope.rows[i].subItems[j]['req_no'];
                                ob.rd_line_no = $scope.rows[i].subItems[j]['rd_line_no'];
                                ob.remark = $scope.rows[i].subItems[j]['remarks'];
                                ob.grn_inv_no = $scope.ex.inv_no;
                                //ob.grn_inv_date = $scope.ex.inv_date;
                                ob.ucode = $rootScope.globals.currentUser.username;
                                ob.typ = 's';
                                ob.val = r;
                                ob.opr = "PU";
                                ar.push(ob);
                            }
                        }
                    }
                }
                if (ar.length == 0) {
                    swal({ title: "Alert", text: 'No order pending for approval', showCloseButton: true, width: 380, });
                    $('#loader').modal('hide');
                    return;
                }

                $http.post(ENV.apiUrl + "api/LPOGRM/approveRejectLPOProcess", ar).then(function (res) {
                    $scope.show2();
                    $('#loader').modal('hide');
                    swal({ title: "Alert", text: res.data.strMessage, showCloseButton: true, width: 380, });
                });

            }

            $scope.approve = function () {
                $('#loader').modal({ backdrop: 'static', keyboard: false });
                var ar = [];
                var docno=[];
                var dep_code=[];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    if ($scope.rows[i].isChecked) {

                        if ($scope.rows[i].ord_status_info != 'Approval pending') {
                            for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                                var ob = {};
                                ob.doc_no = $scope.rows[i].subItems[j].adj_doc_no;
                                try {
                                    if (i==0) {
                                        docno=$scope.rows[i].subItems[j].adj_doc_no;
                                        dep_code=$scope.rows[i].subItems[j].dep_code;
                                    }
                                    else{
                                        docno=docno+','+$scope.rows[i].subItems[j].adj_doc_no;
                                        dep_code=dep_code+','+$scope.rows[i].subItems[j].dep_code;
                                    }
                                } catch (e) {
    
                                }
                                ob.doc_line_no = $scope.rows[i].subItems[j].ad_line_no;
                                ob.item_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                                ob.r_qty = $scope.rows[i].subItems[j].remqty;
                                ob.grv_no = $scope.rows[i].subItems[j].adj_grv_no;
                                ob.t_qty = $scope.rows[i].subItems[j].ad_qty;
                                ob.adj_status = $scope.rows[i].subItems[j].adj_status;
                                ob.r_Type = $scope.rows[i].subItems[j]['r_Type'];
                                ob.req_no = $scope.rows[i].subItems[j]['req_no'];
                                ob.rd_line_no = $scope.rows[i].subItems[j]['rd_line_no'];
                                ob.remark = $scope.rows[i].subItems[j]['remarks'];
                                ob.grn_inv_no = $scope.ex.inv_no;
                                //ob.grn_inv_date = $scope.ex.inv_date;
                                ob.ucode = $rootScope.globals.currentUser.username;
                                ob.typ = 's';
                                ob.val = r;
                                ob.opr = "AU";
                                ar.push(ob);
                            }
                        }
                    }
                }
                if (ar.length == 0) {
                    swal({ title: "Alert", text: 'No order Pending for Relese', showCloseButton: true, width: 380, });
                    $('#loader').modal('hide');
                    return;
                }

                $http.post(ENV.apiUrl + "api/LPOGRM/approveRejectLPOProcess", ar).then(function (res) {
                    //$scope.show2();
                    $('#loader').modal('hide');
                    swal({ title: "Alert", text: res.data.strMessage, showCloseButton: true, width: 380, });

                    var doc_sr_code_user = report_data.data;
                    console.log(doc_sr_code_user);
                    console.log($scope.stroe_report_name);
                    console.log(stroe_report_name);
                    var data = {
                        Param:$scope.com_code,
                        location: $scope.stroe_report_name,
                        dept_code: dep_code,
                        search:docno,
                        state: 'main.lpopro',
                        ready: function () {
                            this.refreshReport();
                        },
                    }
                    console.log(data);
                    window.localStorage["ReportDetails"] = JSON.stringify(data);
                    $state.go('main.ReportCardParameter');
                    
                });

            }

            $scope.reject = function () {
                $('#loader').modal({ backdrop: 'static', keyboard: false });
                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    if ($scope.rows[i].isChecked) {
                        for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                            var ob = {};
                            ob.doc_no = $scope.rows[i].subItems[j].adj_doc_no;
                            ob.doc_line_no = $scope.rows[i].subItems[j].ad_line_no;
                            ob.item_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.r_qty = $scope.rows[i].subItems[j].remqty;
                            ob.grv_no = $scope.rows[i].subItems[j].adj_grv_no;
                            ob.t_qty = $scope.rows[i].subItems[j].ad_qty;
                            ob.adj_status = $scope.rows[i].subItems[j].adj_status;
                            ob.r_Type = $scope.rows[i].subItems[j]['r_Type'];
                            ob.req_no = $scope.rows[i].subItems[j]['req_no'];
                            ob.rd_line_no = $scope.rows[i].subItems[j]['rd_line_no'];
                            ob.remark = $scope.rows[i].subItems[j]['remarks'];
                            ob.grn_inv_no = $scope.ex.inv_no;
                            //ob.grn_inv_date = $scope.ex.inv_date;
                            ob.ucode = $rootScope.globals.currentUser.username;
                            ob.typ = 's';
                            ob.val = r;
                            ob.opr = "RU";
                            ar.push(ob);
                        }
                    }
                }
                if (ar.length == 0) {
                    swal({ title: "Alert", text: 'No order select for Reject', showCloseButton: true, width: 380, });
                    $('#loader').modal('hide');
                    return;
                }

                $http.post(ENV.apiUrl + "api/LPOGRM/approveRejectLPOProcess", ar).then(function (res) {
                    $scope.show2();
                    $('#loader').modal('hide');
                    swal({ title: "Alert", text: res.data.strMessage, showCloseButton: true, width: 380, });
                });

            }
        
            var stroe_report_name = [];
            $http.get(ENV.apiUrl + "api/LPOGRM/getreport_name").then(function (getFullData) {
                debugger;
                $scope.stroe_report_name = getFullData.data;
            });
            var com_code = [];
            $http.get(ENV.apiUrl + "api/LPOGRM/getcomCode_name").then(function (getFullData) {
                debugger;
                $scope.com_code = getFullData.data;
            });
            $scope.cancel = function () {
                $scope.IP = {};
                $scope.rows = {};
                $scope.Allrows = {};
                $state.go('main.TimeLine');
            }

            $scope.Close = function () {
                $('#loader').modal({ backdrop: 'static', keyboard: false });
                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    if ($scope.rows[i].isChecked) {
                        for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                            var ob = {};
                            ob.doc_no = $scope.rows[i].subItems[j].adj_doc_no;
                            ob.doc_line_no = $scope.rows[i].subItems[j].ad_line_no;
                            ob.item_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.r_qty = $scope.rows[i].subItems[j].remqty;
                            ob.grv_no = $scope.rows[i].subItems[j].adj_grv_no;
                            ob.t_qty = $scope.rows[i].subItems[j].ad_qty;
                            ob.adj_status = $scope.rows[i].subItems[j].adj_status;
                            ob.r_Type = $scope.rows[i].subItems[j]['r_Type'];
                            ob.req_no = $scope.rows[i].subItems[j]['req_no'];
                            ob.rd_line_no = $scope.rows[i].subItems[j]['rd_line_no'];
                            ob.remark = $scope.rows[i].subItems[j]['remarks'];
                            ob.grn_inv_no = $scope.ex.inv_no;
                            ob.grn_inv_date = $scope.ex.inv_date;
                            ob.ucode = $rootScope.globals.currentUser.username;
                            ob.typ = 'C';
                            ob.val = r;
                            ar.push(ob);
                        }
                    }
                }
                if (ar.length == 0) {
                    swal({ title: "Alert", text: 'Nothing Selected', showCloseButton: true, width: 380, });
                    $('#loader').modal('hide');
                    return;
                }

                $http.post(ENV.apiUrl + "api/AdjustmentReason/saveAdjDetails", ar).then(function (res) {

                    $scope.show();
                    $('#loader').modal('hide');
                    swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
                });
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.IP = {
                dateFrom: dd + '-' + mm + '-' + yyyy,
                dateUpto: dd + '-' + mm + '-' + yyyy
            }


        }])
})();