﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var deletecode = [];
   
    var finanacecode = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SupplierMasterCont',
       ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

           $scope.pagesize = "All";
           $scope.pager = false;
           $scope.pageindex = 0;
           $scope.save_btn = true;
           var dataforSave = [];
           var formdata = new FormData();
           $scope.Update_btn = false;
           $scope.display = false;
           $scope.table = true;
           $scope.show_btn = false;
           $scope.acc_no = true;
           $scope.temp = {};
           //$scope.cmbstatus = true;
           //$scope.checked = false;
           $scope.user_access = [];

           $scope.appcode = $state.current.name.split('.');
           var user = $rootScope.globals.currentUser.username;

           $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
               debugger;
               $scope.user_rights = usr_rights.data;

               for (var i = 0; i < $scope.user_rights.length; i++) {

                   if (i == 0) {
                       $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                       $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                       $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                       $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                       $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                       $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                   }
               }

               //console.log($scope.user_access);
           });

          
           $timeout(function () {
               $("#fixTable").tableHeadFixer({ 'top': 1 });
           }, 100)

           setTimeout(function () {
               $("#cmb_acc_Code3").select2();

           }, 100);

           //Select Data SHOW
           $http.get(ENV.apiUrl + "api/Supplier/getCountry").then(function (cont) {
               $scope.country = cont.data;
           });

           $http.get(ENV.apiUrl + "api/Supplier/GetSupplierType").then(function (stype) {
               $scope.sup_type = stype.data;
           });

           $http.get(ENV.apiUrl + "api/Supplier/GetSupplierLocation").then(function (suploc) {
               $scope.location = suploc.data;
           });

           $http.get(ENV.apiUrl + "api/Supplier/Get_Currency").then(function (GetCurrency) {
               $scope.currency = GetCurrency.data;
           });

           $scope.lgcode = '02';
          $scope.comp_code = '';
          $scope.year = '';
          $http.get(ENV.apiUrl + "api/Supplier/Get_SblgrAc?lgcode=" + $scope.lgcode + "&comp_code=" + $scope.comp_code + "&year=" + $scope.year).then(function (res1) {
             
              $scope.accountcode = res1.data;

          });

           $http.get(ENV.apiUrl + "api/Supplier/Get_SupplierID").then(function (supl) {
               $scope.Supplier_ID = supl.data;
               $scope.temp.sup_code = $scope.Supplier_ID.sup_code;           
               console.log("$scope.Supplier_ID", $scope.Supplier_ID);
               console.log("$scope.sup_code", $scope.temp.sup_code);
           });
           //$scope.GetAllState = function (country_code) {
           //    $http.get(ENV.apiUrl + "api/Supplier/GetAllState?country_code=" + country_code).then(function (stat) {
           //        $scope.state = stat.data;
           //    });
           //}
           setTimeout(function () {
               $("#cmb_acc_Code3").select2();

           }, 100);

           $scope.lgcode = '02';
           $scope.comp_code = '';
           $scope.year = '';
           $http.get(ENV.apiUrl + "api/Supplier/Get_SblgrAc?lgcode=" + $scope.lgcode + "&comp_code=" + $scope.comp_code + "&year=" + $scope.year).then(function (res1) {

               $scope.accountcode = res1.data;

           });

           $http.get(ENV.apiUrl + "api/Supplier/GetAllCity").then(function (citycode) {
               $scope.city = citycode.data;
           });


           $http.get(ENV.apiUrl + "api/Supplier/GetAllSupplierGroupName").then(function (sgname) {
               $scope.sg = sgname.data;
           });

           $http.get(ENV.apiUrl + "api/Supplier/getSupplier").then(function (res1) {
               $scope.CreDiv = res1.data;
               $scope.totalItems = $scope.CreDiv.length;
               $scope.todos = $scope.CreDiv;
               $scope.makeTodos();
           });


           //$scope.size = function (str) {
           //    if (str == "All" || str == "all") {
           //        $scope.currentPage = '1';
           //        $scope.filteredTodos = $scope.CreDiv;
           //        $scope.pager = false;
           //    }
           //    else {
           //        $scope.pager = true;
           //        $scope.pagesize = str;
           //        $scope.currentPage = 1;
           //        $scope.numPerPage = str;
           //        $scope.makeTodos();
           //    }
           //}
           $scope.size = function (str) {
               debugger;
              
               if (str == "All") {
                   //$scope.currentPage = 1;
                   //$scope.pagesize = $scope.CreDiv.length;
                   $scope.pager = false;
                   //$scope.pagesize = $scope.CreDiv.length;
                   $scope.pagesize = 'All';
                   $scope.currentPage = 1;
                   $scope.numPerPage = $scope.CreDiv.length;
                   $scope.makeTodos();
               }
               else {
                   $scope.pager = true;
                   $scope.pagesize = str;
                   $scope.currentPage = 1;
                   $scope.numPerPage = str;
                   $scope.makeTodos();
               }
           }

          
           $scope.index = function (str) {
               $scope.pageindex = str;
               $scope.currentPage = str;
               console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
               main.checked = false;
               $scope.CheckAllChecked();
           }

           $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 100, $scope.maxSize = 10;

           $scope.makeTodos = function () {
               var rem = parseInt($scope.totalItems % $scope.numPerPage);
               if (rem == '0') {
                   $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
               }
               else {
                   $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
               }

               var begin = (($scope.currentPage - 1) * $scope.numPerPage);
               var end = parseInt(begin) + parseInt($scope.numPerPage);

               console.log("begin=" + begin); console.log("end=" + end);

               $scope.filteredTodos = $scope.todos.slice(begin, end);
           };

           $scope.searched = function (valLists, toSearch) {
               return _.filter(valLists,

               function (i) {
                   /* Search Text in all  fields */
                   return searchUtil(i, toSearch);
               });
           };


           //Search
           $scope.search = function () {
               debugger
               $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
               $scope.totalItems = $scope.todos.length;
               $scope.currentPage = '1';
               if ($scope.searchText == '') {
                   $scope.todos = $scope.CreDiv;
               }
               $scope.makeTodos();
           }

           function searchUtil(item, toSearch) {
               /* Search Text in all 3 fields */
               return (item.sg_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sup_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sup_code == toSearch ||
                        item.sec_stock_account_no == toSearch) ? true : false;
           }

           //NEW BUTTON
           $scope.New = function () {
               debugger;
               if ($scope.user_access.data_insert == false) {
                   swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
               }
               else {

                   $scope.disabled = false;
                   $scope.gdisabled = false;
                   $scope.aydisabled = false;
                   $scope.table = false;
                   $scope.display = true;
                   $scope.save_btn = true;
                   $scope.Update_btn = false;
                   $scope.show_btn = true;
                   $scope.divcode_readonly = false;
                   $scope.temp = {};

                   $scope.temp.sup_code = $scope.Supplier_ID.sup_code;

                   $('#cmb_acc_Code3').select2("val", "");
               }
           }

           //DATA CANCEL
           $scope.Cancel = function () {
               debugger
               $scope.temp = "";
               $scope.table = true;
               $scope.display = false;
           }

           //DATA EDIT
           $scope.edit = function (str) {
               debugger;
               if ($scope.user_access.data_update == false) {
                   swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
               }
               else {
                 
                   $scope.gdisabled = false;
                   $scope.aydisabled = true;
                   $scope.table = false;
                   $scope.display = true;
                   $scope.save_btn = false;
                   $scope.Update_btn = true;
                   $scope.show_btn = false;
                   $scope.divcode_readonly = true;
                   $scope.dep = true;
                  
                   $scope.temp = {
                       sup_code: str.sup_code,
                       sg_name: str.sg_name,
                       sg_desc: str.sg_desc,
                       sup_name: str.sup_name,
                       cur_code: str.cur_code,
                       cur_name: str.cur_name,
                       sup_location_ind: str.sup_location_ind,
                       sup_location_ind_name: str.sup_location_ind_name,
                       sup_orig_code: str.sup_orig_code,
                       sup_contact_name: str.sup_contact_name,
                       sup_contact_designation: str.sup_contact_designation,
                       sup_address1: str.sup_address1,
                       sup_address2: str.sup_address2,
                       sup_address3: str.sup_address3,
                       sup_city: str.sup_city,
                       con_code: str.con_code,
                       con_name: str.con_name,
                       sup_tel_no: str.sup_tel_no,
                       sup_fax_no: str.sup_fax_no,
                       sup_telex_no: str.sup_telex_no,
                       sup_mobile_no: str.sup_mobile_no,
                       sup_email_address: str.sup_email_address,
                       sup_web_site: str.sup_web_site,
                       sup_credit_period: str.sup_credit_period,
                       sup_discount_pct: str.sup_discount_pct,
                       sup_lead_time_air: str.sup_lead_time_air,
                       sup_lead_time_sea: str.sup_lead_time_sea,
                       sup_lead_time_road: str.sup_lead_time_road,
                       sup_lead_time_courier: str.sup_lead_time_courier,
                       sup_type: str.sup_type,
                       sup_type_name: str.sup_type_name,
                       sup_agent_comm_pct: str.sup_agent_comm_pct,
                       sup_remarks: str.sup_remarks,
                       sup_rating: str.sup_rating,
                       sup_sblgr_acno: str.sup_sblgr_acno,
                       sup_sblgr_name: str.sup_sblgr_name,
                       supplier_desc_type: str.supplier_desc_type
                   };
                   $('#cmb_acc_Code3').select2("val", str.sup_sblgr_acno);
                   $scope.Myform.$setPristine();
                   $scope.Myform.$setUntouched();
               }
           }

           $scope.Sblgr_search = function () {
               //$scope.Reset();
               $scope.pagesize = "All";
               $scope.pageindex = 0;
               $('#myModal').modal('show');
               $scope.searchtable = false;
               $scope.edt = '';

               $scope.size = function (str) {
                   debugger;
                   alert(str);
                   if (str == "All" || str == "all") {
                       //$scope.currentPage = 1;
                       //$scope.pagesize = $scope.CreDiv.length;
                       $scope.pager = true;
                       $scope.pagesize = $scope.CreDiv.length;
                       $scope.currentPage = 1;
                       $scope.numPerPage = $scope.CreDiv.length;
                       $scope.makeTodos();
                   }
                   else {
                       $scope.pager = true;
                       $scope.pagesize = str;
                       $scope.currentPage = 1;
                       $scope.numPerPage = str;
                       $scope.makeTodos();
                   }
               }

               $scope.index = function (str) {
                   $scope.pageindex = str;
                   $scope.currentPage = str;
                   console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
               }

               $scope.acsearch = [], $scope.currentPage = 1, $scope.numPerPage = 100, $scope.maxSize = 100;

               $scope.makeTodos = function () {
                   debugger
                   var rem = parseInt($scope.totalItems % $scope.numPerPage);
                   if (rem == '0') {
                       $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                   }
                   else {
                       $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                   }

                   var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                   var end = parseInt(begin) + parseInt($scope.numPerPage);

                   console.log("begin=" + begin); console.log("end=" + end);

                   $scope.acsearch = $scope.todos.slice(begin, end);
               };

               $scope.Sblgr_show = function () {
                   $scope.searchtable = true;
                   if ($scope.edt == undefined || $scope.edt == null) {
                       $scope.edt = '';
                   }
                   
                   $http.get(ENV.apiUrl + "api/Supplier/Get_SblgrAc?lgcode=" + $scope.edt.slac_ldgr_code + "&sblac=" + $scope.edt.sup_sblgr_acno + "&supname=" + $scope.edt.sup_sblgr_name).then(function (res1) {
                      
                       $scope.acsearch = res1.data;
                       $scope.totalItems = $scope.acsearch.length;
                       $scope.todos = $scope.acsearch;
                       $scope.makeTodos();
                       if (res1.data.length > 0) { }
                       else {
                           $scope.ImageView = true;
                       }
                   });
                  

               }

               $scope.Reset = function () {
                   $scope.edt = '';
                   $scope.acsearch = [];
                   $scope.searchtable = false;
               }
           }

           //$scope.acsearch = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;



           $scope.checkonebyone = function (info) {
               for (var i = 0; i < $scope.acsearch.length; i++) {
                   var d = document.getElementById($scope.acsearch[i].sup_sblgr_acno + i);
                   if (d.checked == true) {
                       $scope.acsearch[i].ischange = true;
                   }
                   else {
                       $scope.acsearch[i].ischange = false;
                   }
               }

               $("input[type='radio']").change(function (e) {
                   if ($(this).is(":checked")) { //If the checkbox is checked
                       $(this).closest('tr').addClass("row_selected");
                       //Add class on checkbox checked
                       $scope.color = '#edefef';
                   }
                   else {
                       $(this).closest('tr').removeClass("row_selected");
                       //Remove class on checkbox uncheck
                       $scope.color = '#edefef';
                   }
               });

               main = document.getElementById('$index');
               if (main.checked == true) {

                   main.checked = false;
                   $scope.color = '#edefef';
                   $scope.row1 = '';
               }
           }

           $scope.add = function () {
               debugger;
               for (var j = 0; j < $scope.acsearch.length; j++) {
                   var v = document.getElementById($scope.acsearch[j].sup_sblgr_acno + j);
                   if (v.checked == true) {
                       var values = $scope.acsearch[j].sup_sblgr_acno.split("-");
                       $scope.temp.sup_sblgr_acno = values[0];
                       //$scope.temp.sup_sblgr_acno = $scope.acsearch[j].sup_sblgr_acno;
                   }
               }
               $('#myModal').modal('hide');
           }

           $scope.Cancel1 = function () {
               $('#myModal').modal('hide');
           }

           //DATA SAVE INSERT
           var datasend = [];

           $scope.savedata = function (Myform) {
               debugger;
               
               if (Myform) {
                   var data = $scope.temp;
                   data.opr = 'I';                   
                   datasend.push(data);
                   $http.post(ENV.apiUrl + "api/Supplier/CUDSupplier?invsObj=", datasend).then(function (msg) {
                       $scope.msg1 = msg.data;
                       //$scope.display = false;
                      
                       if ($scope.msg1 == true) {
                           swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });

                           $http.get(ENV.apiUrl + "api/Supplier/Get_SupplierID").then(function (supl) {
                               $scope.Supplier_ID = supl.data;
                               $scope.temp.sup_code = $scope.Supplier_ID.sup_code;
                              
                           });
                          
                           $scope.getgrid();
                           $scope.display = false;
                           $scope.table = true;
                           
                       }
                       else {
                           //swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                           swal({ text: "Supplier Code Already Exist. " + $scope.msg1, imageUrl: "assets/img/close.png", width: 300, height: 200 });
                           $scope.currentPage = true;
                           $scope.display = true;
                           $scope.table = false;
                       }
                   });                                     
                   datasend = [];                   
               }
           }


           //DATA UPDATE
           var dataforUpdate = [];
           $scope.update = function (Myform) {
               debugger;
               if (Myform) {

                   var data = $scope.temp;
                   data.opr = 'U';

                   dataforUpdate.push(data);
                   $http.post(ENV.apiUrl + "api/Supplier/CUDSupplier?invsObj=", dataforUpdate).then(function (msg) {
                       $scope.msg1 = msg.data;
                       debugger;
                       //$scope.display = false;
                       $scope.getgrid();
                       if ($scope.msg1 == true) {
                           swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                           $scope.getgrid();
                       }
                       else if ($scope.msg1 == false) {
                           swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                           $scope.currentPage = true;
                       }
                       else {
                           swal("Error-" + $scope.msg1)
                       }
                   })
                   $scope.display = false;
                   $scope.table = true;
                   dataforUpdate = [];
               }
           }
           // Data DELETE RECORD
           $scope.CheckAllChecked = function () {
               main = document.getElementById('mainchk');

               if (main.checked == true) {
                   for (var i = 0; i < $scope.filteredTodos.length; i++) {
                       var v = document.getElementById($scope.filteredTodos[i].sup_code + i);
                       v.checked = true;
                       $scope.row1 = 'row_selected';
                       $('tr').addClass("row_selected");
                   }
               }
               else {

                   for (var i = 0; i < $scope.filteredTodos.length; i++) {
                       var v = document.getElementById($scope.filteredTodos[i].sup_code + i);
                       v.checked = false;
                       main.checked = false;
                       $scope.row1 = '';
                       $('tr').removeClass("row_selected");
                   }
               }

           }

           $scope.checkonebyonedelete = function () {

               $("input[type='checkbox']").change(function (e) {
                   if ($(this).is(":checked")) { //If the checkbox is checked
                       $(this).closest('tr').addClass("row_selected");
                       $scope.color = '#edefef';
                   }
                   else {
                       $(this).closest('tr').removeClass("row_selected");

                       $scope.color = '#edefef';
                   }
               });

               main = document.getElementById('mainchk');
               if (main.checked == true) {
                   main.checked = false;
                   $("input[type='checkbox']").change(function (e) {
                       if ($(this).is(":checked")) {
                           $(this).closest('tr').addClass("row_selected");
                       }
                       else {
                           $(this).closest('tr').removeClass("row_selected");
                       }
                   });
               }
           }

           $scope.OkDelete = function () {
               if ($scope.user_access.data_delete == false) {
                   swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
               }
               else {
                   deletefin = [];
                   $scope.flag = false;
                   debugger;
                   for (var i = 0; i < $scope.filteredTodos.length; i++) {
                       var v = document.getElementById($scope.filteredTodos[i].sup_code + i);
                       if (v.checked == true) {
                           $scope.flag = true;
                           var deletemodulecode = ({
                               'sup_code': $scope.filteredTodos[i].sup_code,
                               opr: 'D'
                           });
                           deletefin.push(deletemodulecode);
                       }
                   }
                   if ($scope.flag) {
                       swal({
                           title: '',
                           text: "Are you sure you want to Delete?",
                           showCloseButton: true,
                           showCancelButton: true,
                           confirmButtonText: 'Yes',
                           width: 380,
                           cancelButtonText: 'No',

                       }).then(function (isConfirm) {
                           if (isConfirm) {
                               $http.post(ENV.apiUrl + "api/Supplier/CUDSupplier", deletefin).then(function (msg) {
                                   $scope.msg1 = msg.data;
                                   debugger;
                                   if ($scope.msg1 == true) {
                                       swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                           if (isConfirm) {
                                               $scope.getgrid();

                                               // $scope.currentPage = true;
                                           }
                                       });
                                   }
                                   else if ($scope.msg1 == false) {
                                       swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                           if (isConfirm) {
                                               // $scope.getgrid();
                                               $scope.currentPage = true;
                                           }
                                       });
                                   }
                                   else {
                                       swal("Error-" + $scope.msg1)
                                   }
                               });
                           }
                           else {
                               debugger
                               for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                   var v = document.getElementById($scope.filteredTodos[i].sup_code + i);
                                   if (v.checked == true) {
                                       v.checked = false;
                                       main.checked = false;
                                       $('tr').removeClass("row_selected");
                                   }
                               }
                           }
                       });
                   }
                   else {
                       swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                   }
                   $scope.row1 = '';
                   $scope.currentPage = str;
                   main.checked = false;
               }
           }

           $scope.getgrid = function () {
               debugger
               $http.get(ENV.apiUrl + "api/Supplier/getSupplier").then(function (res1) {
                   $scope.CreDiv = angular.copy(res1.data);
                   $scope.totalItems = $scope.CreDiv.length;
                   $scope.todos = $scope.CreDiv;
                   $scope.makeTodos();
               });

           }

           $scope.onlyNumbers = function (event) {
               debugger;
               var keys = {

                   'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                   '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
               };
               for (var index in keys) {
                   if (!keys.hasOwnProperty(index)) continue;
                   if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                       return; //default event
                   }
               }
               event.preventDefault();
           };

           $scope.onlyNumbers2 = function (event) {
               debugger;
               var keys = {

                   'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 45, 'plus': 43,
                   '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57

               };
               for (var index in keys) {
                   if (!keys.hasOwnProperty(index)) continue;
                   if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                       return; //default event
                   }
               }
               event.preventDefault();
           };

           $scope.onlyNumbers1 = function (event) {
               debugger;
               var keys = {

                   'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13,
                   '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
               };
               for (var index in keys) {
                   if (!keys.hasOwnProperty(index)) continue;
                   if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                       return; //default event
                   }
               }
               event.preventDefault();
           };

       }])

})();