﻿(function () {
    'use strict';
    var opr = '';
    var itemcode = [];
    var main;
    var data1 = [];
    var data = [];
    var itemset;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ServiceSupplierCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            itemset = document.getElementById("chk_itemset");
            $scope.pagesize = '10';
            $scope.table1 = true;
            $scope.operation = false;
            $scope.editmode = false;
            $scope.global_search = true;
            $scope.edt = {};
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });
            $scope.CheckItemSet = function () {
                debugger
                itemset = document.getElementById("chk_itemset");
                if (itemset.checked == true) {
                    $scope.supdis = true;
                    $scope.edt = {};
                    $scope.edt =
                        {
                            sg_name: $scope.SupplierGroupNamewithouitem_data[0].sg_name,
                            im_model_name: 'NA'
                        }

                    $scope.edt['im_assembly_ind'] = true;
                }
                else {
                    $scope.supdis = false;
                    $scope.edt = {};
                    $scope.edt['im_assembly_ind'] = false;

                }
            }


            var data = $scope.temp1;
            //function _onLoadFetch_data() {
            //    $http.get(ENV.apiUrl + "api/common/ShipmentDetails/getServieSuppliersDetails").then(function (getItemMasterDetails_Data) {
            //        $scope.ItemMasterDetail = getItemMasterDetails_Data.data;
            //        $scope.totalItems = $scope.ItemMasterDetail.length;
            //        $scope.todos = $scope.ItemMasterDetail;
            //        $scope.makeTodos();
            //    });
            //}

            $http.get(ENV.apiUrl + "api/common/ShipmentDetails/getServieDesc").then(function (getServieDesc_Data) {
                $scope.getServieDesc_Data = getServieDesc_Data.data;

                $scope.edt['im_inv_no'] = $scope.getServieDesc_Data[0].im_inv_no;
            });

            $http.get(ENV.apiUrl + "api/common/ShipmentDetails/getServieSupplier").then(function (getSupplier_Data) {
                $scope.Supplier_Data = getSupplier_Data.data;
                $scope.edt['sup_code'] = $scope.Supplier_Data[0].sup_code;
            });

            $http.get(ENV.apiUrl + "api/common/ShipmentDetails/getServieUnitOfMeasurement").then(function (getUnitOfMeasurement_Data) {
                $scope.UnitOfMeasurement = getUnitOfMeasurement_Data.data;
            });

            $http.get(ENV.apiUrl + "api/common/ShipmentDetails/getServieTradeCategory").then(function (getTradeCategory_Data) {
                $scope.TradeCategory = getTradeCategory_Data.data;
            });

            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage);
            //    $scope.makeTodos();
            //}

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.ItemMasterDetail;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['check_all'] = false;
                $scope.row1 = '';
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.global_search = false;
                    $scope.check = true;
                    $scope.editmode = false;
                    $scope.newmode = true;
                    $scope.readonly = false;
                    $scope.table1 = false;
                    $scope.operation = true;
                    $scope.savebtn = true;
                    $scope.updatebtn = false;

                    $scope.edt = {}

                    $scope.edt = {
                        im_creation_date: dd + '-' + mm + '-' + yyyy,
                    };

                    $scope.edt['im_inv_no'] = $scope.getServieDesc_Data[0].im_inv_no;
                    $scope.edt['sup_code'] = $scope.Supplier_Data[0].sup_code;

                    $scope.myForm.$setPristine();
                    $scope.myForm.$setUntouched();
                    $scope.supdis = false;
                }
            }
            $scope.up = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    console.log(str);
                    $scope.savebtn = false;
                    $scope.updatebtn = true;
                    $scope.table1 = false;
                    $scope.operation = true;
                    $scope.editmode = true;
                    $scope.newmode = false;
                    $scope.readonly = true;
                    $scope.global_search = false;
                    $scope.supdis = true;

                    $scope.edt = {

                        im_inv_no: str.im_inv_no,
                        sup_code: str.sup_code,
                        im_supl_catalog_price: str.im_supl_catalog_price,
                        im_creation_date: str.im_creation_date,
                        im_desc: str.im_desc,
                        sup_name: str.sup_name,
                    }
                    console.log($scope.edt);
                }
            }
            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.global_search = true;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

                $scope.edt = {
                    im_inv_no: "",
                    sup_code: "",
                    im_supl_catalog_price: "",
                    im_creation_date: "",
                    im_desc: "",
                    sup_name: "",
                };
            }

            $scope.numOnly = function (e) {
                var k = e.which;
                var r = (k == 8 || k == 110 || k == 37 || k == 39 || k == 9 || k == 32 || (k >= 48 && k <= 57) || (k >= 96 && k <= 105));
                if (r == false) {
                    e.preventDefault();
                    return;
                }
            }

            //debugger;
            $scope.Save = function (myForm) {
                debugger;
                if (myForm) {
                    data1 = [];
                    var data = $scope.edt;
                    data.opr = 'I';
                    data1.push(data);
                    console.log(data1);
                    $http.post(ENV.apiUrl + "api/common/ShipmentDetails/ServieSuppliersCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == 'I') {
                            swal({ text: 'Record Inserted Successfully', imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == 'U') {
                            swal({ text: 'Record Inserted Successfully.', imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        //_onLoadFetch_data();
                        $scope.onLoadFetch_data();
                    });
                    $scope.table1 = true;
                    $scope.global_search = true;
                    $scope.operation = false;
                }
            }

            // debugger;
            $scope.update = function (myForm) {
                debugger;
                if (myForm) {

                    data1 = [];
                    var data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/common/ShipmentDetails/ServieSuppliersCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == 'I') {
                            swal({ text: 'Record Inserted Successfully', imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == 'U') {
                            swal({ text: 'Record Updated Successfully.', imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        //_onLoadFetch_data();
                        $scope.onLoadFetch_data();
                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                    $scope.global_search = true;
                }

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.onLoadFetch_data = function () {
                $http.get(ENV.apiUrl + "api/common/ShipmentDetails/getServieSuppliersDetails").then(function (getItemMasterDetails_Data) {
                    $scope.ItemMasterDetail = getItemMasterDetails_Data.data;
                    $scope.totalItems = $scope.ItemMasterDetail.length;
                    $scope.todos = $scope.ItemMasterDetail;
                    $scope.makeTodos();
                });
            }
            // _onLoadFetch_data();
            $scope.onLoadFetch_data();


            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].im_inv_no + i);
                        if (v != null) {
                            v.checked = true;
                            $scope.row1 = 'row_selected';
                        }
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].im_inv_no + i);
                        if (v != null) {
                            v.checked = false;
                            main.checked = false;
                            $scope.row1 = '';
                        }
                    }
                }
            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }


            $scope.deleterecord = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    //  debugger
                    itemcode = [];
                    $scope.flag = false;
                    var dele;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].im_inv_no + i);
                        if (v != null) {
                            if (v.checked == true) {
                                $scope.flag = true;
                                //itemcode.push($scope.filteredTodos[i]);
                                //itemcode.cur_code = 'D';
                                var ds = {
                                    'im_inv_no': $scope.filteredTodos[i].im_inv_no,
                                    'cur_code': 'D',
                                    'sup_code': $scope.filteredTodos[i].sup_code,
                                }
                                //itemcode.push($scope.filteredTodos[i]);
                                itemcode.push(ds);
                            }
                        }
                    }

                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                // debugger
                                //$http.post(ENV.apiUrl + "api/common/ShipmentDetails/DeleteServieSuppliers", itemcode).then(function (msg) {
                                $http.post(ENV.apiUrl + "api/common/ShipmentDetails/ServieSuppliersCUD", itemcode).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    //if ($scope.msg1 == true) {
                                    //swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    swal({ text: 'Records Deleted Successfully', imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    });
                                    //_onLoadFetch_data();
                                    $scope.onLoadFetch_data();
                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById($scope.filteredTodos[i].im_inv_no + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        $scope.row1 = '';
                                    }
                                }
                                $scope.onLoadFetch_data();
                            }
                        });
                        $scope.searchText = '';
                        //_onLoadFetch_data();

                    }
                    else {
                        swal({ text: "Please Select Atleast One Record. " + $scope.msg1, imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    //$scope.Show_Data();
                    //$scope.currentPage = true;
                    $scope.global_search = true;
                    $scope.table1 = true;
                    main.checked = false;

                }
            }

            $scope.numOnly = function (e) {
                var k = e.which;
                var r = (k == 8 || k == 110 || k == 37 || k == 39 || k == 9 || k == 32 || (k >= 48 && k <= 57) || (k >= 96 && k <= 105));
                if (r == false) {
                    e.preventDefault();
                    return;
                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.ItemMasterDetail, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.ItemMasterDetail;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                if (item != undefined) {
                    return (
                         item.sg_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.im_item_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.im_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.dep_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                         item.sec_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.pc_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.sup_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.uom_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.im_min_level.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                         item.im_inv_no == toSearch) ? true : false;
                }
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.edt = {
                im_creation_date: dd + '-' + mm + '-' + yyyy
            }


        }])
})();





