﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin, cstatus = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SupplierLocationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.display = false;
            $scope.table1 = true;
            $scope.fee_no = true;
            $scope.table = true;
            $scope.SupplierName = [];
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
               
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                 $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

              
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.getallcomboBox = function () {

                $http.get(ENV.apiUrl + "api/StockAdjustment/getAllSupplierNameCode").then(function (AllSna) {
                    $scope.SupplierName = AllSna.data;
                    $scope.isa1 = { sup_code: $scope.SupplierName[0].sup_code };
                });
            }

            $scope.getallcomboBox();

            $scope.showdata = function () {
                //$http.get(ENV.apiUrl + "api/StockAdjustment/getSupplierItemLocation?sup_code=" + sup_code).then(function (res1) {
                $http.get(ENV.apiUrl + "api/StockAdjustment/getSupplierItemLocation").then(function (res1) {
                    $scope.obj = res1.data;
                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.makeTodos();
                    if ($scope.obj.length == 0) {
                        swal({ title: 'Alert', text: "No Record Found...", showCloseButton: true, width: 450, height: 200 });
                        $scope.display = false;
                        $scope.table = false;
                        $scope.table1 = true;
                    }
                    else {
                        $scope.display = false;
                        $scope.table = true;
                        $scope.table1 = true;
                    }
                })
            }

            $scope.showdata();

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {

                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sup_location_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sup_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sup_location_contact_person == toSearch) ? true : false;

            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.sup_disabled = false;
                    $scope.clearvalue();
                    $scope.display = true;
                    $scope.table = false;
                    $scope.table1 = false;
                    $scope.update1 = false;
                    $scope.save1 = true;
                    $scope.supcode = true;
                    $scope.edt = {
                        sup_location_status: true,
                    }
                }
            }

            var Savedata = [];
            $scope.submit = function (isvalid) {
               
                Savedata = [];
                if (isvalid) {
                    var ins = ({
                        'sup_code': $scope.isa1.sup_code,
                        'sup_location_code': $scope.edt.sup_location_code,
                        'sup_location_desc': $scope.edt.sup_location_desc,
                        'sup_location_address': $scope.edt.sup_location_address,
                        'sup_location_landmark': $scope.edt.sup_location_landmark,
                        'sup_location_contact_no': $scope.edt.sup_location_contact_no,
                        'sup_location_contact_person': $scope.edt.sup_location_contact_person,
                        'sup_location_status': $scope.edt.sup_location_status,
                        opr: 'I',
                    });
                    Savedata.push(ins);

                    $http.post(ENV.apiUrl + "api/StockAdjustment/CUDSupplierLocation", Savedata).then(function (msg) {
                        
                        $scope.msg1 = msg.data;
                        if ($scope.msg1.strMessage != undefined) {
                            if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                                $scope.showdata();
                                $scope.currentPage = true;
                                $scope.getallcomboBox();
                                $scope.clearvalue();
                            }
                        }

                    });
                    Savedata = [];
                }
            }

            $scope.cancel = function () {
                //$scope.showdata();
                $scope.display = false;
                $scope.table = true;
                $scope.table1 = true;
                //$scope.clearvalue();
            }

            $scope.CheckAllChecked = function () {
                
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }
            }

            $scope.checkonebyoneselect = function () {

                $("input[type='checkbox']").change(function (e) {

                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.edit = function (j) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.sup_disabled = true;
                    $scope.update1 = true;
                    $scope.save1 = false;
                    $scope.edt = j;
                    $scope.onPriChange = {
                        sup_name: j.sup_name,
                        sup_code: j.sup_code,
                    }

                    $scope.supcode = false;

                    for (var i = 0; i < $scope.SupplierName.length; i++) {
                        if ($scope.SupplierName[i].sup_code == $scope.onPriChange.sup_code) {
                            $scope.isa1.sup_code = $scope.SupplierName[i].sup_code;
                            break;
                        }
                    }


                    $scope.display = true;
                    $scope.table = false;
                    $scope.table1 = false;
                }
            }
            var updateData = [];
            $scope.Update = function () {
                updateData = [];
                var ins = ({
                    'sup_code': $scope.isa1.sup_code,
                    'sup_location_code': $scope.edt.sup_location_code,
                    'sup_location_desc': $scope.edt.sup_location_desc,
                    'sup_location_address': $scope.edt.sup_location_address,
                    'sup_location_landmark': $scope.edt.sup_location_landmark,
                    'sup_location_contact_no': $scope.edt.sup_location_contact_no,
                    'sup_location_contact_person': $scope.edt.sup_location_contact_person,
                    'sup_location_status': $scope.edt.sup_location_status,
                    opr: 'U',
                });
                updateData.push(ins);

                $http.post(ENV.apiUrl + "api/StockAdjustment/CUDSupplierLocation", updateData).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1.strMessage != undefined) {
                        if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                            swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                            $scope.showdata();
                            $scope.currentPage = true;
                            $scope.getallcomboBox();
                            $scope.clearvalue();
                        }
                    }

                });
                updateData = [];
            }

            $scope.OkDelete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    var data1 = [];
                    $scope.flag = false;
                    var deleteleave = [];
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodulecode = ({
                                'sup_code': $scope.filteredTodos[i].sup_code,
                                'sup_location_code': $scope.filteredTodos[i].sup_location_code,
                                opr: 'D'
                            });
                            deleteleave.push(deletemodulecode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '', text: "Are you sure you want to Delete?", showCloseButton: true, showCancelButton: true, confirmButtonText: 'Yes', width: 380, cancelButtonText: 'No',
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $http.post(ENV.apiUrl + "api/StockAdjustment/CUDSupplierLocation", deleteleave).then(function (msg) {
                                    {

                                        $scope.msg1 = msg.data;
                                        if ($scope.msg1.strMessage != undefined) {

                                            if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                                swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 }).then(function (isConfirm) {
                                                    if (isConfirm) {
                                                        $scope.showdata();
                                                        main = document.getElementById('mainchk');
                                                        if (main.checked == true) {
                                                            main.checked = false;
                                                            $scope.row1 = '';
                                                        }
                                                    }
                                                    $scope.currentPage = true;
                                                });
                                            }
                                        }
                                    }
                                });
                            }
                            else {
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById("test-" + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }

                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }

                    $scope.currentPage = str;
                    $scope.row1 = '';
                }
            }

            $scope.clearvalue = function () {

                $scope.edt = {
                    sup_location_code: '',
                    sup_location_desc: '',
                    sup_location_address: '',
                    sup_location_landmark: '',
                    sup_location_contact_no: '',
                    sup_location_contact_person: '',
                    //sup_location_status: false,
                }
                $scope.MyForm.$setPristine();
                $scope.MyForm.$setUntouched();
                //cstatus = document.getElementById('chk_status1');
                //cstatus.checked = false;
                $scope.searchText = '';
            }

        }])
})();
