﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('InventoryLoanDetailView_RptCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            // Start
            //$scope.name = "Subject Teacher List";
            $scope.temp = {};


           




            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.temp = {};
            $scope.display = false;
            $scope.dublicate = false;
            var user = $rootScope.globals.currentUser.username;


           

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.from_date = dd + '-' + mm + '-' + yyyy;
            $scope.to_date = dd + '-' + mm + '-' + yyyy;

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.from_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.from_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });


            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.to_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.to_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });

            $(function () {
                $('#status_box').multipleSelect({ width: '100%' });
               

            });

          
    
           


            $scope.getInventoryLoanDetailView = function () {
              debugger
                $scope.colsvis = false;
                if ($scope.temp.from_date == undefined) $scope.temp.from_date = '';
                $http.get(ENV.apiUrl + "api/RoleBasedAccessReport/getInventoryLoanDetailViewReport?doc_status=" + $scope.temp.doc_status + "&from_date=" + $scope.from_date + "&to_date=" + $scope.to_date).then(function (res1) {
                    if (res1.data.length > 0) {
                        $scope.report_data = res1.data;
                        angular.forEach($scope.report_data, function (f) {
                            f.student_cnt = parseFloat(f.student_cnt);
                        });
                    }
                    else {
                        swal({ title: "Alert", text: " Data is not Available for selected date", showCloseButton: true, width: 300, height: 200 });
                    }
                    //angular.forEach($scope.report_data, function (f) {
                    //    f.sims_bell_lecture_per_week = parseFloat(f.sims_bell_lecture_per_week);
                    //});
                    console.log($scope.report_data);
                });
            }




            var data_send = [];
            var data1 = [];




            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }





            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            //$scope.colsvis = false;
            $http.get(ENV.apiUrl + "api/RoleBasedAccessReport/get_Status_all").then(function (res1) {
                $scope.status_code = res1.data;
                setTimeout(function () {
                    $('#status_box').change(function () {

                    }).multipleSelect({
                        width: '100%'
                    });
                    $("#status_box").multipleSelect("checkAll");
                }, 1000);
            });

            $scope.exportData = function () {
                debugger              

                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('rpt_data').innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        saveAs(blob, "InventoryLoanDetailViewReport.xls");
                        //$scope.colsvis = false;

                    }

                });
            };

            $scope.print = function (div) {

                //$scope.colsvis = false;

                var docHead = document.head.outerHTML;
                var printContents = document.getElementById('rpt_data').outerHTML;
                //$scope.colsvis = false;
                var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";


                var newWin = window.open("", "_blank", winAttr);
                var writeDoc = newWin.document;
                writeDoc.open();
                writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                writeDoc.close();
                newWin.focus();

            };

            $scope.Report_call = function (str) {
                debugger
                $http.get(ENV.apiUrl + "api/RoleBasedAccessReport/getReportFromProc1?doc_prov_no=" + str.doc_prov_no).then(function (res1) {
                    $scope.result = res1.data;
                    console.log($scope.result);

                    var data = {
                        location: $scope.result[0].sims_appl_form_field,
                        parameter: {
                            doc_prov_no: str.doc_prov_no

                        },
                        state: 'main.ILDV01',
                        ready: function () {
                            this.refreshReport();
                        },
                    }
                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                    $state.go('main.ReportCardParameter')





                });

               
               
            }

          

        }])

})();