﻿(function () {
    'use strict';
    var obj1, temp, opr, CurrentDate, comp;
    var main, deletefin = [], data = [], data1 = [], ItemDetails = [];
    var finanacecode = [];
    var check;
    var reqno;
    var simsController = angular.module('sims.module.Inventory');
    simsController.controller('StockAdjustmentApprovalCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {
            $scope.table = true;


            $scope.hide_save = false;
            $scope.hide_saves = false;
            $scope.hide_table = false;

            $scope.IP = {};
            $scope.sel = {};
            $scope.getdep_no = function () {
                $http.get(ENV.apiUrl + "api/RequestDetail/getDeparments").then(function (deptdata) {
                    $scope.dept_data = deptdata.data;
                    $scope.IP = { dept_code: $scope.dept_data[0].dept_code }
                    $scope.getdoc_no();
                });
            }
            $scope.getdep_no();
            $scope.getdoc_no = function () {
                $http.get(ENV.apiUrl + "api/StockAdjustment/getDocno?dep_no=" + $scope.IP.dept_code).then(function (docdata) {
                    $scope.docno_Data = docdata.data;
                });
            }

            setTimeout(function () {
                $("#cmb_seritm_code").select2();
            }, 100);

            $scope.SelectIetm = function () {
                debugger;
                if ($scope.IP.dept_code == undefined) {
                    $scope.hide_save = true;
                    $scope.hide_saves = false;
                    $scope.hide_table = false;
                    swal({ title: "Alert", text: "Please select department", showCloseButton: true, width: 380, });
                }
                else {
                    $scope.IP.username = $rootScope.globals.currentUser.username;
                    $http.get(ENV.apiUrl + "api/StockAdjustment/getStockDetailsforapprove?data=" + JSON.stringify($scope.IP)).then(function (res) {
                        $scope.rows = res.data.table;
                        $scope.Allrows = res.data.table1;
                        if ($scope.Allrows.length <= 0) {
                            swal({ text: "Records not found", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                            $scope.hide_save = true;
                            $scope.hide_saves = false;
                            $scope.hide_table = false;
                        }
                        else {
                            for (var r in $scope.rows) {
                                $scope.rows[r]['sims_icon'] = "fa fa-plus-circle";
                                $scope.rows[r]['isexpanded'] = "none";
                                $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].req_no);
                                $scope.hide_save = false;
                                $scope.hide_saves = true;
                                $scope.hide_table = true;
                            }
                        }
                        console.clear();
                    });
                }
            }

            function getSubitems(dno) {
                var arr = [];
                for (var i = 0; i < $scope.Allrows.length; i++) {
                    if ($scope.Allrows[i].req_no == dno) {
                        arr.push($scope.Allrows[i]);
                    }
                }
                return arr;
            }

            $scope.expand = function (j, $event) {
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";
                    console.log(j);
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none";
                }
            }

            $scope.checkRange = function (s) {
                var rq = parseInt(s.final_qnt);
                if (rq == NaN)
                    rq = 0;
                s.final_qnt = rq;
            }

            $scope.ordclick = function (gh) {
                var l = gh.subItems.length;
                for (var i = 0; i < l; i++) {
                    gh.subItems[i]['isChecked'] = gh.isChecked;
                }
            }

            $scope.ordlineclick = function (r, m) {
                var l = r.subItems.length;
                var f = false;
                if (m) {
                    f = true;
                    for (var i = 0; i < l; i++) {
                        f = f && r.subItems[i]['isChecked'];
                    }
                }
                r.isChecked = f;
            }

            $scope.save = function (s) {

                $scope.hide_save = true;
                $scope.hide_saves = true;

                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {

                    for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                        if ($scope.rows[i].subItems[j].isChecked) {
                            var ob = {};
                            ob.id_cur_qty = $scope.rows[i].subItems[j].id_cur_qty;
                            ob.final_qnt = $scope.rows[i].subItems[j].ad_qty_new;// -$scope.rows[i].subItems[j].id_cur_qty;
                            ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.ad_line_no = $scope.rows[i].subItems[j].ad_line_no;
                            ob.adj_doc_no = $scope.rows[i].subItems[j].adj_doc_no;
                            ob.remarks = $scope.rows[i].subItems[j].ad_remark;
                            ob.ad_value = $scope.rows[i].subItems[j].ad_value;
                            ob.dep_code = $scope.rows[i].subItems[j].dep_code;
                            ob.adj_date = $scope.rows[i].adj_date;
                            ob.val = r;
                            ar.push(ob);
                        }
                    }
                }
                if (ar.length == 0) {
                    swal({ text: "Please Select Atleast One Request", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    $scope.hide_save = false;
                    $scope.hide_saves = true;

                }
                else {
                    $http.post(ENV.apiUrl + "api/StockAdjustment/UpdateStockAdjustment", ar).then(function (res) {
                        debugger;
                        var arr = [];
                        var ob = {};
                        for (var i = 0; i < $scope.rows.length; i++) {
                            for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                                if ($scope.rows[i].subItems[j].isChecked) {
                                    ob.adj_doc_no = $scope.rows[i].adj_doc_no;
                                    arr.push(ob.adj_doc_no);
                                }
                            }
                        }

                        swal({ text: 'Stock Doc No:- ' + arr + "" + ' Approved', imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });

                        //if ($scope.IP.req_no == undefined && $scope.IP.req_range_no == undefined && arr.length == 0) {
                        //    swal({ title: "Alert", text: 'Please Enter Request No From and Request No To OR Select Atleast One Request', showCloseButton: true, width: 380, });
                        //}

                        //else {
                        //    if ($scope.IP.req_no == undefined) {
                        //        $scope.IP.req_no = 0
                        //    }
                        //    if ($scope.IP.req_range_no == undefined) {
                        //        $scope.IP.req_range_no = 0
                        //    }

                        //    debugger;
                        //    $scope.t = 'Invs.INVR16';
                        //    if (arr.length == 0) {
                        //        var data = {
                        //            location: $scope.t,
                        //            parameter: {
                        //                req_from: $scope.IP.req_no,
                        //                req_to: $scope.IP.req_range_no,
                        //                req_list: 0,
                        //            },
                        //            state: 'main.Inv175'
                        //        }
                        //    }
                        //    else {
                        //        var data = {
                        //            location: $scope.t,
                        //            parameter: {
                        //                req_from: $scope.IP.req_no,
                        //                req_to: $scope.IP.req_range_no,
                        //                req_list: arr + "",
                        //            },
                        //            state: 'main.Inv175'
                        //        }
                        //    }
                        //    window.localStorage["ReportDetails"] = JSON.stringify(data)
                        //    $state.go('main.ReportCardParameter')
                        //}
                        $scope.Reset();
                    });
                }
            }

            $scope.Reset = function () {
                $scope.IP = {};
                $scope.rows = {};
                $scope.hide_save = true;
                $scope.hide_saves = false;
                $scope.hide_table = false;
                $scope.getdep_no();
                $("#cmb_seritm_code").select2("val", "");
                $scope.Allrows = {};
            }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            })

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

        }])

})();
