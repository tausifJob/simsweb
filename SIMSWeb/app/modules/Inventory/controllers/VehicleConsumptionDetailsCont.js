﻿(function () {
    'use strict';
    var opr = '';
    var data1 = [];
    var main, edt;
    var simsController = angular.module('sims.module.Inventory');
    simsController.controller('VehicleConsumptionDetailsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.edt = "";
            $scope.edt1 = "";
            $scope.gridtable = false;
            $scope.editmode = false;
            var user = $rootScope.globals.currentUser.username;
            var comp_code = '1';
            var finance_year = '2016';


            $(function () {
                $('#cmb_supplier_name').multipleSelect({
                    width: '100%'
                });
            });

           
            //Vehicle Details
            $http.get(ENV.apiUrl + "api/VehicleConsumptionDetail/getSupplierDetail").then(function (getSupplier_data) {
                $scope.Supplier_data = getSupplier_data.data;

                setTimeout(function () {
                    $('#cmb_supplier_name').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });

            $(function () {
                $('#cmb_location_name').multipleSelect({
                    width: '100%'
                });
            });


            $http.get(ENV.apiUrl + "api/VehicleConsumptionDetail/getSupplierLocation").then(function (getLocation_data) {
                $scope.Location_data = getLocation_data.data;
                console.log($scope.Location_data)
                setTimeout(function () {

                    $('#cmb_location_name').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });

            $(function () {
                $('#cmb_vehicle_name').multipleSelect({
                    width: '100%'
                });
            });
            $http.get(ENV.apiUrl + "api/VehicleConsumptionDetail/getVehicleDetails?driver_code=" + user).then(function (getVehicle_data) {
                $scope.Vehicle_data = getVehicle_data.data;
                setTimeout(function () {
                    $('#cmb_vehicle_name')
                    .multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });


            $(function () {
                $('#cmb_driver_name').multipleSelect({
                    width: '100%'
                });
            });

            $scope.selectVehicle = function (vehicleCode) {
                console.log(vehicleCode);
                $scope.Driver_data = [];
                if (vehicleCode.length > 0) {
                    $http.get(ENV.apiUrl + "api/VehicleConsumptionDetail/getDriverDetail?vehicle_code=" + vehicleCode).then(function (getDriver_data) {
                        $scope.Driver_data = getDriver_data.data;
                        if ($scope.Driver_data.length > 0)
                        { }
                        else
                        {
                            swal({ text: "Sorry driver list not loaded please check transport route mapping...", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                        }
                        setTimeout(function () {
                            $('#cmb_driver_name').change(function () {
                            }).multipleSelect({
                                width: '100%'
                            });
                        }, 1000);
                    });
                } else {
                    
                }
            }

            if (user == "Admin" || user == "admin") {
                debugger;
                $scope.cmbdriver = false;
                $http.get(ENV.apiUrl + "api/VehicleConsumptionDetail/getAllDrivers?loginuser=" + user).then(function (res) {
                    $scope.comboDriver = res.data;
                });

            } else {
                $http.get(ENV.apiUrl + "api/VehicleConsumptionDetail/getAllDrivers?loginuser=" + user).then(function (res) {
                    $scope.comboDriver = res.data;
                });
                $scope.cmbdriver = true;

            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.createdate = function (end_date, start_date, name) {
                //var month1 = end_date.split("/")[0];
                //var day1 = end_date.split("/")[1];
                //var year1 = end_date.split("/")[2];
                //var new_end_date = year1 + "/" + month1 + "/" + day1;
                var new_end_date = end_date;

                //var year = start_date.split("/")[0];
                //var month = start_date.split("/")[1];
                //var day = start_date.split("/")[2];
                //var new_start_date = year + "/" + month + "/" + day;
                var new_start_date = start_date;

                if (new_end_date < new_start_date) {
                    swal({ title: 'Please Select Next Date From ', width: 380, height: 100 });
                    $scope.edt[name] = '';
                }
                else {
                    $scope.edt[name] = new_end_date;
                }
            }

            $scope.paginationview1 = document.getElementById("paginationview");

            $scope.showdate = function (date, name) {
                debugger
                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //$scope.edt[name] = year + "/" + month + "/" + day;
                $scope.edt[name] = date;
            }

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            if (dd < 10) {
                dd = '0' + dd;
            }
            var yyyy = today.getFullYear();
            //$scope.sdate = yyyy + '-' + mm + '-' + dd;
            //$scope.edate = yyyy + '-' + mm + '-' + dd;
            //$scope.edt = {
            //    from_date: yyyy + '-' + mm + '-' + dd,
            //    to_date: yyyy + '-' + mm + '-' + dd,
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.edt = {
                from_date: dd + '-' + mm + '-' + yyyy,
                to_date: dd + '-' + mm + '-' + yyyy,

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //$scope.size = function (str) {
            //    debugger
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    if (str == 'All')
            //    $scope.numPerPage = $scope.totalItems;
            //    else
            //    $scope.numPerPage = str;


            //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();

            //}

            //$scope.index = function (str) {
            //    $scope.pageindex = str;
            //    $scope.currentPage = str;
            //    console.log("currentPage=" + $scope.currentPage);
            //    $scope.makeTodos();
            //    main.checked = false;
            //    $scope.check_all();
            //}

            $scope.cancel = function () {
                $scope.gridtable = true;
                $scope.operation = false;
                //$scope.bin_desc = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                opr = 'S';
                $scope.editmode = false;
                $scope.edt = "";
                $scope.edt1 = "";
                $scope.newmode = true;
                $scope.readonly = false;
                $scope.gridtable = false;
                $scope.VehicleConsumtionData = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            //$scope.makeTodos = function () {
            //    var rem = parseInt($scope.totalItems % $scope.numPerPage);
            //    if (rem == '0') {
            //        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
            //    }
            //    else {
            //        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
            //    }
            //    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
            //    var end = parseInt(begin) + parseInt($scope.numPerPage);
            //    console.log("begin=" + begin); console.log("end=" + end);
            //    $scope.filteredTodos = $scope.todos.slice(begin, end);
            //};

            //$scope.searched = function (valLists, toSearch) {
            //    return _.filter(valLists, function (i) {
            //        /* Search Text in all  fields */
            //        return searchUtil(i, toSearch);
            //    });
            //};

            //$scope.search = function () {
            //    $scope.todos = $scope.searched($scope.VehicleDataconsumption, $scope.searchText);
            //    $scope.totalItems = $scope.todos.length;
            //    $scope.currentPage = '1';
            //    if ($scope.searchText == '') {
            //        $scope.todos = $scope.VehicleDataconsumption;
            //    }
            //    $scope.makeTodos();
            //    main.checked = false;
            //    $scope.check_all();
            //}

            $scope.onlyNumbers = function (event) {
                ;
                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                    item.sup_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sup_location_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.vehicle_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.im_inv_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.driver_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.recorded_miles_reading.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.recorded_miles_reading == toSearch) ? true : false;

            }

            $scope.up = function (str) {
                debugger
                opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.gridtable = false;
                $scope.operation = true;
                $scope.readonly = true;


                $scope.edt = {
                    vehicle_consumption_number: str.vehicle_consumption_number
                  , sup_code: str.sup_code
                  , sup_name: str.sup_name
                  , sup_location_code: str.sup_location_code
                  , sup_location_address: str.sup_location_address
                  , vehicle_code: str.vehicle_code
                  , im_inv_no: str.im_inv_no
                  , sale_time: str.sale_time
                  , unit_price: str.unit_price
                  , sale_qty: str.sale_qty
                  , sale_period: str.sale_period
                  , driver_code: str.driver_code
                  , recorded_miles_reading: str.recorded_miles_reading
                };
                $scope.getLocationDetails(str.sup_code)
                if (user == "admin" || user == "Admin") {

                    $scope.vehicle_code = false;
                    $scope.im_inv_no = false;
                    $scope.sale_time = false;
                    $scope.unit_price = false;
                    $scope.sale_qty = false;
                    $scope.sale_period = false;
                    $scope.driver_code = false;
                }
                else {

                    $scope.vehicle_code = true;
                    $scope.im_inv_no = true;
                    $scope.sale_time = true;
                    $scope.unit_price = true;
                    $scope.sale_qty = true;
                    $scope.sale_period = true;
                    $scope.driver_code = true;
                }
                $scope.Update();
            }

            $scope.selectDriver = function (drivercode) {
                console.log(drivercode);
                $scope.drvcode = drivercode
            }

            $scope.Update = function () {
                debugger
                //if (myForm) {
                var updatedata = [];
                for (var i = 0; i < $scope.VehicleDataconsumption.length; i++) {

                    var data = ({
                        'sup_code': $scope.VehicleDataconsumption[i].sup_code
             , 'sup_name': $scope.VehicleDataconsumption[i].sup_name
             , 'vehicle_consumption_number': $scope.VehicleDataconsumption[i].vehicle_consumption_number
             , 'sup_location_code': $scope.VehicleDataconsumption[i].sup_location_code
             , 'sup_location_address': $scope.VehicleDataconsumption[i].sup_location_address
             , 'vehicle_code': $scope.VehicleDataconsumption[i].vehicle_code
             , 'im_inv_no': $scope.VehicleDataconsumption[i].im_inv_no
             , 'sale_time': $scope.VehicleDataconsumption[i].sale_time
             , 'unit_price': $scope.VehicleDataconsumption[i].unit_price
             , 'sale_qty': $scope.VehicleDataconsumption[i].sale_qty
             , 'sale_period': $scope.VehicleDataconsumption[i].sale_period
             , 'driver_code': $scope.drvcode
             , 'recorded_miles_reading': $scope.VehicleDataconsumption[i].recorded_miles_reading
              , opr: 'U'
                    });
                    updatedata.push(data);
                }


                //$http.post(ENV.apiUrl + "api/VehicleConsumptionDetail/VehicleConsumtionCUD", updatedata).then(function (msg) {

                //    $scope.msg1 = msg.data;
                //    console.log($scope.msg1);
                //    if ($scope.msg1 == true) {
                //        swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                //    }
                //    else {
                //        swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                //    }
                //    //$http.get(ENV.apiUrl + "api/VehicleConsumptionDetail/getVehicleConsumption?login_name=" + user + "&from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date + "&location=" + $scope.edt.sup_location_code + "&driver=" + $scope.edt.driver_code + "&vehicle=" + $scope.edt.vehicle_code).then(function (getVehicleConsumption_data) {
                //    //    $scope.VehicleDataconsumption = getVehicleConsumption_data.data;
                //    //    $scope.totalItems = $scope.VehicleDataconsumption.length;
                //    //    $scope.todos = $scope.VehicleDataconsumption;
                //    //    $scope.makeTodos();
                //    //});

                //})
                $scope.operation = false;
                $scope.gridtable = true;
                data1 = [];

                //}
            }


            $scope.Show = function () {
                debugger;

                if ($scope.edt.from_date == undefined || $scope.edt.from_date == null)
                    $scope.edt.from_date = '';
                if ($scope.edt.to_date == undefined || $scope.edt.to_date == null)
                    $scope.edt.to_date = '';
                if ($scope.edt.sup_location_code == undefined || $scope.edt.sup_location_code == null)
                    $scope.edt.sup_location_code = '';
                if ($scope.edt.driver_code == undefined || $scope.edt.driver_code == null)
                    $scope.edt.driver_code = '';
                if ($scope.edt.vehicle_code == undefined || $scope.edt.vehicle_code == null)
                    $scope.edt.vehicle_code = '';
                $scope.gridtable = true;
                $scope.ImageView = false;
                $http.get(ENV.apiUrl + "api/VehicleConsumptionDetail/getVehicleConsumption?login_name=" + user + "&from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date + "&location=" + $scope.edt.sup_location_code + "&driver=" + $scope.edt.driver_code + "&vehicle=" + $scope.edt.vehicle_code).then(function (getVehicleConsumption_data) {
                    $scope.VehicleDataconsumption = getVehicleConsumption_data.data;
                    //for (var i = 0;i< $scope.VehicleDataconsumption.length; i++) {
                    //    var id = '#test-' + i;
                    //    $(id).select2();
                    //}

                    for (var i = 0; $scope.VehicleDataconsumption.length > i; i++) {
                            

                      
                        if ($scope.VehicleDataconsumption[i].driver_code == "") {
                            //$scope.cmbdriver = false;
                            $scope.VehicleDataconsumption[i].sims_driver_name = $scope.comboDriver[0].sims_driver_name;
                            $scope.drvcode = $scope.comboDriver[0].driver_code;
                        }
                        if ($scope.VehicleDataconsumption[i].recorded_miles_reading > 0) {
                            $scope.VehicleDataconsumption[i].isDisabled = true;
                        } else {
                            $scope.VehicleDataconsumption[i].isDisabled = false;
                        }

                    }
                    if (getVehicleConsumption_data.data.length > 0) { }
                    else {
                        $scope.ImageView = true;
                        swal({ text: "Sorry There is no Record...", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }
                });
            }

            //$scope.miletxtClick = function (reading) {
            //    console.log(reading);
            //    if (reading != "" || reading != undefined) {
            //        $scope.milesreading = true;
            //    } else {
            //        $scope.milesreading = false;
            //    }

            //}


        }]);

})();



