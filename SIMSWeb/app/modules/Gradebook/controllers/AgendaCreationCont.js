﻿(function () {
    'use strict';
    var main = '';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

    simsController.controller('AgendaCreationCont',
       // ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
       ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.display = false;
            $scope.busyindicator = false;
            $scope.grid = true;
            $scope.color = '#edefef';
            $scope.rgvtbl = false;
            $scope.uploading_doc = true;
            $scope.uploading_doc2 = true;
            $scope.uploading_doc1 = true;
            var uname = $rootScope.globals.currentUser.username;
            $scope.fileformats = '';
            $scope.fileformats_chk = '';

            var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');
           
            
            $('#text-editor').wysihtml5();

            $http.get(ENV.apiUrl + "api/AgendaCreationController/GetAgendaDoc").then(function (GetAgendaDoc) {
                $scope.file_upload_details = GetAgendaDoc.data;
               
              

            });

            $http.get(ENV.apiUrl + "api/AgendaCreationController/GetTextFormat").then(function (GetTextFormat) {
                $scope.GetTextFormat = GetTextFormat.data;
               
                for (var i = 0; i < $scope.GetTextFormat.length; i++) {
                    $scope.fileformats = $scope.fileformats + '.' + $scope.GetTextFormat[i].file_format + ',';
                }
               

            });



            function getCur(flag, comp_code) {
                debugger
                $scope.MultSec = [];
                $scope.MultSub = [];
                if (flag) {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        $scope.GetCur = res.data;
                        $scope.edt['sims_cur_code'] = $scope.GetCur[0].sims_cur_code
                        $scope.select_cur($scope.GetCur[0].sims_cur_code);

                    });
                }
                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        $scope.GetCur = res.data;
                      //  $scope.edt['sims_cur_code'] = $scope.GetCur[0].sims_cur_code
                        $scope.rgvtbl = true;
                        $scope.edt = { sims_cur_code: $scope.GetCur[0].sims_cur_code }
                        $scope.select_cur($scope.GetCur[0].sims_cur_code);
                        $scope.rgvtbl = false;


                      //  $scope.select_cur($scope.GetCur[0].sims_cur_code);

                    });


                }

            }

         
            $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.global_count_comp = res.data;

                if ($scope.global_count_comp) {
                    getCur(true, $scope.user_details.comp);


                }
                else {
                    getCur(false, $scope.user_details.comp)
                }
            });




            $http.get(ENV.apiUrl + "api/AgendaCreationController/GetCur").then(function (GetCur) {
                $scope.GetCur = GetCur.data;
                $scope.rgvtbl = true;
                $scope.edt = { sims_cur_code: $scope.GetCur[0].sims_cur_code }
                $scope.select_cur($scope.GetCur[0].sims_cur_code);
                $scope.rgvtbl = false;
            });

            $scope.select_cur = function (cur) {
                if (cur!=null||cur!=undefined) {
                    $scope.edt.sims_section_code = [];
                    setTimeout(function () {
                        $('#cmb_section').change(function () {
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                    $scope.MultSub = [];
                    $scope.Getgrd = [];
                    debugger;
                    $http.get(ENV.apiUrl + "api/AgendaCreationController/GetAca?cur=" + cur).then(function (GetAca) {
                        $scope.GetAca = GetAca.data;
                        $scope.Getgrd = [];
                        $scope.edt['sims_acaedmic_year'] = $scope.GetAca[0].sims_acaedmic_year;
                        $scope.select_aca(cur, $scope.GetAca[0].sims_acaedmic_year)
                        $scope.edt['selected_date'] = dateyear
                        $scope.edt['from_date'] = dateyear
                        $scope.edt['to_date'] = dateyear;
                    });
                }
            }

            $scope.select_aca = function (cur, acad_yr) {
                debugger;
                //if (cur != null && acad_yr != null) {
                //    $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + cur + "&academic_year=" + acad_yr).then(function (Getgrd) {
                //            $scope.Getgrd = Getgrd.data;
                //            setTimeout(function () {
                //                $('#cmb_grade').change(function () {
                //                    console.log($(this).val());
                //                }).multipleSelect({
                //                    width: '100%'
                //                });
                //            }, 1000);

                //        });


                //}
                if (cur != null && acad_yr != null && cur != undefined && acad_yr != undefined) {
                    $scope.Getgrd = [];
                    $scope.Getsec = [];
                    $scope.MultSec = [];
                    $http.get(ENV.apiUrl + "api/AgendaCreationController/Getgrd?cur=" + cur + "&aca=" + acad_yr + "&user=" + uname).then(function (Getgrd) {
                        $scope.Getgrd = Getgrd.data;

                        setTimeout(function () {
                            $('#cmb_grade').change(function () {
                                console.log($(this).val());
                            }).multipleSelect({
                                width: '100%'
                            });
                        }, 1000);

                    });
                }
                


            }

          
            //////////////////////////////////////////////////////////////Single Days   
            $scope.select_grd = function (grd) {
                debugger;
                if (grd!=null) {
                    $scope.AgendaDetails = '';

                    $http.get(ENV.apiUrl + "api/AgendaCreationController/Getsec?cur=" + $scope.edt.sims_cur_code + "&aca=" + $scope.edt.sims_acaedmic_year + "&grd=" + grd + "&user=" + uname).then(function (Getsec) {
                        $scope.Getsec = Getsec.data;

                    });
                }

            }

            $scope.select_sec = function (section) {
                debugger;
                if (section!=null) {
                    $scope.AgendaDetails = '';
                    $http.get(ENV.apiUrl + "api/AgendaCreationController/GetSubject?cur=" + $scope.edt.sims_cur_code + "&aca=" + $scope.edt.sims_acaedmic_year + "&grd=" + $scope.edt.sims_grade_code + "&sec=" + section + "&user=" + uname).then(function (GetSubject) {
                        $scope.GetSubject = GetSubject.data;
                    });

                }
            }

            $scope.select_sub = function () {
                $scope.AgendaDetails = '';
            }


            $scope.Show_data = function () {
                if ($scope.edt.sims_cur_code == undefined || $scope.edt.sims_cur_code == '' || $scope.edt.sims_acaedmic_year == undefined || $scope.edt.sims_acaedmic_year == '' || $scope.edt.sims_grade_code == undefined || $scope.edt.sims_grade_code == ''
                    || $scope.edt.sims_section_code == undefined || $scope.edt.sims_section_code == '' || $scope.edt.sims_subject_code == undefined || $scope.edt.sims_subject_code == ''
                    || $scope.edt.from_date == undefined || $scope.edt.from_date == ''
                    || $scope.edt.to_date == undefined || $scope.edt.to_date == '') {
                    swal('', 'All Fields Are Mandatory.');
                }
                else {
                    $scope.busy = true;
                    $scope.rgvtbl = false;
                    var data = {
                        sims_cur_code: $scope.edt.sims_cur_code
                           , sims_acaedmic_year: $scope.edt.sims_acaedmic_year
                           , sims_grade_code: $scope.edt.sims_grade_code
                           , sims_section_code: $scope.edt.sims_section_code
                           , sims_subject_code: $scope.edt.sims_subject_code
                           , from_date: $scope.edt.from_date
                           , to_date: $scope.edt.to_date
                           , user_code: uname
                    };

                    $http.post(ENV.apiUrl + "api/AgendaCreationController/AgendaDetails", data).then(function (AgendaDetails) {
                        $scope.AgendaDetails = AgendaDetails.data;
                        setTimeout(function () {
                        for (var i = 0; i < AgendaDetails.data.length; i++) {
                            var t = '#' + i + 'rich';
                            $(t).wysihtml5({
                                "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
                                "emphasis": false, //Italics, bold, etc. Default true
                                "lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                                "html": false, //Button which allows you to edit the generated HTML. Default false
                                "link": false, //Button to insert a link. Default true
                                "image": false, //Button to insert an image. Default true,
                                "color": false //Button to change color of font  
                            });
                            $(t).data("wysihtml5").editor.setValue($scope.AgendaDetails[i].sims_agenda_desc);
                            $(t).data('wysihtml5').editor.composer.disable();
                            //$(t).data('wysihtml5').editor.toolbar.hide();

                        }
                        }, 2000);
                        $scope.allItems = AgendaDetails.data;

                        console.log($scope.AgendaDetails);
                        $scope.busy = false;
                        $scope.rgvtbl = true;
                    });


                }
                //
                //if ($scope.edt.cur == undefined || $scope.edt.cur == '' || $scope.edt.aca == undefined || $scope.edt.aca == '') {
                //    swal('', 'Curriculum And Academic Year Are Mandatory.');
                //}
                //else {
                //    if ($scope.edt.em_login_code == undefined || $scope.edt.em_login_code == '') {
                //        $scope.edt.em_login_code = '';
                //    }
                //    if ($scope.edt.grade == undefined || $scope.edt.grade == '') {
                //        $scope.edt.grade = '';
                //    }
                //    if ($scope.edt.sec == undefined || $scope.edt.sec == '') {
                //        $scope.edt.sec = '';
                //    }

                //    $scope.busy = true;
                //    $scope.rgvtbl = false;

                //    $http.get(ENV.apiUrl + "api/AgendaCreationController/GetStudent?cur=" + $scope.edt.cur + "&aca=" + $scope.edt.aca + "&grade=" + $scope.edt.grade + "&sec=" + $scope.edt.sec + "&enroll=" + $scope.edt.em_login_code).then(function (GetStudent) {
                //        $scope.GetStudent = GetStudent.data;
                //        $scope.busy = false;
                //        $scope.rgvtbl = true;
                //    });

                //    $http.get(ENV.apiUrl + "api/AgendaCreationController/GetFeeCategory").then(function (GetFeeCategory) {
                //        $scope.GetFeeCategory = GetFeeCategory.data;
                //    });
                //}
            }

            $scope.reset_data = function () {
                //$scope.GetCur = '';
                $scope.GetAca = '';
                $scope.Getgrd = '';
                $scope.Getsec = '';
                $scope.GetSubject = '';
                $scope.edt.from_date = '';
                $scope.edt.to_date = '';
                $scope.AgendaDetails = '';
                //$http.get(ENV.apiUrl + "api/AgendaCreationController/GetCur").then(function (GetCur) {
                //    $scope.GetCur = GetCur.data;
                //    $scope.rgvtbl = false;
                //    $scope.edt = { sims_cur_code: $scope.GetCur[0].sims_cur_code }
                //    $scope.select_cur($scope.GetCur[0].sims_cur_code)
                //});
                $scope.rgvtbl = false;
                $scope.edt = { sims_cur_code: $scope.GetCur[0].sims_cur_code }
                $scope.select_cur($scope.GetCur[0].sims_cur_code)
                $scope.busy = false;
                $scope.rgvtbl = true;
            }

            var file_doc = [];
            $scope.deletedoc = function (a_date, a_line) {
               
                for (var i = 0; i < $scope.AgendaDetails.length; i++) {
                    if ($scope.AgendaDetails[i].sims_agenda_date == a_date) {
                        for (var j = 0; j < $scope.AgendaDetails[i].sims_doc.length; j++) {
                            if ($scope.AgendaDetails[i].sims_doc[j].sims_agenda_doc_line_no == a_line) {
                                $scope.AgendaDetails[i].sims_doc.splice(j, 1);
                            }
                        }
                        $scope.teacher_doc = [];
                        $scope.teacher_doc = $scope.AgendaDetails[i].sims_doc;
                        $scope.AgendaDetails[i].sims_doc = [];
                        for (var k = 0; k < $scope.teacher_doc.length; k++) {
                            var c = {
                                sims_agenda_number: $scope.teacher_doc[k].sims_agenda_number,
                                sims_agenda_doc_line_no: k + 1,
                                sims_agenda_doc_name_en: $scope.teacher_doc[k].sims_agenda_doc_name_en,
                                sims_agenda_doc_name: $scope.teacher_doc[k].sims_agenda_doc_name
                            }
                            $scope.AgendaDetails[i].sims_doc.push(c);
                        }

                    }
                }



            }

            $scope.deleteagenda = function (a_number) {
              
                swal({
                    title: '',
                    text: "Are you sure you want to Delete?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        for (var i = 0; i < $scope.AgendaDetails.length; i++) {
                            if ($scope.AgendaDetails[i].sims_agenda_number == a_number) {

                                $scope.AgendaDetails.splice(i, 1);
                            }
                        }


                        $http.post(ENV.apiUrl + "api/AgendaCreationController/UDeleteAgendaDoc?agenda_number=" + a_number).then(function (DeleteAgendaDoc) {
                            $scope.DeleteAgendaDoc = DeleteAgendaDoc.data;
                        });
                    }
                    else {

                    }
                });

            }

            $scope.update_details = function () {

                $scope.busy = true;
                $scope.rgvtbl = false;
                var datasend = [];
              
                for (var i = 0; i < $scope.AgendaDetails.length; i++) {
                    if ($scope.AgendaDetails[i].sims_agenda_name != '' &&  $('#'+i+"rich").val() != '') {
                       
                        var data = {
                            sims_cur_code: $scope.edt.sims_cur_code
                            , sims_acaedmic_year: $scope.edt.sims_acaedmic_year
                        , sims_grade_code: $scope.edt.sims_grade_code
                        , sims_section_code: $scope.edt.sims_section_code
                        , sims_subject_code: $scope.edt.sims_subject_code
                        , sims_agenda_number: $scope.AgendaDetails[i].sims_agenda_number
                        , sims_agenda_date: $scope.AgendaDetails[i].sims_agenda_date
                        , sims_agenda_name: $scope.AgendaDetails[i].sims_agenda_name
                        , sims_agenda_desc: $('#' + i + "rich").val() //$scope.AgendaDetails[i].sims_agenda_desc
                        , sims_agenda_teacher_code: uname
                        , sims_doc: $scope.AgendaDetails[i].sims_doc
                        , sims_agenda_visible_to_parent_portal: $scope.AgendaDetails[i].sims_agenda_visible_to_parent_portal
                        , file: $scope.AgendaDetails[i].file


                        };
                        datasend.push(data);
                    }
                }
              
                $http.post(ENV.apiUrl + "api/AgendaCreationController/AgendaUpdate", datasend).then(function (abcd) {

                    if (abcd.data == true) {
                        swal('', 'Record Is Successfully Inserted.');
                    }
                    else if (abcd.data == false) {
                        swal('', 'Record Is Not Inserted.' );
                    }
                    else {
                        swal("Error-" + abcd.data)
                    }



                });
                //$scope.busy = false;
               $scope.reset_data();
                //console.log(datasend);
                //$scope.reset_data();
            }

            $(function () {
                $('#cmb_grade').multipleSelect({
                    width: '100%'
                });
            });

            //$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";


            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };
            var file_name = '';
            var file_name_en = '';

            var ag_no = '';
            $scope.Objecttodata = function (str, index) {

                file_name = 'Agenda_';
                for (var i = 0; i < $scope.Getgrd.length; i++) {
                    if ($scope.Getgrd[i].sims_grade_code == $scope.edt.sims_grade_code)
                        file_name = file_name + $scope.Getgrd[i].sims_grade_name + '_';
                }
                for (var i = 0; i < $scope.Getsec.length; i++) {
                    if ($scope.Getsec[i].sims_section_code == $scope.edt.sims_section_code)
                        file_name = file_name + $scope.Getsec[i].sims_section_name + '_';
                }
                for (var i = 0; i < $scope.GetSubject.length; i++) {
                    if ($scope.GetSubject[i].sims_subject_code == $scope.edt.sims_subject_code)
                        file_name = file_name + $scope.GetSubject[i].sims_subject_name + '_';
                }


                $scope.dd = JSON.stringify(str);


                file_name = file_name + JSON.parse($scope.dd).sims_agenda_date;
                file_name = file_name.replace(' ', '_');
                var v = new Date();
                file_name = '';
                file_name = file_name + '_' + v.getHours() + '_' + v.getMonth() + '_' + v.getSeconds();
                $scope.maindata = str;
            }


            $scope.file_changed = function (element) {
               
                $scope.photofile = element.files[0];
                file_name_en = $scope.photofile.name;
                var len = 0;
                len = file_name_en.split('.');
                var fortype = file_name_en.split('.')[len.length - 1];
                var formatbool = false;
                for (var i = 0; i < $scope.GetTextFormat.length; i++) {
                    if ($scope.GetTextFormat[i].file_format == fortype)
                        formatbool = true;
                }
                if (formatbool == true) {
                    if ($scope.photofile.size >= 8388608) {
                        swal('', 'File size limit not exceeded 8 MB.')
                    }
                    else
                    {
                      
                        $scope.photo_filename = ($scope.photofile.type);
                      

                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $scope.$apply(function () {
                                $scope.prev_img = e.target.result;
                                file_name = '';
                                var v = new Date();
                                $scope.maindata.loading_ind = false;
                                file_name = 'Agenda' + '_' + v.getHours() + '_' + v.getMonth() + '_' + v.getSeconds() + '_' + v.getMilliseconds() + '_' + Math.floor((Math.random() * 1000) + 1);
                                $scope.file_upload_details.ftppassword = $scope.file_upload_details.ftppassword.replace('&', '%26');
                                if ($scope.filename_status ) {
                                    debugger;
                                    file_name = file_name_en.split('.');
                                    file_name = file_name[0];

                                }

                                var request = {
                                    method: 'POST',
                                    url: ENV.apiUrl + 'api/AgendaCreationController/upload?filename=' + file_name + '.' + fortype +
                                     "&newloc=" + $scope.file_upload_details.newpath +
                                     "&oldloc=" + $scope.file_upload_details.oldpath +
                                     "&uname=" + $scope.file_upload_details.ftpusername +
                                     "&pass=" + $scope.file_upload_details.ftppassword,
                                    data: formdata,
                                    headers: {
                                        'Content-Type': undefined
                                    }


                                };
                                $http(request).success(function (d) {

                                    //$scope.maindata.file = d;
                                   

                                    var data = {
                                        sims_agenda_number: $scope.maindata.sims_agenda_number,
                                        sims_agenda_doc_line_no: $scope.maindata.sims_doc.length + 1,
                                        sims_agenda_doc_name: d,
                                        sims_agenda_doc_name_en: file_name_en
                                    }

                                    $scope.maindata.sims_doc.push(data);
                                    $scope.maindata.loading_ind = true;

                                });

                            });
                        };
                        reader.readAsDataURL($scope.photofile);
                    }
                }
                else {
                    swal('', '.' + fortype + ' File format not allowed.');
                }
            }
            // $scope.uploadImgClickFShow = false;


            $scope.delfile = function (pqr) {

                $scope.maindata = pqr;
                $scope.maindata.file = '';
            }
            ///////////////////////////////////////////////////////////////////////////////Multiple agenda
            $scope.Agenda_multiple_class_click = function () {

            }

            $scope.select_grd_mult = function () {
                $scope.MultSec = [];
                $scope.MultSub = [];
                var grdcoll = [];
                for (var a = 0; a < $scope.edt.sims_grade_code.length; a++) {
                    var d = {
                        sims_cur_code: $scope.edt.sims_cur_code,
                        sims_acaedmic_year: $scope.edt.sims_acaedmic_year,
                        sims_grade_code: $scope.edt.sims_grade_code[a],
                        user_code: uname
                    }
                    grdcoll.push(d);
                }

                $http.post(ENV.apiUrl + "api/AgendaCreationController/MultSec", grdcoll).then(function (MultSec) {
                    $scope.MultSec = MultSec.data;
                    setTimeout(function () {
                        $('#cmb_section').change(function () {
                        }).multipleSelect({
                            width: '100%'
                        });
                        $("#cmb_section").multipleSelect("checkAll");
                    }, 1000);

                });


            }

            $scope.select_sec_mult = function () {
                $scope.MultSub = [];
                var seccoll = [];
                for (var a = 0; a < $scope.edt.sims_section_code.length; a++) {
                    var d = {
                        sims_cur_code: $scope.edt.sims_cur_code,
                        sims_acaedmic_year: $scope.edt.sims_acaedmic_year,
                        sims_section_code: $scope.edt.sims_section_code[a],
                        user_code: uname
                    }
                    seccoll.push(d);
                }

                $http.post(ENV.apiUrl + "api/AgendaCreationController/MultSub", seccoll).then(function (MultSub) {
                    $scope.MultSub = MultSub.data;


                });



            }

            $scope.select_sub = function () {
                $scope.AgendaDetails = '';

            }

            $scope.select_agenda_date = function (str) {
                var seccoll = [];
                for (var a = 0; a < $scope.edt.sims_section_code.length; a++) {
                    var d = {
                        sims_subject_code: $scope.edt.sims_subject_code,
                        sims_agenda_date: str,
                        sims_section_code: $scope.edt.sims_section_code[a],
                        user_code: uname
                    }
                    seccoll.push(d);
                }

                $http.post(ENV.apiUrl + "api/AgendaCreationController/AgendaGetDetails", seccoll).then(function (AgendaGetDetails) {
                    $scope.AgendaGetDetails = AgendaGetDetails.data;
                    if ($scope.AgendaGetDetails.length > 0)
                        $scope.rgvtbl_new = true;
                    else
                        $scope.rgvtbl_new = false;

                });
            }
            var file_name1 = '';
            var file_name1_en = '';
            $scope.sims_doc_new = [];
            $scope.Objecttodata_new = function (str) {
                $scope.maindata = str;
            }

            $scope.file_changed_new = function (element) {
               
                $scope.photofile = element.files[0];
                file_name1_en = '';

                file_name1_en = $scope.photofile.name;
                var len = 0;
                len = file_name1_en.split('.');
                var fortype = file_name1_en.split('.')[len.length - 1];
                var formatbool = false;
                for (var i = 0; i < $scope.GetTextFormat.length; i++) {
                    if ($scope.GetTextFormat[i].file_format == fortype)
                        formatbool = true;
                }
                if (formatbool == true) {

                    if ($scope.photofile.size >= 8388608) {
                        swal('', 'File size limit not exceeded 8 MB.')
                    }
                    else 
                    {
                       
                        $scope.photo_filename = ($scope.photofile.type);
                        
                        var v = new Date();
                        file_name1 = '';
                        file_name1 = 'Agenda' + '_' + v.getHours() + '_' + v.getMonth() + '_' + v.getSeconds() + '_' + v.getMilliseconds() + '_' + Math.floor((Math.random() * 1000) + 1);
                        if ($scope.filename_status ) {
                            debugger;
                            file_name1 = file_name1_en.split('.');
                            file_name1 = file_name1[0];

                        }

                        //    var data = {
                        //        oldpath:$scope.file_upload_details.oldpath,
                        //        newpath:$scope.file_upload_details.newpath,
                        //        ftpusername: $scope.file_upload_details.ftpusername,
                        //        ftppassword: $scope.file_upload_details.ftppassword,
                        //}
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $scope.$apply(function () {
                                $scope.prev_img = e.target.result;
                                $scope.file_upload_details.ftppassword = $scope.file_upload_details.ftppassword.replace('&', '%26');
                               
                                $scope.uploading_doc1 = false;
                                

                                var request = {
                                    method: 'POST',
                                    url: ENV.apiUrl + 'api/AgendaCreationController/upload?filename=' + file_name1 + '.' + fortype +
                                     "&newloc=" + $scope.file_upload_details.newpath +
                                     "&oldloc=" + $scope.file_upload_details.oldpath +
                                     "&uname=" + $scope.file_upload_details.ftpusername +
                                     "&pass=" + $scope.file_upload_details.ftppassword,
                                    data: formdata,
                                    headers: {
                                        'Content-Type': undefined
                                    }


                                };
                                $http(request).success(function (d) {
                                  
                                    //$scope.maindata.file = d;

                                    var data = {
                                        sims_agenda_doc_line_no: $scope.sims_doc_new.length + 1,
                                        sims_agenda_doc_name: d,
                                        sims_agenda_doc_name_en: file_name1_en
                                    }

                                    $scope.sims_doc_new.push(data);



                                    //var data = {
                                    //    sims_agenda_doc_line_no: $scope.maindata.sims_doc.length + 1,
                                    //    sims_agenda_doc_name: d
                                    //}

                                    //$scope.maindata.sims_doc.push(data);
                                    $scope.uploading_doc1 = true;
                                });

                            });
                        };
                        reader.readAsDataURL($scope.photofile);
                    }
                }
                else {
                    swal('', '.' + fortype + ' Files Format is not allowed.');
                }
            }

            $scope.deletedoc_new = function (a_line) {
           


                for (var j = 0; j < $scope.sims_doc_new.length; j++) {
                    if ($scope.sims_doc_new[j].sims_agenda_doc_line_no == a_line) {
                        $scope.sims_doc_new.splice(j, 1);
                    }
                }
                $scope.teacher_doc = [];
                $scope.teacher_doc = $scope.sims_doc_new;
                $scope.sims_doc_new = [];
                for (var k = 0; k < $scope.teacher_doc.length; k++) {
                    var c = {

                        sims_agenda_doc_line_no: k + 1,
                        sims_agenda_doc_name: $scope.teacher_doc[k].sims_agenda_doc_name,
                        sims_agenda_doc_name_en: $scope.teacher_doc[k].sims_agenda_doc_name_en
                    }
                    $scope.sims_doc_new.push(c);
                }

            }

            $http.get(ENV.apiUrl + "api/Agenda/getAgendaDocDetails").then(function (agendaDocData) {
                $scope.agenda_doc_details = agendaDocData.data;
                if ($scope.agenda_doc_details.length > 0) {

                    if ($scope.agenda_doc_details[0].sims_appl_parameter == 'Y')
                        $scope.filename_status = true;
                    else
                        $scope.filename_status = false;
                }
            });

            $scope.SAVE_MULTI_CLASS = function () {
                debugger;
                $scope.busy = true;
                $scope.busy_new = true;
                $scope.rgvtbl_new = false;
               
                if ($scope.edt.sims_grade_code == undefined || $scope.edt.sims_section_code == undefined) {
                    swal('', 'All Fields Are Mandatory.');
                    $scope.busy_new = false;
                    $scope.busy = false;
                }
                else {
                    if ($scope.edt.sims_cur_code == undefined || $scope.edt.sims_cur_code == '' || $scope.edt.sims_acaedmic_year == undefined || $scope.edt.sims_acaedmic_year == ''
                        || $scope.edt.sims_grade_code.length == 0
                      || $scope.edt.sims_section_code.length == 0 || $scope.edt.sims_subject_code == undefined || $scope.edt.sims_subject_code == ''
                      || $scope.edt.selected_date == undefined || $scope.edt.selected_date == '' || $scope.sims_agenda_name == '' || $scope.sims_agenda_name == undefined || $('#text-editor').val() == '' || $('#text-editor').val() == undefined) {
                        swal('', 'All Fields Are Mandatory.');
                        $scope.busy_new = false;
                        $scope.busy = false;
                    }
                    else {

                        var agcoll = [];
                        for (var a = 0; a < $scope.edt.sims_section_code.length; a++) {
                            var d = {
                                sims_cur_code: $scope.edt.sims_cur_code,
                                sims_acaedmic_year: $scope.edt.sims_acaedmic_year,
                                sims_grade_code: $scope.edt.sims_section_code[a],
                                sims_section_code: $scope.edt.sims_section_code[a],
                                sims_subject_code: $scope.edt.sims_subject_code,
                                sims_agenda_date: $scope.edt.selected_date,
                                sims_agenda_name: $scope.sims_agenda_name,
                                sims_agenda_desc: $('#text-editor').val(),
                                sims_agenda_teacher_code: uname,
                                sims_agenda_visible_to_parent_portal: $scope.sims_agenda_visible_to_parent_portal,
                                sims_doc: $scope.sims_doc_new,

                            }
                            agcoll.push(d);
                            
                        }
                        debugger;
                        $http.post(ENV.apiUrl + "api/AgendaCreationController/AgendaUpdateMult", agcoll).then(function (AgendaUpdateMult) {
                            $scope.AgendaUpdateMult = AgendaUpdateMult.data;
                            if ($scope.AgendaUpdateMult) {
                                swal('', 'Agenda successfully created.');
                            }
                            $scope.busy_new = false;
                            $scope.rgvtbl_new = true;
                            $scope.reset_multi();
                        });
                        $scope.busy = false;
                    }
                }
            }

            $scope.reset_multi = function () {
                debugger;
              
               // $scope.GetAca = [];
                setTimeout(function () {
                    $('#cmb_grade').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
                setTimeout(function () {
                    $('#cmb_section').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
                try {
                    $("#cmb_grade").multipleSelect("uncheckAll");
                }
                catch (e) { }
                try {
                    $("#cmb_section").multipleSelect("uncheckAll");
                }
                catch (e) { }
                $scope.MultSub = [];
                $scope.edt.selected_date = '';
                $scope.sims_agenda_name = '';
                $scope.sims_agenda_desc = '';
                $scope.sims_agenda_visible_to_parent_portal = true;
                $scope.sims_doc_new = [];
                $scope.rgvtbl_new = false;
                $scope.AgendaGetDetails = [];
               // $scope.select_cur($scope.GetCur[0].sims_cur_code);

            }

            $(function () {
                $('#cmb_grade').multipleSelect({ width: '100%' });
                $('#cmb_section').multipleSelect({ width: '100%' });
            });

            $scope.Objecttodata_abc = function (str) {


                $scope.maindata = str;
            }

            var file_name2 = '';
            var file_name2_en = '';
            $scope.file_changed_abc = function (element) {
               
                file_name2_en = '';
                $scope.photofile = element.files[0];
                file_name2_en = $scope.photofile.name;
                var len = 0;
                len = file_name2_en.split('.');
                var fortype = file_name2_en.split('.')[len.length - 1];

                var formatbool = false;
                for (var i = 0; i < $scope.GetTextFormat.length; i++) {
                    if ($scope.GetTextFormat[i].file_format == fortype)
                        formatbool = true;
                }
                if (formatbool == true) {
                    if ($scope.photofile.size >= 8388608) {
                        swal('', 'File size limit not exceeded 8 MB.')
                    }
                    else
                    {
                       
                        $scope.photo_filename = ($scope.photofile.type);
                       
                        var v = new Date();
                        $scope.maindata.loading_ind = true;
                        file_name2 = '';
                        file_name2 = 'Agenda' + '_' + v.getHours() + '_' + v.getMonth() + '_' + v.getSeconds() + '_' + v.getMilliseconds() + '_' + Math.floor((Math.random() * 1000) + 1);

                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $scope.$apply(function () {
                                $scope.prev_img = e.target.result;
                                $scope.file_upload_details.ftppassword = $scope.file_upload_details.ftppassword.replace('&', '%26');
                               
                                if ($scope.filename_status ) {
                                    debugger;
                                    file_name2 = file_name2_en.split('.');
                                    file_name2 = file_name2[0];

                                }

                                var request = {
                                    method: 'POST',
                                    url: ENV.apiUrl + 'api/AgendaCreationController/upload?filename=' + file_name2 + '.' + fortype +
                                     "&newloc=" + $scope.file_upload_details.newpath +
                                     "&oldloc=" + $scope.file_upload_details.oldpath +
                                     "&uname=" + $scope.file_upload_details.ftpusername +
                                     "&pass=" + $scope.file_upload_details.ftppassword,
                                    data: formdata,
                                    headers: {
                                        'Content-Type': undefined
                                    }


                                };
                                $http(request).success(function (d) {

                                    //$scope.maindata.file = d;
                                   

                                    var data = {
                                        sims_agenda_number: $scope.maindata.sims_agenda_number,
                                        sims_agenda_doc_line_no: $scope.maindata.sims_doc.length + 1,
                                        sims_agenda_doc_name: d,
                                        sims_agenda_doc_name_en: file_name2_en
                                    }

                                    $scope.maindata.sims_doc.push(data);
                                    $scope.maindata.loading_ind = false;
                                });

                            });
                        };
                        reader.readAsDataURL($scope.photofile);
                    }
                }
                else {
                    swal('', '.' + fortype + ' File format is not allowed.');
                }
            }

            $scope.deletedoc_abc = function (a_number, a_line) {
              

                swal({
                    title: '',
                    text: "Are you sure you want to Delete?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        for (var i = 0; i < $scope.AgendaGetDetails.length; i++) {
                            if ($scope.AgendaGetDetails[i].sims_agenda_number == a_number) {
                                for (var j = 0; j < $scope.AgendaGetDetails[i].sims_doc.length; j++) {
                                    if ($scope.AgendaGetDetails[i].sims_doc[j].sims_agenda_doc_line_no == a_line) {
                                        $scope.AgendaGetDetails[i].sims_doc.splice(j, 1);
                                    }
                                }
                                $scope.teacher_doc = [];
                                $scope.teacher_doc = $scope.AgendaGetDetails[i].sims_doc;
                                $scope.AgendaGetDetails[i].sims_doc = [];
                                for (var k = 0; k < $scope.teacher_doc.length; k++) {
                                    var c = {
                                        sims_agenda_number: $scope.teacher_doc[k].sims_agenda_number,
                                        sims_agenda_doc_line_no: k + 1,
                                        sims_agenda_doc_name: $scope.teacher_doc[k].sims_agenda_doc_name,
                                        sims_agenda_doc_name_en: $scope.teacher_doc[k].sims_agenda_doc_name_en
                                    }
                                    $scope.AgendaGetDetails[i].sims_doc.push(c);
                                }

                                var datasend = [];
                               
                                var data = {
                                    sims_cur_code: $scope.AgendaGetDetails[i].sims_cur_code
                        , sims_acaedmic_year: $scope.AgendaGetDetails[i].sims_acaedmic_year
                    , sims_grade_code: $scope.AgendaGetDetails[i].sims_grade_code
                    , sims_section_code: $scope.AgendaGetDetails[i].sims_section_code
                    , sims_subject_code: $scope.AgendaGetDetails[i].sims_subject_code
                    , sims_agenda_number: $scope.AgendaGetDetails[i].sims_agenda_number
                    , sims_agenda_date: $scope.edt.selected_date
                    , sims_agenda_name: $scope.AgendaGetDetails[i].sims_agenda_name
                    , sims_agenda_desc: $scope.AgendaGetDetails[i].sims_agenda_desc
                    , sims_agenda_teacher_code: uname
                    , sims_doc: $scope.AgendaGetDetails[i].sims_doc
                    , sims_agenda_visible_to_parent_portal: $scope.AgendaGetDetails[i].sims_agenda_visible_to_parent_portal
                    , file: $scope.AgendaGetDetails[i].file
                                }
                                datasend.push(data);
                                $http.post(ENV.apiUrl + "api/AgendaCreationController/AgendaUpdate", datasend).then(function (AgendaUpdate) {
                                    $scope.AgendaUpdate = AgendaUpdate.data;
                                });
                            }


                        }
                    }
                    else {

                    }

                });

            }
            // $scope.uploadImgClickFShow = false;

            $scope.update_details_new = function () {
                
                $scope.busy_abc = true;

                var datasend = [];
               
                for (var i = 0; i < $scope.AgendaGetDetails.length; i++) {

                    var data = {
                        sims_cur_code: $scope.AgendaGetDetails[i].sims_cur_code
            , sims_acaedmic_year: $scope.AgendaGetDetails[i].sims_acaedmic_year
        , sims_grade_code: $scope.AgendaGetDetails[i].sims_grade_code
        , sims_section_code: $scope.AgendaGetDetails[i].sims_section_code
        , sims_subject_code: $scope.AgendaGetDetails[i].sims_subject_code
        , sims_agenda_number: $scope.AgendaGetDetails[i].sims_agenda_number
        , sims_agenda_date: $scope.edt.selected_date
        , sims_agenda_name: $scope.AgendaGetDetails[i].sims_agenda_name
        , sims_agenda_desc: $scope.AgendaGetDetails[i].sims_agenda_desc
        , sims_agenda_teacher_code: uname
        , sims_doc: $scope.AgendaGetDetails[i].sims_doc
        , sims_agenda_visible_to_parent_portal: $scope.AgendaGetDetails[i].sims_agenda_visible_to_parent_portal
        , file: $scope.AgendaGetDetails[i].file
                    }
                    datasend.push(data);
                }

                $http.post(ENV.apiUrl + "api/AgendaCreationController/AgendaUpdate", datasend).then(function (AgendaUpdate) {
                    $scope.AgendaUpdate = AgendaUpdate.data;
                    swal('', 'Agenda are updated successfully.');
                    $scope.reset_multi();
                });
                $scope.busy_abc = false;
            }

            $scope.deleteagendaAll = function (a_number) {
               
                swal({
                    title: '',
                    text: "Are you sure you want to Delete?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        for (var i = 0; i < $scope.AgendaGetDetails.length; i++) {
                            if ($scope.AgendaGetDetails[i].sims_agenda_number == a_number) {

                                $scope.AgendaGetDetails.splice(i, 1);
                            }
                        }


                        $http.post(ENV.apiUrl + "api/AgendaCreationController/UDeleteAgendaDoc?agenda_number=" + a_number).then(function (DeleteAgendaDoc) {
                            $scope.DeleteAgendaDoc = DeleteAgendaDoc.data;
                        });
                    }
                    else {

                    }
                });

            }

            ////////////////////////////////////////////////////////////////////////////////////////////
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "dd-mm-yyyy"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }])
})();