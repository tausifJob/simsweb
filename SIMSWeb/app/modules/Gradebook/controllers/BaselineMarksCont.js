﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('BaselineMarksCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.pagesize = "All";
            $scope.pageindex = 0;
            $scope.display = false;
            $scope.table1 = true;
            $scope.fee_no = true;
            $scope.table = true;
            $scope.visit_no = true;
            var check_no = null;
            $scope.busy = false;
            $scope.chku = false;
            $scope.table_data = false;
            $scope.edt = [];

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };




            $http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (curiculum) {
                $scope.Curriculum_obj = curiculum.data;
                if (curiculum.data.length > 0) {
                    $scope.cur_code = curiculum.data[0].sims_cur_code;
                    $scope.Cur_Change();
                }
            });
            var Current_Username = $rootScope.globals.currentUser.username;

            $scope.Cur_Change = function () {
                $http.get(ENV.apiUrl + "api/attendance/getAttendanceyear?cur_code=" + $scope.cur_code).then(function (academicyears) {
                    $scope.academicyears_obj = academicyears.data;
                    if (academicyears.data.length > 0) {
                        $scope.Acdm_yr = academicyears.data[0].sims_academic_year;
                        $scope.acdm_yr_change();
                    }

                });
          //   $scope.attendance_codes_obj = {};
               
            }


            $scope.acdm_yr_change = function () {
                $http.get(ENV.apiUrl + "api/attendance/getAttendancegrade?cur_code=" + $scope.cur_code + "&academic_year=" + $scope.Acdm_yr + "&userName=" + Current_Username).then(function (grades) {
                    $scope.grades_obj = grades.data;
                    if(grades.data.length>0)
                    {
                        $scope.grade_code = grades.data[0].sims_grade_code;
                        $scope.grade_change();
                        $scope.getSubject();
                    }
                });
               
            }

           

            $scope.grade_change = function () {
                debugger
                $http.get(ENV.apiUrl + "api/attendance/getAttendancesection?cur_code=" + $scope.cur_code + "&academic_year=" + $scope.Acdm_yr
                    + "&gradeCode=" + $scope.grade_code + "&userName=" + Current_Username).then(function (section) {
                        $scope.sections_names_obj = section.data;
                        if(section.data.length>0)
                        {
                            $scope.section_code = section.data[0].sims_section_code;
                        }
                        $scope.getSubject();

                    });

            }


            $scope.getSubject = function () {
                debugger
                $http.get(ENV.apiUrl + "api/Trait/getSectionsubjectforAll?curcode=" + $scope.cur_code + "&academicyear=" + $scope.Acdm_yr + "&login_user=" + Current_Username + "&grade=" + $scope.grade_code + "&section=" + $scope.section_code).then(function (getSectionsubject_name) {
                    $scope.cmb_subject_name = getSectionsubject_name.data;
                    if (getSectionsubject_name.data.length > 0) {
                        $scope.sims_subject_code = getSectionsubject_name.data[0].sims_subject_code;
                    }
                });
            }
           
           
           



            //$scope.getCombination=function(year)
            //{
            //    $scope.promotegrade = false;
            //    $scope.newpromotegrade = true;
            //    debugger;
            //    $http.get(ENV.apiUrl + "api/StudentReport/getgradesectionCombo?cur_year=" + year).then(function (getCombogrdsec) {
            //        $scope.getCombogrd_sec = getCombogrdsec.data;
            //    });
            //}

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.AllBaselineMarks_details;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.AllBaselineMarks_details, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.AllBaselineMarks_details;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_student_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;

            }

            $scope.New = function () {

                $scope.temp = '';
                $scope.edt = '';
                $scope.display = true;
                $scope.table = false;
                $scope.table1 = false;
                $scope.instaminimuma = true;
                $scope.update1 = false;

                $scope.Update_btn = false;
                $scope.show_btn = true;


            }


            $scope.Submit = function () {
                debugger
                $http.get(ENV.apiUrl + "api/Trait/getBaselineMarksData?curcode=" + $scope.cur_code + "&academicyear=" + $scope.Acdm_yr + "&grade=" + $scope.grade_code + "&section=" + $scope.section_code + "&subject=" + $scope.sims_subject_code + "&login_user=" + Current_Username).then(function (AllBaselineMarks) {
                    debugger
                    $scope.AllBaselineMarks_details = AllBaselineMarks.data;
                    $scope.totalItems = $scope.AllBaselineMarks_details.length;
                    $scope.todos = $scope.AllBaselineMarks_details;
                    $scope.makeTodos();
                    $scope.size($scope.pagesize);
                    $scope.pager = true;
                    $scope.table_data = true;
                    if (AllBaselineMarks.data.length <= 0) {
                        swal({ text: "Record Not Present", imageUrl: "assets/img/close.png", width: 380, height: 200 });
                    }

                });
            }


            $scope.savedatapromote = function () {
                $scope.busy = true;
                debugger;
                var deleteintcode = [];
                var Savedata = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    
                    var v = document.getElementById("promo-" + i);
                    if (v.checked == true) {
                        var deleteintcode = ({
                            'sims_cur_code': $scope.cur_code,
                            'bell_academic_year': $scope.Acdm_yr,
                            'bell_grade_code': $scope.grade_code,
                            'sims_section_code': $scope.section_code,
                            'sims_enroll_number': $scope.filteredTodos[i].sims_student_enroll_number,
                            'sims_subject_code': $scope.sims_subject_code,
                            'sims_subject_baseline_obtained_score': $scope.filteredTodos[i].sims_subject_baseline_obtained_score,
                            'sims_user_baseline_created_by': Current_Username,
                            opr: 'U',
                        });
                        $scope.insert = true;
                        Savedata.push(deleteintcode);
                   
                    }
                }
                if ($scope.insert) {
                    $http.post(ENV.apiUrl + "api/Trait/CUDBaselineMarks", Savedata).then(function (msg) {
                        debugger;
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Baseline Marks Inserted Successfully", imageUrl: "assets/img/check.png", width: 380, height: 200 });
                            $scope.busy = false;
                            deleteintcode = [];
                            Savedata = [];
                            $scope.Submit();
                        }
                        else {
                            swal({ text: "Marks not Inserted", imageUrl: "assets/img/close.png", width: 380, height: 200 });
                            $scope.busy = false;
                        }

                    });
                }
            }

            $scope.getmarks = function (str)
            {
                debugger
                if (str > 100) {
                    swal({ title: "Alert", text: "Marks Should be less than 100", width: 380, height: 200 });
                    event.target.value = '';
                }
               
            }

            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("promo-"+i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("promo-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.Reset = function()
            {
                $scope.grade_code='';
                $scope.section_code='';
                $scope.sims_subject_code = '';
                $scope.table_data = false;
            }

            $scope.Cancel = function () {
                // $scope.Reset();
                $scope.table_data = false;
                $scope.filteredTodos = '';

            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

        }])
})();
