﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('AnalysisAssessmentReportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {


            $scope.temp = {};
            $scope.detailsField = true;
            $scope.hideprintpdf = false;
            $scope.decimal_dsc = '';

            $scope.pagesize = '30';
            $scope.pageindex = "0";
            $scope.pager = false;
            $scope.show_period_by_month = false;
            $scope.how_period_by_term = false;
            $scope.busy = false;

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = false;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['chk_all'] = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 30, $scope.maxSize = 100;

            $scope.makeTodos = function () {
                debugger;
                if ($scope.pagesize == 'All') {
                    $scope.currentPage = 1;
                    $scope.showPager = false;
                    $scope.filteredTodos = $scope.report_data_new;
                }
                else {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                }
            };


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res1) {
                $scope.cur_data = res1.data;
                $scope.temp.sims_cur_code = $scope.cur_data[0].sims_cur_code;
                $scope.getAcademic_year($scope.cur_data[0].sims_cur_code);
            });


            $scope.getAcademic_year = function (cur_code1) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code1).then(function (res1) {
                    $scope.acad_data = res1.data;
                    setTimeout(function () {
                        $('#academic_box').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                        $("#academic_box").multipleSelect("checkAll");
                    }, 1000);
                    $scope.temp.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                    $scope.Getterm1($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                });
            }

            $scope.getGrade = function (curCode, accYear) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                        $("#cmb_grade_code").multipleSelect("checkAll");
                    }, 1000);

                });
            }

            $(function () {
                $('#cmb_section_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.getsubject = function (cur_code, acad_year, grade_code) {
                debugger
                $http.get(ENV.apiUrl + "api/SGradebookReportController/GetSubjects?cur_code=" + cur_code + "&acad_year=" + acad_year + "&grade_code=" + grade_code).then(function (Sectioncode) {
                    $scope.subjectdata = Sectioncode.data;
                    $scope.temp.sims_subject_code = $scope.subjectdata[0].sims_subject_code;
                    //setTimeout(function () {
                    //    $('#concession_box').change(function () {
                    //        console.log($(this).val());
                    //    }).multipleSelect({
                    //        width: '100%',
                    //        filter: true
                    //    });
                    //}, 1000);
                })
            }

            $scope.Getterm1 = function (cur_code,acad_year) {
                debugger
                $http.get(ENV.apiUrl + "api/Gradebook/getTermforGradebook?cur_code=" + cur_code + "&academic_year=" + acad_year).then(function (Getsims550_TermsAcademicYear) {
                    $scope.TermsAcademicYear = Getsims550_TermsAcademicYear.data;
                    $scope.temp.sims_term_code = $scope.TermsAcademicYear[0].sims_term_code;
                });
            }

            $scope.getstudentlist = function (cur_code, acad_year, grade, sims_subject_code,term_code) {
                debugger;
                $scope.busy = true;
                $scope.report_data_new = [];
                $scope.detailsField = true;
                var sims_grade_code = '';
                var sims_section_code = '';

                //  $scope.route_dir1 = $('#route_dir_box').multipleSelect('getSelects', 'text');

                $scope.selected_academic_year = $("#academic_box option:selected").text();
                $scope.section = $('#cmb_section_code').multipleSelect('getSelects', 'text');
                $scope.concession_Type = $('#concession_box').multipleSelect('getSelects', 'text');
                $scope.grade = $('#cmb_grade_code').multipleSelect('getSelects', 'text');
                $scope.cur = $("#cur_box option:selected").text();

                //string cur_code, string acad_year, string grade_code, string section_code,string subject_code,string term_code
                $http.get(ENV.apiUrl + "api/SGradebookReportController/GetReportDetails?cur_code=" + cur_code + "&acad_year=" + acad_year
                    + "&grade_code=" + grade + "&subject_code=" + sims_subject_code + "&term_code=" + term_code).then(function (res1) {
                        if (res1.data.length > 0) {
                            $scope.report_data_new = res1.data;
                            console.log("$scope.report_data_new" + $scope.report_data_new);
                         //   $scope.totalItems = $scope.report_data_new.length;
                         //   $scope.todos = $scope.report_data_new;
                         //   $scope.makeTodos();
                        }
                        else {
                            $scope.filteredTodos = [];
                            swal({ title: "Alert", text: " Sorry !!!!Data is not Available", showCloseButton: true, width: 300, height: 200 });
                            $scope.report_data_new = [];
                        }

                    });
                $scope.busy = false;
            }

            $http.get(ENV.apiUrl + "api/StudentFee/getDecimalPlaces").then(function (getDecimal) {
                debugger;
                $scope.decimal_degit = getDecimal.data;
                $scope.decimal_dsc = $scope.decimal_degit[0].comp_curcy_dec;
            });

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $scope.colsvis = false;


            $scope.exportData = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('pdf_print').innerHTML],
                         {
                             type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                         });
                        $scope.detailsField = false;
                        saveAs(blob, "AnalysisAssessment.xls");
                        $scope.colsvis = false;
                        $scope.getsubdetail();

                    }

                });
                $scope.colsvis = true;

            };


            $scope.print = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var docHead = document.head.outerHTML;
                        var printContents = document.getElementById('pdf_print').outerHTML;
                        var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                        var newWin = window.open("", "_blank", winAttr);
                        var writeDoc = newWin.document;
                        writeDoc.open();
                        writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                        writeDoc.close();
                        newWin.focus();
                        $scope.detailsField = false;
                        $scope.colsvis = false;
                        $scope.getsubdetail();

                    }
                });
            };

            $scope.reset_data = function () {
                debugger;
                // $scope.temp.sims_grade_code = '';
                $scope.filteredTodos = [];
            }

            $scope.ischeck = function () {
                debugger
                $scope.checklick = function () {
                    var v = document.getElementById('expire');
                    if (v.checked == true) {
                        v.checked == false;
                    }
                }
            }

        }])

})();

