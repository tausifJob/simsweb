﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Gradebook');

    simsController.controller('ReportCardAttendanceCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.reportHeadig = [];
            $scope.finalData = [];
            $scope.postData = [];
            $scope.obj = [];
            $scope.disabledGenerate = true;
            $scope.showSectionMsg = false;
            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.Preview = function (Myform) {

                $scope.finalData = [];
                $scope.reportHeadig = [];
                $scope.obj = [];
                if (($scope.edt.sims_section_code == "" || $scope.edt.sims_section_code == undefined || $scope.edt.sims_section_code == null) && ($scope.edt.sims_enrollment_number == undefined || $scope.edt.sims_enrollment_number == '' || $scope.edt.sims_enrollment_number == null)) {
                    $scope.showSectionMsg = true;
                    //return false;
                } else {
                    $scope.showSectionMsg = false;
                }

                if (Myform) {
                    var obj = {};
                    var section = "";
                    if ($scope.edt.sims_section_code.length > 0) {
                        for (var i = 0; i < $scope.edt.sims_section_code.length; i++) {
                            section = section + $scope.edt.sims_section_code[i] + ',';
                        }
                    }
                    obj.sims_cur_code = $scope.edt.sims_cur_code;
                    obj.sims_academic_year = $scope.edt.sims_academic_year;
                    obj.sims_grade_code = $scope.edt.sims_grade_code;
                    ///obj.sims_section_code = $scope.edt.sims_section_code;
                    //obj.sims_section_code = section;
                    obj.sims_term_start_date = $scope.edt.termStartDate;
                    obj.sims_term_end_date = $scope.edt.termEndDate;

                    if ($scope.edt.sims_enrollment_number == undefined || $scope.edt.sims_enrollment_number == '' || $scope.edt.sims_enrollment_number == null) {
                        obj.sims_enrollment_number = null;
                    }
                    else {
                        obj.sims_enrollment_number = $scope.edt.sims_enrollment_number;
                    }

                    $http.post(ENV.apiUrl + "api/reportCardAttendance/previewCardAttendance?section_code=" + section, obj).then(function (response) {
                        $scope.obj = response.data.table;
                        $scope.headers = response.data.table[0];
                        angular.forEach($scope.headers, function (value, key) {
                            if (key != 'sims_section_code') {
                                var obj = {
                                    heading: key
                                }
                                $scope.reportHeadig.push(obj);
                            }

                        });
                        angular.forEach($scope.obj, function (val, k) {

                            angular.forEach(val, function (value, key) {
                                var ob = {
                                    sims_attendance_code: key,
                                    sims_attendance_cout: value
                                }
                                $scope.finalData.push(ob);
                            });

                        });

                        if ($scope.obj.length == 0) {
                            swal({  text: "No record(s) found.", imageUrl: "assets/img/close.png", showCloseButton: true, width: 450, height: 200 });
                        }
                        else {
                            $scope.grid = true;
                            $scope.disabledGenerate = false;
                            $scope.totalItems = $scope.obj.length;
                            $scope.todos = $scope.obj;
                            $scope.makeTodos();
                        }

                    });
                }
            }

            $scope.makeTodos = function () {
                var rem = parseFloat($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseFloat(begin) + parseFloat($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edt = {};

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt["sims_section_code"] = '';
                $scope.edt["sims_grade_code"] = '';
                $scope.edt["sims_term_code"] = '';
                $scope.edt["termStartDate"] = '';
                $scope.edt["termEndDate"] = '';
                $("#section_code").multipleSelect("uncheckAll");
            }



            $scope.reset = function () {
                $scope.grid = false;
                $scope.disabledGenerate = true;
                $scope.showSectionMsg = false;
                //$scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                //$scope.edt["sims_cur_code"] = '';
                $scope.edt["sims_section_code"] = '';
                $scope.edt["sims_grade_code"] = '';
                $scope.edt["sims_term_code"] = '';
                $scope.edt["termStartDate"] = '';
                $scope.edt["termEndDate"] = '';
                $scope.edt["sims_enrollment_number"] = '';
                $("#section_code").multipleSelect("uncheckAll");
            }

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCurriculum) {
                $scope.Curriculum = getCurriculum.data;
                $scope.edt['sims_cur_code'] = $scope.Curriculum[0].sims_cur_code
                $scope.getacademicYear($scope.Curriculum[0].sims_cur_code);
            });

            $scope.getacademicYear = function (str) {
                $scope.AcademicYear = [];
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (getAcademicYear) {
                    $scope.AcademicYear = getAcademicYear.data;
                    $scope.edt['sims_academic_year'] = $scope.AcademicYear[0].sims_academic_year;
                    $scope.getGrade($scope.Curriculum[0].sims_cur_code, $scope.AcademicYear[0].sims_academic_year);
                    $scope.Getterm1();
                });
            }

            $scope.getGrade = function (str1, str2) {
                $scope.AllGrades = [];
                var param = {}
                param.sims_cur_code = str1;
                param.sims_academic_year = str2;

                $http.post(ENV.apiUrl + "api/reportCardAttendance/GradeWithPara", param).then(function (getAllGrades) {
                    $scope.AllGrades = getAllGrades.data;
                });
                //$http.get(ENV.apiUrl + "api/common/getAllGradesCommon?cur_code=" + str1 + "&academic=" + str2).then(function (getAllGrades) {
                //    $scope.AllGrades = getAllGrades.data;
                //    console.log($scope.AllGrades);
                //})
            }

            $scope.getsection = function (str, str1, str2) {
                $scope.getSectionFromGrade = [];
                var param = {};
                param.sims_cur_code = str;
                param.sims_grade_code = str1;
                param.sims_academic_year = str2;
                $http.post(ENV.apiUrl + "api/ReportCardAttendance/sectionCommon_p", param).then(function (Allsection) {
                    $scope.getSectionFromGrade = Allsection.data;
                    setTimeout(function () {
                        $('#section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });

            };
            $(function () {
                $('#section_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.Getterm1 = function () {
                $scope.Termsobject = [];
                $http.get(ENV.apiUrl + "api/reportCardAttendance/GetAllterm?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Termsobject) {
                    $scope.Termsobject = Termsobject.data;
                });
            }

            $scope.setTermDate = function (termId) {
                angular.forEach($scope.Termsobject, function (val, key) {
                    if (termId == val.sims_term_code) {

                        $scope.edt["termStartDate"] = val.sims_term_start_date;
                        $scope.edt["termEndDate"] = val.sims_term_end_date;
                        $scope.$apply();

                    }
                    else {
                        $scope.edt["termStartDate"] = "";
                        $scope.edt["termEndDate"] = "";
                    }
                });
            };


            $scope.Save = function (Myform) {
                if (Myform) {
                    $scope.postData = [];
                    $scope.disabledGenerate = true;
                    angular.forEach($scope.obj, function (value, key) {
                        angular.forEach($scope.reportHeadig, function (val, k) {
                            if (val.heading != 'enroll No' && val.heading != "student Name") {
                                var obj = {
                                    opr: 'I',
                                    sims_grade_code: $scope.edt.sims_grade_code,
                                    sims_cur_code: $scope.edt.sims_cur_code,
                                    sims_academic_year: $scope.edt.sims_academic_year,
                                    //sims_section_code: $scope.edt.sims_section_code,
                                    sims_section_code: value['sims_section_code'],
                                    sims_term_code: $scope.edt.sims_term_code,
                                    sims_enrollment_number: value['enroll No'],
                                    sims_attendance_code: val.heading,
                                    sims_attendance_cout: value[val.heading]
                                }
                                $scope.postData.push(obj);
                            }
                        });
                    });

                    if ($scope.edt.sims_enrollment_number == null || $scope.edt.sims_enrollment_number == undefined || $scope.edt.sims_enrollment_number == "") {
                        var section = "";
                        for (var i = 0; i < $scope.edt.sims_section_code.length; i++) {
                            section = section + $scope.edt.sims_section_code[i] + ',';
                        }
                        $scope.edt = {
                            sims_grade_code: $scope.edt.sims_grade_code,
                            sims_cur_code: $scope.edt.sims_cur_code,
                            sims_academic_year: $scope.edt.sims_academic_year,
                            sims_section_code: section,
                            sims_term_code: $scope.edt.sims_term_code,
                            sims_enrollment_number: null
                        }
                    }
                    else {
                        var section = "";
                        for (var i = 0; i < $scope.edt.sims_section_code.length; i++) {
                            section = section + $scope.edt.sims_section_code[i] + ',';
                        }
                        $scope.edt = {
                            sims_grade_code: $scope.edt.sims_grade_code,
                            sims_cur_code: $scope.edt.sims_cur_code,
                            sims_academic_year: $scope.edt.sims_academic_year,
                            sims_section_code: section,
                            sims_term_code: $scope.edt.sims_term_code,
                            sims_enrollment_number: $scope.edt.sims_enrollment_number
                        }
                    }

                    $http.post(ENV.apiUrl + "api/reportCardAttendance/addReportCardAttendance?data1=" + JSON.stringify($scope.edt), $scope.postData).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                        $scope.postData = [];
                        if ($scope.msg1 == true) {
                            swal({ text: 'Report Card Attendance Added Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                            $scope.display = true;
                            $scope.grid = false;
                            $scope.disabledGenerate = false;
                            $scope.reset();
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: 'Not Added. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                            $scope.display = true;
                            $scope.grid = false;
                            $scope.disabledGenerate = false;
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }





            $scope.size = function (str) {

                if (str == 'All') {

                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.filteredTodos = $scope.GetAllStudentFee;
                    $scope.numPerPage = $scope.obj.length;
                    $scope.maxSize = $scope.obj.length
                    $scope.makeTodos();
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in 2 fields */
                //$scope.postData.push()
                return (item['enroll No'].toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item['student Name'].toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

        }])
})();