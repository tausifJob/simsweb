﻿(function () {
    'use strict';
    var teacher_code = [];
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('GradebookCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.headcolor = '1';
            $scope.markentryflag = false;
            $scope.markentryflag1 = false;
            $scope.markentryflag2 = false;
            $scope.newinsert = true;
            $scope.upinsert = false;
            $scope.markentries = true;
            $scope.Expand1 = true;
            $scope.statuss = false;
            $scope.sisqatarcomment = true;
            $scope.show_assignment_to_teacher = false;
            $scope.max_score = false;
            $scope.extra_credit = false;
            $scope.Assign = [];
            $scope.show_roll_no = false;  //getshowrollno
            $scope.showAssignmentstudent = false;
            $http.get(ENV.apiUrl + "api/Gradebook_History/getshowrollno").then(function (GradeScaleData) {
                $scope.show_scale = GradeScaleData.data;
                if ($scope.show_scale[0].sims_appl_parameter == '1') {
                    $scope.show_roll_no = true;
                } else {
                    $scope.show_roll_no = false;
                }
            });

            $http.get(ENV.apiUrl + "api/Gradebook/GetAssignmentstudshow").then(function (GetAssignmentTypes) {
                debugger
                $scope.data_assign_type = GetAssignmentTypes.data;
                if ($scope.data_assign_type[0].assignment_Type == 'Y') {
                    $scope.showAssignmentstudent = true;
                } else {
                    $scope.showAssignmentstudent = false;
                }
            });


            //$scope.show_grade_scale_cat_assign = false;
            //var edison=['eiam','eiama','eiand','eiagps']
            //$scope.Expand = true;

            $scope.fcommn = [];
            $scope.GradeBookAssignment = false;
            getdata();
            $scope.name = 'CREATION';
            var main = '';
            $scope.edt = {};
            $scope.edt['gb_status'] = true;
            var user = $rootScope.globals.currentUser.username;
            $scope.marks_updadated_by = $rootScope.globals.currentUser.username;
            var appl_code = "sim070";

            $scope.Expand = true;
            $scope.Expand5 = false;
            $scope.mark = {};
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#Table8").tableHeadFixer({ 'top': 1 });
                $("#Table7").tableHeadFixer({ 'top': 1 });
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
                $("#fixTable2").tableHeadFixer({ 'top': 1 });
                $("#Table2").tableHeadFixer({ 'top': 1 });
                $("#example2_info").tableHeadFixer({ 'top': 1 });
                $("#GradebookCommenttable").tableHeadFixer({ 'top': 1 });
                $("#example2_info1").tableHeadFixer({ 'top': 1 });
                $("#example2_info2").tableHeadFixer({ 'top': 1 });
                $("#Table4").tableHeadFixer({ 'top': 1 });
                $("#menu-wrapper1").scrollbar();
                $("#scroll-wrapper").css({ 'height': '600px' });
                $("#Table3").tableHeadFixer({ 'top': 1 });
                $("#Table6").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });
                $("#Table10").tableHeadFixer({ 'top': 1 });
                $("#Table11").tableHeadFixer({ 'top': 1 });
                $("#Table12").tableHeadFixer({ 'top': 1 });
                $("#Table13").tableHeadFixer({ 'top': 1 });
                $("#Table14").tableHeadFixer({ 'top': 1 });
                $("#Table9").tableHeadFixer({ 'top': 1 });
                $("#Table15").tableHeadFixer({ 'top': 1 });
                $("#Table17").tableHeadFixer({ 'top': 1 });
                $("#Table16").tableHeadFixer({ 'top': 1 });
                $("#Table18").tableHeadFixer({ 'top': 1 });
                $("#Table23").tableHeadFixer({ 'top': 1 });
                $("#Table24").tableHeadFixer({ 'top': 1 });
                $("#Table26").tableHeadFixer({ 'top': 1 });
                $("#Table24").tableHeadFixer({ 'top': 1 });
                $("#Table25").tableHeadFixer({ 'top': 1 });
                $("#Table27").tableHeadFixer({ 'top': 1 });


            }, 100);

            $http.get(ENV.apiUrl + "api/Gradebook_History/getGradeScaleflag").then(function (GradeScaleData) {
                $scope.show_scale = GradeScaleData.data;
                if ($scope.show_scale[0].sims_appl_parameter == '1') {
                    $scope.show_grade_scale_cat_assign = true; 
                }
            });

            /************************************************************** GradeBook Creation Code*******************************************************************/
            $scope.showbusyindi = true;
            $scope.propertyName = null;
            $scope.reverse = false;
            // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {
                debugger
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.focusedIndex = 0;
            $scope.handleKeyDown = function ($event,index) {
                debugger
                var keyCode = $event.keyCode;
                if (keyCode === 40) {
                    var int_index=0;

                    int_index = parseInt(index) + 1;

                    index = int_index;

                    $('#' + index + '-grade').focus();
                } else if (keyCode === 38) {
                    var int_index = 0;

                    int_index = parseInt(index) - 1;

                    index = int_index;

                    $('#' + index + '-grade').focus();
                }
            }
           

            if ($http.defaults.headers.common['schoolId'] == 'smfc') {
                $scope.show_sims_student_gender=true;
            }
            else {
                $scope.show_sims_student_gender = false;
            }

            setTimeout(function () {
                $("#rcbItem").select2();

            }, 100);

            //for collapse main menu
            $('body').addClass('grey condense-menu');
            $('#main-menu').addClass('mini');
            $('.page-content').addClass('condensed');

            $rootScope.isCondensed = true;
            $scope.GradeBookCreation = true;
            $scope.grdaebookname = true;

            var TeacherCodeList = [];

            var demo1 = [], main1 = '', t = '';
            var data = [];
            var TeacherCodeList = [];
            $scope.gardename = [];
            $scope.updateshow = false;
            $scope.fcommn = {};
            $scope.demo = [];
            $scope.Teacher_name = [];
            $scope.GradeSectionSubjectTeacher1 = [];
            $scope.GradeSectionSubjectTeacher = [];
            $scope.commn = {};

            $scope.getacyr = function (str) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.getAcademicYear = Academicyear.data;
                    $scope.commn['cur_code'] = $scope.curriculum[0].sims_cur_code;
                    $scope.commn['academic_year'] = $scope.getAcademicYear[0].sims_academic_year;
                    $scope.getsections($scope.curriculum[0].sims_cur_code, $scope.getAcademicYear[0].sims_academic_year);
                });
            }

            $scope.getsection = function (str, str1, str2) {
                $http.get(ENV.apiUrl + "api/Gradebook/GetRCSection?curcode=" + str + "&ayear=" + str1 + "&gradecode=" + str2 + "&teachercode=" + user).then(function (Allsection) {
                    $scope.getSectionFromGrade = Allsection.data;
                    $scope.mark.sims_section_code = ($scope.mark.sims_section_code == undefined || $scope.mark.sims_section_code == '') ? $scope.getSectionFromGrade[0].sims_section_code : $scope.mark.sims_section_code;
                    $scope.GetAllGradeSectionwiseGradebookNmae();
                })
            };

            $scope.getsections = function (str, str1) {

                $http.get(ENV.apiUrl + "api/Gradebook/GetRCGrade?curcode=" + str + "&ayear=" + str1 + "&teachercode=" + user).then(function (res) {
                    $scope.getAllGrades = res.data;
                    $scope.mark.sims_grade_code = ($scope.mark.sims_grade_code == undefined || $scope.mark.sims_grade_code == '') ? $scope.getAllGrades[0].sims_grade_code : $scope.mark.sims_grade_code;
                    $scope.getsection(str, str1, $scope.mark.sims_grade_code);

                })
            };

            //Getting default curriculum for academic year in GradeBook tab

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
                $scope.edt = {
                    cur_code: $scope.curriculum[0].sims_cur_code
                }
                $scope.fcommn['cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.Getyear($scope.edt.cur_code);
                $scope.Getyear1($scope.curriculum[0].sims_cur_code);
                $scope.Getyear2($scope.curriculum[0].sims_cur_code);
                $scope.getacyr($scope.curriculum[0].sims_cur_code);
            });

            $scope.Getyear = function (newval) {

                $scope.code = newval;

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + newval).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;

                    $scope.edt = {
                        academic_year: $scope.Academic_year[0].sims_academic_year
                    }

                    $scope.Getterm($scope.edt.academic_year);
                });
            }

            $scope.Getyear1 = function (newval) {

                $scope.code = newval;

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + newval).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    $scope.Getterm1($scope.Academic_year[0].sims_academic_year);
                });
            }

            $scope.Getterm1 = function (oldval) {

                $http.get(ENV.apiUrl + "api/Gradebook/getTermforGradebook?cur_code=" + $scope.code + "&academic_year=" + oldval).then(function (Getsims550_TermsAcademicYear) {
                    $scope.TermsAcademicYear = Getsims550_TermsAcademicYear.data;

                    $scope.getsections1($scope.code, oldval);

                });
            }

            $scope.getsection1 = function (str, str1, str2) {
                $http.get(ENV.apiUrl + "api/Gradebook/GetRCSection?curcode=" + str + "&ayear=" + str1 + "&gradecode=" + str2 + "&teachercode=" + user).then(function (Allsection) {
                    $scope.getSectionFromGrade = Allsection.data;

                    $scope.skipstd = {
                        cur_code: str,
                        academic_year: str1,
                        gb_term_code: $scope.TermsAcademicYear[0].sims_term_code,
                        sims_grade_code: str2,
                        sims_section_code: $scope.getSectionFromGrade[0].sims_section_code
                    }

                })
            };

            $scope.getsections1 = function (str, str1) {

                $http.get(ENV.apiUrl + "api/Gradebook/GetRCGrade?curcode=" + str + "&ayear=" + str1 + "&teachercode=" + user).then(function (res) {
                    $scope.getAllGrades = res.data;
                    $scope.getsection1(str, str1, $scope.getAllGrades[0].sims_grade_code);

                });
            };

            $scope.cat = {};
            $scope.copyg = {};
            $scope.upd = {};

            $scope.Getterm = function (oldval) {

                $http.get(ENV.apiUrl + "api/Gradebook/getTermforGradebook?cur_code=" + $scope.code + "&academic_year=" + oldval).then(function (Getsims550_TermsAcademicYear) {
                    $scope.TermsAcademicYear = Getsims550_TermsAcademicYear.data;
                    $scope.edt = {
                        cur_code: $scope.code,
                        academic_year: oldval,
                        gb_term_code: $scope.TermsAcademicYear[0].sims_term_code,
                        gb_status: true
                    };
                    $scope.cat['gb_term_code1'] = $scope.TermsAcademicYear[0].sims_term_code;
                    // $scope.GradeBookNames();
                    $scope.gradebooknameindi = true;
                    $scope.gradebooknameindi1 = true;
                    $scope.getsectionscopy();
                    $scope.gettermwisedates($scope.edt.gb_term_code);
                });


            }

            $scope.temp = {
                isIncludedInFinalGrade: true,
                status: true

            };

            $scope.Assign = {
                assignmentIncludedInFinalGrade: true,
            };

            $scope.showcaygroryli = function (info) {
                $scope.Expand = info;
            }

            //$http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
            //    $scope.ComboBoxValues = AllComboBoxValues.data;
            //});swal

            $http.get(ENV.apiUrl + "api/Gradebook/GetGradeBookTypes").then(function (GradeBookTypes) {

                $scope.Grade_Book_Types = GradeBookTypes.data;
            });

            $http.get(ENV.apiUrl + "api/Gradebook/GetScoreTypes").then(function (GetScoreTypes) {
                $scope.Get_Score_Types = GetScoreTypes.data;
            });

            $http.get(ENV.apiUrl + "api/Gradebook/getGradeScaleData").then(function (GradeScaleData) {
                $scope.Grade_Scale_Data = GradeScaleData.data;

            })
            $scope.wightage_error = '';
            $http.get(ENV.apiUrl + "api/Gradebook/getCheckedUser?user_code=" + user + "&co_scholastic=" + 'N').then(function (CheckUserData) {
                debugger;
                $scope.CheckUser_Data = CheckUserData.data;

                if ($scope.CheckUser_Data == true) {
                    $scope.adminuser = $scope.CheckUser_Data;
                    $scope.show_assignment_to_teacher = true;
                    if ($http.defaults.headers.common['schoolId'] == 'oes') {
                        $scope.showEffortTab = true;
                        $scope.show_assignment_to_teacher = true;
                    }
                }
                else {
                    $scope.adminuser = $scope.CheckUser_Data;
                    if ($http.defaults.headers.common['schoolId'] == 'ahis') {
                        $scope.show_assignment_to_teacher = true;

                       
                        //Aishwarya
                        $scope.asignment_teacher= function () {
                            debugger;
                            $scope.max_score = true;
                            $scope.extra_credit = true;
                            $scope.weightage = false;
                            $scope.check_box_assignment_teacher = true;
                            $scope.Assign['assignment_Extra_Creadit']=0;
                            $scope.Assign['assignment_Weightage']= 100;
                        }

                        $scope.assignment_ahis = function () {
                            debugger;
                            if ($http.defaults.headers.common['schoolId'] == 'ahis') {
                                $scope.Assign['assignmentMaxScore_correct'] = $scope.Assign.assignmentMaxScore;
                            }

                        }

                        $scope.check_weightage = function (str) {
                            debugger
                            if ($http.defaults.headers.common['schoolId'] == 'ahis') {
                                if (str == "30" || str == "70") {
                                    $scope.wightage_error = '';
                                } else {
                                    $scope.wightage_error = 'Please Keep Weightage Either 30 or 70';
                                }
                            }
                        }


                    }
                    $scope.GradebookMarksEntryShow('4');
              }

            })


          
           
            /***************************************************Start Tag  Show Code***************************************************************************/

            $scope.GradebookCreationShow = function (color) {
                  
                if ($scope.markentryflag1 == true || $scope.markentryflag2 == true) {

                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                } else if ($scope.markentryflag == true) {
                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    $scope.MarkEntryShow = true;
                }
                else {
                    $scope.oesfinalGradebookComment = false;
                    $scope.classwisecopygradebook = false;
                    $scope.teacher_term = false;
                    $scope.pearlfinalGradebookComment = false;
                    $scope.maincomment1 = false;
                    $scope.headcolor = color;
                    $scope.SkipStudentcategoryWiseshow1 = false;
                    $scope.SkipStudentcategoryWiseshow2 = false;
                    $scope.updategradebookrecord = false;
                    $scope.name = 'CREATION';
                    $scope.updateassign1 = 0;
                    $scope.studentwiseMarkEntryShow = false;
                    $scope.studentwiseMarkEntryShow1 = false;
                    $scope.studentwiseMarkEntryShow2 = false;
                    $scope.studentwiseMarkEntryShow3 = false;
                    $scope.SkipStudentFromAssignments = false;
                    $scope.mark = '';
                    $scope.GradeBookCreation = true;
                    $scope.GradeBookCategory = false;
                    $scope.GradeBookAssignment = false;
                    $scope.GradeSchemeShow = false;
                    $scope.MarkEntryShow = false;
                    $scope.GradebookComment = false;
                    $scope.grdaebookname = true;
                    $scope.SubjectAttribute = false;
                    $scope.updateshow = false;
                    $scope.editmode = false;
                    $scope.updateassign = false;
                    $scope.skipstudent = false;
                    $scope.GradeBookCopy = false;
                    $scope.finalGradebookComment = false;
                    $scope.SkipstudentSubjectWise1 = false;
                    $scope.SkipStudentFromSubjectAssignments = false;
                    $scope.shortfinalGradebookComment = false;
                    $scope.copycomment = false;
                    $scope.effortcopygradebook = false;
                    $scope.myForm5.$setPristine();
                    $scope.myForm5.$setUntouched();
                }

            }

            $scope.GradebookCategoryShow = function (color) {


                if ($scope.markentryflag1 == true || $scope.markentryflag2 == true) {

                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                } else if ($scope.markentryflag == true) {
                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    $scope.MarkEntryShow = true;
                }
                else {
                    $scope.classwisecopygradebook = false;
                    $scope.oesfinalGradebookComment = false;
                    $scope.pearlfinalGradebookComment = false;
                    $scope.maincomment1 = false;
                    $scope.headcolor = color;
                    $scope.SkipStudentcategoryWiseshow1 = false;
                    $scope.SkipStudentcategoryWiseshow2 = false;
                    $scope.updategradebookrecord = false;
                    $scope.name = 'CATEGORY';
                    $scope.updateassign1 = 0;
                    $scope.studentwiseMarkEntryShow = false;
                    $scope.studentwiseMarkEntryShow2 = false;
                    $scope.studentwiseMarkEntryShow3 = false;
                    $scope.SkipStudentFromAssignments = false;
                    $scope.mark = '';
                    $scope.studentwiseMarkEntryShow1 = false;
                    $scope.GradeBookCreation = false;
                    $scope.GradeBookCategory = true;
                    $scope.GradeBookAssignment = false;
                    $scope.GradeSchemeShow = false;
                    $scope.MarkEntryShow = false;
                    $scope.GradebookComment = false;
                    $scope.grdaebookname = true;
                    $scope.SubjectAttribute = false;
                    $scope.updateshow = false;
                    $scope.editmode = false;
                    $scope.updateassign = false;
                    $scope.skipstudent = false;
                    $scope.GradeBookCopy = false;
                    $scope.finalGradebookComment = false;
                    $scope.copycomment = false;
                    $scope.shortfinalGradebookComment = false;

                    $scope.SkipstudentSubjectWise1 = false;
                    $scope.SkipStudentFromSubjectAssignments = false;
                    $scope.effortcopygradebook = false;
                    $scope.myForm5.$setPristine();
                    $scope.myForm5.$setUntouched();
                }
            }

            $scope.GradebookAssignmentShow = function (color) {

                if ($scope.markentryflag1 == true || $scope.markentryflag2 == true) {

                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                } else if ($scope.markentryflag == true) {
                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    $scope.MarkEntryShow = true;
                }
                else {

                    $scope.classwisecopygradebook = false;
                    $scope.oesfinalGradebookComment = false;
                    $scope.maincomment1 = false;
                    $scope.pearlfinalGradebookComment = false;
                    $scope.headcolor = color;
                    $scope.SkipStudentcategoryWiseshow1 = false;
                    $scope.SkipStudentcategoryWiseshow2 = false;
                    $scope.updategradebookrecord = false;
                    $scope.studentwiseMarkEntryShow = false;
                    $scope.updateassign1 = 0;
                    $scope.studentwiseMarkEntryShow2 = false;
                    $scope.studentwiseMarkEntryShow3 = false;
                    $scope.SkipStudentFromAssignments = false;
                    $scope.mark = '';
                    $scope.studentwiseMarkEntryShow1 = false;
                    $scope.grdaebookname = true;
                    $scope.name = 'ASSIGNMENT';
                    $scope.MarkEntryShow = false;
                    $scope.GradeBookCreation = false;
                    $scope.GradeBookCategory = false;
                    $scope.GradeBookAssignment = true;
                    $scope.GradeSchemeShow = false;
                    $scope.GradebookComment = false;
                    $scope.SubjectAttribute = false;
                    $scope.updateshow = false;
                    $scope.editmode = false;
                    $scope.updateassign = false;
                    $scope.skipstudent = false;
                    $scope.GradeBookCopy = false;
                    $scope.SkipstudentSubjectWise1 = false;
                    $scope.finalGradebookComment = false;
                    $scope.copycomment = false;
                    $scope.shortfinalGradebookComment = false;
                    $scope.effortcopygradebook = false;

                    $scope.CancelGradeBooAssignmentData();

                }
            }

            $scope.GradebookGradingSchemeShow = function (color) {


                if ($scope.markentryflag1 == true || $scope.markentryflag2 == true) {

                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                } else if ($scope.markentryflag == true) {
                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    $scope.MarkEntryShow = true;
                }
                else {
                    $scope.oesfinalGradebookComment = false;
                    $scope.maincomment1 = false;
                    $scope.pearlfinalGradebookComment = false;
                    $scope.classwisecopygradebook = false;
                    $scope.headcolor = color;
                    $scope.updategradebookrecord = false;
                    $scope.studentwiseMarkEntryShow1 = false;
                    $scope.SkipStudentFromAssignments = false;
                    $scope.updateassign1 = 0;
                    $scope.myForm5.$setPristine();
                    $scope.myForm5.$setUntouched();
                    $scope.MarkEntryShow = false;
                    $scope.studentwiseMarkEntryShow = false;
                    $scope.studentwiseMarkEntryShow2 = false;
                    $scope.studentwiseMarkEntryShow3 = false;
                    $scope.mark = '';
                    $scope.GradeBookCreation = false;
                    $scope.GradeBookCategory = false;
                    $scope.GradeBookAssignment = false;
                    $scope.GradeSchemeShow = true;
                    $scope.GradebookComment = false;
                    $scope.grdaebookname = false;
                    $scope.SubjectAttribute = false;
                    $scope.updateshow = false;
                    $scope.editmode = false;
                    $scope.skipstudent = false;
                    $scope.GradeBookCopy = false;
                    $scope.finalGradebookComment = false;
                    $scope.copycomment = false;
                    $scope.shortfinalGradebookComment = false;
                    $scope.SkipStudentcategoryWiseshow1 = false;
                    $scope.SkipStudentcategoryWiseshow2 = false;
                    $scope.SkipstudentSubjectWise1 = false;
                    $scope.SkipStudentFromSubjectAssignments = false;
                    $scope.effortcopygradebook = false;
                    $scope.gardename = [];
                    var data = [];
                    $http.get(ENV.apiUrl + "api/Gradebook/getGradeScaleData").then(function (GradeScaleData) {
                        $scope.Grade_Scale_Data = GradeScaleData.data;
                        getdata();
                    })
                }
            }

            $scope.GradebookMarksEntryShow = function (color) {

                if ($scope.markentryflag1 == true) {

                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                } else if ($scope.markentryflag2 == true) {
                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                } else {
                    $scope.oesfinalGradebookComment = false;
                    $scope.pearlfinalGradebookComment = false;
                    $scope.grading_status = false;
                    $scope.teacher_term = true;
                    $scope.teacher_preview = true;
                    $scope.headcolor = color;
                    $scope.maincomment1 = false;
                    $scope.updategradebookrecord = false;
                    $scope.GradeBookCreation = false;
                    $scope.updateassign1 = 0;
                    $scope.SkipStudentFromAssignments = false;
                    $scope.studentwiseMarkEntryShow1 = false;
                    $scope.studentwiseMarkEntryShow = false;
                    $scope.studentwiseMarkEntryShow2 = false;
                    $scope.studentwiseMarkEntryShow3 = false;
                    $scope.mark = '';
                    $scope.GradeBookCategory = false;
                    $scope.GradeBookAssignment = false;
                    $scope.GradeSchemeShow = false;
                    $scope.MarkEntryShow = true;
                    $scope.GradebookComment = false;
                    $scope.grdaebookname = true;
                    $scope.SubjectAttribute = false;
                    $scope.updateshow = false;
                    $scope.editmode = false;
                    $scope.updateassign = false;
                    $scope.skipstudent = false;
                    $scope.GradeBookCopy = false;
                    $scope.SkipstudentSubjectWise1 = false;
                    $scope.finalGradebookComment = false;
                    $scope.copycomment = false;
                    $scope.shortfinalGradebookComment = false;
                    $scope.SkipStudentcategoryWiseshow1 = false;
                    $scope.SkipStudentcategoryWiseshow2 = false;
                    $scope.SkipStudentFromSubjectAssignments = false;
                    $scope.classwisecopygradebook = false;
                    $scope.effortcopygradebook = false;

                    $scope.myForm5.$setPristine();
                    $scope.myForm5.$setUntouched();


                    $http.get(ENV.apiUrl + "api/Gradebook/GetadminuserType?username=" + user + "&appl_code=" + appl_code).then(function (getAdmin) {
                        $scope.getadmin = getAdmin.data;

                    });

                }

            }

            $scope.GradebookCommentShow = function (color) {

                if ($scope.markentryflag == true) {
                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    $scope.MarkEntryShow = true;
                }
                else {
                    $scope.pearlfinalGradebookComment = false;
                    $scope.classwisecopygradebook = false;
                    $scope.oesfinalGradebookComment = false;
                    $scope.maincomment1 = false;
                    $scope.headcolor = color;
                    $scope.updategradebookrecord = false;
                    $scope.studentwiseMarkEntryShow = false;
                    $scope.studentwiseMarkEntryShow1 = false;
                    $scope.updateassign1 = 0;
                    $scope.studentwiseMarkEntryShow2 = false;
                    $scope.studentwiseMarkEntryShow3 = false;
                    $scope.SkipStudentFromAssignments = false;
                    $scope.mark = '';
                    $scope.GradeBookCreation = false;
                    $scope.GradeBookCategory = false;
                    $scope.GradeBookAssignment = false;
                    $scope.GradeSchemeShow = false;
                    $scope.MarkEntryShow = false;
                    $scope.GradebookComment = true;
                    $scope.grdaebookname = false;
                    $scope.SubjectAttribute = false;
                    $scope.updateshow = false;
                    $scope.editmode = false;
                    $scope.updateassign = false;
                    $scope.skipstudent = false;
                    $scope.GradeBookCopy = false;
                    $scope.SkipstudentSubjectWise1 = false;
                    $scope.SkipStudentFromSubjectAssignments = false;
                    $scope.finalGradebookComment = false;
                    $scope.copycomment = false;
                    $scope.shortfinalGradebookComment = false;
                    $scope.SkipStudentcategoryWiseshow1 = false;
                    $scope.SkipStudentcategoryWiseshow2 = false;
                    $scope.effortcopygradebook = false;
                    $scope.commn = {
                        cur_code: $scope.curriculum[0].sims_cur_code,
                        academic_year: $scope.Academic_year[0].sims_academic_year,

                    };
                    $scope.myForm5.$setPristine();
                    $scope.myForm5.$setUntouched();


                }
            }

            $scope.GradebookSubjectAttributeShow = function (color) {
                $scope.pearlfinalGradebookComment = false;

                if ($scope.markentryflag1 == true || $scope.markentryflag2 == true) {

                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                } else if ($scope.markentryflag == true) {
                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    $scope.MarkEntryShow = true;
                }
                else {
                    $scope.classwisecopygradebook = false;
                    $scope.oesfinalGradebookComment = false;
                    $scope.maincomment1 = false;
                    $scope.headcolor = color;
                    $scope.updategradebookrecord = false;
                    $scope.studentwiseMarkEntryShow = false;
                    $scope.studentwiseMarkEntryShow1 = false;
                    $scope.updateassign1 = 0;
                    $scope.studentwiseMarkEntryShow2 = false;
                    $scope.studentwiseMarkEntryShow3 = false;
                    $scope.SkipStudentFromAssignments = false;
                    $scope.mark = '';
                    $scope.GradeBookCreation = false;
                    $scope.GradeBookCategory = false;
                    $scope.GradeBookAssignment = false;
                    $scope.GradeSchemeShow = false;
                    $scope.MarkEntryShow = false;
                    $scope.GradebookComment = false;
                    $scope.grdaebookname = false;
                    $scope.SubjectAttribute = true;
                    $scope.updateshow = false;
                    $scope.editmode = false;
                    $scope.updateassign = false;
                    $scope.skipstudent = false;
                    $scope.GradeBookCopy = false;
                    $scope.SkipstudentSubjectWise1 = false;
                    $scope.SkipStudentFromSubjectAssignments = false;
                    $scope.finalGradebookComment = false;
                    $scope.copycomment = false;
                    $scope.shortfinalGradebookComment = false;
                    $scope.SkipStudentcategoryWiseshow1 = false;
                    $scope.SkipStudentcategoryWiseshow2 = false;
                    $scope.effortcopygradebook = false;
                    $scope.myForm5.$setPristine();
                    $scope.myForm5.$setUntouched();
                }
            }

            $scope.GradebookCopyShow = function (color) {
                $scope.pearlfinalGradebookComment = false;

                if ($scope.markentryflag1 == true || $scope.markentryflag2 == true) {

                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                } else if ($scope.markentryflag == true) {
                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    $scope.MarkEntryShow = true;
                }
                else {
                    $scope.oesfinalGradebookComment = false;
                    $scope.maincomment1 = false;
                    $scope.classwisecopygradebook = false;
                    $scope.maincopy = true;
                    $scope.headcolor = color;
                    $scope.finalGradebookComment = false;
                    $scope.copycomment = false;
                    $scope.shortfinalGradebookComment = false;
                    $scope.SkipStudentcategoryWiseshow1 = false;
                    $scope.SkipStudentcategoryWiseshow2 = false;
                    $scope.updategradebookrecord = false;
                    $scope.updateassign1 = 0;
                    $scope.studentwiseMarkEntryShow = false;
                    $scope.studentwiseMarkEntryShow1 = false;
                    $scope.studentwiseMarkEntryShow2 = false;
                    $scope.studentwiseMarkEntryShow3 = false;
                    $scope.SkipStudentFromAssignments = false;
                    $scope.mark = '';
                    $scope.GradeBookCreation = false;
                    $scope.GradeBookCategory = false;
                    $scope.GradeBookAssignment = false;
                    $scope.GradeSchemeShow = false;
                    $scope.MarkEntryShow = false;
                    $scope.GradebookComment = false;
                    $scope.grdaebookname = false;
                    $scope.SubjectAttribute = false;
                    $scope.updateshow = false;
                    $scope.editmode = false;
                    $scope.updateassign = false;
                    $scope.skipstudent = false;
                    $scope.GradeBookCopy = true;
                    $scope.myForm5.$setPristine();
                    $scope.myForm5.$setUntouched();
                    $scope.copygradebook = true;
                    $scope.GradeBookcategory = false;
                    $scope.GradeBookAssignments = false;
                    $scope.curriculum1 = $scope.curriculum;
                    $scope.Academic_year1 = $scope.Academic_year;
                    $scope.TermsAcademicYear1 = $scope.TermsAcademicYear;
                    $scope.getGradeSectionSubjectNameorCode = [];
                    $scope.categoryobject = [];
                    $scope.AllCateories_obj1 = [];
                    $scope.Term_wiseGetGradeBooks = [];
                    $scope.GradeBookName_For_Copy = [];
                    $scope.SkipstudentSubjectWise1 = false;
                    $scope.SkipStudentFromSubjectAssignments = false;
                    $scope.effortcopygradebook = false;
                    $scope.copyg = {
                        cur_code: $scope.curriculum1[0].sims_cur_code,
                        academic_year: $scope.Academic_year1[0].sims_academic_year,
                        gb_term_code: $scope.TermsAcademicYear1[0].sims_term_code,
                        gb_term_code1: $scope.TermsAcademicYear1[0].sims_term_code
                    }
                    $scope.getsectionscopy();
                }
            }

            $scope.gettermwisedates = function () {
                var data = ({
                    sims_cur_code: $scope.edt.cur_code,
                    sims_academic_year: $scope.edt.academic_year,
                    sims_teacher_code: user,
                    sims_gb_term_name: $scope.edt.gb_term_code,

                });

                $http.post(ENV.apiUrl + "api/Gradebook/All_GetTerms", data).then(function (termdates) {
                    $scope.term_dates = termdates.data;
                    $scope.edt.termStartDate = $scope.term_dates[0].termStartDate;
                    $scope.edt.termEndDate = $scope.term_dates[0].termEndDate;
                });

            }

            $scope.GradeBookNames = function () {

                if ($scope.markentryflag1 == true || $scope.markentryflag2 == true || $scope.markentryflag == true) {

                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                } else {
                    $scope.oesfinalGradebookComment = false;
                    $scope.pearlfinalGradebookComment = false;
                    $scope.markentryflag = false;
                    $scope.copycomment = false;
                    $scope.maincomment1 = false;
                    $scope.shortfinalGradebookComment = false;
                    $scope.SkipStudentcategoryWiseshow1 = false;
                    $scope.SkipStudentcategoryWiseshow2 = false;
                    $scope.finalGradebookComment = false;
                    $scope.updategradebookrecord = false;
                    $scope.updateassign1 = 0;
                    $scope.myForm5.$setPristine();
                    $scope.myForm5.$setUntouched();
                    $scope.newinsert = true;
                    $scope.studentwiseMarkEntryShow2 = false;
                    $scope.studentwiseMarkEntryShow3 = false;
                    $scope.classwisecopygradebook = false;

                    $scope.mark = '';
                    $scope.studentwiseMarkEntryShow = false;
                    $scope.studentwiseMarkEntryShow1 = false;
                    $scope.updateassign = false;
                    $scope.Assign = {
                        assignmentIncludedInFinalGrade: true,
                    };
                    $scope.temp = {
                        isIncludedInFinalGrade: true,
                        status: true
                    };
                    $scope.updateshow = false;
                    $scope.demo = [];
                    $scope.edt['gradebookname'] = '';
                    var data = ({
                        sims_cur_code: $scope.edt.cur_code,
                        sims_academic_year: $scope.edt.academic_year,
                        sims_teacher_code: user,
                        sims_gb_term_name: $scope.edt.gb_term_code,

                    });

                    $scope.data1 = {
                        cur_code: $scope.edt.cur_code,
                        academic_year: $scope.edt.academic_year
                    }

                    $scope.gradebooknameindi = false;
                    $scope.gradebooknameindi1 = false;
                    $scope.effortcopygradebook = false;

                    $http.post(ENV.apiUrl + "api/Gradebook/AllGradeBooks", data).then(function (All_GradeBooks) {
                        $scope.All_Grade_Books_names = All_GradeBooks.data;
                        $scope.gradebooknameindi = true;
                    });

                    $http.post(ENV.apiUrl + "api/Gradebook/All_GetTerms", data).then(function (termdates) {
                        $scope.term_dates = termdates.data;
                        $scope.edt.termStartDate = $scope.term_dates[0].termStartDate;
                        $scope.edt.termEndDate = $scope.term_dates[0].termEndDate;
                    });

                    $http.post(ENV.apiUrl + "api/Gradebook/CGradeSectionSubjectNameorCode", $scope.data1).then(function (getGradeSectionSubjectNameorCode) {
                        $scope.getGradeSectionSubjectNameorCode1 = getGradeSectionSubjectNameorCode.data;
                        $scope.gradebooknameindi1 = true;
                    });
                }
            }

            $scope.UpdateGradeBookcategoryassignmentshow = function (color) {


                if ($scope.markentryflag1 == true || $scope.markentryflag2 == true) {

                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                } else if ($scope.markentryflag == true) {
                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    $scope.MarkEntryShow = true;
                }
                else {
                    $scope.oesfinalGradebookComment = false;
                    $scope.pearlfinalGradebookComment = false;
                    $scope.headcolor = color;
                    $scope.upd = {
                        cur_code: $scope.edt.cur_code,
                        academic_year: $scope.edt.academic_year,
                        gb_term_code: $scope.edt.gb_term_code,

                    };
                    $scope.maincomment1 = false;
                    $scope.copycomment = false;
                    $scope.shortfinalGradebookComment = false;
                    $scope.SkipStudentcategoryWiseshow1 = false;
                    $scope.SkipStudentcategoryWiseshow2 = false;
                    $scope.studentwiseMarkEntryShow = false;
                    $scope.studentwiseMarkEntryShow1 = false;
                    $scope.studentwiseMarkEntryShow2 = false;
                    $scope.studentwiseMarkEntryShow3 = false;
                    $scope.SkipStudentFromAssignments = false;
                    $scope.mark = '';
                    $scope.GradeBookCreation = false;
                    $scope.GradeBookCategory = false;
                    $scope.GradeBookAssignment = false;
                    $scope.GradeSchemeShow = false;
                    $scope.MarkEntryShow = false;
                    $scope.GradebookComment = false;
                    $scope.grdaebookname = false;
                    $scope.SubjectAttribute = false;
                    $scope.updateshow = false;
                    $scope.editmode = false;
                    $scope.updateassign = false;
                    $scope.skipstudent = false;
                    $scope.GradeBookCopy = false;
                    $scope.updategradebookrecord = true;
                    $scope.finalGradebookComment = false;
                    $scope.classwisecopygradebook = false;
                    $scope.effortcopygradebook = false;

                    $http.get(ENV.apiUrl + "api/Gradebook/GetTableNames").then(function (GetTable_Names) {
                        $scope.Tablec_Names = GetTable_Names.data;
                    });


                }
            }


            $scope.effortCopyToGradebookshow = function (color) {


                if ($scope.markentryflag1 == true || $scope.markentryflag2 == true) {

                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                } else if ($scope.markentryflag == true) {
                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    $scope.MarkEntryShow = true;
                }
                else {
                    $scope.oesfinalGradebookComment = false;
                    $scope.pearlfinalGradebookComment = false;
                    $scope.headcolor = color;
                    $scope.upd = {
                        cur_code: $scope.edt.cur_code,
                        academic_year: $scope.edt.academic_year,
                        gb_term_code: $scope.edt.gb_term_code,

                    };
                    $scope.maincomment1 = false;
                    $scope.copycomment = false;
                    $scope.shortfinalGradebookComment = false;
                    $scope.SkipStudentcategoryWiseshow1 = false;
                    $scope.SkipStudentcategoryWiseshow2 = false;
                    $scope.studentwiseMarkEntryShow = false;
                    $scope.studentwiseMarkEntryShow1 = false;
                    $scope.studentwiseMarkEntryShow2 = false;
                    $scope.studentwiseMarkEntryShow3 = false;
                    $scope.SkipStudentFromAssignments = false;
                    $scope.mark = '';
                    $scope.GradeBookCreation = false;
                    $scope.GradeBookCategory = false;
                    $scope.GradeBookAssignment = false;
                    $scope.GradeSchemeShow = false;
                    $scope.MarkEntryShow = false;
                    $scope.GradebookComment = false;
                    $scope.grdaebookname = false;
                    $scope.SubjectAttribute = false;
                    $scope.updateshow = false;
                    $scope.editmode = false;
                    $scope.updateassign = false;
                    $scope.skipstudent = false;
                    $scope.GradeBookCopy = false;
                    $scope.updategradebookrecord = false;
                    $scope.finalGradebookComment = false;
                    $scope.classwisecopygradebook = false;
                    $scope.effortcopygradebook = true;
                    $scope.effortflag = false;

                }
            }

            $scope.commentchange = function (str) {


                var report1 = {
                    sims_grade_code: $scope.fcommn.sims_grade_code,
                    sims_academic_year: $scope.fcommn.academic_year,
                    sims_section_code: $scope.fcommn.sims_section_code,
                    sims_employee_code: user
                };

                if (str == 'S') {
                    $scope.subject_type = false;
                    $http.post(ENV.apiUrl + "api/Gradebook/AllGradeSection_subject", report1).then(function (allSection_Subject) {
                        $scope.subject_namesforcomment = allSection_Subject.data;
                        $scope.fcommn['sims_subject_code'] = (($scope.fcommn.sims_subject_code == '' || $scope.fcommn.sims_subject_code == undefined) && $scope.subject_namesforcomment.length > 0) ? $scope.subject_namesforcomment[0].sims_subject_code : $scope.fcommn.sims_subject_code;
                    });
                }
                else {
                    $scope.subject_type = true;
                    $scope.fcommn.sims_subject_code = '';
                }

            }

            $scope.GradebookFinalCommentShow = function (color) {

                if ($scope.markentryflag1 == true || $scope.markentryflag2 == true) {

                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                } else if ($scope.markentryflag == true) {
                    swal({ text: 'Mark(s) are not submitted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    $scope.MarkEntryShow = true;
                }
                else {
                    $scope.subject_type = true;
                    $scope.shortfinalGradebookComment = true;
                    $scope.headcolor = color;
                    $scope.fcommn = [];
                    $scope.GetRCStudentForComments = [];
                    $scope.GetRCCommentsforStudent = [];
                    $scope.maincomment = true;
                    $scope.maincomment1 = false;
                    $scope.copycomment = false;
                    $scope.studentwiseMarkEntryShow = false;
                    $scope.studentwiseMarkEntryShow1 = false;
                    $scope.studentwiseMarkEntryShow2 = false;
                    $scope.studentwiseMarkEntryShow3 = false;
                    $scope.SkipStudentFromAssignments = false;
                    $scope.mark = '';
                    $scope.GradeBookCreation = false;
                    $scope.GradeBookCategory = false;
                    $scope.GradeBookAssignment = false;
                    $scope.GradeSchemeShow = false;
                    $scope.MarkEntryShow = false;
                    $scope.GradebookComment = false;
                    $scope.grdaebookname = false;
                    $scope.SubjectAttribute = false;
                    $scope.updateshow = false;
                    $scope.editmode = false;
                    $scope.updateassign = false;
                    $scope.skipstudent = false;
                    $scope.GradeBookCopy = false;
                    $scope.updategradebookrecord = false;
                    $scope.SkipStudentcategoryWiseshow1 = false;
                    $scope.SkipStudentcategoryWiseshow2 = false;
                    $scope.copycomment = false;
                    $scope.classwisecopygradebook = false;
                    $scope.effortcopygradebook = false;

                    //if ($http.defaults.headers.common['schoolId'] != 'oes' || $http.defaults.headers.common['schoolId'] == 'brs' || $http.defaults.headers.common['schoolId'] == 'zps' || $http.defaults.headers.common['schoolId'] == 'aji')
                    //{
                    //    $scope.sisqatarcomment = true;
                    //} else
                    //{
                    //    $scope.sisqatarcomment = false;
                    //}

                    if ($http.defaults.headers.common['schoolId'] != 'brs') {
                        $scope.oesfinalGradebookComment = false;
                        $scope.finalGradebookComment = true;
                        $scope.shortfinalGradebookComment = false;

                        $scope.comment_btn = true;
                        $scope.comment_btn1 = false;
                        $http.get(ENV.apiUrl + "api/Gradebook/getCommenttype").then(function (Comment_type) {
                            $scope.get_Comment_type = Comment_type.data;

                        });

                        $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                            $scope.curriculum = res.data;
                            $scope.fcommn['cur_code'] = $scope.curriculum[0].sims_cur_code;
                            $scope.getacyr1($scope.curriculum[0].sims_cur_code);
                        });
                    }
                    else {
                        $scope.comment_btn = false;
                        $scope.finalGradebookComment = false;
                        $scope.shortfinalGradebookComment = true;
                        $scope.comment_btn = true;
                        $scope.comment_btn1 = false;
                        $scope.oesfinalGradebookComment = false;
                        $scope.maincomment2 = false;
                        $scope.shortfinalGradebookComment = false;
                    }

                    if ($http.defaults.headers.common['schoolId'] == 'oes') {
                        $scope.comment_btn = false;
                        $scope.comment_btn1 = true;
                        $scope.oesfinalGradebookComment = true;
                        $scope.maincomment1 = false;
                        $scope.maincomment = false;
                        $scope.maincomment2 = true;
                        $scope.shortfinalGradebookComment = false;
                    }

                    if ($http.defaults.headers.common['schoolId'] == 'pearl' || $http.defaults.headers.common['schoolId'] == 'deps' || $http.defaults.headers.common['schoolId'] == 'tsk' || $http.defaults.headers.common['schoolId'] == 'tsis' || $http.defaults.headers.common['schoolId'] == 'abps' || $http.defaults.headers.common['schoolId'] == 'smfc' || $http.defaults.headers.common['schoolId'] == 'asdportal') {
                        $scope.comment_btn = false;
                        $scope.comment_btn1 = false;
                        $scope.pearlfinalGradebookComment = true;
                        $scope.finalGradebookComment = false;
                        $scope.shortfinalGradebookComment = false;
                        $scope.maincomment1 = true;
                        $scope.maincomment = false;
                        $scope.oesfinalGradebookComment = false;
                        $scope.maincomment2 = false;
                    }

                    if ($http.defaults.headers.common['schoolId'] == 'siso') {
                        $scope.comment_btn = false;
                        $scope.comment_btn1 = false;
                        $scope.pearlfinalGradebookComment = false;
                        $scope.finalGradebookComment = false;
                        $scope.shortfinalGradebookComment = false;
                        $scope.GradebookComment = true;
                        $scope.maincomment1 = false;
                        $scope.maincomment = false;
                        $scope.oesfinalGradebookComment = false;
                        $scope.maincomment2 = false;
                    }
                }
            }

            $scope.Active_Inactive_Gradebooks = function () {
                $scope.deleteassign = false;
                var dataobject = [];
                for (var i = 0; i < $scope.All_Grade_Books_names.length; i++) {
                    if ($scope.All_Grade_Books_names[i].ischecked == true) {
                        $scope.deleteassign = true;
                        dataobject.push($scope.All_Grade_Books_names[i]);
                    }
                }
                if ($scope.deleteassign == true) {
                    swal({
                        text: 'Are You Sure? You Want To Delete Category',
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/Gradebook/ActiveInactiveGradebooks", dataobject).then(function (message) {
                                $scope.msgg = message.data;
                                if ($scope.msgg == true) {

                                    swal({ text: 'Gradebook Deleted Successfully',imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                                    $scope.GradeBookNames();
                                }
                                else {
                                    swal({ text: 'Gradebook Not Deleted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                                }

                            });

                        }
                    })
                }

                else {

                    swal({
                        text: 'Select Atleast One Gradebook To Delete',
                        imageUrl: "assets/img/notification-alert.png",
                        width: 300,
                        showCloseButton: true
                    });
                }

            }

            $scope.SkipStudentcategoryWiseshow = function () {

                if ($scope.markentryflag1 == true || $scope.markentryflag2 == true) {

                    swal({ text: 'Mark(s) are not submitted',imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                } else if ($scope.markentryflag == true) {
                    swal({ text: 'Mark(s) are not submitted',imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    $scope.MarkEntryShow = true;
                }
                else {

                    $scope.oesfinalGradebookComment = false;
                    $scope.shortfinalGradebookComment = false;
                    $scope.pearlfinalGradebookComment = true;
                    $scope.maincomment = false;
                    $scope.copycomment = false;
                    $scope.studentwiseMarkEntryShow = false;
                    $scope.studentwiseMarkEntryShow1 = false;
                    $scope.studentwiseMarkEntryShow2 = false;
                    $scope.studentwiseMarkEntryShow3 = false;
                    $scope.SkipStudentFromAssignments = false;
                    $scope.mark = '';
                    $scope.GradeBookCreation = false;
                    $scope.GradeBookCategory = false;
                    $scope.GradeBookAssignment = false;
                    $scope.GradeSchemeShow = false;
                    $scope.MarkEntryShow = false;
                    $scope.GradebookComment = false;
                    $scope.grdaebookname = false;
                    $scope.SubjectAttribute = false;
                    $scope.updateshow = false;
                    $scope.editmode = false;
                    $scope.updateassign = false;
                    $scope.skipstudent = false;
                    $scope.GradeBookCopy = false;
                    $scope.updategradebookrecord = false;
                    $scope.copycomment = false;
                    $scope.SkipStudentcategoryWiseshow1 = true;
                    $scope.SkipStudentcategoryWiseshow2 = false;
                    $scope.classwisecopygradebook = false;
                    $scope.effortcopygradebook = false;
                }
            }

            /*************************************************** END Tag Show Code***************************************************************************/

            $scope.setgradebookfreeze = function (str) {
                if (str == true) {
                    $scope.gb_freeze_status = false;
                }
                else {
                    $scope.gb_freeze_status = false;
                }
            }

            function getdata() {
                $http.post(ENV.apiUrl + "api/Gradebook/GradeScaleDataOpr?teachercode=" + user + "&opr=SC").then(function (All_Grade) {
                    $scope.All_SCALE_GRADE = All_Grade.data;

                });
            }

            $scope.GetSubjectTeacher = function (str) {
                debugger
                //$scope.grade_code1 = '';
                //$scope.section_code1 = '';
                //var grade_name = str.gradebookname.split('/')[0];
                //var section_code = str.gradebookname.split('/')[1];
                //var section_name = section_code.split('-')[0];
                $scope.Teacher_name = [];

                //for (var i = 0; i < $scope.Book_Name_Creation.length; i++) {
                //    if (str.sims_subject_code == $scope.Book_Name_Creation[i].sims_subject_code) {
                //        if ($scope.Book_Name_Creation[i].sims_grade_name_en == str.sims_grade_name_en.trim() && $scope.Book_Name_Creation[i].sims_section_name_en == str.sims_section_name_en.trim()) {
                //            $scope.section_code1 = $scope.Book_Name_Creation[i].sims_section_code;
                //            $scope.grade_code1 = $scope.Book_Name_Creation[i].sims_grade_code;
                //            $scope.Book_Name_Creation[i].status = true;
                //            $scope.Teacher_name.push($scope.Book_Name_Creation[i]);
                //        }
                //        else {
                //            $scope.Book_Name_Creation[i].status = false;
                //            $scope.Teacher_name.push($scope.Book_Name_Creation[i]);
                //        }
                //    }
                //}


                $scope.table = true;

                $scope.GradeSectionSubjectTeacher = [];

                if ($scope.edt.gb_type == "S") {

                    $scope.subject = true;
                    $scope.homeroom = false;
                    //for (var i = 0; i < $scope.Book_Name_Creation.length; i++) {

                    //    if (str.sims_subject_code == $scope.Book_Name_Creation[i].sims_subject_code && $scope.Book_Name_Creation[i].sims_grade_name_en == grade_name.trim() && $scope.Book_Name_Creation[i].sims_section_name_en == section_name.trim()) {

                    //        $scope.Book_Name_Creation[i].status = true;
                    //        $scope.Teacher_name.push($scope.Book_Name_Creation[i]);

                    //    }
                    //    else {
                    //        $scope.Book_Name_Creation[i].status = false;
                    //        $scope.Teacher_name.push($scope.Book_Name_Creation[i]);
                    //    }
                    //}

                    $scope.GradeSectionSubjectTeacher2 = [];

                    $http.get(ENV.apiUrl + "api/Gradebook/GetGradeSectionSubjectTeacher?cur_code=" + $scope.edt.cur_code + "&academic_year=" + $scope.edt.academic_year + "&subject_code=" + str.gradebookname).then(function (GetGradeSectionSubjectTeacher) {
                        $scope.GradeSectionSubjectTeacher = GetGradeSectionSubjectTeacher.data;

                    });

                }
                else {
                    var homeroom_code = '';
                    $scope.subject = false;
                    $scope.homeroom = true;
                    $scope.Teacher_name = [];
                    var values = str.gradebookname.split("/");
                    var number = '';
                    var gradename = values[0].trim();
                    var value2 = values[1];
                    var value3 = value2.split("-");
                    var sectionname = value3[0].trim();
                    var homeroomname = value3[1].trim();
                    var termname = value3[2].trim();

                    for (var i = 0; i < $scope.Book_Name_Creation.length; i++) {
                        if (homeroomname == $scope.Book_Name_Creation[i].sims_homeroom_name) {
                            $scope.Teacher_name.push($scope.Book_Name_Creation[i]);
                            homeroom_code = $scope.Book_Name_Creation[i].sims_homeroom_code;
                        }
                    }

                    $http.get(ENV.apiUrl + "api/Gradebook/GetGradeSectionHomeroomTeacher?cur_code=" + $scope.edt.cur_code + "&academic_year=" + $scope.edt.academic_year + "&homeroom_code=" + homeroom_code).then(function (GetGradeSectionSubjectTeacher) {
                        $scope.GradeSectionSubjectTeacher1 = GetGradeSectionSubjectTeacher.data;

                        if ($scope.GradeSectionSubjectTeacher1.length > 0) {
                            $scope.GradeSectionSubjectTeacher = $scope.GradeSectionSubjectTeacher1;

                        }

                    });
                }

            }

            $scope.TeacherCheckAllChecked = function () {

                if ($scope.edt.gb_type == "S") {
                    main = document.getElementById('mainchk1');
                    for (var i = 0; i < $scope.GradeSectionSubjectTeacher.length; i++) {
                        var t = $scope.GradeSectionSubjectTeacher[i].sims_teacher_code;
                        var v = document.getElementById(t + i + i);
                        if (main.checked == true) {
                            v.checked = true;
                            teacher_code = teacher_code + $scope.GradeSectionSubjectTeacher[i].sims_teacher_code + ',';
                        }
                        else {
                            v.checked = false;
                            main.cheked = false;
                            teacher_code = [];
                        }
                    }
                }
                else {
                    main = document.getElementById('mainchk1');
                    for (var i = 0; i < $scope.GradeSectionSubjectTeacher.length; i++) {
                        var t = $scope.GradeSectionSubjectTeacher[i].sims_homeroom_teacher_code;
                        var v = document.getElementById(t);
                        if (main.checked == true) {
                            v.checked = true;
                            teacher_code = teacher_code + $scope.GradeSectionSubjectTeacher[i].sims_homeroom_teacher_code + ',';
                        }

                        else {
                            v.checked = false;
                            main.cheked = false;
                            teacher_code = [];
                        }
                    }
                }
            }

            $scope.SaveGradeBookData = function (isvalid) {

                if (isvalid) {
                    TeacherCodeList = [];
                    if ($scope.edt.gb_type == "S" || $scope.edt.gb_type == "F") {
                        for (var i = 0; i < $scope.GradeSectionSubjectTeacher.length; i++) {
                            var d = $scope.GradeSectionSubjectTeacher[i].sims_teacher_code + [i] + [i];
                            var v = document.getElementById(d);
                            if (v.checked == true) {
                                TeacherCodeList = TeacherCodeList + $scope.GradeSectionSubjectTeacher[i].sims_employee_code + '/';
                            }
                        }
                        for (var i = 0; i < $scope.getGradeSectionSubjectNameorCode1.length; i++) {
                            if ($scope.getGradeSectionSubjectNameorCode1[i].sectionslis == undefined) {
                                $scope.getGradeSectionSubjectNameorCode1[i].sectionslis = [];
                            }

                            for (var j = 0; j < $scope.getGradeSectionSubjectNameorCode1[i].sectionslis.length; j++) {
                                if ($scope.getGradeSectionSubjectNameorCode1[i].sectionslis[j].subjectslist == undefined) {
                                    $scope.getGradeSectionSubjectNameorCode1[i].sectionslis[j].subjectslist = [];
                                }

                                for (var k = 0; k < $scope.getGradeSectionSubjectNameorCode1[i].sectionslis[j].subjectslist.length; k++) {
                                    if ($scope.getGradeSectionSubjectNameorCode1[i].sectionslis[j].subjectslist[k].ischecked == true) {
                                        var data = {
                                            cur_code: $scope.edt.cur_code,
                                            academic_year: $scope.edt.academic_year,
                                            grade_code: $scope.getGradeSectionSubjectNameorCode1[i].sectionslis[j].subjectslist[k].subjectGrade,
                                            section_code: $scope.getGradeSectionSubjectNameorCode1[i].sectionslis[j].subjectslist[k].subjectSection,
                                            gb_name: $scope.getGradeSectionSubjectNameorCode1[i].grade_Name + ' / ' + $scope.getGradeSectionSubjectNameorCode1[i].sectionslis[j].section_Name + ' - ' + $scope.getGradeSectionSubjectNameorCode1[i].sectionslis[j].subjectslist[k].subjectName + ' - ' + t,
                                            gb_type: $scope.edt.gb_type,
                                            gb_term_code: $scope.edt.gb_term_code,
                                            gb_grade_scale: $scope.edt.gb_grade_scale,
                                            gb_start_date: $scope.edt.termStartDate,
                                            gb_end_date: $scope.edt.termEndDate,
                                            teacher_code: user,
                                            TeacherList: TeacherCodeList,
                                            gb_remark: $scope.edt.gb_remark,
                                            gb_status: $scope.edt.gb_status,
                                            Subject_Code: $scope.getGradeSectionSubjectNameorCode1[i].sectionslis[j].subjectslist[k].subjectCode,
                                            OPR: 'IG'
                                        }
                                        demo1.push(data);
                                    }
                                }
                            }
                        }
                        if (demo1.length > 0) {
                            $http.post(ENV.apiUrl + "api/Gradebook/CUDGradeBookDataOpr", demo1).then(function (All_Grade) {
                                $scope.SCALE_GRADE = All_Grade.data;


                                if ($scope.SCALE_GRADE == "Grade book created successfully") {
                                    swal({
                                        text: 'GradeBook Created Successfully',
                                        imageUrl: "assets/img/check.png",
                                        width: 300,
                                        showCloseButton: true
                                    });


                                    demo1 = [];
                                    $scope.Teacher_name = [];
                                    $scope.GradeSectionSubjectTeacher = [];
                                    $scope.demo = [];
                                    $scope.edt['gb_type'] = '';
                                    $scope.edt['gb_grade_scale'] = '';
                                    $scope.edt['sims_subject_code'] = '';
                                    $scope.edt['sims_batch_code'] = '';
                                    $scope.edt['termStartDate'] = '';
                                    $scope.edt['termEndDate'] = '';
                                    $scope.edt['gb_remark'] = '';

                                    $scope.edt = '';
                                    $scope.edt = {
                                        gb_status: true,
                                        cur_code: $scope.curriculum[0].sims_cur_code,
                                        academic_year: $scope.Academic_year[0].sims_academic_year,
                                        gb_term_code: $scope.TermsAcademicYear[0].sims_term_code
                                    };
                                    $scope.myForm5.$setPristine();
                                    $scope.myForm5.$setUntouched();
                                    $scope.GradeBookNames();
                                }
                                else {
                                    swal({
                                        text: $scope.SCALE_GRADE,
                                        width: 300,
                                        showCloseButton: true
                                    });
                                }

                            });

                        }
                        else {
                            swal({
                                text: 'Select Atleast One Grade Section to Create GradeBook',
                                imageUrl: "assets/img/notification-alert.png",
                                width: 300,
                                showCloseButton: true
                            });
                        }
                    }

                    else {
                        for (var i = 0; i < $scope.GradeSectionSubjectTeacher.length; i++) {
                            var d = $scope.GradeSectionSubjectTeacher[i].sims_homeroom_teacher_code;
                            var v = document.getElementById(d);
                            if (v.checked == true) {
                                TeacherCodeList = TeacherCodeList + $scope.GradeSectionSubjectTeacher[i].sims_employee_code + '/';
                            }
                        }
                        for (var i = 0; i < $scope.Teacher_name.length; i++) {
                            var d = $scope.Teacher_name[i].sims_section_code;
                            var v = document.getElementById(d + i);
                            if ($scope.Teacher_name[i].status1 == true) {
                                var data = {
                                    cur_code: $scope.edt.cur_code,
                                    academic_year: $scope.edt.academic_year,
                                    grade_code: $scope.Teacher_name[i].sims_grade_code,
                                    section_code: $scope.Teacher_name[i].sims_section_code,
                                    gb_name: $scope.Teacher_name[i].sims_grade_name_en + ' / ' + $scope.Teacher_name[i].sims_section_name_en + ' - ' + $scope.Teacher_name[i].sims_homeroom_name + ' - ' + t,
                                    gb_type: $scope.edt.gb_type,
                                    gb_term_code: $scope.edt.gb_term_code,
                                    gb_grade_scale: $scope.edt.gb_grade_scale,
                                    gb_start_date: $scope.edt.termStartDate,
                                    gb_end_date: $scope.edt.termEndDate,
                                    teacher_code: user,
                                    TeacherList: TeacherCodeList,
                                    gb_remark: $scope.edt.gb_remark,
                                    gb_status: $scope.edt.gb_status,
                                    sims_homeroom_code: $scope.edt.sims_homeroom_code,
                                    sims_batch_code: $scope.edt.sims_batch_code,
                                    OPR: 'IG'
                                }
                                demo1.push(data);
                            }
                        }

                        if (demo1.length > 0) {
                            $http.post(ENV.apiUrl + "api/Gradebook/CUDGradeBookDataOpr", demo1).then(function (All_Grade) {
                                $scope.SCALE_GRADE = All_Grade.data;
                                if ($scope.SCALE_GRADE == "Grade book created successfully") {
                                    swal({
                                        text: 'GradeBook Created Successfully',
                                        imageUrl: "assets/img/check.png",
                                        width: 300,
                                        showCloseButton: true
                                    });

                                    var data = ({
                                        sims_cur_code: $scope.edt.cur_code,

                                        sims_academic_year: $scope.edt.academic_year,

                                        sims_teacher_code: user,

                                        sims_gb_term_name: $scope.edt.gb_term_code,
                                    });


                                    $http.post(ENV.apiUrl + "api/Gradebook/AllGradeBooks", data).then(function (All_GradeBooks) {
                                        $scope.All_Grade_Books_names = All_GradeBooks.data;
                                        $scope.edt = '';
                                        TeacherCodeList = [];
                                        $scope.GradeSectionSubjectTeacher = [];
                                    });

                                }
                                else {

                                    swal({
                                        text: $scope.SCALE_GRADE,
                                        width: 300,
                                        showCloseButton: true
                                    });
                                    $scope.GradeSectionSubjectTeacher = [];
                                }

                            });
                        }
                        else {

                            swal({
                                text: 'Select Atleast One Grade Section to Create GradeBook',
                                imageUrl: "assets/img/notification-alert.png",
                                width: 300,
                                showCloseButton: true
                            });
                            TeacherCodeList = [];
                        }

                        demo1 = [];
                        $scope.Teacher_name = [];
                        $scope.GradeSectionSubjectTeacher = [];
                    }
                }
            }

            $scope.GradeBookNameCreation = function (str, term) {


                $scope.demo = [];
                $scope.gradebookname = '';

                for (var i = 0; i < $scope.TermsAcademicYear.length; i++) {
                    if ($scope.TermsAcademicYear[i].sims_term_code == term) {
                        t = $scope.TermsAcademicYear[i].sims_term_desc_en;
                    }
                }

                if (str == 'S') {
                    $scope.homeroom1 = false;

                    $scope.edt.sims_homeroom_code = '';
                    $scope.edt.sims_homeroom_code = '';

                    $http.post(ENV.apiUrl + "api/Gradebook/GradeBookNameCreationBySubject?opr=SU" + "&cur_code=" + $scope.edt.cur_code).then(function (BookNameCreation) {
                        $scope.Book_Name_Creation = BookNameCreation.data;
                        for (var i = 0; i < $scope.Book_Name_Creation.length; i++) {
                            var demo1 = {
                                gradebookname: $scope.Book_Name_Creation[i].sims_subject_name_en,
                                sims_subject_code: $scope.Book_Name_Creation[i].sims_subject_code,
                            }
                            $scope.demo.push(demo1);

                        }

                    });

                }
                else {

                    $scope.homeroom1 = true;

                    $http.post(ENV.apiUrl + "api/Gradebook/GradeBookNameCreationByHomeRoom?opr=HR").then(function (BookNameCreation) {
                        $scope.Book_Name_Creation = BookNameCreation.data;

                        for (var i = 0; i < $scope.Book_Name_Creation.length; i++) {

                            var demo1 = {
                                gradebookname: $scope.Book_Name_Creation[i].sims_grade_name_en + ' / ' + $scope.Book_Name_Creation[i].sims_section_name_en + '-' + $scope.Book_Name_Creation[i].sims_homeroom_name + ' - ' + t
                            }
                            $scope.demo.push(demo1);
                        }
                    });
                }
            }

            $http.get(ENV.apiUrl + "api/homeroom/GetAllHomeroomName").then(function (AllHomeroomName) {
                $scope.Homeroomnames = AllHomeroomName.data;
            });

            $http.get(ENV.apiUrl + "api/homeroom/getAllHomeroomBatchName").then(function (AllHomeroomBatchName) {
                $scope.AllHomeroom_BatchName = AllHomeroomBatchName.data;
            });

            $scope.MultipleSelectGradeSection = function () {

                if ($scope.edt.gb_type == "S") {
                    main1 = document.getElementById('mainchk2');

                    for (var i = 0; i < $scope.Teacher_name.length; i++) {
                        if (main1.checked == true) {
                            $scope.Teacher_name[i].status = true;
                        }
                        else {
                            $scope.Teacher_name[i].status = false;
                            main1.checked == false;
                        }
                    }
                }
                else {
                    main1 = document.getElementById('mainchk2');

                    for (var i = 0; i < $scope.Teacher_name.length; i++) {
                        if (main1.checked == true) {
                            $scope.Teacher_name[i].status1 = true;
                        }
                        else {
                            $scope.Teacher_name[i].status1 = false;
                            main1.checked == false;
                        }
                    }
                }

            }

            $scope.edit = function (str, index) {
                debugger
                $http.get(ENV.apiUrl + "api/Gradebook_History/getGradeScaleflag").then(function (data) {
                    $scope.show_scale_data = data.data;
                    if ($scope.show_scale_data[0].sims_appl_parameter == '1') {
                        $scope.show_grade_scale_cat_assign = true;
                    }
                    else{
                        $scope.show_grade_scale_cat_assign = false;
                    }

                if ($scope.markentryflag == true) {
                    swal({ text: 'Mark(s) are not submitted',imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    $scope.MarkEntryShow = true;
                }
                else {

                    $scope.gb_freeze_status = str.gb_freeze_status;
                    $scope.subjectname = str.subject_name;
                    $scope.cur_gb_name = str.gb_number;
                    $scope.assignmentcolor = '';
                    $scope.catcolor = '';
                    $scope.studentwiseMarkEntryShow1 = false;
                    $scope.myForm5.$setPristine();
                    $scope.myForm5.$setUntouched();
                    $scope.Teacher_name = [];
                    $scope.mark = '';
                    $scope.studentwiseMarkEntryShow3 = false;
                    $scope.mark = '';
                    $scope.studentwiseMarkEntryShow2 = false;
                    $scope.studentwiseMarkEntryShow = false;
                    $scope.updateassign = false;
                    $scope.GradeSchemeShow = false;
                    $scope.updateshow = false;
                    $scope.editmode = false;
                    $scope.grdaebookname = true;
                    $scope.temp = {
                        cur_code: str.cur_code,
                        academic_year: str.academic_year,
                        grade_code: str.grade_code,
                        section_code: str.section_code,
                        gb_name: str.gb_name,
                        gb_number: str.gb_number,
                        isIncludedInFinalGrade: true,
                        status: true
                    };


                    if ($scope.GradeBookCreation == true) {

                        $scope.headcolor = '1';

                        $scope.table = true;
                        TeacherCodeList = [];
                        $scope.GradeSectionSubjectTeacher = [];
                        $scope.GradeSectionSubjectTeacher1 = [];
                        $scope.subject = true;
                        $scope.homeroom1 = false;
                        if (str.gb_type_desc == "HomeRoom") {
                            $scope.subject = false;
                            $scope.homeroom1 = true;
                        }
                        $scope.newinsert = false;
                        $scope.upinsert = true;

                        $scope.edt = {
                            'cur_code': str.cur_code,
                            'academic_year': str.academic_year,
                            'gb_term_code': str.gb_term_code,
                            'gb_type': str.gb_type,
                            'gradeCode': str.gb_grade_scale,
                            'termStartDate': str.gb_start_date,
                            'gb_name': str.gb_name,
                            'termEndDate': str.gb_end_date,
                            'gb_status': str.gb_status,
                            'gb_remark': str.gb_remark,
                            'grade_code': str.grade_code,
                            'section_code': str.section_code,
                            'gb_number': str.gb_number,
                            'gb_grade_scale': str.gb_grade_scale,
                            'teacher_code': str.teacher_code,
                            'sims_batch_code': str.sims_homeroom_batch_code,
                            'sims_homeroom_code': str.sims_homeroom_code,
                            'gb_freeze_status': str.gb_freeze_status
                        }

                        $scope.GradeSectionSubject = [];

                        $http.get(ENV.apiUrl + "api/Gradebook/getAllTeacherName?&gb_number=" + str.gb_number).then(function (GetGradeSectionSubjectTeacher) {
                            $scope.GradeSectionSubject = GetGradeSectionSubjectTeacher.data;
                            $scope.teacher = false;

                            $scope.GradeSectionSubjectTeacher = [];
                            $scope.GradeSectionSubjectTeacher2 = [];
                            $scope.GradeSectionSubjectTeacher1 = [];
                            $http.get(ENV.apiUrl + "api/Gradebook/GetGradeSectionSubjectTeacher?cur_code=" + $scope.edt.cur_code + "&academic_year=" + $scope.edt.academic_year + "&subject_code=" + str.subject_name).then(function (GetGradeSectionSubjectTeacher) {
                                $scope.GradeSectionSubjectTeacher1 = GetGradeSectionSubjectTeacher.data;
                                if ($scope.GradeSectionSubjectTeacher1.length > 0) {
                                    for (var i = 0; i < $scope.GradeSectionSubjectTeacher1.length; i++) {
                                        for (var k = 0; k < $scope.GradeSectionSubject.length; k++) {
                                            if ($scope.GradeSectionSubjectTeacher1[i].sims_employee_code == $scope.GradeSectionSubject[k].sims_teacher_code) {
                                                $scope.teacher = true;
                                                $scope.GradeSectionSubjectTeacher1[i].sims_gb_teacher_status = true;
                                                $scope.GradeSectionSubjectTeacher2 = $scope.GradeSectionSubjectTeacher1;
                                                break;
                                            }
                                            else {
                                                $scope.GradeSectionSubjectTeacher2 = $scope.GradeSectionSubjectTeacher1;
                                            }
                                        }
                                    }
                                    $scope.GradeSectionSubjectTeacher = $scope.GradeSectionSubjectTeacher2;
                                }


                                if ($scope.GradeSectionSubject.length > 0 && $scope.teacher == false) {
                                    for (var i = 0; i < $scope.GradeSectionSubject.length; i++) {
                                        $scope.GradeSectionSubjectTeacher1.push($scope.GradeSectionSubject[i]);
                                    }
                                    $scope.GradeSectionSubjectTeacher = $scope.GradeSectionSubjectTeacher1;
                                }

                                if ($scope.GradeSectionSubject.length == 0) {
                                    $scope.GradeSectionSubjectTeacher = $scope.GradeSectionSubjectTeacher1;
                                }

                                //});

                                $http.post(ENV.apiUrl + "api/Gradebook/AllCateories", str).then(function (AllCateories) {
                                    $scope.AllCateories_obj = AllCateories.data;
                                    str.categories = $scope.AllCateories_obj;
                                    for (var i = 0; i < str.categories.length; i++) {
                                        var check = document.getElementById(str.categories[i].gb_number);


                                        if (str.gb_freeze_status == true) {
                                            str.categories[i]['gb_cat_freeze_status'] = str.gb_freeze_status;
                                            for (var j = 0; j < str.categories[i].assignMents.length; j++) {
                                                str.categories[i].assignMents[j]['sims_gb_cat_assign_freeze'] = str.gb_freeze_status;
                                                $scope.gb_cat_freeze_status = str.gb_freeze_status;
                                                $scope.sims_gb_cat_assign_freeze = str.gb_freeze_status;
                                            }
                                        }
                                        else {

                                            $scope.gb_cat_freeze_status = str.gb_freeze_status;
                                            $scope.sims_gb_cat_assign_freeze = str.gb_freeze_status;
                                        }

                                        if (str.categories[i].gb_cat_freeze_status == true) {
                                            for (var j = 0; j < str.categories[i].assignMents.length; j++) {
                                                str.categories[i].assignMents[j]['sims_gb_cat_assign_freeze'] = str.categories[i].gb_cat_freeze_status;
                                                $scope.sims_gb_cat_assign_freeze = str.categories[i].gb_cat_freeze_status;
                                            }
                                        }

                                        if (str.ischecked == true && str.categories[i].gb_cat_freeze_status != true) {
                                            str.categories[i].ischecked = true;
                                            str.cnt = parseInt(str.cnt) + 1;
                                        }
                                        else {
                                            str.categories[i].ischecked = false;
                                            str.cnt = 0;
                                        }

                                    }

                                    for (var i = 0; i < $scope.All_Grade_Books_names.length; i++) {
                                        for (var j = 0; j < $scope.All_Grade_Books_names[i].categories.length; j++) {
                                            if ($scope.All_Grade_Books_names[i].gb_number != str.gb_number) {
                                                $scope.All_Grade_Books_names[i].categories[j].ischecked = false;
                                                $scope.All_Grade_Books_names[i].ischecked = false;
                                            }
                                        }
                                    }
                                });

                            });
                        });
                    }

                    else {

                        $scope.temp = {
                            cur_code: str.cur_code,
                            academic_year: str.academic_year,
                            grade_code: str.grade_code,
                            section_code: str.section_code,
                            gb_name: str.gb_name,
                            gb_number: str.gb_number,
                            isIncludedInFinalGrade: true,
                            status: true
                        };


                        $http.post(ENV.apiUrl + "api/Gradebook/AllCateories", str).then(function (AllCateories) {
                            $scope.AllCateories_obj = AllCateories.data;
                            str.categories = $scope.AllCateories_obj;

                            for (var i = 0; i < str.categories.length; i++) {
                                var check = document.getElementById(str.categories[i].gb_number);
                                if (str.gb_freeze_status == true) {
                                    str.categories[i]['gb_cat_freeze_status'] = str.gb_freeze_status;
                                    for (var j = 0; j < str.categories[i].assignMents.length; j++) {
                                        str.categories[i].assignMents[j]['sims_gb_cat_assign_freeze'] = str.gb_freeze_status;
                                        $scope.gb_cat_freeze_status = str.gb_freeze_status;
                                        $scope.sims_gb_cat_assign_freeze = str.gb_freeze_status;
                                    }
                                }
                                else {

                                    $scope.gb_cat_freeze_status = str.gb_freeze_status;
                                    $scope.sims_gb_cat_assign_freeze = str.gb_freeze_status;
                                }

                                if (str.categories[i].gb_cat_freeze_status == true) {
                                    for (var j = 0; j < str.categories[i].assignMents.length; j++) {
                                        str.categories[i].assignMents[j]['sims_gb_cat_assign_freeze'] = str.categories[i].gb_cat_freeze_status;
                                        $scope.sims_gb_cat_assign_freeze = str.categories[i].gb_cat_freeze_status;
                                    }
                                }

                                if (str.ischecked == true && str.categories[i].gb_cat_freeze_status != true) {
                                    str.categories[i].ischecked = true;
                                    str.cnt = parseInt(str.cnt) + 1;
                                }
                                else {
                                    str.categories[i].ischecked = false;
                                    str.cnt = 0;
                                }
                            }


                            for (var i = 0; i < $scope.All_Grade_Books_names.length; i++) {
                                for (var j = 0; j < $scope.All_Grade_Books_names[i].categories.length; j++) {
                                    if ($scope.All_Grade_Books_names[i].gb_number != str.gb_number) {
                                        $scope.All_Grade_Books_names[i].categories[j].ischecked = false;
                                        $scope.All_Grade_Books_names[i].ischecked = false;
                                    }
                                }
                            }

                        });
                    }

                    $scope.CancelGradeBooAssignmentData();

                }
                });
            }
            

            $scope.SelectAllCategory = function (info) {
                debugger
                $http.post(ENV.apiUrl + "api/Gradebook/AllCateories", info).then(function (AllCateories) {
                    $scope.AllCateories_obj = AllCateories.data;
                    info.categories = $scope.AllCateories_obj;

                    for (var i = 0; i < info.categories.length; i++) {
                        var check = document.getElementById(info.categories[i].gb_number);
                        if (info.gb_freeze_status == true) {
                            info.categories[i]['gb_cat_freeze_status'] = info.gb_freeze_status;
                            for (var j = 0; j < info.categories[i].assignMents.length; j++) {
                                info.categories[i].assignMents[j]['sims_gb_cat_assign_freeze'] = info.gb_freeze_status;
                                $scope.gb_cat_freeze_status = info.gb_freeze_status;
                                $scope.sims_gb_cat_assign_freeze = info.gb_freeze_status;
                            }
                        }
                        else {

                            $scope.gb_cat_freeze_status = info.gb_freeze_status;
                            $scope.sims_gb_cat_assign_freeze = info.gb_freeze_status;
                        }

                        if (info.categories[i].gb_cat_freeze_status == true) {
                            for (var j = 0; j < info.categories[i].assignMents.length; j++) {
                                info.categories[i].assignMents[j]['sims_gb_cat_assign_freeze'] = info.categories[i].gb_cat_freeze_status;
                                $scope.sims_gb_cat_assign_freeze = info.categories[i].gb_cat_freeze_status;
                            }
                        }
                    }
                    $scope.checkedall(info);

                });

            }

            $scope.checkedall = function (info) {

                for (var j = 0; j < info.categories.length; j++) {
                    if (info.ischecked == true && info.categories[j].gb_cat_freeze_status != true) {
                        info.categories[j].ischecked = true;
                        info.cnt = parseInt(info.cnt) + 1;
                    }
                    else {
                        info.categories[j].ischecked = false;
                        info.cnt = 0;
                    }
                }
            }

            $scope.SelectOneByOneCategory = function (str, info, info1) {

                var cnt1 = parseInt(str.categories.length);

                var check = document.getElementById(info1.cat_code + info1.gb_number);

                if (check.checked == true) {
                    info1.ischecked = true;

                    str.cnt = parseInt(str.cnt) + 1;
                    if (cnt1 == str.cnt) {
                        str.ischecked = true;
                    }

                    else {
                        str.ischecked = false;
                    }
                }
                else {
                    str.cnt = parseInt(str.cnt) - 1;
                    str.ischecked = false;
                    info1.ischecked = false;
                }

            }

            $scope.UpdateGradeBookData = function () {
                //update gradebook            
                TeacherCodeList = '';
                var datasend = [];
                
                if ($scope.edt.gb_type == "S" || $scope.edt.gb_type == "F") {
                    for (var i = 0; i < $scope.GradeSectionSubjectTeacher.length; i++) {
                        var d = $scope.GradeSectionSubjectTeacher[i].sims_teacher_code + [i] + [i];
                        var v = document.getElementById(d);
                        if (v.checked == true) {
                            TeacherCodeList = TeacherCodeList + $scope.GradeSectionSubjectTeacher[i].sims_employee_code + '/';
                        }
                    }
                    datasend.push({
                        'cur_code': $scope.edt.cur_code,
                        'academic_year': $scope.edt.academic_year,
                        'gb_term_code': $scope.edt.gb_term_code,
                        'gb_type': $scope.edt.gb_type,
                        'gradeCode': $scope.edt.gb_grade_scale,
                        'gb_start_date': $scope.edt.termStartDate,
                        'gb_name': $scope.edt.gb_name,
                        'gb_end_date': $scope.edt.termEndDate,
                        'gb_status': $scope.edt.gb_status,
                        'gb_remark': $scope.edt.gb_remark,
                        'grade_code': $scope.edt.grade_code,
                        'section_code': $scope.edt.section_code,
                        'gb_number': $scope.edt.gb_number,
                        'gb_grade_scale': $scope.edt.gb_grade_scale,
                        'teacher_code': $scope.edt.teacher_code,
                        'TeacherList': TeacherCodeList,
                        'sims_homeroom_code': $scope.edt.sims_homeroom_code,
                        'sims_batch_code': $scope.edt.sims_batch_code,
                        'gb_freeze_status': $scope.edt.gb_freeze_status,
                        OPR: 'UG'
                    });
                    $http.post(ENV.apiUrl + "api/Gradebook/CUDGradeBookDataOpr", datasend).then(function (All_Grade) {
                        $scope.SCALE_GRADE = All_Grade.data;
                        swal({
                            text: $scope.SCALE_GRADE,
                            width: 300,
                            showCloseButton: true
                        });

                        var data = ({
                            sims_cur_code: $scope.edt.cur_code,

                            sims_academic_year: $scope.edt.academic_year,

                            sims_teacher_code: user,

                            sims_gb_term_name: $scope.edt.gb_term_code,
                        });

                        $http.post(ENV.apiUrl + "api/Gradebook/AllGradeBooks", data).then(function (All_GradeBooks) {
                            $scope.All_Grade_Books_names = All_GradeBooks.data;
                            $scope.edt = '';
                            $scope.edt = {
                                gb_status: true,
                                cur_code: $scope.curriculum[0].sims_cur_code,
                                academic_year: $scope.Academic_year[0].sims_academic_year,
                                gb_term_code: $scope.TermsAcademicYear[0].sims_term_code
                            };
                            TeacherCodeList = [];
                            data = [];
                            $scope.upinsert = false;
                            $scope.GradeSectionSubjectTeacher = [];
                            $scope.myForm5.$setPristine();
                            $scope.myForm5.$setUntouched();
                            $scope.GradeBookNames();
                        });
                    });
                }
                else {

                    for (var i = 0; i < $scope.GradeSectionSubjectTeacher.length; i++) {
                        var d = $scope.GradeSectionSubjectTeacher[i].sims_homeroom_teacher_code;
                        var v = document.getElementById(d);
                        if (v.checked == true) {
                            TeacherCodeList = TeacherCodeList + $scope.GradeSectionSubjectTeacher[i].sims_homeroom_teacher_code + '/';
                        }
                    }

                    data.push({
                        'cur_code': $scope.edt.cur_code,
                        'academic_year': $scope.edt.academic_year,
                        'gb_term_code': $scope.edt.gb_term_code,
                        'gb_type': $scope.edt.gb_type,
                        'gradeCode': $scope.edt.gb_grade_scale,
                        'gb_start_date': $scope.edt.termStartDate,
                        'gb_name': $scope.edt.gb_name,
                        'gb_end_date': $scope.edt.termEndDate,
                        'gb_status': $scope.edt.gb_status,
                        'gb_remark': $scope.edt.gb_remark,
                        'grade_code': $scope.edt.grade_code,
                        'section_code': $scope.edt.section_code,
                        'gb_number': $scope.edt.gb_number,
                        'gb_grade_scale': $scope.edt.gb_grade_scale,
                        'teacher_code': $scope.edt.teacher_code,
                        'TeacherList': TeacherCodeList,
                        'sims_homeroom_code': $scope.edt.sims_homeroom_code,
                        'gb_freeze_status': $scope.edt.gb_freeze_status,
                        'sims_batch_code': $scope.edt.sims_batch_code,
                        OPR: 'UG'
                    });


                    $http.post(ENV.apiUrl + "api/Gradebook/CUDGradeBookDataOpr", data).then(function (All_Grade) {
                        $scope.SCALE_GRADE = All_Grade.data;
                        swal({
                            text: $scope.SCALE_GRADE,
                            width: 300,
                            showCloseButton: true
                        });

                        var data = ({
                            sims_cur_code: $scope.edt.cur_code,

                            sims_academic_year: $scope.edt.academic_year,

                            sims_teacher_code: user,

                            sims_gb_term_name: $scope.edt.gb_term_code,
                        });

                        $http.post(ENV.apiUrl + "api/Gradebook/AllGradeBooks", data).then(function (All_GradeBooks) {
                            $scope.All_Grade_Books_names = All_GradeBooks.data;

                            $scope.edt = '';
                            $scope.edt = {
                                gb_status: true,
                                cur_code: $scope.curriculum[0].sims_cur_code,
                                academic_year: $scope.Academic_year[0].sims_academic_year,
                                gb_term_code: $scope.TermsAcademicYear[0].sims_term_code
                            };
                            $scope.myForm5.$setPristine();
                            $scope.myForm5.$setUntouched();
                            $scope.upinsert = false;
                            $scope.GradeSectionSubjectTeacher = [];
                            TeacherCodeList = [];
                            data = [];

                            $scope.GradeBookNames();
                        });
                    });

                }
            }

            $scope.NewGradeBookNames = function () {
                $scope.updateassign1 = 0;
                $scope.newinsert = true;
                $scope.upinsert = false;
                $scope.edt['gb_type'] = '';
                $scope.edt['gb_grade_scale'] = '';
                $scope.edt['sims_subject_code'] = '';
                $scope.edt['sims_batch_code'] = '';
                $scope.edt['gb_remark'] = '';
                $scope.demo = [];

                $scope.GradeSectionSubjectTeacher = [];
                $scope.Teacher_name = [];
                $scope.edt['gb_status'] = true;


                $scope.myForm5.$setPristine();
                $scope.myForm5.$setUntouched();

            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.Getcategoryforskipstudent = function () {
                debugger;

                getcategorynames();
            }

            $scope.GetAssignmentforskipstudent = function () {

                getassignmentnames();
            }

            $scope.mark['gb_number'] = null;
            $scope.mark['cat_name'] = null;

            getassignmentnames();
            getcategorynames();

            function getcategorynames() {
                debugger;

                $http.get(ENV.apiUrl + "api/Gradebook/getcateogryforstudentskip?gb_number=" + $scope.mark.gb_number).then(function (getcateogryforstudentskip) {
                    $scope.categoriesforskipstudent = getcateogryforstudentskip.data;

                });

            }

            function getassignmentnames() {

                if ($scope.mark.gb_number == undefined) {
                    $scope.mark['gb_number'] = null;
                }

                $http.get(ENV.apiUrl + "api/Gradebook/getassignforstudentskip?gb_number=" + $scope.mark.gb_number + "&gb_cat_code=" + $scope.mark.cat_name).then(function (assignmentforskipstudent) {
                    $scope.assignmentforskipstudent = assignmentforskipstudent.data;

                });

            }


            /*****************************************************End GradeBook Cration code ******************************************************************************/

            /****************************************************** Create Category Code *********************************************************************************/
            var categorydata = [];

            $http.get(ENV.apiUrl + "api/Gradebook/GetWeightType").then(function (GetWeightType) {
                $scope.Get_Weight_Type = GetWeightType.data;

            });

            $http.get(ENV.apiUrl + "api/Gradebook/GetScoreTypes").then(function (GetScoreTypes) {
                $scope.Get_ScoreTypes = GetScoreTypes.data;

            });

            $scope.SaveGradeBooCategoryData = function (isvalid) {
                debugger
                if (isvalid) {

                    $scope.myForm.$setPristine();
                    $scope.myForm.$setUntouched();

                    $scope.flag = false;
                    var GradebookCategoryDataSend = [];
                    $scope.Expand = true;
                    for (var i = 0; i < $scope.All_Grade_Books_names.length; i++) {
                        if ($scope.All_Grade_Books_names[i].ischecked == true) {
                            $scope.flag = true;
                            categorydata = ({
                                cur_code: $scope.edt.cur_code,
                                academic_year: $scope.edt.academic_year,
                                grade_code: $scope.All_Grade_Books_names[i].grade_code,
                                section_code: $scope.All_Grade_Books_names[i].section_code,
                                gb_number: $scope.All_Grade_Books_names[i].gb_number,
                                cat_name: $scope.temp.cat_name,
                                cat_weightage_type: $scope.temp.cat_weightage_type,
                                cat_weightage_per: $scope.temp.cat_weightage_per,
                                cat_points_possible: $scope.temp.cat_points_possible,
                                cat_extra_points: $scope.temp.cat_extra_points,
                                cat_score_types: $scope.temp.cat_score_types,
                                cat_drop_low: $scope.temp.cat_drop_low,
                                cat_drop_per: $scope.temp.cat_drop_per,
                                publishDate: $scope.temp.publishDate,
                                isIncludedInFinalGrade: $scope.temp.isIncludedInFinalGrade,
                                status: $scope.temp.status,
                                cat_short_name: $scope.temp.cat_short_name,
                                sims_category_grade_scale:$scope.temp.sims_category_grade_scale,
                                cat_color: '#141212',
                                OPR: 'IC'
                            });
                            GradebookCategoryDataSend.push(categorydata);
                        }
                    }
                    if ($scope.flag == true) {
                        $http.post(ENV.apiUrl + "api/Gradebook/CategoryDataOpr", GradebookCategoryDataSend).then(function (CategoryDataOpr) {
                            $scope.CategoryDataOpr = CategoryDataOpr.data;

                            if ($scope.CategoryDataOpr.insert == true) {
                                swal({
                                    text: 'GradeBook Category Created',
                                    imageUrl: "assets/img/check.png",
                                    width: 300,
                                    showCloseButton: true
                                });
                                $scope.GradeBookNames();
                            }
                            else {

                                swal({
                                    text: 'GradeBook Category Not Created',
                                    imageUrl: "assets/img/close.png",
                                    width: 300,
                                    showCloseButton: true
                                });
                            }

                        });

                    }
                    else {

                        swal({ text: 'Select At Least One GradeBook Name To Insert Category',imageUrl: "assets/img/notification-alert.png", width: 380, showCloseButton: true });
                    }
                }
            }

            $scope.EditCateory = function (str, index) {

                if ($scope.markentryflag == true) {
                    swal({ text: 'Mark(s) are not submitted',imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                    $scope.MarkEntryShow = true;
                }
                else {
                    for (var i = 0; i < $scope.All_Grade_Books_names.length; i++) {
                        for (var j = 0; j < $scope.All_Grade_Books_names[i].categories.length; j++) {
                            if ($scope.All_Grade_Books_names[i].categories[j].cat_code == str.cat_code && $scope.All_Grade_Books_names[i].categories[j].gb_number == str.gb_number && str.gb_cat_freeze_status != true) {
                                $scope.All_Grade_Books_names[i].categories[j].ischecked = true;
                            }
                            else {
                                $scope.All_Grade_Books_names[i].categories[j].ischecked = false;
                            }
                        }
                    }

                    $scope.gb_cat_freeze_status = str.gb_cat_freeze_status;
                    $scope.sims_gb_cat_assign_freeze = str.gb_cat_freeze_status;

                    $scope.assignmentcolor = '';
                    $scope.cur_gb_name = str.gb_number;
                    $scope.catcolor = str.cat_code + str.gb_number;

                    $scope.studentwiseMarkEntryShow = false;
                    $scope.studentwiseMarkEntryShow2 = false;
                    $scope.studentwiseMarkEntryShow3 = false;
                    $scope.mark = '';
                    $scope.studentwiseMarkEntryShow1 = false;
                    $scope.updateassign = false;
                    $scope.updateshow = true;
                    $scope.editmode = true;
                    $scope.name = 'CATEGORY';
                    $scope.GradeBookCreation = false;
                    $scope.GradeSchemeShow = false;
                    $scope.grdaebookname = true;

                    if ($scope.MarkEntryShow == true) {
                        $scope.GradeBookCreation = false;
                        $scope.GradeSchemeShow = false;
                        $scope.GradeBookCategory = false;
                        $scope.GradeBookAssignment = false;
                        $scope.MarkEntryShow = true;

                    }
                    else if ($scope.GradeBookAssignment == false) {
                        $scope.headcolor = '2';

                        $scope.MarkEntryShow = false;
                        $scope.GradeBookCategory = true;
                        $scope.GradeBookAssignment = false;
                        $scope.temp = {
                            'gb_name': str.gb_name
                        , 'cat_code': str.cat_code
                        , 'cat_color': str.cat_color
                        , 'cat_drop_low': str.cat_drop_low
                        , 'cat_drop_per': str.cat_drop_per
                        , 'cat_extra_points': str.cat_extra_points
                        , 'cat_name': str.cat_name
                        , 'cat_points_possible': str.cat_points_possible
                        , 'cat_score_types': str.cat_score_types
                        , 'cat_score_types_desc': str.cat_score_types_desc
                        , 'cat_short_name': str.cat_short_name
                        , 'cat_weightage_per': str.cat_weightage_per
                        , 'cat_weightage_type': str.cat_weightage_type
                        , 'cat_weightage_type_desc': str.cat_weightage_type_desc
                        , 'cur_code': str.cur_code
                        , 'cur_name': str.cur_name
                        , 'gb_number': str.gb_number
                        , 'grade_code': str.grade_code
                        , 'grade_name': str.grade_name
                        , 'isIncludedInFinalGrade': str.isIncludedInFinalGrade
                        , 'publishDate': str.publishDate
                        , 'section_code': str.section_code
                        , 'section_name': str.section_name
                        , 'sims_category_grade_scale': str.sims_category_grade_scale
                        , 'status': str.status
                        };

                        //str.active = false;

                        //$http.post(ENV.apiUrl + "api/Gradebook/AllAssignments", str).then(function (GetAllAssignments) {
                        //    $scope.GetAllAssignments_obj = GetAllAssignments.data;

                        //    

                        //    str.assignMents = $scope.GetAllAssignments_obj;

                        //    console.log(str.assignMents);

                        //    for (var i = 0; i < $scope.All_Grade_Books_names.length; i++) {
                        //        if (index == i) {
                        //            str.active = true;
                        //        }
                        //    }
                        //});
                    }

                    else {

                        $scope.headcolor = '3';


                        $scope.GradeBookAssignment = true;
                        $scope.GradeBookCategory = false;
                        $scope.Assign = {
                            assignment_cur_code: str.cur_code,
                            assignment_academic_year: str.academic_year,
                            assignment_grade_code: str.grade_code,
                            assignment_section_code: str.section_code,
                            assignment_gb_name: str.gb_name,
                            assignment_gb_number: str.gb_number,
                            assignment_cat_name: str.cat_name,
                            assignment_cat_code: str.cat_code,
                            cur_code: str.cur_code
                        , academic_year: str.academic_year
                        , grade_code: str.grade_code
                        , section_code: str.section_code,
                            assignmentIncludedInFinalGrade: true,
                        }
                        $scope.CancelGradeBooAssignmentData();

                        //str.active = false;
                        //$http.post(ENV.apiUrl + "api/Gradebook/AllAssignments", str).then(function (GetAllAssignments) {
                        //    $scope.GetAllAssignments_obj = GetAllAssignments.data;
                        //    str.assignMents = $scope.Assignment_Data;

                        //    console.log(str.assignMents);

                        //    for (var i = 0; i < $scope.All_Grade_Books_names.length; i++) {
                        //        if (index == i) {
                        //            str.active = true;
                        //        }
                        //    }
                        //});
                    }
                }
            }

            $scope.GradeBookCateoryUpdate = function () {
                $scope.Expand = true;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                var GradebookCategoryDataSend = [];
                var categordata = ({
                    cur_code: $scope.edt.cur_code,
                    academic_year: $scope.edt.academic_year,
                    grade_code: $scope.temp.grade_code,
                    section_code: $scope.temp.section_code,
                    gb_number: $scope.temp.gb_number,
                    cat_name: $scope.temp.cat_name,
                    cat_code: $scope.temp.cat_code,
                    cat_weightage_type: $scope.temp.cat_weightage_type,
                    cat_weightage_per: $scope.temp.cat_weightage_per,
                    cat_points_possible: $scope.temp.cat_points_possible,
                    cat_extra_points: $scope.temp.cat_extra_points,
                    cat_score_types: $scope.temp.cat_score_types,
                    cat_drop_low: $scope.temp.cat_drop_low,
                    cat_drop_per: $scope.temp.cat_drop_per,
                    publishDate: $scope.temp.publishDate,
                    isIncludedInFinalGrade: $scope.temp.isIncludedInFinalGrade,
                    status: $scope.temp.status,
                    cat_short_name: $scope.temp.cat_short_name,
                    cat_color: '#141212',
                    sims_category_grade_scale: $scope.temp.sims_category_grade_scale,
                    OPR: 'UC'
                });

                GradebookCategoryDataSend.push(categordata);
                $http.post(ENV.apiUrl + "api/Gradebook/CategoryDataOpr", GradebookCategoryDataSend).then(function (CategoryDataOpr) {
                    $scope.CategoryDataOpr = CategoryDataOpr.data;

                    if ($scope.CategoryDataOpr.insert == true) {
                        swal({
                            text: 'GradeBook Category Updated',
                            imageUrl: "assets/img/check.png",
                            width: 300,
                            showCloseButton: true
                        });
                        $scope.updateshow = false;
                        $scope.temp = '';
                        $scope.editmode = false;
                        $scope.GradeBookNames();
                    }
                    else {
                        swal({
                            text: 'GradeBook Category Not Updated',
                            imageUrl: "assets/img/close.png",
                            width: 300,
                            showCloseButton: true
                        });
                    }
                });

            }

            $scope.GradeBookCateoryCancel = function () {
                $scope.updateshow = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.temp = {
                    isIncludedInFinalGrade: true,
                    status: true
                };
                $scope.editmode = false;
            }

            $scope.CheckPercentData = function (str, str1) {

                if (str > 100) {
                    $scope.temp[str1] = '';
                }
            }

            $scope.DeleteGradeBookCateory = function () {
                $scope.deleteassign = false;
                var deletecategorydata = [];
                for (var i = 0; i < $scope.All_Grade_Books_names.length; i++) {
                    if ($scope.All_Grade_Books_names[i].categories == undefined || $scope.All_Grade_Books_names[i].categories == []) {
                        $scope.All_Grade_Books_names[i].categories = [];
                    }
                    for (var j = 0; j < $scope.All_Grade_Books_names[i].categories.length; j++) {
                        if ($scope.All_Grade_Books_names[i].categories[j].ischecked == true) {
                            $scope.deleteassign = true;
                            deletecategorydata.push($scope.All_Grade_Books_names[i].categories[j])
                        }
                    }
                }
                if ($scope.deleteassign == true) {
                    swal({
                        text: 'Are You Sure? You Want To Delete Category',
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/Gradebook/ActiveInactiveGradebooks_category", deletecategorydata).then(function (CategoryDataOpr) {
                                $scope.CategoryDataOpr = CategoryDataOpr.data;

                                if ($scope.CategoryDataOpr == true) {
                                    swal({
                                        text: 'Category Deleted Successfully',
                                        imageUrl: "assets/img/check.png",
                                        width: 300,
                                        showCloseButton: true
                                    });
                                    $scope.GradeBookNames();
                                }
                                else {

                                    swal({
                                        text: 'Category Not Deleted',
                                        imageUrl: "assets/img/close.png",
                                        width: 300,
                                        showCloseButton: true
                                    });
                                }

                            });

                        }
                    })

                } else {

                    swal({
                        text: 'Select Atleast One Category To Delete',
                        imageUrl: "assets/img/notification-alert.png",
                        width: 300,
                        showCloseButton: true
                    });
                }
            }

            /****************************************************** End Create Category Code *********************************************************************************/

            /****************************************************** Create Assignment Code *********************************************************************************/

            $http.get(ENV.apiUrl + "api/Gradebook/GetAssignmentTypes").then(function (GetAssignmentTypes) {
                $scope.Assignment_Types = GetAssignmentTypes.data;

                $scope.Assign['assignment_Type'] = $scope.Assignment_Types[0].assignment_Type;
            });

            $http.get(ENV.apiUrl + "api/Gradebook/GetAssignmentforStudtype").then(function (GetAssignmentTypes) {
                $scope.Assignment_TypesFor_student = GetAssignmentTypes.data;

                $scope.Assign['assignment_Type_for_student'] = $scope.Assignment_TypesFor_student[0].assignment_Type;
            });
  
            var AssingmentData1 = [];
            $scope.student_data = [];

            $scope.SaveGradeBooAssignmentData = function (isvalid) {
                debugger;

                   if (isvalid) {

                    var AssingmentData = [];
                    $scope.Assign;

                    for (var i = 0; i < $scope.All_Grade_Books_names.length; i++) {
                        if ($scope.All_Grade_Books_names[i].categories.length == undefined) {
                            $scope.All_Grade_Books_names[i].categories = [];
                        }
                    }
                    //var AttributeList = ({
                    //    Assignment_cur_code: $scope.Assign.assignment_cur_code
                    //   , Assignment_academic_year: $scope.Assign.assignment_academic_year
                    //    //,Attr_MaxScore_Grading           :
                    //   , Assignment_GradeCompleted_Status: $scope.Assign.assignment_GradeCompleted_Status
                    //    //,Attr_Marks                      :
                    //    //,Attr_Marks_correct              :
                    //    //,Attr_Code                       :
                    //    //,Group_Code                      :
                    //   , Assignment_cat_code: $scope.Assign.assignment_cat_code
                    //   , Assignment_gb_number: $scope.Assign.assignment_gb_number
                    //   , Assignment_section_code: $scope.Assign.assignment_section_code
                    //   , Assignment_grade_code: $scope.Assign.assignment_grade_code
                    //});

                    $scope.flag1 = false;


                    for (var i = 0; i < $scope.All_Grade_Books_names.length; i++) {
                        for (var j = 0; j < $scope.All_Grade_Books_names[i].categories.length; j++) {
                            if ($scope.All_Grade_Books_names[i].categories[j].ischecked == true) {
                                $scope.flag1 = true;

                                var data = {
                                    Assignment_cur_code: $scope.All_Grade_Books_names[i].categories[j].cur_code
                                , Assignment_academic_year: $scope.All_Grade_Books_names[i].categories[j].academic_year
                                , Assignment_grade_code: $scope.All_Grade_Books_names[i].categories[j].grade_code
                                , Assignment_section_code: $scope.All_Grade_Books_names[i].categories[j].section_code
                                , Assignment_gb_number: $scope.All_Grade_Books_names[i].categories[j].gb_number
                                , Assignment_cat_code: $scope.All_Grade_Books_names[i].categories[j].cat_code
                                , Assignment_Code: $scope.Assign.assignment_Code
                                , Assignment_Name: $scope.Assign.assignment_Name
                                , Assignment_Type: $scope.Assign.assignment_Type
                                , Assignment_Extra_Creadit: $scope.Assign.assignment_Extra_Creadit
                                , Assignment_Date: $scope.Assign.assignment_Date
                                , Assignment_Due_Date: $scope.Assign.assignment_Due_Date
                                , Assignment_ExamID: $scope.Assign.assignment_ExamID
                                , AssignmentMaxScore: $scope.Assign.assignmentMaxScore
                                , AssignmentMaxScore_correct: $scope.Assign.assignmentMaxScore_correct
                                , Assignment_IP_BY_STD: $scope.Assign.assignment_IP_BY_STD
                                , Assignment_GradeCompleted_Status: $scope.Assign.assignment_GradeCompleted_Status
                                , Assignment_Score_Visible_To_Portal: $scope.Assign.assignment_Score_Visible_To_Portal
                                , Assignment_Visible_To_Portal: $scope.Assign.assignment_Visible_To_Portal
                                , Assignment_Email: $scope.Assign.assignment_Email
                                , Assignment_NarrativeGrade: $scope.Assign.assignment_NarrativeGrade
                                , Assignment_Due_Time: $scope.Assign.assignment_Due_Time
                                , Assignment_Remark: ($scope.Assign.assignment_Remark == undefined || $scope.Assign.assignment_Remark == "") ? " " : $scope.Assign.assignment_Remark
                                , Assignment_Weightage: $scope.Assign.assignment_Weightage
                                , AssignmentIncludedInFinalGrade: $scope.Assign.assignmentIncludedInFinalGrade
                                , Assignment_Other_Name: $scope.Assign.assignment_Other_Name
                                , sims_category_assignment_grade_scale: $scope.Assign.sims_category_assignment_grade_scale
                                , Assignment_term_code: $scope.All_Grade_Books_names[i].gb_term_code
                                , sims_category_assignment_student_type: $scope.Assign.sims_category_assignment_student_type
                                , OPR: "IA"
                                }
                                AssingmentData.push(data);
                            }
                        }
                    }
                    if ($scope.flag1 == true) {
                        $http.post(ENV.apiUrl + "api/Gradebook/AssignmentDataOpr", AssingmentData).then(function (AssignmentDataOpr) {
                            debugger;
                            $scope.Assignment_Data = AssignmentDataOpr.data;


                            if ($scope.Assignment_Data.insert == true) {
                                swal({
                                    text: 'Assignment Created',
                                    imageUrl: "assets/img/check.png",
                                    width: 300,
                                    showCloseButton: true
                                });
                                $scope.Assignment_Form.$setPristine();
                                $scope.Assignment_Form.$setUntouched();
                                $scope.GradeBookNames();

                            }
                            else {
                                swal({
                                    text: 'Assignment Not Created',
                                    imageUrl: "assets/img/close.png",
                                    width: 300,
                                    showCloseButton: true
                                });
                            }
                        });
                    }
                    else {
                        swal({ text: 'Select At Least One Category To Insert Assignment',imageUrl: "assets/img/notification-alert.png",width: 380, showCloseButton: true });
                    }
                }
            }

            $('.clockpicker').clockpicker({
                autoclose: true
            });

            var setdefaultvalue = [];
            var cnt = 0;
            var n = 0;
            $scope.assignmarkupdate = false;
            $scope.updategrading = {};

            $scope.EditAssignment = function (AssingmentDataObject, info1, cat) {
            debugger
            $scope.student_old_marks = [];
            $scope.propertyName = null;
            $scope.reverse = false;
            // propertyName = null; reverse = false
            //$scope.sortBy = function (propertyName) {
            //    debugger
            //    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
            //    $scope.propertyName = propertyName;
            //};

                if ($scope.markentryflag == true) {
                    swal({ text: 'Mark(s) are not submitted',imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    $scope.MarkEntryShow = true;
                }
                else {

                    if ($http.defaults.headers.common['schoolId'] == 'edison') {
                        $scope.grading_status = false;
                        if ($scope.getadmin.length > 0) {
                            if ($scope.getadmin[0].boolean_value == 'T') {
                                $scope.grading_status = true;
                            }


                        }
                    }

                    if (AssingmentDataObject.sims_gb_cat_assign_freeze == true || info1.gb_freeze_status == true || cat.gb_cat_freeze_status == true) {
                        $scope.freezeflag = true;
                    }
                    else {
                        $scope.freezeflag = false;
                    }


                    $scope.updategrading = {};
                    $scope.updategrading = AssingmentDataObject;

                    $scope.assignmentcolor = AssingmentDataObject.assignment_cat_code + AssingmentDataObject.assignment_gb_number + AssingmentDataObject.assignment_Code;
                    $scope.cur_gb_name = AssingmentDataObject.assignment_gb_number;
                    $scope.catcolor = AssingmentDataObject.assignment_cat_code + AssingmentDataObject.assignment_gb_number;
                    $scope.assignmentsdatafoeallcategories = info1;
                    $scope.studentwiseMarkEntryShow = false;
                    $scope.studentwiseMarkEntryShow2 = false;
                    $scope.studentwiseMarkEntryShow1 = false;
                    $scope.studentwiseMarkEntryShow3 = false;
                    $scope.mark = '';
                    $scope.checked = false;
                    cnt = 0;
                    $scope.grdaebookname = true;
                    $scope.updateshow = false;
                    $scope.editmode = false;
                    $scope.mark1 = false;
                    $scope.grade1 = false;
                    $scope.combovale = [];
                    $scope.updateassign = true;
                    var combovalues = [];
                    if (AssingmentDataObject.assignment_Score_type == 'M') {

                        $scope.combovalues = [
                            {
                                name: 'Mark', value: 'Mark'
                            },
                           // {
                             //   name: 'Comment', value: 'Comment'
                            //},
                            {
                                name: 'Status', value: 'Status'
                            },
                           // {
                             //   name: 'Completed Date', value: 'CompletedDate'
                            //}
                        ];

                        $scope.mark1 = true;

                    }
                    else {

                        $scope.combovalues = [

                            {
                                name: 'Grade', value: 'Grade'
                            },
                          //  {
                            //    name: 'Comment', value: 'Comment'
                            //},
                            {
                                name: 'Status', value: 'Status'
                            },
                            //{
                              //  name: 'Completed Date', value: 'CompletedDate'
                            //}

                        ];

                        $scope.grade1 = true;

                    }

                    setdefaultvalue = AssingmentDataObject;
                    $scope.assignment_cat_name = setdefaultvalue.assignment_Name;
                    $scope.assignment_Completed_Date = setdefaultvalue.assignment_Due_Date;

                    if ($scope.MarkEntryShow == true) {
                        $scope.headcolor = '4';
                        $http.get(ENV.apiUrl + "api/Gradebook/GetStudentStatus").then(function (StudentStatus) {
                            $scope.Student_Status = StudentStatus.data;
                        });
                        $scope.student_data1 = [];
                        $scope.GradeBookCreation = false;
                        $scope.GradeBookCategory = false;
                        $scope.GradeBookAssignment = false;
                        $scope.GradeSchemeShow = false;
                        $scope.assignmarkupdate = false;
                        $scope.busy = true;

                        AssingmentDataObject['teacher_code'] = user;
                        $http.post(ENV.apiUrl + "api/Gradebook/AssignMentStudents", AssingmentDataObject).then(function (AssignMentStudents) {

                            $scope.AssignMent_Students = AssignMentStudents.data;
                            $scope.busy = false;
                            $scope.statusss = false;
                            if ($scope.AssignMent_Students.studentList.length > 0) {
                                $scope.busy = false;
                                for (var j = 0; j < $scope.AssignMent_Students.studentList.length; j++) {
                                    $scope.assignmarkupdate = true;
                                    if ($scope.AssignMent_Students.studentList[j].assignment_Mark == 'NA' || $scope.AssignMent_Students.studentList[j].assignment_Comment == 'NA' || $scope.AssignMent_Students.studentList[j].assignment_Status == 'NA' || $scope.AssignMent_Students.studentList[j].assignment_Completed_Date == 'NA' || $scope.AssignMent_Students.studentList[j].assignment_Mark == '' || $scope.AssignMent_Students.studentList[j].assignment_Comment == '' || $scope.AssignMent_Students.studentList[j].assignment_Status == '' || $scope.AssignMent_Students.studentList[j].assignment_Completed_Date == '') {

                                        $scope.assignment_max_score_for_check = $scope.AssignMent_Students.studentList[j].assignmentMaxScore;

                                        if ($scope.AssignMent_Students.studentList[j].assignment_Status == 'AB' ||
                                            $scope.AssignMent_Students.studentList[j].assignment_Status == 'EL' ||
                                            $scope.AssignMent_Students.studentList[j].assignment_Status == 'ML' ||
                                            $scope.AssignMent_Students.studentList[j].assignment_Status == 'EX' ||
                                            $scope.AssignMent_Students.studentList[j].assignment_Status == 'PL' ||
                                            $scope.AssignMent_Students.studentList[j].assignment_Status == 'LA' ||
                                            $scope.AssignMent_Students.studentList[j].assignment_Status == 'A' ||
                                            $scope.AssignMent_Students.studentList[j].assignment_Status == 'L' ||
                                            $scope.AssignMent_Students.studentList[j].assignment_Status == 'S' ||
                                            $scope.AssignMent_Students.studentList[j].assignment_Status == 'NA') {
                                            $scope.statusss = true;
                                        }
                                        else {
                                            $scope.statusss = false;
                                        }
                                        if ($scope.AssignMent_Students.studentList[j].assignment_Status == 'Absent') {
                                            $scope.AssignMent_Students.studentList[j].assignment_Status == 'AB'
                                        }
                                        var data = ({
                                            editing: false
                                        , assignmentMaxScore: $scope.AssignMent_Students.studentList[j].assignmentMaxScore
                                        , assignment_Comment: $scope.AssignMent_Students.studentList[j].assignment_Comment
                                        , assignment_Completed_Date: $scope.AssignMent_Students.studentList[j].assignment_Completed_Date
                                        , assignment_Mark: $scope.AssignMent_Students.studentList[j].assignment_Mark
                                        , assignment_Status1: $scope.AssignMent_Students.studentList[j].assignment_Status
                                        , assignment_final_grade: $scope.AssignMent_Students.studentList[j].assignment_final_grade
                                        , assignment_final_grade_desc: $scope.AssignMent_Students.studentList[j].assignment_final_grade_desc
                                        , assignment_max_correct: $scope.AssignMent_Students.studentList[j].assignment_max_correct
                                        , assignment_max_point: $scope.AssignMent_Students.studentList[j].assignment_max_point
                                        , color_Code: $scope.AssignMent_Students.studentList[j].color_Code
                                        , stud_AcademicYear: $scope.AssignMent_Students.studentList[j].stud_AcademicYear
                                        , stud_CurrCode: $scope.AssignMent_Students.studentList[j].stud_CurrCode
                                        , StudEnroll: $scope.AssignMent_Students.studentList[j].stud_Enroll
                                        , Stud_roll: $scope.AssignMent_Students.studentList[j].stud_roll  //
                                        , sims_roll_number_display: $scope.AssignMent_Students.studentList[j].sims_roll_number_display  //
                                        , stud_Full_Name: $scope.AssignMent_Students.studentList[j].stud_Full_Name
                                        , stud_GradeCode: $scope.AssignMent_Students.studentList[j].stud_GradeCode
                                        , stud_SectionCode: $scope.AssignMent_Students.studentList[j].stud_SectionCode
                                        , sims_mark_final_grade_code: $scope.AssignMent_Students.studentList[j].sims_mark_final_grade_code
                                        , assignmentIncludedInFinalGrade: $scope.AssignMent_Students.assignmentIncludedInFinalGrade
                                        , assignment_Code: $scope.AssignMent_Students.assignment_Code
                                        , assignment_Email: $scope.AssignMent_Students.assignment_Email
                                        , assignment_GradeCompleted_Status: $scope.AssignMent_Students.assignment_GradeCompleted_Status
                                        , assignment_IP_BY_STD: $scope.AssignMent_Students.assignment_IP_BY_STD
                                        , assignment_Name: $scope.AssignMent_Students.assignment_Name
                                        , assignment_NarrativeGrade: $scope.AssignMent_Students.assignment_NarrativeGrade
                                        , assignment_Score_Visible_To_Portal: $scope.AssignMent_Students.assignment_Score_Visible_To_Portal
                                        , assignment_Score_type: $scope.AssignMent_Students.assignment_Score_type
                                        , assignment_Visible_To_Portal: $scope.AssignMent_Students.assignment_Visible_To_Portal
                                        , assignment_academic_year: $scope.AssignMent_Students.assignment_academic_year
                                        , assignment_cat_code: $scope.AssignMent_Students.assignment_cat_code
                                        , assignment_cat_name: $scope.AssignMent_Students.assignment_cat_name
                                        , assignment_cur_code: $scope.AssignMent_Students.assignment_cur_code
                                        , assignment_gb_number: $scope.AssignMent_Students.assignment_gb_number
                                        , assignment_grade_code: $scope.AssignMent_Students.assignment_grade_code
                                        , assignment_section_code: $scope.AssignMent_Students.assignment_section_code
                                        , sims_gb_grade_scale: $scope.AssignMent_Students.sims_gb_grade_scale
                                        , sims_student_gender: $scope.AssignMent_Students.studentList[j].sims_student_gender
                                        , ischecked: false
                                        , row_color: '#EFFBEF'
                                        , statuss: $scope.statusss
                                        , freezeflag: $scope.freezeflag
                                        , StudEnroll1: parseInt($scope.AssignMent_Students.studentList[j].stud_Enroll)
                                        });
                                        $scope.student_data1.push(data);

                                        $scope.student_data = angular.copy($scope.student_data1);
                                        $scope.student_old_marks = angular.copy($scope.student_data1);
                                    }
                                    else {
                                        $scope.busy = false;
                                        $scope.assignmarkupdate = true;
                                        var data = ({
                                            editing: false
                                    , assignmentMaxScore: $scope.AssignMent_Students.studentList[j].assignmentMaxScore
                                    , assignment_Comment: $scope.AssignMent_Students.studentList[j].assignment_Comment
                                    , assignment_Completed_Date: $scope.AssignMent_Students.studentList[j].assignment_Completed_Date
                                    , assignment_Mark: $scope.AssignMent_Students.studentList[j].assignment_Mark
                                    , assignment_Status1: $scope.AssignMent_Students.studentList[j].assignment_Status
                                    , assignment_final_grade: $scope.AssignMent_Students.studentList[j].assignment_final_grade
                                    , assignment_final_grade_desc: $scope.AssignMent_Students.studentList[j].assignment_final_grade_desc
                                    , assignment_max_correct: $scope.AssignMent_Students.studentList[j].assignment_max_correct
                                    , assignment_max_point: $scope.AssignMent_Students.studentList[j].assignment_max_point
                                    , color_Code: $scope.AssignMent_Students.studentList[j].color_Code
                                    , stud_AcademicYear: $scope.AssignMent_Students.studentList[j].stud_AcademicYear
                                    , stud_CurrCode: $scope.AssignMent_Students.studentList[j].stud_CurrCode
                                    , StudEnroll: $scope.AssignMent_Students.studentList[j].stud_Enroll
                                    , Stud_roll: $scope.AssignMent_Students.studentList[j].Stud_roll  //
                                    , stud_Full_Name: $scope.AssignMent_Students.studentList[j].stud_Full_Name
                                    , stud_GradeCode: $scope.AssignMent_Students.studentList[j].stud_GradeCode
                                    , stud_SectionCode: $scope.AssignMent_Students.studentList[j].stud_SectionCode
                                    , sims_mark_final_grade_code: $scope.AssignMent_Students.studentList[j].sims_mark_final_grade_code
                                    , assignmentIncludedInFinalGrade: $scope.AssignMent_Students.assignmentIncludedInFinalGrade
                                    , assignment_Code: $scope.AssignMent_Students.assignment_Code
                                    , assignment_Email: $scope.AssignMent_Students.assignment_Email
                                    , assignment_GradeCompleted_Status: $scope.AssignMent_Students.assignment_GradeCompleted_Status
                                    , assignment_IP_BY_STD: $scope.AssignMent_Students.assignment_IP_BY_STD
                                    , assignment_Name: $scope.AssignMent_Students.assignment_Name
                                    , assignment_NarrativeGrade: $scope.AssignMent_Students.assignment_NarrativeGrade
                                    , assignment_Score_Visible_To_Portal: $scope.AssignMent_Students.assignment_Score_Visible_To_Portal
                                    , assignment_Score_type: $scope.AssignMent_Students.assignment_Score_type
                                    , assignment_Visible_To_Portal: $scope.AssignMent_Students.assignment_Visible_To_Portal
                                    , assignment_academic_year: $scope.AssignMent_Students.assignment_academic_year
                                    , assignment_cat_code: $scope.AssignMent_Students.assignment_cat_code
                                    , assignment_cat_name: $scope.AssignMent_Students.assignment_cat_name
                                    , assignment_cur_code: $scope.AssignMent_Students.assignment_cur_code
                                    , assignment_gb_number: $scope.AssignMent_Students.assignment_gb_number
                                    , assignment_grade_code: $scope.AssignMent_Students.assignment_grade_code
                                    , assignment_section_code: $scope.AssignMent_Students.assignment_section_code
                                    , sims_gb_grade_scale: $scope.AssignMent_Students.sims_gb_grade_scale
                                    , sims_student_gender: $scope.AssignMent_Students.studentList[j].sims_student_gender
                                    , ischecked: false
                                    , row_color: '##fff'
                                    , statuss: $scope.statusss
                                    , freezeflag: $scope.freezeflag
                                    , StudEnroll1: parseInt($scope.AssignMent_Students.studentList[j].stud_Enroll)
                                        });
                                        $scope.student_data1.push(data);
                                        $scope.student_data = angular.copy($scope.student_data1);
                                        $scope.student_old_marks = angular.copy($scope.student_data1);
                                    }
                                }
                            }
                            else {

                                swal({ text: 'Assignment Student Not Found',imageUrl: "assets/img/close.png", width: 300, showCloseButton: true });
                                $scope.assignmarkupdate = false;
                            }
                        });

                        $scope.val = { assignment_GradeCompleted_Status: AssingmentDataObject.assignment_GradeCompleted_Status };


                    }
                    else {
                        $scope.headcolor = '3';
                        $scope.GradeBookCreation = false;
                        $scope.GradeBookCategory = false;
                        $scope.GradeBookAssignment = true;
                        $scope.GradeSchemeShow = false;
                        $scope.MarkEntryShow = false;
                        $scope.Assign = AssingmentDataObject;

                        var data = {
                            cur_code: $scope.Assign.assignment_cur_code,
                            academic_year: $scope.Assign.assignment_academic_year,
                            grade_code: $scope.Assign.assignment_grade_code,
                            section_code: $scope.Assign.assignment_section_code,
                            gb_number: $scope.Assign.assignment_gb_number,
                            cat_code: $scope.Assign.assignment_cat_code,
                            assign_code: $scope.Assign.assignment_Code,
                            assign_type: AssingmentDataObject.sims_category_assignment_student_type
                        };
                        $scope.All_Grade_Section_Wise_Student = [];
                        $http.post(ENV.apiUrl + "api/Gradebook/AllGradeSectionWiseStudent", data).then(function (AllGradeSectionWiseStudent) {
                            $scope.All_Grade_Section_Wise_Student = AllGradeSectionWiseStudent.data;

                            if ($scope.All_Grade_Section_Wise_Student.length >= 1) {
                                n = parseInt($scope.All_Grade_Section_Wise_Student.length);
                                for (var i = 0; i < $scope.All_Grade_Section_Wise_Student.length; i++) {
                                    if ($scope.All_Grade_Section_Wise_Student[i].stud_status == true) {
                                        cnt = parseInt(cnt) + 1;
                                        $scope.updateassign1 = 1;
                                    }
                                    if (cnt == n) {
                                        $scope.checked = true;
                                    }
                                }
                            }
                            else {
                                $scope.updateassign1 = 0;
                                swal({ text: 'Assignment Student Not Found',imageUrl: "assets/img/close.png", width: 300, showCloseButton: true });
                            }
                            $scope.busy = false;
                        });
                    }
                }

                setTimeout(function () {
                    $('input[id^="name_"]')
        .bind('keyup', function (e) {
            debugger
            var $this = $(this);
            var $tr = $this.closest("tr");
            var id = this.id.substring(0, this.id.indexOf("_"));

            if (e.keyCode == 38) {
                $tr.prev().find('input[id^=' + id + ']').focus();
            }
            else if (e.keyCode == 40) {
                $tr.next().find("input[id^='" + id + "']").focus();
            }

        });
                }, 2000);

            }

            $scope.RemoveStudentFromAssignmentAll = function () {
                for (var i = 0; i < $scope.All_Grade_Section_Wise_Student.length; i++) {
                    if ($scope.checked == false) {
                        $scope.All_Grade_Section_Wise_Student[i].stud_status = false;
                        cnt = 0;
                    }
                    else {
                        $scope.All_Grade_Section_Wise_Student[i].stud_status = true;
                        cnt = n;
                    }
                }
            }

            $scope.RemoveStudentFromAssignmentOneByOne = function (info) {

                if (info.stud_status == false) {
                    $scope.checked = false;
                    cnt = parseInt(cnt) - 1;
                }
                else {
                    cnt = parseInt(cnt) + 1;
                    if (n == cnt) {
                        $scope.checked = true;
                    }
                }
            }

            $scope.UpdateGradeBooAssignmentData = function () {

                var AssingmentData1 = [];
                var AssingmentData = $scope.Assign;
                AssingmentData.OPR = "UA";
                //var AttributeList = ({
                //    Assignment_cur_code: $scope.Assign.assignment_cur_code
                //   , Assignment_academic_year: $scope.Assign.assignment_academic_year
                //    //,Attr_MaxScore_Grading           :
                //   , Assignment_GradeCompleted_Status: $scope.Assign.assignment_GradeCompleted_Status
                //    //,Attr_Marks                      :
                //    //,Attr_Marks_correct              :
                //    //,Attr_Code                       :
                //    //,Group_Code                      :
                //   , Assignment_cat_code: $scope.Assign.assignment_cat_code
                //   , Assignment_gb_number: $scope.Assign.assignment_gb_number
                //   , Assignment_section_code: $scope.Assign.assignment_section_code
                //   , Assignment_grade_code: $scope.Assign.assignment_grade_code
                //});

                AssingmentData1.push(AssingmentData);

                $http.post(ENV.apiUrl + "api/Gradebook/AssignmentDataOpr", AssingmentData1).then(function (AssignmentDataOpr) {
                    $scope.Assignment_Data = AssignmentDataOpr.data;

                    if ($scope.Assignment_Data.insert == true) {
                        swal({
                            text: 'Assignment Updated',
                            imageUrl: "assets/img/check.png",
                            width: 300,
                            showCloseButton: true
                        });
                        $scope.Assignment_Form.$setPristine();
                        $scope.Assignment_Form.$setUntouched();

                        var studentdata = [];
                        for (var i = 0; i < $scope.All_Grade_Section_Wise_Student.length; i++) {
                            if ($scope.All_Grade_Section_Wise_Student[i].stud_status == false) {
                                $scope.flag2 = true;
                                $scope.All_Grade_Section_Wise_Student[i].Assignment_cur_code = $scope.Assign.assignment_cur_code;
                                $scope.All_Grade_Section_Wise_Student[i].Assignment_academic_year = $scope.Assign.assignment_academic_year;

                                $scope.All_Grade_Section_Wise_Student[i].assignment_grade_code = $scope.Assign.assignment_grade_code;
                                $scope.All_Grade_Section_Wise_Student[i].assignment_section_code = $scope.Assign.assignment_section_code;
                                $scope.All_Grade_Section_Wise_Student[i].assignment_gb_number = $scope.Assign.assignment_gb_number;
                                $scope.All_Grade_Section_Wise_Student[i].assignment_cat_code = $scope.Assign.assignment_cat_code;
                                $scope.All_Grade_Section_Wise_Student[i].assignment_Code = $scope.Assign.assignment_Code;
                                $scope.All_Grade_Section_Wise_Student[i].StudEnroll = $scope.All_Grade_Section_Wise_Student[i].stud_Enroll;
                                $scope.All_Grade_Section_Wise_Student[i].OPR = 'DS'
                                studentdata.push($scope.All_Grade_Section_Wise_Student[i]);
                            }

                        }
                        if ($scope.flag2 == true) {
                            $http.post(ENV.apiUrl + "api/Gradebook/AssignmentDataOprDelete", studentdata).then(function (AssignmentDataOpr) {
                                $scope.Assignment = AssignmentDataOpr.data;

                            });

                        }

                    }
                    else {
                        swal({
                            text: 'Assignment Not Updated',
                            imageUrl: "assets/img/close.png",
                            width: 300,
                            showCloseButton: true
                        });
                    }

                    $scope.GradeBookNames();
                });

            }

            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.Assign[name1] = year + "/" + month + "/" + day;
            }

            $scope.showdatecheck = function (exp_date_pass, issue_date_pass, name1) {

                var month1 = exp_date_pass.split("/")[0];
                var day1 = exp_date_pass.split("/")[1];
                var year1 = exp_date_pass.split("/")[2];
                var new_exp_date_pass = year1 + "/" + month1 + "/" + day1;

                var year = issue_date_pass.split("/")[0];
                var month = issue_date_pass.split("/")[1];
                var day = issue_date_pass.split("/")[2];
                var new_issue_date_pass = year + "/" + month + "/" + day;

                if (new_exp_date_pass < new_issue_date_pass) {

                    swal({
                        text: 'Please Select Future Date',
                        width: 300,
                        showCloseButton: true
                    });

                    $scope.Assign[name1] = '';
                }
                else {

                    $scope.Assign[name1] = new_exp_date_pass;
                }
            }

            $scope.CancelGradeBooAssignmentData = function () {
                $scope.updateassign1 = false;
                $scope.Assign = {};
                $scope.Assign['assignmentIncludedInFinalGrade'] = true,
                $scope.Assign['assignment_Type'] = $scope.Assignment_Types[0].assignment_Type;
                $scope.updateassign = false;
                $scope.Assignment_Form.$setPristine();
                $scope.Assignment_Form.$setUntouched();
                if ($http.defaults.headers.common['schoolId'] == 'ahis') {
                    $scope.asignment_teacher();
                }
            }

            $scope.DeleteGradeBookCateoryAssignment = function () {
                $scope.deleteassign = false;
                var deletecategorydata = [];
                for (var i = 0; i < $scope.All_Grade_Books_names.length; i++) {

                    if ($scope.All_Grade_Books_names[i].categories != undefined && $scope.All_Grade_Books_names[i].categories.length != 0) {
                        for (var j = 0; j < $scope.All_Grade_Books_names[i].categories.length; j++) {

                            if ($scope.All_Grade_Books_names[i].categories[j].assignMents != undefined && $scope.All_Grade_Books_names[i].categories[j].assignMents.length != 0) {
                                for (var k = 0; k < $scope.All_Grade_Books_names[i].categories[j].assignMents.length; k++) {
                                    if ($scope.All_Grade_Books_names[i].categories[j].assignMents[k].ischecked == true) {
                                        $scope.deleteassign = true;
                                        var data = {}
                                        data.Assignment_cur_code = $scope.All_Grade_Books_names[i].categories[j].assignMents[k].assignment_cur_code;
                                        data.Assignment_academic_year = $scope.All_Grade_Books_names[i].categories[j].assignMents[k].assignment_academic_year;
                                        data.Assignment_grade_code = $scope.All_Grade_Books_names[i].categories[j].assignMents[k].assignment_grade_code;
                                        data.Assignment_section_code = $scope.All_Grade_Books_names[i].categories[j].assignMents[k].assignment_section_code;
                                        data.Assignment_gb_number = $scope.All_Grade_Books_names[i].categories[j].assignMents[k].assignment_gb_number;
                                        data.Assignment_cat_code = $scope.All_Grade_Books_names[i].categories[j].assignMents[k].assignment_cat_code;
                                        data.Assignment_Code = $scope.All_Grade_Books_names[i].categories[j].assignMents[k].assignment_Code;

                                        deletecategorydata.push(data);

                                    }
                                }
                            }
                        }
                    }
                }
                if ($scope.deleteassign == true) {
                    swal({
                        text: 'Are You Sure? You Want To Delete Assignment',
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/Gradebook/ActiveInactiveGradebooks_category_assign", deletecategorydata).then(function (CategoryDataOpr) {
                                $scope.CategoryDataOpr = CategoryDataOpr.data;

                                if ($scope.CategoryDataOpr == true) {
                                    swal({
                                        text: 'Assignment Deleted Successfully',
                                        imageUrl: "assets/img/check.png",
                                        width: 300,
                                        showCloseButton: true
                                    });
                                    $scope.GradeBookNames();
                                }
                                else {

                                    swal({
                                        text: 'Assignment Not Deleted',
                                        imageUrl: "assets/img/close.png",
                                        width: 300,
                                        showCloseButton: true
                                    });
                                }

                            });
                        }
                    })
                }
                else {

                    swal({
                        text: 'Select Atleast One Assignment To Delete',
                        imageUrl: "assets/img/notification-alert.png",
                        width: 300,
                        showCloseButton: true
                    });
                }
            }

            /******************************************************End Create Assignment Code *********************************************************************************/

            /******************************************************Start Mark Entry*********************************************************************************/
            $scope.comment = true;
            var comment = '';
            var defaultmark = '';
            var defaultgrade = '';
            var defaultmark1 = '';
            var defaultgrade1 = '';
            var defaultdate = '';
            var defaultstatus = '';
            $scope.studentwiseMarkEntryShow1 = false;
            $scope.studentwiseMarkEntryShow2 = false;
            $scope.studentwiseMarkEntryShow3 = false;
            $scope.mark = '';

            $scope.doneEditing1 = function (info) {

                if (info.assignment_Status1 == 'AB' ||
                    info.assignment_Status1 == 'EL' ||
                    info.assignment_Status1 == 'ML' ||
                    info.assignment_Status1 == 'EX' ||
                    info.assignment_Status1 == 'PL' ||
                    info.assignment_Status1 == 'LA' ||
                    info.assignment_Status1 == 'A' ||
                    info.assignment_Status1 == 'S' ||
                    info.assignment_Status1 == 'NA' ||
                    info.assignment_Status1 == 'UF' ||
                    info.assignment_Status1 == 'Absent') {

                    info.statuss = true;
                    info.assignment_final_grade_desc = '';
                    info.assignment_Mark = '';
                    info.assignment_Comment = '';
                    info.sims_mark_final_grade_code = '';
                    var now = new Date();
                    var month = (now.getMonth() + 1);
                    var day = now.getDate();
                    if (month < 10)
                        month = "0" + month;
                    if (day < 10)
                        day = "0" + day;
                    info.assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                }
                else if (info.assignment_Status1 == 'Clear') {
                    info.statuss = false;
                    info.assignment_final_grade_desc = undefined;
                    info.assignment_Mark = undefined;
                    info.assignment_Comment = undefined;
                    info.sims_mark_final_grade_code = undefined;
                    info.assignment_Completed_Date = undefined;
                }
            }

            $scope.CheckValues = function (info) {
                info.ischecked = true;
                $scope.markentryflag = true;

            }

            $scope.CheckGrade = function (str, info) {
                debugger   //check grade
                GradeBookStudentMarkData = [];
                $scope.show_scale = '';
                $http.get(ENV.apiUrl + "api/Gradebook_History/getGradeScaleflag").then(function (GradeScaleData) {
                    $scope.show_scale = GradeScaleData.data;
              

                    if ($scope.show_scale[0].sims_appl_parameter == '1') {
                    if (str != " " && str != 'NA') {
                        if (parseFloat(info.assignment_Mark) > parseFloat(info.assignmentMaxScore)) {
                            swal({ text: 'Mark Should Be Less Than Max Marks', width: 300, height: 250, showCloseButton: true });
                        }
                        else {
                        var mark = parseFloat(info.assignment_Mark + ".00")
                        $http.get(ENV.apiUrl + "api/Gradebook/getGradeScaleData_Cat_Assign?CUR_CODE=" + info.stud_CurrCode +
                            "&ACADEMIC_YEAR=" + info.stud_AcademicYear + "&GRADE_CODE=" + info.stud_GradeCode + "&SECTION_CODE=" + info.stud_SectionCode
                            + "&GB_NUMBER=" + info.assignment_gb_number + "&GB_CAT_NUMBER=" + info.assignment_cat_code + "&GB_ASSIGNMENT_NUMBER=" + info.assignment_Code +
                            "&sims_gb_cat_assign_mark=" + info.assignment_Mark + "&sims_assign_max_marks=" + info.assignmentMaxScore).then(function (GradeScaleData) {
                                $scope.Grade_Scale_Data = GradeScaleData.data;
                                info.assignment_final_grade_desc = $scope.Grade_Scale_Data[0].sims_mark_grade_name;
                                info.sims_mark_final_grade_code = $scope.Grade_Scale_Data[0].sims_mark_grade_code;
                                info.assignment_Comment = $scope.Grade_Scale_Data[0].sims_mark_grade_description;
                                info.sims_mark_grade_name = $scope.Grade_Scale_Data[0].sims_mark_grade_name;
                                info.assignment_Mark = mark;
                                info.ischecked = true;
                                info.ischecked1 = true;
                                if ($scope.studentwiseMarkEntryShow1 != true) {
                                    $scope.markentryflag = true;
                                } else {
                                    $scope.markentryflag2 = true;
                                }

                                var now = new Date();
                                var month = (now.getMonth() + 1);
                                var day = now.getDate();
                                if (month < 10)
                                    month = "0" + month;
                                if (day < 10)
                                    day = "0" + day;

                                var date = info.assignment_Completed_Date.split('-')[0];

                                if (info.assignment_Completed_Date == '' || date == '1900') {
                                    info.assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                }
                            });
                    }
                }
                else {
                    if ($scope.studentwiseMarkEntryShow1 != true) {
                        $scope.markentryflag = true;
                    }
                    else {
                        $scope.markentryflag2 = true;
                    }
                    info.ischecked = true;
                    info.ischecked1 = true;
                    info.assignment_final_grade_desc = "";
                    info.sims_mark_final_grade_code = "";
                    info.assignment_Comment = "";
                    info.sims_mark_grade_name = "";
                    info.assignment_Mark = '';
                    swal({ text: 'Mark Is Empty Or Grade Schema Is Not Defined', width: 300, height: 250, showCloseButton: true });
                }

                if (str == "") {
                    info.ischecked = true;
                    info.ischecked1 = true;
                    if ($scope.studentwiseMarkEntryShow1 != true) {
                        $scope.markentryflag = true;
                    } else {
                        $scope.markentryflag2 = true;
                    }
                    info.assignment_Completed_Date = '';
                }

                if (info.assignment_Mark == '') {
                    info.errormark = 'Red'
                }
                else {
                    info.errormark = '#6abd2d'
                }
//////////////////////end if


                }
                else {
                    if (str != " " && str != 'NA' && $scope.Grade_Scale_Data.length > 0) {
                        var str1 = parseFloat(str);
                        var mark1 = parseFloat(info.assignmentMaxScore);
                        //var mark1 = parseFloat(info.assignmentMaxScore.split(".")[0]);
                        var maxmark = (mark1 / 10) * 100;
                        var maxmark1 = Math.round((str1 * 100) / mark1);
                        //write API FOR GETTING GRADE SCALE

                        if (str1 <= mark1) {

                            var mark = parseFloat(info.assignment_Mark + ".00")
                            for (var i = 0; i < $scope.Grade_Scale_Data.length; i++) {
                                if (maxmark1 <= parseFloat($scope.Grade_Scale_Data[i].sims_mark_grade_high) && maxmark1 >= parseFloat($scope.Grade_Scale_Data[i].sims_mark_grade_low)) {
                                    if (info.sims_gb_grade_scale == $scope.Grade_Scale_Data[i].gradeGroupCode) {
                                        info.assignment_final_grade_desc = $scope.Grade_Scale_Data[i].sims_mark_grade_name;
                                        info.sims_mark_final_grade_code = $scope.Grade_Scale_Data[i].sims_mark_grade_code;
                                        info.assignment_Comment = $scope.Grade_Scale_Data[i].sims_mark_grade_description;
                                        info.sims_mark_grade_name = $scope.Grade_Scale_Data[i].sims_mark_grade_name;
                                        info.assignment_Mark = mark;
                                        info.ischecked = true;
                                        info.ischecked1 = true;
                                        if ($scope.studentwiseMarkEntryShow1 != true) {
                                            $scope.markentryflag = true;
                                        } else {
                                            $scope.markentryflag2 = true;
                                        }
                                        break;
                                    }
                                }
                                else {
                                    info.ischecked = false;
                                    info.ischecked1 = false;
                                    info.assignment_final_grade_desc = "";
                                    info.sims_mark_final_grade_code = "";
                                    info.assignment_Comment = "";
                                    info.sims_mark_grade_name = "";
                                    info.assignment_Mark = '';
                                }
                            }
                            var now = new Date();
                            var month = (now.getMonth() + 1);
                            var day = now.getDate();
                            if (month < 10)
                                month = "0" + month;
                            if (day < 10)
                                day = "0" + day;

                            var date = info.assignment_Completed_Date.split('-')[0];

                            if (info.assignment_Completed_Date == '' || date == '1900') {
                                info.assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                            }

                        }

                        else {
                            info.ischecked = false;
                            info.ischecked1 = false;
                            info.assignment_final_grade_desc = "";
                            info.sims_mark_final_grade_code = "";
                            info.assignment_Comment = "";
                            info.sims_mark_grade_name = "";
                            info.assignment_Mark = '';
                            info.assignment_Completed_Date = "";
                            info.assignment_Status1 = "";
                            if (str1 == undefined || str1 == '')
                                info.assignment_Mark = '';

                        }

                        info.row_color = '##fff';
                    }
                    else {
                        if ($scope.studentwiseMarkEntryShow1 != true) {
                            $scope.markentryflag = true;
                        }
                        else {
                            $scope.markentryflag2 = true;
                        }
                        info.ischecked = true;
                        info.ischecked1 = true;
                        info.assignment_final_grade_desc = "";
                        info.sims_mark_final_grade_code = "";
                        info.assignment_Comment = "";
                        info.sims_mark_grade_name = "";
                        info.assignment_Mark = '';
                        swal({ text: 'Mark Is Empty Or Grade Schema Is Not Defined', width: 300, height: 250, showCloseButton: true });
                    }

                    if (str == "") {
                        info.ischecked = true;
                        info.ischecked1 = true;
                        if ($scope.studentwiseMarkEntryShow1 != true) {
                            $scope.markentryflag = true;
                        } else {
                            $scope.markentryflag2 = true;
                        }
                        info.assignment_Completed_Date = '';
                    }

                    if (info.assignment_Mark == '') {
                        info.errormark = 'Red'
                    }
                    else {
                        info.errormark = '#6abd2d'
                    }

                }
                });
            }

            $scope.onlyNumbers = function (event, str) {


                if (str.assignment_Mark > 100) {
                    str.assignment_Mark = '';
                }
                else {
                    var keys = {

                        'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                        '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                    };
                    for (var index in keys) {
                        if (!keys.hasOwnProperty(index)) continue;
                        if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                            return; //default event
                        }
                    }
                    event.preventDefault();
                }
            };

            $timeout(function () {
                $("#example_wrapper").scrollbar();
                $("#Div7").scrollbar();
                $("#Div8").scrollbar();
                $("#Div9").scrollbar();
            }, 100);

            var GradeBookStudentMarkData = [];

            $scope.SaveGradeBookStudentMarkData = function () {
                debugger
                GradeBookStudentMarkData = [];
                $scope.afterSaveData = [];
                $scope.student_old_marks = [];
                for (var i = 0; i < $scope.student_data.length; i++) {
                    if ($scope.student_data[i].ischecked == true) {
                        var data = {
                            Assignment_cur_code: $scope.student_data[i].stud_CurrCode,
                            Assignment_academic_year: $scope.student_data[i].stud_AcademicYear,
                            Assignment_grade_code: $scope.student_data[i].stud_GradeCode,
                            Assignment_section_code: $scope.student_data[i].stud_SectionCode,
                            Assignment_gb_number: $scope.student_data[i].assignment_gb_number,
                            Assignment_cat_code: $scope.student_data[i].assignment_cat_code,
                            Assignment_Code: $scope.student_data[i].assignment_Code,
                            Assignment_max_point: $scope.student_data[i].assignment_max_point,
                            sims_mark_final_grade_code: $scope.student_data[i].sims_mark_final_grade_code,
                            Assignment_Mark: $scope.student_data[i].assignment_Mark,
                            Assignment_max_correct: $scope.student_data[i].assignment_max_correct,
                            Assignment_Comment: $scope.student_data[i].assignment_Comment,
                            StudEnroll: $scope.student_data[i].StudEnroll,
                            Assignment_Status1: $scope.student_data[i].assignment_Status1,
                            Assignment_Completed_Date: $scope.student_data[i].assignment_Completed_Date
                        };

                        GradeBookStudentMarkData.push(data);
                        $scope.student_data[i].ischecked = false;
                    }
                }

                if (GradeBookStudentMarkData.length > 0) {
                    $http.post(ENV.apiUrl + "api/Gradebook/AssignMarksToStudentsM", GradeBookStudentMarkData).then(function (AssignMarksToStudentsM) {
                        $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;


                        if ($scope.AssignMarks_ToStudentsM == true) {
                            $scope.markentryflag = false;
                            $scope.afterSaveData = angular.copy($scope.student_data);
                            $scope.student_old_marks = angular.copy($scope.afterSaveData);
                            swal({
                                text: 'Student Assignment Mark Updated',
                                imageUrl: "assets/img/check.png",
                                width: 300,
                                showCloseButton: true
                            });
                            $scope.GradeBookNames();

                        }
                        else {
                            $scope.markentryflag = false;
                            swal({
                                text: 'Student Assignment Mark Not Updated',
                                imageUrl: "assets/img/close.png",
                                width: 300,
                                showCloseButton: true
                            });
                        }

                    });
                }
                else {
                    //for (var i = 0; i < $scope.student_data.length; i++) {
                    //    if ($scope.student_data[i].sims_mark_final_grade_code != null) {
                    //        GradeBookStudentMarkData.push($scope.student_data[i]);
                    //    }
                    //}

                    //$http.post(ENV.apiUrl + "api/Gradebook/AssignMarksToStudentsM", GradeBookStudentMarkData).then(function (AssignMarksToStudentsM) {
                    //    $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;

                    //    if ($scope.AssignMarks_ToStudentsM == true) {
                    //        swal({
                    //            text: 'Student Assignment Mark Updated',
                    //            width: 300,
                    //            showCloseButton: true
                    //        });
                    //    }
                    //    else {
                    //        swal({
                    //            text: 'Student Assignment Mark Not Updated',
                    //            width: 300,
                    //            showCloseButton: true
                    //        });
                    //    }
                    //});

                    swal({
                        text: 'Change At Least One Record To Update Student Assignment Mark',
                        imageUrl: "assets/img/notification-alert.png",
                        width: 380,
                        showCloseButton: true
                    });
                }
            }

            $scope.Setdefaultvaluecomment = function (str) {
                comment = str;
            }

            $scope.ChechDefaultvalues = function (str) {
                if (str == 'Grade') {
                    $scope.Completed_Date = false;
                    $scope.comment = false;
                    $scope.Grade = true;
                    $scope.Status = false;
                    $scope.Mark = false;


                }
                else if (str == 'Comment') {
                    $scope.comment = true;
                    $scope.Completed_Date = false;
                    $scope.Grade = false;
                    $scope.Status = false;
                    $scope.Mark = false;

                }
                else if (str == 'CompletedDate') {
                    $scope.Completed_Date = true;
                    $scope.comment = false;
                    $scope.Grade = false;
                    $scope.Status = false;
                    $scope.Mark = false;

                }
                else if (str == 'Status') {
                    $scope.Status = true;
                    $scope.Completed_Date = false;
                    $scope.Grade = false;
                    $scope.comment = false;
                    $scope.Mark = false;
                }

                if (str == 'Mark') {
                    $scope.Completed_Date = false;
                    $scope.comment = false;
                    $scope.Grade = false;
                    $scope.Status = false;
                    $scope.Mark = true;
                }
            }

            $scope.SetdefaultvalueMark = function (str) {

                if (parseInt(str) <= parseInt($scope.assignment_max_score_for_check)) {
                    defaultmark = str;
                }
                else {
                    defaultmark = '';
                    $scope.val.assignment_mark = '';
                }
            }

            $scope.SetAssignmentChangeFlag = function (info) {
                info.status = true;
            }

            $scope.CheckMark = function (str, info) {

                if (str != '') {


                    var now = new Date();
                    var month = (now.getMonth() + 1);
                    var day = now.getDate();
                    if (month < 10)
                        month = "0" + month;
                    if (day < 10)
                        day = "0" + day;
                    if ($scope.Grade_Scale_Data.length > 0) {
                        for (var i = 0; i < $scope.Grade_Scale_Data.length; i++) {
                            if ($scope.Grade_Scale_Data[i].sims_mark_grade_name == str && info.sims_gb_grade_scale == $scope.Grade_Scale_Data[i].gradeGroupCode) {
                                for (var j = 0; j < $scope.student_data.length; j++) {
                                    info.assignment_final_grade_desc = $scope.Grade_Scale_Data[i].sims_mark_grade_name;
                                    info.sims_mark_final_grade_code = $scope.Grade_Scale_Data[i].sims_mark_grade_code;
                                    info.assignment_Comment = $scope.Grade_Scale_Data[i].sims_mark_grade_description;
                                    info.assignment_Mark = $scope.Grade_Scale_Data[i].sims_mark_grade_point;
                                    info.ischecked = true;
                                    info.ischecked1 = true;
                                    if ($scope.studentwiseMarkEntryShow1 != true) {
                                        $scope.markentryflag = true;
                                    } else {
                                        $scope.markentryflag2 = true;
                                    }
                                }

                                info.assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                info.row_color = '##fff';
                                break;
                            }
                            else {

                                info.ischecked = true;
                                info.ischecked1 = true;
                                if ($scope.studentwiseMarkEntryShow1 != true) {
                                    $scope.markentryflag = true;
                                } else {
                                    $scope.markentryflag2 = true;
                                }
                                info.assignment_final_grade_desc = "";
                                info.sims_mark_final_grade_code = "";
                                info.assignment_Comment = "";
                                info.assignment_Mark = "";
                                info.assignment_Completed_Date = "";
                                info.errormark = 'Red'


                            }
                        }
                    } else {
                        info.assignment_final_grade_desc = "";
                        info.errormark = 'Red'
                    }
                }
                else {
                    info.ischecked = true;
                    if ($scope.studentwiseMarkEntryShow1 != true) {
                        $scope.markentryflag = true;
                    } else {
                        $scope.markentryflag2 = true;
                    }
                    info.ischecked1 = true;
                    info.assignment_final_grade_desc = "";
                    info.sims_mark_final_grade_code = "";
                    info.assignment_Comment = "";
                    info.assignment_Mark = "";
                    info.assignment_Completed_Date = '';
                    info.errormark = 'Red'

                }

                if (info.assignment_final_grade_desc == '') {
                    info.errormark = 'Red'
                }
                else {
                    info.errormark = '#6abd2d'
                }

            }

            $scope.SetAllDefaultvalues = function () {
                $scope.student_data = angular.copy($scope.student_data1);
                $scope.val.assignment_Completed_Date = '';
                $scope.val.assignment_Comment = '';
                $scope.markentryflag = false;

            }

            $scope.Setdefaultvalueassignment_Completed_Date = function (str) {

                defaultdate = str;
            }

            $scope.Setdefaultvalueassignment_final_grade_desc = function (str) {
                defaultgrade = str;
            }

            $scope.Setdefaultvalueassignment_status = function (str) {

                defaultstatus = str;
            }

            $scope.ApplyDefualtValues = function () {

                var val = document.getElementById('cmb_term').value;
                if (val == 'Grade') {

                    for (var i = 0; i < $scope.Grade_Scale_Data.length; i++) {
                        if ($scope.Grade_Scale_Data[i].sims_mark_grade_code == defaultgrade) {
                            for (var j = 0; j < $scope.student_data.length; j++) {
                                var t = document.getElementById('Overwrite');
                                if (t.checked == false) {
                                    if ($scope.student_data[j].assignment_GradeCompleted_Status != true) {
                                        if ($scope.student_data[j].assignment_final_grade_desc == "" || $scope.student_data[j].assignment_Completed_Date == "" || $scope.student_data[j].assignment_final_grade_desc == 'NA' || $scope.student_data[j].assignment_Completed_Date == $scope.student_data[j].assignment_Completed_Date) {
                                            $scope.student_data[j].assignment_final_grade_desc = $scope.Grade_Scale_Data[i].sims_mark_grade_name;
                                            $scope.student_data[j].sims_mark_final_grade_code = $scope.Grade_Scale_Data[i].sims_mark_grade_code;
                                            $scope.student_data[j].assignment_Comment = $scope.Grade_Scale_Data[i].sims_mark_grade_description;
                                            $scope.student_data[j].assignment_Mark = $scope.Grade_Scale_Data[i].sims_mark_grade_point;
                                            $scope.student_data[j].ischecked = true;
                                            var now = new Date();
                                            var month = (now.getMonth() + 1);
                                            var day = now.getDate();
                                            if (month < 10)
                                                month = "0" + month;
                                            if (day < 10)
                                                day = "0" + day;
                                            $scope.student_data[j].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                            $scope.student_data[j].row_color = '##fff';
                                        }
                                    }
                                }
                                else {
                                    if ($scope.student_data[j].assignment_GradeCompleted_Status != true) {
                                        $scope.student_data[j].assignment_final_grade_desc = $scope.Grade_Scale_Data[i].sims_mark_grade_name;
                                        $scope.student_data[j].sims_mark_final_grade_code = $scope.Grade_Scale_Data[i].sims_mark_grade_code;
                                        $scope.student_data[j].assignment_Comment = $scope.Grade_Scale_Data[i].sims_mark_grade_description;
                                        $scope.student_data[j].assignment_Mark = $scope.Grade_Scale_Data[i].sims_mark_grade_point;
                                        $scope.student_data[j].ischecked = true;
                                        var now = new Date();
                                        var month = (now.getMonth() + 1);
                                        var day = now.getDate();
                                        if (month < 10)
                                            month = "0" + month;
                                        if (day < 10)
                                            day = "0" + day;
                                        $scope.student_data[j].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                        $scope.student_data[j].row_color = '##fff';
                                    }
                                }

                            }
                        }

                    }
                }
                else if (val == 'Comment') {

                    for (var i = 0; i < $scope.student_data.length; i++) {
                        var t = document.getElementById('Overwrite');
                        if (t.checked == false) {
                            if ($scope.student_data[i].assignment_GradeCompleted_Status != true) {
                                if ($scope.student_data[i].assignment_Mark == "" || $scope.student_data[i].assignment_Completed_Date == "" || $scope.student_data[i].assignment_Mark == "NA" || $scope.student_data[i].assignment_Completed_Date == $scope.student_data[i].assignment_Completed_Date) {
                                    $scope.student_data[i].assignment_Comment = comment;
                                    $scope.student_data[i].ischecked = true;
                                    var now = new Date();
                                    var month = (now.getMonth() + 1);
                                    var day = now.getDate();
                                    if (month < 10)
                                        month = "0" + month;
                                    if (day < 10)
                                        day = "0" + day;
                                    $scope.student_data[i].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                    $scope.student_data[i].row_color = '##fff';
                                }
                            }
                        }
                        else {
                            if ($scope.student_data[i].assignment_GradeCompleted_Status != true) {
                                $scope.student_data[i].assignment_Comment = comment;
                                $scope.student_data[i].ischecked = true;
                                var now = new Date();
                                var month = (now.getMonth() + 1);
                                var day = now.getDate();
                                if (month < 10)
                                    month = "0" + month;
                                if (day < 10)
                                    day = "0" + day;
                                $scope.student_data[i].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                $scope.student_data[i].row_color = '##fff';
                            }
                        }
                    }
                }
                else if (val == 'CompletedDate') {

                    for (var i = 0; i < $scope.student_data.length; i++) {
                        var t = document.getElementById('Overwrite');
                        if (t.checked == false) {
                            if ($scope.student_data[i].assignment_GradeCompleted_Status != true) {
                                if ($scope.student_data[i].assignment_Completed_Date == "" || $scope.student_data[i].assignment_Completed_Date == $scope.student_data[i].assignment_Completed_Date) {
                                    $scope.student_data[i].assignment_Completed_Date = defaultdate;
                                    $scope.student_data[i].ischecked = true;
                                    var now = new Date();
                                    var month = (now.getMonth() + 1);
                                    var day = now.getDate();
                                    if (month < 10)
                                        month = "0" + month;
                                    if (day < 10)
                                        day = "0" + day;
                                    $scope.student_data[i].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                    $scope.student_data[i].row_color = '##fff';
                                }
                            }
                        }
                        else {
                            if ($scope.student_data[i].assignment_GradeCompleted_Status != true) {
                                $scope.student_data[i].assignment_Completed_Date = defaultdate;
                                $scope.student_data[i].ischecked = true;

                                var now = new Date();
                                var month = (now.getMonth() + 1);
                                var day = now.getDate();
                                if (month < 10)
                                    month = "0" + month;
                                if (day < 10)
                                    day = "0" + day;
                                $scope.student_data[i].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                $scope.student_data[i].row_color = '##fff';
                            }
                        }
                    }
                }
                else if (val == 'Status') {
                    for (var i = 0; i < $scope.Student_Status.length; i++) {
                        if ($scope.Student_Status[i].status_id == defaultstatus) {

                            for (var j = 0; j < $scope.student_data.length; j++) {

                                var t = document.getElementById('Overwrite');
                                if (t.checked == false) {
                                    if ($scope.student_data[j].assignment_GradeCompleted_Status != true) {
                                        if ($scope.student_data[j].assignment_Status1 == "" || $scope.student_data[j].assignment_Completed_Date == "" || $scope.student_data[j].assignment_Status1 == "NA") {
                                            $scope.student_data[j].assignment_Status1 = $scope.Student_Status[i].status_id;
                                            $scope.doneEditing1($scope.student_data[j]);
                                            $scope.student_data[j].ischecked = true;
                                            var now = new Date();
                                            var month = (now.getMonth() + 1);
                                            var day = now.getDate();
                                            if (month < 10)
                                                month = "0" + month;
                                            if (day < 10)
                                                day = "0" + day;
                                            $scope.student_data[j].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                            $scope.student_data[j].row_color = '##fff';
                                        }
                                    }
                                }
                                else {
                                    if ($scope.student_data[j].assignment_GradeCompleted_Status != true) {
                                        $scope.student_data[j].assignment_Status1 = $scope.Student_Status[i].status_id;
                                        $scope.doneEditing1($scope.student_data[j]);
                                        $scope.student_data[j].ischecked = true;
                                        var now = new Date();
                                        var month = (now.getMonth() + 1);
                                        var day = now.getDate();
                                        if (month < 10)
                                            month = "0" + month;
                                        if (day < 10)
                                            day = "0" + day;
                                        $scope.student_data[j].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                        $scope.student_data[j].row_color = '##fff';
                                    }
                                }
                            }
                        }
                    }
                }
                else if (val == 'Mark') {
                    var str1 = parseFloat(defaultmark);
                    var mark1 = parseFloat($scope.student_data[0].assignmentMaxScore.split(".")[0]);

                    var maxmark = (mark1 / 10) * 100;
                    var maxmark1 = Math.round((str1 * 100) / mark1);


                    for (var j = 0; j < $scope.student_data.length; j++) {
                        if (str1 <= mark1) {
                            var t = document.getElementById('Overwrite');
                            if (t.checked == false) {

                                if ($scope.student_data[j].assignment_GradeCompleted_Status != true) {
                                    if ($scope.student_data[j].assignment_Mark == "" && $scope.student_data[j].assignment_Completed_Date == "" || $scope.student_data[j].assignment_Mark == "NA") {
                                        var mark = defaultmark + ".00"
                                        for (var i = 0; i < $scope.Grade_Scale_Data.length; i++) {
                                            if ($scope.student_data[j].sims_gb_grade_scale == $scope.Grade_Scale_Data[i].gradeGroupCode) {
                                                if (maxmark1 <= parseFloat($scope.Grade_Scale_Data[i].sims_mark_grade_high) && maxmark1 >= parseFloat($scope.Grade_Scale_Data[i].sims_mark_grade_low) && ($scope.student_data[j].assignment_Status1 == '' || $scope.student_data[j].assignment_Status1 == undefined)) {
                                                    $scope.student_data[j].assignment_final_grade_desc = $scope.Grade_Scale_Data[i].sims_mark_grade_name;
                                                    $scope.student_data[j].sims_mark_final_grade_code = $scope.Grade_Scale_Data[i].sims_mark_grade_code;
                                                    $scope.student_data[j].assignment_Comment = $scope.Grade_Scale_Data[i].sims_mark_grade_description;
                                                    $scope.student_data[j].sims_mark_grade_name = $scope.Grade_Scale_Data[i].sims_mark_grade_name;
                                                    $scope.student_data[j].assignment_Mark = defaultmark;
                                                    $scope.CheckGrade($scope.student_data[j].assignment_Mark, $scope.student_data[j]);
                                                    //$scope.student_data[j].assignment_Status1 = undefined;
                                                    $scope.student_data[j].ischecked = true;
                                                    $scope.student_data[j].ischecked1 = true;
                                                    var now = new Date();
                                                    var month = (now.getMonth() + 1);
                                                    var day = now.getDate();
                                                    if (month < 10)
                                                        month = "0" + month;
                                                    if (day < 10)
                                                        day = "0" + day;
                                                    $scope.student_data[j].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                                    $scope.student_data[j].row_color = '##fff';

                                                    break;
                                                }
                                            }
                                            else {
                                                $scope.student_data[j].ischecked = false;
                                                $scope.student_data[j].ischecked1 = false;

                                                $scope.student_data[j].assignment_final_grade_desc = "";
                                                $scope.student_data[j].assignment_Comment = "";
                                                $scope.student_data[j].assignment_Status1 = "";
                                                $scope.student_data[j].assignment_Mark = '';
                                                $scope.student_data[j].assignment_Completed_Date = '';
                                            }
                                        }
                                    }
                                }
                            }
                            else {
                                if ($scope.student_data[j].assignment_GradeCompleted_Status != true) {
                                    var mark = defaultmark + ".00"
                                    for (var i = 0; i < $scope.Grade_Scale_Data.length; i++) {
                                        if (maxmark1 <= parseFloat($scope.Grade_Scale_Data[i].sims_mark_grade_high) && maxmark1 >= parseFloat($scope.Grade_Scale_Data[i].sims_mark_grade_low) && ($scope.student_data[j].assignment_Status1 == '' || $scope.student_data[j].assignment_Status1 == undefined)) {
                                            if ($scope.student_data[j].sims_gb_grade_scale == $scope.Grade_Scale_Data[i].gradeGroupCode) {
                                                $scope.student_data[j].assignment_final_grade_desc = $scope.Grade_Scale_Data[i].sims_mark_grade_name;
                                                $scope.student_data[j].sims_mark_final_grade_code = $scope.Grade_Scale_Data[i].sims_mark_grade_code;
                                                $scope.student_data[j].assignment_Comment = $scope.Grade_Scale_Data[i].sims_mark_grade_description;
                                                $scope.student_data[j].sims_mark_grade_name = $scope.Grade_Scale_Data[i].sims_mark_grade_name;
                                                $scope.student_data[j].assignment_Mark = defaultmark;
                                                $scope.CheckGrade($scope.student_data[j].assignment_Mark, $scope.student_data[j]);
                                                //$scope.student_data[j].assignment_Status1 = undefined;
                                                var now = new Date();
                                                var month = (now.getMonth() + 1);
                                                var day = now.getDate();
                                                if (month < 10)
                                                    month = "0" + month;
                                                if (day < 10)
                                                    day = "0" + day;
                                                $scope.student_data[j].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                                $scope.student_data[j].row_color = '##fff';
                                                $scope.student_data[j].ischecked = true;
                                                $scope.student_data[j].ischecked1 = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                                else {
                                    if ($scope.student_data[j].assignment_GradeCompleted_Status != true) {
                                        $scope.student_data[j].ischecked = false;
                                        $scope.student_data[j].ischecked1 = false
                                        $scope.student_data[j].assignment_final_grade_desc = "";
                                        $scope.student_data[j].assignment_Comment = "";
                                        $scope.student_data[j].assignment_Status1 = "";
                                        $scope.student_data[j].assignment_Mark = '';
                                        $scope.student_data[j].assignment_Completed_Date = '';
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    $scope.student_data[j].ischecked = false;
                    $scope.student_data[j].ischecked1 = false;

                    $scope.student_data[j].assignment_final_grade_desc = "";
                    $scope.student_data[j].assignment_Comment = "";
                    $scope.student_data[j].assignment_Status1 = "";
                    $scope.student_data[j].assignment_Mark = '';
                    $scope.student_data[j].assignment_Completed_Date = '';

                    swal({ text: 'Enter Less Than Out Of Mark', width: 320, showCloseButton: true });
                }
            }

            $scope.ShowStudentWiseMarkEntry = function () {
                $scope.MarkEntryShow = false;
                $scope.studentwiseMarkEntryShow = true;
                $scope.grdaebookname = false;
                $scope.studentwiseMarkEntryShow2 = false;
                $scope.studentwiseMarkEntryShow3 = false;
                $scope.mark = '';
            } 

            $scope.GetStudentWiseAssignmentMarkEntry = function (info) {
                $scope.Gradebook_name = '';
                for (var i = 0; i < $scope.All_Grade_Books_names.length; i++) {
                    if ($scope.All_Grade_Books_names[i].gb_number == info.assignment_gb_number) {
                        $scope.Gradebook_name = $scope.All_Grade_Books_names[i].gb_name;
                        break;
                    }
                }

                $scope.enroll_number = info.StudEnroll;

                $scope.setdefaultvalue = info;
                $scope.studentwiseMarkEntryShow2 = false;
                $scope.studentwiseMarkEntryShow3 = false;
                $scope.mark = '';
                $scope.student_name = info.stud_Full_Name;
                $scope.busy1 = true;
                $scope.AllStudentAssignments = [];
                $http.post(ENV.apiUrl + "api/Gradebook/AllStudentAssignments", info).then(function (AllStudentAssignments) {
                    $scope.AllStudentAssignments1 = AllStudentAssignments.data;
                    $scope.busy1 = false;
                    $scope.studentwiseMarkEntryShow1 = true;
                    for (var j = 0; j < $scope.AllStudentAssignments1.length; j++) {
                        if ($scope.AllStudentAssignments1[j].assignment_Mark == 'NA' || $scope.AllStudentAssignments1[j].assignment_Comment == 'NA' || $scope.AllStudentAssignments1[j].assignment_Status == 'NA' || $scope.AllStudentAssignments1[j].assignment_Completed_Date == 'NA' || $scope.AllStudentAssignments1[j].assignment_Mark == '' || $scope.AllStudentAssignments1[j].assignment_Comment == '' || $scope.AllStudentAssignments1[j].assignment_Status == '' || $scope.AllStudentAssignments1[j].assignment_Completed_Date == '') {

                            $scope.AllStudentAssignments1[j].row_color = '#EFFBEF';
                        }
                        else {
                            $scope.AllStudentAssignments1[j].row_color = '##fff';
                        }
                    }

                    $scope.AllStudentAssignments = angular.copy($scope.AllStudentAssignments1);
                });
            }

            $scope.CheckValues1 = function (info) {
                info.ischecked1 = true;
            }

            $scope.SaveGradeBookStudentwiseMarkData = function () {

                $scope.markentryflag2 = false;

                GradeBookStudentMarkData = [];

                for (var i = 0; i < $scope.AllStudentAssignments.length; i++) {

                    if ($scope.AllStudentAssignments[i].ischecked1 == true) {
                        var data = {
                            Assignment_cur_code: $scope.AllStudentAssignments[i].assignment_cur_code,
                            Assignment_academic_year: $scope.AllStudentAssignments[i].assignment_academic_year,
                            Assignment_grade_code: $scope.AllStudentAssignments[i].assignment_grade_code,
                            Assignment_section_code: $scope.AllStudentAssignments[i].assignment_section_code,
                            Assignment_gb_number: $scope.AllStudentAssignments[i].assignment_gb_number,
                            Assignment_cat_code: $scope.AllStudentAssignments[i].assignment_cat_code,
                            Assignment_Code: $scope.AllStudentAssignments[i].assignment_Code,
                            Assignment_max_point: $scope.AllStudentAssignments[i].assignment_max_point,
                            sims_mark_final_grade_code: $scope.AllStudentAssignments[i].sims_mark_final_grade_code,
                            Assignment_Mark: $scope.AllStudentAssignments[i].assignment_Mark,
                            Assignment_max_correct: $scope.AllStudentAssignments[i].assignment_max_correct,
                            Assignment_Comment: $scope.AllStudentAssignments[i].assignment_Comment,
                            StudEnroll: $scope.AllStudentAssignments[i].stud_Enroll,
                            Assignment_Status1: $scope.AllStudentAssignments[i].assignment_Status1,
                            Assignment_Completed_Date: $scope.AllStudentAssignments[i].assignment_Completed_Date
                        };

                        GradeBookStudentMarkData.push(data);
                    }
                }

                if (GradeBookStudentMarkData.length > 0) {
                    $http.post(ENV.apiUrl + "api/Gradebook/AssignMarksToStudentsM", GradeBookStudentMarkData).then(function (AssignMarksToStudentsM) {
                        $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;


                        if ($scope.AssignMarks_ToStudentsM == true) {
                            $scope.markentryflag = false;

                            swal({
                                text: 'Student Assignment Mark Updated',
                                imageUrl: "assets/img/check.png",
                                width: 300,
                                showCloseButton: true
                            });
                        }
                        else {
                            swal({
                                text: 'Student Assignment Mark Not Updated',
                                imageUrl: "assets/img/close.png",
                                width: 300,
                                showCloseButton: true
                            });
                        }

                    });
                }
                else {
                    //$scope.insrteed = true;
                    //for (var i = 0; i < $scope.AllStudentAssignments.length; i++) {
                    //    if ($scope.AllStudentAssignments[i].sims_mark_final_grade_code != null) {
                    //        GradeBookStudentMarkData.push($scope.AllStudentAssignments[i]);
                    //        $scope.insrteed = true;
                    //    }
                    //}

                    //$http.post(ENV.apiUrl + "api/Gradebook/AssignMarksToStudentsM", GradeBookStudentMarkData).then(function (AssignMarksToStudentsM) {
                    //    $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;

                    //    if ($scope.AssignMarks_ToStudentsM == true) {
                    //        swal({
                    //            text: 'Student Assignment Mark Updated',
                    //            width: 300,
                    //            showCloseButton: true
                    //        });
                    //    }
                    //    else {
                    //        swal({
                    //            text: 'Student Assignment Mark Not Updated',
                    //            width: 300,
                    //            showCloseButton: true
                    //        });
                    //    }
                    //});


                    swal({
                        text: 'Change At Least One Record To Update Student Assignment Mark',
                        imageUrl: "assets/img/notification-alert.png",
                        width: 380,
                        showCloseButton: true
                    });
                }

            }

            $scope.ApplyDefualtValues1 = function () {

                var val = document.getElementById('cmb_term1').value;
                if (val == 'Grade') {

                    for (var i = 0; i < $scope.Grade_Scale_Data.length; i++) {
                        if ($scope.Grade_Scale_Data[i].sims_mark_grade_code == defaultgrade) {
                            for (var j = 0; j < $scope.AllStudentAssignments.length; j++) {
                                var t = document.getElementById('Overwrite1');
                                if (t.checked == false) {
                                    if ($scope.AllStudentAssignments[j].assignment_GradeCompleted_Status != true) {
                                        if ($scope.AllStudentAssignments[j].assignment_final_grade_desc == "" || $scope.AllStudentAssignments[j].assignment_Completed_Date == "" || $scope.AllStudentAssignments[j].assignment_final_grade_desc == 'NA' || $scope.AllStudentAssignments[j].assignment_Completed_Date == $scope.AllStudentAssignments[j].assignment_Completed_Date) {
                                            $scope.AllStudentAssignments[j].assignment_final_grade_desc = $scope.Grade_Scale_Data[i].sims_mark_grade_name;
                                            $scope.AllStudentAssignments[j].sims_mark_final_grade_code = $scope.Grade_Scale_Data[i].sims_mark_grade_code;
                                            $scope.AllStudentAssignments[j].assignment_Comment = $scope.Grade_Scale_Data[i].sims_mark_grade_description;
                                            $scope.AllStudentAssignments[j].assignment_Mark = $scope.Grade_Scale_Data[i].sims_mark_grade_point;
                                            $scope.AllStudentAssignments[j].ischecked = true;
                                            $scope.AllStudentAssignments[j].ischecked1 = true;
                                            var now = new Date();
                                            var month = (now.getMonth() + 1);
                                            var day = now.getDate();
                                            if (month < 10)
                                                month = "0" + month;
                                            if (day < 10)
                                                day = "0" + day;
                                            $scope.AllStudentAssignments[j].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                            $scope.AllStudentAssignments[j].row_color = '##fff';
                                        }
                                    }
                                }
                                else {
                                    if ($scope.AllStudentAssignments[j].assignment_GradeCompleted_Status != true) {
                                        $scope.AllStudentAssignments[j].assignment_final_grade_desc = $scope.Grade_Scale_Data[i].sims_mark_grade_name;
                                        $scope.AllStudentAssignments[j].sims_mark_final_grade_code = $scope.Grade_Scale_Data[i].sims_mark_grade_code;
                                        $scope.AllStudentAssignments[j].assignment_Comment = $scope.Grade_Scale_Data[i].sims_mark_grade_description;
                                        $scope.AllStudentAssignments[j].assignment_Mark = $scope.Grade_Scale_Data[i].sims_mark_grade_point;
                                        $scope.AllStudentAssignments[j].ischecked = true;
                                        $scope.AllStudentAssignments[j].ischecked1 = true;
                                        var now = new Date();
                                        var month = (now.getMonth() + 1);
                                        var day = now.getDate();
                                        if (month < 10)
                                            month = "0" + month;
                                        if (day < 10)
                                            day = "0" + day;
                                        $scope.AllStudentAssignments[j].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                        $scope.AllStudentAssignments[j].row_color = '##fff';
                                    }
                                }

                            }
                        }

                    }
                }
                else if (val == 'Comment') {

                    for (var i = 0; i < $scope.AllStudentAssignments.length; i++) {
                        var t = document.getElementById('Overwrite1');
                        if (t.checked == false) {
                            if ($scope.AllStudentAssignments[i].assignment_GradeCompleted_Status != true) {
                                if ($scope.AllStudentAssignments[i].assignment_Mark == "" || $scope.AllStudentAssignments[i].assignment_Completed_Date == "" || $scope.AllStudentAssignments[i].assignment_Mark == "NA" || $scope.AllStudentAssignments[i].assignment_Completed_Date == $scope.AllStudentAssignments[i].assignment_Completed_Date) {
                                    $scope.AllStudentAssignments[i].assignment_Comment = comment;
                                    $scope.AllStudentAssignments[i].ischecked = true;
                                    $scope.AllStudentAssignments[j].ischecked1 = true;
                                    var now = new Date();
                                    var month = (now.getMonth() + 1);
                                    var day = now.getDate();
                                    if (month < 10)
                                        month = "0" + month;
                                    if (day < 10)
                                        day = "0" + day;
                                    $scope.AllStudentAssignments[i].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                    $scope.AllStudentAssignments[i].row_color = '##fff';
                                }
                            }
                        }
                        else {
                            if ($scope.AllStudentAssignments[i].assignment_GradeCompleted_Status != true) {
                                $scope.AllStudentAssignments[i].assignment_Comment = comment;
                                $scope.AllStudentAssignments[i].ischecked = true;
                                $scope.AllStudentAssignments[j].ischecked1 = true;
                                var now = new Date();
                                var month = (now.getMonth() + 1);
                                var day = now.getDate();
                                if (month < 10)
                                    month = "0" + month;
                                if (day < 10)
                                    day = "0" + day;
                                $scope.AllStudentAssignments[i].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                $scope.AllStudentAssignments[i].row_color = '##fff';
                            }
                        }
                    }
                }
                else if (val == 'CompletedDate') {

                    for (var i = 0; i < $scope.AllStudentAssignments.length; i++) {
                        var t = document.getElementById('Overwrite1');
                        if (t.checked == false) {
                            if ($scope.AllStudentAssignments[i].assignment_GradeCompleted_Status != true) {
                                if ($scope.AllStudentAssignments[i].assignment_Completed_Date == "" || $scope.AllStudentAssignments[i].assignment_Completed_Date == $scope.AllStudentAssignments[i].assignment_Completed_Date) {
                                    $scope.AllStudentAssignments[i].assignment_Completed_Date = defaultdate;
                                    $scope.AllStudentAssignments[i].ischecked = true;
                                    $scope.AllStudentAssignments[j].ischecked1 = true;
                                    var now = new Date();
                                    var month = (now.getMonth() + 1);
                                    var day = now.getDate();
                                    if (month < 10)
                                        month = "0" + month;
                                    if (day < 10)
                                        day = "0" + day;
                                    $scope.AllStudentAssignments[i].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                    $scope.AllStudentAssignments[i].row_color = '##fff';
                                }
                            }
                        }
                        else {
                            if ($scope.AllStudentAssignments[j].assignment_GradeCompleted_Status != true) {
                                $scope.AllStudentAssignments[i].assignment_Completed_Date = defaultdate;
                                $scope.AllStudentAssignments[i].ischecked = true;
                                $scope.AllStudentAssignments[j].ischecked1 = true;

                                var now = new Date();
                                var month = (now.getMonth() + 1);
                                var day = now.getDate();
                                if (month < 10)
                                    month = "0" + month;
                                if (day < 10)
                                    day = "0" + day;
                                $scope.AllStudentAssignments[i].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                $scope.AllStudentAssignments[i].row_color = '##fff';
                            }
                        }
                    }
                }
                else if (val == 'Status') {
                    for (var i = 0; i < $scope.Student_Status.length; i++) {
                        if ($scope.Student_Status[i].status_id == defaultstatus) {

                            for (var j = 0; j < $scope.AllStudentAssignments.length; j++) {

                                var t = document.getElementById('Overwrite1');
                                if (t.checked == false) {
                                    if ($scope.AllStudentAssignments[j].assignment_GradeCompleted_Status != true) {
                                        if ($scope.AllStudentAssignments[j].assignment_Status1 == "" || $scope.AllStudentAssignments[j].assignment_Completed_Date == "" || $scope.AllStudentAssignments[j].assignment_Status1 == "NA" || $scope.AllStudentAssignments[j].assignment_Completed_Date == $scope.AllStudentAssignments[j].assignment_Completed_Date) {
                                            $scope.AllStudentAssignments[j].assignment_Status1 = $scope.Student_Status[i].status_id;
                                            $scope.AllStudentAssignments[j].ischecked = true;
                                            $scope.AllStudentAssignments[j].ischecked1 = true;
                                            var now = new Date();
                                            var month = (now.getMonth() + 1);
                                            var day = now.getDate();
                                            if (month < 10)
                                                month = "0" + month;
                                            if (day < 10)
                                                day = "0" + day;
                                            $scope.AllStudentAssignments[j].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                            $scope.AllStudentAssignments[j].row_color = '##fff';
                                        }
                                    }
                                }
                                else {
                                    if ($scope.AllStudentAssignments[j].assignment_GradeCompleted_Status != true) {
                                        $scope.AllStudentAssignments[j].assignment_Status1 = $scope.Student_Status[i].status_id;
                                        $scope.AllStudentAssignments[j].ischecked = true;
                                        $scope.AllStudentAssignments[j].ischecked1 = true;
                                        var now = new Date();
                                        var month = (now.getMonth() + 1);
                                        var day = now.getDate();
                                        if (month < 10)
                                            month = "0" + month;
                                        if (day < 10)
                                            day = "0" + day;
                                        $scope.AllStudentAssignments[j].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                        $scope.AllStudentAssignments[j].row_color = '##fff';
                                    }
                                }
                            }
                        }
                    }
                }
                else if (val == 'Mark') {

                    var str1 = parseFloat(defaultmark);
                    var mark1 = parseFloat($scope.AllStudentAssignments[0].assignmentMaxScore.split(".")[0]);

                    var maxmark = (mark1 / 10) * 100;
                    var maxmark1 = Math.round((str1 * 100) / mark1);

                    for (var j = 0; j < $scope.AllStudentAssignments.length; j++) {
                        if (maxmark1 <= maxmark) {
                            var t = document.getElementById('Overwrite1');
                            if (t.checked == false) {
                                if ($scope.AllStudentAssignments[j].assignment_GradeCompleted_Status != true) {
                                    if ($scope.AllStudentAssignments[j].assignment_Mark == "" || $scope.AllStudentAssignments[j].assignment_Completed_Date == "" || $scope.AllStudentAssignments[j].assignment_Mark == "NA" || $scope.AllStudentAssignments[j].assignment_Completed_Date == $scope.AllStudentAssignments[j].assignment_Completed_Date) {
                                        var mark = defaultmark + ".00"
                                        for (var i = 0; i < $scope.Grade_Scale_Data.length; i++) {

                                            if (maxmark1 <= parseFloat($scope.Grade_Scale_Data[i].sims_mark_grade_high) && maxmark1 >= parseFloat($scope.Grade_Scale_Data[i].sims_mark_grade_low)) {
                                                if ($scope.AllStudentAssignments[j].sims_gb_grade_scale == $scope.Grade_Scale_Data[i].gradeGroupCode) {

                                                    $scope.AllStudentAssignments[j].assignment_final_grade_desc = $scope.Grade_Scale_Data[i].sims_mark_grade_name;
                                                    $scope.AllStudentAssignments[j].sims_mark_final_grade_code = $scope.Grade_Scale_Data[i].sims_mark_grade_code;
                                                    $scope.AllStudentAssignments[j].assignment_Comment = $scope.Grade_Scale_Data[i].sims_mark_grade_description;
                                                    $scope.AllStudentAssignments[j].sims_mark_grade_name = $scope.Grade_Scale_Data[i].sims_mark_grade_name;
                                                    $scope.AllStudentAssignments[j].assignment_Mark = defaultmark;
                                                    var now = new Date();
                                                    var month = (now.getMonth() + 1);
                                                    var day = now.getDate();
                                                    if (month < 10)
                                                        month = "0" + month;
                                                    if (day < 10)
                                                        day = "0" + day;
                                                    $scope.AllStudentAssignments[j].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                                    $scope.AllStudentAssignments[j].row_color = '##fff';
                                                    break;
                                                }
                                            }
                                            else {
                                                $scope.AllStudentAssignments[j].assignment_final_grade_desc = "";
                                                $scope.AllStudentAssignments[j].assignment_Comment = "";
                                                $scope.AllStudentAssignments[j].assignment_Status1 = "";
                                                $scope.AllStudentAssignments[j].assignment_Mark = '';
                                                $scope.AllStudentAssignments[j].assignment_Completed_Date = '';
                                            }
                                        }
                                    }
                                }
                            }
                            else {
                                var mark = defaultmark + ".00"
                                for (var i = 0; i < $scope.Grade_Scale_Data.length; i++) {
                                    if (maxmark1 <= parseFloat($scope.Grade_Scale_Data[i].sims_mark_grade_high) && maxmark1 >= parseFloat($scope.Grade_Scale_Data[i].sims_mark_grade_low)) {
                                        if ($scope.AllStudentAssignments[j].assignment_GradeCompleted_Status != true) {
                                            if ($scope.AllStudentAssignments[j].sims_gb_grade_scale == $scope.Grade_Scale_Data[i].gradeGroupCode) {

                                                $scope.AllStudentAssignments[j].assignment_final_grade_desc = $scope.Grade_Scale_Data[i].sims_mark_grade_name;
                                                $scope.AllStudentAssignments[j].sims_mark_final_grade_code = $scope.Grade_Scale_Data[i].sims_mark_grade_code;
                                                $scope.AllStudentAssignments[j].assignment_Comment = $scope.Grade_Scale_Data[i].sims_mark_grade_description;
                                                $scope.AllStudentAssignments[j].sims_mark_grade_name = $scope.Grade_Scale_Data[i].sims_mark_grade_name;
                                                $scope.AllStudentAssignments[j].assignment_Mark = defaultmark;
                                                var now = new Date();
                                                var month = (now.getMonth() + 1);
                                                var day = now.getDate();
                                                if (month < 10)
                                                    month = "0" + month;
                                                if (day < 10)
                                                    day = "0" + day;
                                                $scope.AllStudentAssignments[j].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                                $scope.AllStudentAssignments[j].row_color = '##fff';
                                                break;
                                            }
                                        }
                                    }
                                    else {
                                        if ($scope.AllStudentAssignments[j].assignment_GradeCompleted_Status != true) {
                                            $scope.AllStudentAssignments[j].assignment_final_grade_desc = "";
                                            $scope.AllStudentAssignments[j].assignment_Comment = "";
                                            $scope.AllStudentAssignments[j].assignment_Status1 = "";
                                            $scope.AllStudentAssignments[j].assignment_Mark = '';
                                            $scope.AllStudentAssignments[j].assignment_Completed_Date = '';
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            $scope.AllStudentAssignments[j].ischecked = false;
                            $scope.AllStudentAssignments[j].assignment_final_grade_desc = "";
                            $scope.AllStudentAssignments[j].assignment_Comment = "";
                            $scope.AllStudentAssignments[j].assignment_Status1 = "";
                            $scope.AllStudentAssignments[j].assignment_Mark = '';
                            $scope.AllStudentAssignments[j].assignment_Completed_Date = '';
                            swal({ text: 'Enter Less Than Out Of Mark', width: 320, showCloseButton: true });
                        }
                    }
                }

            }

            $scope.SetAllDefaultvalues1 = function () {
                $scope.val['assignment_Completed_Date'] = '';
                $scope.val['assignment_Comment'] = '';
                $scope.AllStudentAssignments = angular.copy($scope.AllStudentAssignments1);
                $scope.markentryflag = false;
                $scope.markentryflag2 = false;


            }

            $scope.ShowStudentWiseMarkEntryAllCategory = function () {
                $scope.MarkEntryShow = false;
                $scope.studentwiseMarkEntryShow = false;
                $scope.grdaebookname = false;
                $scope.studentwiseMarkEntryShow2 = true;
                $scope.studentwiseMarkEntryShow3 = false;
                $scope.mark = {};

                $scope.mark = {
                    gb_term_code: $scope.assignmentsdatafoeallcategories.gb_term_code
                      , sims_grade_code: $scope.assignmentsdatafoeallcategories.grade_code
                      , sims_section_code: $scope.assignmentsdatafoeallcategories.section_code
                      , gb_number: $scope.assignmentsdatafoeallcategories.gb_number
                      , academic_year: $scope.assignmentsdatafoeallcategories.academic_year
                      , cur_code: $scope.assignmentsdatafoeallcategories.cur_code
                      , teacher_code: $scope.globals.currentUser.username

                };
                $scope.getsections($scope.edt.cur_code, $scope.edt.academic_year);

                //$timeout(function () {
                //    $("#fixTable").tableHeadFixer({ "left": 9 });
                //    //  $("#fixTable").tableHeadFixer({ 'top': 1 });

                //}, 100);
            }

            $scope.GetAllGradeSectionwiseGradebookNmae = function () {
                debugger;

                $scope.mark['academic_year'] = $scope.edt.academic_year;
                $scope.mark['cur_code'] = $scope.edt.cur_code;
                $scope.mark['teacher_code'] = $scope.globals.currentUser.username;
                $http.post(ENV.apiUrl + "api/Gradebook/AllGradeBookNamegradesectionwise", $scope.mark).then(function (AllGradeBookNamegradesectionwise) {
                    $scope.AllGradeBookNamegradesectionwise = AllGradeBookNamegradesectionwise.data;

                });
            }

            $scope.GetAllGradeSectionwiseGradebookNmae1 = function () {

                $scope.studentwiseMarkEntryShow3 = false;
                $scope.SkipStudentFromAssignments = false;
                $scope.studentwiseMarkEntryShow2 = true;
                $scope.isCondensed = false;
                $scope.classdefault1 = 'col-md-10';

                $scope.combovalues1 = [{ name: 'Mark', value: 'Mark' },
                                       { name: 'Grade', value: 'Grade' }];
                $scope.busy2 = true;
                $scope.assignment1 = [];
                $scope.Assignment = [];
                $scope.studentlist = [];
                $scope.studentlist1 = [];
                $scope.assign1 = [];
                $scope.assign = [];
                $scope.categories1 = [];
                $scope.count1 = 0;
                $scope.categories = [];
                $scope.student_assign = [];
                $scope.studentlist = [];
                $scope.count = 0;
                $scope.insertassignmentdata = [];
                $http.post(ENV.apiUrl + "api/Gradebook/AllGradeBookNamegradesectionwiseassignment", $scope.mark).then(function (AllGradeBookNamegradesectionwiseassignment) {
                    $scope.AllGradeBookassignment = AllGradeBookNamegradesectionwiseassignment.data;
                    $scope.busy2 = false;
                    if ($scope.AllGradeBookassignment[0].categories == undefined) {
                        $scope.AllGradeBookassignment[0].categories = [];
                    }
                    for (var j = 0; j < $scope.AllGradeBookassignment[0].categories.length; j++) {

                        if ($scope.AllGradeBookassignment[0].categories[j].assignMents == undefined) {
                            $scope.AllGradeBookassignment[0].categories[j].assignMents = [];
                        }
                        $scope.count1 = parseInt($scope.count1) + 1;
                        $scope.categories1.push($scope.AllGradeBookassignment[0].categories[j]);
                        for (var k = 0; k < $scope.AllGradeBookassignment[0].categories[j].assignMents.length; k++) {
                            $scope.count = parseInt($scope.count) + 1;
                            $scope.AllGradeBookassignment[0].categories[j].count1 = parseInt($scope.AllGradeBookassignment[0].categories[j].assignMents.length);
                            $scope.assignment1.push($scope.AllGradeBookassignment[0].categories[j].assignMents[k]);
                        }
                    }

                    $scope.Assignment = angular.copy($scope.assignment1);
                    $scope.categories = angular.copy($scope.categories1);
                    $scope.student_assign = angular.copy($scope.AllGradeBookassignment[1]);
                    debugger;
                    $timeout(function () {
                   
                        $("#fixTable").tableHeadFixer({ 'top': 1, 'left':2, 'z-index': 10000 });
                        //  $("#fixTable").tableHeadFixer({ 'top': 1 });

                    }, 3000);

                    $scope.AllGradeBookassignment[0].count = parseInt($scope.student_assign[0].assignMents.length - 1);

                    if ($scope.skipstudent == true) {
                        $scope.studentwiseMarkEntryShow2 = false;
                        $scope.studentwiseMarkEntryShow3 = false;
                        $scope.SkipStudentFromAssignments = true;
                        $scope.classdefault1 = 'col-md-12';

                    } else {
                        $scope.studentwiseMarkEntryShow2 = false;
                        $scope.SkipStudentFromAssignments = false;
                        $scope.studentwiseMarkEntryShow3 = true;
                        $scope.classdefault1 = 'col-md-12';
                    }

                    $("#Table9").tableHeadFixer({ 'left': 1 });
                });
            }

            $scope.SetdefaultvalueMark1 = function (str) {

                if (parseInt(str) <= parseInt($scope.assignment_max_score_for_check)) {
                    defaultmark1 = str;
                }
                else {
                    defaultmark1 = '';
                    $scope.val.assignment_mark = '';
                }
            }

            $scope.Setdefaultvalueassignment_final_grade_desc1 = function (str) {

                defaultgrade1 = str;
            }

            $scope.ChechDefaultvalues1 = function (str) {
                if (str == 'Grade') {
                    $scope.Grade = true;
                    $scope.Mark = false;
                }
                if (str == 'Mark') {
                    $scope.Grade = false;
                    $scope.Mark = true;
                }
            }

            $scope.ApplyDefualtValues2 = function () {

                var val = document.getElementById('cmb_term2').value;
                if (val == 'Grade') {
                    for (var i = 0; i < $scope.student_assign.length; i++) {
                        for (var j = 0; j < $scope.student_assign[i].assignMents.length; j++) {
                            if ($scope.student_assign[i].assignMents[j].assignment_Score_type == 'G') {
                                {
                                    for (var k = 0; k < $scope.Grade_Scale_Data.length; k++) {
                                        if ($scope.Grade_Scale_Data[k].sims_mark_grade_code == defaultgrade1) {

                                            var t = document.getElementById('Overwrite2');
                                            if (t.checked == false) {
                                                if ($scope.student_assign[i].assignMents[j].assignment_GradeCompleted_Status != true) {
                                                    if ($scope.student_assign[i].assignMents[j].assignment_final_grade_desc == "" || $scope.student_assign[i].assignMents[j].assignment_final_grade_desc == 'NA') {
                                                        $scope.student_assign[i].assignMents[j].assignment_final_grade_desc = $scope.Grade_Scale_Data[k].sims_mark_grade_name;
                                                        $scope.student_assign[i].assignMents[j].sims_mark_final_grade_code = $scope.Grade_Scale_Data[k].sims_mark_grade_code;
                                                        $scope.student_assign[i].assignMents[j].assignment_Comment = $scope.Grade_Scale_Data[k].sims_mark_grade_description;
                                                        $scope.student_assign[i].assignMents[j].assignment_Mark = $scope.Grade_Scale_Data[k].$scope.Grade_Scale_Data[k].sims_mark_grade_high;
                                                        $scope.student_assign[i].assignMents[j].status = true;
                                                        var now = new Date();
                                                        var month = (now.getMonth() + 1);
                                                        var day = now.getDate();
                                                        if (month < 10)
                                                            month = "0" + month;
                                                        if (day < 10)
                                                            day = "0" + day;
                                                        $scope.student_assign[i].assignMents[j].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                                    }
                                                }
                                            }
                                            else {
                                                if ($scope.student_assign[i].assignMents[j].assignment_GradeCompleted_Status != true) {
                                                    $scope.student_assign[i].assignMents[j].assignment_final_grade_desc = $scope.Grade_Scale_Data[k].sims_mark_grade_name;
                                                    $scope.student_assign[i].assignMents[j].sims_mark_final_grade_code = $scope.Grade_Scale_Data[k].sims_mark_grade_code;
                                                    $scope.student_assign[i].assignMents[j].assignment_Comment = $scope.Grade_Scale_Data[k].sims_mark_grade_description;
                                                    $scope.student_assign[i].assignMents[j].assignment_Mark = $scope.Grade_Scale_Data[k].sims_mark_grade_high;
                                                    $scope.student_assign[i].assignMents[j].status = true;
                                                    var now = new Date();
                                                    var month = (now.getMonth() + 1);
                                                    var day = now.getDate();
                                                    if (month < 10)
                                                        month = "0" + month;
                                                    if (day < 10)
                                                        day = "0" + day;
                                                    $scope.student_assign[i].assignMents[j].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;

                                                }
                                            }

                                        }
                                    }

                                }
                            }
                        }
                    }
                }

                else if (val == 'Mark') {



                    for (var i = 0; i < $scope.student_assign.length; i++) {
                        for (var j = 0; j < $scope.student_assign[i].assignMents.length; j++) {
                            if ($scope.student_assign[i].assignMents[j].assignment_GradeCompleted_Status != true) {
                                if ($scope.student_assign[i].assignMents[j].assignment_Score_type == 'M') {

                                    var str1 = parseFloat(defaultmark1);
                                    var mark1 = parseFloat($scope.student_assign[0].assignMents[0].assignmentMaxScore.split(".")[0]);

                                    var maxmark = (mark1 / 10) * 100;
                                    var maxmark1 = Math.round((str1 * 100) / mark1);

                                    if (str1 <= mark1) {
                                        var t = document.getElementById('Overwrite2');
                                        if (t.checked == false) {
                                            if ($scope.student_assign[i].assignMents[j].assignment_Mark == "" || $scope.student_assign[i].assignMents[j].assignment_Mark == "NA") {
                                                var mark = defaultmark1 + ".00"
                                                for (var k = 0; k < $scope.Grade_Scale_Data.length; k++) {
                                                    if (maxmark1 <= parseFloat($scope.Grade_Scale_Data[k].sims_mark_grade_high) && maxmark1 >= parseFloat($scope.Grade_Scale_Data[k].sims_mark_grade_low)) {
                                                        if ($scope.student_assign[i].assignMents[j].sims_gb_grade_scale == $scope.Grade_Scale_Data[k].gradeGroupCode) {
                                                            $scope.student_assign[i].assignMents[j].assignment_final_grade_desc = $scope.Grade_Scale_Data[k].sims_mark_grade_name;
                                                            $scope.student_assign[i].assignMents[j].sims_mark_final_grade_code = $scope.Grade_Scale_Data[k].sims_mark_grade_code;
                                                            $scope.student_assign[i].assignMents[j].assignment_Comment = $scope.Grade_Scale_Data[k].sims_mark_grade_description;
                                                            $scope.student_assign[i].assignMents[j].sims_mark_grade_name = $scope.Grade_Scale_Data[k].sims_mark_grade_name;
                                                            $scope.student_assign[i].assignMents[j].assignment_Mark = defaultmark1;
                                                            $scope.student_assign[i].assignMents[j].status = true;
                                                            var now = new Date();
                                                            var month = (now.getMonth() + 1);
                                                            var day = now.getDate();
                                                            if (month < 10)
                                                                month = "0" + month;
                                                            if (day < 10)
                                                                day = "0" + day;
                                                            $scope.student_assign[i].assignMents[j].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                                            break;
                                                        }
                                                    }
                                                    else {

                                                        $scope.student_assign[i].assignMents[j].status = false;
                                                        $scope.student_assign[i].assignMents[j].assignment_final_grade_desc = '';
                                                        $scope.student_assign[i].assignMents[j].sims_mark_final_grade_code = '';
                                                        $scope.student_assign[i].assignMents[j].assignment_Comment = '';
                                                        $scope.student_assign[i].assignMents[j].sims_mark_grade_name = '';
                                                        $scope.student_assign[i].assignMents[j].assignment_Mark = '';
                                                        $scope.student_assign[i].assignMents[j].assignment_Completed_Date = '';
                                                    }

                                                }
                                            }
                                        }
                                        else {
                                            for (var k = 0; k < $scope.Grade_Scale_Data.length; k++) {
                                                if (maxmark1 <= parseFloat($scope.Grade_Scale_Data[k].sims_mark_grade_high) && maxmark1 >= parseFloat($scope.Grade_Scale_Data[k].sims_mark_grade_low)) {
                                                    if ($scope.student_assign[i].assignMents[j].sims_gb_grade_scale == $scope.Grade_Scale_Data[k].gradeGroupCode) {
                                                        $scope.student_assign[i].assignMents[j].assignment_final_grade_desc = $scope.Grade_Scale_Data[k].sims_mark_grade_name;
                                                        $scope.student_assign[i].assignMents[j].sims_mark_final_grade_code = $scope.Grade_Scale_Data[k].sims_mark_grade_code;
                                                        $scope.student_assign[i].assignMents[j].assignment_Comment = $scope.Grade_Scale_Data[k].sims_mark_grade_description;
                                                        $scope.student_assign[i].assignMents[j].sims_mark_grade_name = $scope.Grade_Scale_Data[k].sims_mark_grade_name;
                                                        $scope.student_assign[i].assignMents[j].assignment_Mark = defaultmark1;
                                                        $scope.student_assign[i].assignMents[j].status = true;
                                                        var now = new Date();
                                                        var month = (now.getMonth() + 1);
                                                        var day = now.getDate();
                                                        if (month < 10)
                                                            month = "0" + month;
                                                        if (day < 10)
                                                            day = "0" + day;
                                                        $scope.student_assign[i].assignMents[j].assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                                        break;
                                                    }
                                                }
                                                else {

                                                    $scope.student_assign[i].assignMents[j].status = false;
                                                    $scope.student_assign[i].assignMents[j].assignment_final_grade_desc = '';
                                                    $scope.student_assign[i].assignMents[j].sims_mark_final_grade_code = '';
                                                    $scope.student_assign[i].assignMents[j].assignment_Comment = '';
                                                    $scope.student_assign[i].assignMents[j].sims_mark_grade_name = '';
                                                    $scope.student_assign[i].assignMents[j].assignment_Mark = '';
                                                    $scope.student_assign[i].assignMents[j].assignment_Completed_Date = '';
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        $scope.student_assign[i].assignMents[j].ischecked = false;
                                        $scope.student_assign[i].assignMents[j].assignment_final_grade_desc = "";
                                        $scope.student_assign[i].assignMents[j].assignment_Comment = "";
                                        $scope.student_assign[i].assignMents[j].assignment_Status1 = "";
                                        $scope.student_assign[i].assignMents[j].assignment_Mark = '';
                                        $scope.student_assign[i].assignMents[j].assignment_Completed_Date = '';
                                        swal({ text: 'Enter Less Than Out Of Mark', width: 320, showCloseButton: true });
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $scope.ResetGradeData = function () {
                $scope.mark = '';
            }

            $scope.SkipStudentfromGradeBooAssignmentData = function () {
                $scope.SkipStudentcategoryWiseshow1 = false;
                $scope.SkipStudentcategoryWiseshow2 = false;
                $scope.SkipStudentFromAssignments = false;
                $scope.SkipstudentSubjectWise1 = false;
                $scope.SkipStudentFromSubjectAssignments = false;
                $scope.studentwiseMarkEntryShow = false;
                $scope.studentwiseMarkEntryShow1 = false;
                $scope.studentwiseMarkEntryShow2 = true;
                $scope.studentwiseMarkEntryShow3 = false;
                $scope.GradeBookCreation = false;
                $scope.GradeBookCategory = false;
                $scope.GradeBookAssignment = false;
                $scope.GradeSchemeShow = false;
                $scope.MarkEntryShow = false;
                $scope.GradebookComment = false;
                $scope.grdaebookname = false;
                $scope.SubjectAttribute = false;
                $scope.updateshow = false;
                $scope.editmode = false;
                $scope.updateassign = false;

                $scope.skipstudent = true;
            }

            $scope.SaveGradeBookStudentwiseMarkDataAllAssignments = function () {

                $scope.markentryflag1 = false;

                $scope.GradeBookStudentMarkData1 = [];
                for (var i = 0; i < $scope.student_assign.length; i++) {
                    for (var j = 0; j < $scope.student_assign[i].assignMents.length; j++) {
                        //if ($scope.student_assign[i].assignMents[j].status == true && $scope.student_assign[i].assignMents[j].assignment_Mark != '') {
                            var data = {
                                Assignment_cur_code: $scope.student_assign[i].assignMents[j].assignment_cur_code,
                                Assignment_academic_year: $scope.student_assign[i].assignMents[j].assignment_academic_year,
                                Assignment_grade_code: $scope.student_assign[i].assignMents[j].assignment_grade_code,
                                Assignment_section_code: $scope.student_assign[i].assignMents[j].assignment_section_code,
                                Assignment_gb_number: $scope.student_assign[i].assignMents[j].assignment_gb_number,
                                Assignment_cat_code: $scope.student_assign[i].assignMents[j].assignment_cat_code,
                                Assignment_Code: $scope.student_assign[i].assignMents[j].assignment_Code,
                                Assignment_max_point: $scope.student_assign[i].assignMents[j].assignmentMaxScore,
                                sims_mark_final_grade_code: $scope.student_assign[i].assignMents[j].sims_mark_final_grade_code,
                                Assignment_Mark: $scope.student_assign[i].assignMents[j].assignment_Mark,
                                Assignment_Comment: $scope.student_assign[i].assignMents[j].assignment_Comment,
                                StudEnroll: $scope.student_assign[i].assignMents[j].stud_Enroll,
                                AssignmentMaxScore_correct: $scope.student_assign[i].assignMents[j].assignmentMaxScore_correct,
                                Assignment_Completed_Date: $scope.student_assign[i].assignMents[j].assignment_Completed_Date,
                                Assignment_Status1: ''
                            };

                            $scope.GradeBookStudentMarkData1.push(data);
                       // }
                    }
                }

                if ($scope.GradeBookStudentMarkData1.length > 0) {
                    $http.post(ENV.apiUrl + "api/Gradebook/AssignMarksToStudentsM", $scope.GradeBookStudentMarkData1).then(function (AssignMarksToStudentsM) {
                        $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;


                        if ($scope.AssignMarks_ToStudentsM == true) {
                            $scope.markentryflag = false;
                            swal({
                                text: 'Student Assignment Mark Updated',
                                imageUrl: "assets/img/check.png",
                                width: 300,
                                showCloseButton: true
                            });
                            $scope.classdefault1 = 'col-md-12';
                            $scope.GetAllGradeSectionwiseGradebookNmae1();
                        }
                        else {
                            swal({
                                text: 'Student Assignment Mark Not Updated',
                                imageUrl: "assets/img/close.png",
                                width: 300,
                                showCloseButton: true
                            });
                        }

                    });
                }
                else {

                    swal({
                        text: 'Change At Least One Record To Update Student Assignment Mark',
                        imageUrl: "assets/img/notification-alert.png",
                        width: 380,
                        showCloseButton: true
                    });
                }

            }

            $scope.SaveGradeBookStudentwiseMarkAll_AssignmentsChange_Mark = function (info) {

                $scope.markentryflag1 = true;

                var str1 = parseFloat(info.assignment_Mark);
                var mark1 = parseFloat(info.assignmentMaxScore.split(".")[0]);

                var maxmark = (mark1 / 10) * 100;
                var maxmark1 = Math.round((str1 * 100) / mark1);

                if (str1 <= mark1) {

                    var mark = parseFloat(info.assignment_Mark + ".00")
                    for (var i = 0; i < $scope.Grade_Scale_Data.length; i++) {
                        if (maxmark1 <= parseFloat($scope.Grade_Scale_Data[i].sims_mark_grade_high) && maxmark1 >= parseFloat($scope.Grade_Scale_Data[i].sims_mark_grade_low)) {
                            if (info.sims_gb_grade_scale == $scope.Grade_Scale_Data[i].gradeGroupCode) {
                                info.assignment_final_grade_desc = $scope.Grade_Scale_Data[i].sims_mark_grade_name;
                                info.sims_mark_final_grade_code = $scope.Grade_Scale_Data[i].sims_mark_grade_code;
                                info.assignment_Comment = $scope.Grade_Scale_Data[i].sims_mark_grade_description;
                                info.sims_mark_grade_name = $scope.Grade_Scale_Data[i].sims_mark_grade_name;
                                info.status = true;

                                info.assignment_Mark = mark;
                                var now = new Date();
                                var month = (now.getMonth() + 1);
                                var day = now.getDate();
                                if (month < 10)
                                    month = "0" + month;
                                if (day < 10)
                                    day = "0" + day;
                                info.assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                break;
                            }

                        }
                        else {
                            info.status = false;
                            info.assignment_final_grade_desc = "";
                            info.sims_mark_final_grade_code = "";
                            info.assignment_Comment = "";
                            info.sims_mark_grade_name = "";
                            info.assignment_Mark = '';
                            info.assignment_Completed_Date = '';
                        }

                    }

                }
                else {

                    info.status = false;
                    info.assignment_final_grade_desc = "";
                    info.sims_mark_final_grade_code = "";
                    info.assignment_Comment = "";
                    info.sims_mark_grade_name = "";
                    // info.assignment_Mark = '';
                    info.assignment_Completed_Date = '';
                    if (parseFloat(str1) == undefined || parseFloat(str1) == '')
                        info.assignment_Mark = '';
                    else if (parseFloat(str1) >= parseFloat(mark1)) {
                        swal({ text: 'Enter Less Than Out Of Mark', width: 320, showCloseButton: true });
                        info.assignment_Mark = '';
                    }
                }


            }

            $scope.SaveGradeBookStudentwiseMarkAll_AssignmentsChange_grade = function (info) {

                $scope.flag = false;

                $scope.markentryflag1 = true;

                if (info.assignment_Score_type == 'G') {
                    {
                        for (var k = 0; k < $scope.Grade_Scale_Data.length; k++) {
                            if ($scope.Grade_Scale_Data[k].sims_mark_grade_description == info.assignment_final_grade_desc) {
                                info.assignment_final_grade_desc = $scope.Grade_Scale_Data[k].sims_mark_grade_name;
                                info.sims_mark_final_grade_code = $scope.Grade_Scale_Data[k].sims_mark_grade_code;
                                info.assignment_Comment = $scope.Grade_Scale_Data[k].sims_mark_grade_description;
                                info.assignment_Mark = $scope.Grade_Scale_Data[k].$scope.Grade_Scale_Data[k].sims_mark_grade_high;
                                info.status = true;
                                $scope.flag = true;

                                var now = new Date();
                                var month = (now.getMonth() + 1);
                                var day = now.getDate();
                                if (month < 10)
                                    month = "0" + month;
                                if (day < 10)
                                    day = "0" + day;
                                info.assignment_Completed_Date = now.getFullYear() + '/' + month + '/' + day;
                                break;
                            }
                        }
                    }


                    if ($scope.flag == false) {
                        $scope.flag = false;
                        info.assignment_final_grade_desc = '';
                        info.sims_mark_final_grade_code = '';
                        info.assignment_Comment = '';
                        info.assignment_Mark = '';
                        info.status = false;
                        info.assignment_Completed_Date = '';
                        swal({ text: 'This Grade Is Not Available Enter Proper Grade', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    }
                }

            }

            $scope.SetAllDefaultvaluesAllAssignments = function () {

                $scope.markentryflag1 = false;


                $scope.Assignment = angular.copy($scope.assignment1);
                $scope.categories = angular.copy($scope.categories1);
                $scope.student_assign = angular.copy($scope.AllGradeBookassignment[1]);

            }

            $scope.InsertstudentintoAsssignments = function () {

                if ($scope.insertassignmentdata.length > 0) {

                    $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.insertassignmentdata).then(function (AssignMarksToStudentsM) {
                        $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                        $scope.insertassignmentdata = [];
                        if ($scope.AssignMarks_ToStudentsM == true) {
                            swal({
                                text: 'Assignment Mapped Successfully',
                                imageUrl: "assets/img/check.png",
                                width: 320,
                                showCloseButton: true
                            });
                            $scope.GetAllGradeSectionwiseGradebookNmae1();
                        }
                        else {
                            swal({
                                text: 'Assignment Not Mapped',
                                imageUrl: "assets/img/close.png",
                                width: 300,
                                showCloseButton: true
                            });
                        }

                    });

                }

            }

            $scope.SelectAllStudentForskip = function (str, str1, str2, str3) {

                $scope.dataAssignment = [];
                $scope.nodataAssignment = [];
                $scope.Assignstudentrecord = false;
                $scope.nodeleterecord = false;
                $scope.deletestudent = false;

                var check = document.getElementById(str);
                if (check.checked == false) {
                    for (var i = 0; i < $scope.student_assign.length; i++) {
                        for (var j = 0; j < $scope.student_assign[i].assignMents.length; j++) {
                            if ($scope.student_assign[i].assignMents[j].assignment_Code1 == str1 && $scope.student_assign[i].assignMents[j].assignment_cat_code1 == str2 && $scope.student_assign[i].assignMents[j].assignment_gb_number == str3) {
                                if ($scope.student_assign[i].assignMents[j].assignment_Mark == "" || $scope.student_assign[i].assignMents[j].assignment_Mark == 'NA') {
                                    $scope.nodeleterecord = true;
                                    $scope.student_assign[i].assignMents[j].insert = check.checked;
                                    var dataobj1 = {
                                        Assignment_cur_code: $scope.student_assign[i].assignMents[j].assignment_cur_code
                                    , Assignment_academic_year: $scope.student_assign[i].assignMents[j].assignment_academic_year
                                    , Assignment_grade_code: $scope.student_assign[i].assignMents[j].assignment_grade_code
                                    , Assignment_section_code: $scope.student_assign[i].assignMents[j].assignment_section_code
                                    , Assignment_gb_number: $scope.student_assign[i].assignMents[j].assignment_gb_number
                                    , Assignment_cat_code: $scope.student_assign[i].assignMents[j].assignment_cat_code1
                                    , Assignment_Code: $scope.student_assign[i].assignMents[j].assignment_Code1
                                    , StudEnroll: $scope.student_assign[i].assignMents[j].stud_Enroll
                                    , term_code: $scope.mark.gb_term_code
                                    , Assignment_subject_code: $scope.student_assign[i].assignMents[j].assignment_subject_code
                                    , OPR: 'CD'
                                    };

                                    $scope.dataAssignment.push(dataobj1);
                                }
                                else {
                                    $scope.deletestudent = true;
                                    var dataobj1 = {
                                        stud_Enroll: $scope.student_assign[i].assignMents[j].stud_Enroll,
                                        stud_Full_Name: $scope.student_assign[i].stud_Full_Name,
                                    };
                                    $scope.nodataAssignment.push(dataobj1);
                                }
                            }
                        }
                    }


                }
                else {
                    for (var i = 0; i < $scope.student_assign.length; i++) {
                        for (var j = 0; j < $scope.student_assign[i].assignMents.length; j++) {
                            if ($scope.student_assign[i].assignMents[j].assignment_Code1 == str1 && $scope.student_assign[i].assignMents[j].assignment_cat_code1 == str2) {
                                $scope.Assignstudentrecord = true;
                                $scope.student_assign[i].assignMents[j].insert = check.checked;

                                var dataobj1 = {
                                    Assignment_cur_code: $scope.student_assign[i].assignMents[j].assignment_cur_code
                                   , Assignment_academic_year: $scope.student_assign[i].assignMents[j].assignment_academic_year
                                   , Assignment_grade_code: $scope.student_assign[i].assignMents[j].assignment_grade_code
                                   , Assignment_section_code: $scope.student_assign[i].assignMents[j].assignment_section_code
                                   , Assignment_gb_number: $scope.student_assign[i].assignMents[j].assignment_gb_number
                                   , Assignment_cat_code: $scope.student_assign[i].assignMents[j].assignment_cat_code1
                                   , Assignment_Code: $scope.student_assign[i].assignMents[j].assignment_Code1
                                   , StudEnroll: $scope.student_assign[i].assignMents[j].stud_Enroll
                                   , term_code: $scope.mark.gb_term_code
                                    , Assignment_subject_code: $scope.student_assign[i].assignMents[j].assignment_subject_code
                                   , OPR: 'CI'
                                };

                                $scope.insertassignmentdata.push(dataobj1);
                                var dataobj1 = {};
                                break;
                            }
                        }
                    }
                }

                if ($scope.deletestudent == false && check.checked == false && $scope.nodeleterecord == true) {

                    $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.dataAssignment).then(function (AssignMarksToStudentsM) {
                        $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                        if ($scope.AssignMarks_ToStudentsM == true) {
                            swal({
                                text: 'Student Skiped Successfully From This Assignment',
                                imageUrl: "assets/img/check.png",
                                width: 320,
                                showCloseButton: true
                            });
                            $scope.classdefault1 = 'col-md-12';
                            datasend = [];
                            $scope.studentwiseMarkEntryShow2 = false;
                            $scope.GetAllGradeSectionwiseGradebookNmae1();
                        }
                        else {
                            swal({
                                text: 'Student Not Skiped From This Assignment',
                                imageUrl: "assets/img/close.png",
                                width: 300,
                                showCloseButton: true
                            });
                        }

                    });

                }
                else if ($scope.deletestudent == true && check.checked == false && $scope.nodeleterecord == true && $scope.Assignstudentrecord == false) {

                    $scope.datadeleteobject2 = $scope.nodataAssignment;
                    $scope.nodataAssignment = [];
                    $('#Div11').modal({ backdrop: "static" });


                }


            }

            $scope.btn_Ok_Click3 = function () {

                for (var i = 0; i < $scope.student_assign.length; i++) {
                    for (var j = 0; j < $scope.student_assign[i].assignMents.length; j++) {
                        if ($scope.student_assign[i].assignMents[j].assignment_Code1 == $scope.dataAssignment[0].Assignment_Code && $scope.student_assign[i].assignMents[j].assignment_cat_code1 == $scope.dataAssignment[0].Assignment_cat_code && $scope.student_assign[i].assignMents[j].assignment_gb_number == $scope.dataAssignment[0].Assignment_gb_number) {
                            var dataobj1 = {
                                Assignment_cur_code: $scope.student_assign[i].assignMents[j].assignment_cur_code
                               , Assignment_academic_year: $scope.student_assign[i].assignMents[j].assignment_academic_year
                               , Assignment_grade_code: $scope.student_assign[i].assignMents[j].assignment_grade_code
                               , Assignment_section_code: $scope.student_assign[i].assignMents[j].assignment_section_code
                               , Assignment_gb_number: $scope.student_assign[i].assignMents[j].assignment_gb_number
                               , Assignment_cat_code: $scope.student_assign[i].assignMents[j].assignment_cat_code1
                               , Assignment_Code: $scope.student_assign[i].assignMents[j].assignment_Code1
                               , StudEnroll: $scope.student_assign[i].stud_Enroll
                                   , term_code: $scope.mark.gb_term_code
                                , Assignment_subject_code: $scope.student_assign[i].assignMents[j].assignment_subject_code
                                , OPR: 'CD'
                            };

                            $scope.dataAssignment.push(dataobj1);
                            dataobj1 = {}
                        }
                    }
                }


                $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.dataAssignment).then(function (AssignMarksToStudentsM) {
                    $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                    if ($scope.AssignMarks_ToStudentsM == true) {
                        swal({
                            text: 'Student Skiped Successfully From This Assignment',
                            imageUrl: "assets/img/check.png",
                            width: 320,
                            showCloseButton: true
                        });
                        $scope.classdefault1 = 'col-md-12';
                        $scope.GetAllGradeSectionwiseGradebookNmae1();
                    }
                    else {
                        swal({
                            text: 'Student Not Skiped From This Assignment',
                            imageUrl: "assets/img/close.png",
                            width: 300,
                            showCloseButton: true
                        });
                    }

                });

            }

            $scope.btn_cancel_Click3 = function () {

                for (var i = 0; i < $scope.student_assign.length; i++) {
                    for (var j = 0; j < $scope.student_assign[i].assignMents.length; j++) {
                        if ($scope.student_assign[i].assignMents[j].assignment_Code1 == $scope.dataAssignment[0].Assignment_Code && $scope.student_assign[i].assignMents[j].assignment_cat_code1 == $scope.dataAssignment[0].Assignment_cat_code && $scope.student_assign[i].assignMents[j].assignment_gb_number == $scope.dataAssignment[0].Assignment_gb_number) {
                            if ($scope.student_assign[i].assignMents[j].assignment_Mark == "" || $scope.student_assign[i].assignMents[j].assignment_Mark == 'NA') {
                                $scope.nodeleterecord = true;
                                $scope.student_assign[i].assignMents[j].insert = false;
                                var dataobj1 = {
                                    Assignment_cur_code: $scope.student_assign[i].assignMents[j].assignment_cur_code
                                , Assignment_academic_year: $scope.student_assign[i].assignMents[j].assignment_academic_year
                                , Assignment_grade_code: $scope.student_assign[i].assignMents[j].assignment_grade_code
                                , Assignment_section_code: $scope.student_assign[i].assignMents[j].assignment_section_code
                                , Assignment_gb_number: $scope.student_assign[i].assignMents[j].assignment_gb_number
                                , Assignment_cat_code: $scope.student_assign[i].assignMents[j].assignment_cat_code1
                                , Assignment_Code: $scope.student_assign[i].assignMents[j].assignment_Code1
                                , StudEnroll: $scope.student_assign[i].stud_Enroll,
                                    term_code: $scope.mark.gb_term_code
                                   , Assignment_subject_code: $scope.student_assign[i].assignMents[j].assignment_subject_code
                                , OPR: 'CD'
                                };

                                $scope.dataAssignment.push(dataobj1);
                                dataobj1 = {};
                            }

                        }
                    }
                }

                $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.dataAssignment).then(function (AssignMarksToStudentsM) {
                    $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                    if ($scope.AssignMarks_ToStudentsM == true) {
                        swal({
                            text: 'Student Skiped Successfully From This Assignment',
                            imageUrl: "assets/img/check.png",
                            width: 320,
                            showCloseButton: true
                        });
                        $scope.classdefault1 = 'col-md-12';
                        datasend = [];
                        $scope.studentwiseMarkEntryShow2 = false;
                        $scope.GetAllGradeSectionwiseGradebookNmae1();
                    }
                    else {
                        swal({
                            text: 'Student Not Skiped From This Assignment',
                            imageUrl: "assets/img/close.png",
                            width: 300,
                            showCloseButton: true
                        });
                    }

                });

            }

            $scope.btn_Ok_Click4 = function () {

                for (var i = 0; i < $scope.student_assign.length; i++) {
                    for (var j = 0; j < $scope.student_assign[i].assignMents.length; j++) {
                        if ($scope.student_assign[i].stud_Enroll == $scope.deleterecorddataAssignment[0].StudEnroll) {
                            var dataobj1 = {
                                Assignment_cur_code: $scope.student_assign[i].assignMents[j].assignment_cur_code
                               , Assignment_academic_year: $scope.student_assign[i].assignMents[j].assignment_academic_year
                               , Assignment_grade_code: $scope.student_assign[i].assignMents[j].assignment_grade_code
                               , Assignment_section_code: $scope.student_assign[i].assignMents[j].assignment_section_code
                               , Assignment_gb_number: $scope.student_assign[i].assignMents[j].assignment_gb_number
                               , Assignment_cat_code: $scope.student_assign[i].assignMents[j].assignment_cat_code1
                               , Assignment_Code: $scope.student_assign[i].assignMents[j].assignment_Code1
                               , StudEnroll: $scope.student_assign[i].stud_Enroll
                                , term_code: $scope.mark.gb_term_code
                                , Assignment_subject_code: $scope.student_assign[i].assignMents[j].assignment_subject_code
                                , OPR: 'CD'
                            };

                            $scope.dataAssignment.push(dataobj1);
                            dataobj1 = {}
                        }
                    }
                }


                $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.dataAssignment).then(function (AssignMarksToStudentsM) {
                    $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                    if ($scope.AssignMarks_ToStudentsM == true) {
                        swal({
                            text: 'Student Skiped Successfully From This Assignment',
                            imageUrl: "assets/img/check.png",
                            width: 320,
                            showCloseButton: true
                        });
                        $scope.classdefault1 = 'col-md-12';
                        $scope.GetAllGradeSectionwiseGradebookNmae1();
                    }
                    else {
                        swal({
                            text: 'Student Not Skiped From This Assignment',
                            imageUrl: "assets/img/close.png",
                            width: 300,
                            showCloseButton: true
                        });
                    }

                });

            }

            $scope.btn_cancel_Click4 = function () {

                for (var i = 0; i < $scope.student_assign.length; i++) {
                    for (var j = 0; j < $scope.student_assign[i].assignMents.length; j++) {
                        if ($scope.student_assign[i].stud_Enroll == $scope.deleterecorddataAssignment[0].StudEnroll) {
                            if ($scope.student_assign[i].assignMents[j].assignment_Code1 == $scope.dataAssignment[0].Assignment_Code && $scope.student_assign[i].assignMents[j].assignment_cat_code1 == $scope.dataAssignment[0].Assignment_cat_code && $scope.student_assign[i].assignMents[j].assignment_gb_number == $scope.dataAssignment[0].Assignment_gb_number) {
                                if ($scope.student_assign[i].assignMents[j].assignment_Mark == "" || $scope.student_assign[i].assignMents[j].assignment_Mark == 'NA') {
                                    $scope.nodeleterecord = true;
                                    $scope.student_assign[i].assignMents[j].insert = false;
                                    var dataobj1 = {
                                        Assignment_cur_code: $scope.student_assign[i].assignMents[j].assignment_cur_code
                                    , Assignment_academic_year: $scope.student_assign[i].assignMents[j].assignment_academic_year
                                    , Assignment_grade_code: $scope.student_assign[i].assignMents[j].assignment_grade_code
                                    , Assignment_section_code: $scope.student_assign[i].assignMents[j].assignment_section_code
                                    , Assignment_gb_number: $scope.student_assign[i].assignMents[j].assignment_gb_number
                                    , Assignment_cat_code: $scope.student_assign[i].assignMents[j].assignment_cat_code1
                                    , Assignment_Code: $scope.student_assign[i].assignMents[j].assignment_Code1
                                    , StudEnroll: $scope.student_assign[i].assignMents[j].stud_Enroll,
                                        term_code: $scope.mark.gb_term_code
                                       , Assignment_subject_code: $scope.student_assign[i].assignMents[j].assignment_subject_code
                                    , OPR: 'CD'
                                    };

                                    $scope.dataAssignment.push(dataobj1);
                                    dataobj1 = {};
                                }
                            }
                        }
                    }
                }

                $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.dataAssignment).then(function (AssignMarksToStudentsM) {
                    $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                    if ($scope.AssignMarks_ToStudentsM == true) {
                        swal({
                            text: 'Student Skiped Successfully From This Assignment',
                            imageUrl: "assets/img/check.png",
                            width: 320,
                            showCloseButton: true
                        });
                        $scope.classdefault1 = 'col-md-12';
                        $scope.GetAllGradeSectionwiseGradebookNmae1();
                    }
                    else {
                        swal({
                            text: 'Student Not Skiped From This Assignment',
                            imageUrl: "assets/img/close.png",
                            width: 300,
                            showCloseButton: true
                        });
                    }

                });

            }

            $scope.SelectOneStudentskipForAllAssignments = function (str) {

                $scope.Assignstudentrecord = false;
                $scope.deleterecord = false;
                $scope.nodeleterecord = false;
                $scope.dataAssignment = [];
                $scope.deleterecorddataAssignment = [];
                var check = document.getElementById(str);
                if (check.checked == false) {
                    for (var i = 0; i < $scope.student_assign.length; i++) {
                        if ($scope.student_assign[i].stud_Enroll == str) {
                            for (var j = 0; j < $scope.student_assign[i].assignMents.length; j++) {
                                if ($scope.student_assign[i].assignMents[j].assignment_Mark == "" || $scope.student_assign[i].assignMents[j].assignment_Mark == "NA") {
                                    $scope.deleterecord = true;
                                    var dataobj1 = {
                                        Assignment_cur_code: $scope.student_assign[i].assignMents[j].assignment_cur_code
                                    , Assignment_academic_year: $scope.student_assign[i].assignMents[j].assignment_academic_year
                                    , Assignment_grade_code: $scope.student_assign[i].assignMents[j].assignment_grade_code
                                    , Assignment_section_code: $scope.student_assign[i].assignMents[j].assignment_section_code
                                    , Assignment_gb_number: $scope.student_assign[i].assignMents[j].assignment_gb_number
                                    , Assignment_cat_code: $scope.student_assign[i].assignMents[j].assignment_cat_code1
                                    , Assignment_Code: $scope.student_assign[i].assignMents[j].assignment_Code1
                                    , StudEnroll: $scope.student_assign[i].assignMents[j].stud_Enroll,
                                        stud_Enroll: $scope.student_assign[i].assignMents[j].stud_Enroll
                                    , stud_Full_Name: $scope.student_assign[i].stud_Full_Name
                                    , assignment_Name: $scope.student_assign[i].assignMents[j].assignment_Name
                                     , term_code: $scope.mark.gb_term_code
                                   , Assignment_subject_code: $scope.student_assign[i].assignMents[j].assignment_subject_code
                                     , OPR: 'CD'
                                    };

                                    $scope.dataAssignment.push(dataobj1);
                                    var dataobj1 = {};

                                }
                                else {

                                    $scope.nodeleterecord = true;

                                    var dataobj1 = {
                                        Assignment_cur_code: $scope.student_assign[i].assignMents[j].assignment_cur_code
                                    , Assignment_academic_year: $scope.student_assign[i].assignMents[j].assignment_academic_year
                                    , Assignment_grade_code: $scope.student_assign[i].assignMents[j].assignment_grade_code
                                    , Assignment_section_code: $scope.student_assign[i].assignMents[j].assignment_section_code
                                    , Assignment_gb_number: $scope.student_assign[i].assignMents[j].assignment_gb_number
                                    , Assignment_cat_code: $scope.student_assign[i].assignMents[j].assignment_cat_code1
                                    , Assignment_Code: $scope.student_assign[i].assignMents[j].assignment_Code1
                                    , StudEnroll: $scope.student_assign[i].assignMents[j].stud_Enroll,
                                        stud_Enroll: $scope.student_assign[i].assignMents[j].stud_Enroll
                                    , stud_Full_Name: $scope.student_assign[i].stud_Full_Name
                                    , assignment_Name: $scope.student_assign[i].assignMents[j].assignment_Name
                                     , term_code: $scope.mark.gb_term_code
                                   , Assignment_subject_code: $scope.student_assign[i].assignMents[j].assignment_subject_code

                                    };

                                    $scope.deleterecorddataAssignment.push(dataobj1);
                                }
                            }
                        }
                    }
                }

                else {
                    for (var i = 0; i < $scope.student_assign.length; i++) {
                        if ($scope.student_assign[i].stud_Enroll == str) {
                            for (var j = 0; j < $scope.student_assign[i].assignMents.length; j++) {
                                $scope.student_assign[i].assignMents[j].insert = check.checked;
                                $scope.Assignstudentrecord = true;
                                var dataobj1 = {
                                    Assignment_cur_code: $scope.student_assign[i].assignMents[j].assignment_cur_code
                                  , Assignment_academic_year: $scope.student_assign[i].assignMents[j].assignment_academic_year
                                  , Assignment_grade_code: $scope.student_assign[i].assignMents[j].assignment_grade_code
                                  , Assignment_section_code: $scope.student_assign[i].assignMents[j].assignment_section_code
                                  , Assignment_gb_number: $scope.student_assign[i].assignMents[j].assignment_gb_number
                                  , Assignment_cat_code: $scope.student_assign[i].assignMents[j].assignment_cat_code1
                                  , Assignment_Code: $scope.student_assign[i].assignMents[j].assignment_Code1
                                  , StudEnroll: $scope.student_assign[i].assignMents[j].stud_Enroll
                                  , term_code: $scope.mark.gb_term_code
                                  , Assignment_subject_code: $scope.student_assign[i].assignMents[j].assignment_subject_code
                                   , OPR: 'CI'
                                };

                                $scope.insertassignmentdata.push(dataobj1);
                                var dataobj1 = {};
                            }
                        }
                    }
                }

                if ($scope.deleterecord == true && $scope.nodeleterecord == false && check.checked == false) {
                    if ($scope.dataAssignment.length > 0) {
                        $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.dataAssignment).then(function (AssignMarksToStudentsM) {
                            $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                            if ($scope.AssignMarks_ToStudentsM == true) {
                                swal({
                                    text: 'Student Skiped Successfully From This Assignment',
                                    imageUrl: "assets/img/check.png",
                                    width: 320,
                                    showCloseButton: true
                                });
                                $scope.classdefault1 = 'col-md-12';
                                $scope.GetAllGradeSectionwiseGradebookNmae1();
                            }
                            else {
                                swal({
                                    text: 'Student Not Skiped From This Assignment',
                                    imageUrl: "assets/img/close.png",
                                    width: 300,
                                    showCloseButton: true
                                });
                            }

                        });
                    }
                }

                else if (check.checked == false && $scope.nodeleterecord == true) {
                    $scope.datadeleteobject = $scope.deleterecorddataAssignment;
                    $('#Div14').modal({ backdrop: "static" });
                }
            }

            $scope.AddStudentForAssignment = [];

            $scope.SelectOneStudentForskip = function (info) {

                $scope.dataAssignment = [];

                var data = {
                    Assignment_cur_code: info.assignment_cur_code,
                    Assignment_academic_year: info.assignment_academic_year,
                    Assignment_grade_code: info.assignment_grade_code,
                    Assignment_section_code: info.assignment_section_code,
                    Assignment_gb_number: info.assignment_gb_number,
                    Assignment_cat_code: info.assignment_cat_code1,
                    Assignment_Code: info.assignment_Code1,
                    term_code: $scope.mark.gb_term_code
                  , Assignment_subject_code: info.assignment_subject_code
                   , StudEnroll: info.stud_Enroll,
                    Assignment_Status1: '',

                };


                if (info.insert == true) {

                    data.OPR = "CI";
                    $scope.dataAssignment.push(data);

                    $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.dataAssignment).then(function (AssignMarksToStudentsM) {
                        $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                        if ($scope.AssignMarks_ToStudentsM == true) {
                            swal({
                                text: 'Assignment Mapped Successfully',
                                imageUrl: "assets/img/check.png",
                                width: 320,
                                showCloseButton: true
                            });
                            $scope.classdefault1 = 'col-md-12';
                            data = {};
                            $scope.studentwiseMarkEntryShow2 = false;
                            $scope.GetAllGradeSectionwiseGradebookNmae1();
                        }
                        else {
                            swal({
                                text: 'Assignment Not Mapped',
                                imageUrl: "assets/img/close.png",
                                width: 300,
                                showCloseButton: true
                            });
                        }

                    });

                }
                else if (info.insert == false && (info.assignment_Mark == "" || info.assignment_Mark == 'NA')) {

                    data.OPR = "CD";
                    $scope.dataAssignment.push(data);
                    $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.dataAssignment).then(function (AssignMarksToStudentsM) {
                        $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                        if ($scope.AssignMarks_ToStudentsM == true) {
                            swal({
                                text: 'Student Skiped Successfully From This Assignment',
                                imageUrl: "assets/img/check.png",
                                width: 320,
                                showCloseButton: true
                            });
                            $scope.classdefault1 = 'col-md-12';
                            data = {};
                            $scope.studentwiseMarkEntryShow2 = false;
                            $scope.GetAllGradeSectionwiseGradebookNmae1();
                        }
                        else {
                            swal({
                                text: 'Student Not Skiped From This Assignment',
                                imageUrl: "assets/img/close.png",
                                width: 300,
                                showCloseButton: true
                            });
                        }

                    });
                }
                else if (info.insert == false && (info.assignment_Mark != "" || info.assignment_Mark != 'NA')) {

                    info.insert = true;
                    swal({
                        text: 'Are You Sure? You Want Be Skip Student For This Assignment',
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            data.OPR = "CD";
                            $scope.dataAssignment.push(data);
                            $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.dataAssignment).then(function (AssignMarksToStudentsM) {
                                $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                                if ($scope.AssignMarks_ToStudentsM == true) {
                                    swal({
                                        text: 'Student Skiped Successfully From This Assignment',
                                        imageUrl: "assets/img/check.png",
                                        width: 320,
                                        showCloseButton: true
                                    });
                                    $scope.classdefault1 = 'col-md-12';
                                    data = {};
                                    $scope.studentwiseMarkEntryShow2 = false;
                                    $scope.GetAllGradeSectionwiseGradebookNmae1();
                                }
                                else {
                                    swal({
                                        text: 'Student Not Skiped From This Assignment',
                                        imageUrl: "assets/img/close.png",
                                        width: 300,
                                        showCloseButton: true
                                    });
                                }

                            });
                        }
                        else {
                            info.insert = true;

                        }
                    })
                }
            }

            /******************************************************End Mark Entry*********************************************************************************/

            /******************************************************Comment*********************************************************************************/
            var student_number = [];
            var subject_names1 = [];
            var defaultcomment = "";

            $scope.GradeSectionSubject1 = function () {

                $scope.subject_names = [];

                subject_names1 = [];

                $scope.SectionSubject5 = [];

                var report1 = {
                    sims_grade_code: $scope.commn.sims_grade_code,
                    sims_academic_year: $scope.commn.academic_year,
                    sims_section_code: $scope.commn.sims_section_code,
                    sims_employee_code: user
                };

                $http.post(ENV.apiUrl + "api/Gradebook/AllGradeSection_subject", report1).then(function (allSection_Subject) {
                    $scope.subject_names = allSection_Subject.data;
                });

                var report = {
                    SubjectCurr: $scope.commn.cur_code,
                    SubjectGrade: $scope.commn.sims_grade_code,
                    SubjectAcademic: $scope.commn.academic_year,
                    SubjectSection: $scope.commn.sims_section_code,
                    SubjectTerm: $scope.fcommn.sims_term_code,
                    SubTeacherEmpId: user
                };


                $http.post(ENV.apiUrl + "api/Gradebook/ReportCardlevels?data=" + JSON.stringify(report)).then(function (Getlevels) {
                    $scope.Get_levels = Getlevels.data;
                    $scope.commn['level_code'] = $scope.Get_levels[0].level_code;
                    $scope.getreport_card();
                });


                $http.get(ENV.apiUrl + "api/Gradebook/Get_group_code?cur_code=" + $scope.commn.cur_code + "&acadmic_yr=" + $scope.commn.academic_year + "&grade_code=" + $scope.commn.sims_grade_code + "&section=" + $scope.commn.sims_section_code).then(function (Get_group_code) {
                    $scope.group_code = Get_group_code.data;

                });
            }

            $scope.getreport_card = function () {
                var report = {
                    SubjectCurr: $scope.commn.cur_code,
                    SubjectGrade: $scope.commn.sims_grade_code,
                    SubjectAcademic: $scope.commn.academic_year,
                    SubjectSection: $scope.commn.sims_section_code,
                    AssignmentNumber: $scope.commn.level_code, //level code
                    SubjectTerm: $scope.commn.sims_term_code,
                    SubTeacherEmpId: user
                };
                $http.get(ENV.apiUrl + "api/Gradebook/getReportCards?data=" + JSON.stringify(report)).then(function (ReportCards) {
                    $scope.Report_Cards = ReportCards.data;
                });

            }

            $scope.GetAllStudentForComment = function () {

                data = {
                    Curr_Code: $scope.commn.cur_code
                    , Academic_Year: $scope.commn.academic_year
                    , Grade_Code: $scope.commn.sims_grade_code
                    , Section_code: $scope.commn.sims_section_code
                    , Subject_Code: $scope.commn.sims_subject_code
                    , Report_Card_Code: $scope.commn.report_Card_Code
                    , Level_code: $scope.commn.level_code
                };

                $http.get(ENV.apiUrl + "api/Gradebook/GetReportCardStudent?data=" + JSON.stringify(data)).then(function (GetReportCardStudent) {

                    $scope.ReportCard_Student = GetReportCardStudent.data;

                });

                $http.get(ENV.apiUrl + "api/Gradebook/GetReportCardComments?data=" + JSON.stringify(data)).then(function (GetReportCardComments) {

                    $scope.ReportCard_Comments = GetReportCardComments.data;


                });
            }

            $scope.SetComment = function (str) {
                defaultcomment = str;
            }

            $scope.ApplyGradeBookSubjectCommentData = function () {

                for (var i = 0; i < $scope.ReportCard_Student.length; i++) {
                    var id = document.getElementById('idCommment');
                    if ($scope.val.setcomment_code == 'Comment1') {
                        if (id.checked == false) {
                            if ($scope.ReportCard_Student[i].comment2_code == '' || $scope.ReportCard_Student[i].comment1_code == '')
                                $scope.ReportCard_Student[i].comment1_code = defaultcomment;
                            $scope.ReportCard_Student[i].checked = true;
                        }
                        else {
                            $scope.ReportCard_Student[i].comment1_code = defaultcomment;
                            $scope.ReportCard_Student[i].checked = true;
                        }
                    }
                    else {
                        if (id.checked == false) {
                            if ($scope.ReportCard_Student[i].comment2_code == '')
                                $scope.ReportCard_Student[i].comment2_code = defaultcomment;
                            $scope.ReportCard_Student[i].checked = true;
                        }
                        else {
                            $scope.ReportCard_Student[i].comment2_code = defaultcomment;
                            $scope.ReportCard_Student[i].checked = true;
                        }
                    }
                }
            }

            $scope.SaveGradeBookSubjectCommentData = function () {

                student_number = [];

                for (var i = 0; i < $scope.ReportCard_Student.length; i++) {
                    if ($scope.ReportCard_Student[i].checked == true) {
                        var data = {
                            Curr_Code: $scope.commn.cur_code
                        , Academic_Year: $scope.commn.academic_year
                        , Grade_Code: $scope.commn.sims_grade_code
                        , Section_code: $scope.commn.sims_section_code
                        , Subject_Code: $scope.commn.sims_subject_code
                        , Level_code: $scope.commn.level_code
                        , Report_Card_Code: $scope.commn.report_Card_Code
                        , enroll: $scope.ReportCard_Student[i].enroll
                        , Comment1_code: $scope.ReportCard_Student[i].comment1_code
                        , Comment2_code: $scope.ReportCard_Student[i].comment2_code
                        , user1: user
                        , user2: ""
                        , Status: "A"
                        , commnetNo: ""
                        , opr: "I"
                        };

                        student_number.push(data);
                    }
                }
                $http.post(ENV.apiUrl + "api/Gradebook/ReportCardDataOpr", student_number).then(function (ReportCardDataOpr) {

                    $scope.ReportCardDataOpr = ReportCardDataOpr.data;
                    if ($scope.ReportCardDataOpr == true) {
                        swal({
                            text: 'Student Subject Comment Inserted',
                            imageUrl: "assets/img/check.png",
                            width: 300,
                            showCloseButton: true
                        });
                    }
                    else {
                        swal({
                            text: 'Student Subject Comment Not Inserted',
                            imageUrl: "assets/img/close.png",
                            width: 300,
                            showCloseButton: true
                        });
                    }
                });
            }

            $scope.UpdatecommenttudentNumber = function (info) {
                info.checked = true;
            }

            $scope.SetDefaultGradeBookSubjectCommentData = function () {
                $scope.GetAllStudentForComment();
            }

            /******************************************************End Comment*********************************************************************************/

            /******************************************************Subject Attribute*********************************************************************************/

            $scope.buttonssaveupdate = true;
            var groupcheck;
            var Subjectcheck;
            var subjectattribute = [];

            var mainsubjectattribute = [];

            $scope.MultipleGroupCheck = function () {

                var groupcheck = document.getElementById('groupcheck');
                for (var i = 0; i < $scope.group_code.length; i++) {

                    var name = document.getElementById($scope.group_code[i].group_name + $scope.group_code[i].sims_subject_attribute_group_code)
                    if (groupcheck.checked == true) {

                        name.checked = true;
                    }
                    else {
                        name.checked = false;
                    }

                }
            }

            $scope.OneByOneGroupSelect = function () {
                var groupcheck = document.getElementById('groupcheck');
                if (groupcheck.checked == true) {
                    groupcheck.checked = false;
                }
            }

            $scope.MultipleSubjectSelect = function () {
                Subjectcheck = document.getElementById('Subjectcheck');
                for (var i = 0; i < $scope.subject_names1.length; i++) {
                    var name = document.getElementById($scope.subject_names1[i].sims_subject_name + $scope.subject_names1[i].sims_subject_code)
                    if (Subjectcheck.checked == true) {
                        name.checked = true;
                    }
                    else {
                        name.checked = false;
                    }
                }
            }

            $scope.OneByOneSubjectSelect = function () {
                Subjectcheck = document.getElementById('Subjectcheck');
                if (Subjectcheck.checked == true) {
                    Subjectcheck.checked = false;
                }
            }

            $scope.GradeSectionSubjectAttribute = function () {

                $scope.buttonssaveupdate = true;
                $scope.All_subject_attribute = [];
                $scope.SectionSubject5 = [];
                $scope.subject_names1 = [];
                subject_names1 = [];
                $http.get(ENV.apiUrl + "api/common/SectionSubject/getSubjectByIndex?curcode=" + $scope.Attri.cur_code + "&gradecode=" + $scope.Attri.sims_grade_code + "&academicyear=" + $scope.Attri.academic_year + "&sectioncode=" + $scope.Attri.sims_section_code).then(function (allSection_Subject) {

                    $scope.SectionSubject5 = allSection_Subject.data;

                    for (var i = 0; i < $scope.SectionSubject5.length; i++) {

                        if ($scope.SectionSubject5[i].sims_subject_status == true) {

                            var data = ({
                                'sims_subject_code': $scope.SectionSubject5[i].sims_subject_code,
                                'sims_subject_name': $scope.SectionSubject5[i].sims_subject_name
                            })
                            subject_names1.push(data);
                        }
                        $scope.subject_names1 = subject_names1;
                    }

                });

                $http.get(ENV.apiUrl + "api/Gradebook/Get_group_code?cur_code=" + $scope.Attri.cur_code + "&acadmic_yr=" + $scope.Attri.academic_year + "&grade_code=" + $scope.Attri.sims_grade_code + "&section=" + $scope.Attri.sims_section_code).then(function (Get_group_code) {
                    $scope.group_code = Get_group_code.data;
                });


                $http.get(ENV.apiUrl + "api/Gradebook/Get_All_subject_attribute?cur=" + $scope.Attri.cur_code + "&ay=" + $scope.Attri.academic_year + "&grade=" + $scope.Attri.sims_grade_code + "&section=" + $scope.Attri.sims_section_code).then(function (Get_All_subject_attribute) {
                    $scope.All_subject_attribute = Get_All_subject_attribute.data;


                });

                $scope.OneByOneGroupSelect();
                $scope.OneByOneSubjectSelect();

            }

            $scope.SaveGradeBookSubjectAttributeData = function () {

                var attributelist = [], subjectlist = [];
                if (attributelist.length > 0 && subjectlist.length > 0 && maindata != null) {
                    for (var i = 0; i < $scope.group_code.length; i++) {

                        var name1 = document.getElementById($scope.group_code[i].group_name + $scope.group_code[i].sims_subject_attribute_group_code)
                        if (name1.checked == true) {

                            var data = { subject_name: $scope.group_code[i].sims_subject_attribute_group_code }

                            attributelist.push(data);

                        }
                    }

                    for (var i = 0; i < $scope.subject_names1.length; i++) {
                        var name = document.getElementById($scope.subject_names1[i].sims_subject_name + $scope.subject_names1[i].sims_subject_code)
                        if (name.checked == true) {

                            var data = { subject_name: $scope.subject_names1[i].sims_subject_code };
                            subjectlist.push(data);

                        }
                    }
                    subjectattribute.push(attributelist);
                    subjectattribute.push(subjectlist);

                    var maindata = {
                        sims_cur_code: $scope.Attri.cur_code
                    , sims_academic_year: $scope.Attri.academic_year
                    , sims_grade_code: $scope.Attri.sims_grade_code
                    , sims_section_code: $scope.Attri.sims_section_code
                    , sims_subject_attribute_name: $scope.Attri.sims_subject_attribute_name
                    , sims_subject_attribute_name_ot: $scope.Attri.sims_subject_attribute_name_ot
                    , sims_subject_attribute_display_order: $scope.Attri.sims_subject_attribute_display_order
                    , sims_subject_attribute_status: $scope.Attri.sims_subject_attribute_status
                    };


                    $http.post(ENV.apiUrl + "api/Gradebook/Subject_attribute?data=" + JSON.stringify(maindata), subjectattribute).then(function (Insert_Subject_attribute) {
                        $scope.Subject_attribute_Insert = Insert_Subject_attribute.data;
                        if ($scope.Subject_attribute_Insert == true) {
                            swal({
                                text: 'Subject Attribute Inserted',
                                imageUrl: "assets/img/check.png",
                                width: 300,
                                showCloseButton: true
                            });
                        }
                        else {
                            swal({
                                text: 'Subject Attribute Not Inserted',
                                imageUrl: "assets/img/close.png",
                                width: 300,
                                showCloseButton: true
                            });
                        }
                        subjectattribute = [];

                        $http.get(ENV.apiUrl + "api/Gradebook/Get_All_subject_attribute?cur=" + $scope.Attri.cur_code + "&ay=" + $scope.Attri.academic_year + "&grade=" + $scope.Attri.sims_grade_code + "&section=" + $scope.Attri.sims_section_code).then(function (Get_All_subject_attribute) {
                            $scope.All_subject_attribute = Get_All_subject_attribute.data;
                        });

                    });

                    $scope.All_subject_attribute = [];
                    $scope.SectionSubject5 = [];
                    $scope.subject_names1 = [];
                    $scope.group_code = [];
                    $scope.Attri = "";
                }

                else {

                    swal({
                        text: 'Please Select  Attribute Name Or Subject To Insert Subject Attribute',
                        imageUrl: "assets/img/notification-alert.png",
                        width: 380,
                        showCloseButton: true
                    });
                }

            }

            $scope.EditAttribute = function (info) {

                $scope.buttonssaveupdate = false;



                $scope.Attri = {
                    sims_academic_year: info.sims_academic_year
                , sims_cur_code: info.sims_cur_code
                , cur_code: info.sims_cur_code
                , academic_year: info.sims_academic_year
                , sims_grade_code: info.sims_grade_code
                , sims_section_code: info.sims_section_code
                , sims_subject_attribute_name: info.sims_subject_attribute_name
                , sims_subject_attribute_name_ot: info.sims_subject_attribute_name_ot
                , sims_subject_attribute_display_order: info.sims_subject_attribute_display_order
                , sims_subject_attribute_status: info.sims_subject_attribute_status
                , sims_subject_attribute_code: info.sims_subject_attribute_code
                , sims_subject_code: info.sims_subject_code
                , sims_subject_attribute_group_code: info.sims_subject_attribute_group_code
                };
            }

            $scope.UpdateGradeBookSubjectAttributeData = function () {

                $scope.All_subject_attribute = [];
                $scope.SectionSubject5 = [];
                $scope.subject_names1 = [];
                $scope.group_code = [];

                $http.post(ENV.apiUrl + "api/Gradebook/Update_Subject_attribute", $scope.Attri).then(function (Update_Subject_attribute) {
                    $scope.Update_Subject_attribute1 = Update_Subject_attribute.data;


                    if ($scope.Update_Subject_attribute1 == true) {

                        $scope.buttonssaveupdate = true;
                        swal({
                            text: 'Subject Attribute Updated',
                            imageUrl: "assets/img/check.png",

                            width: 300,
                            showCloseButton: true
                        });

                    }
                    else {
                        swal({
                            text: 'Subject Attribute Not Updated',
                            imageUrl: "assets/img/close.png",
                            width: 300,
                            showCloseButton: true
                        });
                    }


                    $http.get(ENV.apiUrl + "api/Gradebook/Get_All_subject_attribute?cur=" + $scope.Attri.cur_code + "&ay=" + $scope.Attri.academic_year + "&grade=" + $scope.Attri.sims_grade_code + "&section=" + $scope.Attri.sims_section_code).then(function (Get_All_subject_attribute) {
                        $scope.All_subject_attribute = Get_All_subject_attribute.data;
                    });

                    $scope.Attri = "";
                });

            }

            $scope.Reset = function () {
                $scope.Attri = '';
                $scope.group_code = [];
                $scope.subject_names1 = [];
                $scopeAll_subject_attribute = [];

            }

            /******************************************************Subject Attribute*********************************************************************************/

            /******************************************************Start Grade Scale*********************************************************************************/
            $scope.gradescalebtn = true;

            $scope.btn_GradeScaleInsert_click = function () {

                var GradeScaleDataSend = [];
                for (var i = 0; i < $scope.All_SCALE_GRADE.length; i++) {
                    var t = document.getElementById($scope.All_SCALE_GRADE[i].gradeGroupCode);
                    if (t.checked == true) {

                        var data = {
                            opr: 'IM',
                            sims_mark_grade_name: $scope.gradescale.sims_mark_grade_name,
                            GradeGroupCode: $scope.All_SCALE_GRADE[i].gradeGroupCode,
                            sims_mark_grade_low: $scope.gradescale.sims_mark_grade_low,
                            sims_mark_grade_high: $scope.gradescale.sims_mark_grade_high,
                            sims_mark_grade_description: $scope.gradescale.sims_mark_grade_description,
                            sims_mark_grade_point: $scope.gradescale.sims_mark_grade_point,
                        }

                        GradeScaleDataSend.push(data);
                    }

                }
                if (GradeScaleDataSend.length != 0) {
                    $http.post(ENV.apiUrl + "api/Gradebook/CUDGradeScaleDataOpr", GradeScaleDataSend).then(function (msg) {
                        $scope.MSG = msg.data;
                        $scope.gradescale = "";
                        GradeScaleDataSend = [];

                        var t = document.getElementById('SelectGrades');
                        if (t.checked == true) {
                            t.checked = false;
                        }

                        $scope.GradebookGradingSchemeShow();

                        swal({ text: $scope.MSG, width: 300, showCloseButton: true });


                    });

                }
                else {

                    swal({ text: 'Select Atleast One Grade To Insert Grade Scale', imageUrl: "assets/img/notification-alert.png", width: 300, showCloseButton: true });
                }
            }

            $scope.btn_GradeScaleDelete_click = function () {

                var GradeScaleDataSend = [];

                for (var i = 0; i < $scope.Grade_Scale_Data.length; i++) {

                    var t = document.getElementById($scope.Grade_Scale_Data[i].sims_mark_grade_code);
                    if (t.checked == true) {
                        $scope.Grade_Scale_Data[i].opr = 'D',
                        GradeScaleDataSend.push($scope.Grade_Scale_Data[i]);
                    }

                }


                if (GradeScaleDataSend.length != 0) {
                    $http.post(ENV.apiUrl + "api/Gradebook/CUDGradeScaleDataOpr", GradeScaleDataSend).then(function (msg) {
                        $scope.MSG = msg.data;
                        $scope.gradescale = "";
                        GradeScaleDataSend = [];

                        $scope.GradebookGradingSchemeShow();
                        swal({ text: $scope.MSG, width: 300, showCloseButton: true });

                    })

                }
                else {

                    swal({ text: 'Select Atleast One Grade Scale To Deleted', imageUrl: "assets/img/notification-alert.png", width: 300, showCloseButton: true });
                }

            }

            $scope.RowUpdateGradeScale = function (info) {
                $scope.gradescalebtn = false;
                $scope.gradescale = info;
            }

            $scope.btn_GradeScaleUpdate_click = function () {

                var GradeScaleDataSend = [];
                $scope.gradescalebtn = true;
                var data = {
                    opr: 'UM',
                    sims_mark_grade_name: $scope.gradescale.sims_mark_grade_name,
                    sims_mark_grade_code: $scope.gradescale.sims_mark_grade_code,
                    sims_mark_grade_low: $scope.gradescale.sims_mark_grade_low,
                    sims_mark_grade_high: $scope.gradescale.sims_mark_grade_high,
                    sims_mark_grade_description: $scope.gradescale.sims_mark_grade_description,
                    sims_mark_grade_point: $scope.gradescale.sims_mark_grade_point.length == 0 ? 0 : $scope.gradescale.sims_mark_grade_point,
                    GradeGroupCode: $scope.gradescale.gradeGroupCode
                }

                GradeScaleDataSend.push(data);

                if (GradeScaleDataSend.length != 0) {
                    $http.post(ENV.apiUrl + "api/Gradebook/CUDGradeScaleDataOpr", GradeScaleDataSend).then(function (msg) {
                        $scope.MSG = msg.data;
                        $scope.gradescale = "";
                        GradeScaleDataSend = [];


                        swal({ text: $scope.MSG, width: 300, showCloseButton: true });

                    })
                }
                else {

                    swal({ text: 'Select Atleast One Grade Scale To Update', imageUrl: "assets/img/notification-alert.png", width: 300, showCloseButton: true });
                }
            }

            $scope.MultipleSelectGrades = function () {

                var main = document.getElementById('SelectGrades');
                for (var i = 0; i < $scope.All_SCALE_GRADE.length; i++) {
                    var t = document.getElementById($scope.All_SCALE_GRADE[i].gradeGroupCode);
                    if (main.checked == true) {
                        t.checked = true;
                    }
                    else {
                        t.checked = false;
                        main.checked = false;
                    }
                }
            }

            $scope.MultipleSelectScaleGrades = function () {

                var main = document.getElementById('MultipleSelect');
                for (var i = 0; i < $scope.Grade_Scale_Data.length; i++) {
                    var t = document.getElementById($scope.Grade_Scale_Data[i].sims_mark_grade_code);
                    if (main.checked == true) {
                        t.checked = true;
                    }
                    else {
                        t.checked = false;
                        main.checked = false;
                    }
                }

            }

            var Grade_Scaledata = [];

            $scope.ShowGradeWiseGradescale = function (info) {

                Grade_Scaledata = [];


                $http.get(ENV.apiUrl + "api/Gradebook/getGradeScaleData").then(function (GradeScaleData) {
                    $scope.Grade_Scale_Data = GradeScaleData.data;
                    for (var i = 0; i < $scope.Grade_Scale_Data.length; i++) {
                        if ($scope.Grade_Scale_Data[i].gradeGroupCode == info.gradeGroupCode) {
                            Grade_Scaledata.push($scope.Grade_Scale_Data[i]);
                        }
                    }
                    $scope.Grade_Scale_Data = [];
                    $scope.Grade_Scale_Data = Grade_Scaledata;
                })

            }

            $scope.btn_NarrativeGradeScaleInsert_click = function () {

                $http.post(ENV.apiUrl + "api/Gradebook/MarkGradeDataOpr", $scope.gradgroup).then(function (msg) {
                    $scope.MSG = msg.data;
                    if ($scope.MSG == true) {
                        $scope.gradgroup = '';
                        var t = document.getElementById('SelectGrades');
                        if (t.checked == true) {
                            t.checked = false;
                        }

                        $scope.GradebookGradingSchemeShow();

                        swal({ text: 'Grade Group Created Successfully', imageUrl: "assets/img/check.png", width: 300, showCloseButton: true });
                    }
                    else {
                        swal({ text: 'Error In Grade Group Creation', imageUrl: "assets/img/close.png", width: 300, showCloseButton: true });
                    }
                })
            }

            $scope.btn_GradeScaleCancel_click = function () {

                $scope.gradescalebtn = true;
                $scope.gradescale = "";
            }

            $scope.btn_NarrativeCancel_click = function () {

                $scope.gradgroup = "";
            }

            /******************************************************End Grade Scale*********************************************************************************/

            /***************************************************************Start Copy GradeBook Code**************************************************************************************************/
            $scope.Expand1 = true;
            $scope.maincopy = true;
            $scope.getsectionscopy = function () {

                $http.get(ENV.apiUrl + "api/Gradebook/GetRCGrade?curcode=" + $scope.copyg.cur_code + "&ayear=" + $scope.copyg.academic_year + "&teachercode=" + user).then(function (res) {
                    $scope.getAllGrades4 = res.data;
                    if ($scope.classwisecopygradebook != true) {
                        $scope.GradeBookNamesforCopy();
                    }
                    else {
                        $scope.copygradebookclasswiseshow();
                    }

                });
            };

            $scope.checkGradebook = function (info) {

                if ($scope.copygradebook == true) {
                    for (var i = 0; i < $scope.Term_wiseGetGradeBooks1.length; i++) {
                        if ($scope.Term_wiseGetGradeBooks1[i].sims_gb_number == info.sims_gb_number) {
                            $scope.Term_wiseGetGradeBooks1[i].ischecked = true;
                            $scope.GradeBookName_For_Copy1 = $scope.Term_wiseGetGradeBooks1[i];
                        }
                        else {
                            $scope.Term_wiseGetGradeBooks1[i].ischecked = false;
                        }
                    }
                }
                else if ($scope.GradeBookcategory == true) {
                    for (var i = 0; i < info.gb_cat_lst.length; i++) {
                        if (info.gb_cat_lst[i].sims_gb_number == info.sims_gb_number && info.ischecked == true) {
                            info.gb_cat_lst[i].ischecked = true;

                        }
                        else {
                            info.gb_cat_lst[i].ischecked = false;
                        }
                    }
                }
                else if ($scope.GradeBookAssignments == true) {
                    for (var i = 0; i < info.gb_cat_lst.length; i++) {
                        for (var j = 0; j < info.gb_cat_lst[i].assign_lst.length; j++) {
                            if (info.gb_cat_lst[i].sims_gb_number == info.sims_gb_number && info.ischecked == true) {
                                info.gb_cat_lst[i].ischecked = true;
                                info.gb_cat_lst[i].assign_lst[j].ischecked = true;
                            }
                            else {
                                info.gb_cat_lst[i].ischecked = false;
                                info.gb_cat_lst[i].assign_lst[j].ischecked = false;
                            }
                        }

                    }

                }
            }

            $scope.GradeBookNamesforCopy = function () {

                $scope.showbusyindi = false;
                $scope.showbusyindi1 = false;
                $scope.GradeBookcategory = false;
                $scope.GradeBookAssignments = false;
                $scope.copygradebook = true;
                $scope.classwisecopygradebook = false;
                $scope.catexpand = true;
                $scope.assignexpand = true;

                if ($scope.copyg.sims_grade_code == undefined) {
                    $scope.copyg.sims_grade_code = '';
                }

                $http.post(ENV.apiUrl + "api/Gradebook/CopyGradeBookNameForCopyGradebook", $scope.copyg).then(function (CopyGradeBookNameForCopyGradebook) {
                    $scope.GradeBookName_For_Copy = CopyGradeBookNameForCopyGradebook.data;
                });

                $http.get(ENV.apiUrl + "api/Gradebook/getTermwiseGetGradeBookForCopynew?termCode=" + $scope.copyg.gb_term_code + "&academic_year=" + $scope.copyg.academic_year + "&cur_code=" + $scope.copyg.cur_code + "&grade_code=" + $scope.copyg.sims_grade_code).then(function (Term_wiseGetGradeBookForCopy) {
                    $scope.Term_wiseGetGradeBooks1 = Term_wiseGetGradeBookForCopy.data;

                    $scope.showbusyindi = true;

                });


                $http.post(ENV.apiUrl + "api/Gradebook/CGradeSectionSubjectNameorCode", $scope.copyg).then(function (getGradeSectionSubjectNameorCode) {
                    $scope.getGradeSectionSubjectNameorCode = getGradeSectionSubjectNameorCode.data;
                    $scope.showbusyindi1 = true;

                });
            }

            $scope.Select_All_Sections_Or_Subject = function (grade) {
                debugger
                for (var k = 0; k < grade.sectionslis.length; k++) {
                    for (var i = 0; i < grade.sectionslis[k].subjectslist.length; i++) {
                        if (grade.ischecked == true) {
                            grade.sectionslis[k].ischecked = true;
                            grade.sectionslis[k].subjectslist[i].ischecked = true;
                        }
                        else {
                            grade.sectionslis[k].ischecked = false;
                            grade.sectionslis[k].subjectslist[i].ischecked = false;
                        }
                    }
                }
            }

            $scope.Select_One_Section_Or_Subject = function (section) {

                for (var i = 0; i < section.subjectslist.length; i++) {
                    if (section.ischecked == true) {
                        section.subjectslist[i].ischecked = true;
                        section.subjectslist[i].ischecked = true;
                    }
                    else {
                        section.subjectslist[i].ischecked = false;
                        section.subjectslist[i].ischecked = false;
                    }
                }
            }

            $scope.SelectAllGradeSectionSubject = function () {

                for (var i = 0; i < $scope.getGradeSectionSubjectNameorCode.length; i++) {
                    for (var j = 0; j < $scope.getGradeSectionSubjectNameorCode[i].sectionslis.length; j++) {
                        for (var k = 0; k < $scope.getGradeSectionSubjectNameorCode[i].sectionslis[j].subjectslist.length; k++) {
                            var t = document.getElementById('forselectgrade');
                            if (t.checked == true) {
                                $scope.getGradeSectionSubjectNameorCode[i].ischecked = true;
                                $scope.getGradeSectionSubjectNameorCode[i].sectionslis[j].ischecked = true;
                                $scope.getGradeSectionSubjectNameorCode[i].sectionslis[j].subjectslist[k].ischecked = true;
                            }
                            else {

                                $scope.getGradeSectionSubjectNameorCode[i].ischecked = false;
                                $scope.getGradeSectionSubjectNameorCode[i].sectionslis[j].ischecked = false;
                                $scope.getGradeSectionSubjectNameorCode[i].sectionslis[j].subjectslist[k].ischecked = false;
                            }
                        }
                    }
                }
            }

            $scope.CopyGradeBookWithCategoryAndAssignment = function (str) {

                var objectCopyGradebook = [];
                if (str == undefined || str == '') {

                    swal({ text: 'Select Term Code', width: 300, showCloseButton: true });
                }
                else {

                    for (var j = 0; j < $scope.getGradeSectionSubjectNameorCode.length; j++) {
                        for (var k = 0; k < $scope.getGradeSectionSubjectNameorCode[j].sectionslis.length; k++) {
                            for (var l = 0; l < $scope.getGradeSectionSubjectNameorCode[j].sectionslis[k].subjectslist.length; l++) {
                                if ($scope.getGradeSectionSubjectNameorCode[j].sectionslis[k].subjectslist[l].ischecked == true) {

                                    var data = {
                                        cur_code: $scope.copyg.cur_code
                                       , academic_year: $scope.copyg.academic_year
                                       , gb_term_code: $scope.copyg.gb_term_code
                                       , grade_code: $scope.copyg.sims_grade_code
                                       , section_code: $scope.copyg.sims_section_code
                                       , gb_number: $scope.GradeBookName_For_Copy1.sims_gb_number
                                       , SubjectCurr: $scope.getGradeSectionSubjectNameorCode[j].sectionslis[k].subjectslist[l].subjectCurr
                                       , SubjectAcademic: $scope.getGradeSectionSubjectNameorCode[j].sectionslis[k].subjectslist[l].subjectAcademic
                                       , dTerm_code: $scope.copyg.gb_term_code1
                                       , SubjectGrade: $scope.getGradeSectionSubjectNameorCode[j].sectionslis[k].subjectslist[l].subjectGrade
                                       , SubjectSection: $scope.getGradeSectionSubjectNameorCode[j].sectionslis[k].subjectslist[l].subjectSection
                                       , SubjectCode: $scope.getGradeSectionSubjectNameorCode[j].sectionslis[k].subjectslist[l].subjectCode
                                       , isCateogry: $scope.withcategoryischeck == true ? 'Y' : 'N'
                                       , isAssignment: $scope.withassignmentsischeck == true ? 'Y' : 'N'
                                       , subject_select_status: true

                                    }
                                    objectCopyGradebook.push(data);
                                }
                            }
                        }
                    }

                    if (objectCopyGradebook.length > 0) {

                        $http.post(ENV.apiUrl + "api/Gradebook/GradeBookCopy", objectCopyGradebook).then(function (msg) {
                            $scope.MSG = msg.data;
                            if ($scope.MSG >= 1) {

                                swal({ text: 'GradeBook Copy Successfully', imageUrl: "assets/img/check.png", width: 300, showCloseButton: true });
                                $scope.withcategoryischeck = false;
                                $scope.withassignmentsischeck = false;

                                $scope.getsectionscopy();
                            }
                            else {

                                swal({ text: 'GradeBook Not Copy', imageUrl: "assets/img/close.png", width: 300, showCloseButton: true });
                            }
                        })
                    }
                    else {
                        swal({ text: 'Select At Least One Subject To Copy', imageUrl: "assets/img/notification-alert.png", width: 300, showCloseButton: true });
                    }
                }

            }

            $scope.GradeBookNamesforCopyshow = function () {
                $scope.classwisecopygradebook = false;
                $scope.GradeBookcategory = false;
                $scope.GradeBookAssignments = false;
                $scope.copygradebook = true;
                $scope.maincopy = true;
                $scope.copyg.subject_Code = '';

                for (var i = 0; i < $scope.Term_wiseGetGradeBooks1.length; i++) {
                    $scope.Term_wiseGetGradeBooks1[i].ischecked = false;
                    if ($scope.Term_wiseGetGradeBooks1[i].gb_cat_lst == undefined) {
                        $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst = [];
                    }
                    for (var j = 0; j < $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst.length; j++) {
                        $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].ischecked = false;
                        if ($scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].assign_lst == undefined) {
                            $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].assign_lst = [];
                        }
                        for (var k = 0; k < $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].assign_lst.length; k++) {
                            $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].assign_lst[k].ischecked = false;
                        }
                    }
                }
            }

            $scope.GradeBookcategoryforCopyshow = function () {

                $scope.GradeBookcategory = true;
                $scope.GradeBookAssignments = false;
                $scope.copygradebook = false;
                $scope.classwisecopygradebook = false;
                $scope.categoryobject = [];
                $scope.Term_wiseGetGradeBooks = [];
                $scope.copyg.subject_Code = '';

                for (var i = 0; i < $scope.Term_wiseGetGradeBooks1.length; i++) {
                    $scope.Term_wiseGetGradeBooks1[i].ischecked = false;
                    if ($scope.Term_wiseGetGradeBooks1[i].gb_cat_lst == undefined) {
                        $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst = [];
                    }
                    for (var j = 0; j < $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst.length; j++) {
                        $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].ischecked = false;
                        if ($scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].assign_lst == undefined) {
                            $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].assign_lst = [];
                        }
                        for (var k = 0; k < $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].assign_lst.length; k++) {
                            $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].assign_lst[k].ischecked = false;
                        }
                    }
                }

                $scope.selectGradebooknameforcopycategory($scope.cat.gb_term_code1);


            }

            $scope.GradeBookAssignmentsforCopyshow = function () {
                $scope.copyg.subject_Code = '';
                $scope.GradeBookcategory = false;
                $scope.GradeBookAssignments = true;
                $scope.copygradebook = false;
                $scope.classwisecopygradebook = false;

                for (var i = 0; i < $scope.Term_wiseGetGradeBooks1.length; i++) {
                    $scope.Term_wiseGetGradeBooks1[i].ischecked = false;
                    if ($scope.Term_wiseGetGradeBooks1[i].gb_cat_lst == undefined) {
                        $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst = [];
                    }
                    for (var j = 0; j < $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst.length; j++) {
                        $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].ischecked = false;
                        if ($scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].assign_lst == undefined) {
                            $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].assign_lst = [];
                        }
                        for (var k = 0; k < $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].assign_lst.length; k++) {
                            $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].assign_lst[k].ischecked = false;
                        }
                    }
                }

                $scope.selectGradebooknameforcopycategory($scope.cat.gb_term_code1);
            }

            $scope.copygradebookclasswiseshow = function () {
                $scope.classwisecopygradebook = true;
                $scope.GradeBookAssignments = false;
                $scope.GradeBookcategory = false;
                $scope.copygradebook = false;
                $scope.maincopy = false;

                $http.get(ENV.apiUrl + "api/Gradebook/getclasswiseGetGradeBookForCopy?termCode=" + $scope.copyg.gb_term_code + "&academic_year=" + $scope.copyg.academic_year + "&cur_code=" + $scope.copyg.cur_code).then(function (classwisegradebooksubject) {
                    $scope.classwisegradebooksubject = classwisegradebooksubject.data;


                });

                $http.get(ENV.apiUrl + "api/Gradebook/getclasswiseGetGradeBookForsubjectlist?academic_year=" + $scope.copyg.academic_year + "&cur_code=" + $scope.copyg.cur_code).then(function (classwisesubject) {
                    $scope.classwisesubject = classwisesubject.data;

                });

            }

            $scope.selectGradebooknameforcopycategory = function (str) {
                $http.get(ENV.apiUrl + "api/Gradebook/getTermwiseGetGradeBookForCopy?termCode=" + str + "&academic_year=" + $scope.copyg.academic_year + "&cur_code=" + $scope.copyg.cur_code).then(function (Term_wiseGetGradeBookForCopy) {
                    $scope.Term_wiseGetGradeBooks = Term_wiseGetGradeBookForCopy.data;
                });
            }

            $scope.CopyGradeBookWithCategorytonewGradebook = function (str) {

                var categorydatasendobject = [];
                if (str == undefined || str == "") {

                    swal({ text: 'Select Term Code', width: 300, showCloseButton: true });
                }
                else {

                    for (var i = 0; i < $scope.Term_wiseGetGradeBooks1.length; i++) {

                        if ($scope.Term_wiseGetGradeBooks1[i].gb_cat_lst == undefined) {
                            $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst = [];
                        }

                        for (var j = 0; j < $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst.length; j++) {
                            if ($scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].ischecked == true) {
                                for (var k = 0; k < $scope.Term_wiseGetGradeBooks.length; k++) {
                                    if ($scope.Term_wiseGetGradeBooks[k].ischecked == true) {
                                        var data = {
                                            gb_number: $scope.Term_wiseGetGradeBooks[k].gb_number
                                        , academic_year: $scope.Term_wiseGetGradeBooks[k].academic_year
                                        , cur_code: $scope.Term_wiseGetGradeBooks[k].cur_code
                                        , grade_code: $scope.Term_wiseGetGradeBooks[k].grade_code
                                        , section_code: $scope.Term_wiseGetGradeBooks[k].section_code
                                        , dTerm_code: $scope.cat.gb_term_code1
                                        , cat_gb_number: $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].sims_gb_number
                                        , cat_code: $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].sims_gb_cat_code
                                        , withAssign: $scope.withassignments == true ? "Y" : "N"

                                        }
                                        categorydatasendobject.push(data);
                                    }
                                }
                            }

                        }
                    }
                    if (categorydatasendobject.length > 0) {

                        $http.post(ENV.apiUrl + "api/Gradebook/CategoryCopy", categorydatasendobject).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 > 0) {
                                swal({ text: 'Category Copy Successfully', imageUrl: "assets/img/check.png", width: 350, showCloseButton: true });
                                $scope.Term_wiseGetGradeBooks = [];
                                $scope.withassignments = false;
                                $scope.GradeBookNamesforCopy();
                            }
                            else {
                                swal({ text: 'Category Not Copy', imageUrl: "assets/img/close.png", width: 350, showCloseButton: true });
                            }

                        });
                    }
                    else {

                        swal({ text: 'Select At Least One Category And Gradebook To Copy Category', imageUrl: "assets/img/notification-alert.png", width: 350, showCloseButton: true });
                    }
                }
            }

            $scope.SelectCategoryOrAssignments = function (info) {

                for (var i = 0; i < info.assign_lst.length; i++) {
                    if (info.ischecked == true) {
                        info.assign_lst[i].ischecked = true;
                    }
                    else {
                        info.assign_lst[i].ischecked = false;
                    }
                }
            }

            $scope.SelectGradebookNameOfAllCategory = function () {

                for (var i = 0; i < $scope.Term_wiseGetGradeBooks.length; i++) {
                    if ($scope.Term_wiseGetGradeBooks[i].categories == undefined) {
                        $scope.Term_wiseGetGradeBooks[i].categories = [];
                    }
                    for (var j = 0; j < $scope.Term_wiseGetGradeBooks[i].categories.length; j++) {

                        if ($scope.Term_wiseGetGradeBooks[i].ischecked == true) {
                            $scope.Term_wiseGetGradeBooks[i].categories[j].ischecked = true;
                        }
                        else {
                            $scope.Term_wiseGetGradeBooks[i].categories[j].ischecked = false;

                        }
                    }
                }
            }

            $scope.CopyGradeBookWithCategorywithAssignmentstonewGradebookCategory = function (str) {

                var gradebookassignmentdataobject = [];
                if (str == undefined || str == "") {

                    swal({ text: 'Select Term Code', width: 300, showCloseButton: true });
                }
                else {
                    for (var i = 0; i < $scope.Term_wiseGetGradeBooks1.length; i++) {

                        if ($scope.Term_wiseGetGradeBooks1[i].gb_cat_lst == undefined) {
                            $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst = [];
                        }

                        for (var j = 0; j < $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst.length; j++) {
                            if ($scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].assign_lst == undefined) {
                                $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].assign_lst = [];
                            }

                            for (var z = 0; z < $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].assign_lst.length; z++) {

                                if ($scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].assign_lst[z].ischecked == true) {
                                    for (var k = 0; k < $scope.Term_wiseGetGradeBooks.length; k++) {
                                        if ($scope.Term_wiseGetGradeBooks[k].categories == undefined) {
                                            $scope.Term_wiseGetGradeBooks[k].categories = [];
                                        }
                                        for (var l = 0; l < $scope.Term_wiseGetGradeBooks[k].categories.length; l++) {
                                            if ($scope.Term_wiseGetGradeBooks[k].categories[l].ischecked == true) {
                                                var data = {
                                                    to_gb_number: $scope.Term_wiseGetGradeBooks[k].categories[l].gb_number
                                                , to_cat_code: $scope.Term_wiseGetGradeBooks[k].categories[l].cat_code
                                                , gb_number: $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].assign_lst[z].sims_gb_number
                                                , cat_code: $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].assign_lst[z].sims_gb_cat_code
                                                , dTerm_code: str
                                                , Assignment_Code: $scope.Term_wiseGetGradeBooks1[i].gb_cat_lst[j].assign_lst[z].sims_gb_cat_assign_number
                                                }
                                                gradebookassignmentdataobject.push(data);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (gradebookassignmentdataobject.length > 0) {

                        $http.post(ENV.apiUrl + "api/Gradebook/AssignmentCopy", gradebookassignmentdataobject).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 > 0) {
                                swal({ text: 'Assignment Copy Successfully', imageUrl: "assets/img/check.png", width: 350, showCloseButton: true });

                                $scope.GradeBookNamesforCopy();
                            }
                            else {
                                swal({ text: 'Assignment Not Copy', imageUrl: "assets/img/close.png", width: 350, showCloseButton: true });
                            }

                        });
                    }
                    else {

                        swal({ text: 'Select At least One Assienment And One Category To Copy Assignment', imageUrl: "assets/img/notification-alert.png", width: 380, showCloseButton: true });
                    }
                }
            }

            $scope.CopyclasswisegradeBookinsert = function () {

                $scope.classwisecopyobject = [];

                for (var i = 0; i < $scope.classwisegradebooksubject.length; i++) {
                    for (var j = 0; j < $scope.classwisegradebooksubject[i].sectionslis.length; j++) {
                        if ($scope.classwisegradebooksubject[i].sectionslis[j].ischecked == true) {
                            for (var k = 0; k < $scope.classwisesubject.length; k++) {
                                for (var p = 0; p < $scope.classwisesubject[k].sectionslis.length; p++) {
                                    if ($scope.classwisesubject[k].sectionslis[p].ischecked == true) {
                                        if ($scope.classwisesubject[k].sectionslis[p].subjectslist == undefined) {
                                            $scope.classwisesubject[k].sectionslis[p].subjectslist = [];
                                        }
                                        for (var q = 0; q < $scope.classwisesubject[k].sectionslis[p].subjectslist.length; q++) {
                                            var data = {}
                                            data.cur_code = $scope.copyg.cur_code;
                                            data.academic_year = $scope.copyg.academic_year;
                                            data.gb_term_code = $scope.copyg.gb_term_code;
                                            data.grade_code = $scope.classwisegradebooksubject[i].sectionslis[j].sec_Grade_Code;
                                            data.section_code = $scope.classwisegradebooksubject[i].sectionslis[j].section_Code;
                                            data.SubjectCurr = $scope.copyg.cur_code;
                                            data.SubjectAcademic = $scope.copyg.academic_year;
                                            data.dTerm_code = $scope.copyg.gb_term_code;
                                            data.SubjectGrade = $scope.classwisesubject[k].sectionslis[p].subjectslist[q].subjectGrade;
                                            data.SubjectSection = $scope.classwisesubject[k].sectionslis[p].subjectslist[q].subjectSection;
                                            data.SubjectCode = $scope.classwisesubject[k].sectionslis[p].subjectslist[q].subjectCode;
                                            $scope.classwisecopyobject.push(data);

                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                $http.post(ENV.apiUrl + "api/Gradebook/ClassWiseGradeBookCopy", $scope.classwisecopyobject).then(function (message) {
                    $scope.message1 = message.data;

                    if ($scope.message1 == true) {
                        swal({ text: 'GradeBook Created Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                        $scope.copygradebookclasswiseshow();
                    } else {

                        swal({ text: 'GradeBook Not Created', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    }

                });


            }

            $scope.classwisecopyselectsection = function (info) {

                for (var i = 0; i < $scope.classwisegradebooksubject.length; i++) {
                    for (var j = 0; j < $scope.classwisegradebooksubject[i].sectionslis.length; j++) {
                        if ($scope.classwisegradebooksubject[i].sectionslis[j].section_Code == info.section_Code) {
                            if (info.ischecked == true) {
                                $scope.classwisegradebooksubject[i].sectionslis[j].ischecked = true;
                            }
                            else {
                                $scope.classwisegradebooksubject[i].sectionslis[j].ischecked = false;
                            }
                        }
                        else {
                            $scope.classwisegradebooksubject[i].sectionslis[j].ischecked = false;
                        }
                    }
                }
            }

            /***************************************************************End Copy GradeBook Code**************************************************************************************************/

            /***************************************************************Skip Student **************************************************************************************************/

            $scope.newheader = true;

            $scope.classdefault3 = 'col-md-10';
            $scope.classdefault1 = 'col-md-10';

            $scope.condenseMenu1 = function () {

                if ($scope.isCondensed) {
                    $scope.studentwiseMarkEntryShow2 = true;
                    $scope.classdefault1 = 'col-md-10'
                    $scope.isCondensed = false;
                } else {
                    $scope.studentwiseMarkEntryShow2 = false;
                    $scope.classdefault1 = 'col-md-12'
                    $scope.isCondensed = true;
                }
            };

            $scope.condenseMenu2 = function () {


                if ($scope.isCondensed) {
                    $scope.SkipStudentFromSubjectAssignments = true;
                    $scope.classdefault2 = 'col-md-10';
                    $scope.isCondensed = false;
                    $scope.SkipstudentSubjectWise1 = true;
                } else {
                    $scope.SkipStudentFromSubjectAssignments = true;
                    $scope.classdefault2 = 'col-md-12';
                    $scope.isCondensed = true;
                    $scope.SkipstudentSubjectWise1 = false;
                }
            };

            $scope.condenseMenu3 = function () {

                if ($scope.isCondensed) {
                    $scope.SkipStudentcategoryWiseshow1 = true;
                    $scope.classdefault3 = 'col-md-10'
                    $scope.isCondensed = false;
                } else {
                    $scope.SkipStudentcategoryWiseshow1 = false;
                    $scope.classdefault3 = 'col-md-12'
                    $scope.isCondensed = true;
                }
            };

            $scope.SkipStudentSubjectWiseshow = function () {

                $scope.updategradebookrecord = false;
                $scope.updateassign1 = 0;
                $scope.studentwiseMarkEntryShow = false;
                $scope.studentwiseMarkEntryShow1 = false;
                $scope.studentwiseMarkEntryShow2 = false;
                $scope.studentwiseMarkEntryShow3 = false;
                $scope.SkipStudentFromAssignments = false;
                $scope.mark = '';
                $scope.GradeBookCreation = false;
                $scope.GradeBookCategory = false;
                $scope.GradeBookAssignment = false;
                $scope.GradeSchemeShow = false;
                $scope.MarkEntryShow = false;
                $scope.GradebookComment = false;
                $scope.grdaebookname = false;
                $scope.SubjectAttribute = false;
                $scope.updateshow = false;
                $scope.editmode = false;
                $scope.updateassign = false;
                $scope.skipstudent = false;
                $scope.GradeBookCopy = false;
                $scope.myForm5.$setPristine();
                $scope.myForm5.$setUntouched();
                $scope.copygradebook = false;
                $scope.GradeBookcategory = false;
                $scope.GradeBookAssignments = false;
                $scope.curriculum1 = $scope.curriculum;
                $scope.Academic_year1 = $scope.Academic_year;
                $scope.TermsAcademicYear1 = $scope.TermsAcademicYear;
                $scope.SkipstudentSubjectWise1 = true;
            }

            $scope.GetAllStudentSubjectWiseForSkip = function () {


                $scope.busy3 = true;
                var count = 0;

                $scope.SkipStudentFromSubjectAssignments = false;
                $scope.classdefault2 = 'col-md-12';
                $http.post(ENV.apiUrl + "api/Gradebook/AllSubjectWiseAssignmentStudentSkip", $scope.skipstd).then(function (AllSubject_WiseAssignmentStudentSkip) {
                    $scope.AllSubject_WiseAssignmentStudentSkip = AllSubject_WiseAssignmentStudentSkip.data;

                    if ($scope.AllSubject_WiseAssignmentStudentSkip[0].length > 0 && $scope.AllSubject_WiseAssignmentStudentSkip[1].length > 0) {
                        $scope.subjectnames = angular.copy($scope.AllSubject_WiseAssignmentStudentSkip[0]);
                        $scope.skipstudentdata = angular.copy($scope.AllSubject_WiseAssignmentStudentSkip[1]);
                        $scope.count = $scope.subjectnames.length;
                        $scope.SkipstudentSubjectWise1 = false;
                        $scope.busy3 = false;
                        $scope.isCondensed = true;
                        $scope.SkipStudentFromSubjectAssignments = true;


                    }
                    else {
                        $scope.busy3 = false;
                        swal({ text: 'Student Not Found', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    }
                });

            }

            $scope.Resetskipstd = function () {
                $scope.skipstd = '';
            }

            $scope.SelectAllStudentskipForAllSubjects = function (str, str1) {

                $scope.dataAssignment = [];
                $scope.Assignstudentrecord = false;
                $scope.deleterecord = false;

                var check = document.getElementById(str);

                var dataobj1 = {};
                for (var i = 0; i < $scope.skipstudentdata.length; i++) {
                    for (var j = 0; j < $scope.skipstudentdata[i].subject_list.length; j++) {

                        if (check.checked == false && $scope.skipstudentdata[i].subject_list[j].subjectCode == str1) {

                            $scope.deleterecord = true;
                            $scope.skipstudentdata[i].subject_list[j].status = check.checked;

                            dataobj1 = {
                                Assignment_cur_code: $scope.skipstudentdata[i].subject_list[j].subjectCurr
                              , Assignment_academic_year: $scope.skipstudentdata[i].subject_list[j].subjectAcademic
                              , Assignment_grade_code: $scope.skipstudentdata[i].subject_list[j].subjectGrade
                              , Assignment_section_code: $scope.skipstudentdata[i].subject_list[j].subjectSection
                              , Assignment_subject_code: $scope.skipstudentdata[i].subject_list[j].subjectCode
                              , StudEnroll: $scope.skipstudentdata[i].stud_Enroll,
                                term_code: $scope.skipstd.gb_term_code
                                , OPR: 'DS'
                            };

                            $scope.dataAssignment.push(dataobj1);
                            dataobj1 = {};
                            break;

                        }
                        else {

                            if ($scope.skipstudentdata[i].subject_list[j].subjectCode == str1) {
                                $scope.Assignstudentrecord = true;
                                $scope.skipstudentdata[i].subject_list[j].status = check.checked;

                                dataobj1 = {
                                    Assignment_cur_code: $scope.skipstudentdata[i].subject_list[j].subjectCurr
                                      , Assignment_academic_year: $scope.skipstudentdata[i].subject_list[j].subjectAcademic
                                      , Assignment_grade_code: $scope.skipstudentdata[i].subject_list[j].subjectGrade
                                      , Assignment_section_code: $scope.skipstudentdata[i].subject_list[j].subjectSection
                                      , Assignment_subject_code: $scope.skipstudentdata[i].subject_list[j].subjectCode
                                      , StudEnroll: $scope.skipstudentdata[i].stud_Enroll,
                                    term_code: $scope.skipstd.gb_term_code
                                      , OPR: 'IS'
                                };

                                $scope.dataAssignment.push(dataobj1);
                                dataobj1 = {};
                                break;
                            }
                        }
                    }
                }



            }

            $scope.dataAssignment = [];

            $scope.Assignstudentrecord = false;

            $scope.deleterecord = false;

            $scope.SelectOneStudentskipForAllSubject = function (str) {

                $scope.Assignstudentrecord = false;
                $scope.deleterecord = false;
                $scope.dataAssignment = [];
                var check = document.getElementById(str);
                var dataobj1 = {};
                for (var i = 0; i < $scope.skipstudentdata.length; i++) {
                    if ($scope.skipstudentdata[i].stud_Enroll + $scope.skipstudentdata[i].stud_Full_Name == str) {
                        for (var j = 0; j < $scope.skipstudentdata[i].subject_list.length; j++) {
                            if (check.checked == false) {
                                $scope.skipstudentdata[i].subject_list[j].status = check.checked;
                                $scope.deleterecord = true;
                                dataobj1 = {
                                    Assignment_cur_code: $scope.skipstudentdata[i].subject_list[j].subjectCurr
                                  , Assignment_academic_year: $scope.skipstudentdata[i].subject_list[j].subjectAcademic
                                  , Assignment_grade_code: $scope.skipstudentdata[i].subject_list[j].subjectGrade
                                  , Assignment_section_code: $scope.skipstudentdata[i].subject_list[j].subjectSection
                                  , Assignment_subject_code: $scope.skipstudentdata[i].subject_list[j].subjectCode
                                  , StudEnroll: $scope.skipstudentdata[i].subject_list[j].stud_enroll,
                                    term_code: $scope.skipstd.gb_term_code
                                  , OPR: 'DS'
                                };

                                $scope.dataAssignment.push(dataobj1);
                                dataobj1 = {};

                            }
                            else {
                                $scope.skipstudentdata[i].subject_list[j].status = check.checked;
                                $scope.Assignstudentrecord = true;
                                dataobj1 = {
                                    Assignment_cur_code: $scope.skipstudentdata[i].subject_list[j].subjectCurr
                                     , Assignment_academic_year: $scope.skipstudentdata[i].subject_list[j].subjectAcademic
                                     , Assignment_grade_code: $scope.skipstudentdata[i].subject_list[j].subjectGrade
                                     , Assignment_section_code: $scope.skipstudentdata[i].subject_list[j].subjectSection
                                     , Assignment_subject_code: $scope.skipstudentdata[i].subject_list[j].subjectCode
                                     , StudEnroll: $scope.skipstudentdata[i].subject_list[j].stud_enroll,
                                    term_code: $scope.skipstd.gb_term_code
                                     , OPR: 'IS'
                                };

                                $scope.dataAssignment.push(dataobj1);
                                dataobj1 = {};
                            }
                        }
                    }
                }
            }

            $scope.SelectOneStudentskipForAllSubjectbtn_click = function () {

                if ($scope.deleterecord == true) {
                    if ($scope.dataAssignment.length > 0) {
                        $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.dataAssignment).then(function (AssignMarksToStudentsM) {
                            $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                            if ($scope.AssignMarks_ToStudentsM == true) {
                                swal({
                                    text: 'Student Skiped Successfully From This Assignment',
                                    imageUrl: "assets/img/check.png",
                                    width: 320,
                                    showCloseButton: true
                                });
                                $scope.classdefault2 = 'col-md-10';
                                $scope.SkipStudentFromSubjectAssignments = false;
                                $scope.SkipstudentSubjectWise1 = true;
                                $scope.GetAllStudentSubjectWiseForSkip();
                            }
                            else {
                                swal({
                                    text: 'Student Not Skiped From This Assignment',
                                    imageUrl: "assets/img/close.png",
                                    width: 300,
                                    showCloseButton: true
                                });
                                $scope.skipstudentdata = [];

                                $scope.skipstudentdata = angular.copy($scope.AllSubject_WiseAssignmentStudentSkip[1]);
                            }

                        });
                    }
                }

                if ($scope.Assignstudentrecord == true) {
                    if ($scope.dataAssignment.length > 0) {
                        $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.dataAssignment).then(function (AssignMarksToStudentsM) {
                            $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                            if ($scope.AssignMarks_ToStudentsM == true) {
                                swal({
                                    text: 'Assignment Mapped Successfully',
                                    imageUrl: "assets/img/check.png",
                                    width: 320,
                                    showCloseButton: true
                                });
                                $scope.classdefault2 = 'col-md-10';
                                $scope.SkipStudentFromSubjectAssignments = false;
                                $scope.SkipstudentSubjectWise1 = true;
                                $scope.GetAllStudentSubjectWiseForSkip();
                            }
                            else {
                                swal({
                                    text: 'Assignment Not Mapped',
                                    imageUrl: "assets/img/close.png",
                                    width: 300,
                                    showCloseButton: true
                                });
                                $scope.skipstudentdata = [];
                                $scope.skipstudentdata = angular.copy($scope.AllSubject_WiseAssignmentStudentSkip[1]);
                            }
                        });
                    }
                }
            }

            $scope.AddStudentForAssignment = [];

            $scope.SelectOneStudentForskipforsubjectwise = function (info) {

                $scope.dataAssignment = [];

                var data = {};

                data = {
                    Assignment_cur_code: info.subjectCurr
                , Assignment_academic_year: info.subjectAcademic
                , Assignment_grade_code: info.subjectGrade
                , Assignment_section_code: info.subjectSection
                , Assignment_subject_code: info.subjectCode
                , StudEnroll: info.stud_enroll,
                    term_code: $scope.skipstd.gb_term_code
                };


                if (info.status == true) {

                    data.OPR = "IS";
                    $scope.dataAssignment.push(data);

                    $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.dataAssignment).then(function (AssignMarksToStudentsM) {
                        $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                        if ($scope.AssignMarks_ToStudentsM == true) {
                            swal({
                                text: 'Assignment Mapped Successfully',
                                imageUrl: "assets/img/check.png",
                                width: 320,
                                showCloseButton: true
                            });
                            $scope.classdefault2 = 'col-md-10';
                            data = {};
                            $scope.SkipStudentFromSubjectAssignments = false;
                            $scope.SkipstudentSubjectWise1 = true;
                            $scope.GetAllStudentSubjectWiseForSkip();
                        }
                        else {
                            swal({
                                text: 'Assignment Not Mapped',
                                imageUrl: "assets/img/close.png",
                                width: 300,
                                showCloseButton: true
                            });
                            $scope.skipstudentdata = [];
                            $scope.skipstudentdata = angular.copy($scope.AllSubject_WiseAssignmentStudentSkip[1]);
                        }

                    });

                }
                else if (info.status == false) {

                    data.OPR = "DS";
                    $scope.dataAssignment.push(data);
                    $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.dataAssignment).then(function (AssignMarksToStudentsM) {
                        $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                        if ($scope.AssignMarks_ToStudentsM == true) {
                            swal({
                                text: 'Student Skiped Successfully From This Assignment',
                                imageUrl: "assets/img/check.png",
                                width: 320,
                                showCloseButton: true
                            });
                            $scope.classdefault1 = 'col-md-10';
                            data = {};
                            $scope.SkipStudentFromSubjectAssignments = false;
                            $scope.SkipstudentSubjectWise1 = true;
                            $scope.GetAllStudentSubjectWiseForSkip();
                        }
                        else {
                            swal({
                                text: 'Student Not Skiped From This Assignment',
                                imageUrl: "assets/img/close.png",
                                width: 300,
                                showCloseButton: true
                            });
                            $scope.skipstudentdata = [];
                            $scope.skipstudentdata = angular.copy($scope.AllSubject_WiseAssignmentStudentSkip[1]);
                        }

                    });
                }
                else {
                    info.insert = true;

                }

            }

            $scope.skipstudentcategoryorassignmentwise = function () {

                $scope.SkipStudentcategoryWiseshow2 = false;
                $scope.SkipStudentcategoryWiseshow1 = true;

                $scope.classdefault3 = 'col-md-10';
                $scope.busy3 = true;
                var data = {}
                data.cur_code = $scope.edt.cur_code;
                data.academic_year = $scope.edt.academic_year;
                data.grade_code = $scope.mark.sims_grade_code;
                data.section_code = $scope.mark.sims_section_code;
                data.sims_term_code = $scope.mark.gb_term_code;
                data.gb_number = $scope.mark.gb_number;
                data.sims_gb_cat_name = $scope.mark.cat_name;
                data.sims_gb_assign_name = $scope.mark.assignment_name;

                $scope.CategoryWiseStudentForSkip1 = [];

                $http.post(ENV.apiUrl + "api/Gradebook/CategoryWiseStudentForSkip", data).then(function (CategoryWiseStudentForSkip) {
                    $scope.CategoryWiseStudentForSkip1 = CategoryWiseStudentForSkip.data;


                    if ($scope.CategoryWiseStudentForSkip1.length > 0) {

                        $scope.subjectheading = angular.copy($scope.CategoryWiseStudentForSkip1[0]);
                        $scope.CategoryWiseStudentForSkip = angular.copy($scope.CategoryWiseStudentForSkip1[1]);
                        $scope.assignments = angular.copy($scope.CategoryWiseStudentForSkip[0].assignMents);
                        $scope.SkipStudentcategoryWiseshow2 = true;
                        $scope.busy3 = false;
                        $scope.SkipStudentcategoryWiseshow1 = false;
                        $scope.classdefault3 = 'col-md-12';
                    }
                    else {
                        $scope.busy3 = false;
                        swal({ text: 'Student Data Not Found', imageUrl: "assets/img/close.png", width: 320, shoeCloseButton: true });
                    }

                });
            }

            $scope.RenmovestudentCategoryWise = function (info, info1) {

                $scope.dataAssignment = [];

                var data = {};

                data = {
                    Assignment_cur_code: info1.assignment_cur_code
                , Assignment_academic_year: info1.assignment_academic_year
                , Assignment_grade_code: info1.assignment_grade_code
                , Assignment_section_code: info1.assignment_section_code
                , Assignment_subject_code: info1.assignment_subject_code
                , StudEnroll: info.stud_Enroll
                , term_code: $scope.mark.gb_term_code
                , Assignment_cat_code: info1.assignment_cat_code
                , Assignment_Code: info1.assignment_Code,
                    Assignment_gb_number: info1.assignment_gb_number
                };


                if (info1.assign_select_status == false && info1.assignment_Mark != '') {
                    info1.assign_select_status = true;
                    swal({
                        text: 'Are You Sure? You Want Be Skip Student For This Assignment',
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            data.OPR = "CD";
                            $scope.dataAssignment.push(data);
                            $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.dataAssignment).then(function (AssignMarksToStudentsM) {
                                $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                                if ($scope.AssignMarks_ToStudentsM == true) {
                                    swal({
                                        text: 'Student Skiped Successfully From This Assignment',
                                        imageUrl: "assets/img/check.png",
                                        width: 350,
                                        showCloseButton: true
                                    });
                                    $scope.skipstudentcategoryorassignmentwise();
                                }
                                else {
                                    swal({
                                        text: 'Student Not Skiped From This Assignment',
                                        imageUrl: "assets/img/close.png",
                                        width: 320,
                                        showCloseButton: true
                                    });
                                    $scope.CategoryWiseStudentForSkip = angular.copy($scope.CategoryWiseStudentForSkip1[1]);
                                }

                            });

                        }
                        else {
                            $scope.CategoryWiseStudentForSkip = angular.copy($scope.CategoryWiseStudentForSkip1[1]);
                        }
                    })
                }
                else if (info1.assign_select_status == false && info1.assignment_Mark == '') {

                    data.OPR = "CD";
                    $scope.dataAssignment.push(data);
                    $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.dataAssignment).then(function (AssignMarksToStudentsM) {
                        $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                        if ($scope.AssignMarks_ToStudentsM == true) {
                            swal({
                                text: 'Student Skiped Successfully From This Assignment',
                                imageUrl: "assets/img/check.png",
                                width: 350,
                                showCloseButton: true
                            });

                            $scope.skipstudentcategoryorassignmentwise();
                        }
                        else {
                            swal({
                                text: 'Student Not Skiped From This Assignment',
                                imageUrl: "assets/img/close.png",
                                width: 320,
                                showCloseButton: true
                            });

                        }

                    });
                }
                else if (info1.assign_select_status == true) {

                    data.OPR = "CI";
                    $scope.dataAssignment.push(data);

                    $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.dataAssignment).then(function (AssignMarksToStudentsM) {
                        $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                        if ($scope.AssignMarks_ToStudentsM == true) {
                            swal({
                                text: 'Assignment Mapped Successfully',
                                imageUrl: "assets/img/check.png",
                                width: 320,
                                showCloseButton: true
                            });
                            $scope.classdefault3 = 'col-md-10';
                            data = {};
                            $scope.skipstudentcategoryorassignmentwise();
                        }
                        else {
                            swal({
                                text: 'Assignment Not Mapped',
                                imageUrl: "assets/img/close.png",
                                width: 300,
                                showCloseButton: true
                            });
                            $scope.skipstudentcategoryorassignmentwise();
                        }

                    });

                }

            }

            $scope.SelectOneAssignmentforRenmoveAllStudentCategoryWise = function (assign_code, cat_code, gb_number, info) {
                $scope.Insertstudentcategorywiseonject = [];
                $scope.datadeleteobject = [];
                $scope.datadeleteobject1 = [];
                $scope.datacancelobject = {};
                $scope.categorywise = false;
                $scope.insert = false;

                for (var i = 0; i < $scope.CategoryWiseStudentForSkip.length; i++) {
                    for (var j = 0; j < $scope.CategoryWiseStudentForSkip[i].assignMents.length; j++) {
                        if (info.assign_status == false) {
                            if (info.assignment_Code == $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_Code && info.assignment_cat_code == $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_cat_code && info.assignment_gb_number == $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_gb_number && ($scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_Mark == '' || $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_Mark == 'NA') && ($scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_final_grade_desc == '' || $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_final_grade_desc == 'NA')) {
                                $scope.CategoryWiseStudentForSkip[i].assignMents[j].assign_select_status = false;
                                $scope.datacancelobject = $scope.CategoryWiseStudentForSkip[i].assignMents[j];
                            }
                            else {
                                if (info.assignment_Code == $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_Code && info.assignment_cat_code == $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_cat_code && info.assignment_gb_number == $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_gb_number) {
                                    $scope.categorywise = true;
                                    var data = {};
                                    data.stud_Enroll = $scope.CategoryWiseStudentForSkip[i].stud_Enroll;
                                    data.stud_Full_Name = $scope.CategoryWiseStudentForSkip[i].stud_Full_Name;
                                    data.assignment_Code = assign_code;
                                    data.assignment_cat_code = cat_code;
                                    data.assignment_gb_number = gb_number
                                    $scope.datadeleteobject1.push(data);
                                }
                            }

                        }
                        else {
                            if (info.assignment_Code == $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_Code && info.assignment_cat_code == $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_cat_code && info.assignment_gb_number == $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_gb_number) {
                                $scope.CategoryWiseStudentForSkip[i].assignMents[j].assign_select_status = true;
                                $scope.insert = true;
                                data = {
                                    Assignment_cur_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_cur_code
                                   , Assignment_academic_year: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_academic_year
                                   , Assignment_grade_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_grade_code
                                   , Assignment_section_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_section_code
                                   , Assignment_subject_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_subject_code
                                   , StudEnroll: $scope.CategoryWiseStudentForSkip[i].stud_Enroll
                                   , term_code: $scope.mark.gb_term_code
                                   , Assignment_cat_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_cat_code
                                   , Assignment_Code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_Code
                                    , Assignment_gb_number: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_gb_number
                                    , OPR: "CI"
                                };

                                $scope.Insertstudentcategorywiseonject.push(data);
                            }
                        }
                    }
                }
                if ($scope.categorywise == true && $scope.insert == false) {
                    $scope.categorywise = false;
                    $scope.datadeleteobject = $scope.datadeleteobject1;
                    $('#StudentSkipmodalpopup').modal({ backdrop: "static" })
                }
                else if (info.assign_status == false && $scope.categorywise == false) {
                    for (var i = 0; i < $scope.CategoryWiseStudentForSkip.length; i++) {
                        for (var j = 0; j < $scope.CategoryWiseStudentForSkip[i].assignMents.length; j++) {
                            data = {
                                Assignment_cur_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_cur_code
                                   , Assignment_academic_year: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_academic_year
                                   , Assignment_grade_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_grade_code
                                   , Assignment_section_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_section_code
                                   , Assignment_subject_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_subject_code
                                   , StudEnroll: $scope.CategoryWiseStudentForSkip[i].stud_Enroll
                                   , term_code: $scope.mark.gb_term_code
                                   , Assignment_cat_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_cat_code
                                   , Assignment_Code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_Code
                                    , Assignment_gb_number: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_gb_number
                                    , OPR: "CD"
                            };

                            $scope.dataAssignment.push(data);
                        }
                    }

                    $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.dataAssignment).then(function (AssignMarksToStudentsM) {
                        $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                        if ($scope.AssignMarks_ToStudentsM == true) {
                            swal({
                                text: 'Student Skiped Successfully From This Assignment',
                                imageUrl: "assets/img/check.png",
                                width: 350,
                                showCloseButton: true
                            });
                            $scope.skipstudentcategoryorassignmentwise();
                        }
                        else {
                            swal({
                                text: 'Student Not Skiped From This Assignment',
                                imageUrl: "assets/img/close.png",
                                width: 320,
                                showCloseButton: true
                            });
                            $scope.CategoryWiseStudentForSkip = angular.copy($scope.CategoryWiseStudentForSkip1[1]);
                        }

                    });
                }
            }

            $scope.btn_Ok_Click = function () {

                $scope.skipstudentcategorywiseonject = [];

                for (var i = 0; i < $scope.CategoryWiseStudentForSkip.length; i++) {
                    for (var j = 0; j < $scope.CategoryWiseStudentForSkip[i].assignMents.length; j++) {
                        if ($scope.datadeleteobject[0].assignment_Code == $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_Code && $scope.datadeleteobject[0].assignment_cat_code == $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_cat_code && $scope.datadeleteobject[0].assignment_gb_number == $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_gb_number) {

                            data = {
                                Assignment_cur_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_cur_code
                                   , Assignment_academic_year: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_academic_year
                                   , Assignment_grade_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_grade_code
                                   , Assignment_section_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_section_code
                                   , Assignment_subject_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_subject_code
                                   , StudEnroll: $scope.CategoryWiseStudentForSkip[i].stud_Enroll
                                   , term_code: $scope.mark.gb_term_code
                                   , Assignment_cat_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_cat_code
                                   , Assignment_Code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_Code
                                    , Assignment_gb_number: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_gb_number
                                    , OPR: "CD"
                            };

                            $scope.skipstudentcategorywiseonject.push(data);
                        }
                    }
                }


                $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.skipstudentcategorywiseonject).then(function (AssignMarksToStudentsM) {
                    $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                    if ($scope.AssignMarks_ToStudentsM == true) {
                        swal({
                            text: 'Student Skiped Successfully From This Assignment',
                            imageUrl: "assets/img/check.png",
                            width: 350,
                            showCloseButton: true
                        });
                        $scope.skipstudentcategoryorassignmentwise();
                    }
                    else {
                        swal({
                            text: 'Student Not Skiped From This Assignment',
                            imageUrl: "assets/img/close.png",
                            width: 320,
                            showCloseButton: true
                        });
                        $scope.CategoryWiseStudentForSkip = angular.copy($scope.CategoryWiseStudentForSkip1[1]);
                    }

                });


            }

            $scope.btn_cancel_Click = function () {

                $scope.skipstudentcategorywiseonject = [];

                for (var i = 0; i < $scope.CategoryWiseStudentForSkip.length; i++) {
                    for (var j = 0; j < $scope.CategoryWiseStudentForSkip[i].assignMents.length; j++) {

                        if ($scope.datacancelobject.assignment_Code == $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_Code && $scope.datacancelobject.assignment_cat_code == $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_cat_code && $scope.datacancelobject.assignment_gb_number == $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_gb_number && ($scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_Mark == '' || $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_Mark == 'NA') && ($scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_final_grade_desc == '' || $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_final_grade_desc == 'NA')) {
                            data = {
                                Assignment_cur_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_cur_code
                                   , Assignment_academic_year: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_academic_year
                                   , Assignment_grade_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_grade_code
                                   , Assignment_section_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_section_code
                                   , Assignment_subject_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_subject_code
                                   , StudEnroll: $scope.CategoryWiseStudentForSkip[i].stud_Enroll
                                   , term_code: $scope.mark.gb_term_code
                                   , Assignment_cat_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_cat_code
                                   , Assignment_Code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_Code
                                   , Assignment_gb_number: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_gb_number
                                    , OPR: "CD"
                            };

                            $scope.skipstudentcategorywiseonject.push(data);
                        }
                    }
                }


                $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.skipstudentcategorywiseonject).then(function (AssignMarksToStudentsM) {
                    $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                    if ($scope.AssignMarks_ToStudentsM == true) {
                        swal({
                            text: 'Student Skiped Successfully From This Assignment',
                            imageUrl: "assets/img/check.png",
                            width: 350,
                            showCloseButton: true
                        });
                        $scope.skipstudentcategoryorassignmentwise();
                    }
                    else {
                        swal({
                            text: 'Student Not Skiped From This Assignment',
                            imageUrl: "assets/img/close.png",
                            width: 320,
                            showCloseButton: true
                        });
                        $scope.CategoryWiseStudentForSkip = angular.copy($scope.CategoryWiseStudentForSkip1[1]);
                    }

                });


            }

            $scope.SelectOnestudentforRenmoveAllAssignmentofCategory = function (enroll, name, status, info) {

                $scope.Insertstudentcategorywiseonject = [];
                $scope.datacancelobject = []
                $scope.datacancelobject1 = [];
                $scope.datadeleteobject1 = [];
                $scope.datadeleteobject = [];
                $scope.categorywise = false;
                if (status == false) {
                    for (var i = 0; i < info.assignMents.length; i++) {
                        if (info.assignMents[i].assignment_Mark == '' || info.assignMents[i].assignment_Mark == 'NA') {
                            info.assignMents[i].assign_select_status = false;
                            info.assignMents[i].stud_Enroll = info.stud_Enroll;
                            info.assignMents[i].stud_Full_Name = info.stud_Full_Name;
                            $scope.datacancelobject1.push(info.assignMents[i]);
                        } else {

                            $scope.categorywise = true;
                            info.assignMents[i].stud_Enroll = info.stud_Enroll;
                            info.assignMents[i].stud_Full_Name = info.stud_Full_Name;
                            $scope.datadeleteobject1.push(info.assignMents[i]);
                        }
                    }

                }
                else {

                    for (var i = 0; i < info.assignMents.length; i++) {
                        info.assignMents[i].assign_select_status = true;
                        data = {
                            Assignment_cur_code: info.assignMents[i].assignment_cur_code
                                   , Assignment_academic_year: info.assignMents[i].assignment_academic_year
                                   , Assignment_grade_code: info.assignMents[i].assignment_grade_code
                                   , Assignment_section_code: info.assignMents[i].assignment_section_code
                                   , Assignment_subject_code: info.assignMents[i].assignment_subject_code
                                   , StudEnroll: info.stud_Enroll
                                   , term_code: $scope.mark.gb_term_code
                                   , Assignment_cat_code: info.assignMents[i].assignment_cat_code
                                   , Assignment_Code: info.assignMents[i].assignment_Code
                                   , Assignment_gb_number: info.assignMents[i].assignment_gb_number
                                    , OPR: "CI"
                        };

                        $scope.Insertstudentcategorywiseonject.push(data);


                    }
                }


                if ($scope.categorywise == true && status == false) {
                    $scope.categorywise = false;
                    $scope.datadeleteobject = $scope.datadeleteobject1;
                    $('#StudentSkipmodalpopup1').modal({ backdrop: "static" })
                }
                else if ($scope.categorywise == false && status == false) {

                    $scope.skipstudentcategorywiseonject = [];

                    for (var i = 0; i < info.assignMents.length; i++) {
                        info.assignMents[i].assign_select_status = false;
                        data = {
                            Assignment_cur_code: info.assignMents[i].assignment_cur_code
                                   , Assignment_academic_year: info.assignMents[i].assignment_academic_year
                                   , Assignment_grade_code: info.assignMents[i].assignment_grade_code
                                   , Assignment_section_code: info.assignMents[i].assignment_section_code
                                   , Assignment_subject_code: info.assignMents[i].assignment_subject_code
                                   , StudEnroll: info.stud_Enroll
                                   , term_code: $scope.mark.gb_term_code
                                   , Assignment_cat_code: info.assignMents[i].assignment_cat_code
                                   , Assignment_Code: info.assignMents[i].assignment_Code
                                   , Assignment_gb_number: info.assignMents[i].assignment_gb_number
                                    , OPR: "CD"
                        };

                        $scope.skipstudentcategorywiseonject.push(data);

                    }
                    $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.skipstudentcategorywiseonject).then(function (AssignMarksToStudentsM) {
                        $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                        if ($scope.AssignMarks_ToStudentsM == true) {
                            swal({
                                text: 'Student Skiped Successfully From This Assignment',
                                imageUrl: "assets/img/check.png",
                                width: 350,
                                showCloseButton: true
                            });
                            $scope.skipstudentcategoryorassignmentwise();
                        }
                        else {
                            swal({
                                text: 'Student Not Skiped From This Assignment',
                                imageUrl: "assets/img/close.png",
                                width: 320,
                                showCloseButton: true
                            });
                            $scope.CategoryWiseStudentForSkip = angular.copy($scope.CategoryWiseStudentForSkip1[1]);
                        }

                    });

                }
            }

            $scope.btn_Ok_Click1 = function () {

                $scope.skipstudentcategorywiseonject = [];

                for (var i = 0; i < $scope.CategoryWiseStudentForSkip.length; i++) {
                    for (var j = 0; j < $scope.CategoryWiseStudentForSkip[i].assignMents.length; j++) {
                        if ($scope.datadeleteobject[0].stud_Enroll == $scope.CategoryWiseStudentForSkip[i].stud_Enroll) {
                            data = {
                                Assignment_cur_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_cur_code
                                   , Assignment_academic_year: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_academic_year
                                   , Assignment_grade_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_grade_code
                                   , Assignment_section_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_section_code
                                   , Assignment_subject_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_subject_code
                                   , StudEnroll: $scope.CategoryWiseStudentForSkip[i].stud_Enroll
                                   , term_code: $scope.mark.gb_term_code
                                   , Assignment_cat_code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_cat_code
                                   , Assignment_Code: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_Code
                                , Assignment_gb_number: $scope.CategoryWiseStudentForSkip[i].assignMents[j].assignment_gb_number
                                    , OPR: "CD"
                            };

                            $scope.skipstudentcategorywiseonject.push(data);
                        }
                    }
                }


                $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.skipstudentcategorywiseonject).then(function (AssignMarksToStudentsM) {
                    $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                    if ($scope.AssignMarks_ToStudentsM == true) {
                        swal({
                            text: 'Student Skiped Successfully From This Assignment',
                            imageUrl: "assets/img/check.png",
                            width: 350,
                            showCloseButton: true
                        });
                        $scope.skipstudentcategoryorassignmentwise();
                    }
                    else {
                        swal({
                            text: 'Student Not Skiped From This Assignment',
                            imageUrl: "assets/img/close.png",
                            width: 320,
                            showCloseButton: true
                        });
                        $scope.CategoryWiseStudentForSkip = angular.copy($scope.CategoryWiseStudentForSkip1[1]);
                    }

                });


            }

            $scope.btn_cancel_Click1 = function () {

                $scope.skipstudentcategorywiseonject = [];

                for (var k = 0; k < $scope.datacancelobject1.length; k++) {
                    data = {
                        Assignment_cur_code: $scope.datacancelobject1[k].assignment_cur_code
                           , Assignment_academic_year: $scope.datacancelobject1[k].assignment_academic_year
                           , Assignment_grade_code: $scope.datacancelobject1[k].assignment_grade_code
                           , Assignment_section_code: $scope.datacancelobject1[k].assignment_section_code
                           , Assignment_subject_code: $scope.datacancelobject1[k].assignment_subject_code
                           , StudEnroll: $scope.datacancelobject1[k].stud_Enroll
                           , term_code: $scope.mark.gb_term_code
                           , Assignment_cat_code: $scope.datacancelobject1[k].assignment_cat_code
                           , Assignment_Code: $scope.datacancelobject1[k].assignment_Code
                            , Assignment_gb_number: $scope.datacancelobject1[k].assignment_gb_number
                            , OPR: "CD"
                    };

                    $scope.skipstudentcategorywiseonject.push(data);
                }


                $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.skipstudentcategorywiseonject).then(function (AssignMarksToStudentsM) {
                    $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                    if ($scope.AssignMarks_ToStudentsM == true) {
                        swal({
                            text: 'Student Skiped Successfully From This Assignment',
                            imageUrl: "assets/img/check.png",
                            width: 350,
                            showCloseButton: true
                        });

                        $scope.skipstudentcategoryorassignmentwise();
                    }
                    else {
                        swal({
                            text: 'Student Not Skiped From This Assignment',
                            imageUrl: "assets/img/close.png",
                            width: 320,
                            showCloseButton: true
                        });
                        $scope.CategoryWiseStudentForSkip = angular.copy($scope.CategoryWiseStudentForSkip1[1]);
                    }

                });


            }

            $scope.InsertstudenttoAssignmentCategorywise = function () {
                $http.post(ENV.apiUrl + "api/Gradebook/skipstudentforSubjectwise", $scope.Insertstudentcategorywiseonject).then(function (AssignMarksToStudentsM) {
                    $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;
                    if ($scope.AssignMarks_ToStudentsM == true) {
                        swal({
                            text: 'Assignment Mapped Successfully',
                            imageUrl: "assets/img/check.png",
                            width: 350,
                            showCloseButton: true
                        });

                        $scope.skipstudentcategoryorassignmentwise();
                    }
                    else {
                        swal({
                            text: 'Assignment Not Mapped',
                            imageUrl: "assets/img/close.png",
                            width: 320,
                            showCloseButton: true
                        });
                        $scope.CategoryWiseStudentForSkip = angular.copy($scope.CategoryWiseStudentForSkip1[1]);
                    }

                });
            }

            /***************************************************************Skip Student **************************************************************************************************/

            /***************************************************************Start updation Code**************************************************************************************************/

            $scope.updatename = true;

            $scope.GetColumns = function (str) {
                var data = {}
                data = {
                    sims_table_name_code: str
                }

                $http.post(ENV.apiUrl + "api/Gradebook/TableColumnNames", data).then(function (TableColumnNames) {
                    $scope.Table_Column_Names = TableColumnNames.data;
                });
            }

            $scope.GetColumnvalues = function (str) {

                $scope.parametervalue = '';
                $scope.updatename = false;
                var data = {};
                for (var i = 0; i < $scope.Table_Column_Names.length; i++) {
                    if ($scope.Table_Column_Names[i].column_name_code == str) {

                        $scope.Table_Column_Names[i]['sims_cur_code'] = $scope.upd.cur_code;
                        $scope.Table_Column_Names[i]['sims_academic_year'] = $scope.upd.academic_year;
                        $scope.Table_Column_Names[i]['sims_grade_code'] = $scope.upd.sims_grade_code;
                        $scope.Table_Column_Names[i]['sims_section_code'] = $scope.upd.sims_section_code;
                        $scope.Table_Column_Names[i]['sims_term_code'] = $scope.upd.gb_term_code;

                        data = $scope.Table_Column_Names[i];
                        $scope.parametervalue = $scope.Table_Column_Names[i].sims_appl_parameter;

                    }
                }

                if (str == 'sims_gb_status' || str == 'sims_gb_cat_status' || str == 'sims_gb_cat_assign_editable' || str == 'sims_gb_cat_completed' || str == 'sims_gb_completed' || str == 'sims_gb_cat_assign_grade_completed_status') {
                    data = {
                        cur_code: $scope.upd.cur_code
                     , academic_year: $scope.upd.academic_year
                     , grade_code: $scope.upd.sims_grade_code
                     , section_code: $scope.upd.sims_section_code
                     , term_code: $scope.upd.gb_term_code
                     , PropertyName: str
                     , OPR: $scope.parametervalue
                    };

                    $scope.updatename = false;

                    $http.post(ENV.apiUrl + "api/Gradebook/MassupdateGbListProps", data).then(function (MassupdateGbListProps) {
                        $scope.Massupdate_GbListProps = MassupdateGbListProps.data;

                    });
                }
                else {
                    $scope.updatename = true;
                    $http.post(ENV.apiUrl + "api/Gradebook/TableUpdationRecords", data).then(function (TableUpdation_Records) {
                        $scope.Table_Updation_Records = TableUpdation_Records.data;
                    });

                }
            }

            $scope.Updatecolumnvalues = function () {

                var data = {};

                if ($scope.upd.column_new_value == '' || $scope.upd.column_new_value == undefined) {

                    swal({ text: 'Enter New Column Value', width: 320, showCloseButton: true });
                }
                else {

                    data = {
                        sims_appl_parameter: $scope.parametervalue
                    , sims_cur_code: $scope.upd.cur_code
                    , sims_table_name_code: $scope.upd.sims_table_name_code
                    , column_name_code: $scope.upd.column_name_code
                    , sims_academic_year: $scope.upd.academic_year
                    , sims_grade_code: $scope.upd.sims_grade_code
                    , sims_section_code: $scope.upd.sims_section_code
                    , sims_term_code: $scope.upd.gb_term_code
                    , column_old_value: $scope.upd.column_old_value
                    , column_new_value: $scope.upd.column_new_value
                    };

                    $http.post(ENV.apiUrl + "api/Gradebook/CUDTableColumnNamesUpdate", data).then(function (MSG) {
                        $scope.msg = MSG.data;

                        if ($scope.msg == true) {
                            swal({ text: 'Record Updated Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                            $scope.upd = '';
                            $scope.upd = {
                                cur_code: $scope.edt.cur_code,
                                academic_year: $scope.edt.academic_year,
                                gb_term_code: $scope.edt.gb_term_code,

                            };
                        }
                        else {
                            swal({ text: 'Record Not Updated', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                        }
                    });
                }
            }

            $scope.CleareValues = function () {
                $scope.upd['sims_table_name_code'] = '';
                $scope.upd['column_name_code'] = '';
                $scope.upd['column_old_value'] = '';
            }

            $scope.ClearEffortValues = function () {
                $scope.CleareValues();
                $scope.effortflag = true;
                var a = { opr: 'A', cur_code: $scope.upd.cur_code, aca_year: $scope.upd.academic_year, term_code: $scope.upd.gb_term_code, grade_code: $scope.upd.sims_grade_code, section_code: $scope.upd.sims_section_code };
                $http.post(ENV.apiUrl + "api/Gradebook/GradeBookEffortCommon", a).then(function (res) {
                    $scope.csData = res.data.table;
                    $scope.gbData = res.data.table1;
                });
            }
            
            $scope.ClearEffortValuesCS = function () {
                var a = { opr: 'B', cur_code: $scope.upd.cur_code, aca_year: $scope.upd.academic_year, term_code: $scope.upd.gb_term_code, grade_code: $scope.upd.sims_grade_code, section_code: $scope.upd.sims_section_code, group_code: $scope.upd.cs_group_code };
                $http.post(ENV.apiUrl + "api/Gradebook/GradeBookEffortCommon", a).then(function (res) {
                    $scope.csAttData = res.data.table;
                });
            }


            $scope.ClearEffortValuesGB = function () {
                var a = { opr: 'C', cur_code: $scope.upd.cur_code, aca_year: $scope.upd.academic_year, term_code: $scope.upd.gb_term_code, grade_code: $scope.upd.sims_grade_code, section_code: $scope.upd.sims_section_code, gb_number: $scope.upd.gb_code };
                $http.post(ENV.apiUrl + "api/Gradebook/GradeBookEffortCommon", a).then(function (res) {
                    $scope.csCatData = res.data.table;
                });
            }


            $scope.ClearEffortValuesCat = function () {
                var a = { opr: 'D', cur_code: $scope.upd.cur_code, aca_year: $scope.upd.academic_year, term_code: $scope.upd.gb_term_code, grade_code: $scope.upd.sims_grade_code, section_code: $scope.upd.sims_section_code, gb_number: $scope.upd.gb_code, cat_code: $scope.upd.catCode };
                $http.post(ENV.apiUrl + "api/Gradebook/GradeBookEffortCommon", a).then(function (res) {
                    $scope.csAssignData = res.data.table;
                });
            }
            $scope.CopyEffortConfig = function () {
                var a = { opr: 'E', cur_code: $scope.upd.cur_code, aca_year: $scope.upd.academic_year, term_code: $scope.upd.gb_term_code, grade_code: $scope.upd.sims_grade_code, section_code: $scope.upd.sims_section_code, gb_number: $scope.upd.gb_code, cat_code: $scope.upd.catCode, assign_number: $scope.upd.assignNum, group_code: $scope.upd.cs_group_code, attr_code: $scope.upd.attrCode };
                $http.post(ENV.apiUrl + "api/Gradebook/GradeBookEffortCommon", a).then(function (res) {
                    $scope.csAssignData = res.data.table;
                    swal({ text: 'Effort Mark(s) are copied successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                });
                console.log("Copy Effort into gradebook", $scope.csAssignData);
            }

            $scope.checkvaluesgardebook = function (gardebook, category, assignment) {

                if (gardebook != undefined) {
                    gardebook.ischange = true;
                    for (var i = 0; i < gardebook.categories.length; i++) {
                        if (gardebook.isSelected == true) {
                            gardebook.categories[i].isSelected = true;
                            gardebook.categories[i].ischange = true;
                            if (gardebook.categories[i].assignMents == undefined) {
                                gardebook.categories[i].assignMents = [];
                            }
                            for (var f = 0; f < gardebook.categories[i].assignMents.length; f++) {
                                gardebook.categories[i].assignMents[f].isSelected = true;
                                gardebook.categories[i].assignMents[f].ischange = true;
                            }
                        }
                        else {
                            gardebook.categories[i].isSelected = false;
                            gardebook.categories[i].ischange = true;
                            for (var f = 0; f < gardebook.categories[i].assignMents.length; f++) {
                                gardebook.categories[i].assignMents[f].isSelected = false;
                                gardebook.categories[i].assignMents[f].ischange = true;
                            }
                        }
                    }
                }
            }

            $scope.checkvaluesgardebook1 = function (gardebook, category, assignment) {
                if (category != undefined) {
                    gardebook.ischange = true;
                    category.ischange = true;
                    for (var k = 0; k < category.assignMents.length; k++) {
                        if (category.isSelected == true) {
                            category.assignMents[k].isSelected = true;
                        }
                        else {
                            category.assignMents[k].isSelected = false;
                        }
                    }

                }
            }

            $scope.checkvaluesgardebook2 = function (gardebook, category, assignment) {
                if (assignment != undefined) {
                    gardebook.ischange = true;
                    category.ischange = true;
                    assignment.ischange = true;

                }
            }

            $scope.SaveMassUpdateAccessCotrol = function () {

                var updatedataobject = [];
                $scope.isinsert = false;

                if ($scope.upd.column_name_code == 'sims_gb_cat_assign_editable' || $scope.upd.column_name_code == 'sims_gb_cat_assign_grade_completed_status') {
                    //Assignment freeze  code and Grade completed status code

                    for (var i = 0; i < $scope.Massupdate_GbListProps.length; i++) {
                        if ($scope.Massupdate_GbListProps[i].categories == undefined) {
                            $scope.Massupdate_GbListProps[i].categories = [];
                        }
                        for (var j = 0; j < $scope.Massupdate_GbListProps[i].categories.length; j++) {
                            if ($scope.Massupdate_GbListProps[i].categories[j].assignMents == undefined) {
                                $scope.Massupdate_GbListProps[i].categories[j].assignMents = [];
                            }
                            for (var k = 0; k < $scope.Massupdate_GbListProps[i].categories[j].assignMents.length; k++) {
                                if ($scope.Massupdate_GbListProps[i].ischange == true && $scope.Massupdate_GbListProps[i].categories[j].ischange == true && $scope.Massupdate_GbListProps[i].categories[j].assignMents[k].ischange == true) {
                                    $scope.isinsert = true;
                                    var data = {}
                                    data.RelationName = $scope.upd.sims_table_name_code;
                                    data.PropertyName = $scope.upd.column_name_code;

                                    if ($scope.Massupdate_GbListProps[i].categories[j].assignMents[k].isSelected == true) {
                                        data.Value = 'T'
                                    }
                                    else {
                                        data.Value = 'F'
                                    }

                                    data.CriteriaValue = "sims_gb_number+sims_gb_cat_code+sims_gb_cat_assign_number";
                                    data.Level = '3';
                                    data.GB_number = $scope.Massupdate_GbListProps[i].categories[j].assignMents[k].sims_gb_number;
                                    data.GB_Cat_number = $scope.Massupdate_GbListProps[i].categories[j].assignMents[k].sims_gb_cat_number;
                                    data.GB_Cat_AssignNumber = $scope.Massupdate_GbListProps[i].categories[j].assignMents[k].assignment_number;
                                    updatedataobject.push(data);
                                }
                            }
                        }
                    }
                }

                else if ($scope.upd.column_name_code == 'sims_gb_cat_status' || $scope.upd.column_name_code == 'sims_gb_cat_completed') {
                    //Category status code and Category Freeze code

                    for (var i = 0; i < $scope.Massupdate_GbListProps.length; i++) {
                        for (var j = 0; j < $scope.Massupdate_GbListProps[i].categories.length; j++) {
                            if ($scope.Massupdate_GbListProps[i].ischange == true && $scope.Massupdate_GbListProps[i].categories[j].ischange == true) {
                                $scope.isinsert = true;

                                var data = {}
                                data.RelationName = $scope.upd.sims_table_name_code;
                                data.PropertyName = $scope.upd.column_name_code;

                                if ($scope.upd.column_name_code == 'sims_gb_cat_completed') {
                                    if ($scope.Massupdate_GbListProps[i].categories[j].isSelected == true) {
                                        data.Value = 'T'
                                    }
                                    else {
                                        data.Value = 'F'
                                    }
                                }
                                else {

                                    if ($scope.Massupdate_GbListProps[i].categories[j].isSelected == true) {
                                        data.Value = 'A'
                                    }
                                    else {
                                        data.Value = 'I'
                                    }

                                }
                                data.CriteriaValue = "sims_gb_number+sims_gb_cat_code";
                                data.Level = '2';
                                data.GB_number = $scope.Massupdate_GbListProps[i].categories[j].sims_gb_number;
                                data.GB_Cat_number = $scope.Massupdate_GbListProps[i].categories[j].category_code;
                                updatedataobject.push(data);

                            }
                        }
                    }
                }

                else if ($scope.upd.column_name_code == 'sims_gb_status' || $scope.upd.column_name_code == 'sims_gb_completed') {
                    //Gradebook status code And Gradebook freeze
                    for (var i = 0; i < $scope.Massupdate_GbListProps.length; i++) {

                        if ($scope.Massupdate_GbListProps[i].ischange == true) {
                            $scope.isinsert = true;

                            var data = {}
                            data.RelationName = $scope.upd.sims_table_name_code;
                            data.PropertyName = $scope.upd.column_name_code;
                            if ($scope.upd.column_name_code == 'sims_gb_status') {
                                if ($scope.Massupdate_GbListProps[i].isSelected == true) {
                                    data.Value = 'A'
                                }
                                else {
                                    data.Value = 'I'
                                }
                            }
                            else {
                                if ($scope.Massupdate_GbListProps[i].isSelected == true) {
                                    data.Value = 'T'
                                }
                                else {
                                    data.Value = 'F'
                                }
                            }
                            data.CriteriaValue = "sims_gb_number";
                            data.Level = '1';
                            data.GB_number = $scope.Massupdate_GbListProps[i].sims_gb_number;
                            updatedataobject.push(data);
                        }
                    }
                }

                if ($scope.isinsert == true) {
                    $http.post(ENV.apiUrl + "api/Gradebook/SaveMassupdateGbListProps", updatedataobject).then(function (message) {
                        $scope.message = message.data;

                        if (parseInt($scope.message) > 0) {
                            swal({ text: 'Record Updated Successfully', imageUrl: "assets/img/check.png", width: 300, showCloseButton: true });

                            $scope.upd['sims_grade_code'] = '';
                            $scope.upd['sims_section_code'] = '';
                            $scope.upd['sims_table_name_code'] = '';
                            $scope.upd['column_name_code'] = '';

                            $scope.updatename = true;
                            $scope.Expand = '';
                        }
                        else {
                            swal({ text: 'Record Not Updated', imageUrl: "assets/img/close.png", width: 300, showCloseButton: true });

                        }
                    });
                }
                else {

                    swal({ text: 'Do Atleast One Change To Update Record', imageUrl: "assets/img/notification-alert.png", width: 320, showCloseButton: true });
                }

            }


            /***************************************************************End updation Code**************************************************************************************************/

            /***************************************************************Final Comment Start**************************************************************************************************/
            $scope.newComment = false;
            $scope.newheader = false;

            $scope.maincomment = true;

            $scope.updategradingoncheckgradecheckbox = function (info) {

                $scope.updategrading['Assignment_GradeCompleted_Status'] = info.assignment_GradeCompleted_Status;
                info.statuss = info.assignment_GradeCompleted_Status;
                $http.post(ENV.apiUrl + "api/Gradebook/updateGgradingcompleted", $scope.updategrading).then(function (AssignMarksToStudentsM) {
                    $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;

                    $scope.GradeBookNames();
                });
            }

            $scope.getacyr1 = function (str) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.getAcademicYear1 = Academicyear.data;
                    $scope.fcommn['academic_year'] = $scope.getAcademicYear1[0].sims_academic_year;
                    $scope.getsections2($scope.fcommn.cur_code, $scope.getAcademicYear1[0].sims_academic_year);
                });
            }

            $scope.Getterm2 = function (cur_code, oldval) {

                $http.get(ENV.apiUrl + "api/Gradebook/getTermforGradebook?cur_code=" + $scope.code + "&academic_year=" + oldval).then(function (Getsims550_TermsAcademicYear) {
                    $scope.TermsAcademicYear1 = Getsims550_TermsAcademicYear.data;
                    $scope.fcommn['sims_term_code'] = $scope.TermsAcademicYear1[0].sims_term_code;
                    $scope.fcommn['sims_comment_code'] = $scope.get_Comment_type[0].sims_comment_code;

                    $scope.GradeSectionSubject2();

                });
            }

            $scope.getsection2 = function (str, str1, str2) {
                $http.get(ENV.apiUrl + "api/Gradebook/GetRCSection?curcode=" + str + "&ayear=" + str1 + "&gradecode=" + str2 + "&teachercode=" + user).then(function (Allsection) {
                    $scope.getSectionFromGrade1 = Allsection.data;
                    $scope.fcommn['sims_section_code'] = $scope.getSectionFromGrade1[0].sims_section_code;
                    $scope.Getterm2(str, str1);
                    $scope.commentchange('S');
                })
            };

            $scope.getsections2 = function (str, str1) {

                $http.get(ENV.apiUrl + "api/Gradebook/GetRCGrade?curcode=" + str + "&ayear=" + str1 + "&teachercode=" + user).then(function (res) {
                    $scope.getAllGrades1 = res.data;
                    $scope.fcommn['sims_grade_code'] = $scope.getAllGrades1[0].sims_grade_code;
                    $scope.getsection2(str, str1, $scope.getAllGrades1[0].sims_grade_code);
                })
            };

            $scope.GradeSectionSubject2 = function () {

                $scope.subject_names = [];

                $scope.commentcountlabel = 0;
                $scope.headercountlabel = 0;
                $scope.subject_names1 = [];

                $scope.SectionSubject5 = [];

                var report1 = {
                    sims_grade_code: $scope.fcommn.sims_grade_code,
                    sims_academic_year: $scope.fcommn.academic_year,
                    sims_section_code: $scope.fcommn.sims_section_code,
                    sims_employee_code: user
                };


                $http.post(ENV.apiUrl + "api/Gradebook/AllGradeSection_subject", report1).then(function (allSection_Subject) {
                    $scope.subject_namesforcomment = allSection_Subject.data;
                    if ($scope.fcommn.sims_comment_code == 'S') {
                        $scope.subject_type = false;
                        if ($http.defaults.headers.common['schoolId'] == 'oes') {
                            $scope.getCommentsheaderforoes();
                        }
                        else {
                            $scope.getCommentsheader();
                        }
                    }
                    else {
                        $scope.subject_type = true;
                    }
                 });

                
                var report = {
                    SubjectCurr: $scope.fcommn.cur_code,
                    SubjectGrade: $scope.fcommn.sims_grade_code,
                    SubjectAcademic: $scope.fcommn.academic_year,
                    SubjectSection: $scope.fcommn.sims_section_code,
                    SubjectTerm: $scope.fcommn.sims_term_code,
                    SubTeacherEmpId: user
                };

                $http.post(ENV.apiUrl + "api/Gradebook/ReportCardlevels?data=" + JSON.stringify(report)).then(function (Getlevels) {
                    $scope.Get_levels = Getlevels.data;
                    $scope.fcommn.level_code = $scope.Get_levels[0].level_code;
                    $scope.getRCreportcardname();

                    if ($http.defaults.headers.common['schoolId'] == 'oes') {
                        $scope.getCommentsheaderforoes();
                    }
                    else {
                        $scope.getCommentsheader();
                    }

                });

            }

            $scope.getRCreportcardname = function () {
                $http.get(ENV.apiUrl + "api/Gradebook/GetRCReportCardNames?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code + "&levelCode=" + $scope.fcommn.level_code + "&term=" + $scope.fcommn.sims_term_code).then(function (GetRCReportCardNames) {
                    $scope.Report_Cards = GetRCReportCardNames.data
                    $scope.fcommn['report_Card_Code'] = $scope.Report_Cards[0].sims_report_card;
                });
            }

            $scope.commenttypechange = function () {

                if ($scope.fcommn.sims_comment_code == 'S') {
                    $scope.subject_type = false;

                }
                else {
                    $scope.subject_type = true;
                    $scope.fcommn['sims_subject_code'] = '';

                }

                $scope.GradeSectionSubject2();
            }

            $scope.SelectReportAndLevel = function () {
                $scope.getCommentsheader();
            }

            $scope.getCommentsheader = function () {

                var report1 = {
                    sims_grade_code: $scope.fcommn.sims_grade_code,
                    sims_academic_year: $scope.fcommn.academic_year,
                    sims_section_code: $scope.fcommn.sims_section_code,
                    sims_employee_code: user
                };

                if ($scope.fcommn.sims_comment_code == 'S') {
                    $scope.subject_type = false;
                    $scope.fcommn['sims_subject_code'] = (($scope.fcommn.sims_subject_code == undefined || $scope.fcommn.sims_subject_code == '') && $scope.subject_namesforcomment.length>0) ? $scope.subject_namesforcomment[0].sims_subject_code : $scope.fcommn.sims_subject_code;
                }
                else {
                    $scope.subject_type = true;
                    $scope.fcommn['sims_subject_code'] = '';
                }

                $http.get(ENV.apiUrl + "api/Gradebook/GetRCStudentForCommentsheaderNew?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code + "&term_code=" + $scope.fcommn.sims_term_code + "&comment_type=" + $scope.fcommn.sims_comment_code + "&subject_code=" + $scope.fcommn.sims_subject_code).then(function (Commentsheader1) {
                    $scope.Comments_header1 = Commentsheader1.data;
                    $scope.fcommn.sims_student_comment_header_id = $scope.Comments_header1[0].sims_comment_code;
                });
            }

            $scope.getCommentsheaderforoes = function () {


                $http.get(ENV.apiUrl + "api/Gradebook/GetRCStudentForCommentsheaderforoes?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code + "&term_code=" + $scope.fcommn.sims_term_code + "&comment_type=H" + "&subject_code=" + $scope.fcommn.sims_subject_code).then(function (Commentsheader1) {
                    $scope.Comments_header = Commentsheader1.data
                    $scope.fcommn.sims_student_comment_header_id = $scope.Comments_header[0].sims_comment_code;

                });
            }

            $scope.GetAllStudentForfinalComment = function () {

                var data = {
                    sims_admission_cur_code: $scope.fcommn.cur_code
                , sims_academic_year: $scope.fcommn.academic_year
                , sims_grade_code: $scope.fcommn.sims_grade_code
                , sims_section_code: $scope.fcommn.sims_section_code
                , sims_subject_code: $scope.fcommn.sims_subject_code
                , sims_term_code: $scope.fcommn.sims_term_code
                , sims_report_card_level: $scope.fcommn.level_code
                , sims_report_card: $scope.fcommn.report_Card_Code

                }
                $http.post(ENV.apiUrl + "api/Gradebook/RCCommentsforStudent", data).then(function (GetRCCommentsforStudent) {
                    $scope.GetRCCommentsforStudent = GetRCCommentsforStudent.data;


                    $http.get(ENV.apiUrl + "api/Gradebook/GetRCStudentForComments?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code + "&term_code=" + $scope.fcommn.sims_term_code + "&sub_code=" + $scope.fcommn.sims_subject_code).then(function (GetRCStudentForComments) {
                        $scope.GetRCStudentForComments = GetRCStudentForComments.data;
                        
                        if ($http.defaults.headers.common['schoolId'] == 'oes') {
                            $scope.getCommentsheaderforoes();
                        }
                        else {
                            $scope.getCommentsheader();
                        }
                    });
                });
            }

            $scope.GetAllStudentForfinalComment1 = function () {

                $scope.headercountlabel = '0';
                $scope.commentcountlabel = '0';

                var data = {
                    sims_admission_cur_code: $scope.fcommn.cur_code
                , sims_academic_year: $scope.fcommn.academic_year
                , sims_grade_code: $scope.fcommn.sims_grade_code
                , sims_section_code: $scope.fcommn.sims_section_code
                , sims_subject_code: $scope.fcommn.sims_subject_code
                , sims_term_code: $scope.fcommn.sims_term_code,

                    sims_student_comment_id: $scope.fcommn.sims_student_comment_header_id

                }
                $http.post(ENV.apiUrl + "api/Gradebook/RCStudentForCommentspearl", data).then(function (GetRCCommentsforStudent) {
                    $scope.commentstudent = GetRCCommentsforStudent.data;

                    if ($http.defaults.headers.common['schoolId'] == 'oes') {
                        $http.post(ENV.apiUrl + "api/Gradebook/RCCommentsforStudentoes", data).then(function (GetRCCommentsforStudent) {
                            $scope.RCCommentsforStudentoes = GetRCCommentsforStudent.data;

                            for (var i = 0; i < $scope.RCCommentsforStudentoes.length; i++) {
                                $scope.RCCommentsforStudentoes[i].ischecked = false;
                            }

                            $http.get(ENV.apiUrl + "api/Gradebook/GetRCStudentForComments?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code + "&term_code=" + $scope.fcommn.sims_term_code + "&sub_code=" + $scope.fcommn.sims_subject_code).then(function (GetRCStudentForComments) {
                                $scope.GetRCStudentForComments = GetRCStudentForComments.data;
                                console.log('$scope.GetRCStudentForComments = ', $scope.GetRCStudentForComments);
                            });


                        });
                    }

                });
            }

            $scope.GetAllStudentForfinalCommentstudent = function (info, str) {
                
                $scope.CommentsStudent = [];

                var data = {};
                str.sims_student_check_status = true;
                data.sims_admission_cur_code = $scope.fcommn.cur_code
                data.sims_academic_year = $scope.fcommn.academic_year
                data.sims_report_card_level = $scope.fcommn.level_code
                data.sims_report_card = $scope.fcommn.report_Card_Code
                data.sims_grade_code = $scope.fcommn.sims_grade_code
                data.sims_section_code = $scope.fcommn.sims_section_code
                data.sims_subject_code = $scope.fcommn.sims_subject_code
                data.sims_enroll_number = str.sims_enroll_number
                data.sims_term_code = $scope.fcommn.sims_term_code

                $http.post(ENV.apiUrl + "api/Gradebook/CommentsStudent", data).then(function (CommentsStudent2) {
                    $scope.CommentsStudent = CommentsStudent2.data;
                   
                    $scope.commentcountlabel = 0;
                    $scope.count5 = 0;
                    $scope.headcount = 0;

                    $scope.catcolor = '';
                    $scope.cur_gb_name = '';
                    $scope.fcommn.sims_comment = 'Enter Characters';
                    $scope.commentforcopy1 = [];

                    for (var j = 0; j < $scope.CommentsStudent.length; j++) {
                        if ($scope.CommentsStudent[j].sims_comment_code != '') {
                            if ($scope.CommentsStudent[j].sims_comment_type == 'C') {
                                $scope.count5 = parseInt($scope.count5) + 1;
                            }
                            else {
                                $scope.headcount = parseInt($scope.headcount) + 1;
                            }
                        }
                    }

                    $scope.Expand = '';
                    for (var i = 0; i < $scope.GetRCCommentsforStudent.length; i++) {
                        for (var k = 0; k < $scope.GetRCCommentsforStudent[i].coments_lst.length; k++) {
                            $scope.GetRCCommentsforStudent[i].coments_lst[k].sims_comment_status1 = false;
                            for (var j = 0; j < $scope.CommentsStudent.length; j++) {
                                if ($scope.GetRCCommentsforStudent[i].coments_lst[k].sims_comment_code == $scope.CommentsStudent[j].sims_comment_code) {
                                    $scope.GetRCCommentsforStudent[i].coments_lst[k].sims_comment_status1 = true;
                                    $scope.cur_gb_name = $scope.GetRCCommentsforStudent[i].sims_student_comment_header_id;

                                    $scope.Expand = $scope.GetRCCommentsforStudent[i].sims_student_comment_header_id;
                                    if ($scope.commentforcopy1.length == 0) {
                                        $scope.commentforcopy1.push($scope.GetRCCommentsforStudent[i]);
                                    }
                                    else {
                                        for (var m = 0; m < $scope.commentforcopy1.length; m++) {
                                            if ($scope.commentforcopy1[m].sims_student_comment_header_id != $scope.GetRCCommentsforStudent[i].sims_student_comment_header_id) {
                                                $scope.commentforcopy1.push($scope.GetRCCommentsforStudent[i]);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $scope.commentcountlabel = $scope.count5;
                    $scope.headercountlabel = $scope.headcount;

                    $scope.GetRCStudentForComments1 = [];
                    for (var k = 0; k < $scope.GetRCStudentForComments.length; k++) {
                        if ($scope.GetRCStudentForComments[k].sims_enroll_number != str.sims_enroll_number) {
                            $scope.GetRCStudentForComments[k].sims_student_check_status = false;
                            $scope.GetRCStudentForComments1.push($scope.GetRCStudentForComments[k]);
                        }
                    }
                    $scope.copystudent_name = str.sims_student_Name;
                    $scope.copystudent_enroll_number = str.sims_enroll_number

                    var v = document.getElementById('copycommentcheck');
                    v.checked = false;
                });
            }

            $scope.GetAllStudentForfinalCommentstudent1 = function (info, str) {

                $scope.fcommn['sims_comment'] = '';
                $scope.fcommn['sims_comment_code1'] = '';
                $scope.CommentsStudent = [];


                for (var i = 0; i < $scope.commentstudent.length; i++) {
                    if ($scope.commentstudent[i].sims_enroll_number == str.sims_enroll_number) {
                        $scope.commentstudent[i].sims_student_check_status = true;
                    }
                    else {

                        $scope.commentstudent[i].sims_student_check_status = false;
                    }
                }

                var data = {};
                str.sims_student_check_status = true;
                data.sims_admission_cur_code = $scope.fcommn.cur_code
                data.sims_academic_year = $scope.fcommn.academic_year

                data.sims_grade_code = $scope.fcommn.sims_grade_code
                data.sims_section_code = $scope.fcommn.sims_section_code
                data.sims_subject_code = $scope.fcommn.sims_subject_code
                data.sims_enroll_number = str.sims_enroll_number
                data.sims_term_code = $scope.fcommn.sims_term_code;
                data.sims_student_comment_header_id = ($scope.fcommn.sims_student_comment_header_id == undefined ? '' : $scope.fcommn.sims_student_comment_header_id);


                $http.post(ENV.apiUrl + "api/Gradebook/CommentsStudent1", data).then(function (CommentsStudent) {
                    $scope.CommentsStudent = CommentsStudent.data;

                    $scope.fcommn['sims_comment'] = $scope.CommentsStudent[0].sims_comment;
                    $scope.fcommn['sims_comment_code1'] = $scope.CommentsStudent[0].sims_comment_code;




                })
            }

            $scope.GetAllStudentForfinalCommentstudentoes = function (info, str) {

                $scope.CommentsStudentoes = [];


                for (var i = 0; i < $scope.GetRCStudentForComments.length; i++) {
                    if ($scope.GetRCStudentForComments[i].sims_enroll_number == str.sims_enroll_number) {
                        $scope.GetRCStudentForComments[i].sims_student_check_status = true;
                    }
                    else {

                        $scope.GetRCStudentForComments[i].sims_student_check_status = false;
                    }
                }

                var data = {};
                str.sims_student_check_status = true;
                data.sims_admission_cur_code = $scope.fcommn.cur_code
                data.sims_academic_year = $scope.fcommn.academic_year

                data.sims_grade_code = $scope.fcommn.sims_grade_code
                data.sims_section_code = $scope.fcommn.sims_section_code
                data.sims_subject_code = $scope.fcommn.sims_subject_code
                data.sims_enroll_number = str.sims_enroll_number
                data.sims_term_code = $scope.fcommn.sims_term_code;
                data.sims_student_comment_header_id = $scope.fcommn.sims_student_comment_header_id;


                $http.post(ENV.apiUrl + "api/Gradebook/CommentsStudent1new", data).then(function (CommentsStudent) {

                    $scope.headercountlabel = '0';
                    $scope.commentcountlabel = '0';

                    $scope.CommentsStudentoes = CommentsStudent.data;
                    for (var j = 0; j < $scope.RCCommentsforStudentoes.length; j++) {
                        $scope.RCCommentsforStudentoes[j].ischecked = false

                        for (var i = 0; i < $scope.CommentsStudentoes.length; i++) {
                            if ($scope.CommentsStudentoes[i].sims_comment_code == $scope.RCCommentsforStudentoes[j].sims_comment_code) {
                                $scope.RCCommentsforStudentoes[j].ischecked = true;
                                $scope.headercountlabel = '1';
                                $scope.cur_gb_name = true;
                            }
                        }
                    }

                    $scope.commentcountlabel = $scope.CommentsStudentoes.length;
                });
            }

            $scope.insertcopycommenttostudent = function () {
                var commentdataobject = [];
                for (var i = 0; i < $scope.GetRCStudentForCommentsforcopycomment.length; i++) {
                    if ($scope.GetRCStudentForCommentsforcopycomment[i].ischecked == true) {
                        for (var j = 0; j < $scope.commentforcopy.length; j++) { //header list
                            for (var k = 0; k < $scope.commentforcopy[j].coments_lst.length; k++) { //child comments
                                if ($scope.commentforcopy[j].coments_lst[k].sims_comment_status1 == true) {
                                    var data = {}
                                    data.sims_admission_cur_code = $scope.fcommn.cur_code;
                                    data.sims_academic_year = $scope.fcommn.academic_year;
                                    data.sims_grade_code = $scope.fcommn.sims_grade_code;
                                    data.sims_section_code = $scope.fcommn.sims_section_code;
                                    data.sims_term_code = $scope.fcommn.sims_term_code;
                                    data.sims_report_card_level = $scope.fcommn.level_code
                                    data.sims_report_card = $scope.fcommn.report_Card_Code

                                    if ($scope.fcommn.sims_comment_code != 'F') {
                                        data.sims_subject_code = $scope.fcommn.sims_subject_code;

                                    }

                                    data.sims_comment_type = $scope.fcommn.sims_comment_code;
                                    data.sims_enroll_number = $scope.GetRCStudentForCommentsforcopycomment[i].sims_enroll_number;
                                    data.sims_comment_header_desc = $scope.commentforcopy[j].sims_comment_header_desc;
                                    data.sims_student_comment_header_id = $scope.commentforcopy[j].sims_student_comment_header_id;
                                    data.sims_comment_code = $scope.commentforcopy[j].coments_lst[k].sims_comment_code;
                                    data.sims_comment_status = $scope.commentforcopy[j].coments_lst[k].sims_comment_status1;

                                    if ($scope.sims_comment_codeupdate == $scope.commentforcopy[j].coments_lst[k].sims_comment_code && $scope.commentforcopy[j].coments_lst[k].ischange1 == true) {
                                        $scope.sims_comment_codeupdate = '';
                                        $scope.commentforcopy[j].coments_lst[k].ischange1 = false;
                                        $scope.commentforcopy[j].coments_lst[k].sims_comment = $scope.fcommn.sims_comment;
                                    }

                                    data.sims_comment = $scope.commentforcopy[j].coments_lst[k].sims_comment
                                    data.sims_user_code = user;
                                    data.sims_comment_type1 = 'C'
                                    commentdataobject.push(data);
                                }
                            }
                        }
                    }
                }

                $http.post(ENV.apiUrl + "api/Gradebook/RCInsertHeaderforStudent", commentdataobject).then(function (RCInsertCommentforStudent) {
                    $scope.RCInsertCommentforStudent = RCInsertCommentforStudent.data;
                    if ($scope.RCInsertCommentforStudent == true) {
                        $scope.fcommn.sims_comment = '';
                        swal({ text: 'Student Comment Inserted Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                        $scope.newComment = false;
                        $scope.newheader = false;
                        $scope.finalGradebookComment = true;
                        $scope.maincomment = true;
                        $scope.copycomment = false;
                        $scope.GetAllStudentForfinalComment();
                    }
                    else {
                        swal({ text: 'Student Comment Not Inserted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    };
                });
            }

            $scope.InsertfinalCommentForStudent = function (isvalid) {

                if ($scope.fcommn.sims_comment_code == 'S' && ($scope.fcommn.sims_subject_code == '' || $scope.fcommn.sims_subject_code == undefined || $scope.fcommn.sims_subject_code == null)) {
                    $scope.checksubject = false;

                } else {
                    $scope.checksubject = true;
                }

                if ($scope.checksubject == true) {

                    if ($scope.newheader == true && $scope.newComment == false) {

                        var data = {};
                        $scope.newheader = false;
                        for (var i = 0; i < $scope.GetRCStudentForComments.length; i++) {
                            if ($scope.GetRCStudentForComments[i].sims_student_check_status == true) {

                                data.sims_admission_cur_code = $scope.fcommn.cur_code
                                data.sims_academic_year = $scope.fcommn.academic_year
                                data.sims_grade_code = $scope.fcommn.sims_grade_code
                                data.sims_section_code = $scope.fcommn.sims_section_code
                                data.sims_term_code = $scope.fcommn.sims_term_code
                                data.sims_report_card_level = $scope.fcommn.level_code
                                data.sims_report_card = $scope.fcommn.report_Card_Code
                                data.sims_comment_type = $scope.fcommn.sims_comment_code
                                data.sims_enroll_number = $scope.GetRCStudentForComments[i].sims_enroll_number
                                data.sims_comment_header_desc = $scope.fcommn.sims_comment
                                data.sims_student_comment_header_id = null
                                data.sims_comment_type1 = 'H'

                                if ($scope.fcommn.sims_comment_code != 'F') {
                                    data.sims_subject_code = $scope.fcommn.sims_subject_code;

                                }
                            }

                        }
                        $http.post(ENV.apiUrl + "api/Gradebook/RCNewInsertCommentforStudent", data).then(function (RCNewInsertCommentforStudent) {
                            $scope.RCNewInsertCommentforStudent = RCNewInsertCommentforStudent.data;
                            if ($scope.RCNewInsertCommentforStudent == true) {
                                swal({ text: 'Header Inserted Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                                $scope.fcommn.sims_comment = '';
                                $scope.newComment = false;
                                $scope.newheader = false;
                                $scope.GetAllStudentForfinalComment();
                            }
                            else {
                                swal({ text: 'Header Not Inserted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                            }

                        });

                    }
                    else if ($scope.newComment == true && $scope.newheader == false) {
                        $scope.newComment = false;

                        $scope.data = {};

                        for (var j = 0; j < $scope.GetRCCommentsforStudent.length; j++) {
                            if ($scope.GetRCCommentsforStudent[j].ischecked == true) {
                                for (var i = 0; i < $scope.GetRCStudentForComments.length; i++) {
                                    if ($scope.GetRCStudentForComments[i].sims_student_check_status == true) {
                                        $scope.data = {};
                                        $scope.data.sims_admission_cur_code = $scope.fcommn.cur_code;
                                        $scope.data.sims_academic_year = $scope.fcommn.academic_year;
                                        $scope.data.sims_grade_code = $scope.fcommn.sims_grade_code;
                                        $scope.data.sims_section_code = $scope.fcommn.sims_section_code;
                                        $scope.data.sims_term_code = $scope.fcommn.sims_term_code;
                                        $scope.data.sims_report_card_level = $scope.fcommn.level_code
                                        $scope.data.sims_report_card = $scope.fcommn.report_Card_Code

                                        if ($scope.fcommn.sims_comment_code != 'F') {
                                            $scope.data.sims_subject_code = $scope.fcommn.sims_subject_code;

                                        }
                                        $scope.data.sims_comment_type = $scope.fcommn.sims_comment_code;
                                        $scope.data.sims_enroll_number = $scope.GetRCStudentForComments[i].sims_enroll_number;
                                        $scope.data.sims_comment_header_desc = $scope.fcommn.sims_comment;
                                        $scope.data.sims_student_comment_header_id = $scope.GetRCCommentsforStudent[j].sims_student_comment_header_id;
                                        $scope.data.sims_comment_type1 = 'C';

                                    }
                                }

                            }
                        }

                        if ($scope.data.sims_student_comment_header_id != undefined && $scope.data.sims_student_comment_header_id != '') {
                            $scope.newComment = false;
                            $scope.newheader = false;
                            $http.post(ENV.apiUrl + "api/Gradebook/RCNewInsertCommentforStudent", $scope.data).then(function (RCNewInsertCommentforStudent) {
                                $scope.RCNewInsertCommentforStudent = RCNewInsertCommentforStudent.data;
                                if ($scope.RCNewInsertCommentforStudent == true) {
                                    swal({ text: 'Comment Inserted Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });

                                    $scope.fcommn.sims_comment = '';
                                    $scope.GetAllStudentForfinalComment();
                                }
                                else {
                                    swal({ text: 'Comment Not Inserted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                                }

                            });
                        }
                        else {
                            swal({ text: 'Select Atleast One Header To Insert Comment', imageUrl: "assets/img/notification-alert.png", width: 320, showCloseButton: true });
                        }
                    }
                    else {


                        $scope.newComment = false;
                        $scope.newheader = false;
                        var commentdataobject = [];
                        for (var i = 0; i < $scope.GetRCStudentForComments.length; i++) {
                            if ($scope.GetRCStudentForComments[i].sims_student_check_status == true) {
                                for (var j = 0; j < $scope.GetRCCommentsforStudent.length; j++) {
                                    for (var k = 0; k < $scope.GetRCCommentsforStudent[j].coments_lst.length; k++) {
                                        if ($scope.GetRCCommentsforStudent[j].coments_lst[k].ischange1 == true) {
                                            var data = {}
                                            data.sims_admission_cur_code = $scope.fcommn.cur_code;
                                            data.sims_academic_year = $scope.fcommn.academic_year;
                                            data.sims_grade_code = $scope.fcommn.sims_grade_code;
                                            data.sims_section_code = $scope.fcommn.sims_section_code;
                                            data.sims_term_code = $scope.fcommn.sims_term_code;
                                            data.sims_report_card_level = $scope.fcommn.level_code
                                            data.sims_report_card = $scope.fcommn.report_Card_Code

                                            if ($scope.fcommn.sims_comment_code != 'F') {
                                                data.sims_subject_code = $scope.fcommn.sims_subject_code;

                                            }

                                            data.sims_comment_type = $scope.fcommn.sims_comment_code;
                                            data.sims_enroll_number = $scope.GetRCStudentForComments[i].sims_enroll_number;
                                            data.sims_comment_header_desc = $scope.GetRCCommentsforStudent[j].sims_comment_header_desc;
                                            data.sims_student_comment_header_id = $scope.GetRCCommentsforStudent[j].sims_student_comment_header_id;
                                            data.sims_comment_code = $scope.GetRCCommentsforStudent[j].coments_lst[k].sims_comment_code;
                                            data.sims_comment_status = $scope.GetRCCommentsforStudent[j].coments_lst[k].sims_comment_status1;

                                            if ($scope.sims_comment_codeupdate == $scope.GetRCCommentsforStudent[j].coments_lst[k].sims_comment_code && $scope.GetRCCommentsforStudent[j].coments_lst[k].ischange1 == true) {
                                                $scope.sims_comment_codeupdate = '';
                                                $scope.GetRCCommentsforStudent[j].coments_lst[k].ischange1 = false;
                                                $scope.GetRCCommentsforStudent[j].coments_lst[k].sims_comment = $scope.fcommn.sims_comment;
                                            }

                                            data.sims_comment = $scope.GetRCCommentsforStudent[j].coments_lst[k].sims_comment

                                            data.sims_user_code = user;
                                            data.sims_comment_type1 = 'C'
                                            commentdataobject.push(data);
                                        }
                                    }
                                }
                            }
                        }

                        $http.post(ENV.apiUrl + "api/Gradebook/RCInsertHeaderforStudent", commentdataobject).then(function (RCInsertCommentforStudent) {
                            $scope.RCInsertCommentforStudent = RCInsertCommentforStudent.data;
                            if ($scope.RCInsertCommentforStudent == true) {
                                $scope.fcommn.sims_comment = '';
                                swal({ text: 'Student Comment Inserted Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                                $scope.newComment = false;
                                $scope.newheader = false;
                                $scope.GetAllStudentForfinalComment();
                            }
                            else {
                                swal({ text: 'Student Comment Not Inserted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                            };
                        });

                    }
                }
                else {
                    swal({ text: "Select Subject", width: 250, heigth: 200, showCloseButton: true });
                }
            }

            $scope.InsertfinalCommentForStudentoes = function () {

                if ($scope.fcommn.sims_comment_code == 'S' && ($scope.fcommn.sims_subject_code == '' || $scope.fcommn.sims_subject_code == undefined || $scope.fcommn.sims_subject_code == null)) {
                    $scope.checksubject = false;

                } else {
                    $scope.checksubject = true;
                }

                if ($scope.checksubject == true) {

                    //for (var i = 0; i < $scope.GetRCStudentForComments.length; i++) {
                    //    $scope.GetRCStudentForComments[i].sims_student_check_status = false;
                    //}


                    var commentdataobject = [];
                    var data = {};

                    data.sims_admission_cur_code = $scope.fcommn.cur_code;
                    data.sims_academic_year = $scope.fcommn.academic_year;
                    data.sims_grade_code = $scope.fcommn.sims_grade_code;
                    data.sims_section_code = $scope.fcommn.sims_section_code;
                    data.sims_term_code = $scope.fcommn.sims_term_code;
                    data.sims_subject_code = $scope.fcommn.sims_subject_code;

                    data.sims_student_comment_header_id = $scope.fcommn.sims_student_comment_header_id;
                    data.sims_user_code = user;
                    data.sims_student_comment_id1 = '';
                    data.sims_student_comment_id = '';

                    for (var i = 0; i < $scope.GetRCStudentForComments.length; i++) {
                        if ($scope.GetRCStudentForComments[i].sims_student_check_status == true) {
                            data.sims_enroll_number = $scope.GetRCStudentForComments[i].sims_enroll_number;
                            for (var j = 0; j < $scope.RCCommentsforStudentoes.length; j++) {
                                if ($scope.RCCommentsforStudentoes[j].ischecked) {

                                    data.sims_student_comment_id = data.sims_student_comment_id + $scope.RCCommentsforStudentoes[j].sims_comment_code + '/';
                                }

                                data.sims_student_comment_id1 = data.sims_student_comment_id1 + $scope.RCCommentsforStudentoes[j].sims_comment_code + '/';

                            }
                            $scope.GetRCStudentForComments[i].sims_student_check_status = false;
                        }
                    }

                    $http.post(ENV.apiUrl + "api/Gradebook/RCInsertCommentforStudentoes", data).then(function (RCInsertCommentforStudent) {
                        $scope.RCInsertCommentforStudent = RCInsertCommentforStudent.data;
                        $scope.headercountlabel = '0';
                        $scope.commentcountlabel = '0';

                        if ($scope.RCInsertCommentforStudent == true) {
                            swal({ text: 'Student Comment Updated Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                            $scope.GetAllStudentForfinalComment1();
                        }
                        else {
                            swal({ text: 'Student Comment Not Updated', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                        };
                    });

                }
                else {
                    swal({ text: "Select Subject", width: 250, height: 200, showCloseButton: true });
                }
            }

            $scope.Click_on_comment_data = function (info) {



                $scope.cur_gb_name = info.sims_student_comment_header_id
                $scope.catcolor = info.sims_student_comment_header_id + info.sims_comment_code;
                $scope.sims_comment_codeupdate = info.sims_comment_code;
                info.sims_comment_status1 = true;
                $scope.fcommn.sims_comment = info.sims_comment;
                info.ischange1 = true;

                if (info.sims_comment == undefined || info.sims_comment == '') {
                    $scope.fcommn.sims_comment = 'Enter Characters';
                }
            }

            $scope.Click_on_comment_data1 = function (info) {
                $scope.Expand = info.sims_student_comment_header_id;
                $scope.cur_gb_name = info.sims_student_comment_header_id;
                $scope.fcommn.sims_comment = info.sims_comment_header_desc;
                for (var i = 0; i < $scope.GetRCCommentsforStudent.length; i++) {
                    if (info.sims_student_comment_header_id == $scope.GetRCCommentsforStudent[i].sims_student_comment_header_id) {
                        info.ischecked = true;
                    }
                    else {
                        $scope.GetRCCommentsforStudent[i].ischecked = false;
                    }
                }
            }

            $scope.click_on_new_header = function () {
                $scope.newheader = true;
                $scope.newComment = false;

                $scope.fcommn.sims_comment = '';

            }

            $scope.Click_OnCommentheaderche_Ckbox = function (info) {

                if (info.ischecked == true) {
                    for (var j = 0; j < info.coments_lst.length; j++) {
                        info.coments_lst[j].sims_comment_status1 = true;
                        info.coments_lst[j].ischange = true;
                        $scope.catcolor = info.sims_student_comment_header_id + info.coments_lst[j].sims_comment_code
                        $scope.cur_gb_name = info.sims_student_comment_header_id;
                        $scope.fcommn.sims_comment = info.sims_comment_header_desc;
                    }
                }
                else {
                    for (var j = 0; j < info.coments_lst.length; j++) {
                        info.coments_lst[j].ischange = true;
                        info.coments_lst[j].sims_comment_status1 = false;
                        $scope.catcolor = '';
                        $scope.cur_gb_name = '';
                    }
                }


            }

            $scope.Click_on_subCommentCheckBox = function (info, info1) {
                var count1 = 0;
                var count = info.coments_lst.length;

                info1.ischange1 = true;
                for (var i = 0; i < info.coments_lst.length; i++) {
                    if (info.coments_lst[i].sims_comment_status1 == true) {
                        $scope.fcommn.sims_comment = info.coments_lst[i].sims_comment;

                        count1 = count1 + 1;
                        if (count == count1) {
                            info.ischecked = true;
                        }
                        else {

                            info.ischecked = false;
                        }
                    }
                    else {

                    }
                }
                if (info1.sims_comment == undefined || info1.sims_comment == '') {
                    $scope.fcommn.sims_comment = 'Enter Characters';
                }

            }

            $scope.ResetFcomment = function () {
                $scope.GetRCStudentForComments = [];
                $scope.GetRCCommentsforStudent = [];
                $scope.fcommn = {};

                $scope.fcommn['cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.fcommn['academic_year'] = $scope.getAcademicYear1[0].sims_academic_year;
            }

            $scope.click_on_new_Comment = function () {
                $scope.newComment = true;
                $scope.newheader = false;
                $scope.fcommn.sims_comment = '';
            }

            $scope.Click_on_CopyComments = function () {

                $scope.commentforcopy = [];
                $scope.GetRCStudentForCommentsforcopycomment = [];

                $scope.commentforcopy = angular.copy($scope.commentforcopy1);

                $scope.GetRCStudentForCommentsforcopycomment = angular.copy($scope.GetRCStudentForComments1);
                $scope.maincomment = false;
                $scope.copycomment = true;
            }

            $scope.BackFcomment = function () {

                $scope.maincomment = true;
                $scope.copycomment = false;
            }

            $scope.SelectAllStudentforcomment = function () {

                var v = document.getElementById('copycommentcheck');
                for (var i = 0; i < $scope.GetRCStudentForCommentsforcopycomment.length; i++) {
                    if (v.checked == true) {
                        $scope.GetRCStudentForCommentsforcopycomment[i].ischecked = true;
                    }
                    else {
                        $scope.GetRCStudentForCommentsforcopycomment[i].ischecked = false;
                    }
                }
            }

            $scope.Click_Submit_Copy_Comment = function () {

                var commentdataobject = [];

                for (var i = 0; i < $scope.GetRCStudentForCommentsforcopycomment.length; i++) {
                    if ($scope.GetRCStudentForCommentsforcopycomment[i].ischecked == true) {
                        for (var j = 0; j < $scope.commentforcopy.length; j++) {
                            for (var k = 0; k < $scope.commentforcopy[j].coments_lst.length; k++) {
                                if ($scope.commentforcopy[j].coments_lst[k].sims_comment_status1 == true) {

                                    var data = {};
                                    data.sims_admission_cur_code = $scope.fcommn.cur_code;
                                    data.sims_academic_year = $scope.fcommn.academic_year;
                                    data.sims_grade_code = $scope.fcommn.sims_grade_code;
                                    data.sims_section_code = $scope.fcommn.sims_section_code;
                                    data.sims_term_code = $scope.fcommn.sims_term_code;
                                    data.sims_subject_code = $scope.fcommn.sims_subject_code;

                                    data.sims_comment_type = $scope.fcommn.sims_comment_code;
                                    data.sims_enroll_number = $scope.GetRCStudentForCommentsforcopycomment[i].sims_enroll_number;
                                    data.sims_comment_header_desc = $scope.GetRCCommentsforStudent[j].sims_comment_header_desc;
                                    data.sims_student_comment_header_id = $scope.commentforcopy[j].sims_student_comment_header_id;
                                    data.sims_comment_code = $scope.commentforcopy[j].coments_lst[k].sims_comment_code;
                                    data.sims_comment_status = $scope.commentforcopy[j].coments_lst[k].sims_comment_status;
                                    data.sims_comment = $scope.commentforcopy[j].coments_lst[k].sims_comment
                                    data.sims_user_code = user;

                                    commentdataobject.push(data);
                                }
                            }
                        }
                    }
                }

                if (commentdataobject.length > 0) {
                    $http.post(ENV.apiUrl + "api/Gradebook/RCInsertHeaderforStudent", commentdataobject).then(function (RCInsertCommentforStudent) {
                        $scope.RCInsertCommentforStudent = RCInsertCommentforStudent.data;
                        if ($scope.RCInsertCommentforStudent == true) {
                            swal({ text: 'Student Comment Inserted Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                            $scope.copycomment = false;
                            $scope.maincomment = true;
                            $scope.GradeSectionSubject2();

                        }
                        else {
                            swal({ text: 'Student Comment Not Inserted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                        };
                    });
                }
                else {

                    swal({ text: 'Select Atleast One Student And One Comment', imageUrl: "assets/img/notification-alert.png", width: 320, showCloseButton: true });
                }

            }

            $scope.DeletestudentComment = function () {

                var data = {};

                var commentdeletedataobject = [];

                for (var i = 0; i < $scope.GetRCStudentForComments.length; i++) {
                    if ($scope.GetRCStudentForComments[i].sims_student_check_status == true)
                        for (var j = 0; j < $scope.GetRCCommentsforStudent.length; j++) {
                            for (var k = 0; k < $scope.GetRCCommentsforStudent[j].coments_lst.length; k++) {
                                if ($scope.GetRCCommentsforStudent[j].coments_lst[k].sims_comment_status1 == false) {

                                    data = {};
                                    data.sims_admission_cur_code = $scope.fcommn.cur_code;
                                    data.sims_academic_year = $scope.fcommn.academic_year;
                                    data.sims_grade_code = $scope.fcommn.sims_grade_code;
                                    data.sims_section_code = $scope.fcommn.sims_section_code;
                                    data.sims_term_code = $scope.fcommn.sims_term_code;
                                    data.sims_subject_code = $scope.fcommn.sims_subject_code;

                                    data.sims_comment_type = 'C'
                                    data.sims_enroll_number = $scope.GetRCStudentForComments[i].sims_enroll_number;
                                    data.sims_comment_header_desc = $scope.GetRCCommentsforStudent[j].sims_comment_header_desc;
                                    data.sims_student_comment_header_id = $scope.GetRCCommentsforStudent[j].sims_student_comment_header_id;
                                    data.sims_student_comment_id = $scope.GetRCCommentsforStudent[j].coments_lst[k].sims_comment_code;
                                    data.sims_root_comment_id = $scope.GetRCCommentsforStudent[j].sims_student_comment_header_id;

                                    commentdeletedataobject.push(data);
                                }
                            }

                        }
                }
                if (commentdeletedataobject.length > 0) {
                    $http.post(ENV.apiUrl + "api/Gradebook/RCDeleteComments", commentdeletedataobject).then(function (RCInsertCommentforStudent) {
                        $scope.RCInsertCommentforStudent = RCInsertCommentforStudent.data;
                        if ($scope.RCInsertCommentforStudent == true) {
                            swal({ text: 'Student Comment Deleted Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                            $scope.copycomment = false;
                            $scope.maincomment = true;
                            $scope.GradeSectionSubject2();

                        }
                        else {
                            swal({ text: 'Student Comment Not Deleted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                        };
                    });
                }
                else {

                    swal({ text: 'Select Atleast One Student And Uncheck  One Comment to Delete Student Comment', imageUrl: "assets/img/notification-alert.png", width: 350, showCloseButton: true });
                }
            }

            $scope.InsertfinalCommentForStudent1 = function () {
                debugger
                if ($scope.fcommn.sims_comment_code == 'S' && ($scope.fcommn.sims_subject_code == '' || $scope.fcommn.sims_subject_code == undefined || $scope.fcommn.sims_subject_code == null)) {
                    $scope.checksubject = false;

                } else {
                    $scope.checksubject = true;
                }

                if ($scope.checksubject == true) {
                    var commentdataobject = [];
                    for (var i = 0; i < $scope.commentstudent.length; i++) {
                        if ($scope.commentstudent[i].sims_student_check_status == true) {

                            var data = {}
                            data.sims_admission_cur_code = $scope.fcommn.cur_code;
                            data.sims_academic_year = $scope.fcommn.academic_year;
                            data.sims_grade_code = $scope.fcommn.sims_grade_code;
                            data.sims_section_code = $scope.fcommn.sims_section_code;
                            data.sims_term_code = $scope.fcommn.sims_term_code;
                            data.sims_subject_code = $scope.fcommn.sims_subject_code;

                            data.sims_comment_type = $scope.fcommn.sims_comment_code;
                            data.sims_enroll_number = $scope.commentstudent[i].sims_enroll_number;
                            data.sims_comment_header_desc = $scope.fcommn.sims_comment;
                            data.sims_student_comment_header_id = $scope.fcommn.sims_student_comment_header_id;
                            data.sims_student_comment_id = $scope.fcommn.sims_comment_code1;
                            data.sims_comment_status = $scope.commentstudent[i].sims_comment_status1;
                            data.sims_comment = $scope.fcommn.sims_comment
                            data.sims_user_code = user;
                            data.sims_comment_type1 = 'C'
                            commentdataobject.push(data);
                        }
                    }

                    $http.post(ENV.apiUrl + "api/Gradebook/RCInsertCommentforStudentpearl", commentdataobject).then(function (RCInsertCommentforStudent) {
                        $scope.RCInsertCommentforStudent = RCInsertCommentforStudent.data;

                        if ($scope.RCInsertCommentforStudent == true) {
                            $scope.fcommn.sims_comment = '';
                            swal({ text: 'Student Comment Inserted Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                            $scope.newComment = false;
                            $scope.newheader = false;
                            $scope.GetAllStudentForfinalComment1();
                        }
                        else {
                            swal({ text: 'Student Comment Not Inserted', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                        };
                    });
                }
                else {
                    swal({ text: 'Select Subject', width: 250, height: 200, showCloseButton: true });
                }
            }

            /***************************************************************Final Comment End**************************************************************************************************/
            $scope.sComment = {};

            $scope.Getyear2 = function (newval) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + newval).then(function (Academicyear) {
                    $scope.Academic_year2 = Academicyear.data;

                    $scope.sComment['cur_code'] = newval;
                    $scope.sComment['academic_year'] = $scope.Academic_year2[0].sims_academic_year;
                    $scope.getsections3(newval, $scope.Academic_year2[0].sims_academic_year);

                });
            }

            $scope.getsection3 = function (str, str1, str2) {
                $http.get(ENV.apiUrl + "api/Gradebook/GetRCSection?curcode=" + str + "&ayear=" + str1 + "&gradecode=" + str2 + "&teachercode=" + user).then(function (Allsection) {
                    $scope.getSectionFromGrade2 = Allsection.data;


                })
            };

            $scope.getsections3 = function (str, str1) {

                $http.get(ENV.apiUrl + "api/Gradebook/GetRCGrade?curcode=" + str + "&ayear=" + str1 + "&teachercode=" + user).then(function (res) {
                    $scope.getAllGrades2 = res.data;

                })
            };

            $scope.getreportcardlevelforcomment = function () {

                $http.get(ENV.apiUrl + "api/Gradebook/GetRCReportCardLevel?curcode=" + $scope.sComment.cur_code + "&ayear=" + $scope.sComment.academic_year + "&gradecode=" + $scope.sComment.sims_grade_code + "&section=" + $scope.sComment.sims_section_code).then(function (Reportlevels) {
                    $scope.Reportlevels = Reportlevels.data;
                });

            }

            $scope.getreportcardforcomment = function () {
                $http.get(ENV.apiUrl + "api/Gradebook/GetRCReportCardNames?curcode=" + $scope.sComment.cur_code + "&ayear=" + $scope.sComment.academic_year + "&gradecode=" + $scope.sComment.sims_grade_code + "&section=" + $scope.sComment.sims_section_code + "&levelCode=" + $scope.sComment.level_code).then(function (GetRCReportCardNames) {
                    $scope.GetRCReportCardNames = GetRCReportCardNames.data;


                });
            }

            $scope.GetfinalCommentsstudents = function () {

                var data = {};
                data.Curr_Code = $scope.sComment.cur_code
                data.Academic_Year = $scope.sComment.academic_year
                data.Grade_Code = $scope.sComment.sims_grade_code
                data.Section_code = $scope.sComment.sims_section_code
                data.Report_Card_Code = $scope.sComment.report_Card_Code
                data.Level_code = $scope.sComment.level_code


                $http.post(ENV.apiUrl + "api/Gradebook/ReportCardStudent_Finalcomment", data).then(function (finalcommentstudent) {
                    $scope.finalcommentstudent = finalcommentstudent.data;



                    for (var i = 0; i < $scope.finalcommentstudent.length; i++) {

                        if ($scope.finalcommentstudent[i].comment1_code != '') {
                            $scope.finalcommentstudent[i].comment1_status = false;
                            $scope.finalcommentstudent[i].comment2_status = true;
                            $scope.finalcommentstudent[i].comment3_status = true;
                        }
                        else if ($scope.finalcommentstudent[i].comment2_code != '') {
                            $scope.finalcommentstudent[i].comment2_status = false;
                            $scope.finalcommentstudent[i].comment1_status = true;
                            $scope.finalcommentstudent[i].comment3_status = true;
                        }
                        else if ($scope.finalcommentstudent[i].comment3_code != '') {
                            $scope.finalcommentstudent[i].comment3_status = false;
                            $scope.finalcommentstudent[i].comment1_status = true;
                            $scope.finalcommentstudent[i].comment2_status = true;
                        }
                        else {

                            $scope.finalcommentstudent[i].comment1_status = false;
                            $scope.finalcommentstudent[i].comment2_status = false;
                            $scope.finalcommentstudent[i].comment3_status = false;
                        }
                    }

                    $scope.finalCommentcombovalues = [{
                        value: 'Comment1',
                        name: 'Comment 1',
                        disabled: false
                    },
                    {
                        value: 'Comment2',
                        name: 'Comment 2',
                        disabled: true
                    },
                    {
                        value: 'Comment2',
                        name: 'Comment 2',
                        disabled: true
                    }]


                });


                $http.get(ENV.apiUrl + "api/Gradebook/GetReportCardComments?data=" + JSON.stringify(data)).then(function (GetReportCardComments) {
                    $scope.GetReportCardComments = GetReportCardComments.data;
                });
            }

            $scope.Click_Apply_Comment_of_Student = function () {

                var v = document.getElementById('Overwritecomment');
                for (var i = 0; i < $scope.finalcommentstudent.length; i++) {

                    if ($scope.finalcommentstudent[i].comment1_code == '' && $scope.finalcommentstudent[i].comment2_code == '' && $scope.finalcommentstudent[i].comment3_code == '') {
                        if ($scope.val.finalcomment_status == 'Comment1') {
                            $scope.finalcommentstudent[i].comment1_status = false;
                            $scope.finalcommentstudent[i].comment2_status = true;
                            $scope.finalcommentstudent[i].comment3_status = true;
                            $scope.finalcommentstudent[i].comment1_code = $scope.val.comment_code;
                            $scope.finalcommentstudent[i].comment2_code = '';
                            $scope.finalcommentstudent[i].comment3_code = '';
                        }
                        else if ($scope.val.finalcomment_status == 'Comment2') {
                            $scope.finalcommentstudent[i].comment2_status = false;
                            $scope.finalcommentstudent[i].comment1_status = true;
                            $scope.finalcommentstudent[i].comment3_status = true;
                            $scope.finalcommentstudent[i].comment2_code = $scope.val.comment_code;
                            $scope.finalcommentstudent[i].comment1_code = '';
                            $scope.finalcommentstudent[i].comment3_code = '';
                        }
                        else if ($scope.val.finalcomment_status == 'Comment3') {
                            $scope.finalcommentstudent[i].comment3_status = false;
                            $scope.finalcommentstudent[i].comment1_status = true;
                            $scope.finalcommentstudent[i].comment2_status = true;
                            $scope.finalcommentstudent[i].comment3_code = $scope.val.comment_code;
                            $scope.finalcommentstudent[i].comment2_code = '';
                            $scope.finalcommentstudent[i].comment1_code = '';
                        }
                    }

                    if (v.checked == true) {

                        if ($scope.val.finalcomment_status == 'Comment1') {
                            $scope.finalcommentstudent[i].comment1_status = false;
                            $scope.finalcommentstudent[i].comment2_status = true;
                            $scope.finalcommentstudent[i].comment3_status = true;
                            $scope.finalcommentstudent[i].comment1_code = $scope.val.comment_code;
                            $scope.finalcommentstudent[i].comment2_code = '';
                            $scope.finalcommentstudent[i].comment3_code = '';
                        }
                        else if ($scope.val.finalcomment_status == 'Comment2') {
                            $scope.finalcommentstudent[i].comment2_status = false;
                            $scope.finalcommentstudent[i].comment1_status = true;
                            $scope.finalcommentstudent[i].comment3_status = true;
                            $scope.finalcommentstudent[i].comment2_code = $scope.val.comment_code;
                            $scope.finalcommentstudent[i].comment1_code = '';
                            $scope.finalcommentstudent[i].comment3_code = '';
                        }
                        else if ($scope.val.finalcomment_status == 'Comment3') {
                            $scope.finalcommentstudent[i].comment3_status = false;
                            $scope.finalcommentstudent[i].comment1_status = true;
                            $scope.finalcommentstudent[i].comment2_status = true;
                            $scope.finalcommentstudent[i].comment3_code = $scope.val.comment_code;
                            $scope.finalcommentstudent[i].comment1_code = '';
                            $scope.finalcommentstudent[i].comment2_code = '';
                        }
                    }
                }


            }

            $scope.Click_InsertFinal_Comment_of_Student = function () {

                var data = {};
                $scope.finalcommentdat = [];
                for (var i = 0; i < $scope.finalcommentstudent.length; i++) {

                    if ($scope.finalcommentstudent[i].ischange == true) {
                        data = {};
                        data.Curr_Code = $scope.sComment.cur_code;
                        data.Academic_Year = $scope.sComment.academic_year;
                        data.Grade_Code = $scope.sComment.sims_grade_code;
                        data.Section_code = $scope.sComment.sims_section_code;
                        data.Level_code = $scope.sComment.level_code;
                        data.Report_Card_Code = $scope.sComment.report_Card_Code;
                        data.enroll = $scope.finalcommentstudent[i].enroll;
                        data.Comment1_code = $scope.finalcommentstudent[i].comment1_code;
                        data.Comment2_code = $scope.finalcommentstudent[i].comment2_code;
                        data.Comment3_code = $scope.finalcommentstudent[i].comment3_code;
                        data.user1 = user;
                        data.user2 = user;
                        data.user3 = user;
                        data.Status = 'A';
                        data.EnrollList_I = '';
                        data.EnrollList_U = '';
                        data.commnetNo = '';
                        data.opr = 'I';

                        $scope.finalcommentdat.push(data);
                    }
                }
                if ($scope.finalcommentdat.length >= 1) {
                    $http.post(ENV.apiUrl + "api/Gradebook/ReportCardDataOprF", $scope.finalcommentdat).then(function (finalcommentstudent) {
                        $scope.finalcommentstudent = finalcommentstudent.data;
                        if ($scope.finalcommentstudent == true) {

                            swal({ text: 'Comment Inserted Successfully', imageUrl: "assets/img/check.png", width: 350, showCloseButton: true });
                            $scope.GetfinalCommentsstudents();
                        }
                        else {
                            swal({ text: 'Comment Not Inserted', imageUrl: "assets/img/close.png", width: 350, showCloseButton: true });
                        }
                    });
                }
                else {

                    swal({ text: 'Select or Change Atleast One Record To Update Or Insert Comment', imageUrl: "assets/img/notification-alert.png", width: 350, showCloseButton: true });
                }
            }

            $scope.selectfinalcomment = function (info) {
                info.ischange = true;

                if (info.comment1_code != '') {
                    info.comment2_status = true;
                    info.comment3_status = true;

                }
                if (info.comment2_code != '') {
                    info.comment3_status = true;
                    info.comment1_status = true;

                }
                if (info.comment3_code != '') {
                    info.comment1_status = true;
                    info.comment2_status = true;

                }
            }

            $scope.Settextchange = function (info) {

                if (info != '') {
                    $scope.checkdisable = true;
                }
                else {
                    $scope.checkdisable = false;
                }

          }

            $scope.InsertIntoHistory = function (str) {
                debugger
            //    $scope.result=false;
                if ($http.defaults.headers.common['schoolId'] == 'ahis') {
                    console.log("student_data", $scope.student_old_marks);
                    $scope.data = [];
                    $scope.count = 1;
                    for (var i = 0; i < $scope.student_old_marks.length; i++) {
                        if ($scope.student_old_marks[i].StudEnroll == str.StudEnroll) {
                            if ($scope.student_old_marks[i].assignment_Mark != str.assignment_Mark) {
                                var d = {
                                    sims_academic_year: $scope.student_old_marks[i].assignment_academic_year,
                                    sims_cur_code: $scope.student_old_marks[i].stud_CurrCode,
                                    sims_grade_code: $scope.student_old_marks[i].assignment_grade_code,
                                    sims_section_code: $scope.student_old_marks[i].assignment_section_code,
                                    sims_gb_number: $scope.student_old_marks[i].assignment_gb_number,
                                    sims_gb_cat_code: $scope.student_old_marks[i].assignment_cat_code,
                                    sims_gb_cat_assign_number: $scope.student_old_marks[i].assignment_Code,
                                    sims_gb_cat_assign_enroll_number: $scope.student_old_marks[i].StudEnroll,
                                    sims_gb_cat_assign_mark: $scope.student_old_marks[i].assignment_Mark,
                                    sims_gb_cat_assign_final_grade: $scope.student_old_marks[i].assignment_final_grade,
                                    sims_gb_cat_assign_max_point: $scope.student_old_marks[i].assignment_max_point,
                                    sims_gb_cat_assign_max_correct: $scope.student_old_marks[i].assignment_max_point,
                                    sims_gb_cat_assign_comment: $scope.student_old_marks[i].assignment_Comment,
                                    sims_gb_cat_assign_status: 'Marks Updated User',
                                    sims_gb_updated_by: $scope.marks_updadated_by,
                                }
                                $scope.data.push(d);
                            }
                        }
                    }
               
                        $http.post(ENV.apiUrl + "api/Gradebook_History/CreateHistory", $scope.data).then(function (res) {
                            $scope.result = res.data;
                            if ($scope.result == true) {
                                for (var i = 0; i < $scope.student_data.length; i++) {
                                    if ($scope.student_data[i].StudEnroll == str.StudEnroll) {
                                        $scope.student_data[i].ischecked = true
                                    }
                                }
                                GradeBookStudentMarkData = [];
                                $scope.afterSaveData = [];
                              //  $scope.student_old_marks = [];
                                for (var i = 0; i < $scope.student_data.length; i++) {
                                    if ($scope.student_data[i].ischecked == true) {
                                        var data = {
                                            Assignment_cur_code: $scope.student_data[i].stud_CurrCode,
                                            Assignment_academic_year: $scope.student_data[i].stud_AcademicYear,
                                            Assignment_grade_code: $scope.student_data[i].stud_GradeCode,
                                            Assignment_section_code: $scope.student_data[i].stud_SectionCode,
                                            Assignment_gb_number: $scope.student_data[i].assignment_gb_number,
                                            Assignment_cat_code: $scope.student_data[i].assignment_cat_code,
                                            Assignment_Code: $scope.student_data[i].assignment_Code,
                                            Assignment_max_point: $scope.student_data[i].assignment_max_point,
                                            sims_mark_final_grade_code: $scope.student_data[i].sims_mark_final_grade_code,
                                            Assignment_Mark: $scope.student_data[i].assignment_Mark,
                                            Assignment_max_correct: $scope.student_data[i].assignment_max_correct,
                                            Assignment_Comment: $scope.student_data[i].assignment_Comment,
                                            StudEnroll: $scope.student_data[i].StudEnroll,
                                            Assignment_Status1: $scope.student_data[i].assignment_Status1,
                                            Assignment_Completed_Date: $scope.student_data[i].assignment_Completed_Date
                                        };

                                        GradeBookStudentMarkData.push(data);
                                        $scope.student_data[i].ischecked = false;
                                    }
                                }

                                if (GradeBookStudentMarkData.length > 0) {
                                    $http.post(ENV.apiUrl + "api/Gradebook/AssignMarksToStudentsM", GradeBookStudentMarkData).then(function (AssignMarksToStudentsM) {
                                        $scope.AssignMarks_ToStudentsM = AssignMarksToStudentsM.data;


                                        if ($scope.AssignMarks_ToStudentsM == true) {
                                            $scope.student_old_marks = [];
                                            $scope.markentryflag = false;
                                            $scope.markentryflag1 = false;
                                            $scope.markentryflag2 = false;
                                            $scope.afterSaveData = angular.copy($scope.student_data);
                                            $scope.student_old_marks = angular.copy($scope.afterSaveData);
                                            //swal({
                                            //    text: 'Student Assignment Mark Updated',
                                            //    imageUrl: "assets/img/check.png",
                                            //    width: 300,
                                            //    showCloseButton: true
                                            //});
                                            $scope.GradeBookNames();

                                        }
                                        else {
                                            $scope.markentryflag = false;
                                            swal({
                                                text: 'Student Assignment Mark Not Updated',
                                                imageUrl: "assets/img/close.png",
                                                width: 300,
                                                showCloseButton: true
                                            });
                                        }

                                    });
                                }
                                else {
                   
                                    swal({
                                        text: 'Change At Least One Record To Update Student Assignment Mark',
                                        imageUrl: "assets/img/notification-alert.png",
                                        width: 380,
                                        showCloseButton: true
                                    });
                                }
                            }
                        });                     
                   
                    console.log("$scope.data", $scope.data);
                }

            }
            //edt.cur_code,edt.academic_year,edt.sims_subject_code,edt

            $scope.check_grade_scale = function (edt_list) {
            debugger
            $scope.check_scale_entered = '';
                $http.get(ENV.apiUrl + "api/Gradebook_History/getGBScaleDataForUpdate?CUR_CODE=" + edt_list.cur_code + "&ACADEMIC_YEAR=" + edt_list.academic_year + "&GRADE_CODE=" + edt_list.grade_code + "&SECTION_CODE=" + edt_list.section_code
            + "&GB_NUMBER=" + edt_list.gb_numbessssssr).then(function (GradeScaleData) {
                $scope.table_data = GradeScaleData.data;
                $scope.check_scale_entered = $scope.table_data[0].marks_student;
           
                if ($scope.check_scale_entered == '1') {
                    $scope.upinsert = false;
                    swal({
                        text: 'Marks Has Been Entered Already ,Unable To Change Scale Group!',
                        // imageUrl: "assets/img/check.png",
                        width: 300,
                        showCloseButton: true
                    });
                }
            });
            }


        }]);
})();   