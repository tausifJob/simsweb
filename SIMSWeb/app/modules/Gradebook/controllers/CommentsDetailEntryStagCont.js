﻿(function () {
    'use strict';
    var teacher_code = [];
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CommentsDetailEntryStagCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var user = $rootScope.globals.currentUser.username;
            var appl_code = "sim070";
            $scope.fcommn = [];
            $scope.pagesize = 'All';
            $scope.pageindex = "0";
            $scope.pager = false;
            $scope.toppager = false;
            $scope.busyindicator = true;
            $scope.showtable = false;
            $scope.detailsField = true;
            var sub_head = '';
            var effort_head = '';
            $scope.arebicolm = true;
            $http.get(ENV.apiUrl + "api/Gradebook/GetadminuserType?username=" + user + "&appl_code=" + appl_code).then(function (getAdmin) {
                $scope.getadmin = getAdmin.data;

            });
           

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            setTimeout(function () {
                $("#rcbItem").select2();

            }, 100);

            var username = $rootScope.globals.currentUser.username;
            $http.get(ENV.apiUrl + "api/Gradebook/getCommenttype_new?uname=" + username).then(function (Comment_type) {
                $scope.get_Comment_type = Comment_type.data;
                $scope.fcommn.sims_comment_code = $scope.get_Comment_type[1].sims_comment_code;
            })
            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['chk_all'] = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                debugger;
                if ($scope.pagesize == 'All') {
                    $scope.currentPage = 1;
                    $scope.showPager = false;
                    $scope.filteredTodos = $scope.st_lst1;
                }
                else {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                }
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.student_list, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.student_list;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.sComment = {};

            // arebic coloumn hide

            $scope.checklickarebic = function (str)
            {
                debugger;
                if (str == true) {
                    $scope.arebicolm = false;
                }
                else {
                    $scope.arebicolm = true;
                }
                
            }

            //get curriculm
            debugger
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                debugger
                $scope.curriculum = res.data;
                $scope.edt = {
                    cur_code: $scope.curriculum[0].sims_cur_code
                }
                $scope.fcommn['cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr1($scope.curriculum[0].sims_cur_code);
            });

            // get academic year
            $scope.getacyr1 = function (str) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.getAcademicYear1 = Academicyear.data; //academic_year
                    $scope.fcommn['academic_year'] = $scope.getAcademicYear1[0].sims_academic_year;
                    $scope.getcycleterm($scope.fcommn.cur_code, $scope.getAcademicYear1[0].sims_academic_year);
                });
            }

            // get cycle /term
            $scope.getcycleterm = function (str, str1) {
                debugger;
                $http.get(ENV.apiUrl + "api/CommentsDetailsEntryLeamsController/GetCycle?curcode=" + str + "&ayear=" + str1 + "&teachercode=" + user).then(function (res) {
                    $scope.TermsAcademicYear1 = res.data;
                    //$scope.fcommn['sims_report_card_term_code'] = $scope.TermsAcademicYear1[0].sims_report_card_config_code;
                    // $scope.getgrade(str, str1, $scope.TermsAcademicYear1[0].sims_report_card_config_code +','+$scope.TermsAcademicYear1[0].sims_report_card_term_code);
                    $scope.GradeSectionSubject2();
                })
            };

            // get grade
            $scope.getgrade = function (str, str1,str2) {
                debugger;
                 
                $scope.str5 = str2.split(',');
                $scope.fcommn.sims_term_code = $scope.str5[1];
                $http.get(ENV.apiUrl + "api/CommentsDetailsEntryLeamsController/GetCEGrade?curcode=" + str + "&ayear=" + str1 + "&termcode=" + $scope.str5[0] + "&teachercode=" + user).then(function (Allsection) {
                    $scope.getFromGrade = Allsection.data;
                     //$scope.fcommn['sims_grade_code'] = $scope.getFromGrade[0].sims_grade_code;
                    
                    $scope.getsection(str, str1, str2, $scope.getFromGrade[0].sims_grade_code);
                })
            };

            //get Section
            $scope.getsection = function (str, str1, str2,str3) {
                $http.get(ENV.apiUrl + "api/CommentsDetailsEntryLeamsController/GetCESection?curcode=" + str + "&ayear=" + str1 + "&termcode=" + str2 + "&gradecode=" + str3 + "&teachercode=" + user).then(function (Allsection) {
                    $scope.getSectionFromGrade = Allsection.data;
                     //$scope.fcommn['sims_section_code'] = $scope.getSectionFromGrade[0].sims_section_code;
                     //$scope.GradeSectionSubject2();
            
                })
            };

           // Get Subject
            $scope.GradeSectionSubject2 = function () {
                debugger;
                
                var subject_names1 = [];

               

                var report1 = {
                    sims_grade_code: $scope.fcommn.sims_grade_code,
                    sims_academic_year: $scope.fcommn.academic_year,
                    sims_section_code: $scope.fcommn.sims_section_code,
                    sims_employee_code: user,
                    sims_cur_code: $scope.fcommn.cur_code
                };


                $http.post(ENV.apiUrl + "api/CommentsDetailsEntryLeamsController/Subjectsget", report1).then(function (allSection_Subject) {
                    $scope.subject_names1 = allSection_Subject.data;

                });
                }

            // get student list


            $scope.GetfinalCommentsstudents = function () {
                debugger;
                $scope.busyindicator = false;
                $scope.showtable = false;
                var sub_selection = document.getElementById('subject_dropdown').value;
                console.log(sub_selection);
                if (sub_selection == "" && $scope.fcommn.sims_comment_code == 'S') {
                    swal({ title: "Alert", text: "Please Select Subject", width: 300, height: 200 });
                }
                else {
                    //var a = { opr: 'AA', sims_cur_code: $scope.fcommn.cur_code, sims_academic_year: $scope.fcommn.academic_year, sims_grade_code: $scope.fcommn.sims_grade_code, sims_section_code: $scope.fcommn.sims_section_code, sims_term_code: $scope.fcommn.sims_term_code, sims_comment_type: $scope.fcommn.sims_comment_code, sims_subject_code: $scope.fcommn.sims_subject_code, sims_level_code: $scope.fcommn.sims_level_code, sims_report_card_code: $scope.fcommn.sims_report_card_code };

                    var demo = [];
                    var demo1 = [];
                    $http.get(ENV.apiUrl + "api/CommentsDetailsEntryLeamsController/getGradeBookComment?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code + "&term_code=" + $scope.fcommn.sims_term_code + "&comment_type=" + 'S' + "&subject=" + $scope.fcommn.sims_subject_code + "&user=" + $rootScope.globals.currentUser.username).then(function (Commentsheader1) {
                        $scope.comment_lst = Commentsheader1.data
                        console.log($scope.comment_lst);

                        //if (Commentsheader1.data.length > 0) {
                        //    for (var j = 0; j < $scope.comment_lst[0].sublist1.length; j++) {
                        //        demo = {
                        //            'sims_comment_code': $scope.comment_lst[0].sublist1[j].sims_comment_code,
                        //            'sims_comment_desc': $scope.comment_lst[0].sublist1[j].sims_comment_desc
                        //        }
                        //        demo1.push(demo);
                        //    }
                        //}
                        $scope.info1 = demo1;
                        $scope.btn_disable = false;

                        $scope.totalItems = $scope.comment_lst.length;
                        $scope.todos = $scope.comment_lst;
                        $scope.makeTodos();
                        $scope.busyindicator = true;
                        $scope.showtable = true;
                    });

                    
                }
            }


            //save update table
            $scope.btn_disable = false;
            $scope.Insert_data = function () {
                debugger;
                $scope.btn_disable = true;
                $scope.datasend1 = [];
                for (var i = 0; i < $scope.comment_lst.length; i++) {
                    if ($scope.comment_lst[i].subcheck == true) {



                        var datasnd = {

                            sims_cur_code: $scope.fcommn.cur_code,
                            sims_academic_year: $scope.fcommn.academic_year,
                            sims_grade_code: $scope.fcommn.sims_grade_code,
                            sims_section_code: $scope.fcommn.sims_section_code,
                            sims_term_code: $scope.fcommn.sims_term_code,
                            sims_subject_code: $scope.fcommn.sims_subject_code,
                            sims_comment_type: $scope.fcommn.sims_comment_code,

                            //sims_student_comment_id: $scope.comment_lst[i].sublist1[j].s_sims_comment_code,
                            //sims_comment_code: $scope.fcommn.sims_comment_code,

                            sims_user_code: $rootScope.globals.currentUser.username,
                            sims_enroll_number: $scope.comment_lst[i].sims_enroll_number,
                            Student_TotalMark: $scope.comment_lst[i].student_TotalMark,
                            sims_comment_desc: $scope.comment_lst[i].s_sims_comment_desc,
                            sims_comment_desc_ot: $scope.comment_lst[i].sims_comment_desc_ot,

                        }

                        $scope.datasend1.push(datasnd);

                    }
                }

                $http.post(ENV.apiUrl + "api/CommentsDetailsEntryLeamsController/CUInsertcommentNN", $scope.datasend1).then(function (msg) {

                    if (msg.data) {
                        $scope.datasend1 = [];
                        swal({ title: "Alert", text: "Records Inserted successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        $scope.GetfinalCommentsstudents();
                    }
                    else {
                        swal({ title: "Alert", text: "Records Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        $scope.btn_disable = false;
                    }


                });




            }

        
            $scope.CheckAllChecked = function () {
                var main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }
            }

          
            $scope.getSubHead = function () {
                debugger
                $http.get(ENV.apiUrl + "api/CommentsDetailsEntryController/getsubjectheader?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code + "&level=" + $scope.fcommn.sims_level_code + "&report=" + $scope.fcommn.sims_report_card_code).then(function (sub_code) {
                    $scope.getsubjectheader1 = sub_code.data;
                    $scope.sub_head = $scope.getsubjectheader1[0].sims_report_card_code;
                });
            }

            $scope.getEffHead = function () {
                debugger
                $http.get(ENV.apiUrl + "api/CommentsDetailsEntryController/getsubjecteffortheader?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code + "&level=" + $scope.fcommn.sims_level_code + "&report=" + $scope.fcommn.sims_report_card_code).then(function (sub_code) {
                    $scope.getsubjecteffortheader1 = sub_code.data;
                    $scope.effort_head = $scope.getsubjecteffortheader1[0].sims_report_card_code;
                });
            }

            $scope.getgeneralHead = function () {
                debugger
                $http.get(ENV.apiUrl + "api/CommentsDetailsEntryController/getgeneraltheader?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code + "&level=" + $scope.fcommn.sims_level_code + "&report=" + $scope.fcommn.sims_report_card_code).then(function (sub_code) {
                    $scope.getsubjecteffortheader1 = sub_code.data;
                    effort_head = $scope.getsubjecteffortheader1[0].sims_report_card_code;
                });
            }

            $scope.save_sub_comments = function () {
                $scope.busyindicator = false;
                $http.get(ENV.apiUrl + "api/CommentsDetailsEntryController/getsubjectheader?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code + "&level=" + $scope.fcommn.sims_level_code + "&report=" + $scope.fcommn.sims_report_card_code).then(function (sub_code) {
                    $scope.getsubjectheader1 = sub_code.data;
                    $scope.sub_head = $scope.getsubjectheader1[0].sims_report_card_code;
                    $http.get(ENV.apiUrl + "api/CommentsDetailsEntryController/getsubjecteffortheader?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code + "&level=" + $scope.fcommn.sims_level_code + "&report=" + $scope.fcommn.sims_report_card_code).then(function (sub_code) {
                        $scope.getsubjecteffortheader1 = sub_code.data;
                        $scope.effort_head = $scope.getsubjecteffortheader1[0].sims_report_card_code;

                        $scope.datasend = [];
                        $scope.datasend1 = [];
                        $scope.datasend2 = [];
                        //for (var i = 0; i < $scope.st_lst1.length; i++) {
                        //    var t = $scope.st_lst1[i].sims_enroll_number;
                        //    var v = document.getElementById(t + i);
                        //    if (v != null) {
                        //        if (v.checked == true) {
                        //            $scope.datasend.push($scope.st_lst1[i]);
                        //        }
                        //    }
                        //}
                        console.log($scope.datasend);

                        for (var i = 0; i < $scope.st_lst1.length; i++) {
                            if ($scope.st_lst1[i].subcheck == true) {
                                debugger;

                                var datasnd = {
                                    sims_comment_sr: $scope.st_lst1[i].sims_comment_sr,
                                    sims_academic_year: $scope.st_lst1[i].sims_academic_year,
                                    sims_cur_code: $scope.st_lst1[i].sims_cur_code,
                                    sims_grade_code: $scope.st_lst1[i].sims_grade_code,
                                    sims_section_code: $scope.st_lst1[i].sims_section_code,
                                    sims_enroll_number: $scope.st_lst1[i].sims_enroll_number,
                                    sims_term_code: $scope.fcommn.sims_term_code,
                                    sims_subject_code: $scope.fcommn.sims_subject_code,
                                    sims_level_code: $scope.fcommn.sims_level_code,
                                    sims_report_card_code: $scope.fcommn.sims_report_card_code,
                                    sims_student_comment_id: $scope.st_lst1[i].sims_student_comment_id,
                                    sims_student_root_comment_display_order: $scope.st_lst1[i].sims_student_root_comment_display_order,
                                    sims_comment_status: $scope.st_lst1[i].sims_comment_status,
                                    sims_comment_type: $scope.st_lst1[i].sims_comment_type,
                                    sims_user_code: $rootScope.globals.currentUser.username,
                                    sims_student_header_code: $scope.sub_head,//$scope.st_lst1[i].sims_student_header_code,
                                    //    sims_student_header_code1: sub_head,
                                    student_name: $scope.st_lst1[i].student_name,
                                    sims_comment_desc: $scope.st_lst1[i].sims_comment_desc
                                }
                                $scope.datasend1.push(datasnd);
                            }
                        }

                        for (var i = 0; i < $scope.st_lst1.length; i++) {
                            if ($scope.st_lst1[i].subcheck == true) {
                                debugger;
                                var datasnd2 = {
                                    sims_comment_sr: $scope.st_lst1[i].sims_comment_sr,
                                    sims_academic_year: $scope.st_lst1[i].sims_academic_year,
                                    sims_cur_code: $scope.st_lst1[i].sims_cur_code,
                                    sims_grade_code: $scope.st_lst1[i].sims_grade_code,
                                    sims_section_code: $scope.st_lst1[i].sims_section_code,
                                    sims_enroll_number: $scope.st_lst1[i].sims_enroll_number,
                                    sims_term_code: $scope.fcommn.sims_term_code,
                                    sims_subject_code: $scope.fcommn.sims_subject_code,
                                    sims_level_code: $scope.fcommn.sims_level_code,
                                    sims_report_card_code: $scope.fcommn.sims_report_card_code,
                                    sims_student_comment_id: $scope.st_lst1[i].sims_student_comment_id,
                                    sims_student_root_comment_display_order: $scope.st_lst1[i].sims_student_root_comment_display_order,
                                    sims_comment_status: $scope.st_lst1[i].sims_comment_status,
                                    sims_comment_type: $scope.st_lst1[i].sims_comment_type,
                                    sims_user_code: $rootScope.globals.currentUser.username,
                                    sims_student_header_code: $scope.effort_head,//$scope.st_lst1[i].sims_student_header_code,
                                    //sims_student_header_code1: effort_head,
                                    student_name: $scope.st_lst1[i].student_name,
                                    sims_comment_desc: $scope.st_lst1[i].sims_effort_desc
                                }
                                $scope.datasend2.push(datasnd2);
                            }
                        }

                        console.log($scope.datasend1);
                        console.log($scope.datasend2);


                        $http.post(ENV.apiUrl + "api/CommentsDetailsEntryController/CUDEleteComment", $scope.datasend1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            console.log("msg", msg);

                            if ($scope.msg1 == true) {
                                console.log("deleted");
                            }

                            // swal({ title: "Alert", text: "Records Inserted successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            $http.post(ENV.apiUrl + "api/CommentsDetailsEntryController/CUInsertcomment", $scope.datasend1).then(function (msg) {
                                $scope.msg1 = msg.data;
                                console.log("msg", msg);

                                if ($scope.msg1 == true) {
                                    $scope.datasend1 = [];
                                    //swal({ title: "Alert", text: "Records Inserted successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                    //  inserting efforts comment
                                    $http.post(ENV.apiUrl + "api/CommentsDetailsEntryController/CUInsertcomment", $scope.datasend2).then(function (msg) {
                                        $scope.msg1 = msg.data;
                                        console.log("msg", msg);

                                        if ($scope.msg1 == true) {
                                            $scope.datasend2 = [];
                                            swal({ title: "Alert", text: "Records Inserted successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                            $scope.GetfinalCommentsstudents();
                                        }
                                        else {
                                            swal({ title: "Alert", text: "Records Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Records Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                            });
                        });
                    });
                });
                $scope.busyindicator = true;
            }

            $scope.save_general_comments = function () {
                $scope.busyindicator = false;
                $scope.datasend3 = [];

                for (var i = 0; i < $scope.st_lst1.length; i++) {
                    if ($scope.st_lst1[i].subcheck == true) {
                        var datasnd = {
                            sims_comment_sr: $scope.st_lst1[i].sims_comment_sr,
                            sims_academic_year: $scope.st_lst1[i].sims_academic_year,
                            sims_cur_code: $scope.st_lst1[i].sims_cur_code,
                            sims_grade_code: $scope.st_lst1[i].sims_grade_code,
                            sims_section_code: $scope.st_lst1[i].sims_section_code,
                            sims_enroll_number: $scope.st_lst1[i].sims_enroll_number,
                            sims_term_code: $scope.fcommn.sims_term_code,
                            sims_subject_code: $scope.fcommn.sims_subject_code,
                            sims_level_code: $scope.fcommn.sims_level_code,
                            sims_report_card_code: $scope.fcommn.sims_report_card_code,
                            sims_student_comment_id: $scope.st_lst1[i].sims_student_comment_id,
                            sims_student_root_comment_display_order: $scope.st_lst1[i].sims_student_root_comment_display_order,
                            sims_comment_status: $scope.st_lst1[i].sims_comment_status,
                            sims_comment_type: $scope.st_lst1[i].sims_comment_type,
                            sims_user_code: $rootScope.globals.currentUser.username,
                            sims_student_header_code: $scope.general_head,
                            student_name: $scope.st_lst1[i].student_name,
                            sims_comment_desc: $scope.st_lst1[i].sims_comment_desc
                        }
                        $scope.datasend3.push(datasnd);
                    }
                }




                $http.post(ENV.apiUrl + "api/CommentsDetailsEntryController/CUDEleteComment", $scope.datasend3).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        console.log("deleted");
                    }

                    $http.post(ENV.apiUrl + "api/CommentsDetailsEntryController/CUInsertcomment", $scope.datasend3).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            $scope.datasend3 = [];
                            swal({ title: "Alert", text: "Records Inserted successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            $scope.GetfinalCommentsstudents();
                        }
                        else {
                            swal({ title: "Alert", text: "Unable to Proceed,Please select atleast one record", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                    });

                });
                $scope.busyindicator = true;
            }

            $scope.export = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('excel_data').innerHTML],
                         {
                             type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                         });
                        $scope.detailsField = false;
                        saveAs(blob, "Comment Detail Entry Details.xls");
                        $scope.colsvis = false;
                        $scope.GetfinalCommentsstudents();

                    }
                    $scope.GetfinalCommentsstudents();
                });


            };
        }]);
})();