﻿(function () {
    'use strict';
    var teacher_code = [];
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('GradeBookCopyYoYCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            //$(function () {
            //    $('#Select3').multipleSelect({
            //        width: '100%'
            //    });
            //});
            $scope.getCurriculum = function () {
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                    $scope.Allcurriculum = res.data;
                    if ($scope.Allcurriculum.length > 0) {
                        $scope.sims_cur_code = $scope.Allcurriculum[0].sims_cur_code;
                        $scope.getAcademicYear($scope.sims_cur_code);
                    }
                });
            }

            $scope.getAcademicYear = function (curCode) {
                var a = { opr: 'C', sims_cur_code: $scope.sims_cur_code}
                $http.post(ENV.apiUrl + "api/GradeBookCopyYoY/GradeBookCopyYoYCommon", a).then(function (res) {
                    $scope.AllAcademicYear = res.data.table;
                    if ($scope.AllAcademicYear.length > 0) {
                        $scope.sims_academic_year = $scope.AllAcademicYear[0].sims_academic_year;
                        $scope.ImportTerms();
                    }
                });
            };

            $scope.ImportTerms = function () {
                var a = { opr: 'A', sims_cur_code: $scope.sims_cur_code, sims_academic_year: $scope.sims_academic_year }
                $http.post(ENV.apiUrl + "api/GradeBookCopyYoY/GradeBookCopyYoYCommon", a).then(function (res) {
                    $scope.AllTerms = res.data.table;
                    if ($scope.AllTerms.length > 0) {
                        $scope.sims_term_code = $scope.AllTerms[0].sims_term_code;
                        $scope.ImportGrades();
                    }
                });
            }

            $scope.ImportGrades = function () {
                var a = { opr: 'B', sims_cur_code: $scope.sims_cur_code, sims_academic_year: $scope.sims_academic_year, sims_term_code: $scope.sims_term_code }
                $http.post(ENV.apiUrl + "api/GradeBookCopyYoY/GradeBookCopyYoYCommon", a).then(function (res) {
                    $scope.AllGrades = res.data.table;
                    setTimeout(function () {
                        $('#Select3').change(function () {
                        }).multipleSelect({
                            width: '100%'

                        });
                        $("#Select3").multipleSelect("checkAll");
                    }, 1000);
                });
            }

            $scope.readOnlyGrade = function () {
                if ($scope.temp.GradeList == 'O') {
                    $scope.flag = true;
                    $scope.temp.count = 1;
                }
                else
                    $scope.flag = false;
            }

            $scope.GradeBookCopyYoYData = function () {
                console.log("temp.GradeList Array = ", $scope.temp.GradeList.toString());
                var a = { opr: 'D', sims_cur_code: $scope.sims_cur_code, sims_academic_year: $scope.sims_academic_year, sims_term_code: $scope.sims_term_code, grade_list: $scope.temp.GradeList.toString() }
                console.log("object a = ", a);
                $http.post(ENV.apiUrl + "api/GradeBookCopyYoY/GradeBookCopyYoYCommon", a).then(function (res) {
                    var cnt = res.data.table.column1;
                    if (cnt > 0) {
                        swal({ text: cnt + "Gradebooks Successfully Copied", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    }
                    else {
                        swal({ text: "Gradebook Already Defined or source doesn't exist", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }
                });
            }

            $(document).ready(function () {
                $scope.getCurriculum();
                $scope.user = $rootScope.globals.currentUser.username;
                $scope.temp = {};
            });

        }]);
})();