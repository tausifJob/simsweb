﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Gradebook');

    simsController.controller('ScholasticEvalGroupMasterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.activityGroupDetails = [];
            $scope.finalGrade = [];
            $scope.AllGrades = [];
            $scope.gradeFromGroupCode = [];
            $scope.deleteGrade = [];

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.getGroupMasterDetails = function () {
                $http.get(ENV.apiUrl + "api/ScholasticGroupMaster/GetALLScholasticGroupMaster").then(function (response) {
                    $scope.groupMasterDetails = response.data;
                    $scope.totalItems = $scope.groupMasterDetails.length;
                    $scope.todos = $scope.groupMasterDetails;
                    $scope.makeTodos();
                });
            }
            $scope.getGroupMasterDetails();

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseFloat($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseFloat(begin) + parseFloat($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

            };

            $scope.edt = {};
            $scope.edt["sims_cur_code"] = '';
            $scope.edt["sims_academic_year"] = '';
            $scope.edt["sims_scholastic_evaluation_group_desc"] = '';
            $scope.edt["sims_scholastic_evaluation_group_status"] = false;
            $scope.disabledGrade = false;
            $scope.grid = true;
            $scope.display = false;

            $scope.New = function () {
                $scope.showGridGrade = false;
                $scope.finalGrade = [];
                $scope.AllGrades = [];
                $scope.gradeFromGroupCode = [];
                $scope.deleteGrade = [];
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.disabledGrade = false;
                $scope.disableCur = false;
                $scope.disableAcYear = false;
                //$scope.edt["sims_cur_code"] = '';
                $scope.edt["sims_academic_year"] = '';
                $scope.edt["sims_scholastic_evaluation_group_desc"] = '';
                $scope.edt["sims_scholastic_evaluation_group_status"] = false;
            }

            $scope.edit = function (str) {
                $scope.showGridGrade = true;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.disableCur = true;
                $scope.disableAcYear = true;
                $scope.edt = angular.copy(str);
                $scope.getacademicYear($scope.edt.sims_cur_code);
                $scope.getGradeFromGroupCode($scope.edt.sims_scholastic_evaluation_group_code);

                setTimeout(function () {
                    $scope.getGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                }, 500);

                $scope.edt = {
                    sims_cur_code: str.sims_cur_code,
                    sims_academic_year: str.sims_academic_year,
                    sims_scholastic_evaluation_group_desc: str.sims_scholastic_evaluation_group_desc,
                    sims_scholastic_evaluation_group_status: str.sims_scholastic_evaluation_group_status,
                    sims_scholastic_evaluation_group_code: str.sims_scholastic_evaluation_group_code
                }
                $scope.edt.sims_academic_year = str.sims_academic_year;

            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                // $scope.edt["sims_cur_code"] = '';
                $scope.edt["sims_academic_year"] = '';
                $scope.edt["sims_scholastic_evaluation_group_desc"] = '';
                $scope.edt["sims_scholastic_evaluation_group_status"] = false;
                //$scope.showGridGrade = false;
                //$scope.finalGrade = [];
                //$scope.AllGrades = [];
                //$scope.gradeFromGroupCode = [];                
            }
            $scope.getActivityGroupGrade = function () {
                $http.get(ENV.apiUrl + "api/ScholasticGroupMaster/GetScholasticMasterGrade").then(function (response) {
                    $scope.activityGroupDetails = response.data;
                });
            }
            $scope.getActivityGroupGrade();

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCurriculum) {
                $scope.Curriculum = getCurriculum.data;
                $scope.edt['sims_cur_code'] = $scope.Curriculum[0].sims_cur_code
                $scope.getacademicYear($scope.Curriculum[0].sims_cur_code);
            });

            $scope.getacademicYear = function (str) {
                $scope.AcademicYear = [];
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (getAcademicYear) {
                    $scope.AcademicYear = getAcademicYear.data;
                    // $scope.getGrade($scope.Curriculum[0].sims_cur_code, $scope.AcademicYear[0].sims_academic_year);
                });
            }

            $scope.getGradeFromGroupCode = function (groupCode) {
                $scope.finalGrade = [];
                var param = {}
                param.sims_scholastic_evaluation_group_code = groupCode;
                $http.post(ENV.apiUrl + "api/ScholasticGroupMaster/GradecodefromScholasticGradeDetails", param).then(function (res) {
                    $scope.gradeFromGroupCode = res.data;
                    if ($scope.gradeFromGroupCode.length > 0) {
                        for (var i = 0; i < $scope.gradeFromGroupCode.length; i++) {
                            $scope.finalGrade.push($scope.gradeFromGroupCode[i].sims_grade_code);
                        }
                    }
                });
            }

            $scope.getGrade = function (str1, str2) {
                $scope.showGridGrade = true;
                var param = {}

                param.sims_cur_code = str1;
                param.sims_academic_year = str2;
                $http.post(ENV.apiUrl + "api/ScholasticGroupMaster/GradeWith_Para", param).then(function (getAllGrades) {
                    $scope.AllGrades = getAllGrades.data;
                    if ($scope.gradeFromGroupCode.length > 0) {

                        for (var i = 0; i < $scope.AllGrades.length; i++) {
                            for (var j = 0; j < $scope.activityGroupDetails.length; j++) {

                                for (var k = 0; k < $scope.gradeFromGroupCode.length; k++) {

                                    if ($scope.AllGrades[i].sims_grade_code == $scope.activityGroupDetails[j].sims_grade_code && $scope.activityGroupDetails[j].sims_academic_year == $scope.edt.sims_academic_year) {
                                        $scope.AllGrades[i]['disable'] = true;
                                    }
                                    if ($scope.AllGrades[i].sims_grade_code == $scope.gradeFromGroupCode[k].sims_grade_code) {
                                        $scope.AllGrades[i]['disable'] = false;
                                        $scope.AllGrades[i]['gradeChekboxModel'] = true;
                                        //$scope.AllGrades[i]['check'] = true;

                                    }
                                }

                            }
                        }
                    }
                    else {
                        for (var i = 0; i < $scope.AllGrades.length; i++) {

                            for (var j = 0; j < $scope.activityGroupDetails.length; j++) {

                                if ($scope.AllGrades[i].sims_grade_code == $scope.activityGroupDetails[j].sims_grade_code && $scope.activityGroupDetails[j].sims_academic_year == $scope.edt.sims_academic_year) {
                                    $scope.AllGrades[i]['disable'] = true;
                                }
                            }
                        }
                    }

                });

            }




            $scope.selectGrade = function (grade) {
                var id = event.target.id;
                var value = event.target.value;;
                if (document.getElementById(id).checked == true) {
                    $scope.finalGrade.push(grade.sims_grade_code);
                }
                else {
                    var index = $scope.finalGrade.indexOf(value);
                    $scope.finalGrade.splice(index, 1);
                }
            }


            $scope.Save = function (Myform) {
                if (Myform) {
                    var data = [];

                    if ($scope.finalGrade.length == 0) {
                        swal({ text: 'Please Select At Least One Grade', imageUrl: "assets/img/notification-alert.png", width: 360, showCloseButton: true });
                        return false;
                    }

                    for (var i = 0; i < $scope.finalGrade.length; i++) {
                        var opr = {
                            sims_grade_code: $scope.finalGrade[i],
                            sims_academic_year: $scope.edt.sims_academic_year,
                            sims_scholastic_evaluation_group_desc: $scope.edt.sims_scholastic_evaluation_group_desc,
                            sims_scholastic_evaluation_group_status: $scope.edt.sims_scholastic_evaluation_group_status,
                            sims_cur_code: $scope.edt.sims_cur_code
                        };
                        data.push(opr);
                    }

                    $http.post(ENV.apiUrl + "api/ScholasticGroupMaster/AddScholasticGroupMaster?data1=" + JSON.stringify($scope.edt), data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                        $scope.getActivityGroupGrade();
                        $scope.getGroupMasterDetails();
                        if ($scope.msg1 == true) {
                            swal({ text: 'Scholastic Eval Group Added Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true, timer: 5000 });
                            $scope.display = false;
                            $scope.grid = true;
                            $scope.finalGrade = [];
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: 'Scholastic Eval Group Not Added. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true, timer: 5000 });
                            $scope.display = false;
                            $scope.grid = true;
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.Update = function (Myform) {
                if (Myform) {
                    var data = [];
                    if ($scope.finalGrade.length == 0) {
                        swal({
                            text: 'Please Select At Least One Grade', imageUrl: "assets/img/notification-alert.png",width: 360, showCloseButton: true
                        });
                        return false;
                    }

                    for (var i = 0; i < $scope.finalGrade.length; i++) {
                        var opr = {
                            sims_grade_code: $scope.finalGrade[i],
                            sims_academic_year: $scope.edt.sims_academic_year,
                            sims_scholastic_evaluation_group_desc: $scope.edt.sims_scholastic_evaluation_group_desc,
                            sims_scholastic_evaluation_group_status: $scope.edt.sims_scholastic_evaluation_group_status,
                            sims_cur_code: $scope.edt.sims_cur_code,
                            sims_scholastic_evaluation_group_code: $scope.edt.sims_scholastic_evaluation_group_code
                        };
                        data.push(opr);
                    }

                    $http.post(ENV.apiUrl + "api/ScholasticGroupMaster/UpdateScholasticGroupMaster?data1=" + JSON.stringify($scope.edt), data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                        $scope.getActivityGroupGrade();
                        $scope.getGroupMasterDetails();
                        if ($scope.msg1 == true) {
                            swal({ text: 'Scholastic Eval Group Updated Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true, timer: 5000 });
                            $scope.display = false;
                            $scope.grid = true;
                            $scope.finalGrade = [];
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: 'Scholastic Eval Group Not Updated. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true, timer: 5000 });
                            $scope.display = false;
                            $scope.grid = true;
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }


            $scope.size = function (str) {

                if (str == 'All') {

                    $scope.totalItems = $scope.groupMasterDetails.length;
                    $scope.todos = $scope.groupMasterDetails;
                    $scope.filteredTodos = $scope.GetAllStudentFee;
                    $scope.numPerPage = $scope.groupMasterDetails.length;
                    $scope.maxSize = $scope.groupMasterDetails.length
                    $scope.makeTodos();
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.groupMasterDetails, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.groupMasterDetails;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in 2 fields */
                return (item.sims_scholastic_evaluation_group_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.alertMsg = function () {
                swal({ text: 'Can Not Edit Scholastic Evaluation Group', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true, timer: 5000 });
            };

        }])
})();