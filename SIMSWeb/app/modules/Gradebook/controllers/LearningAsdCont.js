﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Student');
    simsController.controller('LearningAsdCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var user = $rootScope.globals.currentUser.username;
            var username = $rootScope.globals.currentUser.username;
            
            $scope.temp = [];

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            //$scope.att_date = mm + '/' + dd + '/' + yyyy;
            $scope.temp['att_date'] = ('0' + dd).slice(-2) + '-' + ('0' + (mm)).slice(-2) + '-' + yyyy;


            $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                $scope.curiculum = res.data;
                $scope.temp['cur_code'] = $scope.curiculum[0].sims_cur_code;

                $scope.cur_change();
            });



            $scope.cur_change = function () {

                $http.get(ENV.apiUrl + "api/attendance/getAttendanceyear?cur_code=" + $scope.temp.cur_code).then(function (academicyears) {
                    $scope.academicyears = academicyears.data;
                    $scope.temp['acdm_yr'] = $scope.academicyears[0].sims_academic_year;

                    $scope.acdm_yr_change();
                });

               
            }

            $scope.acdm_yr_change = function () {
                
                $http.get(ENV.apiUrl + "api/LearningAsd/getgrade?cur_code=" + $scope.temp.cur_code + "&academic_year=" + $scope.temp.acdm_yr + "&userName=" + username).then(function (grades) {
                    $scope.grades = grades.data;
                    $http.get(ENV.apiUrl + "api/LearningAsd/getSubject?cur=" + $scope.temp.cur_code + "&academic_year=" + $scope.temp.acdm_yr + "&grade=" + '' + "&section=" + '' + "&user_name=" + username + "&date=" + $scope.temp.att_date).then(function (res) {
                        $scope.subjectlst = res.data;
                    });
                    $scope.grade_change();
                });

               

                //$http.get(ENV.apiUrl + "api/LearningAsd/getSlot?cur=" + $scope.temp.cur_code + "&academic_year=" + $scope.temp.acdm_yr + "&grade=" + $scope.temp.grade_code + "&section=" + $scope.temp.sec_code + "&sims_teacher_code=" + username).then(function (allSectionStudent) {

                //    $scope.attSlot = allSectionStudent.data;
                //});

            }

            $scope.grade_change = function () {
             
                $http.get(ENV.apiUrl + "api/LearningAsd/getsection?cur_code=" + $scope.temp.cur_code + "&academic_year=" + $scope.temp.acdm_yr + "&gradeCode=" + $scope.temp.grade_code + "&userName=" + username).then(function (section) {
                    $scope.section = section.data;
                    // $scope.sec_change();
                });
            }

            $scope.subject_change = function () {
                if ($scope.temp.grade_code == undefined) { $scope.temp['grade_code'] = '' }
                if ($scope.temp.sec_code == undefined) { $scope.temp['sec_code'] = '' }

                $http.get(ENV.apiUrl + "api/LearningAsd/getEmployee?cur=" + $scope.temp.cur_code + "&academic_year=" + $scope.temp.acdm_yr + "&grade=" + $scope.temp.grade_code + "&section=" + $scope.temp.sec_code + "&user_name=" + username + "&subject=" + $scope.temp.subject_code).then(function (allSectionStudent) {

                    $scope.attSlot = allSectionStudent.data;
                });
            }

            $scope.getSubject = function () {
                if ($scope.temp.grade_code == undefined) { $scope.temp['grade_code'] = '' }
                if ($scope.temp.sec_code == undefined) { $scope.temp['sec_code'] = '' }
                $http.get(ENV.apiUrl + "api/LearningAsd/getSubject?cur=" + $scope.temp.cur_code + "&academic_year=" + $scope.temp.acdm_yr + "&grade=" + $scope.temp.grade_code + "&section=" + $scope.temp.sec_code + "&user_name=" + username + "&date=" + $scope.temp.att_date).then(function (res) {
                    $scope.subjectlst = res.data;
                });
            }

           


            $http.get(ENV.apiUrl + "api/LearningAsd/getTemplate").then(function (res) {
                $scope.templatelst = res.data;
                $scope.temp['temp_id'] = $scope.templatelst[0].sims_comment_template_id;

            });


            $scope.getdata = function (str) {

                if (str) {

                    $http.get(ENV.apiUrl + "api/LearningAsd/getTemplateDetails?cur=" + $scope.temp.cur_code + "&academic=" + $scope.temp.acdm_yr + "&temp_id=" + $scope.temp.temp_id + "&grade=" + $scope.temp.grade_code + "&sec=" + $scope.temp.sec_code + "&sub=" + $scope.temp.subject_code + "&dt=" + $scope.temp.att_date + "&slot=" + $scope.temp.sims_lesson_created_for).then(function (res) {

                        $scope.templateDetaillst = res.data;
                        setTimeout(function () {
                            for (var i = 0; i < $scope.templateDetaillst.length; i++) {
                                if ($scope.templateDetaillst[i].sims_value_type == 'R') {
                                    var t = '#' + $scope.templateDetaillst[i].sims_comment_code + 'rich';
                                    $(t).wysihtml5();
                                    //var v = document.getElementById(t);
                                    //v.value = $scope.templateDetaillst[i].desc;
                                  
                                    $(t).data("wysihtml5").editor.setValue($scope.templateDetaillst[i].desc);
                                }

                             
                            }
                        }, 2000);
                    });

                    //for (var i = 0; i < $scope.templateDetaillst.length; i++) {
                    //    if ($scope.templateDetaillst[i].sims_value_type == 'D') {
                    //        $scope.templateDetaillst[i]['desc'] = $scope.templateDetaillst[i].sims_drop_down_display_code;

                    //    }
                    //}
                }
            }


            $http.get(ENV.apiUrl + "api/LearningAsd/getDropDown").then(function (res) {
                $scope.drop_lst = res.data;
            });


            $scope.drop_down_change = function (str) {
                debugger
                //var sd = "#" + str.sims_comment_code + "drop" + " option:selected"
                //str['display_text_short'] = $(sd).text();

                for (var i = 0; i <$scope.drop_lst.length; i++) {
                    if (str.desc == $scope.drop_lst[i].sims_comment_code) {
                        str['display_text'] = $scope.drop_lst[i].sims_header_desc;
                        str['display_text_short'] = $scope.drop_lst[i].sims_drop_down_display_text;
                        break;
                    }
                }
            }

            $scope.Submit_btn_click = function () {

                var t = '';

                for (var i = 0; i < $scope.templateDetaillst.length; i++) {
                    if ($scope.templateDetaillst[i].desc == undefined)
                        $scope.templateDetaillst[i].desc = '';
                    if ($scope.templateDetaillst[i].sims_value_type == 'C') {
                        if ($scope.templateDetaillst[i].sims_comment_code_value) {
                            $scope.templateDetaillst[i].desc='1'
                        }
                    }
                    if ($scope.templateDetaillst[i].sims_value_type == 'R') {
                        var id = '#' + $scope.templateDetaillst[i].sims_comment_code + 'rich';
                       
                        $scope.templateDetaillst[i].desc = $(id).val();
                    }
                    if ($scope.templateDetaillst[i].desc!='')
                    t = t+$scope.templateDetaillst[i].sims_comment_code + '-' + $scope.templateDetaillst[i].desc+ ',';
                }

                var data = {
                    sims_cur_code: $scope.temp.cur_code,
                    sims_academic_year:$scope.temp.acdm_yr,
                    sims_grade_code:$scope.temp.grade_code,
                    sims_section_code:$scope.temp.sec_code,
                    sims_lesson_subject_code: $scope.temp.subject_code,
                    sims_lesson_id:$scope.templateDetaillst[0]['sims_lesson_id'],
                    sims_lesson_date: $scope.temp.att_date   ,
                    sims_lesson_period_no: $scope.temp.slot_code,
                    sims_lesson_remark: $scope.temp.comment,
                    user_name: username,
                    sims_comment_description: t,
                    sims_comment_template_id: $scope.temp.temp_id,
                    sims_lesson_created_for: $scope.temp.sims_lesson_created_for,
                    sims_lesson_date_time:  $scope.temp.att_date_time
                }

                $http.post(ENV.apiUrl + "api/LearningAsd/CUDLearning", data).then(function (res) {
                    $scope.ins = res.data;
                    if (res.data) {

                        swal({ text: "Data Inserted Successfully.", imageUrl: "assets/img/check.png", });

                    }
                });

            }


            $scope.print = function () {
                debugger;
                $scope.colsvis = true;
                $scope.show_report_header = true;
                $scope.pacad = $('select[name=cmb_academic] option:selected').text();
                $scope.pclass = $('select[name=cmb_grade] option:selected').text() + '-' + $('select[name=cmb_section] option:selected').text();
                $scope.psubject = $('select[name=cmb_subject] option:selected').text();
                $scope.pPeriod = $('select[name=cmb_slot] option:selected').text();


                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        debugger
                       

                        //var printContents = document.getElementById('printable').innerHTML;
                        //var originalContents = document.body.innerHTML;
                        //document.body.innerHTML = printContents;
                        //window.print();
                        //document.body.innerHTML = originalContents;

                      
                        var docHead = document.head.outerHTML;
                        var printContents = document.getElementById('printable').outerHTML;
                        var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

                        var newWin = window.open("", "_blank", winAttr);
                        var writeDoc = newWin.document;
                        writeDoc.open();
                        writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                        writeDoc.close();
                        newWin.focus();

                        //$("#printable").print();
                    

                    }
                });
            };





         }])
})();