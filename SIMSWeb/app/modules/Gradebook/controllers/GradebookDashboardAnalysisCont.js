﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('GradebookDashboardAnalysisCont',
       ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV',
           function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

               $scope.modalGraphId = '';
               $scope.temp = [];

               $scope.graphbtn = false;
             


               $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res1) {
                   $scope.cur_data = res1.data;
                   $scope.temp.sims_cur_code = $scope.cur_data[0].sims_cur_code;
                   $scope.getAcademic_year($scope.cur_data[0].sims_cur_code);
               });

               $scope.getAcademic_year = function (cur_code1) {
                   debugger
                   $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code1).then(function (res1) {
                       $scope.acad_data = res1.data;
                       $scope.temp.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                       $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);

                   });
               }

               $scope.getGrade = function (curCode, acad_year) {
                   debugger;
                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getAllGrades?curCode=" + $scope.temp.sims_cur_code + "&acaYear=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                       $scope.Grade_code = Gradecode.data;
                       setTimeout(function () {
                           $('#cmb_grade_code').change(function () {
                               console.log($(this).val());
                           }).multipleSelect({
                               width: '100%',
                               filter: true
                           });
                       }, 1000);

                   });
               }

               $scope.getSection = function (curCode, academic_year, grade_code) {
                   debugger;
                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getSectionFromGrade?curCode=" + $scope.temp.sims_cur_code + "&acaYear=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code).then(function (Sectioncode) {
                       $scope.Section_code = Sectioncode.data;
                       setTimeout(function () {
                           $('#cmb_section_code').change(function () {
                               console.log($(this).val());
                           }).multipleSelect({
                               width: '100%',
                               filter: true
                           });
                           //$("#cmb_section_code").multipleSelect("checkAll");
                       }, 1000);
                   });
               }

               $scope.getAllTerm = function (curCode, acaYear) {
                   debugger
                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getAllTerm?curCode=" + $scope.temp.sims_cur_code + "&acaYear=" + $scope.temp.sims_academic_year).then(function (res1) {
                       $scope.term_code = res1.data;
                       setTimeout(function () {
                           $('#term_box').change(function () {
                               debugger
                           })

                               .multipleSelect({
                                   width: '100%'
                               });
                           //$("#show_box").multipleSelect("checkAll");
                       }, 1000);
                   });
               }

               $scope.getsubjectdata = function (cur_code, academic_year, grade_code, section, group) {
                   debugger;
                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getsubjectdata?curCode=" + $scope.temp.sims_cur_code + "&acaYear=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&group=" + $scope.temp.sims_subject_group_code).then(function (Sectioncode) {
                       $scope.subject = Sectioncode.data;
                       setTimeout(function () {
                           $('#cmb_subject_code').change(function () {
                               console.log($(this).val());
                           }).multipleSelect({
                               width: '100%',
                               filter: true
                           });
                           //$("#cmb_subject_code").multipleSelect("checkAll");
                       }, 1000);
                   });
               }


               $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getAllnationality").then(function (res1) {
                   $scope.nationality_data = res1.data;
                   setTimeout(function () {
                       $('#cmb_nationality_code').change(function () {
                           console.log($(this).val());
                       }).multipleSelect({
                           width: '100%',
                           filter: true
                       });
                       //$("#cmb_subject_code").multipleSelect("checkAll");
                   }, 1000);
                  
               });

               $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getAllgender").then(function (res1) {
                   $scope.gender_data = res1.data;
                   setTimeout(function () {
                       $('#cmb_gender').change(function () {
                           console.log($(this).val());
                       }).multipleSelect({
                           width: '100%',
                           filter: true
                       });
                       //$("#cmb_subject_code").multipleSelect("checkAll");
                   }, 1000);
               });

               $scope.getgrouptdata = function (curCode) {
                   debugger;
                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getAllsubgroup?curCode=" + $scope.temp.sims_cur_code).then(function (res1) {
                       $scope.group = res1.data;
                       setTimeout(function () {
                           $('#cmb_group_code').change(function () {
                               console.log($(this).val());
                           }).multipleSelect({
                               width: '100%',
                               filter: true
                           });
                           //$("#cmb_subject_code").multipleSelect("checkAll");
                       }, 1000);
                   });
               }

               // 1. Current Year Gradebook//////////
               $scope.hideGraph = true;

              
               $scope.getDatafirstGraph = function (cur, year) {
                   $scope.graphbtn = false;
                   $("#searhbtn").css('display', 'block');
                   $("#searhbtn1").css('display', 'block');
                   $("#searhbtn2").css('display', 'block');
                   $scope.dash1M_recieved_lst = [];
                   $scope.dash1M_status_lst = [];
                   $scope.dash1M_sims_academic_yeart = [];
                   $scope.dash3M_recieved_lst = [];
                   $scope.dash3M_status_lst = [];
                   $scope.dash3M_sims_academic_yeart = [];
                   $scope.dash4M_recieved_lst = [];
                   $scope.dash4M_status_lst = [];
                   $scope.dash4M_sims_academic_yeart = [];

                   $scope.dash1term1_recieved_lst = [];
                   $scope.dash1term1_status_lst = [];
                   $scope.dash1term1_sims_academic_yeart = [];
                   $scope.dash3term1_recieved_lst = [];
                   $scope.dash3term1_status_lst = [];
                   $scope.dash3term1_sims_academic_yeart = [];
                   $scope.dash4term1_recieved_lst = [];
                   $scope.dash4term1_status_lst = [];
                   $scope.dash4term1_sims_academic_yeart = [];

                   $scope.dash1term2_recieved_lst = [];
                   $scope.dash1term2_status_lst = [];
                   $scope.dash1term2_sims_academic_yeart = [];
                   $scope.dash3term2_recieved_lst = [];
                   $scope.dash3term2_status_lst = [];
                   $scope.dash3term2_sims_academic_yeart = [];
                   $scope.dash4term2_recieved_lst = [];
                   $scope.dash4term2_status_lst = [];
                   $scope.dash4term2_sims_academic_yeart = [];

                   $scope.dash1term3_recieved_lst = [];
                   $scope.dash1term3_status_lst = [];
                   $scope.dash1term3_sims_academic_yeart = [];
                   $scope.dash3term3_recieved_lst = [];
                   $scope.dash3term3_status_lst = [];
                   $scope.dash3term3_sims_academic_yeart = [];
                   $scope.dash4term3_recieved_lst = [];
                   $scope.dash4term3_status_lst = [];
                   $scope.dash4term3_sims_academic_yeart = [];
                   
                   if (window.bar1 != undefined) {
                       window.bar1.destroy();
                   }

                   if (window.bar2 != undefined) {
                       window.bar2.destroy();
                   }

                   if (window.bar3 != undefined) {
                       window.bar3.destroy();
                   }
                   
                   if (window.bar1term1 != undefined) {
                       window.bar1term1.destroy();
                   }

                   if (window.bar2term1 != undefined) {
                       window.bar2term1.destroy();
                   }

                   if (window.bar3term1 != undefined) {
                       window.bar3term1.destroy();
                   }
                   
                   if (window.bar1term2 != undefined) {
                       window.bar1term2.destroy();
                   }

                   if (window.bar2term2 != undefined) {
                       window.bar2term2.destroy();
                   }

                   if (window.bar3term2 != undefined) {
                       window.bar3term2.destroy();
                   }

                   if (window.bar1term3 != undefined) {
                       window.bar1term3.destroy();
                   }

                   if (window.bar2term3 != undefined) {
                       window.bar2term3.destroy();
                   }

                   if (window.bar3term3 != undefined) {
                       window.bar3term3.destroy();
                   }
                  
                  
                   //current year
                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getgradebookcurrentdata?curCode=" + $scope.temp.sims_cur_code + "&acaYear=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&subject_code=" + $scope.temp.sims_subject_code + "&opr=" + 'E' + "&term_code=" + $scope.temp.sims_term_code
                     + "&nationality=" + $scope.temp.sims_nationality_code + "&gender=" + $scope.temp.sims_appl_parameter + "&sen=" + $scope.temp.sen_status).then(function (res) {
                       $scope.gradebookcurrent_m = res.data;

                       console.log($scope.gradebookcurrent_m);
                       for (var i = 0; $scope.gradebookcurrent_m.length > i; i++) {

                           if ($scope.gradebookcurrent_m[i].average == 0) {
                               $scope.errormsg = "Data Not Available";
                           } else {
                               $scope.dash1M_recieved_lst.push($scope.gradebookcurrent_m[i].average);
                               $scope.dash1M_status_lst.push($scope.gradebookcurrent_m[i].sims_mark_grade_name);
                               $scope.dash1M_sims_academic_yeart = ($scope.gradebookcurrent_m[0].sims_academic_year);
                               $scope.sims_academic_year1 = $scope.gradebookcurrent_m[0].sims_academic_year;


                           }
                       }


                       var ctx = document.getElementById("compBarChart3").getContext("2d");
                       var data = {
                           //labels: ["In Process", "Not Applicable"],
                           labels: $scope.dash1M_status_lst,
                           datasets: [{
                               data: $scope.dash1M_recieved_lst,
                               backgroundColor: ['#FF5A5E', '#5AD3D1', '#9467bd', '#2fc32f', '#b0dc0b', '#eab404', '#de672c', '#ec2e2e', '#d5429b', '#6f52b8', 
                                                 '#1c7cd5', '#56b9f7', '#0ae8eb','#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', 
                                                  '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000',
                                                  '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000']
                           }]
                       }


                       if ($scope.sims_academic_year1 != undefined) {
                           window.bar1 = new Chart(ctx, {
                               type: 'pie',
                               data: data,
                               options: {
                                   legend: {
                                       display: true,
                                       labels: {
                                           boxWidth: 10,
                                           fontSize: 10,
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: $scope.sims_academic_year1,
                                       position: 'bottom'
                                   }
                               }
                           });
                       }
                       else {

                           window.bar1 = new Chart(ctx, {
                               type: 'pie',
                               data: data,
                               options: {
                                   legend: {
                                       display: true,
                                       labels: {
                                           boxWidth: 10,
                                           fontSize: 10,
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: 'No Data',
                                       position: 'bottom'
                                   }
                               }
                           });
                       }
                   })

                   //current year-1
                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getgradebookoneprevdata?curCode=" + $scope.temp.sims_cur_code + "&acaYear=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&subject_code=" + $scope.temp.sims_subject_code + "&opr=" + 'F' + "&term_code=" + $scope.temp.sims_term_code
                        + "&nationality=" + $scope.temp.sims_nationality_code + "&gender=" + $scope.temp.sims_appl_parameter + "&sen=" + $scope.temp.sen_status).then(function (res) {
                       debugger;
                       $scope.gradebookoneyearprev_m = res.data;
                       console.log($scope.gradebookoneyearprev_m);
                       for (var i = 0; $scope.gradebookoneyearprev_m.length > i; i++) {

                           if ($scope.gradebookoneyearprev_m[i].average == 0) {
                               $scope.errormsg = "Data Not Available";
                           } else {
                               $scope.dash3M_recieved_lst.push($scope.gradebookoneyearprev_m[i].average);
                               $scope.dash3M_status_lst.push($scope.gradebookoneyearprev_m[i].sims_mark_grade_name);
                               $scope.dash3M_sims_academic_yeart.push($scope.gradebookoneyearprev_m[0].sims_academic_yeart);
                               $scope.sims_academic_year2 = $scope.gradebookoneyearprev_m[0].sims_academic_year;

                           }
                       }


                       var ctx = document.getElementById("compBarChart2").getContext("2d");
                       var data = {
                           //labels: ["In Process", "Not Applicable"],
                           labels: $scope.dash3M_status_lst,
                           datasets: [{
                               data: $scope.dash3M_recieved_lst,
                               backgroundColor: ['#FF5A5E', '#5AD3D1', '#9467bd', '#2fc32f', '#b0dc0b', '#eab404', '#de672c', '#ec2e2e', '#d5429b', '#6f52b8',
                                                  '#1c7cd5', '#56b9f7', '#0ae8eb', '#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0',
                                                   '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000',
                                                   '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000']
                           }]
                       }


                       if ($scope.sims_academic_year2 != undefined) {
                           window.bar2 = new Chart(ctx, {
                               type: 'pie',
                               data: data,
                               options: {
                                   legend: {
                                       display: true,
                                       labels: {
                                           boxWidth: 10,
                                           fontSize: 10,
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: $scope.sims_academic_year2,
                                       position: 'bottom'
                                   }
                               }
                           });
                       }
                       else {

                           window.bar2 = new Chart(ctx, {
                               type: 'pie',
                               data: data,
                               options: {
                                   legend: {
                                       display: true,
                                       labels: {
                                           boxWidth: 10,
                                           fontSize: 10,
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: 'No Data',
                                       position: 'bottom'
                                   }
                               }
                           });
                       }
                   })

                   //current year-2
                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getgradebooktwoprevdata?curCode=" + $scope.temp.sims_cur_code + "&acaYear=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&subject_code=" + $scope.temp.sims_subject_code + "&opr=" + 'G' + "&term_code=" + $scope.temp.sims_term_code
                        + "&nationality=" + $scope.temp.sims_nationality_code + "&gender=" + $scope.temp.sims_appl_parameter + "&sen=" + $scope.temp.sen_status).then(function (res) {
                       debugger;
                       $scope.gradebooktwoyearprev_m = res.data;
                       console.log($scope.gradebooktwoyearprev_m);
                       for (var i = 0; $scope.gradebooktwoyearprev_m.length > i; i++) {

                           if ($scope.gradebooktwoyearprev_m[i].average == 0) {
                               $scope.errormsg = "Data Not Available";
                           } else {
                               $scope.dash4M_recieved_lst.push($scope.gradebooktwoyearprev_m[i].average);
                               $scope.dash4M_status_lst.push($scope.gradebooktwoyearprev_m[i].sims_mark_grade_name);
                               $scope.dash4M_sims_academic_yeart.push($scope.gradebooktwoyearprev_m[0].sims_academic_yeart);
                               $scope.sims_academic_year3 = $scope.gradebooktwoyearprev_m[0].sims_academic_year;

                           }
                       }


                       var ctx = document.getElementById("compBarChart1O").getContext("2d");
                       var data = {
                           //labels: ["In Process", "Not Applicable"],
                           labels: $scope.dash4M_status_lst,
                           datasets: [{
                               data: $scope.dash4M_recieved_lst,
                               backgroundColor: ['#FF5A5E', '#5AD3D1', '#9467bd', '#2fc32f', '#b0dc0b', '#eab404', '#de672c', '#ec2e2e', '#d5429b', '#6f52b8',
                                                 '#1c7cd5', '#56b9f7', '#0ae8eb', '#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0',
                                                  '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000',
                                                  '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000']
                           }]
                       }

                       if ($scope.sims_academic_year3 != undefined) {
                           window.bar3 = new Chart(ctx, {
                               type: 'pie',
                               data: data,
                               options: {
                                   legend: {
                                       display: true,
                                       labels: {
                                           boxWidth: 10,
                                           fontSize: 10,
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: $scope.sims_academic_year3,
                                       position: 'bottom'
                                   }
                               }
                           });
                       }
                       else {

                           window.bar3 = new Chart(ctx, {
                               type: 'pie',
                               data: data,
                               options: {
                                   legend: {
                                       display: true,
                                       labels: {
                                           boxWidth: 10,
                                           fontSize: 10,
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: 'No Data',
                                       position: 'bottom'
                                   }
                               }
                           });
                       }
                   })


                   //current year Term 1
                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getgradebookcurrentdataterm1?curCode=" + $scope.temp.sims_cur_code + "&acaYear=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&subject_code=" + $scope.temp.sims_subject_code + "&opr=" + 'O' + "&term_code=" + $scope.temp.sims_term_code
                     + "&nationality=" + $scope.temp.sims_nationality_code + "&gender=" + $scope.temp.sims_appl_parameter + "&sen=" + $scope.temp.sen_status).then(function (res) {
                         $scope.gradebookcurrentterm1 = res.data;

                         console.log($scope.gradebookcurrentterm1);
                         for (var i = 0; $scope.gradebookcurrentterm1.length > i; i++) {

                             if ($scope.gradebookcurrentterm1[i].average == 0) {
                                 $scope.errormsg = "Data Not Available";
                             } else {
                                 $scope.dash1term1_recieved_lst.push($scope.gradebookcurrentterm1[i].average);
                                 $scope.dash1term1_status_lst.push($scope.gradebookcurrentterm1[i].sims_mark_grade_name);
                                 $scope.dash1term1_sims_academic_yeart = ($scope.gradebookcurrentterm1[0].sims_academic_year);
                                 $scope.sims_academic_year4term1 = $scope.gradebookcurrentterm1[0].sims_academic_year;


                             }
                         }


                         var ctx = document.getElementById("compBarChart3term1").getContext("2d");
                         var data = {
                             //labels: ["In Process", "Not Applicable"],
                             labels: $scope.dash1term1_status_lst,
                             datasets: [{
                                 data: $scope.dash1term1_recieved_lst,
                                 backgroundColor: ['#FF5A5E', '#5AD3D1', '#9467bd', '#2fc32f', '#b0dc0b', '#eab404', '#de672c', '#ec2e2e', '#d5429b', '#6f52b8',
                                                 '#1c7cd5', '#56b9f7', '#0ae8eb', '#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0',
                                                  '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000',
                                                  '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000']
                             }]
                         }


                         if ($scope.sims_academic_year4term1 != undefined) {
                             window.bar1term1 = new Chart(ctx, {
                                 type: 'pie',
                                 data: data,
                                 options: {
                                     legend: {
                                         display: true,
                                         labels: {
                                             boxWidth: 10,
                                             fontSize: 10,
                                         }
                                     },
                                     title: {
                                         display: true,
                                         text: $scope.sims_academic_year4term1,
                                         position: 'bottom'
                                     }
                                 }
                             });
                         }
                         else {

                             window.bar1term1 = new Chart(ctx, {
                                 type: 'pie',
                                 data: data,
                                 options: {
                                     legend: {
                                         display: true,
                                         labels: {
                                             boxWidth: 10,
                                             fontSize: 10,
                                         }
                                     },
                                     title: {
                                         display: true,
                                         text: 'No Data',
                                         position: 'bottom'
                                     }
                                 }
                             });
                         }
                     })

                   //current year-1 term 1
                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getgradebookoneprevdataterm1?curCode=" + $scope.temp.sims_cur_code + "&acaYear=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&subject_code=" + $scope.temp.sims_subject_code + "&opr=" + 'P' + "&term_code=" + $scope.temp.sims_term_code
                        + "&nationality=" + $scope.temp.sims_nationality_code + "&gender=" + $scope.temp.sims_appl_parameter + "&sen=" + $scope.temp.sen_status).then(function (res) {
                            debugger;
                            $scope.gradebookoneyearprevterm1 = res.data;
                            console.log($scope.gradebookoneyearprevterm1);
                            for (var i = 0; $scope.gradebookoneyearprevterm1.length > i; i++) {

                                if ($scope.gradebookoneyearprevterm1[i].average == 0) {
                                    $scope.errormsg = "Data Not Available";
                                } else {
                                    $scope.dash3term1_recieved_lst.push($scope.gradebookoneyearprevterm1[i].average);
                                    $scope.dash3term1_status_lst.push($scope.gradebookoneyearprevterm1[i].sims_mark_grade_name);
                                    $scope.dash3term1_sims_academic_yeart.push($scope.gradebookoneyearprevterm1[0].sims_academic_yeart);
                                    $scope.sims_academic_year5term1 = $scope.gradebookoneyearprevterm1[0].sims_academic_year;

                                }
                            }


                            var ctx = document.getElementById("compBarChart2term1").getContext("2d");
                            var data = {
                                //labels: ["In Process", "Not Applicable"],
                                labels: $scope.dash3term1_status_lst,
                                datasets: [{
                                    data: $scope.dash3term1_recieved_lst,
                                    backgroundColor: ['#FF5A5E', '#5AD3D1', '#9467bd', '#2fc32f', '#b0dc0b', '#eab404', '#de672c', '#ec2e2e', '#d5429b', '#6f52b8',
                                                 '#1c7cd5', '#56b9f7', '#0ae8eb', '#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0',
                                                  '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000',
                                                  '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000']
                                }]
                            }


                            if ($scope.sims_academic_year5term1 != undefined) {
                                window.bar2term1 = new Chart(ctx, {
                                    type: 'pie',
                                    data: data,
                                    options: {
                                        legend: {
                                            display: true,
                                            labels: {
                                                boxWidth: 10,
                                                fontSize: 10,
                                            }
                                        },
                                        title: {
                                            display: true,
                                            text: $scope.sims_academic_year5term1,
                                            position: 'bottom'
                                        }
                                    }
                                });
                            }
                            else {

                                window.bar2term1 = new Chart(ctx, {
                                    type: 'pie',
                                    data: data,
                                    options: {
                                        legend: {
                                            display: true,
                                            labels: {
                                                boxWidth: 10,
                                                fontSize: 10,
                                            }
                                        },
                                        title: {
                                            display: true,
                                            text: 'No Data',
                                            position: 'bottom'
                                        }
                                    }
                                });
                            }
                        })

                   //current year-2 term 1
                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getgradebooktwoprevdataterm1?curCode=" + $scope.temp.sims_cur_code + "&acaYear=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&subject_code=" + $scope.temp.sims_subject_code + "&opr=" + 'Q' + "&term_code=" + $scope.temp.sims_term_code
                       + "&nationality=" + $scope.temp.sims_nationality_code + "&gender=" + $scope.temp.sims_appl_parameter + "&sen=" + $scope.temp.sen_status).then(function (res) {
                           debugger;
                           $scope.gradebooktwoyearprevterm1 = res.data;
                           console.log($scope.gradebooktwoyearprevterm1);
                           for (var i = 0; $scope.gradebooktwoyearprevterm1.length > i; i++) {

                               if ($scope.gradebooktwoyearprevterm1[i].average == 0) {
                                   $scope.errormsg = "Data Not Available";
                               } else {
                                   $scope.dash4term1_recieved_lst.push($scope.gradebooktwoyearprevterm1[i].average);
                                   $scope.dash4term1_status_lst.push($scope.gradebooktwoyearprevterm1[i].sims_mark_grade_name);
                                   $scope.dash4term1_sims_academic_yeart.push($scope.gradebooktwoyearprevterm1[0].sims_academic_yeart);
                                   $scope.sims_academic_year6term1 = $scope.gradebooktwoyearprevterm1[0].sims_academic_year;

                               }
                           }


                           var ctx = document.getElementById("compBarChart1term1").getContext("2d");
                           var data = {
                               //labels: ["In Process", "Not Applicable"],
                               labels: $scope.dash4term1_status_lst,
                               datasets: [{
                                   data: $scope.dash4term1_recieved_lst,
                                   backgroundColor: ['#FF5A5E', '#5AD3D1', '#9467bd', '#2fc32f', '#b0dc0b', '#eab404', '#de672c', '#ec2e2e', '#d5429b', '#6f52b8',
                                                 '#1c7cd5', '#56b9f7', '#0ae8eb', '#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0',
                                                  '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000',
                                                  '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000']
                               }]
                           }

                           if ($scope.sims_academic_year6term1 != undefined) {
                               window.bar3term1 = new Chart(ctx, {
                                   type: 'pie',
                                   data: data,
                                   options: {
                                       legend: {
                                           display: true,
                                           labels: {
                                               boxWidth: 10,
                                               fontSize: 10,
                                           }
                                       },
                                       title: {
                                           display: true,
                                           text: $scope.sims_academic_year6term1,
                                           position: 'bottom'
                                       }
                                   }
                               });
                           }
                           else {

                               window.bar3term1 = new Chart(ctx, {
                                   type: 'pie',
                                   data: data,
                                   options: {
                                       legend: {
                                           display: true,
                                           labels: {
                                               boxWidth: 10,
                                               fontSize: 10,
                                           }
                                       },
                                       title: {
                                           display: true,
                                           text: 'No Data',
                                           position: 'bottom'
                                       }
                                   }
                               });
                           }
                       })


                   //current year Term 2
                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getgradebookcurrentdataterm2?curCode=" + $scope.temp.sims_cur_code + "&acaYear=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&subject_code=" + $scope.temp.sims_subject_code + "&opr=" + 'R' + "&term_code=" + $scope.temp.sims_term_code
                     + "&nationality=" + $scope.temp.sims_nationality_code + "&gender=" + $scope.temp.sims_appl_parameter + "&sen=" + $scope.temp.sen_status).then(function (res) {
                         $scope.gradebookcurrentterm2 = res.data;

                         console.log($scope.gradebookcurrentterm2);
                         for (var i = 0; $scope.gradebookcurrentterm2.length > i; i++) {

                             if ($scope.gradebookcurrentterm2[i].average == 0) {
                                 $scope.errormsg = "Data Not Available";
                             } else {
                                 $scope.dash1term2_recieved_lst.push($scope.gradebookcurrentterm2[i].average);
                                 $scope.dash1term2_status_lst.push($scope.gradebookcurrentterm2[i].sims_mark_grade_name);
                                 $scope.dash1term2_sims_academic_yeart = ($scope.gradebookcurrentterm2[0].sims_academic_year);
                                 $scope.sims_academic_year7term2 = $scope.gradebookcurrentterm2[0].sims_academic_year;


                             }
                         }


                         var ctx = document.getElementById("compBarChart3term2").getContext("2d");
                         var data = {
                             //labels: ["In Process", "Not Applicable"],
                             labels: $scope.dash1term2_status_lst,
                             datasets: [{
                                 data: $scope.dash1term2_recieved_lst,
                                 backgroundColor: ['#FF5A5E', '#5AD3D1', '#9467bd', '#2fc32f', '#b0dc0b', '#eab404', '#de672c', '#ec2e2e', '#d5429b', '#6f52b8',
                                                 '#1c7cd5', '#56b9f7', '#0ae8eb', '#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0',
                                                  '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000',
                                                  '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000']
                             }]
                         }


                         if ($scope.sims_academic_year7term2 != undefined) {
                             window.bar1term2 = new Chart(ctx, {
                                 type: 'pie',
                                 data: data,
                                 options: {
                                     legend: {
                                         display: true,
                                         labels: {
                                             boxWidth: 10,
                                             fontSize: 10,
                                         }
                                     },
                                     title: {
                                         display: true,
                                         text: $scope.sims_academic_year7term2,
                                         position: 'bottom'
                                     }
                                 }
                             });
                         }
                         else {

                             window.bar1term2 = new Chart(ctx, {
                                 type: 'pie',
                                 data: data,
                                 options: {
                                     legend: {
                                         display: true,
                                         labels: {
                                             boxWidth: 10,
                                             fontSize: 10,
                                         }
                                     },
                                     title: {
                                         display: true,
                                         text: 'No Data',
                                         position: 'bottom'
                                     }
                                 }
                             });
                         }
                     })

                   //current year-1 term 2
                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getgradebookoneprevdataterm2?curCode=" + $scope.temp.sims_cur_code + "&acaYear=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&subject_code=" + $scope.temp.sims_subject_code + "&opr=" + 'S' + "&term_code=" + $scope.temp.sims_term_code
                        + "&nationality=" + $scope.temp.sims_nationality_code + "&gender=" + $scope.temp.sims_appl_parameter + "&sen=" + $scope.temp.sen_status).then(function (res) {
                            debugger;
                            $scope.gradebookoneyearprevterm2 = res.data;
                            console.log($scope.gradebookoneyearprevterm2);
                            for (var i = 0; $scope.gradebookoneyearprevterm2.length > i; i++) {

                                if ($scope.gradebookoneyearprevterm2[i].average == 0) {
                                    $scope.errormsg = "Data Not Available";
                                } else {
                                    $scope.dash3term2_recieved_lst.push($scope.gradebookoneyearprevterm2[i].average);
                                    $scope.dash3term2_status_lst.push($scope.gradebookoneyearprevterm2[i].sims_mark_grade_name);
                                    $scope.dash3term2_sims_academic_yeart.push($scope.gradebookoneyearprevterm2[0].sims_academic_yeart);
                                    $scope.sims_academic_year8term2 = $scope.gradebookoneyearprevterm2[0].sims_academic_year;

                                }
                            }


                            var ctx = document.getElementById("compBarChart2term2").getContext("2d");
                            var data = {
                                //labels: ["In Process", "Not Applicable"],
                                labels: $scope.dash3term2_status_lst,
                                datasets: [{
                                    data: $scope.dash3term2_recieved_lst,
                                    backgroundColor: ['#FF5A5E', '#5AD3D1', '#9467bd', '#2fc32f', '#b0dc0b', '#eab404', '#de672c', '#ec2e2e', '#d5429b', '#6f52b8',
                                                 '#1c7cd5', '#56b9f7', '#0ae8eb', '#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0',
                                                  '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000',
                                                  '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000']
                                }]
                            }


                            if ($scope.sims_academic_year8term2 != undefined) {
                                window.bar2term2 = new Chart(ctx, {
                                    type: 'pie',
                                    data: data,
                                    options: {
                                        legend: {
                                            display: true,
                                            labels: {
                                                boxWidth: 10,
                                                fontSize: 10,
                                            }
                                        },
                                        title: {
                                            display: true,
                                            text: $scope.sims_academic_year8term2,
                                            position: 'bottom'
                                        }
                                    }
                                });
                            }
                            else {

                                window.bar2term2 = new Chart(ctx, {
                                    type: 'pie',
                                    data: data,
                                    options: {
                                        legend: {
                                            display: true,
                                            labels: {
                                                boxWidth: 10,
                                                fontSize: 10,
                                            }
                                        },
                                        title: {
                                            display: true,
                                            text: 'No Data',
                                            position: 'bottom'
                                        }
                                    }
                                });
                            }
                        })

                   //current year-2 term 2
                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getgradebooktwoprevdataterm2?curCode=" + $scope.temp.sims_cur_code + "&acaYear=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&subject_code=" + $scope.temp.sims_subject_code + "&opr=" + 'T' + "&term_code=" + $scope.temp.sims_term_code
                       + "&nationality=" + $scope.temp.sims_nationality_code + "&gender=" + $scope.temp.sims_appl_parameter + "&sen=" + $scope.temp.sen_status).then(function (res) {
                           debugger;
                           $scope.gradebooktwoyearprevterm2 = res.data;
                           console.log($scope.gradebooktwoyearprevterm2);
                           for (var i = 0; $scope.gradebooktwoyearprevterm2.length > i; i++) {

                               if ($scope.gradebooktwoyearprevterm2[i].average == 0) {
                                   $scope.errormsg = "Data Not Available";
                               } else {
                                   $scope.dash4term2_recieved_lst.push($scope.gradebooktwoyearprevterm2[i].average);
                                   $scope.dash4term2_status_lst.push($scope.gradebooktwoyearprevterm2[i].sims_mark_grade_name);
                                   $scope.dash4term2_sims_academic_yeart.push($scope.gradebooktwoyearprevterm2[0].sims_academic_yeart);
                                   $scope.sims_academic_year9term2 = $scope.gradebooktwoyearprevterm2[0].sims_academic_year;

                               }
                           }


                           var ctx = document.getElementById("compBarChart1term2").getContext("2d");
                           var data = {
                               //labels: ["In Process", "Not Applicable"],
                               labels: $scope.dash4term2_status_lst,
                               datasets: [{
                                   data: $scope.dash4term2_recieved_lst,
                                   backgroundColor: ['#FF5A5E', '#5AD3D1', '#9467bd', '#2fc32f', '#b0dc0b', '#eab404', '#de672c', '#ec2e2e', '#d5429b', '#6f52b8',
                                                 '#1c7cd5', '#56b9f7', '#0ae8eb', '#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0',
                                                  '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000',
                                                  '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000']
                               }]
                           }

                           if ($scope.sims_academic_year9term2 != undefined) {
                               window.bar3term2 = new Chart(ctx, {
                                   type: 'pie',
                                   data: data,
                                   options: {
                                       legend: {
                                           display: true,
                                           labels: {
                                               boxWidth: 10,
                                               fontSize: 10,
                                           }
                                       },
                                       title: {
                                           display: true,
                                           text: $scope.sims_academic_year9term2,
                                           position: 'bottom'
                                       }
                                   }
                               });
                           }
                           else {

                               window.bar3term2 = new Chart(ctx, {
                                   type: 'pie',
                                   data: data,
                                   options: {
                                       legend: {
                                           display: true,
                                           labels: {
                                               boxWidth: 10,
                                               fontSize: 10,
                                           }
                                       },
                                       title: {
                                           display: true,
                                           text: 'No Data',
                                           position: 'bottom'
                                       }
                                   }
                               });
                           }
                       })


                   //current year Term 3
                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getgradebookcurrentdataterm3?curCode=" + $scope.temp.sims_cur_code + "&acaYear=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&subject_code=" + $scope.temp.sims_subject_code + "&opr=" + 'U' + "&term_code=" + $scope.temp.sims_term_code
                     + "&nationality=" + $scope.temp.sims_nationality_code + "&gender=" + $scope.temp.sims_appl_parameter + "&sen=" + $scope.temp.sen_status).then(function (res) {
                         $scope.gradebookcurrentterm3 = res.data;

                         console.log($scope.gradebookcurrentterm3);
                         for (var i = 0; $scope.gradebookcurrentterm3.length > i; i++) {

                             if ($scope.gradebookcurrentterm3[i].average == 0) {
                                 $scope.errormsg = "Data Not Available";
                             } else {
                                 $scope.dash1term3_recieved_lst.push($scope.gradebookcurrentterm3[i].average);
                                 $scope.dash1term3_status_lst.push($scope.gradebookcurrentterm3[i].sims_mark_grade_name);
                                 $scope.dash1term3_sims_academic_yeart = ($scope.gradebookcurrentterm3[0].sims_academic_year);
                                 $scope.sims_academic_year7term3 = $scope.gradebookcurrentterm3[0].sims_academic_year;


                             }
                         }


                         var ctx = document.getElementById("compBarChart3term3").getContext("2d");
                         var data = {
                             //labels: ["In Process", "Not Applicable"],
                             labels: $scope.dash1term3_status_lst,
                             datasets: [{
                                 data: $scope.dash1term3_recieved_lst,
                                 backgroundColor: ['#FF5A5E', '#5AD3D1', '#9467bd', '#2fc32f', '#b0dc0b', '#eab404', '#de672c', '#ec2e2e', '#d5429b', '#6f52b8',
                                                 '#1c7cd5', '#56b9f7', '#0ae8eb', '#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0',
                                                  '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000',
                                                  '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000']
                             }]
                         }


                         if ($scope.sims_academic_year7term3 != undefined) {
                             window.bar1term3 = new Chart(ctx, {
                                 type: 'pie',
                                 data: data,
                                 options: {
                                     legend: {
                                         display: true,
                                         labels: {
                                             boxWidth: 10,
                                             fontSize: 10,
                                         }
                                     },
                                     title: {
                                         display: true,
                                         text: $scope.sims_academic_year7term3,
                                         position: 'bottom'
                                     }
                                 }
                             });
                         }
                         else {

                             window.bar1term3 = new Chart(ctx, {
                                 type: 'pie',
                                 data: data,
                                 options: {
                                     legend: {
                                         display: true,
                                         labels: {
                                             boxWidth: 10,
                                             fontSize: 10,
                                         }
                                     },
                                     title: {
                                         display: true,
                                         text: 'No Data',
                                         position: 'bottom'
                                     }
                                 }
                             });
                         }
                     })

                   //current year-1 term 3
                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getgradebookoneprevdataterm3?curCode=" + $scope.temp.sims_cur_code + "&acaYear=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&subject_code=" + $scope.temp.sims_subject_code + "&opr=" + 'V' + "&term_code=" + $scope.temp.sims_term_code
                        + "&nationality=" + $scope.temp.sims_nationality_code + "&gender=" + $scope.temp.sims_appl_parameter + "&sen=" + $scope.temp.sen_status).then(function (res) {
                            debugger;
                            $scope.gradebookoneyearprevterm3 = res.data;
                            console.log($scope.gradebookoneyearprevterm3);
                            for (var i = 0; $scope.gradebookoneyearprevterm3.length > i; i++) {

                                if ($scope.gradebookoneyearprevterm3[i].average == 0) {
                                    $scope.errormsg = "Data Not Available";
                                } else {
                                    $scope.dash3term3_recieved_lst.push($scope.gradebookoneyearprevterm3[i].average);
                                    $scope.dash3term3_status_lst.push($scope.gradebookoneyearprevterm3[i].sims_mark_grade_name);
                                    $scope.dash3term3_sims_academic_yeart.push($scope.gradebookoneyearprevterm3[0].sims_academic_yeart);
                                    $scope.sims_academic_year8term3 = $scope.gradebookoneyearprevterm3[0].sims_academic_year;

                                }
                            }


                            var ctx = document.getElementById("compBarChart2term3").getContext("2d");
                            var data = {
                                //labels: ["In Process", "Not Applicable"],
                                labels: $scope.dash3term3_status_lst,
                                datasets: [{
                                    data: $scope.dash3term3_recieved_lst,
                                    backgroundColor: ['#FF5A5E', '#5AD3D1', '#9467bd', '#2fc32f', '#b0dc0b', '#eab404', '#de672c', '#ec2e2e', '#d5429b', '#6f52b8',
                                                 '#1c7cd5', '#56b9f7', '#0ae8eb', '#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0',
                                                  '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000',
                                                  '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000']
                                }]
                            }


                            if ($scope.sims_academic_year8term3 != undefined) {
                                window.bar2term3 = new Chart(ctx, {
                                    type: 'pie',
                                    data: data,
                                    options: {
                                        legend: {
                                            display: true,
                                            labels: {
                                                boxWidth: 10,
                                                fontSize: 10,
                                            }
                                        },
                                        title: {
                                            display: true,
                                            text: $scope.sims_academic_year8term3,
                                            position: 'bottom'
                                        }
                                    }
                                });
                            }
                            else {

                                window.bar2term3 = new Chart(ctx, {
                                    type: 'pie',
                                    data: data,
                                    options: {
                                        legend: {
                                            display: true,
                                            labels: {
                                                boxWidth: 10,
                                                fontSize: 10,
                                            }
                                        },
                                        title: {
                                            display: true,
                                            text: 'No Data',
                                            position: 'bottom'
                                        }
                                    }
                                });
                            }
                        })

                   //current year-2 term 3
                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getgradebooktwoprevdataterm3?curCode=" + $scope.temp.sims_cur_code + "&acaYear=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&subject_code=" + $scope.temp.sims_subject_code + "&opr=" + 'W' + "&term_code=" + $scope.temp.sims_term_code
                       + "&nationality=" + $scope.temp.sims_nationality_code + "&gender=" + $scope.temp.sims_appl_parameter + "&sen=" + $scope.temp.sen_status).then(function (res) {
                           debugger;
                           $scope.gradebooktwoyearprevterm3 = res.data;
                           console.log($scope.gradebooktwoyearprevterm3);
                           for (var i = 0; $scope.gradebooktwoyearprevterm3.length > i; i++) {

                               if ($scope.gradebooktwoyearprevterm3[i].average == 0) {
                                   $scope.errormsg = "Data Not Available";
                               } else {
                                   $scope.dash4term3_recieved_lst.push($scope.gradebooktwoyearprevterm3[i].average);
                                   $scope.dash4term3_status_lst.push($scope.gradebooktwoyearprevterm3[i].sims_mark_grade_name);
                                   $scope.dash4term3_sims_academic_yeart.push($scope.gradebooktwoyearprevterm3[0].sims_academic_yeart);
                                   $scope.sims_academic_year9term3 = $scope.gradebooktwoyearprevterm3[0].sims_academic_year;

                               }
                           }


                           var ctx = document.getElementById("compBarChart1term3").getContext("2d");
                           var data = {
                               //labels: ["In Process", "Not Applicable"],
                               labels: $scope.dash4term3_status_lst,
                               datasets: [{
                                   data: $scope.dash4term3_recieved_lst,
                                   backgroundColor: ['#FF5A5E', '#5AD3D1', '#9467bd', '#2fc32f', '#b0dc0b', '#eab404', '#de672c', '#ec2e2e', '#d5429b', '#6f52b8',
                                                 '#1c7cd5', '#56b9f7', '#0ae8eb', '#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0',
                                                  '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000',
                                                  '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000']
                               }]
                           }

                           if ($scope.sims_academic_year9term3 != undefined) {
                               window.bar3term3 = new Chart(ctx, {
                                   type: 'pie',
                                   data: data,
                                   options: {
                                       legend: {
                                           display: true,
                                           labels: {
                                               boxWidth: 10,
                                               fontSize: 10,
                                           }
                                       },
                                       title: {
                                           display: true,
                                           text: $scope.sims_academic_year9term3,
                                           position: 'bottom'
                                       }
                                   }
                               });
                           }
                           else {

                               window.bar3term3 = new Chart(ctx, {
                                   type: 'pie',
                                   data: data,
                                   options: {
                                       legend: {
                                           display: true,
                                           labels: {
                                               boxWidth: 10,
                                               fontSize: 10,
                                           }
                                       },
                                       title: {
                                           display: true,
                                           text: 'No Data',
                                           position: 'bottom'
                                       }
                                   }
                               });
                           }
                       })



               }


               $scope.showcompBarChartModal = function () {
                   if (window.bar3 != undefined) {
                       window.bar3.destroy();
                   }
                   if (window.bar4 != undefined) {
                       window.bar4.destroy();
                   }
                   $scope.dash1_recieved_lst = [];
                   $scope.dash1_status_lst = [];
                   $scope.dash1_subject_lst = [];
                   $('#firstgraphModal').modal({ backdrop: 'static', keyboard: false });


                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getgradebookcurrentdata?curCode=" + $scope.temp.sims_cur_code + "&acaYear=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&subject_code=" + $scope.temp.sims_subject_code + "&opr=" + 'E' + "&term_code=" + $scope.temp.sims_term_code
                        + "&nationality=" + $scope.temp.sims_nationality_code + "&gender=" + $scope.temp.sims_appl_parameter + "&sen=" + $scope.temp.sen_status).then(function (res) {
                       $scope.gradebookcurrent_c = res.data;

                       console.log($scope.gradebookcurrent_c);
                       for (var i = 0; $scope.gradebookcurrent_c.length > i; i++) {
                         
                           if ($scope.gradebookcurrent_c[i].average == 0) {
                               $scope.errormsg = "Data Not Available";
                           } else {
                               $scope.dash1_recieved_lst.push($scope.gradebookcurrent_c[i].average);
                               $scope.dash1_status_lst.push($scope.gradebookcurrent_c[i].sims_mark_grade_name);
                               $scope.sims_academic_year1 = $scope.gradebookcurrent_c[0].sims_academic_year;
       
                           }
                       }
                  

                       var ctx = document.getElementById("compBarChart4").getContext("2d");
                       var data = {
                           //labels: ["In Process", "Not Applicable"],
                           labels: $scope.dash1_status_lst,
                           datasets: [{
                               data: $scope.dash1_recieved_lst,
                               backgroundColor: ['#FF5A5E', '#5AD3D1', '#9467bd', '#2fc32f', '#b0dc0b', '#eab404', '#de672c', '#ec2e2e', '#d5429b', '#6f52b8',
                                                 '#1c7cd5', '#56b9f7', '#0ae8eb', '#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0',
                                                  '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000',
                                                  '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000']
                           }]
                       }


                       if ($scope.sims_academic_year1 != undefined) {
                           window.bar4 = new Chart(ctx, {
                               type: 'pie',
                               data: data,
                               options: {
                                   legend: {
                                       display: true,
                                       labels: {
                                           boxWidth: 10,
                                           fontSize: 10,
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: $scope.sims_academic_year1,
                                       position: 'bottom'
                                   }
                               }
                           });
                       }
                       else {

                           window.bar4 = new Chart(ctx, {
                               type: 'pie',
                               data: data,
                               options: {
                                   legend: {
                                       display: true,
                                       labels: {
                                           boxWidth: 10,
                                           fontSize: 10,
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: 'No Data',
                                       position: 'bottom'
                                   }
                               }
                           });
                       }
                   })
               }

               $scope.showcompBarChartModal1 = function () {
                   if (window.bar3 != undefined) {
                       window.bar3.destroy();
                   }
                   if (window.bar5 != undefined) {
                       window.bar5.destroy();
                   }
                   $scope.dash3_recieved_lst = [];
                   $scope.dash3_status_lst = [];
                   $scope.dash3_subject_lst = [];
                   $('#firstgraphModal1').modal({ backdrop: 'static', keyboard: false });


                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getgradebookoneprevdata?curCode=" + $scope.temp.sims_cur_code + "&acaYear=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&subject_code=" + $scope.temp.sims_subject_code + "&opr=" + 'F' + "&term_code=" + $scope.temp.sims_term_code
                        + "&nationality=" + $scope.temp.sims_nationality_code + "&gender=" + $scope.temp.sims_appl_parameter + "&sen=" + $scope.temp.sen_status).then(function (res) {
                       $scope.gradebookoneyearprev_c = res.data;

                       console.log($scope.gradebookoneyearprev_c);
                       for (var i = 0; $scope.gradebookoneyearprev_c.length > i; i++) {

                           if ($scope.gradebookoneyearprev_c[i].average == 0) {
                               $scope.errormsg = "Data Not Available";
                           } else {
                               $scope.dash3_recieved_lst.push($scope.gradebookoneyearprev_c[i].average);
                               $scope.dash3_status_lst.push($scope.gradebookoneyearprev_c[i].sims_mark_grade_name);
                               $scope.sims_academic_year3 = $scope.gradebookoneyearprev_c[0].sims_academic_year;

                           }
                       }


                       var ctx = document.getElementById("compBarChart5").getContext("2d");
                       var data = {
                           //labels: ["In Process", "Not Applicable"],
                           labels: $scope.dash3_status_lst,
                           datasets: [{
                               data: $scope.dash3_recieved_lst,
                               backgroundColor: ['#FF5A5E', '#5AD3D1', '#9467bd', '#2fc32f', '#b0dc0b', '#eab404', '#de672c', '#ec2e2e', '#d5429b', '#6f52b8',
                                                 '#1c7cd5', '#56b9f7', '#0ae8eb', '#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0',
                                                  '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000',
                                                  '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000']
                           }]
                       }

                       if ($scope.sims_academic_year3!='undefined') {
                           window.bar5 = new Chart(ctx, {
                               type: 'pie',
                               data: data,
                               options: {
                                   legend: {
                                       display: true,
                                       labels: {
                                           boxWidth: 10,
                                           fontSize: 10,
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: $scope.sims_academic_year3,
                                       position: 'bottom'
                                   }
                               }
                           });
                       }
                       else{
                       window.bar5 = new Chart(ctx, {
                           type: 'pie',
                           data: data,
                           options: {
                               legend: {
                                   display: true,
                                   labels: {
                                       boxWidth: 10,
                                       fontSize: 10,
                                   }
                               },
                               title: {
                                   display: true,
                                   text: 'No Data',
                                   position: 'bottom'
                               }
                           }
                       });
                       }
                   })
               }

               $scope.showcompBarChartModal2 = function () {
                   debugger;
                   if (window.bar3 != undefined) {
                       window.bar3.destroy();
                   }
                   if (window.bar6 != undefined) {
                       window.bar6.destroy();
                   }
                   $scope.dash4_recieved_lst = [];
                   $scope.dash4_status_lst = [];
                   $scope.dash4_subject_lst = [];
                   $('#firstgraphModal2').modal({ backdrop: 'static', keyboard: false });


                   $http.get(ENV.apiUrl + "api/gradebookanalysisreport/getgradebookoneprevdata?curCode=" + $scope.temp.sims_cur_code + "&acaYear=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&subject_code=" + $scope.temp.sims_subject_code + "&opr=" + 'G' + "&term_code=" + $scope.temp.sims_term_code
                        + "&nationality=" + $scope.temp.sims_nationality_code + "&gender=" + $scope.temp.sims_appl_parameter + "&sen=" + $scope.temp.sen_status).then(function (res) {
                       $scope.gradebooktwoyearprev_c = res.data;

                       console.log($scope.gradebooktwoyearprev_c);
                       for (var i = 0; $scope.gradebooktwoyearprev_c.length > i; i++) {

                           if ($scope.gradebooktwoyearprev_c[i].average == 0) {
                               $scope.errormsg = "Data Not Available";
                           } else {
                               $scope.dash4_recieved_lst.push($scope.gradebooktwoyearprev_c[i].average);
                               $scope.dash4_status_lst.push($scope.gradebooktwoyearprev_c[i].sims_mark_grade_name);
                               $scope.sims_academic_year4 = $scope.gradebooktwoyearprev_c[0].sims_academic_year;

                           }
                       }


                       var ctx = document.getElementById("compBarChart6").getContext("2d");
                       var data = {
                           //labels: ["In Process", "Not Applicable"],
                           labels: $scope.dash4_status_lst,
                           datasets: [{
                               data: $scope.dash4_recieved_lst,
                               backgroundColor: ['#FF5A5E', '#5AD3D1', '#9467bd', '#2fc32f', '#b0dc0b', '#eab404', '#de672c', '#ec2e2e', '#d5429b', '#6f52b8',
                                                '#1c7cd5', '#56b9f7', '#0ae8eb', '#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0',
                                                 '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000',
                                                 '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000']
                           }]
                       }


                       if ($scope.sims_academic_year4 != undefined) {
                           window.bar6 = new Chart(ctx, {
                               type: 'pie',
                               data: data,
                               options: {
                                   legend: {
                                       display: true,
                                       labels: {
                                           boxWidth: 10,
                                           fontSize: 10,
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: $scope.sims_academic_year4,
                                       position: 'bottom'
                                   }
                               }
                           });
                       }
                       else {

                           window.bar6 = new Chart(ctx, {
                               type: 'pie',
                               data: data,
                               options: {
                                   legend: {
                                       display: true,
                                       labels: {
                                           boxWidth: 10,
                                           fontSize: 10,
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: 'No Data',
                                       position: 'bottom'
                                   }
                               }
                           });
                       }
                   })
               }


             

               $scope.showTableData = function () {
                   $scope.hideGraph = false;
               }
               $scope.showGraph = function () {
                   $scope.hideGraph = true;
               }






               /////  Export Excel ///////

               $scope.exportTable = function (tablename, id) {
                   var blob = new Blob([document.getElementById(id).innerHTML], {
                       type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                   });
                   saveAs(blob, tablename + ".xls");
               }
               /// end //

               $scope.printGraph = function (curgraph) {
                   
                   $("#searhbtn").css('display', 'none');
                   $("#searhbtn1").css('display', 'none');
                   $("#searhbtn2").css('display', 'none');
                   var options = {
                   };
                   var pdf = new jsPDF('p', 'pt', 'a3');
                   pdf.setTextColor(100)
                   //                    pdf.setFontSize(30)
                   //                    pdf.addImage(img, 'JPEG', 15, 40, 50, 50)
                   pdf.text(20, 20, $scope.user_details['lic_school_name']);
                   pdf.addHTML($("#" + curgraph), 30, 30, options, function () {
                       pdf.save(curgraph + '.pdf');
                   });
                   //                    addPage()
                   //   pdf.autoPrint();
                   
               }


         

           }]);

    simsController.filter('titleCase', function () {
        return function (input) {
            input = input || '';
            return input.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
        };
    });



})();