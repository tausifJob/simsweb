﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    //supriya
    simsController.controller('GraccingMarksActivityCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.edt = {};

            $scope.temp = {};
            $scope.display = false;
            $scope.itemsPerPage = "10";
            $scope.currentPage = 0;
            $scope.items = [];
            // $scope.CreTable = [];
            $scope.edt.sims_grade_code = [];
            $scope.edt.sims_section_code = [];
            $scope.url = ENV.apiUrl + 'Content' + '/' + $http.defaults.headers.common['schoolId'];
            var photofile;
            var main;



            $scope.getgrid = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getgracingmarks/Get_Grace_Marks").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.CreTable = res.data;
                    console.log(res.data);
                    // $scope.totalItems = $scope.CreTable.length;
                    // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                    // $scope.todos = $scope.CreTable;
                    // $scope.makeTodos();
                });

            }
            $scope.getgrid();
            //$http.get(ENV.apiUrl + "api/common/MedicalMedicine/GetMedicineType").then(function (res) {
            //    $scope.cmbMedicineType = res.data;
            //});



            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str;
            //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}



            $scope.size1 = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    //$scope.makeTodos();
                    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos1();
                }
            }





            $scope.index1 = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos1();
                main.checked = false;
                $scope.CheckAll();
            }

            $scope.filteredTodos1 = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 100;


            $scope.makeTodos1 = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos1 = $scope.todos.slice(begin, end);
            };



            $scope.getcur_code = function () {
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res1) {
                    $scope.cur_data = res1.data;
                    $scope.edt.sims_cur_code = $scope.cur_data[0].sims_cur_code;
                    $scope.getAcademic_year($scope.cur_data[0].sims_cur_code);
                });
            }


            $scope.getAcademic_year = function (cur_code1) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code1).then(function (res1) {
                    $scope.acad_data = res1.data;
                    $scope.edt.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                    $scope.getGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);

                });
            }


            $scope.getGrade = function (cur_code, acad_year) {
                
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                        // $("#cmb_grade_code").multipleSelect("setSelects", $scope.edt.sims_grade_code);
                        //$("#cmb_grade_code").multipleSelect("checkAll");
                    }, 1000);

                });
            }


            $scope.getSection = function (cur_code, academic_year, grade_code) {
              
                $http.get(ENV.apiUrl + "api/common/getgracingmarks/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    // $scope.getteachername($scope.temp.sims_cur_code, $scope.temp.sims_academic_year, $scope.temp.sims_grade_code, $scope.temp.sims_section_code);
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });

                        //  $("#cmb_section_code").multipleSelect("setSelects", $scope.edt.sims_section_code);
                        //$("#cmb_section_code").multipleSelect("checkAll");
                    }, 1000);
                });
            }



            $scope.getGracetype = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getgracingmarks/getGracetype").then(function (res1) {

                    $scope.grace_code = res1.data;
                    $scope.edt.sims_gracing_type = $scope.grace_code[0].sims_appl_parameter;

                });

            }



        
            $scope.getteachername = function (sims_cur_code, sims_academic_year, sims_grade_code, sims_section_code) {

                var grade_code = '';
                for (var i = 0; i < $scope.edt.sims_grade_code.length; i++) {
                    grade_code += $scope.edt.sims_grade_code[i] + ',';
                }

                var section_code = '';
                for (var i = 0; i < $scope.edt.sims_section_code.length; i++) {
                    section_code += $scope.edt.sims_section_code[i] + ',';

                }

                var obj = {
                    sims_cur_code: $scope.edt.sims_cur_code,
                    sims_academic_year: $scope.edt.sims_academic_year,
                    sims_grade_code: $scope.edt.sims_grade_code,
                    sims_section_code: $scope.edt.sims_section_code

                }


                obj.sims_grade_code = grade_code;
                obj.sims_section_code = section_code;


                $http.post(ENV.apiUrl + "api/common/getgracingmarks/postgetteacher", obj).then(function (res1) {
                    $scope.teacher_code = res1.data;
                    $scope.edt.sims_activity_Prefrred_teacher_code = $scope.teacher_code[0].sims_teacher_code;
                    setTimeout(function () {
                        $('#cmb_pref_teacher_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });

                    }, 1000);

                });

            }
            //modal

            $scope.showgracingmodal = function () {
                $('#GraccingModal').modal('show');

            }



            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };



            //Search
            $scope.search = function () {
                debugger;
                $scope.CreTable = $scope.searched($scope.CreTable, $scope.searchText);
                $scope.totalItems = $scope.CreTable.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    // $scope.todos = $scope.CreTable;
                }
                $scope.getgrid = function () {
                    debugger;
                    $http.get(ENV.apiUrl + "api/common/getgracingmarks/Get_Grace_Marks").then(function (res) {
                        $scope.display = false;
                        $scope.grid = true;
                        $scope.CreTable = res.data;
                        console.log(res.data);
                        // $scope.totalItems = $scope.CreTable.length;
                        // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                        // $scope.todos = $scope.CreTable;
                        // $scope.makeTodos();
                    });

                }
            }

            function searchUtil(item, toSearch) {
                debugger;
                /* Search Text in all 3 fields */
                return (item.sims_activity_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_activity_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_gracing_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_activity_max_point.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_activity_max_point_subject.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_activity_priority.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_activity_Prefrred_teacher_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }


            $scope.edit = function (str) {
                debugger;
                //  $scope.edt = str;
                $scope.display = true;
                $scope.grid = false;
                $scope.update1 = true;
                $scope.editflg = true;
                $scope.save1 = false;
                //$scope.getcur_code();
                //$scope.getAcademic_year();
                //$scope.getGrade();
                //$scope.getSection();
                //$scope.getGracetype();

                $http.post(ENV.apiUrl + "api/common/getgracingmarks/postgetteacher").then(function (res1) {
                    $scope.teacher_code = res1.data;
                    $scope.edt.sims_activity_Prefrred_teacher_code = str.sims_activity_Prefrred_teacher_code;


                });
                var grade_lst = [];
                var section_lst = [];
                $http.get(ENV.apiUrl + "api/common/getgracingmarks/GetSims_Grace_Marks?sims_activity_number=" + str.sims_activity_number).then(function (res) {
                    
                     
                    $scope.gredsec = res.data;
                    console.log($scope.gredsec);
                    
                    for (var i = 0; i < $scope.gredsec.length; i++) {

                        grade_lst.push($scope.gredsec[i].sims_grade_code)
                        section_lst.push($scope.gredsec[i].sims_section_code)
                    }

                    $("#cmb_grade_code").multipleSelect("setSelects", grade_lst);
                    //$scope.edt.sims_grade_code = grade_lst;
                    $("#cmb_section_code").multipleSelect("setSelects", section_lst);
                   // $scope.edt.sims_section_code = section_lst;
                });

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str.sims_cur_code + "&academic_year=" + str.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                        // $("#cmb_grade_code").multipleSelect("setSelects", $scope.edt.sims_grade_code);
                        //$("#cmb_grade_code").multipleSelect("checkAll");
                    }, 1000);

                });

                $http.get(ENV.apiUrl + "api/common/getgracingmarks/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + grade_lst + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                  
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });

                       // $("#cmb_section_code").multipleSelect("setSelects", section_lst);
                        //$("#cmb_section_code").multipleSelect("checkAll");
                    }, 1000);
                });

                $http.get(ENV.apiUrl + "api/common/getgracingmarks/getGracetype").then(function (res1) {

                    $scope.grace_code = res1.data;
                    $scope.edt.sims_gracing_type = str.sims_gracing_type;

                });

                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res1) {
                    $scope.cur_data = res1.data;
                    $scope.edt.sims_cur_code = str.sims_cur_code;

                });

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str.sims_cur_code).then(function (res1) {
                    $scope.acad_data = res1.data;
                });

                

                $scope.edt =
                    {

                        sims_cur_code: str.sims_cur_code,
                        sims_academic_year: str.sims_academic_year,
                        sims_activity_name: str.sims_activity_name,
                        sims_activity_number: str.sims_activity_number,
                        //sims_grade_code: str.sims_grade_code,
                        //sims_section_code: str.sims_section_code,
                        sims_gracing_type: str.sims_gracing_type,
                        sims_activity_Prefrred_teacher_code: str.sims_activity_Prefrred_teacher_code,
                        sims_activity_symbol: str.sims_activity_symbol,
                        sims_activity_max_point: str.sims_activity_max_point,
                        sims_activity_max_point_subject: str.sims_activity_max_point_subject,
                        sims_activity_priority: str.sims_activity_priority,
                        sims_activity_status: str.sims_activity_status,


                    }
                


            }

            $scope.Update = function (newEventForm) {
                debugger;
                if (newEventForm.$valid) {




                    var data = $scope.edt;
                    data.opr = 'U';

                    debugger;
                    $http.post(ENV.apiUrl + "api/common/getgracingmarks/UpdateNewSims_Grace_Marks", data).then(function (res) {
                        $scope.ins = res.data;
                        if (res.data) {
                            swal({ text: "Record Updated Successfully.", imageUrl: "assets/img/check.png" });
                            $http.get(ENV.apiUrl + "api/common/getgracingmarks/GetSims_Grace_Marks").then(function (res) {
                                $scope.display = false;
                                $scope.grid = true;
                                $scope.items = res.data;
                                $scope.currentPage = 0;
                            });
                            $scope.getgrid();
                        }
                        else
                            swal({ text: "Record Not Updated ." + $scope.ins, imageUrl: "assets/img/close.png" });


                    });




                }
            }


            //$scope.UpdateSeat_No = function () {
            //    debugger;
            //    var datacoll = [];
            //    var collenroll = '';

            //    for (var i = 0; i < $scope.AllStudentData.length; i++) {
            //        if ($scope.AllStudentData[i].sims_board_exam_registration_no != undefined)

            //            collenroll = collenroll + $scope.AllStudentData[i].sims_student_enroll_number + '/' + $scope.AllStudentData[i].sims_board_exam_registration_no + ','

            //    }

            //    if (collenroll.length > 0)
            //        var data = {
            //            sims_cur_code: $scope.AllStudentData[0].sims_cur_code,
            //            sims_academic_year: $scope.AllStudentData[0].sims_academic_year,
            //            sims_grade_code: $scope.AllStudentData[0].sims_grade_code,
            //            sims_section_code: $scope.AllStudentData[0].sims_section_code,
            //            //sims_student_enroll_number: $scope.AllStudentData[i].sims_student_enroll_number,
            //            //sims_roll_number: $scope.AllStudentData[i].sims_roll_number,
            //            sims_board_exam_enroll_number: collenroll,

            //            //string: $scope.AllStudentData[i].string 
            //        }
            //    datacoll.push(data);


            //    $http.post(ENV.apiUrl + "api/AllocationRollNo/updateSeatNo", datacoll).then(function (msg1) {
            //        $scope.msg = msg1.data;

            //        if ($scope.msg) {
            //            swal('', 'Record Updated Successfully.');
            //            $scope.reset_form();
            //        }
            //        //else {
            //        //    swal('','Roll Number Already Mapped/Data Not Updated');

            //        //}


            //    });



            //}


            $scope.Fetchdata = function (info) {
                debugger;

                $('#GraccingModal').modal('show')
                //$scope.prdocdata = [];

                $http.get(ENV.apiUrl + "api/common/getgracingmarks/GetSims_Grace_Marks?sims_activity_number=" + info.sims_activity_number).then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.filteredTodos1 = res.data;
                    console.log(res.data);
                    $scope.totalItems = $scope.filteredTodos1.length;
                    // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                    $scope.todos = $scope.filteredTodos1;
                    $scope.makeTodos1();
                });

            }


            $scope.Save = function (newEventForm) {
                debugger;
                if (newEventForm.$valid) {
                    var grade_code = '';
                    for (var i = 0; i < $scope.edt.sims_grade_code.length; i++) {
                        grade_code += $scope.edt.sims_grade_code[i] + ',';
                    }

                    var section_code = '';
                    for (var i = 0; i < $scope.edt.sims_section_code.length; i++) {
                        section_code += $scope.edt.sims_section_code[i] + ',';

                    }

                    //var teacher_code = ''
                    //for (var i = 0; i < $scope.edt.sims_activity_Prefrred_teacher_code.length; i++) {
                    //    teacher_code += $scope.edt.sims_activity_Prefrred_teacher_code[i] + ',';

                    //}
                    var data = $scope.edt;
                    data.opr = 'I';
                    data.sims_grade_code = grade_code;
                    data.sims_section_code = section_code;
                    //data.sims_activity_Prefrred_teacher_code = teacher_code;

                    $http.post(ENV.apiUrl + "api/common/getgracingmarks/InsertUpdateSims_Grace_Marks", data).then(function (res) {
                        $scope.ins = res.data;
                        if (res.data) {
                            swal({ text: "Record Inserted Successfully.", imageUrl: "assets/img/check.png" });
                            $http.get(ENV.apiUrl + "api/common/getgracingmarks/Get_Grace_Marks").then(function (res) {
                                $scope.display = false;
                                $scope.grid = true;
                                $scope.items = res.data;
                                $scope.currentPage = 0;
                            });
                            $scope.getgrid();
                        }
                        else
                            swal({ text: "Record Not Inserted." + $scope.ins, imageUrl: "assets/img/close.png" });


                    });




                }
            }

            $scope.New = function () {
                $scope.edt = "";
                $scope.grid = false;
                $scope.display = true;
                $scope.save1 = true;
                $scope.editflg = false;
                $scope.getGracetype();
                $scope.getcur_code();

                $scope.edt = {};
                $scope.edt.sims_activity_status = true;

                //$scope.getcur_code = function () {
                //    $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res1) {
                //        $scope.cur_data = res1.data;
                //        $scope.edt.sims_cur_code = $scope.cur_data[0].sims_cur_code;
                //        $scope.getAcademic_year($scope.cur_data[0].sims_cur_code);
                //    });
                //}


                $scope.getAcademic_year = function (cur_code1) {
                    debugger
                    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code1).then(function (res1) {
                        $scope.acad_data = res1.data;
                        $scope.edt.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                        $scope.getGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);

                    });
                }

            }









            //var main, del = '', delvar = [];

            //$scope.chk = function () {
            //    main = document.getElementById('chk_min');
            //    if (main.checked == true) {
            //        for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //            var v = document.getElementById($scope.filteredTodos[i].sims_activity_number + i);

            //            v.checked = true;
            //            $scope.row1 = 'row_selected';
            //            $('tr').addClass("row_selected");
            //        }
            //    }
            //    else {
            //        for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //            var v = document.getElementById($scope.filteredTodos[i].sims_activity_number + i);
            //            v.checked = false;
            //            main.checked = false;
            //            $scope.row1 = '';
            //            $('tr').removeClass("row_selected");
            //        }
            //    }

            //}

            //$scope.Delete = function () {
            //    debugger
            //    var deletefin = [];
            //    $scope.flag = false;
            //    for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //        var v = document.getElementById($scope.filteredTodos[i].sims_activity_number + i);
            //        if (v.checked == true) {
            //            $scope.flag = true;
            //            var deletemodulecode = ({
            //                'sims_activity_number': $scope.filteredTodos[i].sims_activity_number,
            //                opr: 'D'
            //            });
            //            deletefin.push(deletemodulecode);
            //        }
            //    }
            //    if ($scope.flag) {
            //        swal({
            //            title: '',
            //            text: "Are you sure you want to Delete?",
            //            showCloseButton: true,
            //            showCancelButton: true,
            //            confirmButtonText: 'Yes',
            //            width: 380,
            //            cancelButtonText: 'No',

            //        }).then(function (isConfirm) {

            //            if (isConfirm) {
            //                debugger;
            //                $http.post(ENV.apiUrl + "api/common/getgracingmarks/dataforDelete", deletefin).then(function (msg) {
            //                    $scope.msg1 = msg.data;
            //                    if ($scope.msg1 == true) {
            //                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
            //                            if (isConfirm) {
            //                                $scope.getgrid();
            //                                main = document.getElementById('mainchk');
            //                                if (main.checked == true) {
            //                                    main.checked = false;
            //                                }

            //                                $scope.CheckAllChecked();
            //                            }
            //                            $scope.currentPage = true;
            //                        });
            //                    }
            //                    else {
            //                        swal({ text: "Record Not Deleted. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
            //                            if (isConfirm) {
            //                                $scope.getgrid();
            //                                main.checked = false;
            //                                $('tr').removeClass("row_selected");
            //                            }
            //                        });
            //                    }

            //                });
            //                deletefin = [];
            //            }
            //            else {
            //                for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //                    var v = document.getElementById($scope.filteredTodos[i].sims_activity_number+ i);
            //                    if (v.checked == true) {
            //                        v.checked = false;
            //                        main.checked = false;
            //                        $('tr').removeClass("row_selected");
            //                    }
            //                }
            //            }
            //        });
            //    }
            //    else {
            //        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
            //    }
            //    $scope.currentPage = true;
            //}


            $scope.CheckAllChecked = function () {
                debugger;
                var main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.CreTable.length; i++) {
                        var v = document.getElementById($scope.CreTable[i].sims_activity_number + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.CreTable.length; i++) {
                        var v = document.getElementById($scope.CreTable[i].sims_activity_number + i);

                        //var v = document.getElementById('asset-'+i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                var main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }



            $scope.CheckAll = function () {
                debugger;
                var main = document.getElementById('mainchk1');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                        // var v = document.getElementById($scope.filteredTodos[i].sims_grade_code + $scope.filteredTodos[i].sims_section_code + i);
                        // v.checked = true;
                        $("#test-" + i).prop('checked', true);
                        //$scope.CreDiv[i].get_check = true;

                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                        //var v = document.getElementById($scope.filteredTodos[i].sims_grade_code + $scope.filteredTodos[i].sims_section_coder + i);
                        $("#test-" + i).prop('checked', false);
                        //var v = document.getElementById('asset-'+i);
                        // v.checked = false;
                        //$scope.CreDiv[i].get_check = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                var main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.Delete= function () {
                debugger;
                var data1 = [];
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                    var v = document.getElementById('test-'+ i);
                    //$("#test-" + i).prop('checked', false);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_activity_number': $scope.filteredTodos1[i].sims_activity_number,
                            'sims_grade_code': $scope.filteredTodos1[i].sims_grade_code,
                            'sims_section_code': $scope.filteredTodos1[i].sims_section_code,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger;
                            $http.post(ENV.apiUrl + "api/common/getgracingmarks/dataforDelete", deleteleave).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        $http.get(ENV.apiUrl + "api/common/getgracingmarks/GetSims_Grace_Marks?sims_activity_number=" + deleteleave[0].sims_activity_number).then(function (res) {
                                            $scope.display = false;
                                            $scope.grid = true;
                                            $scope.filteredTodos1 = res.data;
                                            console.log(res.data);
                                            $scope.totalItems = $scope.filteredTodos1.length;
                                            // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                                            $scope.todos = $scope.filteredTodos1;
                                            $scope.makeTodos1();
                                        });
                                        if (isConfirm) {
                                            $scope.Show();
                                        }
                                        $scope.currentPage = true;

                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    //if(){}
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.Show();
                                        }

                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                                var v = document.getElementById('test-' + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                data1 = [];
                $scope.row1 = '';
                main.checked = false;
                $scope.currentPage = str;
                $scope.Fetchdata();
            }


            $scope.DeleteAll= function () {
                debugger;
                var data1 = [];
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.CreTable.length; i++) {
                    var v = document.getElementById($scope.CreTable[i].sims_activity_number + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_activity_number': $scope.CreTable[i].sims_activity_number,
                            opr: 'R'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger;
                            $http.post(ENV.apiUrl + "api/common/getgracingmarks/dataforDelete", deleteleave).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        $scope.getgrid();
                                        if (isConfirm) {
                                            $scope.Show();
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    //if(){}
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.Show();
                                        }

                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.CreTable.length; i++) {
                                var v = document.getElementById($scope.CreTable[i].sims_activity_number + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                data1 = [];
                $scope.row1 = '';
                main.checked = false;
                $scope.currentPage = str;
            }


            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.update1 = false;
                $scope.save1 = false;
                $scope.editflg = false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.edt = {
                sims_medicine_production_date: dd + '-' + mm + '-' + yyyy,
                sims_medicine_expirey_date: dd + '-' + mm + '-' + yyyy
            }


        }]);


})();



