﻿(function () {
    'use strict';
    var teacher_code = [];
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CommentsDetailEntryCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
           
            var user = $rootScope.globals.currentUser.username;
            var appl_code = "sim070";
            $scope.fcommn = [];
            $scope.pagesize = 'All';
            $scope.pageindex = "0";
            $scope.pager = false;
            $scope.toppager = false;
            $scope.busyindicator = true;
            $scope.showtable = false;
            var sub_head='';
            var effort_head = '';

            $http.get(ENV.apiUrl + "api/Gradebook/GetadminuserType?username=" + user + "&appl_code=" + appl_code).then(function (getAdmin) {
                $scope.getadmin = getAdmin.data;
                
            });
            debugger
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                debugger
                $scope.curriculum = res.data;
                $scope.edt = {
                    cur_code: $scope.curriculum[0].sims_cur_code
                }
                $scope.fcommn['cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr1($scope.curriculum[0].sims_cur_code);
            });
         
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            setTimeout(function () {
                $("#rcbItem").select2();

            }, 100);

            var username = $rootScope.globals.currentUser.username;
            $http.get(ENV.apiUrl + "api/Gradebook/getCommenttype_new?uname=" + username).then(function (Comment_type) {
                $scope.get_Comment_type = Comment_type.data;

            })
            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['chk_all'] = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                debugger;
                if ($scope.pagesize == 'All') {
                    $scope.currentPage = 1;
                    $scope.showPager = false;
                    $scope.filteredTodos = $scope.st_lst1;
                }
                else {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                }
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.student_list, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.student_list;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.sComment = {};
            
            $scope.getacyr1 = function (str) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.getAcademicYear1 = Academicyear.data; //academic_year
                    $scope.fcommn['academic_year'] = $scope.getAcademicYear1[0].sims_academic_year;
                    $scope.getsections2($scope.fcommn.cur_code, $scope.getAcademicYear1[0].sims_academic_year);
                });
            }

            $scope.getsection2 = function (str, str1, str2) {
                $http.get(ENV.apiUrl + "api/Gradebook/GetRCSection?curcode=" + str + "&ayear=" + str1 + "&gradecode=" + str2 + "&teachercode=" + user).then(function (Allsection) {
                    $scope.getSectionFromGrade1 = Allsection.data;
                    $scope.fcommn['sims_section_code'] = $scope.getSectionFromGrade1[0].sims_section_code;
                    $scope.Getterm2(str, str1)

                })
            };

            $scope.getsections2 = function (str, str1) {

                $http.get(ENV.apiUrl + "api/Gradebook/GetRCGrade?curcode=" + str + "&ayear=" + str1 + "&teachercode=" + user).then(function (res) {
                    $scope.getAllGrades1 = res.data;
                    $scope.fcommn['sims_grade_code'] = $scope.getAllGrades1[0].sims_grade_code;
                    $scope.getsection2(str, str1, $scope.getAllGrades1[0].sims_grade_code);
                })
            };
                
            var subject_names1 = [];
            $scope.GradeSectionSubject2 = function () {

                if ($scope.fcommn.sims_comment_code == 'S') {
                    $scope.subject_type = false;
                }
                else {
                    $scope.subject_type = true;
                    $scope.fcommn.sims_subject_code = '';
                }

                $scope.subject_names = [];

                $scope.commentcountlabel = 0;
                $scope.headercountlabel = 0;
                subject_names1 = [];

                $scope.SectionSubject5 = [];

                var report1 = {
                    sims_grade_code: $scope.fcommn.sims_grade_code,
                    sims_academic_year: $scope.fcommn.academic_year,
                    sims_section_code: $scope.fcommn.sims_section_code,
                    sims_employee_code: user,
                    sims_cur_code: $scope.fcommn.cur_code
                };


                $http.post(ENV.apiUrl + "api/Gradebook/Subjectget", report1).then(function (allSection_Subject) {
                    $scope.subject_names1 = allSection_Subject.data;

                });

                var report = {
                    SubjectCurr: $scope.fcommn.cur_code,
                    SubjectGrade: $scope.fcommn.sims_grade_code,
                    SubjectAcademic: $scope.fcommn.academic_year,
                    SubjectSection: $scope.fcommn.sims_section_code,
                    SubjectTerm: $scope.fcommn.sims_term_code,
                    SubTeacherEmpId: user
                };

                $http.post(ENV.apiUrl + "api/Gradebook/ReportCardlevels?data=" + JSON.stringify(report)).then(function (Getlevels) {
                    $scope.Get_levels = Getlevels.data;
                    $scope.fcommn['level_code'] = $scope.Get_levels[0].level_code;
                    $scope.getRCreportcardname();
                });


                $http.get(ENV.apiUrl + "api/Gradebook/GetRCStudentForComments?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code).then(function (GetRCStudentForComments) {
                    $scope.GetRCStudentForComments = GetRCStudentForComments.data;

                });
            }

            $scope.Getterm2 = function (cur_code, oldval) {
                debugger
                $http.get(ENV.apiUrl + "api/Gradebook/getTermforGradebook?cur_code=" + cur_code + "&academic_year=" + oldval).then(function (Getsims550_TermsAcademicYear) {
                        $scope.TermsAcademicYear1 = Getsims550_TermsAcademicYear.data;
                        $scope.fcommn['sims_term_code'] = $scope.TermsAcademicYear1[0].sims_term_code;
                        $scope.fcommn['sims_comment_code'] = $scope.get_Comment_type[0].sims_comment_code;

                        $scope.GradeSectionSubject2();

                });
                $scope.getRCLevelList();
            }

            $scope.GradeSectionSubject2 = function () {

                    if ($scope.fcommn.sims_comment_code == 'S') {
                        $scope.subject_type = false;
                    }
                    else {
                        $scope.subject_type = true;
                        $scope.fcommn.sims_subject_code = '';
                    }

                    $scope.subject_names = [];

                    $scope.commentcountlabel = 0;
                    $scope.headercountlabel = 0;
                    subject_names1 = [];

                    $scope.SectionSubject5 = [];

                    var report1 = {
                        sims_grade_code: $scope.fcommn.sims_grade_code,
                        sims_academic_year: $scope.fcommn.academic_year,
                        sims_section_code: $scope.fcommn.sims_section_code,
                        sims_employee_code: user,
                        sims_cur_code: $scope.fcommn.cur_code
                    };


                    $http.post(ENV.apiUrl + "api/Gradebook/Subjectget", report1).then(function (allSection_Subject) {
                        $scope.subject_names1 = allSection_Subject.data;

                    });

                    var report = {
                        SubjectCurr: $scope.fcommn.cur_code,
                        SubjectGrade: $scope.fcommn.sims_grade_code,
                        SubjectAcademic: $scope.fcommn.academic_year,
                        SubjectSection: $scope.fcommn.sims_section_code,
                        SubjectTerm: $scope.fcommn.sims_term_code,
                        SubTeacherEmpId: user
                    };

                    //$http.post(ENV.apiUrl + "api/Gradebook/ReportCardlevels?data=" + JSON.stringify(report)).then(function (Getlevels) {
                    //    $scope.Get_levels = Getlevels.data;
                    //    $scope.fcommn['level_code'] = $scope.Get_levels[0].level_code;
                    //    $scope.getRCreportcardname();
                    //});


                    $http.get(ENV.apiUrl + "api/Gradebook/GetRCStudentForComments?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code).then(function (GetRCStudentForComments) {
                        $scope.GetRCStudentForComments = GetRCStudentForComments.data;

                    });
                }
            
            $scope.comment = function (str) {
                $scope.showtable = false;
                    if (str == 'S') {
                        $scope.subject_type = false;
                    }
                    else {
                        $scope.subject_type = true;
                        $scope.fcommn.sims_subject_code = '';
                    }
                    $scope.getCommentsheader();
                }

            $scope.getCommentsheader = function () {

                    if ($scope.fcommn.sims_comment_code == 'S') {
                        $scope.subject_type = false;
                    }
                    else {
                        $scope.subject_type = true;
                        $scope.fcommn.sims_subject_code = '';
                    }

                    $http.get(ENV.apiUrl + "api/Gradebook/GetRCStudentForCommentsheader?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code + "&term_code=" + $scope.fcommn.sims_term_code + "&comment_type=" + $scope.fcommn.sims_comment_code).then(function (Commentsheader1) {
                        $scope.Comments_header1 = Commentsheader1.data

                    });
            }

            $scope.GetfinalCommentsstudents = function () {
                $scope.busyindicator = false;
                $scope.showtable = false;
                var sub_selection = document.getElementById('subject_dropdown').value;
                console.log(sub_selection);
                if (sub_selection == "" && $scope.fcommn.sims_comment_code == 'S') {
                    swal({ title: "Alert", text: "Please Select Subject", width: 300, height: 200 });
                }
                else {
                    var a = { opr: 'AA', sims_cur_code: $scope.fcommn.cur_code, sims_academic_year: $scope.fcommn.academic_year, sims_grade_code: $scope.fcommn.sims_grade_code, sims_section_code: $scope.fcommn.sims_section_code, sims_term_code: $scope.fcommn.sims_term_code, sims_comment_type: $scope.fcommn.sims_comment_code, sims_subject_code: $scope.fcommn.sims_subject_code, sims_level_code: $scope.fcommn.sims_level_code, sims_report_card_code: $scope.fcommn.sims_report_card_code };

                    $http.post(ENV.apiUrl + "api/CommentsDetailsEntryController/CommentDetailsCommon",a).then(function (studentlist) {
                        $scope.student_list1 = studentlist.data.table;
                        if($scope.student_list1.length > 0) {
                            $scope.general_head = $scope.student_list1[0].sims_student_header_code;
                        }
                        $scope.st_lst1 = [];
                        $scope.st_lst2 = [];
                        $scope.st_lst3 = [];
                        for (var i = 0; i < $scope.student_list1.length; i++) {
                            if ($scope.student_list1[i].sims_comment_sr == 'Effort' && $scope.student_list1[i].student_type == '2') {
                                var data = {
                                    sims_comment_sr: $scope.student_list1[i].sims_comment_sr,
                                    sims_academic_year: $scope.student_list1[i].sims_academic_year,
                                    sims_cur_code: $scope.student_list1[i].sims_cur_code,
                                    sims_grade_code: $scope.student_list1[i].sims_grade_code,
                                    sims_section_code: $scope.student_list1[i].sims_section_code,
                                    sims_enroll_number: $scope.student_list1[i].sims_enroll_number,
                                    sims_term_code: $scope.student_list1[i].sims_term_code,
                                    sims_subject_code: $scope.student_list1[i].sims_subject_code,
                                    sims_level_code: $scope.student_list1[i].sims_level_code,
                                    sims_report_card_code: $scope.student_list1[i].sims_report_card_code,
                                    sims_student_comment_id: $scope.student_list1[i].sims_student_comment_id,
                                    sims_student_root_comment_display_order: $scope.student_list1[i].sims_student_root_comment_display_order,
                                    sims_comment_status: $scope.student_list1[i].sims_comment_status,
                                    sims_comment_type: $scope.student_list1[i].sims_comment_type,
                                    sims_user_code: $rootScope.globals.currentUser.username,
                                    sims_student_header_code: $scope.student_list1[i].sims_student_header_code,
                                    student_name: $scope.student_list1[i].student_name,
                                    sims_effort_desc: $scope.student_list1[i].sims_comment_desc
                                }

                                $scope.st_lst1.push(data)
                            }
                        }

                        for (var i = 0; i < $scope.student_list1.length; i++) {
                            if ($scope.student_list1[i].student_type == '1') {
                                var data2 = {
                                    sims_comment_sr: $scope.student_list1[i].sims_comment_sr,
                                    sims_academic_year: $scope.student_list1[i].sims_academic_year,
                                    sims_cur_code: $scope.student_list1[i].sims_cur_code,
                                    sims_grade_code: $scope.student_list1[i].sims_grade_code,
                                    sims_section_code: $scope.student_list1[i].sims_section_code,
                                    sims_enroll_number: $scope.student_list1[i].sims_enroll_number,
                                    sims_term_code: $scope.student_list1[i].sims_term_code,
                                    sims_subject_code: $scope.student_list1[i].sims_subject_code,
                                    sims_level_code: $scope.student_list1[i].sims_level_code,
                                    sims_report_card_code: $scope.student_list1[i].sims_report_card_code,
                                    sims_student_comment_id: $scope.student_list1[i].sims_student_comment_id,
                                    sims_student_root_comment_display_order: $scope.student_list1[i].sims_student_root_comment_display_order,
                                    sims_comment_status: $scope.student_list1[i].sims_comment_status,
                                    sims_comment_type: $scope.student_list1[i].sims_comment_type,
                                    sims_user_code: $rootScope.globals.currentUser.username,
                                    sims_student_header_code: $scope.student_list1[i].sims_student_header_code,
                                    student_name: $scope.student_list1[i].student_name,
                                    sims_comment_desc: $scope.student_list1[i].sims_comment_desc
                                }
                                $scope.st_lst1.push(data2)
                            }
                        }

                        for (var i = 0; i < $scope.student_list1.length; i++) {

                            if ($scope.student_list1[i].sims_comment_sr == 'Comment' && $scope.student_list1[i].student_type == '2') {
                                $scope.flgchk = false;
                                for (var j = 0; j < $scope.st_lst1.length; j++) {

                                    if ($scope.st_lst1[j].sims_enroll_number == $scope.student_list1[i].sims_enroll_number) {
                                        $scope.flgchk = true
                                        $scope.st_lst1[j]['sims_comment_desc'] = $scope.student_list1[i].sims_comment_desc
                                    }


                                }
                                if (!$scope.flgchk) {
                                    var data1 = {
                                        sims_comment_sr: $scope.student_list1[i].sims_comment_sr,
                                        sims_academic_year: $scope.student_list1[i].sims_academic_year,
                                        sims_cur_code: $scope.student_list1[i].sims_cur_code,
                                        sims_grade_code: $scope.student_list1[i].sims_grade_code,
                                        sims_section_code: $scope.student_list1[i].sims_section_code,
                                        sims_enroll_number: $scope.student_list1[i].sims_enroll_number,
                                        sims_term_code: $scope.student_list1[i].sims_term_code,
                                        sims_subject_code: $scope.student_list1[i].sims_subject_code,
                                        sims_level_code: $scope.student_list1[i].sims_level_code,
                                        sims_report_card_code: $scope.student_list1[i].sims_report_card_code,
                                        sims_student_comment_id: $scope.student_list1[i].sims_student_comment_id,
                                        sims_student_root_comment_display_order: $scope.student_list1[i].sims_student_root_comment_display_order,
                                        sims_comment_status: $scope.student_list1[i].sims_comment_status,
                                        sims_comment_type: $scope.student_list1[i].sims_comment_type,
                                        sims_user_code: $rootScope.globals.currentUser.username,
                                        sims_student_header_code: $scope.student_list1[i].sims_student_header_code,
                                        student_name: $scope.student_list1[i].student_name,
                                        sims_comment_desc: $scope.student_list1[i].sims_comment_desc
                                    }
                                    $scope.st_lst1.push(data1)
                                }
                            }
                        }
                        $scope.totalItems = $scope.st_lst1.length;
                        $scope.todos = $scope.st_lst1;
                        $scope.makeTodos();
                        $scope.busyindicator = true;
                        $scope.showtable = true;

                    });
                }
            }
            $scope.getRCLevelList = function () {
                $http.post(ENV.apiUrl + "api/CommentsDetailsEntryController/gerRclevels?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code).then(function (level) {
                    $scope.level_list = level.data
                    $scope.fcommn['sims_level_code'] = $scope.level_list[0].sims_level_code;
                    $scope.getRCNameListnew($scope.fcommn.cur_code, $scope.fcommn.academic_year, $scope.fcommn.sims_grade_code, $scope.fcommn.sims_section_code, $scope.fcommn.sims_level_code, $scope.fcommn.sims_term_code);
                });
            }

            $scope.getRCNameList = function () {
                $http.post(ENV.apiUrl + "api/CommentsDetailsEntryController/gerRcname?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code + "&level=" + $scope.fcommn.sims_level_code + "&term=" + $scope.fcommn.sims_term_code).then(function (report) {
                    $scope.reportname_list = report.data;
                  //  for (var i = 0; i < $scope.reportname_list.length; i++) {
                    $scope.fcommn['sims_report_card_code'] = $scope.reportname_list[0].sims_report_card_code;
                   // }
                });
            }

            $scope.getRCNameListnew = function (curcode, acad, grade, section, level, term) {
                $scope.reportname_list = [];
                $http.post(ENV.apiUrl + "api/CommentsDetailsEntryController/gerRcname?curcode=" + curcode + "&ayear=" + acad + "&gradecode=" + grade + "&section=" + section + "&level=" + level + "&term=" + term).then(function (report) {
                    $scope.reportname_list = report.data;
                    //  for (var i = 0; i < $scope.reportname_list.length; i++) {
                    $scope.fcommn['sims_report_card_code'] = $scope.reportname_list[0].sims_report_card_code;
                    // }
                });
            }

            $scope.CheckAllChecked = function () {
                var main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }
            }
            
            $scope.Insert_data = function () {
                debugger
                if ($scope.fcommn.sims_comment_code == 'S')
                {
                    $scope.save_sub_comments();
                }
                else{
                    $scope.save_general_comments();
                }

            }

            $scope.getSubHead = function () {
                debugger
                $http.get(ENV.apiUrl + "api/CommentsDetailsEntryController/getsubjectheader?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code + "&level=" + $scope.fcommn.sims_level_code + "&report=" + $scope.fcommn.sims_report_card_code).then(function (sub_code) {
                    $scope.getsubjectheader1 = sub_code.data;      
                   $scope.sub_head = $scope.getsubjectheader1[0].sims_report_card_code;
                });
            }

            $scope.getEffHead = function () {
                debugger
                $http.get(ENV.apiUrl + "api/CommentsDetailsEntryController/getsubjecteffortheader?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code + "&level=" + $scope.fcommn.sims_level_code + "&report=" + $scope.fcommn.sims_report_card_code).then(function (sub_code) {
                    $scope.getsubjecteffortheader1 = sub_code.data;
                   $scope.effort_head = $scope.getsubjecteffortheader1[0].sims_report_card_code;
                });
            }

            $scope.getgeneralHead = function () {
                debugger
                $http.get(ENV.apiUrl + "api/CommentsDetailsEntryController/getgeneraltheader?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code + "&level=" + $scope.fcommn.sims_level_code + "&report=" + $scope.fcommn.sims_report_card_code).then(function (sub_code) {
                    $scope.getsubjecteffortheader1 = sub_code.data;
                    effort_head = $scope.getsubjecteffortheader1[0].sims_report_card_code;
                });
            }

            $scope.save_sub_comments = function () {
                $scope.busyindicator = false;
                $http.get(ENV.apiUrl + "api/CommentsDetailsEntryController/getsubjectheader?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code + "&level=" + $scope.fcommn.sims_level_code + "&report=" + $scope.fcommn.sims_report_card_code).then(function (sub_code) {
                    $scope.getsubjectheader1 = sub_code.data;      
                    $scope.sub_head = $scope.getsubjectheader1[0].sims_report_card_code;
                    $http.get(ENV.apiUrl + "api/CommentsDetailsEntryController/getsubjecteffortheader?curcode=" + $scope.fcommn.cur_code + "&ayear=" + $scope.fcommn.academic_year + "&gradecode=" + $scope.fcommn.sims_grade_code + "&section=" + $scope.fcommn.sims_section_code + "&level=" + $scope.fcommn.sims_level_code + "&report=" + $scope.fcommn.sims_report_card_code).then(function (sub_code) {
                        $scope.getsubjecteffortheader1 = sub_code.data;
                        $scope.effort_head = $scope.getsubjecteffortheader1[0].sims_report_card_code;
              
                        $scope.datasend = [];
                        $scope.datasend1 = [];
                        $scope.datasend2 = [];
                        //for (var i = 0; i < $scope.st_lst1.length; i++) {
                        //    var t = $scope.st_lst1[i].sims_enroll_number;
                        //    var v = document.getElementById(t + i);
                        //    if (v != null) {
                        //        if (v.checked == true) {
                        //            $scope.datasend.push($scope.st_lst1[i]);
                        //        }
                        //    }
                        //}
                        console.log($scope.datasend);

                        for (var i = 0; i < $scope.st_lst1.length; i++) {
                            if ($scope.st_lst1[i].subcheck == true) {
                                debugger;

                                var datasnd = {
                                    sims_comment_sr: $scope.st_lst1[i].sims_comment_sr,
                                    sims_academic_year: $scope.st_lst1[i].sims_academic_year,
                                    sims_cur_code: $scope.st_lst1[i].sims_cur_code,
                                    sims_grade_code: $scope.st_lst1[i].sims_grade_code,
                                    sims_section_code: $scope.st_lst1[i].sims_section_code,
                                    sims_enroll_number: $scope.st_lst1[i].sims_enroll_number,
                                    sims_term_code: $scope.fcommn.sims_term_code,
                                    sims_subject_code: $scope.fcommn.sims_subject_code,
                                    sims_level_code: $scope.fcommn.sims_level_code,
                                    sims_report_card_code: $scope.fcommn.sims_report_card_code,
                                    sims_student_comment_id: $scope.st_lst1[i].sims_student_comment_id,
                                    sims_student_root_comment_display_order: $scope.st_lst1[i].sims_student_root_comment_display_order,
                                    sims_comment_status: $scope.st_lst1[i].sims_comment_status,
                                    sims_comment_type: $scope.st_lst1[i].sims_comment_type,
                                    sims_user_code: $rootScope.globals.currentUser.username,
                                    sims_student_header_code:$scope.sub_head,//$scope.st_lst1[i].sims_student_header_code,
                                    //    sims_student_header_code1: sub_head,
                                    student_name: $scope.st_lst1[i].student_name,
                                    sims_comment_desc: $scope.st_lst1[i].sims_comment_desc
                                }
                                $scope.datasend1.push(datasnd);
                            }                       
                        }

                        for (var i = 0; i < $scope.st_lst1.length; i++) {
                            if ($scope.st_lst1[i].subcheck == true) {
                                debugger;
                                var datasnd2 = {
                                    sims_comment_sr: $scope.st_lst1[i].sims_comment_sr,
                                    sims_academic_year: $scope.st_lst1[i].sims_academic_year,
                                    sims_cur_code: $scope.st_lst1[i].sims_cur_code,
                                    sims_grade_code: $scope.st_lst1[i].sims_grade_code,
                                    sims_section_code: $scope.st_lst1[i].sims_section_code,
                                    sims_enroll_number: $scope.st_lst1[i].sims_enroll_number,
                                    sims_term_code: $scope.fcommn.sims_term_code,
                                    sims_subject_code: $scope.fcommn.sims_subject_code,
                                    sims_level_code: $scope.fcommn.sims_level_code,
                                    sims_report_card_code: $scope.fcommn.sims_report_card_code,
                                    sims_student_comment_id: $scope.st_lst1[i].sims_student_comment_id,
                                    sims_student_root_comment_display_order: $scope.st_lst1[i].sims_student_root_comment_display_order,
                                    sims_comment_status: $scope.st_lst1[i].sims_comment_status,
                                    sims_comment_type: $scope.st_lst1[i].sims_comment_type,
                                    sims_user_code: $rootScope.globals.currentUser.username,
                                    sims_student_header_code:$scope.effort_head,//$scope.st_lst1[i].sims_student_header_code,
                                    //sims_student_header_code1: effort_head,
                                    student_name: $scope.st_lst1[i].student_name,
                                    sims_comment_desc: $scope.st_lst1[i].sims_effort_desc
                                }
                                $scope.datasend2.push(datasnd2);
                            }
                        }

                        console.log($scope.datasend1);
                        console.log($scope.datasend2);


                        $http.post(ENV.apiUrl + "api/CommentsDetailsEntryController/CUDEleteComment", $scope.datasend1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            console.log("msg", msg);

                            if ($scope.msg1 == true) {
                                console.log("deleted");
                            }
                       
                        // swal({ title: "Alert", text: "Records Inserted successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        $http.post(ENV.apiUrl + "api/CommentsDetailsEntryController/CUInsertcomment", $scope.datasend1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            console.log("msg", msg);

                            if ($scope.msg1 == true) {
                                $scope.datasend1 = [];
                                //swal({ title: "Alert", text: "Records Inserted successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                //  inserting efforts comment
                                $http.post(ENV.apiUrl + "api/CommentsDetailsEntryController/CUInsertcomment", $scope.datasend2).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    console.log("msg", msg);

                                    if ($scope.msg1 == true) {
                                        $scope.datasend2 = [];
                                        swal({ title: "Alert", text: "Records Inserted successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                        $scope.GetfinalCommentsstudents();
                                    }
                                    else {
                                        swal({ title: "Alert", text: "Records Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                    }
                                });
                            }
                            else {
                                swal({ title: "Alert", text: "Records Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                        });
                        });
                    });
                });
                $scope.busyindicator = true;
            }

            $scope.save_general_comments = function () {
                $scope.busyindicator = false;
                $scope.datasend3 = [];

                for (var i = 0; i < $scope.st_lst1.length; i++) {
                    if ($scope.st_lst1[i].subcheck == true) {
                        var datasnd = {
                            sims_comment_sr: $scope.st_lst1[i].sims_comment_sr,
                            sims_academic_year: $scope.st_lst1[i].sims_academic_year,
                            sims_cur_code: $scope.st_lst1[i].sims_cur_code,
                            sims_grade_code: $scope.st_lst1[i].sims_grade_code,
                            sims_section_code: $scope.st_lst1[i].sims_section_code,
                            sims_enroll_number: $scope.st_lst1[i].sims_enroll_number,
                            sims_term_code: $scope.fcommn.sims_term_code,
                            sims_subject_code: $scope.fcommn.sims_subject_code,
                            sims_level_code: $scope.fcommn.sims_level_code,
                            sims_report_card_code: $scope.fcommn.sims_report_card_code,
                            sims_student_comment_id: $scope.st_lst1[i].sims_student_comment_id,
                            sims_student_root_comment_display_order: $scope.st_lst1[i].sims_student_root_comment_display_order,
                            sims_comment_status: $scope.st_lst1[i].sims_comment_status,
                            sims_comment_type: $scope.st_lst1[i].sims_comment_type,
                            sims_user_code: $rootScope.globals.currentUser.username,
                            sims_student_header_code: $scope.general_head,
                            student_name: $scope.st_lst1[i].student_name,
                            sims_comment_desc: $scope.st_lst1[i].sims_comment_desc
                        }
                        $scope.datasend3.push(datasnd);
                    }
                }


                $http.post(ENV.apiUrl + "api/CommentsDetailsEntryController/CUDEleteComment", $scope.datasend3).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        console.log("deleted");
                    }
               
                    $http.post(ENV.apiUrl + "api/CommentsDetailsEntryController/CUInsertcomment", $scope.datasend3).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        $scope.datasend3 = [];
                        swal({ title: "Alert", text: "Records Inserted successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        $scope.GetfinalCommentsstudents();
                    }
                    else {
                        swal({ title: "Alert", text: "Unable to Proceed,Please select atleast one record", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    });

                });
                $scope.busyindicator = true;
            }
}]);
})();