﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Gradebook');

    simsController.controller('ScholasticEvalExamMasterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.deleteList = [];

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.getExamMaster = function () {
                $http.get(ENV.apiUrl + "api/ScholasticExamMaster/GetAllScholasticEvalExamMaster").then(function (response) {
                    $scope.examMaster = response.data;
                    $scope.totalItems = $scope.examMaster.length;
                    $scope.todos = $scope.examMaster;
                    $scope.makeTodos();
                });
            }
            $scope.getExamMaster();

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseFloat($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseFloat(begin) + parseFloat($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                $scope.selectedAll = false;
                $scope.isSelectAll();

            };

            $scope.edt = {};
            $scope.grid = true;
            $scope.display = false;

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.disabledGrade = false;
                //$scope.edt["sims_cur_code"] = '';
                //$scope.edt["sims_academic_year"] = '';
                $scope.edt["sims_term_code"] = '';
                $scope.edt["sims_exam_marks_entry_start_date"] = '';
                $scope.edt["sims_exam_mark_entry_end_date"] = '';
                $scope.edt["sims_exam_status"] = false;
                $scope.edt["sims_exam_desc"] = "";
                $scope.deleteList = [];
                $scope.disableCur = false;
                $scope.disableAcYear = false;
                $scope.disableTerm = false;
            }

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.disableCur = true;
                $scope.disableAcYear = true;
                $scope.disableTerm = true;
                $scope.edt = angular.copy(str);

                $scope.getacademicYear($scope.edt.sims_cur_code);
                $scope.Getterm1($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);

                $scope.edt = {
                    sims_cur_code: str.sims_cur_code,
                    sims_academic_year: str.sims_academic_year,
                    sims_term_code: str.sims_exam_term_code,
                    sims_exam_marks_entry_start_date: str.sims_exam_marks_entry_start_date,
                    sims_exam_mark_entry_end_date: str.sims_exam_mark_entry_end_date,
                    sims_exam_status: str.sims_exam_status,
                    sims_exam_code: str.sims_exam_code,
                    sims_exam_desc: str.sims_exam_desc
                }
                $scope.edt.sims_academic_year = str.sims_academic_year;

            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                // $scope.edt["sims_cur_code"] = '';
                //$scope.edt["sims_academic_year"] = '';
                $scope.edt["sims_term_code"] = '';
                $scope.edt["sims_exam_marks_entry_start_date"] = '';
                $scope.edt["sims_exam_mark_entry_end_date"] = '';
                $scope.edt["sims_exam_status"] = false;
                $scope.edt["sims_exam_desc"] = "";
            }


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCurriculum) {
                $scope.Curriculum = getCurriculum.data;
                $scope.edt['sims_cur_code'] = $scope.Curriculum[0].sims_cur_code;
                $scope.getacademicYear($scope.Curriculum[0].sims_cur_code);
            });

            $scope.getacademicYear = function (str) {
                $scope.AcademicYear = [];
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (getAcademicYear) {
                    $scope.AcademicYear = getAcademicYear.data;
                    $scope.edt['sims_academic_year'] = $scope.AcademicYear[0].sims_academic_year;
                    $scope.Getterm1($scope.Curriculum[0].sims_cur_code, $scope.AcademicYear[0].sims_academic_year);
                });
            }

            $scope.Getterm1 = function (curCode, acYear) {
                $scope.Termsobject = [];
                $http.get(ENV.apiUrl + "api/ScholasticExamMaster/GetTerm_p?cur_code=" + curCode + "&academic_year=" + acYear).then(function (Termsobject) {
                    $scope.Termsobject = Termsobject.data;                    
                });
            }

            $scope.Save = function (Myform) {
                if (Myform) {
                    var data = [];
                    var obj = {
                        opr: 'I',
                        sims_academic_year: $scope.edt.sims_academic_year,
                        sims_cur_code: $scope.edt.sims_cur_code,
                        sims_exam_term_code: $scope.edt.sims_term_code,
                        sims_exam_mark_entry_end_date: $scope.edt.sims_exam_mark_entry_end_date,
                        sims_exam_marks_entry_start_date: $scope.edt.sims_exam_marks_entry_start_date,
                        sims_exam_status: $scope.edt.sims_exam_status,
                        sims_exam_desc: $scope.edt.sims_exam_desc
                    }                    
                    data.push(obj);
                    
                    $http.post(ENV.apiUrl + "api/ScholasticExamMaster/CURDScholasticEvalExamMaster", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                        $scope.getExamMaster();
                        if ($scope.msg1 == true) {
                            swal({ text: 'Scholastic Evaluation Exam Added Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                            $scope.finalGrade = [];
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: 'Scholastic Eval Exam Not Added. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.Update = function (Myform) {
                if (Myform) {
                    var data = [];
                    var obj = {
                        opr: 'U',
                        sims_academic_year: $scope.edt.sims_academic_year,
                        sims_cur_code: $scope.edt.sims_cur_code,
                        sims_exam_term_code: $scope.edt.sims_term_code,
                        sims_exam_mark_entry_end_date: $scope.edt.sims_exam_mark_entry_end_date,
                        sims_exam_marks_entry_start_date: $scope.edt.sims_exam_marks_entry_start_date,
                        sims_exam_status: $scope.edt.sims_exam_status,
                        sims_exam_code: $scope.edt.sims_exam_code,
                        sims_exam_desc: $scope.edt.sims_exam_desc
                    }
                    
                    data.push(obj);
                    
                    $http.post(ENV.apiUrl + "api/ScholasticExamMaster/CURDScholasticEvalExamMaster", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                        $scope.getExamMaster();
                        if ($scope.msg1 == true) {
                            swal({ text: 'Scholastic Evaluation Exam Updated Successfully', imageUrl: "assets/img/check.png", width: 330, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: 'Scholastic Evaluation Exam Not Updated. ', imageUrl: "assets/img/close.png", width: 330, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.isSelectAll = function () {
                $scope.deleteList = [];
                if ($scope.selectedAll) {
                    $scope.selectedAll = true;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.deleteList.push($scope.filteredTodos[i].sims_exam_code);
                    }
                    $scope.row1 = 'row_selected';
                }
                else {
                    $scope.selectedAll = false;
                    $scope.row1 = '';
                }

                angular.forEach($scope.filteredTodos, function (item) {
                    item.ischecked = $scope.selectedAll;
                });

            }

            $scope.checkIfAllSelected = function () {
                var id = event.target.id;
                var value = event.target.value;
                if (document.getElementById(id).checked) {
                    $scope.deleteList.push(value);
                    if ($scope.deleteList.length == $scope.filteredTodos.length) {
                        $scope.selectedAll = true;
                    }
                    $("#" + id).closest('tr').addClass("row_selected");
                } else {
                    $scope.selectedAll = false;
                    $scope.row1 = '';
                    var index = $scope.deleteList.indexOf(value);
                    $scope.deleteList.splice(index, 1);
                    $("#" + id).closest('tr').removeClass("row_selected");
                }                
            };

            $scope.Delete = function () {

                var data = [];

                for (var i = 0; i < $scope.deleteList.length; i++) {
                    var obj = {
                        opr: 'D',
                        sims_exam_code: $scope.deleteList[i]
                    }
                    data.push(obj);
                }
                
                if (data.length > 0) {

                    swal({
                        title: '',
                        text: "Are you sure you want to delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/ScholasticExamMaster/CURDScholasticEvalExamMaster", data).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: 'Scholastic Evaluation Exam Deleted Successfully', imageUrl: "assets/img/check.png", width: 340, showCloseButton: true });
                                    $scope.display = false;
                                    $scope.grid = true;
                                    $scope.getExamMaster();
                                    $scope.deleteList = [];
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: 'Scholastic Evaluation Exam Not Deleted. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            })

                        }
                        else {
                            $scope.selectedAll = false;
                            $scope.isSelectAll();
                        }
                    })

                }
                else {
                    swal({ text: 'Select Atleast One Record To Delete', imageUrl: "assets/img/notification-alert.png", width: 320, showCloseButton: true });
                }
            }


            $scope.size = function (str) {

                if (str == 'All') {

                    $scope.totalItems = $scope.examMaster.length;
                    $scope.todos = $scope.examMaster;
                    $scope.filteredTodos = $scope.GetAllStudentFee;
                    $scope.numPerPage = $scope.examMaster.length;
                    $scope.maxSize = $scope.examMaster.length
                    $scope.makeTodos();
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.examMaster, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.examMaster;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in 2 fields */
                return (item.sims_cur_full_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_academic_year_description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_term_desc_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_exam_marks_entry_start_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_exam_mark_entry_end_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

        }])
})();