﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('StudentBellowspecifiedAverageCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.temp = {};
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();            
            $scope.display = false;
            $scope.dublicate = false;
            $scope.temp.sims_opr_code = 'A';
            var user = $rootScope.globals.currentUser.username;


            $(function () {
               // $('#grade_box').multipleSelect({ width: '100%' });
               // $('#section_box').multipleSelect({ width: '100%' });
                $('#subject_box').multipleSelect({ width: '100%' });
                $('#term_box').multipleSelect({ width: '100%' });

            });

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_start_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });


            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_end_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });



           

            $http.get(ENV.apiUrl + "api/bellwAverage/getCuriculum").then(function (res1) {
                debugger
                $scope.sims_cur = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_cur_code'] = res1.data[0].sims_cur_code;

                    $scope.getAccYear(res1.data[0].sims_cur_code);
                    $scope.getAllsubject_type(res1.data[0].sims_cur_code);
                }
            });

            $scope.getAccYear = function (cur_code) {
                debugger;
                $http.get(ENV.apiUrl + "api/bellwAverage/getAcademicYear?curCode=" + cur_code).then(function (res1) {
                    $scope.academic_year = res1.data;
                    if (res1.data.length > 0) {
                        $scope.temp['sims_academic_year'] = res1.data[0].sims_academic_year;
                        $scope.getAllGrades(cur_code, res1.data[0].sims_academic_year)
                        $scope.getAllTerm(cur_code, res1.data[0].sims_academic_year)
                        $scope.getAllsubject_Code(cur_code, res1.data[0].sims_academic_year)
                    }
                });

            }

            $scope.getAllGrades = function (cur_code, academic_year) {
                debugger
                $http.get(ENV.apiUrl + "api/bellwAverage/getAllGradesNew?cur_code=" + cur_code + "&academic_year=" + academic_year).then(function (res1) {
                    $scope.grade_code = res1.data;
                    setTimeout(function () {
                        //$('#grade_box').change(function () {

                        //}).multipleSelect({
                        //    width: '100%'
                        //});
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }

            $scope.getSectionFromGrade = function (cur_code, academic_year, grade_code) {
                debugger
                $http.get(ENV.apiUrl + "api/bellwAverage/getSectionFromGradeNew?cur_code=" + cur_code + "&academic_year=" + academic_year + "&grade_code=" + grade_code + "&user=" + user).then(function (res1) {
                    $scope.section_code = res1.data;

                    setTimeout(function () {
                        //$('#section_box').change(function () {

                        //}).multipleSelect({
                        //    width: '100%'
                        //});
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }

            $scope.getAllsubject_type = function (cur_code) {
                debugger
                $http.get(ENV.apiUrl + "api/bellwAverage/getsubjecttype?cur_code=" + cur_code).then(function (res1) {
                    $scope.type_code = res1.data;
                    $scope.temp.sims_subject_type_code = res1.data[0].sims_subject_type_code;
                    $scope.getAllsubject_Code($scope.temp.sims_cur_code,$scope.temp.sims_academic_year,$scope.temp.sims_grade_code,$scope.temp.sims_section_code,$scope.temp.sims_subject_type_code);
                    setTimeout(function () {
                       // $('#term_box').change(function () {

                        //})

                        //    .multipleSelect({
                        //    width: '100%'
                        //});
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }

            $scope.getAllsubject_Code = function (cur_code, academic_year, grade_code, section_code, subject_type) {
                debugger
                $http.get(ENV.apiUrl + "api/bellwAverage/getsubjectname?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&subject_type=" + $scope.temp.sims_subject_type_code).then(function (res1) {
                    $scope.subject_code = res1.data;
                    setTimeout(function () {
                        $('#subject_box').change(function () {

                        })

                            .multipleSelect({
                            width: '100%'
                        });
                        $("#subject_box").multipleSelect("checkAll");
                    }, 1000);
                    
                    });
            }


            $scope.getAllTerm = function (cur_code, academic_year) {
                debugger
                $http.get(ENV.apiUrl + "api/bellwAverage/getAllTerm?cur_code=" + cur_code + "&academic_year=" + academic_year).then(function (res1) {
                    $scope.term_code = res1.data;
                    setTimeout(function () {
                        $('#term_box').change(function () {
                            debugger
                        })

                            .multipleSelect({
                            width: '100%'
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }



            $scope.BellowAverageReport = function () {

                $scope.term_nm = $('#term_box').multipleSelect('getSelects', 'text');
                $scope.subject_nm = $('#subject_box').multipleSelect('getSelects', 'text');
                if ($scope.temp.sims_opr_code == 'A') {
                    $scope.view_for = 'Regular'
                }
                else {
                    $scope.view_for = 'Retest'
                    $scope.temp.sims_opr_code = 'R';
                }
                
                debugger
                $scope.colsvis = false;
                if ($scope.subject_code.length == $scope.temp.sims_subject_code.length) {
                    $scope.temp.sims_subject_code = '00'
                }
                if ($scope.term_code.length == $scope.temp.sims_term_code.length) {
                    $scope.temp.sims_term_code = '04'
                }
                $http.get(ENV.apiUrl + "api/bellwAverage/getbellowAverageReport?opr=" + $scope.temp.sims_opr_code + "&cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&term_code=" + $scope.temp.sims_term_code + "&subject_type=" + $scope.temp.sims_subject_type_code + "&subject_code=" + $scope.temp.sims_subject_code + "&value=" + $scope.temp.sims_average_marks_value).then(function (res1) {
                    if (res1.data.length > 0) {
                        $scope.report_data = res1.data;
                        angular.forEach($scope.report_data, function (f) {
                            f.student_cnt = parseFloat(f.student_cnt);
                        });
                      
                    }
                    else {
                        swal({ title: "Alert", text: " Data is not Available for selected criteria", showCloseButton: true, width: 300, height: 200 });
                         $scope.report_data = [];
                    }
                    //angular.forEach($scope.report_data, function (f) {
                    //    f.sims_bell_lecture_per_week = parseFloat(f.sims_bell_lecture_per_week);
                    //});
                    console.log($scope.report_data, $scope.temp.sims_subject_type_code);
                });
            }




            var data_send = [];
            var data1 = [];




            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }


            $scope.reset_data = function () {
                debugger;
                // $scope.temp.sims_grade_code = '';


                try {
                    $('#subject_box').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#term_box').multipleSelect('uncheckAll');
                } catch (e) {

                }
                $scope.temp.sims_subject_code = '';
                $scope.temp.sims_term_code = '';
                $scope.temp.sims_average_marks_value = '';
                $scope.temp.sims_grade_code = '';
                $scope.temp.sims_section_code = '';
                $scope.report_data = [];


            }


            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            //$scope.colsvis = false;


            $scope.exportData = function () {
                //$scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('rpt_data').innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        saveAs(blob, "StudentAverageBellowSpeciedMarks.xls");
                        //$scope.colsvis = false;

                    }

                });
            };

            $scope.print = function (div) {

                //$scope.colsvis = false;

                var docHead = document.head.outerHTML;
                var printContents = document.getElementById('rpt_data').outerHTML;
                //$scope.colsvis = false;
                var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";


                var newWin = window.open("", "_blank", winAttr);
                var writeDoc = newWin.document;
                writeDoc.open();
                writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                writeDoc.close();
                newWin.focus();

            };

          
        }])

})();

