﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ActivityIndicatorCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.deleteList = [];

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.getGroupIndicator = function () {
                $http.get(ENV.apiUrl + "api/ActivityIndicator/GetAllActivityGroupIndicator").then(function (response) {
                    $scope.groupIndicator = response.data;
                    $scope.totalItems = $scope.groupIndicator.length;
                    $scope.todos = $scope.groupIndicator;
                    $scope.makeTodos();
                });
            }
            $scope.getGroupIndicator();

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseFloat($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseFloat(begin) + parseFloat($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);                
            };

            $scope.edt = {};
            $scope.grid = true;
            $scope.display = false;

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.disabledGrade = false;
                //$scope.edt["sims_cur_code"] = '';
                //$scope.edt["sims_academic_year"] = '';
                $scope.edt["sims_activity_group_code"] = '';
                $scope.edt["sims_indicator_group_short_name"] = '';
                $scope.edt["sims_indicator_group_name"] = '';
                $scope.edt["sims_indicator_group_desc"] = '';
                $scope.edt["sims_indicator_group_remark"] = '';
                $scope.edt["sims_indicator_group_value"] = '';
                $scope.edt["sims_status"] = false;
                $scope.disableCur = false;
                $scope.disableAcYear = false;
                $scope.disableGroupCode = false;
            }

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.disableCur = true;
                $scope.disableAcYear = true;
                $scope.disableGroupCode = true;
                $scope.edt = angular.copy(str);

                $scope.getacademicYear($scope.edt.sims_cur_code);
                $scope.GetActivityGroup($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);

                $scope.edt = {
                    sims_cur_code: str.sims_cur_code,
                    sims_academic_year: str.sims_academic_year,
                    sims_activity_group_code: str.sims_activity_group_code,
                    sims_indicator_group_short_name: str.sims_indicator_group_short_name,
                    sims_indicator_group_name: str.sims_indicator_group_name,
                    sims_indicator_group_desc: str.sims_indicator_group_desc,
                    sims_indicator_group_remark: str.sims_indicator_group_remark,
                    sims_indicator_group_value: str.sims_indicator_group_value,
                    sims_status: str.sims_status,
                    sims_indicator_group_code: str.sims_indicator_group_code,
                }
                $scope.edt.sims_academic_year = str.sims_academic_year;

            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                // $scope.edt["sims_cur_code"] = '';
                //$scope.edt["sims_academic_year"] = '';                
                $scope.edt["sims_activity_group_code"] = '';
                $scope.edt["sims_indicator_group_short_name"] = '';
                $scope.edt["sims_indicator_group_name"] = '';
                $scope.edt["sims_indicator_group_desc"] = '';
                $scope.edt["sims_indicator_group_remark"] = '';
                $scope.edt["sims_indicator_group_value"] = '';
                $scope.edt["sims_status"] = false;
            }


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCurriculum) {
                $scope.Curriculum = getCurriculum.data;
                $scope.edt['sims_cur_code'] = $scope.Curriculum[0].sims_cur_code;
                $scope.getacademicYear($scope.Curriculum[0].sims_cur_code);
            });

            $scope.getacademicYear = function (str) {
                $scope.AcademicYear = [];
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (getAcademicYear) {
                    $scope.AcademicYear = getAcademicYear.data;
                    $scope.edt['sims_academic_year'] = $scope.AcademicYear[0].sims_academic_year;
                    $scope.GetActivityGroup($scope.Curriculum[0].sims_cur_code, $scope.AcademicYear[0].sims_academic_year);
                });
            }

            $scope.GetActivityGroup = function (curCode, acYear) {
                $scope.groupObject = [];
                $http.get(ENV.apiUrl + "api/ActivityIndicator/GetActivityGroup?cur_code=" + curCode + "&academic_year=" + acYear).then(function (groupObject) {
                    $scope.groupObject = groupObject.data;                    
                });
            }

            $scope.Save = function (Myform) {
                if (Myform) {
                    var data = [];                    
                    var obj = {
                        opr: 'I',
                        sims_academic_year: $scope.edt.sims_academic_year,
                        sims_cur_code: $scope.edt.sims_cur_code,
                        sims_activity_group_code:$scope.edt.sims_activity_group_code,
                        sims_indicator_group_short_name: $scope.edt.sims_indicator_group_short_name,
                        sims_indicator_group_name: $scope.edt.sims_indicator_group_name,
                        sims_indicator_group_desc: $scope.edt.sims_indicator_group_desc,
                        sims_indicator_group_remark: $scope.edt.sims_indicator_group_remark,
                        sims_indicator_group_value: parseFloat($scope.edt.sims_indicator_group_value),
                        sims_status: $scope.edt.sims_status
                    }                    
                    data.push(obj);
                    
                    $http.post(ENV.apiUrl + "api/ActivityIndicator/CURDActivityIndicatorGroup", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                        $scope.getGroupIndicator();
                        if ($scope.msg1 == true) {
                            swal({ text: 'Activity Group Indicator Added Successfully', imageUrl: "assets/img/check.png", width: 330, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;                            
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: 'Activity Group Indicator Not Added. ', imageUrl: "assets/img/close.png", width: 330, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.Update = function (Myform) {
                
                if (Myform) {
                   
                    var data = [];
                    var obj = {
                        opr: 'U',
                        sims_academic_year: $scope.edt.sims_academic_year,
                        sims_cur_code: $scope.edt.sims_cur_code,
                        sims_activity_group_code: $scope.edt.sims_activity_group_code,
                        sims_indicator_group_short_name: $scope.edt.sims_indicator_group_short_name,
                        sims_indicator_group_name: $scope.edt.sims_indicator_group_name,
                        sims_indicator_group_desc: $scope.edt.sims_indicator_group_desc,
                        sims_indicator_group_remark: $scope.edt.sims_indicator_group_remark,
                        sims_indicator_group_value: parseFloat($scope.edt.sims_indicator_group_value),
                        sims_status: $scope.edt.sims_status,
                        sims_indicator_group_code: $scope.edt.sims_indicator_group_code
                    }                    
                    data.push(obj);
                    
                    $http.post(ENV.apiUrl + "api/ActivityIndicator/CURDActivityIndicatorGroup", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                        $scope.getGroupIndicator();
                        if ($scope.msg1 == true) {
                            swal({ text: 'Activity Group Indicator Updated Successfully', imageUrl: "assets/img/check.png", width: 330, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: 'Activity Group Indicator Not Updated. ', imageUrl: "assets/img/close.png", width: 330, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }
           
           
            $scope.size = function (str) {

                if (str == 'All') {

                    $scope.totalItems = $scope.groupIndicator.length;
                    $scope.todos = $scope.groupIndicator;
                    $scope.filteredTodos = $scope.GetAllStudentFee;
                    $scope.numPerPage = $scope.groupIndicator.length;
                    $scope.maxSize = $scope.groupIndicator.length
                    $scope.makeTodos();
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.groupIndicator, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.groupIndicator;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in 2 fields */
                return (item.sims_cur_full_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_academic_year_description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_indicator_group_short_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_indicator_group_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_indicator_group_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }
            

        }])
})();