﻿(function () {
    'use strict';
    var teacher_code = [];
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('MarkEntryChangeReportCont',
    ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
        $scope.display = true;
        $scope.busyindicator = false;
        $scope.show_ins_table_loading = false;
        //$scope.balance;

        $http.get(ENV.apiUrl + "api/LibraryAttribute/getCuriculum").then(function (res1) {
            $scope.cur_data = res1.data;
            $scope.sims_Cur = $scope.cur_data[0].sims_cur_code;
            $scope.getAcademic_year($scope.cur_data[0].sims_cur_code);
        });

        $scope.getAcademic_year = function (cur_code1) {
            debugger
            $http.get(ENV.apiUrl + "api/LibraryAttribute/getAcademicYear?curCode=" + cur_code1).then(function (res1) {
                $scope.acad_data = res1.data;
                $scope.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                $scope.getAllGrades();
               $scope.getterm();
            });
        }

        $scope.getAllGrades = function () {
            debugger
            $http.get(ENV.apiUrl + "api/LibraryAttribute/getAllGrades?cur_code=" + $scope.sims_Cur + "&academic_year=" + $scope.sims_academic_year).then(function (res1) {
                $scope.grade_data = res1.data;
                //   $scope.sims_grade_code = $scope.grade_data[0].sims_grade_code;
                //  $scope.getSectionFromGrade($scope.sims_Cur, $scope.sims_grade_code,$scope.sims_academic_year);
                setTimeout(function () {
                    $('#grade_box').change(function () {

                    }).multipleSelect({
                        width: '100%'
                    });
                    //$("#show_box").multipleSelect("checkAll");
                }, 1000);

            });
        }

        $scope.getterm = function () {
            debugger;
            $http.get(ENV.apiUrl + "api/common/GetRCTerm?curcode=" + $scope.sims_Cur + "&ayear=" + $scope.sims_academic_year).then(function (res1) {
                $scope.term_data = res1.data;

                setTimeout(function () {
                    $('#term_box').change(function () { }).multipleSelect({
                        width: '100%'
                    }); }, 1000);

                $scope.getcategory();
                
        });
       
        }
      
      

        $scope.getSectionFromGrade = function (cur_code1, grade_code1, academic_year1) {

            $http.get(ENV.apiUrl + "api/LibraryAttribute/getSectionFromGrade?cur_code=" + $scope.sims_Cur + "&grade_code=" + $scope.sims_grade_code + "&academic_year=" + $scope.sims_academic_year).then(function (res1) {
                $scope.section_data = res1.data;
                //  $scope.sims_section_code = $scope.section_data[0].sims_section_code;
                setTimeout(function () {
                    $('#section_box').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                    //$("#show_box").multipleSelect("checkAll");
                }, 1000);
            });
        }

        $(function () {
            $('#section_box').multipleSelect({ width: '100%' });
        });

        $scope.getSubjectsFromGrade = function (cur_code1, academic_year1,grade_code1,section_code ) {

            $http.get(ENV.apiUrl + "api/MarksEntryChangeReport/getSubjectsFromGrade?cur_code=" + cur_code1 + "&academic_year=" + academic_year1 + "&grade_code=" + grade_code1  + "&section_code=" + section_code).then(function (res1) {
                $scope.subject_data = res1.data;
                //  $scope.sims_section_code = $scope.section_data[0].sims_section_code;
                setTimeout(function () {
                    $('#subject_box').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                    //$("#show_box").multipleSelect("checkAll");
                }, 1000);
            });
        }
        $(function () {
            $('#subject_box').multipleSelect({ width: '100%' });
        });

  
        $scope.getcategory = function () {
            $http.get(ENV.apiUrl + "api/MarksEntryChangeReport/getcategory?cur_code=" + $scope.sims_Cur + "&academic_year=" + $scope.sims_academic_year + "&grade_code=" + $scope.sims_grade_code + "&section_code=" + $scope.sims_section_code + "&subject_code=" + $scope.sims_subject_code + "&term_code=" + $scope.sims_term_code + "&opr=" + 'C').then(function (res1) {
                $scope.category_data = res1.data;
                //  $scope.sims_section_code = $scope.section_data[0].sims_section_code;
                setTimeout(function () {
                    $('#category_box').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                    //$("#show_box").multipleSelect("checkAll");
                }, 1000);
            });
        }
        

        $scope.getcategoryassignment = function (cur_code1, academic_year1, grade_code1, section_code, subject_code,category,term_code) {
            debugger
            $http.get(ENV.apiUrl + "api/MarksEntryChangeReport/getcategoryassignment?cur_code=" + cur_code1 + "&academic_year=" + academic_year1 + "&grade_code=" + grade_code1 + "&section_code=" + section_code + "&subject_code=" + subject_code + "&category=" + category + "&term_code=" + term_code + "&opr=" + 'D').then(function (res1) {
                $scope.assignment_data = res1.data;
                //  $scope.sims_section_code = $scope.section_data[0].sims_section_code;
                setTimeout(function () {
                    $('#assignment_box').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                    //$("#show_box").multipleSelect("checkAll");
                }, 1000);
            });
        }
        $(function () {
            $('#assignment_box').multipleSelect({ width: '100%' });
        });

        $scope.getStudentMarksList = function (cur_code1, academic_year1, grade_code1, section_code,subject_code,term_code,cat_code,assign_number) {
            debugger
            $http.get(ENV.apiUrl + "api/MarksEntryChangeReport/getStudentMarksList?cur_code=" + cur_code1 + "&academic_year=" + academic_year1 + "&grade_code=" + grade_code1 + "&section_code=" + section_code + "&subject_code=" + subject_code
                + "&term_code=" + term_code + "&cat_code=" + cat_code + "&assign_number=" + assign_number).then(function (res1) {
                $scope.student_data = res1.data;
                console.log($scope.student_data);
            });
        }

        $scope.colname = null;
        $scope.reverse = false;

        $scope.sort = function (col) {
            $scope.colname = col;
            $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
        }


    }]);
})();