﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('RoleBasedAccessReportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            // Start
            //$scope.name = "Subject Teacher List";
            $scope.temp = {};
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.temp = {};
            $scope.display = false;
            $scope.dublicate = false;
            var user = $rootScope.globals.currentUser.username;

            $scope.getAllGrades = function (cur_code, academic_year) {
                $http.get(ENV.apiUrl + "api/RoleBasedAccessReport/getAllGradesNew?cur_code=" + cur_code + "&academic_year=" + academic_year + "&user=" + user).then(function (res1) {
                    $scope.grade_code = res1.data;
                    setTimeout(function () {
                        $('#grade_box').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }

            $scope.getAccYear = function (cur_code) {
                debugger
                $http.get(ENV.apiUrl + "api/RoleBasedAccessReport/getAcademicYear?curCode=" + cur_code).then(function (res1) {
                    $scope.academic_year = res1.data;
                    if (res1.data.length > 0) {
                        $scope.temp['sims_academic_year'] = res1.data[0].sims_academic_year;
                        $scope.getAllGrades(cur_code, res1.data[0].sims_academic_year)
                        $scope.getAllTerm(cur_code, res1.data[0].sims_academic_year)
                    }
                });

            }
            
            $http.get(ENV.apiUrl + "api/RoleBasedAccessReport/getCuriculum").then(function (res1) {
                $scope.sims_cur = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_cur_code'] = res1.data[0].sims_cur_code;

                    $scope.getAccYear(res1.data[0].sims_cur_code);
                }
            });

            $(function () {
                $('#grade_box').multipleSelect({ width: '100%' });
                $('#section_box').multipleSelect({ width: '100%' });
                
            });

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_start_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });


            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_end_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });


            $scope.getSectionFromGrade = function (cur_code, academic_year, grade_code) {
                $http.get(ENV.apiUrl + "api/RoleBasedAccessReport/getSectionFromGradeNew?cur_code=" + cur_code + "&academic_year=" + academic_year + "&grade_code=" + grade_code + "&user=" + user).then(function (res1) {
                    $scope.section_code = res1.data;

                    setTimeout(function () {
                        $('#section_box').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }

            $scope.getAllTerm = function (cur_code, academic_year) {
                debugger
                $http.get(ENV.apiUrl + "api/RoleBasedAccessReport/getAllTerm?cur_code=" + cur_code + "&academic_year=" + academic_year).then(function (res1) {
                    $scope.term_code = res1.data;
                    setTimeout(function () {
                        $('#term_box').change(function () {

                        })

                        //    .multipleSelect({
                        //    width: '100%'
                        //});
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }



            $scope.RoleBasedAccessReport = function () {
                debugger
                $scope.colsvis = false;
                if ($scope.temp.sims_bell_lecture_per_week == undefined) $scope.temp.sims_bell_lecture_per_week = '';
                $http.get(ENV.apiUrl + "api/RoleBasedAccessReport/getRoleBasedAccessReport?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&term_code=" + $scope.temp.sims_term_code+"&user="+user).then(function (res1) {
                    if (res1.data.length > 0) {
                        $scope.report_data = res1.data;
                        angular.forEach($scope.report_data, function (f) {
                            f.student_cnt = parseFloat(f.student_cnt);
                        });
                    }
                    else {
                        swal({ title: "Alert", text: " Data is not Available for selected date", showCloseButton: true, width: 300, height: 200 });
                    }
                    //angular.forEach($scope.report_data, function (f) {
                    //    f.sims_bell_lecture_per_week = parseFloat(f.sims_bell_lecture_per_week);
                    //});
                    console.log($scope.report_data);
                });
            }


         

            var data_send = [];
            var data1 = [];
           



            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }





            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            //$scope.colsvis = false;


            $scope.exportData = function () {
                //$scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('rpt_data').innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        saveAs(blob, "DetentionSummaryReport.xls");
                        //$scope.colsvis = false;

                    }

                });
            };

            $scope.print = function (div) {

                //$scope.colsvis = false;

                var docHead = document.head.outerHTML;
                var printContents = document.getElementById('rpt_data').outerHTML;
                //$scope.colsvis = false;
                var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";


                var newWin = window.open("", "_blank", winAttr);
                var writeDoc = newWin.document;
                writeDoc.open();
                writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                writeDoc.close();
                newWin.focus();

            };

            $scope.Report_call = function (str) {
                debugger
                $http.get(ENV.apiUrl + "api/RoleBasedAccessReport/getReportFromProc?report_call=" + str.report_call).then(function (res1) {
                    $scope.result1 = res1.data;


                    var data = {

                        location: $scope.result1[0].sims_appl_form_field,//"Gradebook.GBR039QR", 
                        parameter: {
                            cur_code: str.sims_cur_code,
                            acad_year: str.sims_academic_year,
                            grade_code: str.sims_grade_code,
                            section_code: str.sims_section_code,
                            term_code: str.sims_term_code,
                            sub_code: str.sims_subject_code,
                            serch_student: null,
                        },
                        state: 'main.RBAR01',
                        ready: function () {
                            this.refreshReport();
                        },
                    }
                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                    $state.go('main.ReportCardParameter')


                });
                
            //    console.log(str.sims_cur_code, str.sims_academic_year, str.sims_grade_code, str.sims_section_code, str.sims_term_code, str.sims_subject_code);
                
            }



            //With Enroll number

            $scope.Report_call1 = function (str) {
                debugger
                $http.get(ENV.apiUrl + "api/RoleBasedAccessReport/getReportFromProc?report_call=" + str.report_call).then(function (res1) {
                    $scope.result = res1.data;


                    var data = {

                        location: $scope.result[0].sims_appl_form_field,
                        parameter: {
                            cur_code: str.sims_cur_code,
                            acad_year: str.sims_academic_year,
                            grade_code: str.sims_grade_code,
                            section_code: str.sims_section_code,
                            term_code: str.sims_term_code,
                            serch_student: str.sims_enroll_number,
                            sub_code: str.sims_subject_code
                        },
                        state: 'main.RBAR01',
                        ready: function () {
                            this.refreshReport();
                        },
                    }
                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                    $state.go('main.ReportCardParameter')



                });

                //console.log(str.sims_cur_code, str.sims_academic_year, str.sims_grade_code, str.sims_section_code, str.sims_term_code, str.sims_enroll_number, str.sims_subject_code);
               
            }

        }])

})();

