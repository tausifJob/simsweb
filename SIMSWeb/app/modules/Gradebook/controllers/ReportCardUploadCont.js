﻿(function () {
    'use strict';
    var selected_enroll_number = [];
    var opr = '';
    var main;
    var comn_files = [];
    var datasend = [];
    var file_doc = [];
    var file_temp_doc = [];
    var sims_doc_line = 1;
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ReportCardUploadCont',['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {
        $scope.display = false;
        $scope.grid = true;
        $scope.hide_item = true;
        $scope.pagesize = "10";
        $scope.pageindex = "1";
        var arr_files = [];
        var comp_code = "1"
        var fortype = '';

        var arr_files1 = [];
        $scope.uploading_doc1 = true;
        $scope.images = [];
        $scope.images1 = [];
        $scope.StudentsList_Data = [];
        $scope.student = [];
        $scope.edt = {}
        $scope.emp = {}
        var comn_files = "";
        $scope.obj = [];
        $scope.enrollno = true;
        var formdata = new FormData();

        var user = $rootScope.globals.currentUser.username;

        $timeout(function () {
            $("#fixTable").tableHeadFixer({
                'top': 1
            });
        }, 100);

        $timeout(function () {
            $("#fixTable1").tableHeadFixer({
                'top': 1
            });

        }, 100);

        $http.get(ENV.apiUrl + "api/AssignmentUpload/GetTextFormat").then(function (GetTextFormat) {
            $scope.GetTextFormat = GetTextFormat.data;
            for (var i = 0; i < $scope.GetTextFormat.length; i++) {
                $scope.fileformats = $scope.fileformats + '.' + $scope.GetTextFormat[i].file_format + ',';
            }
        });

        $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

        $scope.makeTodos = function () {
            var rem = parseInt($scope.totalItems % $scope.numPerPage);
            if (rem == '0') {
                $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
            } else {
                $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
            }
            var begin = (($scope.currentPage - 1) * $scope.numPerPage);
            var end = parseInt(begin) + parseInt($scope.numPerPage);



            $scope.filteredTodos = $scope.todos.slice(begin, end);
        };

        $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
            $scope.curriculum = res.data;
            $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
            $scope.getacyr($scope.edt.sims_cur_code)
        })

        $scope.getacyr = function (str) {
            $scope.cur_code = str;
            $http.get(ENV.apiUrl + "api/AssignmentUpload/GetAcademicYear").then(function (GetAcademicYear) {
                $scope.Academic_year = GetAcademicYear.data;
                $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.edt['sims_academic_year'] = $scope.Academic_year[0].sims_academic_year;
                $scope.edt['sims_assignment_status'] = true;
                $scope.getGrade(user, $scope.Academic_year[0].sims_academic_year, $scope.edt.sims_cur_code)
            })
        }

        $scope.getGrade = function (username, acadyear, curcode) {
            //$http.get(ENV.apiUrl + "api/AssignmentUpload/getGradeByUsername?username=" + user + "&acadyear=" + $scope.edt.sims_academic_year + "&curcode=" + $scope.edt.sims_cur_code).then(function (getGradeByUsername_Data) {
            $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (getGradeByUsername_Data) {
                $scope.GradeByUsername = getGradeByUsername_Data.data;
                setTimeout(function () {
                    $('#cmb_grade').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });
        }

        var sims_grade_code = '';
        var sims_section_code = '';

        $scope.select_grd_mult = function () {

            sims_grade_code = '';
            for (var a = 0; a < $scope.edt.sims_grade_code.length; a++) {
                sims_grade_code = sims_grade_code + $scope.edt.sims_grade_code[a] + ',';
            }

            //$http.get(ENV.apiUrl + "api/AssignmentUpload/getSectionByUserGrade?grade=" + sims_grade_code + "&username=" + user + "&acadyear=" + $scope.edt.sims_academic_year + "&curcode=" + $scope.edt.sims_cur_code).then(function (getSectionByUserGrade) {
            $http.get(ENV.apiUrl + "api/StudentReport/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + sims_grade_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (getSectionByUserGrade) {
                $scope.getSectionByUserGrade = getSectionByUserGrade.data;
                setTimeout(function () {
                    $('#cmb_section').change(function () { }).multipleSelect({
                        width: '100%'
                    });
                    $("#cmb_section").multipleSelect("checkAll");
                }, 1000);

            });

        }

        $scope.select_sec_mult = function () {
            var seccoll = [];
            sims_section_code = '';
            for (var a = 0; a < $scope.edt.sims_section_code.length; a++) {

                sims_section_code = sims_section_code + $scope.edt.sims_section_code[a] + ',';
            }

        }

        $(function () {
            $('#cmb_grade').multipleSelect({
                width: '100%'
            });
            $('#cmb_section').multipleSelect({
                width: '100%'
            });
        });

        $scope.edit = function (str) {
            $scope.display = true;
            $scope.grid = false;
            $scope.save1 = false;
            $scope.update1 = true;
            $scope.delete1 = false;
            $scope.temp = str;
            $scope.enrollno = false;
            $scope.ClearCheckBox();
            $scope.student = [];
            $scope.emp = {};
            $scope.file_doc = [];
            $http.get(ENV.apiUrl + "api/StudentReport/getReprotcardlevel?cur_name=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&grd_code=" + str.sims_grade_code + "&sec_code=" + str.sims_section_code).then(function (res1) {
                $scope.Reportlevel = res1.data;
                for (var i = 0; i <= $scope.Reportlevel.length; i++) {
                    if ($scope.temp.sims_assignment_desc == $scope.Reportlevel[i].sims_assignment_term_code) {
                        $scope.emp['sims_assignment_term_code'] = $scope.temp.sims_assignment_desc;
                    }
                }
            });
            if ($scope.temp.sims_assignment_doc_path_name != ''){
                var t = {
                    sims508_doc: 0,
                    sims_doc_line: '1',
                    sims508_doc_name: $scope.temp.sims_assignment_doc_path_name,
                }
                $scope.file_doc.push(t);
            }
        }

        $scope.Show_Data = function () {
            $http.get(ENV.apiUrl + "api/StudentReport/getStudentReportCard?cur_name=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&grd_code=" + sims_grade_code + "&sec_code=" + sims_section_code).then(function (res) {
                $scope.AssignmentDetailData = res.data;
                $scope.totalItems = $scope.AssignmentDetailData.length;
                $scope.todos = $scope.AssignmentDetailData;
                $scope.makeTodos();
                if ($scope.AssignmentDetailData.length == 0) {
                    swal({ title: 'Alert', text: "Record Not Found...", showCloseButton: true, width: 450, height: 200 });
                }
                else {
                }
            })
        }

        $scope.getreportcode = function () {
            $http.get(ENV.apiUrl + "api/StudentReport/getReprotcardcode?cur_name=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&grd_code=" + $scope.temp.sims_grade_code + "&sec_code=" + $scope.temp.sims_section_code + "&report_levelcode=" + $scope.emp.sims_assignment_term_code).then(function (res2) {
                $scope.Reportcode = res2.data;
                for (var i = 0; i <= $scope.Reportcode.length; i++) {
                    if ($scope.temp.sims_assignment_doc_line_no == $scope.Reportcode[i].sims_gb_cat_code) {
                        $scope.emp['sims_gb_cat_code'] = $scope.temp.sims_assignment_doc_line_no;
                    }
                }
            });
        }

        $scope.CheckAllChecked = function () {
            main = document.getElementById('mainchk');
            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                var v = document.getElementById($scope.filteredTodos[i].sims_student_enroll_number + i);
                if (main.checked == true) {
                    v.checked = true;
                    $('tr').addClass("row_selected");
                }

                else {
                    v.checked = false;
                    $('tr').removeClass("row_selected");
                }
            }
        }

        $scope.checkonebyonedelete = function () {

            //main = document.getElementById('mainchk');

            //if (main.checked == true) {
            //    main.checked = false;
            //    $scope.row1 = '';
            //} else {
            //    main.checked = false;
            //    $scope.row1 = '';
            //}

            $("input[type='checkbox']").change(function (e) {
                if ($(this).is(":checked")) {
                    $(this).closest('tr').addClass("row_selected");
                    $scope.color = '#edefef';
                } else {
                    $(this).closest('tr').removeClass("row_selected");
                    $scope.color = '#edefef';
                }
            });

            main = document.getElementById('mainchk');
            if (main.checked == true) {
                main.checked = false;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                    }
                });
            }

        }

        $scope.cancel = function () {

            $scope.display = false;
            $scope.grid = true;
            $scope.MyForm.$setPristine();
            $scope.MyForm.$setUntouched();
            $scope.file_doc = [];
            $scope.student = [];
            $scope.emp = '';

        }

        var formdata = new FormData();

        $scope.getTheFiles = function ($files) {

            angular.forEach($files, function (value, key) {
                formdata.append(key, value);
            });
        };

        var file_name = '';
        var file_name_name = '';
        $scope.file_doc = [];

        $scope.file_changed = function (element) {
            $scope.file_doc = [];
            file_name = '';
            file_name_name = '';
            fortype = '';
           //var v = new Date();
            var v= $filter('date')(new Date(), 'dd-MM-yyyy')
            if ($scope.file_doc == undefined) {
                $scope.file_doc = '';
            }

            if ($scope.file_doc.length > 1) {
                swal('', 'Upload maximum 1 files.');

            } else {
                
                file_name = 'Report_Card_' + $scope.temp.sims_student_enroll_number + '_' + v;
                $scope.photofile = element.files[0];
                file_name_name = $scope.photofile.name;
                var len = 0;
                len = file_name_name.split('.');
                fortype = file_name_name.split('.')[len.length - 1];
                var formatbool = false;
                for (var i = 0; i < $scope.GetTextFormat.length; i++) {
                    if ($scope.GetTextFormat[i].file_format == fortype)
                        formatbool = true;
                }
                if (formatbool == true) {
                    if ($scope.photofile.size > 2097152) {
                        swal('', 'File size limit not exceed upto 2 MB.')
                    } else {
                        $scope.uploading_doc1 = false;
                        $scope.photo_filename = ($scope.photofile.type);
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $scope.$apply(function () {
                                $scope.prev_img = e.target.result;
                                var request = {
                                    method: 'POST',
                                    url: ENV.apiUrl + 'api/StudentReport/upload712?filename=' + file_name + '.' + fortype + "&location=" + "ResultUpload",
                                    data: formdata,
                                    headers: {
                                        'Content-Type': undefined
                                    }
                                };
                                $http(request).success(function (d) {


                                    var t = {
                                        sims508_doc: d,
                                        sims_doc_line: sims_doc_line,
                                        sims508_doc_name: file_name_name,
                                    }
                                    $scope.file_doc.push(t);
                                    sims_doc_line++;
                                    $scope.uploading_doc1 = true;
                                    //file_doc = [];
                                    //for (var i = 0; i < $scope.teacher_doc.length; i++) {
                                    //    var c = {
                                    //        sims509_doc: $scope.teacher_doc[i].sims508_doc,
                                    //        sims_doc_line: i + 1
                                    //    }
                                    //    file_doc.push(c);
                                    //}

                                    //$scope.maindata.file = d;

                                });

                            });
                        };
                        reader.readAsDataURL($scope.photofile);
                    }
                } else {
                    swal('', '.' + fortype + ' File format not allowed.');
                }
            }

        }

        $scope.downloaddoc1 = function (str) {
            $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/ResultUpload/' + str;
            window.open($scope.url);
        }

        $scope.ViewDocument = function (info) {
            $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/ResultUpload/' + info;
            window.open($scope.url);
        }

        $scope.CancelFileUpload = function (idx) {
            $scope.images.splice(idx, 1);

        };

        var sims_student_enrolls = [];
      
        $scope.size = function (str) {
            /*
			$scope.pagesize = str;
			$scope.currentPage = 1;
			$scope.numPerPage = str; */
            main = document.getElementById('mainchk');
            if (main.checked == true) {
                main.checked = false;
                $scope.row1 = '';
                $scope.color = '#edefef';
            }

            if (str == "All") {
                $scope.currentPage = 1;
                $scope.numPerPage = $scope.AssignmentDetailData.length;
                $scope.makeTodos();

            } else {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }
        }

        $scope.index = function (str) {
            $scope.pageindex = str;
            $scope.currentPage = str;

            $scope.makeTodos();

            main = document.getElementById('mainchk');
            if (main.checked == true) {
                main.checked = false;
                $scope.row1 = '';
                $scope.color = '#edefef';
            }
        }

        $scope.Update = function () {

            var Savedata = [];

            if ($scope.temp.sims_assignment_number == '' || $scope.temp.sims_assignment_number == undefined)
                $scope.temp.sims_assignment_number = 1;

            if (file_name == '' || file_name == undefined)
                file_name = $scope.temp.sims_assignment_doc_path;

            var data = {
                sims_assignment_number:$scope.temp.sims_assignment_number,
                sims_cur_code: $scope.temp.sims_cur_code,
                sims_academic_year: $scope.temp.sims_academic_year,
                sims_grade_code: $scope.temp.sims_grade_code,
                sims_section_code: $scope.temp.sims_section_code,
                sims_assignment_term_code: $scope.emp.sims_assignment_term_code,
                sims_gb_cat_code: $scope.emp.sims_gb_cat_code,
                sims_student_enroll_number: $scope.temp.sims_student_enroll_number,
                sims_assignment_doc_path: file_name + '.' + fortype,
                sims_assignment_status: $scope.edt.sims_assignment_status,
                opr: 'I'
            }

            Savedata.push(data);

            $http.post(ENV.apiUrl + "api/StudentReport/CUDResultUpload", Savedata).then(function (MSG1) {
                $scope.msg1 = MSG1.data;
                $scope.grid = true;
                $scope.display = false;
                
                if ($scope.msg1.strMessage != undefined) {
                    swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                }

                $scope.Show_Data();
                $scope.emp = {};
                $scope.file_doc = [];
                fortype = '';
            });
        }

        $scope.deleterecord = function () {
            var report712 = [];
            $scope.flag = false;
            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                var v = document.getElementById($scope.filteredTodos[i].sims_student_enroll_number + i);
                
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deleteroutecode = ({
                            'sims_assignment_number': $scope.filteredTodos[i].sims_assignment_number,
                            'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                            'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                            'sims_assignment_term_code': $scope.filteredTodos[i].sims_assignment_term_code,
                            'sims_gb_cat_code': $scope.filteredTodos[i].sims_gb_cat_code,
                            'sims_student_enroll_number': $scope.filteredTodos[i].sims_student_enroll_number,
                            'sims_assignment_doc_path': $scope.filteredTodos[i].sims_assignment_doc_path,
                            'sims_assignment_status':  $scope.filteredTodos[i].sims_assignment_status,
                            opr: 'D'
                        });
                        report712.push(deleteroutecode);
                    }
                
            }

            if ($scope.flag) {
                swal({
                    title: '',
                    text: "Are you sure you want to Delete?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        $http.post(ENV.apiUrl + "api/StudentReport/CUDResultUpload", report712).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1.strMessage != undefined) {
                                swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                            }
                        });
                    }
                    else {
                        for (var i = 0; i < $scope.filteredTodos.length; i++) {
                            var v = document.getElementById($scope.filteredTodos[i].sims_student_enroll_number + i);
                            if (v.checked == true) {
                                v.checked = false;
                                $scope.row1 = '';
                                main.checked = false;
                                $('tr').removeClass("row_selected");
                            }
                        }
                    }

                    $scope.Show_Data();
                    $scope.emp = {};
                    $scope.file_doc = [];
                });
            }
            else {
                swal({text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png",
                    showCloseButton: true, width: 380,
                });
            }

            $scope.currentPage = true;

        }

        $scope.searched = function (valLists, toSearch) {
            return _.filter(valLists,
				function (i) {
				    /* Search Text in all  fields */
				    return searchUtil(i, toSearch);
				});
        };

        $scope.search = function () {
            $scope.todos = $scope.searched($scope.AssignmentDetailData, $scope.searchText);
            $scope.totalItems = $scope.todos.length;
            $scope.currentPage = '1';
            if ($scope.searchText == '') {
                $scope.todos = $scope.AssignmentDetailData;
            }
            $scope.makeTodos();
        }

        function searchUtil(item, toSearch) {
            /* Search Text in all 3 fields */

            return (
				item.sims_student_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
				item.sims_student_passport_first_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
				item.sims_assignment_doc_path_name == toSearch) ? true : false;
        }

        $scope.Reset = function () {
            $scope.temp = '';
        }

        $scope.ClearCheckBox = function () {

            main = document.getElementById('mainchk');

            if (main.checked == true) {
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_student_enrolls;
                    var v = document.getElementById(t);
                    v.checked = false;
                    selected_enroll_number = [];
                    $scope.row1 = '';

                }
                main.checked = false;
            }

        }

        /*
        $scope.RemoveEnrollMentNo = function ($event, index, str) {
            str.splice(index, 1);
        }

        $scope.MultipleStudentSelect = function (str) {
            if (str) {
                for (var i = 0; i < $scope.StudentsList_Data.length; i++) {
                    $scope.StudentsList_Data[i]['stud_chk'] = true;
                }
            } else {
                for (var i = 0; i < $scope.StudentsList_Data.length; i++) {
                    $scope.StudentsList_Data[i]['stud_chk'] = false;
                    $scope.row1 = '';
                }
            }
        }
        */

        $scope.student = [];
        var data1 = [];
        $scope.DataEnroll = function () {
            for (var i = 0; i < $scope.StudentsList_Data.length; i++) {
                var t = $scope.StudentsList_Data[i].sims_student_enroll_number;
                var v = document.getElementById(t + i);
                if (v.checked == true) {
                    data1 = $scope.StudentsList_Data[i];
                    $scope.student.push(data1);
                }
            }
        }

        $('*[data-datepicker="true"] input[type="text"]').datepicker({

            todayBtn: true,
            orientation: "top left",
            autoclose: true,
            todayHighlight: true,
            format: "dd-mm-yyyy"
        });

        $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
            $('input[type="text"]', $(this).parent()).focus();
        });

        simsController.directive('ngFiles', ['$parse', function ($parse) {

            function fn_link(scope, element, attrs) {
                var onChange = $parse(attrs.ngFiles);
                element.on('change', function (event) {
                    onChange(scope, {
                        $files: event.target.files
                    });
                });
            };

            return {
                link: fn_link
            }
        }])

        $scope.deletedoc = function (str) {

            file_temp_doc = [];
            //for (var i = 0; i < $scope.file_doc.length; i++) {
            //    if ($scope.file_doc[i].sims_doc_line == str.sims_doc_line && $scope.file_doc[i].sims508_doc == str.sims508_doc) {
            //        $scope.file_doc.splice(i, 1);
            //        sims_doc_line--;
            //        sims_doc_line: i--;
            //    }
            //}

            for (var i = 0; i < $scope.file_doc.length; i++) {
                if ($scope.file_doc[i].sims_doc_line != str.sims_doc_line) {
                    var x = {
                        sims508_doc: $scope.file_doc[i].sims508_doc,
                        sims508_doc_name: $scope.file_doc[i].sims508_doc_name,
                    }
                    file_temp_doc.push(x);
                }
            }
            $scope.file_doc = [];

            var j = 1;
            for (var i = 0; i < file_temp_doc.length; i++) {
                var c = {
                    sims508_doc: file_temp_doc[i].sims508_doc,
                    sims508_doc_name: $scope.file_doc[i].sims508_doc_name,
                    sims_doc_line: i + 1
                }
                $scope.file_doc.push(c);
                //sims_doc_line = i + 1;
                j++;
            }
            sims_doc_line = j;


        }

        $scope.Clear = function () {
            $scope.edt = [];
            $scope.file_doc = [];
            $scope.student = [];
            $scope.enrollno = false;
        }

        $timeout(function () {
            $("#fixTable2").tableHeadFixer({
                'top': 1
            });
            $("#fixTable1").tableHeadFixer({
                'top': 1
            });

        }, 100);

        $timeout(function () {
            $("#fixTable").tableHeadFixer({ 'top': 1 });
        }, 0);

    }])
})();