﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Gradebook');
    debugger
    simsController.controller('Co-ScholsaticConfigCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.showAll = false;
            $scope.showData = false;
            $scope.attr = {};
            $scope.config1 !== undefined && ($scope.config1.sims_report_card_config_desc = '', $scope.config1.sims_report_card_config_status = true)
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.Allcurriculum = res.data;
                if (res.data.length > 0) {
                    $scope.curr_code = res.data[0].sims_cur_code;

                    $scope.getAcademicYear($scope.curr_code);
                }

                $scope.closeAddNew();

            });
            
            $scope.getAcademicYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (res) {
                    $scope.AllAcademicYear = res.data;
                    $scope.showAll = false;
                    $scope.showData = false;

                    if (res.data.length > 0) {
                        $scope.aca_year = res.data[0].sims_academic_year;
                        $scope.getConfig();
                    }
                });
            };

            $scope.get_group_score_type = function () {
                $http.get(ENV.apiUrl + "api/co_scholastic/get_group_score_type").then(function (group_score_type) {
                    $scope.group_score_type = group_score_type.data.table;
                    $scope.get_group_scale_code();
                });

            }

            $scope.getresult_type = function () {
                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllresultType").then(function (GetAllresultType) {
                    $scope.Getresulttype = GetAllresultType.data.table;

                });

            }

            $scope.getAllTeachers = function () {
                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllTeachersName?grade_code=" + $scope.conindic.sims_grade_code + "&section_code=" + $scope.conindic.sims_section_code + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (GetAllTeachersName) {
                    $scope.GetAllteacher = GetAllTeachersName.data.table;
                });

            }

            $scope.get_group_scale_code = function () {
                $http.get(ENV.apiUrl + "api/co_scholastic/Get_group_scale_code?academic_year=" + $scope.aca_year + "&cur_code=" + $scope.curr_code).then(function (group_scale_code) {
                    $scope.group_scale = group_scale_code.data.table;

                    $scope.getresult_type();
                });
            }

            $scope.getConfig = function () {
                //var a = { opr: 'A', cur_code: $scope.curr_code, aca_year: $scope.aca_year };
                //$http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                //    $scope.AllConfigs = res.data.table;
                //    $scope.showData = true;
                //});

                debugger
                $http.get(ENV.apiUrl + "api/co_scholastic/get_config_details?academic_year=" + $scope.aca_year + "&cur_code=" + $scope.curr_code).then(function (res) {
                    $scope.AllConfigs = res.data.table;
                    $scope.showData = true;
                });

            }

            $scope.getDataByConfig = function (e, config) {
                $scope.config_code = config.sims_report_card_config_code;
                $("#config_div .div_item").removeClass('bg-red').addClass('bg-grey-md');
                $(".div_item h6").removeClass('text-white');
                if ($(e.currentTarget).hasClass('div_item')) {
                    $(e.currentTarget).addClass('bg-red').removeClass('bg-grey-md');
                    $(e.currentTarget).find('h6').addClass('text-white');
                }
                else {
                    $(e.currentTarget).parents('.div_item').addClass('bg-red').removeClass('bg-grey-md');
                    $(e.currentTarget).parents('.div_item').find('h6').addClass('text-white');
                }
                $scope.getAssignmentDataByConfig();
                $scope.getGrade();
                $scope.showAll = true;
            }

            $scope.getGrade = function () {
                $scope.gradeSecArr = [];
                var a = { opr: 'E', cur_code: $scope.curr_code, aca_year: $scope.aca_year, config_code: $scope.config_code };
                $http.post(ENV.apiUrl + "api/co_scholastic/ImportSavedConfigSecData?cur_code=" + $scope.curr_code + "&&aca_year=" + $scope.aca_year + "&&config_code=" + $scope.config_code).then(function (res) {
                    //console.log("GetGrade Function", res.data);
                    $scope.gradeSecArr = res.data;
                    console.log("gradeSecArr co-scho", $scope.gradeSecArr);

                    $($scope.gradeSecArr).each(function (key, val) {
                        val.grade_check = 'I';
                        $(val.section).each(function (k, v) {
                            if (v.status == 'A') {
                                val.grade_check = 'A';
                            }
                        })
                    })

                    //console.log("GetGrade Function after check", res.data);
                });
            }

            $scope.accordion = function (e, str) {
                e.stopPropagation();
                e.preventDefault();
                if (e.target.tagName == 'LABEL') {
                    if ($(e.target).parents('.grades_div').find('.grade_select_checkbox').hasClass('checked')) {
                        $(e.target).parents('.grades_div').find('.grade_select_checkbox').removeClass('checked');
                        $(e.target).parents('.grades_div').find('.grade_select_checkbox input').prop('checked', false);
                        $(e.target).parents('.grades_div').find('.section_select_checkbox').removeClass('checked');
                        $(e.target).parents('.grades_div').find('.section_select_checkbox input').prop('checked', false);
                    }
                    else {
                        $(e.target).parents('.grades_div').find('.grade_select_checkbox').addClass('checked');
                        $(e.target).parents('.grades_div').find('.grade_select_checkbox input').prop('checked', true);

                        setTimeout(function () {
                            $(e.target).parents('.grades_div').find('.section_select_checkbox').addClass('checked');
                            $(e.target).parents('.grades_div').find('.section_select_checkbox input').prop('checked', true);

                        }, 200);
                    }
                }
                else if (e.target.tagName == 'I' || e.target.tagName == 'H5') {
                    $(e.target).parents('.grades_div').siblings('.grades_div').find('.grade_section_container').hide().addClass('closed');

                    $(e.target).parents('.grades_div').find('.grade_section_container').slideToggle().toggleClass('closed');
                }

            }

            $scope.saveConfigGrade = function () {
                debugger
                //var term_arr = [];
                //for (var s = 0; s < $scope.AllTerms.length; s++) {
                //    if ($scope.AllTerms[s].sims_report_card_term_status == 'A') {
                //        term_arr.push($scope.AllTerms[s]);
                //    }
                //}

                console.log($scope.gradeSecArr);
                $scope.saveData = [];

                for (var k = 0; k < $scope.AllAssignment.length; k++) {

                    if ($scope.AllAssignment[k].chk_assign_config == true) {
                        for (var i = 0; i < $scope.gradeSecArr.length; i++) {
                            for (var j = 0; j < $scope.gradeSecArr[i].section.length; j++) {
                                var t = 'section_' + $scope.gradeSecArr[i].grade_code + '_' + $scope.gradeSecArr[i].section[j].sims_section_code;
                                var v = document.getElementById(t);
                                if (v.checked == true) {
                                    var d = {
                                        'aca_year': $scope.aca_year,
                                        'cur_code': $scope.curr_code,
                                        'grade_code': $scope.gradeSecArr[i].grade_code,
                                        'sims_section_code': $scope.gradeSecArr[i].section[j].sims_section_code,
                                        'config_code': $scope.config_code,
                                        'status': 'A',
                                        'sims_report_card_frequncy': $scope.AllAssignment[k].sims_report_card_assign_number,
                                        'sims_report_card_frequncy_flag': $scope.AllAssignment[k].flg,
                                    }
                                    $scope.saveData.push(d);
                                }
                                else {
                                    var d = {
                                        'aca_year': $scope.aca_year,
                                        'cur_code': $scope.curr_code,
                                        'grade_code': $scope.gradeSecArr[i].grade_code,
                                        'sims_section_code': $scope.gradeSecArr[i].section[j].sims_section_code,
                                        'config_code': $scope.config_code,
                                        'status': 'I',
                                        'sims_report_card_frequncy': $scope.AllAssignment[k].sims_report_card_assign_number,
                                        'sims_report_card_frequncy_flag': $scope.AllAssignment[k].flg,
                                    }
                                    $scope.saveData.push(d);
                                }
                            }
                        }
                    }

                }
                var gradeSecArr = [];

                gradeSecArr = $scope.saveData;

                console.log("gradeSecArr ", gradeSecArr);

                //$('.grade_heading').each(function (k, v) {
                //    secArr = [];
                //    var grade_code = $(this).find('input:checkbox').val();
                //    var grade_name = $(this).find('input:checkbox').data('gradename');
                //    var issecsele, isgradesele;

                //    $(this).parent().find('.section_select_checkbox').each(function (key, val) {
                //        if (val.children[0].checked) {
                //            issecsele = 'A';
                //        }
                //        else {
                //            issecsele = 'I';
                //        }
                //        var sec_code = $(this).find('input:checkbox').val();
                //        var sec_namecode = $(this).find('input:checkbox').data('secname');

                //        secArr.push({ 'sims_section_code': sec_code, 'sims_section_name': sec_namecode, 'status': issecsele });
                //    })

                //    gradeSecArr.push({ 'grade_code': grade_code, 'grade_name': grade_name, 'aca_year': $scope.aca_year, 'cur_code': $scope.cur_code, 'config_code': $scope.config_code, 'section': secArr });
                //});



                //console.log('gradeSecArr =', gradeSecArr);
                $http.post(ENV.apiUrl + "api/co_scholastic/CoscholasticConfigSave?cTemp=AA", gradeSecArr).then(function (res) {
                    if (res.data == true) {
                        swal({ title: "Alert", text: "Configuration Created successfully.", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    }
                    else {
                        swal({ title: "Alert", text: "Unable To Create Configuration. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                });
            }

            $scope.addNew = function (e, str) {
                $scope.add_or_edit = 'add';
                $scope.closeAddNew();
                $scope.setcontrols();
                $scope.addNewScreen = true;
                if (str == 'Groups') {
                    $scope.group = {};
                    $scope.btn_group = true;
                    $scope.actionForm = 'Groups';
                    $scope.term_status = true;
                    $scope.addNewgroup = true;
                    $scope.getallMasterGroup();
                    // $scope.get_group_score_type();
                }

                else if (str == 'Attributes') {
                    $scope.actionForm = 'Attributes';
                    $scope.assign_include_in_final_grade = true;
                    $scope.assign_status = true;
                    $scope.addNewAttribute = true;
                    $scope.btn_attri = true;
                    $scope.attr = {};

                    $scope.selected_group_lst = [];
                    for (var i = 0; i < $scope.getAllGroups.length; i++) {
                        if ($scope.getAllGroups[i].sims_status) {
                            $scope.selected_group_lst.push($scope.getAllGroups[i]);
                        }

                    }
                    $scope.GetAllGroupAttriData_function1();
                    //$scope.get_group_scale_code();
                }
                else if (str == 'Indicators') {
                    $scope.indi = {};
                    $scope.actionForm = 'Indicators';
                    $scope.cat_status = true;
                    $scope.includeFinnal = true;
                    $scope.cat_config_status = true;
                    $scope.addNewindi = true;
                    $scope.btn_indic = true;
                    $scope.get_group_score_type();
                }

            }

            $scope.closeAddNew = function () {
                $scope.addNewScreen = false;
                $scope.addNewindi = false;
                $scope.addNewAttribute = false;
                $scope.IndicConfigShow = false;
                $scope.addNewgroup = false;
            }

            $scope.getAssignmentDataByConfig = function () {
                $http.get(ENV.apiUrl + "api/co_scholastic/get_co_scholasticNewCommon?academic_year=" + $scope.aca_year + "&cur_code=" + $scope.curr_code + "&config_code=" + $scope.config_code).then(function (res) {
                    $scope.AllAssignment = res.data.table;
                    console.log("AllAssignment", $scope.AllAssignment);
                    $scope.term = '';
                    for (var i = 0; i < $scope.AllAssignment.length; i++) {
                        //$scope.AllAssignment[i].chk_assign_config = true;
                        $scope.term = $scope.term + $scope.AllAssignment[i].sims_report_card_assign_number + $scope.AllAssignment[i].flg + ','
                        if ($scope.AllAssignment[i].sims_report_card_frequncy == undefined || $scope.AllAssignment[i].sims_report_card_frequncy == null || $scope.AllAssignment[i].sims_report_card_frequncy == "") {

                        }
                        else {
                            $scope.AllAssignment[i].chk_assign_config = true;
                        }
                    }




                    $scope.getAllGroups_function();
                });
            }

            $scope.Save_Group_Btn_Click = function (isvalid) {
                var data_lst = [];

                if (isvalid == true) {
                    debugger
                    for (var i = 0; i < $scope.AllAssignment.length; i++) {
                        if ($scope.AllAssignment[i].chk_assign_config) {
                            var data = {}
                            data = angular.copy($scope.group);
                            data['opr'] = "GIC";
                            data['sims_report_card_config_code'] = $scope.config_code;
                            data['sims_academic_year'] = $scope.aca_year;
                            data['sims_cur_code'] = $scope.curr_code;
                            data['sims_co_scholastic_group_heading'] = $scope.AllAssignment[i].sims_report_card_assign_number;
                            data['sims_co_scholastic_group_note'] = $scope.AllAssignment[i].flg;
                            data_lst.push(data);
                        }
                    }




                    $http.post(ENV.apiUrl + "api/co_scholastic/CoScholGroupDataOprN", data_lst).then(function (res) {
                        $scope.message = res.data;
                        if ($scope.message == true) {
                            swal({ text: 'Group Created Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                            $scope.getAllGroups_function();
                        }
                        else {
                            swal({ text: 'Error In Group Creating', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                        }

                    });
                }
            }

            $scope.Update_Group_Btn_Click = function (isvalid) {

                if (isvalid == true) {

                    $scope.group['opr'] = "GUC";
                    $http.post(ENV.apiUrl + "api/co_scholastic/CoScholGroupDataOpr", $scope.group).then(function (res) {
                        $scope.message = res.data;
                        if ($scope.message == true) {
                            swal({ text: 'Group Updated Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                            $scope.getAllGroups_function();
                        }
                        else {
                            swal({ text: 'Error In Group Updating', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                        }

                    });
                }
            }

            $scope.Save_Attribute_Btn_Click = function (isvalid) {

                if (isvalid) {
                    $scope.DataObject = [];
                    for (var i = 0; i < $scope.AllAssignment.length; i++) {
                        if ($scope.AllAssignment[i].chk_assign_config) {
                            var data = {};
                            data.sims_co_scholastic_group_code = $scope.attr.sims_co_scholastic_group_code;
                            data.sims_cur_code = $scope.curr_code;
                            data.sims_academic_year = $scope.aca_year;
                            data.sims_co_scholastic_attribute_code = $scope.attr.sims_co_scholastic_attribute_code;
                            data.sims_co_scholastic_attribute_name_ot = $scope.attr.sims_co_scholastic_attribute_name_ot;
                            data.sims_co_scholastic_group_grade_scale_code = $scope.attr.sims_co_scholastic_group_grade_scale_code;
                            data.sims_co_scholastic_attribute_display_order = $scope.attr.sims_co_scholastic_attribute_display_order;
                            data.sims_co_scholastic_attribute_status = $scope.attr.sims_co_scholastic_attribute_status;
                            data['sims_report_card_config_code'] = $scope.config_code;
                            data.term_code = $scope.AllAssignment[i].sims_report_card_assign_number;
                            data.flag = $scope.AllAssignment[i].flg;

                            data.opr = "AIC";
                            $scope.DataObject.push(data);
                        }

                    }



                    $http.post(ENV.apiUrl + "api/co_scholastic/CoScholAttributeDataOprN", $scope.DataObject).then(function (attrmessage) {
                        $scope.message = attrmessage.data;
                        if ($scope.message == true) {
                            swal({ text: 'Attribute Created Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                            $scope.GetAllGroupAttriData_function();
                        }
                        else {
                            swal({ text: 'Error In Attribute Creating', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                        }
                    });
                }
            }

            $scope.Update_Attribute_Btn_Click = function (isvalid) {
                if (isvalid) {
                    $scope.attr['opr'] = "AU";
                    $scope.dataobject = [];
                    $scope.dataobject.push($scope.attr);

                    $http.post(ENV.apiUrl + "api/co_scholastic/CoScholAttributeDataOpr", $scope.dataobject).then(function (attrmessage) {
                        $scope.message = attrmessage.data;
                        if ($scope.message == true) {
                            swal({ text: 'Attribute Updated Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                            $scope.GetAllGroupAttriData_function();
                        }
                        else {
                            swal({ text: 'Error In Attribute Updating', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                        }
                    });
                }
            }

            $scope.Save_Indicator_Btn_Click = function (isvalid) {

                if (isvalid) {
                    $scope.indi.sims_cur_code = $scope.curr_code;
                    $scope.indi.sims_academic_year = $scope.aca_year;
                    $scope.indi.opr = "II";

                    $http.post(ENV.apiUrl + "api/co_scholastic/CoScholIndicatorDataOpr", $scope.indi).then(function (CoScholIndicatorData) {
                        $scope.message = CoScholIndicatorData.data;
                        if ($scope.message == true) {
                            swal({ text: 'Indicator Created Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                            $scope.GetAllGroupIndicData_function();
                        }
                        else {
                            swal({ text: 'Error In Indicator Creating', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                        }
                    });
                }
            }

            $scope.Update_Indicator_Btn_Click = function (isvalid) {

                if (isvalid) {
                    $scope.indi['opr'] = "IU";
                    $http.post(ENV.apiUrl + "api/co_scholastic/CoScholIndicatorDataOpr", $scope.indi).then(function (attrmessage) {
                        $scope.message = attrmessage.data;
                        if ($scope.message == true) {
                            swal({ text: 'Indicator Updated Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                            $scope.GetAllGroupIndicData_function();
                        }
                        else {
                            swal({ text: 'Error In Indicator Updating', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                        }
                    });
                }
            }

            $scope.getAllGroups_function = function () {
                $scope.GetAllGroupAttri = [];
                $scope.GetAllGroupIndicData = [];
                $scope.AllGroupIndicDataConfigdata = [];
                // $http.get(ENV.apiUrl + "api/co_scholastic/GetAllCoScholData?config_code=" + $scope.config_code + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (getAllGroups) {
                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllCoScholData1?config_code=" + $scope.config_code + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year + "&term=" + $scope.term).then(function (getAllGroups) {
                    $scope.getAllGroups = getAllGroups.data;
                    if ($scope.getAllGroups.length > 0) {
                        $scope.GetAllGroupAttriData_function();
                    } else {
                        $scope.closeAddNew();
                    }
                });
            }

            $scope.GetAllGroupAttriData_function = function () {
                $scope.GetAllGroupAttri = [];
                $scope.GetAllGroupIndicData = [];

                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllGroupAttriData?config_code=" + $scope.config_code + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (GetAllGroupAttri) {
                    $scope.GetAllGroupAttri = GetAllGroupAttri.data;
                    if ($scope.GetAllGroupAttri.length > 0) {
                        $scope.GetAllGroupIndicData_function();
                    } else {
                        $scope.closeAddNew();
                    }
                })
            }

            $scope.GetAllGroupIndicData_function = function () {
                $scope.GetAllGroupIndicData = [];
                $scope.AllGroupIndicDataConfigdata = [];
                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllGroupIndicData?config_code=" + $scope.config_code + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (GetAllGroupIndicData) {
                    $scope.GetAllGroupIndicData = GetAllGroupIndicData.data;
                    if ($scope.GetAllGroupIndicData.length > 0) {
                        $scope.GetAllGroupIndicDataConfigdata();
                    } else {
                        $scope.closeAddNew();
                    }
                })
            }

            $scope.setcontrols = function () {
                $scope.Group_Form.$setPristine();
                $scope.Group_Form.$setUntouched();
                $scope.Attribute_Form.$setPristine();
                $scope.Attribute_Form.$setUntouched();
                $scope.Indicator_Form.$setPristine();
                $scope.Indicator_Form.$setUntouched();
            }

            $scope.Edit_Master_Group = function (str) {
                $scope.setcontrols();


                $scope.edit_status = false;
                //for (var i = 0; i < $scope.getAllGroups.length; i++) {
                //    if ($scope.getAllGroups[i].sims_status == true) {
                //        $scope.group = angular.copy($scope.getAllGroups[i]);
                //        $scope.edit_status = true;
                //        break;
                //    }
                //}

                $scope.group = str;
                $scope.edit_status = true;

                if ($scope.edit_status == false) {
                    swal({ text: 'select atleast one Group', imageUrl: "assets/img/notification-alert.png", width: 300, height: 250, showCloseButton: true });
                } else {

                    $scope.btn_group = false;
                    $scope.add_or_edit = 'add';
                    $scope.closeAddNew();
                    $scope.addNewScreen = true;
                    $scope.actionForm = 'Groups';
                    $scope.addNewgroup = true;
                    $scope.get_group_score_type();

                }
            }

            $scope.Edit_Master_Attribute = function () {

                $scope.setcontrols();

                $scope.btn_attri = false;

                $scope.edit_status = false;
                for (var i = 0; i < $scope.GetAllGroupAttri.length; i++) {
                    if ($scope.GetAllGroupAttri[i].sims_status == true) {
                        $scope.attr = angular.copy($scope.GetAllGroupAttri[i]);
                        $scope.edit_status = true;
                        break;
                    }
                }

                if ($scope.edit_status != true) {
                    swal({ text: 'Select atleast one Attribute', imageUrl: "assets/img/notification-alert.png", width: 300, height: 250, showCloseButton: true });
                } else {
                    $scope.add_or_edit = 'add';
                    $scope.closeAddNew();
                    $scope.addNewScreen = true;
                    $scope.actionForm = 'Attributes';
                    $scope.assign_include_in_final_grade = true;
                    $scope.assign_status = true;
                    $scope.addNewAttribute = true;
                    $scope.get_group_score_type();
                }
            }

            $scope.Edit_Master_Indicator = function () {
                $scope.setcontrols();

                $scope.edit_status = false;
                $scope.btn_indic = false;
                for (var i = 0; i < $scope.GetAllGroupIndicData.length; i++) {
                    if ($scope.GetAllGroupIndicData[i].sims_status == true) {
                        $scope.indi = angular.copy($scope.GetAllGroupIndicData[i]);
                        $scope.edit_status = true;
                        break;
                    }
                }

                if ($scope.edit_status != true) {
                    swal({ text: 'Select atleast one Indicator', imageUrl: "assets/img/notification-alert.png", width: 300, height: 250, showCloseButton: true });
                }
                else {
                    $scope.add_or_edit = 'add';
                    $scope.closeAddNew();
                    $scope.actionForm = 'Indicators';
                    $scope.cat_status = true;
                    $scope.includeFinnal = true;
                    $scope.cat_config_status = true;
                    $scope.addNewindi = true;
                    $scope.addNewScreen = true;
                    $scope.get_group_score_type();
                }

            }

            $scope.Save_Indic_Config_Btn_Click = function (isvalid) {
                if (isvalid) {
                    $scope.dataobject = [];
                    for (var i = 0; i < $scope.AllAssignment.length; i++) {
                        if ($scope.AllAssignment[i].chk_assign_config == true) {

                            $scope.conindic['opr'] = 'CI';
                            $scope.conindic['sims_co_scholastic_discriptive_indicator_code'] = $scope.congiginfo.sims_co_scholastic_discriptive_indicator_code;
                            $scope.conindic['sims_co_scholastic_attribute_code'] = $scope.conindic.config_code.split('/')[0];
                            $scope.conindic['sims_co_scholastic_group_code'] = $scope.conindic.config_code.split('/')[1];
                            $scope.conindic['sims_academic_year'] = $scope.congiginfo.sims_academic_year;
                            $scope.conindic['sims_cur_code'] = $scope.congiginfo.sims_cur_code;
                            $scope.conindic['sims_co_scholastic_teacher_code'] = $scope.conindic.sims_co_scholastic_teacher_code;
                            $scope.conindic['sims_report_card_config_code'] = $scope.AllAssignment[i].sims_report_card_config_code;
                            $scope.conindic['sims_report_card_term_code'] = $scope.AllAssignment[i].sims_report_card_term_code;
                            $scope.conindic['sims_report_card_category_code'] = $scope.AllAssignment[i].sims_report_card_category_code;
                            $scope.conindic['sims_report_card_assign_number'] = $scope.AllAssignment[i].sims_report_card_assign_number;
                            $scope.conindic['sims_report_card_assign_max_score'] = $scope.AllAssignment[i].sims_report_card_assign_max_score;
                            $scope.conindic['sims_co_scholastic_grading_scale_code'] = $scope.congiginfo.sims_co_scolastic_grading_scale;
                            $scope.dataobject.push($scope.conindic);
                        }
                    }
                    $http.post(ENV.apiUrl + "api/co_scholastic/CUDGroupAttriIndicConfigData", $scope.dataobject).then(function (GetAllGroupIndicConfig) {
                        $scope.message = GetAllGroupIndicConfig.data;
                        if ($scope.message == true) {
                            swal({ text: 'Indicator Configured Successfully', imageUrl: "assets/img/check.png", width: 300, height: 250, showCloseButton: true });
                            $scope.GetAllGroupIndicDataConfigdata();

                        } else {
                            swal({ text: 'Indicator Not Configured', imageUrl: "assets/img/close.png", width: 300, height: 250, showCloseButton: true });
                            $scope.closeAddNew();
                        }
                    });
                }
            }

            $scope.GetAllGroupIndicDataConfigdata = function () {
                $scope.AllGroupIndicDataConfigdata = [];
                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllGroupIndicconfigData?config_code=" + $scope.config_code + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (AllGroupIndicDataConfigdata) {
                    $scope.AllGroupIndicDataConfigdata = AllGroupIndicDataConfigdata.data;
                    $scope.closeAddNew();
                });
            }

            $scope.Edit_Indicator_Config_Btn_Click = function (info) {

                $scope.btn_conf = true;
                $scope.setcontrols();
                $scope.conindic = {};
                $scope.add_or_edit = 'add';
                $scope.closeAddNew();
                $scope.IndicConfigShow = true;
                $scope.addNewScreen = true;
                $scope.TilePostFix = info.sims_co_scholastic_discriptive_indicator_name;
                $scope.congiginfo = info;

                $scope.Config_Data_Object = [];
                for (var i = 0; i < $scope.AllGroupIndicDataConfigdata.length; i++) {
                    if ($scope.AllGroupIndicDataConfigdata[i].sims_co_scholastic_discriptive_indicator_code == info.sims_co_scholastic_discriptive_indicator_code) {
                        $scope.Config_Data_Object.push($scope.AllGroupIndicDataConfigdata[i]);
                    }
                }

                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllGrade?config_code=" + $scope.config_code + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (GetAllGrade) {
                    $scope.GetAllGrade = GetAllGrade.data;
                });
            }

            $scope.get_sections = function () {

                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllGradeSection?config_code=" + $scope.config_code + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year + "&grade_code=" + $scope.conindic.sims_grade_code).then(function (GetAllGradeSection) {
                    $scope.GetAllGradeSection = GetAllGradeSection.data;
                    $scope.getAllTeachers();
                });
            }

            $scope.Update_Config_indic = function (info) {
                $scope.setcontrols();
                $scope.btn_conf = false;
                $scope.conindic = info;
                $scope.conindic['config_code'] = info.sims_co_scholastic_attribute_code + '/' + info.sims_co_scholastic_group_code;
                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllGrade?config_code=" + $scope.config_code + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (GetAllGrade) {
                    $scope.GetAllGrade = GetAllGrade.data;
                    $scope.get_sections();
                });
            }

            $scope.Update_Indic_Config_Btn_Click = function (isvalid) {


                if (isvalid) {
                    for (var i = 0; i < $scope.GetAllGroupIndicData.length; i++) {
                        $scope.GetAllGroupIndicData[i].sims_status = false;
                    }

                    $scope.conindic['opr'] = 'CU';

                    $http.post(ENV.apiUrl + "api/co_scholastic/CUDGroupAttriIndicConfigData", $scope.conindic).then(function (GetAllGroupIndicConfig) {
                        $scope.message = GetAllGroupIndicConfig.data;
                        if ($scope.message == true) {
                            swal({ text: 'Indicator Configuration Updated', imageUrl: "assets/img/check.png", width: 300, height: 250, showCloseButton: true });
                            $scope.GetAllGroupIndicDataConfigdata();

                        } else {
                            swal({ text: 'Indicator Configuration Not Updated', imageUrl: "assets/img/close.png", width: 300, height: 250, showCloseButton: true });
                            $scope.closeAddNew();
                        }
                    });
                }
            }

            $scope.Delete_Config_indic = function (info) {
                swal({
                    text: 'Are you sure to delete indicator configuration?',
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 400,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        $scope.Config_Data = [];
                        $scope.Config_Data1 = [];
                        info['opr'] = 'CD';
                        $scope.Config_Data.push(info);
                        $scope.Config_Data1.push($scope.Config_Data);
                        $http.post(ENV.apiUrl + "api/co_scholastic/CUDGroupAttriIndicConfigData", info).then(function (GetAllGroupIndicConfig) {
                            $scope.message = GetAllGroupIndicConfig.data;
                            if ($scope.message == true) {
                                swal({ text: 'Indicator Configuraton Deleted', imageUrl: "assets/img/check.png", width: 300, height: 250, showCloseButton: true });
                                $scope.GetAllGroupIndicDataConfigdata();

                            } else {
                                swal({ text: 'Indicator Configuration Not Deleted', imageUrl: "assets/img/close.png", width: 300, height: 250, showCloseButton: true });
                                $scope.closeAddNew();
                            }
                        })
                    }
                });
            }



            $scope.Delete_Master_Attribute = function () {

                $scope.delattr = [];

                for (var i = 0; i < $scope.GetAllGroupAttri.length; i++) {
                    if ($scope.GetAllGroupAttri[i].sims_status == true) {
                        $scope.GetAllGroupAttri[i]['opr'] = 'DA';
                        $scope.delattr.push($scope.GetAllGroupAttri[i]);
                    }
                }


                $http.post(ENV.apiUrl + "api/co_scholastic/CoScholAttributeDataOpr", $scope.delattr).then(function (attrmessage) {
                    $scope.message = attrmessage.data;
                    if ($scope.message == true) {
                        swal({ text: 'Attribute Deleted Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                        $scope.GetAllGroupAttriData_function();
                    }
                    else {
                        swal({ text: 'Error In Attribute Deleting', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                    }
                });
            }

            $scope.Delete_Master_Group = function (str, i) {
                if (str.entry_status == 'N') {
                    //$scope.add_group_lst.splice(i, 1);
                    $scope.add_group_lst = $scope.add_group_lst.filter(function (cr) {
                        return cr.sims_co_scholastic_group_code != str.sims_co_scholastic_group_code
                    });
                }
                else {
                    $scope.delgroup = {};
                    $scope.sims_co_scholastic_group_code = '';
                    $scope.delgroup = str;
                    $scope.delgroup['opr'] = "DG";
                    $http.post(ENV.apiUrl + "api/co_scholastic/CoScholGroupDataOpr", $scope.delgroup).then(function (res) {
                        $scope.message = res.data;
                        if ($scope.message == true) {
                            swal({ text: 'Group Deleted Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                            $scope.addNew1('', 'Groups')
                            //$scope.getAllGroups_function_new();
                        }
                        else {
                            swal({ text: 'Error In Group Deleting', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                        }
                    });
                }
            }


            $scope.Delete_Master_attribute_new = function (str, i) {
                if (str.entry_status == 'N') {
                    $scope.add_attribute_lst = $scope.add_attribute_lst.filter(function (cr) {
                        return cr.sims_co_scholastic_attribute_code != str.sims_co_scholastic_attribute_code
                    });
                    //$scope.add_attribute_lst.splice(i, 1);
                }
                else {
                    $scope.delgroup = [];
                    // $scope.sims_co_scholastic_group_code = '';
                    $scope.data = str;
                    $scope.data['opr'] = "DA";
                    $scope.delgroup.push($scope.data);
                    $http.post(ENV.apiUrl + "api/co_scholastic/CoScholAttributeDataOpr", $scope.delgroup).then(function (res) {
                        $scope.message = res.data;
                        if ($scope.message == true) {
                            swal({ text: 'Attribute Deleted Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                            $scope.GetAllGroupAttriData_function_new();
                        }
                        else {
                            swal({ text: 'Error In Attribute Deleting', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                        }
                    });
                }
            }


            $scope.checkmultiple = function (str) {
                if (str == false) {
                    $scope.indi['sims_co_scholastic_overall_result'] = '';
                }

            }


            // my code

            // Start master Config
            $scope.getDataByConfig1 = function (e, config) {
                $scope.sims_report_card_config_desc_currentValue = '';
                $scope.config_code = config.sims_report_card_config_code;

                $("#config_div1 .div_item1").removeClass('bg-red').addClass('bg-grey-md');
                $(".div_item1 h6").removeClass('text-white');
                if ($(e.currentTarget).hasClass('div_item1')) {
                    $(e.currentTarget).addClass('bg-red').removeClass('bg-grey-md');
                    $(e.currentTarget).find('h6').addClass('text-white');
                }
                else {
                    $(e.currentTarget).parents('.div_item1').addClass('bg-red').removeClass('bg-grey-md');
                    $(e.currentTarget).parents('.div_item1').find('h6').addClass('text-white');
                }

                //$scope.getAssignmentDataByConfig();
                //$scope.getAllGroups_function1();

                $scope.GetAllGroupAttri = [];
                $scope.GetAllGroupIndicData = [];
                $scope.AllGroupIndicDataConfigdata = [];

                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllCoScholData?config_code=" + $scope.config_code + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (getAllGroups) {
                    $scope.getAllGroups = getAllGroups.data;
                    $http.get(ENV.apiUrl + "api/co_scholastic/GetAllGroupAttriData?config_code=" + $scope.config_code + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (GetAllGroupAttri) {
                        $scope.GetAllGroupAttri = GetAllGroupAttri.data;
                        $http.get(ENV.apiUrl + "api/co_scholastic/GetAllGroupIndicData?config_code=" + $scope.config_code + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (GetAllGroupIndicData) {
                            $scope.GetAllGroupIndicData = GetAllGroupIndicData.data;
                            var hc = "";
                            $scope.getAllGroups.forEach(function (cr) {
                                hc += '<tr><td style="width: 150px; padding: 0px !important; padding-left:6px  !important">' + cr.sims_co_scholastic_group_name + '</td><td>';
                                var tempGroupAttri = $scope.GetAllGroupAttri.filter(function (cri1) { return +cr.sims_report_card_config_code === +$scope.config_code && +cri1.sims_co_scholastic_group_code === +cr.sims_co_scholastic_group_code })
                                if (tempGroupAttri !== undefined) {
                                    var hcIndicator = '';
                                    var hcGroupAttri = '';
                                    tempGroupAttri.forEach(function (cri1) {
                                        hc += '<table style="border: none;"><tbody><tr><td style="width: 150px; border-right: solid 1px #dddddd;">' + cri1.sims_co_scholastic_attribute_dispaly_name + '</td><td style="padding:0px !important;">';
                                        var tempGroupIndic = $scope.GetAllGroupIndicData.filter(function (cri2) {
                                            return +cri2.sims_report_card_config_code === +$scope.config_code && +cri2.sims_co_scholastic_group_code === +cri1.sims_co_scholastic_group_code && +cri2.sims_co_scholastic_attribute_code === +cri1.sims_co_scholastic_attribute_code
                                        })
                                        if (tempGroupIndic !== undefined) {
                                            hc += '<table  style="border: none;"><tbody>';
                                            tempGroupIndic.forEach(function (cri2) {
                                                hc += '<tr><td>' + cri2.sims_co_scholastic_discriptive_indicator_name + '</td></tr>';
                                            })
                                            hc += '</tbody></table>';
                                        }
                                        hc += '</td></tr></tbody></table>';
                                    })
                                }
                                hc += '</td></tr>';
                            })
                            $('#appendDetails').html(hc);
                        })
                    })
                })
                $scope.showAll = true;
            }

            $scope.get_subject = function () {
                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllSubject?cur_code=" + $scope.curr_code).then(function (res) {
                    $scope.subject_lst = res.data;
                });
            }

            $scope.addNew1 = function (e, str) {

                $scope.gradgroup = [];
                $scope.add_or_edit = 'add';
                $scope.closeAddNew1();
                $scope.setcontrols1();
                $scope.addNewScreen1 = true;
                if (str == 'Groups') {
                    $scope.group = {};
                    $scope.btn_group = true;
                    $scope.actionForm = 'Groups';
                    $scope.term_status = true;
                    $scope.addNewgroup1 = true;

                    $scope.get_subject();
                    // $scope.get_group_score_type();
                    $scope.add_group_lst = [];
                    // $sope.group = {};
                    $scope.getAllGroups_function_new();
                    // $scope.add_group_lst = angular.copy($scope.getAllGroups);
                    $scope.group['sims_co_scholastic_group_status'] = true;
                }

                else if (str == 'Attributes') {
                    $scope.actionForm = 'Attributes';
                    $scope.assign_include_in_final_grade = true;
                    $scope.assign_status = true;
                    $scope.addNewAttribute1 = true;
                    $scope.btn_attri = true;
                    $scope.attr = {};
                    // $scope.get_group_scale_code();
                    $scope.add_attribute_lst = [];
                    $scope.GetAllGroupAttriData_function_new();
                    $scope.attr['sims_co_scholastic_attribute_status'] = true;
                    $scope.getAllGroups_function_new();
                }
                else if (str == 'Indicators') {
                    $scope.indi = {};
                    $scope.actionForm = 'Indicators';
                    $scope.cat_status = true;
                    $scope.includeFinnal = true;
                    $scope.cat_config_status = true;
                    $scope.addNewindi1 = true;
                    $scope.btn_indic = true;
                    $scope.add_indi_lst = [];
                    $scope.get_cambo_val();
                    $scope.get_grade_scale();
                    $scope.indi['sims_co_scholastic_discriptive_indicator_status'] = true;
                    $scope.GetAllGroupIndicData_functionNew();
                    // $scope.get_group_score_type();
                }

                else if (str == 'config') {
                    $scope.actionForm = 'Configuration';
                    $scope.sims_report_card_config_desc_currentValue = '';
                    $scope.assign_include_in_final_grade = true;
                    //$scope.assign_status = true;
                    $scope.addNewConfig1 = true;
                    $scope.btn_config = true;
                    $scope.config1 = {};
                    // $scope.get_group_scale_code();
                    //  $scope.add_attribute_lst = [];
                    // $scope.GetAllGroupAttriData_function_new();
                    $scope.config1['sims_report_card_config_status'] = true;
                    // $scope.getAllGroups_function_new();
                }

                else if (str == 'gradingmaster') {
                    $scope.addNewScreen1 = false;
                    $scope.addNewScreen1Gradding = false;
                    $scope.addNewGradeScaleMaster = true;
                    $scope.addNewGradeScaletrans = false;
                    $scope.gradgroup.grade_status = true;
                    $scope.gradgroup.GradeGroupcode = '';
                    $scope.gradgroup.gradeGroupDesc = '';
                    $scope.gradesaveUpdateBtn = true;
                }
                else if (str == 'gradingstrn') {
                    $scope.addNewScreen1 = false;
                    $scope.copy_grade_code = '';
                    $scope.addNewScreen1Gradding = false;
                    $scope.addNewGradeScaleMaster = false;
                    $scope.addNewGradeScaletrans = true;
                    $scope.gradescalebtn = true;
                    //for (var i = 0; i < $scope.GetAllgradeScale.length; i++) {
                    //    if ($scope.GetAllgradeScale[i].status) {
                    //        $scope.copy_grade_code = angular.copy($scope.GetAllgradeScale[i].gradeGroupCode);
                    //        break;
                    //    }
                    //}
                }
            }
            $scope.closeAddNew1Grade = function () {
                $scope.addNewGradeScaletrans = false;
                $scope.addNewGradeScaleMaster = false;
                $scope.addNewScreen1Gradding = true;
            }

            $scope.closeAddNew1 = function () {
                $scope.addNewScreen1 = false;
                $scope.addNewindi1 = false;
                $scope.addNewAttribute1 = false;
                $scope.IndicConfigShow1 = false;
                $scope.addNewgroup1 = false;
                $scope.addNewConfig1 = false;
                $scope.addNewGradeScaleMaster = false;
                $scope.addNewGradeScaletrans = false;
                $scope.sims_report_card_config_desc = '';
                $scope.sims_report_card_config_status = true;
            }
            $scope.setcontrols1 = function () {
                //$scope.Group_Form1.$setPristine();
                //$scope.Group_Form1.$setUntouched();
                //$scope.Attribute_Form1.$setPristine();
                //$scope.Attribute_Form1.$setUntouched();
                //$scope.Indicator_Form1.$setPristine();
                //$scope.Indicator_Form1.$setUntouched();

                //$scope.add_new_config.$setPristine();
                //$scope.add_new_config.$setUntouched();
            }

            $scope.saveConfig = function (isvalid) {
                if (isvalid == true) {
                    //if ($scope.config1.sims_report_card_config_desc === undefined || $scope.config1.sims_report_card_config_desc.trim() === "") {
                    //    swal({ title: "Alert", text: "Enter config name.", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    //    return
                    //}
                    var sel;
                    if ($scope.AllConfigs !== undefined) {
                        sel = $scope.AllConfigs.find(function (cr) { return cr.sims_report_card_config_desc === $scope.config1.sims_report_card_config_desc.trim() && cr.sims_report_card_config_type === $scope.config1.sims_report_card_config_type })
                    }
                    if (sel !== undefined && $scope.sims_report_card_config_desc_currentValue !== $scope.config1.sims_report_card_config_desc) {
                        swal({ title: "Alert", text: "Configuration name already exists.", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        $scope.config1['opr'] = 'F'
                        if ($scope.config1.saveOrUpdate == 'S')
                            $scope.config1['entry_status'] = 'N';
                        else
                            $scope.config1['entry_status'] = 'O';

                        $scope.config1['sims_cur_code'] = $scope.curr_code;
                        $scope.config1['sims_academic_year'] = $scope.aca_year;


                        var dataTosend = $scope.config1;
                        delete dataTosend.saveOrUpdate;
                        $http.post(ENV.apiUrl + "api/co_scholastic/CoScholConfDataOpr", dataTosend).then(function (attrmessage) {
                            $scope.message = attrmessage.data;
                            if ($scope.message == true) {
                                if ($scope.config1.saveOrUpdate == 'S')
                                    swal({ text: 'Config Created Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                                else
                                    swal({ text: 'Config Updated Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                                $scope.closeAddNew1();
                                $scope.getConfig();
                                //$scope.GetAllGroupAttriData_function_new()
                            }
                            else {
                                swal({ text: 'Error In Config Creating', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                            }
                        });
                    }
                }
            }

            $scope.editConfig = function (e, config) {
                $scope.setcontrols1();
                if (angular.element(idAllMasterConfigs).children().hasClass('bg-red')) {
                    $scope.btn_config = false;
                    $scope.add_or_edit = 'add';
                    $scope.closeAddNew1();
                    $scope.addNewScreen1 = true;
                    $scope.actionForm = 'Configuration';
                    $scope.addNewConfig1 = true;
                    // $scope.get_group_score_type();

                    $scope.AllConfigs && ($scope.AllConfigs.forEach(function (cr, ind) {
                        cr.sims_report_card_config_code === $scope.config_code && ($scope.config1 = angular.copy(cr)), (cr.sims_report_card_config_status === 'A' && (cr.sims_report_card_config_status = true))
                    }))
                    $scope.config1 && (document.getElementById('sims_report_card_config_type').value = $scope.config1.sims_report_card_config_type, $scope.config1.sims_report_card_config_status = true)
                    debugger
                    //for (var i = 0; i < $scope.AllConfigs.length; i++) {
                    //    if ($scope.AllConfigs[i].sims_report_card_config_code == $scope.config_code) {
                    //        $scope.config1 = angular.copy($scope.AllConfigs[i]);
                    //        if ($scope.AllConfigs[i].sims_report_card_config_status == 'A')
                    //            $scope.AllConfigs[i].sims_report_card_config_status = true;
                    //        break;
                    //    }
                    //}

                } else {
                    swal({ text: 'select atleast one Group', imageUrl: "assets/img/notification-alert.png", width: 300, height: 250, showCloseButton: true });
                }
            }

            $scope.deleteConfig = function (e) {
                debugger
                if (angular.element(idAllMasterConfigs).children().hasClass('bg-red')) {
                    $scope.config1 = {};
                    $scope.config1['opr'] = 'H';
                    $scope.config1['sims_report_card_config_code'] = $scope.config_code;
                    $http.post(ENV.apiUrl + "api/co_scholastic/CoScholConfDataOpr", $scope.config1).then(function (attrmessage) {
                        $scope.message = attrmessage.data;
                        $scope.getConfig();
                        if ($scope.message == true) {
                            swal({ text: 'Config Deleted Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Select altest one configuration.", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                }
            }

            $scope.Save_Group_Btn_Click1 = function (isvalid) {
                if (isvalid == true) {
                    var sel;
                    if ($scope.add_group_lst !== undefined) {
                        if ($scope.group.sims_co_scholastic_group_type == '2') {
                            sel = $scope.add_group_lst.find(function (cr) {
                                return cr.sims_report_card_config_code === $scope.group.sims_report_card_config_code &&
                                       cr.sims_co_scholastic_group_dispaly_name === $scope.group.sims_co_scholastic_group_name &&
                                       cr.sims_co_scholastic_group_type === $scope.group.sims_co_scholastic_group_type &&
                                       cr.sims_co_scholastic_group_status === $scope.group.sims_co_scholastic_group_status
                            })
                        }
                        else {
                            sel = $scope.add_group_lst.find(function (cr) {
                                return cr.sims_report_card_config_code === $scope.group.sims_report_card_config_code &&
                                       cr.sims_co_scholastic_group_dispaly_name === $scope.group.sims_co_scholastic_group_name &&
                                       cr.sims_co_scholastic_group_type === $scope.group.sims_co_scholastic_group_type
                            })
                        }
                    }
                    //if (sel !== undefined && $scope.att.sims_attribute_name_currentValue !== $scope.att.sims_attribute_name) {
                    if (sel !== undefined) {
                        swal({ title: "Alert", text: "This configuration already exists.", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        $scope.group['sims_report_card_config_desc'] = $("#txt_sims_co_scholastic_scheme_name option:selected").text();
                        $scope.group['sims_report_card_type_desc'] = $("#cmb_type option:selected").text();
                        if ($("#cmb_subject option:selected").text() == 'Please Select')
                            $scope.group['sims_subject_name_en'] = '';
                        else
                            $scope.group['sims_subject_name_en'] = $("#cmb_subject option:selected").text();
                        $scope.group['sims_co_scholastic_group_name'] = $scope.group.sims_co_scholastic_group_name;
                        $scope.group['sims_co_scholastic_group_dispaly_name'] = $scope.group.sims_co_scholastic_group_name;
                        $scope.group['sims_co_scholastic_group_display_order'] = $scope.add_group_lst.length + 1;
                        $scope.group['entry_status'] = 'N';
                        $scope.group['sims_co_scholastic_group_code'] = Math.max.apply(Math, $scope.add_group_lst.map(function (o) { return +o.sims_co_scholastic_group_code; })) + 1;
                        //$scope.group_copy = angular.copy($scope.group);
                        $scope.add_group_lst.push($scope.group);
                        $scope.copy_config = angular.copy($scope.group.sims_report_card_config_code);
                        $scope.copy_type = angular.copy($scope.group['sims_co_scholastic_group_type']);
                        $scope.group = {};
                        $scope.group['sims_report_card_config_code'] = $scope.copy_config;
                        $scope.group['sims_co_scholastic_group_status'] = true;
                        $scope.group['sims_co_scholastic_group_type'] = $scope.copy_type;
                        $scope.Group_Form1.$setPristine();
                        $scope.Group_Form1.$setUntouched();
                    }
                }
            }

            $scope.Save_Group_Btn_Click1_lst = function () {
                //$scope.group['sims_report_card_config_code'] = $scope.config_code;
                //$scope.group['sims_academic_year'] = $scope.aca_year;
                //$scope.group['sims_cur_code'] = $scope.curr_code;
                //$scope.group['opr'] = "GI";
                $scope.group_lst = [];

                var temp = $scope.add_group_lst;
                for (var i = 0; i < $scope.add_group_lst.length; i++) {
                    var sel;
                    if (temp[i].sims_co_scholastic_group_type == '2') {
                        sel = temp.filter(function (cr) {
                            return cr.sims_report_card_config_code === $scope.add_group_lst[i].sims_report_card_config_code &&
                                   cr.sims_co_scholastic_group_name === $scope.add_group_lst[i].sims_co_scholastic_group_name &&
                                   cr.sims_co_scholastic_group_type === $scope.add_group_lst[i].sims_co_scholastic_group_type &&
                                   cr.sims_co_scholastic_group_status === $scope.add_group_lst[i].sims_co_scholastic_group_status &&
                                   cr.sims_subject_code === $scope.add_group_lst[i].sims_subject_code
                        })
                    }
                    else {
                        sel = temp.filter(function (cr) {
                            return cr.sims_report_card_config_code === $scope.add_group_lst[i].sims_report_card_config_code &&
                                   cr.sims_co_scholastic_group_name === $scope.add_group_lst[i].sims_co_scholastic_group_name &&
                                   cr.sims_co_scholastic_group_type === $scope.add_group_lst[i].sims_co_scholastic_group_type &&
                                   cr.sims_co_scholastic_group_status === $scope.add_group_lst[i].sims_co_scholastic_group_status
                        })
                    }

                    if (sel.length > 1) {
                        swal({ title: "Alert", text: "Duplicate entries exists.", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        return
                    }
                    else {
                        $scope.add_group_lst[i]['sims_academic_year'] = $scope.aca_year;
                        $scope.add_group_lst[i]['sims_cur_code'] = $scope.curr_code;
                        $scope.add_group_lst[i]['opr'] = "GI";
                        $scope.group_lst.push($scope.add_group_lst[i]);
                    }
                }

                debugger

                //var data_lst = [];
                //for (var i = 0; i < $scope.AllAssignment.length; i++) {
                //    if ($scope.AllAssignment[i].chk_assign_config) {
                //        var data = {}
                //        data = angular.copy($scope.group);
                //        data['opr'] = "GIC";
                //        data['sims_report_card_config_code'] = $scope.config_code;
                //        data['sims_academic_year'] = $scope.aca_year;
                //        data['sims_cur_code'] = $scope.curr_code;
                //        data['sims_co_scholastic_group_heading'] = $scope.AllAssignment[i].sims_report_card_assign_number;
                //        data['sims_co_scholastic_group_note'] = $scope.AllAssignment[i].flg;
                //        data_lst.push(data);
                //    }

                //}

                $http.post(ENV.apiUrl + "api/co_scholastic/CoScholGroupDataOprN", $scope.group_lst).then(function (res) {
                    $scope.message = res.data;
                    if ($scope.message == true) {
                        swal({ text: 'Group Created Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                        $scope.addNew1('', 'Groups')
                        //$scope.getAllGroups_function1();
                    }
                    else {
                        swal({ text: 'Error In Group Creating', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                    }

                });
            }

            $scope.Edit_Master_Group1 = function () {
                $scope.setcontrols1();


                $scope.edit_status = false;
                for (var i = 0; i < $scope.getAllGroups.length; i++) {
                    if ($scope.getAllGroups[i].sims_status == true) {
                        $scope.group = angular.copy($scope.getAllGroups[i]);
                        $scope.edit_status = true;
                        break;
                    }
                }
                if ($scope.edit_status == false) {
                    swal({ text: 'select atleast one Group', imageUrl: "assets/img/notification-alert.png", width: 300, height: 250, showCloseButton: true });
                } else {

                    $scope.btn_group = false;
                    $scope.add_or_edit = 'add';
                    $scope.closeAddNew1();
                    $scope.addNewScreen1 = true;
                    $scope.actionForm = 'Groups';
                    $scope.addNewgroup1 = true;
                    // $scope.get_group_score_type();

                }
            }

            $scope.Update_Group_Btn_Click1 = function (isvalid) {

                if (isvalid == true) {

                    $scope.group['opr'] = "GU";
                    $http.post(ENV.apiUrl + "api/co_scholastic/CoScholGroupDataOpr", $scope.group).then(function (res) {
                        $scope.message = res.data;
                        if ($scope.message == true) {
                            swal({ text: 'Group Updated Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                            $scope.getAllGroups_function1();
                        }
                        else {
                            swal({ text: 'Error In Group Updating', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                        }

                    });
                }
            }

            $scope.Delete_Master_Group1 = function () {
                $scope.delgroup = {};
                $scope.sims_co_scholastic_group_code = '';

                for (var i = 0; i < $scope.getAllGroups.length; i++) {
                    if ($scope.getAllGroups[i].sims_status == true) {
                        $scope.delgroup['opr'] = "DG";
                        $scope.delgroup['sims_academic_year'] = $scope.getAllGroups[i].sims_academic_year;
                        $scope.delgroup['sims_cur_code'] = $scope.getAllGroups[i].sims_cur_code;
                        $scope.sims_co_scholastic_group_code = $scope.sims_co_scholastic_group_code + $scope.getAllGroups[i].sims_co_scholastic_group_code + ',';
                    }
                }
                $scope.delgroup['sims_co_scholastic_group_code'] = $scope.sims_co_scholastic_group_code;


                $http.post(ENV.apiUrl + "api/co_scholastic/CoScholGroupDataOpr", $scope.delgroup).then(function (res) {
                    $scope.message = res.data;
                    if ($scope.message == true) {
                        swal({ text: 'Group Deleted Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                        $scope.getAllGroups_function1();
                    }
                    else {
                        swal({ text: 'Error In Group Deleting', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                    }

                });
            }

            //$scope.attribute_change = function (s) {

            //    $scope.attr['sims_report_card_scheme_grade_name'] = s.sims_report_card_scheme_grade_name;
            //}
            $scope.Save_Attribute_Btn_Click1 = function (isvalid) {
                if (isvalid) {
                    //var sel;
                    //if ($scope.add_group_lst !== undefined) {
                    //    if ($scope.group.sims_co_scholastic_group_type == '2') {
                    //        sel = $scope.add_group_lst.find(function (cr) {
                    //            return cr.sims_report_card_config_code === $scope.group.sims_report_card_config_code &&
                    //                   cr.sims_co_scholastic_group_dispaly_name === $scope.group.sims_co_scholastic_group_name &&
                    //                   cr.sims_co_scholastic_group_type === $scope.group.sims_co_scholastic_group_type &&
                    //                   cr.sims_co_scholastic_group_status === $scope.group.sims_co_scholastic_group_status &&
                    //                   cr.sims_subject_code === $scope.group.sims_subject_code
                    //        })
                    //    }
                    //    else {
                    //        sel = $scope.add_group_lst.find(function (cr) {
                    //            return cr.sims_report_card_config_code === $scope.group.sims_report_card_config_code &&
                    //                   cr.sims_co_scholastic_group_dispaly_name === $scope.group.sims_co_scholastic_group_name &&
                    //                   cr.sims_co_scholastic_group_type === $scope.group.sims_co_scholastic_group_type &&
                    //                   cr.sims_co_scholastic_group_status === $scope.group.sims_co_scholastic_group_status
                    //        })
                    //    }
                    //}

                    var sel;
                    if ($scope.add_attribute_lst !== undefined) {
                        sel = $scope.add_attribute_lst.find(function (cr) {
                            return cr.sims_co_scholastic_attribute_name === $scope.attr.sims_co_scholastic_attribute_name &&
                                cr.sims_co_scholastic_attribute_status === $scope.attr.sims_co_scholastic_attribute_status &&
                                cr.sims_co_scholastic_group_code === $scope.attr.sims_co_scholastic_group_code
                        })
                    }
                    debugger
                    //if (sel !== undefined && $scope.att.sims_attribute_name_currentValue !== $scope.att.sims_attribute_name) {
                    if (sel !== undefined) {
                        swal({ title: "Alert", text: "attribute name already exists.", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        $scope.attr['sims_report_card_scheme_grade_name'] = $("#txt_sims_co_scholastic_attribute_group_name option:selected").text();
                        $scope.attr['sims_co_scholastic_attribute_display_order'] = $scope.add_attribute_lst.length + 1;
                        for (var i = 0; i < $scope.getAllGroups_new.length; i++) {
                            if ($scope.attr['sims_co_scholastic_group_code'] == $scope.getAllGroups_new[i].sims_co_scholastic_group_code)
                                $scope.attr['sims_report_card_config_code'] = $scope.getAllGroups_new[i].sims_report_card_config_code
                        }
                        $scope.attr['entry_status'] = "N";
                        $scope.attr['sims_co_scholastic_attribute_code'] = Math.max.apply(Math, $scope.add_attribute_lst.map(function (o) { return +o.sims_co_scholastic_attribute_code; })) + 1;
                        $scope.add_attribute_lst.push($scope.attr);
                        $scope.copy_config = angular.copy($scope.attr.sims_co_scholastic_group_code);
                        $scope.attr = {};
                        $scope.attr['sims_co_scholastic_group_code'] = $scope.copy_config;
                        $scope.attr['sims_co_scholastic_attribute_status'] = true;
                        $scope.Attribute_Form1.$setPristine();
                        $scope.Attribute_Form1.$setUntouched();
                    }
                }
            }


            $scope.Save_atrribute = function () {
                $scope.DataObject = [];
                for (var i = 0; i < $scope.add_attribute_lst.length; i++) {
                    //      if ($scope.getAllGroups[i].sims_status == true) {
                    var data = {};
                    //   data.sims_co_scholastic_group_code = $scope.getAllGroups[i].sims_co_scholastic_group_code;


                    data.sims_cur_code = $scope.curr_code;
                    data.sims_academic_year = $scope.aca_year;
                    data.sims_co_scholastic_attribute_name = $scope.add_attribute_lst[i].sims_co_scholastic_attribute_name;
                    data.sims_co_scholastic_group_code = $scope.add_attribute_lst[i].sims_co_scholastic_group_code;

                    data.sims_co_scholastic_attribute_name_ot = $scope.add_attribute_lst[i].sims_co_scholastic_attribute_name_ot;
                    data.sims_co_scholastic_group_grade_scale_code = $scope.add_attribute_lst[i].sims_co_scholastic_group_grade_scale_code;
                    data.sims_co_scholastic_attribute_display_order = $scope.add_attribute_lst[i].sims_co_scholastic_attribute_display_order;
                    data.sims_co_scholastic_attribute_status = $scope.add_attribute_lst[i].sims_co_scholastic_attribute_status;
                    data.sims_report_card_config_code = $scope.add_attribute_lst[i].sims_report_card_config_code;
                    data.sims_co_scholastic_attribute_code = $scope.add_attribute_lst[i]['sims_co_scholastic_attribute_code'];

                    data.entry_status = $scope.add_attribute_lst[i].entry_status;
                    data.opr = "AI";
                    $scope.DataObject.push(data);
                    //        }

                }

                $http.post(ENV.apiUrl + "api/co_scholastic/CoScholAttributeDataOpr", $scope.DataObject).then(function (attrmessage) {
                    $scope.message = attrmessage.data;
                    if ($scope.message == true) {
                        swal({ text: 'Attribute Created Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                        $scope.GetAllGroupAttriData_function1();
                        $scope.GetAllGroupAttriData_function_new()
                    }
                    else {
                        swal({ text: 'Error In Attribute Creating', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                    }
                });
            }


            $scope.Edit_Master_Attribute1 = function () {

                $scope.setcontrols1();

                $scope.btn_attri = false;

                $scope.edit_status = false;
                for (var i = 0; i < $scope.GetAllGroupAttri.length; i++) {
                    if ($scope.GetAllGroupAttri[i].sims_status == true) {
                        $scope.attr = angular.copy($scope.GetAllGroupAttri[i]);
                        $scope.edit_status = true;
                        break;
                    }
                }

                if ($scope.edit_status != true) {
                    swal({ text: 'Select atleast one Attribute', imageUrl: "assets/img/notification-alert.png", width: 300, height: 250, showCloseButton: true });
                } else {
                    $scope.add_or_edit = 'add';
                    $scope.closeAddNew1();
                    $scope.addNewScreen1 = true;
                    $scope.actionForm = 'Attributes';
                    $scope.assign_include_in_final_grade = true;
                    $scope.assign_status = true;
                    $scope.addNewAttribute1 = true;
                    $scope.get_group_score_type();
                }
            }



            $scope.Update_Attribute_Btn_Click1 = function (isvalid) {
                if (isvalid) {
                    $scope.attr['opr'] = "AU";
                    $scope.dataobject = [];
                    $scope.dataobject.push($scope.attr);

                    $http.post(ENV.apiUrl + "api/co_scholastic/CoScholAttributeDataOpr", $scope.dataobject).then(function (attrmessage) {
                        $scope.message = attrmessage.data;
                        if ($scope.message == true) {
                            swal({ text: 'Attribute Updated Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                            $scope.GetAllGroupAttriData_function1();
                        }
                        else {
                            swal({ text: 'Error In Attribute Updating', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                        }
                    });
                }
            }


            $scope.Delete_Master_Attribute1 = function () {

                $scope.delattr = [];

                for (var i = 0; i < $scope.GetAllGroupAttri.length; i++) {
                    if ($scope.GetAllGroupAttri[i].sims_status == true) {
                        $scope.GetAllGroupAttri[i]['opr'] = 'DA';
                        $scope.delattr.push($scope.GetAllGroupAttri[i]);
                    }
                }


                $http.post(ENV.apiUrl + "api/co_scholastic/CoScholAttributeDataOpr", $scope.delattr).then(function (attrmessage) {
                    $scope.message = attrmessage.data;
                    if ($scope.message == true) {
                        swal({ text: 'Attribute Deleted Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                        $scope.GetAllGroupAttriData_function1();
                    }
                    else {
                        swal({ text: 'Error In Attribute Deleting', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                    }
                });
            }

            $scope.Save_Indicator_Btn_Click1 = function (isvalid) {
                if (isvalid) {
                    var sel;
                    if ($scope.add_indi_lst !== undefined) {
                        if ($scope.indi.sims_co_scholastic_descriptive_indicator_type == '1') {
                            sel = $scope.add_indi_lst.find(function (cr) {
                                return cr.config_code === $scope.indi.config_code &&
                                       cr.sims_co_scholastic_discriptive_indicator_name === $scope.indi.sims_co_scholastic_discriptive_indicator_name &&
                                       cr.sims_co_scholastic_descriptive_indicator_mark === $scope.indi.sims_co_scholastic_descriptive_indicator_mark &&
                                       cr.sims_co_scholastic_discriptive_indicator_status === $scope.indi.sims_co_scholastic_discriptive_indicator_status
                            })
                        }
                        else if ($scope.indi.sims_co_scholastic_descriptive_indicator_type == '2') {
                            sel = $scope.add_indi_lst.find(function (cr) {
                                return cr.config_code === $scope.indi.config_code &&
                                       cr.sims_co_scholastic_discriptive_indicator_name === $scope.indi.sims_co_scholastic_discriptive_indicator_name &&
                                       cr.sims_co_scholastic_descriptive_indicator_grade_scale === $scope.indi.sims_co_scholastic_descriptive_indicator_grade_scale &&
                                       cr.sims_co_scholastic_discriptive_indicator_status === $scope.indi.sims_co_scholastic_discriptive_indicator_status
                            })
                        }
                        //else {
                        //    sel = $scope.add_indi_lst.find(function (cr) {
                        //        return cr.sims_report_card_config_code === $scope.group.sims_report_card_config_code &&
                        //               cr.sims_co_scholastic_group_dispaly_name === $scope.group.sims_co_scholastic_group_name &&
                        //               cr.sims_co_scholastic_group_type === $scope.group.sims_co_scholastic_group_type &&
                        //               cr.sims_co_scholastic_group_status === $scope.group.sims_co_scholastic_group_status
                        //    })
                        //}
                    }
                    //if (sel !== undefined && $scope.att.sims_attribute_name_currentValue !== $scope.att.sims_attribute_name) {
                    if (sel !== undefined) {
                        swal({ title: "Alert", text: "This configuration already exists.", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        $scope.indi.sims_cur_code = $scope.curr_code;
                        $scope.indi.sims_academic_year = $scope.aca_year;
                        $scope.indi.opr = "II";
                        $scope.indi.entry_status = "N";
                        var t = $scope.indi.config_code.split('/');
                        if (t.length > 0) {
                            $scope.indi.sims_report_card_config_code = t[0];
                            $scope.indi.sims_co_scholastic_group_code = t[1];
                            $scope.indi.sims_co_scholastic_attribute_code = t[2];
                        }
                        $scope.indi.sims_co_scholastic_discriptive_indicator_code = Math.max.apply(Math, $scope.add_indi_lst.map(function (o) { return +o.sims_co_scholastic_discriptive_indicator_code; })) + 1;
                        $scope.copy_config_c = angular.copy($scope.indi.config_code);
                        debugger
                        $scope.add_indi_lst.push($scope.indi);
                        $scope.indi = {};
                        $scope.indi['config_code'] = $scope.copy_config_c;
                        $scope.indi['sims_co_scholastic_discriptive_indicator_status'] = true;
                        $scope.Indicator_Form1.$setPristine();
                        $scope.Indicator_Form1.$setUntouched();
                    }
                }
            }

            $scope.Save_indicator_Btn_Click1_lst = function () {
                var datalst = [];
                var temp = $scope.add_indi_lst;
                for (var i = 0; i < $scope.add_indi_lst.length; i++) {
                    var sel;
                    if (temp[i].sims_co_scholastic_descriptive_indicator_type == '1') {
                        sel = $scope.add_indi_lst.filter(function (cr) {
                            return cr.config_code === $scope.indi.config_code &&
                                   cr.sims_co_scholastic_discriptive_indicator_name === $scope.add_indi_lst[i].sims_co_scholastic_discriptive_indicator_name &&
                                   cr.sims_co_scholastic_descriptive_indicator_mark === $scope.add_indi_lst[i].sims_co_scholastic_descriptive_indicator_mark &&
                                   cr.sims_co_scholastic_discriptive_indicator_status === $scope.add_indi_lst[i].sims_co_scholastic_discriptive_indicator_status
                        })
                    }
                    else if (temp[i].sims_co_scholastic_descriptive_indicator_type == '2') {
                        sel = $scope.add_indi_lst.filter(function (cr) {
                            return cr.config_code === $scope.indi.config_code &&
                                   cr.sims_co_scholastic_discriptive_indicator_name === $scope.add_indi_lst[i].sims_co_scholastic_discriptive_indicator_name &&
                                   cr.sims_co_scholastic_descriptive_indicator_grade_scale === $scope.add_indi_lst[i].sims_co_scholastic_descriptive_indicator_grade_scale &&
                                   cr.sims_co_scholastic_discriptive_indicator_status === $scope.add_indi_lst[i].sims_co_scholastic_discriptive_indicator_status
                        })
                    }
                    if (sel.length > 1) {
                        swal({ title: "Alert", text: "Duplicate entries exists.", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        return
                    }
                }
                $http.post(ENV.apiUrl + "api/co_scholastic/CoScholIndicatorDataOprN", $scope.add_indi_lst).then(function (CoScholIndicatorData) {
                    $scope.message = CoScholIndicatorData.data;
                    if ($scope.message == true) {
                        swal({ text: 'Indicator Created Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                        $scope.GetAllGroupIndicData_function1();
                        $scope.GetAllGroupIndicData_functionNew();
                    }
                    else {
                        swal({ text: 'Error In Indicator Creating', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                    }
                });
            }

            $scope.Edit_Master_Indicator1 = function () {
                $scope.setcontrols1();

                $scope.edit_status = false;
                $scope.btn_indic = false;
                for (var i = 0; i < $scope.GetAllGroupIndicData.length; i++) {
                    if ($scope.GetAllGroupIndicData[i].sims_status == true) {
                        $scope.indi = angular.copy($scope.GetAllGroupIndicData[i]);
                        $scope.edit_status = true;
                        break;
                    }
                }

                if ($scope.edit_status != true) {
                    swal({ text: 'Select atleast one Indicator', imageUrl: "assets/img/notification-alert.png", width: 300, height: 250, showCloseButton: true });
                }
                else {
                    $scope.add_or_edit = 'add';
                    $scope.closeAddNew();
                    $scope.actionForm = 'Indicators';
                    $scope.cat_status = true;
                    $scope.includeFinnal = true;
                    $scope.cat_config_status = true;
                    $scope.addNewindi1 = true;
                    $scope.addNewScreen1 = true;
                    $scope.get_group_score_type();
                }

            }


            $scope.Update_Indicator_Btn_Click1 = function (isvalid) {

                if (isvalid) {
                    $scope.indi['opr'] = "IU";
                    $http.post(ENV.apiUrl + "api/co_scholastic/CoScholIndicatorDataOpr", $scope.indi).then(function (attrmessage) {
                        $scope.message = attrmessage.data;
                        if ($scope.message == true) {
                            swal({ text: 'Indicator Updated Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                            $scope.GetAllGroupIndicData_function1();
                        }
                        else {
                            swal({ text: 'Error In Indicator Updating', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                        }
                    });
                }
            }

            $scope.get_grade_scale = function () {
                $http.get(ENV.apiUrl + "api/co_scholastic/Getgrade_scale?cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (GetAllGroupAttri) {
                    $scope.grad_scale_lst = GetAllGroupAttri.data;
                });
            }


            $scope.Delete_Master_Indicator1 = function () {
                $scope.delindi = [];
                for (var i = 0; i < $scope.GetAllGroupIndicData.length; i++) {
                    if ($scope.GetAllGroupIndicData[i].sims_status == true) {
                        $scope.GetAllGroupIndicData[i]['opr'] = 'DI';
                        $scope.delindi.push($scope.GetAllGroupIndicData[i]);
                    }
                }
                $http.post(ENV.apiUrl + "api/co_scholastic/CoScholIndicatorDataOpr1", $scope.delindi).then(function (CoScholIndicatorData) {
                    $scope.message = CoScholIndicatorData.data;
                    if ($scope.message == true) {
                        swal({ text: 'Indicator Deleted Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                        $scope.GetAllGroupIndicData_function1();
                    }
                    else {
                        swal({ text: 'Error In Indicator Deleting', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                    }
                });
            }

            $scope.Delete_Master_indicator = function (ob, i) {
                $scope.delindi = [];
                if (ob.entry_status == 'O') {
                    ob['opr'] = 'DI';
                    $scope.delindi.push(ob);
                    $http.post(ENV.apiUrl + "api/co_scholastic/CoScholIndicatorDataOpr1", $scope.delindi).then(function (CoScholIndicatorData) {
                        $scope.message = CoScholIndicatorData.data;
                        if ($scope.message == true) {
                            swal({ text: 'Indicator Deleted Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
                            $scope.GetAllGroupIndicData_function1();
                            $scope.GetAllGroupIndicData_functionNew();
                        }
                        else {
                            swal({ text: 'Error In Indicator Deleting', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
                        }
                    });
                }
                else if (ob.entry_status == 'N') {
                    //$scope.add_indi_lst.splice(i, 1);
                    $scope.add_indi_lst = $scope.add_indi_lst.filter(function (cr) {
                        return cr.sims_co_scholastic_discriptive_indicator_code != ob.sims_co_scholastic_discriptive_indicator_code
                    });
                }
            }

            //$scope.Delete_Master_Indicator = function () {
            //    for (var i = 0; i < $scope.GetAllGroupIndicData.length; i++) {
            //        if ($scope.GetAllGroupIndicData[i].sims_status == true) {
            //            $scope.GetAllGroupIndicData[i]['opr'] = 'DI';
            //            $scope.delindi.push($scope.GetAllGroupIndicData[i]);
            //        }
            //    }
            //    $http.post(ENV.apiUrl + "api/co_scholastic/CoScholIndicatorDataOpr", $scope.delindi).then(function (CoScholIndicatorData) {
            //        $scope.message = CoScholIndicatorData.data;
            //        if ($scope.message == true) {
            //            swal({ text: 'Indicator Deleted Successfully', imageUrl: "assets/img/check.png", width: 350, height: 250, showCloseButton: true });
            //            $scope.GetAllGroupIndicData_function();
            //        }
            //        else {
            //            swal({ text: 'Error In Indicator Deleting', imageUrl: "assets/img/close.png", width: 350, height: 250, showCloseButton: true });
            //        }
            //    });
            //}


            $scope.masterConfig_click = function () {
                $scope.showAll = true;
                $scope.getAllGroups_function1();
                $scope.GetGradecodes();
            }

            $scope.getAllGroups_function1 = function () {
                $scope.GetAllGroupAttri = [];
                $scope.GetAllGroupIndicData = [];
                $scope.AllGroupIndicDataConfigdata = [];
                $scope.getAllGroups !== undefined && ($scope.getAllGroups.length = 0)
                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllCoScholData?config_code=" + $scope.config_code + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (getAllGroups) {
                    $scope.getAllGroups = getAllGroups.data;
                    if ($scope.getAllGroups.length > 0) {
                        $scope.GetAllGroupAttriData_function1();
                    } else {
                        $scope.closeAddNew();
                    }
                });
            }


            $scope.getAllGroups_function_new = function () {
                //$scope.GetAllGroupAttri = [];
                // $scope.GetAllGroupIndicData = [];
                // $scope.AllGroupIndicDataConfigdata = [];
                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllCoScholData?config_code=" + '' + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (getAllGroups) {
                    $scope.getAllGroups_new = getAllGroups.data;
                    $scope.add_group_lst = getAllGroups.data;
                    //if ($scope.getAllGroups.length > 0) {
                    //    $scope.GetAllGroupAttriData_function1();
                    //} else {
                    //    $scope.closeAddNew();
                    //}
                });
            }



            $scope.GetAllGroupAttriData_function_new = function () {
                //$scope.GetAllGroupAttri = [];
                //$scope.GetAllGroupIndicData = [];
                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllGroupAttriData?config_code=" + '' + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (GetAllGroupAttri) {
                    $scope.add_attribute_lst = GetAllGroupAttri.data;
                })
            }

            $scope.GetAllGroupAttriData_function1 = function () {
                $scope.GetAllGroupAttri = [];
                $scope.GetAllGroupIndicData = [];

                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllGroupAttriData?config_code=" + $scope.config_code + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (GetAllGroupAttri) {
                    $scope.GetAllGroupAttri = GetAllGroupAttri.data;
                    if ($scope.GetAllGroupAttri.length > 0) {
                        $scope.GetAllGroupIndicData_function1();
                    } else {
                        $scope.closeAddNew();
                    }
                })
            }

            $scope.GetAllGroupIndicData_function1 = function () {
                $scope.GetAllGroupIndicData = [];
                //  $scope.AllGroupIndicDataConfigdata = [];
                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllGroupIndicData?config_code=" + $scope.config_code + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (GetAllGroupIndicData) {
                    $scope.GetAllGroupIndicData = GetAllGroupIndicData.data;
                    //if ($scope.GetAllGroupIndicData.length > 0) {
                    //    $scope.GetAllGroupIndicDataConfigdata();
                    //} else {
                    //    $scope.closeAddNew();
                    //}
                })
            }

            $scope.GetAllGroupIndicData_functionNew = function () {
                $scope.GetAllGroupIndicData = [];
                //  $scope.AllGroupIndicDataConfigdata = [];
                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllGroupIndicData?config_code=" + '' + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (GetAllGroupIndicData) {
                    $scope.add_indi_lst = GetAllGroupIndicData.data;
                    //if ($scope.GetAllGroupIndicData.length > 0) {
                    //    $scope.GetAllGroupIndicDataConfigdata();
                    //} else {
                    //    $scope.closeAddNew();
                    //}
                })
            }

            $scope.get_cambo_val = function () {
                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllGroupAttriDataCmbo?config_code=" + ($scope.config_code || "") + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (GetAllGroupAttri) {
                    $scope.cmbo_lst = GetAllGroupAttri.data;
                });
            }
            // End master Config

            //  Grade Book Config Start


            $scope.getallMasterGroup = function () {

                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllCoScholData?config_code=" + $scope.config_code + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (getAllGroups) {
                    $scope.getAllGroupsLst = getAllGroups.data;

                });

                $scope.term = '';
                for (var i = 0; i < $scope.AllAssignment.length; i++) {
                    if ($scope.AllAssignment[i].chk_assign_config) {
                        $scope.term = $scope.term + $scope.AllAssignment[i].sims_report_card_assign_number + $scope.AllAssignment[i].flg + ','
                    }
                }

                $http.get(ENV.apiUrl + "api/co_scholastic/GetAllCoScholData1?config_code=" + $scope.config_code + "&cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year + "&term=" + $scope.term).then(function (getAllGroups) {
                    $scope.getAllGroupsLsts = getAllGroups.data;

                });

            }




            // Grade Book Config end

            //grade scale ///

            // show grade group data
            $scope.GetGradecodes = function () {
                $scope.addNewScreen1Gradding = true;
                $http.get(ENV.apiUrl + "api/Gradebook/GetAllgradeScaleforco_scholastic?cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (GetAllgrade_Scale) {
                    $scope.GetAllgradeScale = GetAllgrade_Scale.data;
                    var gradelist = [];
                    for (var i = 0; i < $scope.GetAllgradeScale.length; i++) {
                        $scope.GetAllgradeScale[i].ischecked = false;
                        if ($scope.GetAllgradeScale[i].gradelst.length == 0) {
                            $scope.GetAllgradeScale[i].gradelst = [];
                        }
                        for (var j = 0; j < $scope.GetAllgradeScale[i].gradelst.length; j++) {
                            $scope.GetAllgradeScale[i].gradelst[j].ischecked = false;
                            gradelist.push($scope.GetAllgradeScale[i].gradelst[j]);
                        }
                    }
                    $scope.co_scholasticgradelistlateruse = angular.copy(gradelist);
                    $scope.co_scholasticgradelist = angular.copy(gradelist);
                    $scope.All_SCALE_GRADE = GetAllgrade_Scale.data;
                    //$scope.getgradesectiondata();

                });


            }


            $scope.GetGradecodesNew = function (grade_code) {
                $http.get(ENV.apiUrl + "api/Gradebook/GetAllgradeScaleforco_scholastic?cur_code=" + $scope.curr_code + "&academic_year=" + $scope.aca_year).then(function (GetAllgrade_Scale) {
                    $scope.GetAllgradeScale = GetAllgrade_Scale.data;
                    var gradelist = [];
                    for (var i = 0; i < $scope.GetAllgradeScale.length; i++) {
                        $scope.GetAllgradeScale[i].ischecked = false;
                        if ($scope.GetAllgradeScale[i].gradelst.length == 0) {
                            $scope.GetAllgradeScale[i].gradelst = [];
                        }
                        for (var j = 0; j < $scope.GetAllgradeScale[i].gradelst.length; j++) {
                            $scope.GetAllgradeScale[i].gradelst[j].ischecked = false;

                            gradelist.push($scope.GetAllgradeScale[i].gradelst[j]);
                        }
                        if ($scope.GetAllgradeScale[i].gradeGroupCode == grade_code)
                            $scope.GetAllgradeScale[i]['status'] = true;
                    }
                    $scope.co_scholasticgradelistlateruse = angular.copy(gradelist);
                    $scope.co_scholasticgradelist = gradelist.filter(function (cr) { return cr.gradegruopCode === grade_code });
                    $scope.All_SCALE_GRADE = GetAllgrade_Scale.data;
                    //$scope.getgradesectiondata();

                });
            }

            // Save grade group data

            $scope.btn_GruopGradeInsert_click = function () {
                if ($scope.gradgroup.gradeGroupDesc === undefined || $scope.gradgroup.gradeGroupDesc.trim() === "") {
                    swal({ title: "Alert", text: "Enter group description.", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    return
                }
                var datasendobject = [];
                var data = {
                    opr: "I"
                    , GradeGroupDesc: $scope.gradgroup.gradeGroupDesc
                    , Curr_Code: $scope.curr_code
                    , Academic_Year: $scope.aca_year
                    , grade_status: $scope.gradgroup.grade_status
                }
                datasendobject.push(data);
                if (datasendobject.length > 0) {
                    $http.post(ENV.apiUrl + "api/Gradebook/CUDgradeScaleforco_scholastic", datasendobject).then(function (MSG) {
                        $scope.msg = MSG.data;
                        if ($scope.msg == true) {
                            swal({
                                text: 'Grade Group Inserted Successfully', imageUrl: "assets/img/check.png"
                                , width: 320, showCloseButton: true
                            });
                            $scope.GetGradecodes();
                            $scope.closeAddNew1();
                        }
                        else if ($scope.msg == false) {
                            swal({
                                text: 'Grade Group Not Inserted. ', imageUrl: "assets/img/close.png"
                                , width: 320, showCloseButton: true
                            });
                        }
                        else {
                            swal("Error-" + $scope.msg)
                        }
                    });
                }
                else {
                    swal({ text: 'Fill Field To Insert Grade Group', imageUrl: "assets/img/notification-alert.png", width: 320, showCloseButton: true });
                }
            }

            // edit group data 

            $scope.editMasterGrade = function (e) {
                $scope.gradgroup = [];
                if ($("#grade_div2_master").find('input:checked').length == 1) {

                    $scope.addNewGradeScaleMaster = true;
                    $scope.gradesaveUpdateBtn = false;
                    $scope.addNewScreen1Gradding = false;
                    $scope.add_or_edit = 'edit';
                    $scope.actionForm = 'grade';
                    $("#grade_div2_master").find('input:checked').each(function (k, v) {
                        //console.log($(v).parents('.div_grade_item').data('gradegroupstatus'))
                        if ($(v).parents('.div_grade_item').data('gradegroupstatus') == true) {
                            $scope.gradgroup.grade_status = true;
                        }
                        else {
                            $scope.gradgroup.grade_status = false;
                        }
                        $scope.gradgroup.Gradecode = $(v).parents('.div_grade_item').data('gradegroupcode');
                        $scope.gradgroup.gradeGroupDesc = $(v).parents('.div_grade_item').data('gradegroupdesc');
                    });
                }
                else {
                    //alert("Select one at a time for edit");
                    swal({ text: 'Select one at a time for edit', imageUrl: "assets/img/notification-alert.png", width: 350, showCloseButton: true });

                }
            }

            // update group data
            $scope.btn_GruopGradeUpdate_click = function () {
                debugger;
                var datasendobject = [];
                for (var i = 0; i < $scope.GetAllgradeScale.length; i++) {
                    if ($scope.GetAllgradeScale[i].status == true) {
                        var data = {
                            opr: "U"
                            , GradeGroupDesc: $scope.gradgroup.gradeGroupDesc
                            , Curr_Code: $scope.GetAllgradeScale[i].curr_Code
                            , Academic_Year: $scope.GetAllgradeScale[i].academic_Year
                            , GradeGroupCode: $scope.GetAllgradeScale[i].gradeGroupCode
                            , grade_status: $scope.gradgroup.grade_status
                        }
                    }
                }
                datasendobject.push(data);

                if (datasendobject.length > 0) {

                    $http.post(ENV.apiUrl + "api/Gradebook/CUDgradeScaleforco_scholastic", datasendobject).then(function (MSG) {
                        $scope.msg = MSG.data;
                        if ($scope.msg == true) {
                            swal({ text: 'Grade Group Updated Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                            $scope.GetGradecodes();
                            $scope.closeAddNew1();
                        }
                        else if ($scope.msg == false) {
                            swal({ text: 'Grade Group Not Updated. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                        }
                        else {
                            swal("Error-" + $scope.msg)
                        }
                    });
                }
                else {

                    swal({
                        text: 'Change At Least One Field To Update Grade Group', imageUrl: "assets/img/notification-alert.png"
                        , width: 350, showCloseButton: true
                    });
                }
            }

            //delete group data
            $scope.btn_GruopGradeDelete_click = function () {
                debugger;
                var datasendobject = [];
                for (var i = 0; i < $scope.GetAllgradeScale.length; i++) {
                    if ($scope.GetAllgradeScale[i].status == true) {
                        $scope.GetAllgradeScale[i].opr = "D";
                        datasendobject.push($scope.GetAllgradeScale[i]);
                    }
                }


                if (datasendobject.length > 0) {
                    $http.post(ENV.apiUrl + "api/Gradebook/CUDgradeScaleforco_scholastic", datasendobject).then(function (MSG) {
                        $scope.msg = MSG.data;
                        if ($scope.msg == true) {
                            swal({ text: 'Grade scale deleted successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                            $scope.GetGradecodes();
                            $scope.closeAddNew1();
                        }
                        else if ($scope.msg == false) {
                            swal({ text: 'Grade scale not deleted. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                        }
                        else {
                            swal("Error-" + $scope.msg)
                        }
                    });
                }
                else {
                    swal({
                        text: 'Select atleast one grade scale to delete', imageUrl: "assets/img/notification-alert.png"
                        , width: 350, showCloseButton: true
                    });
                }
            }

            // cancle group btn
            $scope.btn_GruopGradeCancel_click = function () {
                $scope.btn_insert = true;
                $scope.gradgroup.gradeGroupDesc = '';
            }

            // show grade wise grade scale
            $scope.ShowGradeWiseGradescale = function (info) {
                $scope.gcode = info;
                //info.ischange = true;
                $scope.btn_insert = false;
                //$scope.gradgroup = angular.copy(info);
                $scope.co_scholasticgradelist = [];
                var gardescaledata = [];
                $scope.gradescale = '';
                //for (var i = 0; i < $scope.co_scholasticgradelistlateruse.length; i++) {
                //    if ($scope.co_scholasticgradelistlateruse[i].gradegruopCode == info) {
                //        gardescaledata.push(angular.copy($scope.co_scholasticgradelistlateruse[i]));
                //    }
                //}
                $scope.co_scholasticgradelist = $scope.co_scholasticgradelistlateruse.filter(function (cr) { return cr.gradegruopCode === info });
                $scope.co_scholasticgradelist && ($scope.copy_grade_code = $scope.co_scholasticgradelist[0].gradegruopCode)
            }

            //edit grade scale data
            //$scope.RowUpdateGradeScale = function (info) {
            //    $scope.gradescalebtn = false;
            //    info.ischecked1 === undefined || info.ischecked1 === false ? (info.ischecked1 = true, $scope.gradescale = angular.copy(info)) : (info.ischecked1 = false, $scope.gradescale = "")
            //}

            $scope.updateSelection = function (info, position, entities) {
                $scope.gradescalebtn = false;
                if (info.checked === false) {
                    //angular.forEach(entities, function (subscription) {
                    //    subscription.checked = false;
                    //    $scope.gradescale = "";
                    //    $scope.gradescalebtn = true;
                    //});
                    $scope.btn_GradeScaleCancel_click(entities);
                }
                else {
                    angular.forEach(entities, function (subscription, index) {
                        if (position != index)
                            subscription.checked = false;
                        else {
                            $scope.gradescale = angular.copy(subscription)
                        }
                    });
                }
            }


            // cancel button grade scale 
            $scope.btn_GradeScaleCancel_click = function (entities) {
                angular.forEach(entities, function (subscription) {
                    subscription.checked = false;
                });
                $scope.gradescalebtn = true;
                $scope.gradescale = "";
            }

            // grade scale insert
            $scope.btn_GradeScaleInsert_click = function () {
                var datasendobject = [];
                //for (var i = 0; i < $scope.GetAllgradeScale.length; i++) {
                //    var t = document.getElementById($scope.GetAllgradeScale[i].gradeGroupCode);
                //    if (t.checked == true) {

                //        var data = {
                //            opr: "I"
                //            , Curr_Code: $scope.GetAllgradeScale[0].curr_Code
                //            , Academic_Year: $scope.GetAllgradeScale[0].academic_Year
                //            , GradegruopCode: $scope.GetAllgradeScale[i].gradeGroupCode
                //            , Grade_Name: $scope.gradescale.grade_Name
                //            , Grade_Point_low: $scope.gradescale.grade_Point_low
                //            , Grade_Point_high: $scope.gradescale.grade_Point_high
                //            , Grade_Description: $scope.gradescale.grade_Description
                //            , Status: true
                //        }

                //        datasendobject.push(data);
                //    }

                //}

                //var datasendobject = [];
                //for (var i = 0; i < $scope.GetAllgradeScale.length; i++) {
                //    if ($scope.GetAllgradeScale[i].grade_status == true && $scope.gradescale != '') {

                //        var data = {
                //            opr: "I"
                //            , Curr_Code: $scope.GetAllgradeScale[0].curr_Code
                //            , Academic_Year: $scope.GetAllgradeScale[0].academic_Year
                //            , GradegruopCode: $scope.GetAllgradeScale[i].gradeGroupCode
                //            , Grade_Name: $scope.gradescale.grade_Name
                //            , Grade_Point_low: $scope.gradescale.grade_Point_low
                //            , Grade_Point_high: $scope.gradescale.grade_Point_high
                //            , Grade_Description: $scope.gradescale.grade_Description
                //            , Status: true
                //        }
                //        datasendobject.push(data);
                //    }
                //}

                var sel = $scope.co_scholasticgradelist.find(function (cr) {
                    return angular.lowercase(cr.grade_Name).trim() === angular.lowercase($scope.gradescale.grade_Name).trim()
                })
                if (sel === undefined) {
                    debugger
                    datasendobject.push(angular.copy($scope.gradescale))
                    datasendobject[0].opr = "I";
                    datasendobject[0].Curr_Code = $scope.GetAllgradeScale[0].curr_Code;
                    datasendobject[0].Academic_Year = $scope.GetAllgradeScale[0].academic_Year;
                    datasendobject[0].GradegruopCode = $scope.co_scholasticgradelist[0].gradegruopCode;
                    datasendobject[0].Status = true;
                    if (datasendobject.length > 0) {
                        $http.post(ENV.apiUrl + "api/Gradebook/CUDgradeforco_scholastic", datasendobject).then(function (MSG) {
                            $scope.msg = MSG.data;
                            if ($scope.msg == true) {
                                swal({ text: 'Grade Inserted Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                                // $scope.GetGradecodes();
                                $scope.GetGradecodesNew($scope.copy_grade_code);
                            }
                            else if ($scope.msg == false) {
                                swal({ text: 'Grade Not Inserted. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                            }
                            else {
                                swal("Error-" + $scope.msg)
                            }
                        });

                    }
                    else {
                        swal({ text: 'Fill the grade details and select at least one Grade Scale To Insert Grade', imageUrl: "assets/img/notification-alert.png", width: 320, showCloseButton: true });
                    }
                }
                else {
                    swal({ text: 'Grade scale name already existed. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                }
            }

            // grade scale update
            $scope.btn_GradeScaleUpdate_click = function () {
                debugger;
                var datasendobject = [];
                //for (var i = 0; i < $scope.co_scholasticgradelist.length; i++) {
                //    if ($scope.co_scholasticgradelist[i].ischecked1 == true) {
                //        $scope.co_scholasticgradelist[i].opr = 'U';
                //        $scope.co_scholasticgradelist[i].Grade_Name = $scope.gradescale.grade_Name;
                //        $scope.co_scholasticgradelist[i].Grade_Point_low = $scope.gradescale.grade_Point_low;
                //        $scope.co_scholasticgradelist[i].Grade_Point_high = $scope.gradescale.grade_Point_high;
                //        $scope.co_scholasticgradelist[i].Grade_Description = $scope.gradescale.grade_Description;
                //        $scope.co_scholasticgradelist[i].Status = $scope.gradescale.status
                //        datasendobject.push($scope.co_scholasticgradelist[i]);
                //    }
                //}

                datasendobject.push($scope.gradescale);
                datasendobject[0].opr = 'U';

                debugger
                if (datasendobject.length > 0) {
                    $http.post(ENV.apiUrl + "api/Gradebook/CUDgradeforco_scholastic", datasendobject).then(function (MSG) {
                        $scope.msg = MSG.data;
                        if ($scope.msg == true) {
                            swal({ text: 'Grade Updated Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                            $scope.btn_insert = true;
                            $scope.gradescalebtn = true;
                            $scope.gradescale = "";
                            $scope.GetGradecodesNew($scope.copy_grade_code);
                        }
                        else if ($scope.msg == false) {
                            swal({ text: 'Grade Not Updated. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                        }
                        else {
                            swal("Error-" + $scope.msg)
                        }
                    });
                }
                else {
                    swal({ text: 'Change At Least One Field To Update', imageUrl: "assets/img/notification-alert.png", width: 320, showCloseButton: true });
                }
            }

            // grade scale delete
            $scope.btn_GradeScaleDelete_click = function () {
                debugger;
                var datasendobject = [];
                for (var i = 0; i < $scope.co_scholasticgradelist.length; i++) {
                    if ($scope.co_scholasticgradelist[i].ischecked1 == true) {
                        $scope.co_scholasticgradelist[i].opr = 'D';
                        datasendobject.push($scope.co_scholasticgradelist[i]);
                    }
                }

                if (datasendobject.length > 0) {
                    $http.post(ENV.apiUrl + "api/Gradebook/CUDgradeforco_scholastic", datasendobject).then(function (MSG) {
                        $scope.msg = MSG.data;
                        if ($scope.msg == true) {
                            swal({ text: 'Grade Deleted Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                            $scope.GetGradecodesNew($scope.copy_grade_code);
                        }
                        else if ($scope.msg == false) {
                            swal({ text: 'Grade Not Deleted. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                        }
                        else {
                            swal("Error-" + $scope.msg)
                        }
                    });

                }
                else {
                    swal({ text: 'Select At Least One Grade To Delete', imageUrl: "assets/img/notification-alert.png", width: 320, showCloseButton: true });
                }
            }

            /////  Export Excel ///////
            $scope.exportdata = function () {
                console.log($scope.AllTableData)
                // Prepare Excel data:
                $scope.fileName = "SchemeSubjectMapping";
                $scope.exportData = [];

                var cur = document.getElementById('curr_select');
                var curname = cur.options[cur.selectedIndex].text;
                alert(curname);

                var aca = document.getElementById('aca_select');
                var acaname = aca.options[aca.selectedIndex].text;
                alert(acaname);

                // Headers:
                $scope.exportData.push(["Curriculum", "Academic Year", "Scheme Name", "Frequency Name", "Grade", "Section"]);

                angular.forEach($scope.AllConfigs, function (value, key) {
                    if (value.sims_report_card_config_status == 'A') {
                        angular.forEach($scope.AllAssignment, function (val, ke) {
                            if (val.chk_assign_config) {
                                angular.forEach($scope.gradeSecArr, function (v, k) {
                                    if (v.grade_check == 'A') {
                                        angular.forEach(v.section, function (v1, k1) {
                                            if (v1.status == 'A') {
                                                $scope.exportData.push([
                                                   curname,
                                                   acaname,
                                                   value.sims_report_card_config_desc,
                                                   val.sims_report_card_assign_desc,
                                                   v.grade_name,
                                                   v1.sims_section_name
                                                ]);
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
                $scope.exportexcel($scope.exportData)

            }

            $scope.exportexcel = function (exdata) {
                function datenum(v, date1904) {
                    if (date1904) v += 1462;
                    var epoch = Date.parse(v);
                    return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
                };

                function getSheet(data, opts) {
                    var ws = {};
                    var range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
                    for (var R = 0; R != data.length; ++R) {
                        for (var C = 0; C != data[R].length; ++C) {
                            if (range.s.r > R) range.s.r = R;
                            if (range.s.c > C) range.s.c = C;
                            if (range.e.r < R) range.e.r = R;
                            if (range.e.c < C) range.e.c = C;
                            var cell = { v: data[R][C] };
                            if (cell.v == null) continue;
                            var cell_ref = XLSX.utils.encode_cell({ c: C, r: R });

                            if (typeof cell.v === 'number') cell.t = 'n';
                            else if (typeof cell.v === 'boolean') cell.t = 'b';
                            else if (cell.v instanceof Date) {
                                cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                                cell.v = datenum(cell.v);
                            }
                            else cell.t = 's';

                            ws[cell_ref] = cell;
                        }
                    }
                    if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
                    return ws;
                };

                function Workbook() {
                    if (!(this instanceof Workbook)) return new Workbook();
                    this.SheetNames = [];
                    this.Sheets = {};
                }
                var wb = new Workbook(), ws = getSheet(exdata);
                /* add worksheet to workbook */
                wb.SheetNames.push($scope.fileName);
                wb.Sheets[$scope.fileName] = ws;
                var wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });
                function s2ab(s) {
                    var buf = new ArrayBuffer(s.length);
                    var view = new Uint8Array(buf);
                    for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                    return buf;
                }
                saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), $scope.fileName + '.xlsx');
            };
            //////////////////////////////////////// END//////////////////////////////////////////


        }]);
})();