﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Gradebook');
    simsController.controller('ReportcardBlockController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
       

          
            $scope.edt = {};
            $scope.copy1 = 'L';
            $scope.parent_list = [];
            $scope.student_list = [];

            $rootScope.visible_stud = false;
            $rootScope.visible_parent = true;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = false;
            $rootScope.chkMulti = true;
            $scope.chk_internal = false;
            $scope.is_parent = true;
            $scope.global_Search_click()
            //$scope.Showsave = false;

            $http.get(ENV.apiUrl + "api/ActivityIndicator/GetReportCardBlock").then(function (res) {
                if (res.data.length > 0)
                    $scope.rpt_list = res.data;
                    else
                swal('No Record Found');

            })

            $http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (res) {
                $scope.cmbCur = res.data;
                if ($scope.cmbCur.length > 0) {
                    $scope.edt['sims_cur_code'] = $scope.cmbCur[0].sims_cur_code;
                    $scope.getAcadm_yr($scope.cmbCur[0].sims_cur_code)

                }
            })

            //$scope.pdf = {
            //    src: 'Docs/PortalReference/Prayer4.pdf',
            //};

            $scope.getAcadm_yr = function (cur_code) {
             
                $http.get(ENV.apiUrl + "api/attendance/getAttendanceyear?cur_code=" + cur_code).then(function (res) {
                    $scope.Acdm_year = res.data; 

                    if ($scope.cmbCur.length > 0) {
                        $scope.edt['sims_academic_year'] = res.data[0].sims_academic_year;
                        $scope.academic_change(res.data[0].sims_academic_year);


                    }
                });
            }


            $scope.academic_change = function (acd) {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.global_Search.global_curriculum_code + "&academic_year=" + acd).then(function (res) {
                    $scope.All_grade_names = res.data;
                })
            }

            $scope.getallsectioncode = function (gr_code) {

                $http.get(ENV.apiUrl + "api/common/SectionSubject/getSectionNames?curcode=" + $scope.global_Search.global_curriculum_code + "&academicyear=" + $scope.global_Search.global_acdm_yr + "&gradecode=" + gr_code).then(function (res) {
                    $scope.All_Section_names = res.data;
                  
                });
            }

            $scope.New = function () {
                $scope.Showsave = true;
            }
         
            $scope.add_new_parent = function () {
                $rootScope.visible_stud = false;
                $rootScope.visible_parent = true;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = false;
                $rootScope.chkMulti = true;
                $scope.chk_internal = false;
                $scope.is_parent = true;
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }



            $scope.add_new_student = function () {
                $rootScope.visible_stud = true;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = false;
                $rootScope.chkMulti = true;
                $scope.chk_internal = false;
                $scope.is_parent = false;
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }

            $scope.remove_new_users=function(list,index){
                list.splice(index, 1);
            }

            $scope.$on('global_cancel', function (str) {

                if ($scope.is_parent)
               $scope.parent_list= angular.copy($scope.SelectedUserLst)
               else
               $scope.student_list = angular.copy($scope.SelectedUserLst)

            });
        

            $scope.savenew = function () {
               
                var final_list=''
                if ($scope.new_parent_search == undefined)
                    final_list = '';
                else
                    final_list = $scope.new_parent_search+',';

                for (var i = 0; i < $scope.parent_list.length; i++)
                {
                    final_list += $scope.parent_list[i].s_parent_id + ',';
                }
                var final_list_new = ''
                if ($scope.new_student_search == undefined)
                    final_list_new = '';
                else
                    final_list_new = $scope.new_student_search + ',';

                for (var i = 0; i < $scope.student_list.length; i++) {
                    final_list_new += $scope.student_list[i].s_parent_id + ',';
                }

                final_list += final_list_new;

                $http.post(ENV.apiUrl + "api/ActivityIndicator/CURDReportCardBlock?opr=" + "A" + "&list=" + final_list + "&type=" + $scope.copy1).then(function (res) {
                    if (res.data) {
                        swal('Record Inserted Successfully');
                        $scope.cancel();
                        $http.get(ENV.apiUrl + "api/ActivityIndicator/GetReportCardBlock").then(function (res) {
                            $scope.rpt_list = res.data;
                        });
                    }
                })



            }
            $scope.CheckAllChecked = function (str) {
                for (var i = 0; i < $scope.rpt_list.length; i++) {
                    $scope.rpt_list[i]['chk'] = str;
                }
            }

            $scope.Remove_link = function () {
                swal({
                    title: '',
                    text: "Are you sure you want to Remove?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {

                        var sid = '';
                        for (var i = 0; i < $scope.rpt_list.length; i++) {

                            if ($scope.rpt_list[i]['chk']) {
                                sid += $scope.rpt_list[i].comn_user_name + ',';
                            }
                        }

                        console.log(sid)


                        $http.post(ENV.apiUrl + "api/ActivityIndicator/CURDReportCardBlock?opr=" + "C" + "&list=" + sid + "&type=" + $scope.copy1).then(function (res) {
                            if (res.data) {
                                swal('Record Removed Successfully');
                                $scope.cancel();
                                $http.get(ENV.apiUrl + "api/ActivityIndicator/GetReportCardBlock").then(function (res) {
                                    $scope.rpt_list = res.data;
                                });
                            }
                        })
                    }
                    else {
                        for (var i = 0; i < $scope.rpt_list.length; i++) {

                            $scope.rpt_list[i]['chk'] = false

                        }
                        $scope.main_chk = false;
                    }

                });
            }

           // $('#popover').popover();

            $scope.cancel = function () {
                $scope.Showsave = false;
                $scope.parent_list = [];
                $scope.student_list = [];
                $scope.new_parent_search = '';
                $scope.new_student_search = '';
            }



        }])

})();



