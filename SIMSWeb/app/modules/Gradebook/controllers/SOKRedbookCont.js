﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('SOKRedbookCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $http.get(ENV.apiUrl + "api/rdbook/getresultstatus").then(function (resultstatus) {
                $scope.resultstatusobj = resultstatus.data;

            })

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;

                $scope.sims_cur_code = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr();
            })

            $scope.getacyr = function () {

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.sims_cur_code).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    $scope.sims_academic_year = $scope.Academic_year[0].sims_academic_year;
                    $scope.getsections();
                })
            }

            $scope.getsections = function () {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.sims_cur_code + "&academic_year=" + $scope.sims_academic_year).then(function (res) {
                    $scope.grade = res.data;
                    $scope.sims_grade_code = $scope.grade[0].sims_grade_code;
                    $scope.getsection();
                })
            };

            $scope.getsection = function () {

                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.sims_cur_code + "&grade_code=" + $scope.sims_grade_code + "&academic_year=" + $scope.sims_academic_year).then(function (Allsection) {
                    $scope.section1 = Allsection.data;

                })
            };

            $scope.Getstudentdata = function () {
                var data = {};
                data.sims_cur_code = $scope.sims_cur_code;
                data.sims_academic_year = $scope.sims_academic_year;
                data.sims_grade_code = $scope.sims_grade_code;
                data.sims_section_code = $scope.sims_section_code;

                $http.post(ENV.apiUrl + "api/rdbook/stdentdatardbook", data).then(function (res) {
                    $scope.StudentdataObject = res.data[0];
                    $scope.StudentdataObjectnew = angular.copy(res.data[0]);

                    for (var i = 0; i < $scope.StudentdataObject.length; i++) {
                        if ($scope.StudentdataObject[i].sims_student_attr1 == '' || $scope.StudentdataObject[i].sims_student_attr1 == null) {
                            $scope.StudentdataObject[i].sims_student_attr1 = i + 1;
                        }
                    }
                    $scope.retestobject = res.data[1];

                });
            }

            $scope.SveRedbookData = function () {

                $http.post(ENV.apiUrl + "api/rdbook/CUDstdentdatardbook", $scope.StudentdataObject).then(function (res) {
                    $scope.msg1 = res.data;
                    if ($scope.msg1 == true) {
                        $scope.columnname_changed = false;
                        for (var i = 0; i < $scope.retestobject.length; i++) {
                            if ($scope.retestobject[i].sims_column_name != $scope.retestobject[i].sims_column_name_dummy) {
                                $scope.columnname_changed = true;
                            }
                        }
                        $scope.msg = false;
                        if ($scope.columnname_changed == true) {
                            $http.post(ENV.apiUrl + "api/rdbook/CUDstdentdatardbookdetails", $scope.retestobject).then(function (res1) {
                                $scope.msg = res1.data;
                            });
                        }
                        if ($scope.msg1 == true && $scope.msg == true) {
                            swal({ text: 'Data Submitted Successfully', imageUrl: "assets/img/check.png", width: 300, height: 250, showCloseButton: true });
                        } else if ($scope.msg1 == false && $scope.msg == true) {
                            swal({ text: 'Data Not Submitted But Column Name Is Changed Successfully', imageUrl: "assets/img/close.png", width: 300, height: 250, showCloseButton: true });
                        } else if ($scope.msg1 == true && $scope.msg == false) {
                            swal({ text: 'Data Submitted Successfully', imageUrl: "assets/img/check.png", width: 300, height: 250, showCloseButton: true });
                        }
                        else {
                            swal({ text: 'Data Not Submitted. ', imageUrl: "assets/img/close.png", width: 300, height: 250, showCloseButton: true });
                        }
                    } else {

                        swal({ text: 'Data Not Submitted. ', imageUrl: "assets/img/close.png", width: 300, height: 250, showCloseButton: true });
                    }

                })
            }

            $scope.TeacherCheckAllCheckedonebyone = function (info) {


                if (info.sims_student_attr2 == true) {
                    info.sims_student_attr2 = true;
                }
                else {
                    info.sims_student_attr2 = false;
                }
            }

            $scope.TeacherCheckAllChecked = function () {

                var main = document.getElementById('mainchk1');
                for (var i = 0; i < $scope.StudentdataObject.length; i++) {

                    if (main.checked == true) {
                        $scope.StudentdataObject[i].sims_student_attr2 = true;
                    }
                    else {
                        $scope.StudentdataObject[i].sims_student_attr2 = true;
                    }
                }
            }


            $scope.applynewattendancetoall_student = function () {

                var m = document.getElementById('newattendance');
                if (m.checked == true) {
                    for (var i = 0; i < $scope.StudentdataObject.length; i++) {
                        $scope.StudentdataObject[i].sims_attendance = $scope.StudentdataObject[i].new_attendance;
                        $scope.StudentdataObject[i].sims_student_absent_count_with_excuse = $scope.StudentdataObject[i].new_sims_student_absent_count_with_excuse;
                        $scope.StudentdataObject[i].sims_student_absent_count_without_excuse = $scope.StudentdataObject[i].new_sims_student_absent_count_without_excuse;

                    }
                }
                else {
                    for (var j = 0; j < $scope.StudentdataObjectnew.length; j++) {
                        $scope.StudentdataObject[j].sims_attendance = $scope.StudentdataObjectnew[j].sims_attendance;
                        $scope.StudentdataObject[j].sims_student_absent_count_with_excuse = $scope.StudentdataObjectnew[j].sims_student_absent_count_with_excuse;
                        $scope.StudentdataObject[j].sims_student_absent_count_without_excuse = $scope.StudentdataObjectnew[j].sims_student_absent_count_without_excuse;

                    }
                }

            }


        }])
})();
