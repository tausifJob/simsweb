﻿(function () {
    'use strict';
    var selected_enroll_number = [];
    var registration_num = [];
    var main;
    var categorycode = [];
    var data1 = [];
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ActivityPointEntryCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            var obj1;
            $scope.display = false;
            $scope.examstudentdata = false;
            // $scope.searchStudent = true;
            $scope.grid = true;
            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.enrollno = true;
            $scope.enroll_number = [];
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            var sims_student_enrolls = [];
            $scope.temp = {};
            $scope.edt = {};
            $scope.termDrop = false;
            $scope.ActivityDrop = false;
            $scope.savebtn = true;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.makeTodos = function () {
                if ($scope.numPerPage == 'All')
                    $scope.numPerPage = $scope.totalItems;

                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.getActivity = function (cur,acd) {
                debugger;
                $http.get(ENV.apiUrl + "api/StudentActivity/GetAllActivity?cur_code=" + cur + "&academic_year=" + acd).then(function (res) {
                $scope.Activity = res.data;
            });
            }
            var maxmark = 0;
            $scope.getshowSearchStud = function (actcode) {
                debugger;
                if (actcode == 'undefined') {
                    $scope.searchStudent = true;
                }
                else {
                    $scope.searchStudent = false;
                     
                    for (var i = 0; i < $scope.Activity.length; i++) {
                        if ($scope.Activity[i].sims_activity_number == actcode) {
                              maxmark = $scope.Activity[i].sims_activity_max_point;
                        }

                    }
                }
            }

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;
                $scope.temp["s_cur_code"] = $scope.ComboBoxValues[0].sims_cur_code;
                $scope.temp["sims_academic_year"] = $scope.ComboBoxValues[0].sims_acadmic_year;
            });

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;

                $scope.edt.sims_cur_code = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr($scope.edt.sims_cur_code);
            });

            $scope.getacyr = function (str) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    $scope.edt.sims_academic_year = $scope.Academic_year[0].sims_academic_year;

                    $scope.getGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                   
                    $scope.getActivity($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                });
            }
            $scope.getGrade = function (cur,acd) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + cur + "&academic_year=" + acd).then(function (res) {
                    $scope.allGrade = res.data;

                    $scope.getSection($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_grade_code);
                    $scope.Getterm($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_grade_code);
                    $scope.Getterm2($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_grade_code);
                });
            }

            $scope.getSection = function (cur, acd,grd) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + cur + "&academic_year=" + acd + "&grade_code=" + grd).then(function (res) {
                    $scope.allSection = res.data;

                });
            }

            $scope.edit = function (str) {
                debugger;
                $scope.display = true;
                $scope.grid = false;
                $scope.savebtn = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.readonly = true;
                $scope.searchStudent = false;
                $scope.cur = true;
                $scope.sttt = true;
                $scope.year = true;
                $scope.examboard = true;
                $scope.boardname = true;
                //getacyr(str.sims_cur_name);
                $scope.enrollno = false;
                $scope.ClearCheckBox();
                $scope.enroll_number = [];
                $scope.ActivityDrop = true;
                $scope.Studentsearch = false;

                $http.get(ENV.apiUrl + "api/StudentActivity/GetAllActivity").then(function (res) {
                    $scope.Activity = res.data;
                });

                $scope.edt = {
                            sims_student_full_name: str.sims_student_full_name
                             , sims_enrollment_number: str.sims_enrollment_number
                             , sims_cur_code: str.sims_cur_code
                             , sims_academic_year: str.sims_academic_year
                            , sims_activity_points: str.sims_activity_points
                            , sims_activity_number:str.sims_activity_number
                            , sims_enroll_number: str.sims_enrollment_number
                    , sims_grade_code: str.sims_grade_code
                    , sims_term_code: str.sims_term_number
                    , sims_section_code:str.sims_section_code

                };
                //$scope.temp = {
                //    sims_academic_year: str.sims_academic_year
                //}
            }

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.Studentsearch = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.searchStudent = true;
                $scope.cur = false;
                $scope.sttt = false;
                $scope.year = false;
                $scope.examboard = false;
                $scope.boardname = false;
                $scope.enrollno = true;
                $scope.ClearCheckBox();
                $scope.enroll_number = [];
                $scope.MyForm.$setPristine();
                $scope.MyForm.$setUntouched();
                $scope.edt = {};
                $scope.edt.sims_cur_code = $scope.curriculum[0].sims_cur_code;
                $scope.edt.sims_academic_year = $scope.Academic_year[0].sims_academic_year;
                $scope.temp.sims_board_exam_status = true;
                $scope.ActivityDrop = false;
            }

            $scope.Show = function () {
                debugger;
                if ($scope.edt.sims_cur_code == undefined || $scope.edt.sims_academic_year == undefined || $scope.edt.sims_grade_code == undefined || $scope.edt.sims_term_code == undefined || $scope.edt.sims_activity_number == undefined ) {

                    swal({ text: "Please Select All fields", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380 });
                    $scope.filteredTodos = [];
                }
                else{
                $scope.examstudentdata = true;
                $scope.ImageView = false;

                $http.get(ENV.apiUrl + "api/StudentActivity/getStudentsActivityDetails?curcode=" + $scope.edt.sims_cur_code +
                "&academicyear=" + $scope.edt.sims_academic_year +
                "&activitynumber=" + $scope.edt.sims_activity_number +
                "&gradecode=" + $scope.edt.sims_grade_code +
                "&sectioncode=" + $scope.edt.sims_section_code +
                "&termcode=" + $scope.edt.sims_term_code).then(function (Student_Data) {

                    $scope.StudenteData = Student_Data.data;
                    $scope.totalItems = $scope.StudenteData.length;
                    $scope.todos = $scope.StudenteData;
                    $scope.makeTodos();
                    
                    if (Student_Data.data.length > 0) { }
                    else {
                        $scope.ImageView = true;
                    }
                });
            }
            }
            $scope.Save = function (isvalid) {
                debugger;
                if (isvalid) {
                    debugger
                    if ($scope.enroll_number.length > 0) {

                        for (var i = 0; i < $scope.enroll_number.length; i++) {

                            debugger;
                            //   $scope.edt.sims_student_passport_first_name_en = sims_student_enrolls;
                            var data = {

                                sims_cur_code: $scope.edt.sims_cur_code,
                                sims_academic_year: $scope.edt.sims_academic_year,
                                sims_grade_code: $scope.enroll_number[i].grade_code,
                                sims_section_code: $scope.enroll_number[i].section_code,
                                 
                                sims_enroll_number: $scope.enroll_number[i].s_enroll_no,
                                sims_activity_number: $scope.edt.sims_activity_number,
                                sims_activity_points: $scope.enroll_number[i].sims_activity_points,
                                sims_term_code:$scope.edt.sims_term_code,
                                opr: 'X'
                            }
                            data1.push(data);
                        }
                        $http.post(ENV.apiUrl + "api/StudentActivity/CUDStudentActivity", data1).then(function (MSG1) {
                            $scope.msg = MSG1.data;
                            $scope.grid = true;
                            $scope.display = false;
                            if ($scope.msg == true) {
                                swal({ text: "Student Activity Created Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380 });
                                //$scope.checkonebyonedelete();
                                $scope.examstudentdata = true;
                            }
                            else {
                                swal({ text: "This Record Already Exists. ", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380 });
                            }

                            //$scope.Show();
                        });
                        $scope.edt.sims_cur_code = data.sims_cur_code;
                        $scope.edt.sims_academic_year = data.sims_academic_year;
                        $scope.edt.sims_grade_code = data.sims_grade_code;
                        $scope.edt.sims_section_code = data.sims_section_code;
 
                        $scope.getGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                        $scope.getSection($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_grade_code);

                        $scope.Show();
                        $scope.enroll_number = [];
                        sims_student_enrolls = [];
                        data1 = [];
                    }
                    else {

                        swal('', 'Select Atleast One Student To Insert Record');
                    }
                }
            }

            $scope.Update = function () {
                debugger
                var maxmark=0
                $http.get(ENV.apiUrl + "api/StudentActivity/getActMaxMark?act_code=" + $scope.edt.sims_activity_number).then(function (res) {
                    $scope.res = res.data;
                    maxmark = $scope.res[0].sims_activity_max_point;

                    if (parseInt(maxmark) < parseInt($scope.edt.sims_activity_points)) {
                        swal({ text: "Student can earn maximum " + maxmark + " point", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        });

                    }
                    else {
                        //  if (myForm) {
                        var data = $scope.edt;
                        data.opr = 'U';
                        //data.sims_academic_year = $scope.temp.sims_academic_year;
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/StudentActivity/CUDStudentActivity", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            $scope.BoardData1 = false;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                               // $scope.examstudentdata = true;
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }

                            //$scope.Show();
                        })

                        $scope.Show();
                        $scope.display = false;
                        $scope.grid = true;
                        $scope.examstudentdata = true;
                    }
                });

             



            }
            //  }
            $scope.Delete = function () {
                Student_bacth_code = [];
                $scope.enroll_number = [];

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_batch_enroll_number;
                    var v = document.getElementById(t);

                    if (v.checked == true)

                        Student_bacth_code = Student_bacth_code + $scope.filteredTodos[i].sims_batch_enroll_number + ',';

                }

                var deletebacthcode = ({
                    'sims_batch_enroll_number': Student_bacth_code,
                    'opr': 'D'
                });


                main.checked = false;
                $scope.check();
                Student_bacth_code = [];
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_enrollment_number + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_enrollment_number + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.DeleteRecord = function () {
                debugger;
                var data1 = [];
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_enrollment_number + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
                            'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                            'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                            'sims_activity_number': $scope.filteredTodos[i].sims_activity_number,
                            'sims_enroll_number': $scope.filteredTodos[i].sims_enrollment_number,
                            'sims_term_code': $scope.filteredTodos[i].sims_term_number,

                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger;
                            $http.post(ENV.apiUrl + "api/StudentActivity/CUDStudentActivity", deleteleave).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.Show();
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    //if(){}
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.Show();
                                        }

                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_board_exam_enroll_number + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                data1 = [];
                $scope.row1 = '';
                main.checked = false;
              //  $scope.currentPage = str;
            }

            

            $scope.cancel = function () {
                $scope.display = false;
                $scope.grid = true;
                $scope.MyForm.$setPristine();
                $scope.MyForm.$setUntouched();
            }

            
            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.StudenteData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.SearchStudentWindow = function () {
                $scope.searchtable = false;
                $scope.student = [];
                $('#MyModal').modal('show');
                $scope.busy = false;
                $scope.temp.search_std_grade_name = '';
                $scope.temp.search_std_section_name = '';
                $scope.temp.sims_nationality_name_en = '';
                $scope.temp.search_std_enroll_no = '';
                $scope.temp.search_std_name = '';
                $scope.temp.search_std_passport_no = '';
                $scope.temp.search_std_family_name = '';
                $scope.temp.search_std_family_name = '';

                $scope.temp.sims_academic_year = $scope.edt.sims_academic_year;
            }

            $scope.SearchSudent = function () {
                debugger;
                if ($scope.edt.sims_term_code == undefined)
                {
                    swal({ text: "Please Select Term", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380 });
                }
                else{
                $scope.Okbutton = false;
                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    main.checked = false;
                }

                $scope.searchtable = false;
                $scope.busy = true;
                $scope.sibling_result
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.temp)).then(function (Allstudent) {

                    $scope.student = Allstudent.data;
                    console.log("student", $scope.student);
                    $scope.searchtable = true;
                    $scope.busy = false;


                });
            }
            }
            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {

                $scope.todos = $scope.searched($scope.StudenteData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.StudenteData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_student_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_enrollment_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_grade_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_section_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_activity_points.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;

            }




            $scope.Reset = function () {
                $scope.temp.search_std_grade_name = '';
                $scope.temp.search_std_section_name = '';
                $scope.temp.sims_nationality_name_en = '';
                $scope.temp.search_std_enroll_no = '';
                $scope.temp.search_std_name = '';
                $scope.temp.search_std_passport_no = '';
                $scope.temp.search_std_family_name = '';
                $scope.temp.search_std_family_name = '';
                $scope.searchtable = false;
            }

            $scope.ClearCheckBox = function () {

                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sims_batch_enroll_number;
                        var v = document.getElementById(t);
                        v.checked = false;
                        Student_bacth_code = [];
                        $scope.row1 = '';
                    }
                    main.checked = false;
                }
            }

            $scope.RemoveEnrollMentNo = function ($event, index, str) {

                str.splice(index, 1);

            }

            $scope.MultipleStudentSelect = function () {
                debugger;
                $scope.enroll_number = [];
                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.student.length; i++) {
                        var t = $scope.student[i].s_enroll_no;
                        var v = document.getElementById(t);
                        v.checked = true;

                        $scope.enroll_number.push($scope.student[i]);
                    }
                }
                else {
                    for (var i = 0; i < $scope.student.length; i++) {
                        var t = $scope.student[i].s_enroll_no;
                        var v = document.getElementById(t);
                        v.checked = false;
                        $scope.enroll_number = [];
                    }
                }
            }

            $scope.DataEnroll = function () {
                debugger
                $scope.enroll_number = [];
                for (var i = 0; i < $scope.student.length; i++) {
                    var t = $scope.student[i].s_enroll_no;
                    var v = document.getElementById(t);
                    $scope.student[i]['flg'] = false;
                    if (v.checked == true) {
                        for (var j = 0; j < $scope.filteredTodos.length; j++) {
                            if ($scope.student[i].s_enroll_no == $scope.filteredTodos[j].sims_enrollment_number && $scope.edt.sims_activity_number == $scope.filteredTodos[j].sims_activity_number && $scope.edt.sims_term_code == $scope.filteredTodos[j].sims_term_number) {
                                $scope.student[i]['flg'] = true;

                                //swal({ title: "Alert", text: "already exist", showCloseButton: true, width: 380, });
                            }

                        }
                        if (!$scope.student[i]['flg'])
                            $scope.enroll_number.push($scope.student[i]);
                        $scope.termDrop = true;
                        $scope.save1 = true;
                        //$scope.Getterm2($scope.enroll_number[i].s_cur_code, $scope.enroll_number[i].sims_academic_year, $scope.enroll_number[i].grade_code)
                    }
                }
                $scope.student = [];
            }

            $scope.Getterm = function (cur_code, oldval,grade,section) {
                debugger
                $http.get(ENV.apiUrl + "api/StudentActivity/getTermforGradebook?cur_code=" + cur_code + "&academic_year=" + oldval + "&grade_code=" + grade + "&section_code=" + section).then(function (Getsims550_TermsAcademicYear) {
                    $scope.TermsAcademicYear = Getsims550_TermsAcademicYear.data;
                    $scope.edt['sims_term_code'] = $scope.TermsAcademicYear[0].sims_term_code;
                });

            }

            $scope.Getterm2 = function (cur_code, oldval, grade, section) {
                debugger
                $http.get(ENV.apiUrl + "api/StudentActivity/getTermforGradebook?cur_code=" + cur_code + "&academic_year=" + oldval + "&grade_code=" + grade  + "&section_code=" + section).then(function (Getsims550_TermsAcademicYear) {
                    $scope.TermsAcademicYear1 = Getsims550_TermsAcademicYear.data;
                    //$scope.edt['sims_term_code'] = $scope.TermsAcademicYear1[0].sims_term_code;
                });

            }

            $scope.Okbutton = false;
            $scope.showok = function (str)
            {
                debugger;
                if (str == 'undefined' || str == null) {
                    $scope.Okbutton = false;
                }
                else {
                    $scope.Okbutton = true;
                }
            }

            $scope.sortBy = function (propertyName) {
                debugger;

                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };
            
            var cnt=0;

            $scope.checkmark = function (str) {
                debugger;
                var cnt = 0;
                for (var i = 0; i <= str.length - 1; i++) {
                    if (str[i].sims_activity_points == undefined) {
                        str[i].sims_activity_points = 0;
                    }

                    if (parseInt(maxmark) >= parseInt(str[i].sims_activity_points)) {
                        str[i].colour = null;

                        cnt = cnt + 1;
                    }
                    else {

                        str[i].colour = "red";
                        cnt = cnt - 1;
                        //swal({ text: "Student can earn maximum " + maxmark + " point", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        //});
                        //break;
                    }

                }
                if (cnt == str.length) {
                    $scope.save1 = false;
                } else {
                    $scope.save1 = true;
                    swal({ text: "Student can earn maximum " + maxmark + " point", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                    });
                }
            }


            $scope.exportData = function () {
                var check = true;
                debugger;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure " +"<br/>" +" you want to export activity point Entry.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger;
                            var blob = new Blob([document.getElementById('export_table').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, "Activity point entry" + ".xls");


                        }
                    });
                }
                else {
                    swal({ text: "Report Not Saved", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, })
                }

            }


        }])
})();