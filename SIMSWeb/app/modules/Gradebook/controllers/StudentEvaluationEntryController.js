﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudentEvaluationEntryController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            $('*[data-datepicker="true"] input[type="text"]').datepicker(
                         {
                             todayBtn: true,
                             orientation: "top left",
                             autoclose: true,
                             todayHighlight: true,
                             format: 'yyyy-mm-dd'
                         });
            var username = $rootScope.globals.currentUser.username;

            $scope.condenseMenu();
            $scope.expanded = true;
            $scope.expand_func = function () {
                if ($scope.expanded)
                    $scope.expanded = !$scope.expanded;

            }
            /* PAGER FUNCTIONS*/
            $scope.edt = [];
            $scope.global_temp = { 'sims_user_code': '', 'sims_cur_code': '', 'sims_academic_year': '', 'sims_term_code': '', 'sims_grade_code': '', 'sims_section_code': '', 'sims_subject_code': '', 'sims_config_desc': '', 'sims_type_code': '', 'sims_type_value': '', 'sims_activity_status': '' };
            $scope.temp_tab1 = { 'sims_user_code': '', 'sims_cur_code': '', 'sims_academic_year': '', 'sims_term_code': '', 'sims_grade_code': '', 'sims_section_code': '', 'sims_subject_code': '', 'sims_config_desc': '', 'sims_type_code': '', 'sims_type_value': '', 'sims_activity_status': '' };


            /*CHANGE*/
            $scope.global_temp['sims_user_code'] = username;

            $http.get(ENV.apiUrl + "api/scholastic_evaluation/GetActCurName").then(function (activityCur) {
                $scope.activityCur = activityCur.data;
            });

            $scope.cur_change = function () {
                $http.get(ENV.apiUrl + "api/scholastic_evaluation/GetActAcademic?cur_code=" + $scope.global_temp.sims_cur_code).then(function (activityacde) {
                    $scope.activityacde = activityacde.data;
                });
            }

            $scope.year_change = function () {
                $scope.global_temp['sims_user_code'] = username;

                $http.get(ENV.apiUrl + "api/scholastic_evaluation/GetActGrades?cur_code=" + $scope.global_temp.sims_cur_code + "&acdemic_year=" + $scope.global_temp.sims_academic_year + "&user_name=" + $scope.global_temp.sims_user_code).then(function (activityGrades) {
                    $scope.activityGrades = activityGrades.data;
                });

                $http.get(ENV.apiUrl + "api/scholastic_evaluation/GetActTerms?cur_code=" + $scope.global_temp.sims_cur_code + "&acdemic_year=" + $scope.global_temp.sims_academic_year).then(function (activityTerms) {
                    $scope.activityTerms = activityTerms.data;

                });
            }

            $scope.grade_change = function () {
                $http.get(ENV.apiUrl + "api/scholastic_evaluation/GetActSections?cur_code=" + $scope.global_temp.sims_cur_code + "&acdemic_year=" + $scope.global_temp.sims_academic_year + "&grade=" + $scope.global_temp.sims_grade_code + "&user_name=" + $scope.global_temp.sims_user_code).then(function (activitySections) {
                    $scope.activitySections = activitySections.data;
                });
            }

            /*Activity Type- Tab 1*/
            $http.get(ENV.apiUrl + "api/scholastic_evaluation/GetActTypes").then(function (activityTypes) {
                $scope.activityTypes = activityTypes.data;
            });

            $scope.Expand = true, $scope.Expand_act = true, $scope.Expand_indi = true;

            $scope.getAll_activities = function () {
                $http.post(ENV.apiUrl + "api/scholastic_evaluation/MEAllActivitiesConfig", $scope.global_temp).then(function (allactivity) {
                    $scope.allactivity = allactivity.data;
                    $scope.students = '';
                    $scope.commentEntry = false;
                    });
            }

            /*GET ALL ACTIVITY CONFIG AND SUBJECTS*/
            $scope.bnt_preview_click = function () {
                $scope.getAll_activities();
                $scope.indi_opr = "I";
                /*INDICATOR GROUP*/
                $http.get(ENV.apiUrl + "api/scholastic_evaluation/GetIndicatorGroups?cur_code=" + $scope.global_temp.sims_cur_code + "&acdemic_year=" + $scope.global_temp.sims_academic_year + "&group_code=").then(function (indicatorgroups) {
                    $scope.indicatorgroups = indicatorgroups.data;
                    });

                $http.post(ENV.apiUrl + "api/scholastic_evaluation/ActSectionsSubjects", $scope.global_temp).then(function (activitySectionsSubjects) {
                    $scope.activitySectionsSubjects = activitySectionsSubjects.data;


                    setTimeout(function () {
                        $('#cmd_subjects').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }
            $scope.reset_tab1 = function () {
                $scope.global_temp = '';
                $scope.current_student_obj = '';
                $scope.current_student_comment = '';
                $scope.allactivity = '';
                $scope.students = '';
                $scope.commentEntry = false;
            }
            $scope.editconfig = function (obj, inx) {
                $scope.selected_group = obj;
                $scope.temp_tab2 = '';
                $scope.temp_tab1['sims_acativity_config_name'] = obj.sims_acativity_config_name;

                /*Loading On Demand*/
                //$http.post(ENV.apiUrl + "api/scholastic_evaluation/AllSubActivities", obj).then(function (activities) {
                //    for (var x = 0; x < $scope.allactivity.length; x++) {
                //        if ($scope.allactivity[x].sims_config_sr_no == obj.sims_config_sr_no && $scope.allactivity[x].sims_subject_code == obj.sims_subject_code) {
                //            $scope.allactivity[x].lst_activity = activities.data;
                //            break;
                //        }
                //    }
                //});
                //$scope.acti_indicator = '';
                //$scope.editActivity = true;
                //$scope.temp_tab1['sims_cur_code'] = obj.sims_cur_code;
                //$scope.temp_tab1['sims_academic_year'] = obj.sims_academic_year;
                //$scope.temp_tab1['sims_term_code'] = obj.sims_term_code;
                //$scope.temp_tab1['sims_grade_code'] = obj.sims_grade_code;
                //$scope.temp_tab1['sims_config_sr_no'] = obj.sims_config_sr_no;
                //$scope.temp_tab1['sims_subject_name'] = obj.sims_subject_name;
                //$scope.temp_tab1['sims_subject_code'] = obj.sims_subject_code;
                //$scope.temp_tab1['sims_config_desc'] = obj.sims_config_desc;
                //$scope.temp_tab1['sims_type_code'] = obj.sims_type_code;
                //$scope.temp_tab1['sims_type_value'] = obj.sims_type_value;
                //$scope.temp_tab1['sims_activity_status'] = obj.sims_activity_status;
            }
            $scope.commentEntry = false;
            $scope.txt_search = '';
            $scope.currentActivity = '';

            $scope.fetch_data = function (main_act_c) {
                $scope.loading = true;
                $http.post(ENV.apiUrl + "api/scholastic_evaluation/MEStudentComemnts", main_act_c).then(function (students) {
                    $scope.students = students.data;
                    $scope.loading = false;
                });
            }
            $scope.loading = false;

            $scope.Activity_edit = function (act_obj, main_act_c, ind) {
                $scope.currentActivity = act_obj;
                act_obj['sims_cur_code'] = main_act_c.sims_cur_code;
                act_obj['sims_academic_year'] = main_act_c.sims_academic_year;
                act_obj['sims_term_code'] = main_act_c.sims_term_code;
                act_obj['sims_grade_code'] = main_act_c.sims_grade_code;
                act_obj['sims_section_code'] = main_act_c.sims_section_code;
                $scope.activity_full_name = main_act_c.sims_acativity_config_name + ' [ ' + act_obj.sims_activity_desc + ' ]';
                $scope.loading = true;

                if (act_obj.indicators.length > 0) {
                    $scope.commentEntry = true;
                    $http.post(ENV.apiUrl + "api/scholastic_evaluation/MEStudentComemnts", act_obj).then(function (students) {
                        $scope.students = students.data;
                        $scope.loading = false;

                    });
                }
                else
                    $scope.commentEntry = false;
            }
            /*
            $scope.current_student_obj = '';

            $scope.current_student_comment = '';
            $scope.studentcomments = function (obj, inx) {
                $scope.current_student_comment = obj;
            }
            $scope.viewComments = function () {
                $scope.current_student_obj = $scope.current_student_comment;
                $http.post(ENV.apiUrl + "api/scholastic_evaluation/MEStudentComemntsIndicator?enroll_no=" + $scope.current_student_comment.sims_enroll_number, $scope.currentActivity).then(function (commentIndicators) {
                    $scope.commentIndicators = commentIndicators.data;
                    for (var i = 0; i < $scope.students.length; i++) {
                        if ($scope.students[i].sims_enroll_number == $scope.current_student_comment.sims_enroll_number) {
                            $scope.students[i].indicators = $scope.commentIndicators;
                            break;
                        }
                    }
                    $scope.current_student_comment = '';
                    console.log($scope.commentIndicators);
                });
            }
            $scope.selected_comment = {'sims_indicator_comment_code':''};
            */
            $scope.submitComments = function () {
                $scope.loading = true;
                // $scope.current_student_obj
                $http.post(ENV.apiUrl + "api/scholastic_evaluation/MEDefineStudentComemnt", $scope.students).then(function (comments) {
                    $scope.comments = comments.data;
                    if ($scope.comments == true) {
                        swal(
                        {
                            showCloseButton: true,
                            text: 'Comment mapped for student Successfully.',
                            imageUrl: "assets/img/check.png",
                            width: 350,
                            showCloseButon: true
                        });

                    }
                    else {
                        swal(
                        {
                            showCloseButton: true,
                            text: 'Error in Mapping comment for student.',
                            imageUrl: "assets/img/close.png",
                            width: 350,
                            showCloseButon: true
                        });
                    }
                    $scope.loading = false;
                    $scope.fetch_data($scope.currentActivity);
                });
            }

            $scope.CHECK = function (c, co) {
                for (var i = 0; i < co.comments.length; i++) {
                    if (co.comments[i].sims_indicator_comment_code == c.sims_indicator_comment_code) {
                        if (co.comments[i].sims_indicator_comment_status == true) {
                            co.comments[i].sims_indicator_comment_delete_status = true;
                            co.comments[i].sims_indicator_comment_status = false;
                        }
                        else {
                            co.comments[i].sims_indicator_comment_status = true;
                            co.comments[i].sims_indicator_comment_delete_status = false;
                        }
                    }
                    else {
                        co.comments[i].sims_indicator_comment_status = false;
                        co.comments[i].sims_indicator_comment_delete_status = false;
                    }
                }
            }


        }])

})();



