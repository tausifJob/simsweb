﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Gradebook');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('GradeBookNewDesignCont',
        ['$scope', '$compile', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$locale', '$http', 'ENV', function ($scope, $compile, $state, $rootScope, $timeout, gettextCatalog, $locale, $http, ENV) {

            $scope.showAllMaster = true;
            $scope.busy = false;
            $scope.multi_array = [{}];
            $scope.att = [];

            $scope.user = $rootScope.globals.currentUser.username;

            $http.post(ENV.apiUrl + "api/Gradebook/GradeScaleDataOpr?teachercode=" + $scope.user + "&opr=SC").then(function (All_Grade) {
                debugger;
                $scope.All_SCALE_GRADE = All_Grade.data;

            });

            var aaaa = { opr: 'Y' };
            $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", aaaa).then(function (res) {
                $scope.AllAssignmentType = res.data.table;
                $scope.assign_type = $scope.AllAssignmentType[0].sims_appl_parameter;
                $scope.getTermsmaster();
                $scope.getMasterCategories('test');
                $scope.getMasterAssignments();
                $scope.getattributemaster();
            });

            $scope.hide_grading_scheme = false;

            $scope.getCurriculum = function () {
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                    $scope.Allcurriculum = res.data;
                    if ($scope.Allcurriculum.length > 0) {
                        $scope.showAll = false;
                        $scope.showData = false;
                        $scope.attributedisplay = false;
                        $scope.cur_code = $scope.Allcurriculum[0].sims_cur_code;
                        $scope.getAcademicYear($scope.cur_code);
                    }
                });
            }
            
            $scope.getAcademicYear = function (curCode) {
                var a = { opr: 'AK', cur_code: $scope.cur_code};
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllAcademicYear = res.data.table;
                    if ($scope.AllAcademicYear.length > 0) {
                        $scope.aca_year = $scope.AllAcademicYear[0].sims_academic_year;
                        $scope.getData($scope.aca_year);
                        $scope.getMasterAssignments();
                        $scope.getattributemaster();
                    }
                });
            };

            $scope.getData = function (str) {
                $scope.aca_year = str;
                $scope.showData = true;
                $scope.attributedisplay = false;
                $scope.getConfig();
                $scope.getGradeScalePointForSubject();
                $scope.getTermsmaster();
                $scope.getMasterCategories('test');
                $scope.getMasterAssignments();
            }

            $scope.getConfig = function () {
                var a = { opr: 'A', cur_code: $scope.cur_code, aca_year: $scope.aca_year };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllConfigs = res.data.table;
                    $scope.sims_report_card_config_code = $scope.AllConfigs[0].sims_report_card_config_code;
                    $scope.getsubjectConfigGrade($scope.sims_report_card_config_code);
                    $scope.getConfigTerm();
                });
            }

            $scope.getConfigTerm = function () {
                debugger
                var a = { opr: 'AL', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.sims_report_card_config_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllConfigTerm = res.data.table;
                    $scope.sims_report_card_term_code = $scope.AllConfigTerm[0].sims_report_card_term_code;

                    setTimeout(function () {
                        $('#term_select').change(function () {
                        }).multipleSelect({
                            width: '100%'
                        });
                        $("#term_select").multipleSelect("checkAll");
                    }, 1000);

                    $scope.getConfigTermSub();
                });
            }

            $scope.getConfigTermSub = function () {
                debugger
                var a = { opr: 'AM',cur_code:'',aca_year:'',config_code:'' };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllConfigTermSub = res.data.table;
                    $scope.sims_subject_code = $scope.AllConfigTermSub[0].sims_subject_code;

                    setTimeout(function () {
                        $('#subject_select').change(function () {
                        }).multipleSelect({
                            width: '100%'
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);

                });
            }


            $scope.getDataByConfig = function (e) {

                if ($(e.currentTarget).hasClass('div_item')) {

                    $scope.config_code = parseInt($(e.currentTarget).attr('configcode'), 10);
                }
                else {
                    $scope.config_code = parseInt($(e.currentTarget).parents('.div_item').attr('configcode'), 10);
                }

                $("#config_div .div_item").removeClass('bg-red').addClass('bg-grey-md');
                $(".div_item h6").removeClass('text-white');
                if ($(e.currentTarget).hasClass('div_item')) {
                    $(e.currentTarget).addClass('bg-red').removeClass('bg-grey-md');
                    $(e.currentTarget).find('h6').addClass('text-white');
                }
                else {
                    $(e.currentTarget).parents('.div_item').addClass('bg-red').removeClass('bg-grey-md');
                    $(e.currentTarget).parents('.div_item').find('h6').addClass('text-white');
                }

                $scope.getGrade();
                $scope.getTermsmaster();
                $scope.getTerms();
                $scope.getCategories();
                //$scope.getUnAssignedCategories();
                $scope.getMasterCategories();
                $scope.getAssignments();
                // $scope.getUnAssignedAssignments();
                $scope.getMasterAssignments();
                $scope.showAll = true;
            }

            $scope.checkboxClick = function (e) {
                e.stopPropagation();
            }

            $scope.setCatType = function (str) {
                if (str == "M") {
                    $scope.showMaxAssign = true;
                }
                else {
                    $scope.showMaxAssign = false;
                }
                $scope.cat_type = str;
            }
            $scope.getGrade = function () {
                $scope.gradeSecArr = [];
                var a = { opr: 'E', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/importSavedConfigSecData?cur_code=" + $scope.cur_code + "&&aca_year=" + $scope.aca_year + "&&config_code=" + $scope.config_code).then(function (res) {
                    //console.log("GetGrade Function", res.data);
                    $scope.gradeSecArr = res.data;

                    $($scope.gradeSecArr).each(function (key, val) {
                        val.grade_check = 'I';
                        $(val.section).each(function (k, v) {
                            if (v.status == 'A') {
                                val.grade_check = 'A';
                            }
                        })
                    })

                    //console.log("GetGrade Function after check", res.data);
                });
            }

            $scope.getAssignData = function (e) {
                if ($(e.currentTarget).find('input').is(':checked')) {
                    $scope.assigndivshow = true;
                    $("#category_div").removeClass('b-0');
                    $("#category_div").removeClass('col-md-6').addClass('col-md-4', { duration: 600 });
                }
            }

            $scope.getTerms = function () {
                var a = { opr: 'G', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllTerms = res.data.table;
                });
            }

            $scope.getTerms_master_term = function (str) {
                var a = { opr: 'AX', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code,sims_report_card_master_term_code:str };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllTermsData = res.data.table;
                });
            }


            $scope.getTermsmaster = function () {
                var a = { opr: 'AQ', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllTermsMaster = res.data.table;
                    $scope.sims_report_card_term_code_master = null;

                    $scope.getSelectedTermData($scope.sims_report_card_term_code_master);
                });
            }

            $scope.getattributemaster = function () {
                debugger
                var a = { opr: 'BE', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllMasterAttribute = res.data.table;
                });
            }

            $scope.getCategories = function (obj) {
                var a = { opr: 'Q', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllCategories = res.data.table;
                    //console.log("Categories Data", $scope.AllCategories);
                });
            }

            $scope.saveConfigGrade = function () {
                debugger
                var term_arr = [];
                for (var s = 0; s < $scope.AllTerms.length;s++){
                    if($scope.AllTerms[s].sims_report_card_term_status=='A'){
                        term_arr.push($scope.AllTerms[s]);
                    }
                }

                console.log($scope.gradeSecArr);
                $scope.saveData = [];
                for (var i = 0; i < $scope.gradeSecArr.length; i++) {
                    for (var j = 0; j < $scope.gradeSecArr[i].section.length; j++) {
                        var t = 'section_' + $scope.gradeSecArr[i].grade_code + '_' + $scope.gradeSecArr[i].section[j].sims_section_code;
                    var v = document.getElementById(t);
                    if (v.checked == true) {
                        var d = {
                            'aca_year': $scope.aca_year,
                            'cur_code': $scope.cur_code,
                            grade_code: $scope.gradeSecArr[i].grade_code,
                            sims_section_code: $scope.gradeSecArr[i].section[j].sims_section_code,
                            'config_code': $scope.config_code,
                            status: 'A'
                        }
                        $scope.saveData.push(d);
                    }
                  }
                }
                var gradeSecArr = [];
                
                gradeSecArr=$scope.saveData;
              
                //$('.grade_heading').each(function (k, v) {
                //    secArr = [];
                //    var grade_code = $(this).find('input:checkbox').val();
                //    var grade_name = $(this).find('input:checkbox').data('gradename');
                //    var issecsele, isgradesele;

                //    $(this).parent().find('.section_select_checkbox').each(function (key, val) {
                //        if (val.children[0].checked) {
                //            issecsele = 'A';
                //        }
                //        else {
                //            issecsele = 'I';
                //        }
                //        var sec_code = $(this).find('input:checkbox').val();
                //        var sec_namecode = $(this).find('input:checkbox').data('secname');

                //        secArr.push({ 'sims_section_code': sec_code, 'sims_section_name': sec_namecode, 'status': issecsele });
                //    })

                //    gradeSecArr.push({ 'grade_code': grade_code, 'grade_name': grade_name, 'aca_year': $scope.aca_year, 'cur_code': $scope.cur_code, 'config_code': $scope.config_code, 'section': secArr });
                //});
                //console.log('gradeSecArr =', gradeSecArr);
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeSecSave?cTemp=F", gradeSecArr).then(function (res) {
                    if (res.data == true) {
                        swal({ title: "Alert", text: "Configuration Created successfully.", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    }
                    else {
                        swal({ title: "Alert", text: "Unable To Create Configuration. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                });
            }

            $scope.getUnAssignedCategories = function (obj) {
                var a = { opr: 'Z', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.UnCategories = res.data.table;
                    //console.log("Categories Data", $scope.UnCategories);
                });
            }

            $scope.getMasterCategories = function (obj) {
                var a = { opr: 'K', cur_code: $scope.cur_code, aca_year: $scope.aca_year };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.MasterCategories = res.data.table;
                    //console.log("MasterCategories Data", $scope.MasterCategories);
                    $scope.sims_report_card_category_code_master = $scope.AllMaterCategories[0].sims_report_card_category_code;
                    $scope.getAllActiveUnmappedTerms_master($scope.sims_report_card_category_code_master);
                    //$scope.getAllActiveUnmappedTerms();
                    var a = { opr: 'AD', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code, cat_code: $scope.sims_report_card_category_code_master };
                    $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                        $scope.AllMappedCategoryData = res.data.table;
                        //console.log("Mapped categories", res.data.table);
                    });
                });
            }

            $scope.accordion = function (e, str) {
                e.stopPropagation();
                e.preventDefault();
                if (e.target.tagName == 'LABEL') {
                    if ($(e.target).parents('.grades_div').find('.grade_select_checkbox').hasClass('checked')) {
                        $(e.target).parents('.grades_div').find('.grade_select_checkbox').removeClass('checked');
                        $(e.target).parents('.grades_div').find('.grade_select_checkbox input').prop('checked', false);
                        $(e.target).parents('.grades_div').find('.section_select_checkbox').removeClass('checked');
                        $(e.target).parents('.grades_div').find('.section_select_checkbox input').prop('checked', false);
                    }
                    else {
                        $(e.target).parents('.grades_div').find('.grade_select_checkbox').addClass('checked');
                        $(e.target).parents('.grades_div').find('.grade_select_checkbox input').prop('checked', true);

                        setTimeout(function () {
                            $(e.target).parents('.grades_div').find('.section_select_checkbox').addClass('checked');
                            $(e.target).parents('.grades_div').find('.section_select_checkbox input').prop('checked', true);

                        }, 200);
                    }
                }
                else if (e.target.tagName == 'I' || e.target.tagName == 'H5') {
                    $(e.target).parents('.grades_div').siblings('.grades_div').find('.grade_section_container').hide().addClass('closed');

                    $(e.target).parents('.grades_div').find('.grade_section_container').slideToggle().toggleClass('closed');
                }

            }

            $scope.getAssignments = function () {
                var a = { opr: 'R', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllAssignment = res.data.table;
                });
            }

            $scope.getMasterAssignments = function () {
                var a = { opr: 'AA', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.MasterAssignment = res.data.table;
                });
            }

            $scope.addNew = function (e, str) {
                $scope.add_or_edit = 'add';
                $scope.closeAddNew();
                $scope.addNewScreen = true;
                $scope.getGradingScaleData();
                if (str == 'config') {
                    $scope.actionForm = 'config';
                    $scope.config_status = true;
                    $scope.addNewConfig = true;
                    //$scope.assign_status = true;
                    //$scope.cat_status = true;
                }
                else if (str == 'term') {
                    $scope.getTermsmaster();
                    $scope.actionForm = 'term';
                    $scope.term_status = true;
                    $scope.addNewTerm = true;
                    $scope.termcode_edit = '';
                    $scope.term_desc_en = '';
                    $scope.term_desc_ot = '';
                    $scope.term_start_date = '';
                    $scope.term_end_date = '';
                    $scope.term_grade_scale = '';
                    $scope.assign_status = true;
                }
                else if (str == 'category') {
                    $scope.actionForm = 'category';
                    $scope.getAllActiveUnmappedTerms();
                    $scope.getCatWeightage();
                    //$scope.getmasterCategories();
                    $scope.getAllActiveUnmappedTerms_master();
                    $scope.cat_status = true;
                    $scope.includeFinnal = true;
                    $scope.cat_config_status = true;
                    $scope.addNewCat = true;
                    $scope.cat_code = '';
                    $scope.cat_name_en = '';
                    $scope.cat_name_ot = '';
                    $scope.cat_desc = '';
                    $scope.cat_type = '';
                    $scope.cat_weightage_type = '';
                    $scope.cat_weightage_value = '';
                    $scope.cat_max_of_assign_count = '';
                    $scope.cat_score_type = '';
                    $scope.cat_point_poss = '';
                    $scope.cat_color = '';
                    $scope.cat_grade_scale = '';
                    $scope.includeFinnal = '';
                    $scope.cat_parents_code = '';
                    $scope.cat_status = true;
                }

                else if (str == 'assignment') {
                    $scope.actionForm = 'assignment';
                    $scope.getAssignmentType();
                    //$scope.getAllActiveCatTerms();
                    $scope.getAllActiveCatTerms_master();
                    $scope.assign_include_in_final_grade = true;
                    $scope.assign_status = true;
                    $scope.addNewAssign = true;
                    $scope.assign_number = "";
                    $scope.assign_name = "";
                    $scope.assign_name_ot = "";
                    $scope.assign_type = "";
                    $scope.cat_grade_scale = "";
                    $scope.assign_status = true;
                }
            }

            $scope.addNewMaster = function (e, str) {
                debugger;
                $scope.gradgroup = [];
                $scope.add_or_edit = 'add';
                $scope.closeAddNewMaster();
                $scope.getCatWeightage();
                $scope.addNewScreenMaster = true;
                $scope.getGradingScaleData();
                if (str == 'config') {
                    $scope.actionForm = 'config';
                    $scope.config_status = true;
                    $scope.addNewConfigMaster = true;
                    //$scope.assign_status = true;
                    //$scope.cat_status = true;
                }
                else if (str == 'term') {
                    $scope.actionForm = 'term';
                    $scope.term_status = true;
                    $scope.addNewTermMaster = true;
                    $scope.termcode_edit = '';
                    $scope.term_desc_en = '';
                    $scope.term_desc_ot = '';
                    $scope.term_start_date = '';
                    $scope.term_end_date = '';
                    $scope.term_grade_scale = '';
                    $scope.assign_status = true;
                }
                else if (str == 'category') {
                    $scope.actionForm = 'category';
                    $scope.getCatWeightage();
                    $scope.getmasterCategories();
                    $scope.cat_status = true;
                    $scope.includeFinnal = true;
                    $scope.cat_config_status = true;
                    $scope.addNewCatMaster = true;
                    $scope.cat_code = '';
                    $scope.cat_name_en = '';
                    $scope.cat_name_ot = '';
                    $scope.cat_desc = '';
                    $scope.cat_type = '';
                    $scope.cat_weightage_type = '';
                    $scope.cat_weightage_value = '';
                    $scope.cat_max_of_assign_count = '';
                    $scope.cat_score_type = '';
                    $scope.cat_point_poss = '';
                    $scope.cat_color = '';
                    $scope.cat_grade_scale = '';
                    $scope.includeFinnal = '';
                    $scope.cat_parents_code = '';
                    $scope.cat_status = true;
                }

                else if (str == 'assignment') {
                    $scope.actionForm = 'assignment';
                    $scope.getAssignmentType();
                    $scope.getMasterAssignments();
                    $scope.assign_include_in_final_grade = true;
                    $scope.assign_status = true;
                    $scope.addNewAssignMaster = true;
                    $scope.getAllMasterAssignment();
                    $scope.assign_number = "";
                    $scope.assign_name = "";
                    $scope.assign_name_ot = "";
                    $scope.assign_desc = "";
                    $scope.assign_type = "";
                    $scope.cat_grade_scale = "";
                    $scope.assign_status = true;
                }

                else if (str == 'gradingstrn') {
                    debugger;
                    $scope.addNewGradeScaleMaster = false;
                    $scope.addNewGradeScaletrans = true;
                    $scope.gradescalebtn = true;
                  //  $scope.GradebookGradingSchemeShow();
                   
                }

                else if (str == 'gradingmaster') {
                    debugger;
                    $scope.addNewGradeScaleMaster = true;
                    $scope.addNewGradeScaletrans = false;
                    $scope.gradgroup.status = true;
                    $scope.gradgroup.Gradecode = '';
                    $scope.gradgroup.GradeName = '';
                    $scope.gradgroup.Description = '';

                    $scope.gradesaveUpdateBtn=true;
                }

                else if (str == 'attributeMaster') {
                    debugger;
                    $scope.addNewAattributeMaster = true;
                    //$scope.addNewGradeScaletrans = true;
                    //$scope.gradescalebtn = true;
                    //  $scope.GradebookGradingSchemeShow();

                }
            }

            $scope.showGradesscale = function () {
                debugger;
                  $scope.addNewGradeScaletrans = true;
                    $scope.gradescalebtn = true;
                    $scope.GradebookGradingSchemeShow();

        }
            $scope.closeAddNew = function () {
                $scope.addNewScreen = false;
                $scope.addNewConfig = false;
                $scope.addNewTerm = false;
                $scope.addNewCat = false;
                $scope.addNewAssign = false;
                $scope.assignConfigShow = false;
                $scope.catConfigShow = false;
                $scope.sims_allow_edit = false;
                $scope.AllMaterCategories = [];
            }

            $scope.closeAddNewMaster = function () {
                $scope.addNewScreenMaster = false;
                $scope.addNewConfigMaster = false;
                $scope.addNewTermMaster = false;
                $scope.addNewCatMaster = false;
                $scope.addNewAssignMaster = false;
                $scope.assignConfigShowMaster = false;
                $scope.catConfigShowMaster = false;
                $scope.sims_allow_edit = false;
                $scope.addNewGradeScaleMaster = false;
                $scope.addNewGradeScaletrans = false;
                $scope.gradescale = '';
                $scope.addNewAattributeMaster = false;
            }

            $scope.editConfig = function (e) {
                $scope.add_or_edit = 'edit';
                $scope.actionForm = 'config';
                $scope.addNewScreen = true;
                $scope.addNewConfig = true;

                $scope.config_code_edit = $("#config_div").find('.bg-red').attr('configcode');
                $scope.config_desc = $("#config_div").find('.bg-red').attr('configdesc');

                if ($("#config_div").find('.bg-red').attr('configstatus') == 'A') {
                    $scope.config_status = true;
                }
                else {
                    $scope.config_status = false;
                }
            }

            $scope.updateStatus = function (e) {
                var configcode = $(e.target).parents('.div_item').attr('configcode');
                var configdesc = $(e.target).parents('.div_item').attr('configdesc');
                var configstatus = $(e.target).parents('.div_item').attr('configstatus');
                var new_status;
                if (configstatus == 'A') {
                    new_status = 'I';
                }
                else {
                    new_status = 'A';
                }

                var a = { opr: 'D', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: configcode, config_status: new_status };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    if (configstatus == 'A') {
                        $(e.target).removeClass('label-danger').addClass('label-default').text('I');
                        $(e.target).parents('.div_item').attr('configstatus', 'I');
                    }
                    else {
                        $(e.target).removeClass('label-default').addClass('label-danger').text('A');
                        $(e.target).parents('.div_item').attr('configstatus', 'A');
                    }
                });
            }

            $scope.deleteConfig = function (e, str) {
                var tab_pan_id = $(e.target).parents('.tab-pane').attr('id');

                if ($("#config_div").find('.bg-red').length > 0) {
                    var configcode = $("#config_div").find('.bg-red').attr('configcode');

                    var a = { opr: 'C', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: configcode };
                    $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                        $("#config_div").find('.bg-red').remove();
                    });
                }
                else {
                    alert("Select any config");
                }
            }

            $scope.deleteTerm = function (e, str) {
                debugger
                if ($("#term_divMaster").find('input:checked').length > 0) {
                    $("#term_divMaster").find('input:checked').each(function (k, v) {
                        var term_code = $(v).parents('.div_item').data('termcode');

                      
                        var a = { opr: 'W', cur_code: $scope.cur_code, aca_year: $scope.aca_year, term_code: term_code };
                        $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {

                            if (res.data.table[0].inserted_flg > 0) {
                                $(document).ready(function () {
                                });
                                swal({ title: "Alert", text: "Term Deleted successfully.", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                $scope.getTermsmaster();
                            }
                            else {
                                swal({ title: "Alert", text: "Unable To Delete Term. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }

                            $scope.getTerms();
                            $scope.getCategories();
                            $scope.getMasterCategories();
                            $scope.getAssignments();
                            $scope.getMasterAssignments();
                        });
                    })
                }
                else {
                    alert("Select atleast one term");
                }
            }

            $scope.deleteCategory = function (e) {
                debugger
                if ($("#cat_div2").find('input:checked').length > 0) {
                    $("#cat_div2").find('input:checked').each(function (k, v) {
                        var cat_code = $(v).parents('.div_cat_item').data('catcode');

                        var a = { opr: 'AF', cur_code: $scope.cur_code, aca_year: $scope.aca_year, cat_code: cat_code };
                        $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {

                            if (res.data.table[0].inserted_flg > 0) {
                                $(document).ready(function () {
                                });
                                swal({ title: "Alert", text: "Category Deleted successfully.", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Unable To Delete Category. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }

                            $scope.getCategories();
                            $scope.getMasterCategories();
                            $scope.getAssignments();
                            $scope.getMasterAssignments();
                        });
                    })
                }
                else {
                    alert("Select atleast one Category"); 
                }
            }

            $scope.deleteAttribute = function (e) {
                debugger
                if ($("#Div55").find('input:checked').length > 0) {
                    $("#Div55").find('input:checked').each(function (k, v) {
                        $scope.att.sims_attribute_code = $(v).parents('.div_cat_item').data('attrcode');
                        var a = { opr: 'BF', cur_code: $scope.cur_code, aca_year: $scope.aca_year, sims_attribute_code: $scope.att.sims_attribute_code };
                        $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {

                            if (res.data.table[0].inserted_flg > 0) {
                                $(document).ready(function () {
                                });
                                swal({ title: "Alert", text: "Category Deleted successfully.", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                $scope.getattributemaster();
                            }
                            else {
                                swal({ title: "Alert", text: "Unable To Delete Category. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }

                            $scope.getCategories();
                            $scope.getMasterCategories();
                            $scope.getAssignments();
                            $scope.getMasterAssignments();
                        });
                    })
                }
                else {
                    alert("Select atleast one Category");
                }
            }

            $scope.deleteAssignment = function (e) {
                debugger
                if ($("#assign_div2_master").find('input:checked').length > 0) {
                    $("#assign_div2_master").find('input:checked').each(function (k, v) {
                        var assign_number = $(v).parents('.div_assign_item').data('assignnum');

                        var a = { opr: 'AG', cur_code: $scope.cur_code, aca_year: $scope.aca_year, assign_number: assign_number };
                        $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {

                            if (res.data.table[0].inserted_flg > 0) {
                                $(document).ready(function () {
                                });
                                swal({ title: "Alert", text: "Assignment Deleted successfully.", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Unable To Delete Assignment. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }

                            $scope.getAssignments();
                            $scope.getMasterAssignments();
                        });
                    })
                }
                else {
                    alert("Select atleast one Assignment");
                }
            }

            $scope.saveConfig = function () {
                var config_status;
                if ($scope.config_status == true) {
                    config_status = 'A';
                }
                else {
                    config_status = 'I';
                }

                if ($scope.add_or_edit == 'edit' && $scope.actionForm == 'config') {
                    var a = { sims_cur_code: $scope.cur_code, sims_academic_year: $scope.aca_year, sims_report_card_config_code: $scope.config_code_edit, sims_report_card_config_desc: $scope.config_desc, sims_report_card_config_status: config_status };
                }
                else if ($scope.add_or_edit == 'add' && $scope.actionForm == 'config') {
                    var a = { sims_cur_code: $scope.cur_code, sims_academic_year: $scope.aca_year, sims_report_card_config_desc: $scope.config_desc, sims_report_card_config_status: config_status };
                }

                var bubble_class;
                if ($scope.config_status == 'A') {
                    bubble_class = 'label-danger';
                }
                else {
                    bubble_class = 'label-daefault';
                }
                $http.post(ENV.apiUrl + "api/GradebookNew/ConfigInsertion?cTemp=B", a).then(function (res) {


                    if ($scope.add_or_edit == 'edit' && $scope.actionForm == "config") {

                        $scope.findIndex($scope.config_code_edit, 'config');
                        $scope.AllConfigs[$scope.configindextoupdate] = res.data;

                    }
                    else if ($scope.add_or_edit == 'add' && $scope.actionForm == "config") {
                        $scope.AllConfigs.push(res.data);
                    }
                    $scope.config_status = "";
                    $scope.config_desc = "";
                    $scope.closeAddNew();
                });
            }

            $scope.saveTermMaster = function () {
                var term_status;
                if ($scope.term_status == true) {
                    term_status = 'A';
                }
                else {
                    term_status = 'I';
                }
                if ($scope.sims_report_card_is_co_scolastic_term == true) {
                    $scope.sims_report_card_is_co_scolastic_term = 'A';
                }
                else {
                    $scope.sims_report_card_is_co_scolastic_term = 'I';
                }

                if ($scope.add_or_edit == 'edit' && $scope.actionForm == "term") {

                    var data = {
                        sims_cur_code: $scope.cur_code, sims_academic_year: $scope.aca_year, sims_report_card_config_code: $scope.config_code, sims_report_card_term_code: $scope.termcode_edit, sims_report_card_term_desc: $scope.term_desc_en,
                        sims_report_card_term_desc_ot: $scope.term_desc_ot, sims_report_card_term_st_date: $scope.term_start_date, sims_report_card_term_end_date: $scope.term_end_date, sims_report_card_term_grade_scale: $scope.term_grade_scale,
                        sims_report_card_term_status: term_status, sims_term_display_order: $scope.sims_term_display_order, sims_report_card_is_co_scolastic: $scope.sims_report_card_is_co_scolastic_term
                    }
                }
                else if ($scope.add_or_edit == 'add' && $scope.actionForm == "term") {
                    var data = {
                        sims_cur_code: $scope.cur_code, sims_academic_year: $scope.aca_year, sims_report_card_config_code: $scope.config_code, sims_report_card_term_desc: $scope.term_desc_en,
                        sims_report_card_term_desc_ot: $scope.term_desc_ot, sims_report_card_term_st_date: $scope.term_start_date, sims_report_card_term_end_date: $scope.term_end_date, sims_report_card_term_grade_scale: $scope.term_grade_scale,
                        sims_report_card_term_status: term_status, sims_term_display_order: $scope.sims_term_display_order, sims_report_card_is_co_scolastic: $scope.sims_report_card_is_co_scolastic_term
                    }
                }


                $http.post(ENV.apiUrl + "api/gradebooknew/terminsertion?ctemp=h", data).then(function (res) {
                    debugger
                    if ($scope.add_or_edit == 'edit' && $scope.actionForm == "term") {

                        $scope.findIndex($scope.termcode_edit, 'term');
                        $scope.AllTerms[$scope.termindextoupdate] = res.data;

                    }
                    else if ($scope.add_or_edit == 'add' && $scope.actionForm == "term") {
                        $scope.AllTerms.push(res.data);
                    }

                    if (res.data.inserted_flg == true) {
                        $(document).ready(function () {
                        });
                        swal({ title: "Alert", text: "Term Created successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        $scope.getTermsmaster();
                        $scope.closeAddNewMaster();
                    }
                    else {
                        swal({ title: "Alert", text: "Unable To Create Term. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }

                    $scope.closeAddNew();
                });
            }

            $scope.saveTerm = function () {
                debugger
                var term_status;
                if ($scope.term_status == true) {
                    term_status = 'A';
                }
                else {
                    term_status = 'D';
                }

                if ($scope.sims_report_card_is_co_scolastic_term == true) {
                    $scope.sims_report_card_is_co_scolastic_term = 'A';
                }
                else {
                    $scope.sims_report_card_is_co_scolastic_term = 'I';
                }

                //if ($scope.add_or_edit == 'edit' && $scope.actionForm == "term") {

                //    var data = {
                //        sims_cur_code: $scope.cur_code, sims_academic_year: $scope.aca_year, sims_report_card_config_code: $scope.config_code, sims_report_card_term_code: $scope.termcode_edit, sims_report_card_term_desc: $scope.term_desc_en,
                //        sims_report_card_term_desc_ot: $scope.term_desc_ot, sims_report_card_term_st_date: $scope.term_start_date, sims_report_card_term_end_date: $scope.term_end_date, sims_report_card_term_grade_scale: $scope.term_grade_scale,
                //        sims_report_card_term_status: term_status, sims_term_display_order: $scope.sims_term_display_order, sims_report_card_is_co_scolastic: $scope.sims_report_card_is_co_scolastic_term
                //    }
                //}
                //else if ($scope.add_or_edit == 'add' && $scope.actionForm == "term") {
                    var data = {
                        sims_cur_code: $scope.cur_code, sims_academic_year: $scope.aca_year, sims_report_card_config_code: $scope.config_code, sims_report_card_term_desc: $scope.term_desc_en, sims_report_card_term_code: $scope.sims_report_card_term_code_master,
                        sims_report_card_term_desc_ot: $scope.term_desc_ot, sims_report_card_term_st_date: $scope.term_start_date, sims_report_card_term_end_date: $scope.term_end_date, sims_report_card_term_grade_scale: $scope.term_grade_scale,
                        sims_report_card_term_status: term_status, sims_term_display_order: $scope.sims_term_display_order, sims_report_card_master_term_code: $scope.sims_report_card_term_code_master, sims_report_card_is_co_scolastic: $scope.sims_report_card_is_co_scolastic_term
                    }
               // }


                $http.post(ENV.apiUrl + "api/gradebooknew/terminsertion?ctemp=AS", data).then(function (res) {
                    debugger
                    if ($scope.add_or_edit == 'edit' && $scope.actionForm == "term") {

                        $scope.findIndex($scope.termcode_edit, 'term');
                        $scope.AllTerms[$scope.termindextoupdate] = res.data;

                    }
                    else if ($scope.add_or_edit == 'add' && $scope.actionForm == "term") {
                      //  $scope.AllTerms.push(res.data);
                    }

                    if (res.data.inserted_flg == true) {
                        $(document).ready(function () {
                        });
                        swal({ title: "Alert", text: "Term Created successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        $scope.getTerms();
                        $scope.closeAddNew();
                    }
                    else {
                        swal({ title: "Alert", text: "Unable To Create Term. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }

                    $scope.closeAddNew();
                });
            }

            $scope.editCategoryMapping = function (str, str1) {
                debugger
                $scope.cat_code = parseInt(str, 10);
                $scope.TilePostFix = str1;
                $scope.catConfigShow = true;
                $scope.addNewScreen = true;
                $scope.add_or_edit = 'add';
                $scope.cat_code_of_config = $scope.cat_code;
                $scope.getAllActiveUnmappedTerms();
                var a = { opr: 'AD', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code, cat_code: $scope.cat_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllMappedCategoryData = res.data.table;
                    //console.log("Mapped categories", res.data.table);
                });
            }

            $scope.editCategoryMappingMaster = function (str, str1) {
                debugger
                $scope.cat_code = parseInt(str, 10);
                $scope.TilePostFix = str1;
                $scope.catConfigShowMaster = true;
                $scope.addNewScreenMaster = true;
                $scope.assignConfigShowMaster = false;
                $scope.add_or_edit = 'add';
                $scope.cat_code_of_config = $scope.cat_code;
                $scope.getAllActiveUnmappedTerms();
                var a = { opr: 'AD', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code, cat_code: $scope.cat_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllMappedCategoryData = res.data.table;
                    //console.log("Mapped categories", res.data.table);
                });
            }

            $scope.editTermMappingMaster = function (str, str1) {
                debugger
                $scope.cat_code = parseInt(str, 10);
                $scope.TilePostFix = str1;
               // $scope.addNewScreenMaster = true;
                $scope.termConfigShow = true;
                $scope.add_or_edit = 'add';
                $scope.cat_code_of_config = $scope.cat_code;
                $scope.getAllActiveUnmappedTerms();
                var a = { opr: 'AD', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code, cat_code: $scope.cat_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllMappedCategoryData = res.data.table;
                    //console.log("Mapped categories", res.data.table);
                });
            }


            $scope.getAllMasterAssignment = function () {
                debugger
                var a = { opr: 'BG', cur_code: $scope.cur_code, aca_year: $scope.aca_year };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllmasterAssignment = res.data.table;
                    //console.log("Mapped categories", res.data.table);
                });
            }

            $scope.findIndex = function (id, str) {
                if (str == 'term') {
                    $($scope.AllTerms).each(function (k, v) {
                        if (v.sims_report_card_term_code == id) {
                            $scope.termindextoupdate = k;
                        }
                    });
                }
                else if (str == 'config') {
                    $($scope.AllConfigs).each(function (k, v) {
                        if (v.sims_report_card_config_code == id) {
                            $scope.configindextoupdate = k;
                        }
                    });
                }
            }

            $scope.editTerm = function (e) {
                $scope.add_or_edit = 'edit';
                $scope.actionForm = 'term';
                $scope.getGradingScaleData();
                if ($("#term_divMaster").find('input:checked').length == 1) {
                    $scope.addNewScreen = true;
                    $scope.addNewTerm = true;
                    $("#term_divMaster").find('input:checked').each(function (k, v) {

                        if ($(v).parents('.div_item').data('status') == 'A') {
                            $scope.term_status = true;
                        }
                        else {
                            $scope.term_status = false;
                        }
                        $scope.termcode_edit = $(v).parents('.div_item').data('termcode');
                        $scope.term_desc_en = $(v).parents('.div_item').data('termdesc');
                        $scope.term_desc_ot = $(v).parents('.div_item').data('termdescot');
                        $scope.term_start_date = $(v).parents('.div_item').data('termstart');
                        $scope.term_end_date = $(v).parents('.div_item').data('termend');
                        $scope.term_grade_scale = $(v).parents('.div_item').data('gradescale');
                        $scope.sims_term_display_order = $(v).parents('.div_item').data('simstermdisplayorder');
                    });
                }
                else {
                    alert("Select one at a time for edit");
                }
            }

            $scope.editTermMaster = function (e) {
                debugger
                $scope.add_or_edit = 'edit';
                $scope.actionForm = 'term';
                $scope.getGradingScaleData();
                if ($("#term_divMaster").find('input:checked').length == 1) {
                    $scope.addNewScreenMaster = true;
                    $scope.addNewTermMaster = true;
                    $("#term_divMaster").find('input:checked').each(function (k, v) {

                        if ($(v).parents('.div_item').data('status') == 'A') {
                            $scope.term_status = true;
                        }
                        else {
                            $scope.term_status = false;
                        }
                        $scope.termcode_edit = $(v).parents('.div_item').data('termcode');
                        $scope.term_desc_en = $(v).parents('.div_item').data('termdesc');
                        $scope.term_desc_ot = $(v).parents('.div_item').data('termdescot');
                        $scope.term_start_date = $(v).parents('.div_item').data('termstart');
                        $scope.term_end_date = $(v).parents('.div_item').data('termend');
                        $scope.term_grade_scale = $(v).parents('.div_item').data('gradescale');
                        $scope.sims_term_display_order = $(v).parents('.div_item').data('simstermdisplayorder');
                    });
                }
                else {
                    alert("Select one at a time for edit");
                }
            }

            $scope.editMasterCategory = function (e) {
                $scope.add_or_edit = 'edit';
                $scope.actionForm = 'category';
                $scope.getGradingScaleData();
                $scope.getCatWeightage();
                if ($("#category_div_master").find('input:checked').length == 1) {
                    $scope.addNewScreenMaster = true;
                    $scope.addNewCatMaster = true;
                    $("#category_div_master").find('input:checked').each(function (k, v) {

                        if ($(v).parents('.div_cat_item').data('catstatus') == 'A') {
                            $scope.cat_status = true;
                        }
                        else {
                            $scope.cat_status = false;
                        }
                        if ($(v).parents('.div_cat_item').data('catincludeinfinal') == 'Y') {
                            $scope.includeFinnal = true;
                        }
                        else {
                            $scope.includeFinnal = false;
                        }
                        $scope.cat_code = $(v).parents('.div_cat_item').data('catcode');
                        $scope.cat_name_en = $(v).parents('.div_cat_item').data('catname');
                        $scope.cat_name_ot = $(v).parents('.div_cat_item').data('catnameot');
                        $scope.cat_desc = $(v).parents('.div_cat_item').data('catdesc');
                        $scope.cat_type = $(v).parents('.div_cat_item').data('cattype');
                        $scope.cat_weightage_type = $(v).parents('.div_cat_item').data('catwttype');
                        $scope.cat_weightage_value = $(v).parents('.div_cat_item').data('catwtvalue');
                        $scope.cat_max_of_assign_count = $(v).parents('.div_cat_item').data('catassigncnt');
                        $scope.cat_score_type = $(v).parents('.div_cat_item').data('catscoretype');
                        $scope.cat_point_poss = $(v).parents('.div_cat_item').data('catpoints');
                        $scope.cat_color = $(v).parents('.div_cat_item').data('catcolorcode');
                        $scope.cat_grade_scale = $(v).parents('.div_cat_item').data('catscalecode');
                        $scope.cat_parents_code = $(v).parents('.div_cat_item').data('catparentscode');
                    });
                }
                else {
                    alert("Select one at a time for edit");
                }
            }

            $scope.editMasterAttribute = function (e) {
                debugger;
                $scope.add_or_edit = 'edit';
                $scope.actionForm = 'Attribute';
                if ($("#Div55").find('input:checked').length == 1) {
                    $scope.addNewScreenMaster = true;
                    $scope.addNewAattributeMaster = true;
                    $("#Div55").find('input:checked').each(function (k, v) {

                        if ($(v).parents('.div_cat_item').data('attrstatus') == 'A') {
                            $scope.att.sims_attribute_status = true;
                        }
                        else {
                            $scope.att.sims_attribute_status = false;
                        }

                        $scope.att.sims_attribute_name = $(v).parents('.div_cat_item').data('attrname');
                        $scope.att.sims_attribute_name_ot = $(v).parents('.div_cat_item').data('attrnameot');
                        $scope.att.sims_attribute_display_order = $(v).parents('.div_cat_item').data('attrdisplay');
                        $scope.att.sims_cat_assign_subject_attribute_max_score = $(v).parents('.div_cat_item').data('attrmax');
                       // $scope.att.sims_attribute_status = $(v).parents('.div_cat_item').data('attrstatus');
                        $scope.att.sims_attribute_code = $(v).parents('.div_cat_item').data('attrcode');
                        $scope.att.sims_attribute_type = $(v).parents('.div_cat_item').data('attrtype');
                    });
                }
                else {
                    alert("Select one at a time for edit");
                }
            }

            $scope.editMasterAssignment = function (e) {
                debugger
                $scope.add_or_edit = 'edit';
                $scope.actionForm = 'assignment';
                $scope.getGradingScaleData();
                $scope.getAllMasterAssignment();
               // $scope.getAssignmentType();
                if ($("#assign_div2_master").find('input:checked').length == 1) {
                    $scope.addNewScreenMaster = true;
                    $scope.addNewAssignMaster = true;
                    $("#assign_div2_master").find('input:checked').each(function (k, v) {
                        if ($(v).parents('.div_assign_item').data('assignstatus') == 'A') {
                            $scope.assign_status = true;
                        }
                        else {
                            $scope.assign_status = false;
                        }
                        if ($(v).parents('.div_assign_item').data('includeinfinal') == 'Y') {
                            $scope.assign_include_in_final_grade = true;
                        }
                        else {
                            $scope.assign_include_in_final_grade = false;
                        }
                        $scope.assign_number = $(v).parents('.div_assign_item').data('assignnum');
                        $scope.assign_name = $(v).parents('.div_assign_item').data('assignname');
                        $scope.assign_name_ot = $(v).parents('.div_assign_item').data('assignnameot');
                        $scope.assign_desc = $(v).parents('.div_assign_item').data('assigndesc');
                        $scope.assign_type = $(v).parents('.div_assign_item').data('assigntype');
                        $scope.cat_grade_scale = $(v).parents('.div_assign_item').data('gradescale');
                        //$scope.sims_report_card_parent_assignment_code = $(v).parents('.div_assign_item').data('assignmastercode');

                        for (var i = 0; i < $scope.AllmasterAssignment.length; i++) {
                            if ($scope.AllmasterAssignment[i].sims_report_card_assign_number == $(v).parents('.div_assign_item').data('assignmastercode')) {
                                $scope.sims_report_card_parent_assignment_code = $scope.AllmasterAssignment[i].sims_report_card_assign_number;
                            } else {
                                $scope.sims_report_card_parent_assignment_code = '';
                            }
                        }
                    });
                }
                else {
                    alert("Select one at a time for edit");
                }
            }


            $scope.editMasterGrade = function (e) {
                debugger;
                $scope.gradesaveUpdateBtn=false;
                $scope.add_or_edit = 'edit';
                $scope.actionForm = 'grade';
                $scope.gradgroup = [];
                $scope.getGradingScaleData();
               // $scope.getCatWeightage();
                if ($("#grade_div2_master").find('input:checked').length == 1) {
                    $scope.addNewScreenMaster = true;
                     $scope.addNewGradeScaleMaster = true;
                     $("#grade_div2_master").find('input:checked').each(function (k, v) {

                        if ($(v).parents('.div_grade_item').data('gradegroupstatus') == 'A') {
                            $scope.gradgroup.status = true;
                        }
                        else {
                            $scope.gradgroup.status = false;
                        }
                        
                        $scope.gradgroup.Gradecode = $(v).parents('.div_grade_item').data('gradegroupcode');
                        $scope.gradgroup.gradeName = $(v).parents('.div_grade_item').data('groupname');
                        $scope.gradgroup.description = $(v).parents('.div_grade_item').data('gradegroupdesc');
                        
                         
                    });
                }
                else {
                    alert("Select one at a time for edit");
                }
            }

            $scope.getCatWeightage = function () {
                var a = { opr: 'X', cur_code: $scope.cur_code, aca_year: $scope.aca_year };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllCategoryType = res.data.table;
                    $scope.AllCatWeightage = res.data.table1;
                    $scope.AllScoreType = res.data.table2;
                    $scope.AllCategoryData = res.data.table3;


                    $scope.cat_weightage_type = $scope.AllCatWeightage[0].sims_appl_parameter;
                    $scope.cat_score_type = $scope.AllScoreType[0].sims_appl_parameter;
                    $scope.cat_type = $scope.AllCategoryType[0].sims_appl_parameter;
                });
            }

            $scope.getmasterCategories = function () {
                debugger
                var aa = { opr: 'AP', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code, term_code: $scope.term_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", aa).then(function (res) {
                    $scope.AllMaterCategories = res.data.table;
                    
                    $scope.getAllActiveUnmappedTerms_master($scope.term_code);
                    $scope.sims_report_card_category_code_master = $scope.AllMaterCategories[0].sims_report_card_category_code;
                });
            }

            $scope.getMasterCategories_created = function () {
            }

            $scope.getMasterAssignments = function () {
                var a = { opr: 'AA', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.MasterAssignment = res.data.table;
                });
            }

            $scope.getAllActiveUnmappedTerms = function () {
                debugger
                var a = { opr: 'AC', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code, cat_code: $scope.cat_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllActiveTerms = res.data.table;
                    //$scope.term_code = $scope.AllActiveTerms[0].sims_report_card_term_code;
                    //$scope.getAllActiveUnmappedTerms_master($scope.term_code);
                    var a = { opr: 'AD', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code, cat_code: $scope.sims_report_card_category_code_master, term_code: null };
                    $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                        $scope.AllMappedCategoryData = res.data.table;
                        //console.log("Mapped categories", res.data.table);
                        //});

                    });
                    //console.log("AllActiveTerms", $scope.AllActiveTerms);
                });
            }


            $scope.getAllActiveUnmappedTerms_master = function (str) {
                debugger
                //var a = { opr: 'AC', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code, cat_code: $scope.cat_code };
                //$http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                //    $scope.AllActiveTerms = res.data.table;
                   

                var a = { opr: 'AD', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code, cat_code: $scope.sims_report_card_category_code_master, term_code: $scope.term_code };
                    $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                        if (res.data.table.length > 0) {
                            $scope.AllMappedCategoryData = res.data.table;
                        } else {
                            $scope.AllMappedCategoryData = [];
                        }
                        //console.log("Mapped categories", res.data.table);
                    //});

                });
            }

            //$scope.getAllActiveUnmappedCategory_master = function (str) {
            //    debugger
            //    var a = { opr: 'AP', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code, term_code: $scope.term_code };
            //    $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
            //        $scope.AllMaterCategories = res.data.table;


            //        var a = { opr: 'AD', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code, cat_code: $scope.sims_report_card_category_code_master };
            //        $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
            //            $scope.AllMappedCategoryData = res.data.table;
            //            //console.log("Mapped categories", res.data.table);
            //        });

            //    });
            //}

            $scope.getSelectedTermData = function (str) {
                debugger;
                var a = { opr: 'AR', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.SelectedTermData = res.data.table;

                    $scope.term_desc_en = $scope.SelectedTermData[0].sims_report_card_term_desc;
                    $scope.term_desc_ot = $scope.SelectedTermData[0].sims_report_card_term_desc_ot;
                    //console.log("Mapped categories", res.data.table);
                  //  $scope.getTerms_master_term($scope.sims_report_card_term_code_master);
                });
            }


            $scope.getSelectedTermDataSingleTerm = function (str) {
                debugger;
                var a = { opr: 'BD', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code, term_code: str };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.SelectedSingleTermData = res.data.table;

                    $scope.term_desc_en = $scope.SelectedSingleTermData[0].sims_report_card_term_desc;
                    $scope.term_desc_ot = $scope.SelectedSingleTermData[0].sims_report_card_term_desc_ot;
                    $scope.term_start_date = $scope.SelectedSingleTermData[0].sims_report_card_term_st_date;
                    $scope.term_end_date = $scope.SelectedSingleTermData[0].sims_report_card_term_end_date;
                    //console.log("Mapped categories", res.data.table);
                    //  $scope.getTerms_master_term($scope.sims_report_card_term_code_master);
                });
            }

            $scope.getAllActiveCatTerms = function (obj) {
                var a = { opr: 'AJ', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllActiveCatTerms = res.data.table;

                    $scope.ct = null;
                    $scope.getAllRemainingAssignment($scope.ct);
                });
            }

            $scope.getAllRemainingAssignment = function (obj) {
                debugger
                var catcode;
                var termcode;
                if (obj == null) {
                    catcode = null;
                    termcode = null;
                } else {
                    catcode = obj.split("_")[0];
                    termcode = obj.split("_")[1];
                }
                var a = { opr: 'BB', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code, term_code: termcode, cat_code: catcode };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.MasterAssignment_remain = res.data.table;
                   
                    var aa = {
                        opr: 'AH', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code, term_code: termcode, cat_code: catcode
                    };
                    $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", aa).then(function (res) {
                        if(res.data.table.length>0){
                        $scope.AllMappedAssignmentData = res.data.table
                    }else{
                        $scope.AllMappedAssignmentData=[];
                }
                        //console.log("Mapped Assignment", res.data.table);
                        $scope.sims_report_card_assign_number_master = $scope.MasterAssignment_remain[0].sims_report_card_assign_number;
                    });
                });
            }

            $scope.getAllActiveCatTerms_master = function (obj) {
                debugger
                var a = { opr: 'AJ', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllActiveCatTerms = res.data.table;
                    $scope.ct = null;//$scope.AllActiveCatTerms[0].sims_report_card_category_code + "_" + $scope.AllActiveCatTerms[0].sims_report_card_term_code;
                    //console.log("All Active Category Terms Data", $scope.AllActiveCatTerms);
                    $scope.getAllRemainingAssignment($scope.ct);
                });
            }

            $scope.getAssignmentType = function () {
                var a = { opr: 'Y' };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllAssignmentType = res.data.table;
                    $scope.assign_type = $scope.AllAssignmentType[0].sims_appl_parameter;
                });
            }

            $scope.saveCategory = function () {
                debugger
                var includeFinnal, cat_status;
                if ($scope.includeFinnal == true) {
                    $scope.includeFinnal = 'Y';
                }
                else {
                    $scope.includeFinnal = 'N'
                }

                if ($scope.cat_status == true) {
                    $scope.cat_status = 'A';
                }
                else {
                    $scope.cat_status = 'I';
                }
                if ($scope.sims_report_card_is_co_scolastic_cat == true) {
                    $scope.sims_report_card_is_co_scolastic_cat = 'A';
                } else {
                    $scope.sims_report_card_is_co_scolastic_cat = 'I';
                }

                var a = {
                    sims_cur_code: $scope.cur_code,
                    sims_academic_year: $scope.aca_year,
                    sims_report_card_category_code: $scope.cat_code,
                    sims_report_card_category_name: $scope.cat_name_en,
                    sims_report_card_category_name_ot: $scope.cat_name_ot,
                    sims_report_card_category_desc: $scope.cat_desc,
                    sims_report_card_category_max_of_assign_count: $scope.cat_max_of_assign_count,
                    sims_report_card_category_type: $scope.cat_type,
                    sims_report_card_category_weightage_type: $scope.cat_weightage_type,
                    sims_report_card_category_weightage_value: 0,
                    sims_report_card_category_score_type: $scope.cat_score_type,
                    sims_report_card_category_points_possible: 0,
                    sims_report_card_category_grade_scale_code: $scope.cat_grade_scale,
                    sims_report_card_category_color_code: $scope.cat_color,
                    sims_report_card_category_include_in_final_grade: $scope.includeFinnal,
                    sims_report_card_category_status: $scope.cat_status,
                    sims_report_card_parent_category_code: $scope.cat_parents_code,
                    sims_report_card_is_co_scolastic: $scope.sims_report_card_is_co_scolastic_cat
                };

                $http.post(ENV.apiUrl + "api/GradebookNew/CategoryInsertion?cTemp=J", a).then(function (res) {


                    if (res.data> 0) {
                        $(document).ready(function () {
                        });
                        swal({ title: "Alert", text: "Category Created successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        $scope.getMasterCategories();
                        $scope.closeAddNewMaster();
                    }
                    else {
                        swal({ title: "Alert", text: "Unable To Create Category.", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        $scope.getMasterCategories();
                        $scope.closeAddNewMaster();
                    }

                    $scope.getMasterCategories();
                    $scope.closeAddNew();
                });
            }

            $scope.configCat = function (obj) {
                $scope.closeAddNew();
                $scope.addNewScreen = true;
                $scope.catConfigShow = true;
                $scope.cat_config_status = true;
                $scope.cat_code_of_config = obj.sims_report_card_category_code;

            }

            $scope.editConfigAssign = function (str, str1) {
                debugger
                $scope.closeAddNew();
                $scope.addNewScreen = true;
                $scope.assignConfigShow = true;
                $scope.assign_code_of_config = parseInt(str, 10);
                $scope.TilePostFix = str1;
                $scope.assign_number = parseInt(str, 10);
                $scope.getAllActiveCatTerms();
                var a = { opr: 'AH', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code, assign_number: $scope.assign_number };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllMappedAssignmentData = res.data.table;
                    //console.log("Mapped Assignment", res.data.table);
                });
            }


            $scope.editConfigAssignMaster = function (str, str1) {
                debugger
                $scope.closeAddNew();
                $scope.addNewScreenMaster = true;
                $scope.assignConfigShowMaster = true;
                $scope.assign_code_of_config = parseInt(str, 10);
                $scope.TilePostFix = str1;
                $scope.assign_number = parseInt(str, 10);
                $scope.getAllActiveCatTerms();
                var a = { opr: 'AH', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code, assign_number: $scope.assign_number };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllMappedAssignmentData = res.data.table;
                    //console.log("Mapped Assignment", res.data.table);
                });
            }

            $scope.saveCatConfig = function () {
                debugger
                var cat_config_status, cat_freeze_status;
                if ($scope.cat_config_status == true) {
                    $scope.cat_config_status = 'A';
                }
                else {
                    $scope.cat_config_status = 'I';
                }

                if ($scope.cat_freeze_status == true) {
                    $scope.cat_freeze_status = 'A';
                }
                else {
                    $scope.cat_freeze_status = 'I';
                }

                var cTemp;
                if ($scope.add_or_edit == 'edit') {
                    cTemp = 'AE';
                }
                else {
                    cTemp = 'S';
                    $scope.term_code_old = $scope.term_code;
                }

                var a = {
                    sims_cur_code: $scope.cur_code, sims_academic_year: $scope.aca_year, sims_report_card_config_code: $scope.config_code, sims_report_card_term_code: $scope.term_code, sims_report_card_term_code_old: $scope.term_code_old,
                    sims_report_card_category_code: $scope.sims_report_card_category_code_master, sims_report_card_category_st_date: $scope.cat_start_date, sims_report_card_category_end_date: $scope.cat_end_date,
                    sims_report_card_category_publish_date: $scope.cat_publish_date, sims_report_card_category_freeze_status: $scope.cat_freeze_status, sims_report_card_category_config_status: $scope.cat_config_status
                }


                $http.post(ENV.apiUrl + "api/GradebookNew/ConfigCategoryInsertion?cTemp=" + cTemp, a).then(function (res) {
                    if (res.data) {
                        swal({ title: "Alert", text: "Category Created successfully", imageUrl: "assets/img/check.png", width: 300, height: 150 });
                        $scope.closeAddNew();
                        $scope.getCategories();
                    }
                    else {
                        swal({ title: "Alert", text: "Unable To Create Category", imageUrl: "assets/img/close.png", width: 300, height: 150 });
                    }

                });
            }

            $scope.postSavedMappedCategory = function (e, str) {
                debugger
                var idx = $scope.AllMappedCategoryData.indexOf(e);
                //console.log('idx value', idx);
                if (str == "edit") {
                    //console.log("Edit saved mapped data", e);
                    $scope.term_code = e.sims_report_card_term_code;
                    $scope.term_code_old = e.sims_report_card_term_code_old;
                    $scope.cat_code_of_config = e.sims_report_card_category_code;
                    $scope.cat_name_en = e.sims_report_card_category_name;
                    $scope.cat_start_date = e.sims_report_card_category_st_date;
                    $scope.cat_end_date = e.sims_report_card_category_end_date;
                    $scope.cat_publish_date = e.sims_report_card_category_publish_date;
                    $scope.cat_freeze_status = e.sims_report_card_category_freeze_status == 'A' ? true : false;
                    $scope.cat_config_status = e.sims_report_card_category_config_status == 'A' ? true : false;
                    $scope.sims_report_card_category_code_master = e.sims_report_card_category_code;
                    $scope.add_or_edit = 'edit';
                  //  $scope.AllActiveTerms.push({ sims_report_card_term_code: $scope.term_code, sims_report_card_term_code_old: $scope.term_code_old, sims_report_card_term_desc: e.sims_report_card_term_desc });
                    $scope.AllMaterCategories = [];
                    $scope.AllMaterCategories.push({
                        sims_academic_year: e.sims_academic_year,
                        sims_cur_code: e.sims_cur_code,
                        sims_report_card_category_code: e.sims_report_card_category_code,
                        sims_report_card_category_config_status: e.sims_report_card_category_config_status,
                        sims_report_card_category_desc: e.sims_report_card_category_desc,
                        sims_report_card_category_end_date: e.sims_report_card_category_end_date,
                        sims_report_card_category_freeze_status: e.sims_report_card_category_freeze_status,
                        sims_report_card_category_publish_date:e.sims_report_card_category_publish_date,
                        sims_report_card_category_st_date: e.sims_report_card_category_st_date,
                        sims_report_card_config_code:e.sims_report_card_config_code,
                        sims_report_card_term_code: e.sims_report_card_term_code,
                        sims_report_card_term_code_old: e.sims_report_card_term_code_old,
                        sims_report_card_term_desc:e.sims_report_card_term_desc,
                        sims_report_card_category_name: e.sims_report_card_category_desc
                    });

                    $scope.sims_report_card_category_code_master = $scope.AllMaterCategories[0].sims_report_card_category_code;
                }
                if (str == "trash") {
                    var retVal = confirm("Do you want to delete the record ?");
                    if (retVal == true) {
                        //console.log("Delete this saved mapped data", e);
                        var a = { opr: 'U', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code, cat_code: e.sims_report_card_category_code, term_code: e.sims_report_card_term_code };
                        $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                            //console.log("Delete Data flag", res.data);
                            $scope.AllMappedCategoryData.splice(idx, 1);
                        });
                    }
                    else {
                        alert("Record not deleted!");
                    }
                }
            }

            $scope.postSavedMappedTerm = function (e, str) {
                debugger
                var idx = $scope.AllTermsMaster.indexOf(e);
                //console.log('idx value', idx);
                if (str == "edit") {

                    if (e.sims_report_card_term_status=='A') {
                        $scope.term_status = true;
                    }else{
                        $scope.term_status = false;
                }
                    //console.log("Edit saved mapped data", e);
                    $scope.sims_report_card_term_code_master = e.sims_report_card_term_code;
                    $scope.term_desc_en = e.sims_report_card_term_desc;
                    $scope.term_desc_ot = e.sims_report_card_term_desc_ot;
                    $scope.term_start_date = e.sims_report_card_term_st_date;
                    $scope.term_end_date = e.sims_report_card_term_end_date;
                    $scope.term_grade_scale = e.sims_report_card_term_grade_scale_code;
                    $scope.sims_term_display_order = e.sims_term_display_order;
                    //$scope.term_status = e.sims_report_card_term_status;
                    //$scope.AllTermsMaster = [];
                    //$scope.AllTermsMaster.push({
                    //        sims_report_card_term_code_master : e.sims_report_card_term_code,
                    //        sims_report_card_term_desc : e.sims_report_card_term_desc,
                    //        sims_report_card_term_desc_ot : e.sims_report_card_term_desc_ot,
                    //        sims_report_card_term_st_date : e.sims_report_card_term_st_date,
                    //        sims_report_card_term_end_date : e.sims_report_card_term_end_date,
                    //        sims_report_card_term_grade_scale_code : e.sims_report_card_term_grade_scale_code,
                    //        sims_term_display_order : e.sims_term_display_order
                    //});
                }
                if (str == "trash") {
                    var retVal = confirm("Do you want to delete the record ?");
                    if (retVal == true) {
                        //console.log("Delete this saved mapped data", e);
                        var a = { opr: 'AY', cur_code: $scope.cur_code, aca_year: $scope.aca_year, term_code: term_code ,config_code:$scope.sims_report_card_config_code};
                        $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {

                            if (res.data.table[0].inserted_flg > 0) {
                                $(document).ready(function () {
                                });
                                swal({ title: "Alert", text: "Term Deleted successfully.", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Unable To Delete Term. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }

                            $scope.getTerms();
                            $scope.getCategories();
                            $scope.getMasterCategories();
                            $scope.getAssignments();
                            $scope.getMasterAssignments();
                        });
                    }
                    else {
                        alert("Record not deleted!");
                    }
                }
            }

            $scope.postSavedMappedAssignment = function (e, str) {
                debugger
                var idx = $scope.AllMappedAssignmentData.indexOf(e);
                //console.log('idx value', idx);
                if (str == "edit") {
                    //console.log("Edit saved mapped data", e);
                    $scope.term_code = e.sims_report_card_term_code;
                    $scope.cat_code = e.sims_report_card_category_code;
                    $scope.term_code_old = e.sims_report_card_term_code_old;
                    $scope.cat_code_old = e.sims_report_card_category_code_old;
                    $scope.assign_number = e.sims_report_card_assign_number;
                    $scope.assign_max_score = e.sims_report_card_assign_max_score;
                    $scope.assign_start_date = e.sims_report_card_assign_st_date;
                    $scope.assign_end_date = e.sims_report_card_assign_end;
                    $scope.assign_publish_date = e.sims_report_card_assign_publish_date;
                    $scope.cat_term = $scope.cat_code + "_" + $scope.term_code;
                    $scope.assign_freeze_status = e.sims_report_card_assign_freeze_status == 'A' ? true : false;
                    $scope.assign_config_status = e.sims_report_card_assign_config_status == 'A' ? true : false;
                    $scope.sims_allow_edit = e.sims_allow_edit == 'A' ? true : false;
                    $scope.sims_assign_remark_visible = e.sims_assign_remark_visible=='A'? true:false;

                    $scope.ct = e.sims_report_card_category_code + "_" + e.sims_report_card_term_code;

                    $scope.MasterAssignment_remain = [];

                    $scope.MasterAssignment_remain.push({
                        sims_academic_year: e.sims_academic_year,
                        sims_allow_edit: e.sims_allow_edit,
                        sims_cur_code: e.sims_cur_code,
                        sims_report_card_assign_name: e.sims_report_card_assign_name,
                        sims_report_card_assign_number: e.sims_report_card_assign_number,
                        sims_report_card_category_code: e.sims_report_card_category_code,
                        sims_report_card_category_code_old: e.sims_report_card_category_code_old,
                        sims_report_card_category_desc: e.sims_report_card_category_desc,
                        sims_report_card_config_code: e.sims_report_card_config_code,
                        sims_report_card_term_code: e.sims_report_card_term_code,
                        sims_report_card_term_code_old: e.sims_report_card_term_code_old,
                        sims_report_card_term_desc: e.sims_report_card_term_desc
                    });
                    $scope.sims_report_card_assign_number_master = $scope.MasterAssignment_remain[0].sims_report_card_assign_number

                    $scope.add_or_edit = 'edit';
                    $scope.AllMappedAssignmentData.splice(idx, 1);
                }
                if (str == "trash") {
                    var retVal = confirm("Do you want to delete the record ?");
                    if (retVal == true) {
                        //console.log("Delete this saved mapped data", e);
                        var a = { opr: 'V', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code, cat_code: e.sims_report_card_category_code, term_code: e.sims_report_card_term_code, assign_number: e.sims_report_card_assign_number };
                        $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                            //console.log("Delete Data flag", res.data);
                            $scope.AllMappedAssignmentData.splice(idx, 1);
                        });
                    }
                    else {
                        alert("Record not deleted!");
                    }
                }
            }

            $scope.saveAssignConfig = function () {
                debugger
                if ($scope.assign_max_score == '' || $scope.assign_max_score == undefined) {
                    swal({ title: "Alert", text: "Please make sure you enter a max marks", imageUrl: "assets/img/check.png", width: 300, height: 150 });
                }else{
                var assign_config_status, assign_freeze_status;
                if ($scope.assign_config_status == true) {
                    $scope.assign_config_status = 'A';
                }
                else {
                    $scope.assign_config_status = 'I';
                }

                if ($scope.assign_freeze_status == true) {
                    $scope.assign_freeze_status = 'A';
                }
                else {
                    $scope.assign_freeze_status = 'I';
                }

                if ($scope.sims_allow_edit == true) {
                    $scope.sims_allow_edit = 'A';
                }
                else {
                    $scope.sims_allow_edit = 'I';
                }

                if ($scope.sims_assign_remark_visible == true) {
                    $scope.sims_assign_remark_visible = 'A';
                }
                else {
                    $scope.sims_assign_remark_visible = 'I';
                }



                var cTemp;
                if ($scope.add_or_edit == 'edit') {
                    cTemp = 'AI';
                }
                else {
                    cTemp = 'T';
                    $scope.term_code_old = $scope.ct.split("_")[1];
                    $scope.cat_code_old = $scope.ct.split("_")[0];
                }


                var a = {
                    sims_cur_code: $scope.cur_code, sims_academic_year: $scope.aca_year, sims_report_card_config_code: $scope.config_code, sims_report_card_term_code: $scope.ct.split("_")[1],
                    sims_report_card_category_code: $scope.ct.split("_")[0], sims_report_card_term_code_old: $scope.term_code_old, sims_report_card_category_code_old: $scope.cat_code_old,
                    sims_report_card_assign_number: $scope.sims_report_card_assign_number_master, sims_report_card_assign_st_date: $scope.assign_start_date, sims_report_card_assign_end: $scope.assign_end_date, sims_report_card_assign_max_score: $scope.assign_max_score,
                    sims_report_card_assign_publish_date: $scope.assign_publish_date, sims_report_card_assign_freeze_status: $scope.assign_freeze_status, sims_report_card_assign_config_status: $scope.assign_config_status,
                    sims_allow_edit: $scope.sims_allow_edit, sims_assign_remark_visible: $scope.sims_assign_remark_visible
                }

                $http.post(ENV.apiUrl + "api/GradebookNew/ConfigAssignmentInsertion?cTemp=" + cTemp, a).then(function (res) {
                    if (res.data) {
                        if (res.data == true) {
                            $(document).ready(function () {
                            });
                            swal({ title: "Alert", text: "Assignment Updated successfully", imageUrl: "assets/img/check.png", width: 300, height: 150 });
                            $scope.closeAddNew();
                        }
                        else {
                            swal({ title: "Alert", text: "Unable To Update Assignment", imageUrl: "assets/img/close.png", width: 300, height: 150 });
                        }
                        $scope.closeAddNewMaster();
                     //   $scope.editConfigAssign($scope.assign_number, $scope.TilePostFix);
                        $scope.getAssignments();
                    }
                    else {
                        alert("Data not saved");
                    }
                });
            }
        }

            $scope.saveAssign = function () {
                debugger
                var includeFinnal, assign_status;

                if ($scope.assign_include_in_final_grade == true) {
                    $scope.assign_include_in_final_grade = 'Y';
                }
                else {
                    $scope.assign_include_in_final_grade = 'N'
                }

                if ($scope.assign_status == true) {
                    $scope.assign_status = 'A';
                }
                else {
                    $scope.assign_status = 'I';
                }
                if ($scope.sims_report_card_is_co_scolastic_assign == true) {
                    $scope.sims_report_card_is_co_scolastic_assign = 'A';
                }
                else {
                    $scope.sims_report_card_is_co_scolastic_assign = 'I';
                }
                var a = {
                    sims_cur_code: $scope.cur_code,
                    sims_academic_year: $scope.aca_year,
                    sims_report_card_assign_number: $scope.assign_number,
                    sims_report_card_assign_name: $scope.assign_name,
                    sims_report_card_assign_name_ot: $scope.assign_name_ot,
                    sims_report_card_assign_desc: $scope.assign_desc,
                    sims_report_card_assign_type: $scope.assign_type,
                    sims_report_card_assign_score_type: $scope.assign_score_type,
                    sims_report_card_assign_grade_scale_code: $scope.assign_grade_code,
                    sims_report_card_assign_include_in_final_grade: $scope.assign_include_in_final_grade,
                    sims_report_card_assign_status: $scope.assign_status,
                    sims_report_card_assign_weightage_value: $scope.assign_weightage_value,
                    sims_report_card_assign_weightage_type: $scope.assign_weightage_type,
                    sims_report_card_assign_points_possible: $scope.assign_point_poss,
                    sims_report_card_is_co_scolastic: $scope.sims_report_card_is_co_scolastic_assign,
                    sims_report_card_parent_assignment_code: $scope.sims_report_card_parent_assignment_code
                };

                $http.post(ENV.apiUrl + "api/GradebookNew/AssignmentInsertion?cTemp=M", a).then(function (res) {
                    if (res.data) {

                        if (res.data > 0) {
                            $(document).ready(function () {
                            });
                            swal({ title: "Alert", text: "Assignment Created successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            $scope.closeAddNewMaster();
                        }
                        else {
                            swal({ title: "Alert", text: "Unable To Create Assignment.", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                         //   $scope.closeAddNewMaster();
                        }

                        $scope.getMasterAssignments();
                        $scope.closeAddNew();
                    }
                    else {
                        alert("Nothing to save");
                    }
                });
            }

            $(document).ready(function () {
                //$scope.termdivshow = false;
                $scope.showAll = false;
                $scope.showData = false;
                $scope.getCurriculum();
                $scope.user = $rootScope.globals.currentUser.username;
                $scope.gradesCheck = [];
                $scope.AllAssignment = [];
                $scope.UnAssignment = [];
                $scope.MasterAssignment = [];
                $scope.UnCategories = [];
                $scope.MasterCategories = [];
                var term_grade_scale;                
            });

            $scope.div_maingrid = false;

            $scope.getsubjectConfigGrade = function (config_code) {
                debugger
                $scope.gradeSecArr = [];
                $scope.config_code = parseInt(config_code, 10);
                var a = { opr: 'G', cur_code: $scope.cur_code, aca_year: $scope.aca_year, config_code: $scope.config_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/SubjectConfigCommon", a).then(function (res) {
                    $scope.AllsubjectConfigGrades = res.data.table;
                    $scope.sims_grade_code = $scope.AllsubjectConfigGrades[0].sims_grade_code;
                    setTimeout(function () {
                        $('#grade_select').change(function () {
                        }).multipleSelect({
                            width: '100%'
                        });
                        $("#grade_select").multipleSelect("checkAll");



                    }, 1000);

                });
            }

            $scope.getsubjectConfigsection = function (sims_grade_code) {
                debugger;
                var gradecode = '';
                for (var i = 0; i < sims_grade_code.length; i++) {
                    gradecode = gradecode + sims_grade_code + ',';
                }
                $scope.grade_code = sims_grade_code;
                var a = { opr: 'S', cur_code: $scope.cur_code, aca_year: $scope.aca_year, grade_code: gradecode, config_code: $scope.config_code };
                $http.post(ENV.apiUrl + "api/GradebookNew/SubjectConfigCommon", a).then(function (res) {
                    $scope.AllsubjectConfigSectionFromGrade = res.data.table;

                    setTimeout(function () {
                        $('#section_select').change(function () {
                        }).multipleSelect({
                            width: '100%'
                        });
                        $("#section_select").multipleSelect("checkAll");
                    }, 1000);

                });
            }

            $scope.getsectionchange = function (section_name) {
                $scope.section_code = section_name;
            }

            $scope.getGradeScalePointForSubject = function () {
                var a = { opr: 'A' };
                $http.post(ENV.apiUrl + "api/GradebookNew/SubjectConfigCommon", a).then(function (res) {
                    $scope.AllGradesScalePtforSubject = res.data.table;
                });
            }

            $scope.getGradeScalePointForCategory = function (obj1, index, subject_code) {
                debugger
                $scope.sims_mark_grade = obj1.narr_grade_group_code;
                $scope.sims_mark_grade_nm = obj1.narr_grade_group_name;

                var a = { opr: 'B', cur_code: $scope.cur_code, aca_year: $scope.aca_year, sims_mark_grade: $scope.sims_mark_grade };
                $http.post(ENV.apiUrl + "api/GradebookNew/SubjectConfigCommon", a).then(function (res) {
                    $scope.AllGradesScaleforCat = res.data.table;
                });


                var check = document.getElementById(subject_code);
                console.log(check);

                for (var i = 0; i < $scope.allconfigData.length; i++) {
                    for (var j = 0; j < $scope.allconfigData[i].sub_assign.length; j++) {
                        //$scope.subject_det = $scope.subject_term[i].catDetChild[j].assignDetchild[k].actualDet[l];
                        //$scope.subject_det1 = l + $scope.subject_det.sims_subject_code;
                        if (subject_code == $scope.allconfigData[i].sub_assign[j].sims_subject_code) {
                            $scope.allconfigData[i].sub_assign[j]['assignmentstatus'] = true;
                            //  $scope.subjectchk = check.checked;
                            //  $scope.subjectchk = true;
                            // $scope.subject_det['assignmentstatus'] = check.unchecked;
                            $scope.allconfigData[i].sub_assign[j]['narr_grade_group_code'] = $scope.sims_mark_grade;
                            $scope.allconfigData[i].sub_assign[j]['narr_grade_name'] = $scope.sims_mark_grade_nm;
                            //$scope.checkonebyonedelete1($scope.subject_det);
                            $scope.allconfigData[i].sub_assign[j]['checkeddelstatus'] = true;
                            $scope.allconfigData[i].sub_assign[j].ischange = true;
                        }
                    }
                }

            }


            $scope.getGradeScalePointForCategoryCheck = function (obj1, index, subject_code,checkvalue) {
                debugger
                if (checkvalue == true) {
                    $scope.sims_mark_grade = obj1.narr_grade_group_code;
                    $scope.sims_mark_grade_nm = obj1.narr_grade_group_name;

                    var a = { opr: 'B', cur_code: $scope.cur_code, aca_year: $scope.aca_year, sims_mark_grade: $scope.sims_mark_grade };
                    $http.post(ENV.apiUrl + "api/GradebookNew/SubjectConfigCommon", a).then(function (res) {
                        $scope.AllGradesScaleforCat = res.data.table;
                    });


                    var check = document.getElementById(subject_code);
                    console.log(check);

                    for (var i = 0; i < $scope.allconfigData.length; i++) {
                        for (var j = 0; j < $scope.allconfigData[i].sub_assign.length; j++) {
                                    //$scope.subject_det = $scope.subject_term[i].catDetChild[j].assignDetchild[k].actualDet[l];
                                    //$scope.subject_det1 = l + $scope.subject_det.sims_subject_code;
                                    if (subject_code == $scope.allconfigData[i].sub_assign[j].sims_subject_code) {
                                        $scope.allconfigData[i].sub_assign[j]['assignmentstatus'] = true;
                                        //  $scope.subjectchk = check.checked;
                                        //  $scope.subjectchk = true;
                                        // $scope.subject_det['assignmentstatus'] = check.unchecked;
                                        $scope.allconfigData[i].sub_assign[j]['narr_grade_group_code'] = $scope.sims_mark_grade;
                                        $scope.allconfigData[i].sub_assign[j]['narr_grade_name'] = $scope.sims_mark_grade_nm;
                                        //$scope.checkonebyonedelete1($scope.subject_det);
                                        $scope.allconfigData[i].sub_assign[j]['checkeddelstatus'] = true;
                                        $scope.allconfigData[i].sub_assign[j].ischange = true;
                                    }
                        }
                    }
                } else {
                    for (var i = 0; i < $scope.allconfigData.length; i++) {
                        for (var j = 0; j < $scope.allconfigData[i].sub_assign.length; j++) {
                            //$scope.subject_det = $scope.subject_term[i].catDetChild[j].assignDetchild[k].actualDet[l];
                            //$scope.subject_det1 = l + $scope.subject_det.sims_subject_code;
                            if (subject_code == $scope.allconfigData[i].sub_assign[j].sims_subject_code) {
                                $scope.allconfigData[i].sub_assign[j]['assignmentstatus'] = false;
                                //  $scope.subjectchk = check.checked;
                                //  $scope.subjectchk = true;
                                // $scope.subject_det['assignmentstatus'] = check.unchecked;
                                $scope.allconfigData[i].sub_assign[j]['narr_grade_group_code'] = $scope.sims_mark_grade;
                                $scope.allconfigData[i].sub_assign[j]['narr_grade_name'] = $scope.sims_mark_grade_nm;
                                //$scope.checkonebyonedelete1($scope.subject_det);
                                $scope.allconfigData[i].sub_assign[j]['checkeddelstatus'] = false;
                                $scope.allconfigData[i].sub_assign[j].ischange = true;
                            }
                        }
                    }

                    checkvalue = false;
                }
            }

            $scope.checkAllSubject = function (str) {
                if (str == true) {
                    for (var i = 0; i < $scope.allconfigData.length; i++) {
                        for (var j = 0; j < $scope.allconfigData[i].sub_assign.length; j++) {
                            //$scope.subject_det = $scope.subject_term[i].catDetChild[j].assignDetchild[k].actualDet[l];
                            //$scope.subject_det1 = l + $scope.subject_det.sims_subject_code;
                                $scope.allconfigData[i].sub_assign[j]['assignmentstatus'] = true;
                                //  $scope.subjectchk = check.checked;
                                //  $scope.subjectchk = true;
                                // $scope.subject_det['assignmentstatus'] = check.unchecked;
                                $scope.allconfigData[i].sub_assign[j]['narr_grade_group_code'] = $scope.sims_mark_grade;
                                $scope.allconfigData[i].sub_assign[j]['narr_grade_name'] = $scope.sims_mark_grade_nm;
                                //$scope.checkonebyonedelete1($scope.subject_det);
                                $scope.allconfigData[i].sub_assign[j]['checkeddelstatus'] = true;
                                $scope.allconfigData[i].sub_assign[j].ischange = true;
                        }
                    }

                    for (var i = 0; i < $scope.allconfigData.length; i++) {
                        for (var j = 0; j < $scope.allconfigData[i].sub_assign.length; j++) {
                            $scope.allconfigData[i].sub_assign[j].sims_subject_code_check = true;
                        }
                    }
                    console.log($scope.subject_data);
                } else {

                    // -----------------------------------------
                    for (var i = 0; i < $scope.allconfigData.length; i++) {
                        for (var j = 0; j < $scope.allconfigData[i].sub_assign.length; j++) {
                            //$scope.subject_det = $scope.subject_term[i].catDetChild[j].assignDetchild[k].actualDet[l];
                            //$scope.subject_det1 = l + $scope.subject_det.sims_subject_code;
                            $scope.allconfigData[i].sub_assign[j]['assignmentstatus'] = false;
                            //  $scope.subjectchk = check.checked;
                            //  $scope.subjectchk = true;
                            // $scope.subject_det['assignmentstatus'] = check.unchecked;
                            $scope.allconfigData[i].sub_assign[j]['narr_grade_group_code'] = $scope.sims_mark_grade;
                            $scope.allconfigData[i].sub_assign[j]['narr_grade_name'] = $scope.sims_mark_grade_nm;
                            //$scope.checkonebyonedelete1($scope.subject_det);
                            $scope.allconfigData[i].sub_assign[j]['checkeddelstatus'] = false;
                            $scope.allconfigData[i].sub_assign[j].ischange = true;
                        }
                    }
                    for (var i = 0; i < $scope.allconfigData.length; i++) {
                        for (var j = 0; j < $scope.allconfigData[i].sub_assign.length; j++) {
                            $scope.allconfigData[i].sub_assign[j].sims_subject_code_check = false;
                        }
                    }
                    // -----------------------------------------

                }
            }

            $scope.getGradingScaleData = function () {
                var a = { opr: 'AB', cur_code: $scope.cur_code, aca_year: $scope.aca_year };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {
                    $scope.AllGradeScale = res.data.table;
                    //console.log("My All GradeScale Data", $scope.AllGradeScale);
                });
            }

            $scope.setGradeCode = function (str1) {
                $scope.term_grade_scale = str1;
                $scope.cat_grade_scale = str1;
                $scope.assign_grade_code = str1;
            }

            $scope.subject_term = [], $scope.subject_cat = [], $scope.subject_assign = [];
            var main;
            var subdetails = '', subdetails1 = '';

            $scope.getSubjectConfigData = function () {
                debugger
                $scope.busy = true;
                $http.get(ENV.apiUrl + "api/GradebookNew/getAllSubjectConfigData?cur_code=" + $scope.cur_code + "&acad_year=" + $scope.aca_year + "&config_code=" + $scope.config_code + "&grade_code=" + $scope.grade_code + "&section_code=" + $scope.section_code + "&term_code=" + $scope.sims_report_card_term_code + "&subject=" + $scope.sims_subject_code).then(function (res1) {
                    
                    $scope.subject_term = res1.data.termDet;
                    $scope.subject_cat = res1.data.catDet;
                    $scope.subject_assign = res1.data.assignDet;
                    $scope.subject_data = res1.data.subjectDet;
                    console.log($scope.subject_term);
                    // $scope.div_addgradept1 = true;

                    var a = { opr: 'B', cur_code: $scope.cur_code, aca_year: $scope.aca_year, sims_mark_grade: $scope.sims_mark_grade };
                    $http.post(ENV.apiUrl + "api/GradebookNew/SubjectConfigCommon", a).then(function (res) {
                        $scope.AllGradesScaleforCat = res.data.table;



                        $http.get(ENV.apiUrl + "api/GradebookNew/getAllSubjectConfigDataSave?cur_code=" + $scope.cur_code + "&acad_year=" + $scope.aca_year + "&config_code=" + $scope.config_code + "&grade_code=" + $scope.grade_code + "&section_code=" + $scope.section_code + "&term_code=" + $scope.sims_report_card_term_code + "&subject=" + $scope.sims_subject_code).then(function (res1) {


                            $scope.allconfigData = res1.data;
                            for (var i = 0; i < $scope.allconfigData.length; i++) {
                                for (var j = 0; j < $scope.AllGradesScalePtforSubject.length; j++) {
                                    if ($scope.allconfigData[i].sims_report_card_assign_grade_scale_code == $scope.AllGradesScalePtforSubject[j].narr_grade_group_code) {
                                        $scope.allconfigData[i].sims_report_card_assign_grade_scale_code = $scope.AllGradesScalePtforSubject[j];
                                        $scope.allconfigData[i].sims_subject_code_check = $scope.allconfigData[i].assignmentstatus;
                                    }
                                }
                            }

                        });
                        $scope.busy = false;
                    });
                });
            }

            $scope.checkonebyonedelete = function (str) {
                var check = document.getElementById(str);
                var cnt = 0;
                var assgn_cnt = 0;

                for (var i = 0; i < $scope.subject_term.length; i++) {
                    for (var j = 0; j < $scope.subject_term[i].catDetChild.length; j++) {
                        console.log($scope.subject_term[i].catDetChild[j].assignDetchild.length);
                        cnt = cnt + $scope.subject_term[i].catDetChild[j].assignDetchild.length;

                        for (var k = 0; k < $scope.subject_term[i].catDetChild[j].assignDetchild.length; k++) {


                            for (var l = 0; l < $scope.subject_term[i].catDetChild[j].assignDetchild[k].actualDet.length; l++) {
                                $scope.subject_det = $scope.subject_term[i].catDetChild[j].assignDetchild[k].actualDet[l];

                                $scope.subject_det1 = l + $scope.subject_det.sims_subject_code;
                                if (str == $scope.subject_det1) {
                                    //  $scope.narr_grade_name = $("#cmb_main_grade_pt option:selected").text();
                                    $scope.subject_det['assignmentstatus'] = check.checked;
                                    $scope.subject_term[i].ischange = true;
                                }
                            }
                        }
                    }
                }

                // console.log(assgn_cnt1);
                console.log(cnt);
            }

            $scope.checkonebyonedelete1 = function (assgn_status) {
                debugger
                subdetails = "";

                $scope.sub_details = assgn_status.sims_report_card_term_code + assgn_status.sims_report_card_category_code + assgn_status.sims_report_card_assign_number + assgn_status.sims_subject_code;

                $scope.sub_details_change = assgn_status.sims_report_card_term_code + '#' + assgn_status.sims_report_card_category_code + '#' + assgn_status.sims_report_card_assign_number + '#' + assgn_status.sims_subject_code + '#' + assgn_status.assignmentstatus + '#' + assgn_status.sims_report_card_assign_max_score1;//+ '#' + $scope.sims_mark_grade;

                $scope.assign_status = assgn_status.assignmentstatus;

                var v = document.getElementById('assign_' + $scope.sub_details);

                if (v.checked == true) {
                    subdetails = subdetails + $scope.sub_details_change + ','
                    console.log(subdetails);
                    $scope.row1 = '';
                }
                else {
                    subdetails = subdetails + $scope.sub_details_change + ','
                    v.checked = false;
                    console.log(subdetails);
                }
                assgn_status.ischange = true;

            }

            $scope.getInsertSubjectConfigData = function () {
                debugger
                $scope.busy = true;
                var senddata = [];
                var data = [];
                $scope.table = false;
                var Sdata = [];
                $scope.insert = false;

                $scope.newArr = [];
                var addarr = {};
                for (var i = 0; i < $scope.section_code.length; i++) {
                    addarr = {
                        grade_code: $scope.section_code[i].slice(0, $scope.section_code[i].indexOf("_")),
                        section_code: $scope.section_code[i].slice($scope.section_code[i].indexOf("_"), 100),
                    }
                    $scope.newArr.push(addarr);
                }
                console.log($scope.newArr)

                for (var i = 0; i < $scope.allconfigData.length; i++) {
                    for (var j = 0; j < $scope.allconfigData[i].sub_assign.length; j++) {
                        if ($scope.allconfigData[i].sub_assign[j].ischange == true) {
                            $scope.subject_details = $scope.allconfigData[i].sub_assign[j];

                            if ($scope.allconfigData[i].sub_assign[j].assignmentstatus == true) {
                                $scope.allconfigData[i].sub_assign[j].assignmentstatus = 'A'
                              } else {
                                $scope.allconfigData[i].sub_assign[j].assignmentstatus = 'D'
                                    }

                                    for (var m = 0; m < $scope.section_code.length; m++) {
                                    data =
                                        {
                                            sims_cur_code: $scope.cur_code,
                                            sims_academic_year: $scope.aca_year,
                                            sims_report_card_config_code: $scope.config_code,
                                            sims_grade_code: $scope.section_code[m].slice(0, $scope.section_code[m].indexOf("_")),
                                            sims_section_code: $scope.section_code[m].slice($scope.section_code[m].indexOf("_") + 1, 100),

                                            sims_subject_code1: $scope.allconfigData[i].sub_assign[j].sims_subject_code,
                                            sims_report_card_category_code: $scope.allconfigData[i].sub_assign[j].sims_report_card_category_code,
                                            sims_report_card_assign_number: $scope.allconfigData[i].sub_assign[j].sims_report_card_assign_number,
                                            sims_report_card_term_code: $scope.allconfigData[i].sub_assign[j].sims_report_card_term_code,
                                            sims_report_card_assign_max_score1: $scope.allconfigData[i].sub_assign[j].sims_report_card_assign_max_score1,
                                            assignmentstatus: $scope.allconfigData[i].sub_assign[j].assignmentstatus,
                                            narr_grade_group_code: $scope.allconfigData[i].sub_assign[j].narr_grade_group_code,
                                        };
                                    

                                    $scope.sub_details_change = $scope.subject_details.sims_report_card_term_code + '#' + $scope.subject_details.sims_report_card_category_code + '#' + $scope.subject_details.sims_report_card_assign_number + '#' + $scope.subject_details.sims_subject_code + '#' + $scope.assign_status + '#' + $scope.subject_details.sims_report_card_assign_max_score1 + '#' + $scope.subject_details.narr_grade_group_code;//+ '#' + $scope.sims_mark_grade;

                                    subdetails1 = subdetails1 + $scope.sub_details_change + ','

                                 //   data.sims_subject_code = subdetails1;
                                    $scope.insert = true;
                                    senddata.push(data);
                                    }

                                    if ($scope.assign_status == true) {
                                        $scope.assign_status = 'A';
                                    }
                                    else if ($scope.assign_status == false) {
                                        $scope.assign_status = 'I';
                                    }


                                }
                    }
                }

                //senddata.push(data);
                $scope.busy = true;
                console.log(subdetails1);
                console.log(senddata);

                if (subdetails1 == "") {
                    swal({
                        text: 'Please select Atleast one Record', imageUrl: "assets/img/notification-alert.png",
                        showCloseButton: true, width: 380, height: 300
                    });
                    $scope.getSubjectConfigData();
                    return;
                }
                else {
                    $http.post(ENV.apiUrl + "api/GradebookNew/CUD_Insertsubjectmapping", senddata).then(function (InsertsubjectAssign) {
                        $scope.sub_data = InsertsubjectAssign.data;
                        console.log($scope.sub_data);
                        $scope.table = true;
                        if ($scope.sub_data == true) {
                             swal({ text: 'Subject Mapped Succefully.', showCloseButton: true, width: 380, height: 300 });
                             $scope.getSubjectConfigData();
                             $scope.busy = false;
                             $scope.maincheckbox = false;
                        }
                        else {
                             swal({ text: 'Subject Not Mapped', showCloseButton: true, width: 380, height: 300 });
                             $scope.getSubjectConfigData();
                             $scope.busy = false;
                        }

                    });
                }


                Sdata = [];
                data = [];
                subdetails1 = "";
            }

            $scope.addgradepoint = function (info) {
                $('#gradepointModal').modal({ backdrop: 'static', keyboard: true });
                info.checkeddelstatus = false;
                info.ischecked = true;
                $scope.temp =
                   {
                       narr_grade_group_code1: info.narr_grade_group_code
                   }
                $scope.checkonebyonedelete1(info);

            }

            $scope.gradepointdetails = function () {
                for (var i = 0; i < $scope.subject_term.length; i++) {
                    for (var j = 0; j < $scope.subject_term[i].catDetChild.length; j++) {
                        for (var k = 0; k < $scope.subject_term[i].catDetChild[j].assignDetchild.length; k++) {
                            for (var l = 0; l < $scope.subject_term[i].catDetChild[j].assignDetchild[k].actualDet.length; l++) {
                                $scope.subject_details = $scope.subject_term[i].catDetChild[j].assignDetchild[k].actualDet[l];

                                if ($scope.subject_details.ischecked == true) {
                                    $scope.subject_details.narr_grade_group_code = $scope.temp.narr_grade_group_code1;
                                    console.log($scope.subject_details.narr_grade_group_code);
                                    $scope.narr_grade_name = $("#cmb_grade_point option:selected").text();
                                    $scope.subject_details.narr_grade_name = $scope.narr_grade_name;
                                    // console.log($scope.narr_grade_name);
                                    $scope.subject_details.ischecked = false;
                                    $scope.div_addgradept1 = true;
                                }

                            }
                        }
                    }
                }
            }

            $scope.closeGradeOption = function (assgn_status) {
                assgn_status.checked = true;
                $scope.sub_details = assgn_status.sims_report_card_term_code + assgn_status.sims_report_card_category_code + assgn_status.sims_report_card_assign_number + assgn_status.sims_subject_code;
                //assgn_status.narr_grade_name = '';
                //  assgn_status['assignmentstatus'] = false;

                if (assgn_status.checkeddelstatus == true) {
                    var obj_data =
                        {
                            sims_cur_code: $scope.cur_code,
                            sims_academic_year: $scope.aca_year,
                            sims_report_card_config_code: $scope.config_code,
                            sims_grade_code: $scope.grade_code,
                            sims_section_code: $scope.section_code,
                            sims_report_card_term_code: assgn_status.sims_report_card_term_code,
                            sims_report_card_category_code: assgn_status.sims_report_card_category_code,
                            sims_report_card_assign_number: assgn_status.sims_report_card_assign_number,
                            sims_subject_code: assgn_status.sims_subject_code,
                            narr_grade_group_code: assgn_status.narr_grade_group_code
                        }

                    console.log(obj_data);

                    $http.post(ENV.apiUrl + "api/GradebookNew/CUD_delsubjectmapping", obj_data).then(function (DelsubjectAssign) {
                        $scope.sub_data = DelsubjectAssign.data;
                    });
                    $scope.getSubjectConfigData();
                }
                else {
                    assgn_status.narr_grade_name = '';
                    assgn_status['assignmentstatus'] = false;
                    // $scope.narr_grade_group_code = '';
                }
            }

            $scope.addAnotherGradept = function (info1) {

                $scope.addgradepoint(info1);

            }


            $scope.sims_report_card_assign_number_insert ='';
            $scope.sims_report_card_config_code_insert ='';
            $scope.sims_report_card_category_code_insert = '';

            $scope.InsertSubjectAttribute = function (obj) {
                debugger
                if (obj.all_saved_config != '') {
                    $scope.all_saved_config = obj.all_saved_config;
                } else { $scope.all_saved_config = ''; }
                var gradecode = '';
                for (var i = 0; i < $scope.sims_grade_code.length; i++) {
                    gradecode = gradecode + $scope.sims_grade_code[i] + ',';
                }

                var sectioncode = '';
                for (var i = 0; i < $scope.sims_section_code.length; i++) {
                    sectioncode = sectioncode + $scope.sims_section_code[i] + ',';
                }

                var subjectcode = '';
                for (var i = 0; i < $scope.sims_subject_code.length; i++) {
                    subjectcode = subjectcode + $scope.sims_subject_code[i] + ',';
                }

                $scope.sims_report_card_assign_number_insert = obj.sims_report_card_assign_number;
                $scope.sims_report_card_config_code_insert = obj.sims_report_card_config_code;
                $scope.sims_report_card_category_code_insert = obj.sims_report_card_category_code;
                $scope.sims_report_card_term_code_insert = obj.sims_report_card_term_code;

                var subattr = {
                    opr: 'AN', cur_code: obj.sims_cur_code, aca_year: obj.sims_academic_year, grade_code: gradecode, section_code: sectioncode,
                    subject_code: obj.sims_subject_code, sims_report_card_config_code: obj.sims_report_card_config_code, term_code: obj.sims_report_card_term_code
                };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", subattr).then(function (res) {
                    $scope.subjectAtrribute = res.data.table;


                    $('#subjectAtribute').modal({ backdrop: 'static', keyboard: true });
                });
            }

            $scope.prepareforsave = function (str) {
                debugger
                if (str.sims_cat_assign_subject_attribute_max_score != '' || str.sims_cat_assign_subject_attribute_max_score != null) { 
                    str.sims_insert = true;
                }
            }

            $scope.savesubjectAttributes = function () {
                debugger
                var subattr = { opr: 'AZ', subject_config: $scope.all_saved_config };
                $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", subattr).then(function (res) {
                    $scope.subjectConfigData = res.data.table;

                $scope.savedata = [];
                for (var i = 0; i < $scope.subjectAtrribute.length; i++) {
                    if ($scope.subjectAtrribute[i].sims_insert == true) {
                       // $scope.subjectAtrribute[i].sims_cat_assign_subject_attribute_max_score=
                        $scope.savedata.push($scope.subjectAtrribute[i]);
                    }
                }
                for (var i = 0; i < $scope.savedata.length; i++) {
                    for (var j = 0; j < $scope.subjectConfigData.length; j++) {
                        if ($scope.savedata[i].sims_cur_code == $scope.subjectConfigData[j].sims_cur_code && $scope.savedata[i].sims_academic_year == $scope.subjectConfigData[j].sims_academic_year) {
                            $scope.savedata[i].sims_subject_co_scholastic_srl_no = $scope.subjectConfigData[j].sims_subject_co_scholastic_srl_no;
                            $scope.savedata[i].sims_grade_code = $scope.subjectConfigData[j].sims_grade_code;
                            $scope.savedata[i].sims_section_code = $scope.subjectConfigData[j].sims_section_code;
                        }
                    }
                }
                

                console.log($scope.savedata);

                $scope.data = [];
                for (var i = 0; i < $scope.savedata.length; i++) {
                    var datasend = {
                        sims_cur_code:$scope.savedata[i].sims_cur_code,
                        sims_academic_year:$scope.savedata[i].sims_academic_year,
                        sims_grade_code:$scope.savedata[i].sims_grade_code,
                        sims_section_code:$scope.savedata[i].sims_section_code,
                        sims_gb_number: $scope.savedata[i].sims_subject_co_scholastic_srl_no,
                        sims_gb_cat_code:$scope.sims_report_card_category_code_insert,
                        sims_gb_cat_assign_number:$scope.sims_report_card_assign_number_insert,
                        sims_subject_attribute_group_code:$scope.savedata[i].sims_attribute_group_code,
                        sims_subject_attribute_code:$scope.savedata[i].sims_attribute_code,
                        sims_cat_assign_subject_attribute_max_score:$scope.savedata[i].sims_cat_assign_subject_attribute_max_score,
                        sims_gb_cat_assign_subject_attribute_max_score_correct:$scope.savedata[i].sims_cat_assign_subject_attribute_max_score,
                        sims_gb_cat_assign_subject_attribute_grade_completed_status: '',
                        sims_report_card_config_code: $scope.sims_report_card_config_code_insert,
                        sims_report_card_term_code: $scope.sims_report_card_term_code_insert
                    }

                    $scope.data.push(datasend);
                }
                console.log($scope.data);

                    $http.post(ENV.apiUrl + "api/gradebooknew/insertsubjectattribute", $scope.data).then(function (msg) {
                        debugger
                        if (msg.data == true) {
                            $(document).ready(function () {
                            });
                            swal({ title: "Alert", text: "Marks Updated successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            $('#subjectAtribute').modal('hide');
                        }
                        else {
                            swal({ title: "Alert", text: "Unable To Update Marks. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }

                     //   $scope.closeAddNew();
                    });
                });
            }

            $scope.getAcademic_year = function (cur_code1) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code1).then(function (res1) {
                    $scope.acad_data = res1.data;
                    $scope.att.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                    $scope.getGradeAttr($scope.att.sims_cur_code, $scope.att.sims_academic_year);
                    //  $scope.getSection($scope.temp.sims_academic_year, $scope.temp.grade_code, $scope.temp.sims_cur_code);
                });
            }

            $scope.getGradeAttr = function (cur_code, academic_year) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.att.sims_cur_code + "&academic_year=" + $scope.att.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%',
                            filter: true
                        });
                        $("#cmb_grade_code").multipleSelect("checkAll");
                    }, 1000);



                });
                $scope.getSection($scope.att.sims_cur_code, $scope.att.sims_academic_year, $scope.att.grade_code);
            }

            $scope.getSection = function (cur_code, academic_year, grade_code) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/Subject/getSectionFromGradeForAttr?cur_code=" + $scope.att.sims_cur_code + "&grade_code=" + $scope.att.sims_grade_code + "&academic_year=" + $scope.att.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%',
                            filter: true
                        });
                        $("#cmb_section_code").multipleSelect("checkAll");
                    }, 1000);
                });
            }

            $scope.InsertAttribute = function (obj) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res1) {
                    debugger
                    $scope.cur_data = res1.data;
                    $scope.att.sims_cur_code = $scope.cur_data[0].sims_cur_code;
                    $scope.getAcademic_year($scope.cur_data[0].sims_cur_code);

                    var subd = {
                        opr: 'BA', subject_code: obj.sims_subject_code };
                        $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", subd).then(function (res) {
                            $scope.SubjectDetails = res.data.table;

                            $scope.sims_attribute_group_code = $scope.SubjectDetails[0].sims_subject_code;
                            $scope.sims_attribute_group_name_en = $scope.SubjectDetails[0].sims_subject_name_en;
                            $scope.sims_attribute_group_name_ot = $scope.SubjectDetails[0].sims_subject_name_ot;
                            $scope.sims_attribute_group_type = $scope.SubjectDetails[0].sims_subject_type;
                            $scope.sims_attribute_group_status = $scope.SubjectDetails[0].sims_subject_status;

                //$scope.attributedisplay = true;
                //$scope.showData = false;
                $scope.multi_array = [{}];
                            //$scope.showtable();
                $http.get(ENV.apiUrl + "api/common/Subject/getAttributeConfig?cur_code=" + $scope.cur_code + "&academic_year=" + $scope.aca_year + "&grade_code=" + $scope.att.sims_grade_code + "&section_code=" + $scope.att.sims_section_code + "&subject=" + $scope.sims_attribute_group_code).then(function (res) {
                    $scope.attribute_data = res.data;
                    $scope.totalItems = $scope.attribute_data.length;
                    //$scope.attributedisplay = true;

                $scope.all_saved_config_insert = obj.all_saved_config
                $scope.sims_report_card_assign_number_attr = obj.sims_report_card_assign_number
                $scope.sims_report_card_assign_type_attr = obj.sims_report_card_assign_type
                $scope.sims_report_card_category_code_attr = obj.sims_report_card_category_code
                $scope.sims_report_card_config_code_attr = obj.sims_report_card_config_code
                $scope.sims_report_card_term_code_attr = obj.sims_report_card_term_code
                
                $scope.att.sims_attribute_status = true;
                $('#attributedisplay').modal({ backdrop: 'static', keyboard: true });
                    });
                  });
                });
            }

            $scope.showtable = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/common/Subject/getAttributeConfig?cur_code=" + $scope.cur_code + "&academic_year=" + $scope.aca_year + "&grade_code=" + $scope.att.sims_grade_code + "&section_code=" + $scope.att.sims_section_code + "&subject=" + $scope.sims_attribute_group_code).then(function (res) {
                    $scope.attribute_data = res.data;
                    $scope.totalItems = $scope.attribute_data.length;
                    $scope.attributedisplay = true;

                });
                main = document.getElementById('mainchkatt');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myFormN.$setPristine();
                $scope.myFormN.$setUntouched();
            }

            $scope.Add = function () {
                $scope.multi_array.push({});
                for (var i = 0; i < $scope.multi_array.length; i++) {
                    $scope.multi_array[i].sims_attribute_status = true;
                }
            }

            $scope.cancel = function () {
                $scope.attributedisplay = false;
                $scope.showData = true;
            }

            $scope.changeAttrstatus = function (str) {
                if (str.sims_attribute_code_check == true) {
                    str.ischange = true;
                } else {
                    str.ischange = false;
                }
            }

            $scope.SaveAttribute = function (MyformN) {
                debugger
                var datasend = [];
             //   if (MyformN) {

                    var aa = {
                        opr: 'AA',
                        sims_gb_number: $scope.all_saved_config_insert,
                    };
                    $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", aa).then(function (res) {
                        if (res.data.table.length > 0) {
                            $scope.Allconfig_grade = res.data.table;
                        }
                    
                    var sec = ''
                    
                    if ($scope.att.sims_attribute_code == undefined || $scope.att.sims_attribute_code == '') {
                        $scope.att.sims_attribute_code = null;
                    }
                    var agcollm = [];
                    for (var i = 0; i < $scope.Allconfig_grade.length; i++) {
                        for (var j = 0; j < $scope.attribute_data.length; j++) {
                            if ($scope.attribute_data[j].ischange == true) { 
                            var dm = {
                                opr: '5',
                                sims_academic_year: $scope.aca_year,
                                sims_cur_code: $scope.cur_code,
                                // sims_grade_code:'01',//$scope.att.sims_grade_code,
                                sims_section_code: sec,//$scope.att.sims_section_code,
                                sims_attribute_group_code: $scope.attribute_data[j].sims_attribute_group_code,
                                sims_attribute_code: $scope.attribute_data[j].sims_attribute_code,
                                sims_attribute_grade: '',
                                sims_attribute_name: $scope.attribute_data[j].sims_attribute_name,
                                sims_attribute_name_ot: $scope.attribute_data[j].sims_attribute_name_ot,
                                sims_attribute_display_order: $scope.attribute_data[j].sims_display_order,
                                sims_attribute_status: $scope.attribute_data[j].sims_attribute_status,
                                sims_attribute_group_name_en: $scope.sims_attribute_group_name_en,
                                sims_attribute_group_name_ot: $scope.sims_attribute_group_name_ot,
                                sims_attribute_group_type: $scope.sims_attribute_group_type,
                                sims_attribute_group_status: $scope.sims_attribute_group_status,
                                sims_attribute_type: 'B',
                                sims_gb_number: $scope.Allconfig_grade[i].sims_subject_co_scholastic_srl_no,
                                sims_gb_cat_assign_number: $scope.sims_report_card_assign_number_attr,
                                sims_gb_cat_code: $scope.sims_report_card_category_code_attr,
                                sims_report_card_config_code: $scope.sims_report_card_config_code_attr,
                                sims_report_card_term_code: $scope.sims_report_card_term_code_attr,
                                sims_cat_assign_subject_attribute_max_score: $scope.attribute_data[j].sims_cat_assign_subject_attribute_max_score
                            }
                            agcollm.push(dm);

                            }
                        }
                    }
                    $http.post(ENV.apiUrl + "api/common/Subject/CUDAttributeConfig", agcollm).then(function (msg) {
                        $scope.msg2 = msg.data.insert;
                        if ($scope.msg2 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.att.sims_attribute_name = '';
                            $scope.att.sims_attribute_name_ot = '';
                            $scope.att.sims_attribute_display_order = '';
                            $scope.showtable();
                            $scope.reset_multi();

                        }
                        else {
                            swal({ text: "Record Not Inserted " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.showtable();
                        }


                    });
                    });
                    $scope.table = true;
                    $scope.display = false;

              //  }

            }

            $scope.SaveMasterAttribute = function (MyformN) {
                debugger
                var datasend = [];
                //   if (MyformN) {

                var aa = {
                    opr: 'AA',
                    sims_gb_number: $scope.all_saved_config_insert,
                };
                //$http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", aa).then(function (res) {
                //    if (res.data.table.length > 0) {
                //        $scope.Allconfig_grade = res.data.table;
                //    }

                var sec = ''

                if ($scope.att.sims_attribute_code == undefined || $scope.att.sims_attribute_code == '') {
                    $scope.att.sims_attribute_code = null;
                }

                var agcoll = [];
                var d = {
                    opr: '1',
                    sims_attribute_code: $scope.att.sims_attribute_code,
                    sims_academic_year: $scope.aca_year,
                    sims_cur_code: $scope.cur_code,
                    // sims_grade_code:'01',//$scope.att.sims_grade_code,
                    sims_section_code: sec,//$scope.att.sims_section_code,
                    sims_attribute_group_code: '',
                    sims_attribute_grade: '',
                    sims_attribute_name: $scope.att.sims_attribute_name,
                    sims_attribute_name_ot: $scope.att.sims_attribute_name_ot,
                    sims_attribute_display_order: $scope.att.sims_attribute_display_order,
                    sims_attribute_status: $scope.att.sims_attribute_status,
                    sims_attribute_max_score: $scope.att.sims_cat_assign_subject_attribute_max_score,

                    sims_attribute_group_name_en: $scope.sims_attribute_group_name_en,
                    sims_attribute_group_name_ot: $scope.sims_attribute_group_name_ot,
                    sims_attribute_group_type: $scope.sims_attribute_group_type,
                    sims_attribute_group_status: $scope.sims_attribute_group_status,
                    sims_attribute_type: 'B',
                    sims_gb_number: $scope.all_saved_config_insert,
                    sims_gb_cat_assign_number: $scope.sims_report_card_assign_number_attr,
                    sims_gb_cat_code: $scope.sims_report_card_category_code_attr,
                    sims_report_card_config_code: $scope.sims_report_card_config_code_attr,
                    sims_report_card_term_code: $scope.sims_report_card_term_code_attr,
                    sims_cat_assign_subject_attribute_max_score: $scope.att.sims_cat_assign_subject_attribute_max_score
                }

                agcoll.push(d);
                $http.post(ENV.apiUrl + "api/common/Subject/CUDAttributeConfig", agcoll).then(function (msg) {
                    $scope.msg1 = msg.data.insert;

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        $scope.getattributemaster();
                        $scope.att.sims_attribute_name = '';
                        $scope.att.sims_attribute_name_ot = '';
                        $scope.att.sims_attribute_display_order = '';
                        $scope.showtable();
                        $scope.reset_multi();

                    }
                    else {
                        swal({ text: "Record Not Inserted " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        $scope.showtable();
                    }
                });
                //});
                $scope.table = true;
                $scope.display = false;

                //  }

            }

            $scope.btn_GradeScaleInsert_click = function () {
                debugger;
                var GradeScaleDataSend = [];
                for (var i = 0; i < $scope.All_SCALE_GRADE.length; i++) {
                    var t = document.getElementById($scope.All_SCALE_GRADE[i].gradeGroupCode);
                    if (t.checked == true) {

                        var data = {
                            opr: 'IM',
                            sims_mark_grade_name: $scope.gradescale.sims_mark_grade_name,
                            GradeGroupCode: $scope.All_SCALE_GRADE[i].gradeGroupCode,
                            sims_mark_grade_low: $scope.gradescale.sims_mark_grade_low,
                            sims_mark_grade_high: $scope.gradescale.sims_mark_grade_high,
                            sims_mark_grade_description: $scope.gradescale.sims_mark_grade_description,
                            sims_mark_grade_point: $scope.gradescale.sims_mark_grade_point,
                        }

                        GradeScaleDataSend.push(data);
                    }

                }
                if (GradeScaleDataSend.length != 0) {
                    $http.post(ENV.apiUrl + "api/Gradebook/CUDGradeScaleDataOpr", GradeScaleDataSend).then(function (msg) {
                        $scope.MSG = msg.data;
                        $scope.gradescale = "";
                        GradeScaleDataSend = [];

                        

                        $scope.ShowGradeWiseGradescale($scope.gcode);

                        swal({ text: $scope.MSG, width: 300, showCloseButton: true });


                    });

                }
                else {

                    swal({ text: 'Select Atleast One Grade To Insert Grade Scale', imageUrl: "assets/img/notification-alert.png", width: 300, showCloseButton: true });
                }
            }

            $scope.btn_GradeScaleDelete_click = function () {
                debugger;
                var GradeScaleDataSend = [];

                for (var i = 0; i < $scope.Grade_Scale_Data.length; i++) {

                    var t = document.getElementById($scope.Grade_Scale_Data[i].sims_mark_grade_code);
                    if (t.checked == true) {
                        $scope.Grade_Scale_Data[i].opr = 'D',
                        GradeScaleDataSend.push($scope.Grade_Scale_Data[i]);
                    }

                }


                if (GradeScaleDataSend.length != 0) {
                    $http.post(ENV.apiUrl + "api/Gradebook/CUDGradeScaleDataOpr", GradeScaleDataSend).then(function (msg) {
                        $scope.MSG = msg.data;
                        $scope.gradescale = "";
                        GradeScaleDataSend = [];

                        $scope.ShowGradeWiseGradescale($scope.gcode);
                        swal({ text: $scope.MSG, width: 300, showCloseButton: true });

                    })

                }
                else {

                    swal({ text: 'Select Atleast One Grade Scale To Deleted', imageUrl: "assets/img/notification-alert.png", width: 300, showCloseButton: true });
                }

            }



            $scope.GradebookGradingSchemeShow = function (color) {

                var Grade_Scaledata = [];


                $http.get(ENV.apiUrl + "api/Gradebook/getGradeScaleData").then(function (GradeScaleData) {
                    $scope.Grade_Scale_Data = GradeScaleData.data;
                    for (var i = 0; i < $scope.Grade_Scale_Data.length; i++) {
                        if ($scope.Grade_Scale_Data[i].gradeGroupCode == info.gradeGroupCode) {
                            Grade_Scaledata.push($scope.Grade_Scale_Data[i]);
                        }
                    }
                    $scope.Grade_Scale_Data = [];
                    $scope.Grade_Scale_Data = Grade_Scaledata;
                })
            }

            $scope.ShowGradeWiseGradescale = function (info) {
                debugger;
                var Grade_Scaledata = [];
                $scope.gradedesc = '';
                $scope.gcode = info;

                $http.get(ENV.apiUrl + "api/Gradebook/getGradeScaleData").then(function (GradeScaleData) {
                    $scope.Grade_Scale_Data = GradeScaleData.data;
                    for (var i = 0; i < $scope.Grade_Scale_Data.length; i++) {
                        if ($scope.Grade_Scale_Data[i].gradeGroupCode == info) {
                            Grade_Scaledata.push($scope.Grade_Scale_Data[i]);
                            $scope.gradedesc = $scope.Grade_Scale_Data[i].gradeGroupDesc;
                        }
                    }
                    $scope.Grade_Scale_Data = [];
                    $scope.Grade_Scale_Data = Grade_Scaledata;

                })

                for (var j = 0; j < $scope.All_SCALE_GRADE.length; j++) {
                    if ($scope.All_SCALE_GRADE[j].gradeGroupCode == info) {
                        $scope.gradedesc = $scope.All_SCALE_GRADE[j].gradeGroupDesc;
                        break;
                    }
                }

            }

           
            var datasend = [];
            $scope.savegradedata = function () {
                debugger;
                     
                $http.post(ENV.apiUrl + "api/Gradebook/CUDgradeScaleData?opr=" + "IG" + "&gradeName=" + $scope.gradgroup.gradeName + "&description=" + $scope.gradgroup.description + "&status=" + $scope.gradgroup.status).then(function (msg) {
                        
                        $scope.msg1 = msg.data;
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });

                            $http.post(ENV.apiUrl + "api/Gradebook/GradeScaleDataOpr?teachercode=" + $scope.user + "&opr=SC").then(function (All_Grade) {
                                debugger;
                                $scope.All_SCALE_GRADE = All_Grade.data;


                            });
                            $scope.gradgroup.gradeName = '';
                            $scope.gradgroup.description='';
                            $scope.gradgroup.status = true;
                            $scope.closeAddNewMaster();
                        }
                        else {
                            swal({ text: "Record already present. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }

                        

                    });
                    datasend = [];
                    
              

            }

            $scope.deletegrade = function (e) {
                debugger
                if ($("#grade_div2_master").find('input:checked').length > 0) {
                    $("#grade_div2_master").find('input:checked').each(function (k, v) {
                        var grdgrp_number = $(v).parents('.div_grade_item').data('gradegroupcode');

                        var a = { opr: 'AW', cur_code: $scope.cur_code, aca_year: $scope.aca_year, grdgrp_number: grdgrp_number };
                        $http.post(ENV.apiUrl + "api/GradebookNew/GradeBookNewCommon", a).then(function (res) {

                            if (res.data.table[0].inserted_flg > 0) {
                                $(document).ready(function () {
                                });
                                swal({ title: "Alert", text: "Assignment Deleted successfully.", imageUrl: "assets/img/check.png", width: 300, height: 200 });


                                $http.post(ENV.apiUrl + "api/Gradebook/GradeScaleDataOpr?teachercode=" + $scope.user + "&opr=SC").then(function (All_Grade) {
                            debugger;
                            $scope.All_SCALE_GRADE = All_Grade.data;


                        });

                            }
                            else {
                                swal({ title: "Alert", text: "Unable To Delete Assignment. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }

                            $scope.getAssignments();
                            $scope.getMasterAssignments();
                        });
                    })
                }
                else {
                    alert("Select atleast one Assignment");
                }
            }


               var datasend = [];
               $scope.updategradedata = function () {
                debugger;
                     
                $http.post(ENV.apiUrl + "api/Gradebook/CUDgradeScaleDataupdate?opr=" + "UG" + "&groupcode=" + $scope.gradgroup.Gradecode + "&gradeName=" + $scope.gradgroup.gradeName + "&description=" + $scope.gradgroup.description + "&status=" + $scope.gradgroup.status).then(function (msg) {
                        
                        $scope.msg1 = msg.data;
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record update Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });

                            $http.post(ENV.apiUrl + "api/Gradebook/GradeScaleDataOpr?teachercode=" + $scope.user + "&opr=SC").then(function (All_Grade) {
                                debugger;
                                $scope.All_SCALE_GRADE = All_Grade.data;


                            });
                            $scope.gradgroup.gradeName = '';
                            $scope.gradgroup.description='';
                            $scope.gradgroup.status = true;
                            $scope.closeAddNewMaster();
                        }
                        else {
                            swal({ text: "Record already present. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }

                        

                    });
                    datasend = [];
                    
              

            }
               $scope.RowUpdateGradeScale = function (info) {
                   $scope.gradescalebtn = false;
                   $scope.gradescale = info;
               }
               $scope.btn_GradeScaleUpdate_click = function () {

                   var GradeScaleDataSend = [];
                   $scope.gradescalebtn = true;
                   var data = {
                       opr: 'UM',
                       sims_mark_grade_name: $scope.gradescale.sims_mark_grade_name,
                       sims_mark_grade_code: $scope.gradescale.sims_mark_grade_code,
                       sims_mark_grade_low: $scope.gradescale.sims_mark_grade_low,
                       sims_mark_grade_high: $scope.gradescale.sims_mark_grade_high,
                       sims_mark_grade_description: $scope.gradescale.sims_mark_grade_description,
                       sims_mark_grade_point: $scope.gradescale.sims_mark_grade_point.length == 0 ? 0 : $scope.gradescale.sims_mark_grade_point,
                       GradeGroupCode: $scope.gradescale.gradeGroupCode
                   }

                   GradeScaleDataSend.push(data);

                   if (GradeScaleDataSend.length != 0) {
                       $http.post(ENV.apiUrl + "api/Gradebook/CUDGradeScaleDataOpr", GradeScaleDataSend).then(function (msg) {
                           $scope.MSG = msg.data;
                           $scope.gradescale = "";
                           GradeScaleDataSend = [];


                           swal({ text: $scope.MSG, width: 300, showCloseButton: true });

                       })
                   }
                   else {

                       swal({ text: 'Select Atleast One Grade Scale To Update', imageUrl: "assets/img/notification-alert.png", width: 300, showCloseButton: true });
                   }
               }
               $scope.btn_GradeScaleCancel_click = function () {

                   $scope.gradescalebtn = true;
                   $scope.gradescale = "";
               }

        }]);
})();


