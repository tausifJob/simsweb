﻿(function () {
    'use strict';
    var teacher_code = [];
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('GraceMarksTransactionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var user = $rootScope.globals.currentUser.username;
            var appl_code = "sim070";
            $scope.fcommn = [];
            $scope.pagesize = 'All';
            $scope.pageindex = "0";
            $scope.pager = false;
            $scope.toppager = false;
            $scope.busyindicator = true;
            $scope.showtable = false;
            var sub_head = '';
            var effort_head = '';
            var general_head = '';
            $scope.save_all_data = [];
            $scope.stud_lst = [];
            $scope.busy = false;
            $http.get(ENV.apiUrl + "api/Gradebook/GetadminuserType?username=" + user + "&appl_code=" + appl_code).then(function (getAdmin) {
                $scope.getadmin = getAdmin.data;

            });
            debugger
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                debugger
                $scope.curriculum = res.data;
                $scope.edt = {
                    cur_code: $scope.curriculum[0].sims_cur_code
                }
                $scope.fcommn['cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr1($scope.curriculum[0].sims_cur_code);
            });


            $http.get(ENV.apiUrl + "api/GracingMarksTrans/getsortopt").then(function (res) {
                debugger
                $scope.sortData = res.data;
                $scope.fcommn.sort_by = $scope.sortData[0].sims_appl_parameter;
            });

            $http.get(ENV.apiUrl + "api/GracingMarksTrans/getcategory").then(function (res) {
                debugger
                $scope.categoryData = res.data;
                $scope.fcommn.sims_category_name = $scope.categoryData[0].sims_appl_form_field_value1;
            });

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            setTimeout(function () {
                $("#rcbItem").select2();

            }, 100);

            var username = $rootScope.globals.currentUser.username;
            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['chk_all'] = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                debugger;
                if ($scope.pagesize == 'All') {
                    $scope.currentPage = 1;
                    $scope.showPager = false;
                    $scope.filteredTodos = $scope.st_lst1;
                }
                else {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                }
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.student_list, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.student_list;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.sComment = {};

            $scope.getacyr1 = function (str) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.getAcademicYear1 = Academicyear.data; //academic_year
                    $scope.fcommn['academic_year'] = $scope.getAcademicYear1[0].sims_academic_year;
                    $scope.getsections2($scope.fcommn.cur_code, $scope.getAcademicYear1[0].sims_academic_year);
                });
            }

            $scope.getsection2 = function (str, str1, str2) {
                $http.get(ENV.apiUrl + "api/Gradebook/GetRCSection?curcode=" + str + "&ayear=" + str1 + "&gradecode=" + str2 + "&teachercode=" + user).then(function (Allsection) {
                    $scope.getSectionFromGrade1 = Allsection.data;
                    $scope.fcommn['sims_section_code'] = $scope.getSectionFromGrade1[0].sims_section_code;
                    $scope.Getterm2(str, str1, str2, $scope.fcommn['sims_section_code'])

                })
            };

            $scope.getsections2 = function (str, str1) {

                $http.get(ENV.apiUrl + "api/Gradebook/GetRCGrade?curcode=" + str + "&ayear=" + str1 + "&teachercode=" + user).then(function (res) {
                    $scope.getAllGrades1 = res.data;
                    $scope.fcommn['sims_grade_code'] = $scope.getAllGrades1[0].sims_grade_code;
                    $scope.getsection2(str, str1, $scope.getAllGrades1[0].sims_grade_code);
                })
            };

            $scope.Getterm2 = function (cur_code, oldval,grade,section) {
                debugger
                //$http.get(ENV.apiUrl + "api/Gradebook/getTermforGradebook?cur_code=" + cur_code + "&academic_year=" + oldval).then(function (Getsims550_TermsAcademicYear) {
                //    $scope.TermsAcademicYear1 = Getsims550_TermsAcademicYear.data;
                //    $scope.fcommn['sims_term_code'] = $scope.TermsAcademicYear1[0].sims_term_code;
                //});

                $http.get(ENV.apiUrl + "api/StudentActivity/getTermforGradebook?cur_code=" + cur_code + "&academic_year=" + oldval + "&grade_code=" + grade + "&section_code=" + section).then(function (Getsims550_TermsAcademicYear) {
                    $scope.TermsAcademicYear1 = Getsims550_TermsAcademicYear.data;
                    $scope.fcommn['sims_term_code'] = $scope.TermsAcademicYear1[0].sims_term_code;
                });

              
            }

            $scope.CheckAllChecked = function () {
                debugger;
                $scope.save_all_data = [];
                var main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.fail_student_list.length; i++) {
                        if ($scope.fail_student_list[i].marks_needed_to_pass > 0) {
                            var v = document.getElementById($scope.fail_student_list[i].sims_gb_cat_assign_enroll_number + i);
                            v.checked = true;
                            $('tr').addClass("row_selected");
                            $scope.save_all_data.push($scope.fail_student_list[i]);
                        }
                        else {
                            var v = document.getElementById($scope.fail_student_list[i].sims_gb_cat_assign_enroll_number + i);
                            v.checked = false;
                            $scope.row1 = '';
                            $('tr').removeClass("row_selected");
                        }
                    }
                }
                else {

                    for (var i = 0; i < $scope.fail_student_list.length; i++) {
                        var v = document.getElementById($scope.fail_student_list[i].sims_gb_cat_assign_enroll_number + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                        $scope.save_all_data = [];
                    }
                }

                console.log("$scope.save_data", $scope.save_all_data);
            }


            $scope.checklick = function () {

                debugger
                $scope.save_all_data = [];
                for (var i = 0; i < $scope.fail_student_list.length; i++) {
                    var t = $scope.fail_student_list[i].sims_gb_cat_assign_enroll_number;
                    var v = document.getElementById(t + i);
                    if (v.checked == true) {
                        if ($scope.fail_student_list[i].marks_needed_to_pass > 0) { 
                            $scope.save_all_data.push($scope.fail_student_list[i]);
                        } else {
                            v.checked = false;
                            swal({ title: "Alert", text: "Students applied with activity points will be saved !!!", width: 300, height: 200 });
                        }
                    }
                    
                }

                console.log("$scope.save_data", $scope.save_data);
            }

            $http.get(ENV.apiUrl + "api/GracingMarksTrans/getActivity").then(function (res) {
                $scope.activityData = res.data;
            });

            $scope.sortBy = function (propertyName) {
                debugger
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.show_fail_students = function (cur_code, academic_year, grade, section, term,category) {
                debugger
                $scope.stud_lst = [];
                $http.get(ENV.apiUrl + "api/GracingMarksTrans/GetFailStudent?sims_cur_code=" + cur_code + "&sims_academic_year=" + academic_year + "&sims_grade_code=" + grade + "&sims_section_code=" + section + "&sims_gb_term_code=" + term + "&sort_by=" + $scope.fcommn.sort_by + "&category_name=" + category).then(function (data_all) {
                    $scope.fail_student_list = data_all.data;

                    for (var i = 0; i < $scope.fail_student_list.length; i++) {
                        if ($scope.fail_student_list[i].marks_needed_to_pass>0) {
                        $scope.fail_student_list[i].exclude_sub_marks = true;
                        }else
                        $scope.fail_student_list[i].exclude_sub_marks = false;
                    }

                    $scope.total_student_failed_count = $scope.fail_student_list[0].distinct_sims_gb_cat_assign_enroll_number;
                    $scope.total_subject_failed_count = $scope.fail_student_list[0].distinct_sims_gb_subject_code;
                    $scope.total_student_pass_subject_count = $scope.fail_student_list[0].total_student_pass_subject_count;

                    $scope.total_student_pass_subject = [];
                    for (var i = 0; i < $scope.fail_student_list.length; i++) {
                        if ($scope.fail_student_list[i].marks_needed_to_pass>0) {
                            $scope.fail_student_list[i].marks_needed_to_pass_background_color = '#f7c9da';
                            $scope.total_student_pass_subject.push($scope.fail_student_list[i]);
                        }
                    }

                    $scope.total_student_passed = 0;

                    if ($scope.total_student_pass_subject.length>0) {
                    for (var i = 0; i < $scope.fail_student_list.length; i++) {
                        var flag = 0;
                        for (var j = 0; j < $scope.total_student_pass_subject.length; j++) {
                            if ($scope.fail_student_list[i].sims_gb_cat_assign_enroll_number != $scope.total_student_pass_subject[j].sims_gb_cat_assign_enroll_number) {
                                flag = 1;
                            } else {
                                flag = 0;
                            }
                        }
                        if (flag == 0) {
                            $scope.total_student_passed = $scope.total_student_passed + 1;
                        }
                    }
                    }

                    for (var b = 0; b < $scope.fail_student_list.length; b++) {
                        $scope.fail_student_list[b].marks_applied_other_than_general = 0;
                        $scope.fail_student_list[b].remaining_points = $scope.fail_student_list[b].sims_activity_points;
                        $scope.fail_student_list[b].marks_needed_to_pass_remaining = $scope.fail_student_list[b].marks_needed_to_pass;
                        if ($scope.fail_student_list[b].sims_gracing_type == 'GE') {
                            $scope.fail_student_list[b].points_from_other_than_general_gracing_remaining = 0;
                        } else {
                        $scope.fail_student_list[b].points_from_other_than_general_gracing_remaining = $scope.fail_student_list[b].points_from_other_than_general_gracing;
                        }
                        $scope.fail_student_list[b].formula_value = 0;
                        if ($scope.fail_student_list[b].sims_sports_flag == 'N') {
                            $scope.fail_student_list[b].formula_value = Math.round((parseInt($scope.fail_student_list[b].sims_gb_cat_assign_max_score)/100) * 5)
                        } else if ($scope.fail_student_list[b].sims_sports_flag == 'Y') {
                            $scope.fail_student_list[b].formula_value = Math.round((parseInt($scope.fail_student_list[b].passing_marks) / 100) * 50)
                        } else {
                            $scope.fail_student_list[b].formula_value = 0;
                        }
                    }
                             
                    //new code 04-02-19

                    for (var i = 0; i < $scope.fail_student_list.length; i++) {
                        if (parseInt($scope.fail_student_list[i].marks_needed_to_pass) > 0) {
                            if (parseInt($scope.fail_student_list[i].sims_activity_points_gen) > 0) {
                                if (parseInt($scope.fail_student_list[i].points_from_other_than_general_gracing_remaining) > 0) {
                                    if ((parseInt($scope.fail_student_list[i].sims_activity_points) - $scope.fail_student_list[i].formula_value) >= 0) {
                                        if (parseInt($scope.fail_student_list[i].sims_activity_points) > parseInt($scope.fail_student_list[i].formula_value) && parseInt($scope.fail_student_list[i].formula_value) >= parseInt($scope.fail_student_list[i].marks_needed_to_pass)) {
                                            $scope.fail_student_list[i].marks_applied_other_than_general = parseInt($scope.fail_student_list[i].marks_needed_to_pass);

                                            for (var s = 0; s < $scope.fail_student_list.length; s++) {
                                                $scope.fail_student_list[s].remaining_points = (parseInt($scope.fail_student_list[s].sims_activity_points) - $scope.fail_student_list[i].formula_value);
                                                $scope.fail_student_list[s].marks_needed_to_pass_remaining = parseInt($scope.fail_student_list[s].marks_needed_to_pass) - parseInt($scope.fail_student_list[i].marks_applied_other_than_general);
                                                $scope.fail_student_list[s].points_from_other_than_general_gracing_remaining = $scope.fail_student_list[s].points_from_other_than_general_gracing_remaining - parseInt($scope.fail_student_list[i].marks_applied_other_than_general);
                                                $scope.fail_student_list[s].points_from_other_than_general_gracing = $scope.fail_student_list[s].points_from_other_than_general_gracing - parseInt($scope.fail_student_list[i].marks_applied_other_than_general);
                                            }
                                            $scope.fail_student_list[i].marks_needed_to_pass_remaining = 0;

                                        } else { 

                                            if (parseInt($scope.fail_student_list[i].formula_value) >= parseInt($scope.fail_student_list[i].points_from_other_than_general_gracing)) {
                                                $scope.fail_student_list[i].marks_applied_other_than_general = parseInt($scope.fail_student_list[i].points_from_other_than_general_gracing);

                                                for (var s = 0; s < $scope.fail_student_list.length; s++) {
                                                    $scope.fail_student_list[s].remaining_points = (parseInt($scope.fail_student_list[s].sims_activity_points) - parseInt($scope.fail_student_list[s].marks_applied_other_than_general));
                                                    $scope.fail_student_list[s].marks_needed_to_pass_remaining = parseInt($scope.fail_student_list[s].marks_needed_to_pass) - parseInt($scope.fail_student_list[s].marks_applied_other_than_general);
                                                    $scope.fail_student_list[s].points_from_other_than_general_gracing_remaining = $scope.fail_student_list[s].points_from_other_than_general_gracing_remaining - parseInt($scope.fail_student_list[s].marks_applied_other_than_general);
                                                    $scope.fail_student_list[s].points_from_other_than_general_gracing = $scope.fail_student_list[s].points_from_other_than_general_gracing - parseInt($scope.fail_student_list[i].marks_applied_other_than_general);
                                                }

                                            } else {
                                                $scope.fail_student_list[i].marks_applied_other_than_general = parseInt($scope.fail_student_list[i].formula_value);
                                                for (var s = 0; s < $scope.fail_student_list.length; s++) {
                                                    $scope.fail_student_list[s].remaining_points = (parseInt($scope.fail_student_list[s].sims_activity_points) - $scope.fail_student_list[s].formula_value);
                                                    $scope.fail_student_list[s].marks_needed_to_pass_remaining = (parseInt($scope.fail_student_list[s].marks_needed_to_pass) - $scope.fail_student_list[s].formula_value);
                                                    $scope.fail_student_list[s].points_from_other_than_general_gracing_remaining = $scope.fail_student_list[s].points_from_other_than_general_gracing_remaining - $scope.fail_student_list[s].formula_value;
                                                }

                                            }
                                            //  $scope.fail_student_list[i].remaining_points = (parseInt($scope.fail_student_list[i].sims_activity_points) - $scope.fail_student_list[i].formula_value);
                                            //for (var s = 0; s < $scope.fail_student_list.length; s++) {
                                            //    $scope.fail_student_list[s].remaining_points = (parseInt($scope.fail_student_list[s].sims_activity_points) - $scope.fail_student_list[s].formula_value);
                                            //}
                                           // $scope.fail_student_list[i].marks_needed_to_pass_remaining = (parseInt($scope.fail_student_list[i].marks_needed_to_pass) - $scope.fail_student_list[i].formula_value);

                                        }

                                        //for (var k = 0; k < $scope.fail_student_list.length; k++) {
                                        //    $scope.fail_student_list[k].points_from_other_than_general_gracing_remaining = $scope.fail_student_list[k].points_from_other_than_general_gracing_remaining - $scope.fail_student_list[k].formula_value;
                                        //}
                                      //  $scope.fail_student_list[i].points_from_other_than_general_gracing_remaining = 0;
                                        if ($scope.fail_student_list[i].remaining_points >= $scope.fail_student_list[i].marks_needed_to_pass_remaining && parseInt($scope.fail_student_list[i].marks_needed_to_pass_remaining)>0) {
                                            $scope.fail_student_list[i].marks_applied_general_gracing = parseInt($scope.fail_student_list[i].marks_needed_to_pass_remaining);
                                            for (var s = 0; s < $scope.fail_student_list.length; s++) {
                                                $scope.fail_student_list[s].remaining_points = (parseInt($scope.fail_student_list[s].remaining_points) - parseInt($scope.fail_student_list[i].marks_applied_general_gracing));
                                                $scope.fail_student_list[s].marks_needed_to_pass_remaining = parseInt($scope.fail_student_list[s].marks_needed_to_pass_remaining) - parseInt($scope.fail_student_list[s].marks_applied_general_gracing);
                                            }
                                        } else {
                                            $scope.fail_student_list[i].marks_applied_general_gracing = 0;
                                        }                                     
                                    }

                                    // if activity point is less than formule value
                                    if ((parseInt($scope.fail_student_list[i].remaining_points) - $scope.fail_student_list[i].marks_needed_to_pass_remaining) >= 0 && parseInt($scope.fail_student_list[i].marks_needed_to_pass_remaining) > 0) {

                                        if ((parseInt($scope.fail_student_list[i].sims_activity_points) >= $scope.fail_student_list[i].marks_needed_to_pass)) {
                                            $scope.fail_student_list[i].marks_applied_general_gracing = parseInt($scope.fail_student_list[i].marks_needed_to_pass);
                                        } else {
                                            $scope.fail_student_list[i].marks_applied_general_gracing = 0;
                                        }

                                    } else {
                                       // $scope.fail_student_list[i].marks_applied_general_gracing = 0;
                                    }
                                    // end activity point is less than formule value
                                }

                                else {
                                    //only for general gracing not for other than general gracing
                                    if ((parseInt($scope.fail_student_list[i].sims_activity_points) - $scope.fail_student_list[i].marks_needed_to_pass) >= 0) {

                                        if ((parseInt($scope.fail_student_list[i].sims_activity_points) >= $scope.fail_student_list[i].marks_needed_to_pass)) {
                                            $scope.fail_student_list[i].marks_applied_general_gracing = parseInt($scope.fail_student_list[i].marks_needed_to_pass);
                                        } else {
                                            $scope.fail_student_list[i].marks_applied_general_gracing = 0;
                                        }

                                    } else {
                                        $scope.fail_student_list[i].marks_applied_general_gracing = 0;
                                    }
                                }
                            }
                            else {
                                //code for other than general gracing
                                $scope.fail_student_list[i].marks_applied_general_gracing = 0;
                                if ((parseInt($scope.fail_student_list[i].sims_activity_points) - $scope.fail_student_list[i].marks_needed_to_pass) >= 0) {

                                    if ((parseInt($scope.fail_student_list[i].sims_activity_points) >= $scope.fail_student_list[i].marks_needed_to_pass)) {
                                        $scope.fail_student_list[i].marks_applied_other_than_general = parseInt($scope.fail_student_list[i].marks_needed_to_pass);
                                    } else {
                                        $scope.fail_student_list[i].marks_applied_other_than_general = 0;
                                    }

                                } else {
                                    $scope.fail_student_list[i].marks_applied_other_than_general = 0;
                                }

                            }
                        } else {
                            $scope.fail_student_list[i].marks_applied_other_than_general = 0;
                            $scope.fail_student_list[i].marks_applied_general_gracing = 0;
                        }
                    }

                    //code ends here
                });
            }

            $scope.changeStudAct = function (index, info, sims_activity_number, category) {
              //  $scope.fail_student_list.splice(index, 1)
                $http.get(ENV.apiUrl + "api/GracingMarksTrans/GetsingleFailStudent?sims_cur_code=" + info.sims_cur_code + "&sims_academic_year=" + info.sims_academic_year + "&sims_grade_code=" + info.sims_grade_code + "&sims_section_code=" + info.sims_section_code + "&sims_gb_term_code=" + info.sims_gb_term_code + "&sims_gb_cat_assign_enroll_number=" + info.sims_gb_cat_assign_enroll_number + "&sims_activity_number=" + info.sims_activity_number + "&category_name=" + category).then(function (data_all) {
                    $scope.fail_student_list_new = data_all.data;
                    console.log("$scope.fail_student_list_new", $scope.fail_student_list_new);

                    for(var i=0;i<$scope.fail_student_list.length;i++){
                        for (var j = 0; j < $scope.fail_student_list_new.length; j++) {
                            if ($scope.fail_student_list[i].sims_gb_cat_assign_enroll_number == $scope.fail_student_list_new[j].sims_gb_cat_assign_enroll_number && $scope.fail_student_list[i].sims_gb_subject_code == $scope.fail_student_list_new[j].sims_gb_subject_code
                                ) {
                                $scope.fail_student_list[i].sims_activity_number = $scope.fail_student_list_new[j].sims_activity_number;
                                $scope.fail_student_list[i].sims_activity_name = $scope.fail_student_list_new[j].sims_activity_name;
                                $scope.fail_student_list[i].sims_activity_symbol = $scope.fail_student_list_new[j].sims_activity_symbol;
                                $scope.fail_student_list[i].sims_activity_max_point = $scope.fail_student_list_new[j].sims_activity_max_point;
                                $scope.fail_student_list[i].sims_activity_max_point_subject = $scope.fail_student_list_new[j].sims_activity_max_point_subject;
                                $scope.fail_student_list[i].sims_activity_priority = $scope.fail_student_list_new[j].sims_activity_priority;
                                $scope.fail_student_list[i].sims_activity_points = $scope.fail_student_list_new[j].sims_activity_points;
                                $scope.fail_student_list[i].sims_gb_subject_code = $scope.fail_student_list_new[j].sims_gb_subject_code;
                                $scope.fail_student_list[i].sims_subject_name_en = $scope.fail_student_list_new[j].sims_subject_name_en;
                                $scope.fail_student_list[i].sims_gb_cat_assign_mark = $scope.fail_student_list_new[j].sims_gb_cat_assign_mark;
                                $scope.fail_student_list[i].marks_needed_to_pass = $scope.fail_student_list_new[j].marks_needed_to_pass;
                                $scope.fail_student_list[i].passing_marks = $scope.fail_student_list_new[j].passing_marks;
                                $scope.fail_student_list[i].balance_activity = $scope.fail_student_list_new[j].balance_activity;
                                $scope.fail_student_list[i].bal_act = $scope.fail_student_list_new[j].bal_act;
                                $scope.fail_student_list[i].sims_gb_term_code = $scope.fail_student_list_new[j].sims_gb_term_code;

                                if ($scope.fail_student_list_new[j].marks_needed_to_pass > 0) {
                                    $scope.fail_student_list[i].marks_needed_to_pass_background_color = '#f7c9da';
                                }

                            }
                        }
                    }

                });

            }

            $scope.Insert_data = function () {
                debugger
                $scope.savedata = [];
                $scope.busy = true;
                for (var i = 0; i < $scope.save_all_data.length; i++) {
                            var datasend = {
                                'sims_cur_code': $scope.save_all_data[i].sims_cur_code,
                                'sims_academic_year': $scope.save_all_data[i].sims_academic_year,
                                'sims_grade_code': $scope.save_all_data[i].sims_grade_code,
                                'sims_section_code': $scope.save_all_data[i].sims_section_code,
                                'sims_enroll_number': $scope.save_all_data[i].sims_gb_cat_assign_enroll_number,
                                'sims_activity_number': $scope.save_all_data[i].sims_activity_number,
                                'sims_subject_code': $scope.save_all_data[i].sims_gb_subject_code,
                                'sims_marks_obtain': $scope.save_all_data[i].sims_gb_cat_assign_mark,
                                'sims_marks_applied': $scope.save_all_data[i].marks_needed_to_pass,
                                'sims_balance_activity_points': $scope.save_all_data[i].bal_act,
                                'sims_activity_points_gen': $scope.save_all_data[i].sims_activity_points_gen,
                                'points_from_other_than_general_gracing': $scope.save_all_data[i].points_from_other_than_general_gracing,
                                'sims_passing_marks_of_student': $scope.save_all_data[i].passing_marks,
                                'user': username,
                                'sims_gb_term_code': $scope.save_all_data[i].sims_gb_term_code,
                                'sims_category_name': $scope.fcommn.sims_category_name,
                            }
                            $scope.savedata.push(datasend);
                }

                   console.log("savedata", $scope.savedata);


                   $http.post(ENV.apiUrl + "api/GracingMarksTrans/InsertGraceMarks", $scope.savedata).then(function (msg) {
                    $scope.msg1 = msg.data;
                    console.log("msg", msg);

                    if ($scope.msg1 == true) {
                        $(document).ready(function () {
                            $scope.datasend = [];
                            $scope.savedata = [];
                        });
                        swal({ title: "Alert", text: "Records Inserted successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        $scope.busy = false;
                        $scope.show_fail_students($scope.fcommn.cur_code, $scope.fcommn.academic_year, $scope.fcommn.sims_grade_code, $scope.fcommn.sims_section_code, $scope.fcommn.sims_term_code);
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        $scope.busy = false;
                    }
                });


            }


            $scope.show_only_gracing_applied_students = function (only_gracing_applied) {
                debugger
                $scope.new_fail_student = [];
                if (only_gracing_applied == true) {
                    for (var i = 0; i < $scope.fail_student_list.length; i++) {
                        if ($scope.fail_student_list[i].marks_needed_to_pass > 0) {
                            $scope.new_fail_student.push($scope.fail_student_list[i]);
                        }
                    }
                    $scope.fail_student_list = [];
                    $scope.fail_student_list = angular.copy($scope.new_fail_student);
                } else {
                    $scope.show_fail_students($scope.fcommn.cur_code, $scope.fcommn.academic_year, $scope.fcommn.sims_grade_code, $scope.fcommn.sims_section_code, $scope.fcommn.sims_term_code);
                }
            }


            $scope.removeGraceMarksofsub = function (str) {
                debugger

                $scope.exclude_sub_list = '';
                if (str.exclude_sub_marks == false) {



                    for (var i = 0; i < $scope.fail_student_list.length; i++) {
                        if ($scope.fail_student_list[i].sims_gb_cat_assign_enroll_number == str.sims_gb_cat_assign_enroll_number) {
                        var t = $scope.fail_student_list[i].sims_gb_cat_assign_enroll_number;
                        var v = document.getElementById(t + i);
                        if (str.exclude_sub_marks == false) {
                            v.checked = false;
                            }
                        }
                    }



                    for (var i = 0; i < $scope.fail_student_list.length; i++) {
                        if (str.sims_gb_cat_assign_enroll_number == $scope.fail_student_list[i].sims_gb_cat_assign_enroll_number) {
                            if ($scope.fail_student_list[i].exclude_sub_marks == false) {
                                $scope.exclude_sub_list = $scope.exclude_sub_list + $scope.fail_student_list[i].sims_gb_subject_code + ',';
                            }
                        }
                    }

                    console.log($scope.exclude_sub_list);
                    $http.get(ENV.apiUrl + "api/GracingMarksTrans/GetexcludeSub?sims_cur_code=" + str.sims_cur_code + "&sims_academic_year=" + str.sims_academic_year + "&sims_grade_code=" + str.sims_grade_code + "&sims_section_code=" + str.sims_section_code + "&sims_gb_term_code=" + str.sims_gb_term_code + "&sims_gb_cat_assign_enroll_number=" + str.sims_gb_cat_assign_enroll_number + "&sims_activity_number=" + str.sims_activity_number + "&sims_subject_code=" + $scope.exclude_sub_list + "&sort_by=" + $scope.fcommn.sort_by + "&category_name=" + $scope.fcommn.sims_category_name).then(function (data_all) {
                        $scope.fail_student_list_new = data_all.data;

                        //-------------------------------------------------------------------------
                        //old Code
                        //for (var i = 0; i < $scope.fail_student_list_new.length; i++) {
                        //    if ($scope.fail_student_list_new[i].marks_needed_to_pass > 0) {
                        //        $scope.fail_student_list_new[i].exclude_sub_marks = true;
                        //    } else
                        //        $scope.fail_student_list_new[i].exclude_sub_marks = false;
                        //}

                        $scope.total_student_failed_count = $scope.fail_student_list_new[0].distinct_sims_gb_cat_assign_enroll_number;
                        $scope.total_subject_failed_count = $scope.fail_student_list_new[0].distinct_sims_gb_subject_code;
                        $scope.total_student_pass_subject_count = $scope.fail_student_list_new[0].total_student_pass_subject_count;

                        $scope.total_student_pass_subject = [];
                        for (var i = 0; i < $scope.fail_student_list_new.length; i++) {
                            if ($scope.fail_student_list_new[i].marks_needed_to_pass > 0) {
                                $scope.fail_student_list_new[i].marks_needed_to_pass_background_color = '#f7c9da';
                                $scope.total_student_pass_subject.push($scope.fail_student_list_new[i]);
                            }
                        }

                        $scope.total_student_passed = 0;

                        if ($scope.total_student_pass_subject.length > 0) {
                            for (var i = 0; i < $scope.fail_student_list_new.length; i++) {
                                var flag = 0;
                                for (var j = 0; j < $scope.total_student_pass_subject.length; j++) {
                                    if ($scope.fail_student_list_new[i].sims_gb_cat_assign_enroll_number != $scope.total_student_pass_subject[j].sims_gb_cat_assign_enroll_number) {
                                        flag = 1;
                                    } else {
                                        flag = 0;
                                    }
                                }
                                if (flag == 0) {
                                    $scope.total_student_passed = $scope.total_student_passed + 1;
                                }
                            }
                        }

                        for (var b = 0; b < $scope.fail_student_list_new.length; b++) {
                            $scope.fail_student_list_new[b].marks_applied_other_than_general = 0;
                            $scope.fail_student_list_new[b].remaining_points = $scope.fail_student_list_new[b].sims_activity_points;
                            $scope.fail_student_list_new[b].marks_needed_to_pass_remaining = $scope.fail_student_list_new[b].marks_needed_to_pass;
                            if ($scope.fail_student_list_new[b].sims_gracing_type == 'GE') {
                                $scope.fail_student_list_new[b].points_from_other_than_general_gracing_remaining = 0;
                            } else {
                                $scope.fail_student_list_new[b].points_from_other_than_general_gracing_remaining = $scope.fail_student_list_new[b].points_from_other_than_general_gracing;
                            }
                            $scope.fail_student_list_new[b].formula_value = 0;
                            if ($scope.fail_student_list_new[b].sims_sports_flag == 'N') {
                                $scope.fail_student_list_new[b].formula_value = Math.round((parseInt($scope.fail_student_list_new[b].sims_gb_cat_assign_max_score) / 100) * 5)
                            } else if ($scope.fail_student_list_new[b].sims_sports_flag == 'Y') {
                                $scope.fail_student_list_new[b].formula_value = Math.round((parseInt($scope.fail_student_list_new[b].passing_marks) / 100) * 50)
                            } else {
                                $scope.fail_student_list_new[b].formula_value = 0;
                            }
                        }

                        //new code 04-02-19

                        for (var i = 0; i < $scope.fail_student_list_new.length; i++) {
                            if (parseInt($scope.fail_student_list_new[i].marks_needed_to_pass) > 0) {
                                if (parseInt($scope.fail_student_list_new[i].sims_activity_points_gen) > 0) {
                                    if (parseInt($scope.fail_student_list_new[i].points_from_other_than_general_gracing_remaining) > 0) {
                                        if ((parseInt($scope.fail_student_list_new[i].sims_activity_points) - $scope.fail_student_list_new[i].formula_value) >= 0) {
                                            if (parseInt($scope.fail_student_list_new[i].sims_activity_points) > parseInt($scope.fail_student_list_new[i].formula_value) && parseInt($scope.fail_student_list_new[i].formula_value) >= parseInt($scope.fail_student_list_new[i].marks_needed_to_pass)) {
                                                $scope.fail_student_list_new[i].marks_applied_other_than_general = parseInt($scope.fail_student_list_new[i].marks_needed_to_pass);

                                                for (var s = 0; s < $scope.fail_student_list_new.length; s++) {
                                                    $scope.fail_student_list_new[s].remaining_points = (parseInt($scope.fail_student_list_new[s].sims_activity_points) - $scope.fail_student_list_new[i].formula_value);
                                                    $scope.fail_student_list_new[s].marks_needed_to_pass_remaining = parseInt($scope.fail_student_list_new[s].marks_needed_to_pass) - parseInt($scope.fail_student_list_new[i].marks_applied_other_than_general);
                                                    $scope.fail_student_list_new[s].points_from_other_than_general_gracing_remaining = $scope.fail_student_list_new[s].points_from_other_than_general_gracing_remaining - parseInt($scope.fail_student_list_new[i].marks_applied_other_than_general);
                                                    $scope.fail_student_list_new[s].points_from_other_than_general_gracing = $scope.fail_student_list_new[s].points_from_other_than_general_gracing - parseInt($scope.fail_student_list_new[i].marks_applied_other_than_general);
                                                }
                                                $scope.fail_student_list_new[i].marks_needed_to_pass_remaining = 0;

                                            } else {

                                                if (parseInt($scope.fail_student_list_new[i].formula_value) >= parseInt($scope.fail_student_list_new[i].points_from_other_than_general_gracing)) {
                                                    $scope.fail_student_list_new[i].marks_applied_other_than_general = parseInt($scope.fail_student_list_new[i].points_from_other_than_general_gracing);

                                                    for (var s = 0; s < $scope.fail_student_list_new.length; s++) {
                                                        $scope.fail_student_list_new[s].remaining_points = (parseInt($scope.fail_student_list_new[s].sims_activity_points) - parseInt($scope.fail_student_list_new[s].marks_applied_other_than_general));
                                                        $scope.fail_student_list_new[s].marks_needed_to_pass_remaining = parseInt($scope.fail_student_list_new[s].marks_needed_to_pass) - parseInt($scope.fail_student_list_new[s].marks_applied_other_than_general);
                                                        $scope.fail_student_list_new[s].points_from_other_than_general_gracing_remaining = $scope.fail_student_list_new[s].points_from_other_than_general_gracing_remaining - parseInt($scope.fail_student_list_new[s].marks_applied_other_than_general);
                                                        $scope.fail_student_list_new[s].points_from_other_than_general_gracing = $scope.fail_student_list_new[s].points_from_other_than_general_gracing - parseInt($scope.fail_student_list_new[i].marks_applied_other_than_general);
                                                    }

                                                } else {
                                                    $scope.fail_student_list_new[i].marks_applied_other_than_general = parseInt($scope.fail_student_list_new[i].formula_value);
                                                    for (var s = 0; s < $scope.fail_student_list_new.length; s++) {
                                                        $scope.fail_student_list_new[s].remaining_points = (parseInt($scope.fail_student_list_new[s].sims_activity_points) - $scope.fail_student_list_new[s].formula_value);
                                                        $scope.fail_student_list_new[s].marks_needed_to_pass_remaining = (parseInt($scope.fail_student_list_new[s].marks_needed_to_pass) - $scope.fail_student_list_new[s].formula_value);
                                                        $scope.fail_student_list_new[s].points_from_other_than_general_gracing_remaining = $scope.fail_student_list_new[s].points_from_other_than_general_gracing_remaining - $scope.fail_student_list_new[s].formula_value;
                                                    }

                                                }
                                                //  $scope.fail_student_list_new[i].remaining_points = (parseInt($scope.fail_student_list_new[i].sims_activity_points) - $scope.fail_student_list_new[i].formula_value);
                                                //for (var s = 0; s < $scope.fail_student_list_new.length; s++) {
                                                //    $scope.fail_student_list_new[s].remaining_points = (parseInt($scope.fail_student_list_new[s].sims_activity_points) - $scope.fail_student_list_new[s].formula_value);
                                                //}
                                                // $scope.fail_student_list_new[i].marks_needed_to_pass_remaining = (parseInt($scope.fail_student_list_new[i].marks_needed_to_pass) - $scope.fail_student_list_new[i].formula_value);

                                            }

                                            //for (var k = 0; k < $scope.fail_student_list_new.length; k++) {
                                            //    $scope.fail_student_list_new[k].points_from_other_than_general_gracing_remaining = $scope.fail_student_list_new[k].points_from_other_than_general_gracing_remaining - $scope.fail_student_list_new[k].formula_value;
                                            //}
                                            //  $scope.fail_student_list_new[i].points_from_other_than_general_gracing_remaining = 0;
                                            if ($scope.fail_student_list_new[i].remaining_points >= $scope.fail_student_list_new[i].marks_needed_to_pass_remaining && parseInt($scope.fail_student_list_new[i].marks_needed_to_pass_remaining) > 0) {
                                                $scope.fail_student_list_new[i].marks_applied_general_gracing = parseInt($scope.fail_student_list_new[i].marks_needed_to_pass_remaining);
                                                for (var s = 0; s < $scope.fail_student_list_new.length; s++) {
                                                    $scope.fail_student_list_new[s].remaining_points = (parseInt($scope.fail_student_list_new[s].remaining_points) - parseInt($scope.fail_student_list_new[i].marks_applied_general_gracing));
                                                    $scope.fail_student_list_new[s].marks_needed_to_pass_remaining = parseInt($scope.fail_student_list_new[s].marks_needed_to_pass_remaining) - parseInt($scope.fail_student_list_new[s].marks_applied_general_gracing);
                                                }
                                            } else {
                                                $scope.fail_student_list_new[i].marks_applied_general_gracing = 0;
                                            }
                                        }

                                        // if activity point is less than formule value
                                        if ((parseInt($scope.fail_student_list_new[i].remaining_points) - $scope.fail_student_list_new[i].marks_needed_to_pass_remaining) >= 0 && parseInt($scope.fail_student_list_new[i].marks_needed_to_pass_remaining) > 0) {

                                            if ((parseInt($scope.fail_student_list_new[i].sims_activity_points) >= $scope.fail_student_list_new[i].marks_needed_to_pass)) {
                                                $scope.fail_student_list_new[i].marks_applied_general_gracing = parseInt($scope.fail_student_list_new[i].marks_needed_to_pass);
                                            } else {
                                                $scope.fail_student_list_new[i].marks_applied_general_gracing = 0;
                                            }

                                        } else {
                                            // $scope.fail_student_list_new[i].marks_applied_general_gracing = 0;
                                        }
                                        // end activity point is less than formule value
                                    }

                                    else {
                                        //only for general gracing not for other than general gracing
                                        if ((parseInt($scope.fail_student_list_new[i].sims_activity_points) - $scope.fail_student_list_new[i].marks_needed_to_pass) >= 0) {

                                            if ((parseInt($scope.fail_student_list_new[i].sims_activity_points) >= $scope.fail_student_list_new[i].marks_needed_to_pass)) {
                                                $scope.fail_student_list_new[i].marks_applied_general_gracing = parseInt($scope.fail_student_list_new[i].marks_needed_to_pass);
                                            } else {
                                                $scope.fail_student_list_new[i].marks_applied_general_gracing = 0;
                                            }

                                        } else {
                                            $scope.fail_student_list_new[i].marks_applied_general_gracing = 0;
                                        }
                                    }
                                }
                                else {
                                    //code for other than general gracing
                                    $scope.fail_student_list_new[i].marks_applied_general_gracing = 0;
                                    if ((parseInt($scope.fail_student_list_new[i].sims_activity_points) - $scope.fail_student_list_new[i].marks_needed_to_pass) >= 0) {

                                        if ((parseInt($scope.fail_student_list_new[i].sims_activity_points) >= $scope.fail_student_list_new[i].marks_needed_to_pass)) {
                                            $scope.fail_student_list_new[i].marks_applied_other_than_general = parseInt($scope.fail_student_list_new[i].marks_needed_to_pass);
                                        } else {
                                            $scope.fail_student_list_new[i].marks_applied_other_than_general = 0;
                                        }

                                    } else {
                                        $scope.fail_student_list_new[i].marks_applied_other_than_general = 0;
                                    }

                                }
                            } else {
                                $scope.fail_student_list_new[i].marks_applied_other_than_general = 0;
                                $scope.fail_student_list_new[i].marks_applied_general_gracing = 0;
                            }
                        }

                        //code ends here
                        //-------------------------------------------------------------------------
                        console.log("$scope.fail_student_list_new", $scope.fail_student_list_new);
                        for (var i = 0; i < $scope.fail_student_list.length; i++) {
                            for (var j = 0; j < $scope.fail_student_list_new.length; j++) {
                                if ($scope.fail_student_list[i].sims_gb_cat_assign_enroll_number == $scope.fail_student_list_new[j].sims_gb_cat_assign_enroll_number) {
                                    if (    $scope.fail_student_list[i].sims_gb_subject_code == $scope.fail_student_list_new[j].sims_gb_subject_code ) { 
                                            $scope.fail_student_list[i].sims_activity_number = $scope.fail_student_list_new[j].sims_activity_number;
                                            $scope.fail_student_list[i].sims_activity_name = $scope.fail_student_list_new[j].sims_activity_name;
                                            $scope.fail_student_list[i].sims_activity_symbol = $scope.fail_student_list_new[j].sims_activity_symbol;
                                            $scope.fail_student_list[i].sims_activity_max_point = $scope.fail_student_list_new[j].sims_activity_max_point;
                                            $scope.fail_student_list[i].sims_activity_max_point_subject = $scope.fail_student_list_new[j].sims_activity_max_point_subject;
                                            $scope.fail_student_list[i].sims_activity_priority = $scope.fail_student_list_new[j].sims_activity_priority;
                                            $scope.fail_student_list[i].sims_activity_points = $scope.fail_student_list_new[j].sims_activity_points;
                                            $scope.fail_student_list[i].sims_gb_subject_code = $scope.fail_student_list_new[j].sims_gb_subject_code;
                                            $scope.fail_student_list[i].sims_subject_name_en = $scope.fail_student_list_new[j].sims_subject_name_en;
                                            $scope.fail_student_list[i].sims_gb_cat_assign_mark = $scope.fail_student_list_new[j].sims_gb_cat_assign_mark;
                                            $scope.fail_student_list[i].marks_needed_to_pass = $scope.fail_student_list_new[j].marks_needed_to_pass;
                                            $scope.fail_student_list[i].passing_marks = $scope.fail_student_list_new[j].passing_marks;
                                            $scope.fail_student_list[i].balance_activity = $scope.fail_student_list_new[j].balance_activity;
                                            $scope.fail_student_list[i].bal_act = $scope.fail_student_list_new[j].bal_act;
                                            $scope.fail_student_list[i].sims_gb_term_code = $scope.fail_student_list_new[j].sims_gb_term_code;
                                            $scope.fail_student_list[i].marks_applied_other_than_general=$scope.fail_student_list_new[j].marks_applied_other_than_general ;
                                            $scope.fail_student_list[i].marks_applied_general_gracing = $scope.fail_student_list_new[j].marks_applied_general_gracing;

                                            if ($scope.fail_student_list_new[j].marks_needed_to_pass > 0) {
                                                $scope.fail_student_list[i].marks_needed_to_pass_background_color = '#f7c9da';
                                                $scope.fail_student_list[i].exclude_sub_marks = true;
                                            }
                                            else {
                                                $scope.fail_student_list[i].marks_needed_to_pass_background_color = 'transparent';
                                                $scope.fail_student_list[i].exclude_sub_marks = false;
                                            }
                                    }
                                }
                            }
                        }

                    });
                }

            }


        }]);
})();