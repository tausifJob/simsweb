﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Gradebook');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('MarksEntryCont', ['$scope', '$compile', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$locale', '$http', 'ENV', 'XLSXReaderService', '$filter',
        function ($scope, $compile, $state, $rootScope, $timeout, gettextCatalog, $locale, $http, ENV,XLSXReaderService,$filter) {

            $scope.visibleDownloadbtn = false;
            $scope.visibleUploadbtn = false;
        $scope.hasChangedSubattr = false;
        $scope.getCurriculum = function () {
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                if (res.data) {
                    $scope.Allcurriculum = res.data;
                    $scope.cur_code = $scope.Allcurriculum[0].sims_cur_code;
                    $scope.getAcademicYear();
                }
            });
        }

        $scope.getAcademicYear = function () {
            $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.cur_code).then(function (res) {
                if (res.data) {
                    $scope.AllAcademicYear = res.data;
                    $scope.showAll = false;
                    $scope.showData = false;
                    $scope.aca_year = $scope.AllAcademicYear[0].sims_academic_year;

                    $scope.getData($scope.aca_year);
                }
            });
        };


        $scope.academicYearChange = function () {
            $scope.teacherInfo = false;
            $scope.examDateInfo = false;
            $scope.singleTable = false;
            $scope.ScholasticTable = false;
            $scope.showAll = false;
            $scope.showData = false;
            $scope.sims_grade = '';
            $scope.sims_section = '';
            $scope.sims_term = '';
            $scope.sims_category = {};
            $scope.sims_assignment = {};
            $scope.sims_subject = {};
            $scope.sims_indicator = {};
        }


        $scope.getData = function () {
            $scope.showData = true;
            $scope.getAllGrade();
        }


        $scope.getAllGrade = function () {
            var a = {
                opr: 'A',
                cur_code: $scope.cur_code,
                aca_year: $scope.aca_year,
                user: $rootScope.globals.currentUser.username
            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", a).then(function (res) {
                if (res.data.table.length > 0) {
                    $scope.AllGrades = res.data.table;
                    $scope.sims_grade = $scope.AllGrades[0].sims_grade_code;
                    $scope.getGradeSection();
                }
            });
        }


        $scope.getGradeSection = function () {
            $scope.teacherInfo = false;
            $scope.examDateInfo = false;
            $scope.singleTable = false;
            $scope.ScholasticTable = false;
            $scope.sims_section = '';
            $scope.sims_term = '';
            $scope.sims_category = {};
            $scope.AllSectionFromGrade = [];
            $scope.sims_assignment = {};
            $scope.sims_subject = {};
            $scope.sims_indicator = {};
            var a = {
                opr: 'B',
                cur_code: $scope.cur_code,
                aca_year: $scope.aca_year,
                grade_code: $scope.sims_grade,
                user: $rootScope.globals.currentUser.username
            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", a).then(function (res) {
                if (res.data.table.length > 0) {
                    $scope.AllSectionFromGrade = res.data.table;
                    $scope.sims_section = $scope.AllSectionFromGrade[0].sims_section_code;
                    $scope.getSectionChange();
                }
            });
        }

        $scope.getSectionChange = function () {
            $scope.teacherInfo = false;
            $scope.examDateInfo = false;
            $scope.singleTable = false;
            $scope.ScholasticTable = false;
            $scope.sims_term = '';
            $scope.sims_category = {};
            $scope.sims_assignment = {};
            $scope.AllGradeMappedTerm = [];
            $scope.sims_subject = {};
            $scope.sims_indicator = {};
            var a = {
                opr: 'C',
                cur_code: $scope.cur_code,
                aca_year: $scope.aca_year,
                grade_code: $scope.sims_grade,
                section_code: $scope.sims_section
            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", a).then(function (res) {
                if (res.data.table.length > 0) {
                    $scope.AllGradeMappedTerm = res.data.table;
                    $scope.sims_term = $scope.AllGradeMappedTerm[0].sims_report_card_term_code;
                    $scope.getTermChange();
                }
            });
        }

        $scope.getTermChange = function () {
            $scope.teacherInfo = false;
            $scope.examDateInfo = false;
            $scope.singleTable = false;
            $scope.ScholasticTable = false;
            $scope.sims_category = {};
            $scope.AllMappedCategory = [];
            $scope.sims_assignment = {};
            $scope.sims_subject = {};
            $scope.sims_indicator = {};
            var a = {
                opr: 'D',
                cur_code: $scope.cur_code,
                aca_year: $scope.aca_year,
                grade_code: $scope.sims_grade,
                section_code: $scope.sims_section,
                term_code: $scope.sims_term
            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", a).then(function (res) {
                if (res.data.table.length > 0) {
                    $scope.AllMappedCategory = res.data.table;
                    $scope.sims_category = $scope.AllMappedCategory[0];
                    $scope.getCategoryChange();
                }
            });
        }


        $scope.getCategoryChange = function () {
            $scope.teacherInfo = false;
            $scope.examDateInfo = false;
            $scope.singleTable = false;
            $scope.ScholasticTable = false;
            $scope.sims_assignment = {};
            $scope.sims_subject = {};
            $scope.sims_indicator = {};
            $scope.AllMappedAssignment = [];
            $scope.getAllStatus();
            var a = {
                opr: 'E',
                cur_code: $scope.cur_code,
                aca_year: $scope.aca_year,
                grade_code: $scope.sims_grade,
                section_code: $scope.sims_section,
                term_code: $scope.sims_term,
                category_code: parseInt($scope.sims_category.category_code, 10)
            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", a).then(function (res) {
                if (res.data.table.length > 0) {
                    $scope.AllMappedAssignment = res.data.table;
                    $($scope.AllMappedAssignment).each(function (key, val) {
                        val.disflag = val.disflag == 1 ? true : false;
                    })
                    $scope.sims_assignment = $scope.AllMappedAssignment[0];
                   // $scope.show_marks_entry_screen = $scope.AllMappedAssignment[0].sims_report_card_assign_type;
                    $scope.getAssignmentChange();
                }
            });
        }

        $scope.getAssignmentChange = function () {
            

            var a = {
                opr: 'W',
                cur_code: $scope.cur_code,
                aca_year: $scope.aca_year,
                grade_code: $scope.sims_grade,
                section_code: $scope.sims_section,
                term_code: $scope.sims_term,
                category_code: parseInt($scope.sims_category.category_code, 10),
                assignment_number: $scope.sims_assignment.assign_number
            };
        $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", a).then(function (res) {
            if (res.data.table.length > 0) {
                $scope.Data = res.data.table;
                $scope.show_marks_entry_screen = $scope.Data[0].sims_report_card_assign_type;
                $scope.sims_assign_remark_visible = $scope.Data[0].sims_assign_remark_visible;
               // $scope.getAssignmentChange();
            }
       

            $scope.teacherInfo = false;
            $scope.examDateInfo = false;
            $scope.singleTable = false;
            $scope.ScholasticTable = false;
            $scope.sims_subject = {};
            $scope.sims_indicator = {};
            $scope.AllGradingScale = {};
            $scope.AllMappedSubject = [];
            $scope.AllMappedIndicators = [];
            $scope.getGradingScale();
            var a = {
                    opr: $scope.selectedTab == 0 ? 'F' : 'N',
                cur_code: $scope.cur_code,
                aca_year: $scope.aca_year,
                grade_code: $scope.sims_grade,
                section_code: $scope.sims_section,
                term_code: $scope.sims_term,
                category_code: parseInt($scope.sims_category.category_code, 10),
                assignment_number: $scope.sims_assignment.assign_number,
                user: $rootScope.globals.currentUser.username
            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", a).then(function (res) {
                if (res.data.table.length > 0) {
                    if ($scope.selectedTab == 0) {
                        $scope.AllMappedSubject = res.data.table;
                        $scope.sims_subject = $scope.AllMappedSubject[0];
                        $scope.getSubjectChange();
                    }
                    else {
                        $scope.AllMappedIndicators = res.data.table;
                        console.log("$scope.AllMappedIndicators is ", $scope.AllMappedIndicators);
                        $scope.sims_indicator = $scope.AllMappedIndicators[0];
                        $scope.getIndicatorChange();
                    }
                }
              });
            });
        }

        $scope.getGradingScale = function () {
            
            var a = {
                opr: 'H',
                cur_code: $scope.cur_code,
                aca_year: $scope.aca_year,
                group_code: $scope.sims_assignment.assign_grade_scale
            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", a).then(function (res) {
                if (res.data.table.length > 0) {
                    $scope.AllGradingScale = res.data.table;
                }
            });
        }

        $scope.getGradingScaleAssignment = function () {
            var a = {
                opr: 'H',
                cur_code: $scope.cur_code,
                aca_year: $scope.aca_year,
                group_code: $scope.sims_assignment.assign_grade_scale
            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", a).then(function (res) {
                if (res.data.table.length > 0) {
                    $scope.AllGradingScale = res.data.table;
                }
            });
        }

        $scope.getCoscholasticGradingScale = function () {
            var a = {
                opr: 'O',
                cur_code: $scope.cur_code,
                aca_year: $scope.aca_year,
                group_code: $scope.sims_indicator.grade_scale
            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", a).then(function (res) {
                if (res.data.table.length > 0) {
                    $scope.AllCoscholasticGradingScale = res.data.table;
                    console.log("AllCoscholasticGradingScale value is ", $scope.AllCoscholasticGradingScale);
                }
            });
        }

        $scope.findGrade = function (index) {
           
            $scope.isValidMarks(index);
            if ($scope.isValidMark == true) {
                var maxInHigh = (($scope.AllTableData[index].sims_student_assign_received_point == NaN ? 0 : $scope.AllTableData[index].sims_student_assign_received_point) * $scope.AllGradingScale[0].grade_high / $scope.sims_assignment.assign_max_score);
                var code = $scope.AllGradingScale[$scope.AllGradingScale.length - 1].grade_code;
                $($scope.AllGradingScale).each(function (key, val) {
                    if (val.grade_low <= maxInHigh && maxInHigh <= val.grade_high) {
                        code = val.grade_code;
                    }
                });
                $scope.AllTableData[index].sims_student_assign_final_grade = code;
                $scope.enableIsDirty(index);
                $scope.setchangeFlag();
            }
            else {
                alert("Please enter correct Marks between 0 and " + $scope.sims_assignment.assign_max_score);
                $scope.AllTableData[index].sims_student_assign_received_point = 0;
            }
        }

        $scope.checkValid = function (index) {
            $scope.isValidCoScolasticMarks(index);
            if ($scope.isValidCoScholasticMark == true) {
                $scope.enableIsDirty(index);
                $scope.setchangeFlag();
            }
            else {
                alert("Please enter correct Marks between 0 and " + $scope.AllCoscholasticGradingScale[0].sims_co_scholastic_grade_point_high);
                $scope.AllCoScholasticTableData[index].sims_report_card_co_scholastic_grade_point = 0;
            }
        }

        $scope.Subattcheck = function (data, enteredvalue) {
          
            if (parseInt(data.sims_gb_cat_assign_max_point) < parseInt(data.sims_student_assign_received_point)) {
                swal({ title: "Alert", text: "Marks Should be Less Than " + data.sims_gb_cat_assign_max_point, width: 300, height: 200 });
                data.sims_student_assign_received_point = 0;
            }

            $scope.setchangeFlagAttribute(data, enteredvalue);
        }

        $scope.SubattcheckCom = function (data, enteredvalue) {
          
            if (parseInt(data.sims_gb_cat_assign_max_point) < parseInt(data.sims_student_assign_received_point)) {
                swal({ title: "Alert", text: "Marks Should be Less Than " + data.sims_gb_cat_assign_max_point, width: 300, height: 200 });
            }

            for (var i = 0; i < $scope.AllsubjectAttributedata.length; i++) {
                for (var j = 0; j < $scope.AllsubjectAttributedata[i].stud_attr.length; j++) {
                    if (data.sims_cur_code == $scope.AllsubjectAttributedata[i].sims_cur_code && data.sims_academic_year == $scope.AllsubjectAttributedata[i].sims_academic_year && data.sims_grade_code == $scope.AllsubjectAttributedata[i].sims_grade_code &&
                        data.sims_section_code == $scope.AllsubjectAttributedata[i].sims_section_code && data.sims_report_card_config_code == $scope.AllsubjectAttributedata[i].sims_report_card_config_code && data.sims_gb_cat_code == $scope.AllsubjectAttributedata[i].sims_gb_cat_code &&
                        data.sims_gb_cat_assign_number == $scope.AllsubjectAttributedata[i].sims_gb_cat_assign_number && data.sims_gb_number == $scope.AllsubjectAttributedata[i].sims_gb_number && data.sims_enroll_number == $scope.AllsubjectAttributedata[i].sims_enroll_number &&
                        data.sims_subject_srl_no == $scope.AllsubjectAttributedata[i].sims_subject_srl_no) {

                        $scope.AllsubjectAttributedata[i].sims_student_assign_comment = data.sims_student_assign_comment;
                        $scope.AllsubjectAttributedata[i].stud_attr[j].saveData = true;
                    }
                }
            }
            $scope.setchangeFlagAttribute(data, enteredvalue);

        }


        $scope.findCommonGrade = function () {
            $scope.isValidMark = ($scope.student_received_point >= 0 && $scope.student_received_point <= $scope.sims_assignment.assign_max_score) ? true : false;;
            if ($scope.isValidMark == true) {
                var maxInHigh = (($scope.student_received_point == NaN ? 0 : $scope.student_received_point) * $scope.AllGradingScale[0].grade_high / $scope.sims_assignment.assign_max_score);
                var code = $scope.AllGradingScale[$scope.AllGradingScale.length - 1].grade_code;
                $($scope.AllGradingScale).each(function (key, val) {
                    if (val.grade_low <= maxInHigh && maxInHigh <= val.grade_high) {
                        code = val.grade_code;
                    }
                });
                $scope.student_final_grade = code;
            }
            else {
                alert("Please enter correct Marks between 0 and " + $scope.sims_assignment.assign_max_score);
                $scope.student_received_point = 0;
            }
        }

        $scope.ResetAll = function () {
            $scope.getTableData();
            $scope.student_final_grade = "";
            $scope.student_received_point = "";
            $scope.overwriteExisting = false;
            $scope.student_comment = "";
        }

        $scope.ApplyToAll = function () {
            console.log("student_final_grade = ", $scope.student_final_grade);
            console.log("student_received_point = ", $scope.student_received_point);
            console.log("student_comment = ", $scope.student_comment);
            console.log("overwriteExisting = ", $scope.overwriteExisting);
            $($scope.AllTableData).each(function (key, val) {
                if (val.sims_student_assign_exam_status == 'P') {
                    val.isDirty = (val.sims_student_assign_received_point == undefined || val.sims_student_assign_received_point == "") ? true : ($scope.overwriteExisting == true) ? true : false;
                    val.sims_student_assign_final_grade = (val.sims_student_assign_final_grade == undefined || val.sims_student_assign_final_grade == "") ? $scope.student_final_grade : ($scope.overwriteExisting == true) ? $scope.student_final_grade : val.sims_student_assign_final_grade;
                    val.sims_student_assign_received_point = (val.sims_student_assign_received_point == undefined || val.sims_student_assign_received_point == "") ? $scope.student_received_point : ($scope.overwriteExisting == true) ? $scope.student_received_point : val.sims_student_assign_received_point;
                    val.sims_student_assign_comment = (val.sims_student_assign_comment == undefined || val.sims_student_assign_comment == "") ? $scope.student_comment : ($scope.overwriteExisting == true) ? $scope.student_comment : val.sims_student_assign_comment;
                }
                $scope.setchangeFlag();
            });
        }

        $scope.isValidMarks = function (index) {
            $scope.isValidMark = ($scope.AllTableData[index].sims_student_assign_received_point >= 0 && $scope.AllTableData[index].sims_student_assign_received_point <= $scope.sims_assignment.assign_max_score) ? true : false;
        }

        $scope.isValidCoScolasticMarks = function (index) {
            $scope.isValidCoScholasticMark = ($scope.AllCoScholasticTableData[index].sims_report_card_co_scholastic_grade_point >= 0 && $scope.AllCoScholasticTableData[index].sims_report_card_co_scholastic_grade_point <= $scope.AllCoscholasticGradingScale[0].sims_co_scholastic_grade_point_high) ? true : false;
        }

        $scope.findMarks = function (index) {
            var code = $scope.AllTableData[index].grade_code;
            var idx = $scope.AllGradingScale.map(function (e) {
                return e.grade_code;
            }).indexOf(code);
            $scope.AllTableData[index].sims_student_assign_received_point = ($scope.AllGradingScale[idx].grade_high * $scope.sims_assignment.assign_max_score / $scope.AllGradingScale[0].grade_high);
            $scope.enableIsDirty(index);
            $scope.setchangeFlag();
        }

        $scope.findCommonMarks = function () {
            var code = $scope.student_final_grade;
            var idx = $scope.AllGradingScale.map(function (e) {
                return e.grade_code;
            }).indexOf(code);
            $scope.student_received_point = ($scope.AllGradingScale[idx].grade_high * $scope.sims_assignment.assign_max_score / $scope.AllGradingScale[0].grade_high);
        }


        $scope.SaveData = function () {
            
            if ($scope.selectedTab == 0) {
                console.log("$scope.AllTableData = ", $scope.AllTableData);
                $http.post(ENV.apiUrl + "api/MarksEntryNew/SubjectDataInsertion?cTemp=M", $scope.AllTableData).then(function (res) {
                    if (res.data) {
                        alert("Data Saved Successfully");
                        $scope.resetchangeFlag();
                    }
                });
            }
            else if ($scope.selectedTab == 1) {
                console.log("$scope.AllCoScholasticTableData = ", $scope.AllCoScholasticTableData);
                $http.post(ENV.apiUrl + "api/MarksEntryNew/CoscholasticDataInsertion?cTemp=Q", $scope.AllCoScholasticTableData).then(function (res) {
                    if (res.data == 0) {
                        alert("Data Saved Successfully");
                        $scope.resetchangeFlag();
                    }
                    else {
                        alert(res.data + " records not inserted");
                    }
                });
            }
        }

        $scope.SaveDataAttribute = function () {
          
            if ($scope.selectedTab == 0) {
                console.log("$scope.AllTableData = ", $scope.AllsubjectAttributedata);
                var savedata = [];
                var data = {};
                for (var i = 0; i < $scope.AllsubjectAttributedata.length; i++) {
                    for (var j = 0; j < $scope.AllsubjectAttributedata[i].stud_attr.length; j++) {
                        if ($scope.AllsubjectAttributedata[i].stud_attr[j].saveData == true) {                      
                            data = {
                                sims_cur_code: $scope.AllsubjectAttributedata[i].stud_attr[j].sims_cur_code,
                                sims_academic_year: $scope.AllsubjectAttributedata[i].stud_attr[j].sims_academic_year,
                                sims_grade_code: $scope.AllsubjectAttributedata[i].stud_attr[j].sims_grade_code,
                                sims_section_code: $scope.AllsubjectAttributedata[i].stud_attr[j].sims_section_code,
                                sims_gb_number: $scope.AllsubjectAttributedata[i].stud_attr[j].sims_gb_number,
                                sims_gb_cat_code: $scope.AllsubjectAttributedata[i].stud_attr[j].sims_gb_cat_code,
                                sims_gb_cat_assign_number: $scope.AllsubjectAttributedata[i].stud_attr[j].sims_gb_cat_assign_number,
                                sims_subject_attribute_group_code: $scope.AllsubjectAttributedata[i].stud_attr[j].sims_subject_attribute_group_code,
                                sims_subject_attribute_code: $scope.AllsubjectAttributedata[i].stud_attr[j].sims_attribute_code,
                                sims_subject_attribute_enroll_number: $scope.AllsubjectAttributedata[i].stud_attr[j].sims_enroll_number,
                                sims_gb_cat_assign_subject_attribute_mark: $scope.AllsubjectAttributedata[i].stud_attr[j].sims_student_assign_received_point,
                                sims_gb_cat_assign_subject_attribute_final_grade: '',
                                sims_gb_cat_assign_subject_attribute_max_point: $scope.AllsubjectAttributedata[i].stud_attr[j].sims_gb_cat_assign_max_point,
                                sims_gb_cat_assign_subject_attribute_max_correct: $scope.AllsubjectAttributedata[i].stud_attr[j].sims_gb_cat_assign_max_point,
                            sims_gb_cat_assign_subject_attribute_comment: $scope.AllsubjectAttributedata[i].sims_student_assign_comment,
                            sims_gb_cat_assign_subject_attribute_status: $scope.AllsubjectAttributedata[i].sims_student_assign_exam_status,
                            sims_report_card_config_code: $scope.AllsubjectAttributedata[i].stud_attr[j].sims_report_card_config_code,
                            sims_report_card_term_code: $scope.AllsubjectAttributedata[i].stud_attr[j].sims_report_card_term_code,
                        }
                            savedata.push(data);
                        }
                    }
                }

                console.log("$scope.AllTableData = ", savedata);



                $http.post(ENV.apiUrl + "api/MarksEntryNew/insertsubjectattribute?cTemp=X", savedata).then(function (res) {
                    if (res.data) {
                        //alert("Data Saved Successfully");
                        swal({ title: "Alert", text: "Marks Entered successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        $scope.resetchangeFlag();
                    } else {
                        swal({ title: "Alert", text: "Unable To Enter Marks", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                });
            }
        }

        $scope.getAllStatus = function () {
            var a = {
                opr: 'J'
            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", a).then(function (res) {
                if (res.data.table.length > 0) {
                    $scope.AllStatus = res.data.table;
                }
            });
        }

        $scope.EnDisMarks = function (index) {
            $scope.enableIsDirty(index);
            $scope.setchangeFlag();
            if ($scope.selectedTab == 0) {
                if ($scope.AllTableData[index].sims_student_assign_exam_status == 'P') {
                    $scope.AllTableData[index].sims_student_assign_received_point = 0;
                    $scope.AllTableData[index].sims_student_assign_final_grade = $scope.AllGradingScale[$scope.AllGradingScale.length - 1].grade_code;
                } else {
                    $scope.AllTableData[index].sims_student_assign_received_point = "";
                    $scope.AllTableData[index].sims_student_assign_final_grade = "";
                }
            }
            else if ($scope.selectedTab == 1) {
                if ($scope.AllCoScholasticTableData[index].sims_student_assign_exam_status == 'P') {
                    $scope.AllCoScholasticTableData[index].sims_report_card_co_scholastic_grade_point = "";
                    $scope.AllCoScholasticTableData[index].sims_co_scholastic_discriptive_indicator_grade_code = $scope.AllCoscholasticGradingScale[$scope.AllCoscholasticGradingScale.length - 1].sims_co_scholastic_grade_code;
                } else {
                    $scope.AllCoScholasticTableData[index].sims_report_card_co_scholastic_grade_point = "";
                    $scope.AllCoScholasticTableData[index].sims_co_scholastic_discriptive_indicator_grade_code = "";
                }
            }
        }

        $scope.EnDisMarks_subattr = function (index) {
            
            $scope.enableIsDirty_subattr(index);
            $scope.setchangeFlagAttributeStatus(index);
            if ($scope.selectedTab == 0) {
                if ($scope.AllsubjectAttributedata[index].sims_student_assign_exam_status == 'P') {
                    $scope.AllsubjectAttributedata[index].sims_student_assign_received_point = 0;
                    $scope.AllsubjectAttributedata[index].sims_student_assign_final_grade = $scope.AllGradingScale[$scope.AllGradingScale.length - 1].grade_code;
                } else {
                    for (var i = 0; i < $scope.AllsubjectAttributedata.length; i++) {
                        if (i == index) {
                        for (var j = 0; j < $scope.AllsubjectAttributedata[i].stud_attr.length; j++) {
                            $scope.AllsubjectAttributedata[i].stud_attr[j].sims_student_assign_received_point = "";
                            $scope.AllsubjectAttributedata[i].stud_attr[j].sims_student_assign_final_grade = "";
                        }
                    }
                    }

                }
            }
            else if ($scope.selectedTab == 1) {
                if ($scope.AllCoScholasticTableData[index].sims_student_assign_exam_status == 'P') {
                    $scope.AllCoScholasticTableData[index].sims_report_card_co_scholastic_grade_point = "";
                    $scope.AllCoScholasticTableData[index].sims_co_scholastic_discriptive_indicator_grade_code = $scope.AllCoscholasticGradingScale[$scope.AllCoscholasticGradingScale.length - 1].sims_co_scholastic_grade_code;
                } else {
                    $scope.AllCoScholasticTableData[index].sims_report_card_co_scholastic_grade_point = "";
                    $scope.AllCoScholasticTableData[index].sims_co_scholastic_discriptive_indicator_grade_code = "";
                }
            }
        }

        $scope.enableIsDirty = function (index) {
            if ($scope.selectedTab == 0) {
                $scope.AllTableData[index].isDirty = true;
            }
            else if ($scope.selectedTab == 1) {
                $scope.AllCoScholasticTableData[index].isDirty = true;
            }
            $scope.setchangeFlag();
        }

        $scope.enableIsDirty_subattr = function (index) {
            if ($scope.selectedTab == 0) {
                $scope.AllsubjectAttributedata[index].isDirty = true;
            }
            else if ($scope.selectedTab == 1) {
                $scope.AllCoScholasticTableData[index].isDirty = true;
            }
          //  $scope.setchangeFlagAttribute();
        }
        $scope.editMaxMarks = '';
        $scope.AllTableData = [];
        $scope.getTableData = function () {
            if ($scope.show_marks_entry_screen == 'A') {              
                var a = {
                    opr: 'L',
                    cur_code: $scope.cur_code,
                    aca_year: $scope.aca_year,
                    grade_code: $scope.sims_grade,
                    section_code: $scope.sims_section,
                    gradebook_number: $scope.sims_subject.gradebook_number,
                    subject_code: $scope.sims_subject.sims_subject_code
                };
                $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", a).then(function (res) {
                    if (res.data) {
                        $scope.AllTableData = res.data.table;
                        $scope.resetchangeFlag();
                        $($scope.AllTableData).each(function (key, val) {
                            val.sims_student_assign_exam_status = val.sims_student_assign_exam_status == undefined ? 'P' : val.sims_student_assign_exam_status;
                            val.sims_gb_cat_assign_max_point = $scope.sims_assignment.assign_max_score;
                            val.sims_student_assign_entered_by = val.sims_student_assign_entered_by == undefined ? $rootScope.globals.currentUser.username : val.sims_student_assign_entered_by;
                            val.sims_subject_srl_no = $scope.sims_subject.gradebook_number;
                            val.isDirty = false;
                        });
                    }
                });
            } else {
                $http.get(ENV.apiUrl + "api/MarksEntryNew/GetAttributeStudents?cur_code=" + $scope.cur_code + "&aca_year=" + $scope.aca_year + "&grade_code=" + $scope.sims_grade
                   + "&section_code=" + $scope.sims_section + "&subject_code=" + $scope.sims_subject.sims_subject_code + "&gradebook_number=" + $scope.sims_subject.gradebook_number
                   + "&sims_attribute_type=" + $scope.show_marks_entry_screen).then(function (data_all) {
                       $scope.AllsubjectAttributedata = data_all.data;
                       console.log("attr-", $scope.AllsubjectAttributedata);
                       for (var i = 0; i < $scope.AllsubjectAttributedata.length; i++) {
                            for (var j = 0; j < $scope.AllsubjectAttributedata[i].stud_attr.length; j++) {
                                $scope.AllsubjectAttributedata[i].stud_attr[j]['saveData'] = false;
                            }                          
                       }

                   });
            }
        }

        $scope.getSubjectConfigData = function () {
            $scope.getTableData();
            $scope.visibleDownloadbtn = true;
            $scope.visibleUploadbtn = true;
            $scope.singleTable = $scope.exam_marks == 'single' ? true : false;
        }

        $scope.getCoScholasticsTableData = function () {
            $scope.AllCoScholasticTableData = [];
            var a = {
                opr: 'P',
                cur_code: $scope.cur_code,
                aca_year: $scope.aca_year,
                grade_code: $scope.sims_grade,
                section_code: $scope.sims_section,
                gradebook_number: $scope.sims_indicator.gradebook_number
            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", a).then(function (res) {
                if (res.data) {
                    $scope.AllCoScholasticTableData = res.data.table;
                    console.log("$scope.AllCoScholasticTableData value is ", $scope.AllCoScholasticTableData);
                    $scope.resetchangeFlag();
                    $($scope.AllCoScholasticTableData).each(function (key, val) {
                        val.sims_student_assign_status = val.sims_student_assign_status == undefined ? 'P' : val.sims_student_assign_status;
                        val.sims_student_assign_entered_by = val.sims_student_assign_entered_by == undefined ? $rootScope.globals.currentUser.username : val.sims_student_assign_entered_by;
                        val.sims_subject_srl_no = $scope.sims_indicator.gradebook_number;
                        val.sims_student_assign_comment = val.sims_student_assign_comment == undefined ? "" : val.sims_student_assign_comment;
                        val.isDirty = false;
                    });
                }
            });
        }


        $scope.getCoscholasticsData = function () {
            $scope.getCoScholasticsTableData();
            $scope.ScholasticTable = true;
        }

        $scope.getSubjectChange = function () {
            
            $scope.teacherInfo = false;
            $scope.examDateInfo = false;
            var a = {
                opr: 'I',
                aca_year: $scope.aca_year,
                grade_code: $scope.sims_grade,
                section_code: $scope.sims_section,
                subject_code: $scope.sims_subject.sims_subject_code
            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", a).then(function (res) {
                if (res.data.table.length > 0) {
                    $scope.sims_subject_teacher = res.data.table[0].sims_teacher_name;
                    $scope.teacherInfo = true;
                }
            });
        }

        $scope.getIndicatorChange = function () {
            console.log("$scope.getIndicatorChange function");
            $scope.getCoscholasticGradingScale();
            $scope.cteacherInfo = false;
            $scope.examDateInfo = false;
            var a = {
                opr: 'R',
                aca_year: $scope.aca_year,
                cur_code: $scope.cur_code,
                grade_code: $scope.sims_grade,
                section_code: $scope.sims_section,
                ind_code: $scope.sims_indicator.indicator_code,
                att_code: $scope.sims_indicator.attribute_code,
                group_code: $scope.sims_indicator.co_group_code
            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", a).then(function (res) {
                if (res.data.table.length > 0) {
                    $scope.sims_subject_teacher = res.data.table[0].sims_teacher_name;
                    $scope.cteacherInfo = true;
                }
            });
        }

        $scope.setkExamMarks = function () {
            $scope.showDisplayExam = $scope.exam_marks == 'all' ? true : false;
        }

        $scope.setchangeFlag = function () {
            $scope.hasChanged = true;
            $scope.hasChangedSubattr = false;
        }

        $scope.setchangeFlagAttribute = function (str, point_entered) {
            
            str.saveData = true;
            $scope.hasChangedSubattr = true;
            $scope.hasChanged = false;
        }

        $scope.setchangeFlagAttributeStatus = function (index) {
         
            for (var i = 0; i < $scope.AllsubjectAttributedata.length; i++) {
                if (i == index) {
                    for (var j = 0; j < $scope.AllsubjectAttributedata[i].stud_attr.length; j++) {
                        $scope.AllsubjectAttributedata[i].stud_attr[j].saveData = true;
                    }
                }
            }
            $scope.hasChangedSubattr = true;
            $scope.hasChanged = false;
        }


        $scope.resetchangeFlag = function () {
            $scope.hasChanged = false;
            $scope.hasChangedSubattr = false;
        }

        $scope.selectMarksEntryLevel = function (e) {
            $("#marksEntryTab span").removeClass('btn-primary').addClass('btn-default');
            $(".tab-pane").removeClass('active').fadeOut();
            $(e.currentTarget).removeClass('btn-default').addClass('btn-primary');
            $scope.selectedTab = e.currentTarget.innerText == "Marks Entry" ? 0 : 1;
            console.log("selectedTab value is", $scope.selectedTab);
            var id = $(e.currentTarget).data('target');
            $(id).addClass('active').fadeIn();
        }


        $scope.selectMarksEntryLevelNew = function (e) {
            $("#marksEntryTab span").removeClass('btn-primary').addClass('btn-default');
            $(".tab-pane").removeClass('active').fadeOut();
            $(e.currentTarget).removeClass('btn-default').addClass('btn-primary');
            $scope.selectedTab = e.currentTarget.innerText == "Marks Entry" ? 0 : 1;
            console.log("selectedTab value is", $scope.selectedTab);
            var id = $(e.currentTarget).data('target');
            $(id).addClass('active').fadeIn();

            var a = {
                opr: 'A',
                cur_code: $scope.cur_code,
                aca_year: $scope.aca_year,
                user: $rootScope.globals.currentUser.username
            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", a).then(function (res) {
                if (res.data.table.length > 0) {
                    $scope.AllGrades = res.data.table;
                    $scope.sims_grade_new = $scope.AllGrades[0].sims_grade_code;
                    $scope.getGradeSectionNew();
                }
            });

        }

        $scope.getGradeSectionNew = function () {

            var a = {
                opr: 'B',
                cur_code: $scope.cur_code,
                aca_year: $scope.aca_year,
                grade_code: $scope.sims_grade_new,
                user: $rootScope.globals.currentUser.username
            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", a).then(function (res) {
                if (res.data.table.length > 0) {
                    $scope.AllSectionFromGrade_new = res.data.table;
                    $scope.sims_section_new = $scope.AllSectionFromGrade_new[0].sims_section_code;
                    $scope.getSectionChangenew();
                }
            });
        }

        $(document).ready(function () {
            $scope.hasChanged = false;
            $scope.Allcurriculum = [];
            $scope.cur_code;
            $scope.exam_marks = 'single';
            $scope.showDisplayExam = false;
            $scope.singleTable = false;
            $scope.ScholasticTable = false;
            $scope.teacherInfo = false;
            $scope.cTeacherInfo = false;
            $scope.examDateInfo = false;
            $scope.getCurriculum();
            $scope.condenseMenu();
            $scope.selectedTab = 0;
        });

        $scope.changeMaxAssignAttr = function (data, max_marks) {
           
            var a = {
                opr: 'Y',
                sims_cur_code: data.sims_cur_code,
                sims_academic_year: data.sims_academic_year,
                sims_grade_code: data.sims_grade_code,
                sims_section_code: data.sims_section_code,
                sims_gb_number: data.sims_gb_number,
                sims_gb_cat_code: data.sims_gb_cat_code,
                sims_gb_cat_assign_number: data.sims_gb_cat_assign_number,
                sims_subject_attribute_group_code: data.sims_subject_attribute_group_code,
                sims_subject_attribute_code: data.sims_attribute_code,
                sims_report_card_term_code: data.sims_report_card_term_code,
                sims_report_card_config_code: data.sims_report_card_config_code,
                sims_gb_cat_assign_max_point: data.sims_gb_cat_assign_max_point
            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", a).then(function (res) {
                if (res.data.table[0].row_count > 0) {
                    swal({ title: "Alert", text: "Max Marks Updated successfully.", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    $scope.getSubjectConfigData();
                } else {
                    swal({ title: "Alert", text: "Unable To Update Max Marks ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                }
            });
        }

        $scope.changeMaxAssign = function (data, max_marks) {
            
            var a = {
                opr: 'Z',
                sims_cur_code: $scope.cur_code,
                sims_academic_year: data.sims_academic_year,
                sims_report_card_config_code: data.sims_report_card_config_code,
                sims_report_card_term_code: data.sims_report_card_term_code,
                category_code: data.sims_report_card_category_code,
                sims_gb_cat_assign_number: data.assign_number,
                sims_gb_cat_assign_max_point: data.assign_max_score

            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/MarksEntryCommon", a).then(function (res) {
                if (res.data.table[0].row_count > 0) {
                    swal({ title: "Alert", text: "Max Marks Updated successfully.", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                } else {
                    swal({ title: "Alert", text: "Unable To Update Max Marks ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                }
            });
        }


        // my code
        $scope.getSectionChangenew = function () {
            var a = {
                opr: 'A',
                cur_code: $scope.cur_code,
                aca_year: $scope.aca_year,
                grade_code: $scope.sims_grade_new,
                section_code: $scope.sims_section_new
            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/all_combo", a).then(function (res) {
                if (res.data.table.length > 0) {
                    $scope.AllGradeMappedTerm = res.data.table;
                    $scope.sims_term = res.data.table[0];
                    $scope.getTermChangeNew();
                }
            });
        }

        $scope.getTermChangeNew = function () {
            var a = {
                opr: 'B',
                cur_code: $scope.cur_code,
                aca_year: $scope.aca_year,
                //grade_code: $scope.sims_grade_new,
                //section_code: $scope.sims_section_new
                sims_report_card_config_code: $scope.sims_term.sims_report_card_assign_number
            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/all_combo", a).then(function (res) {
                if (res.data.table.length > 0) {
                    $scope.AllMappedGroup = res.data.table;

                    $scope.sims_group = res.data.table[0].sims_co_scholastic_group_code;

                }
            });
        }

        $scope.getGroupChangeNew = function () {
            
            var a = {
                opr: 'C',
                cur_code: $scope.cur_code,
                aca_year: $scope.aca_year,
                //grade_code: $scope.sims_grade,
                //section_code: $scope.sims_section
                sims_report_card_config_code: $scope.sims_term.sims_report_card_assign_number,
                sims_co_scholastic_group_code: $scope.sims_group
            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/all_combo", a).then(function (res) {
                if (res.data.table.length > 0) {
                    $scope.AllMappedAttribute = res.data.table;
                }
            });
        }

        $scope.getAttributeChange = function () {
            
            var a = {
                opr: 'D',
                cur_code: $scope.cur_code,
                aca_year: $scope.aca_year,
                //grade_code: $scope.sims_grade,
                //section_code: $scope.sims_section
                sims_report_card_config_code: $scope.sims_term.sims_report_card_assign_number,
                sims_co_scholastic_group_code: $scope.sims_group,
                sims_co_scholastic_attribute_code: $scope.sims_assignment

            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/all_combo", a).then(function (res) {
                if (res.data.table.length > 0) {
                    $scope.AllMappedIndicators = res.data.table;
                }
            });
        }

        $scope.getIndicatorChange = function () {

        }

        $scope.getCoscholasticsDataNew = function () {
            $scope.ScholasticTable = true;
            $scope.assignment1 = [];
            $scope.Assignment = [];
            $scope.studentlist = [];
            $scope.studentlist1 = [];
            $scope.assign1 = [];
            $scope.assign = [];
            $scope.categories1 = [];
            $scope.count1 = 0;
            $scope.categories = [];
            $scope.student_assign = [];
            $scope.studentlist = [];
            $scope.count = 0;
            $scope.insertassignmentdata = [];
            $scope.AllGradeBookassignment = [];

            var a = {
               // opr: 'D',
                cur_code: $scope.cur_code,
                aca_year: $scope.aca_year,
                grade_code: $scope.sims_grade_new,
                section_code: $scope.sims_section_new,
                sims_report_card_config_code: $scope.sims_term.sims_report_card_assign_number,
                sims_co_scholastic_group_code: $scope.sims_group,
                sims_co_scholastic_attribute_code: $scope.sims_assignment,
                sims_co_scholastic_descriptive_indicator_code: $scope.sims_indicator,
                term_code: $scope.sims_term.sims_report_card_assign_number,
                flag: $scope.sims_term.flg,


            };
            $http.post(ENV.apiUrl + "api/MarksEntryNew/AllList", a).then(function (res) {
                console.log(res.data)

                $scope.AllGradeBookassignment = res.data;
           


//            var data1 = {
//                academic_year: "2019"
//, adminUser: false
//, attributes: [{
//    indicators: [{ sims_co_scholastic_descriptive_indicator_master_code: "1", sims_co_scholastic_descriptive_indicator_name: "Indicator1" }, { sims_co_scholastic_descriptive_indicator_master_code: "2", sims_co_scholastic_descriptive_indicator_name: "Indicator2" }]
//, sims_co_scholastic_attribute_master_code: "1"
//, sims_co_scholastic_attribute_name: "Attribute1"
//}, {
//    indicators: [{ sims_co_scholastic_descriptive_indicator_master_code: "3", sims_co_scholastic_descriptive_indicator_name: "Indicator1" }, { sims_co_scholastic_descriptive_indicator_master_code: "4", sims_co_scholastic_descriptive_indicator_name: "Indicator2" }]
//, sims_co_scholastic_attribute_master_code: "2"
//, sims_co_scholastic_attribute_name: "Attribute2"
//}]

//, sims_co_scholastic_group_name: "Master Category 1 / Group Name"
//, sims_subject_name: "English"
//            }

//            var data2 = [{
               
//                 stud_Enroll: "2438"
//                , stud_Full_Name: 'ABC'
//                , indicators: [{ sims_co_scholastic_descriptive_indicator_master_code: '1' }, { sims_co_scholastic_descriptive_indicator_master_code: '2' }, { sims_co_scholastic_descriptive_indicator_master_code: '3' }, { sims_co_scholastic_descriptive_indicator_master_code: '4' }]
                
//            }, {
              
//                     stud_Enroll: "2439"
//                , stud_Full_Name: 'sdsd'
//                , indicators: [{ sims_co_scholastic_descriptive_indicator_master_code: '1' }, { sims_co_scholastic_descriptive_indicator_master_code: '2' }, { sims_co_scholastic_descriptive_indicator_master_code: '3' }, { sims_co_scholastic_descriptive_indicator_master_code: '4' }]
//            }]

         //   $scope.AllGradeBookassignment.push(data1)
                // $scope.AllGradeBookassignment.push(data2)



            $scope.busy2 = false;
            if ($scope.AllGradeBookassignment[0].attributes == undefined) {
                $scope.AllGradeBookassignment[0].attributes = [];
            }
            for (var j = 0; j < $scope.AllGradeBookassignment[0].attributes.length; j++) {

                if ($scope.AllGradeBookassignment[0].attributes[j].indicators == undefined) {
                    $scope.AllGradeBookassignment[0].attributes[j].indicators = [];
                }
                $scope.count1 = parseInt($scope.count1) + 1;
                $scope.categories1.push($scope.AllGradeBookassignment[0].attributes[j]);
                for (var k = 0; k < $scope.AllGradeBookassignment[0].attributes[j].indicators.length; k++) {
                    $scope.count = parseInt($scope.count) + 1;
                    $scope.AllGradeBookassignment[0].attributes[j].count1 = parseInt($scope.AllGradeBookassignment[0].attributes[j].indicators.length);
                    $scope.assignment1.push($scope.AllGradeBookassignment[0].attributes[j].indicators[k]);
                }
            }

            $scope.Assignment = angular.copy($scope.assignment1);
            $scope.categories = angular.copy($scope.categories1);
            $scope.student_assign = angular.copy($scope.AllGradeBookassignment[1]);

            $timeout(function () {

                $("#fixTable").tableHeadFixer({ 'top': 1, 'left': 2, 'z-index': 10000 });
                //  $("#fixTable").tableHeadFixer({ 'top': 1 });

            }, 3000);

            });
        }


        $scope.Save_coscholastic = function () {
            var data_lst=[];

            for (var i = 0; i < $scope.student_assign.length; i++) {

                for (var j = 0; j < $scope.student_assign[i].indicators.length; j++) {
                    
                    if ($scope.student_assign[i].indicators[j]['is_changed']) {
                        var data = {
                            aca_year: $scope.aca_year,
                            cur_code: $scope.cur_code,
                            sims_subject_srl_no: $scope.student_assign[i].indicators[j].sims_co_scholastic_cdi_srl_no,
                            sims_co_scholastic_enroll_number: $scope.student_assign[i].stud_Enroll,
                            sims_co_scholastic_discriptive_indicator_grade_code: $scope.student_assign[i].indicators[j].sims_co_scholastic_discriptive_indicator_grade_code,
                            sims_co_scholastic_discriptive_indicator_grade: $scope.student_assign[i].indicators[j].sims_co_scholastic_descriptive_indicator_grade_scale,
                            sims_report_card_co_scholastic_grade_point: $scope.student_assign[i].indicators[j].sims_report_card_co_scholastic_grade_point,
                            sims_student_assign_entered_by: $rootScope.globals.currentUser.username

                        }

                        data_lst.push(data);
                    }

                }


            }

            $http.post(ENV.apiUrl + "api/MarksEntryNew/insertsubjectattribute", data_lst).then(function (res) {
                if (res.data) {
                    swal({ title: "Alert", text: "Marks Entered successfully.", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    $scope.getCoscholasticsDataNew();
                } else {
                    swal({ title: "Alert", text: "Marks Not Entered", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                }
            });

        }

        $scope.mark_change = function (str) {
            
            if(str.sims_report_card_co_scholastic_grade_point=='' || str.sims_report_card_co_scholastic_grade_point==undefined){
            }
            else
                str['is_changed'] = true;

            if (parseFloat( str.sims_report_card_co_scholastic_grade_point) > parseFloat( str.sims_co_scholastic_descriptive_indicator_mark)) {
                str.sims_report_card_co_scholastic_grade_point = '';
            }
        
        }

        $scope.grade_change = function (str) {
            
            if (str.sims_co_scholastic_discriptive_indicator_grade_code == '' || str.sims_co_scholastic_discriptive_indicator_grade_code == undefined) {
            }
            else
                str['is_changed'] = true;
        }

      

///////////////////////// Import Export code (shahaji) //////////////////////////////
        //// Import start  /////////
        $scope.showPreview = false;
        $scope.showJSONPreview = true;
        $scope.json_string = "";

        $scope.filesize = false;
        $scope.sheets = [];


        var formdata = new FormData();
        $scope.getTheFiles = function ($files) {

            $scope.flag = 0;
            angular.forEach($files, function (value, key) {
                formdata.append(key, value);
                var i = 0;
            });
        };
        $scope.file_changed = function (element) {
            var photofile = element.files[0];
            $scope.filename = element.files[0];
            $scope.photo_filename = (photofile.type);
            var reader = new FileReader();
            reader.onload = function (e) {
                $scope.$apply(function () {
                    $scope.prev_img = e.target.result;
                });
            };
            reader.readAsDataURL(photofile);
            var f = document.getElementById('file1').files[0];
            $scope.sheets = [];
            $scope.excelFile = element.files[0];
            XLSXReaderService.readFile($scope.excelFile,false,true).then(function (xlsxData) {
                $scope.sheets = xlsxData.sheets
                if ($scope.show_marks_entry_screen == 'A') {
                    
                    angular.forEach($scope.sheets.report, function (value, key) {
                        angular.forEach($scope.AllTableData, function (val, k) {
                            if (value.Enroll_No == val.sims_enroll_number && value.Marks_obtained != undefined) {
                                if (parseInt(value.Marks_obtained) > parseInt($scope.sims_assignment.assign_max_score)) {
                                    alert("Please enter correct Marks between 0 and " + $scope.sims_assignment.assign_max_score);
                                    val.sims_student_assign_received_point = "0";
                                } else {
                                    val.sims_student_assign_received_point = value.Marks_obtained;
                                    if (val.sims_student_assign_received_point != undefined) {
                                        $scope.findGrade(k);
                                    }                                    
                                    $scope.hasChanged = true;
                                    $scope.hasChangedSubattr = false;
                                }                                
                            }
                        })
                    })
                    angular.forEach(
                    angular.element("input[type='file']"),
                    function (inputElem) {
                        angular.element(inputElem).val(null);
                    });
                } else {
                    $scope.spl=[]
                    angular.forEach($scope.sheets.report, function (value, key) {
                        angular.forEach(value, function (val, k) {
                            angular.forEach($scope.AllsubjectAttributedata, function (v2, k2) {
                                angular.forEach($scope.AllsubjectAttributedata[key].stud_attr, function (v1, k1) {
                                    $scope.s = k.split("(");
                                    if ($scope.s[0] == v1.sims_attribute_name && v2.sims_enroll_number == v1.sims_enroll_number) {
                                        if(parseInt(val)>parseInt(v1.sims_gb_cat_assign_max_point)){
                                            v1.sims_student_assign_received_point = '0';                                        
                                        }else{
                                            v1.sims_student_assign_received_point = val
                                            if (v1.sims_student_assign_received_point != undefined) {
                                                v1.saveData = true;
                                            }                                            
                                            $scope.hasChangedSubattr = true;
                                        }
                                    }
                                });
                            });
                        });
                    });
                    angular.forEach(
                   angular.element("input[type='file']"),
                   function (inputElem) {
                       angular.element(inputElem).val(null);
                   });
                }
            });

            $scope.fname = $scope.cur_code + $scope.aca_year + $scope.sims_grade + $scope.sims_section + $scope.sims_term + $scope.sims_category.category_code + $scope.sims_assignment.assign_grade_scale + $scope.sims_subject.sims_subject_code + "report_config_code_" + $scope.sims_assignment.sims_report_card_config_code + $scope.exam_marks;
            var request = {
                method: 'POST',
                url: ENV.apiUrl + '/api/file/uploadDocument?filename=' + $scope.fname + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss') + "&location=" + "Docs/MarksEntry",
                data: formdata,
                headers: {
                    'Content-Type': undefined
                }
            };
            $http(request).success(function (d) {
                //alert(d);
                console.log(d);
                // swal({ title: "Document Uploaded Successfully", imageUrl: "assets/img/check.png", });
            });
        };

        $scope.cancelClick = function (cur) {
            $scope.AllTableData = $scope.AllTableDatanew;
            angular.forEach(
            angular.element("input[type='file']"),
            function (inputElem) {
                angular.element(inputElem).val(null);
            });
        }
        $scope.importdata = function () {
            formdata = new FormData();
        }
        /////  Import end   ///////
        /////  Export Excel ///////
        $scope.exportdata = function () {
            console.log($scope.AllTableData)
            // Prepare Excel data:
            $scope.fileName = "report";
            $scope.exportData = [];

            // Headers:
            if ($scope.show_marks_entry_screen == 'A') {
                $scope.exportData.push(["Enroll_No", "Student_Name", "Marks_obtained"]);

                angular.forEach($scope.AllTableData, function (value, key) {
                    $scope.exportData.push([
                           value.sims_enroll_number,
                           value.student_name,
                           value.sims_student_assign_received_point,
                           $scope.sims_assignment.assign_max_score
                    ]);
                })
                $scope.exportexcel($scope.exportData)
            } else {
                $scope.exportData.push(["Enroll_No", "Student_Name"]);
                angular.forEach($scope.AllsubjectAttributedata[0].stud_attr, function (value, key) {
                    $scope.exportData[0].push(value.sims_attribute_name+'(Max-'+value.sims_gb_cat_assign_max_point+')');
                });
                console.log($scope.AllsubjectAttributedata);
                angular.forEach($scope.AllsubjectAttributedata, function (value, key) {
                    $scope.exportData.push([
                           value.sims_enroll_number,
                           value.student_name,                           
                    ]);
                    angular.forEach($scope.AllsubjectAttributedata[key].stud_attr, function (val, k) {
                        $scope.exportData[key+1].push(val.sims_student_assign_received_point);
                    });
                })
                $scope.exportexcel($scope.exportData)
            }       
        }
        /// export end //
    

        $scope.exportexcel = function (exdata) {
            function datenum(v, date1904) {
                if (date1904) v += 1462;
                var epoch = Date.parse(v);
                return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
            };

            function getSheet(data, opts) {
                var ws = {};
                var range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
                for (var R = 0; R != data.length; ++R) {
                    for (var C = 0; C != data[R].length; ++C) {
                        if (range.s.r > R) range.s.r = R;
                        if (range.s.c > C) range.s.c = C;
                        if (range.e.r < R) range.e.r = R;
                        if (range.e.c < C) range.e.c = C;
                        var cell = { v: data[R][C] };
                        if (cell.v == null) continue;
                        var cell_ref = XLSX.utils.encode_cell({ c: C, r: R });

                        if (typeof cell.v === 'number') cell.t = 'n';
                        else if (typeof cell.v === 'boolean') cell.t = 'b';
                        else if (cell.v instanceof Date) {
                            cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                            cell.v = datenum(cell.v);
                        }
                        else cell.t = 's';

                        ws[cell_ref] = cell;
                    }
                }
                if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
                return ws;
            };

            function Workbook() {
                if (!(this instanceof Workbook)) return new Workbook();
                this.SheetNames = [];
                this.Sheets = {};
            }
            var wb = new Workbook(), ws = getSheet(exdata);
            /* add worksheet to workbook */
            wb.SheetNames.push($scope.fileName);
            wb.Sheets[$scope.fileName] = ws;
            var wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });
            function s2ab(s) {
                var buf = new ArrayBuffer(s.length);
                var view = new Uint8Array(buf);
                for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                return buf;
            }
            saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), $scope.fileName + '.xlsx');
        };
//////////////////////////////////////// END//////////////////////////////////////////

    }]);
    simsController.directive('ngFiles', ['$parse', '$scope', function ($parse, $scope) {
        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])


    simsController.factory("XLSXReaderService", ['$q', '$rootScope',
    function ($q, $rootScope) {
        var service = function (data) {
            angular.extend(this, data);
        }

        service.readFile = function (file, readCells, toJSON) {
            var deferred = $q.defer();

            XLSXReader(file, readCells, toJSON, function (data) {
                $rootScope.$apply(function () {
                    deferred.resolve(data);
                });
            });

            return deferred.promise;
        }


        return service;
    }
    ])
    

})();