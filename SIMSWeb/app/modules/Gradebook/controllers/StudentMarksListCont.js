﻿(function () {
    'use strict';
    var teacher_code = [];
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('StudentMarksListCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var user = $rootScope.globals.currentUser.username;
            var appl_code = "sim070";
            $scope.fcommn = [];
            $scope.pagesize = 'All';
            $scope.pageindex = "0";
            $scope.pager = false;
            $scope.toppager = false;
            $scope.busyindicator = true;
            $scope.showtable = false;
            var sub_head = '';
            var effort_head = '';
            var general_head = '';
            $scope.save_all_data = [];
            $scope.stud_lst = [];
            $scope.busy = false;
            $scope.exporttable = false;

            $http.get(ENV.apiUrl + "api/Gradebook/GetadminuserType?username=" + user + "&appl_code=" + appl_code).then(function (getAdmin) {
                $scope.getadmin = getAdmin.data;

            });
            debugger
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                debugger
                $scope.curriculum = res.data;
                $scope.edt = {
                    cur_code: $scope.curriculum[0].sims_cur_code
                }
                $scope.fcommn['cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr1($scope.curriculum[0].sims_cur_code);
            });

            $http.get(ENV.apiUrl + "api/MarksList/GetStatus").then(function (res) {
                debugger
                $scope.status_data = res.data;
                $scope.fcommn['sims_student_status'] = $scope.status_data[0].code;
            });

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            setTimeout(function () {
                $("#rcbItem").select2();

            }, 100);

            var username = $rootScope.globals.currentUser.username;
            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['chk_all'] = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                debugger;
                if ($scope.pagesize == 'All') {
                    $scope.currentPage = 1;
                    $scope.showPager = false;
                    $scope.filteredTodos = $scope.st_lst1;
                }
                else {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                }
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.student_list, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.student_list;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.sComment = {};

            $scope.getacyr1 = function (str) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.getAcademicYear1 = Academicyear.data; //academic_year
                    $scope.fcommn['academic_year'] = $scope.getAcademicYear1[0].sims_academic_year;
                    $scope.getsections2($scope.fcommn.cur_code, $scope.getAcademicYear1[0].sims_academic_year);
                });
            }

            $scope.getsection2 = function (str, str1, str2) {
                $http.get(ENV.apiUrl + "api/Gradebook/GetRCSection?curcode=" + str + "&ayear=" + str1 + "&gradecode=" + str2 + "&teachercode=" + user).then(function (Allsection) {
                    $scope.getSectionFromGrade1 = Allsection.data;
                    $scope.fcommn['sims_section_code'] = $scope.getSectionFromGrade1[0].sims_section_code;
                    $scope.Getterm2(str, str1, str2)

                })
            };

            $scope.getsections2 = function (str, str1) {

                $http.get(ENV.apiUrl + "api/Gradebook/GetRCGrade?curcode=" + str + "&ayear=" + str1 + "&teachercode=" + user).then(function (res) {
                    $scope.getAllGrades1 = res.data;
                    $scope.fcommn['sims_grade_code'] = $scope.getAllGrades1[0].sims_grade_code;
                    $scope.getsection2(str, str1, $scope.getAllGrades1[0].sims_grade_code);
                })
            };

            $scope.Getterm2 = function (cur_code, oldval, grade) {
                debugger
                //$http.get(ENV.apiUrl + "api/Gradebook/getTermforGradebook?cur_code=" + cur_code + "&academic_year=" + oldval).then(function (Getsims550_TermsAcademicYear) {
                //    $scope.TermsAcademicYear1 = Getsims550_TermsAcademicYear.data;
                //    $scope.fcommn['sims_term_code'] = $scope.TermsAcademicYear1[0].sims_term_code;
                //});
                $http.get(ENV.apiUrl + "api/Gradebook/getTermforGradebook?cur_code=" + $scope.fcommn.cur_code + "&academic_year=" + oldval).then(function (Getsims550_TermsAcademicYear) {
                    $scope.TermsAcademicYear1 = Getsims550_TermsAcademicYear.data;

                    $scope.fcommn['sims_term_code'] = $scope.TermsAcademicYear1[0].sims_term_code;

                    $scope.GetSubject($scope.fcommn.cur_code, $scope.fcommn.academic_year, $scope.fcommn.sims_grade_code, $scope.fcommn.sims_section_code, $scope.fcommn.sims_term_code);
                });

            }

            $scope.GetSubject = function (cur_code, acad, grade,section,term) {
                debugger
                $http.get(ENV.apiUrl + "api/MarksList/GetSubjects?cur_code=" + $scope.fcommn.cur_code + "&acad_year=" + $scope.fcommn.academic_year + "&grade_code=" + $scope.fcommn.sims_grade_code
                   + "&section=" + $scope.fcommn.sims_section_code + "&term=" + $scope.fcommn.sims_term_code + "&user=" + username).then(function (Getsims550_TermsAcademicYear) {
                    $scope.subject_list = Getsims550_TermsAcademicYear.data;

                    $scope.fcommn['sims_subject_code'] = $scope.subject_list[0].sims_gb_number;

                    $scope.GetCategories($scope.fcommn.cur_code, $scope.fcommn.academic_year, $scope.fcommn.sims_grade_code, $scope.fcommn.sims_section_code, $scope.fcommn.sims_subject_code);
                });

            }

            $scope.GetCategories = function (cur_code, acad, grade, section, category) {
                debugger
                $http.get(ENV.apiUrl + "api/MarksList/GetCategory?cur_code=" + $scope.fcommn.cur_code + "&acad_year=" + $scope.fcommn.academic_year + "&grade_code=" + $scope.fcommn.sims_grade_code
                   + "&section=" + $scope.fcommn.sims_section_code + "&sims_gb_number=" + $scope.fcommn.sims_subject_code).then(function (Getsims550_TermsAcademicYear) {
                       $scope.category_list = Getsims550_TermsAcademicYear.data;
                       $scope.fcommn['sims_category_code'] = $scope.category_list[0].sims_gb_number + $scope.category_list[0].sims_gb_cat_code;

                       setTimeout(function () {
                           $('#category_id').change(function () {
                               console.log($(this).val());
                           }).multipleSelect({
                               width: '100%',
                               filter: true
                           });
                          // $("#category_id").multipleSelect("checkAll");
                       }, 1000);
                   });

            }

            $scope.CheckAllChecked = function () {
                debugger;
                $scope.save_all_data = [];
                var main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.fail_student_list.length; i++) {
                        if ($scope.fail_student_list[i].marks_needed_to_pass > 0) {
                            var v = document.getElementById($scope.fail_student_list[i].sims_gb_cat_assign_enroll_number + i);
                            v.checked = true;
                            $('tr').addClass("row_selected");
                            $scope.save_all_data.push($scope.fail_student_list[i]);
                        }
                        else {
                            var v = document.getElementById($scope.fail_student_list[i].sims_gb_cat_assign_enroll_number + i);
                            v.checked = false;
                            $scope.row1 = '';
                            $('tr').removeClass("row_selected");
                        }
                    }
                }
                else {

                    for (var i = 0; i < $scope.fail_student_list.length; i++) {
                        var v = document.getElementById($scope.fail_student_list[i].sims_gb_cat_assign_enroll_number + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                        $scope.save_all_data = [];
                    }
                }

                console.log("$scope.save_data", $scope.save_all_data);
            }


            $http.get(ENV.apiUrl + "api/GracingMarksTrans/getActivity").then(function (res) {
                $scope.activityData = res.data;
            });

            $scope.sortBy = function (propertyName) {
                debugger
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.show_students_marks = function (cur_code, academic_year, grade, section, gb_number,status) {
                debugger
                $scope.busyindicator = false;
                $scope.stud_lst = [];
                $http.get(ENV.apiUrl + "api/MarksList/GetStudentsList?cur_code=" + $scope.fcommn.cur_code + "&acad_year=" + $scope.fcommn.academic_year + "&grade_code=" + $scope.fcommn.sims_grade_code
                   + "&section=" + $scope.fcommn.sims_section_code + "&sims_gb_number=" + $scope.fcommn.sims_category_code + "&status=" + $scope.fcommn.sims_student_status).then(function (data_all) {
                       $scope.fail_student_list = data_all.data;

                       // $scope.colspans = $scope.fail_student_list[1].cnt_assign;
                       for (var i = 0; i < $scope.fail_student_list[0].length; i++) {
                           var count = 0;
                           for (var j = 0; j < $scope.fail_student_list[1].length; j++) {
                               if ($scope.fail_student_list[0][i].sims_gb_cat_code == $scope.fail_student_list[1][j].sims_gb_cat_code) {
                               count = count + 1;
                               $scope.fail_student_list[0][i].total_assignmet_count = count ;
                           }
                           }
                       }
                       $scope.busyindicator = true;
                       $timeout(function () {
                           $("#fixTable").tableHeadFixer({ 'top': 1, 'left': 2, 'z-index': 10000 });
                           //  $("#fixTable").tableHeadFixer({ 'top': 1 });

                       }, 3000);

                });
            }

            $scope.exportData = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('pdf_print').innerHTML],
                         {
                             type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                         });
                        saveAs(blob, "Marks_List.xls");

                    }

                });

            };

            $scope.print = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var docHead = document.head.outerHTML;
                        var printContents = document.getElementById('pdf_print').outerHTML;
                        var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                        var newWin = window.open("", "_blank", winAttr);
                        var writeDoc = newWin.document;
                        writeDoc.open();
                        writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                        writeDoc.close();
                        newWin.focus();
                    }
                });
            };
        }]);
})();