﻿(function () {
    'use strict';
    angular.module('sims.module.StudentActivity', [
        'sims',
        'bw.paging',
        'gettext'
    ]);
})();