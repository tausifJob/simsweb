﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var comp_code;
    var finance_year;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.StudentActivity');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('MinutesofMeetingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = true;
            $scope.table = true;
            $scope.edt = [];
            $scope.temp = [];
            $scope.info = [];
            $scope.attendeesList = [];
            $scope.pointstable = true;
            $scope.tableshow = true;
            $scope.errorshow = true;
            $scope.addshow = false;
            var username = $rootScope.globals.currentUser.username;

            $scope.attendees = false;
            var date = new Date();
            var month = (date.getMonth() + 1);
            var day = date.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            $scope.td = (day) + '-' + (month) + '-' + date.getFullYear();
            $scope.temp['sims_mom_actual_start_date'] = $scope.td;



            $scope.viewCancel = function () {
                $scope.table = true;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.getdata = function () {
                $http.get(ENV.apiUrl + "api/meeting/getMeetings?empcode=" + username + "&momstatus=S").then(function (res) {
                    $scope.records = res.data;
                  
                    if ($scope.records.length > 0) {
                        $scope.tableshow = true;
                        $scope.errorshow = true;
                      
                        $scope.totalItems = $scope.records.length;
                        $scope.todos = $scope.records;
                        $scope.makeTodos();
                    } else {
                        $scope.errorshow = false;
                        $scope.errmsg = "Records Not Found"
                    }

                });
            }

            $scope.getdata();

            $scope.back = function () {
                $scope.tableshow = true;
                $scope.pointstable = true;
            }

            $scope.info['sims_mom_receipient_attendance'] = true;

            $scope.rowClick = function (currec) {

                $scope.temp['sims_mom_description'] = "";
                $scope.temp['sims_mom_actual_start_date'] = currec.sims_mom_date
                $scope.temp['sims_mom_actual_start_time'] =currec.sims_mom_start_time
                $scope.temp['sims_mom_actual_end_time'] = currec.sims_mom_end_time
                $scope.currRecord = currec;
                $scope.tableshow = false;
                $http.get(ENV.apiUrl + "api/meeting/getAttendees?sims_mom_number=" + $scope.currRecord.sims_mom_number).then(function (res) {
                    $scope.attendees = res.data;
                   
                });
            }

            $scope.checktime = function (min, max) {
               
                $scope.check_time_flag = true;
                if (min >= max) {
                    swal({ title: "Alert", text: "Please Select Correct Time" });
                    $scope.temp['sims_mom_actual_end_time'] = "";
                    $scope.check_time_flag = false;
                }
            }

            $('.clockpicker').clockpicker({
                autoclose: true
            });

            $scope.markAttendance = function () {
                $scope.succmsg = "";
                $scope.ermsg = "";
                $('#viewModal').modal({ backdrop: "static" });
            }

            var datasend = [];

            $scope.getmark = function (curratt) {
              
                $scope.currAtt = curratt;
                var obj = {
                    opr: 'Q',
                    sims_mom_number: $scope.currAtt.sims_mom_number,
                    sims_mom_receipient_attendance: $scope.currAtt.sims_mom_receipient_attendance,
                    sims_mom_chairperson_user_code: $scope.currAtt.sims_mom_chairperson_user_code
                }
                datasend.push(obj)
            }

            $scope.addAttendees = function () {
                $http.post(ENV.apiUrl + "api/meeting/CUDMarkAttendance", datasend).then(function (msg1) {
                    $scope.result1 = msg1.data;
                    if ($scope.result1) {
                        $scope.ermsg = "";
                        $scope.succmsg = "Attendance Marked successfully";
                    } else {
                        $scope.succmsg = "";
                        $scope.ermsg = "Attendance Not Marked";
                    }
                });

            }

            $scope.raisedChange = function (curraiseusr, currname) {
               
                $scope.info['sims_mom_raised_user_name'] = $('#raised_user').find("option:selected").text()
                $scope.info['sims_mom_raised_user_code'] = curraiseusr;
            }

            $scope.doneChange = function (curdoneusr) {
               
                $scope.info['sims_mom_tobedone_user_code'] = curdoneusr;
                $scope.info['sims_mom_tobedone_user_name'] = $('#tobedone_user').find("option:selected").text()

            }


            $scope.actualdate = function (actualdate) {
                //var date = new Date(actualdate);
                //var month = (date.getMonth() + 1);
                //var day = date.getDate();
                //if (month < 10)
                //    month = "0" + month;
                //if (day < 10)
                //    day = "0" + day;
                //$scope.td = (day) + '-' + (month) + '-' + date.getFullYear();
                //console.log($scope.td);
                $scope.temp['sims_mom_actual_start_date'] = actualdate;//$scope.td;
            }

            $scope.tdate = function (t_date) {
                //var date = new Date(t_date);
                //var month = (date.getMonth() + 1);
                //var day = date.getDate();
                //if (month < 10)
                //    month = "0" + month;
                //if (day < 10)
                //    day = "0" + day;
                //$scope.td = (day) + '-' + (month) + '-' + date.getFullYear();
                //console.log($scope.td);
                $scope.temp['donedate'] = t_date; //$scope.td;
            }

            $scope.meetingpoints = [];
            var flag = true;

            $scope.addClick = function (currdata, temp) {
               
                if (temp.sims_mom_description != "") {
                    if ($scope.info['sims_mom_raised_user_code'] != undefined && $scope.info['sims_mom_tobedone_user_code'] != undefined) {
                        if ($scope.temp['donedate'] != undefined) {
                            var data = {
                                opr: 'R',
                                sims_mom_number: currdata.sims_mom_number,
                                sims_mom_type_code: currdata.sims_mom_type_code,
                                sims_mom_description: temp.sims_mom_description,
                                sims_mom_raised_user_code: $scope.info['sims_mom_raised_user_code'],
                                sims_mom_tobedone_user_code: $scope.info['sims_mom_tobedone_user_code'],
                                sims_mom_tobedone_date: $scope.temp['donedate']
                            }
                            $http.post(ENV.apiUrl + "api/meeting/CUDInsertMinMeeting", data).then(function (msg) {
                                $scope.result = msg.data;
                                if ($scope.result==true) {
                                    $scope.ermsg = "";
                                    swal('', 'Record Added Successfully');
                                    $scope.temp['sims_mom_description'] = "";
                                    $scope.edt['sims_mom_raised_user_code'] = "";
                                    $scope.edt['sims_mom_tobedone_user_code'] = "";
                                    $scope.temp['sims_mom_tobedone_date'] = "";
                                    $http.get(ENV.apiUrl + "api/meeting/getMeetingsPoints?momnumber=" + $scope.currRecord.sims_mom_number).then(function (res) {
                                        $scope.meetingpoints = res.data;
                                        $scope.pointstable = false;
                                       
                                    });
                                } else if ($scope.result == false) {
                                    $scope.succmsg = "";
                                    swal('', 'Record Not Added. ' );
                                }
                                else {
                                    swal("Error-" + $scope.result)
                                }
                            });
                        } else {
                            swal('', 'Please Select Deadline Date');
                        }
                    } else {
                        swal('', 'Please Select User');
                    }
                } else {
                    swal('', 'Please Insert Discussed points');
                }
            }

            $scope.datetimeupdate = function (currdata) {
              
                var data = {
                    opr: 'K',
                    sims_mom_number: $scope.currRecord.sims_mom_number,
                    sims_mom_actual_start_date: currdata.sims_mom_actual_start_date,
                    sims_mom_actual_start_time: currdata.sims_mom_actual_start_time,
                    sims_mom_actual_end_time: currdata.sims_mom_actual_end_time
                }
                if (data.sims_mom_actual_start_date != undefined && data.sims_mom_actual_start_time != undefined && data.sims_mom_actual_end_time != undefined) {
                    $http.post(ENV.apiUrl + "api/meeting/CUDInsertMinMeeting", data).then(function (res) {
                        $scope.res = res.data;
                      
                        if ($scope.res) {
                            swal('', 'Meeting Date and time set Successfully');
                        } else {
                            swal('', 'Meeting Date and time not set');

                        }
                    });
                } else {
                    swal('', 'Enter date and time ');
                }
            }

            $scope.removeUser = function (curr, i) {
                //                console.log(i);
               
                var data = {
                    opr: 'Z',
                    sims_mom_number: curr.sims_mom_number,
                    sims_mom_srl_no: curr.sims_mom_srl_no
                }
               
                //                $scope.meetingpoints.push(data);
                $http.post(ENV.apiUrl + "api/meeting/CUDInsertMinMeeting", data).then(function (msg) {
                    $scope.result = msg.data;
                    if ($scope.result==true) {
                        swal('', 'Record Deleted Successfully');
                        $scope.meetingpoints.splice(i, 1);
                    } else if ($scope.result == false) {
                        swal('', 'Record Not Deleted. ' );
                    }
                    else {
                        swal("Error-" + $scope.result)
                    }
                });


            }

            $scope.edit = function (currdata) {
              
                $scope.addshow = true;
                $scope.sims_mom_number = currdata.sims_mom_number;
                $scope.sims_mom_srl_no = currdata.sims_mom_srl_no;
                $scope.temp['sims_mom_description'] = currdata.sims_mom_description;
                $scope.edt['sims_mom_raised_user_code'] = currdata.sims_mom_raised_user_code;
                $scope.edt['sims_mom_tobedone_user_code'] = currdata.sims_mom_tobedone_user_code;
                $scope.temp['sims_mom_tobedone_date'] = $scope.temp['donedate'];
            }

            //Select Data SHOW
            $scope.updateClick = function () {
                $scope.addshow = false;
                if ($scope.temp['sims_mom_description'] != "") {
                    if ($scope.edt['sims_mom_raised_user_code'] != undefined && $scope.edt['sims_mom_tobedone_user_code'] != undefined) {
                        if ($scope.temp['sims_mom_tobedone_date'] != undefined) {
                            var obj = {
                                opr: 'G',
                                sims_mom_number: $scope.sims_mom_number,
                                sims_mom_srl_no: $scope.sims_mom_srl_no,
                                sims_mom_description: $scope.temp['sims_mom_description'],
                                sims_mom_raised_user_code: $scope.edt['sims_mom_raised_user_code'],
                                sims_mom_tobedone_user_code: $scope.edt['sims_mom_tobedone_user_code'],
                                sims_mom_tobedone_date: $scope.temp['sims_mom_tobedone_date']
                            }
                            $http.post(ENV.apiUrl + "api/meeting/CUDInsertMinMeeting", obj).then(function (msg) {
                                $scope.result = msg.data;
                                if ($scope.result==true) {
                                    $scope.ermsg = "";
                                    swal('', 'Record Updated Successfully');
                                    $http.get(ENV.apiUrl + "api/meeting/getMeetingsPoints?momnumber=" + $scope.currRecord.sims_mom_number).then(function (res) {
                                        $scope.meetingpoints = res.data;
                                        $scope.pointstable = false;
                                        $scope.temp = [];
                                        $scope.edt = [];
                                      
                                    });
                                } else if ($scope.result == false) {
                                    $scope.succmsg = "";
                                    swal('', 'Record Not Updated. ' );
                                }
                                else {
                                    swal("Error-" + $scope.result)
                                }
                            });

                        } else {
                            swal('', 'Please Select Deadline Date');
                        }
                    } else {
                        swal('', 'Please Select User');
                    }
                } else {
                    swal('', 'Please Insert points');
                }
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.addshow = false;
                $scope.temp['sims_mom_description'] = "";
                $scope.edt['sims_mom_raised_user_code'] = "";
                $scope.edt['sims_mom_tobedone_user_code'] = "";
                $scope.temp['sims_mom_tobedone_date'] = "";
            }

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;

                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.records;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.records, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.records;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_mom_reference_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_mom_subject.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_mom_date == toSearch) ? true : false;
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


        }])

})();
