﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.StudentActivity');

    simsController.controller('MeetingTypeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.deleteList = [];
            $scope.meetingType;

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.getMettingType = function () {
                $http.get(ENV.apiUrl + "api/MeetingType/GetAllMeetingType").then(function (response) {
                    $scope.meetingType = response.data;
                    $scope.totalItems = $scope.meetingType.length;
                    $scope.todos = $scope.meetingType;
                    $scope.makeTodos();
                    $scope.checkMeetingCodeExists = function (meetingCode) {
                        angular.forEach($scope.meetingType, function (value, key) {
                            if (value.sims_mom_type_code == meetingCode) {
                                swal({ text: 'Meeting Code Already Exists.', imageUrl: "assets/img/notification-alert.png", width: 320, showCloseButton: true });
                                return false;
                            }
                        });
                    }
                });
            }
            $scope.getMettingType();

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseFloat($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseFloat(begin) + parseFloat($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                $scope.selectedAll = false;
                $scope.isSelectAll();

            };

            $scope.edt = {};
            $scope.grid = true;
            $scope.display = false;

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.disableMeetingCode = false;
                $scope.edt["sims_mom_type_code"] = '';
                $scope.edt["sims_mom_type_desc_en"] = '';
                $scope.edt["sims_mom_type_status"] = true;
                $scope.deleteList = [];
            }

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.disableMeetingCode = true;
                $scope.edt = angular.copy(str);

                $scope.edt = {
                    sims_mom_type_code: str.sims_mom_type_code,
                    sims_mom_type_desc_en: str.sims_mom_type_desc_en,
                    sims_mom_type_status: str.sims_mom_type_status,
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.edt["sims_mom_type_code"] = '';
                $scope.edt["sims_mom_type_desc_en"] = '';
                $scope.edt["sims_mom_type_status"] = false;
            }

            $scope.Save = function (Myform) {
                if (Myform) {
                    var data = [];
                    var obj = {
                        opr: 'I',
                        sims_mom_type_code: $scope.edt.sims_mom_type_code,
                        sims_mom_type_desc_en: $scope.edt.sims_mom_type_desc_en,
                        sims_mom_type_status: $scope.edt.sims_mom_type_status
                    }
                    data.push(obj);

                    $http.post(ENV.apiUrl + "api/MeetingType/CURDMeetingType", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                        $scope.getMettingType();
                        if ($scope.msg1 == true) {
                            swal({ text: 'Meeting Type Added Successfully', imageUrl: "assets/img/check.png", width: 350, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                            $scope.finalGrade = [];
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: 'Meeting Type Not Added OR Already Exists.', imageUrl: "assets/img/close.png", width: 350, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.Update = function (Myform) {
                if (Myform) {
                    var data = [];
                    var obj = {
                        opr: 'U',
                        sims_mom_type_code: $scope.edt.sims_mom_type_code,
                        sims_mom_type_desc_en: $scope.edt.sims_mom_type_desc_en,
                        sims_mom_type_status: $scope.edt.sims_mom_type_status
                    }
                    data.push(obj);

                    $http.post(ENV.apiUrl + "api/MeetingType/CURDMeetingType", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                        $scope.getMettingType();
                        if ($scope.msg1 == true) {
                            swal({ text: 'Meeting Type Updated Successfully', imageUrl: "assets/img/check.png", width: 330, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: 'Meeting Type Not Updated. ', imageUrl: "assets/img/close.png", width: 330, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.isSelectAll = function () {
                $scope.deleteList = [];
                if ($scope.selectedAll) {
                    $scope.selectedAll = true;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.deleteList.push($scope.filteredTodos[i].sims_mom_type_code);
                    }
                    $scope.row1 = 'row_selected';
                }
                else {
                    $scope.selectedAll = false;
                    $scope.row1 = '';
                }

                angular.forEach($scope.filteredTodos, function (item) {
                    item.ischecked = $scope.selectedAll;
                });

            }

            $scope.checkIfAllSelected = function () {
                var id = event.target.id;
                var value = event.target.value;
                if (document.getElementById(id).checked) {
                    $scope.deleteList.push(value);
                    if ($scope.deleteList.length == $scope.filteredTodos.length) {
                        $scope.selectedAll = true;
                    }
                    $("#" + id).closest('tr').addClass("row_selected");
                } else {
                    $scope.selectedAll = false;
                    $scope.row1 = '';
                    var index = $scope.deleteList.indexOf(value);
                    $scope.deleteList.splice(index, 1);
                    $("#" + id).closest('tr').removeClass("row_selected");
                }
            };

            $scope.Delete = function () {

                var data = [];

                for (var i = 0; i < $scope.deleteList.length; i++) {
                    var obj = {
                        opr: 'D',
                        sims_mom_type_code: $scope.deleteList[i]
                    }
                    data.push(obj);
                }

                if (data.length > 0) {

                    swal({
                        title: '',
                        text: "Are you sure you want to delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/MeetingType/CURDMeetingType", data).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: 'Meeting Type Deleted Successfully', imageUrl: "assets/img/check.png", width: 360, showCloseButton: true });
                                    $scope.display = false;
                                    $scope.grid = true;
                                    $scope.getMettingType();
                                    $scope.deleteList = [];
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: 'Meeting Type Not Deleted. ', imageUrl: "assets/img/close.png", width: 360, showCloseButton: true });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            })

                        }
                        else {
                            $scope.selectedAll = false;
                            $scope.isSelectAll();
                        }
                    })

                }
                else {
                    swal({ text: 'Select Atleast One Record To Delete', imageUrl: "assets/img/notification-alert.png", width: 320, showCloseButton: true });
                }
            }


            $scope.size = function (str) {

                //if (str == 'All') {

                //    $scope.totalItems = $scope.meetingType.length;
                //    $scope.todos = $scope.meetingType;
                //    $scope.filteredTodos = $scope.meetingType;
                //    $scope.numPerPage = $scope.meetingType.length;
                //    $scope.maxSize = $scope.meetingType.length
                //    $scope.makeTodos();
                //}
                //else {
                //    $scope.pagesize = str;
                //    $scope.currentPage = 1;
                //    $scope.numPerPage = str;
                //    $scope.makeTodos();
                //}
                
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.meetingType;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.meetingType, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.meetingType;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in 2 fields */
                return (item.sims_mom_type_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_mom_type_desc_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

        }])
})();