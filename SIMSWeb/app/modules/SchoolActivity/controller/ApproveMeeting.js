﻿
(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var comp_code;
    var finance_year;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.StudentActivity');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ApproveMeetingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = false;
            $scope.tableshow = false;
            $scope.edt = [];
            $scope.temp = [];
            $scope.attendeesList = [];
            var username = $rootScope.globals.currentUser.username;
            $rootScope.visible_stud = false;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = true;
            $rootScope.chkMulti = true;

            $scope.addAttendees = function () {
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }

            $scope.$on('global_cancel', function () {
               
                if ($scope.SelectedUserLst.length > 0) {
                    $scope.edt = {
                        empName: $scope.SelectedUserLst[0].empName,
                        em_number: $scope.SelectedUserLst[0].em_number
                    }
                }
                $scope.attendeesClick($scope.SelectedUserLst);
            });

            $http.get(ENV.apiUrl + "api/meeting/getApproveAccess?empno=" + username).then(function (res) {
                $scope.approverAcc = res.data;
                if ($scope.approverAcc) {
                    $scope.approvebtn = true;
                    $scope.rejectbtn = true;

                } else {
                    $http.get(ENV.apiUrl + "api/meeting/getAccess?empno=" + username).then(function (res) {
                       
                        $scope.access = res.data;
                        if ($scope.access) {
                            $scope.approvebtn = true;
                            $scope.rejectbtn = true;
                        } else {
                            $scope.approvebtn = false;
                            $scope.rejectbtn = false;
                        }
                      
                    });

                }
            });

            $http.get(ENV.apiUrl + "api/meeting/getRequester2").then(function (res) {
                $scope.requester = res.data;
               
            });

            $http.get(ENV.apiUrl + "api/meeting/getSubjects").then(function (res) {
                $scope.subjects = res.data;
                
                setTimeout(function () {
                    $("#cmb_ac_code").select2("val", $scope.temp.subject);
                }, 100);
            });


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
            $timeout(function () {
                $("#fixTable2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.reqChange = function (curreq) {
                $scope.temp['sims_mom_requester_user_code'] = curreq;
            }

            $scope.appChange = function (currapp) {
                $scope.temp['sims_mom_approver_user_code'] = currapp;
            }

            $scope.fdate = function (f_date) {
                //var date = new Date(f_date);
                //var month = (date.getMonth() + 1);
                //var day = date.getDate();
                //if (month < 10)
                //    month = "0" + month;
                //if (day < 10)
                //    day = "0" + day;
                //$scope.fd = date.getFullYear() + '-' + (month) + '-' + (day);
                //console.log($scope.fd);
                $scope.temp['from_date'] = f_date;
            }

            $scope.tdate = function (t_date) {
                //var date = new Date(t_date);
                //var month = (date.getMonth() + 1);
                //var day = date.getDate();
                //if (month < 10)
                //    month = "0" + month;
                //if (day < 10)
                //    day = "0" + day;
                //$scope.td = date.getFullYear() + '-' + (month) + '-' + (day);
                //console.log($scope.td);
                $scope.temp['to_date'] = t_date;
            }

            $scope.getdata = function () {
               
                if ($scope.temp['from_date'] == undefined) {
                    $scope.temp['from_date'] = ''
                }
                if ($scope.temp['to_date'] == undefined) {
                    $scope.temp['to_date'] = ''
                }
                if ($scope.temp['sims_mom_requester_user_code'] == undefined) {
                    $scope.temp['sims_mom_requester_user_code'] = ''
                }
                if ($scope.temp['sims_mom_subject'] == undefined) {
                    $scope.temp['sims_mom_subject'] = ''
                }
                $http.get(ENV.apiUrl + "api/meeting/getMeetings?fdate=" + $scope.temp['from_date'] + "&sdate=" + $scope.temp['to_date'] + "&requsrcode=" + $scope.temp['sims_mom_requester_user_code'] + "&subject=" + $scope.temp['sims_mom_subject']).then(function (res) {
                    $scope.records = res.data;
                    if ($scope.records.length > 0) {
                        $scope.tableshow = true;
                     
                        $scope.totalItems = $scope.records.length;
                        $scope.todos = $scope.records;
                        $scope.makeTodos();
                    } else {
                        $scope.tableshow = false;
                        $scope.errmsg = "Records Not Found"
                    }
                });
            }

            $scope.resetdata = function () {
                $scope.temp['from_date'] = '';
                $scope.temp['to_date'] = '';
                $scope.temp['sims_mom_subject'] = '';
                $scope.temp['em_number'] = '';
                $scope.tableshow = false;
                $scope.errmsg = '';
            }

            $scope.viewRecord = function (currRec) {
                $scope.currRecord = currRec;
              
                $scope.succmsg = "";
                $scope.ermsg = ""
                $('#viewModal').modal({ backdrop: "static" });
                $http.get(ENV.apiUrl + "api/meeting/getAttendees?sims_mom_number=" + currRec.sims_mom_number).then(function (res) {
                    $scope.attendees = res.data;
                   
                });
            }

            $scope.attendeesClick = function (glsearchemp) {
                $scope.attendees1 = [];
                for (var i = 0; glsearchemp.length > i; i++) {
                    if (glsearchemp[i].em_number != $scope.attendees[i].sims_mom_chairperson_user_code) {
                        $scope.att = {
                            opr: 'j',
                            sims_mom_chairperson_user_code: glsearchemp[i].em_number,
                            sims_mom_chairperson_user_name: glsearchemp[i].empName,
                            sims_mom_reference_number: $scope.currRecord.sims_mom_reference_number
                        }                            
                            $scope.attendees1.push($scope.att);
                            $http.post(ENV.apiUrl + "api/meeting/CUDInsertAttendees", $scope.attendees1).then(function (msg1) {
                                $scope.result1 = msg1.data;
                                if ($scope.result1) {
                                    $scope.ermsg = "";
                                    $scope.succmsg = "Record added successfully";
                                    $http.get(ENV.apiUrl + "api/meeting/getAttendees?sims_mom_number=" + $scope.currRecord.sims_mom_number).then(function (res) {
                                        $scope.attendees = res.data;
                                    });
                                } else {
                                    $scope.ermsg = "Record already exists"
                                    $scope.succmsg=""
                                }
                            });
                        } else {
                        $scope.ermsg = "Record already exists"
                        $scope.succmsg = ""
                        }
                    
                }
               
            }


            $scope.removeUser = function (rmuser, i) {
               
                $scope.attend = [];
                $scope.att = {
                    opr: 'F',
                    sims_mom_chairperson_user_code: rmuser,
                    sims_mom_reference_number: $scope.currRecord.sims_mom_reference_number
                }
                $scope.succmsg = "";
                $scope.attend.push($scope.att);
                $http.post(ENV.apiUrl + "api/meeting/CUDInsertAttendees", $scope.attend).then(function (msg1) {
                    $scope.result1 = msg1.data;
                    if ($scope.result1) {
                        $scope.attendees.splice(i, 1);
                        $scope.ermsg = "Record removed successfully"
                    }
                });

            }

            var datasend = [];
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_mom_reference_number);
                        $scope.filteredTodos[i]['check_mom_status'] = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_mom_reference_number);
                        $scope.filteredTodos[i]['check_mom_status'] = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.approveClick = function () {
                
                datasend = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i]['check_mom_status'] == true) {
                        var obj = {
                            opr: 'U',
                            sims_mom_reference_number: $scope.filteredTodos[i].sims_mom_reference_number,
                            sims_mom_status: 'S'
                        }
                        datasend.push(obj);
                    }
                }

                $http.post(ENV.apiUrl + "api/meeting/CUDMinutesOfMeeting", datasend).then(function (msg) {
                    $scope.result = msg.data;
                    if ($scope.result) {
                        swal('', 'Record Approved Successfully');
                        $scope.getdata();
                    } else {
                        swal('', 'Record Not Approved');
                    }
                });
            }

            $scope.rejectClick = function () {
                datasend = [];

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i]['check_mom_status'] == true) {
                        var obj = {
                            opr: 'U',
                            sims_mom_reference_number: $scope.filteredTodos[i].sims_mom_reference_number,
                            sims_mom_status: 'C'
                        }
                        datasend.push(obj);
                    }
                }

                $http.post(ENV.apiUrl + "api/meeting/CUDMinutesOfMeeting", datasend).then(function (msg) {
                    $scope.result = msg.data;
                    if ($scope.result) {
                        swal('', 'Record Rejected Successfully');
                        $scope.getdata();
                    } else {
                        swal('', 'Record Not Rejected');
                    }
                });
            }

            $('.clockpicker').clockpicker({
                autoclose: true
            });

            $scope.checktime = function (min, max) {
               
                $scope.check_time_flag = true;

            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
            }

            //DATA EDIT
            $scope.edit = function (str) {
                
                $scope.gtcreadonly = true;
                $scope.gtdreadonly = true;
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                //$scope.table = false;
                //$scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.temp = str;

                //$scope.getDocumentN(str.sims_doc_mod_code);


                $scope.temp = {
                    gldc_doc_code: str.gldc_doc_code
                   , gldc_doc_name: str.gldc_doc_name
                   , gldc_doc_type: str.gldc_doc_type
                   , gldc_next_srl_no: str.gldc_next_srl_no
                   , gldc_next_prv_no: str.gldc_next_prv_no

                };
            }

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.records;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.records, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.records;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_mom_reference_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_mom_subject.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_mom_date == toSearch) ? true : false;
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


        }])

})();
