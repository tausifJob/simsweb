﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var comp_code;
    var finance_year;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.StudentActivity');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ViewMeetingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = false;
            $scope.tableshow = false;
            $scope.edt = [];
            $scope.temp = [];
            $scope.attendeesList = [];
            var username = $rootScope.globals.currentUser.username;

            $scope.remark = true;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
            $timeout(function () {
                $("#fixTable2").tableHeadFixer({ 'top': 1 });
            }, 100);

            //  $scope.getdata = function () {
            $http.get(ENV.apiUrl + "api/meeting/getScheduleMeeting?empcode=" + username + "&momstatus=S").then(function (res) {
                $scope.records = res.data;
                console.log($scope.records);
                if ($scope.records.length > 0) {
                    $scope.tableshow = true;
                    console.log($scope.records);
                    $scope.totalItems = $scope.records.length;
                    $scope.todos = $scope.records;
                    $scope.makeTodos();
                } else {
                    $scope.tableshow = false;
                    $scope.errmsg = "Records Not Found"
                }
            });

            //  }



            $scope.viewRecord = function (currRec) {
                $scope.currRecord = currRec;
                console.log($scope.currRecord);
                $scope.succmsg = "";
                $scope.ermsg = ""
                $scope.edt['sims_mom_receipient_response_flag'] = 'T';
                $scope.remark = true;
                $http.get(ENV.apiUrl + "api/meeting/getAttendees?sims_mom_number=" + currRec.sims_mom_number).then(function (res) {
                    $scope.attendees = res.data;
                    console.log($scope.attendees);
                    $('#viewModal').modal({ backdrop: "static" });

                });
            }
            $scope.edt['sims_mom_receipient_response_flag'] = 'T';
            $scope.attending = function (resflag, remark) {
                $scope.succmsg = "";
                $scope.ermsg = "";
                if (resflag == 'T') {
                    $scope.remark = true;
                    $scope.edt['sims_mom_receipient_response_flag'] = resflag;
                } else if (resflag == 'F') {
                    $scope.remark = false;
                    $scope.edt['sims_mom_receipient_response_flag'] = resflag;
                    $scope.edt['sims_mom_receipient_response_remark'] = remark;
                }
            }

            $scope.saveClick = function () {
                if ($scope.edt['sims_mom_receipient_response_flag'] == 'T') {
                    $scope.edt['sims_mom_receipient_response_remark'] = "";
                    $scope.upd();
                } else {
                    if ($scope.edt['sims_mom_receipient_response_remark'] != undefined && $scope.edt['sims_mom_receipient_response_remark'] != "") {
                        $scope.upd();
                    } else {
                        $scope.ermsg = "Enter Remark";
                        $scope.succmsg = "";
                    }
                }
            }
            $scope.upd = function () {
                var obj = {
                    opr: 'W',
                    employee_code: username,
                    sims_response_flag: $scope.edt['sims_mom_receipient_response_flag'],
                    sims_mom_number: $scope.currRecord.sims_mom_number,
                    sims_mom_receipient_response_remark: $scope.edt.sims_mom_receipient_response_remark
                }
                console.log(obj);
                $http.post(ENV.apiUrl + "api/meeting/UpdateResponse", obj).then(function (msg) {
                    $scope.result = msg.data;
                    if ($scope.result) {
                        $scope.succmsg = "Record saved successfully";
                        $scope.ermsg = ""
                    } else {
                        $scope.ermsg = "Record not saved. " ;
                        $scope.succmsg = "";
                    }
                });
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger;
                $scope.gtcreadonly = true;
                $scope.gtdreadonly = true;
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                //$scope.table = false;
                //$scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.temp = str;

                //$scope.getDocumentN(str.sims_doc_mod_code);


                $scope.temp = {
                    gldc_doc_code: str.gldc_doc_code
                   , gldc_doc_name: str.gldc_doc_name
                   , gldc_doc_type: str.gldc_doc_type
                   , gldc_next_srl_no: str.gldc_next_srl_no
                   , gldc_next_prv_no: str.gldc_next_prv_no

                };
            }

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.records;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.records, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.records;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_mom_reference_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_mom_subject.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_mom_date == toSearch) ? true : false;
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });


        }])

})();
