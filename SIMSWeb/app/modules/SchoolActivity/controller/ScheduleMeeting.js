﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var comp_code;
    var finance_year;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.StudentActivity');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ScheduleMeetingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = true;
            $scope.table = true;
            $scope.edt = [];
            $scope.temp = [];
            $scope.attendeesList = [];

            var date = new Date();
            var month = (date.getMonth() + 1);
            var day = date.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            $scope.fd = (day) + '-' + (month) + '-' + date.getFullYear();
            $scope.temp['sims_mom_date'] = $scope.fd;

            var username = $rootScope.globals.currentUser.username;
            console.log($rootScope.globals);

            $scope.gloSearch = function () {
                $rootScope.visible_stud = false;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = true;
                $rootScope.chkMulti = false;
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                $scope.temp1 = true;
                $scope.temp2 = false;
                $scope.temp3 = false;

            }

            $scope.gloSearch1 = function () {
                $rootScope.visible_stud = false;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = true;
                $rootScope.chkMulti = false;
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                $scope.temp1 = false;
                $scope.temp2 = true;
                $scope.temp3 = false;

            }

            $scope.gloSearch3 = function () {
                $rootScope.visible_stud = false;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = true;
                $rootScope.chkMulti = true;
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                $scope.temp1 = false;
                $scope.temp2 = false;
                $scope.temp3 = true;

            }

            $scope.$on('global_cancel', function () {
                if ($scope.SelectedUserLst.length > 0) {
                    $scope.edt =
                        {
                            empName: $scope.SelectedUserLst[0].empName,
                            em_number: $scope.SelectedUserLst[0].em_number
                        }
                }
                if ($scope.temp1) {
                    $scope.temp['sims_mom_chairperson_user_code'] = $scope.edt.em_number;
                    $scope.temp['sims_mom_chairperson_user_name'] = $scope.edt.empName;
                    $scope.type = "Chairperson";
                    var ob = {
                        opr: 'J',
                        type: $scope.type,
                        sims_mom_chairperson_user_code: $scope.temp.sims_mom_chairperson_user_code,
                        sims_mom_chairperson_user_name: $scope.edt.empName,
                        sims_mom_reference_number: $scope.temp.sims_mom_reference_number
                    }
                    for (var i = $scope.attendeesList.length - 1; i >= 0; i--) {
                        if ($scope.attendeesList[i].sims_mom_chairperson_user_code == ob.sims_mom_chairperson_user_code) {
                            if ($scope.attendeesList[i].type == "Recorder") {
                                $scope.temp['sims_mom_recorder_user_code'] = "";
                                $scope.temp['sims_mom_recorder_user_name'] = "";
                            }

                            $scope.attendeesList.splice(i, 1);
                            break;
                        }
                    }

                    for (var i = $scope.attendeesList.length - 1; i >= 0; i--) {
                        if ($scope.attendeesList[i].type == $scope.type) {
                            $scope.attendeesList.splice(i, 1);

                            break;
                        }
                    }
                    $scope.attendeesList.push(ob);

                }
                else if ($scope.temp2) {
                    $scope.temp['sims_mom_recorder_user_code'] = $scope.edt.em_number;
                    $scope.temp['sims_mom_recorder_user_name'] = $scope.edt.empName;
                    $scope.type = "Recorder";
                    var ob = {
                        opr: 'J',
                        type: $scope.type,
                        sims_mom_chairperson_user_code: $scope.temp.sims_mom_recorder_user_code,
                        sims_mom_chairperson_user_name: $scope.edt.empName,
                        sims_mom_reference_number: $scope.temp.sims_mom_reference_number
                    }
                    for (var i = $scope.attendeesList.length - 1; i >= 0; i--) {
                        if ($scope.attendeesList[i].sims_mom_chairperson_user_code == ob.sims_mom_chairperson_user_code) {
                            if ($scope.attendeesList[i].type == "Chairperson") {
                                $scope.temp['sims_mom_chairperson_user_code'] = "";
                                $scope.temp['sims_mom_chairperson_user_name'] = "";
                            }
                            $scope.attendeesList.splice(i, 1);
                            break;
                        }
                    }

                    for (var i = $scope.attendeesList.length - 1; i >= 0; i--) {
                        if ($scope.attendeesList[i].type == $scope.type) {
                            $scope.attendeesList.splice(i, 1);
                            break;
                        }
                    }
                    $scope.attendeesList.push(ob);
                }
                else if ($scope.temp3) {
                    $scope.attendeesClick($scope.SelectedUserLst)
                }

            });

            $scope.attendees = false;

            $scope.meetinghistroy = function () {
                $scope.table = false;
                $scope.getdata()
            }

            $scope.viewCancel = function () {
                $scope.table = true;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.getdata = function () {
                $http.get(ENV.apiUrl + "api/meeting/getMinutesofMeeting").then(function (res) {
                    $scope.records = res.data;
                    console.log($scope.records);
                    $scope.totalItems = $scope.records.length;
                    $scope.todos = $scope.records;
                    $scope.makeTodos();
                });
            }

            $http.get(ENV.apiUrl + "api/meeting/getApprover").then(function (res) {
                $scope.approver = res.data;
                console.log($scope.approver);
            });

            $http.get(ENV.apiUrl + "api/meeting/getAccess?empno=" + username).then(function (res) {
                debugger;
                $scope.access = res.data;
                if ($scope.access) {
                    $scope.inputbox = false;
                    $scope.selectbox = true;
                } else {
                    $scope.inputbox = true;
                    $scope.selectbox = false;
                    $scope.temp['sims_mom_requester_user_code'] = username;
                    $scope.temp['sims_mom_requester_user_name'] = $scope.user_details.comn_user_alias
                }
                console.log($scope.access);
            });

            $http.get(ENV.apiUrl + "api/meeting/getRequester").then(function (res) {
                $scope.requester = res.data;
                console.log($scope.requester);
            });

            $scope.reqChange = function (curreq) {
                $scope.temp['sims_mom_requester_user_code'] = curreq;
            }

            $scope.appChange = function (currapp) {
                $scope.temp['sims_mom_approver_user_code'] = currapp;
            }

            $scope.getdata();

            $http.get(ENV.apiUrl + "api/meeting/getMeetingType").then(function (res) {
                $scope.mtypes = res.data;
                console.log($scope.records);
            });

            $scope.exits = false

            $scope.checkRefno = function (refno) {
                $http.get(ENV.apiUrl + "api/meeting/getReferenceNo?refno=" + refno).then(function (res) {
                    $scope.count = res.data;
                    if ($scope.count > 0) {
                        $scope.exits = true;
                        $scope.temp['sims_mom_reference_number'] = "";
                        $scope.exitsno = "Reference number already exits";
                    } else {
                        $scope.exitsno = "";
                    }
                    console.log($scope.count);
                });
            }

            $scope.attendeesClick = function (glsearchemp) {
                $scope.attendees = true;
                for (var i = 0; $scope.SelectedUserLst.length > i; i++) {
                    var ob = {
                        opr: 'j',
                        type: 'Attendees',
                        sims_mom_chairperson_user_code: $scope.SelectedUserLst[i].em_number,
                        sims_mom_chairperson_user_name: $scope.SelectedUserLst[i].empName,
                        sims_mom_reference_number: $scope.temp.sims_mom_reference_number
                    }
                    var x = 0;
                    for (var j = 0; j < $scope.attendeesList.length ; j++) {
                        if ($scope.attendeesList[i].sims_mom_chairperson_user_code == ob.sims_mom_chairperson_user_code) {
                            x++;
                        }
                    }
                    if (x == 0) {
                        $scope.attendeesList.push(ob);
                    }
                }
                console.log($scope.attendeesList);
            }

            $scope.removeUser = function (rmuser, i) {
                console.log(rmuser);
                console.log(i);
                //if (i != 0 && i!=1) {                    
                $scope.attendeesList.splice(i, 1);
                //}
                console.log($scope.attendeesList);
            }

            $('.clockpicker').clockpicker({
                autoclose: true
            });

            $scope.checktime = function (min, max) {
                $scope.check_time_flag = true;
                if (min >= max) {
                    swal({ title: "Alert", text: "Please Select Correct Time" });
                    $scope.temp['sims_mom_end_time'] = "";
                    $scope.check_time_flag = false;
                }
            }

            $scope.saved = false;
            $scope.savedata = function (isValidate) {
                var datasend = [];
                debugger;
                if (isValidate) {
                    $scope.saved = true;
                    console.log($scope.temp);
                    var obj = {
                        opr: 'I',
                        sims_mom_reference_number: $scope.temp.sims_mom_reference_number,
                        sims_mom_type_code: $scope.temp.sims_mom_type_code,
                        sims_mom_subject: $scope.temp.sims_mom_subject,
                        sims_mom_venue: $scope.temp.sims_mom_venue,
                        sims_mom_date: $scope.temp.sims_mom_date,
                        sims_mom_start_time: $scope.temp.sims_mom_start_time,
                        sims_mom_end_time: $scope.temp.sims_mom_end_time,
                        sims_mom_chairperson_user_code: $scope.temp.sims_mom_chairperson_user_code,
                        sims_mom_recorder_user_code: $scope.temp.sims_mom_recorder_user_code,
                        sims_mom_requester_user_code: $scope.temp.sims_mom_requester_user_code,
                        sims_mom_approver_user_code: $scope.temp.sims_mom_approver_user_code,
                        sims_mom_date_creation_user_code: $scope.temp.sims_mom_date_creation_user_code,
                        sims_mom_agenda: $scope.temp.sims_mom_agenda
                    }
                    datasend.push(obj);
                    console.log(datasend);
                    $http.post(ENV.apiUrl + "api/meeting/CUDMinutesOfMeeting", datasend).then(function (msg) {
                        $scope.result = msg.data;
                        if ($scope.result==true) {
                            swal('', 'Record Inserted Successfully');
                            $scope.saved = false;
                            $scope.Myform.$setPristine();
                            $scope.Myform.$setUntouched();
                            $http.post(ENV.apiUrl + "api/meeting/CUDInsertAttendees", $scope.attendeesList).then(function (msg1) {
                                $scope.result1 = msg1.data;
                                $scope.attendeesList = [];
                                $scope.temp = "";
                            });
                            $scope.getdata();
                        } else if ($scope.result == false) {
                            swal('', 'Record Not Inserted. ');
                            $scope.saved = false;
                        }
                        else {
                            swal("Error-" + $scope.result)
                        }
                    });
                }
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = [];
                if ($scope.temp == '') {
                    $scope.temp['sims_mom_requester_user_name'] = $scope.user_details.comn_user_alias
                }
                $scope.attendeesList = [];
                $scope.attendees = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;

                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.records;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.records, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.records;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_mom_reference_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_mom_subject.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_mom_date == toSearch) ? true : false;
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }])

})();
