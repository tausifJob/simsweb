﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.TimeTable');
    var oSlotDayNames = ["XYZ","Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var AllAcademicYear;
    var AllGradesSec;
    var gradeSelected;
    var getSectionFromGrade;
    var data = [];
    var secArr = [];
    var dayLec = [];
    var gradeSecArr = [];
    var settings = { 'on': 'fa fa-check-square-o', 'off': 'fa fa-square-o' };
    var counter = 0;
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    
    simsController.controller('BellConfigCont',
        ['$scope', '$compile', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$locale', '$http', 'ENV', function ($scope, $compile, $state, $rootScope, $timeout, gettextCatalog, $locale, $http, ENV) {
            $scope.saveShow = false;
            $scope.loaderShow = false;
            $scope.messageShow = false;

            $scope.getCurriculum = function () {
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                    $scope.Allcurriculum = res.data;
                    $scope.curr_code = $scope.Allcurriculum[0].sims_cur_code;
                    $scope.getAcademicYear($scope.curr_code);
                });
            }

            $scope.getAcademicYear = function ()
            {
                $scope.gradeSelected = true;
                $http.get(ENV.apiUrl + "api/BellConfig/getAcademicYear").then(function (getAcaYear) {
                    $scope.AllAcademicYear = getAcaYear.data;
                    $($scope.AllAcademicYear).each(function (k, v) {
                        if (v.aca_year_status == 'C') {
                            $scope.aca_year = v.aca_year;
                            $scope.getGradeSec();
                            return false;
                        }
                    });
                });
            }

            $scope.getGradeSec = function ()
            {
                $(".addlecturediv").html('');
                $scope.bellName = "";
                $http.get(ENV.apiUrl + "api/BellConfig/getAllGradesSecNew?aca_year=" + $scope.aca_year + "&cur_code=" + $scope.curr_code).then(function (res) {
                    $(".datacontent").removeClass('hide');
                    $scope.AllGradesSec = res.data;
                    $("#grade_select").prop("disabled", false).removeClass('seldisabled');
                });
            }

            $scope.removeSelectedClass = function(e)
            {
                if (e.target.tagName == 'i' || e.target.tagName == 'I') {
                    var $checkbox = $(e.target.parentNode).next('input:checkbox');
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.triggerHandler('change');
                    $scope.updateDisplay(e.target.parentNode.parentNode);
                    var id = $(e.target.parentNode).parents('.grade_section').attr("id");
                }
                else {
                    var $checkbox = $(e.target.parentNode).find('input:checkbox');
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.triggerHandler('change');
                    $scope.updateDisplay(e.target.parentNode);
                    var id = $(e.target).parents('.grade_section').attr("id");
                }

                if ($checkbox.is(':checked'))
                {
                    $("#" + id + " .sectionbyclass .button-checkbox").find('button');
                    $("#" + id + " .sectionbyclass .button-checkbox").find('input:checkbox').prop("checked", true);
                }
                else
                {
                    $("#" + id + " .sectionbyclass .button-checkbox").find('button');
                    $("#" + id + " .sectionbyclass .button-checkbox").find('input:checkbox').prop("checked", false);
                }
                

                $("#" + id + " .sectionbyclass .button-checkbox").each(function (k, v)
                {
                    $scope.updateDisplay(this);
                })
            }

            $scope.selectRemoveSections = function(e)
            {
                if (e.target.tagName == 'i' || e.target.tagName == 'I')
                {
                    $checkbox = $(e.target.parentNode).next('input:checkbox');
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.triggerHandler('change');
                    $scope.updateDisplay(e.target.parentNode.parentNode);
                }
                else
                {
                    var $checkbox = $(e.target.parentNode).find('input:checkbox');
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.triggerHandler('change');
                    $scope.updateDisplay(e.target.parentNode);
                }

                var classSelect = 'I';

                if ($(e.target).parents('.sectionbyclass').find('.active').length > 0) {
                    classSelect = 'A';
                }
                //$(e.target).parents('.sectionbyclass').find('input:checkbox').each(function (k, v) {
                //    if ($(v).is(':checked')) {
                //        classSelect = 'A';
                //    }
                //})

                if (classSelect == 'I') {
                    $(e.target).parents('.grade_section_row').find('.gradeClass').find('.i').removeClass('fa-check-square-o').addClass('fa-square-o');
                    
                    $(e.target).parents('.grade_section_row').find('.gradeClass').find('.btn').removeClass('btn-danger').addClass('btn-default');
                    $(e.target).parents('.grade_section_row').find('.gradeinput').prop('checked', false);
                }
                else {
                    $(e.target).parents('.grade_section_row').find('.gradeClass').find('i').addClass('fa-check-square-o').removeClass('fa-square-o');
                    $(e.target).parents('.grade_section_row').find('.gradeClass').find('.btn').addClass('btn-danger').removeClass('btn-default');
                    $(e.target).parents('.grade_section_row').find('.gradeinput').prop('checked', true);
                }
            }

            $scope.selectDays = function(e)
            {
                $(e.target).toggleClass('active');
            }

            $scope.addLecturefield = function ()
            {
                counter++;
                var randNum = Math.floor((Math.random() * 100) + 1);
                var html = '<div class="addlecturefield" id="lec_' + counter + '" ><i class="fa fa-times removelec" aria-hidden="true" ng-click="removeLec($event)"></i>';
                html += '<div class="form-group">';
                html += '<input type="text" class="form-control sujectClass input-sm k-textbox" placeholder="Lecture Name">';
                html += '</div>';
                html += '<div class="form-group">';
                html += '<label><i class="fa fa-clock-o"></i>&nbsp;From</label>';
                html += '<input type="time" class="form-control startClass input-sm " placeholder="yy">';
                html += '</div>';
                html += '<div class="form-group">';
                html += '<label><i class="fa fa-clock-o"></i>&nbsp;to</label>';
                html += '<input type="time" class="form-control endClass input-sm" placeholder="yy">';
                html += '</div>';
                html += '<div class="breakDiv"><div class="checkbox check-primary"><input class="k-checkbox" type="checkbox" value="' + randNum + '" id="break_' + randNum + '" name="break" /><label for="break_' + randNum + '"> Is Break ?</label></div></div>';
                html += '</div>';
                
                var temp = $compile(html)($scope);
                $(".addlecturediv").append(temp);
            }

            $scope.removeLec = function(e)
            {
                $(e.target.parentNode).remove();
            }
            
            $scope.updateDisplay = function (obj)
            {
                var $button = $(obj).find('button'), $checkbox = $(obj).find('input:checkbox'), color = $button.attr('data-color');
                
                var isChecked = $checkbox.is(':checked');

                $button.data('state', (isChecked) ? "on" : "off");
                
                if (isChecked)
                {
                    $button.find('.state-icon').removeClass('fa fa-square-o').addClass('fa fa-check-square-o');
                }
                else {
                    $button.find('.state-icon').removeClass('fa fa-check-square-o').addClass('fa fa-square-o');
                }

                
                if (isChecked)
                {
                    $button.removeClass('btn-default').addClass('btn-' + color + ' active');
                }
                else
                {
                    $button.removeClass('btn-' + color + ' active').addClass('btn-default');
                }
            }


            $scope.saveData = function()
            {
                
                data = [];
                gradeSecArr = [];
                var acaDesc = $("#acaYear option:selected").text();
                $('.grade_section_row').each(function (k, v) {
                    secArr = [];

                    var rowid = $(this).attr('id');
                    var grade_code = $(this).find('.gradeClass').find('input:checkbox').val();
                    var grade_name = $(this).find('.gradeClass').find('input:checkbox').data('name');
                    var issecsele, isgradesele;

                    $(this).find('.sectionbyclass').find('input:checkbox').each(function (key, val) {
                        if (val.checked) {
                            issecsele = 'A';
                        }
                        else {
                            issecsele = 'I';
                        }
                        var sec_code = $(this).val();
                        var sec_namecode = $(this).data('name');
                        secArr.push({ 'sims_section_code': sec_code, 'sims_section_name': sec_namecode, 'status': issecsele });

                    })

                    gradeSecArr.push({ 'grade_code': grade_code, 'grade_name': grade_name, 'section': secArr });
                });
             
                if ($scope.bellName == "" || $scope.bellName == 'undefined')
                {
                    alert("Bell Name Required");
                }
                else if (gradeSecArr.length == 0)
                {
                    alert("Please Select Grades And Section");
                }
                else
                {
                    data.push({ 'aca_year': $scope.aca_year, 'bell_name': $scope.bellName, 'bellGrade': gradeSecArr, 'bellDay': dayLec });
                }

                if (data.length != 0)
                {
                    $scope.loaderShow = true;
                    var bell_code = '00';
                    $scope.cTemp = 'I';
                    if ($('body').hasClass('editmode'))
                    {
                        bell_code = $scope.edit_bell_code;
                        $scope.cTemp = 'U';
                    }
                    $http.post(ENV.apiUrl + "api/bellconfig/bellcongifdataupdate?ctemp=" + $scope.cTemp + "&bell_code=" + bell_code + "&aca_year=" + $scope.aca_year + "&cur_code=" + $scope.curr_code, data).success(function (res) {
                       
                        if ($('body').hasClass('editmode'))
                        {
                            $scope.loaderShow = false;
                            $scope.messageShow = true;
                            $scope.message = 'Updated SuccessFully';
                        }
                        else {
                            if (res != false) {
                                console.log("This is after save  = ", res);
                                $scope.savedBellData.push(res[0]);
                                $scope.loaderShow = false;
                                $scope.messageShow = true;
                                $scope.message = 'Save SuccessFully';
                            }
                        }
                    });
                }
            }

            $scope.addNewBell = function ()
            {
                location.reload();
            }

            $scope.saveLectures = function ()
            {
                var isbreak;
                var lectures = [];

                if ($('body').hasClass('editmode'))
                {
                    dayLec = $scope.AllDayLec;
                }

                $('.addlecturefield').each(function (k, v)
                {
                    var subName = $(this).find('.sujectClass').val();
                    var from = $(this).find('.startClass').val();
                    var to = $(this).find('.endClass').val();

                    if ($(this).find('input:checkbox').is(':checked')) {
                        isbreak = 'Y'
                    }
                    else {
                        isbreak = 'N'
                    }

                    lectures.push({ 'slot_code': k + 1, 'lect_desc': subName, 'from_time': from, 'to_time': to, 'isBreak': isbreak });
                    
                        
                });
                $scope.saveShow = true

                $('.daysdiv').find('button.active').each(function (k, v) {
                    var dayid = $(v).data('day').toString();
                    var isexist = $scope.in_array(dayLec, dayid);
                    if (isexist) {

                        dayLec.splice((isexist - 1), 1);
                    }

                    dayLec.push({ 'day_code': dayid, 'lecture': lectures });
                });
                
            }

            $scope.in_array = function (array, dayid)
            {
                for (var i = 0; i < array.length; i++)
                {
                    if (array[i].day_code === dayid)
                    {
                         return i+1;
                    }
                }
                return false;
            }

            $scope.accordian = function(e)
            {
                if ($(e.target.parentNode).next('.toggle_section').css('display') != 'none') {
                    $(e.target).removeClass('fa fa-minus').addClass('fa fa-plus');
                }
                else {
                    $(e.target).removeClass('fa fa-plus').addClass('fa fa-minus');
                }
                $(e.target.parentNode).next('.toggle_section').slideToggle();
            }

            $scope.getBellData = function ()
            {
                $http.get(ENV.apiUrl + "api/BellConfig/getBellData").then(function (res) {
                    $scope.savedBellData = res.data;
                });
            }

            $scope.deleteBell = function (str1, str2)
            {
                var conf = confirm("Are you sure to delete bell?");
                if (conf == true) {
                    $scope.AllGradesSec = "";
                    $(".addlecturediv").html('');
                    $http.post(ENV.apiUrl + "api/BellConfig/CUDBellDetailSectionData?aca_year=" + str1 + "&bell_code=" + str2).then(function (res) {
                        $("#bellsum_" + str2 + '_' + str1).remove();
                    });
                }
            }

            $scope.editBell = function (str1, str2, str3, str4)
            {

                $("body").addClass('editmode');
                $("body").attr('data-bellcode', str2);
                $scope.edit_bell_code = str2;
                $http.get(ENV.apiUrl + "api/BellConfig/getSavedBellSecData?aca_year=" + str1 + "&bell_code=" + str2 + "&cur_code=" + str4).then(function (res) {
                    
                    $(".datacontent").removeClass('hide');
                    $scope.AllGradesSec = res.data;
                    $scope.aca_year = str1;
                    $scope.bellName = str3;
                    $scope.curr_code = str4;



                    $($scope.AllGradesSec).each(function (key, val)
                    {
                        val.classStatus == 'I';
                        $(val.section).each(function (k, v)
                        {
                            if (v.status == 'A')
                            {
                                val.classStatus = 'A';
                            }
                        })
                    })
                });

                $http.get(ENV.apiUrl + "api/BellConfig/getSavedBellDetailData?aca_year=" + str1 + "&bell_code=" + str2).then(function (res)
                {
                    $(".dayTab").removeClass('hide');
                    $(".daysdiv  .days").removeClass('active');
                    var randNum = Math.floor((Math.random() * 100) + 1);
                    $scope.AllDayLec = res.data;
                    $scope.selectLectureByDay('1');
                });
            }

            $scope.selectLectureByDay = function (dayid)
            {
                $("#daytab_" + dayid).addClass('active').siblings('.daytabitem').removeClass('active');
                $("#day_" + dayid).addClass('active').siblings('.days').removeClass('active');
              
                $(".addlecturediv").html('');
                $($scope.AllDayLec).each(function (key, val)
                {
                    if (val.day_code == dayid)
                    {
                        $(val.lecture).each(function (k, v)
                        {
                            var randNum = Math.floor((Math.random() * 100) + 1);
                            var lectid = document.getElementById('lec_' + v.slot_code);
                            var isbreak;
                            if (lectid === null) {

                                var checkboxid = val.day_code + '_' + v.slot_code;

                                if (v.isBreak == 'Y') {
                                    isbreak = '<input type="checkbox" class="" value="' + val.day_code + '_' + v.slot_code + '" checked id="' + checkboxid + '" name="break" />';
                                }
                                else {
                                    isbreak = '<input type="checkbox" class="" value="' + val.day_code + '_' + v.slot_code + '"  id="' + checkboxid + '" name="break" />';
                                }

                                var html = '<div class="addlecturefield" id="lec_' + v.slot_code + '"><i class="fa fa-times removelec" aria-hidden="true" ng-click="removeLec($event)"></i>';
                                html += '<div class="form-group">';
                                html += '<input type="text" class="form-control sujectClass k-textbox" placeholder="Lecture Name" value="' + v.lect_desc + '">';
                                html += '</div>';
                                html += '<div class="form-group">';
                                html += '<label>From</label>';
                                html += '<input type="time" class="form-control startClass k-textbox" value="' + v.from_time + '" placeholder="yy">';
                                html += '</div>';
                                html += '<div class="form-group">';
                                html += '<label>to</label>';
                                html += '<input type="time" class="form-control endClass" value="' + v.to_time + '" placeholder="yy">';
                                html += '</div>';
                                html += '<div class="breakDiv"><div class="checkbox check-primary">' + isbreak + ' <label for="' + checkboxid + '">  Is Break ?</label></div></div></div>';
                                html += '</div>';

                                var temp = $compile(html)($scope);
                                $(".addlecturediv").append(temp);
                            }

                        })
                    }
                })

                
            }

            $(document).ready(function ()
            {
                $scope.getBellData();
                $scope.getCurriculum();
                $scope.condenseMenu();
                $('body').removeClass('editmode');
            });

        }]);
})();
