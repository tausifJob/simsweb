﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.TimeTable');
    var iSlotCount; //= [7,7,7,7,5];
    var iSlotDay; //= 5;
    var sHtmlContent = "";
    var a = {};

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SubjectPreferenceCont', ['$scope', '$compile', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$locale', '$http', 'ENV', function ($scope, $compile, $state, $rootScope, $timeout, gettextCatalog, $locale, $http, ENV) {

        $scope.getCurriculum = function () {
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.Allcurriculum = res.data;
                $scope.cur_code = $scope.Allcurriculum[0].sims_cur_code;
                $scope.getAcademicYear();
            });
        }

        $scope.getAcademicYear = function () {
            var a = {
                opr: 'A',
                cur_code: $scope.cur_code
            };
            $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableCommon", a).then(function (res) {

                $scope.AllAcademicYear = res.data.table;
                $($scope.AllAcademicYear).each(function (k, v) {
                    if (v.sims_academic_year_status == 'C') {
                        $scope.aca_year = v.sims_academic_year;
                        $scope.getBellData();
                        return false;
                    }
                });
            });
        };

        $scope.getBellData = function () {
            a = {
                Opr: 'B',
                AcaYear: $scope.aca_year,
                cur_code: $scope.cur_code
            };
            $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableImport", a).then(function (getBellData) {

                $scope.AllBellData = getBellData.data.table;
                if (getBellData.data.table.length > 0) {
                    $scope.bell_code = $scope.AllBellData[0].sims_bell_code;
                } else {
                    $scope.bell_code = "";
                }
                $scope.fetchSubGrade();
                $scope.getTeachers('', '', 'teacheronly');
            });
        }

        $scope.getRules = function () {
            var a = {
                'opr': 'Q'
            };
            $http.post(ENV.apiUrl + "api/PreferenceController/TimeTableRules", a).then(function (res) {
                $scope.AllTimeTableRules = res.data;
                $($scope.AllTimeTableRules).each(function (k, v) {
                    if (v.sims_bell_appl_form_field == "Class Teacher") {
                        if (v.sims_bell_appl_parameter == 'Y') {
                            $scope.ct_pre = true;
                        } else {
                            $scope.ct_pre = false;
                        }
                    } else if (v.sims_bell_appl_form_field == "Subject Preference") {
                        if (v.sims_bell_appl_parameter == '1') {
                            $scope.sp_pre = true;
                        } else {
                            $scope.sp_pre = false;
                        }

                    } else if (v.sims_bell_appl_form_field == "Teacher Preference") {

                        if (v.sims_bell_appl_parameter == '1') {
                            $scope.tp_pre = true;
                        } else {
                            $scope.tp_pre = false;
                        }
                    }
                });
            })
        };

        $scope.fetchSubGrade = function () {
            $scope.subject_grade_code = "";
            $scope.teacher_grade_code = "";
            $scope.fetchSubjects('bell');
            $scope.getGrade($scope.bell_code);
            $scope.getTeachers('', '', 'allteachers')
            $scope.getDaySlotBreakDetail();
        }

        $scope.fetchSubSaveddata = function (str) {
            $scope.savedSubjectPreference(str);
            $scope.genGrid('subject');
        }

        $scope.genGrid = function (type) {
            var id;
            var html_text;
            var fun_name;
            var div_id;

            var html = '<table id="pereferencetable" class="table table-bordered">';
            html += '<thead>';
            html += '<tr><th class="nobg text-left"> D/Lec</th>';
            for (var i = 0; i < $scope.slot_count; i++) {
                var lec_c;
                if ($scope.time_break_detail[i]['sims_bell_break'] == 'Y') {
                    lec_c = $scope.time_break_detail[i]['sims_bell_slot_desc'] + ' [Break]';
                } else {
                    lec_c = $scope.time_break_detail[i]['sims_bell_slot_desc'];
                }
                html += '<th class="slotWidth text-center">' + lec_c + '<span class="display_block font-sm">[' + $scope.time_break_detail[i]['sims_bell_start_time'] + ' to ' + $scope.time_break_detail[i]['sims_bell_end_time'] + ']</span></th>';
            }
            html += '</tr>';
            html += '<tbody>';
            for (var i = 0; i < $scope.day_count; i++) {
                html += '<tr class="grid_row"> <td class="slotWidth daytd text-white text-left ">' + $scope.oSlotDayNames[i] + '</td>';
                var break_count = 0;
                for (var j = 0; j < $scope.time_break_detail_new[i].length - 1; j++) {
                    var tdbg;
                    var tdid;
                    var day, slot;

                    if (type == 'subject') {
                        id = 'preference_grid_sub';
                        fun_name = '';
                        html_text = '<i class="glyphicon glyphicon-plus cursor-pointer circle_plus_icon " ng-click="addPreSelect($event, \'' + type + '\', \'save\')"></i>';
                    } else if (type == 'teacher') {
                        id = 'preference_grid_teacher';
                        fun_name = '';
                        html_text = '<i class="glyphicon glyphicon-plus cursor-pointer circle_plus_icon " ng-click="addPreSelect($event, \'' + type + '\', \'save\')"></i>';
                    } else if (type == 'subject_no') {
                        div_id = 'subjectnotoallocate_div'
                        fun_name = 'allocateOrNot($event, \'' + div_id + '\')';
                        html_text = '';
                        id = 'subjectnotoallocate_div';
                    } else if (type == 'teacher_no') {
                        div_id = 'teachernotoallocate_div';
                        fun_name = 'allocateOrNot($event, \'' + div_id + '\')';
                        html_text = '';
                        id = 'teachernotoallocate_div';
                    }


                    if ($scope.time_break_detail_new[i][j + 1] == 'Y') {
                        tdbg = "url('assets/img/stbg.png')";
                        tdid = "";
                        day = "";
                        slot = "";
                        break_count = break_count + 1;
                        html_text = "";
                    } else {
                        tdbg = '#fff';
                        tdid = ($scope.min_day + i) + '_' + ((j + 1) - break_count);
                        //tdid = (i + 1) + '_' + (j + 1);
                        day = ($scope.min_day + i);
                        slot = (j + 1) - break_count;
                    }
                    html += '<td id="' + tdid + '" style="background: ' + tdbg + '" day="' + day + '" slot = "' + slot + '" class="slotWidth datatd nodata text-center" ng-click="' + fun_name + '">' + html_text + '</td>';
                }
            }
            html += '</tbody>';
            html += '</table>';

            var temp = $compile(html)($scope);
            $("#" + id).html(temp);

        }

        $scope.setSubPrefFromTT = function () {
            var a = {
                opr: 'U',
                aca_year: $scope.aca_year,
                bell_code: $scope.bell_code,
                grade_code: $scope.subject_grade_code,
                sec_code: $scope.subject_sec_code
            };
            $http.post(ENV.apiUrl + "api/PreferenceController/DaySlotBreakDetail", a).then(function (res) {
                //$scope.Allcurriculum = res.data;
                //$scope.cur_code = $scope.Allcurriculum[0].sims_cur_code;
                //$scope.getAcademicYear();
                $("#showmessage").html('<h6 class="text-white">Subject Preference Saved Successfully</h6>');
                setTimeout(function () {
                    $scope.messageShow = false;
                }, 1500);
                $scope.fetchSubSaveddata('section');
            });
        }

        $scope.addPreSelect = function (e, type, action) {
            var tdid = $(e.target).parent().attr('id');
            var day = $(e.target).parent().attr('day');
            var slot = $(e.target).parent().attr('slot');
            var resethtml, id;

            if (type == 'subject') {
                resethtml = '<i class="glyphicon glyphicon-plus cursor-pointer circle_plus_icon " ng-click="addPreSelect($event, \'subject\', \'save\')"></i>';
                id = 'preference_grid_sub';

                $scope.fetchSubjects('section', day, slot);

                if (action != 'edit') {
                    var html = '<select class="input-sm" data-role="dropdownlist"   ng-model="selsub_' + tdid + '" style="width: 94%;" ng-change="selectSubjectTeacher($event,\'subject\', selsub_' + tdid + ', \'' + tdid + '\')">';
                    html += '<option disabled selected ng-selected="true" value="">Select Subject</option>';
                    html += '<option ng-repeat="subject in getAllSubjects" value="{{subject.sims_subject_code}}">{{subject.sims_subject_name_en }}</option>';
                    html += '</select>';
                } else {
                    var sub = $(e.target).parent().attr('sub');

                    var html = '<select class="input-sm" data-role="dropdownlist"  ng-model="selsub_' + tdid + '" style="width: 94%;" ng-change="selectSubjectTeacher($event, \'subject\', selsub_' + tdid + ', \'' + tdid + '\')">';
                    html += '<option disabled  value="">Select Subject</option>';
                    html += '<option ng-selected="subject.sims_subject_code == ' + sub + '" ng-repeat="subject in getAllSubjects" value="{{subject.sims_subject_code}}">{{subject.sims_subject_name_en }}</option>';
                    html += '</select>';
                }
            } else {
                resethtml = '<i class="glyphicon glyphicon-plus cursor-pointer circle_plus_icon " ng-click="addPreSelect($event, \'teacher\', \'save\')"></i>';
                id = 'preference_grid_teacher';

                $scope.getTeachers(day, slot);

                if (action != 'edit') {
                    var html = '<select class="input-sm" data-role="dropdownlist"   ng-model="seltea_' + tdid + '" style="width: 94%;" ng-change="selectSubjectTeacher($event, \'teacher\', seltea_' + tdid + ', \'' + tdid + '\')">';
                    html += '<option disabled selected ng-selected="true" value="">Select Teacher</option>';
                    html += '<option ng-repeat="teacher in getAllTeacher" value="{{teacher.teacher_code}}">{{teacher.teacher_name}}</option>';
                    html += '</select>';
                } else {
                    var teacher = $(e.target).parent().attr('teacher');

                    var html = '<select class="input-sm" data-role="dropdownlist"   ng-model="seltea_' + tdid + '" style="width: 94%;" ng-change="selectSubjectTeacher($event, \'teacher\', seltea_' + tdid + ', \'' + tdid + '\')">';
                    html += '<option disabled selected  value="">Select Teacher</option>';
                    html += '<option ng-selected="subject.sims_subject_code == ' + teacher + '" ng-repeat="teacher in getAllTeacher" value="{{teacher.teacher_code}}">{{teacher.teacher_name}}</option>';
                    html += '</select>';
                }
            }

            var resettemp = $compile(resethtml)($scope);
            $("#" + id).find('.selectdropdown').html(resettemp).removeClass('selectdropdown');

            var temp = $compile(html)($scope);
            $(e.target).parent().html(temp).addClass('selectdropdown');
        }

        $scope.selectSubjectTeacher = function (e, type, val, tdid) {

            $("#" + tdid).removeClass("selectdropdown");

            if (type == 'subject') {
                var text_name = $("#preference_grid_sub #" + tdid).find('select option:selected').text();
                var html = '<i class="material-icons font-md pull-left line-height-40 b-grey b-r delete_sub_pre cursor-pointer" ng-click="deletePreferenceRow($event, \'single\', \'subject\')">clear</i><h6 class="no-margin semi-bold line-height-40 inline-block">' + text_name + '</h6><i class="material-icons pull-right b-grey b-l line-height-40 edit_sub_pre font-md cursor-pointer" ng-click="addPreSelect($event, \'subject\', \'edit\')">mode_edit</i> ';
                var temp = $compile(html)($scope);
                $("#preference_grid_sub #" + tdid).html(temp).attr({
                    sub: val,
                    sub_name: text_name
                }).addClass('filledsub').removeClass('nodata');
            } else {
                var text_name = $("#preference_grid_teacher #" + tdid).find('select option:selected').text();
                var html = '<i style="width:15%" class="material-icons font-md pull-left line-height-40 b-grey b-r delete_sub_pre cursor-pointer" ng-click="deletePreferenceRow($event, \'single\', \'teacher\')">clear</i><h6 style="width:70%" class="no-margin semi-bold line-height-40 pull-left ellipsis" title="' + text_name + '">' + text_name + '</h6><i class="material-icons pull-right b-grey b-l line-height-40 edit_sub_pre font-md cursor-pointer" style="width:15%" ng-click="addPreSelect($event, \'subject\', \'edit\')">mode_edit</i> ';
                var temp = $compile(html)($scope);
                $("#preference_grid_teacher #" + tdid).html(temp).attr({
                    teacher: val,
                    teacher_name: text_name
                }).addClass('filledteacher').removeClass('nodata');
            }
        }

        $scope.fetchTeacherSaveddata = function () {
            $scope.savedTeacherPreference();
            $scope.genGrid('teacher');
        }

        $scope.getGrade = function (str) {

            $http.post(ENV.apiUrl + "api/PreferenceController/ImportGrades?aca_year=" + $scope.aca_year + "&bell_code=" + str).then(function (res) {
                $scope.AllGrades = res.data;

            });
        };

        $scope.getsection = function (model) {
            $scope.sectionflag = '0';
            var grade_code;
            if (model == 'subject') {
                $scope.subject_sec_code = "";
                grade_code = $scope.subject_grade_code;
            } else if (model == 'teacher') {
                $scope.teacher_sec_code = "";
                grade_code = $scope.teacher_grade_code;
            } else if (model == 'subjectno') {
                $scope.sub_no_sec_code = "";
                grade_code = $scope.sub_no_grade_code;
            } else if (model == 'teacherno') {
                $scope.teacher_no_sec_code = "";
                grade_code = $scope.teacher_no_grade_code;
            }

            var a = {
                'opr': 'S',
                AcaYear: $scope.aca_year,
                cur_code: $scope.cur_code,
                grade_code: grade_code
            };

            $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableCommon", a).then(function (res) {
                if (model == 'subject') {
                    $scope.getSectionFromGrade = res.data.table;
                } else if (model == 'teacher') {
                    $scope.getSectionFromGradeTeacher = res.data.table;
                } else if (model == 'subjectno') {
                    $scope.getSectionFromGradeSubjectNo = res.data.table;
                } else if (model == 'teacherno') {
                    $scope.getSectionFromGradeTeacherNo = res.data.table;
                }


                $scope.sectionflag = '1';

            });

        };

        $scope.fetchSubjects = function (str, str1, str2) {
            $scope.getAllSubjects = "";
            $scope.getAllSubjectsGrid = "";
            var obj = [];
            if (str == 'bell') {
                obj = {
                    opr: 'A',
                    aca_year: $scope.aca_year,
                    bell_code: $scope.bell_code,
                    cur_code: $scope.cur_code
                };
            } else if (str == 'grade') {
                obj = {
                    opr: 'AA',
                    aca_year: $scope.aca_year,
                    bell_code: $scope.bell_code,
                    grade_code: $scope.sub_no_grade_code,
                    cur_code: $scope.cur_code
                };
            } else if (str == 'section') {
                obj = {
                    opr: 'p',
                    aca_year: $scope.aca_year,
                    bell_code: $scope.bell_code,
                    grade_code: $scope.subject_grade_code,
                    sec_code: $scope.subject_sec_code,
                    day_code: str1,
                    slot_code: str2,
                    cur_code: $scope.cur_code
                };
            } else if (str == 'sectiononly') {

                var grade_code = $("#sub_no_grade_select").val();
                var sec_code = $("#sub_no_sec_select").val();

                obj = {
                    opr: 'v',
                    aca_year: $scope.aca_year,
                    bell_code: $scope.bell_code,
                    grade_code: grade_code,
                    sec_code: sec_code
                };
            }

            console.log("This is data object =", obj);

            $http.post(ENV.apiUrl + "api/PreferenceController/fetchAllSubject", obj).then(function (res) {
                $scope.getAllSubjects = res.data;

                if ($scope.getAllSubjects.length > 0) {
                    $scope.preference = $scope.getAllSubjects[0].sims_subject_preference_code;
                    if (str == 'sectiononly') {
                        $scope.getAllSubjectsGrid = res.data;
                    }
                }


            });
        }

        $scope.getDaySlotBreakDetail = function () {
            var a = {
                opr: 'R',
                aca_year: $scope.aca_year,
                bell_code: $scope.bell_code
            };
            $http.post(ENV.apiUrl + "api/PreferenceController/DaySlotBreakDetail", a).then(function (res) {

                if (res.data.table.length > 0) {
                    $scope.slot_count = res.data.table[0]['slot_count'];
                    $scope.day_count = res.data.table1[0]['day_count'];
                    $scope.time_break_detail = res.data.table2;
                    $scope.myTmpData = res.data.table3;
                    $scope.min_day = parseInt(res.data.table4[0]['min_day'], 10);
                    var time_break_detail_new = new Array($scope.day_count);
                    var iTmp = -2;
                    $($scope.myTmpData).each(function (key, val) {
                        if (iTmp == -2 || iTmp < (parseInt(val.sims_bell_day_code, 10) - $scope.min_day)) {
                            iTmp = parseInt(val.sims_bell_day_code, 10) - $scope.min_day;
                            time_break_detail_new[parseInt(val.sims_bell_day_code, 10) - $scope.min_day] = new Array();
                            time_break_detail_new[parseInt(val.sims_bell_day_code, 10) - $scope.min_day][0] = undefined;
                        }
                        time_break_detail_new[parseInt(val.sims_bell_day_code, 10) - $scope.min_day][parseInt(val.sims_bell_slot_code, 10)] = val.sims_bell_break;
                    });
                    $scope.time_break_detail_new = time_break_detail_new;
                }

                $scope.loaderShow = false;
            });
        }

        $scope.getTeachers = function (str1, str2, str3) {
            var obj;
            if (str3 == 'teacheronly' || str3 == 'allteachers') {
                obj = {
                    opr: 'W',
                    aca_year: $scope.aca_year,
                    bell_code: $scope.bell_code,
                    cur_code: $scope.cur_code
                };
            } else {
                obj = {
                    opr: 'T',
                    aca_year: $scope.aca_year,
                    bell_code: $scope.bell_code,
                    grade_code: $scope.teacher_grade_code,
                    sec_code: $scope.teacher_sec_code,
                    day_code: str1,
                    slot_code: str2
                };
            }

            $http.post(ENV.apiUrl + "api/PreferenceController/fetchAllTeacher", obj).then(function (res) {
                $scope.getAllTeacher = res.data;
                if (str3 == 'teacheronly') {
                    $scope.getAllTeacherGrid = res.data;
                }
            });
        }

        $scope.savedTeacherPreference = function (str1, str2) {
            $scope.AllSavedTeaPre = "";

            var obj = {
                opr: 'F',
                aca_year: $scope.aca_year,
                bell_code: $scope.bell_code,
                grade_code: $scope.teacher_grade_code,
                sec_code: $scope.teacher_sec_code
            };

            $http.post(ENV.apiUrl + "api/PreferenceController/fetchTeacherPreference", obj).then(function (res) {
                $scope.AllSavedTeaPre = res.data;
                if ($scope.AllSavedTeaPre.length > 0) {
                    $($scope.AllSavedTeaPre).each(function (key, val) {
                        var html = '<i style="width:15%" class="material-icons font-md pull-left line-height-40 b-grey b-r delete_sub_pre cursor-pointer" ng-click="deletePreferenceRow($event, \'single\', \'teacher\')">clear</i><h6 style="width:70%" class="no-margin semi-bold line-height-40 inline-block ellipsis" title="' + val.teacher_name + '">' + val.teacher_name + '</h6><i style="width:15%" class="material-icons pull-right b-grey b-l line-height-40 edit_sub_pre font-md cursor-pointer" ng-click="addPreSelect($event,\'teacher\', \'edit\')">mode_edit</i>';
                        var temp = $compile(html)($scope);
                        $("#preference_grid_teacher #" + val.day_code + '_' + val.slot_code).html(temp).attr({
                            day: val.day_code,
                            slot: val.slotadd_code,
                            teacher: val.teacher_code,
                            teacher_name: val.teacher_name
                        }).addClass('filledteacher');
                    });
                }
            });
        }

        var options = [];

        $scope.selectSection = function (event) {

            var $target = $(event.currentTarget),
				val = $target.attr('data-value'),
				$inp = $target.find('input'),
				idx;

            if ((idx = options.indexOf(val)) > -1) {
                options.splice(idx, 1);
                setTimeout(function () {
                    $inp.prop('checked', false)
                }, 0);
            } else {
                options.push(val);
                setTimeout(function () {
                    $inp.prop('checked', true)
                }, 0);
            }

            $(event.target).blur();

            return false;
        }

        $scope.savePreference = function (type) {
            $scope.loaderShow = true;
            var arr_pre = [];
            var api_name;
            var loop_div_id;
            if (type == 'subject') {
                api_name = 'SaveSubjectPreference';
                loop_div_id = 'exceptionlevel .filledsub';
            } else {
                api_name = 'SaveTeacherPreference';
                loop_div_id = 'teacherpreference .filledteacher';
            }

            $("#" + loop_div_id).each(function (key, val) {

                var day_code = $(val).attr('day');
                var slot_code = $(val).attr('slot');
                var preference = '1';


                if (type == 'subject') {
                    var sub_code = $(val).attr('sub');
                    var sub_name = $(val).attr('sub_name');
                    arr_pre.push({
                        aca_year: $scope.aca_year,
                        bell_code: $scope.bell_code,
                        grade_code: $scope.subject_grade_code,
                        sec_code: $scope.subject_sec_code,
                        day_code: day_code,
                        slot_code: slot_code,
                        sub_code: sub_code,
                        preference: preference
                    });
                } else {
                    var teacher_code = $(val).attr('teacher');
                    var teacher_name = $(val).attr('teacher_name');
                    arr_pre.push({
                        aca_year: $scope.aca_year,
                        bell_code: $scope.bell_code,
                        grade_code: $scope.teacher_grade_code,
                        sec_code: $scope.teacher_sec_code,
                        day_code: day_code,
                        slot_code: slot_code,
                        teacher_code: teacher_code,
                        preference: preference
                    });
                }
            });


            if (arr_pre.length > 0) {
                $http.post(ENV.apiUrl + "api/PreferenceController/" + api_name, arr_pre).then(function (res) {
                    $scope.messageShow = true;
                    $scope.loaderShow = false;
                    $("#showmessage").html('<h6 class="text-white">Prefeence Saved Successfully</h6>');
                    setTimeout(function () {
                        $scope.messageShow = false;
                    }, 3000);
                });
            } else {
                alert("no data selected");
                $scope.loaderShow = false;
            }
        }

        $scope.savedSubjectPreference = function (str, str1) {

            $scope.AllSavedSubPre = "";
            var existing = [];
            var obj;
            if (str == 'section') {
                if (str1 == "subject_no") {
                    obj = {
                        opr: 'EC',
                        aca_year: $scope.aca_year,
                        bell_code: $scope.bell_code,
                        grade_code: $scope.sub_no_grade_code,
                        sec_code: $scope.sub_no_sec_code,
                        sub_code: $scope.sub_no_subject
                    };
                } else {
                    obj = {
                        opr: 'EF',
                        aca_year: $scope.aca_year,
                        bell_code: $scope.bell_code,
                        grade_code: $scope.subject_grade_code,
                        sec_code: $scope.subject_sec_code
                    };
                }
            }

            $http.post(ENV.apiUrl + "api/PreferenceController/fetchSubjectPreference", obj).then(function (res) {
                $scope.AllSavedSubPre = res.data;

                if ($scope.AllSavedSubPre.length > 0) {
                    if (str1 == 'subject_no') {
                        $($scope.AllSavedSubPre).each(function (key, val) {
                            var icon;
                            var text_color;
                            if (val.preference == '1') {
                                icon = 'done';
                                text_color = 'text-green';
                            } else if (val.preference == '2') {
                                //icon = 'help_outline';
                                //text_color = 'text-yellow';
                                icon = 'clear';
                                text_color = 'text-red';
                            } else if (val.preference == '3') {
                                icon = 'clear';
                                text_color = 'text-red';
                            }
                            var html = '<i class="material-icons ' + text_color + ' cursor-pointer">' + icon + '</i>';
                            var temp = $compile(html)($scope);
                            $("#subjectnotoallocate_div #" + val.day_code + '_' + val.slot_code).html(temp).attr({
                                day: val.day_code,
                                slot: val.slotadd_code,
                                sub: val.sub_code,
                                sub_name: val.sub_name,
                                preferencecode: val.preference,
                                clickcount: val.preference
                            }).addClass('hasdata').removeClass('nodata');
                        });
                    } else {
                        $($scope.AllSavedSubPre).each(function (key, val) {
                            var html = '<i class="material-icons font-md pull-left line-height-40 b-grey b-r delete_sub_pre cursor-pointer" ng-click="deletePreferenceRow($event, \'single\', \'subject\')">clear</i><h6 class="no-margin semi-bold inline-block ellipsis" style="max-width: 70%" title="' + val.sub_name + '">' + val.sub_name + '</h6><i class="material-icons pull-right b-grey b-l line-height-40 edit_sub_pre font-md cursor-pointer" ng-click="addPreSelect($event,\'subject\', \'edit\')">mode_edit</i>';
                            var temp = $compile(html)($scope);
                            $("#preference_grid_sub #" + val.day_code + '_' + val.slot_code).html(temp).attr({
                                day: val.day_code,
                                slot: val.slotadd_code,
                                sub: val.sub_code,
                                sub_name: val.sub_name
                            }).addClass('filledsub').removeClass('nodata');
                        });
                    }
                }
            });
        }

        $scope.deletePreferenceRow = function (e, type, str) {

            if (str == 'subject') {
                $(e.currentTarget).parents('td').removeClass('filledsub');
                var day_code = $(e.target).parents().attr('day');
                var slot_code = $(e.target).parents().attr('slot');
                var sub_code = $(e.target).parents().attr('sub');
                var preference = '1';

                var obj = {
                    Opr: 'H',
                    aca_year: $scope.aca_year,
                    bell_code: $scope.bell_code,
                    grade_code: $scope.subject_grade_code,
                    sec_code: $scope.subject_sec_code,
                    day_code: day_code,
                    slot_code: slot_code,
                    sub_code: sub_code,
                    preference: preference
                };

                $http.post(ENV.apiUrl + 'api/PreferenceController/RemovePreference', obj).then(function (res) {
                    var html = '<i class="glyphicon glyphicon-plus cursor-pointer circle_plus_icon " ng-click="addPreSelect($event,\'subject\', \'save\')"></i>';
                    var temp = $compile(html)($scope);
                    $scope['selsub_' + $(e.target).parent().attr('id')] = undefined;
                    $(e.target).parents('td').html(temp).attr({
                        sub: undefined,
                        sub_name: undefined
                    });
                });
            } else if (str == 'teacher') {
                $(e.target).parents('td').removeClass('filledteacher');
                var day_code = $(e.target).parents().attr('day');
                var slot_code = $(e.target).parents().attr('slot');
                var teacher_code = $(e.target).parents().attr('teacher');
                var preference = '1';


                var obj = {
                    Opr: 'J',
                    aca_year: $scope.aca_year,
                    bell_code: $scope.bell_code,
                    grade_code: $scope.teacher_grade_code,
                    sec_code: $scope.teacher_sec_code,
                    day_code: day_code,
                    slot_code: slot_code,
                    teacher_code: teacher_code,
                    preference: preference
                };

                $http.post(ENV.apiUrl + 'api/PreferenceController/RemovePreference', obj).then(function (res) {
                    var html = '<i class="glyphicon glyphicon-plus cursor-pointer circle_plus_icon " ng-click="addPreSelect($event,\'teacher\', \'save\')"></i>';
                    var temp = $compile(html)($scope);
                    $scope['seltea_' + $(e.target).parent().attr('id')] = undefined;
                    $(e.target).parents('td').html(temp).attr({
                        teacher: undefined,
                        teacher_name: undefined
                    });
                });
            }

        }

        $scope.selectSubPreLevel = function (e, str) {
            $("#subjectPreferenceTab span").removeClass('btn-primary').addClass('btn-default');
            $scope.fetchSubjects(str);
            //if ($scope.radiofilter == 'all') {
            $scope.getTeachers('', '', 'allteachers')
            //}
            $(".tab-pane").removeClass('active').fadeOut();
            $(e.currentTarget).removeClass('btn-default').addClass('btn-primary');
            var id = $(e.currentTarget).data('id');
            $(id).addClass('active').fadeIn();
            $scope.getRules();
        }

        $scope.schoolLevelPreferenceSave = function () {
            var prefArr = [];
            $(".scoollevelsubpre").each(function (key, val) {
                var subname = $(val).find('.subName').text();
                var subcode = $(val).find('.subName').data('subcode');
                var subtype = $(val).find('.subName').data('type');
                var pref = $(val).find('.S_L_preference').val();
                if (pref != null && pref != undefined) {
                    prefArr.push({
                        sub_code: subcode,
                        preference: pref
                    });
                }
            });

            $http.post(ENV.apiUrl + 'api/PreferenceController/schoolLevelSubPref', prefArr).then(function (res) {
                $scope.messageShow = true;
                $("#showmessage").html('<h6 class="text-white">School Level Preference Saved Successfully</h6>');
                setTimeout(function () {
                    $scope.messageShow = false;
                }, 3000);
            });
        }

        $scope.saveRules = function () {
            $scope.loaderShow = true;
            var appl_code_CT, field_value_CT;
            var appl_code_SP, field_value_SP;
            var appl_code_TP, field_value_TP;

            if ($("#classT").is(':checked')) {
                appl_code_CT = 'Y';
                field_value_CT = 'Yes';
            } else {
                appl_code_CT = 'N';
                field_value_CT = 'No';
            }

            var fp = $('input[name=pre]:checked').val();

            if (fp == 'teacher') {
                appl_code_TP = '1';
                appl_code_SP = '2';

                field_value_TP = '1';
                field_value_SP = '2';
            } else {
                appl_code_TP = '2';
                appl_code_SP = '1';

                field_value_TP = '2';
                field_value_SP = '1';
            }

            a = {
                opr: 'O',
                appl_code_CT: appl_code_CT,
                field_value_CT: field_value_CT,
                appl_code_TP: appl_code_TP,
                field_value_TP: field_value_TP,
                appl_code_SP: appl_code_SP,
                field_value_SP: field_value_SP
            };

            $http.post(ENV.apiUrl + "api/PreferenceController/saveRules", a).then(function (res) {
                if (res.data) {
                    $scope.loaderShow = false;
                    $scope.messageShow = true;
                    $("#showmessage").html('<h6 class="text-white">Rules Saved Successfully</h6>');
                    setTimeout(function () {
                        $scope.messageShow = false;
                    }, 3000);
                }
            });
        }

        $scope.fetchSubNodata = function () {
            $scope.getDaySlotBreakDetail();
            $scope.genGrid('subject_no');
            $scope.savedSubjectPreference('section', 'subject_no');
        }

        $scope.fetchAllSubject = function () {
            $scope.sub_no_grade_code = "";
            $scope.fetchSubjects('bell');
        }

        $scope.fetchSubSection = function (str) {
            $scope.sub_no_subject = "";
            $scope.getsection(str, 'subjectno');
            $scope.fetchSubjects('grade');
        }

        $scope.fetchTeacherNodata = function (e) {
            $scope.getTeachers('', '', 'teacheronly');
            $scope.getDaySlotBreakDetail();
            $scope.genGrid('teacher_no');
        }

        $scope.fetchSubData = function () {
            $scope.sub_no_subject = "";
            $scope.fetchSubjects('sectiononly');
        }

        $scope.allocateOrNot = function (e, div_id) {

            var click_old = $(e.target).parent().attr('clickcount') == undefined ? $(e.target).attr('clickcount') : $(e.target).parent().attr('clickcount');
            var tdid = $(e.target).attr('id');
            if (e.target.tagName == 'I') {
                tdid = $(e.target).parents().attr('id');
            }
            if ($scope.prev_td != tdid) {

                if (typeof click_old !== typeof undefined && click_old !== false) {
                    var newcount = parseInt(click_old, 10) + 1;
                    newcount = newcount % 3;

                    if (newcount == 1) {
                        $("#" + div_id + " #" + tdid).html('<i class="material-icons text-green cursor-pointer">done</i>');

                    } else if (newcount == 2) {
                        //$("#" + div_id + " #" + tdid).html('<i class="material-icons text-yellow cursor-pointer">help_outline</i>');
                        $("#" + div_id + " #" + tdid).html('<i class="material-icons text-red cursor-pointer">clear</i>');
                        //} else if (newcount == 3) {
                        //    $("#" + div_id + " #" + tdid).html('<i class="material-icons text-red cursor-pointer">clear</i>');
                    } else {
                        $("#" + div_id + " #" + tdid).html('');
                    }
                    $("#" + div_id + " #" + tdid).attr({
                        'clickcount': newcount,
                        'preferencecode': newcount
                    }).addClass('hasdata').removeClass('nodata');
                } else {
                    $("#" + div_id + " #" + tdid).attr({
                        'clickcount': 1,
                        'preferencecode': 1
                    }).addClass('hasdata').removeClass('nodata');
                    $("#" + div_id + " #" + tdid).html('<i class="material-icons text-green cursor-pointer">done</i>');
                }
            } else {
                if (typeof click_old !== typeof undefined && click_old !== false) {
                    var newcount = parseInt(click_old) + 1;
                    //if (newcount > 3) {
                    //    newcount = 1;
                    //}
                    //if (newcount == 2) {
                    //    $("#" + div_id + " #" + tdid).html('<i class="material-icons text-yellow cursor-pointer">help_outline</i>');
                    //} else if (newcount == 3) {
                    //    $("#" + div_id + " #" + tdid).html('<i class="material-icons text-red cursor-pointer">clear</i>');
                    //} else {
                    //    $("#" + div_id + " #" + tdid).html('<i class="material-icons text-green cursor-pointer">done</i>');

                    //}
                    newcount = newcount % 3;

                    if (newcount == 1) {
                        $("#" + div_id + " #" + tdid).html('<i class="material-icons text-green cursor-pointer">done</i>');

                    } else if (newcount == 2) {
                        //$("#" + div_id + " #" + tdid).html('<i class="material-icons text-yellow cursor-pointer">help_outline</i>');
                        $("#" + div_id + " #" + tdid).html('<i class="material-icons text-red cursor-pointer">clear</i>');
                        //} else if (newcount == 3) {
                        //    $("#" + div_id + " #" + tdid).html('<i class="material-icons text-red cursor-pointer">clear</i>');
                    } else {
                        $("#" + div_id + " #" + tdid).html('');
                    }

                    $("#" + div_id + " #" + tdid).attr({
                        'clickcount': newcount,
                        'preferencecode': newcount
                    }).addClass('hasdata').removeClass('nodata');
                } else {
                    $("#" + div_id + " #" + tdid).attr({
                        'clickcount': 1,
                        'preferencecode': 1
                    }).addClass('hasdata').removeClass('nodata');
                    $("#" + div_id + " #" + tdid).html('<i class="material-icons text-green cursor-pointer">done</i>');
                }
            }
            $scope.prev_td = tdid;
        }

        $scope.saveSubjectNoAllocate = function () {
            $scope.loaderShow = true;
            $scope.arr = [];
            if ($scope.radiofilter == 'section') {
                $("#subjectnotoallocate_div").find(".grid_row .hasdata").each(function (k, v) {
                    var day = $(v).attr('day');
                    var slot = $(v).attr('slot');
                    var pre_code = $(v).attr('preferencecode');
                    $scope.arr.push({
                        aca_year: $scope.aca_year,
                        bell_code: $scope.bell_code,
                        cur_code: $scope.cur_code,
                        preference: pre_code,
                        grade_code: $scope.sub_no_grade_code,
                        sec_code: $scope.sub_no_sec_code,
                        sub_code: $scope.sub_no_subject,
                        day_code: day,
                        slot_code: slot
                    });
                });
            }
            if ($scope.arr.length > 0) {
                setTimeout(function () {
                    $http.post(ENV.apiUrl + "api/PreferenceController/SubjectAllocationPreference", $scope.arr).then(function (res) {
                        if (res.data == true) {
                            $scope.loaderShow = false;
                            $scope.messageShow = true;
                            $("#showmessage").html('Preferences saved successfully !');
                            setTimeout(function () {
                                $scope.messageShow = false;
                            }, 3000);
                        }
                    });
                }, 2000);
            }
        }

        $scope.saveTeacherNoAllocate = function () {
            $scope.loaderShow = true;
            var arr = [];
            var teacher_code = $("#teacher_no").val();
            $("#teachernotoallocate_div").find(".grid_row .hasdata").each(function (k, v) {
                var pre_code;
                var day = $(v).attr('day');
                var slot = $(v).attr('slot');
                pre_code = $(v).attr('preferencecode');

                if (pre_code != '0') {
                    arr.push({
                        aca_year: $scope.aca_year,
                        cur_code: $scope.cur_code,
                        bell_code: $scope.bell_code,
                        teacher_code: teacher_code,
                        preference: pre_code,
                        day_code: day,
                        slot_code: slot
                    });
                }
            });
            if (arr.length > 0) {
                $http.post(ENV.apiUrl + "api/PreferenceController/TeacherAllocationPreference", arr).then(function (res) {
                    $scope.loaderShow = false;
                    $scope.messageShow = true;
                    $("#showmessage").html('Preference Allocation saved successfully!');
                    setTimeout(function () {
                        $scope.messageShow = false;
                    }, 3000);
                })
            }
        }

        $scope.fetchTeacherAllocationData = function () {
            $scope.genGrid('teacher_no');
            $scope.savedTeacherAlloPreference();
        }

        $scope.savedTeacherAlloPreference = function () {

            a = {
                opr: 'AC',
                aca_year: $scope.aca_year,
                bell_code: $scope.bell_code,
                teacher_code: $scope.teacher_no,
                cur_code: $scope.cur_code
            };
            $http.post(ENV.apiUrl + "api/PreferenceController/savedTeacherAlloPreference", a).then(function (res) {
                $scope.TeacherAllocationPreferences = res.data;
                $($scope.TeacherAllocationPreferences).each(function (k, v) {

                    var icon, text_color;
                    if (v.preference == '1') {
                        icon = 'done';
                        text_color = 'text-green';
                    } else if (v.preference == '2') {
                        //icon = 'help_outline';
                        // text_color = 'text-yellow';
                        icon = 'clear';
                        text_color = 'text-red';
                    } else if (v.preference == '3') {
                        icon = 'clear';
                        text_color = 'text-red';
                    }
                    var html = '<i class="material-icons ' + text_color + ' cursor-pointer">' + icon + '</i>';
                    $("#teachernotoallocate_div #" + v.day_code + "_" + v.slot_code).html(html).attr({
                        day: v.day_code,
                        slot: v.slot_code,
                        preferencecode: v.preference,
                        clickcount: v.preference
                    }).addClass('hasdata').removeClass('nodata');

                })
            })
        }

        $scope.getdays = function () {
            var a = {
                opr: 'Q'
            };
            $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableCommon", a).then(function (res) {
                $scope.oSlotDayNamestemp = res.data.table;
                $scope.oSlotDayNames = [];
                $($scope.oSlotDayNamestemp).each(function (k, v) {
                    $scope.oSlotDayNames[parseInt(v.sims_appl_parameter) - 1] = v.sims_appl_form_field_value1;
                });
            });
        }

        $(document).ready(function () {
            $scope.radiofilter = 'section';
            $scope.getCurriculum();
            $scope.condenseMenu();
            $scope.getdays();
            $scope.AllPreferences = [1, 2, 3, 4, 5, 6, 7, 8, 9];
            $scope.getRules();
            $scope.loaderShow = false;
            $scope.messageShow = false;
        });

    }]);
})();