﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.TimeTable');

    simsController.controller('LectureSubstitutionCont', ['$scope', '$compile', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$locale', '$http', 'ENV', function ($scope, $compile, $state, $rootScope, $timeout, gettextCatalog, $locale, $http, ENV) {
        
        $scope.getAssignedData = function () {
            var a = {
                opr: 'F',
                aca_year: $scope.aca_year,
                bell_code: $scope.bell_code,
                teacher_code: $scope.abteacher_code
            };

            $http.post(ENV.apiUrl + "api/LectureSubstitution/LectureSubstitutionCommon", a).then(function (res) {
                $scope.AllAssignedData = res.data.table;
                $scope.assignFlag = true;
            });
        }
        
        $scope.getAcademicYear = function () {
            var a = {
                opr: 'A',
                cur_code: ''
            };
            $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableCommon", a).then(function (res) {

                $scope.AllAcademicYear = res.data.table;
                $($scope.AllAcademicYear).each(function (k, v) {
                    if (v.sims_academic_year_status == 'C') {
                        $scope.aca_year = v.sims_academic_year;
                        $scope.getBellData();
                        return false;
                    }
                });
            });
        };

        $scope.getBellData = function () {
            var a = {
                Opr: 'B',
                AcaYear: $scope.aca_year,
                cur_code: ''
            };
            $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableImport", a).then(function (getBellData) {

                $scope.AllBellData = getBellData.data.table;
                if (getBellData.data.table.length > 0) {
                    $scope.bell_code = $scope.AllBellData[0].sims_bell_code;
                } else {
                    $scope.bell_code = "";
                }
            });
        }

        $scope.getTeachers = function () {
            var a = {
                opr: 'A',
                aca_year: $scope.aca_year,
                bell_code: $scope.bell_code
            };
            $http.post(ENV.apiUrl + "api/LectureSubstitution/LectureSubstitutionCommon", a).then(function (res) {
                $scope.AllTeachers = res.data.table;
            });
        }

        $scope.getAbsentTeachers = function () {
            debugger
            $http.post(ENV.apiUrl + "api/LectureSubstitution/fetchAbsentTeacher?aca_year=" + $scope.aca_year + "&bell_code=" + $scope.bell_code + "&absentdate=" + $scope.filter_date).then(function (res) {
                $scope.AllAbsentTeachers = res.data;
            });
        }

        $scope.getFreeTeachers = function (tdid) {
            debugger
            var a = {
                opr: 'C',
                aca_year: $scope.aca_year,
                bell_code: $scope.bell_code,
                day_code: $scope.day_code,
                slot_code: $scope.slot_code,
                teacher_code: $scope.abteacher_code,
                filter_date: $scope.filter_date.split("-").reverse().join("-")
            };
            $http.post(ENV.apiUrl + "api/LectureSubstitution/LectureSubstitutionCommon", a).then(function (res) {
                $scope.AllFreeTeachers = res.data.table;
                var html = '<select class="input-sm m-t-5 m-b-5" style="width:90%; margin-left: 5%" ng-model="assignedTeacher" ng-change="assignSubstitution(assignedTeacher, \'' + tdid + '\')">';
                html += '<option selected disabled value="">Select Teacher</option>';
                html += '<option ng-repeat="fteacher in AllFreeTeachers" data-slotgroup="{{fteacher.sims_bell_slot_group}}" data-slotroom="{{fteacher.sims_bell_slot_room}}" data-subject="{{fteacher.sims_subject_name_en}}" data-subjectcode="{{fteacher.sims_bell_subject_code}}" data-teachername = "{{fteacher.sims_teacher_name}}" value="{{fteacher.sims_teacher_code}}">{{fteacher.sims_teacher_name + " - " +fteacher.sims_subject_name_en + " - " +fteacher.worklod}}</option>';
                html += '</select>';
                var temp = $compile(html)($scope);
                $("#" + tdid).html(temp);

            });
        }

        $scope.loadData = function () {
            $scope.getGradeSection();
            $scope.genTeachersView();
        }

        $scope.loadReasonData = function () {
            debugger
            var a = {
                Opr: 'J'
            };
            $http.post(ENV.apiUrl + "api/LectureSubstitution/LectureSubstitutionCommon", a).then(function (res) {

                $scope.AllReason = res.data.table;
                $scope.reason = $scope.AllReason[0].sims_appl_parameter;
                $scope.loadData();
            });
        }

        $scope.getGradeSection = function () {
            debugger
            var a = {
                Opr: 'C',
                AcaYear: $scope.aca_year,
                bellCode: $scope.bell_code
            };
            $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableInitial", a).then(function (res) {

                $scope.iSlotCount = [];
                for (var i = 0; i < res.data.table.length; i++) {
                    $scope.iSlotCount[i + 1] = res.data.table[i].column1
                }

                $scope.iSlotDay = res.data.table1[0].column1;

                $scope.AllGradeSec = res.data.table2;

            });
        };

        $scope.genTeachersView = function (str) {
            debugger
            var d = new Date($scope.filter_date.split('-').reverse().join('-'));
            $scope.day_code = d.getDay() + 1;
            $scope.assignFlag = false;
            $scope.day = $scope.oSlotDayNames[$scope.day_code - 1];

            $scope.getAssignedData();

            $(".table .contenttd").html('');
            $scope.teacher_code = str;
            var a = {
                opr: 'I',
                aca_year: $scope.aca_year,
                bell_code: $scope.bell_code,
                day_code: $scope.day_code,
                teacher_code: $scope.abteacher_code
            };

            var aca_name = $("#aca_select option:selected").text();
            var bell_name = $("#bell_select option:selected").text();
            $http.post(ENV.apiUrl + "api/LectureSubstitution/LectureSubstitutionCommon", a).then(function (res) {

                var tname = $("#teacher_select option:selected").text();

                $scope.showGrid = true;
                $scope.gridData = res.data.table2;
                $scope.slot_count = res.data.table[0].column1;
                $scope.day_count = res.data.table1[0].column1;
                $scope.time_break_detail = res.data.table3;
                $scope.genGrid();

            }).then(function () {
                $($scope.gridData).each(function (k, v) {
                    var html = '<div class="sub_tec_cont"  ng-mouseenter = "assignToOtherShow($event, \'div\')"  data-grade="' + v.sims_bell_grade_code + '" data-section="' + v.sims_bell_section_code + '" data-subcode="' + v.sims_bell_subject_code + '"><div class="cvsubname ellipsis" title="' + v.sims_subject_name_en + '" ng-mouseenter = "assignToOtherShow($event, \'div\')">' + v.sims_subject_name_en + '</div></div>';
                    var temp = $compile(html)($scope);
                    if ($("#" + v.sims_bell_grade_code + '_' + v.sims_bell_section_code + '_' + v.sims_bell_slot_code).children('.sub_tec_cont').length > 0) {
                        $("#" + v.sims_bell_grade_code + '_' + v.sims_bell_section_code + '_' + v.sims_bell_slot_code).addClass('group');
                    }
                    $("#" + v.sims_bell_grade_code + '_' + v.sims_bell_section_code + '_' + v.sims_bell_slot_code).addClass('occupied').append(temp);
                    if ($("#" + v.sims_bell_grade_code + '_' + v.sims_bell_section_code + '_' + v.sims_bell_slot_code).children('.lec_sub').length > 0) { } else {
                        $("#" + v.sims_bell_grade_code + '_' + v.sims_bell_section_code + '_' + v.sims_bell_slot_code).append($compile('<div class="lec_sub text-white cursor-pointer" ng-click="allocatetoOther($event)" style="display:none">Assign to Other Teacher</div>')($scope));
                        $("#" + v.sims_bell_grade_code + '_' + v.sims_bell_section_code + '_' + v.sims_bell_slot_code).attr('subject_code', v.sims_bell_subject_code);
                    }
                });
                if ($scope.assignFlag == true) {
                    $($scope.AllAssignedData).each(function (k, v) {
                        if ($("#" + v.sims_bell_grade_code + '_' + v.sims_bell_section_code + '_' + v.sims_bell_slot_code).find('h6').length == 0) {
                            $("#" + v.sims_bell_grade_code + '_' + v.sims_bell_section_code + '_' + v.sims_bell_slot_code).prepend('<h6 class="m-t-0 m-b-0 bg-grey-light b-b b-grey-md p-b-5 p-t-5 bold"> Assigned to: ' + v.sims_teacher_name + '</h6>').attr({
                                'subs-teacher-code': v.sims_bell_substitution_teacher_code,
                                'subs-subject-code': v.sims_bell_substitution_subject_code,
                                'slotroom': v.sims_bell_slot_room,
                                'slotgroup': v.sims_bell_slot_group
                            }).addClass('assigned');
                            $("#" + v.sims_bell_grade_code + '_' + v.sims_bell_section_code + '_' + v.sims_bell_slot_code).find('.cvsubname ').text(v.sims_subject_name_en);
                        }
                    });
                }
            });
        }

        $scope.allocatetoOther = function (e) {
            debugger
            var tdid = $(e.currentTarget).parents('td').attr('id');
            $scope.oldhtml = $("#" + tdid).html();
            if ($("#" + tdid).find('h6').length > 0) {
                $scope.alreadyAssigned = true;
            } else {
                $scope.alreadyAssigned = false;
            }

            // original code
            $scope.slot_code = $('#' + tdid).data('slotcode');
            console.log($scope.slot_code);

            $("#" + tdid).html('<span class="loadersmall"></span>');
            $scope.getFreeTeachers(tdid);

        }

        $scope.assignSubstitution = function (str, tdid, oldhtml) {
            var selected_teacher = $("#" + tdid).find('option:selected').data('teachername');
            var selected_code = $("#" + tdid).find('option:selected').val();
            var sub_name = $("#" + tdid).find('option:selected').data('subject');
            var sub_code = $("#" + tdid).find('option:selected').data('subjectcode');
            var slotgroup = $("#" + tdid).find('option:selected').data('slotgroup');
            var slotroom = $("#" + tdid).find('option:selected').data('slotroom');

            var temp = $compile($scope.oldhtml)($scope);

            if ($scope.alreadyAssigned) {
                $("#" + tdid).html(temp);
                $("#" + tdid).find('h6').text('Assigned to: ' + selected_teacher);
            } else {
                $("#" + tdid).html(temp).prepend('<h6 class="m-t-0 m-b-0 bg-grey-light b-b b-grey-md p-b-5 p-t-5 bold"> Assigned to: ' + selected_teacher + '</h6>');
            }

            $("#" + tdid).find('.sub_tec_cont').each(function (k, v) {
                $(v).find('.cvsubname').text(sub_name);
            });

            $scope.assignedTeacher = "";

            $("#" + tdid).addClass("assigned").attr({
                'subs-teacher-code': selected_code,
                'subs-subject-code': sub_code,
                'slotroom': slotroom,
                'slotgroup': slotgroup
            });
        }


        $scope.assignToOtherShow = function (e, tag) {
            if (tag == 'td') {
                $(e.target).parent().siblings().find('.lec_sub').hide();
                $(e.target).find('.lec_sub').show();
            } else {
                $(e.target).parents('td').siblings().find('.lec_sub').hide();
                $(e.target).parents('td').find('.lec_sub').show();
            }
        }

        $scope.genGrid = function () {
            debugger
            $scope.clickAttr = "";
            var html = '<table id="substitution" class="table table-hover table-bordered tttable">';
            html += '<thead>';
            html += '<tr>';
            html += '<th class="nobg">D/Lec</th>';
            for (var i = 0; i < $scope.slot_count; i++) {
                var lec_c;
                if ($scope.time_break_detail[i]['sims_bell_break'] == 'Y') {
                    lec_c = $scope.time_break_detail[i]['sims_bell_slot_desc'] + ' [Break]';
                } else {
                    lec_c = $scope.time_break_detail[i]['sims_bell_slot_desc'];
                }

                html += '<th class="nobg slotWidth text-center"> ' + lec_c + '<span class="display_block font-sm">[' + $scope.time_break_detail[i]['sims_bell_start_time'] + ' to ' + $scope.time_break_detail[i]['sims_bell_end_time'] + ']</span></th>';

            }
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            $($scope.AllGradeSec).each(function (key, val) {

                html += '<tr class="trrow bg-green" id="' + val.sims_bell_grade_code + "_" + val.sims_bell_section_code + '" data-gradecode="' + val.sims_bell_grade_code + '" data-seccode="' + val.sims_bell_section_code + '" data-grade="' + val.sims_grade_name_en + '" data-secname="' + val.sims_section_name_en + '"> <td class="cellWidth text-white grade_fix ellipsis" title="' + val.sims_grade_name_en + " " + val.sims_section_name_en + '" > ' + val.sims_grade_name_en + " " + val.sims_section_name_en + '</td>';
                //var break_count = 0;
                for (var j = 0; j < $scope.slot_count; j++) {
                    var tdbg, tdid, slot;

                    if ($scope.time_break_detail[j]['sims_bell_break'] == 'Y') {
                        tdbg = "url('assets/img/stbg.png')";
                        tdid = "";
                     //   break_count = break_count + 1;
                        slot = "";
                    } else {
                        tdbg = '#fff';
                        tdid = val.sims_bell_grade_code + "_" + val.sims_bell_section_code + '_' + ((j + 1));//- break_count
                        //  slot = ((j + 1) - break_count);  //old already commeted code
                        slot = $scope.time_break_detail[j].sims_bell_slot_code;
                    }

                    html += '<td id="' + tdid + '" ng-mouseenter = "assignToOtherShow($event, \'td\')" data-slotcode= "' + slot + '" style="background: ' + tdbg + '" class="slotWidth contenttd text-center position-relative" ></td>';

                }
                html += '</tr>';
            });
            html += '</tbody>';
            html += '</table>';

            var temp = $compile(html)($scope);
            $("#TVgrid").html(temp);
        }

        $scope.saveSubstitution = function () {
            $scope.loaderShow = true;
            var data_arr = [];
            $(".assigned .sub_tec_cont").each(function (k, v) {
                var tdid = $(v).parents('td').attr('id');
                var id_arr = tdid.split("_");
                var slot_code = $("#" + tdid).data('slotcode');
                var subs_teacher_code = $("#" + tdid).attr('subs-teacher-code');
                var subs_subject_code = $("#" + tdid).attr('subs-subject-code');
                var slot_group = $("#" + tdid).attr('slotgroup');
                var slot_room = $("#" + tdid).attr('slotroom');
                var subject_code = $("#" + tdid).attr('subject_code');
                var grade_code = $(v).data('grade');
                var sec_code = $(v).data('section');

                data_arr.push({
                    aca_year: $scope.aca_year,
                    bell_code: $scope.bell_code,
                    grade_code: grade_code,
                    sec_code: sec_code,
                    day_code: $scope.day_code,
                    slot_code: slot_code,
                    slot_group: slot_group,
                    slot_room: slot_room,
                    teacher_code: $scope.abteacher_code,
                    subject_code: subject_code,
                    filter_date: $scope.filter_date,
                    subs_teacher_code: subs_teacher_code,
                    subs_subject_code: subs_subject_code,
                    substitution_status: 'A',
                    substitution_acknowledgement_status: 'P',
                    substitution_remark: $scope.reason
                });
            });

            $http.post(ENV.apiUrl + "api/LectureSubstitution/SaveSubjectSubstitution", data_arr).then(function (res) {

                if (res.data == true) {
                    $scope.loaderShow = false;
                    $scope.messageShow = true;
                    $scope.message = "Substitution Saved Sucessfully";
                }

            });
        }

        $scope.getdays = function () {
            var a = {
                opr: 'Q'
            };
            $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableCommon", a).then(function (res) {
                $scope.oSlotDayNamestemp = res.data.table;
                $scope.oSlotDayNames = [];
                $($scope.oSlotDayNamestemp).each(function (k, v) {
                    $scope.oSlotDayNames[parseInt(v.sims_appl_parameter) - 1] = v.sims_appl_form_field_value1;
                });
            });
        }

        $(document).ready(function () {
            $scope.getAcademicYear();
            $scope.condenseMenu();
            $scope.getdays();
            $scope.loaderShow = false;
        });
    }]);
})();