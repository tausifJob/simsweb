﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.TimeTable');

    simsController.controller('FreeTeacherCont',
        ['$scope', '$compile', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$locale', '$http', 'ENV', function ($scope, $compile, $state, $rootScope, $timeout, gettextCatalog, $locale, $http, ENV) {
            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
            dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; 
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();          
            $scope.filter_date = dd + '-' + mm + '-' + yyyy;

            $scope.getCurriculum = function () {
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                    $scope.Allcurriculum = res.data;
                    $scope.cur_code = $scope.Allcurriculum[0].sims_cur_code;
                    $scope.getAcademicYear();
                });
            }

            $scope.getAcademicYear = function () {

                var a = { opr: 'A', cur_code: $scope.cur_code };
                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableCommon", a).then(function (res) {

                    $scope.AllAcademicYear = res.data.table;
                    $($scope.AllAcademicYear).each(function (k, v) {
                        if (v.sims_academic_year_status == 'C') {
                            $scope.aca_year = v.sims_academic_year;
                            $scope.getBellData();
                            return false;
                        }
                    });
                });
            };

            $scope.getTeachers = function () {
                $scope.teacher_code = "";
                var a = { opr: 'O', AcaYear: $scope.aca_year, bellCode: $scope.bell_code };
                $http.post(ENV.apiUrl + "api/FreeTeacher/TimeTableView", a).then(function (res) {
                    $scope.AllTeachers = res.data.table;
                    $scope.teacher_code = $scope.AllTeachers[0].sims_bell_teacher_code;
                    $scope.genTeachersView();
                });
            }

            $scope.getBellData = function () {
                var a = { Opr: 'B', AcaYear: $scope.aca_year, cur_code: $scope.cur_code };
                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableImport", a).then(function (getBellData) {

                    $scope.AllBellData = getBellData.data.table;
                    if (getBellData.data.table.length > 0) {
                        $scope.bell_code = $scope.AllBellData[0].sims_bell_code;
                    }
                    else {
                        $scope.bell_code = "";
                    }
                    $scope.loadLectures();
                   
                });

                
            }

            $scope.loadLectures = function () {
                var a = { opr: 'A', aca_year: $scope.aca_year, bell_code: $scope.bell_code };
                $http.post(ENV.apiUrl + "api/FreeTeacher/FreeTeacherCommon", a).then(function (res) {
                    $scope.slots = [];
                    var i;
                    for (i = 1 ; i <= res.data.table[0].slotCount; i++) {
                        $scope.slots.push(i);
                    }
                    
                    $scope.$watch('slots[0]', function () {
                        $scope.slot_code = $scope.slots[0].toString();
                        
                        $scope.DataLoadAfterLecture();
                    });
                    
                    

                })
            }

            $scope.DataLoadAfterLecture = function () {
                var d = new Date($scope.filter_date.split('-').reverse().join('-'));
                $scope.day_code = d.getDay() + 1;
                $scope.send_date = yyyy + '-' + mm + '-' + dd;
                
                var a = { opr: 'B', cur_code: $scope.cur_code, aca_year: $scope.aca_year, bell_code: $scope.bell_code, filter_date: $scope.send_date, slot_code: $scope.slot_code, day_code: $scope.day_code };
                console.log("This is object =", a);
                $http.post(ENV.apiUrl + "api/FreeTeacher/FreeTeacherCommon", a).then(function (res) {
                    $scope.teacherList = res.data.table;
                    $scope.datashow = true;
                })
            }

            $(document).ready(function () {
                $scope.getCurriculum();
                $scope.condenseMenu();
                $scope.datashow = false;
              
            });

            //$scope.getWeekNumber = function(d) {
            //    d = new Date(+d);
            //    d.setHours(0, 0, 0);
            //    d.setDate(d.getDate() + 4 - (d.getDay() || 7));
           
            //    var yearStart = new Date(d.getFullYear(), 0, 1);
               
            //    var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7)
            //    return [d.getFullYear(), weekNo];
            //}

            //$scope.weeksInYear = function(year) {
            //    var month = 11, day = 31, week;
            //    do {    
            //        var d = new Date(year, month, day--);
            //        $scope.weeks = $scope.getWeekNumber(d)[1];
            //    } while ($scope.weeks == 1);

            //    console.log("This is jajajajajja=", $scope.weeks);
           
            //}

           




        }]);
})();

