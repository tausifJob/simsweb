﻿var allowDrop = function (ev) {
    if (!$('body').hasClass("viewtt")) {
        $('td.dropabletd').css({
            'background': "transparent"
        });

        if (dragstartsfrom.tagName == 'TD') {
            if (ev.target.parentNode.id == dragstartsfrom.parentNode.id) {
                $(ev.target).css({
                    'background': "#ccc"
                });
            }
        } else {

            var str = dragstartsfrom.id;
            if (ev.target.parentNode.id == pendingtrid) {
                $(ev.target).css({
                    'background': "#ccc"
                });
            }
        }
        ev.preventDefault();
    }

};

var teacherBlockData;
var dragstartsfrom;
var pendingtrid;
var teachercode;
var drag = function (ev) {
    if (!$('body').hasClass("viewtt")) {
        teachercode = $(ev.target).children('.hoverdiv').data('teachercode');
        if (ev.target.parentNode.id == 'pendingsubid') {
            pendingtrid = $(ev.target).attr('trid');
            $(ev.target).addClass('filled');
            dragstartsfrom = document.getElementById(ev.target.parentNode.id);
            ev.dataTransfer.setData("text", ev.target.id);
        } else {
            dragstartsfrom = ev.target.parentNode;
            ev.dataTransfer.setData("text", ev.target.id);
        }
    }
};

var drop = function (ev) {
    if (!$('body').hasClass("viewtt")) {
        var trid = $(ev.target).parents('tr').attr('id');
        var str2;
        if (dragstartsfrom.tagName == 'TD') {
            str2 = dragstartsfrom.parentNode.id;
        } else {
            str2 = pendingtrid;
        }
        var data = ev.dataTransfer.getData("text");
        var str4 = data.split("_");
        sub_code = $("#" + data).data('subcode');
        var cntFlag = true;
        var str3 = (ev.target.id).split("_");
        var subid = 'sub_' + str3[2] + "_" + str3['3'] + "_" + sub_code + '_' + str4['4'];
        var teacherallo = str3[2] + "_" + str3['3'] + "_" + str4['4'];
        if (trid == str2) {
            if ((teacherBlockData[str4['4']] === undefined || teacherBlockData[str4['4']][$(ev.currentTarget).data('day')] === undefined || teacherBlockData[str4['4']][$(ev.currentTarget).data('day')][$(ev.currentTarget).data('slot')] === undefined || teacherBlockData[str4['4']][$(ev.currentTarget).data('day')][$(ev.currentTarget).data('slot')] != "2")) {
            if ($(ev.target).is('div')) {
                if ($("#" + data).data('slotgroup') != '01') {
                    if ($(ev.currentTarget).data('subcode') != $("#" + data).data('subcode') && $(ev.currentTarget).data('slotgroup') == $("#" + data).data('slotgroup') && $(".dropabletd").find('[data-teacherallo= ' + $(ev.currentTarget).data('day') + "_" + $(ev.currentTarget).data('slot') + "_" + str4['4'] + ']').length == '0') {
                        $(ev.currentTarget).parents('td').append(document.getElementById(data));
                        $(ev.currentTarget).parents('td').addClass('groupClass');
                        $(ev.currentTarget).parents('td').children('.filled').attr('id', subid);
                        $(ev.currentTarget).parents('td').children('.filled').attr('data-teacherallo', teacherallo);
                    } else {
                        //if ($(ev.currentTarget).data('subcode') == $("#" + data).data('subcode')) {
                        //    $("#showmessage").show();
                        //    $("#showmessage").find('h5').text('Same Subject Not Allowed!');
                        //    cntFlag = false;
                        //}

                        if ($(ev.currentTarget).data('slotgroup') != $("#" + data).data('slotgroup')) {
                            $("#showmessage").show();
                            $("#showmessage").find('h5').text('This is not parallel with given slot group');
                            cntFlag = false;
                        }

                        if ($(".dropabletd").find('[data-teacherallo= ' + $(ev.currentTarget).data('day') + "_" + $(ev.currentTarget).data('slot') + "_" + str4['4'] + ']').length != '0') {
                            $("#showmessage").show();
                            var grade_sec = $(".dropabletd").find('[data-teacherallo= ' + $(ev.currentTarget).data('day') + "_" + $(ev.currentTarget).data('slot') + "_" + str4['4'] + ']').parents('tr').attr('id');
                            $("#showmessage").find('h5').text('Teacher Busy at ' + grade_sec);
                            cntFlag = false;
                        }
                    }
                }
            } else {
                if ($("#" + data).data('slotgroup') != '01') {
                    if (!$(ev.currentTarget).is(':empty')) {
                        if ($(ev.currentTarget).find('.dragablesubjects').data('slotgroup') == $("#" + data).data('slotgroup')) {

                            if ($(".dropabletd").find('[data-teacherallo= ' + $(ev.currentTarget).data('day') + "_" + $(ev.currentTarget).data('slot') + "_" + str4['4'] + ']').length == '0') {
                                if ($(ev.currentTarget).find('.dragablesubjects').data('subcode') != $("#" + data).data('subcode')) {
                                    smallfunction(ev, data, subid, teacherallo);
                                } else {
                                    if ($(ev.currentTarget).find('.dragablesubjects').data('teachercode') != str4['4']) {
                                        smallfunction(ev, data, subid, teacherallo);
                                    } else {
                                        msgFunction(ev, data, grade_sec);
                                    }
                                }
                            } else {
                                if ($(ev.currentTarget).find('.dragablesubjects').data('slotroom') == $("#" + data).data('slotroom')) {
                                    smallfunction(ev, data);
                                } else {
                                    msgFunction(ev, data, grade_sec);
                                }
                            }

                        } else {
                            msgFunction(ev, data, grade_sec);
                        }

                    } else {
                        if ($(".dropabletd").find('[data-teacherallo= ' + $(ev.currentTarget).data('day') + "_" + $(ev.currentTarget).data('slot') + "_" + str4['4'] + ']').length == '0') {
                            $(ev.currentTarget).append(document.getElementById(data));
                            $(ev.currentTarget).addClass('groupClass');
                            $(ev.currentTarget).children('.filled').attr('id', subid);
                            $(ev.currentTarget).children('.filled').attr('data-teacherallo', teacherallo);
                        } else {
                            if ($(ev.currentTarget).data('slotroom') != '01' && $(".dropabletd").find('[data-teacherallo= ' + $(ev.currentTarget).data('day') + "_" + $(ev.currentTarget).data('slot') + "_" + str4['4'] + ']').data('slotroom') == $(ev.currentTarget).data('slotroom')) {
                                $(ev.currentTarget).append(document.getElementById(data));
                                $(ev.currentTarget).addClass('groupClass');
                                $(ev.currentTarget).children('.filled').attr('id', subid);
                                $(ev.currentTarget).children('.filled').attr('data-teacherallo', teacherallo);
                            } else {
                                var grade_sec = $(".dropabletd").find('[data-teacherallo= ' + $(ev.currentTarget).data('day') + "_" + $(ev.currentTarget).data('slot') + "_" + str4['4'] + ']').parents('tr').find('.gradetd').text();
                                $("#showmessage").removeClass('ng-hide');
                                $("#showmessage").find('h5').text('Teacher Busy at ' + grade_sec);
                                cntFlag = false;
                            }

                        }
                    }
                } else {
                    if ($(ev.currentTarget).is(':empty')) {
                        if ($(".dropabletd").find('[data-teacherallo= ' + $(ev.currentTarget).data('day') + "_" + $(ev.currentTarget).data('slot') + "_" + str4['4'] + ']').length == '0') {


                            $(ev.currentTarget).append(document.getElementById(data));
                            $(dragstartsfrom).removeClass('groupClass');
                            $(ev.currentTarget).children('.filled').attr('id', subid);
                            $(ev.currentTarget).children('.filled').attr('data-teacherallo', teacherallo);
                        } else {
                            if ($(ev.currentTarget).data('slotroom') != '01' && $(ev.currentTarget).data('slotroom') != undefined) {
                                $(ev.currentTarget).append(document.getElementById(data));
                                $(dragstartsfrom).removeClass('groupClass');
                                $(ev.currentTarget).children('.filled').attr('id', subid);
                                $(ev.currentTarget).children('.filled').attr('data-teacherallo', teacherallo);
                            } else {
                                var grade_sec = $(".dropabletd").find('[data-teacherallo= ' + $(ev.currentTarget).data('day') + "_" + $(ev.currentTarget).data('slot') + "_" + str4['4'] + ']').parents('tr').find('.gradetd').text();
                                $("#showmessage").removeClass('ng-hide');
                                $("#showmessage").find('h5').text('Teacher Busy at ' + grade_sec);
                                cntFlag = false;
                            }

                        }

                    }

                }
            }

            if ($(dragstartsfrom).is(':empty')) {
                $(dragstartsfrom).removeClass('occupied groupClass').addClass('emptd');
            }

            if (cntFlag == true) {
                $(ev.currentTarget).addClass('occupied').removeClass('emptd');
            }

            $(ev.currentTarget).parents('tr').find('.count_td').html($(ev.target).parents('tr').children('.occupied').length);
            }
            else {
                $("#showmessage").removeClass('ng-hide');
                $("#showmessage").find('h5').text('Teacher cannot be allocated due to Teacher Allocation Criteria');
            }
        } else {
            if ($(ev.target).attr('id') == 'pendingsubid') {
                var trid = $(dragstartsfrom).parents('tr').attr('id');
                $("#" + data).attr('trid', trid);
                $(dragstartsfrom).removeClass('groupClass');
                $(ev.target).append(document.getElementById(data));
                $(ev.target).children('.dragablesubjects').removeClass('filled');

            }

            $(dragstartsfrom).removeClass('occupied').addClass('emptd');

            $(dragstartsfrom).parents('tr').find('.count_td').html($(dragstartsfrom).parents('tr').children('.occupied').length);

        }
    }
};

var smallfunction = function (ev, data, subid, teacherallo) {
    if ($(ev.currentTarget).is('div')) {
        $(ev.currentTarget).parents('td').append(document.getElementById(data));
        $(ev.currentTarget).parents('td').addClass('groupClass');
        $(ev.currentTarget).parents('td').children('.filled').attr('id', subid);
        $(ev.currentTarget).parents('td').children('.filled').attr('data-teacherallo', teacherallo);
    } else {
        $(ev.currentTarget).append(document.getElementById(data));
        $(ev.currentTarget).parents('td').removeClass('groupClass');
        $(ev.currentTarget).children('.filled').attr('id', subid);
        $(ev.currentTarget).children('.filled').attr('data-teacherallo', teacherallo);
    }
}

var messageFunction = function (ev, data, grade_sec) {
    if ($(ev.currentTarget).data('subcode') == $("#" + data).data('subcode')) {
        $("#showmessage").removeClass('ng-hide');
        $("#showmessage").find('h5').text('Same Subject Not Allowed!');
    } else if ($(ev.currentTarget).data('slotgroup') != $("#" + data).data('slotgroup')) {
        $("#showmessage").removeClass('ng-hide');
        $("#showmessage").find('h5').text('This is not parallel with');
    } else if ($(".dropabletd").find('[data-teacherallo= ' + $(ev.currentTarget).data('day') + "_" + $(ev.currentTarget).data('slot') + "_" + $("#" + data).data('teachercode') + ']').length != '0') {
        $("#showmessage").removeClass('ng-hide');
        var grade = $(".dropabletd").find('[data-teacherallo= ' + $(ev.currentTarget).data('day') + "_" + $(ev.currentTarget).data('slot') + "_" + $("#" + data).data('teachercode') + ']').parents('tr').find('.gradetd').text();
        $("#showmessage").find('h5').text('Teacher Busy at ' + grade_sec);

    }
}

var filterPendingSubject = function (trid) {
    $("#fixTable .trrow").removeClass('activetr');
    $("#" + trid).addClass('activetr');

    $("#hiddenpending").append($("#pendingsubid .dragablesubjects"));

    $("#pendingsubid").append($("#hiddenpending").find('[trid=' + trid + ']'));

};

(function () {
    'use strict';
    var simsController = angular.module('sims.module.TimeTable');
    var iSlotCount; //= [7,7,7,7,5];
    var iSlotDay; //= 5;
    var a = {};
    var AllBellData;
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SchoolTimeTableCont', ['$scope', '$compile', '$anchorScroll', '$location', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$locale', '$http', 'ENV', function ($scope, $compile, $anchorScroll, $location, $state, $rootScope, $timeout, gettextCatalog, $locale, $http, ENV) {

        $scope.subinfo = [];
        $scope.iSlotDayDetail_list = [];
        $scope.getCurriculum = function () {
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.Allcurriculum = res.data;
                $scope.cur_code = $scope.Allcurriculum[0].sims_cur_code;
                $scope.getAcademicYear();
            });
        }

        $scope.getAcademicYear = function () {

            var a = {
                opr: 'A',
                cur_code: $scope.cur_code
            };
            $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableCommon", a).then(function (res) {

                $scope.AllAcademicYear = res.data.table;
                $($scope.AllAcademicYear).each(function (k, v) {
                    if (v.sims_academic_year_status == 'C') {
                        $scope.aca_year = v.sims_academic_year;
                        $scope.getBellData();
                        return false;
                    }
                });
            });
        };

        $scope.getBellData = function () {
            var a = {
                Opr: 'B',
                AcaYear: $scope.aca_year,
                cur_code: $scope.cur_code
            };
            $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableImport", a).then(function (getBellData) {

                $scope.AllBellData = getBellData.data.table;
                if (getBellData.data.table.length > 0) {
                    $scope.bell_code = $scope.AllBellData[0].sims_bell_code;
                } else {
                    $scope.bell_code = "";
                }
                $scope.DataLoadAfterBell();
            });
        }

        $scope.getGrade = function () {
            var a = {
                opr: 'G',
                aca_year: $scope.aca_year,
                bell_code: $scope.bell_code,
                cur_code: $scope.cur_code
            };
            $http.post(ENV.apiUrl + "api/PreferenceController/DaySlotBreakDetail", a).then(function (res) {
                $scope.AllGrades = res.data;
            });
        };

        $scope.getTeachers = function () {
            var a = {
                opr: 'O',
                AcaYear: $scope.aca_year,
                bellCode: $scope.bell_code,
                cur_code: $scope.cur_code
            };
            $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableView", a).then(function (res) {
                $scope.AllTeachers = res.data.table;
                $scope.TeacherAllocation = res.data.table1;
                //console.log("$scope.TeacherAllocation value is ", $scope.TeacherAllocation);
                var sTmp = "noData";
                var iTmp = -2;
                $scope.data = [];
                $($scope.TeacherAllocation).each(function (key, val) {
                    if (sTmp == "noData" || sTmp != val.sims_bell_teacher_code) {
                        sTmp = val.sims_bell_teacher_code;
                        $scope.data[val.sims_bell_teacher_code] = new Array();
                        iTmp = -2;
                    }
                    if (iTmp == -2 || iTmp < parseInt(val.sims_bell_day_code, 10)) {
                        iTmp = parseInt(val.sims_bell_day_code, 10);
                        $scope.data[val.sims_bell_teacher_code][parseInt(val.sims_bell_day_code, 10)] = new Array();
                    }
                    $scope.data[val.sims_bell_teacher_code][parseInt(val.sims_bell_day_code, 10)][parseInt(val.sims_bell_slot_code, 10)] = new Array();
                    $scope.data[val.sims_bell_teacher_code][parseInt(val.sims_bell_day_code, 10)][parseInt(val.sims_bell_slot_code, 10)].push(val.sims_bell_preference_code);
                });
                console.log("$scope.data value", $scope.data);
                teacherBlockData = $scope.data;
            });
        }

        $scope.DataLoadAfterBell = function () {
            $("#pendingsubid").html('');
            $scope.bellLoaded = false;
            $scope.getGradeSection();
            $scope.getTeachers();
        }

        $scope.getGradeSection = function () {
            debugger
            a = {
                Opr: 'C',
                AcaYear: $scope.aca_year,
                cur_code: $scope.cur_code,
                bellCode: $scope.bell_code
            };


            $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableInitial", a).then(function (res) {

                $scope.iSlotCount = [];
                $(".timetablecontainer").removeClass('hide');
                for (var i = 0; i < res.data.table.length; i++) {
                    $scope.iSlotCount[i + 1] = res.data.table[i].column1
                }

                $scope.iSlotDay = res.data.table1[0].column1;


                $scope.iSlotDayDetail = res.data.table3;

                $scope.iSlotDayDetail_list = [];
                for (var i = 0; i < $scope.iSlotDayDetail.length; i++) {
                    if ($scope.iSlotDayDetail_list.length > 0) {
                        $scope.chk_flg = false;
                        for (var j = 0; j < $scope.iSlotDayDetail_list.length; j++) {
                            if ($scope.iSlotDayDetail_list[j].sims_bell_day_code == $scope.iSlotDayDetail[i].sims_bell_day_code) {
                                $scope.chk_flg = true;
                                $scope.iSlotDayDetail_list[j].slot_code_list.push({
                                    slot_code: $scope.iSlotDayDetail[i].sims_bell_slot_code
                                });
                            }
                        }
                        if ($scope.chk_flg == false)
                            $scope.iSlotDayDetail_list.push({
                                "slot_code_list": [{
                                    slot_code: $scope.iSlotDayDetail[i].sims_bell_slot_code
                                }],
                                sims_bell_day_code: $scope.iSlotDayDetail[i].sims_bell_day_code
                            })


                    } else {
                        $scope.iSlotDayDetail_list.push({
                            "slot_code_list": [{
                                slot_code: $scope.iSlotDayDetail[i].sims_bell_slot_code
                            }],
                            sims_bell_day_code: $scope.iSlotDayDetail[i].sims_bell_day_code
                        })

                    }
                    console.log('$scope.iSlotDayDetail_list.length', $scope.iSlotDayDetail_list);

                }
                $scope.AllGradeSec = res.data.table2;
                $scope.RenderTilesnGridClassView();
                $(".saved_tt").removeClass('hide');
                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableHistoryVersionData?aca_year=" + $scope.aca_year + "&bell_code=" + $scope.bell_code).then(function (res) {
                    $scope.AllSavedVersions = res.data;
                    $scope.loaderShow = false;
                });
            });

            
            console.log('$scope.iSlotDayDetail_list.length', $scope.iSlotDayDetail_list);
        };

        $scope.RenderTilesnGridClassView = function () {
            debugger
            var l;
            var html = '<tr>';
            html += '<th class="cellWidth nobg text-center grade_fix top_fix" title="Grade/Days"> G/D </th>';
            html += '<th class="nobg text-center countth top_fix">Count</th>';

            $($scope.iSlotDayDetail_list).each(function (k, v) {
                html += '<th id="day_' + v.sims_bell_day_code + '" colspan="' + v.slot_code_list.length + '" class="nobg text-center">' + $scope.oSlotDayNames[v.sims_bell_day_code - 1] + '</th>';
            })

            html += '</tr>';
            html += '<tr>';
            html += '<td class="cellWidth text-center nobg grade_fix top_fix" title="Grade/Slot"></td><td class="nobg text-center"></td>';

            $($scope.iSlotDayDetail_list).each(function (k, v) {
                $(v.slot_code_list).each(function (key, val) {

                    html += '<td id="period_' + val.slot_code + '" class="cellWidth text-center nobg">' + (key + 1) + '</td>';
                })
            });
            html += '</tr>';

            $(".slotArea .headerpart").html("");
            $(".slotArea .headerpart").append(html);

            var html1;
            $($scope.AllGradeSec).each(function (key, val) {
                var str = val.sims_grade_name_en;
                var gradeName = str.replace("Class", "");
                html1 += '<tr class="trrow" id="' + val.sims_bell_grade_code + "_" + val.sims_bell_section_code + '" data-gradecode="' + val.sims_bell_grade_code + '" data-seccode="' + val.sims_bell_section_code + '" data-grade="' + val.sims_grade_name_en + '" data-secname="' + val.sims_section_name_en + '"> <td class="cellWidth gradetd grade_fix ellipsis" onclick="filterPendingSubject(\'' + val.sims_bell_grade_code + "_" + val.sims_bell_section_code + '\')" title="' + gradeName + " " + val.sims_section_name_en + '" > ' + gradeName + " " + val.sims_section_name_en + '</td>';
                html1 += '<td class="count_td text-center text-white count_fix" style="background-color: white;"></td>';

                $($scope.iSlotDayDetail_list).each(function (ke, vl) {
                    $(vl.slot_code_list).each(function (keyy, vall) {
                        html1 += '<td id="' + val.sims_bell_grade_code + "_" + val.sims_bell_section_code + "_" + (vl.sims_bell_day_code) + "_" + (vall.slot_code) + '" data-day="' + vl.sims_bell_day_code + '" data-slot="' + vall.slot_code + '" class="cellWidth dropabletd emptd" ondrop="drop(event)" ondragover="allowDrop(event)"></td>';
                    })
                });

                html1 += "</tr>";
            });

            $(".slotArea .contentpart").html("");
            $(".slotArea .contentpart").append(html1);
            $("#fixTable").tableHeadFixer();
            $("#fixTable").tableHeadFixer({
                'left': 2,
                'z-index': 100
            });

        }

        $scope.RenderTilesnGridTeacherView = function () {
            debugger
            //            $scope.start_day = 1;
            $http.get(ENV.apiUrl + "api/LibraryAttribute/getSlotDay?AcaYear=" + $scope.aca_year + "&bellCode=" + $scope.bell_code).then(function (res1) {
                $scope.bell_data = res1.data;
                $scope.start_day = $scope.bell_data[0].start_day;
           
            $scope.iSlotDayDetail_list;
            $(".timetablecontainer").addClass("hide");
            $scope.teacherViewShow = true;
            $scope.loaderShow = true;

            var l;
            var html = '<tr>';
            html += '<th class="cellWidth nobg text-center grade_fix top_fix" title="Grade/Days"> T/D <span ng-click="sortTable($event, \'0\', \'str\')" class="cursor-pointer"><i class="material-icons">arrow_drop_down</i></span></th>';
            html += '<th class="nobg text-center countth top_fix" >TC <span ng-click="sortTable($event,\'1\', \'num\')" class="cursor-pointer"><i class="material-icons" >arrow_drop_down</i></span></th>';
            html += '<th class="nobg text-center countth top_fix" >AC <span ng-click="sortTable($event,\'2\', \'num\')" class="cursor-pointer"><i class="material-icons" >arrow_drop_down</i></span></th>';
            for (var i = 1; i <= $scope.iSlotDayDetail_list.length; i++) {
                html += '<th id="day_' + i + '" colspan="' + $scope.iSlotDayDetail_list[i - 1].slot_code_list.length + '" class="nobg text-center">' + $scope.oSlotDayNames[i - 1] + '</th>';
            }

            //html += '</tr>';
            //html += '<tr>';
            //html += '<td class="cellWidth text-center nobg grade_fix top_fix" title="Grade/Slot"></td><td class="cellWidth text-center nobg grade_fix top_fix" title="Grade/Slot"></td><td class="nobg text-center"></td>';
            //for (var j = 1; j <= $scope.iSlotDayDetail_list.length; j++) {
            //    for (var k = 1; k <= $scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list.length; k++) {

            //        html += '<td id="period_' + $scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list[k - 1].slot_code + '" class="cellWidth text-center nobg">' + (k) + '</td>';
            //    }
            //}
            html += '</tr>';
            html += '<tr>';
            html += '<td class="cellWidth text-center nobg grade_fix top_fix" title="Grade/Slot"></td><td class="cellWidth text-center nobg grade_fix top_fix" title="Grade/Slot"></td><td class="nobg text-center"></td>';
            for (var j = 1; j <= $scope.iSlotDayDetail_list.length; j++) {
                for (var k = 1; k <= $scope.iSlotDayDetail_list[j - $scope.start_day].slot_code_list.length; k++) {

                    html += '<td id="period_' + $scope.iSlotDayDetail_list[j - $scope.start_day].slot_code_list[k - 1].slot_code + '" class="cellWidth text-center nobg">' + (k) + '</td>';
                }
            }
            html += '</tr>';

            $(".teacherviewdiv .headerpart").html("");
            var temp = $compile(html)($scope);
            $(".teacherviewdiv .headerpart").append(temp);
            var html1;

            $($scope.AllTeachers).each(function (key, val) {

                var templeccount = 0;
                var assignccount = 0;
                if (val.lecture_count != undefined) {

                    templeccount = val.lecture_count;
                }

                if (val.assign_count != undefined) {

                    assignccount = val.assign_count;
                }
                html1 += '<tr class="trrow" id="' + val.sims_bell_teacher_code + '"> <td class="cellWidth gradetd grade_fix ellipsis" onclick="filterPendingSubject(\'' + val.sims_bell_grade_code + "_" + val.sims_bell_section_code + '\')" title="' + val.sims_teacher_name + '" > ' + val.sims_teacher_name + '</td>';
                html1 += '<td class="count_ttd text-center count_fix">' + templeccount + '</td>';
                html1 += '<td class="count_td text-center count_fix">' + assignccount + '</td>';
                //for (var j = 1; j <= $scope.iSlotDayDetail_list.length; j++) {
                //    for (var i = 1; i <= $scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list.length; i++) {
                //        html1 += '<td id="' + val.sims_bell_teacher_code + "_" + (j) + "_" + ($scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list[i - 1].slot_code) + '" data-day="' + j + '" data-slot="' + $scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list[i - 1].slot_code + '" class="cellWidth dropabletd emptd text-center" ondrop="drop(event)" ondragover="allowDrop(event)"></td>';
                //    }
                for (var j = 1; j <= $scope.iSlotDayDetail_list.length; j++) {
                    for (var i = 1; i <= $scope.iSlotDayDetail_list[j - $scope.start_day].slot_code_list.length; i++) {
                        html1 += '<td id="' + val.sims_bell_teacher_code + "_" + (j) + "_" + ($scope.iSlotDayDetail_list[j - $scope.start_day].slot_code_list[i - 1].slot_code) + '" data-day="' + j + '" data-slot="' + $scope.iSlotDayDetail_list[j - $scope.start_day].slot_code_list[i - 1].slot_code + '" class="cellWidth dropabletd emptd text-center" ondrop="drop(event)" ondragover="allowDrop(event)"></td>';
                    }
                }

                html1 += "</tr>";
            });

            $(".teacherviewdiv .contentpart").html("");
            $(".teacherviewdiv .contentpart").append(html1);
            $("#fixTableTeacher").tableHeadFixer();
            $("#fixTableTeacher").tableHeadFixer({
                'left': 3,
                'z-index': 100
            });
            $scope.genTeachersView();
            });
            }


        $scope.genTeachersView = function () {

            $("#fixTableTeacher .contenttd").html('');

            $("#fixTableTeacher").find('.trrow').each(function (key, val) {

                var a = {
                    opr: 'N',
                    AcaYear: $scope.aca_year,
                    cur_code: $scope.cur_code,
                    bellCode: $scope.bell_code,
                    teacher_code: val.id
                };

                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableView", a).then(function (res) {
                    $scope.gridData = res.data.table2;

                    $($scope.gridData).each(function (k, v) {
                        var html = '<div class="sub_tec_cont"><div class="cvsubname ellipsis" title="' + v.sims_subject_name_en + '">' + v.sims_subject_name_en + '</div><div class="viewsep"></div><div class="cvtrname ellipsis" title="' + v.sims_grade_name_en + ' ' + v.sims_section_name_en + '">[' + v.sims_grade_name_en + ' ' + v.sims_section_name_en + ']</div></div>';
                        var temp = $compile(html)($scope);
                        if ($("#" + val.id + "_" + v.sims_bell_day_code + '_' + v.sims_bell_slot_code).children('.sub_tec_cont').length > 0) {
                            $("#" + val.id + "_" + v.sims_bell_day_code + '_' + v.sims_bell_slot_code).addClass('group');
                        }
                        $("#" + val.id + "_" + v.sims_bell_day_code + '_' + v.sims_bell_slot_code).addClass('occupied').append(temp);

                    });

                    $scope.loaderShow = false;

                }).then(function () {

                    $('#fixTableTeacher .trrow').each(function (k, v) {

                        $(v).find('.count_td').html($(v).children('.occupied').length);
                        if (k + 1 == $("#fixTableTeacher .trrow").length) {
                            $scope.loaderShow = false;
                        }
                    });


                });

            });
        }

        $scope.closeTeacherView = function () {
            $(".timetablecontainer").removeClass('hide');
            $scope.teacherViewShow = false;
        }


        $scope.saveTimeTable = function () {

            var finalData = [];
            var timeTableData = [];
            var pendingsub = [];
            var version = 'Finalized Time table';

            var finalData = $scope.makeObjOfAllocatedSubjects();

            if (finalData.length > 0) {
                $scope.loaderShow = true;
                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableDatasave", finalData).then(function (res) {
                    if (res.data == true) {
                        $scope.messageShow = true;
                        $scope.message = 'Final Time Table saved successfully';
                        $scope.loaderShow = false;
                    }
                }).then(function () {

                    $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableHistorysave?ttVersion=" + version + "&type=add", finalData).then(function (res) {

                        var html = '<div class="inline-block version_item">';
                        html += '<div class="p-t-5 p-b-5 p-l-10 p-r-10 m-b-5 m-t-5 primary-bg text-white saved_tt_item">';
                        html += '<span>' + res.data + '</span> <span class="pull-right tt_edit_icons "><i class="material-icons cursor-pointer" ng-click="editVersion($event, \'' + res.data + '\')">create</i></span>';
                        html += '</div>';
                        html += '</div>';
                        var temp = $compile(html)($scope);

                        if (!$("#saved_tt").find('div:contains("Finalized Time table")').length) {
                            $("#version_div").append(temp);
                        }
                    });

                });
            }
        };

        $scope.viewTimeTable = function () {
            debugger
            $scope.enableSave = true;
            $('body').addClass("viewtt");
            $scope.loaderShow = true;
            $(".contentpart td.dropabletd").html('').removeClass('occupied').addClass('emptd');
            $("#pendingsubid").html('');
            $("#fixTable .trrow").removeClass('activetr');

            a = {
                'opr': 'P',
                'AcaYear': $scope.aca_year,
                'bellCode': $scope.bell_code,
                cur_code: $scope.cur_code
            };
            $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableViewData", a).then(function (res) {

                $scope.AllGradeSectionView = res.data;

                $($scope.AllGradeSectionView).each(function (k, v) {
                    if (v.day_code == '0') {
                        $scope.insertIntoPending(v, 'editversion');
                    } else {
                        $scope.insertIntoSlot(v);
                    }
                });
            }).then(function () {
                $('#fixTable .trrow').each(function (k, v) {
                    $(v).find('.count_td').html($(v).children('.occupied').length);
                    if (k + 1 == $("#fixTable .trrow").length) {
                        $scope.loaderShow = false;
                    }
                });
            });
        }


        $scope.showDetail = function (e) {
            $scope.subinfo.subject = $(e.currentTarget).find('.hoverdiv').data('subname');
            $scope.subinfo.teacher = $(e.currentTarget).find('.hoverdiv').data('teacher');
            $scope.subinfo.gradename = $(e.currentTarget).find('.hoverdiv').data('gradename');
            $scope.subinfo.secname = $(e.currentTarget).find('.hoverdiv').data('secname');
        }

        $scope.hideDetail = function () {
            $scope.subinfo = [];
        }

        $scope.genTimeTableNew = function (e) {
            $scope.editversionshow = false;
            $('body').removeClass('viewtt');
            $("#fixTable .trrow").removeClass('activetr');
            $scope.pendingShow = true;
            $scope.loaderShow = true;
            $(".contentpart td.dropabletd").html('').removeClass('occupied').addClass('emptd').attr('slotgid', '');
            $(".contentpart td.count_td").html('');
            $("#pendingsubid").html('');
            $(".slotArea").removeClass("editmode").attr('version', '');
            a = {
                Opr: 'I',
                AcaYear: $scope.aca_year,
                bellCode: $scope.bell_code,
                cur_code: $scope.cur_code
            };

            //var ttt = ['#03_1'];
            $("#fixTable .trrow").each(function (k, v) {
                //$(ttt).each(function (k, v) {

                var trid = $(v).attr('id');
                var arr = trid.split("_");
                var grade_code = arr[0];
                var sec_code = arr[1];
                var daypointer = 1;
                var slotpointer = 1;

                a = {
                    Opr: 'I',
                    AcaYear: $scope.aca_year,
                    bellCode: $scope.bell_code,
                    grade_code: grade_code,
                    sec_code: sec_code,
                    cur_code: $scope.cur_code
                };

                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableDataNew", a).then(function (res) {

                    $scope.pendingArr = [];
                    $scope.PendingViewd = false;
                    $scope.insertingPendingsFlag = false;


                    $scope.genTimeTableNewData = res.data.table;
                    $scope.subjectPreference = res.data.table1;
                    $scope.teacherPreference = res.data.table2;

                    $scope.classTeacherPreference = res.data.table3;
                    $scope.schoolPreference = res.data.table4;
                    $scope.class_teacher_first = res.data.table5[0].sims_bell_appl_parameter;
                    $scope.Subject_preference_rule = res.data.table6[0].sims_bell_appl_parameter;
                    $scope.Teacher_preference_rule = res.data.table7[0].sims_bell_appl_parameter;
                    $scope.parallelSubjects = res.data.table8;
                    $scope.start_day = parseInt(res.data.table9[0].start_day, 10) - 1;
                    daypointer = $scope.start_day + 1;
                    if ($scope.class_teacher_first == 'Y') {
                        if ($scope.classTeacherPreference.length > 0) {
                            var cteacherData = $scope.findClassTeacherIndexData(grade_code, sec_code, $scope.classTeacherPreference[0].teacher_code);

                            if (cteacherData != undefined) {
                                var ct = cteacherData['data'];
                                $scope.lastfilled = $scope.fillIntoGrid(cteacherData['data'], daypointer, slotpointer);
                                $scope.genTimeTableNewData.splice(cteacherData['tindex'], 1);
                            }
                        }
                    }

                    if ($scope.genTimeTableNewData.length > 0) {
                        if ($scope.lastfilled != undefined) {
                            $scope.normalGen($scope.genTimeTableNewData, $scope.lastfilled['next_day'], $scope.lastfilled['next_slot']);
                        } else {
                            $scope.normalGen($scope.genTimeTableNewData, daypointer, slotpointer, 'normal');
                        }
                    }

                    if ($scope.subjectPreference.length > 0 || $scope.teacherPreference.length > 0) {
                        if ($scope.subjectPreference.length > 0 && $scope.teacherPreference.length > 0) {
                            $scope.removeSubTeachCommon();
                        }

                        if ($scope.Subject_preference_rule == '1') {

                            if ($scope.subjectPreference.length >= 0) {
                                $($scope.subjectPreference).each(function (k, v) {
                                    //$scope.fillIntoGrid(v, v.day_code, v.slot_code, 'preference');
                                    $scope.findSlotAndAllocate(v);
                                    //$scope.findSlotAndAllocate.splice(0,1);
                                });
                            }

                            if ($scope.teacherPreference.length >= 0) {
                                $($scope.teacherPreference).each(function (k, v) {
                                    //$scope.fillIntoGrid(v, v.day_code, v.slot_code, 'preference');
                                    $scope.findSlotAndAllocate(v);
                                });
                            }
                        } else {
                            if ($scope.teacherPreference.length >= 0) {
                                $($scope.teacherPreference).each(function (k, v) {
                                    //$scope.fillIntoGrid(v, v.day_code, v.slot_code, 'preference');
                                    $scope.findSlotAndAllocate(v);
                                });
                            }
                            if ($scope.subjectPreference.length >= 0) {
                                $($scope.subjectPreference).each(function (k, v) {
                                    //$scope.fillIntoGrid(v, v.day_code, v.slot_code, 'preference');
                                    $scope.findSlotAndAllocate(v);
                                });
                            }
                        }
                    }


                    if ($scope.parallelSubjects.length > 0) {

                        if ($scope.lastfilled != undefined) {
                            $scope.normalGen($scope.parallelSubjects, $scope.lastfilled['next_day'], $scope.lastfilled['next_slot'], 'parallel');
                        } else {
                            $scope.normalGen($scope.parallelSubjects, daypointer, slotpointer, 'parallel');
                        }
                    }



                }).then(function () {
                    if ($scope.pendingArr.length > 0) {
                        $scope.allDone = false;
                        var temarr = angular.copy($scope.pendingArr);
                        $(temarr).each(function (kk, vv) {

                            var empty_day = $(v).find('.emptd').data('day');
                            var empty_slot = $(v).find('.emptd').data('slot');
                            if (empty_day != undefined && empty_slot != undefined) {

                                $scope.insertingPendingsFlag = true;
                                $scope.fillIntoGrid(vv, empty_day, empty_slot);
                            }

                            if (kk + 1 == temarr.length) {
                                $scope.PendingViewd = true;
                                $($scope.pendingArr).each(function (kee, vll) {
                                    $scope.insertIntoPending(vll);
                                })
                                $scope.allDone = true;
                                $scope.pendingArr = [];
                            }
                        });


                        if ($scope.allDone == true) {
                            $(v).find('.count_td').html($(v).children('.occupied').length);
                            $scope.insertingPendingsFlag = false;
                        }

                    } else {
                        $(v).find('.count_td').html($(v).children('.occupied').length);
                    }

                    if (k + 1 == $("#fixTable .trrow").length) {

                        $scope.loaderShow = false;
                        $scope.enableSave = false;
                        // $scope.iNumber = $scope.iNumber !== undefined ? $scope.iNumber-1 : 0;
                        //if ($scope.iterativeTTFlag !== undefined && $scope.iterativeTTFlag === true) {
                        //    $scope.IterationLoop();
                        //    if ($scope.iNumber == 0) {
                        //        $scope.getBestTT();
                        //    }
                        //}
                    }
                });
            });
        }


        $scope.findSlotAndAllocate = function (v) {
            var ind = -1;
            if (v.sub_code) { //id,data,day,slot,sub_id
                ind = $scope.findIndexUsingArr('subject', $scope.parallelSubjects, v.sub_code);

            }
            if (v.teacher_code) { //id,data,day,slot,sub_id
                ind = $scope.findIndexUsingArr('teacher', $scope.parallelSubjects, v.teacher_code);
            }
            if (ind >= 0 && $scope.parallelSubjects[ind].sims_bell_lecture_count > 0) {
                var avl = $scope.checkAvl($scope.parallelSubjects[ind].sims_bell_subject_code,
                    $scope.parallelSubjects[ind].sims_bell_grade_code,
                    $scope.parallelSubjects[ind].sims_bell_section_code,
                    $scope.parallelSubjects[ind].sims_bell_teacher_code,
                    v.day_code,
                    v.slot_code,
                    "",
                    $scope.parallelSubjects[ind].sims_bell_slot_group.trim(), "preference",
                    $scope.parallelSubjects[ind].sims_bell_slot_room);
                if (avl !== undefined) {
                    var id = $scope.parallelSubjects[ind].sims_bell_grade_code + "_" + $scope.parallelSubjects[ind].sims_bell_section_code + "_" + avl.day + "_" + avl.slot;
                    $scope.fillSlot(id, $scope.parallelSubjects[ind], avl.day, avl.slot, avl.sub_id);
                    $scope.parallelSubjects[ind].sims_bell_lecture_count = $scope.parallelSubjects[ind].sims_bell_lecture_count - 1;
                }
            }
            //$scope.exceptionFlag = false;
        }

        $scope.updateSubjectTeacherPreferenceArr = function () {
            $scope.updatedSubjectPre = [];
            $scope.updatedTeacherPre = [];
            var i;
            var j;
            for (i = 0; i < $scope.subjectPreference.length; i++) {
                for (j = 0; j < $scope.genTimeTableNewData.length; j++) {

                    if ($scope.genTimeTableNewData[j]['sims_bell_grade_code'] == $scope.subjectPreference[i]['grade_code'] && $scope.genTimeTableNewData[j]['sims_bell_section_code'] == $scope.subjectPreference[i]['sec_code'] && $scope.genTimeTableNewData[j]['sims_bell_subject_code'] == $scope.subjectPreference[i]['sub_code']) {
                        var index_arr = $scope.findIndexData('subject', $scope.subjectPreference[i]['grade_code'], $scope.subjectPreference[i]['sec_code'], $scope.subjectPreference[i]['sub_code']);

                        if (index_arr['tindex'] != undefined) {
                            $scope.genTimeTableNewData[j]['day_code'] = $scope.subjectPreference[i]['day_code'];
                            $scope.genTimeTableNewData[j]['slot_code'] = $scope.subjectPreference[i]['slot_code'];
                            $scope.genTimeTableNewData[j]['lec_count'] = '1';

                            var current_data = angular.copy($scope.genTimeTableNewData[j]);
                            $scope.updatedSubjectPre.push(current_data);
                            $scope.genTimeTableNewData[j]['sims_bell_lecture_count'] = $scope.genTimeTableNewData[j]['sims_bell_lecture_count'] - 1;

                            if ($scope.genTimeTableNewData[j]['sims_bell_lecture_count'] == '0') {
                                $scope.genTimeTableNewData.splice(index_arr['tindex'], 1);
                            }
                            break;
                        }
                    }
                }
            }

            for (i = 0; i < $scope.teacherPreference.length; i++) {
                for (j = 0; j < $scope.genTimeTableNewData.length; j++) {
                    if ($scope.genTimeTableNewData[j]['sims_bell_grade_code'] == $scope.teacherPreference[i]['grade_code'] && $scope.genTimeTableNewData[j]['sims_bell_section_code'] == $scope.teacherPreference[i]['sec_code'] && $scope.genTimeTableNewData[j]['sims_bell_teacher_code'] == $scope.teacherPreference[i]['teacher_code']) {
                        var index_arr = $scope.findIndexData('teacher', $scope.teacherPreference[i]['grade_code'], $scope.teacherPreference[i]['sec_code'], $scope.teacherPreference[i]['teacher_code']);

                        $scope.genTimeTableNewData[j]['lec_count'] = '1';

                        $scope.genTimeTableNewData[j]['day_code'] = $scope.teacherPreference[i]['day_code'];
                        $scope.genTimeTableNewData[j]['slot_code'] = $scope.teacherPreference[i]['slot_code'];
                        var current_data = angular.copy($scope.genTimeTableNewData[j]);
                        $scope.updatedTeacherPre.push(current_data);
                        $scope.genTimeTableNewData[j]['sims_bell_lecture_count'] = $scope.genTimeTableNewData[j]['sims_bell_lecture_count'] - 1;

                        if ($scope.genTimeTableNewData[j]['sims_bell_lecture_count'] == '0') {
                            $scope.genTimeTableNewData.splice(index_arr['tindex'], 1);
                        }
                    }
                }
            }
        }



        $scope.removeSubTeachCommon = function () {
            var tempindexarr = [];
            var i;
            var j;
            for (i = 0; i < $scope.subjectPreference.length; i++) {
                for (j = 0; j < $scope.teacherPreference.length; j++) {
                    if ($scope.subjectPreference[i]['grade_code'] == $scope.teacherPreference[j]['grade_code'] && $scope.subjectPreference[i]['sec_code'] == $scope.teacherPreference[j]['sec_code'] && $scope.subjectPreference[i]['day_code'] == $scope.teacherPreference[j]['day_code'] && $scope.subjectPreference[i]['slot_code'] == $scope.teacherPreference[j]['slot_code']) {

                        $($scope.schoolPreference).each(function (k, v) {
                            if (v.value == '1') {
                                if (v.field == "Subject Preference") {
                                    tempindexarr.push({
                                        index: i,
                                        pre: 'subject',
                                        data: $scope.subjectPreference[i]
                                    });
                                } else {
                                    tempindexarr.push({
                                        index: j,
                                        pre: 'teacher',
                                        data: $scope.teacherPreference[j]
                                    });
                                }
                            }
                        })
                    }
                }
            }

            for (var i = tempindexarr.length - 1; i >= 0; i--) {
                if (tempindexarr[i]['pre'] == 'subject') {
                    $scope.teacherPreference.splice(tempindexarr[i]['index'], 1);
                } else {
                    $scope.subjectPreference.splice(tempindexarr[i]['index'], 1);
                }
            }
        }


        $scope.normalGen = function (data, daypointer, slotpointer, type) {

            var lastfilled;
            $(data).each(function (key, val) {
                var sub_arr = {
                    aca_year: $scope.aca_year,
                    bell_code: val.sims_bell_grade_code,
                    grade_code: val.sims_bell_grade_code,
                    sec_code: val.sims_bell_section_code,
                    sub_code: val.sims_bell_subject_code
                };
                var trach_arr = {
                    aca_year: $scope.aca_year,
                    bell_code: val.sims_bell_grade_code,
                    grade_code: val.sims_bell_grade_code,
                    sec_code: val.sims_bell_section_code,
                    teacher_code: val.sims_bell_teacher_code
                };

                if (lastfilled != undefined) {
                    var sub_preference = $scope.checkSubjectPriority(sub_arr, lastfilled['next_day'], lastfilled['next_slot']);
                    var teacher_preference = $scope.checkTeacherPriority(trach_arr, lastfilled['next_day'], lastfilled['next_slot']);
                } else {
                    var sub_preference = $scope.checkSubjectPriority(sub_arr, daypointer, slotpointer);
                    var teacher_preference = $scope.checkTeacherPriority(trach_arr, daypointer, slotpointer);
                }

                if (daypointer > $scope.iSlotDayDetail_list.length) {
                    daypointer = daypointer - $scope.iSlotDayDetail_list.length;
                }

                if (slotpointer > $scope.iSlotDayDetail_list[daypointer - 1].slot_code_list.length) {
                    slotpointer = slotpointer - $scope.iSlotDayDetail_list[daypointer - 1].slot_code_list.length;
                    daypointer++;
                }


                if (lastfilled != undefined) {
                    lastfilled = $scope.fillIntoGrid(val, lastfilled['next_day'], lastfilled['next_slot'], type);
                } else {
                    lastfilled = $scope.fillIntoGrid(val, daypointer, slotpointer, type);
                }

                if (key + 1 == data.length) {
                    $scope.normalGenFlag = true;
                }
            })
        }

        $scope.findClassTeacherIndexData = function (str1, str2, str3) {

            var i;
            for (i = 0; i < $scope.genTimeTableNewData.length; i++) {
                if ($scope.genTimeTableNewData[i]['sims_bell_grade_code'] == str1 && $scope.genTimeTableNewData[i]['sims_bell_section_code'] == str2 && $scope.genTimeTableNewData[i]['sims_bell_teacher_code'] == str3) {
                    return {
                        tindex: i,
                        data: $scope.genTimeTableNewData[i]
                    };
                }
            }
        }

        $scope.findIndexData = function (type, str1, str2, str3) {
            var i, tocheck;

            if (type == 'subject') {
                tocheck = 'sims_bell_subject_code';
            } else {
                tocheck = 'sims_bell_teacher_code';
            }
            for (i = 0; i < $scope.genTimeTableNewData.length; i++) {

                if ($scope.genTimeTableNewData[i]['sims_bell_grade_code'] == str1 && $scope.genTimeTableNewData[i]['sims_bell_section_code'] == str2 && $scope.genTimeTableNewData[i][tocheck] == str3) {
                    return {
                        tindex: i,
                        data: $scope.genTimeTableNewData[i]
                    };
                }
            }
        }

        $scope.findIndexUsingArr = function (type, data, str3) {
            var i, tocheck;

            if (type == 'subject') {
                tocheck = 'sims_bell_subject_code';
            } else {
                tocheck = 'sims_bell_teacher_code';
            }
            for (i = 0; i < data.length; i++) {

                if (data[i][tocheck] == str3 && data[i].sims_bell_lecture_count > 0) {
                    return i;
                }
            }
        }

        $scope.findIndexInDataArr = function (data) {
            var i;
            for (i = 0; i < $scope.genTimeTableNewData.length; i++) {
                if (angular.equals($scope.genTimeTableNewData[i], data)) {
                    return i;
                }
            }
        }

        $scope.fillIntoGrid = function (data, daypointer, slotpointer, type, key) {
            var i = 1;
            var count = 1;
            var lecpra_count;
            var gtype = 'no';
            $scope.isSubjectExistCount = 0;
            $scope.changeSlotCalledCount = 0;
            $scope.insPending = false;
            $scope.sameSlotRoom = false;
            $scope.slotroomArr = [];

            if (data.sims_bell_slot_room != '01') {
                $scope.sameSlotRoom = true;

                if ($(".contentpart").find("[data-slotroom = " + data.sims_bell_slot_room + "]").length > 0) {
                    $(".contentpart").find("[data-slotroom = " + data.sims_bell_slot_room + "]").parents('td').each(function (k, v) {

                        if ($(v).parents('tr').attr('id') != data.sims_bell_grade_code + "_" + data.sims_bell_section_code) {
                            $scope.slotroomArr.push(v.id);
                        }
                    });
                }
            }

            if (data.sims_bell_slot_group.trim() != '01') {
                $scope.slot_type = 'group';
            } else {
                $scope.slot_type = 'single';
            }


            if (data.sims_bell_lecture_count != 0 && data.sims_bell_lecture_practical_count == 0) {

                if (type == 'preference') {
                    lecpra_count = data.lec_count;
                } else {
                    lecpra_count = data.sims_bell_lecture_count;
                }
            } else if (data.sims_bell_lecture_practical_count != 0 && data.sims_bell_lecture_count == 0) {
                lecpra_count = data.sims_bell_lecture_practical_count;
            }

            if ($scope.slot_type == 'group') {
                if ($("#" + data.sims_bell_grade_code + "_" + data.sims_bell_section_code).find('[slotgid="' + data.sims_bell_slot_group.trim() + '"]').length > 0) {
                    gtype = 'yes';
                } else {
                    gtype = 'no';
                }
            }

            if (daypointer > ($scope.start_day + $scope.iSlotDayDetail_list.length)) {
                daypointer = daypointer - $scope.iSlotDayDetail_list.length;
            }
            if (slotpointer > $scope.iSlotDayDetail_list[daypointer - $scope.start_day - 1].slot_code_list.length) {
                slotpointer = slotpointer - $scope.iSlotDayDetail_list[daypointer - $scope.start_day - 1].slot_code_list.length;
                daypointer++;
            }


            while (i <= lecpra_count) {
                $scope.slotroomflag = false;
                var id;

                if ($scope.sameSlotRoom == true && $scope.slotroomArr.length > 0) {
                    $scope.checkSlotRoom(data);
                }

                if ($scope.slotroomflag == true) {
                    id = data.sims_bell_grade_code + "_" + data.sims_bell_section_code + "_" + $scope.spl_daypointer + "_" + $scope.spl_slotpointer;
                    var sub_id = 'sub_' + $scope.spl_daypointer + "_" + $scope.spl_slotpointer + "_" + data.sims_bell_subject_code + "_" + data.sims_bell_teacher_code + "_" + data.sims_bell_grade_code + "_" + data.sims_bell_section_code;
                    if (!$("#" + id).is(':empty')) {
                        if (data.sims_bell_slot_group.trim() != $("#" + id).find('.dragablesubjects').data('slotgroup')) {
                            $scope.findNextEmptySlot(id, data.sims_bell_grade_code, data.sims_bell_section_code, $scope.spl_daypointer, $scope.spl_slotpointer);
                            if ($scope.nextSlotForSlotRoom.id != undefined) {
                                if ($("#" + id).hasClass('groupClass')) {
                                    $("#" + $scope.nextSlotForSlotRoom.id).addClass('groupClass occupied').removeClass('cellWidth');
                                    $("#" + $scope.nextSlotForSlotRoom.id).attr('slotgid', $("#" + id).attr('slotgid'));
                                }
                                $("#" + $scope.nextSlotForSlotRoom.id).html($("#" + id).html());
                                $("#" + id).html('').removeClass('groupClass occupied ').addClass('cellWidth');
                            }
                        }
                    }
                    var nextday = $scope.spl_daypointer + 1;
                    var nextslot = $scope.spl_slotpointer;

                    if (nextday > ($scope.start_day + $scope.iSlotDayDetail_list.length)) {
                        nextday = nextday - $scope.iSlotDayDetail_list.length;
                    }

                    var avl = {
                        id: id,
                        day: $scope.spl_daypointer,
                        slot: $scope.spl_slotpointer,
                        sub_id: sub_id,
                        nextday: nextday,
                        nextslot: nextslot
                    };
                }
                    //else if ($scope.subjectPreference.length > 0 || $scope.teacherPreference.length > 0) {
                    //        if ($scope.Subject_preference_rule == '1') {

                    //            if ($scope.subjectPreference.length >= 0) {
                    //                $($scope.subjectPreference).each(function (k, v) {
                    //                    //$scope.fillIntoGrid(v, v.day_code, v.slot_code, 'preference');
                    //                    if(v.sub_code == data.sub_code)
                    //                    console.log('$scope.subjectPreference = ', v);
                    //                    $scope.subjectPreference.splice(0, 1);
                    //                });
                    //            }

                    //            if ($scope.teacherPreference.length >= 0) {
                    //                $($scope.teacherPreference).each(function (k, v) {
                    //                    //$scope.fillIntoGrid(v, v.day_code, v.slot_code, 'preference');
                    //                    console.log('$scope.teacherPreference = ', v);
                    //                    $scope.teacherPreference.splice(0, 1);
                    //                });
                    //            }
                    //        }
                    //        else {
                    //            if ($scope.teacherPreference.length >= 0) {
                    //                $($scope.teacherPreference).each(function (k, v) {
                    //                    //$scope.fillIntoGrid(v, v.day_code, v.slot_code, 'preference');
                    //                    console.log('$scope.teacherPreference = ', v);
                    //                    $scope.teacherPreference.splice(0, 1);
                    //                });
                    //            }
                    //            if ($scope.subjectPreference.length >= 0) {
                    //                $($scope.subjectPreference).each(function (k, v) {
                    //                    //$scope.fillIntoGrid(v, v.day_code, v.slot_code, 'preference');
                    //                    console.log('$scope.subjectPreference = ', v);
                    //                    $scope.subjectPreference.splice(0, 1);
                    //                });
                    //            }
                    //        }
                    //    }
                else {
                    var avl = $scope.checkAvl(data.sims_bell_subject_code,
                        data.sims_bell_grade_code,
                        data.sims_bell_section_code,
                        data.sims_bell_teacher_code,
                        daypointer,
                        slotpointer,
                        gtype,
                        data.sims_bell_slot_group.trim(),
                        type,
                        data.sims_bell_slot_room);
                }

                if (avl != undefined && $scope.insPending != true) {
                    if ($scope.insertingPendingsFlag == true) {

                        if (avl.day != daypointer && avl.slot != slotpointer) {
                            break;
                        }
                    }
                    if ($scope.slotroomflag != true) {

                        if (type != 'preference' && type != 'parallel') {
                            $scope.isSubjectExist(data, avl);
                            if ($scope.result != 'false' && $scope.result != undefined) {
                                if (Math.abs(parseInt(avl.slot) - parseInt($scope.result.slot)) < 2) {
                                    if ($scope.insertingPendingsFlag != true) {
                                        $scope.changeSlot(avl.id, data);
                                        if ($scope.insPending != true) {
                                            if ($scope.changed_id != false) {
                                                id = $scope.changed_id;
                                            }
                                        } else {
                                            $scope.insertIntoPending(data);
                                            break;
                                        }
                                    } else {
                                        break;
                                    }
                                } else {
                                    if ($scope.insertingPendingsFlag == true) {
                                        $scope.pendingArr.splice($scope.pendingSubjectArrIndex, 1);
                                        //$("#pendingsubid").find('#sub_' + data.sims_bell_subject_code + '_' + data.sims_bell_teacher_code + '_' + data.sims_bell_grade_code + '_' + data.sims_bell_section_code + '_' + 0 + '').remove();
                                    }
                                    id = avl.id;
                                }
                            } else {
                                id = avl.id;
                            }

                        } else {
                            id = avl.id;
                        }
                    }

                    //if ($scope.slot_type == 'group') {

                    //    $("#" + id).addClass('groupClass').removeClass('cellWidth');
                    //}
                    //else {
                    //    $("#" + id).removeClass('groupClass').addClass('cellWidth');
                    //}

                    //var html = '<div ng-mouseover="showDetail($event)" data-teachercode="' + data.sims_bell_teacher_code + '" ng-mouseleave="hideDetail($event)" data-teacherallo="' + avl.day + '_' + avl.slot + '_' + data.sims_bell_teacher_code + '" id="' + avl.sub_id + '" class="dragablesubjects filled" data-slotgroup="' + data.sims_bell_slot_group.trim() + '" data-slotroom="' + data.sims_bell_slot_room + '" data-subcode="' + data.sims_bell_subject_code + '" data-color="' + data.sims_subject_color + '" draggable="true" ondragstart="drag(event)" style="background-color: ' + data.sims_subject_color + '">';
                    //html += '<div class="hoverdiv ellipsis" data-teachercode="' + data.sims_bell_teacher_code + '" data-teacher="' + data.sims_teacher_name + '" data-subname="' + data.sims_subject_name_en + '" data-secname="' + data.sims_section_name_en + '" data-grade="' + data.sims_bell_grade_code + '" data-gradename="' + data.sims_grade_name_en + '" ng-mouseover="showdetail($event)" ng-mouseleave="hideDetail($event)">' + data.sims_subject_name_en + '</div></div>';

                    //var temp = $compile(html)($scope);
                    //if ($scope.slot_type == 'group') {
                    //    $("#" + id).append(temp).addClass('occupied').removeClass('emptd');
                    //    //var total_count = $("#" + id).find('.dragablesubjects').length;
                    //    var total_count = data.slotGroupCount;
                    //    var divwidth = 100 / (total_count == 0 ? 1 : total_count);
                    //    $("#" + id).find('.dragablesubjects').css({ 'width': divwidth + '%' });
                    //    $("#" + id).attr('slotgid', data.sims_bell_slot_group.trim());
                    //}
                    //else {
                    //    $("#" + id).html(temp).addClass('occupied').removeClass('emptd');
                    //}
                    $scope.fillSlot(id, data, avl.day, avl.slot, avl.sub_id);
                    data.sims_bell_lecture_count = data.sims_bell_lecture_count - 1;


                } else {
                    $scope.pendingShow = true;
                    $scope.insertIntoPending(data);
                    break;
                }

                i++;
                daypointer++;
            }
            if (avl != undefined) {
                return {
                    next_day: avl.nextday,
                    next_slot: avl.nextslot
                };
            }
        }


        $scope.fillSlot = function (id, data, day, slot, sub_id) {


            if (data.sims_bell_slot_group.trim() != '01') {
                $scope.slot_type = 'group';
            } else {
                $scope.slot_type = 'single';
            }

            if ($scope.slot_type == 'group') {

                $("#" + id).addClass('groupClass').removeClass('cellWidth');
            } else {
                $("#" + id).removeClass('groupClass').addClass('cellWidth');
            }

            var html = '<div ng-mouseover="showDetail($event)" ng-mouseleave="hideDetail($event)" data-teacherallo="' + day + '_' + slot + '_' + data.sims_bell_teacher_code + '" id="' + sub_id + '" class="dragablesubjects filled" data-slotgroup="' + data.sims_bell_slot_group.trim() + '" data-teachercode="' + data.sims_bell_teacher_code + '" data-slotroom="' + data.sims_bell_slot_room + '" data-subcode="' + data.sims_bell_subject_code + '" data-color="' + data.sims_subject_color + '" draggable="true" ondragstart="drag(event)" style="background-color: ' + data.sims_subject_color + '">';
            html += '<div class="hoverdiv ellipsis" data-teachercode="' + data.sims_bell_teacher_code + '" data-teacher="' + data.sims_teacher_name + '" data-subname="' + data.sims_subject_name_en + '" data-secname="' + data.sims_section_name_en + '" data-grade="' + data.sims_bell_grade_code + '" data-gradename="' + data.sims_grade_name_en + '" ng-mouseover="showdetail($event)" ng-mouseleave="hideDetail($event)">' + data.sims_subject_name_en + '</div></div>';

            var temp = $compile(html)($scope);
            if ($scope.slot_type == 'group') {
                $("#" + id).append(temp).addClass('occupied').removeClass('emptd');
                //var total_count = $("#" + id).find('.dragablesubjects').length;
                var total_count = data.slotGroupCount;
                var divwidth = 100 / (total_count == 0 ? 1 : total_count);
                $("#" + id).find('.dragablesubjects').css({
                    'width': divwidth + '%'
                });
                $("#" + id).attr('slotgid', data.sims_bell_slot_group.trim());
            } else {
                $("#" + id).html(temp).addClass('occupied').removeClass('emptd');
            }
        }

        $scope.findNextEmptySlot = function (id, grade_code, sec_code, daypointer, slotpointer) {

            var subcode = $("#" + id).find('.dragablesubjects').data('subcode');
            var slotroom = $("#" + id).find('.dragablesubjects').data('slotroom');
            var slotgroup = $("#" + id).find('.dragablesubjects').data('slotgroup');
            var teachercode = $("#" + id).find('.hoverdiv').data('teachercode');

            outerloop:
                for (var j = daypointer; j <= ($scope.start_day + $scope.iSlotDayDetail_list.length) ; j++) {
                    for (var k = slotpointer; k <= $scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list.length; k++) {
                        if ($("#" + grade_code + "_" + sec_code + "_" + (j) + "_" + (k)).is(':empty')) {

                            $scope.isTeacerAvailable(j, k, teachercode, slotgroup, slotroom, subcode);
                            if ($scope.teacheralloc == false) {
                                $scope.nextSlotForSlotRoom = {
                                    'id': grade_code + "_" + sec_code + "_" + j + "_" + k
                                };
                                break outerloop;
                            } else {
                                continue;
                            }
                        }
                    }
                }

        }

        $scope.checkSlotRoom = function (data) {
            var tempid = angular.copy($scope.slotroomArr[0]);
            $scope.spl_daypointer = $("#" + tempid).data('day');
            $scope.spl_slotpointer = $("#" + tempid).data('slot');

            if ($("#" + data.sims_bell_grade_code + "_" + data.sims_bell_section_code + "_" + $scope.spl_daypointer + "_" + $scope.spl_slotpointer).find('[data-teachercode = ' + data.sims_bell_teacher_code + ']').length == '0') {
                $scope.slotroomflag = true;
            }
            $scope.slotroomArr.splice(0, 1);
        }

        $scope.insertIntoPending = function (data, type) {

            var dummy_day = Math.floor(Math.random() * 5) + 1;
            var dummy_slot = Math.floor(Math.random() * 8) + 1;

            if (type == 'editversion') {
                var html = '<div ng-mouseover="showDetail($event)" data-gradename="' + data.grade_name + '" ng-click="goToRow(\'' + data.grade_code + '_' + data.section_code + '\')"    ng-mouseleave="hideDetail($event)" data-teachercode="' + data.teacher_code + '" id="sub_0_0_' + data.subject_code + '_' + data.teacher_code + '_' + data.grade_code + '_' + data.section_code + '" trid="' + data.grade_code + '_' + data.section_code + '" class="dragablesubjects filled" data-slotgroup="' + data.slot_group + '" data-slotroom="' + data.slot_room + '" data-subcode="' + data.subject_code + '" data-color="' + data.subject_color + '" draggable="true" ondragstart="drag(event)" style="background-color: ' + data.subject_color + '">';
                html += '<div class="hoverdiv ellipsis" data-gradename="' + data.grade_name + '" data-teachercode="' + data.teacher_code + '" data-teacher="' + data.teacher_name + '" data-subname="' + data.subject_name + '" data-secname="' + data.section_name + '" data-grade="' + data.grade_code + '" ng-mouseover="showdetail($event)" ng-mouseleave="hidedetail($event)">' + data.subject_name + '</div>';
                html += '</div>';

                var temp = $compile(html)($scope);
                $("#pendingsubid").append(temp);
            } else {
                if ($scope.PendingViewd == true) {
                    var i;
                    for (i = 0; i < data.sims_bell_lecture_count; i++) {

                        var html = '<div   ng-mouseleave="hideDetail($event)" data-teacher="' + data.sims_teacher_name + '" data-teachercode="' + data.sims_bell_teacher_code + '" data-subname="' + data.sims_subject_name_en + '" data-secname="' + data.sims_section_name_en + '" data-grade="' + data.sims_grade_name_en + '" data-gradename="' + data.sims_grade_name_en + '" ng-mouseover="showDetail($event)" ng-mouseleave="hideDetail($event)" ng-click="goToRow(\'' + data.sims_bell_grade_code + '_' + data.sims_bell_section_code + '\')" data-slotroom="' + data.sims_bell_slot_room + '"  id="sub_0_0_' + data.sims_bell_subject_code + '_' + data.sims_bell_teacher_code + '_' + data.sims_bell_grade_code + '_' + data.sims_bell_section_code + '_' + i + '" data-color="' + data.sims_subject_color + '" data-subcode="' + data.sims_bell_subject_code + '" trid="' + data.sims_bell_grade_code + '_' + data.sims_bell_section_code + '" class="dragablesubjects" data-slotgroup="' + data.sims_bell_slot_group.trim() + '" draggable="true" ondragstart="drag(event)" style="background-color: ' + data.sims_subject_color + '">';
                        html += '<div class="hoverdiv ellipsis" data-teacher="' + data.sims_teacher_name + '" data-teachercode="' + data.sims_bell_teacher_code + '" data-subname="' + data.sims_subject_name_en + '" data-secname="' + data.sims_section_name_en + '" data-grade="' + data.sims_grade_name_en + '" data-gradename="' + data.sims_grade_name_en + '" ng-mouseover="showDetail($event)" ng-mouseleave="hideDetail($event)" >' + data.sims_subject_name_en + '</div>';
                        html += '</div>';


                        var temp = $compile(html)($scope);
                        $("#pendingsubid").append(temp);
                    }
                } else {
                    $scope.inpendingexist = false;

                    $($scope.pendingArr).each(function (k, v) {
                        if (data.sims_bell_subject_code == v.sims_bell_subject_code && data.sims_bell_teacher_code == v.sims_bell_teacher_code) {
                            $scope.inpendingexist = true;
                            return false;
                        }
                    })

                    if (!$scope.inpendingexist) {
                        $scope.pendingArr.push(data);
                    }
                }
            }
        }



        $scope.insertIntoSlot = function (data) {
            debugger
            var html = '<div ng-mouseover="showDetail($event)" ng-mouseleave="hideDetail($event)" id="sub_' + data.day_code + '_' + data.slot_code + '_' + data.subject_code + '_' + data.teacher_code + '_' + data.grade_code + '_' + data.section_code + '" class="dragablesubjects filled" data-slotgroup="' + data.slot_group + '" data-slotroom="' + data.slot_room + '" data-gradename="' + data.grade_name + '" data-subcode="' + data.subject_code + '" data-color="' + data.subject_color + '" data-secname="' + data.section_name + '" data-teacherallo="' + data.day_code + '_' + data.slot_code + '_' + data.teacher_code + '" draggable="true" ondragstart="drag(event)" style="background-color: ' + data.subject_color + '">';
            html += '<div class="hoverdiv ellipsis" data-teachercode="' + data.teacher_code + '" data-teacher="' + data.teacher_name + '" data-subname="' + data.subject_name + '" data-secname="' + data.section_name + '" data-grade="' + data.grade_code + '" data-gradename="' + data.grade_name + '" ng-mouseover="showdetail($event)" ng-mouseleave="hidedetail($event)">' + data.subject_name + '</div>';
            html += '</div>';

            var id = data.grade_code + "_" + data.section_code + "_" + data.day_code + "_" + data.slot_code;

            $("#" + id).addClass('occupied').removeClass('emptd');
            var temp = $compile(html)($scope);
            if (data.slot_group == "1") {
                $("#" + id).html(temp).removeClass('groupClass').addClass('cellWidth ');
            } else {
                $("#" + id).addClass('groupClass').removeClass('cellWidth');
                $("#" + id).append(temp);
                var total_count = $("#" + id).find('.dragablesubjects').length;
                var divwidth = 100 / total_count;
                $("#" + id).find('.dragablesubjects').css({
                    'width': divwidth + '%'
                });
            }
        }

        $scope.goToRow = function (id) {

            $("#fixTable .trrow").removeClass('activetr')
            $("#" + id).addClass('activetr');
            var header_height = $(".headerpart").height() + 1;

            $('.slotArea').scrollTop(($("#" + id).index()) * 24);
        };

        $scope.isTeacerAvailable = function (day, slot, teacher_code, slot_group, slot_room, subject_code) {
            var bFlag = $scope.data[teacher_code] === undefined || $scope.data[teacher_code][day.toString()] === undefined || $scope.data[teacher_code][day.toString()][slot.toString()] === undefined || $scope.data[teacher_code][day.toString()][slot.toString()] != "2";
            // $scope.teacheralloc represents if the teacher is already allocated or not, If $scope.teacheralloc is false it sends the message that the teacher is free in the day/slot
            if (bFlag) { //this condition is to check if the teacher is already blocked as per the Teacher Allocation creteria
                if ($("#fixTable .trrow").find('[data-teacherallo=' + day + '_' + slot + '_' + teacher_code + ']').length > 0) {
                    var slot_g = $("#fixTable .trrow").find('[data-teacherallo=' + day + '_' + slot + '_' + teacher_code + ']').data('slotgroup');
                    var slot_r = $("#fixTable .trrow").find('[data-teacherallo=' + day + '_' + slot + '_' + teacher_code + ']').data('slotroom');
                    var sub_c = $("#fixTable .trrow").find('[data-teacherallo=' + day + '_' + slot + '_' + teacher_code + ']').data('subcode');
                    var grade_code = $("#fixTable .trrow").find('[data-teacherallo=' + day + '_' + slot + '_' + teacher_code + ']').parents('tr').data('gradecode');
                    var sec_code = $("#fixTable .trrow").find('[data-teacherallo=' + day + '_' + slot + '_' + teacher_code + ']').parents('tr').data('seccode');
                    var id = grade_code + "_" + sec_code + "_" + day + "_" + slot;

                    if (subject_code == sub_c /*&& slot_group == slot_g*/ && slot_room == slot_r && slot_group != '01' && slot_room != '01') {
                        $scope.teacheralloc = false;
                    } else {
                        $scope.teacheralloc = true;
                    }
                } else {
                    $scope.teacheralloc = false;
                }
            }
            else {
                $scope.teacheralloc = true;
            }
        }


        $scope.isTeacherBusyPreviousSlot = function (day, slot, teacher_code, slot_group, slot_room, subject_code, str2, str3) {

            if ($("#fixTable .trrow").find('[data-teacherallo=' + day + '_' + slot + '_' + teacher_code + ']').parents('tr').data('gradecode') == str2 && $("#fixTable .trrow").find('[data-teacherallo=' + day + '_' + slot + '_' + teacher_code + ']').parents('tr').data('seccode') == str3 && $("#fixTable .trrow").find('[data-teacherallo=' + day + '_' + slot + '_' + teacher_code + ']').length > 0) {
                $scope.checkBusyPrevious = $scope.checkBusyPrevious || true;
            } else {
                $scope.checkBusyPrevious = $scope.checkBusyPrevious || false;
            }
        }

        $scope.isSubjectExist = function (data, avl) {

            var p;
            for (p = 1; p <= $scope.iSlotDayDetail_list[avl.day - $scope.start_day - 1].slot_code_list.length; p++) {

                if ($("#" + data.sims_bell_grade_code + "_" + data.sims_bell_section_code + "_" + avl.day + '_' + p).find('.filled').length > 0) {
                    $("#" + data.sims_bell_grade_code + "_" + data.sims_bell_section_code + "_" + avl.day + '_' + p).find('.filled').each(function (k, v) {

                        if ($(v).data('subcode') == data.sims_bell_subject_code) {

                            $scope.result = ({
                                'id': data.sims_bell_grade_code + "_" + data.sims_bell_section_code + "_" + avl.day + '_' + p,
                                'day': avl.day,
                                'slot': p
                            });
                            return false;
                        } else {
                            $scope.result = 'false';
                        }
                    })
                } else {
                    $scope.result = 'false';
                }
                if ($scope.result != 'false') {
                    break;
                } else {
                    continue;
                }
            }
            $scope.isSubjectExistCount++;
        }

        $scope.changeSlot = function (tdid, data) {
            var gtype;
            if (data.sims_bell_slot_group.trim() != '01') {
                $scope.slot_type = 'group';
            } else {
                $scope.slot_type = 'single';
            }

            if ($scope.slot_type == 'group') {
                if ($("#" + data.sims_bell_grade_code + "_" + data.sims_bell_section_code).find('[slotgid="' + data.sims_bell_slot_group.trim() + '"]').length > 0) {
                    gtype = 'yes';
                } else {
                    gtype = 'no';
                }
            }

            var empty = 0;
            if ($("#" + tdid).parents('tr').find('.occupied').length > 0) {
                empty = $("#" + tdid).parents('tr').find('.dropabletd').length - $("#" + tdid).parents('tr').find('.occupied').length;
            }


            if ($scope.changeSlotCalledCount > empty) {
                $scope.insPending = true;
                $scope.result = false;
            }

            $scope.changeSlotCalledCount++;

            if ($scope.insPending != true) {
                var arr = tdid.split('_');
                var daypointer = parseInt(arr[2]);

                var slotpointer = (parseInt(arr[3]) + 3);

                if (daypointer > ($scope.start_day + $scope.iSlotDayDetail_list.length)) {
                    daypointer = daypointer - $scope.iSlotDayDetail_list.length;
                }


                if (slotpointer > $scope.iSlotDayDetail_list[daypointer - $scope.start_day - 1].slot_code_list.length) {
                    slotpointer = slotpointer - $scope.iSlotDayDetail_list[daypointer - $scope.start_day - 1].slot_code_list.length;
                    daypointer++;
                }

                var avl = $scope.checkAvl(data.sims_bell_subject_code,
                    data.sims_bell_grade_code,
                    data.sims_bell_section_code,
                    data.sims_bell_teacher_code,
                    daypointer,
                    slotpointer,
                    gtype,
                    data.sims_bell_slot_group.trim(), "",
                    data.sims_bell_slot_room);

                if (avl != undefined) {
                    $scope.isSubjectExist(data, avl);
                    if ($scope.result != 'false' && $scope.result != undefined) {
                        if (Math.abs(parseInt(avl.slot) - parseInt($scope.result.slot)) < 2) {
                            $scope.changeSlot(avl.id, data);
                        }
                    } else {
                        if ($scope.insPending != true) {
                            $scope.changed_id = avl.id;
                        }
                    }
                } else {
                    $scope.insertIntoPending(data);
                }
            }
        }


        $scope.checkAvl = function (str1, str2, str3, str4, daypointer, slotpointer, gtype, slot_group, type, slot_room) {

            $scope.gemptyfound = false;
            $scope.flag = '1';
            var arr = [];
            var tt = "";
            if (gtype == 'yes') {

                if ($("#" + str2 + "_" + str3).find('[slotgid= ' + slot_group + ']').length > 0) {
                    $("#" + str2 + "_" + str3).find('[slotgid= ' + slot_group + ']').each(function (k, v) {
                        if ($(v).find('[data-teachercode = ' + str4 + ']').length == '0') {

                            var id = $(v).attr('id');
                            var sib_sub_id = $(v).find('.dragablesubjects').attr('id');
                            var arr = sib_sub_id.split('_');
                            var day = arr[1];
                            var slot = arr[2];
                            var sub_id = 'sub_' + day + "_" + slot + "_" + str1 + "_" + str4 + "_" + str2 + "_" + str3;
                            var prev_id = str2 + "_" + str3 + "_" + day + "_" + slot;
                            tt = {
                                day: day,
                                slot: slot,
                                id: id,
                                sub_id: sub_id,
                                nextday: daypointer,
                                nextslot: slotpointer,
                                prev_id: prev_id,
                                slot_type: 'group'
                            };
                            $scope.gemptyfound = true;
                            return false;
                        } else {
                            $scope.gemptyfound = false;
                            return;
                        }
                    });
                }
                if (tt != "") {
                    $scope.flag = '0';
                    return tt;
                } else {
                    $scope.flag = '1';
                }
            }

            if ($scope.flag != '0') {
                if (daypointer > ($scope.start_day + $scope.iSlotDayDetail_list.length)) {
                    daypointer = daypointer - $scope.iSlotDayDetail_list.length;
                }

                if ($scope.iSlotDayDetail_list[daypointer - 1] != undefined && slotpointer > $scope.iSlotDayDetail_list[daypointer - $scope.start_day - 1].slot_code_list.length) {
                    slotpointer = slotpointer - $scope.iSlotDayDetail_list[daypointer - $scope.start_day - 1].slot_code_list.length;
                    daypointer++
                }

                if (type != 'preference') {
                    var isprevempty = $scope.checkPreviousEmptySlot(str2, str3, daypointer, slotpointer);
                    if (isprevempty != undefined && $scope.teacheralloc == false) {
                        slotpointer = isprevempty;
                    }
                }
                $scope.checkBusyPrevious = false;
                $scope.daycounter = $scope.start_day + 1;

                dayloop:
                    for (var j = daypointer; $scope.daycounter <= ($scope.start_day + $scope.iSlotDayDetail_list.length) ; j++) {
                        if (j > ($scope.start_day + $scope.iSlotDayDetail_list.length)) {
                            j = $scope.start_day + 1;
                        }
                        $scope.daycounter++;
                        slotloop:
                            for (var k = slotpointer, slotcounter = 1; slotcounter <= $scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list.length; k++) {

                                if (k > $scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list.length) {
                                    k = k - $scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list.length;
                                    //j = j + 1;
                                }
                                slotcounter++;
                                //console.log("j-1 = ",j-1);
                                if ($("#" + str2 + "_" + str3 + "_" + (j) + "_" + ($scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list[k - 1].slot_code)).is(':empty')) {

                                    $scope.isTeacerAvailable(j, $scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list[k - 1].slot_code, str4, slot_group, slot_room, str1);

                                    $scope.checkBusyPrevious = false;

                                    if (parseInt($scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list[k - 1].slot_code, 10) > 1 && (parseInt($scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list[k - 1].slot_code, 10) < ($scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list.length)) && type != 'preference') {
                                        $scope.isTeacherBusyPreviousSlot(j, parseInt($scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list[k - 1].slot_code, 10) - 1, str4, slot_group, slot_room, str1, str2, str3);
                                        $scope.isTeacherBusyPreviousSlot(j, parseInt($scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list[k - 1].slot_code, 10) + 1, str4, slot_group, slot_room, str1, str2, str3);
                                    }
                                    if ($scope.teacheralloc != false || $scope.checkBusyPrevious === true) {
                                        continue slotloop;
                                    }
                                    var sub_id = 'sub_' + (j) + "_" + ($scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list[k - 1].slot_code) + "_" + str1 + "_" + str4 + "_" + str2 + "_" + str3;

                                    var nextday = parseInt(j) + 1;
                                    var nextslot = $scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list[k - 1].slot_code;

                                    var id = str2 + "_" + str3 + "_" + (j) + "_" + ($scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list[k - 1].slot_code);

                                    if (nextday > ($scope.start_day + $scope.iSlotDayDetail_list.length)) {
                                        nextday = nextday - $scope.iSlotDayDetail_list.length;
                                    }

                                    if (nextslot > $scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list.length) {
                                        nextslot = nextslot - $scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list.length;
                                    }

                                    var prev_id = str2 + "_" + str3 + "_" + daypointer + "_" + slotpointer;
                                    arr = {
                                        day: (j),
                                        slot: ($scope.iSlotDayDetail_list[j - $scope.start_day - 1].slot_code_list[k - 1].slot_code),
                                        id: id,
                                        sub_id: sub_id,
                                        nextday: nextday,
                                        nextslot: nextslot,
                                        prev_id: prev_id
                                    };

                                    return arr;

                                } else {
                                    continue;
                                }
                            }
                    }
            }
        }

        $scope.checkPreviousEmptySlot = function (str2, str3, daypointer, slotpointer) {

            var k;
            for (var k = 1; k <= slotpointer; k++) {
                if ($("#" + str2 + "_" + str3 + "_" + (daypointer) + "_" + (k)).is(':empty')) {
                    return (k);
                }
            }
        }

        $scope.checkCalssTeacher = function (arr) {
            for (var i = 0; i < $scope.classTeacherPreference.length; i++) {

                if (arr['teacher_code'] == $scope.classTeacherPreference[i]['teacher_code'] && arr['grade_code'] == $scope.classTeacherPreference[i]['grade_code'] && arr['sec_code'] == $scope.classTeacherPreference[i]['sec_code']) {

                    return 'yes';
                    break;
                } else {
                    continue;
                }
            }

            return 'no';
        }

        $scope.findClassTeacher = function (str1, str2) {
            for (var i = 0; i < $scope.classTeacherPreference.length; i++) {
                if ($scope.classTeacherPreference[i]['grade_code'] == str1 && $scope.classTeacherPreference[i]['sec_code'] == str2) {
                    return $scope.classTeacherPreference[i]['teacher_code'];
                }
            }
        }

        $scope.fetchDataByTeacher = function (str1, str2, str3) {
            for (var i = 0; i < $scope.genTimeTableNewData.length; i++) {
                if ($scope.genTimeTableNewData[i]['sims_bell_grade_code'] == str1 && $scope.genTimeTableNewData[i]['sims_bell_section_code'] == str2 && $scope.genTimeTableNewData[i]['sims_bell_teacher_code'] == str3) {
                    return $scope.genTimeTableNewData[i];
                }
            }
        }

        $scope.checkSubjectPriority = function (toarr, daypointer, slotpointer) {
            var arr_par = ['aca_year', 'bell_code', 'grade_code', 'sec_code', 'sub_code'];
            var index = findIndex($scope.subjectPreference, toarr, arr_par, daypointer, slotpointer);

            if (index != '-1') {
                return $scope.subjectPreference[index]['preference'];
            }
        }

        $scope.checkTeacherPriority = function (toarr, daypointer, slotpointer) {
            var arr_par = ['aca_year', 'bell_code', 'grade_code', 'sec_code', 'teacher_code'];
            var index = findIndex($scope.teacherPreference, toarr, arr_par, daypointer, slotpointer);

            if (index != '-1') {
                return $scope.teacherPreference[index]['preference'];
            }
        }


        function findIndex(array, toarr, attr, day, slot) {
            for (var i = 0; i < array.length; i++) {
                var flag_index = [];

                if ((array[i]['slot_code'] == slot && array[i]['day_code'] == day)) {
                    $(attr).each(function (k, v) {
                        if (array[i][v] == toarr[v]) {
                            flag_index.push(1);
                        } else {
                            flag_index.push(0);
                        }
                    });
                }

                if (jQuery.inArray(0, flag_index) == -1) {
                    return i;
                };
            }
            return -1;
        }

        $scope.saveVersionName = function () {

            if ($scope.versionName != undefined) {
                if ($scope.versionName != "") {
                    $scope.versionNameReq = false;
                    $scope.draft();
                } else {
                    $scope.versionNameReq = true;
                }
            }
        }

        $scope.draft = function () {

            $("#saveVersion").modal('hide');
            $scope.loaderShow = true;
            var version, type;
            if ($('#exportTT').hasClass("editmode")) {
                version = $("#exportTT").attr('version');
                type = 'edit';
            } else {
                version = $scope.versionName;
                type = 'add';
            }

            var finaldata = $scope.makeObjOfAllocatedSubjects();

            if (finaldata.length > 0) {
                $scope.loadershow = true;
                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableHistorysave?ttVersion=" + version + "&type=" + type, finaldata).then(function (res) {

                    var html = '<div class="inline-block version_item">';
                    html += '<div class="p-t-5 p-b-5 p-l-10 p-r-10 m-b-5 m-t-5 primary-bg text-white saved_tt_item">';
                    html += '<span>' + res.data + '</span> <span class="pull-right tt_edit_icons "><i class="material-icons cursor-pointer" ng-click="deleteVersion($event, \'' + res.data + '\')">clear</i><i class="material-icons cursor-pointer" ng-click="editVersion($event, \'' + res.data + '\')">create</i></span>';
                    html += '</div>';
                    html += '</div>';
                    var temp = $compile(html)($scope);
                    if (!$("#exportTT").hasClass("editmode")) {
                        $("#version_div").append(temp);
                    }
                    $scope.loaderShow = false;
                });
            }
        }

        $scope.IterationLoop = function () {

            var finaldata = $scope.makeObjOfAllocatedSubjects();

            if (finaldata.length > 0) {
                $scope.loadershow = true;
                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableIterationsave?iNumber=" + $scope.iNumber, finaldata).then(function (res) {
                    $scope.iNumber = $scope.iNumber - 1;
                    if ($scope.iNumber == 0) {
                        $scope.getBestTT();
                    } else {
                        $scope.genTimeTableNew();
                    }
                });
            }
        }

        $scope.getBestTT = function () {
            $scope.editversionshow = false;
            $('body').removeClass('viewtt');
            $("#fixTable .trrow").removeClass('activetr');
            $scope.pendingShow = true;
            $(".contentpart td.dropabletd").html('').removeClass('occupied').addClass('emptd').attr('slotgid', '');
            $(".contentpart td.count_td").html('');
            $("#pendingsubid").html('');
            $(".slotArea").removeClass("editmode").attr('version', '');
            $scope.enableSave = false;
            $http.post(ENV.apiUrl + "api/SchoolTimeTable/BestIterativeTTRetrieval?aca_year=" + $scope.aca_year + "&bell_code=" + $scope.bell_code + "&cur_code=" + $scope.cur_code).then(function (res) {

                $scope.editVersionData = res.data;
                $($scope.editVersionData).each(function (k, v) {
                    if (v.day_code == '0') {
                        $scope.insertIntoPending(v, 'editversion');
                    } else {
                        $scope.insertIntoSlot(v);
                    }
                })
            }).then(function () {
                $("#fixTable .trrow").each(function (k, v) {
                    $(v).find('.count_td').html($(v).children('.occupied').length);
                    $scope.loaderShow = false;
                })
            });
        }

        $scope.deleteIterationData = function () {
            var a = {
                opr: 'U',
                AcaYear: $scope.aca_year,
                cur_code: $scope.cur_code,
                bellCode: $scope.bell_code
            };
            $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableViewData", a).then(function (res) { });
        }

        $scope.genIterativeTT = function () {
            $scope.loaderShow = true;
            $scope.iterativeTTFlag = true;
            $scope.deleteIterationData();
            $scope.iNumber = 3;
            //for (var count = 3; count > 0; count--) {
            $scope.genTimeTableNew();
            //$scope.IterationLoop();
            //}
        }

        $scope.makeObjOfAllocatedSubjects = function () {
            var finalData = [];
            //if ($(".slotArea table").children('.filled').length > 0)
            {
                $("#fixTable .trrow").each(function (key, val) {
                    if ($(val).find('.filled').length != 0) {
                        var str = $(val).attr('id');
                        var str1 = str.split("_");
                        var grade_code = str1[0];
                        var grade_name = $(val).data('grade');
                        var sec_code = str1[1];
                        var sec_name = $(val).data('secname');
                        $(val).find(".filled").each(function (k, v) {
                            var slot_group = $(v).data('slotgroup');
                            var slot_room = $(v).data('slotroom');
                            var str = $(v).parents('td').attr('id');
                            var str1 = str.split("_");
                            var day_code = str1[2];
                            var slot_code = str1[3];
                            var sub_color = $(v).data('color');
                            var sub_name = $(v).find('.hoverdiv').data('subname');
                            var subject_code = $(v).data('subcode');
                            var teacher_code = $(v).find('.hoverdiv').data('teachercode');
                            var teacher_name = $(v).find('.hoverdiv').data('teacher').trim();
                            finalData.push({
                                'aca_year': $scope.aca_year,
                                'cur_code': $scope.cur_code,
                                'bell_code': $scope.bell_code,
                                'grade_code': grade_code,
                                'grade_name': grade_name,
                                'sec_code': sec_code,
                                'sec_name': sec_name,
                                'slot_group': slot_group,
                                'slot_room': slot_room,
                                'day_code': day_code,
                                'slot_code': slot_code,
                                'sub_color': sub_color,
                                'sub_name': sub_name,
                                'subject_code': subject_code,
                                'teacher_code': teacher_code,
                                'teacher_name': teacher_name
                            });
                        });
                    }
                });
            }




            if ($("#pendingsubid .dragablesubjects").length > 0) {
                $("#pendingsubid .dragablesubjects").each(function (k, v) {

                    var str1 = $(v).attr('trid').split("_");
                    var grade_code = str1[0];
                    var slot_room = $(v).data('slotroom');
                    var grade_name = $(v).find('.hoverdiv').data('grade');
                    var sec_code = str1[1];
                    var sec_name = $(v).find('.hoverdiv').data('secname');
                    var str2 = $(v).attr('id').split("_");
                    var slot_group = $(v).data('slotgroup');
                    var day_code = 0;
                    var slot_code = 0;
                    var sub_color = $(v).data('color');
                    var sub_name = $(v).find('.hoverdiv').data('subname');
                    var subject_code = $(v).data('subcode');
                    var teacher_code = $(v).find('.hoverdiv').data('teachercode');
                    var teacher_name = $(v).find('.hoverdiv').data('teacher').trim();
                    finalData.push({
                        'aca_year': $scope.aca_year,
                        'cur_code': $scope.cur_code,
                        'bell_code': $scope.bell_code,
                        'grade_code': grade_code,
                        'grade_name': grade_name,
                        'sec_code': sec_code,
                        'sec_name': sec_name,
                        'slot_group': slot_group,
                        'slot_room': slot_room,
                        'day_code': day_code,
                        'slot_code': slot_code,
                        'sub_color': sub_color,
                        'sub_name': sub_name,
                        'subject_code': subject_code,
                        'teacher_code': teacher_code,
                        'teacher_name': teacher_name
                    });

                });
            }

            if ($("#hiddenpending .dragablesubjects").length > 0) {
                $("#hiddenpending .dragablesubjects").each(function (k, v) {

                    var str1 = $(v).attr('trid').split("_");
                    var grade_code = str1[0];
                    var grade_name = $(v).find('.hoverdiv').data('grade');
                    var sec_code = str1[1];
                    var sec_name = $(v).find('.hoverdiv').data('secname');
                    var str2 = $(v).attr('id').split("_");
                    var slot_group = $(v).data('slotgroup');
                    var slot_room = $(v).data('slotroom');
                    var day_code = 0;
                    var slot_code = 0;
                    var sub_color = $(v).data('color');
                    var sub_name = $(v).find('.hoverdiv').data('subname');
                    var subject_code = $(v).data('subcode');
                    var teacher_code = $(v).find('.hoverdiv').data('teachercode');
                    var teacher_name = $(v).find('.hoverdiv').data('teacher').trim();
                    finalData.push({
                        'aca_year': $scope.aca_year,
                        'cur_code': $scope.cur_code,
                        'bell_code': $scope.bell_code,
                        'grade_code': grade_code,
                        'grade_name': grade_name,
                        'sec_code': sec_code,
                        'sec_name': sec_name,
                        'slot_group': slot_group,
                        'slot_room': slot_room,
                        'day_code': day_code,
                        'slot_code': slot_code,
                        'sub_color': sub_color,
                        'sub_name': sub_name,
                        'subject_code': subject_code,
                        'teacher_code': teacher_code,
                        'teacher_name': teacher_name
                    });

                });
            }

            if (finalData.length > 0) {
                return finalData;
            } else {
                $scope.messageShow = true;
                $scope.message = "Time Table Slots are Empty";
            }
        }


        $scope.editVersion = function (e, ttVersion) {
            $scope.enableSave = false;
            $scope.loaderShow = true;
            $scope.editversionshow = true;
            $(".contentpart td.count_td").html('');
            $("#fixTable .trrow").removeClass('activetr');
            $('body').removeClass('viewtt');
            $(".contentpart td.dropabletd").html('').removeClass('occupied').addClass('emptd');
            $("#pendingsubid").html('');
            $("#exportTT").attr('version', ttVersion).addClass('editmode');
            $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableHistoryDataRetrieval?aca_year=" + $scope.aca_year + "&bell_code=" + $scope.bell_code + "&ttVersion=" + ttVersion + "&cur_code=" + $scope.cur_code).then(function (res) {

                $scope.editVersionData = res.data;
                $($scope.editVersionData).each(function (k, v) {
                    if (v.day_code == '0') {
                        $scope.insertIntoPending(v, 'editversion');
                    } else {

                        $scope.insertIntoSlot(v);
                    }
                })
            }).then(function () {
                $("#fixTable .trrow").each(function (k, v) {
                    $(v).find('.count_td').html($(v).children('.occupied').length);
                    $scope.loaderShow = false;
                })
            });
        }

        $scope.deleteVersion = function (e, ttVersion) {
            $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableHistoryDelete?aca_year=" + $scope.aca_year + "&bell_code=" + $scope.bell_code + "&ttVersion=" + ttVersion).then(function (res) {
                $(e.currentTarget).parents('.version_item').remove();
            });
        }

        $scope.sortTable = function (e, index, str) {
            $scope.temporder;

            if ($scope.temporder == 'asc') {
                $scope.temporder = 'desc';
                $(e.currentTarget).html('<i class="material-icons">arrow_drop_down</i>');
            } else {
                $scope.temporder = 'asc';
                $(e.currentTarget).html('<i class="material-icons">arrow_drop_up</i>');
            }
            var table, rows, switching, i, x, y, shouldSwitch;
            table = document.getElementById("fixTableTeacher");
            switching = true;
            while (switching) {
                switching = false;
                rows = table.getElementsByClassName("trrow");
                for (i = 0; i < (rows.length) ; i++) {
                    shouldSwitch = false;
                    x = rows[i].getElementsByTagName("TD")[index];
                    y = rows[i + 1].getElementsByTagName("TD")[index];
                    var temp;
                    if ($scope.temporder == 'asc') {
                        if (str == 'str') {
                            temp = x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase();
                        } else {
                            temp = parseInt(x.innerHTML) > parseInt(y.innerHTML);
                        }

                    } else {
                        if (str == 'str') {
                            temp = x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase();
                        } else {
                            temp = parseInt(x.innerHTML) < parseInt(y.innerHTML);
                        }
                    }
                    if (temp) {
                        shouldSwitch = true;
                        break;
                    }
                }
                if (shouldSwitch) {
                    rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                    switching = true;
                }
            }
        }

        $scope.hidePanel = function (e) {
            var slotarea_height = $(".slotArea").height();
            var div_height = $(e.target).parents('.hide_panel').outerHeight();
            var final_height = slotarea_height + div_height;

            $(".slotArea").height(final_height);

            $(e.target).parents('.hide_panel').hide();
            var id = $(e.target).parents('.hide_panel').attr('id');
            var title = $(e.target).parents('.hide_panel').find('h5').text();
            var html = '<span class="m-t-5 m-b-5 m-l-5 m-r-5 cursor-pointer" data-height = "' + div_height + '" data-id="' + id + '" ng-click="show_div($event)">' + title + '</span>';
            var temp = $compile(html)($scope);
            $('.hide_div_container').append(temp);
        }

        $scope.show_div = function (e) {
            var slotarea_height = $(".slotArea").height();
            var div_height = $(e.target).data('height');
            var final_height = slotarea_height - div_height;
            $(".slotArea").height(final_height);

            var class_name = $(e.target).data('id');
            $(e.target).remove();
            $('#' + class_name).show();
        }

        $scope.resetPendingSubjects = function () {
            $("#fixTable .trrow").removeClass('activetr');
            $("#pendingsubid").append($("#hiddenpending .dragablesubjects"));
        }


        $scope.immport = function (table) {
            $(".count_td").css({
                'background-color': '#fff'
            });
            $(".top_fix").find('.cursor-pointer i').remove();
            var filename;
            if (table == '') {
                filename = 'Time Table';
            } else {
                filename = 'Teacher Time Table';
            }

            var check = true;

            if (check == true) {
                swal({
                    title: '',
                    text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + ' ' + filename + ".xls ?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 390,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {

                        var blob = new Blob([document.getElementById(table).innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        saveAs(blob, $scope.obj1.lic_school_name + " Time Table" + ".xls");
                        $(".count_td").css({
                            'background-color': '#000'
                        });
                        $(".top_fix").find('.cursor-pointer').html('<i class="material-icons">arrow_drop_down</i>');

                    }
                });
            } else {
                swal({
                    text: "Report Not Save",
                    imageUrl: "assets/img/close.png",
                    showCloseButton: true,
                    width: 380,
                })
            }
        }

        $scope.messagehide = function () {
            var temp = $compile('<h5 class="semi-bold text-white">{{message}}</h5><button class="btn btn-small btn-primary m-b-10">Ok</button>')($scope);
            $("#showmessage").html(temp);
            $scope.messageShow = false;
            $("#showmessage").addClass('ng-hide');
        }

        $scope.getSlotDays = function () {
            var a = {
                opr: 'T',
                cur_code: $scope.cur_code
            };
            $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableView", a).then(function (res) {
                $scope.oSlotDayNames = [];
                $(res.data.table).each(function (k, v) {
                    $scope.oSlotDayNames.push(v.sims_appl_form_field_value1);
                });
            });
        }

        $(document).ready(function () {
            $scope.getCurriculum();
            $scope.condenseMenu();
            $scope.loaderShow = true;
            $scope.enableSave = true;
            $scope.editversionshow = false;
            $scope.teacherViewShow = false;
            $scope.bellLoaded = true;
            $scope.oSlotDayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

        });

    }]);
})();