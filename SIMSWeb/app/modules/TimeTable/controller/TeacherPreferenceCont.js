﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.TimeTable');
    var iSlotCount; //= [7,7,7,7,5];
    var iSlotDay; //= 5;
    var oSlotDayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TeacherPreferenceCont',
        ['$scope', '$compile', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$locale', '$http', 'ENV', function ($scope, $compile, $state, $rootScope, $timeout, gettextCatalog, $locale, $http, ENV) {
            $scope.getAcademicYear = function () {
                $http.post(ENV.apiUrl + "api/SubjectTeacher/ImportAcademicYear").then(function (getAcaYear) {
                    $scope.AllAcademicYear = getAcaYear.data;
                });
            };

            $scope.setAcaYear = function (str) {
                $scope.academicYear = str;
                $scope.getBellData($scope.academicYear);
            }

            $scope.getBellData = function () {
               
                var a = { Opr: 'B', AcaYear: $scope.academicYear };
                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableImport", a).then(function (res) {
                    $scope.AllBellData = res.data.table;
                });
            };

            $scope.getGrade = function (str) {
                $scope.bell_code = str;
                $http.post(ENV.apiUrl + "api/PreferenceController/ImportGrades?aca_year=" + $scope.academicYear + "&bell_code=" + $scope.bell_code).then(function (res) {
                    $scope.AllGrades = res.data;
                });
            };

            $scope.getsection = function (str) {
                $scope.sec_code = "";
                $scope.grade_code = str;
                $http.post(ENV.apiUrl + "api/SubjectTeacher/ImportAllSection?aca_year=" + $scope.academicYear + "&param=" + $scope.grade_code).then(function (res) {
                    $scope.getSectionFromGrade = res.data;
                    
                });
            };

            $scope.getTeachers = function (str) {
                $(".preference_container").removeClass('hide');
                $scope.sec_code = str;
                var obj = { opr: 'T', aca_year: $scope.academicYear, bell_code: $scope.bell_code, grade_code: $scope.grade_code, sec_code: $scope.sec_code };
            
                $http.post(ENV.apiUrl + "api/PreferenceController/fetchAllTeacher", obj).then(function (res) {
                    $scope.getAllTeacher = res.data;
                });
                $scope.getDaySlot();
                $scope.savedTeacherPreference();
            }

            $scope.getDaySlot = function () {
                var slot = [];
                $http.post(ENV.apiUrl + "api/PreferenceController/ImportDaySlot?aca_year=" + $scope.academicYear + "&bell_code=" + $scope.bell_code).then(function (res) {
                    $scope.DaySlotCount = res.data;
                    var i;
                    for (i = 1; i <= $scope.DaySlotCount[0].slot_count; i++) {
                        slot.push(i);
                    }

                    $scope.slot_count = slot;

                });
            }

            $scope.addAnotherRow = function () {

                $(".savePreBtn").prop("disabled", false);

                var html = '<div  class="row margin-b-medium tea_pre_row"><div class="col-lg-1 col-md-1 col-sm-12 p-r-5 text-center "><i class="cursor-pointer material-icons" ng-click="removeRow($event)">close</i></div>';
                html += '<div class="col-lg-3 col-md-3 col-sm-12 p-r-5 ">';
                html += '<select class="input-sm full-width day_select">';
                html += '<option selected disabled value="">Please Select Day</option>';
                html += '<option ng-repeat="day in DaySlotCount" value="{{day.day_count}}">{{oSlotDayNames[day.day_count]}}</option>';
                html += '</select>';
                html += '</div>';
                html += '<div class="col-lg-3 col-md-3 col-sm-12 p-r-5 p-l-5 ">';
                html += '<select class="input-sm full-width slot_select">';
                html += '<option selected disabled value="">Please Select Slot</option>';
                html += '<option ng-repeat="x in slot_count">{{x}}</option>';
                html += '</select>';
                html += '</div>';
                html += '<div class="col-lg-3 col-md-3 col-sm-12 p-r-5 p-l-5 ">';
                html += '<select class="input-sm full-width teacher_select ">';
                html += '<option selected disabled value="">Please Select Teacher</option>';
                html += '<option ng-repeat="teacher in getAllTeacher" value="{{teacher.teacher_code}}">{{teacher.teacher_name}}</option>';
                html += '</select>';
                html += '</div>';
                html += '<div class="col-lg-2 col-md-2 col-sm-12 p-r-5 p-l-5 ">';
                html += '<select class="input-sm full-width priority_select">';
                html += '<option selected disabled value="">Please Select Subject Preference</option>';
                html += '<option ng-repeat="pre in AllPreferences">{{pre}}</option>';
                html += '</select></div></div>';


                var temp = $compile(html)($scope);
                $("#setpriority").append(temp);

            }


            var options = [];

            $scope.selectSection = function (event) {

                var $target = $(event.currentTarget),
                    val = $target.attr('data-value'),
                    $inp = $target.find('input'),
                    idx;

                if ((idx = options.indexOf(val)) > -1) {
                    options.splice(idx, 1);
                    setTimeout(function () { $inp.prop('checked', false) }, 0);
                } else {
                    options.push(val);
                    setTimeout(function () { $inp.prop('checked', true) }, 0);
                }

                $(event.target).blur();
                return false;
            }

            $scope.saveTeacherPreference = function ()
            {
                var tea_pri = [];

                $("#setpriority .row").each(function (key, val)
                {
                    var day_code = $(val).find('.day_select').val();
                    var slot_code = $(val).find('.slot_select').val();
                    var teacher_code = $(val).find('.teacher_select').val();
                    var teacher_name = $(val).find('.teacher_select option:selected').text();
                    var preference = $(val).find('.priority_select').val();
                  
                    if (day_code != undefined && slot_code != undefined && teacher_code != undefined && preference != undefined)
                    {
                        tea_pri.push({ aca_year: $scope.academicYear, bell_code: $scope.bell_code, grade_code: $scope.grade_code, sec_code: $scope.sec_code, day_code: day_code, slot_code: slot_code, teacher_code: teacher_code, preference: preference });
                        $http.post(ENV.apiUrl + "api/PreferenceController/SaveTeacherPreference", tea_pri).then(function (res) {
                            if (res.data == true) {
                                var d = new Date();
                                var n = d.getTime();
                                var thisobj = '0';

                                var html = '<tr " data-slot="' + slot_code + '" data-subname="' + teacher_name + '" data-day="' + day_code + '" data-grade="' + $scope.grade_code + '" data-section="' + $scope.sec_code + '" data-subcode="' + teacher_code + '" data-preference="' + preference + '">';
                                html += '<td class=" ">';
                                html += '<div class="checkbox check-default">';
                                html += '<input id="check_' + n + '" type="checkbox" value="check_' + n + '" class="row_checkbox">';
                                html += '<label for="check_' + n + '"></label>';
                                html += '</div>';
                                html += '</td>';
                                html += '<td class="day_td " style="width:10%"><span class="muted ">' + $scope.oSlotDayNames[day_code] + '</span></td>';
                                html += '<td class="slot_td" style="width:15%"><span class="muted">' + slot_code + '</span></td>';
                                html += '<td class="sub_td" style="width:12%"><span class="muted">' + teacher_name + '</span></td>';
                                html += '<td class="pre_td" style="width:10%"><span class="muted">' + preference + '</span></td>';
                                html += '<td style="width: 5%"><i ng-click="editPreferenceRow($event, ' + teacher_code + ', ' + day_code + ', ' + slot_code + ', ' + preference + ', ' + thisobj + ' )" class="material-icons cursor-pointer edit_row">edit</i> <i ng-click="savePreferenceRow($event)" class="material-icons hide save_row cursor-pointer">done</i></td>';
                                html += '<td style="width: 5%"><i ng-click="deletePreferenceRow($event, "single")" class="material-icons cursor-pointer delete_icon">delete_forever</i></td>';
                                html += '</tr>';

                                var temp = $compile(html)($scope);
                                $("#savedSubjectPreference tbody").prepend(temp);

                                $("#setpriority").html('');
                                $(".savePreBtn").prop('disabled', true);

                            }
                        });
                    }
                });
                
            }

            $scope.removeRow = function(e)
            {
                if ($(e.currentTarget).parents('.tea_pre_row').siblings('.tea_pre_row').size() == 0) {
                    $(".savePreBtn").prop('disabled', true);
                }
                $(e.currentTarget).parents('.tea_pre_row').remove();
            }

            $scope.savedTeacherPreference = function () {
                $scope.AllSavedTeaPre = "";
                console.log($scope.sec_code);
                var obj = { opr: 'F', aca_year: $scope.academicYear, bell_code: $scope.bell_code, grade_code: $scope.grade_code, sec_code: $scope.sec_code };
                console.log(obj);
                $http.post(ENV.apiUrl + "api/PreferenceController/fetchTeacherPreference", obj).then(function (res) {
                    $scope.AllSavedTeaPre = res.data;

                    $($scope.AllSavedTeaPre).each(function (key, val) {
                        val.check_id = key;
                    });
                });
            }

            $scope.editPreferenceRow = function (e, str1, str2, str3, str4, str5) {
                var selected_day = str2;
                var selected_slot = str3;
                var selected_teacher = str1;
                var selected_preference = str4;
                
                var obj;

                if (str5 == '0') {
                    obj = e.currentTarget;
                }
                else {
                    obj = str5;
                }


                $(obj).addClass('hide').siblings('.save_row').removeClass('hide');


                var html1 = '<select class="input-sm day_select">';
                html1 += '<option selected disabled value="">Please Select Day</option>';
                html1 += '<option ng-repeat="day in DaySlotCount" ng-selected="{{' + selected_day + ' == day.day_count}}" value="{{day.day_count}}">{{oSlotDayNames[day.day_count]}}</option>';
                html1 += '</select>';

                var html2 = '<select class="input-sm slot_select">';
                html2 += '<option selected disabled value="">Please Select Slot</option>';
                html2 += '<option ng-repeat="x in slot_count" value="{{x}}" ng-selected="{{' + selected_slot + ' == x}}">{{x}}</option>';
                html2 += '</select>';

                var html3 = '<select class="input-sm teacher_select">';
                html3 += '<option selected disabled value="">Please Select Teacher</option>';
                html3 += '<option ng-repeat="teacher in getAllTeacher" value="{{teacher.teacher_code}}" ng-selected="{{\'' + selected_teacher + '\' == teacher.teacher_code}}">{{teacher.teacher_name}}</option>';
                html3 += '</select>';

                var html4 = '<select class="input-sm priority_select">';
                html4 += '<option selected disabled value="">Please Select Subject Preference</option>';
                html4 += '<option ng-repeat="pre in AllPreferences" ng-selected="{{' + selected_preference + ' == pre}}">{{pre}}</option>';
                html4 += '</select>';

                var temp1 = $compile(html1)($scope);
                var temp2 = $compile(html2)($scope);
                var temp3 = $compile(html3)($scope);
                var temp4 = $compile(html4)($scope);

                $(obj).parents('tr').find('.day_td').html(temp1);
                $(obj).parents('tr').find('.slot_td').html(temp2);
                $(obj).parents('tr').find('.sub_td').html(temp3);
                $(obj).parents('tr').find('.pre_td').html(temp4);

            }

            $scope.savePreferenceRow = function (e) {

                var sub_pre = [];

                var old_day_code = $(e.currentTarget).parents('tr').data('day').toString();
                var old_slot_code = $(e.currentTarget).parents('tr').data('slot').toString();
                var old_teacher_code = $(e.currentTarget).parents('tr').data('teachercode').toString();
                var old_preference = $(e.currentTarget).parents('tr').data('preference').toString();


                if (old_day_code != undefined && old_slot_code != undefined && old_teacher_code != undefined && old_preference != undefined) {
                    var obj = { Opr: 'J', aca_year: $scope.academicYear, bell_code: $scope.bell_code, grade_code: $scope.grade_code, sec_code: $scope.sec_code, day_code: old_day_code, slot_code: old_slot_code, teacher_code: old_teacher_code, preference: old_preference };

                    $http.post(ENV.apiUrl + 'api/PreferenceController/RemovePreference', obj).then(function (res) {

                    });
                }

                var day_code = $(e.currentTarget).parents('tr').find('.day_select').val();
                var slot_code = $(e.currentTarget).parents('tr').find('.slot_select').val();
                var teacher_code = $(e.currentTarget).parents('tr').find('.teacher_select').val();
                var teacher_name = $(e.currentTarget).parents('tr').find('.teacher_select option:selected').text();
                var preference = $(e.currentTarget).parents('tr').find('.priority_select').val();

                if (day_code != undefined && slot_code != undefined && teacher_code != undefined && preference != undefined) {
                    sub_pre.push({ aca_year: $scope.academicYear, bell_code: $scope.bell_code, grade_code: $scope.grade_code, sec_code: $scope.sec_code, day_code: day_code, slot_code: slot_code, teacher_code: teacher_code, preference: preference });
                }

                if (sub_pre.length > 0) {
                    $http.post(ENV.apiUrl + "api/PreferenceController/SaveTeacherPreference", sub_pre).then(function (res) {
                        if (res.data == true) {
                            $(e.currentTarget).parents('tr').find('.edit_row').removeClass('hide');
                            $(e.currentTarget).parents('tr').find('.save_row').addClass('hide');
                            $(e.currentTarget).parents('tr').attr('data-day', day_code);
                            $(e.currentTarget).parents('tr').attr('data-slot', slot_code);
                            $(e.currentTarget).parents('tr').attr('data-teachercode', teacher_code);
                            $(e.currentTarget).parents('tr').attr('data-teachername', teacher_name);
                            $(e.currentTarget).parents('tr').attr('data-preference', preference);


                            $(e.currentTarget).parents('tr').find(".day_td").html('<span class="muted ">' + $scope.oSlotDayNames[day_code] + '</span>');
                            $(e.currentTarget).parents('tr').find(".slot_td").html('<span class="muted ">' + slot_code + '</span>');
                            $(e.currentTarget).parents('tr').find(".sub_td").html('<span class="muted ">' + teacher_name + '</span>');
                            $(e.currentTarget).parents('tr').find(".pre_td").html('<span class="muted ">' + preference + '</span>');
                        }
                    });
                }

            }

            $scope.deletePreferenceRow = function (e, type) {
                if (type == 'miltiple'){
                    var old_day_code = $(e).data('day').toString();
                    var old_slot_code = $(e).data('slot').toString();
                    var old_teacher_code = $(e).data('teachercode').toString();
                    var old_preference = $(e).data('preference').toString();
                }
                else {
                    var old_day_code = $(e.currentTarget).parents('tr').data('day').toString();
                    var old_slot_code = $(e.currentTarget).parents('tr').data('slot').toString();
                    var old_teacher_code = $(e.currentTarget).parents('tr').data('teachercode').toString();
                    var old_preference = $(e.currentTarget).parents('tr').data('preference').toString();
                }
                


                if (old_day_code != undefined && old_slot_code != undefined && old_teacher_code != undefined && old_preference != undefined) {
                    var obj = { Opr: 'J', aca_year: $scope.academicYear, bell_code: $scope.bell_code, grade_code: $scope.grade_code, sec_code: $scope.sec_code, day_code: old_day_code, slot_code: old_slot_code, teacher_code: old_teacher_code, preference: old_preference };

                    $http.post(ENV.apiUrl + 'api/PreferenceController/RemovePreference', obj).then(function (res) {
                        if (type == 'miltiple') {
                            $(e).remove();
                        }
                        else {
                            $(e.currentTarget).parents('tr').remove();
                        }
                        
                    });
                }
            }

            $scope.selectAll = function (e) {
                if ($(e.currentTarget).is(':checked')) {
                    $(".row_checkbox").each(function (k, v) {
                        $(v).prop("checked", true);
                    })
                }
                else {
                    $(".row_checkbox").each(function (k, v) {
                        $(v).prop("checked", false);
                    })
                }
            }


            $scope.applyAction = function () {
                if ($("#applyaction").val() == 'edit') {
                    $("#savedSubjectPreference tr").each(function (key, val) {

                        if ($(val).find('.row_checkbox').is(':checked')) {
                            var teacher_code = $(val).data('teachercode');
                            var day_code = $(val).data('day');
                            var slot_code = $(val).data('slot');
                            var preference = $(val).data('preference');
                            var elem = $(val).find('.edit_row');
                            var eventobj = 0;

                            $scope.editPreferenceRow(eventobj, teacher_code, day_code, slot_code, preference, elem);
                        }
                    })
                }
                else if ($("#applyaction").val() == 'delete') {
                    $("#savedSubjectPreference tr").each(function (key, val) {
                        if ($(val).find('.row_checkbox').is(':checked')) {
                            var teacher_code = $(val).data('teachercode');
                            var day_code = $(val).data('day');
                            var slot_code = $(val).data('slot');
                            var preference = $(val).data('preference');
                            $scope.deletePreferenceRow(val, 'multiple');
                        }
                    })
                }
            }

            $(document).ready(function () {
                $scope.getAcademicYear();
                $scope.condenseMenu();
                $scope.oSlotDayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                $scope.AllPreferences = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
            });

        }]);
})();

