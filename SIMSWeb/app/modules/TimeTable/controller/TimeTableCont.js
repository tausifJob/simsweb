﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.TimeTable');
    var iSlotCount;// = 5;
    var iSlotDay;// = 5;
    var oSlotDayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var sHtmlContent = "";
    var fTableWidth = 0;
    var fTableHeight = 0;
    var fSlotWidth = 0;
    var source = "";
    var dest = "";
    var fSlotHeight = 0;
    var iIndex = 0;
    var oDataTiles = [];
    var iSlotCountName = [];
    var oTileObj;
    var oDataTile;
    var iData;
    var a = {},dayStart=0;
    var opr;
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    var sizeSet = function () {
        fTableWidth = $(".slotArea").width();
        fSlotWidth = fTableWidth / (iSlotCount + 1);
        fTableHeight = $(".slotArea").height();
        fSlotHeight = fTableHeight / (iSlotDay + 1);
        $(".slotWidth").css("width", fSlotWidth);
        $(".slotWidth").css("height", fSlotHeight);
        $(".width1").css("width", "" + (fSlotWidth - 10) + "px");
        $(".width2").css("width", "" + ((2 * fSlotWidth) - 10) + "px");
        $(".width3").css("width", "" + ((3 * fSlotWidth) - 10) + "px");
    }

    var RenderTilesnGrid = function ()
    {
        sHtmlContent = "<table> <tr> <th class=\"slotWidth\"> Day/Slot</th>";
        for (var i = 1; i <= iSlotCount; i++) {
            sHtmlContent += "<th class=\"slotWidth\">" + iSlotCountName[i-1].sims_bell_slot_desc + "</th>";
        }
        var t1 = 101;
        sHtmlContent += "</tr>";
        for (var i = 0; i < 7; i++) {
            sHtmlContent += "<tr " + (i<(dayStart-1)||i>(iSlotDay+dayStart-1)?"class=\"ng-hide\"":"") + "> <td class=\"slotWidth\">" + oSlotDayNames[i] + "</td>";
            for (var j = 0; j < iSlotCount; j++) {
                sHtmlContent += "<td id=\"" + t1 + "\" class=\"slotWidth\"> </td>";
                t1++;
            }
            sHtmlContent += "</tr>";
        }
        t1--;
        sHtmlContent += "</table>";
        $(".slotArea").html("");
        $(".slotArea").append(sHtmlContent);
        $(".slotWidth").css("border", "0.5px solid black");
        $(".slotWidth").css("text-align", "center");

        //Tiles Rendering
        var temp = 0;
        var itemp = 0;
        var k = 1;
        sHtmlContent = "";
        $(".tilesArea").html("");
        for (var i = 0; i < oTileObj.length; i++) {
            if (oTileObj[i].lessonType === "1") {
                temp = (opr === 'Y') ? 1 : oTileObj[i].lecturePerWeek;
                for (var j = 1; j <= temp; j++) {
                    oDataTile = {
                        teacher_code: oTileObj[i].teacherCode,
                        grade_code: oTileObj[i].gradeCode,
                        section_code: oTileObj[i].sectionCode,
                        subject_code: oTileObj[i].subjectCode,
                        day_code: oTileObj[i].dayCode == undefined ? "0" : oTileObj[i].dayCode,
                        slot_code: oTileObj[i].slotCode == undefined ? "0" : oTileObj[i].slotCode,
                        subject_name: oTileObj[i].subjectName,
                        aca_year: oTileObj[i].academicYear,
                        sims_bell_sr_no: oTileObj[i].sims_bell_sr_no == undefined ? "0" : oTileObj[i].sims_bell_sr_no
                    }
                    oDataTiles.push(oDataTile);
                    sHtmlContent = "<div id=\"div" + k + "\" class =\"tileT\" >";
                    sHtmlContent += "<div class =\"subjectName width" + oTileObj[i].slotPerLecture + "\" style=\"background-color:" + oTileObj[i].subjectColor + "\" >";
                    sHtmlContent += oTileObj[i].subjectName + "<br>" + oTileObj[i].gradeName + "  " + oTileObj[i].sectionName;
                    sHtmlContent += "</div></div>";
                    if (oDataTile.day_code === "0")
                    {
                        $(".tilesArea").append(sHtmlContent);
                    }
                    else
                    {
                        itemp = 100 + (iSlotCount * (parseInt(oDataTile.day_code, 10) - 1)) + parseInt(oDataTile.slot_code, 10);
                        $("td#" + itemp).html(sHtmlContent);
                    }
                    k++;
                }
            }
            if (oTileObj[i].lessonType === "2") {
                temp = (opr === 'Y') ? 1 : oTileObj[i].lecturePerWeek;
                for (var j = 1; j <= temp; j++) {
                    oDataTile = {
                        teacher_code: oTileObj[i].teacherCode,
                        grade_code: oTileObj[i].gradeCode,
                        section_code: oTileObj[i].sectionCode,
                        subject_code: oTileObj[i].subjectCode,
                        day_code: oTileObj[i].dayCode == undefined ? "0" : oTileObj[i].dayCode,
                        slot_code: oTileObj[i].slotCode == undefined ? "0" : oTileObj[i].slotCode,
                        subject_name: oTileObj[i].subjectName,
                        aca_year: oTileObj[i].academicYear,
                        sims_bell_sr_no: oTileObj[i].sims_bell_sr_no == undefined ? "0" : oTileObj[i].sims_bell_sr_no
                    }
                    oDataTiles.push(oDataTile);
                    sHtmlContent = "<div id=\"div" + k + "\" class =\"tileP\" >";
                    sHtmlContent += "<div class =\"subjectName width" + oTileObj[i].slotPerPractical + "\" style=\"background-color:" + oTileObj[i].subjectColor + "\" >";
                    sHtmlContent += oTileObj[i].subjectName + "<br>" + oTileObj[i].gradeName + "  " + oTileObj[i].sectionName;
                    sHtmlContent += "</div></div>";
                    if (oDataTile.day_code === "0") {
                        $(".tilesArea").append(sHtmlContent);
                    }
                    else {
                        itemp = 100 + (iSlotCount * (parseInt(oDataTile.day_code, 10) - 1)) + parseInt(oDataTile.slot_code, 10);
                        $("td#" + itemp).html(sHtmlContent);
                    }
                    k++;
                }
            }
        }
        k--;
        sizeSet();
        var test = "";
        var testdrop = ""
        if (k > 0) {
            for (var i = 1; i < k; i++) {
                test += "#div" + i + ", ";
            }
            test += "#div" + i;
        }
        //$(".tilesArea").sortable();
        $(test).draggable({
            start: function (event) {
                source = $(event.target).attr("id");
            }
        });
        if (k > 0) {
            for (var i = 101; i < t1; i++) {
                testdrop += "td#" + i + ", ";
            }
            testdrop += "td#" + i;
        }
        $(testdrop).css("padding-left", "3px");
        $(testdrop).droppable({
            activeClass: "active",
            hoverClass: "hover",
            drop: function (event, ui) {
                dest = $(event.target).attr("id");
                console.log("Source is", source);
                console.log("dest is", dest);
                if ($("td#" + dest + " div").hasClass("ui-draggable") === false) {
                    $("td#" + dest).html($("#" + source));
                    iIndex = parseInt(source.substring(3, source.length), 10);
                    iData = parseInt(((dest - 100) / iSlotCount) + 1, 10);
                    oDataTiles[iIndex - 1].day_code = (((dest - 100) % iSlotCount === 0) ? iData - 1 : iData);
                    iData = parseInt((dest - 100) % iSlotCount, 10);
                    oDataTiles[iIndex - 1].slot_code = ((iData === 0) ? iSlotCount : iData);
                    console.log("Day Code is", oDataTiles[iIndex - 1].day_code);
                    console.log("slot code is", oDataTiles[iIndex - 1].slot_code);
                }
            }
        });
    }

    simsController.controller('TimeTableCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$locale', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $locale, $http, ENV) {
            $scope.MyData = function () {
                a = { EmpID: $rootScope.globals.currentUser.username, Opr: 'C', aca_year: $scope.aca_year, bell_code: $scope.bell_code };
                $http.post(ENV.apiUrl + "api/TimeTableNew/TeachersLecture", a).then(function (res) {
                    oTileObj = res.data.table;
                    iSlotCount = res.data.table1[0].column1;
                    iSlotDay = res.data.table2[0].column1;
                    opr = res.data.table3[0].column1;
                    dayStart = parseInt(res.data.table4[0].column1, 10);
                    iSlotCountName = res.data.table5;
                    oDataTiles = [];
                    RenderTilesnGrid();
                });

            };
            $scope.getAcademicYear = function () {
                a = { Opr: 'A' };
                $http.post(ENV.apiUrl + "api/TimeTableNew/TeachersLecture", a).then(function (res) {
                    $scope.AllAcademicYear = res.data.table;
                    $scope.aca_year = $scope.AllAcademicYear[0].sims_academic_year;
                    $scope.getBell();
                });
            }

            $scope.getBell = function () {
                a = { Opr: 'B', EmpID: $rootScope.globals.currentUser.username, aca_year: $scope.aca_year };
                $http.post(ENV.apiUrl + "api/TimeTableNew/TeachersLecture", a).then(function (res) {
                    $scope.AllBellDetails = res.data.table;
                    $scope.bell_code = $scope.AllBellDetails[0].bell_code;
                });
            }

            $scope.saveRecord = function () {
                $('#loader').modal({ backdrop: 'static', keyboard: false });
                var cTemp = 'I';
                if (opr === 'Y')
                {
                    cTemp = 'U';

                }
                $http.post(ENV.apiUrl + "api/TimeTableNew/CUDTeachersLecture?cTemp=" + cTemp, oDataTiles).then(function (res) {
                    //alert('Data Submitted');
                    swal({ title: "Alert", text: 'Time Table slot saved', showCloseButton: true, width: 450, });
                    $('#loader').modal('hide');
                    opr = 'Y';
                });

            };

            $(window).resize(function () {
                sizeSet();
            });

            $(document).ready(function () {
                $scope.getAcademicYear();
                //$scope.MyData();
            });
        }]);

  
})();
