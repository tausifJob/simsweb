﻿var allowDrop = function (ev) {
    $('td.dropabletd').css({ 'background': "transparent" });

    if (dragstartsfrom.tagName == 'TD') {
        if (ev.target.parentNode.id == dragstartsfrom.parentNode.id) {
            $(ev.target).css({ 'background': "#ccc" });
        }
    }
    else {

        var str = dragstartsfrom.id;
        if (ev.target.parentNode.id == pendingtrid) {
            $(ev.target).css({ 'background': "#ccc" });
        }
    }
    ev.preventDefault();
};

var dragstartsfrom;
var pendingtrid;
var teachercode;
var drag = function (ev) {
    teachercode = $(ev.target).children('.hoverdiv').data('teachercode');
    $("#savett").text("Save Changes !").prop('disabled', false);
    if (ev.target.parentNode.id == 'pendingsubid') {
        pendingtrid = $(ev.target).attr('trid');
        $(ev.target).addClass('filled');
        dragstartsfrom = document.getElementById(ev.target.parentNode.id);
        ev.dataTransfer.setData("text", ev.target.id);
    }
    else {
        dragstartsfrom = ev.target.parentNode;
        ev.dataTransfer.setData("text", ev.target.id);
    }
};

var drop = function (ev) {
    var trid = $(ev.target).parents('tr').attr('id');
    var str2;
    if (dragstartsfrom.tagName == 'TD') {
        str2 = dragstartsfrom.parentNode.id;
    }
    else {
        str2 = pendingtrid;
    }
    var data = ev.dataTransfer.getData("text");
    var str4 = data.split("_");
    sub_code = str4['3'];

    if (trid == str2) {
        if ($(ev.target).is('div')) {
            var str3 = $(ev.target).parents('td').attr('id').split("_");
            var subid = 'sub_' + str3[2] + "_" + str3['3'] + "_" + sub_code + '_' + teachercode + "_" + trid;
            $(ev.target).parents('td').append(document.getElementById(data)).addClass('groupClass');
            $(ev.target).attr('id', subid);
        }
        else {
            var str3 = (ev.target.id).split("_");
            var subid = 'sub_' + str3[2] + "_" + str3['3'] + "_" + sub_code + '_' + teachercode + "_" + trid;
            $(ev.target).append(document.getElementById(data));
            $(dragstartsfrom).removeClass('groupClass');
            $(ev.target).children('.filled').attr('id', subid);
        }

        $(dragstartsfrom).removeClass('occupied');
        $(ev.target).addClass('occupied');

        $(ev.target).parents('tr').find('.count_td').html($(ev.target).parents('tr').children('.occupied').length);
    }
    else {
        if ($(ev.target).attr('id') == 'pendingsubid') {
            var trid = $(dragstartsfrom).parents('tr').attr('id');
            $("#" + data).attr('trid', trid);
            $(dragstartsfrom).removeClass('groupClass');
            $(ev.target).append(document.getElementById(data));
            $(ev.target).children('.dragablesubjects').removeClass('filled');

        }

        $(dragstartsfrom).removeClass('occupied');

        $(dragstartsfrom).parents('tr').find('.count_td').html($(dragstartsfrom).parents('tr').children('.occupied').length);
        $("#savett").text("Save Changes!").prop('disabled', false);
    }
};

(function () {
    'use strict';
    var simsController = angular.module('sims.module.TimeTable');
    var iSlotCount; //= [7,7,7,7,5];
    var iSlotDay; //= 5;
    var oSlotDayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var sHtmlContent = "";
    var a = {};
    var AllAcademicYear;
    var getBellData;
    var AllBellData;
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TimeTableTeachersCont',
        ['$scope', '$compile', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$locale', '$http', 'ENV', function ($scope, $compile, $state, $rootScope, $timeout, gettextCatalog, $locale, $http, ENV) {

            $scope.getGradeSection = function (aca_year, bellCode) {
                a = { aca_year: aca_year, 'bell_code': bellCode };

                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableInitial", a).then(function (res) {
                    $scope.iSlotCount = [];
                    $(".timetablecontainer").removeClass('hide');
                    for (var i = 0; i < res.data.table.length; i++) {
                        $scope.iSlotCount.push(res.data.table[i].column1);
                    }

                    $scope.iSlotDay = res.data.table1[0].column1;

                    $scope.AllGradeSec = res.data.table2;
                    $scope.RenderTilesnGridClassView();
                    $(".saved_tt").removeClass('hide');
                    $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableHistoryVersionData?aca_year=" + aca_year + "&bell_code=" + bellCode).then(function (res) {
                        $scope.AllSavedVersions = res.data;
                    });
                });
            };

            $scope.RenderTilesnGridClassView = function () {

                var l;

                var html = '<tr>';
                html += '<th class="cellWidth nobg" title="Grade/Days"> G/D </th>';
                for (var i = 0; i < $scope.iSlotDay; i++) {
                    html += '<th id="day_' + i + '" colspan="' + $scope.iSlotCount[i] + '" class="nobg">' + oSlotDayNames[i] + '</th>';
                }
                html += '<th class="nobg">Co</th>';
                html += '</tr>';
                html += '<tr>';
                html += '<td class="cellWidth text-center nobg" title="Grade/Slot">G/S</td>';
                for (var j = 0; j < $scope.iSlotDay; j++) {
                    for (var k = 0; k < $scope.iSlotCount[j]; k++) {
                        html += '<td id="period_' + i + '" class="cellWidth text-center nobg">' + (k + 1) + '</td>';
                    }
                }

                html += '<td class="nobg"></td></tr>';

                $(".slotArea .headerpart").html("");
                $(".slotArea .headerpart").append(html);

                var html1;
                $($scope.AllGradeSec).each(function (key, val) {
                    var str = val.sims_grade_name_en;
                    var gradeName = str.replace("Class", "");
                    html1 += "<tr class=\"trrow\" id=" + val.sims_bell_grade_code + "_" + val.sims_bell_section_code + " data-grade=\"" + val.sims_grade_name_en + "\" data-secname=\"" + val.sims_section_name_en + "\"> <td class=\"cellWidth gradetd ellipsis\" title=\"" + gradeName + " " + val.sims_section_name_en + "\" > " + gradeName + " " + val.sims_section_name_en + "</td>";
                    for (var j = 0; j < $scope.iSlotDay; j++) {
                        for (var i = 0; i < $scope.iSlotCount[j]; i++) {
                            html1 += "<td id=\"" + val.sims_bell_grade_code + "_" + val.sims_bell_section_code + "_" + (j + 1) + "_" + (i + 1) + "\" class=\"cellWidth dropabletd\" ondrop=\"drop(event)\" ondragover=\"allowDrop(event)\"></td>";
                        }
                    }
                    html1 += '<td class="count_td"></td>';
                    html1 += "</tr>";
                });
                $(".slotArea .contentpart").html("");
                $(".slotArea .contentpart").append(html1);
                $("#fixTable").tableHeadFixer();
            }

            $scope.getAcademicYear = function () {
                AllAcademicYear = [];
                $http.post(ENV.apiUrl + "api/SubjectTeacher/ImportAcademicYear").then(function (getAcaYear) {
                    $scope.AllAcademicYear = getAcaYear.data;
                });
            };

            $scope.setAcaYear = function (str) {
                $scope.academicYear = str;
                $scope.getBellData($scope.academicYear);
            }

            $scope.getBellData = function (aca_year) {
                AllBellData = [];
                a = { Opr: 'B', AcaYear: aca_year };
                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableImport", a).then(function (getBellData) {
                    $scope.AllBellData = getBellData.data.table;
                });
            }


            $scope.saveTimeTable = function () {
                $scope.loaderShow = true;
                $("#savett").html('<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>');
                var finalData = [];
                var timeTableData = [];
                var pendingsub = [];
                var pendingArr = [];
                var bell_code = $("#bell_select").val();
                $(".trrow").each(function (key, val) {
                    if ($(val).find('.filled').length != 0) {
                        var str = $(val).attr('id');
                        var str1 = str.split("_");
                        var grade_code = str1[0];
                        var grade_name = $(val).data('grade');
                        var sec_code = str1[1];
                        var sec_name = $(val).data('secname');
                        var slot_group = $(val).find('.filled').data('slotgroup');

                        $(val).find(".filled").each(function (k, v) {
                            var str = $(v).parents('td').attr('id');
                            var str1 = str.split("_");
                            var day_code = str1[2];
                            var slot_code = str1[3];
                            var sub_color = $(v).data('color');
                            var sub_name = $(v).find('.hoverdiv').data('subname');
                            var subject_code = $(v).data('subcode');
                            var teacher_code = $(v).find('.hoverdiv').data('teachercode');
                            var teacher_name = $(v).find('.hoverdiv').data('teacher').trim();
                            finalData.push({ 'aca_year': $scope.academicYear, 'bell_code': bell_code, 'grade_code': grade_code, 'grade_name': grade_name, 'sec_code': sec_code, 'sec_name': sec_name, 'slot_group': slot_group, 'day_code': day_code, 'slot_code': slot_code, 'sub_color': sub_color, 'sub_name': sub_name, 'subject_code': subject_code, 'teacher_code': teacher_code, 'teacher_name': teacher_name });
                        });
                    }
                });

                $("#pendingsubid .dragablesubjects").each(function (k, v) {

                    var str1 = $(v).attr('trid').split("_");
                    var grade_code = str1[0];
                    var grade_name = $(v).find('.hoverdiv').data('grade');
                    var sec_code = str1[1];
                    var sec_name = $(v).find('.hoverdiv').data('secname');
                    var str2 = $(v).attr('id').split("_");
                    var slot_group = $(v).data('slotgroup');
                    var day_code = 0;
                    var slot_code = 0;
                    var sub_color = $(v).data('color');
                    var sub_name = $(v).find('.hoverdiv').data('subname');
                    var subject_code = $(v).data('subcode');
                    var teacher_code = $(v).find('.hoverdiv').data('teachercode');
                    var teacher_name = $(v).find('.hoverdiv').data('teacher').trim();
                    finalData.push({ 'aca_year': $scope.academicYear, 'bell_code': bell_code, 'grade_code': grade_code, 'grade_name': grade_name, 'sec_code': sec_code, 'sec_name': sec_name, 'slot_group': slot_group, 'day_code': day_code, 'slot_code': slot_code, 'sub_color': sub_color, 'sub_name': sub_name, 'subject_code': subject_code, 'teacher_code': teacher_code, 'teacher_name': teacher_name });
                });
                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableDatasave", finalData).then(function (res) {
                    if (res.data == true) {
                        $("#savett").text("Data saved successfully!").prop('disabled', true);
                        $scope.loaderShow = false;
                    }
                });
            };

            $scope.viewTimeTable = function () {
                $(".pending_hide").addClass('hide');

                var bell_code = $("#bell_select").val();
                a = { 'opr': 'R', 'aca_year': $scope.academicYear, 'bell_code': bell_code };
                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableViewData", a).then(function (res) {
                    $scope.AllGradeSectionView = res.data;

                    $($scope.AllGradeSectionView).each(function (k, v) {
                        if (v.day_code == '0') {
                            var html = '<div ng-mouseover="showDetail($event)" ng-mouseleave="hideDetail($event)" id="sub_' + v.day_code + '_' + v.slot_code + '_' + v.subject_code + '_' + v.teacher_code + '_' + v.grade_code + '_' + v.section_code + '" trid="' + v.grade_code + '_' + v.section_code + '" class="dragablesubjects filled" data-slotgroup="' + v.slot_group + '" data-subcode="' + v.subject_code + '" data-color="' + v.subject_color + '" draggable="true" ondragstart="drag(event)" style="background-color: ' + v.subject_color + '">';
                            html += '<div class="hoverdiv ellipsis" data-teachercode="' + v.teacher_code + '" data-teacher="' + v.teacher_name + '" data-subname="' + v.subject_name + '" data-secname="' + v.section_name + '" data-grade="' + v.grade_code + '" ng-mouseover="showdetail($event)" ng-mouseleave="hidedetail($event)">' + v.subject_name + '</div>';
                            html += '</div>';

                            var temp = $compile(html)($scope);
                            $(".pendingsub").append(temp);
                        }
                        else {

                            var html = '<div ng-mouseover="showDetail($event)" ng-mouseleave="hideDetail($event)" id="sub_' + v.day_code + '_' + v.slot_code + '_' + v.subject_code + '_' + v.teacher_code + '_' + v.grade_code + '_' + v.section_code + '" class="dragablesubjects filled" data-slotgroup="' + v.slot_group + '" data-subcode="' + v.subject_code + '" data-color="' + v.subject_color + '" draggable="true" ondragstart="drag(event)" style="background-color: ' + v.subject_color + '">';
                            html += '<div class="hoverdiv ellipsis" data-teachercode="' + v.teacher_code + '" data-teacher="' + v.teacher_name + '" data-subname="' + v.subject_name + '" data-secname="' + v.section_name + '" data-grade="' + v.grade_code + '" ng-mouseover="showdetail($event)" ng-mouseleave="hidedetail($event)">' + v.subject_name + '</div>';
                            html += '</div>';
                            $("#" + v.grade_code + "_" + v.section_code + "_" + v.day_code + "_" + v.slot_code).addClass('occupied');
                            var temp = $compile(html)($scope);
                            if (v.slot_group == "1") {
                                $("#" + v.grade_code + "_" + v.section_code + "_" + v.day_code + "_" + v.slot_code).html(temp);
                            }
                            else {
                                $("#" + v.grade_code + "_" + v.section_code + "_" + v.day_code + "_" + v.slot_code).addClass('groupClass').removeClass('cellwidth');
                                $("#" + v.grade_code + "_" + v.section_code + "_" + v.day_code + "_" + v.slot_code).append(temp);
                            }
                        }
                    })
                    
                })
            }

            $scope.genTimeTable = function () {
                $(".pending_hide").removeClass('hide');
                var bell_code = $("#bell_select").val();
                $(".pendingsub").html();
                a = { 'aca_year': $scope.academicYear, 'bell_code': bell_code };

                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableData", a).then(function (res) {
                    $scope.AllGradeSection = res.data;
                    $($scope.AllGradeSection).each(function (key, val) {
                        $(val.gradeSec).each(function (k, v) {
                            var sloat = v.slot_code;
                            var day = v.day_code;
                            var html = '<div id="sub_' + day + '_' + sloat + '_' + v.subject_code + '_' + v.teacher_code + '_' + val.grade_code + '_' + val.sec_code + '" class="dragablesubjects filled" data-subcode = "' + v.subject_code + '" data-color="' + v.sub_color + '" draggable="true"  ondragstart="drag(event)" style="background-color: ' + v.sub_color + '"><div class="hoverdiv ellipsis" data-teachercode="' + v.teacher_code + '" data-teacher="' + v.teacher_name + '" data-subname="' + v.sub_name + '" data-secname ="' + val.sec_name + '" data-grade="' + val.grade_name + '" ng-mouseover="showDetail($event)" ng-mouseleave="hideDetail($event)">' + v.sub_name + '</div></div>';
                            var temp = $compile(html)($scope);

                            if ($("#" + val.grade_code + "_" + val.sec_code + "_" + day + "_" + sloat).children('.dragablesubjects').length > 0) {
                                $("#" + val.grade_code + "_" + val.sec_code + "_" + day + "_" + sloat).addClass('groupClass');
                                $("#" + val.grade_code + "_" + val.sec_code + "_" + day + "_" + sloat).append(temp);
                            }
                            else {
                                $("#" + val.grade_code + "_" + val.sec_code + "_" + day + "_" + sloat).html(temp);
                            }
                        })
                    })
                });

                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableConflictData", a).then(function (res) {
                    $(".pendingsub").html('');
                    $(res.data).each(function (key, val) {
                        $(val.gradeSec).each(function (k, v) {
                            var html = '<div id="sub_' + v.day_code + '_' + v.slot_code + '_' + v.subject_code + '_' + v.teacher_code + '_' + val.grade_code + '_' + val.sec_code + '" data-color = "' + v.sub_color + '" data-subcode = "' + v.subject_code + '" trid = "' + val.grade_code + '_' + val.sec_code + '" class="dragablesubjects " draggable="true"  ondragstart="drag(event)" style="background-color: ' + v.sub_color + '"><div class="hoverdiv ellipsis" data-teacher="' + v.teacher_name + '" data-teachercode = "' + v.teacher_code + '" data-subname="' + v.sub_name + '" data-secname ="' + val.sec_name + '" data-grade="' + val.grade_name + '" ng-mouseover="showDetail($event)" ng-mouseleave="hideDetail($event)">' + v.sub_name + '</div></div>';
                            var temp = $compile(html)($scope);
                            $(".pendingsub").append(temp);
                        })
                    })
                })
            };

            $scope.showDetail = function (e) {
                var teachername = $(e.target).data('teacher');
                var sectionname = $(e.target).data('secname');
                var gradename = $(e.target).data('grade');
                var subname = $(e.target).data('subname');

                var html = '<li class="col-lg-6 col-md-6 col-sm-12"><span class="glyphicon glyphicon-book"></span> Subject : ' + subname + '</li>';
                html += '<li class="col-lg-6 col-md-6 col-sm-12"><span class="fa fa-users" aria-hidden="true"></span> Grade : ' + gradename + ' - ' + sectionname + '</li>';
                html += '<li class="col-lg-12 col-md-12 col-sm-12"><span class="glyphicon glyphicon-user"></span> Teacher : ' + teachername + '</li>';
                $(".subjectInfocontainer").html(html);
            }

            $scope.hideDetail = function (e) {
                $(".subjectInfocontainer").html('');
            }

            $scope.genTimeTableNew = function (e) {
                $(".pending_hide").removeClass('hide');

                $scope.loaderShow = true;
                $(".contentpart td.dropabletd").html('').removeClass('occupied');

                $("#pendingsubid").html('');
                $(".slotArea").removeClass("editmode").attr('version', '');
                var bell_code = $("#bell_select").val();
                a = { Opr: 'I', AcaYear: $scope.academicYear, bellCode: bell_code };
                var class_teacher_first = true;

                $(".trrow").each(function (k, v) {

                    var trid = $(v).attr('id');
                    var arr = trid.split("_");
                    var grade_code = arr[0];
                    var sec_code = arr[1];
                    var lastfilled;
                    var daypointer = 0;
                    var slotpointer = 0;

                    a = { Opr: 'I', AcaYear: $scope.academicYear, bellCode: bell_code, grade_code: grade_code, sec_code: sec_code };

                    $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableDataNew", a).then(function (res) {
                        $scope.genTimeTableNewData = res.data.table;
                        $scope.subjectPreference = res.data.table1;
                        $scope.teacherPreference = res.data.table2;
                        $scope.classTeacherPreference = res.data.table3;
                        $scope.schoolPreference = res.data.table4;
                        if (class_teacher_first == true) {
                            if ($scope.classTeacherPreference.length > 0) {
                                var cteacherData = $scope.findClassTeacherIndexData(grade_code, sec_code, $scope.classTeacherPreference[0].teacher_code);

                                if (cteacherData != undefined) {
                                    var ct = cteacherData['data'];
                                    lastfilled = $scope.fillIntoGrid(cteacherData['data'], daypointer, slotpointer);
                                    $scope.genTimeTableNewData.splice(cteacherData['tindex'], 1);
                                }
                            }
                        }

                        if ($scope.subjectPreference.length > 0 || $scope.teacherPreference.length > 0) {
                            if ($scope.subjectPreference.length > 0 && $scope.teacherPreference.length > 0) {
                                $scope.removeSubTeachCommon();
                            }
                            $scope.updateSubjectTeacherPreferenceArr();
                            if ($scope.updatedSubjectPre.length >= 0) {
                                $($scope.updatedSubjectPre).each(function (k, v) {
                                    $scope.fillIntoGrid(v, v.day_code, v.slot_code, 'preference');
                                });
                            }
                            if ($scope.updatedTeacherPre.length >= 0) {

                                $($scope.updatedTeacherPre).each(function (k, v) {
                                    $scope.fillIntoGrid(v, v.day_code, v.slot_code, 'preference');
                                });
                            }
                        }
                        if ($scope.genTimeTableNewData.length > 0) {
                            if (lastfilled == undefined) {
                                $scope.normalGen($scope.genTimeTableNewData, daypointer, slotpointer);
                            }
                            else {
                                $scope.normalGen($scope.genTimeTableNewData, lastfilled['next_day'], lastfilled['next_slot']);
                            }
                        }
                    }).then(function () {
                        $(v).find('.count_td').html($(v).children('.occupied').length);
                        $scope.loaderShow = false;
                    });
                });
            }

        

            $(document).ready(function () {
                $scope.getAcademicYear();
                $scope.condenseMenu();
                $scope.loaderShow = false;
            });

        }]);
})();

