﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.TimeTable');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('SubstitueLectureDetailsReportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            
            $scope.temp = {};
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.temp.report_status = 'D';

            $(function () {
                $('#bell_box').multipleSelect({ width: '100%' });
                $('#teacher_box').multipleSelect({ width: '100%', filter: true });

            });
            
            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_start_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });


            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_end_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });



            $http.get(ENV.apiUrl + "api/lecturedetails/getCuriculum").then(function (res1) {
                $scope.sims_cur = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_cur_code'] = res1.data[0].sims_cur_code;

                    $scope.getAccYear(res1.data[0].sims_cur_code);
                }
            });

            $scope.getAccYear = function (cur_code) {
                debugger
                $http.get(ENV.apiUrl + "api/lecturedetails/getAcademicYear?curCode=" + cur_code).then(function (res1) {
                    $scope.academic_year = res1.data;
                    if (res1.data.length > 0) {
                        for (var i = 0; i < $scope.academic_year.length; i++) {
                            if ($scope.academic_year[i].sims_academic_year_status == 'C') {
                                $scope.temp['sims_academic_year'] = $scope.academic_year[i].sims_academic_year;
                                $scope.getBellData($scope.academic_year[i].sims_academic_year)
                                $scope.getAllteachercode($scope.academic_year[i].sims_academic_year)

                            }
                        }

                       
                       
                    }
                });

            }

            $scope.getBellData = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/lecturedetails/getbell_code?academic_year=" + $scope.temp.sims_academic_year).then(function (res1) {
                    $scope.bell_data = res1.data;

                    setTimeout(function () {
                        $('#bell_box').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }

            $scope.getAllteachercode = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/lecturedetails/getAllteacher?academic_year=" + $scope.temp.sims_academic_year + "&bell_code=" + $scope.temp.sims_bell_code).then(function (res1) {
                    $scope.teacher_data = res1.data;
                    setTimeout(function () {
                        $('#teacher_box').change(function () {

                        }).multipleSelect({
                            width: '100%',
                            filter: true
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }


            $scope.getsubdetail = function () {
                debugger;
                $scope.colsvis = false;
                $http.get(ENV.apiUrl + "api/lecturedetails/getSubstitueLectureDetailsReport?mom_start_date=" + $scope.mom_start_date + "&mom_end_date=" + $scope.mom_end_date + "&bell_code=" + $scope.temp.sims_bell_code + "&teacher_code=" + $scope.temp.sims_teacher_code + "&report_status=" + $scope.temp.report_status).then(function (res1) {
                    if (res1.data.length > 0) {
                        $scope.report_data_new = res1.data;
                        //console.log($scope.repreport_data_new);
                    }
                    else {
                        swal({ title: "Alert", text: " Sorry !!!!Data is not Available", showCloseButton: true, width: 300, height: 200 });
                        $scope.report_data_new = [];
                    }

                    debugger;
                    if ($scope.temp.report_status == 'S') {
                        $scope.showsumaryField = true;
                        $scope.detailsField = false;
                    }
                    else {
                        $scope.showsumaryField = false;
                        $scope.detailsField = true;
                    }

                });
            }
            

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.colsvis = false;


            $scope.exportData = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {


                        if ($scope.temp.report_status == 'S') {
                            var blob = new Blob([document.getElementById('rpt_data').innerHTML],
                             {
                                 type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                             });
                            $scope.detailsField = false;
                            
                        }
                        else {
                            
                            var blob = new Blob([document.getElementById('rpt_data1').innerHTML],
                            {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            $scope.showsumaryField = false;
                            
                        }
                        saveAs(blob, "SubstituteLectureDetailsReport.xls");
                        $scope.colsvis = false;
                        $scope.getsubdetail();

                    }
                    
                });
                $scope.colsvis = true;
                
            };


            $scope.print = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {


                        if ($scope.temp.report_status == 'S') {
                            var docHead = document.head.outerHTML;
                            var printContents = document.getElementById('rpt_data').outerHTML;
                            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                            var newWin = window.open("", "_blank", winAttr);
                            var writeDoc = newWin.document;
                            writeDoc.open();
                            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                            writeDoc.close();
                            newWin.focus();
                            $scope.detailsField = false;
                        }
                        else {
                            var docHead = document.head.outerHTML;
                            var printContents = document.getElementById('rpt_data1').outerHTML;
                            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                            var newWin = window.open("", "_blank", winAttr);
                            var writeDoc = newWin.document;
                            writeDoc.open();
                            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                            writeDoc.close();
                            newWin.focus();
                            
                        }
                        $scope.colsvis = false;
                        $scope.getsubdetail();

                    }
                    
                });
                $scope.colsvis = true;
               
            };

            $scope.reset_data = function () {
                debugger;
                // $scope.temp.sims_grade_code = '';


                try {
                    $('#bell_box').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#teacher_box').multipleSelect('uncheckAll');
                } catch (e) {

                     }
                $scope.temp.sims_bell_code = '';
               $scope.report_data_new  = [];


            }          

            $scope.viewsubtitution = function (obj) {
                debugger;
               
                $('#MyModal6').modal('show');
                $http.get(ENV.apiUrl + "api/lecturedetails/getSubstitutionfromDetails?bell_code=" + obj.sims_bell_code + "&cur_teacher=" + obj.current_teacher_code + "&sub_teacher=" + obj.sub_teacher_code + "&date=" + obj.sims_bell_substitution_date).then(function (res1) {
                    $scope.subformdetails = res1.data;
                    //$scope.class1 = obj.class1;
                    //$scope.current_emp_code = $scope.subformdetails[0].current_emp_code;
                    //$scope.current_subject_name = $scope.subformdetails[0].current_subject_name;
                    $scope.current_teacher_name = $scope.subformdetails[0].current_teacher_name;
                    $scope.day_date = $scope.subformdetails[0].day_date;
                    //$scope.sims_bell_slot_desc = $scope.subformdetails[0].sims_bell_slot_desc;
                    //$scope.sub_emp_code = $scope.subformdetails[0].sub_emp_code;
                    //$scope.sub_subject_name = $scope.subformdetails[0].sub_subject_name;
                    $scope.sub_teacher_name = $scope.subformdetails[0].sub_teacher_name;
                    $scope.approved_by = $scope.subformdetails[0].approved_by;
                    $scope.remark = $scope.subformdetails[0].remark;
                    $scope.time = obj.time;

                });
            }

            $scope.send_email = function (str) {
                debugger
                $scope.savedata = [];
                var datasend = {
                    'mom_start_date': $scope.mom_start_date,
                    'mom_end_date': $scope.mom_end_date,
                    'sims_bell_code':str.sims_bell_code,
                    'sims_teacher_code': str.sub_teacher_code,
                    'report_status':'',
                }

                $scope.savedata.push(datasend);

                $http.post(ENV.apiUrl + "api/lecturedetails/Insert_email", $scope.savedata).then(function (msg) {
                    $scope.msg1 = msg.data;
                    console.log("msg", msg);
                    
                    if ($scope.msg1 == true) {
                        $(document).ready(function () {
                            $scope.datasend = [];
                            $scope.savedata = [];
                        });
                        swal({ title: "Alert", text: "Email Sent successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        $scope.busy = false;
                        $scope.show_fail_students($scope.fcommn.cur_code, $scope.fcommn.academic_year, $scope.fcommn.sims_grade_code, $scope.fcommn.sims_section_code, $scope.fcommn.sims_term_code);
                    }
                    else {
                        swal({ title: "Alert", text: "Unable To Send Email. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        $scope.busy = false;
                    }
                });
            }

            $scope.printsubtitution = function (div) {

                //$scope.colsvis = false;

                var docHead = document.head.outerHTML;
                var printContents = document.getElementById('subform').outerHTML;
                //$scope.colsvis = false;
                var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";


                var newWin = window.open("", "_blank", winAttr);
                var writeDoc = newWin.document;
                writeDoc.open();
                writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                writeDoc.close();
                newWin.focus();

            };
                           

        }])

})();

