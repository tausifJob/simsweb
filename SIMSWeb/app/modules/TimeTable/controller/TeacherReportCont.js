﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.TimeTable');

    simsController.controller('TeacherReportCont',
        ['$scope', '$compile', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$locale', '$http', 'ENV', function ($scope, $compile, $state, $rootScope, $timeout, gettextCatalog, $locale, $http, ENV) {

            $scope.getAcademicYear = function () {
                $http.post(ENV.apiUrl + "api/SubjectTeacher/ImportAcademicYear").then(function (res) {
                    $scope.AllAcademicYear = res.data;
                    $scope.setAcaYear($scope.AllAcademicYear[0].aca_year);
                });
            };

            $scope.setAcaYear = function (str) {
                $scope.aca_year = str;
                $scope.getBellData($scope.aca_year);
            }

            $scope.getBellData = function (aca_year) {
                var a = { Opr: 'B', AcaYear: aca_year };
                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableImport", a).then(function (getBellData) {
                    $scope.AllBellData = getBellData.data.table;
                });
            }

            $scope.getTeachers = function (str) {
                $scope.bell_code = str;
                var a = { opr: 'A', aca_year: $scope.aca_year, bell_code: $scope.bell_code };
                $http.post(ENV.apiUrl + "api/LectureSubstitution/fetchAllTeacher", a).then(function (res) {
                    $scope.AllTeachers = res.data;
                    console.log("All teachers", $scope.AllTeachers);
                });
            }

            $scope.genTeachersView = function (str) {
                $(".table .contenttd").html('');
                $scope.teacher_code = str;
                var a = { opr: 'T', aca_year: $scope.aca_year, bell_code: $scope.bell_code, teacher_code: $scope.teacher_code };
                var aca_name = $("#aca_select option:selected").text();
                var bell_name = $("#bell_select option:selected").text();
                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableView", a).then(function (res) {
                    console.log("This is new one = ", res.data);
                    var tname = $("#teacher_select option:selected").text();
                    $('.tt_title').html('<div class="inline-block"><span class="semi-bold p-r-5"> Acadenic Year: ' + aca_name + '</span> </div> | <div class="inline-block"><span class="semi-bold p-r-5"> Teacher: ' + tname + '</span> | <span class="semi-bold p-r-5"> Teacher Code:  ' + $scope.teacher_code + '</span></div>');
                    $(".grid-body").removeClass('hide');
                    $scope.gridData = res.data.table2;
                    $scope.slot_count = res.data.table[0].column1;
                    $scope.day_count = res.data.table1[0].column1;
                    $scope.time_break_detail = res.data.table3;
                    $scope.genGrid();

                }).then(function () {
                    
                    $($scope.gridData).each(function (k, v) {
                        var html = '<div class="sub_tec_cont" ng-mouseenter = "assignToOtherShow($event, \'div\')"><div class="cvsubname ellipsis" title="' + v.sims_subject_name_en + '" ng-mouseenter = "assignToOtherShow($event, \'div\')">' + v.sims_subject_name_en + '</div><div class="viewsep" ></div><div ng-mouseenter = "assignToOtherShow($event, \'div\')" class="cvtrname ellipsis" title="' + v.sims_teacher_name + '">[' + v.sims_grade_name_en + ' ' + v.sims_section_name_en + ']</div></div>';
                        var temp = $compile(html)($scope);
                        if ($("#" + v.sims_bell_day_code + '_' + v.sims_bell_slot_code).children('.sub_tec_cont').length > 0) {
                            $("#" + v.sims_bell_day_code + '_' + v.sims_bell_slot_code).addClass('group');
                        }
                        $("#" + v.sims_bell_day_code + '_' + v.sims_bell_slot_code).addClass('occupied').append(temp);

                        if ($("#" + v.sims_bell_day_code + '_' + v.sims_bell_slot_code).children('.lec_sub').length > 0) {
                        }
                        else {
                            $("#" + v.sims_bell_day_code + '_' + v.sims_bell_slot_code).append($compile('<div class="lec_sub text-white cursor-pointer" ng-click="allocatetoOther($event)" style="display:none">Assign to Other Teacher</div>')($scope));
                       
                        }
                    });
                });
            }

            $scope.allocatetoOther = function (e) {
                var tdid = $(e.target).parents('td').attr('id');
                var html = '<select class="input-small" ng-model="assignedTeacher" ng-change="assignSubstitution(assignedTeacher, '+tdid+')">';
                html += '<option selected disabled value="">Select Teacher</option>';
                html += '<option ng-repeat="teacher in AllTeachers" value="{{teacher.teacher_code}}">{{teacher.teacher_name}}</option>';
                html += '</select>';
                var temp = $compile(html)($scope);
                $("#" + tdid).html(temp);
            }

            $scope.assignSubstitution = function (str , tdid) {
                //$("#" + tdid).html();
            }


            $scope.assignToOtherShow = function (e, tag) {
                if (tag == 'td') {
                    $(e.target).parent().siblings().find('.lec_sub').hide();
                    $(e.target).find('.lec_sub').show();                    
                }
                else {
                    $(e.target).parents('td').siblings().find('.lec_sub').hide();
                    $(e.target).parents('td').find('.lec_sub').show();                    
                }
            }

            $scope.genGrid = function () {
                $scope.clickAttr = "";
                var html = '<table id="classViewTable" class="table table-hover table-bordered">';
                html += '<thead>';
                html += '<tr>';
                html += '<th class="nobg">D/Lec</th>';
                for (var i = 0; i < $scope.slot_count; i++) {
                    var lec_c;
                    if ($scope.time_break_detail[i]['sims_bell_break'] == 'Y') {
                        lec_c = $scope.time_break_detail[i]['sims_bell_slot_desc'] + ' [Break]';
                    }
                    else {
                        lec_c = $scope.time_break_detail[i]['sims_bell_slot_desc'] + (i + 1);
                    }

                    html += '<th class="nobg slotWidth"> ' + lec_c + '<span class="display_block font-sm">[' + $scope.time_break_detail[i]['sims_bell_start_time'] + ' to ' + $scope.time_break_detail[i]['sims_bell_end_time'] + ']</span></th>';

                }
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';
                for (var i = 0; i < $scope.day_count; i++) {

                    html += '<tr class=""> <td class="slotWidth daytd">' + $scope.oSlotDayNames[i] + '</td>';
                    var break_count = 0;
                    for (var j = 0; j < $scope.slot_count; j++) {
                        var tdbg;
                        var tdid;

                        if ($scope.time_break_detail[j]['sims_bell_break'] == 'Y') {
                            tdbg = "url('assets/img/stbg.png')";
                            tdid = "";
                            break_count = break_count + 1;
                        }
                        else {
                            tdbg = '#fff';
                            tdid = (i + 1) + '_' + ((j + 1) - break_count);
                        }

                        html += '<td id="' + tdid + '" ng-mouseenter = "assignToOtherShow($event, \'td\')"  style="background: ' + tdbg + '" class="slotWidth contenttd" ></td>';

                    }
                    html += '</tr>';
                }
                html += '</tbody>';
                html += '</table>';

                var temp = $compile(html)($scope);
                $("#TVgrid").html(temp);
            }

            $(document).ready(function () {
                $scope.getAcademicYear();
                $scope.condenseMenu();
                $scope.oSlotDayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                $scope.currentYear = '2018';

                $scope.setAcaYear($scope.currentYear);
            });




        }]);
})();

