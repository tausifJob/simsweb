﻿(function () {
    'use strict';
    //var pdfDelegate;
    var simsController = angular.module('sims.module.TimeTable');
    simsController.controller('StaffTimeTableCont',

            ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

                $scope.getcur = function () {
                    $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                        $scope.curriculum = AllCurr.data;
                        $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                        $scope.getAccYear($scope.edt.sims_cur_code);
                    });
                }

                $scope.getcur();

                $scope.getAccYear = function (curCode) {
                    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                        $scope.Acc_year = Acyear.data;
                        $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                        $scope.getGrade();
                    });
                }

                $scope.getGrade = function () {
                    $http.get(ENV.apiUrl + "api/VacancyMst/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.temp.sims_academic_year + "&log_code=" + $rootScope.globals.currentUser.username).then(function (Gradecode) {
                        $scope.Grade_code = Gradecode.data;
                        //setTimeout(function () {
                        //    $('#cmb_grade_code').change(function () {
                        //        console.log($(this).val());
                        //    }).multipleSelect({
                        //        width: '100%'
                        //    });
                        //}, 1000);
                    });
                }

                $scope.getSection = function () {
                    $http.get(ENV.apiUrl + "api/VacancyMst/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.edt.sims_grade_code + "&log_code=" + $rootScope.globals.currentUser.username).then(function (Sectioncode) {
                        $scope.Section_code = Sectioncode.data;
                        //setTimeout(function () {
                        //    $('#cmb_section_code').change(function () {
                        //        console.log($(this).val());
                        //    }).multipleSelect({
                        //        width: '100%'
                        //    });
                        //}, 1000);
                    });
                }

                //$(function () {
                //    $('#cmb_section_code').multipleSelect({
                //        width: '100%'
                //    });
                //});

                $scope.Search = function () {
                    if ($scope.edt.sims_grade_code != undefined) {
                        if ($scope.edt.sims_section_code != undefined) {
                            $http.get(ENV.apiUrl + "api/VacancyMst/gettimetable?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.edt.sims_grade_code + "&section_code=" + $scope.edt.sims_section_code + "&log_code=" + $rootScope.globals.currentUser.username).then(function (res) {

                                if (res.data.length > 0) {
                                    $scope.record = res.data;
                                    setTimeout(function () {
                                        for (var i = 0; i < res.data.length; i++) {
                                            // $scope.url_pdf = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/Docs/timetable/'
                                            $('#timetableshow' + i).attr('src', $scope.url_pdf = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/Docs/timetable/' + res.data[i].sims_timetable_filename);
                                            //$('#timetableshow' + i).attr('src', 'https://api.mograsys.com/kindoapi/Content/siserp/Images/Docs/timetable/' + res.data[i].sims_timetable_filename);
                                            //$("#p" + i).attr('src', ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/Docs/timetable/' + res.data[i].sims_timetable_filename);
                                            $scope.hide = true;
                                        }
                                    }, 0)

                                } else {
                                    $scope.NR = true;
                                    $scope.hide = false;
                                    swal({ title: "Alert", text: "Record Not Found." });
                                }
                            });
                        }
                        else {
                            swal({ title: "Alert", text: "Please Select Section." });
                        }
                    }
                    else {
                        swal({ title: "Alert", text: "Please Select Gread." });
                    }
                }

                $scope.Reset = function () {
                    $scope.edt = '';
                    $scope.temp = '';
                    $scope.hide = false;
                    $scope.getcur();
                }

                $scope.link = function (Str) {
                    window.open(ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/Docs/timetable/' + Str, "_new");
                    //window.open('https://api.mograsys.com/kindoapi/Content/siserp/Images/Docs/timetable/' + Str, "_new");
                }

            }]);
})();