﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.TimeTable');

    simsController.controller('TimeTableClassViewCont',
        ['$scope', '$compile', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$locale', '$http', 'ENV', function ($scope, $compile, $state, $rootScope, $timeout, gettextCatalog, $locale, $http, ENV) {
            $scope.AllAcademicYear = [];

            $scope.getCurriculum = function () {
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                    $scope.Allcurriculum = res.data;
                    $scope.cur_code = $scope.Allcurriculum[0].sims_cur_code;
                    $scope.getAcademicYear();
                });
            }

            $scope.getAcademicYear = function () {
                var a = { opr: 'A', cur_code: ''};
                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableCommon", a).then(function (res) {
                    
                    $scope.AllAcademicYear = res.data.table;
                    $($scope.AllAcademicYear).each(function (k, v) {
                        if (v.sims_academic_year_status == 'C') {
                            $scope.aca_year = v.sims_academic_year;
                            
                            return false;
                        }
                    });

                    $scope.getGrade();
                });
            };

            $scope.getGrade = function () {
                $scope.grade_code = "";
                $scope.sec_code = "";
                var a = { 'opr': 'W', AcaYear: $scope.aca_year, cur_code: $scope.cur_code };
                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableCommon", a).then(function (res) {
                    $scope.AllGrades = res.data.table;
                    if ($scope.AllGrades.length > 0) {
                        $("#grade_select").prop("disabled", false).removeClass('seldisabled');
                        $scope.grade_code = $scope.AllGrades[0].sims_grade_code;
                        $scope.getsection();
                    }
                    
                });
            };


            $scope.getsection = function () {
                $scope.sec_code = "";
                var a = {'opr': 'S', AcaYear: $scope.aca_year, grade_code: $scope.grade_code, cur_code : $scope.cur_code};
                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableCommon", a).then(function (res) {

                    if (res.data.table.length > 0) {
                        $scope.getSectionFromGrade = res.data.table;                    
                        $scope.sec_code = $scope.getSectionFromGrade[0].sims_section_code;
                        $scope.genClassView();
                    }
                });
            };

            $scope.genClassView = function () {
                $(".table .contenttd").html('');
                var a = { opr: 'M', AcaYear: $scope.aca_year, grade_code: $scope.grade_code, sec_code: $scope.sec_code };
                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableView", a).then(function (res) {
                    var gname = $("#grade_select option:selected").text();
                    var sname = $("#section_select option:selected").text();
                    var aca_name = $("#aca_select option:selected").text();
                    $('.tt_title').html('<div class="inline-block"><span class="semi-bold p-r-5"> Acadenic Year: ' + aca_name + '</span> </div> | <div class="inline-block"><span class="semi-bold p-r-5"> Class: ' + gname + ' ' + sname + '</span></div>');
                    $(".grid-body").removeClass('hide');
                    $scope.gridData = res.data.table;
                    $scope.slot_count = res.data.table1[0].column1;
                    $scope.day_count = res.data.table2[0].column1;
                    $scope.time_break_detail = res.data.table3;
                    $scope.myTmpData = res.data.table4;
                    $scope.min_day = parseInt(res.data.table5[0]['min_day'], 10);
                    $scope.sims_bell_code = res.data.table6[0]['sims_bell_code'];
                    var time_break_detail_new = new Array($scope.day_count);
                    var iTmp = -2;
                    $($scope.myTmpData).each(function (key, val) {
                        if (iTmp == -2 || iTmp < (parseInt(val.sims_bell_day_code, 10) - $scope.min_day)) {
                            iTmp = parseInt(val.sims_bell_day_code, 10) - $scope.min_day;
                            time_break_detail_new[parseInt(val.sims_bell_day_code, 10) - $scope.min_day] = new Array();
                            time_break_detail_new[parseInt(val.sims_bell_day_code, 10) - $scope.min_day][0] = undefined;
                        }
                        time_break_detail_new[parseInt(val.sims_bell_day_code, 10) - $scope.min_day][parseInt(val.sims_bell_slot_code, 10)] = new Array();
                        time_break_detail_new[parseInt(val.sims_bell_day_code, 10) - $scope.min_day][parseInt(val.sims_bell_slot_code, 10)].push(val.sims_bell_start_time, val.sims_bell_end_time, val.sims_bell_break, val.sims_bell_slot_desc);
                    });
                    $scope.time_break_detail_new = time_break_detail_new;
                                        
                }).then(function () {
                    $scope.publishData();
                });
            }

            $scope.publishData = function () {
                if ($scope.slot_count != undefined) {
                    $scope.genGrid();
                }
                $($scope.gridData).each(function (k, v) {
                    var html = '<div class="sub_tec_cont"><div class="cvsubname ellipsis" title="' + v.sims_subject_name_en + '">' + v.sims_subject_name_en + '</div><span ng-show="showTeacher"><div class="viewsep"></div><div class="cvtrname ellipsis" title="' + v.sims_teacher_name + '">' + v.sims_teacher_name + '</div></span></div>';
                    var temp = $compile(html)($scope);

                    if ($("#" + v.sims_bell_day_code + '_' + v.sims_bell_slot_code + ' .lectureData').children('.sub_tec_cont').length > 0) {
                        $("#" + v.sims_bell_day_code + '_' + v.sims_bell_slot_code + ' .lectureData').addClass('group');
                    }

                    $("#" + v.sims_bell_day_code + '_' + v.sims_bell_slot_code + ' .lectureData').addClass('occupied').append(temp);
                });
                $('.lec_count').html($(".occupied").length);
                $scope.showTeacher = true;
                $scope.showDLec = false;
                $scope.showLectureName = true;
                $scope.showLectureTime = true;
            }

            $scope.toggleTeacher = function () {
                $scope.showTeacher = !$scope.showTeacher;
            }

            $scope.toggleDLec = function () {
                $scope.showDLec = !$scope.showDLec;
            }

            $scope.toggleLectureName = function () {
                $scope.showLectureName = !$scope.showLectureName;
            }

            $scope.toggleLectureTime = function () {
                $scope.showLectureTime = !$scope.showLectureTime;
            }

            $scope.genGrid = function () {
                var html = '<table id="classViewTable" class="table table-hover table-bordered text-center">';
                html += '<thead>';
                html += '<tr>';
                html += '<th class="nobg text-center lecturewidth" ng-show="showDLec">D/Lec</th>';

                for (var i = 0; i < $scope.day_count; i++) {
                    html += '<th class="slotWidth daytd text-center">' + $scope.oSlotDayNames[i] + '</th>';
                }

                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';

                var lecture_num = 1;
                var break_count = 0;
                for (var i = 0; i < $scope.slot_count; i++) {

                    if ($scope.time_break_detail[i]['sims_bell_break'] == 'Y') {
                        break_count = break_count + 1;
                    }

                    var lec_c;

                    if ($scope.time_break_detail[i]['sims_bell_break'] == 'Y') {
                        lec_c = $scope.time_break_detail[i]['sims_bell_slot_desc'];
                    }
                    else {

                        lec_c = $scope.time_break_detail[i]['sims_bell_slot_desc'];
                        lecture_num++;
                    }
                    var arrs = $scope.time_break_detail[i]['sims_bell_start_time'].split(":");
                    var timestart = arrs[0] + ':' + arrs[1];

                    var arre = $scope.time_break_detail[i]['sims_bell_end_time'].split(":");
                    var timesend = arre[0] + ':' + arre[1];

                    html += '<tr class=""> <td class="slotWidth text-center text-white" ng-show="showDLec"> ' + lec_c + '<span class="display_block font-sm">[' + timestart + ' to ' + timesend + ']</span></td>';
                    
                    for (var j = 0; j < $scope.day_count; j++) {
                        var tdbg, tdid, data, data1;
                        if ($scope.time_break_detail_new[j][i + 1] !== undefined && $scope.time_break_detail_new[j][i + 1][2] == 'Y') {
                            data = $scope.time_break_detail_new[j][i + 1][3];
                            data1 = $scope.time_break_detail_new[j][i + 1][0] + '-' + $scope.time_break_detail_new[j][i + 1][1];
                            tdbg = "url('assets/img/stbg.png')";
                            tdid = "";
                            //html += '<td id="' + tdid + '" style="background: ' + tdbg + '" class="slotWidth contenttd text-center" ><div class="lectureData"></div><span ng-show="showLectureName"><div class="ellipsis" title="' + data + '">' + data + '</div></span><span ng-show="showLectureTime"><div class="ellipsis" title="' + data1 + '">' + data1 + '</div></span></td>';
                        }
                        else {
                            data = $scope.time_break_detail_new[j][i + 1] === undefined ? '' : $scope.time_break_detail_new[j][i + 1][3];
                            data1 = $scope.time_break_detail_new[j][i + 1] === undefined ? '' : $scope.time_break_detail_new[j][i + 1][0] + '-' + $scope.time_break_detail_new[j][i + 1][1];
                            //InnerFlag = $scope.time_break_detail_new[j][i + 1] === undefined ? false : true;
                            tdbg = '#fff';
                            tdid = ((j + 1)) + '_' + ((i + 1));
                           // html += '<td id="' + tdid + '" style="background: ' + tdbg + '" class="slotWidth contenttd text-center" ><div class="lectureData"></div><span ng-show="showLectureName&&' + InnerFlag + '"></span><div class="ellipsis" title="' + data + '">' + data + '</div></span><span ng-show="showLectureTime&&' + InnerFlag + '"><div class="ellipsis" title="' + data1 + '">' + data1 + '</div></span></td>';
                        }
                        html += '<td id="' + tdid + '" style="background: ' + tdbg + '" class="slotWidth contenttd text-center" ><div class="lectureData"></div><span ng-show="showLectureName"><div class="ellipsis" title="' + data + '">' + data + '</div></span><span ng-show="showLectureTime"><div class="ellipsis" title="' + data1 + '">' + data1 + '</div></span></td>';
                    }
                }
                html += '</tr>';
                html += '</tbody>';
                html += '</table>';

                var temp = $compile(html)($scope);
                $("#CVgrid").html(temp);
            }

            $scope.getdays = function () {
                var a = { opr: 'Q' };
                $http.post(ENV.apiUrl + "api/SchoolTimeTable/TimeTableCommon", a).then(function (res) {
                    $scope.oSlotDayNamestemp = res.data.table;
                    $scope.oSlotDayNames = [];
                    $($scope.oSlotDayNamestemp).each(function (k, v) {
                        $scope.oSlotDayNames[parseInt(v.sims_appl_parameter) - 1] = v.sims_appl_form_field_value1;
                    });
                });
            }

            $scope.printDiv = function () {
                var data = {
                    location: 'Schedulling.SCHR05',
                    // location: rname,
                    parameter: {
                        acad_year: $scope.aca_year,
                        bell: $scope.sims_bell_code,
                        gradecode: $scope.grade_code,
                        seccode: $scope.sec_code,
                        disp_lect: ($scope.showLectureName === true ? 1 : 0),
                        disp_tim: ($scope.showLectureTime === true ? 1 : 0),
                        disp_d_lect: ($scope.showDLec === true ? 1 : 0),
                        disp_teac: ($scope.showLectureName === true ? 1 : 0)
                    },
                    state: 'main.TtClVw',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
            }

            $(document).ready(function () {
                $scope.getCurriculum();
                $scope.condenseMenu();
                $scope.getdays();
            });



        }]);
})();

