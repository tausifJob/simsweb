﻿var oData = [];
var selectedSubject = [];

(function () {
    'use strict';
    var simsController = angular.module('sims.module.TimeTable');
    var iSlotCount;// = 5;
    var iSlotDay;// = 5;
    var AllGrades;
    var AllGradesOther;
    var selectedAcaYear
    var AllAcademicYear = [];
    var getSectionFromGrade;
    var getOtherSectionFromGrade;
    var addAnotherSubject_year;
    var getAllSubjects;
    var oSlotDayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var sHtmlContent = "";
    var opr;
    var a;
    var idata;
    var odataTiles = [];
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    var selectedSubject = [];

    simsController.controller('SubjectTeacherCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$locale', '$compile', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $locale, $compile, $http, ENV) {

            $scope.RenderTilesnGridClassView = function () {

                sHtmlContent = "<table class=\"table table-hover table-bordered \"> <thead><tr> <th class=\"slotWidth\"> Day/Slot</th>";
                for (var i = 0; i < $scope.iSlotCount; i++) {
                    var lec_c;
                    if ($scope.time_break_detail[i]['sims_bell_break'] == 'Y') {
                        lec_c = $scope.time_break_detail[i]['sims_bell_slot_desc'];
                    }
                    else {
                        lec_c = $scope.time_break_detail[i]['sims_bell_slot_desc'];
                    }


                    sHtmlContent += '<th class=\"slotWidth\"> ' + lec_c + '<span class="display_block font-sm">[' + $scope.time_break_detail[i]['sims_bell_start_time'] + ' to ' + $scope.time_break_detail[i]['sims_bell_end_time'] + ']</span></th>';
                }
                sHtmlContent += "</tr></thead>";
                for (var i = 0; i < $scope.iSlotDay; i++) {
                    sHtmlContent += "<tr> <td class=\"slotWidth\">" + oSlotDayNames[i] + "</td>";
                    var break_count = 0;
                    for (var j = 0; j < $scope.iSlotCount; j++) {

                        var tdbg, tdata, tdid;

                        if ($scope.time_break_detail[j]['sims_bell_break'] == 'Y') {
                            tdbg = "url('assets/img/stbg.png')";
                            tdid = "";
                            tdata = "";
                            break_count = break_count + 1;

                        }
                        else {
                            tdbg = '#fff';
                            tdid = (i + 1) + '_' + ((j + 1) - break_count);
                            tdata = "<i tdid=\"" + tdid + "\" class=\"nosubject glyphicon glyphicon-plus\" ng-click=\"addSubject($event)\"></i>";

                        }

                        sHtmlContent += '<td id="' + tdid + '" class="slotWidth add_sub"  style="background: ' + tdbg + '" > '+ tdata +' </td>';

                    }
                    sHtmlContent += "</tr>";
                }
                sHtmlContent += "</table>";
                var temp = $compile(sHtmlContent)($scope);
                $(".slotArea").html("");

                $(".slotArea").append(temp);
                $(".slotWidth").css("text-align", "center");
            }

            $scope.getAcademicYear = function () {
                AllAcademicYear = [];
                $http.post(ENV.apiUrl + "api/SubjectTeacher/ImportAcademicYear").then(function (getAcaYear) {
                    $scope.allAcademicYear = getAcaYear.data;
                    $scope.selectedYear = getAcaYear.data[0].aca_year;
                    $scope.getGrade($scope.selectedYear);
                });
            };

            $scope.getGrade = function (str) {
                AllGrades = [];
                $http.post(ENV.apiUrl + "api/SubjectTeacher/ImportAllGrades?aca_year=" + str).then(function (getAllGrades) {
                    $scope.AllGrades = getAllGrades.data;
                    $("#grade_select").prop("disabled", false).removeClass('seldisabled');
                });
            };

            $scope.getsection = function (str1, str2) {
                getSectionFromGrade = [];
                $http.post(ENV.apiUrl + "api/SubjectTeacher/ImportAllSection?aca_year=" + str1 + "&param=" + str2).then(function (Allsection) {
                    $scope.getSectionFromGrade = Allsection.data;
                    $("#section_select").prop("disabled", false).removeClass('seldisabled');
                });
            };

            $scope.getSubjects = function (str1, str2, str3) {
                $(".msg").hide();
                $(".slotAreaholder").removeClass('hide');
                $http.post(ENV.apiUrl + "api/SubjectTeacher/ImportAllSubject?aca_year=" + str1 + "&grade=" + str2 + "&section=" + str3).then(function (Allsubjects) {
                    $scope.getAllSubjects = Allsubjects.data;
                });
                $scope.MyData(str1, str2, str3);
            }

            $scope.MyData = function (str1, str2, str3) {
                if (str1 != undefined && str2 != undefined && str3 != undefined) {
                    a = { OPR: 'B', EmpID: $rootScope.globals.currentUser.username, sims_academic_year: str1, sims_grade_code: str2, sims_section_code: str3 };
                }
                $http.post(ENV.apiUrl + "api/SubjectTeacher/SubjectTeacherDataImport", a).then(function (res) {

                    console.log("This is break detaail", res);

                    $scope.iSlotCount = res.data.table1[0].column1;
                    $scope.iSlotDay = res.data.table2[0].column1;
                    $scope.opr = res.data.table3[0].column1;
                    $scope.teacherCode = res.data.table3[0].column2;
                    $scope.time_break_detail = res.data.table4;
                    $scope.RenderTilesnGridClassView();

                    if ($scope.opr == 'Y' && res.data.table.length > 0) {
                        oData = [];
                        oData = res.data.table;
                        selectedSubject = [];
                        $(".msg").hide();
                        $(".slotAreaholder").removeClass('hide');
                        $.each(oData, function (key, value) {
                            selectedSubject.push({ teacher_code: value.teacherCode, aca_year: value.academicYear, bell_code: value.bellCode, day_code: value.dayCode, slot_code: value.slotCode, grade_code: value.gradeCode, grade_name: value.gradeName, section_code: value.sectionCode, section_name: value.sectionName, subject_name: value.subjectName, subject_color: value.subjectColor, subject_code: value.subjectCode });
                            var tdid = value.dayCode +"_"+ value.slotCode;
                            var border = 'solid';

                            var gradeName = value.gradeName;
                            gradeName = gradeName.replace("Class", "");

                            if (value.subjectType == '01') {
                                border: 'dashed';
                            }

                            if ($('#' + tdid).find('i.nosubject').length == 0) {
                                $("#" + tdid + " .slotClassSection").append("<span>[" + gradeName + " " + value.sectionName + "]<span>");
                            }
                            else {
                                var temp = $compile("<div title=\"" + value.subjectName + "\" class=\"slot\" data-subjecttype=\"" + value.subjectType + "\" data-day = \"" + value.dayCode + "\" data-slot=\"" + value.slotCode + "\" data-color= \"" + value.subjectColor + "\" data-subjectcode= \"" + value.subjectCode + "\"  style=\"background:" + value.subjectColor + "; border-style: " + border + "\"><h6>" + value.subjectName + "</h6><div  class=\"slotClassSection\"><span>[" + gradeName + " " + value.sectionName + "]<span></div><i class=\"close_select glyphicon glyphicon-remove\" ng-click=\"closeSubjectOptionu('" + tdid + "')\"></i> <i class=\"add_another_subject glyphicon glyphicon-plus\" ng-click=\"addAnotherSubject('" + value.academicYear + "', '" + value.gradeCode + "' , '" + value.sectionCode + "', '" + value.subjectCode + "', '" + tdid + "', '" + value.bellCode + "')\"></i></div>")($scope);
                                $("#" + tdid).html(temp);
                            }
                            $("#" + tdid + " .slotClassSection").attr('title', $("#" + tdid + " .slotClassSection").text());
                        });
                        $scope.academicYear = selectedSubject[0].aca_year;

                    }
                });
            };

            $scope.addSubject = function ($event) {
                $scope.selsub = '0';
                var element = $event.target;
                if ($('#section_select')[0].value !== "" && $('#grade_select')[0].value !== "") {
                    var tdid = $(element).parent().attr('id');
                    var select_str = "<select id=\"sel" + tdid + "\" data-role=\"dropdownlist\"  class=\"selectpicker\" ng-change=\"selectSubject('" + tdid + "')\" ng-model=\"selsub\"  ><option selected disabled value=\"\">Please Select</option><option ng-repeat=\"subject in getAllSubjects\" value=\"{{subject.sims_subject_code}}\" data-type=\"{{subject.sims_subject_type}}\" data-color=\"{{subject.sims_subject_color}}\">{{subject.sims_subject_name_en}}</option></select>";
                    var temp = $compile(select_str)($scope);
                    $(element).parent('td').text("").html(temp);
                }
            };

            $scope.selectSubject = function (tdid) {
                var subject_code = $("#sel" + tdid).val();
                var subject_name = $("#sel" + tdid + " option:selected").text();
                var subject_color = $("#sel" + tdid + " option:selected").attr('data-color');
                var subject_type = $("#sel" + tdid + " option:selected").attr('data-type');
                var grade_name = $("#grade_select option:selected").text();
                var section_name = $("#section_select option:selected").text();
                var grade_code = $("#grade_select").val();
                var acc_year = $("#grade_select option:selected").attr('data-acayear');
                var bell_code = $("#grade_select option:selected").attr('data-bellcode');
                var section_code = $("#section_select").val();
                var border = "solid";
                if (subject_type == "02") {
                    border = "dashed";
                }
                else {
                    border = "solid";
                }

                var id_arr = tdid.split("_");
                odataTiles.day_code = id_arr[0];
                odataTiles.slot_count = id_arr[1];

                selectedSubject.push({
                    teacher_code: $scope.teacherCode, aca_year: acc_year, bell_code: bell_code,
                    day_code: odataTiles.day_code, slot_code: odataTiles.slot_count, slotileId: tdid, grade_code: grade_code, grade_name: grade_name, section_code: section_code, section_name: section_name, subject_name: subject_name, subject_color: subject_color, subject_code: subject_code
                });

                console.log("this is gagagag = ", selectedSubject);

                var temp = $compile("<div class=\"slot \" title=\"" + subject_name + "\" data-subjecttype=\"" + subject_type + "\" data-day = \"" + odataTiles.day_code + "\" data-slot=\"" + odataTiles.slot_count + "\" data-color= \"" + subject_color + "\" data-subjectcode= \"" + subject_code + "\"  style=\"background:" + subject_color + "; border-style: " + border + "\" ><h6>" + subject_name + "</h6><div  class=\"slotClassSection\"><span>[" + grade_name.replace("Class", "") + " " + section_name + "]</span></div><i class=\"close_select glyphicon glyphicon-remove\" ng-click=\"closeSubjectOptionu('" + tdid + "')\"></i><i class=\"add_another_subject glyphicon glyphicon-plus\" ng-click=\"addAnotherSubject('" + acc_year + "', '" + grade_code + "', '" + section_code + "', '" + subject_code + "', '" + tdid + "' )\"></i></div>")($scope);
                $("#" + tdid).html(temp);
            }


            $scope.closeSubjectOptionu = function (tdid) {

                var index;
                if ($scope.opr == 'Y') {
                    var id_arr = tdid.split("_");
                    odataTiles.day_code = id_arr[0];
                    odataTiles.slot_count = id_arr[1];

                    indexes = selectedSubject.findIndex(function (e) {
                        return e.day_code == odataTiles.day_code && e.slot_code == odataTiles.slot_count;
                    });

                    var indexes = $scope.getAllIndexes(selectedSubject, tdid, odataTiles);
                    indexes = indexes.sort(function (a, b) { return b - a });
                }
                else {
                    var indexes = $scope.getAllIndexes(selectedSubject, tdid);
                    indexes = indexes.sort(function (a, b) { return b - a });
                }

                if (indexes.length > 0) {

                    $.each(indexes, function (key, value) {
                        if (value >= 0) {
                            selectedSubject.splice(value, 1);
                        }
                    })
                }

                var temp = $compile("<i class=\"nosubject glyphicon glyphicon-plus\" ng-click=\"addSubject($event)\"></i>")($scope);
                $("#" + tdid).html(temp);
            }

            $scope.getAllIndexes = function (arr, val, odataTiles) {
                var indexes = [];

                if ($scope.opr == 'Y') {
                    $.each(arr, function (key, value) {
                        if (value.day_code == odataTiles.day_code && value.slot_code == odataTiles.slot_count) {
                            indexes.push(key);
                        }
                    });
                }
                else {
                    $.each(arr, function (key, value) {
                        if (value.slotileId == val) {
                            indexes.push(key);
                        }
                    });
                }
                return indexes;
            }



            $scope.saveRecord = function () {
                $('#loader').modal({ backdrop: 'static', keyboard: false });
                var cTemp = 'I';
                if ($scope.opr === 'Y') {
                    cTemp = 'U';
                }

                if (selectedSubject.length > 0) {
                    $scope.opr = 'Y';
                }
                console.log("This is selected subjects", selectedSubject);
                $http.post(ENV.apiUrl + "api/SubjectTeacher/SubjectTeacherDataUpdate?cTemp=" + cTemp + "&empID=" + $rootScope.globals.currentUser.username + "&aca_year=" + $scope.academicYear, selectedSubject).then(function (res) {
                    swal({ title: "Alert", text: 'Data Submitted.', showCloseButton: true, width: 450, });
                    $('#loader').modal('hide');
                });

            };

            $scope.activateBtn = function () {
                $(".saveanotherbtn").prop("disabled", false);
            }

            $scope.addAnotherSubject = function (acaYear, gradeCode, section, subCode, tdid, bell_code) {

                $scope.another_grade_select = '0';
                $scope.another_section_select = '0';
                $("#another_section_select").hide();
                $(".saveanotherbtn").prop("disabled", true);
                $(".cssload-container").hide();
                $("#addAnotherSubjectModal .saveanotherbtn").attr("tdid", tdid);
                $scope.addAnotherSubject_year = acaYear;
                $scope.getAnotherGrades(acaYear, subCode, bell_code);
                $("#addAnotherSubjectModal").modal('show');
            }

            $scope.getAnotherGrades = function (str1, str2, str3) {
                console.log("hahahaa = ", str3);
                $http.post(ENV.apiUrl + "api/SubjectTeacher/ImportSubjectGrades?aca_year=" + str1 + "&sub_code=" + str2 + "&bell_code=" + str3).then(function (res) {
                    $scope.AllGradesOther = res.data;

                    console.log("This is tegst == ", $scope.AllGradesOther);
                    $("#another_grade_select").prop("disabled", false).removeClass('seldisabled');
                });
            }

            $scope.getAnotherSection = function (str1, str2) {
                $("#another_section_select").hide();
                $(".cssload-container").css({ 'display': 'inline-block' });

                var tdid = $("#addAnotherSubjectModal .saveanotherbtn").attr("tdid");
                var subCode = $("#" + tdid + " .slot").attr("data-subjectcode");
                getOtherSectionFromGrade = [];
                $http.post(ENV.apiUrl + "api/SubjectTeacher/ImportSubjectSection?aca_year=" + str1 + "&param=" + str2 + "&sub_code=" + subCode).then(function (res) {
                    $scope.getOtherSectionFromGrade = res.data;
                    $(".cssload-container").hide();
                    $("#another_section_select").show();
                });
            }

            $scope.SaveAnotherSubject = function (e) {
                $scope.tdid = $(e.target).attr('tdid');
                $scope.subject_code = $("#" + $scope.tdid + " .slot ").attr('data-subjectcode');
                $scope.subject_name = $("#" + $scope.tdid + " .slot ").attr('title');
                $scope.subject_color = $("#" + $scope.tdid + " .slot ").attr('data-color');
                $scope.subject_type = $("#" + $scope.tdid + " .slot ").attr('data-subjecttype');
                $scope.grade_name = $("#another_grade_select option:selected").text();
                $scope.section_name = $("#another_section_select option:selected").text();
                $scope.section_code = $("#another_section_select").val();
                $scope.grade_code = $("#another_grade_select").val();
                $scope.bell_code = $("#another_grade_select option:selected").attr('data-bellcode');
                $scope.dayCode = $("#" + $scope.tdid + " .slot ").attr('data-day');
                $scope.slotCount = $("#" + $scope.tdid + " .slot ").attr('data-slot');


                selectedSubject.push({ teacher_code: $scope.teacherCode, aca_year: $scope.addAnotherSubject_year, bell_code: $scope.bell_code, day_code: $scope.dayCode, slot_code: $scope.slotCount, slotileId: $scope.tdid, grade_code: $scope.grade_code, grade_name: $scope.grade_name, section_code: $scope.section_code, section_name: $scope.section_name, subject_name: $scope.subject_name, subject_color: $scope.subject_color, subject_code: $scope.subject_code });
                $scope.grade_name = $scope.grade_name.replace("Class", "");
                $scope.section_name = $scope.section_name.replace("Class", "");
                var html = "<span>[" + $scope.grade_name + " " + $scope.section_name + "]</span>";
                html = $compile(html)($scope);
                $("#" + $scope.tdid + " .slotClassSection").append(html);
                $("#" + $scope.tdid + " .slotClassSection").attr('title', $("#" + $scope.tdid + " .slotClassSection").text());

            }

            $scope.setAcaYear = function (str) {
                $scope.academicYear = str;
                //$scope.MyData($scope.academicYear);
                $scope.getGrade($scope.academicYear);
            }

            $(document).ready(function () {

                //$scope.MyData();
                $scope.getAcademicYear();
            });
        }]);

})();





