﻿(function () {
    'use strict';
    angular.module('sims.module.TimeTable', [
        'sims',
        'bw.paging',
        'gettext'
    ]);
})();