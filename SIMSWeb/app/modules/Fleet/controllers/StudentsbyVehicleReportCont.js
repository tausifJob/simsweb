﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Fleet');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('StudentsbyVehicleReportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.temp = {};
            $scope.temp.sims_appl_parameter = '';
            $scope.pagesize = "50";
            $scope.pageindex = 0;
            $scope.pager = true;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.route_dir1 = '';
            $scope.route_name1 = '';
            $scope.date = '';
            var user = $rootScope.globals.currentUser.username;
            $scope.SchoolLogo = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/SchoolLogo/';
            

            $(function () {
                $('#route_dir_box1').multipleSelect({ width: '100%' });
                $('#route_name_box1').multipleSelect({ width: '100%', filter: true });

            });

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_start_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });


            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_end_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });



            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res1) {
                $scope.cur_data = res1.data;
                $scope.temp.sims_cur_code = $scope.cur_data[0].sims_cur_code;
                $scope.getAcademic_year($scope.cur_data[0].sims_cur_code);
            });

            $scope.getAcademic_year = function (cur_code1) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code1).then(function (res1) {
                    $scope.acad_data = res1.data;
                    $scope.temp.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                    
                });
            }


           
            $http.get(ENV.apiUrl + "api/studentslist/getDirection").then(function (res1) {
                debugger;
                    $scope.route_dir = res1.data;

                    setTimeout(function () {
                        $('#route_dir_box').change(function () {

                        }).multipleSelect({
                            width: '100%',
                            filter: true
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            

            $scope.getroute_name = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/studentslist/getRouteName?acad_year=" + $scope.temp.sims_academic_year + "&route_direction=" + $scope.temp.sims_appl_parameter).then(function (res1) {
                    $scope.route_name = res1.data;
                    setTimeout(function () {
                        $('#route_name_box').change(function () {

                        }).multipleSelect({
                            width: '100%',
                            filter: true
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }


            $scope.getdetail = function () {
                debugger;
                $scope.route_dir1 = $('#route_dir_box').multipleSelect('getSelects', 'text');
                $scope.route_name1 = $('#route_name_box').multipleSelect('getSelects', 'text');
                $scope.date = $scope.mom_start_date;
                $scope.cur = $scope.cur_data[0].sims_cur_short_name_en;                
                $scope.acad = $scope.acad_data[0].sims_academic_year_desc;
                debugger; 
                $scope.colsvis = false;
                $http.get(ENV.apiUrl + "api/studentslist/getStudentBtVehicleList?acad_year=" + $scope.temp.sims_academic_year + "&tranport_route="+ $scope.temp.sims_transport_route_code + "&route_direction=" + $scope.temp.sims_appl_parameter + "&mom_start_date=" + $scope.mom_start_date).then(function (res1) {
                    if (res1.data.length > 0) {
                        $scope.total_length = angular.copy(res1.data.length);
                            $scope.report_data = res1.data;
                            $scope.totalItems = $scope.report_data.length;
                            $scope.todos = $scope.report_data;
                            $scope.makeTodos();
                            $scope.pager = true;

                        }
                        else {
                            swal({ title: "Alert", text: " Data Not Found", showCloseButton: true, width: 300, height: 200 });
                            $scope.filteredTodos = [];
                            $scope.pager = false;
                           
                        }



                    });
            }


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.colsvis = false;

            $scope.exportData = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('rpt_data1').innerHTML],
                         {
                             type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                         });
                        $scope.detailsField = false;
                        saveAs(blob, "StudentsByVehicle.xls");
                        $scope.colsvis = false;
                        $scope.getdetail();

                    }
                    $scope.getdetail();
                });

            };

            $scope.print = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var docHead = document.head.outerHTML;
                        var printContents = document.getElementById('rpt_data1').outerHTML;
                        var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                        var newWin = window.open("", "_blank", winAttr);
                        var writeDoc = newWin.document;
                        writeDoc.open();
                        writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                        writeDoc.close();
                        newWin.focus();
                        $scope.getdetail();

                    }
                    $scope.getdetail();
                });

            };
          

          


            $scope.reset_data = function () {
                debugger;
                $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
                $scope.filteredTodos = [];
                $scope.temp.sims_appl_parameter = '';
                $scope.temp.sims_transport_route_code = '';
                $scope.pager = false;
                $scope.route_name1 = '';
                $scope.route_dir1 = '';
                $scope.total_length = '';
                $scope.date = '';
                $scope.searchText = '';
                $scope.cur = '';
                $scope.acad = '';

                try {
                    $('#route_name_box').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#route_dir_box').multipleSelect('uncheckAll');
                } catch (e) {

                }


            }


            //sort
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.size = function (str) {


                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.obj;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 50, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };



        }])

})();

