﻿(function () {
    'use strict';
    var Student_bacth_code = [], selected_enroll_number = [];
    var main;
    var data1 = [];
    var code = '', enddate;
    var itemset;
    var enroll_number = [];
    var enroll_number1 = [];
    var enroll_number2 = [];
    var common_user = [];
    var simsController = angular.module('sims.module.Fleet');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TransportRouteStudAHGSCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {
            $scope.display = true;
            $scope.grid = false;
            $scope.edt2 = [];
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.pagesize1 = '10';
            $scope.pageindex1 = "0";
            $scope.pager1 = true;

            var route_code_new;
            $scope.saveStudent = false;
            $scope.saveEmployee = false;
            $scope.selectsecond = false;
            $scope.student_grid_record = false;
            $scope.employee_grid_record = false;
            $scope.inward1 = true;
            $scope.outward1 = true;
            $scope.enroll_number = [];
            $scope.common_user = [];
            $scope.edt = {};
            $scope.enroll_number1 = [];
            $scope.enroll_number = [];
            $scope.info = {};
            $scope.temp3 = {};
            //var today = new Date();
            //var dd = today.getDate();
            //var mm = today.getMonth() + 1; //January is 0!
            //if (mm < 10) {
            //    mm = '0' + mm;
            //}

            //var yyyy = today.getFullYear();
            //$scope.sdate = yyyy + '-' + mm + '-' + dd;
            //$scope.edate = yyyy + '-' + mm + '-' + dd;

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };


            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');
            $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getAcademicDate?cur_code=" + $rootScope.globals.currentUser.username).then(function (academicdates) {
                debugger;
                enddate = academicdates.data[0].sims_transport_effective_upto1;
                $scope.dt = {
                    'sims_transport_effective_from': $scope.ddMMyyyy,
                    'sims_transport_effective_upto': enddate,
                }
            });


            //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');
            //$scope.dt = {
            //    'sims_transport_effective_from': $scope.ddMMyyyy,
            //    'sims_transport_effective_upto': '2017-03-31',
            //}

            itemset = document.getElementById("chk_term10");
            ////$scope.size = function (str) {
            ////    console.log(str);
            ////    $scope.pagesize = str;
            ////    $scope.currentPage = 1;
            ////    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            ////}

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            ////$scope.size1 = function (str) {
            ////    console.log(str);
            ////    $scope.pagesize1 = str;
            ////    $scope.currentPage1 = 1;
            ////    $scope.numPerPage1 = str; console.log("numPerPage1=" + $scope.numPerPage1); $scope.makeTodos1();
            ////}

            $scope.index1 = function (str) {
                $scope.pageindex1 = str;
                $scope.currentPage1 = str; console.log("currentPage1=" + $scope.currentPage1);
                $scope.makeTodos1();
                main.checked = false;
                $scope.row1 = '';
            }

            $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getBusCode_AHGS").then(function (getBusCode_Data) {
                $scope.BusCode_Data = getBusCode_Data.data;
                //s$scope.v_Code = $scope.BusCode_Data[0];
            });

            $scope.getroutebybuscode = function () {
                debugger;
                $scope.SelectsecondBus_uncheck();
               
                $scope.hide_asroute = true;
                $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getRouteNameByBusCode?AcaYear=" + $scope.edt.sims_academic_year + "&Vehicle_Code=" + $scope.Bcode.v_Code).then(function (getRouteNameByBusCode_Data) {
                    $scope.AllRouteName = getRouteNameByBusCode_Data.data;
                    $scope.sims_transport_route_code = [];
                    $scope.route_code_new1 = [];
                    $scope.sims_transport_route_code = $scope.AllRouteName[0];
                    $scope.route_code_new1 = $scope.sims_transport_route_code;
                    $scope.sims_transport_route_code['sims_transport_route_code'] = $scope.AllRouteName[0].sims_transport_route_code,

                    $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getSecondBusCode?V_code=" + $scope.Bcode.v_Code).then(function (get_VehicleCode_2) {
                        $scope.VehicleCode_2 = get_VehicleCode_2.data;
                        //$scope.temp3 = { sims_transport_vehicle_code_2: $scope.VehicleCode_2[0].sims_transport_vehicle_code_2 }
                        //{ sims_cur_code: $scope.curriculum[0].sims_cur_code };
                        $scope.getroutebybuscode_2();
                    })

                    $scope.getdirectbyroute($scope.sims_transport_route_code.sims_transport_route_code, $scope.edt.sims_academic_year);
                    $scope.getdirectbyroute1($scope.sims_transport_route_code.sims_transport_route_code, $scope.edt.sims_academic_year);
                });

                for (var i = 0; i < $scope.BusCode_Data.length; i++) {
                    if ($scope.BusCode_Data[i].v_Code == $scope.Bcode.v_Code) {
                        $scope.safe_travel_bus_code_1 = $scope.BusCode_Data[i].sims_transport_vehicle_safe_travel_code;
                        break;
                    }
                }
                console.log("safe_travel_bus_code_1",$scope.safe_travel_bus_code_1);

            }

            $http.get(ENV.apiUrl + "api/common/getAllAcademicYear").then(function (get_AllAcademicYear) {
                $scope.ActiveYear = get_AllAcademicYear.data;

                $scope.edt = { sims_academic_year: $scope.ActiveYear[0].sims_academic_year };
                $scope.getroutecodename($scope.edt.sims_academic_year);
                $http.get(ENV.apiUrl + "api/TransRouteStudDetails/GetAllStudentName_AHGS?academic_year=" + $scope.edt.sims_academic_year).then(function (GetAllStudName) {
                    $scope.GetStudentName = GetAllStudName.data;
                });
            });

            $http.get(ENV.apiUrl + "api/TransRouteStudDetails/GetAllParentName").then(function (res) {
                $scope.GetAllParentName = res.data;
            });

            var enrollIncludeArray = [];
            $scope.getStudFromParent = function (parentId) {
                $http.get(ENV.apiUrl + "api/TransRouteStudDetails/GetStudentFromParent_AHGS?parent_login_code=" + parentId).then(function (res) {
                    $scope.GetStudNameFromParent = [];
                    //$scope.enroll_number1 = [];
                    $scope.GetStudNameFromParent = res.data;
                    console.log("GetStudNameFromParent", $scope.GetStudNameFromParent);
                    if ($scope.GetStudNameFromParent.length > 0) {
                        debugger
                        $scope.student_grid_record = true;
                        $scope.employee_grid_record = false;
                        $scope.saveStudent = true;
                        $scope.saveEmployee = false;

                        for (var i = 0; i < $scope.GetStudNameFromParent.length; i++) {

                            var obj = {
                                s_enroll_no: $scope.GetStudNameFromParent[i].sims_enroll_number,
                                s_sname_in_english: $scope.GetStudNameFromParent[i].student_name,
                                s_cur_code: $scope.GetStudNameFromParent[i].sims_cur_code,
                                grade_code: $scope.GetStudNameFromParent[i].sims_grade_code,
                                section_code: $scope.GetStudNameFromParent[i].sims_section_code,
                                icon: 'fa fa-plus-circle',
                                sims_student_dob: $scope.GetStudNameFromParent[i].sims_student_dob,
                                parent_name: $scope.GetStudNameFromParent[i].parent_name,
                                sims_parent_father_summary_address: $scope.GetStudNameFromParent[i].sims_parent_father_summary_address,
                                sims_parent_father_mobile: $scope.GetStudNameFromParent[i].sims_parent_father_mobile,
                                sims_parent_father_email: $scope.GetStudNameFromParent[i].sims_parent_father_email,
                                sims_parent_mother_mobile: $scope.GetStudNameFromParent[i].sims_parent_mother_mobile,
                                sims_parent_mother_email: $scope.GetStudNameFromParent[i].sims_parent_mother_email,
                                sims_parent_father_area_number: $scope.GetStudNameFromParent[i].sims_parent_father_area_number,
                                sims_student_date: $scope.GetStudNameFromParent[i].sims_student_date,
                                sims_student_transport_status: $scope.GetStudNameFromParent[i].sims_student_transport_status,
                                nationality: $scope.GetStudNameFromParent[i].nationality,
                                sims_student_gender: $scope.GetStudNameFromParent[i].sims_student_gender,
                                rfid_card: $scope.GetStudNameFromParent[i].rfid_card,
                                mother_name: $scope.GetStudNameFromParent[i].mother_name,
                                sims_transport_grade_code: $scope.GetStudNameFromParent[i].sims_transport_grade_code,
                                sims_transport_section_code: $scope.GetStudNameFromParent[i].sims_transport_section_code
                            }

                            for (var j = 0; j < $scope.enroll_number1.length; j++) {

                                if ($scope.enroll_number1[j].s_enroll_no == $scope.GetStudNameFromParent[i].sims_enroll_number) {
                                }
                                else {
                                    if (!enrollIncludeArray.includes($scope.GetStudNameFromParent[i].sims_enroll_number)) {
                                        enrollIncludeArray.push($scope.GetStudNameFromParent[i].sims_enroll_number);
                                        $scope.enroll_number1.push(obj);
                                    }

                                }
                            }

                            if ($scope.enroll_number1.length == 0) {
                                if (!enrollIncludeArray.includes($scope.GetStudNameFromParent[i].sims_enroll_number)) {
                                    enrollIncludeArray.push($scope.GetStudNameFromParent[i].sims_enroll_number);
                                    $scope.enroll_number1.push(obj);
                                }
                            }

                        }
                        $scope.enroll_number = $scope.enroll_number1;
                    }
                });

                console.log("enroll_number", $scope.enroll_number);
            }

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {

                $scope.curriculum = AllCurr.data;

                $scope.temp = { s_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear();
            });

            $scope.getAccYear = function (curCode) {

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.temp.s_cur_code).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = {
                        //s_cur_code: $scope.curriculum[0].sims_cur_code,
                        sims_academic_year: $scope.Acc_year[0].sims_academic_year
                    }


                    $scope.getGrade();
                });

            }

            //$scope.getGrade = function (curCode, accYear) {
            //    $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.s_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
            //        $scope.Grade_code = Gradecode.data;
            //    });
            //}

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.temp.s_cur_code + "&grade_code=" + $scope.temp.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }

            $scope.getroutecodename = function (Acayear) {
                debugger;
                //$http.get(ENV.apiUrl + "api/common/getAllRouteName?Acayear=" + Acayear).then(function (get_AllRouteName) {
                //    $scope.AllRouteName = get_AllRouteName.data;
                //    $scope.sims_transport_route_code = [];
                //    $scope.route_code_new1 = [];
                //    $scope.sims_transport_route_code = $scope.AllRouteName[0];
                //    $scope.route_code_new1 = $scope.sims_transport_route_code;
                //    $scope.sims_transport_route_code['sims_transport_route_code'] = $scope.AllRouteName[0].sims_transport_route_code,
                //    $scope.getdirectbyroute1($scope.sims_transport_route_code.sims_transport_route_code, $scope.edt.sims_academic_year);
                //})

                $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getEffectiveDate?date=" + Acayear).then(function (academicdates) {
                    if (academicdates.data.length != 0) {
                        if (academicdates.data[0].sims_academic_year_status == 'A') {

                            var dateStart = academicdates.data[0].sims_transport_effective_from1;
                            var dateEnd = academicdates.data[0].sims_transport_effective_upto1;
                            $scope.dt = [];
                            $scope.dt = {
                                'sims_transport_effective_from': dateStart,
                                'sims_transport_effective_upto': dateEnd,
                            }
                        }
                    }
                    else {
                        $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');
                        $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getAcademicDate?cur_code=" + $rootScope.globals.currentUser.username).then(function (academicdates) {
                            enddate = academicdates.data[0].sims_transport_effective_upto1;
                            $scope.dt = [];
                            $scope.dt = {
                                'sims_transport_effective_from': $scope.ddMMyyyy,
                                'sims_transport_effective_upto': enddate,
                            }
                        });
                    }
                });
                $http.get(ENV.apiUrl + "api/TransRouteStudDetails/getAllRouteNameForOutward?Acayear=" + Acayear).then(function (getAllRouteNameForOutward_Data) {
                    $scope.AllRouteNameForOutward_Data = getAllRouteNameForOutward_Data.data;
                })

                $http.get(ENV.apiUrl + "api/TransRouteStudDetails/getAllRouteNameForInward?Acayear=" + Acayear).then(function (getAllRouteNameForInward_Data) {
                    $scope.AllRouteNameForInward_Data = getAllRouteNameForInward_Data.data;
                })

            }

            $scope.getdirectbyroute1 = function (route_code, aca_year) {

                var direct = [];
                for (var i = 0; i < $scope.AllRouteName.length; i++) {
                    if ($scope.AllRouteName[i].sims_transport_route_name == $scope.sims_transport_route_code.sims_transport_route_name) {
                        direct.push($scope.AllRouteName[i])
                    }
                }
                $scope.DirectionName = direct;
                //$scope.edt = {
                //    sims_academic_year: $scope.ActiveYear[0].sims_academic_year,

                //    sims_transport_route_direction:$scope.DirectionName[0].sims_transport_route_direction
                //};

                $scope.sims_transport_route_code['sims_transport_route_code'] = $scope.AllRouteName[0].sims_transport_route_code,

                $scope.getstopbydirection($scope.route_code_new1.sims_transport_route_code, $scope.edt.sims_academic_year, $scope.edt.sims_transport_route_direction);

                $scope.getlangitude($scope.route_code_new1.sims_transport_route_code, $scope.edt.sims_academic_year, $scope.edt.sims_transport_pickup_stop_code);

                $scope.getlangitudedropstop($scope.route_code_new1.sims_transport_route_code, $scope.edt.sims_academic_year, $scope.edt.sims_transport_drop_stop_code);

                $http.get(ENV.apiUrl + "api/common/getSitingCapacity?route_code=" + $scope.route_code_new1.sims_transport_route_code + "&aca_year=" + aca_year).then(function (get_SitCapacity) {
                    $scope.SitCapacity = get_SitCapacity.data;
                    for (var i = 0; i < $scope.SitCapacity.length; i++) {
                        $scope.tempp = {
                            sims_transport_vehicle_seating_capacity: $scope.SitCapacity[i].sims_transport_vehicle_seating_capacity,
                        };
                    }
                })


            }

            $scope.getdirectbyroute = function (route_code, aca_year) {

                $scope.route_code_new = route_code;
                if (route_code.sims_transport_route_direction != null)
                    route_code_new = route_code.sims_transport_route_code;
                else
                    route_code_new = route_code;

                var direct = [];
                for (var i = 0; i < $scope.AllRouteName.length; i++) {
                    if ($scope.AllRouteName[i].sims_transport_route_name == $scope.sims_transport_route_code.sims_transport_route_name) {
                        direct.push($scope.AllRouteName[i])
                    }
                }

                $scope.DirectionName = direct;

                $scope.edt['sims_transport_route_direction'] = $scope.DirectionName[0].sims_transport_route_direction;

                $scope.getstopbydirection(route_code, $scope.edt.sims_academic_year, $scope.edt.sims_transport_route_direction);

                $http.get(ENV.apiUrl + "api/common/getSitingCapacity?route_code=" + route_code + "&aca_year=" + aca_year).then(function (get_SitCapacity) {
                    $scope.SitCapacity = get_SitCapacity.data;
                    for (var i = 0; i < $scope.SitCapacity.length; i++) {
                        $scope.tempp = {
                            sims_transport_vehicle_seating_capacity: $scope.SitCapacity[i].sims_transport_vehicle_seating_capacity,
                        };
                    }
                })
            }



            $scope.getstopbydirection = function (route_code, aca_year, str) {
                debugger
                var itemset = document.getElementById("chk_term10");
                if (itemset.checked == true) {
                    $scope.SelectsecondBus(str)
                }

                $scope.selectsecond = true;
                if (str == "01") {
                    $scope.inward1 = false;
                    $scope.outward1 = true;
                    $scope.selectsecond = true;
                    $scope.RoutenamesOutward = true;
                    $scope.DirectOutward = true;
                    $scope.DirectInward = false;
                    $scope.RoutenamesInward = false;
                    $http.get(ENV.apiUrl + "api/TransportRouteStudDetails/getAllRouteNameForOutward?Acayear=" + aca_year).then(function (getAllRouteNameForOutward_Data) {
                        $scope.AllRouteNameForOutward_Data = getAllRouteNameForOutward_Data.data;
                    })
                }

                else if (str == "02") {
                    $scope.outward1 = false;
                    $scope.inward1 = true;
                    $scope.selectsecond = true;
                    $scope.RoutenamesOutward = false;
                    $scope.DirectOutward = false;
                    $scope.DirectInward = true;
                    $scope.RoutenamesInward = true;
                    $http.get(ENV.apiUrl + "api/TransportRouteStudDetails/getAllRouteNameForInward?Acayear=" + aca_year).then(function (getAllRouteNameForInward_Data) {
                        $scope.AllRouteNameForInward_Data = getAllRouteNameForInward_Data.data;
                    })
                }
                else {
                    $scope.outward1 = false;
                    $scope.inward1 = false;
                    itemset.checked = false;
                    $scope.selectsecond = false;
                }

                $http.get(ENV.apiUrl + "api/common/getStopLanLogByDirection?route_code=" + $scope.sims_transport_route_code.sims_transport_route_code + "&aca_year=" + aca_year).then(function (get_PickupDrop) {
                    $scope.PickupDrop = get_PickupDrop.data;

                    $scope.edt = {
                        // sims_academic_year: $scope.ActiveYear[0].sims_academic_year,
                        sims_academic_year: $scope.edt.sims_academic_year,
                        sims_transport_route_direction: $scope.DirectionName[0].sims_transport_route_direction,
                        sims_transport_drop_stop_code: $scope.PickupDrop[0].sims_transport_stop_code,
                        sims_transport_pickup_stop_code: $scope.PickupDrop[0].sims_transport_stop_code
                    };

                    $scope.getlangitude($scope.sims_transport_route_code.sims_transport_route_code, $scope.edt.sims_academic_year, $scope.edt.sims_transport_pickup_stop_code);
                    //$scope.sims_transport_route_code['sims_transport_route_code'] = $scope.AllRouteName[0].sims_transport_route_code
                })
            }

            $scope.getlangitude = function (route_code, year, route_stop) {
                $http.get(ENV.apiUrl + "api/TransRouteStudDetails/getLanLogByRouteStop?route_code=" + $scope.sims_transport_route_code.sims_transport_route_code + "&aca_year=" + year + "&route_stop=" + route_stop).then(function (get_PickupLann_Data) {
                    $scope.PickupLanLog = get_PickupLann_Data.data;

                    for (var i = 0; i < $scope.PickupLanLog.length; i++) {
                        $scope.templat = {
                            sims_transport_route_student_stop_lat: $scope.PickupLanLog[i].sims_transport_route_student_stop_lat,
                            sims_transport_route_student_stop_long: $scope.PickupLanLog[i].sims_transport_route_student_stop_long,
                        };
                    }
                    $scope.getlangitudedropstop($scope.sims_transport_route_code.sims_transport_route_code, $scope.edt.sims_academic_year, $scope.edt.sims_transport_drop_stop_code);
                });
            }

            $scope.getlangitudedropstop = function (route_code, year, route_stop) {
                $http.get(ENV.apiUrl + "api/TransRouteStudDetails/getLanLogByRouteStop?route_code=" + $scope.sims_transport_route_code.sims_transport_route_code + "&aca_year=" + year + "&route_stop=" + route_stop).then(function (get_PickupLann_Data) {
                    $scope.PickupLanLog = get_PickupLann_Data.data;
                    for (var i = 0; i < $scope.PickupLanLog.length; i++) {
                        $scope.edt1 = {
                            sims_transport_stop_lat_drop: $scope.PickupLanLog[i].sims_transport_route_student_stop_lat,
                            sims_transport_stop_long_drop: $scope.PickupLanLog[i].sims_transport_route_student_stop_long,
                        };
                    }
                });
            }

            $scope.SelectsecondBus = function (str) {

                $scope.secondbus = true;
                itemset = document.getElementById("chk_term10");
                if (itemset.checked == true) {

                    if (str == '01') {
                        $scope.secondbusinward = false;
                        $scope.secondbusoutward = true;
                    }

                    else if (str == "02") {
                        $scope.secondbusinward = true;
                        $scope.secondbusoutward = false;
                    }

                    else {
                        $scope.secondbus = false;
                        $scope.edt2['sims_transport_route_code'] = '';
                    }
                }
                else {
                    $scope.secondbus = false;

                    $scope.edt2['sims_transport_route_code'] = '';
                    $scope.edtt['sims_transport_vehicle_seating_capacity'] = '';
                    $scope.tempp['sims_transport_route_student_stop_lat'] = '';
                    $scope.tempp['sims_transport_route_student_stop_long'] = '';
                    $scope.edtt1['sims_transport_stop_lat_drop'] = '';
                    $scope.edtt1['sims_transport_stop_long_drop'] = '';

                    if ($scope.secondbus = false) {
                        $scope.temp3['sims_transport_vehicle_code_2'] = '';
                    }

                }
            }

            $scope.SelectsecondBus_uncheck = function () {

                $scope.edt.status = false;
                $scope.secondbus = false;
                //$scope.SelectsecondBus('04');
            }

            $scope.getlangitudedropstop1 = function (route_code, year, route_stop) {
                $http.get(ENV.apiUrl + "api/TransRouteStudDetails/getLanLogByRouteStop?route_code=" + route_code + "&aca_year=" + year + "&route_stop=" + route_stop).then(function (get_PickupLann_Data1) {
                    $scope.PickupLanLog1 = get_PickupLann_Data1.data;
                    for (var i = 0; i < $scope.PickupLanLog1.length; i++) {
                        $scope.edtt1 = {
                            sims_transport_stop_lat_drop: $scope.PickupLanLog1[i].sims_transport_route_student_stop_lat,
                            sims_transport_stop_long_drop: $scope.PickupLanLog1[i].sims_transport_route_student_stop_long,
                        };
                    }

                });
            }

            $scope.getlangitude1 = function (route_code, year, route_stop) {
                $http.get(ENV.apiUrl + "api/TransRouteStudDetails/getLanLogByRouteStop?route_code=" + route_code + "&aca_year=" + year + "&route_stop=" + route_stop).then(function (get_PickupLann_Data1) {
                    $scope.PickupLanLog1 = get_PickupLann_Data1.data;
                    for (var i = 0; i < $scope.PickupLanLog1.length; i++) {
                        $scope.tempp = {
                            sims_transport_route_student_stop_lat: $scope.PickupLanLog1[i].sims_transport_route_student_stop_lat,
                            sims_transport_route_student_stop_long: $scope.PickupLanLog1[i].sims_transport_route_student_stop_long,
                        };
                        $scope.edtt1 = {
                            sims_transport_route_student_stop_lat: $scope.PickupLanLog1[i].sims_transport_route_student_stop_lat,
                            sims_transport_route_student_stop_long: $scope.PickupLanLog1[i].sims_transport_route_student_stop_long,
                        };
                    }
                });
            }

            $scope.getstopbydirection1 = function (route_code, aca_year) {

                $scope.PickupDrop1 = [];
                $http.get(ENV.apiUrl + "api/common/getStopLanLogByDirection?route_code=" + route_code + "&aca_year=" + aca_year).then(function (get_PickupDrop1) {
                    $scope.PickupDrop1 = get_PickupDrop1.data;
                    $scope.edtt.sims_transport_drop_stop_code = $scope.PickupDrop1[0].sims_transport_stop_code;
                    $scope.edtt.sims_transport_pickup_stop_code = $scope.PickupDrop1[0].sims_transport_stop_code;
                    //$scope.edtt.sims_transport_pickup_stop_code = $scope.PickupDrop1[0].sims_transport_pickup_stop_code;
                    //$scope.edtt.sims_transport_drop_stop_code = $scope.PickupDrop1[0].sims_transport_drop_stop_code;
                    $scope.getlangitudedropstop1($scope.edt2.sims_transport_route_code, $scope.edt.sims_academic_year, $scope.edtt.sims_transport_pickup_stop_code)
                });
            }

            $scope.getdirectionfor2 = function (route_code, aca_year) {

                var directoutward = [];
                var directinward = [];
                var DirectionNameOutward = [];
                var DirectionNameInward = [];

                var selectedText1;

                if ($scope.secondbusinward) {
                    var t = document.getElementById("cmb_routeCode_name_in2");
                    selectedText1 = t.options[t.selectedIndex].text;
                }
                if ($scope.secondbusoutward) {
                    var t = document.getElementById("cmb_routeCode_name_out2");
                    selectedText1 = t.options[t.selectedIndex].text;
                }
                for (var i = 0; i < $scope.AllRouteNameForOutward_Data.length; i++) {
                    //if ($scope.AllRouteNameForOutward_Data[i].sims_transport_route_name == selectedText1) {
                    directoutward.push($scope.AllRouteNameForOutward_Data[i])
                    //}
                }

                $scope.DirectionNameOutward = directoutward;


                for (var i = 0; i < $scope.AllRouteNameForInward_Data.length; i++) {
                    //if ($scope.AllRouteNameForInward_Data[i].sims_transport_route_name == selectedText1) {
                    directinward.push($scope.AllRouteNameForInward_Data[i])
                    //}
                }

                $scope.DirectionNameInward = directinward;
                if ($scope.DirectionNameOutward.length > 0) {
                    $scope.edt2.sims_transport_route_direction = $scope.DirectionNameOutward[0].sims_transport_route_direction;
                    $scope.getstopbydirection1(route_code, aca_year);
                }
                else if ($scope.DirectionNameInward.length > 0) {
                    $scope.edt2.sims_transport_route_direction = $scope.DirectionNameInward[0].sims_transport_route_direction;
                    $scope.getstopbydirection1(route_code, aca_year);
                }

                $http.get(ENV.apiUrl + "api/common/getSitingCapacity?route_code=" + route_code + "&aca_year=" + aca_year).then(function (get_SitCapacity1) {
                    $scope.SitCapacity1 = get_SitCapacity1.data;
                    $scope.edtt = {
                        sims_transport_vehicle_seating_capacity: $scope.SitCapacity1[0].sims_transport_vehicle_seating_capacity,
                    }
                });

            }

            $scope.getroutebybuscode_2 = function () {

                $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getSecondRouteName?AcaYear=" + $scope.edt.sims_academic_year + "&Vehicle_Code=" + $scope.temp3.sims_transport_vehicle_code_2 + "&r_code=" + $scope.sims_transport_route_code.sims_transport_route_code + "&d_code=" + $scope.edt.sims_transport_route_direction).then(function (getRouteNameanddirection2) {
                    $scope.AllRouteNameForInward_Data = getRouteNameanddirection2.data;
                    $scope.AllRouteNameForOutward_Data = getRouteNameanddirection2.data;
                    console.log("AllRouteNameForInward_Data", $scope.AllRouteNameForInward_Data);
                    console.log("AllRouteNameForOutward_Data", $scope.AllRouteNameForOutward_Data);
                    $scope.edt2 = { sims_transport_route_code: $scope.AllRouteNameForInward_Data[0].sims_transport_route_code }
                    $scope.getdirectionfor2($scope.edt2.sims_transport_route_code, $scope.edt.sims_academic_year)
                    $scope.getstopbydirection1($scope.edt2.sims_transport_route_code, $scope.edt.sims_academic_year)
                });

                for (var i = 0; i < $scope.BusCode_Data.length; i++) {
                    if ($scope.BusCode_Data[i].v_Code == $scope.temp3.sims_transport_vehicle_code_2) {
                        $scope.safe_travel_bus_code_2 = $scope.BusCode_Data[i].sims_transport_vehicle_safe_travel_code;
                        break;
                    }
                }
                console.log("safe_travel_bus_code_2", $scope.safe_travel_bus_code_2);
            }

            //$scope.getstopbydirection1 = function (route_code, aca_year) {

            //    $scope.PickupDrop1 = [];
            //    $http.get(ENV.apiUrl + "api/common/getStopLanLogByDirection?route_code=" + route_code + "&aca_year=" + aca_year).then(function (get_PickupDrop1) {
            //        $scope.PickupDrop1 = get_PickupDrop1.data;
            //        $scope.edtt.sims_transport_drop_stop_code = $scope.PickupDrop1[0].sims_transport_stop_code;
            //        $scope.edtt.sims_transport_pickup_stop_code = $scope.PickupDrop1[0].sims_transport_stop_code;


            //        //$scope.edtt.sims_transport_pickup_stop_code = $scope.PickupDrop1[0].sims_transport_pickup_stop_code;
            //        //$scope.edtt.sims_transport_drop_stop_code = $scope.PickupDrop1[0].sims_transport_drop_stop_code;
            //    });
            //}

            //$scope.getlangitude1 = function (route_code, year, route_stop) {
            //    $http.get(ENV.apiUrl + "api/TransRouteStudDetails/getLanLogByRouteStop?route_code=" + route_code + "&aca_year=" + year + "&route_stop=" + route_stop).then(function (get_PickupLann_Data1) {
            //        $scope.PickupLanLog1 = get_PickupLann_Data1.data;
            //        for (var i = 0; i < $scope.PickupLanLog1.length; i++) {
            //            $scope.tempp = {
            //                sims_transport_route_student_stop_lat: $scope.PickupLanLog1[i].sims_transport_route_student_stop_lat,
            //                sims_transport_route_student_stop_long: $scope.PickupLanLog1[i].sims_transport_route_student_stop_long,
            //            };
            //        }
            //    });
            //}

            //$scope.getlangitudedropstop1 = function (route_code, year, route_stop) {
            //    $http.get(ENV.apiUrl + "api/TransRouteStudDetails/getLanLogByRouteStop?route_code=" + route_code + "&aca_year=" + year + "&route_stop=" + route_stop).then(function (get_PickupLann_Data1) {
            //        $scope.PickupLanLog1 = get_PickupLann_Data1.data;
            //        for (var i = 0; i < $scope.PickupLanLog1.length; i++) {
            //            $scope.edtt1 = {
            //                sims_transport_stop_lat_drop: $scope.PickupLanLog1[i].sims_transport_route_student_stop_lat,
            //                sims_transport_stop_long_drop: $scope.PickupLanLog1[i].sims_transport_route_student_stop_long,
            //            };
            //        }

            //    });
            //}

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);



                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.filteredTodos1 = [], $scope.currentPage1 = 1, $scope.numPerPage1 = 10, $scope.maxSize1 = 10;

            $scope.makeTodos1 = function () {
                var rem1 = parseInt($scope.totalItems1 % $scope.numPerPage1);
                if (rem1 == '0') {
                    $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1);
                }
                else {
                    $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1) + 1;
                }
                var begin1 = (($scope.currentPage1 - 1) * $scope.numPerPage1);
                var end1 = parseInt(begin1) + parseInt($scope.numPerPage1);



                $scope.filteredTodos1 = $scope.todos1.slice(begin1, end1);
            };

            $scope.cancel = function () {

                $scope.enroll_number1 = [];
                $scope.enroll_number = [];
                $scope.common_user = [];
                $scope.common_user1 = [];
                $scope.saveStudent = false;
                $scope.saveEmployee = false;
                $scope.student_grid_record = false;
                $scope.employee_grid_record = false;
                $scope.row1 = '';
                $scope.tempp = [];
                //$scope.edt = [];
                //$scope.temp = '';
                $scope.edt1 = [];
                $scope.display = true;
                $scope.edt = {};
                $scope.edt['status'] = false;
                $scope.inward = false;
                $scope.outward = false;
                $scope.inward1 = true;
                $scope.outward1 = true;

                $scope.edt = {
                    //sims_academic_year: '',
                    sims_transport_route_direction: '',
                    sims_transport_pickup_stop_code: '',
                    sims_transport_drop_stop_code: ''
                }
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllAcademicYear").then(function (get_AllAcademicYear) {

                    $scope.ActiveYear = get_AllAcademicYear.data;
                    $scope.edt = { sims_academic_year: $scope.ActiveYear[0].sims_academic_year };
                    $scope.getroutecodename($scope.edt.sims_academic_year);


                });

                $scope.info.sims_enroll_number = '';
                $scope.info.sims_parent_login_code = '';

                $scope.sims_transport_route_code = { 'sims_transport_route_code': '' }

                $scope.temp = {
                    //sims_cur_code: '',
                    //sims_academic_year: '',
                    sims_grade_code: '',
                    sims_section_code: '',
                    sims_transport_route_student_stop_lat: '',
                    sims_transport_route_student_stop_long: '',
                }

                $scope.tempp = {
                    sims_transport_vehicle_seating_capacity: '',
                    sims_transport_route_student_stop_lat: '',
                    sims_transport_route_student_stop_long: ''
                }

                $scope.edt1 = {
                    sims_transport_stop_lat_drop: '',
                    sims_transport_stop_long_drop: ''
                }

                $scope.edt2 = {
                    sims_transport_route_code: '',
                    sims_transport_route_direction: ''
                }

                $scope.edtt = {
                    sims_transport_vehicle_seating_capacity: '',
                    sims_transport_pickup_stop_code: '',
                    sims_transport_drop_stop_code: ''
                }

                $scope.edtt1 = {
                    sims_transport_stop_lat_drop: '',
                    sims_transport_stop_long_drop: ''
                }
                $scope.Bcode.v_Code = '';
                $scope.SelectsecondBus();

            }

            var sims_student_enrolls = [];

            $http.get(ENV.apiUrl + "api/TransRouteStudDetails/getBusShiftDetailsSafeTravelAPI").then(function (res) {
                $scope.shiftDetails = JSON.parse(res.data);
                console.log("shiftDetails", $scope.shiftDetails);
                console.log("shiftDetails parse", JSON.parse($scope.shiftDetails));
            });

            $http.get(ENV.apiUrl + "api/TransRouteStudDetails/getPickPointDetailsSafeTravelAPI").then(function (res) {
                $scope.pickPointDeatils = JSON.parse(res.data);
                console.log("pickPointDeatils", $scope.pickPointDeatils);
                $scope.pick_point_id = $scope.pickPointDeatils.data[0].loc_id;
                $scope.drop_point_id = $scope.pickPointDeatils.data[0].loc_id;
            });

            $scope.Save = function () {
                debugger

                var apiDataArray = [];

                data1 = [];
                if ($scope.enroll_number.length > 0) {
                    for (var i = 0; i < $scope.enroll_number.length; i++) {
                        if (route_code_new != undefined) {
                            if ($scope.edt.sims_transport_route_direction == '03') {
                                var data = {
                                    sims_transport_enroll_number: $scope.enroll_number[i].s_enroll_no,

                                    sims_cur_code: $scope.enroll_number[i].s_cur_code,
                                    sims_grade_code: $scope.enroll_number[i].grade_code,
                                    sims_section_code: $scope.enroll_number[i].section_code,

                                    sims_academic_year: $scope.edt.sims_academic_year,
                                    //sims_transport_route_code: $scope.route_code_new,
                                    sims_transport_route_code: route_code_new,
                                    sims_transport_route_direction: $scope.edt.sims_transport_route_direction,
                                    sims_transport_vehicle_seating_capacity: $scope.tempp.sims_transport_vehicle_seating_capacity,
                                    sims_transport_effective_from: $scope.dt.sims_transport_effective_from,
                                    sims_transport_effective_upto: $scope.dt.sims_transport_effective_upto,

                                    sims_transport_pickup_stop_code: $scope.edt.sims_transport_pickup_stop_code,
                                    sims_transport_route_student_stop_lat: $scope.templat.sims_transport_route_student_stop_lat,
                                    sims_transport_route_student_stop_long: $scope.templat.sims_transport_route_student_stop_long,

                                    sims_transport_drop_stop_code: $scope.edt.sims_transport_drop_stop_code,
                                    sims_transport_stop_lat_drop: $scope.edt1.sims_transport_stop_lat_drop,
                                    sims_transport_stop_long_drop: $scope.edt1.sims_transport_stop_long_drop,

                                    opr: 'R'
                                }
                            }

                            else if ($scope.edt.sims_transport_route_direction == '01') {

                                var data = {

                                    sims_transport_enroll_number: $scope.enroll_number[i].s_enroll_no,

                                    sims_cur_code: $scope.enroll_number[i].s_cur_code,
                                    sims_grade_code: $scope.enroll_number[i].grade_code,
                                    sims_section_code: $scope.enroll_number[i].section_code,

                                    sims_academic_year: $scope.edt.sims_academic_year,
                                    //sims_transport_route_code: $scope.route_code_new.sims_transport_route_code,
                                    sims_transport_route_code: route_code_new,
                                    sims_transport_route_direction: $scope.edt.sims_transport_route_direction,
                                    sims_transport_vehicle_seating_capacity: $scope.tempp.sims_transport_vehicle_seating_capacity,
                                    sims_transport_effective_from: $scope.dt.sims_transport_effective_from,
                                    sims_transport_effective_upto: $scope.dt.sims_transport_effective_upto,

                                    sims_transport_pickup_stop_code: $scope.edt.sims_transport_pickup_stop_code,
                                    sims_transport_route_student_stop_lat: $scope.templat.sims_transport_route_student_stop_lat,
                                    sims_transport_route_student_stop_long: $scope.templat.sims_transport_route_student_stop_long,


                                    //sims_transport_route_code1: $scope.edt2.sims_transport_route_code,
                                    //sims_transport_route_direction: $scope.edt2.sims_transport_route_direction,
                                    //sims_transport_vehicle_seating_capacity: $scope.edtt.sims_transport_vehicle_seating_capacity,

                                    //sims_transport_pickup_stop_code: $scope.edtt.sims_transport_pickup_stop_code,
                                    //sims_transport_route_student_stop_lat: $scope.tempp.sims_transport_route_student_stop_lat,
                                    //sims_transport_route_student_stop_long: $scope.tempp.sims_transport_route_student_stop_long,

                                    //sims_transport_drop_stop_code: $scope.edtt.sims_transport_drop_stop_code,
                                    //sims_transport_stop_lat_drop: $scope.edtt1.sims_transport_stop_lat_drop,
                                    //sims_transport_stop_long_drop: $scope.edtt1.sims_transport_stop_long_drop,

                                    opr: 'R'
                                }

                            }

                            else if ($scope.edt.sims_transport_route_direction == '02') {


                                var data = {
                                    sims_transport_enroll_number: $scope.enroll_number[i].s_enroll_no,

                                    sims_cur_code: $scope.enroll_number[i].s_cur_code,
                                    sims_grade_code: $scope.enroll_number[i].grade_code,
                                    sims_section_code: $scope.enroll_number[i].section_code,

                                    sims_academic_year: $scope.edt.sims_academic_year,
                                    //sims_transport_route_code: $scope.route_code_new,
                                    sims_transport_route_code: route_code_new,
                                    sims_transport_route_direction: $scope.edt.sims_transport_route_direction,
                                    sims_transport_vehicle_seating_capacity: $scope.tempp.sims_transport_vehicle_seating_capacity,
                                    sims_transport_effective_from: $scope.dt.sims_transport_effective_from,
                                    sims_transport_effective_upto: $scope.dt.sims_transport_effective_upto,

                                    sims_transport_drop_stop_code: $scope.edt.sims_transport_drop_stop_code,
                                    sims_transport_stop_lat_drop: $scope.edt1.sims_transport_stop_lat_drop,
                                    sims_transport_stop_long_drop: $scope.edt1.sims_transport_stop_long_drop,

                                    //sims_transport_route_code: $scope.edt2.sims_transport_route_code,
                                    //sims_transport_route_direction: $scope.edt2.sims_transport_route_direction,
                                    //sims_transport_vehicle_seating_capacity: $scope.edtt.sims_transport_vehicle_seating_capacity,

                                    //sims_transport_pickup_stop_code: $scope.edtt.sims_transport_pickup_stop_code,
                                    //sims_transport_route_student_stop_lat: $scope.tempp.sims_transport_route_student_stop_lat,
                                    //sims_transport_route_student_stop_long: $scope.tempp.sims_transport_route_student_stop_long,

                                    opr: 'R'
                                }
                            }
                            data1.push(data);

                            if (data != null) {
                                // this following loop for if user select 2nd route and bus then it will go inside other wise not going
                                if (itemset.checked == true) {
                                    for (var i = 0; i < $scope.enroll_number.length; i++) {
                                        if ($scope.edt2.sims_transport_route_direction == '01' || $scope.edt2.sims_transport_route_direction == '02') {

                                            var data = {

                                                sims_transport_enroll_number: $scope.enroll_number[i].s_enroll_no,
                                                sims_cur_code: $scope.enroll_number[i].s_cur_code,
                                                sims_grade_code: $scope.enroll_number[i].grade_code,
                                                sims_section_code: $scope.enroll_number[i].section_code,
                                                sims_academic_year: $scope.edt.sims_academic_year,
                                                sims_transport_effective_from: $scope.dt.sims_transport_effective_from,
                                                sims_transport_effective_upto: $scope.dt.sims_transport_effective_upto,

                                                sims_transport_route_code: $scope.edt2.sims_transport_route_code,
                                                sims_transport_route_direction: $scope.edt2.sims_transport_route_direction,

                                                sims_transport_vehicle_seating_capacity: $scope.edtt.sims_transport_vehicle_seating_capacity,
                                                sims_transport_pickup_stop_code: $scope.edtt.sims_transport_pickup_stop_code,

                                                sims_transport_route_student_stop_lat: $scope.tempp.sims_transport_route_student_stop_lat,
                                                sims_transport_route_student_stop_long: $scope.tempp.sims_transport_route_student_stop_long,

                                                sims_transport_drop_stop_code: $scope.edtt.sims_transport_drop_stop_code,
                                                sims_transport_stop_lat_drop: $scope.edtt1.sims_transport_stop_lat_drop,
                                                sims_transport_stop_long_drop: $scope.edtt1.sims_transport_stop_long_drop,

                                                opr: 'R'
                                            }

                                        }
                                    }
                                    data1.push(data);
                                }


                            }
                            else {
                                swal('', 'Please select route,direction,stop then try to apply...');
                            }

                        }

                        else {
                            swal('', 'Please select route,direction,stop then try to apply...');
                        }
                    }
                        $scope.display = true;
                        $scope.inward = false;
                        $scope.outward = false;
                        debugger;
                        if ($scope.edt.sims_transport_route_direction == '03') {
                            $scope.safe_travel_bus_code_2 = $scope.safe_travel_bus_code_1;                            
                        }
                        if ($scope.safe_travel_bus_code_2 == undefined || $scope.safe_travel_bus_code_2 == "" || $scope.safe_travel_bus_code_2 == null) {
                            $scope.safe_travel_bus_code_2 = $scope.safe_travel_bus_code_1;
                        }

                        if ($scope.edt.status == true) {
                            $scope.edt.sims_transport_route_direction = '03';
                        }

                        angular.forEach($scope.shiftDetails.p.tp_points, function (value, key) {
                            if ($scope.safe_travel_bus_code_1 == value.brno_id && value.lbl_trip.toLowerCase() == "pickup") {                                
                                $scope.pick_shift_id = value.brnoshift_id;
                            }
                            
                            //if ($scope.safe_travel_bus_code_2 == value.brno_id && value.lbl_trip.toLowerCase() == "pickup") {
                            //    $scope.pick_shift_id = value.brnoshift_id;
                            //}
                            if ($scope.safe_travel_bus_code_2 == undefined || $scope.safe_travel_bus_code_2 == "" || $scope.safe_travel_bus_code_2 == null) {
                                if ($scope.safe_travel_bus_code_1 == value.brno_id && value.lbl_trip.toLowerCase() == "drop") {
                                    $scope.drop_shift_id = value.brnoshift_id;
                                }
                            }
                            else {
                                if ($scope.safe_travel_bus_code_2 == value.brno_id && value.lbl_trip.toLowerCase() == "drop") {
                                    $scope.drop_shift_id = value.brnoshift_id;
                                }
                            }
                        });
                       
                        console.log("pick_shift_id", $scope.pick_shift_id);
                        console.log("drop_shift_id", $scope.drop_shift_id);
                        console.log("enroll_number", $scope.enroll_number);
                        for(var k = 0; k < $scope.enroll_number.length; k++) {
                            var obj = {
                                'enroll_no': $scope.enroll_number[k].s_enroll_no,
                                'adm_date': $scope.enroll_number[k].sims_student_date,
                                'first_name': $scope.enroll_number[k].name,
                                // "last_name":"Albert", 
                                'card_no': $scope.enroll_number[k].rfid_card,
                                'gender': $scope.enroll_number[k].sims_student_gender,
                                'class_id': $scope.enroll_number[k].sims_transport_grade_code,
                                'section_id': $scope.enroll_number[k].sims_transport_section_code,
                                'roll_no': $scope.enroll_number[k].s_enroll_no,
                                'dob': $scope.enroll_number[k].sims_student_dob,
                                //"blood_group":"A+",
                                'nationality': $scope.enroll_number[k].nationality,
                                'address1': $scope.enroll_number[k].search_parent_Summary_address,
                                //"address2":"Nerul",   
                                'father_name': $scope.enroll_number[k].s_parent_name,
                                'father_mob': $scope.enroll_number[k].sims_parent_father_mobile,
                                'mother_name': $scope.enroll_number[k].mother_name,
                                'mother_mob': $scope.enroll_number[k].sims_parent_mother_mobile,
                                'area': $scope.enroll_number[k].sims_parent_father_area_number,
                                //"alternate_mob":"0123456789", 
                                'is_transport': "1",
                                //"sts_transfee":"1", 
                                'email': $scope.enroll_number[k].mail_id,
                                'trip_type': parseInt($scope.edt.sims_transport_route_direction),
                                'pick_bs_no_id': $scope.safe_travel_bus_code_1,
                                'drop_bs_no_id': $scope.safe_travel_bus_code_2,
                                'pick_shift_id': $scope.pick_shift_id,
                                'drop_shift_id': $scope.drop_shift_id,
                                'pick_point_id': $scope.pick_point_id,
                                'drop_point_id': $scope.drop_point_id,
                                'status': '1'
                                //"photo":"y"      
                            };
                            apiDataArray.push(obj);
                        }

                        //$scope.edt = {
                        //    sims_academic_year: $scope.ActiveYear[0].sims_academic_year,
                        //    sims_transport_route_direction: '',
                        //    sims_transport_pickup_stop_code: '',
                        //    sims_transport_drop_stop_code: ''
                        //}

                        //$scope.sims_transport_route_code.sims_transport_route_code = '';
                        //$scope.temp = {
                        //    sims_transport_route_student_stop_lat: '',
                        //    sims_transport_route_student_stop_long: ''
                        //}
                        //$scope.tempp = {
                        //    sims_transport_vehicle_seating_capacity: '',
                        //    sims_transport_route_student_stop_lat: '',
                        //    sims_transport_route_student_stop_long: ''
                        //}
                        //$scope.edt1 = {

                        //    sims_transport_stop_lat_drop: '',
                        //    sims_transport_stop_long_drop: ''
                        //}
                        //$scope.edt2 = {

                        //    sims_transport_route_code: '',
                        //    sims_transport_route_direction: ''
                        //}
                        //$scope.edtt = {

                        //    sims_transport_vehicle_seating_capacity: '',
                        //    sims_transport_pickup_stop_code: '',
                        //    sims_transport_drop_stop_code: ''
                        //}
                        //$scope.edtt1 = {
                        //    sims_transport_stop_lat_drop: '',
                        //    sims_transport_stop_long_drop: ''
                        //}

                        $scope.student_grid_record = false;
                        $scope.searchtableEmployee = false;
                        $scope.selectsecond = false;
                        $scope.secondbus = false;
                        //$scope.enroll_number = [];
                        sims_student_enrolls = [];
                        $scope.common_user = [];
                        $scope.common_user1 = [];
                        data = [];

                    
                    if (data1.length > 0) {
                        $http.post(ENV.apiUrl + "api/TransRouteStudDetails/CUDTransportRouteStudent", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            $scope.grid = false;
                            debugger
                            if ($scope.msg1.status == true) {
                                swal({ title: "Alert", text: $scope.msg1.strMessage, width: 450, height: 250 });
                                //$state.go('main.traahg');
                                //$scope.cancel();
                                //$scope.SelectsecondBus_uncheck();
                            }
                            else {
                                swal({ title: "Alert", text: $scope.msg1.strMessage, width: 450, height: 250 });                                                            
                            }

                            $http.post(ENV.apiUrl + "api/TransRouteStudDetails/SafeTravelAPI", apiDataArray).then(function (msg) {
                                $scope.resmsg = JSON.parse(msg.data);
                                console.log("travel post", msg, '---- ', $scope.resmsg);
                                console.log("safe travel post", $scope.resmsg);
                                if ($scope.resmsg.res_code == '1') {
                                    console.log(" Saved in my safe travel");

                                    $state.go('main.traahg');
                                    $scope.cancel();
                                    $scope.SelectsecondBus_uncheck();
                                }
                                else {
                                    console.log($scope.resmsg.message);
                                    swal({ title: "Alert", text: $scope.resmsg.message, width: 450, height: 250 });                                    
                                    $scope.cancel();
                                    $scope.SelectsecondBus_uncheck();
                                }

                            });

                        });
                    }
                    else {
                        swal('', 'Select Atleast One Student To Apply Transport');
                    }
                }
                else {
                    swal('', 'Select Atleast One Student To Apply Transport');
                }

            }

            $scope.SaveEmployee = function () {

                data1 = [];
                if ($scope.common_user.length > 0) {

                    for (var i = 0; i < $scope.common_user.length; i++) {
                        if ($scope.route_code_new != undefined) {
                            if ($scope.edt.sims_transport_route_direction == '03') {
                                var data = {
                                    sims_transport_em_number: $scope.common_user[i].comn_user_name,
                                    sims_academic_year: $scope.edt.sims_academic_year,

                                    sims_transport_route_code: $scope.route_code_new,
                                    sims_transport_route_direction: $scope.edt.sims_transport_route_direction,
                                    sims_transport_vehicle_seating_capacity: $scope.tempp.sims_transport_vehicle_seating_capacity,
                                    sims_transport_effective_from: $scope.dt.sims_transport_effective_from,
                                    sims_transport_effective_upto: $scope.dt.sims_transport_effective_upto,
                                    sims_transport_pickup_stop_code: $scope.edt.sims_transport_pickup_stop_code,
                                    sims_transport_route_student_stop_lat: $scope.templat.sims_transport_route_student_stop_lat,
                                    sims_transport_route_student_stop_long: $scope.templat.sims_transport_route_student_stop_long,
                                    sims_transport_drop_stop_code: $scope.edt.sims_transport_drop_stop_code,
                                    sims_transport_stop_lat_drop: $scope.edt1.sims_transport_stop_lat_drop,
                                    sims_transport_stop_long_drop: $scope.edt1.sims_transport_stop_long_drop,
                                    opr: 'R'
                                }
                            }

                            else if ($scope.edt.sims_transport_route_direction == '01') {

                                var data = {
                                    sims_transport_em_number: $scope.common_user[i].comn_user_name,
                                    sims_academic_year: $scope.edt.sims_academic_year,
                                    sims_transport_route_code: $scope.route_code_new,
                                    sims_transport_route_direction: $scope.edt.sims_transport_route_direction,
                                    sims_transport_vehicle_seating_capacity: $scope.tempp.sims_transport_vehicle_seating_capacity,
                                    sims_transport_effective_from: $scope.dt.sims_transport_effective_from,
                                    sims_transport_effective_upto: $scope.dt.sims_transport_effective_upto,
                                    sims_transport_pickup_stop_code: $scope.edt.sims_transport_pickup_stop_code,
                                    sims_transport_route_student_stop_lat: $scope.templat.sims_transport_route_student_stop_lat,
                                    sims_transport_route_student_stop_long: $scope.templat.sims_transport_route_student_stop_long,
                                    opr: 'R'
                                }

                            }

                            else if ($scope.edt.sims_transport_route_direction == '02') {

                                var data = {
                                    sims_transport_em_number: $scope.common_user[i].comn_user_name,
                                    sims_academic_year: $scope.edt.sims_academic_year,
                                    sims_transport_route_code: $scope.route_code_new,
                                    sims_transport_route_direction: $scope.edt.sims_transport_route_direction,
                                    sims_transport_vehicle_seating_capacity: $scope.tempp.sims_transport_vehicle_seating_capacity,
                                    sims_transport_effective_from: $scope.dt.sims_transport_effective_from,
                                    sims_transport_effective_upto: $scope.dt.sims_transport_effective_upto,
                                    sims_transport_drop_stop_code: $scope.edt.sims_transport_drop_stop_code,
                                    sims_transport_stop_lat_drop: $scope.edt1.sims_transport_stop_lat_drop,
                                    sims_transport_stop_long_drop: $scope.edt1.sims_transport_stop_long_drop,
                                    opr: 'R'
                                }
                            }

                            data1.push(data);
                            if (itemset.checked == true) {
                                for (var i = 0; i < $scope.common_user.length; i++) {
                                    if ($scope.edt2.sims_transport_route_direction == '01' || $scope.edt2.sims_transport_route_direction == '02') {

                                        var data = {
                                            sims_transport_em_number: $scope.common_user[i].comn_user_name,
                                            sims_academic_year: $scope.edt.sims_academic_year,
                                            sims_transport_effective_from: $scope.dt.sims_transport_effective_from,
                                            sims_transport_effective_upto: $scope.dt.sims_transport_effective_upto,
                                            sims_transport_route_code: $scope.edt2.sims_transport_route_code,
                                            sims_transport_route_direction: $scope.edt2.sims_transport_route_direction,
                                            sims_transport_vehicle_seating_capacity: $scope.edtt.sims_transport_vehicle_seating_capacity,
                                            sims_transport_pickup_stop_code: $scope.edtt.sims_transport_pickup_stop_code,
                                            sims_transport_route_student_stop_lat: $scope.tempp.sims_transport_route_student_stop_lat,
                                            sims_transport_route_student_stop_long: $scope.tempp.sims_transport_route_student_stop_long,
                                            sims_transport_drop_stop_code: $scope.edtt.sims_transport_drop_stop_code,
                                            sims_transport_stop_lat_drop: $scope.edtt1.sims_transport_stop_lat_drop,
                                            sims_transport_stop_long_drop: $scope.edtt1.sims_transport_stop_long_drop,
                                            opr: 'R'
                                        }

                                    }
                                }
                                data1.push(data);
                            }
                            $http.post(ENV.apiUrl + "api/TransRouteStudDetails/CUDTransportRouteEmployee", data1).then(function (msg) {
                                $scope.msg1 = msg.data;
                                $scope.grid = false;


                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 });
                                }
                                else {
                                    swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 });
                                }

                            });
                        }
                        else {
                            swal('', 'Please select route,direction,stop then try to apply...');
                        }
                    }



                    $scope.display = true;
                    $scope.inward = false;
                    $scope.outward = false;
                    //$scope.edt = {
                    //    sims_academic_year: '',
                    //    sims_transport_route_direction: '',
                    //    sims_transport_pickup_stop_code: '',
                    //    sims_transport_drop_stop_code: ''
                    //}
                    //$scope.sims_transport_route_code.sims_transport_route_code = '';

                    //$scope.temp = {


                    //    sims_transport_route_student_stop_lat: '',
                    //    sims_transport_route_student_stop_long: ''
                    //}
                    //$scope.tempp = {
                    //    sims_transport_vehicle_seating_capacity: '',
                    //    sims_transport_route_student_stop_lat: '',
                    //    sims_transport_route_student_stop_long: ''
                    //}
                    //$scope.edt1 = {

                    //    sims_transport_stop_lat_drop: '',
                    //    sims_transport_stop_long_drop: ''
                    //}
                    //$scope.edt2 = {

                    //    sims_transport_route_code: '',
                    //    sims_transport_route_direction: ''
                    //}
                    //$scope.edtt = {

                    //    sims_transport_vehicle_seating_capacity: '',
                    //    sims_transport_pickup_stop_code: '',
                    //    sims_transport_drop_stop_code: ''
                    //}
                    //$scope.edtt1 = {
                    //    sims_transport_stop_lat_drop: '',
                    //    sims_transport_stop_long_drop: ''
                    //}
                    $scope.student_grid_record = false;
                    $scope.searchtableEmployee = false;
                    $scope.selectsecond = false;
                    $scope.enroll_number = [];
                    sims_student_enrolls = [];
                    $scope.common_user = [];
                    $scope.common_user1 = [];
                    data = '';
                }
                else {
                    swal('', 'Select Atleast One Employee To Apply Transport');
                }
            }

            $scope.SearchStudentWindow = function () {
                $scope.searchtable = false;
                $scope.searchtableEmployee = false;
                $scope.student = '';
                $('#MyModal').modal('show');

                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {

                    $scope.curriculum = AllCurr.data;

                    $scope.temp = { s_cur_code: $scope.curriculum[0].sims_cur_code };
                    $scope.getAccYear($scope.temp.s_cur_code);
                });

                $scope.getAccYear = function (curCode) {

                    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.temp.s_cur_code).then(function (Acyear) {
                        $scope.Acc_year = Acyear.data;
                        $scope.temp = {
                            s_cur_code: $scope.temp.s_cur_code,//$scope.curriculum[0].sims_cur_code,
                            sims_academic_year: $scope.Acc_year[0].sims_academic_year
                        }
                        $scope.getGrade();
                    });

                }

                $scope.getGrade = function (curCode, accYear) {
                    $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.s_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                        $scope.Grade_code = Gradecode.data;
                    });
                }

                $scope.getSection = function (curCode, gradeCode, accYear) {
                    $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.temp.s_cur_code + "&grade_code=" + $scope.temp.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                        $scope.Section_code = Sectioncode.data;
                    });
                }
            }

            $scope.Student_Details = function () {

                $scope.searchtable = false;
                $scope.searchtableEmployee = false;

            }

            $scope.emp_information = function () {
                $scope.searchtable = false;
                $scope.searchtableEmployee = false;

            }
            //--------------------For_Student_Search------------------//

            $scope.SearchSudent = function (cur_code, academic_year) {

                if ((cur_code != undefined && cur_code != '') &&
                   (academic_year != undefined && academic_year != '')) {
                    //$scope.enroll_number = [];
                    main = document.getElementById('mainchk1');
                    if (main.checked == true) {
                        main.checked = false;
                    }

                    $scope.searchtable = false;
                    $scope.searchtableEmployee = false;
                    $scope.busy = true;
                    var data = $scope.temp;
                    $scope.pagesize = '10';
                    $scope.numPerPage = 10;
                    $scope.currentPage = 1;

                    $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent1_ahgs?data=" + JSON.stringify(data)).then(function (Allstudent) {
                        $scope.searchtable = true;
                        $scope.student = Allstudent.data;
                        if ($scope.student.length > 0) {
                            $scope.pager = true;

                            if ($scope.countData.length > 3) {
                                $scope.countData.splice(3, 1);
                                $scope.countData.push({ val: $scope.student.length, data: 'All' })
                            }
                            else {
                                $scope.countData.push({ val: $scope.student.length, data: 'All' })
                            }

                            $scope.totalItems = $scope.student.length;
                            $scope.todos = $scope.student;
                            $scope.makeTodos();
                            $scope.searchtable = true;
                            $scope.searchtableEmployee = false;
                            $scope.busy = false;
                            $scope.student_grid_record = true;
                            $scope.currentPage = 1;
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                            $scope.page_index = false;
                            $scope.pager = false;
                            $scope.busy = false;
                            $scope.filteredTodos = [];
                        }
                    });

                    //$http.get(ENV.apiUrl + "api/TransRouteStudDetails/GetAllStudentName?Accd=" + $scope.edt.sims_academic_year).then(function (GetAllTeacherName) {
                    //    debugger;
                    //    $scope.GetAllStudentName = GetAllTeacherName.data;
                    //});

                    //////$http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent1?data=" + JSON.stringify(data)).then(function (Allstudent) {
                    //////    $scope.student = Allstudent.data;
                    //////    if ($scope.student.length > 0) {
                    //////        $scope.totalItems = $scope.student.length;
                    //////        $scope.todos = $scope.student;
                    //////        $scope.makeTodos();
                    //////        $scope.searchtable = true;
                    //////        $scope.searchtableEmployee = false;
                    //////        $scope.busy = false;
                    //////        $scope.student_grid_record = true;
                    //////        console.log($scope.student);
                    //////    }
                    //////    else {
                    //////        swal({ title: "Alert", text: "Sorry, There is no record Found", width: 300, height: 200 });
                    //////        $scope.busy = false;
                    //////    }
                    //////});
                }
                else {
                    swal({ title: "Alert", text: "Please Select Curriculum and Academic year", width: 300, height: 200 });
                }


            }

            $scope.MultipleStudentSelect = function () {
                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].s_enroll_no);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].s_enroll_no);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }


            $scope.AddStud = function (student) {
                //$scope.tempdata = $scope.filteredTodos;
                $scope.student_grid_record = true;
                $scope.employee_grid_record = false;
                $scope.saveStudent = true;
                $scope.saveEmployee = false;
                debugger;


                //if (student != $scope.enroll_number.s_enroll_no) {
                for (var i = 0; i <= $scope.GetStudentName.length; i++) {
                    if ($scope.GetStudentName[i].sims_enroll_number == student) {

                        var t = $scope.GetStudentName[i]
                        t['s_enroll_no'] = $scope.GetStudentName[i].sims_enroll_number;
                        t['s_sname_in_english'] = $scope.GetStudentName[i].student_name;
                        t['s_cur_code'] = $scope.GetStudentName[i].sims_cur_code;
                        t['grade_code'] = $scope.GetStudentName[i].sims_grade_code;
                        t['section_code'] = $scope.GetStudentName[i].sims_section_code;
                        t['sims_student_dob'] = $scope.GetStudentName[i].sims_student_dob;
                        t['parent_name'] = $scope.GetStudentName[i].parent_name;
                        t['sims_parent_father_summary_address'] = $scope.GetStudentName[i].sims_parent_father_summary_address;
                        t['sims_parent_father_mobile'] = $scope.GetStudentName[i].sims_parent_father_mobile;
                        t['sims_parent_father_email'] = $scope.GetStudentName[i].sims_parent_father_email;
                        t['sims_parent_mother_mobile'] = $scope.GetStudentName[i].sims_parent_mother_mobile;
                        t['sims_parent_mother_email'] = $scope.GetStudentName[i].sims_parent_mother_email;
                        t['sims_parent_father_area_number'] = $scope.GetStudentName[i].sims_parent_father_area_number;
                        t['sims_student_date'] = $scope.GetStudentName[i].sims_student_date;
                        t['sims_student_transport_status'] = $scope.GetStudentName[i].sims_student_transport_status;
                        t['nationality'] = $scope.GetStudentName[i].nationality;
                        t['sims_student_gender'] = $scope.GetStudentName[i].sims_student_gender;
                        t['rfid_card'] = $scope.GetStudentName[i].rfid_card;
                        t['mother_name'] = $scope.GetStudentName[i].mother_name;
                        t['sims_transport_grade_code'] = $scope.GetStudentName[i].sims_transport_grade_code,
                        t['sims_transport_section_code'] = $scope.GetStudentName[i].sims_transport_section_code

                        $scope.GetStudentName[i].icon = 'fa fa-plus-circle';

                        var flag = 0;
                        for (var j = 0; j < $scope.enroll_number1.length; j++) {
                            if ($scope.enroll_number1[j].s_enroll_no == $scope.GetStudentName[i].sims_enroll_number) {
                                flag = 1;
                                break;
                            }
                        }
                        if (flag == 0) {
                            $scope.enroll_number1.push(t);
                            break;

                        }
                        if (flag == 1) {
                            swal('', 'All Ready Exists...');
                            break;

                        }
                        // $scope.enroll_number1.push(t);
                        //   break;
                    }
                }

                $scope.enroll_number = $scope.enroll_number1;

                //}

                //$scope.enroll_number1.push(student);

                //$scope.enroll_number = $scope.enroll_number1;

            }


            $scope.StudentEnrollNumber = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }
            $scope.enroll_number = [];
            $scope.enroll_number1 = [];
            $scope.tempdata = [];

            $scope.DataEnroll = function () {
                debugger;
                $scope.tempdata = $scope.filteredTodos;
                $scope.student_grid_record = true;
                $scope.employee_grid_record = false;
                $scope.saveStudent = true;
                $scope.saveEmployee = false;
                //$scope.enroll_number2 = [];
                for (var i = 0; i < $scope.tempdata.length; i++) {
                    var t = $scope.tempdata[i].s_enroll_no;
                    var v = document.getElementById(t);
                    if (v.checked == true) {
                        $scope.tempdata[i].disabled = false;
                        $scope.tempdata[i].icon = 'fa fa-plus-circle';

                        var flag = 0;
                        for (var j = 0; j < $scope.enroll_number1.length; j++) {
                            if ($scope.enroll_number1[j].s_enroll_no == $scope.tempdata[i].s_enroll_no) {
                                flag = 1;
                                break;
                            }
                        }
                        if (flag == 0) {
                            $scope.enroll_number1.push($scope.tempdata[i]);

                        }
                        $scope.row1 = '';
                    }
                    $('tr').removeClass("row_selected");

                }

                $scope.enroll_number = $scope.enroll_number1;

                $scope.enroll_number2 = $scope.enroll_number1;


                $('#MyModal').modal('hide');

            }

            $scope.newdata = [];
            $scope.flag = true;
            var dom = [];

            $scope.expand = function (roll, $event) {


                $scope.enrollrelatedrecords = [];

                $http.get(ENV.apiUrl + "api/TransRouteStudDetails/getenrollrelatedrecords?trans_Acayear=" + roll.sims_academic_year + "&trans_enroll=" + roll.s_enroll_no).then(function (getenrollrelatedrecords_data) {
                    $scope.enrollrelatedrecords = getenrollrelatedrecords_data.data;
                    //if ($scope.enrollrelatedrecords.length > 0) {
                    //    for (var i = 0; i < $scope.enrollrelatedrecords.length; i++) {
                    //        //$scope.enrollrelatedrecords[i].disabled = true;
                    //        //$scope.enroll_number.push($scope.enrollrelatedrecords[i]);
                    //    }
                    //}

                    //  $(dom).remove();
                    if ($scope.enrollrelatedrecords.length > 0) {
                        if ($scope.flag == true) {

                            for (var i = 0; i < $scope.enrollrelatedrecords.length; i++) {
                                roll.icon = "fa fa-minus-circle"
                                dom[i] = ($("<tr id='innerRow'><td class='details' colspan='12'>" +
                                    "<table class='inner-table table table-bordered table-condensed' cellpadding='5' cellspacing='0' style='width:100%'>" +
                                    "<tbody>" +
                                     "<tr style='background-color: #edefef;'><th class='semi-bold'>" + "Enroll No" + "</th><th class='semi-bold'>" + "Route Code" + "</th> <th class='semi-bold'>" + "Route Direction" + " </th><th class='semi-bold'>" + "Effective From" + "</th><th class='semi-bold'>" + "Effective Upto" + "</th>" +
                                   "</tr>" +
                                    "<tr><td>" + '<input disabled="disabled" ng-readonly="readonly" type="text" style="width:100px" class="form-control input-sm"  value=' + $scope.enrollrelatedrecords[i].sims_transport_enroll_number + '></input>' + "</td><td>" +
                                                 '<input disabled="disabled" type="text" style="width:100px" class="form-control input-sm" value=' + $scope.enrollrelatedrecords[i].sims_transport_route_code + '></input>' + "</td><td>" +
                                                 '<input disabled="disabled" type="text" style="width:100px" class="form-control input-sm"  value=' + $scope.enrollrelatedrecords[i].sims_transport_route_direction + '></input>' + "</td> <td>" +
                                                 '<input disabled="disabled" type="text" style="width:100px" class="form-control input-sm"  value=' + $scope.enrollrelatedrecords[i].sims_transport_effective_from + '></input>' + "</td><td>" +
                                                 '<input disabled="disabled" type="text" style="width:100px" class="form-control input-sm"  value=' + $scope.enrollrelatedrecords[i].sims_transport_effective_upto + '></input>' + "</td>" +


                                     //"<tr><td>" + '<input disabled="disabled" ng-readonly="readonly" type="text" style="width:70px" class="form-control input-sm"  value=' + $scope.enrollrelatedrecords[i].sims_transport_enroll_number + '>'
                                    + " </table></td></tr>"))
                                $($event.currentTarget).parents("tr").after(dom[i]);
                                $scope.flag = false;
                            }
                        }
                        else {

                            roll.icon = "fa fa-plus-circle";

                            for (var i = 0; i < $scope.enrollrelatedrecords.length; i++) {
                                $(dom[i]).remove();

                            }
                            $scope.flag = true;
                        }
                    }
                    else {
                        swal('', 'No Records Found For Transportation...');
                    }
                });

            };
            //--------------------For_Employee_Search------------------//

            $scope.SearchEmployee = function (comn_user_name, EmpName) {

                main = document.getElementById('mainchkemp');
                if (main.checked == true) {
                    main.checked = false;
                }
                $scope.searchtable = false;
                $scope.searchtableEmployee = false;
                $scope.busy = true;
                var data = $scope.temp2;

                $scope.pagesize1 = '10';
                $scope.numPerPage1 = 10;
                $scope.currentPage1 = 1;

                $http.get(ENV.apiUrl + "api/TransRouteStudDetails/getSearchEmployee?data=" + JSON.stringify(data)).then(function (Allemployee) {
                    $scope.Allemployee = Allemployee.data;
                    if ($scope.Allemployee.length > 0) {
                        $scope.pageremp = true;
                        if ($scope.countDataemp.length > 3) {
                            $scope.countDataemp.splice(3, 1);
                            $scope.countDataemp.push({ val: $scope.Allemployee.length, data: 'All' })
                        }
                        else {
                            $scope.countDataemp.push({ val: $scope.Allemployee.length, data: 'All' })
                        }

                        $scope.totalItems1 = $scope.Allemployee.length;
                        $scope.todos1 = $scope.Allemployee;
                        $scope.makeTodos1();
                        $scope.searchtableEmployee = true;
                        $scope.searchtable = false;
                        $scope.busy = false;


                    }
                    else {
                        swal({ title: "Alert", text: "Sorry, There Is no record Found", width: 300, height: 200 });
                        $scope.busy = false;
                        $scope.pageremp = false;
                    }
                });
            }

            $scope.MultipleEmployeeSelect = function () {
                main = document.getElementById('mainchkemp');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                        var v = document.getElementById($scope.filteredTodos1[i].comn_user_name);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                        var v = document.getElementById($scope.filteredTodos1[i].comn_user_name);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.EmployeeNumber = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchkemp');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.common_user = [];
            $scope.common_user1 = [];
            $scope.common_user2 = [];
            $scope.DataCommonUser = function () {

                $scope.student_grid_record = false;
                $scope.employee_grid_record = true;
                $scope.saveStudent = false;
                $scope.saveEmployee = true;

                for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                    var t = $scope.filteredTodos1[i].comn_user_name;
                    //var t = document.getElementById($scope.filteredTodos1[i].comn_user_name);
                    var v = document.getElementById(t);
                    if (v.checked == true) {
                        $scope.filteredTodos1[i].disabled = false;
                        $scope.filteredTodos1[i].icon = 'fa fa-plus-circle';

                        $scope.common_user1.push($scope.filteredTodos1[i]);
                    }
                    $('tr').removeClass("row_selected");
                }
                //for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                //    var v = document.getElementById($scope.filteredTodos1[i].comn_user_name);
                //    if (v.checked == true) {

                //        $scope.common_user1.push($scope.filteredTodos1[i]);
                //    }
                //}
                $scope.common_user = $scope.common_user1;

                $scope.common_user2 = $scope.common_user1;

                $('#MyModal').modal('hide');
            }

            $scope.flag = true;

            var dom1 = [];

            $scope.expand1 = function (user, $event) {
                $scope.enrollrelatedEmployeerecords = [];

                $http.get(ENV.apiUrl + "api/TransRouteStudDetails/getenrollrelatedEmployeerecords?trans_Acayear=" + $scope.edt.sims_academic_year + "&trans_enroll=" + user.comn_user_name).then(function (getenrollrelatedEmployeerecords_data) {
                    $scope.enrollrelatedEmployeerecords = getenrollrelatedEmployeerecords_data.data;


                    //  $(dom).remove();
                    if ($scope.enrollrelatedEmployeerecords.length > 0) {
                        if ($scope.flag == true) {

                            for (var i = 0; i < $scope.enrollrelatedEmployeerecords.length; i++) {
                                user.icon = "fa fa-minus-circle"
                                dom1[i] = ($("<tr id='innerRow'><td class='details' colspan='12'>" +
                                    "<table class='inner-table table table-bordered table-condensed' cellpadding='5' cellspacing='0' style='width:100%'>" +
                                    "<tbody>" +
                                     "<tr style='background-color: #edefef'><th class='semi-bold'>" + "Enroll No" + "</th><th class='semi-bold'>" + "Route Code" + "</th> <th class='semi-bold'>" + "Route Direction" + " </th><th class='semi-bold'>" + "Effective From" + "</th><th class='semi-bold'>" + "Effective Upto" + "</th>" +
                                   "</tr>" +
                                    "<tr><td>" + '<input disabled="disabled" ng-readonly="readonly" type="text" style="width:100px" class="form-control input-sm"  value=' + $scope.enrollrelatedEmployeerecords[i].sims_transport_em_number + '></input>' + "</td><td>" +
                                                 '<input disabled="disabled" type="text" style="width:100px" class="form-control input-sm" value=' + $scope.enrollrelatedEmployeerecords[i].sims_transport_route_code + '></input>' + "</td><td>" +
                                                 '<input disabled="disabled" type="text" style="width:100px" class="form-control input-sm"  value=' + $scope.enrollrelatedEmployeerecords[i].sims_transport_route_direction + '></input>' + "</td> <td>" +
                                                 '<input disabled="disabled" type="text" style="width:100px" class="form-control input-sm"  value=' + $scope.enrollrelatedEmployeerecords[i].sims_transport_effective_from + '></input>' + "</td><td>" +
                                                 '<input disabled="disabled" type="text" style="width:100px" class="form-control input-sm"  value=' + $scope.enrollrelatedEmployeerecords[i].sims_transport_effective_upto + '></input>' + "</td>" +


                                     //"<tr><td>" + '<input disabled="disabled" ng-readonly="readonly" type="text" style="width:70px" class="form-control input-sm"  value=' + $scope.enrollrelatedrecords[i].sims_transport_enroll_number + '>'
                                    + " </table></td></tr>"))
                                $($event.currentTarget).parents("tr").after(dom1[i]);
                                $scope.flag = false;
                            }
                        }
                        else {

                            //$('#innerRow').css({ 'display': 'none' });
                            user.icon = "fa fa-plus-circle";

                            for (var i = 0; i < $scope.enrollrelatedEmployeerecords.length; i++) {
                                $(dom1[i]).remove();

                            }
                            $scope.flag = true;
                        }
                    }
                    else {
                        swal({ title: "Alert", text: 'No Records found for Transportation...', width: 450, height: 250 });
                    }
                });

            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.student, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.student;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                   item.s_sname_in_english.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                   item.s_class.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                   item.s_enroll_no == toSearch) ? true : false;


                // return (item.s_enroll_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.searched1 = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil1(i, toSearch);
                });
            };

            $scope.search1 = function () {
                $scope.todos1 = $scope.searched1($scope.Allemployee, $scope.searchText);
                $scope.totalItems = $scope.todos1.length;
                $scope.currentPage1 = '1';
                if ($scope.searchText == '') {
                    $scope.todos1 = $scope.Allemployee;
                }
                $scope.makeTodos1();
            }

            function searchUtil1(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                    item.comn_user_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.empName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.em_mobile == toSearch) ? true : false;


            }

            $scope.Reset = function () {



                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {

                    $scope.curriculum = AllCurr.data;

                    $scope.temp = { s_cur_code: $scope.curriculum[0].sims_cur_code };
                    $scope.getAccYear($scope.temp.s_cur_code);
                });

                $scope.getAccYear = function (curCode) {

                    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.temp.s_cur_code).then(function (Acyear) {
                        $scope.Acc_year = Acyear.data;
                        $scope.temp = {
                            s_cur_code: $scope.curriculum[0].sims_cur_code,
                            sims_academic_year: $scope.Acc_year[0].sims_academic_year
                        }
                        $scope.getGrade();
                    });

                }

                //$scope.temp = {
                //    search_std_enroll_no: '',
                //    sims_section_code: '',
                //    sims_grade_code: '',
                //    search_std_name: ''
                //}
            }

            $scope.ResetEmployee = function () {

                $scope.temp2 = {
                    comn_user_name: '',
                    EmpName: ''
                }
            }

            $scope.ClearCheckBox = function () {

                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sims_batch_enroll_number;
                        var v = document.getElementById(t);
                        v.checked = false;
                        Student_bacth_code = [];
                        $scope.row1 = '';

                    }
                    main.checked = false;
                }
            }

            $scope.RemoveEnrollMentNo = function ($event, index, str) {
                str.splice(index, 1);
            }

            $scope.RemoveCommonUserNo = function ($event, index, str) {
                str.splice(index, 1);
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.ChkDate = function (noissueafter) {

                if ($scope.dt.sims_transport_effective_upto < $scope.dt.sims_transport_effective_from) {
                    swal({ title: "Alert", text: "Please Select future Date", showCloseButton: true, width: 380, });
                    $scope.dt.sims_transport_effective_upto = '2017-03-31';
                }
            }

            $scope.myFunct = function (keyEvent) {
                if (keyEvent.which == 13)
                    $scope.dt.sims_transport_effective_from = yyyy + '-' + mm + '-' + dd;
            }

            $scope.myFunct1 = function (keyEvent) {
                if (keyEvent.which == 13)
                    $scope.dt.sims_transport_effective_upto = yyyy + '-' + mm + '-' + dd;
            }


            //$scope.createdate = function (end_date, start_date, name) {
            //    var month1 = end_date.split("/")[0];
            //    var day1 = end_date.split("/")[1];
            //    var year1 = end_date.split("/")[2];
            //    var new_end_date = year1 + "/" + month1 + "/" + day1;

            //    var year = start_date.split("/")[0];
            //    var month = start_date.split("/")[1];
            //    var day = start_date.split("/")[2];
            //    var new_start_date = year + "/" + month + "/" + day;

            //    if (new_end_date < new_start_date) {
            //        $rootScope.strMessage = "Please Select Future Date";
            //        $('#message').modal('show');
            //        $scope.temp[name] = '';
            //    }
            //    else {
            //        $scope.temp[name] = new_end_date;
            //    }
            //}

            //$scope.showdate = function (date, name) {
            //    var month = date.split("/")[0];
            //    var day = date.split("/")[1];
            //    var year = date.split("/")[2];
            //    $scope.temp[name] = year + "/" + month + "/" + day;
            //}

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable2").tableHeadFixer({ 'top': 1 });

            }, 100);

            $timeout(function () {
                $("#fixTable3").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable4").tableHeadFixer({ 'top': 1 });

            }, 100);

            $scope.countData = [
           { val: 5, data: 5 },
           { val: 10, data: 10 },
           { val: 15, data: 15 },

            ]

            $scope.size = function (str) {
                //if (str == 5 || str == 10 || str == 15) {
                //    $scope.pager = true;
                //}
                //else {
                //    $scope.pager = false;
                //}

                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;  $scope.makeTodos();

                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.student;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.countDataemp = [
           { val: 5, data: 5 },
           { val: 10, data: 10 },
           { val: 15, data: 15 },

            ]

            $scope.size1 = function (str) {
                //if (str == 5 || str == 10 || str == 15) {
                //    $scope.pageremp = true;
                //}
                //else {
                //    $scope.pageremp = false;
                //}

                //$scope.pagesize1 = str;
                //$scope.currentPage1 = 1;
                //$scope.numPerPage1 = str;  $scope.makeTodos1();

                if (str == "All") {
                    $scope.currentPage1 = '1';
                    $scope.filteredTodos1 = $scope.Allemployee;
                    $scope.pager1 = false;
                }
                else {
                    $scope.pager1 = true;
                    $scope.pagesize1 = str;
                    $scope.currentPage1 = 1;
                    $scope.numPerPage1 = str;
                    $scope.makeTodos1();
                }

                $scope.numPerPage1 = str; console.log("numPerPage=" + $scope.numPerPage1); //$scope.makeTodos1();
            }

        }])
})();