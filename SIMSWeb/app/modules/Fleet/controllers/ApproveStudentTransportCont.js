﻿(function () {
    'use strict';
    var studentcode = [];
    var data1 = [];
    var main;
    var d712 = 0;
    var simsController = angular.module('sims.module.Fleet');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ApproveStudentTransportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.subbtn = false;
            $scope.cancelbtn = false;
            $scope.NodataStudent = false;
            var username = $rootScope.globals.currentUser.username;

            $scope.size = function (str) {
                /*
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; */

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }

                if (str == "All") {
                    $scope.currentPage = 1;
                    $scope.numPerPage = $scope.obj.length;
                    $scope.makeTodos();

                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_student_code);
                    v.checked = false;
                    main.checked = false;
                    //  $scope.row1 = '';
                    $scope.filteredTodos[i]['row1'] = '';

                }
                $scope.pageindex = str;
                $scope.currentPage = str; 
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    if ($scope.filteredTodos[i].sims_transport_route_reject_reason != '') {
                        $scope.filteredTodos[i]['read'] = true;
                    }
                    else {
                        $scope.filteredTodos[i]['read'] = false;

                    }
                    // var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_student_code);
                    // v.checked = true;
                    //$scope.row1 = 'row_selected';
                }
            };

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.edt = {
                    sims_cur_code: $scope.curriculum[0].sims_cur_code
                };
                $scope.getAccYear($scope.edt.sims_cur_code);
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.edt = {
                        sims_academic_year: $scope.Acc_year[0].sims_academic_year,
                        sims_cur_code: $scope.curriculum[0].sims_cur_code
                    };
                    $scope.getGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                });
            }

            $scope.Reset = function () {
                $scope.ApproveDataStudent = false;
                $scope.subbtn = false;
                $scope.cancelbtn = false;
                $scope.edt = {
                    //sims_cur_code: '',
                    //sims_academic_year: '',
                    sims_grade_code: '',
                    sims_section_code: '',
                    sims_transport_vehicle_code: '',
                    sims_transport_route_reject_reason: ''
                }
            }

            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
            }

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }

            $http.get(ENV.apiUrl + "api/common/getVehicleCode").then(function (get_VehicleCode) {
                $scope.VehicleCode = get_VehicleCode.data;
            })

            $scope.Cancel = function () {
                $scope.edt = {
                    //sims_cur_code: '',
                    //sims_academic_year: '',
                    sims_grade_code: '',
                    sims_section_code: '',
                    sims_transport_vehicle_code: '',
                    sims_transport_route_reject_reason: ''
                }
                $scope.ApproveDataStudent = false;
                $scope.NodataStudent = false;
                $scope.cancelbtn = false;
                $scope.subbtn = false;
            }

            $scope.Show_Data = function (cur_code, grade_code, academic_year, section_code, bus_code) {

                if ((cur_code != undefined && cur_code != '') &&
                    (academic_year != undefined && academic_year != '')) {
                    $scope.NodataStudent = false;
                    $scope.subbtn = true;
                    $scope.cancelbtn = true;
                    $http.get(ENV.apiUrl + "api/ApproveStudentTransportDetails/getApproveStudentTransport?cur_code=" + $scope.edt.sims_cur_code + "&acayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&bus_code=" + $scope.edt.sims_transport_vehicle_code).then(function (getApproveStudentTransport_Data) {
                        $scope.ApproveStudentTransport = getApproveStudentTransport_Data.data;
                        $scope.totalItems = $scope.ApproveStudentTransport.length;
                        $scope.todos = $scope.ApproveStudentTransport;
                        $scope.makeTodos();
                        
                        if ($scope.ApproveStudentTransport < 1) {
                            swal({ text: "Sorry, There Is No Data Found..!", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            //$scope.NodataStudent = true;
                            $scope.ApproveDataStudent = false;
                            $scope.subbtn = false;
                            $scope.cancelbtn = false;
                        }
                        else {

                            $scope.ApproveDataStudent = true;
                            $scope.NodataStudent = false;
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Curriculum and Academic year", width: 300, height: 200 });
                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_student_code);
                        if ($scope.filteredTodos[i].read == false) {
                            v.checked = true;
                            $scope.filteredTodos[i]['row1'] = 'row_selected';
                        }
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_student_code);
                        v.checked = false;
                        main.checked = false;
                        //  $scope.row1 = '';
                        $scope.filteredTodos[i]['row1'] = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
              
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.insertrecord = function (str) {

                studentcode = [];
                data1 = [];
                var reason = null;

                if (str == undefined || str == '') {
                    reason = null;
                }
                else {
                    reason = str;
                }

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_student_code);

                    if (v.checked == true) {
                        //studentcode =studentcode+$scope.filteredTodos[i].sims_transport_route_student_code+',';
                        var insertrecord = ({
                            sims_transport_route_student_code: $scope.filteredTodos[i].sims_transport_route_student_code,
                            //sims_transport_route_student_code: studentcode,
                            sims_transport_enroll_number: $scope.filteredTodos[i].sims_transport_enroll_number,
                            sims_cur_code: $scope.filteredTodos[i].sims_cur_code,
                            sims_transport_academic_year: $scope.filteredTodos[i].sims_transport_academic_year,
                            sims_grade_code: $scope.filteredTodos[i].sims_grade_code,
                            sims_section_code: $scope.filteredTodos[i].sims_section_code,
                            sims_transport_route_code: $scope.filteredTodos[i].sims_transport_route_code,
                            C_direction: $scope.filteredTodos[i].c_direction,
                            sims_transport_effective_from: $scope.filteredTodos[i].sims_transport_effective_from,
                            sims_transport_effective_upto: $scope.filteredTodos[i].sims_transport_effective_upto,
                            sims_transport_pickup_stop_code: $scope.filteredTodos[i].sims_transport_pickup_stop_code,
                            sims_transport_drop_stop_code: $scope.filteredTodos[i].sims_transport_drop_stop_code,
                            sims_transport_vehicle_code: $scope.filteredTodos[i].sims_transport_route_code,
                            sims_transport_route_reject_reason: reason,
                            sims_transport_route_approve_by: username,
                            opr: 'I',

                        });
                        data1.push(insertrecord);
                        d712 = 1;
                    }
                    else {
                        swal({ text: "Please select atleast one record..!", imageUrl: "assets/img/notification-alert.png", width: 350, height: 200 });
                        d712 = 0;
                    }

                }
                if (reason != null && reason != '') {

                    $http.post(ENV.apiUrl + "api/ApproveStudentTransportDetails/ApproveStudentTransportCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 });
                        }
                        $scope.ApproveDataStudent = false;
                        $scope.cancelbtn = false;
                        $scope.edt = {
                            //sims_cur_code: '',
                            //sims_academic_year: '',
                            sims_grade_code: '',
                            sims_section_code: '',
                            sims_transport_vehicle_code: '',
                            sims_transport_route_reject_reason: ''

                        }
                        main.checked = false;
                        $http.get(ENV.apiUrl + "api/ApproveStudentTransportDetails/getApproveStudentTransport?cur_code=" + $scope.edt.sims_cur_code + "&acayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&bus_code=" + $scope.edt.sims_transport_vehicle_code).then(function (getApproveStudentTransport_Data) {
                            $scope.ApproveStudentTransport = getApproveStudentTransport_Data.data;
                            $scope.totalItems = $scope.ApproveStudentTransport.length;
                            $scope.todos = $scope.ApproveStudentTransport;
                            $scope.makeTodos();

                        });
                    });

                    $scope.ApproveDataStudent = false;
                    $scope.cancelbtn = false;
                    $scope.ApproveRecord = false;
                    $scope.subbtn = false;
                }

                else {
                    if (d712 == 1) {

                        $http.post(ENV.apiUrl + "api/ApproveStudentTransportDetails/ApproveStudentTransportCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 });
                            }
                            $scope.ApproveDataStudent = false;
                            $scope.cancelbtn = false;
                            $scope.edt = {
                                //sims_cur_code: '',
                                //sims_academic_year: '',
                                sims_grade_code: '',
                                sims_section_code: '',

                                sims_transport_vehicle_code: '',
                                sims_transport_route_reject_reason: ''

                            }
                            main.checked = false;
                            $http.get(ENV.apiUrl + "api/ApproveStudentTransportDetails/getApproveStudentTransport?cur_code=" + $scope.edt.sims_cur_code + "&acayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&bus_code=" + $scope.edt.sims_transport_vehicle_code).then(function (getApproveStudentTransport_Data) {
                                $scope.ApproveStudentTransport = getApproveStudentTransport_Data.data;
                                $scope.totalItems = $scope.ApproveStudentTransport.length;
                                $scope.todos = $scope.ApproveStudentTransport;
                                $scope.makeTodos();

                            });
                        });

                        $scope.ApproveDataStudent = false;
                        $scope.cancelbtn = false;
                        $scope.ApproveRecord = false;
                        $scope.subbtn = false;
                    }
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.ApproveStudentTransport, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.ApproveStudentTransport;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                return (
                     item.sims_transport_route_reject_reason.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_transport_route_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_transport_enroll_number == toSearch) ? true : false;


            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

        }]);
})();



