﻿(function () {
    'use strict';
    var obj1, temp, opr, CurrentDate, comp;
    var main, deletefin = [], data = [];

    var simsController = angular.module('sims.module.Fleet');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TransportMaintenanceCont',
         ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

             //console.clear();
             $scope.table = true;
             $scope.hide6 = true;
             $scope.hide5 = false;
             $scope.hide4 = false;
             var doc_no = null;
             var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');
             $scope.dt = {
                 service_date: dateyear,
             }

             //var date = moment(dateyear);
             //date.add(1, 'months');

             //date.add(1, 'months');
             //date.add({ months: 1 });


             //var dt2 = new Date();
             //var currentDate = ('0' + dt2.getDate()).slice(-2) + '-' + ('0' + (dt2.getMonth() + 1)).slice(-2) + '-' + dt2.getFullYear();
             //                = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();;
             //$scope.temp.em_agreement_start_date = currentDate;
             //
             //var day = $scope.temp.em_date_of_join.split("-")[0];
             //var month = $scope.temp.em_date_of_join.split("-")[1];
             //var year = $scope.temp.em_date_of_join.split("-")[2];

             var day = dateyear.split("-")[0];
             var month = dateyear.split("-")[1];
             var year = dateyear.split("-")[2];
             var expMonth = parseInt(month) + parseInt(1);
             if (expMonth < 10)
                 expMonth = '0' + expMonth;

             $scope.dt1 = {
                 service_next_date: day + "-" + expMonth + "-" + year,//date.format('dd-MM-yyyy'),
             }

             //$('#loader0712').modal({ backdrop: 'static', keyboard: false });

             $timeout(function () {
                 $("#fixTable").tableHeadFixer({ 'top': 1 });
             }, 100);

             $scope.getallcomboBox = function () {

                 $http.get(ENV.apiUrl + "api/TransportFees/getvehiclecode").then(function (Alldata) {
                     $scope.vehicale = Alldata.data;
                     $scope.temp = { sims_transport_vehicle_code: $scope.vehicale[0].sims_transport_vehicle_code };
                 });

                 $http.get(ENV.apiUrl + "api/TransportFees/getvehicledoccode").then(function (Alldatadoc) {
                     $scope.vehicaledoc = Alldatadoc.data;
                     $scope.temp = { sims_transport_maintenance_doc_no: $scope.vehicaledoc[0].sims_transport_maintenance_doc_no };
                 });
             }

             $scope.getdriver = function () {

                 $http.get(ENV.apiUrl + "api/TransportFees/getvehicledrivercode?vehicle_code=" + $scope.temp.sims_transport_vehicle_code).then(function (Alldriverdata) {
                     $scope.driverdata = Alldriverdata.data;
                     $scope.temp = { sims_driver_code: $scope.driverdata[0].sims_driver_code };
                 });

             }

             setTimeout(function () {
                 $("#cmb_vehicle_code").select2();
             }, 100);

             setTimeout(function () {
                 $("#cmd_driver_code").select2();
             }, 100);

             setTimeout(function () {
                 $("#cmd_doc_no").select2();
             }, 100);

             $scope.getallcomboBox();

             $scope.CurrentDate = new Date();
             var today = new Date();
             var dd = today.getDate();
             var mm = today.getMonth() + 1;
             var yyyy = today.getFullYear();

             var datasend = [];
             $scope.vehicaleservice = [];

             $scope.Add = function (myForm) {
                 debugger;

                 if ($scope.temp.sims_transport_maintenance_doc_no != undefined) {
                     $scope.savedisabled = false;
                     $scope.Update_btn = true;
                 }
                 else if ($scope.temp.sims_transport_maintenance_doc_no != undefined) {
                     $scope.savedisabled = false;
                     $scope.Update_btn = true;
                 }
                 else {
                     $scope.savedisabled = true;
                     $scope.Update_btn = false;
                 }

                 if ($scope.temp.service_desc == '' || $scope.temp.service_desc == undefined) {
                     swal({ title: "Alert", text: "Please Enter Service Dscription", width: 300, height: 200 });
                     return;
                 }

                 if ($scope.temp.service_charges == undefined || $scope.temp.service_charges == '') {
                     swal({ title: "Alert", text: "Please Enter Service Charges In Digit", width: 300, height: 200 });
                     return;
                 }

                 var GetItemsadd = ({
                     'service_desc': $scope.temp.service_desc,
                     'service_charges': $scope.temp.service_charges,
                     'service_quantity':$scope.temp.service_quantity,
                     'sims_status': $scope.temp.sims_status,
                 });

                 $scope.vehicaleservice.push(GetItemsadd);
                 $scope.Main_table = true;
                 $scope.Grid = true;
                 $scope.temp.service_desc = '';
                 $scope.temp.service_charges = '';
                 $scope.temp.service_quantity = '';
                 $('#Text1').focus();

             };

             $scope.Removeservicedetails = function ($event, index, str) {
                 str.splice(index, 1);
                 if ($scope.vehicaleservice.length == 0) {
                     $scope.Main_table = false;
                 }
             }

             $scope.Edit = function () {
                 $scope.savedisabled = false;
                 $scope.Update_btn = true;
                 var objnew = {
                     sims_transport_maintenance_doc_no: $scope.temp.sims_transport_maintenance_doc_no,
                     //sims_academic_year: $scope.temp.sims_academic_year,
                 }

                 $http.post(ENV.apiUrl + "api/TransportFees/Targetcodesdetailstudent", objnew).then(function (res) {
                     $scope.vehicaleservice = [];
                     $scope.rows = res.data.table;
                     $scope.ve = res.data.table1;

                     if ($scope.ve.length <= 0) {
                         swal({ title: "Alert", text: "Records not found", showCloseButton: true, width: 380, });
                         $scope.Main_table = false;
                         $scope.Grid = false;
                     }
                     else {

                         for (var i = 0; i < $scope.rows.length; i++) {
                             $scope.temp.Purpose = $scope.rows[i].sims_transport_maintenance_purpose;
                             $scope.dt.service_date = $scope.rows[i].sims_transport_maintenance_date;
                             $scope.temp.sims_transport_vehicle_registration_number = $scope.rows[i].sims_transport_vehicle_name;
                             $scope.temp.sims_driver_name = $scope.rows[i].sims_driver_name;
                             $scope.temp.serviceat = $scope.rows[i].sims_transport_maintenance_service_station;
                             $scope.temp.Purpose = $scope.rows[i].sims_transport_maintenance_purpose;
                             $scope.temp.approveby = $scope.rows[i].sims_transport_maintenance_approve_by;
                             $scope.dt1.service_next_date = $scope.rows[i].sims_transport_maintenance_next_date;
                             $scope.temp.sims_status = $scope.rows[i].sims_transport_maintenance_status == 'A' ? true : false;
                             doc_no = $scope.rows[i].sims_transport_maintenance_doc_no;
                         }

                         for (var i = 0; i < $scope.ve.length; i++) {
                             var Getadd = ({
                                 'service_desc': $scope.ve[i].service_desc,
                                 'service_charges': $scope.ve[i].service_charges,
                                 'service_quantity': $scope.ve[i].service_quantity,
                                 'sims_status': $scope.ve[i].sims_status == 'A' ? true : false,
                             });
                             $scope.vehicaleservice.push(Getadd);
                         }
                         $scope.Main_table = true;
                         $scope.Grid = true;
                         $scope.hide6 = false;
                         $scope.hide5 = true;
                         $scope.hide4 = true;
                     }
                     //console.clear();
                 });
                 //if ($scope.vehicaledoc.length > 0)
                 //    $scope.hide4 = false;
                 //else
                 //    $scope.hide4 = true;
             }

             $scope.savedata = function (myform) {
                 if (myform) {

                     if ($scope.vehicaleservice.length > 0) {
                         var j = 0;
                         for (var i = 0; i < $scope.vehicaleservice.length; i++) {
                             j = i;
                             data = {
                                 'opr': 'R',
                                 'sims_transport_maintenance_line_no': j + 1,
                                 'sims_transport_maintenance_desc': $scope.vehicaleservice[i].service_desc,
                                 'sims_transport_maintenance_amount': $scope.vehicaleservice[i].service_charges,
                                 'sims_transport_maintenance_Quantity': $scope.vehicaleservice[i].service_quantity,
                                 'sims_transport_maintenance_detail_status': $scope.vehicaleservice[i].sims_status,
                             }
                             datasend.push(data);
                         }

                         var g = 0,q=0;
                         for (var i = 0; i < $scope.vehicaleservice.length; i++) {
                             g = parseFloat(g) + parseFloat($scope.vehicaleservice[i].service_charges);
                             q = parseFloat(q) + parseFloat($scope.vehicaleservice[i].service_quantity);
                         }

                         $scope.data1 = {
                             'opr': 'I',
                             'sims_transport_maintenance_date': $scope.dt.service_date,
                             'sims_transport_vehicle_code': $scope.temp.sims_transport_vehicle_code,
                             'sims_transport_maintenance_driver_code': $scope.temp.sims_driver_code,
                             'sims_transport_maintenance_service_station': $scope.temp.serviceat,
                             'sims_transport_maintenance_purpose': $scope.temp.Purpose,
                             'sims_transport_maintenance_approve_by': $scope.temp.approveby,
                             //'salesman_code': $rootScope.globals.currentUser.username,
                             'sims_transport_maintenance_Quantity':q,
                             'sims_transport_maintenance_total_amount': g,
                             'sims_transport_maintenance_next_date': $scope.dt1.service_next_date,
                             'sims_transport_maintenance_status': $scope.temp.sims_status
                         }

                         $http.post(ENV.apiUrl + "api/TransportFees/CUDMaintenanceDetails?data1=" + JSON.stringify($scope.data1), datasend).then(function (msg) {
                             $scope.msg1 = msg.data;
                             if ($scope.msg1 != null || $scope.msg1 != undefined) {
                                 var m = 'Service Details Add Succesfully With Doc Code: ' + $scope.msg1.strMessage;
                                 swal({ title: "Alert", text: m, width: 450, height: 200 });
                             }
                             else {
                                 swal({ title: "Alert", text: "Service Details Not Added", width: 300, height: 200 });
                             }

                         });
                         datasend = [];
                         $scope.table = true;
                         $scope.display = false;
                         $scope.Cancel();
                     }

                     else {
                         swal({ title: "Alert", text: "Please Enter Service Details", width: 300, height: 200 });
                     }
                 }
             }

             $scope.Update = function () {

                 if ($scope.vehicaleservice.length > 0) {
                     var j = 0;
                     for (var i = 0; i < $scope.vehicaleservice.length; i++) {
                         j = i;
                         data = {
                             'opr': 'Q',
                             'sims_transport_maintenance_line_no': j + 1,
                             'sims_transport_maintenance_desc': $scope.vehicaleservice[i].service_desc,
                             'sims_transport_maintenance_amount': $scope.vehicaleservice[i].service_charges,
                             'sims_transport_maintenance_Quantity': $scope.vehicaleservice[i].service_quantity,
                             'sims_transport_maintenance_detail_status': $scope.vehicaleservice[i].sims_status,
                         }
                         datasend.push(data);
                     }

                     var g = 0,q=0;
                     for (var i = 0; i < $scope.vehicaleservice.length; i++) {
                         g = parseFloat(g) + parseFloat($scope.vehicaleservice[i].service_charges);
                         q = parseFloat(q) + parseFloat($scope.vehicaleservice[i].service_quantity);
                     }


                     $scope.data1 = {
                         'opr': 'N',
                         //'sims_transport_maintenance_date': $scope.dt.service_date,
                         //'sims_transport_vehicle_code': $scope.temp.sims_transport_vehicle_code,
                         //'sims_transport_maintenance_driver_code': $scope.temp.sims_driver_code,
                         //'sims_transport_maintenance_service_station': $scope.temp.serviceat,
                         //'sims_transport_maintenance_status': $scope.temp.sims_status
                         //'sims_transport_maintenance_approve_by': $scope.temp.approveby,
                         //'salesman_code': $rootScope.globals.currentUser.username,
                         'sims_transport_maintenance_doc_no': doc_no,
                         'sims_transport_maintenance_Quantity': q,
                         'sims_transport_maintenance_total_amount': g,
                         'sims_transport_maintenance_next_date': $scope.dt1.service_next_date,
                         'sims_transport_maintenance_purpose': $scope.temp.Purpose,
                     }


                     $http.post(ENV.apiUrl + "api/TransportFees/CUDMaintenanceDetails?data1=" + JSON.stringify($scope.data1), datasend).then(function (msg) {
                         $scope.msg1 = msg.data;
                         if ($scope.msg1 != null || $scope.msg1 != undefined) {
                             var m = 'Service Details Updated Succesfully With Doc Code: ' + $scope.msg1.strMessage;
                             swal({ title: "Alert", text: m, width: 450, height: 200 });
                         }
                         else {
                             swal({ title: "Alert", text: "Service Details Not Updated", width: 300, height: 200 });
                         }

                     });
                     $scope.Cancel();
                 }

                 else {
                     swal({ title: "Alert", text: "Please Enter Service Details", width: 300, height: 200 });
                 }
             }

             $scope.Clear = function () {
                 //$scope.vehicaleservice = [];
                 $scope.vehicaleservice = '';
                 $scope.dt.service_date = '';
                 $("#cmb_vehicle_code").select2("val", "");
                 $("#cmd_driver_code").select2("val", "");
                 $("#cmd_doc_no").select2("val", "");
                 $scope.temp.serviceat = '';
                 $scope.temp.Purpose = '';
                 $scope.temp.approveby = '';
                 $scope.dt1.service_next_date = '';
                 $scope.temp.sims_status = false
                 $scope.temp.service_desc = '';
                 $scope.temp.service_charges = '';
                 $scope.dt = {
                     service_date: dateyear,
                 }
                 $scope.dt1 = {
                     service_next_date: day + "-" + expMonth + "-" + year,
                 }
                 $scope.getallcomboBox();
                 $state.go('main.SimTVS');
                 $scope.Main_table = false;
                 $scope.hide6 = true;
                 $scope.hide5 = false;
                 $scope.hide4 = false;
                 $scope.savedisabled = false;
                 $scope.Update_btn = false;
                 //$scope.getallcomboBox();
             }

             $scope.Cancel = function () {
                 $scope.Clear();
                 $scope.Myform.$setPristine();
                 $scope.Myform.$setUntouched();
             }

             $scope.onlyNumbers2 = function (event) {
                 debugger;
                 var keys = {

                     'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 45, 'plus': 43,
                     '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57

                 };
                 for (var index in keys) {
                     if (!keys.hasOwnProperty(index)) continue;
                     if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                         return; 
                     }
                 }
                 event.preventDefault();
             };

             $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                 $('input[type="text"]', $(this).parent()).focus();
             });

             $('*[data-datepicker="true"] input[type="text"]').datepicker({
                 todayBtn: true,
                 orientation: "top left",
                 autoclose: true,
                 todayHighlight: true,
                 format: 'dd-mm-yyyy'
             });

             $scope.dateupdate = function () {
                 date = moment($scope.dt.service_date)
                 console.log("date",date);
                 date.add(1, 'months');
                 $scope.dt1 = {
                     service_next_date: date.format('YYYY-MM-DD'),
                 }
                 var today = '';
                 today = $scope.dt1.service_next_date;
   
                //$('*[data-datepicker1="true"] input[type="text"]').val("");

                var today = $scope.dt1.service_next_date;

                $('*[data-datepicker1="true"] input[type="text"]').datepicker({
                    todayHighlight: true,
                    startDate: today,
                    autoclose: true,
                    todayBtn: true,
                    orientation: "top left",
                    format: 'YYYY-MM-DD',
                    immediateUpdates: true,
                    defaultViewDate: today
                });
             }

             $(document).ready(function () {

                 var today = $scope.dt1.service_next_date;

                 $('*[data-datepicker1="true"] input[type="text"]').datepicker({
                     todayHighlight: true,
                     startDate: today,
                     autoclose: true,
                     todayBtn: true,
                     orientation: "top left",
                     format: 'yyyy-mm-dd'
                 });
             });

             $(document).on('touch click', '*[data-datepicker1="true"] .add-on', function (e) {
                 $('input[type="text"]', $(this).parent()).focus();
             });

         }])
})();

//$(document).ready(function () {
//    var date = new Date();
//    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
//    var end = new Date(date.getFullYear(), date.getMonth(), date.getDate());

//    $('#datepicker1').datepicker({
//        format: "mm/dd/yyyy",
//        todayHighlight: true,
//        startDate: today,
//        endDate: end,
//        autoclose: true
//    });
//    $('#datepicker2').datepicker({
//        format: "mm/dd/yyyy",
//        todayHighlight: true,
//        startDate: today,
//        endDate: end,
//        autoclose: true
//    });

//    $('#datepicker1,#datepicker2').datepicker('setDate', today);
//});
