﻿(function () {
    'use strict';

    var opr = '';
    var routecode = [];
    var stopcode = [];
    var routestopcode = [];
    var restore = [];
    var main;
    var mainv;
    var maind;
    var check;
    var chk_term0712_aassign;
    var checkmonthwise;
    var data1 = [];
    var dcode;
    var sims_transport_route_student_code, sims_appl_parameter, PaidAmount, UnpaidAmount, TotalPayAmount;
    var d;
    var rt;

    //Transport Fee Period...
    var jantp, febtp, martp, aprtp, maytp, juntp, jultp, augtp, septp, octtp, novtp, dectp;

    //Fee Period...
    var janfp, febfp, marfp, aprfp, mayfp, junfp, julfp, augfp, sepfp, octfp, novfp, decfp;

    //Fee Period Paid Values...
    var janfpp, febfpp, marfpp, aprfpp, mayfpp, junfpp, julfpp, augfpp, sepfpp, octfpp, novfpp, decfpp;

    //Fee Update For Perticuler Period...
    var cp1, cp2, cp3, cp4, cp5, cp6, cp7, cp8, cp9, cp10, cp11, cp12, enddate;

    //All Periods...
    var sims_fee_period1, sims_fee_period2, sims_fee_period3, sims_fee_period4, sims_fee_period5, sims_fee_period6, sims_fee_period7, sims_fee_period8, sims_fee_period9, sims_fee_period10, sims_fee_period11, sims_fee_period12;

    //Total Fee
    var total_fee;

    var simsController = angular.module('sims.module.Fleet');

    simsController.controller('TransportCancelUpdateCont', ['$scope', '$state', '$rootScope', '$filter', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $filter, $timeout, gettextCatalog, $http, ENV) {

        check = document.getElementById("chk_term10");
        checkmonthwise = document.getElementById("chk_term11");
        chk_term0712_aassign = document.getElementById("chk_term0712")
        $scope.pagesize = '10';
        $scope.pagesize1 = '10';
        $scope.busyindicatorforbuswise = false;
        $scope.January_fee = true;
        $scope.February_fee = true;
        $scope.March_fee = true;
        $scope.April_fee = true;
        $scope.May_fee = true;
        $scope.June_fee = true;
        $scope.July_fee = true;
        $scope.August_fee = true;
        $scope.September_fee = true;
        $scope.October_fee = true;
        $scope.November_fee = true;
        $scope.December_fee = true;
        $scope.cancel_btn = false;
        $scope.update_btn = false;
        $scope.Check_valid = true;
        $scope.colors_menu = false;
        $scope.check_jan = false;
        $scope.check_feb = false;
        $scope.check_mar = false;
        $scope.check_apr = false;
        $scope.check_may = false;
        $scope.check_jun = false;
        $scope.check_jul = false;
        $scope.check_aug = false;
        $scope.check_sep = false;
        $scope.check_oct = false;
        $scope.check_nov = false;
        $scope.check_dec = false;
        $scope.hide_feecheck = true;
        //$scope.update_btn = false;
        //$scope.disable_cancel = false;
        //$scope.disable_apply = false;

        //$scope.Apply_New_T = function () {

        //}

        //

        $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');

        //$scope.temp.sims_transport_effective_from1= $scope.ddMMyyyy,
        //$scope.temp.sims_transport_effective_upto1=sims_transport_effective_upto='2017-03-31',

        $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getAcademicDate?cur_code=" + $rootScope.globals.currentUser.username).then(function (academicdates) {

            enddate = academicdates.data[0].sims_transport_effective_upto1;
        });

        $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getfeeeditaccess?user_code=" + $rootScope.globals.currentUser.username).then(function (salesmantype) {

            var fgh = salesmantype.data;
            if (fgh != undefined) {
                $scope.hide_feecheck = false;
                $scope.edit_code = false;
            }
            else {
                $scope.hide_feecheck = true;
                $scope.edit_code = true;
            }

        });

        $scope.size = function (str) {
            if (str == "All" || str == "all") {
                $scope.currentPage = '1';
                $scope.filteredTodos = $scope.todos;
                $scope.pager = false;
            }
            else {
                $scope.pager = true;
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }
        }

        $scope.index = function (str) {
            $scope.pageindex = str;
            $scope.currentPage = str;  $scope.makeTodos();
        }

        //-------------------------------------Bus_Wise_Transfer_Tab------------------------------------------------//

        $scope.size2 = function (str) {
            
            if (str = 'All') {
                $scope.filteredTodos2 = $scope.BUSWiseStudentTransDetail;
                $scope.pagesize2 = 1;
                $scope.currentPage2 = 1
            }
            else {
                $scope.pagesize2 = str;
                $scope.currentPage2 = 1;
                $scope.numPerPage2 = str;
               
                $scope.makeTodos2();
            }
           
        }

        $scope.index2 = function (str) {
            if (str = 'All') {
                $scope.filteredTodos2 = $scope.BUSWiseStudentTransDetail;
                $scope.pageindex2 = 1;
                $scope.currentPage2 = 1;
            }
            else {
                $scope.pageindex2 = str;
                $scope.currentPage2 = str;
               
                $scope.makeTodos2();
                main.checked = false;
                $scope.row1 = '';
            }
        }

        $scope.filteredTodos2 = [], $scope.currentPage2 = 1, $scope.numPerPage2 = 100, $scope.maxSize1 = 1000;

        $scope.makeTodos2 = function () {
            var rem2 = parseInt($scope.totalItems2 % $scope.numPerPage2);
            if (rem2 == '0') {
                $scope.pagersize2 = parseInt($scope.totalItems2 / $scope.numPerPage2);
            }
            else {
                $scope.pagersize2 = parseInt($scope.totalItems2 / $scope.numPerPage2) + 1;
            }
            var begin2 = (($scope.currentPage2 - 1) * $scope.numPerPage2);
            var end2 = parseInt(begin2) + parseInt($scope.numPerPage2);

          

            $scope.filteredTodos2 = $scope.todos2.slice(begin2, end2);
        };

        setTimeout(function () {
            $("#Select17,#Select19,#cmb_bus,#Select6").select2();
        }, 100);

        $http.get(ENV.apiUrl + "api/common/getAllAcademicYear").then(function (get_AllAcademicYear) {
            $scope.ActiveYear = get_AllAcademicYear.data;
            $scope.trans = { sims_academic_year: $scope.ActiveYear[0].sims_academic_year };
            $scope.getroutecodebyacademic($scope.trans.sims_academic_year);
        });

        $scope.getroutecodebyacademic = function (academic_yr) {
            $http.get(ENV.apiUrl + "api/common/getAllrotenameforBuswiseTransfer?academic_yr=" + $scope.trans.sims_academic_year).then(function (getAllrotenameforBuswiseTransfer_data) {
                $scope.AllrotenameforBuswiseTransfer = getAllrotenameforBuswiseTransfer_data.data;
            })
        }

        $scope.getdirectbyrouteacademic = function (academic_yr, route_code) {
            $scope.trans.sims_transport_route_direction = '';
            $http.get(ENV.apiUrl + "api/common/getAllDirectionforBuswiseTransfer?academic_yr=" + $scope.trans.sims_academic_year + "&route_code=" + $scope.trans.sims_transport_route_code).then(function (getAllDirectionforBuswiseTransfer_data) {
                $scope.AllDirectionforBuswiseTransfer = getAllDirectionforBuswiseTransfer_data.data;
            })
        }

        $scope.getdirectbyrouteacademic2 = function (academic_yr, route_code) {
            $http.get(ENV.apiUrl + "api/common/getAllDirectionforBuswiseTransfer?academic_yr=" + $scope.trans.sims_academic_year + "&route_code=" + $scope.trans.sims_transport_route_code).then(function (getAllDirectionforBuswiseTransfer_data) {
                $scope.AllDirectionforBuswiseTransfer = getAllDirectionforBuswiseTransfer_data.data;
            })
        }

        $scope.getStudentTransData = function (academic_yr, route_code, direction_aca_route) {
            $scope.busyindicatorforbuswise = true;

            $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getAllrotenameforBuswiseTransfer?academic_yr=" + $scope.trans.sims_academic_year + "&route_code=" + $scope.trans.sims_transport_route_code + "&direction_aca_route=" + $scope.trans.sims_transport_route_direction).then(function (getAllrotenameforBuswiseTransfer_data_712) {
                $scope.AllrotenameforBuswiseTransfer_name = getAllrotenameforBuswiseTransfer_data_712.data;
            })

            $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getBUSWiseStudentTransDetails?academic_yr=" + $scope.trans.sims_academic_year + "&route_code=" + $scope.trans.sims_transport_route_code + "&direction_aca_route=" + $scope.trans.sims_transport_route_direction).then(function (getBUSWiseStudentTransDetails_Data) {
                $scope.BUSWiseStudentTransDetail = getBUSWiseStudentTransDetails_Data.data;
                $scope.totalItems2 = $scope.BUSWiseStudentTransDetail.length;
                $scope.todos2 = $scope.BUSWiseStudentTransDetail;
                $scope.makeTodos2();

                if (getBUSWiseStudentTransDetails_Data.data.length > 0) {

                    $scope.sims_enroll_number = getBUSWiseStudentTransDetails_Data.data[0].sims_enroll_number;
                    $scope.sims_fee_code = getBUSWiseStudentTransDetails_Data.data[0].sims_fee_code;
                    $scope.sims_fee_cur_code = getBUSWiseStudentTransDetails_Data.data[0].sims_fee_cur_code;
                    $scope.sims_fee_grade_code = getBUSWiseStudentTransDetails_Data.data[0].sims_fee_grade_code;
                    $scope.sims_fee_number = getBUSWiseStudentTransDetails_Data.data[0].sims_fee_number;

                    $scope.sims_transport_route_code_old = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_route_code;
                    $scope.sims_transport_route_direction_old = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_route_direction;
                    $scope.sims_transport_route_direction_name = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_route_direction_1;

                    $scope.sims_transport_route_student_code = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_route_student_code;
                    $scope.sims_transport_seat_number = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_seat_number;
                    $scope.v_Code_old = getBUSWiseStudentTransDetails_Data.data[0].v_Code;
                    $scope.v_Name_Code_old = getBUSWiseStudentTransDetails_Data.data[0].v_Name_Code;

                    $scope.sims_transport_effective_from_old = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_effective_from;
                    $scope.sims_transport_effective_upto_old = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_effective_upto;

                    $scope.sims_transport_pickup_stop_code_old = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_pickup_stop_code;
                    $scope.sims_transport_drop_stop_code_old = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_drop_stop_code;

                    //$scope.sims_transport_effective_from_old = $filter('date')(new Date(getStudentTransportDetails_Data.data[0].sims_transport_effective_from), 'yyyy-mm-dd');
                    //$scope.sims_transport_effective_upto_old = $filter('date')(new Date(getStudentTransportDetails_Data.data[0].sims_transport_effective_upto), 'yyyy-mm-dd');


                    $scope.sims_transport_period1 = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_period1;
                    $scope.sims_transport_period2 = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_period2;
                    $scope.sims_transport_period3 = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_period3;
                    $scope.sims_transport_period4 = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_period4;
                    $scope.sims_transport_period5 = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_period5;
                    $scope.sims_transport_period6 = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_period6;
                    $scope.sims_transport_period7 = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_period7;
                    $scope.sims_transport_period8 = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_period8;
                    $scope.sims_transport_period9 = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_period9;
                    $scope.sims_transport_period10 = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_period10;
                    $scope.sims_transport_period11 = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_period11;
                    $scope.sims_transport_period12 = getBUSWiseStudentTransDetails_Data.data[0].sims_transport_period12;

                    $scope.sims_fee_period1 = getBUSWiseStudentTransDetails_Data.data[0].sims_fee_period1;
                    $scope.sims_fee_period2 = getBUSWiseStudentTransDetails_Data.data[0].sims_fee_period2;
                    $scope.sims_fee_period3 = getBUSWiseStudentTransDetails_Data.data[0].sims_fee_period3;
                    $scope.sims_fee_period4 = getBUSWiseStudentTransDetails_Data.data[0].sims_fee_period4;
                    $scope.sims_fee_period5 = getBUSWiseStudentTransDetails_Data.data[0].sims_fee_period5;
                    $scope.sims_fee_period6 = getBUSWiseStudentTransDetails_Data.data[0].sims_fee_period6;
                    $scope.sims_fee_period7 = getBUSWiseStudentTransDetails_Data.data[0].sims_fee_period7;
                    $scope.sims_fee_period8 = getBUSWiseStudentTransDetails_Data.data[0].sims_fee_period8;
                    $scope.sims_fee_period9 = getBUSWiseStudentTransDetails_Data.data[0].sims_fee_period9;
                    $scope.sims_fee_period10 = getBUSWiseStudentTransDetails_Data.data[0].sims_fee_period10;
                    $scope.sims_fee_period11 = getBUSWiseStudentTransDetails_Data.data[0].sims_fee_period11;
                    $scope.sims_fee_period12 = getBUSWiseStudentTransDetails_Data.data[0].sims_fee_period12;

                    $scope.searchtable_Buswise = true;
                    $scope.busyindicatorforbuswise = false;
                }

                else {
                    $scope.searchtable_Buswise = false;
                    $scope.searchtable_employee = false;
                    $scope.busyindicatorforbuswise = false;
                    swal({ title: "Alert", text: "Student not using the bus..!", width: 300, height: 100 });
                }
            });
        }

        $scope.getStopnamesbyRoute = function () {
            $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getStopByRouteBusWise?academic_yr=" + $scope.trans.sims_academic_year + "&route_code=" + $scope.trans1.sims_transport_route_code + "&direction=" + $scope.trans1.sims_transport_route_direction).then(function (getStopByRouteBusWise_data) {
                $scope.StopByRouteBusWise = getStopByRouteBusWise_data.data;
            });
        }

        $scope.CheckAllChecked_BusWise = function () {
            main = document.getElementById('mainchk');
            for (var i = 0; i < $scope.filteredTodos2.length; i++) {
                var v = document.getElementById($scope.filteredTodos2[i].sims_enroll_number + i);
                if (main.checked == true) {
                    v.checked = true;
                    $('tr').addClass("row_selected");
                }

                else {
                    v.checked = false;
                    $('tr').removeClass("row_selected");
                }
            }
        }

        $scope.checkonebyone_BusWise = function () {
            $("input[type='checkbox']").change(function (e) {
                if ($(this).is(":checked")) {
                    $(this).closest('tr').addClass("row_selected");
                    $scope.color = '#edefef';
                } else {
                    $(this).closest('tr').removeClass("row_selected");
                    $scope.color = '#edefef';
                }
            });
            main = document.getElementById('mainchk');
            if (main.checked == true) {
                main.checked = false;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                    }
                });
            }
        }

        $scope.submit_bus_wise = function () {
            $scope.flag = false;
            data1 = [];
            for (var i = 0; i < $scope.filteredTodos2.length; i++) {
                var v = document.getElementById($scope.filteredTodos2[i].sims_enroll_number + i);
                if (v.checked == true) {
                    $scope.flag = true;
                    var data = ({
                        sims_transport_enroll_number: $scope.filteredTodos2[i].sims_enroll_number,
                        sims_cur_code: $scope.filteredTodos2[i].sims_fee_cur_code,
                        sims_grade_code: $scope.filteredTodos2[i].sims_fee_grade_code,
                        sims_section_code: $scope.filteredTodos2[i].section_code,

                        sims_academic_year: $scope.trans.sims_academic_year,
                        sims_transport_route_code: $scope.trans1.sims_transport_route_code,
                        sims_transport_route_code_old: $scope.sims_transport_route_code_old,

                        sims_transport_route_direction: $scope.trans1.sims_transport_route_direction,
                        sims_transport_route_direction_old: $scope.sims_transport_route_direction_old,

                        v_Code_old: $scope.filteredTodos2[i].v_Code_old,
                        sims_transport_route_student_code: $scope.filteredTodos2[i].sims_transport_route_student_code,
                        //sims_transport_vehicle_seating_capacity: $scope.tempp.sims_transport_vehicle_seating_capacity,
                        sims_transport_effective_from: $scope.trans.sims_transport_effective_from,
                        sims_transport_effective_upto: $scope.filteredTodos2[i].sims_transport_effective_upto,

                        sims_transport_effective_from_old: $scope.sims_transport_effective_from_old,
                        sims_transport_effective_upto_old: $scope.sims_transport_effective_upto_old,

                        sims_transport_pickup_stop_code: $scope.trans.sims_transport_pickup_stop_code,
                        sims_transport_drop_stop_code: $scope.trans.sims_transport_drop_stop_code,

                        sims_fee_code: $scope.filteredTodos2[i].sims_fee_code,
                        sims_transport_route_student_stop_lat: null,
                        ims_transport_route_student_stop_long: null,


                        sims_transport_stop_lat_drop: null,
                        sims_transport_stop_long_drop: null,

                        sims_1: $scope.filteredTodos2[i].sims_fee_period1,
                        sims_2: $scope.filteredTodos2[i].sims_fee_period2,
                        sims_3: $scope.filteredTodos2[i].sims_fee_period3,
                        sims_4: $scope.filteredTodos2[i].sims_fee_period4,
                        sims_5: $scope.filteredTodos2[i].sims_fee_period5,
                        sims_6: $scope.filteredTodos2[i].sims_fee_period6,
                        sims_7: $scope.filteredTodos2[i].sims_fee_period7,
                        sims_8: $scope.filteredTodos2[i].sims_fee_period8,
                        sims_9: $scope.filteredTodos2[i].sims_fee_period9,
                        sims_10: $scope.filteredTodos2[i].sims_fee_period10,
                        sims_11: $scope.filteredTodos2[i].sims_fee_period11,
                        sims_12: $scope.filteredTodos2[i].sims_fee_period12,
                        opr: 'U',
                        sub_opr: 'I'
                    });
                    data1.push(data);

                }


            }
            if ($scope.flag) {
                $http.post(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/CUDTransportStudentBusWise", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                   

                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 });
                    }
                    else {
                        swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 });
                    }
                });
            }
            else {
                swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
            }
            $scope.buscheck = {}
            $scope.buscheck['buswisetrans'] = false;
            $scope.searchtable_Buswise = false;
            $scope.filteredTodos2 = [];
        }

        $scope.searched2 = function (valLists, toSearch) {
            return _.filter(valLists,
            function (i) {
                return searchUtil2(i, toSearch);
            });
        };

        $scope.search2 = function () {
            $scope.todos2 = $scope.searched2($scope.BUSWiseStudentTransDetail, $scope.searchText);
            $scope.totalItems2 = $scope.todos2.length;
            $scope.currentPage2 = '1';
            if ($scope.searchText == '') {
                $scope.todos2 = $scope.BUSWiseStudentTransDetail;
            }
            $scope.makeTodos2();
        }

        function searchUtil2(item, toSearch) {
            return (
                item.sims_student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                item.v_Name_Code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                item.sims_transport_route_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                 item.sims_transport_route_direction_1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                item.sims_enroll_number == toSearch) ? true : false;
        }

        $scope.ResetBuswisetrans = function () {
            $scope.trans = {
                sims_transport_route_code: '',
                sims_transport_route_direction: '',
                sims_transport_pickup_stop_code: '',
                sims_transport_drop_stop_code: '',
                sims_transport_effective_from: ''
            }

            $scope.trans1 = {
                sims_transport_route_code: '',
                sims_transport_route_direction: ''
            }
            //$("#Select17,#Select19,#cmb_bus").select2();
            $("#Select17").select2("val", "");
            $("#Select19").select2("val", "");

            $scope.searchtable_Buswise = false;
            $scope.filteredTodos2 = [];

            $http.get(ENV.apiUrl + "api/common/getAllAcademicYear").then(function (get_AllAcademicYear) {
                $scope.ActiveYear = get_AllAcademicYear.data;
                $scope.trans = { sims_academic_year: $scope.ActiveYear[0].sims_academic_year };
                $scope.getroutecodebyacademic($scope.trans.sims_academic_year);
            });

            $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getAllrotenameforBuswiseTransfer?academic_yr=" + $scope.trans.sims_academic_year + "&route_code=" + $scope.trans.sims_transport_route_code + "&direction_aca_route=" + $scope.trans.sims_transport_route_direction).then(function (getAllrotenameforBuswiseTransfer_data_712) {
                $scope.AllrotenameforBuswiseTransfer_name = getAllrotenameforBuswiseTransfer_data_712.data;
            })
        }

        //-------------------------------------End_Bus_Wise_Transfer_Tab_End----------------------------------------//

        $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

        $scope.makeTodos = function () {
            var rem = parseInt($scope.totalItems % $scope.numPerPage);
            if (rem == '0') {
                $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
            }
            else {
                $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
            }
            var begin = (($scope.currentPage - 1) * $scope.numPerPage);
            var end = parseInt(begin) + parseInt($scope.numPerPage);

            

            $scope.filteredTodos = $scope.todos.slice(begin, end);


            //    for (var i = 0; i < $scope.filteredTodos.length; i++) {

            //        if (i > 0) {
            //            for (var j = 0; j < $scope.filteredTodos.length; j++) {
            //                if ($scope.filteredTodos[i - 1].sims_transport_effective_upto < $scope.filteredTodos[j].sims_transport_effective_from && $scope.filteredTodos[i].sims_enroll_number == $scope.filteredTodos[j].sims_enroll_number) {
            //                    $scope.filteredTodos[i].isshow = true;
            //                }
            //                else if ($scope.filteredTodos[i - 1].sims_enroll_number != $scope.filteredTodos[j].sims_enroll_number) {
            //                    $scope.filteredTodos[i].isshow = true;
            //                }
            //                else if ($scope.filteredTodos[i].sims_enroll_number == $scope.filteredTodos[j].sims_enroll_number) {
            //                    $scope.filteredTodos[i - 1].isshow = false;
            //                }
            //            }
            //        } else {
            //            $scope.filteredTodos[i].isshow = true;
            //        }

            //    }
            //};

            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                if (i > 0) {
                    for (var j = 0; j < $scope.filteredTodos.length; j++) {
                        if ($scope.filteredTodos[i - 1].sims_transport_effective_upto < $scope.filteredTodos[j].sims_transport_effective_from && $scope.filteredTodos[i].sims_enroll_number == $scope.filteredTodos[j].sims_enroll_number) {
                            $scope.filteredTodos[i].isshow = true;
                        }
                        else if ($scope.filteredTodos[i - 1].sims_enroll_number != $scope.filteredTodos[j].sims_enroll_number) {
                            $scope.filteredTodos[i].isshow = true;
                        }
                        else if ($scope.filteredTodos[i].sims_enroll_number == $scope.filteredTodos[j].sims_enroll_number) {
                            if ($scope.filteredTodos[i - 1].sims_transport_route_direction != $scope.filteredTodos[j].sims_transport_route_direction) {
                                $scope.filteredTodos[i].isshow = true;
                                $scope.filteredTodos[i - 1].isshow = true;
                            }
                            else {
                                $scope.filteredTodos[i - 1].isshow = false;
                            }
                            if (i > 1) {
                                if ($scope.filteredTodos[0].sims_enroll_number == $scope.filteredTodos[j].sims_enroll_number && $scope.filteredTodos[0].sims_transport_route_direction == $scope.filteredTodos[j].sims_transport_route_direction && $scope.filteredTodos[0].sims_transport_effective_upto < $scope.filteredTodos[j].sims_transport_effective_from) {
                                    $scope.filteredTodos[0].isshow = false;
                                }
                                if ($scope.filteredTodos[i].sims_enroll_number == $scope.filteredTodos[j].sims_enroll_number && $scope.filteredTodos[i].sims_transport_route_direction == $scope.filteredTodos[j].sims_transport_route_direction && $scope.filteredTodos[i].sims_transport_effective_upto < $scope.filteredTodos[j].sims_transport_effective_from) {
                                    $scope.filteredTodos[i].isshow = false;
                                }
                            }
                        }
                    }
                } else {
                    $scope.filteredTodos[i].isshow = true;
                }
            }
        };

        $scope.CheckItemSet = function () {

            check = document.getElementById("chk_term10");
            if (check.checked == true) {
                $scope.disable_cancel = true;
                $scope.disable_apply = true;
                $scope.cancel_btn = true;
                $scope.update_btn = true;
                $scope.submit_btn = true;
                $scope.end_date = true;
                $scope.end_date_c = true;
                $scope.start_date = true;
                $scope.disable_update = false;
                $scope.jan = false;
                $scope.feb = false;
                $scope.mar = false;
                $scope.aprl = false;
                $scope.may = false;
                $scope.jun = false;
                $scope.july = false;
                $scope.aug = false;
                $scope.sept = false;
                $scope.oct = false;
                $scope.nov = false;
                $scope.dec = false;
            }
            else {
                $scope.disable_apply = false;
                $scope.update_btn = false;
                $scope.submit_btn = true;
                $scope.cancel_btn = false;
                $scope.end_date = false;
                $scope.end_date_c = false;
                $scope.start_date = false;
                $scope.jan = true;
                $scope.feb = true;
                $scope.mar = true;
                $scope.aprl = true;
                $scope.may = true;
                $scope.jun = true;
                $scope.july = true;
                $scope.aug = true;
                $scope.sept = true;
                $scope.oct = true;
                $scope.nov = true;
                $scope.dec = true;
            }
        }

        $scope.CheckMonthWise = function () {

            checkmonthwise = document.getElementById("chk_term11");

            if (checkmonthwise.checked == true) {
                //$scope.check_jan = true;
                //$scope.check_feb = true;
                //$scope.check_mar = true;
                //$scope.check_apr = true;
                //$scope.check_may = true;
                //$scope.check_jun = true;
                //$scope.check_jul = true;
                //$scope.check_aug = true;
                //$scope.check_sep = true;
                //$scope.check_oct = true;
                //$scope.check_nov = true;
                //$scope.check_dec = true;
                $scope.CheckDetails();
            }
            else {

                $scope.check_jan = false;
                $scope.check_feb = false;
                $scope.check_mar = false;
                $scope.check_apr = false;
                $scope.check_may = false;
                $scope.check_jun = false;
                $scope.check_jul = false;
                $scope.check_aug = false;
                $scope.check_sep = false;
                $scope.check_oct = false;
                $scope.check_nov = false;
                $scope.check_dec = false;

                $scope.colors_menu = false;
                $scope.cancel_btn = false;
                $scope.valid_date = false;
                $scope.update_btn = false;
                $scope.apply_new = false;
                $scope.disable_cancel = false;
                $scope.disable_update = false;


            }

        }

        $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
            $scope.curriculum = AllCurr.data;
            $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
            $scope.getAccYear();
        });

        $scope.getAccYear = function (curCode) {
            $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.edt.sims_cur_code).then(function (Acyear) {
                $scope.Acc_year = Acyear.data;
                $scope.edt = {
                    sims_cur_code: $scope.edt.sims_cur_code,//$scope.curriculum[0].sims_cur_code,
                    sims_academic_year: $scope.Acc_year[0].sims_academic_year
                }
                $scope.getGrade();
            });
        }

        $scope.getGrade = function (curCode, accYear) {
            $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Gradecode) {
                $scope.Grade_code = Gradecode.data;
            });
        }

        $scope.getSection = function (curCode, gradeCode, accYear) {
            $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Sectioncode) {
                $scope.Section_code = Sectioncode.data;
            });
        }

        $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getAcademicYear").then(function (getAcademicYear) {
            $scope.AcademicYear_data = getAcademicYear.data;
        });

        $http.get(ENV.apiUrl + "api/common/getVehicleCode").then(function (get_VehicleCode) {
            $scope.VehicleCode = get_VehicleCode.data;
        })

        $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getBusCode").then(function (getBusCode_Data) {
            $scope.BusCode_Data = getBusCode_Data.data;
           
        });

        $scope.getroutebybuscode = function () {
            $scope.hide_asroute = true;
            $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getRouteNameByBusCode?AcaYear=" + $scope.temp.sims_academic_year + "&Vehicle_Code=" + $scope.temp.v_Code).then(function (getRouteNameByBusCode_Data) {
                
                $scope.RouteNameByBusCode_Data = getRouteNameByBusCode_Data.data;
               // $scope.temp.sims_transport_route_code = $scope.RouteNameByBusCode_Data[0].sims_transport_route_code;

                //dcode = $scope.RouteNameByBusCode_Data[0].sims_transport_route_direction;

                $scope.getStopbyRoute($scope.temp.sims_transport_route_code, $scope.temp.sims_academic_year)
                if ($scope.tempck.chk_term0712 = true) {
                    $scope.tempck.chk_term0712 = false;
                    $scope.hide_feecheck_NT = false;
                    $scope.hide_feecheck = false;
                    $scope.end_date_c = false;
                    $scope.hide_second_NT = false;
                    $scope.hide_second_pick1_NT = false;
                    $scope.hide_second_pick2_NT = false;
                    $scope.hide_asroute = true;
                }
            });
        }

        $scope.getStopbyRoute = function (route_code, aca_year) {

            $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getDirectionByRouteCodeName?route_code=" + route_code + "&aca_year=" + aca_year).then(function (get_DirectionName) {
                
                $scope.DirectionName = get_DirectionName.data;
                $scope.RouteNameByBusCode_Data.sims_transport_route_direction
                if (d != undefined || d != '')
                    $scope.temp.sims_transport_route_direction = d;
                else
                    $scope.temp.sims_transport_route_direction = $scope.DirectionName[0].sims_transport_route_direction;
                
                //$scope.temp.sims_transport_route_direction = dcode;
            })

            $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getStopByRoute?route_code=" + $scope.temp.sims_transport_route_code + "&aca_year=" + $scope.temp.sims_academic_year).then(function (getStopByRoute_Data) {
                $scope.StopByRoute_Data = getStopByRoute_Data.data;
                $scope.temp.sims_transport_pickup_stop_code = $scope.StopByRoute_Data[0].sims_transport_stop_code;
                $scope.temp.sims_transport_drop_stop_code = $scope.StopByRoute_Data[0].sims_transport_stop_code;
                 });

        }

        $scope.getStopbyRoute0712 = function (route_code, aca_year) {
           
            

            //if (d == '01' && d != '') {
            //    $scope.temp.sims_transport_route_direction = '01';
            //    rt = '01';
            //}

            //else if (d == '02' && d != '') {
            //    $scope.temp.sims_transport_route_direction = '02';
            //    rt = '02';
            //}

            //if (rt == '01' && d == '') {
            //    $scope.temp.sims_transport_route_direction = '02';
            //    rt = '02';
            //}

            //else if (rt == '02' && d == '') {
            //    $scope.temp.sims_transport_route_direction = '01';
            //    rt = '01';
            //}

            //d = '';

            

        }

        $scope.Reset = function () {

            $scope.edt = {
                sims_cur_code: "",
                sims_academic_year: "",
                sims_grade_code: "",
                sims_section_code: "",
                sims_enroll_number: '',

            }
            $("#cmb_bus").select2("val", "");
            $scope.searchtable = false;
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear();
            });
        }

        $scope.Show_Data = function (cur_code, acayear, grade, sec_code, enroll, vehicle_code) {
            debugger;
            $("#Student_Details div").removeClass('error-control');
            $("#Bus_information div").removeClass('error-control');
            
            if ($scope.edt.sims_cur_code == undefined) {
                $(".cur_select").parent('div').addClass('error-control');
            }
            else if ($scope.edt.sims_academic_year == undefined) {
                $(".aca_select").parent('div').addClass('error-control');
            }
            //else if ($scope.edt.sims_grade_code == undefined) {
            //    $(".grade_select").parent('div').addClass('error-control');
            //}
            //else if ($scope.edt.sims_section_code == undefined) {
            //    $(".section_select").parent('div').addClass('error-control');
            //}

            $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getStudentTransportDetails?cur_code=" + $scope.edt.sims_cur_code + "&acayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&enroll=" + $scope.edt.sims_enroll_number + "&vehicle_code=" + $scope.edt.sims_transport_vehicle_code).then(function (getStudentTransportDetails_Data) {
                $scope.StudentTransportDetails = getStudentTransportDetails_Data.data;
                $scope.totalItems = $scope.StudentTransportDetails.length;
                $scope.todos = $scope.StudentTransportDetails;
                $scope.makeTodos();
                if ($scope.edt.sims_grade_code == undefined && $scope.edt.sims_section_code == undefined && $scope.edt.sims_enroll_number == undefined && $scope.edt.sims_transport_vehicle_code == undefined) {
                    if (getStudentTransportDetails_Data.data.length > 0) {
                        //$scope.sims_transport_route_code_old = getStudentTransportDetails_Data.data[0].sims_transport_route_code;
                        //$scope.sims_transport_pickup_stop_code_old = getStudentTransportDetails_Data.data[0].sims_transport_pickup_stop_code;
                        //$scope.sims_transport_route_direction_old = getStudentTransportDetails_Data.data[0].sims_transport_route_direction;
                        //$scope.v_Code_old = getStudentTransportDetails_Data.data[0].v_Code;
                                //$scope.sims_transport_effective_from_old = $filter('date')(new Date(getStudentTransportDetails_Data.data[0].sims_transport_effective_from), 'yyyy-mm-dd');
                                //$scope.sims_transport_effective_upto_old = $filter('date')(new Date(getStudentTransportDetails_Data.data[0].sims_transport_effective_upto), 'yyyy-mm-dd');

                        //$scope.sims_transport_effective_from_old = getStudentTransportDetails_Data.data[0].sims_transport_effective_from;
                        //$scope.sims_transport_effective_upto_old = getStudentTransportDetails_Data.data[0].sims_transport_effective_upto;

                        //$scope.sims_transport_drop_stop_code_old = getStudentTransportDetails_Data.data[0].sims_transport_drop_stop_code;
                        $scope.searchtable = true;
                        $scope.searchtable_employee = false;

                    }

                }
                else {
                    for (var i = 0; i <= $scope.StudentTransportDetails.length; i++) {
                        if ($scope.StudentTransportDetails[i].sims_png == '') {
                            if (getStudentTransportDetails_Data.data.length > 0) {
                                //$scope.sims_transport_route_code_old = getStudentTransportDetails_Data.data[i].sims_transport_route_code;
                                //$scope.sims_transport_pickup_stop_code_old = getStudentTransportDetails_Data.data[i].sims_transport_pickup_stop_code;
                                //$scope.sims_transport_route_direction_old = getStudentTransportDetails_Data.data[i].sims_transport_route_direction;
                                //$scope.v_Code_old = getStudentTransportDetails_Data.data[i].v_Code;
                                //$scope.sims_transport_effective_from_old = $filter('date')(new Date(getStudentTransportDetails_Data.data[0].sims_transport_effective_from), 'yyyy-mm-dd');
                                //$scope.sims_transport_effective_upto_old = $filter('date')(new Date(getStudentTransportDetails_Data.data[0].sims_transport_effective_upto), 'yyyy-mm-dd');

                                //$scope.sims_transport_effective_from_old = getStudentTransportDetails_Data.data[i].sims_transport_effective_from;
                                //$scope.sims_transport_effective_upto_old = getStudentTransportDetails_Data.data[i].sims_transport_effective_upto;

                                //$scope.sims_transport_drop_stop_code_old = getStudentTransportDetails_Data.data[i].sims_transport_drop_stop_code;
                                $scope.searchtable = true;
                                $scope.searchtable_employee = false;

                            }
                        }
                        else {

                            if ($scope.StudentTransportDetails[i].sims_png != '') {
                                if ($scope.StudentTransportDetails[i].sims_png != null || $scope.StudentTransportDetails[i].sims_png != "" || $scope.StudentTransportDetails[i].sims_png != undefined) {
                                    swal({ title: "Alert", text: $scope.StudentTransportDetails[i].sims_png, width: 300, height: 100 });
                                }
                            }
                            $scope.searchtable = false;
                            $scope.searchtable_employee = false;

                        }
                    }
                }
            });
            //}
            //else {
            //    swal({ title: "Alert", text: "Please Select Curriculum and Academic year", width: 300, height: 200 });
            //}
        }

        $scope.CheckDetails = function () {

            checkmonthwise = document.getElementById("chk_term11");
            debugger;
            if (checkmonthwise.checked == true) {
                $scope.check_jan = true;
                $scope.check_feb = true;
                $scope.check_mar = true;
                $scope.check_apr = true;
                $scope.check_may = true;
                $scope.check_jun = true;
                $scope.check_jul = true;
                $scope.check_aug = true;
                $scope.check_sep = true;
                $scope.check_oct = true;
                $scope.check_nov = true;
                $scope.check_dec = true;
            }
            else {
                $scope.check_jan = false;
                $scope.check_feb = false;
                $scope.check_mar = false;
                $scope.check_apr = false;
                $scope.check_may = false;
                $scope.check_jun = false;
                $scope.check_jul = false;
                $scope.check_aug = false;
                $scope.check_sep = false;
                $scope.check_oct = false;
                $scope.check_nov = false;
                $scope.check_dec = false;

                //$scope.colors_menu = false;
                //$scope.cancel_btn = false;
                //$scope.valid_date = false;
                //$scope.update_btn = false;
                //$scope.disable_cancel = false;
                //$scope.disable_update = false;
                //return;
            }

            $scope.colors_menu = true;
            $scope.cancel_btn = true;
            $scope.valid_date = true;
            $scope.update_btn = true;
            $scope.disable_cancel = false;
            $scope.disable_update = true;

            var start_date = new Date();
            start_date = $scope.temp.sims_transport_effective_from;
            var end_date = new Date();
            end_date = $scope.temp.sims_transport_effective_upto;

            var check_date1 = start_date;
            var day = check_date1.split("-")[0];
            var month = check_date1.split("-")[1];
            var year = check_date1.split("-")[2];
            var check_date = year + '-' + month + '-' + day;

            var check_date2 = end_date;
            var day1 = check_date2.split("-")[0];
            var month1 = check_date2.split("-")[1];
            var year1 = check_date2.split("-")[2];
            end_date = year1 + '-' + month1 + '-' + day1;

            for (; new Date(check_date) <= new Date(end_date) ;) {

                check_date = moment(check_date)
                var d = moment(check_date).format('MM')
                if (d == '01') {
                    $scope.jantp = jantp;
                }
                else if (d == '02') {
                    $scope.febtp = febtp;
                }
                else if (d == '03') {
                    $scope.martp = martp;
                }
                else if (d == '04') {
                    $scope.aprtp = aprtp;
                }
                else if (d == '05') {
                    $scope.maytp = maytp;
                }
                else if (d == '06') {
                    $scope.juntp = juntp;
                }
                else if (d == '07') {
                    $scope.jultp = jultp;
                }

                else if (d == '08') {
                    $scope.augtp = augtp;
                }
                else if (d == '09') {
                    $scope.septp = septp;
                }
                else if (d == '10') {
                    $scope.octtp = octtp;
                }
                else if (d == '11') {
                    $scope.novtp = novtp;
                }
                else if (d == '12') {
                    $scope.dectp = dectp;
                }
                check_date.add(1, 'months');
                check_date = moment(check_date).format('YYYY/MM/DD');
            }

            if ($scope.dectp == 'undifined' || $scope.dectp == null) {
                $scope.dectp = "0.0000";
            }

            if ($scope.novtp == 'undifined' || $scope.novtp == null) {
                $scope.novtp = "0.0000";
            }
            if ($scope.octtp == 'undifined' || $scope.octtp == null) {
                $scope.octtp = "0.0000";
            }
            if ($scope.septp == 'undifined' || $scope.septp == null) {
                $scope.septp = "0.0000";
            }
            if ($scope.augtp == 'undifined' || $scope.augtp == null) {
                $scope.augtp = "0.0000";
            }
            if ($scope.jultp == 'undifined' || $scope.jultp == null) {
                $scope.jultp = "0.0000";
            }
            if ($scope.juntp == 'undifined' || $scope.juntp == null) {
                $scope.juntp = "0.0000";
            }
            if ($scope.maytp == 'undifined' || $scope.maytp == null) {
                $scope.maytp = "0.0000";
            }
            if ($scope.aprtp == 'undifined' || $scope.aprtp == null) {
                $scope.aprtp = "0.0000";
            }
            if ($scope.martp == 'undifined' || $scope.martp == null) {
                $scope.martp = "0.0000";
            }
            if ($scope.febtp == 'undifined' || $scope.febtp == null) {
                $scope.febtp = "0.0000";
            }
            if ($scope.jantp == 'undifined' || $scope.jantp == null) {
                $scope.jantp = "0.0000";
            }

            function checkFilled() {

                var jancol = document.getElementById("Text17");
                //if (jancol.value == "") {
                    if ($scope.jantp == "0.0000") {
                        jancol.style.color = "purple";
                        jancol.style.fontSize = 25;
                    }
                    else if ($scope.janfpp != "0.0000" && $scope.janfpp != 'undefined') {
                        jancol.style.color = "green";
                        jancol.style.fontSize = 25;
                    }
                    else {
                        jancol.style.color = "red";
                    }
                //}
                //else {
                //    jancol.style.color = "";
                //}

                var febcol = document.getElementById("Text18");
                //if (febcol.value == "") {
                    if ($scope.febtp == "0.0000") {
                        febcol.style.color = "purple";
                        febcol.style.fontSize = 25;
                    }
                    else if ($scope.febfpp != "0.0000" && $scope.febfpp != 'undefined') {
                        febcol.style.color = "green";
                        febcol.style.fontSize = 25;
                    }
                    else {
                        febcol.style.color = "red";
                    }
                //}
                //else {
                //    febcol.style.color = "";
                //}

                var marcol = document.getElementById("Text19");
                //if (marcol.value == "") {
                    if ($scope.martp == "0.0000") {
                        marcol.style.color = "purple";
                        marcol.style.fontSize = 25;
                    }
                    else if ($scope.marfpp != "0.0000" && $scope.marfpp != 'undefined') {
                        marcol.style.color = "green";
                        marcol.style.fontSize = 25;
                    }
                    else {
                        marcol.style.color = "red";
                    }
                //}
                //else {
                //    marcol.style.color = "";
                //}

                var aprtcol = document.getElementById("Text20");
                //if (aprtcol.value == "") {
                    if ($scope.aprtp == "0.0000") {
                        aprtcol.style.color = "purple";
                        aprtcol.style.fontSize = 25;
                    }
                    else if ($scope.aprfpp != "0.0000" && $scope.aprfpp != undefined) {
                        aprtcol.style.color = "green";
                        aprtcol.style.fontSize = 25;
                    }
                    else {
                        aprtcol.style.color = "red";
                    }
                //}
                //else {
                //    aprtcol.style.color = "";
                //}

                var maycol = document.getElementById("Text21");
                //if (maycol.value == "") {
                    if ($scope.maytp == "0.0000") {
                        maycol.style.color = "purple";
                        maycol.style.fontSize = 25;
                    }
                    else if ($scope.mayfpp != "0.0000" && $scope.mayfpp != 'undefined') {
                        maycol.style.color = "green";
                        maycol.style.fontSize = 25;
                    }
                    else {
                        maycol.style.color = "red";
                    }
                //}
                //else {
                //    maycol.style.color = "";
                //}

                var juncol = document.getElementById("Text22");
                //if (juncol.value == "") {
                    if ($scope.juntp == "0.0000") {
                        juncol.style.color = "purple";
                        juncol.style.fontSize = 25;
                    }
                    else if ($scope.junfpp != "0.0000" && $scope.junfpp != 'undefined') {
                        juncol.style.color = "green";
                        juncol.style.fontSize = 25;
                    }
                    else {
                        juncol.style.color = "red";
                    }
                //}
                //else {
                //    juncol.style.color = "";
                //}

                var julcol = document.getElementById("Text23");
                //if (julcol.value == "") {
                    if ($scope.jultp == "0.0000") {
                        julcol.style.color = "purple";
                        julcol.style.fontSize = 25;
                    }
                    else if ($scope.julfpp != "0.0000" && $scope.julfpp != undefined) {
                        julcol.style.color = "green";
                        julcol.style.fontSize = 25;
                    }
                    else {
                        julcol.style.color = "red";
                    }
                //}
                //else {
                //    julcol.style.color = "";
                //}

                var augcol = document.getElementById("Text24");
                //if (augcol.value == "") {
                    if ($scope.augtp == "0.0000") {
                        augcol.style.color = "purple";
                        augcol.style.fontSize = 25;
                    }
                    else if ($scope.augfpp != "0.0000" && $scope.augfpp != undefined) {
                        augcol.style.color = "green";
                        augcol.style.fontSize = 25;
                    }
                    else {
                        augcol.style.color = "red";
                    }
                //}
                //else {
                //    augcol.style.color = "";
                //}

                var sepcol = document.getElementById("Text25");
                //if (sepcol.value == "") {
                    if ($scope.septp == "0.0000") {
                        sepcol.style.color = "purple";
                        sepcol.style.fontSize = 25;
                    }
                    else if ($scope.sepfpp != "0.0000" && $scope.sepfpp != 'undefined') {
                        sepcol.style.color = "green";
                        sepcol.style.fontSize = 25;
                    }
                    else {
                        sepcol.style.color = "red";
                    }
                //}
                //else {
                //    sepcol.style.color = "";
                //}

                var octcol = document.getElementById("Text26");
                //if (octcol.value == "") {
                    if ($scope.octtp == "0.0000") {
                        octcol.style.color = "purple";
                        octcol.style.fontSize = 25;
                    }
                    else if ($scope.octfpp != "0.0000" && $scope.octfpp != 'undefined') {
                        octcol.style.color = "green";
                        octcol.style.fontSize = 25;
                    }
                    else {
                        octcol.style.color = "red";
                    }
                //}
                //else {
                //    octcol.style.color = "";
                //}

                var novcol = document.getElementById("Text27");
                //if (novcol.value == "") {
                    if ($scope.novtp == "0.0000") {
                        novcol.style.color = "purple";
                        novcol.style.fontSize = 25;
                    }
                    else if ($scope.novfpp != "0.0000" && $scope.novfpp != 'undefined') {
                        novcol.style.color = "green";
                        novcol.style.fontSize = 25;
                    }
                    else {
                        novcol.style.color = "red";
                    }
                //}
                //else {
                //    novcol.style.color = "";
                //}

                var deccol = document.getElementById("Text28");
                //if (deccol.value == "") {
                    if ($scope.dectp == "0.0000") {
                        deccol.style.color = "purple";
                        deccol.style.fontSize = 25;
                    }
                    else if ($scope.decfpp != "0.0000" && $scope.decfpp != 'undefined') {
                        deccol.style.color = "green";
                        deccol.style.fontSize = 25;
                    }
                    else {
                        deccol.style.color = "red";
                    }
                //}
                //else {
                //    deccol.style.color = "";
                //}
            }

            checkFilled();


            $scope.transport_fee_period1 = true;
            $scope.transport_fee_period2 = true;
            $scope.transport_fee_period3 = true;
            $scope.transport_fee_period4 = true;
            $scope.transport_fee_period5 = true;
            $scope.transport_fee_period6 = true;
            $scope.transport_fee_period7 = true;
            $scope.transport_fee_period8 = true;
            $scope.transport_fee_period9 = true;
            $scope.transport_fee_period10 = true;
            $scope.transport_fee_period11 = true;
            $scope.transport_fee_period12 = true;

            $scope.January_fee = false;
            $scope.February_fee = false;
            $scope.March_fee = false;
            $scope.April_fee = false;
            $scope.May_fee = false;
            $scope.June_fee = false;
            $scope.July_fee = false;
            $scope.August_fee = false;
            $scope.September_fee = false;
            $scope.October_fee = false;
            $scope.November_fee = false;
            $scope.December_fee = false;

        }

        $scope.Check_jan_1 = function () {
            var Check_jan_1 = document.getElementById("chk_term21");
            if (Check_jan_1.checked == true) {
                cp1 = 1;
            }
            else {
                cp1 = 0;
            }
        }

        $scope.check_feb_2 = function () {
            var check_feb_2 = document.getElementById("chk_term22");
            if (check_feb_2.checked == true) {
                cp2 = 1;
            }
            else {
                cp2 = 0;
            }
        }

        $scope.check_feb_3 = function () {
            var check_feb_3 = document.getElementById("chk_term23");
            if (check_feb_3.checked == true) {
                cp3 = 1;
            }
            else {
                cp3 = 0;
            }
        }

        $scope.check_apr_4 = function () {
            var check_apr_4 = document.getElementById("chk_term24");
            if (check_apr_4.checked == true) {
                cp4 = 1;
            }
            else {
                cp4 = 0;
            }
        }

        $scope.check_may_5 = function () {
            var check_may_5 = document.getElementById("chk_term25");
            if (check_may_5.checked == true) {
                cp5 = 1;
            }
            else {
                cp5 = 0;
            }
        }

        $scope.check_jun_6 = function () {
            var check_jun_6 = document.getElementById("chk_term26");
            if (check_jun_6.checked == true) {
                cp6 = 1;
            }
            else {
                cp6 = 0
            }
        }

        $scope.check_jul_7 = function () {
            var check_jul_7 = document.getElementById("chk_term27");
            if (check_jul_7.checked == true) {
                cp7 = 1;
            }
            else {
                cp7 = 0
            }
        }

        $scope.check_aug_8 = function () {
            var check_aug_8 = document.getElementById("chk_term28");
            if (check_aug_8.checked == true) {
                cp8 = 1;
            }
            else {
                cp8 = 0;
            }
        }

        $scope.check_aug_9 = function () {
            var check_aug_9 = document.getElementById("chk_term29");
            if (check_aug_9.checked == true) {
                cp9 = 1;
            }
            else {
                cp9 = 0;
            }
        }

        $scope.check_oct_10 = function () {
            var check_oct_10 = document.getElementById("chk_term30");
            if (check_oct_10.checked == true) {
                cp10 = 1;
            }
            else {
                cp10 = 0;
            }
        }

        $scope.check_nov_11 = function () {
            var check_nov_11 = document.getElementById("chk_term31");
            if (check_nov_11.checked == true) {
                cp11 = 1;
            }
            else {
                cp11 = 0;
            }
        }

        $scope.check_dec_12 = function () {
            var check_dec_12 = document.getElementById("chk_term32");
            if (check_dec_12.checked == true) {
                cp12 = 1;
            }
            else {
                cp12 = 0;
            }
        }

        $scope.ModalClear = function () {
            $('#MyModal').modal('hide');

            $scope.temp = '';
            $scope.temp1 = '';
            $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getStudentTransportDetails?cur_code=" + $scope.edt.sims_cur_code + "&acayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&enroll=" + $scope.edt.sims_enroll_number + "&vehicle_code=" + $scope.edt.sims_transport_vehicle_code).then(function (getStudentTransportDetails_Data) {
                $scope.StudentTransportDetails = getStudentTransportDetails_Data.data;
                $scope.totalItems = $scope.StudentTransportDetails.length;
                $scope.todos = $scope.StudentTransportDetails;
                $scope.makeTodos();
            });
            $scope.tempck = {};
            $scope.tempmw = {};
            $scope.tempck['chk_term10'] = false;
            $scope.tempmw['chk_term11'] = false;
            $scope.check_jan = false;
            $scope.check_feb = false;
            $scope.check_mar = false;
            $scope.check_apr = false;
            $scope.check_may = false;
            $scope.check_jun = false;
            $scope.check_jul = false;
            $scope.check_aug = false;
            $scope.check_sep = false;
            $scope.check_oct = false;
            $scope.check_nov = false;
            $scope.check_dec = false;
            $scope.colors_menu = false;
            $scope.cancel_btn = false;
            $scope.valid_date = false;
            $scope.update_btn = false;
            $scope.disable_cancel = true;
            $scope.disable_update = false;



        }

        $scope.up = function (obj) {
            debugger;
            $scope.restore = obj;
            $scope.temp = '';
            $scope.temp1 = '';
            $scope.tempck = {};
            $scope.tempmw = {};

            $scope.tempck = {
                chk_term10: false,
            }

            $scope.tempmw = {
                chk_term11: false,
            }
            //$scope.tempck['chk_term10'] = false;
            //$scope.tempmw['chk_term11'] = false;
            $scope.cancel_btn = false;
            $scope.update_btn = false;
            $scope.disable_apply = false;

            $scope.colors_menu = false;
            $scope.cancel_btn = false;
            $scope.apply_new = false;
            $scope.valid_date = true;

            $scope.disable_cancel = true;
            $scope.disable_update = false;


            $scope.checkcancelmonth = {
                chk_term21: false,
                chk_term22: false,
                chk_term23: false,
                chk_term24: false,
                chk_term25: false,
                chk_term26: false,
                chk_term27: false,
                chk_term28: false,
                chk_term29: false,
                chk_term30: false,
                chk_term31: false,
                chk_term32: false

            }

            $scope.sims_transport_route_code_old = obj.sims_transport_route_code;
            $scope.sims_transport_pickup_stop_code_old = obj.sims_transport_pickup_stop_code;
            $scope.sims_transport_route_direction_old = obj.sims_transport_route_direction;
            $scope.v_Code_old = obj.v_Code;
            $scope.sims_transport_effective_from_old = obj.sims_transport_effective_from;
            $scope.sims_transport_effective_upto_old = obj.sims_transport_effective_upto;
            $scope.sims_transport_drop_stop_code_old = obj.sims_transport_drop_stop_code;

            sims_transport_route_student_code = obj.sims_transport_route_student_code;
            sims_appl_parameter = obj.sims_appl_parameter;

            //$scope.temp['sims_enroll_number'] = obj.sims_enroll_number;
            d=obj.sims_transport_route_direction,

            $scope.temp = {
                sims_transport_route_student_code: obj.sims_transport_route_student_code,
                sims_fee_cur_code: obj.sims_fee_cur_code,
                sims_academic_year: obj.sims_transport_academic_year,
                sims_fee_grade_code: obj.sims_fee_grade_code,
                sims_grade_name_en: obj.sims_grade_name_en,
                sims_fee_section_code: obj.sims_fee_section_code,

                sims_enroll_number: obj.sims_enroll_number,
                sims_student_name: obj.sims_student_name,


                sims_fee_number: obj.sims_fee_number,
                sims_section_name_en: obj.sims_section_name_en,
                sims_transport_route_student_code:obj.sims_transport_route_student_code,
                sims_transport_route_code: obj.sims_transport_route_code,
                sims_transport_route_name: obj.sims_transport_route_name,
                sims_transport_route_direction: obj.sims_transport_route_direction,
                sims_transport_route_direction_1: obj.sims_transport_route_direction_1,

                sims_transport_pickup_stop_code: obj.sims_transport_pickup_stop_code,
                sims_transport_drop_stop_code: obj.sims_transport_drop_stop_code,

                sims_transport_effective_from: obj.sims_transport_effective_from,
                sims_transport_effective_upto: obj.sims_transport_effective_upto,
                sims_transport_effective_from1: obj.sims_transport_effective_from,//$scope.ddMMyyyy,
                sims_transport_effective_upto1: obj.sims_transport_effective_upto,//enddate,

                v_Code: obj.v_Code,
                //sims_transport_route_code: obj.sims_transport_route_code,

            };

            $scope.startdt = $scope.temp.sims_transport_effective_from;
            $scope.enddt = $scope.temp.sims_transport_effective_upto;
            $scope.direction_one_chege($scope.temp.sims_transport_route_direction)
            var df=$scope.temp.sims_transport_route_direction;
            var rc=$scope.temp.sims_transport_route_code;
            //$scope.temp1 = {

            //    sims_fee_period1: obj.sims_fee_period1,
            //    sims_fee_period2: obj.sims_fee_period2,
            //    sims_fee_period3: obj.sims_fee_period3,
            //    sims_fee_period4: obj.sims_fee_period4,
            //    sims_fee_period5: obj.sims_fee_period5,
            //    sims_fee_period6: obj.sims_fee_period6,
            //    sims_fee_period7: obj.sims_fee_period7,
            //    sims_fee_period8: obj.sims_fee_period8,
            //    sims_fee_period9: obj.sims_fee_period9,
            //    sims_fee_period10: obj.sims_fee_period10,
            //    sims_fee_period11: obj.sims_fee_period11,
            //    sims_fee_period12: obj.sims_fee_period12
            //};
            $scope.temp1 = {};

            $scope.getroutebybuscode(obj.v_Code, obj.sims_transport_academic_year);

            $scope.getStopbyRoute(obj.sims_transport_route_code, obj.sims_transport_academic_year);

            if (obj.sims_fee_period1 != '0.0000' || parseInt(obj.sims_fee_period1) != 0) {
                $scope.temp1.sims_fee_period1 = obj.sims_fee_period1;
            }
            else {
                $scope.temp1.sims_fee_period1 = obj.sims_transport_period1;
            }
            if (obj.sims_fee_period2 != '0.0000' || parseInt(obj.sims_fee_period2) != 0) {
                $scope.temp1.sims_fee_period2 = obj.sims_fee_period2;
            }
            else {
                $scope.temp1.sims_fee_period2 = obj.sims_transport_period2;
            }
            if (obj.sims_fee_period3 != '0.0000' || parseInt(obj.sims_fee_period3) != 0) {
                $scope.temp1.sims_fee_period3 = obj.sims_fee_period3;
            }
            else {
                $scope.temp1.sims_fee_period3 = obj.sims_transport_period3;
            }
            if (obj.sims_fee_period4 != '0.0000' || parseInt(obj.sims_fee_period4) != 0) {
                $scope.temp1.sims_fee_period4 = obj.sims_fee_period4;
            }
            else {
                $scope.temp1.sims_fee_period4 = obj.sims_transport_period4;
            }
            if (obj.sims_fee_period5 != '0.0000' || parseInt(obj.sims_fee_period5) != 0) {
                $scope.temp1.sims_fee_period5 = obj.sims_fee_period5;
            }
            else {
                $scope.temp1.sims_fee_period5 = obj.sims_transport_period5;
            }
            if (obj.sims_fee_period6 != '0.0000' || parseInt(obj.sims_fee_period6) != 0) {
                $scope.temp1.sims_fee_period6 = obj.sims_fee_period6;
            }
            else {
                $scope.temp1.sims_fee_period6 = obj.sims_transport_period6;
            }
            if (obj.sims_fee_period7 != '0.0000' || parseInt(obj.sims_fee_period7) != 0) {
                $scope.temp1.sims_fee_period7 = obj.sims_fee_period7;
            }
            else {
                $scope.temp1.sims_fee_period7 = obj.sims_transport_period7;
            }
            if (obj.sims_fee_period8 != '0.0000' || parseInt(obj.sims_fee_period8) != 0) {
                $scope.temp1.sims_fee_period8 = obj.sims_fee_period8;
            }
            else {
                $scope.temp1.sims_fee_period8 = obj.sims_transport_period8;
            }
            if (obj.sims_fee_period9 != '0.0000' || parseInt(obj.sims_fee_period9) != 0) {
                $scope.temp1.sims_fee_period9 = obj.sims_fee_period9;
            }
            else {
                $scope.temp1.sims_fee_period9 = obj.sims_transport_period9;
            }
            if (obj.sims_fee_period10 != '0.0000' || parseInt(obj.sims_fee_period10) != 0) {
                $scope.temp1.sims_fee_period10 = obj.sims_fee_period10;
            }
            else {
                $scope.temp1.sims_fee_period10 = obj.sims_transport_period10;
            }
            if (obj.sims_fee_period11 != '0.0000' || parseInt(obj.sims_fee_period11) != 0) {
                $scope.temp1.sims_fee_period11 = obj.sims_fee_period11;
            }
            else {
                $scope.temp1.sims_fee_period11 = obj.sims_transport_period11;
            }
            if (obj.sims_fee_period12 != '0.0000' || parseInt(obj.sims_fee_period12) != 0) {
                $scope.temp1.sims_fee_period12 = obj.sims_fee_period12;
            }
            else {
                $scope.temp1.sims_fee_period12 = obj.sims_transport_period12;
            }

            //Transport Route Period Fee Values...
            jantp = obj.sims_transport_period1,
            febtp = obj.sims_transport_period2,
            martp = obj.sims_transport_period3,
            aprtp = obj.sims_transport_period4,
            maytp = obj.sims_transport_period5,
            juntp = obj.sims_transport_period6,
            jultp = obj.sims_transport_period7,
            augtp = obj.sims_transport_period8,
            septp = obj.sims_transport_period9,
            octtp = obj.sims_transport_period10,
            novtp = obj.sims_transport_period11,
            dectp = obj.sims_transport_period12,

            //Fee Period Paid Values...

            $scope.janfpp = obj.sims_fee_period1_paid,
            $scope.febfpp = obj.sims_fee_period2_paid,
            $scope.marfpp = obj.sims_fee_period3_paid,
            $scope.aprfpp = obj.sims_fee_period4_paid,
            $scope.mayfpp = obj.sims_fee_period5_paid,
            $scope.junfpp = obj.sims_fee_period6_paid,
            $scope.julfpp = obj.sims_fee_period7_paid,
            $scope.augfpp = obj.sims_fee_period8_paid,
            $scope.sepfpp = obj.sims_fee_period9_paid,
            $scope.octfpp = obj.sims_fee_period10_paid,
            $scope.novfpp = obj.sims_fee_period11_paid,
            $scope.decfpp = obj.sims_fee_period12_paid,

            //Fee Period Values...
            janfp = obj.sims_fee_period1,
            febfp = obj.sims_fee_period2,
            marfp = obj.sims_fee_period3,
            aprfp = obj.sims_fee_period4,
            mayfp = obj.sims_fee_period5,
            junfp = obj.sims_fee_period6,
            julfp = obj.sims_fee_period7,
            augfp = obj.sims_fee_period8,
            sepfp = obj.sims_fee_period9,
            octfp = obj.sims_fee_period10,
            novfp = obj.sims_fee_period11,
            decfp = obj.sims_fee_period12
            // decfp = $scope.temp1.sims_fee_period12

            $scope.edt["PaidAmount"] = obj.sims_paid_total;
            $scope.edt["UnpaidAmount"] = obj.sims_pending_amount;
            $scope.edt["TotalPayAmount"] = obj.sims_total_amount;


            $scope.jan = true;
            $scope.feb = true;
            $scope.mar = true;
            $scope.aprl = true;
            $scope.may = true;
            $scope.jun = true;
            $scope.july = true;
            $scope.aug = true;
            $scope.sept = true;
            $scope.oct = true;
            $scope.nov = true;
            $scope.dec = true;

            $scope.transport_fee_period1 = false;
            $scope.transport_fee_period2 = false;
            $scope.transport_fee_period3 = false;
            $scope.transport_fee_period4 = false;
            $scope.transport_fee_period5 = false;
            $scope.transport_fee_period6 = false;
            $scope.transport_fee_period7 = false;
            $scope.transport_fee_period8 = false;
            $scope.transport_fee_period9 = false;
            $scope.transport_fee_period10 = false;
            $scope.transport_fee_period11 = false;
            $scope.transport_fee_period12 = false;

            $scope.January_fee = true;
            $scope.February_fee = true;
            $scope.March_fee = true;
            $scope.April_fee = true;
            $scope.May_fee = true;
            $scope.June_fee = true;
            $scope.July_fee = true;
            $scope.August_fee = true;
            $scope.September_fee = true;
            $scope.October_fee = true;
            $scope.November_fee = true;
            $scope.December_fee = true;
            //$scope.temp = { 'sims_transport_route_direction': df }
            //$scope.temp = { 'sims_transport_route_code': rc }


            $('#MyModal').modal('show');
            $scope.TransportUpdate = true;
            $scope.submit_btn = true;

        }

        $scope.Apply = function (obj) {

            if ($scope.temp.sims_transport_effective_from1 && $scope.temp.sims_transport_effective_upto1 != null) {
                $scope.January_fee = true;
                $scope.February_fee = true;
                $scope.March_fee = true;
                $scope.April_fee = true;
                $scope.May_fee = true;
                $scope.June_fee = true;
                $scope.July_fee = true;
                $scope.August_fee = true;
                $scope.September_fee = true;
                $scope.October_fee = true;
                $scope.November_fee = true;
                $scope.December_fee = true;

                $scope.cancel_btn = true;
                $scope.update_btn = true;
                $scope.apply_new = true;
                $scope.disable_cancel = true;

                $scope.transport_fee_period1 = false;
                $scope.transport_fee_period2 = false;
                $scope.transport_fee_period3 = false;
                $scope.transport_fee_period4 = false;
                $scope.transport_fee_period5 = false;
                $scope.transport_fee_period6 = false;
                $scope.transport_fee_period7 = false;
                $scope.transport_fee_period8 = false;
                $scope.transport_fee_period9 = false;
                $scope.transport_fee_period10 = false;
                $scope.transport_fee_period11 = false;
                $scope.transport_fee_period12 = false;

                if (chk_term0712_aassign.checked == true) {
                    $scope.disable_update = true;
                    $scope.disable_apply_new = false;
                    $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getFeesnewapply?aca_year=" + $scope.temp.sims_academic_year + "&route_code=" + $scope.temp.sims_transport_route_code + "&route_code_new=" + $scope.temp.sims_transport_route_code_2 + "&route_direct=" + $scope.temp.sims_transport_route_direction + "&route_direct_new=" + $scope.temp.sims_transport_route_direction_2 + "&sims_transport_effective_from=" + $scope.temp.sims_transport_effective_from1 + "&sims_transport_effective_upto=" + $scope.temp.sims_transport_effective_upto1).then(function (getFees_Data) {
                        $scope.Fees_Data = getFees_Data.data;
                        for (var i = 0; i < $scope.Fees_Data.length; i++) {
                            $scope.temp1 = {

                                'sims_fee_period1': $scope.Fees_Data[i].sims_transport_period1
                                 , 'sims_fee_period2': $scope.Fees_Data[i].sims_transport_period2
                                 , 'sims_fee_period3': $scope.Fees_Data[i].sims_transport_period3
                                 , 'sims_fee_period4': $scope.Fees_Data[i].sims_transport_period4
                                 , 'sims_fee_period5': $scope.Fees_Data[i].sims_transport_period5
                                 , 'sims_fee_period6': $scope.Fees_Data[i].sims_transport_period6
                                 , 'sims_fee_period7': $scope.Fees_Data[i].sims_transport_period7
                                 , 'sims_fee_period8': $scope.Fees_Data[i].sims_transport_period8
                                 , 'sims_fee_period9': $scope.Fees_Data[i].sims_transport_period9
                                 , 'sims_fee_period10': $scope.Fees_Data[i].sims_transport_period10
                                 , 'sims_fee_period11': $scope.Fees_Data[i].sims_transport_period11
                                 , 'sims_fee_period12': $scope.Fees_Data[i].sims_transport_period12
                                 , 'sims_transport_toatal': $scope.Fees_Data[i].sims_transport_toatal
                            };
                        }
                        $scope.total_fee = $scope.temp1.sims_transport_toatal;
                    });
                }
                else {
                    $scope.disable_update = false;
                    $scope.disable_apply_new = true;
                    $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getFees?aca_year=" + $scope.temp.sims_academic_year + "&route_code=" + $scope.temp.sims_transport_route_code + "&route_direct=" + $scope.temp.sims_transport_route_direction + "&sims_transport_effective_from=" + $scope.temp.sims_transport_effective_from1 + "&sims_transport_effective_upto=" + $scope.temp.sims_transport_effective_upto1).then(function (getFees_Data) {
                        $scope.Fees_Data = getFees_Data.data;
                        for (var i = 0; i < $scope.Fees_Data.length; i++) {
                            $scope.temp1 = {

                                'sims_fee_period1': $scope.Fees_Data[i].sims_transport_period1
                                    , 'sims_fee_period2': $scope.Fees_Data[i].sims_transport_period2
                                    , 'sims_fee_period3': $scope.Fees_Data[i].sims_transport_period3
                                    , 'sims_fee_period4': $scope.Fees_Data[i].sims_transport_period4
                                    , 'sims_fee_period5': $scope.Fees_Data[i].sims_transport_period5
                                    , 'sims_fee_period6': $scope.Fees_Data[i].sims_transport_period6
                                    , 'sims_fee_period7': $scope.Fees_Data[i].sims_transport_period7
                                    , 'sims_fee_period8': $scope.Fees_Data[i].sims_transport_period8
                                    , 'sims_fee_period9': $scope.Fees_Data[i].sims_transport_period9
                                    , 'sims_fee_period10': $scope.Fees_Data[i].sims_transport_period10
                                    , 'sims_fee_period11': $scope.Fees_Data[i].sims_transport_period11
                                    , 'sims_fee_period12': $scope.Fees_Data[i].sims_transport_period12
                                    , 'sims_transport_toatal': $scope.Fees_Data[i].sims_transport_toatal
                            };
                        }
                        $scope.total_fee = $scope.temp1.sims_transport_toatal;
                        //var start_date = new Date();
                        //start_date = $scope.temp.sims_transport_effective_from1;
                        //var end_date = new Date();
                        //end_date = $scope.temp.sims_transport_effective_upto1;
                        //var check_date = start_date;

                        //var jancol = document.getElementById("txt_jan");
                        //var febcol = document.getElementById("txt_feb");
                        //var marcol = document.getElementById("Text6");
                        //var aprtcol = document.getElementById("Text7");
                        //var maycol = document.getElementById("Text8");
                        //var juncol = document.getElementById("Text9");
                        //var julcol = document.getElementById("Text10");
                        //var augcol = document.getElementById("Text11");
                        //var sepcol = document.getElementById("Text12");
                        //var octcol = document.getElementById("Text13");
                        //var novcol = document.getElementById("Text14");
                        //var deccol = document.getElementById("Text15");

                        //for (; new Date(check_date) <= new Date(end_date) ;) {
                        //    
                        //    check_date = moment(check_date)
                        //    var d = moment(check_date).format('MM')
                        //    if (d == '01') {
                        //        if (jancol.value == "") {
                        //            if ($scope.temp1.sims_fee_period1 == 0) {
                        //                jancol.style.color = "purple";
                        //                jancol.style.fontSize = 25;
                        //            }
                        //            else if ($scope.temp1.sims_fee_period1 != 0 && $scope.temp1.sims_fee_period1 != 'undefined') {
                        //                jancol.style.color = "orange";
                        //                jancol.style.fontSize = 25;
                        //            }
                        //        }
                        //        else {
                        //            jancol.style.color = "";
                        //        }
                        //    }

                        //    else if (d == '02') {
                        //        if (febcol.value == "") {
                        //            if ($scope.temp1.sims_fee_period2 == 0) {
                        //                febcol.style.color = "purple";
                        //                febcol.style.fontSize = 25;
                        //            }
                        //            else if ($scope.temp1.sims_fee_period2 != 0 && $scope.temp1.sims_fee_period2 != 'undefined') {
                        //                febcol.style.color = "orange";
                        //                febcol.style.fontSize = 25;
                        //            }
                        //        }
                        //        else {
                        //            febcol.style.color = "";
                        //        }
                        //    }

                        //    else if (d == '03') {
                        //        if (marcol.value == "") {
                        //            if ($scope.temp1.sims_fee_period3 == 0) {
                        //                marcol.style.color = "purple";
                        //                marcol.style.fontSize = 25;
                        //            }
                        //            else if ($scope.temp1.sims_fee_period3 != 0 && $scope.temp1.sims_fee_period3 != 'undefined') {
                        //                marcol.style.color = "orange";
                        //                marcol.style.fontSize = 25;
                        //            }
                        //        }
                        //        else {
                        //            marcol.style.color = "";
                        //        }
                        //    }

                        //    else if (d == '04') {
                        //        if (aprtcol.value == "") {
                        //            if ($scope.temp1.sims_fee_period4 == 0) {
                        //                aprtcol.style.color = "purple";
                        //                aprtcol.style.fontSize = 25;
                        //            }
                        //            else if ($scope.temp1.sims_fee_period4 != 0 && $scope.temp1.sims_fee_period4 != 'undefined') {
                        //                aprtcol.style.color = "orange";
                        //                aprtcol.style.fontSize = 25;
                        //            }
                        //        }
                        //        else {
                        //            aprtcol.style.color = "";
                        //        }
                        //    }

                        //    else if (d == '05') {
                        //        if (maycol.value == "") {
                        //            if ($scope.temp1.sims_fee_period5 == 0) {
                        //                maycol.style.color = "purple";
                        //                maycol.style.fontSize = 25;
                        //            }
                        //            else if ($scope.temp1.sims_fee_period5 != 0 && $scope.temp1.sims_fee_period5 != 'undefined') {
                        //                maycol.style.color = "orange";
                        //                maycol.style.fontSize = 25;
                        //            }
                        //        }
                        //        else {
                        //            maycol.style.color = "";
                        //        }
                        //    }

                        //    else if (d == '06') {
                        //        if (juncol.value == "") {
                        //            if ($scope.temp1.sims_fee_period6 == 0) {
                        //                juncol.style.color = "purple";
                        //                juncol.style.fontSize = 25;
                        //            }
                        //            else if ($scope.temp1.sims_fee_period6 != 0 && $scope.temp1.sims_fee_period6 != 'undefined') {
                        //                juncol.style.color = "orange";
                        //                juncol.style.fontSize = 25;
                        //            }
                        //        }
                        //        else {
                        //            juncol.style.color = "";
                        //        }
                        //    }

                        //    else if (d == '07') {
                        //        if (julcol.value == "") {
                        //            if ($scope.temp1.sims_fee_period7 == 0) {
                        //                julcol.style.color = "purple";
                        //                julcol.style.fontSize = 25;
                        //            }
                        //            else if ($scope.temp1.sims_fee_period7 != 0 && $scope.temp1.sims_fee_period7 != 'undefined') {
                        //                julcol.style.color = "orange";
                        //                julcol.style.fontSize = 25;
                        //            }
                        //        }
                        //        else {
                        //            julcol.style.color = "";
                        //        }
                        //    }

                        //    else if (d == '08') {
                        //        if (augcol.value == "") {
                        //            if ($scope.temp1.sims_fee_period8 == 0) {
                        //                augcol.style.color = "purple";
                        //                augcol.style.fontSize = 25;
                        //            }
                        //            else if ($scope.temp1.sims_fee_period8 != 0 && $scope.temp1.sims_fee_period8 != 'undefined') {
                        //                augcol.style.color = "orange";
                        //                augcol.style.fontSize = 25;
                        //            }
                        //        }
                        //        else {
                        //            augcol.style.color = "";
                        //        }
                        //    }

                        //    else if (d == '09') {
                        //        if (sepcol.value == "") {
                        //            if ($scope.temp1.sims_fee_period9 == 0) {
                        //                sepcol.style.color = "purple";
                        //                sepcol.style.fontSize = 25;
                        //            }
                        //            else if ($scope.temp1.sims_fee_period9 != 0 && $scope.temp1.sims_fee_period9 != 'undefined') {
                        //                sepcol.style.color = "orange";
                        //                sepcol.style.fontSize = 25;
                        //            }
                        //        }
                        //        else {
                        //            sepcol.style.color = "";
                        //        }
                        //    }

                        //    else if (d == '10') {
                        //        if (octcol.value == "") {
                        //            if ($scope.temp1.sims_fee_period10 == 0) {
                        //                octcol.style.color = "purple";
                        //                octcol.style.fontSize = 25;
                        //            }
                        //            else if ($scope.temp1.sims_fee_period10 != 0 && $scope.temp1.sims_fee_period10 != 'undefined') {
                        //                octcol.style.color = "orange";
                        //                octcol.style.fontSize = 25;
                        //            }
                        //        }
                        //        else {
                        //            octcol.style.color = "";
                        //        }
                        //    }

                        //    else if (d == '11') {
                        //        if (novcol.value == "") {
                        //            if ($scope.temp1.sims_fee_period11 == 0) {
                        //                novcol.style.color = "purple";
                        //                novcol.style.fontSize = 25;
                        //            }
                        //            else if ($scope.temp1.sims_fee_period11 != 0 && $scope.temp1.sims_fee_period11 != 'undefined') {
                        //                novcol.style.color = "orange";
                        //                novcol.style.fontSize = 25;
                        //            }
                        //        }
                        //        else {
                        //            novcol.style.color = "";
                        //        }
                        //    }

                        //    else if (d == '12') {
                        //        if (deccol.value == "") {
                        //            if ($scope.temp1.sims_fee_period12 == 0) {
                        //                deccol.style.color = "purple";
                        //                deccol.style.fontSize = 25;
                        //            }
                        //            else if ($scope.temp1.sims_fee_period12 != 0 && $scope.temp1.sims_fee_period12 != 'undefined') {
                        //                deccol.style.color = "orange";
                        //                deccol.style.fontSize = 25;
                        //            }
                        //        }
                        //        else {
                        //            deccol.style.color = "";
                        //        }
                        //    }

                        //    check_date.add(1, 'months');
                        //    check_date = moment(check_date).format('YYYY/MM/DD');
                        //}

                        //

                        //if (jancol.value == "0.0000") {
                        //    $scope.temp1.sims_fee_period1 = "0.0000";
                        //    jancol.style.color = "purple";
                        //}

                        //if (febcol.value == "0.0000") {
                        //    $scope.temp1.sims_fee_period2 = "0.0000";
                        //    jancol.style.color = "purple";
                        //}
                        //if (marcol.value == "0.0000") {
                        //    $scope.temp1.sims_fee_period3 = "0.0000";
                        //    jancol.style.color = "purple";
                        //}

                        //if (aprtcol.value == "0.0000") {
                        //    $scope.temp1.sims_fee_period4 = "0.0000";
                        //    jancol.style.color = "purple";
                        //}

                        //if (maycol.value == "0.0000") {
                        //    $scope.temp1.sims_fee_period5 = "0.0000";
                        //    jancol.style.color = "purple";
                        //}

                        //if (juncol.value == "0.0000") {
                        //    $scope.temp1.sims_fee_period6 = "0.0000";
                        //    jancol.style.color = "purple";
                        //}

                        //if (julcol.value == "0.0000") {
                        //    $scope.temp1.sims_fee_period7 = "0.0000";
                        //    jancol.style.color = "purple";
                        //}

                        //if (augcol.value == "0.0000") {
                        //    $scope.temp1.sims_fee_period8 = "0.0000";
                        //    jancol.style.color = "purple";
                        //}

                        //if (sepcol.value == "0.0000") {
                        //    $scope.temp1.sims_fee_period9 = "0.0000";
                        //    jancol.style.color = "purple";
                        //}

                        //if (octcol.value == "0.0000") {
                        //    $scope.temp1.sims_fee_period10 = "0.0000";
                        //    jancol.style.color = "purple";
                        //}

                        //if (novcol.value == "0.0000") {
                        //    $scope.temp1.sims_fee_period11 = "0.0000";
                        //    jancol.style.color = "purple";
                        //}

                        //if (deccol.value == "0.0000") {
                        //    $scope.temp1.sims_fee_period12 = "0.0000";
                        //    jancol.style.color = "purple";
                        //}

                    });
                }
            }
            else {

                swal({ text: "Please Select Start Date And End Date...!", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
            }
        }

        $scope.Update = function () {
            var data1 = [];
            var sub_opr1 = null;
            
            if (check.checked == true) {
                sub_opr1 = 'A';
            }
            else if ($scope.sims_transport_route_code_old == $scope.temp.sims_transport_route_code && $scope.sims_transport_route_direction_old == $scope.temp.sims_transport_route_direction && $scope.v_Code_old == $scope.temp.v_Code && check.checked == false) {
                sub_opr1 = 'B';
            }
            else if ($scope.sims_transport_route_code_old == $scope.temp.sims_transport_route_code && $scope.sims_transport_route_direction_old != $scope.temp.sims_transport_route_direction && $scope.v_Code_old == $scope.temp.v_Code && check.checked == false) {
                sub_opr1 = 'C';
            }
            else if (($scope.v_Code_old != $scope.temp.v_Code || $scope.sims_transport_route_code_old != $scope.temp.sims_transport_route_code) && check.checked == false) {
                sub_opr1 = 'E';
            }
          
            var updaterecord = {

                sims_transport_academic_year: $scope.temp.sims_academic_year,
                sims_transport_enroll_number: $scope.temp.sims_enroll_number,
                sims_transport_route_student_code: sims_transport_route_student_code,
                sims_appl_parameter: sims_appl_parameter,
                sims_fee_cur_code: $scope.temp.sims_fee_cur_code,
                sims_fee_grade_code: $scope.temp.sims_fee_grade_code,
                sims_fee_section_code: $scope.temp.sims_fee_section_code,
                sims_fee_number: $scope.temp.sims_fee_number,

                sims_transport_route_code: $scope.temp.sims_transport_route_code,
                sims_transport_route_direction: $scope.temp.sims_transport_route_direction,
                sims_transport_effective_from: $scope.temp.sims_transport_effective_from1,
                sims_transport_effective_upto: $scope.temp.sims_transport_effective_upto1,

                sims_transport_route_direction_old: $scope.sims_transport_route_direction_old,
                sims_transport_effective_from_old: $scope.temp.sims_transport_effective_from,
                sims_transport_effective_upto_old: $scope.temp.sims_transport_effective_upto,
                sims_transport_pickup_stop_code_old: $scope.sims_transport_pickup_stop_code_old,
                sims_transport_drop_stop_code_old: $scope.sims_transport_drop_stop_code_old,
                sims_transport_route_code_old: $scope.sims_transport_route_code_old,
                v_Code: $scope.v_Code_old,
                v_Name_Code: $scope.temp.v_Code,

                sims_transport_pickup_stop_code: $scope.temp.sims_transport_pickup_stop_code,
                sims_transport_drop_stop_code: $scope.temp.sims_transport_drop_stop_code,

                sims_transport_period1: jantp,
                sims_transport_period2: febtp,
                sims_transport_period3: martp,
                sims_transport_period4: aprtp,
                sims_transport_period5: maytp,
                sims_transport_period6: juntp,
                sims_transport_period7: jultp,
                sims_transport_period8: augtp,
                sims_transport_period9: septp,
                sims_transport_period10: octtp,
                sims_transport_period11: novtp,
                sims_transport_period12: dectp,

                sims_fee_period1: $scope.temp1.sims_fee_period1,
                sims_fee_period2: $scope.temp1.sims_fee_period2,
                sims_fee_period3: $scope.temp1.sims_fee_period3,
                sims_fee_period4: $scope.temp1.sims_fee_period4,
                sims_fee_period5: $scope.temp1.sims_fee_period5,
                sims_fee_period6: $scope.temp1.sims_fee_period6,
                sims_fee_period7: $scope.temp1.sims_fee_period7,
                sims_fee_period8: $scope.temp1.sims_fee_period8,
                sims_fee_period9: $scope.temp1.sims_fee_period9,
                sims_fee_period10: $scope.temp1.sims_fee_period10,
                sims_fee_period11: $scope.temp1.sims_fee_period11,
                sims_fee_period12: $scope.temp1.sims_fee_period12,

                sims_fee_period1_paid: janfp,
                sims_fee_period2_paid: febfp,
                sims_fee_period3_paid: marfp,
                sims_fee_period4_paid: aprfp,
                sims_fee_period5_paid: mayfp,
                sims_fee_period6_paid: junfp,
                sims_fee_period7_paid: julfp,
                sims_fee_period8_paid: augfp,
                sims_fee_period9_paid: sepfp,
                sims_fee_period10_paid: octfp,
                sims_fee_period11_paid: novfp,
                sims_fee_period12_paid: decfp,
                sims_transport_toatal:$scope.total_fee,
                sims_png: $rootScope.globals.currentUser.username,
                opr: 'U',
                sub_opr: sub_opr1,

            }
            data1.push(updaterecord);
            $http.post(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/UpdateTransportCUD", data1).then(function (msg) {
                debugger;
                $scope.msg1 = msg.data;

                if ($scope.msg1 == true) {
                    swal({ text: "Transport Update Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                }
                else if ($scope.msg1 == false) {
                    swal({ text: "Transport Not Update. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                }
                else {
                    swal("Error-" + $scope.msg1)
                }

                $('#MyModal').modal('hide');
                $scope.searchtable = true;
                $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getStudentTransportDetails?cur_code=" + $scope.edt.sims_cur_code + "&acayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&enroll=" + $scope.edt.sims_enroll_number + "&vehicle_code=" + $scope.edt.sims_transport_vehicle_code).then(function (getStudentTransportDetails_Data) {
                    $scope.StudentTransportDetails = getStudentTransportDetails_Data.data;
                    $scope.totalItems = $scope.StudentTransportDetails.length;
                    $scope.todos = $scope.StudentTransportDetails;
                    $scope.makeTodos();
                });
            });
        }

        $scope.CancelTransport = function () {
            debugger

            if ($scope.temp.valid_date != "" && $scope.temp.valid_date != undefined) {
                if ($scope.temp.valid_date != null) {
                    var cancelrecord = {

                        sims_transport_academic_year: $scope.temp.sims_academic_year,
                        sims_transport_enroll_number: $scope.temp.sims_enroll_number,

                        sims_fee_cur_code: $scope.temp.sims_fee_cur_code,
                        sims_fee_grade_code: $scope.temp.sims_fee_grade_code,
                        sims_fee_section_code: $scope.temp.sims_fee_section_code,
                        sims_fee_number: $scope.temp.sims_fee_number,

                        sims_transport_route_code: $scope.temp.sims_transport_route_code,
                        sims_transport_route_direction: $scope.temp.sims_transport_route_direction,
                        sims_transport_effective_from: $scope.temp.sims_transport_effective_from1,
                        sims_transport_effective_upto: $scope.temp.sims_transport_effective_upto1,

                        sims_transport_route_direction_old: $scope.sims_transport_route_direction_old,

                        //sims_transport_effective_from_old: $scope.temp.sims_transport_effective_from,
                        //sims_transport_effective_upto_old: $scope.temp.sims_transport_effective_upto,

                        sims_transport_effective_from_old: $scope.temp.sims_transport_effective_from1,
                        sims_transport_effective_upto_old: $scope.temp.sims_transport_effective_upto1,

                        sims_transport_pickup_stop_code_old: $scope.sims_transport_pickup_stop_code_old,
                        sims_transport_drop_stop_code_old: $scope.sims_transport_drop_stop_code_old,
                        sims_transport_route_code_old: $scope.sims_transport_route_code_old,
                        v_Code: $scope.v_Code_old,
                        v_Name_Code: $scope.temp.v_Code,

                        sims_transport_pickup_stop_code: $scope.temp.sims_transport_pickup_stop_code,
                        sims_transport_drop_stop_code: $scope.temp.sims_transport_drop_stop_code,

                        sims_transport_route_rejecte_date: $scope.temp.valid_date,

                        sims_fee_period1: $scope.temp1.sims_fee_period1,
                        sims_fee_period2: $scope.temp1.sims_fee_period2,
                        sims_fee_period3: $scope.temp1.sims_fee_period3,
                        sims_fee_period4: $scope.temp1.sims_fee_period4,
                        sims_fee_period5: $scope.temp1.sims_fee_period5,
                        sims_fee_period6: $scope.temp1.sims_fee_period6,
                        sims_fee_period7: $scope.temp1.sims_fee_period7,
                        sims_fee_period8: $scope.temp1.sims_fee_period8,
                        sims_fee_period9: $scope.temp1.sims_fee_period9,
                        sims_fee_period10: $scope.temp1.sims_fee_period10,
                        sims_fee_period11: $scope.temp1.sims_fee_period11,
                        sims_fee_period12: $scope.temp1.sims_fee_period12,

                        sims_transport_period1: jantp,
                        sims_transport_period2: febtp,
                        sims_transport_period3: martp,
                        sims_transport_period4: aprtp,
                        sims_transport_period5: maytp,
                        sims_transport_period6: juntp,
                        sims_transport_period7: jultp,
                        sims_transport_period8: augtp,
                        sims_transport_period9: septp,
                        sims_transport_period10: octtp,
                        sims_transport_period11: novtp,
                        sims_transport_period12: dectp,
                        sims_check_month_wise: false,
                        sims_transport_route_student_update_by: $rootScope.globals.currentUser.username,
                        opr: 'U',
                        sub_opr: 'G'

                    }

                    $http.post(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/CancelTransport", cancelrecord).then(function (msg) {
                        $scope.msg1 = msg.data;
                        swal({ text: "Transport Cancel Successfully </b>" + 'From ' + $scope.temp.valid_date + ' to ' + $scope.temp.sims_transport_effective_upto1, imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });

                        $('#MyModal').modal('hide');
                        $scope.searchtable = true;
                        $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getStudentTransportDetails?cur_code=" + $scope.edt.sims_cur_code + "&acayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&enroll=" + $scope.edt.sims_enroll_number + "&vehicle_code=" + $scope.edt.sims_transport_vehicle_code).then(function (getStudentTransportDetails_Data) {
                            $scope.StudentTransportDetails = getStudentTransportDetails_Data.data;
                            $scope.totalItems = $scope.StudentTransportDetails.length;
                            $scope.todos = $scope.StudentTransportDetails;
                            $scope.makeTodos();
                        });
                    });
                }
            }

            else if (checkmonthwise.checked == true) {

                if ((cp1 != 0 || cp2 != 0 || cp3 != 0 || cp4 != 0 || cp5 != 0 || cp6 != 0 || cp7 != 0 || cp8 != 0 || cp9 != 0 || cp10 != 0 || cp11 != 0 || cp12 != 0)) {
                    var pc = (cp1 == 1 ? "1," : "") + (cp2 == 1 ? "2," : "") + (cp3 == 1 ? "3," : "") + (cp4 == 1 ? "4," : "") + (cp5 == 1 ? "5," : "") + (cp6 == 1 ? "6," : "");
                    pc = pc + (cp7 == 1 ? "7," : "") + (cp8 == 1 ? "8," : "") + (cp9 == 1 ? "9," : "") + (cp10 == 1 ? "10," : "") + (cp11 == 1 ? "11," : "") + (cp12 == 1 ? "12," : "");
                    var cancelrecord = {

                        sims_transport_academic_year: $scope.temp.sims_academic_year,
                        sims_transport_enroll_number: $scope.temp.sims_enroll_number,

                        sims_fee_cur_code: $scope.temp.sims_fee_cur_code,
                        sims_fee_grade_code: $scope.temp.sims_fee_grade_code,
                        sims_fee_section_code: $scope.temp.sims_fee_section_code,
                        sims_fee_number: $scope.temp.sims_fee_number,

                        sims_transport_route_code: $scope.temp.sims_transport_route_code,
                        sims_transport_route_direction: $scope.temp.sims_transport_route_direction,
                        sims_transport_effective_from: $scope.temp.sims_transport_effective_from1,
                        sims_transport_effective_upto: $scope.temp.sims_transport_effective_upto1,

                        sims_transport_route_direction_old: $scope.sims_transport_route_direction_old,
                        //sims_transport_effective_from_old: $scope.temp.sims_transport_effective_from,
                        //sims_transport_effective_upto_old: $scope.temp.sims_transport_effective_upto,
                        sims_transport_effective_from_old: $scope.temp.sims_transport_effective_from1,
                        sims_transport_effective_upto_old: $scope.temp.sims_transport_effective_upto1,

                        sims_transport_pickup_stop_code_old: $scope.sims_transport_pickup_stop_code_old,
                        sims_transport_drop_stop_code_old: $scope.sims_transport_drop_stop_code_old,
                        sims_transport_route_code_old: $scope.sims_transport_route_code_old,
                        v_Code: $scope.v_Code_old,
                        v_Name_Code: $scope.temp.v_Code,

                        sims_transport_pickup_stop_code: $scope.temp.sims_transport_pickup_stop_code,
                        sims_transport_drop_stop_code: $scope.temp.sims_transport_drop_stop_code,

                        sims_transport_route_rejecte_date: $scope.temp.valid_date,

                        sims_fee_period1: $scope.temp1.sims_fee_period1,
                        sims_fee_period2: $scope.temp1.sims_fee_period2,
                        sims_fee_period3: $scope.temp1.sims_fee_period3,
                        sims_fee_period4: $scope.temp1.sims_fee_period4,
                        sims_fee_period5: $scope.temp1.sims_fee_period5,
                        sims_fee_period6: $scope.temp1.sims_fee_period6,
                        sims_fee_period7: $scope.temp1.sims_fee_period7,
                        sims_fee_period8: $scope.temp1.sims_fee_period8,
                        sims_fee_period9: $scope.temp1.sims_fee_period9,
                        sims_fee_period10: $scope.temp1.sims_fee_period10,
                        sims_fee_period11: $scope.temp1.sims_fee_period11,
                        sims_fee_period12: $scope.temp1.sims_fee_period12,

                        sims_transport_period1: jantp,
                        sims_transport_period2: febtp,
                        sims_transport_period3: martp,
                        sims_transport_period4: aprtp,
                        sims_transport_period5: maytp,
                        sims_transport_period6: juntp,
                        sims_transport_period7: jultp,
                        sims_transport_period8: augtp,
                        sims_transport_period9: septp,
                        sims_transport_period10: octtp,
                        sims_transport_period11: novtp,
                        sims_transport_period12: dectp,

                        sims_fee_period1_paid: cp1,
                        sims_fee_period2_paid: cp2,
                        sims_fee_period3_paid: cp3,
                        sims_fee_period4_paid: cp4,
                        sims_fee_period5_paid: cp5,
                        sims_fee_period6_paid: cp6,
                        sims_fee_period7_paid: cp7,
                        sims_fee_period8_paid: cp8,
                        sims_fee_period9_paid: cp9,
                        sims_fee_period10_paid: cp10,
                        sims_fee_period11_paid: cp11,
                        sims_fee_period12_paid: cp12,
                        sims_transport_route_student_code: pc,
                        sims_check_month_wise: true,
                        sims_transport_route_student_update_by: $rootScope.globals.currentUser.username,
                        opr: 'U',
                        sub_opr: 'P'

                    }

                    $http.post(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/CancelTransport", cancelrecord).then(function (msg) {
                        $scope.msg1 = msg.data;
                        swal({ text: "Transport Cancel Successfully ", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                        $('#MyModal').modal('hide');
                        $scope.searchtable = true;
                        $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getStudentTransportDetails?cur_code=" + $scope.edt.sims_cur_code + "&acayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&enroll=" + $scope.edt.sims_enroll_number + "&vehicle_code=" + $scope.edt.sims_transport_vehicle_code).then(function (getStudentTransportDetails_Data) {
                            $scope.StudentTransportDetails = getStudentTransportDetails_Data.data;
                            $scope.totalItems = $scope.StudentTransportDetails.length;
                            $scope.todos = $scope.StudentTransportDetails;
                            $scope.makeTodos();
                        });

                    });
                }
            }

            else if (($scope.temp.valid_date == "" || $scope.temp.valid_date != undefined) || checkmonthwise.checked == false) {
                if ($scope.temp.valid_date == null || checkmonthwise.checked == false) {
                    var cancelrecord = {

                        sims_transport_academic_year: $scope.temp.sims_academic_year,
                        sims_transport_enroll_number: $scope.temp.sims_enroll_number,

                        sims_fee_cur_code: $scope.temp.sims_fee_cur_code,
                        sims_fee_grade_code: $scope.temp.sims_fee_grade_code,
                        sims_fee_section_code: $scope.temp.sims_fee_section_code,
                        sims_fee_number: $scope.temp.sims_fee_number,

                        sims_transport_route_code: $scope.temp.sims_transport_route_code,
                        sims_transport_route_direction: $scope.temp.sims_transport_route_direction,
                        //sims_transport_effective_from: $scope.temp.sims_transport_effective_from1,
                        //sims_transport_effective_upto: $scope.temp.sims_transport_effective_upto1,

                        sims_transport_route_direction_old: $scope.sims_transport_route_direction_old,

                        //sims_transport_effective_from_old: $scope.temp.sims_transport_effective_from,
                        //sims_transport_effective_upto_old: $scope.temp.sims_transport_effective_upto,

                        sims_transport_effective_from_old: $scope.temp.sims_transport_effective_from1,
                        sims_transport_effective_upto_old: $scope.temp.sims_transport_effective_upto1,

                        sims_transport_pickup_stop_code_old: $scope.sims_transport_pickup_stop_code_old,
                        sims_transport_drop_stop_code_old: $scope.sims_transport_drop_stop_code_old,
                        sims_transport_route_code_old: $scope.sims_transport_route_code_old,
                        v_Code: $scope.v_Code_old,
                        v_Name_Code: $scope.temp.v_Code,

                        sims_transport_pickup_stop_code: $scope.temp.sims_transport_pickup_stop_code,
                        sims_transport_drop_stop_code: $scope.temp.sims_transport_drop_stop_code,
                       // sims_transport_route_student_code:
                        //sims_transport_route_rejecte_date: "",

                        sims_fee_period1: $scope.temp1.sims_fee_period1,
                        sims_fee_period2: $scope.temp1.sims_fee_period2,
                        sims_fee_period3: $scope.temp1.sims_fee_period3,
                        sims_fee_period4: $scope.temp1.sims_fee_period4,
                        sims_fee_period5: $scope.temp1.sims_fee_period5,
                        sims_fee_period6: $scope.temp1.sims_fee_period6,
                        sims_fee_period7: $scope.temp1.sims_fee_period7,
                        sims_fee_period8: $scope.temp1.sims_fee_period8,
                        sims_fee_period9: $scope.temp1.sims_fee_period9,
                        sims_fee_period10: $scope.temp1.sims_fee_period10,
                        sims_fee_period11: $scope.temp1.sims_fee_period11,
                        sims_fee_period12: $scope.temp1.sims_fee_period12,

                        sims_transport_period1: jantp,
                        sims_transport_period2: febtp,
                        sims_transport_period3: martp,
                        sims_transport_period4: aprtp,
                        sims_transport_period5: maytp,
                        sims_transport_period6: juntp,
                        sims_transport_period7: jultp,
                        sims_transport_period8: augtp,
                        sims_transport_period9: septp,
                        sims_transport_period10: octtp,
                        sims_transport_period11: novtp,
                        sims_transport_period12: dectp,
                        sims_check_month_wise: false,
                        sims_transport_route_student_update_by: $rootScope.globals.currentUser.username,
                        opr: 'U',
                        sub_opr: 'G'

                    }

                    $http.post(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/CancelTransport", cancelrecord).then(function (msg) {
                        $scope.msg1 = msg.data;
                        //swal({ title: "Alert", text: "Transport Cancel Successfully ", showCloseButton: true, width: 380, });
                        swal({ text: "Transport Cancel Successfully </b>" + 'From ' + $scope.temp.sims_transport_effective_from + ' to ' + $scope.temp.sims_transport_effective_upto1, imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });

                        $('#MyModal').modal('hide');
                        $scope.searchtable = true;
                        $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getStudentTransportDetails?cur_code=" + $scope.edt.sims_cur_code + "&acayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&enroll=" + $scope.edt.sims_enroll_number + "&vehicle_code=" + $scope.edt.sims_transport_vehicle_code).then(function (getStudentTransportDetails_Data) {
                            $scope.StudentTransportDetails = getStudentTransportDetails_Data.data;
                            $scope.totalItems = $scope.StudentTransportDetails.length;
                            $scope.todos = $scope.StudentTransportDetails;
                            $scope.makeTodos();
                        });
                    });
                }
            }
            else {
                swal({ text: "Transport Not Cancel", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
            }
        }

        $('*[data-datepicker="true"] input[type="text"]').datepicker({

            todayBtn: true,
            orientation: "top left",
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy',
        });

        $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
            $('input[type="text"]', $(this).parent()).focus();
        });

        $('*[data-datepicker1="true"] input[type="text"]').datepicker({

            todayBtn: true,
            orientation: "top left",
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy',
            startDate: new Date($scope.startdt),
            endDate: new Date($scope.enddt)
        });

        $(document).on('touch click', '*[data-datepicker1="true"] .add-on', function (e) {
            $('input[type="text"]', $(this).parent()).focus();
        });

        $scope.Comparedate = function () {

            if ($scope.temp.sims_transport_effective_upto1 < $scope.temp.sims_transport_effective_from1) {
                swal({ title: "Alert", text: "End Date Should greater than Start Date", width: 300, height: 200 });
            }
            else if ($scope.temp.valid_date != undefined) {
                if ($scope.temp.valid_date != "") {
                    $scope.CheckDetails();
                }
                else {
                    $scope.CheckMonthWise();
                }
            }
        }

        $scope.searched = function (valLists, toSearch) {
            return _.filter(valLists,
            function (i) {
                /* Search Text in all  fields */
                return searchUtil(i, toSearch);
            });
        };

        $scope.search = function () {
            $scope.todos = $scope.searched($scope.StudentTransportDetails, $scope.searchText);
            $scope.totalItems = $scope.todos.length;
            $scope.currentPage = '1';
            if ($scope.searchText == '') {
                $scope.todos = $scope.StudentTransportDetails;
            }
            $scope.makeTodos();
        }

        function searchUtil(item, toSearch) {
            /* Search Text in all 3 fields */

            return (
                 item.v_Name_Code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                 item.sims_student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                item.sims_enroll_number == toSearch) ? true : false;


        }

        $timeout(function () {
            $("#fixTable").tableHeadFixer({ 'top': 1 });
        }, 100);
        $timeout(function () {
            $("#fixTable1").tableHeadFixer({ 'top': 1 });
        }, 100);


        //-------------------------Employee_Cancel_Update----------------------------------------//

        $scope.std_grade_search_tab = function () {
            $scope.searchtable = false;
            $scope.searchtable_employee = false;
            $scope.searchtable_Buswise = false;
            $("#cmb_bus").select2("val", "");
            $("#Select6").select2("val", "");
            $("#Select17").select2("val", "");
            $("#Select19").select2("val", "");
            $scope.Reset();
        }

        $scope.std_bus_search_tab = function () {
            $scope.searchtable = false;
            $scope.searchtable_employee = false;
            $scope.searchtable_Buswise = false;
            $("#cmb_bus").select2("val", "");
            $("#Select6").select2("val", "");
            $("#Select17").select2("val", "");
            $("#Select19").select2("val", "");
            $scope.Reset();
        }

        $scope.std_buswise_transfer_tab = function () {
            $scope.searchtable = false;
            $scope.searchtable_employee = false;
            $scope.searchtable_Buswise = false;
            $("#cmb_bus").select2("val", "");
            $("#Select6").select2("val", "");
            $("#Select17").select2("val", "");
            $("#Select19").select2("val", "");
            $scope.ResetBuswisetrans();
        }
        
        $scope.emp_search_tab = function () {
            $scope.searchtable = false;
            $scope.searchtable_employee = false;
            $scope.searchtable_Buswise = false;
            $("#cmb_bus").select2("val", "");
            $("#Select6").select2("val", "");
            $("#Select17").select2("val", "");
            $("#Select19").select2("val", "");
            $scope.Reset_Employee();
        }

        $scope.size1 = function (str) {
            $scope.pagesize1 = str;
            $scope.currentPage1 = 1;
            $scope.numPerPage1 = str;  $scope.makeTodos1();
        }

        $scope.index1 = function (str) {
            $scope.pageindex1 = str;
            $scope.currentPage1 = str;             $scope.makeTodos1();
            main.checked = false;
            $scope.row1 = '';
        }

        $scope.filteredTodos1 = [], $scope.currentPage1 = 1, $scope.numPerPage1 = 5, $scope.maxSize1 = 5;

        $scope.makeTodos1 = function () {
            var rem1 = parseInt($scope.totalItems1 % $scope.numPerPage1);
            if (rem1 == '0') {
                $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1);
            }
            else {
                $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1) + 1;
            }
            var begin1 = (($scope.currentPage1 - 1) * $scope.numPerPage1);
            var end1 = parseInt(begin1) + parseInt($scope.numPerPage1);

           

            $scope.filteredTodos1 = $scope.todos1.slice(begin1, end1);
        };

        $scope.searched1 = function (valLists, toSearch) {
            return _.filter(valLists,
            function (i) {
                return searchUtil1(i, toSearch);
            });
        };

        $scope.search1 = function () {
            $scope.todos1 = $scope.searched1($scope.EmployeeTransportDetails_Data, $scope.searchText);
            $scope.totalItems = $scope.todos1.length;
            $scope.currentPage1 = '1';
            if ($scope.searchText == '') {
                $scope.todos1 = $scope.EmployeeTransportDetails_Data;
            }
            $scope.makeTodos1();
        }

        function searchUtil1(item, toSearch) {
            return (
                item.employee_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                item.sims_transport_route_direction_1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                item.sims_transport_em_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                item.sims_transport_route_name == toSearch) ? true : false;

        }

        $scope.Reset_Employee = function () {

            $scope.edt = {
                //sims_academic_year: '',
                sims_transport_vehicle_code: '',
                sims_transport_em_number: ''
            }
            $("#Select6").select2("val", "");
            $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getAcademicYear").then(function (getAcademicYear) {
                $scope.AcademicYear_data = getAcademicYear.data;
                $scope.edt = { sims_academic_year: $scope.AcademicYear_data[0].sims_academic_year };
            });
        }

        $scope.Show_Data_Employee = function (acayear, common_user_code, vehicle_code) {
            $scope.pagersize = '10';
            $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getEmployeeTransportDetails?acayear=" + $scope.edt.sims_academic_year + "&common_user_code=" + $scope.edt.sims_transport_em_number + "&vehicle_code=" + $scope.edt.sims_transport_vehicle_code).then(function (getEmployeeTransportDetails_Data) {
                $scope.EmployeeTransportDetails_Data = getEmployeeTransportDetails_Data.data;
                $scope.totalItems1 = $scope.EmployeeTransportDetails_Data.length;
                $scope.todos1 = $scope.EmployeeTransportDetails_Data;
                $scope.makeTodos1();

                if (getEmployeeTransportDetails_Data.data.length > 0) {
                    $scope.sims_transport_route_code_old = getEmployeeTransportDetails_Data.data[0].sims_transport_route_code;

                    $scope.sims_transport_route_direction_old = getEmployeeTransportDetails_Data.data[0].sims_transport_route_direction;
                    $scope.v_Code_old = getEmployeeTransportDetails_Data.data[0].v_Code;

                    $scope.sims_transport_effective_from_old = getEmployeeTransportDetails_Data.data[0].sims_transport_effective_from;
                    $scope.sims_transport_effective_upto_old = getEmployeeTransportDetails_Data.data[0].sims_transport_effective_upto;

                    $scope.sims_transport_pickup_stop_code_old = getEmployeeTransportDetails_Data.data[0].sims_transport_pickup_stop_code;
                    $scope.sims_transport_drop_stop_code_old = getEmployeeTransportDetails_Data.data[0].sims_transport_drop_stop_code;
                    $scope.sims_transport_seat_number = getEmployeeTransportDetails_Data.data[0].sims_transport_seat_number;
                    $scope.searchtable = false;
                    $scope.searchtable_employee = true;

                }
                else {
                    $scope.searchtable = false;
                    $scope.searchtable_employee = false;
                    swal({ title: "Alert", text: "Student not using the bus..!", width: 300, height: 100 });
                }
            });

        }

        $scope.edit_employee = function (emp) {

            $scope.edtemp = {
                updatetransport_emp: false,
            }

            $scope.temp = {

                sims_academic_year: emp.sims_transport_academic_year,
                sims_transport_em_number: emp.sims_transport_em_number,
                pays_employee_name: emp.pays_employee_name,
                sims_transport_route_code: emp.sims_transport_route_code,
                sims_transport_route_name: emp.sims_transport_route_name,
                sims_transport_route_direction: emp.sims_transport_route_direction,
                sims_transport_route_direction_1: emp.sims_transport_route_direction_1,
                sims_transport_pickup_stop_code: emp.sims_transport_pickup_stop_code,
                sims_transport_drop_stop_code: emp.sims_transport_drop_stop_code,
                sims_transport_effective_from: emp.sims_transport_effective_from,
                sims_transport_effective_upto: emp.sims_transport_effective_upto,

                v_Code: emp.v_Code,
            };
            $scope.sims_transport_seat_number = emp.sims_transport_seat_number;
            $scope.sims_transport_route_employee_code = emp.sims_transport_route_employee_code;

            $scope.getroutebybuscode(emp.v_Code, emp.sims_transport_academic_year);

            $scope.getStopbyRoute(emp.sims_transport_route_code, emp.sims_transport_academic_year);

            $scope.disable_update_employee = true;

            $scope.cancel_btn_emp = true;
            $scope.update_btn_emp = true;

            $scope.disabled_bus_code = true;
            $scope.disabled_route_code = true;
            $scope.disabled_route_direction = true;
            $scope.disabled_drop_stop = true;
            $scope.disabled_pick_up = true;


            $('#MyModalEmployee').modal('show');

        }

        $scope.ModalEmployeeClear = function () {

            $scope.temp = [];
            $('#MyModalEmployee').modal('hide');
        }

        $scope.update_transport = function () {

            var update_transport_emp = document.getElementById('updatetransport');
            if (update_transport_emp.checked == true) {

                $scope.disable_cancel_employee = true;
                $scope.disable_update_employee = false;

                $scope.disabled_bus_code = false;
                $scope.disabled_route_code = false;
                $scope.disabled_route_direction = false;
                $scope.disabled_drop_stop = false;
                $scope.disabled_pick_up = false;
                $scope.start_dt_forupdate_trans = false;
                $scope.end_dt_update_trans = false;
                $scope.from_dt_cancel_trans = true;
            }
            else {
                $scope.disable_cancel_employee = false;
                $scope.disable_update_employee = true;
                $scope.disabled_bus_code = true;
                $scope.disabled_route_code = true;
                $scope.disabled_route_direction = true;
                $scope.disabled_drop_stop = true;
                $scope.disabled_pick_up = true;
                $scope.start_dt_forupdate_trans = true;
                $scope.end_dt_update_trans = true;
                $scope.from_dt_cancel_trans = false;
            }

        }

        $scope.UpdateEmployeetransport = function () {

            if ($scope.temp.sims_transport_effective_from1 && $scope.temp.sims_transport_effective_upto1 != null ||
                $scope.temp.sims_transport_effective_from1 && $scope.temp.sims_transport_effective_upto1 != "") {
                var data1 = [];
                var sub_opr1 = null;
                if (($scope.temp.sims_transport_effective_from == $scope.temp.sims_transport_effective_from1 &&
                    $scope.temp.sims_transport_effective_upto == $scope.temp.sims_transport_effective_upto1 &&
                    $scope.v_Code_old == $scope.temp.v_Code && $scope.sims_transport_route_code_old == $scope.temp.sims_transport_route_code &&
                    $scope.sims_transport_route_direction_old != $scope.temp.sims_transport_route_direction) ||

                    ($scope.temp.sims_transport_effective_from == $scope.temp.sims_transport_effective_from1 &&
                    $scope.temp.sims_transport_effective_upto == $scope.temp.sims_transport_effective_upto1 &&
                    $scope.v_Code_old != $scope.temp.v_Code && $scope.sims_transport_route_code_old != $scope.temp.sims_transport_route_code &&
                    (($scope.sims_transport_route_direction_old != $scope.temp.sims_transport_route_direction) ||
                    ($scope.sims_transport_route_direction_old == $scope.temp.sims_transport_route_direction)))) {
                    sub_opr1 = 'H';
                }

                else if ($scope.temp.sims_transport_effective_from != $scope.temp.sims_transport_effective_from1 &&
                         $scope.temp.sims_transport_effective_upto != $scope.temp.sims_transport_effective_upto1 &&
                         $scope.v_Code_old == $scope.temp.v_Code &&
                         $scope.sims_transport_route_code_old == $scope.temp.sims_transport_route_code &&
                         $scope.sims_transport_route_direction_old == $scope.temp.sims_transport_route_direction) {
                    sub_opr1 = 'I';
                }

                else if (($scope.temp.sims_transport_effective_from == $scope.temp.sims_transport_effective_from1 &&
                         $scope.temp.sims_transport_effective_upto != $scope.temp.sims_transport_effective_upto1 &&
                         $scope.v_Code_old == $scope.temp.v_Code && $scope.sims_transport_route_code_old == $scope.temp.sims_transport_route_code &&
                         $scope.sims_transport_route_direction_old != $scope.temp.sims_transport_route_direction) ||

                         ($scope.temp.sims_transport_effective_from == $scope.temp.sims_transport_effective_from1 &&
                         $scope.temp.sims_transport_effective_upto != $scope.temp.sims_transport_effective_upto1 &&
                         $scope.v_Code_old != $scope.temp.v_Code && $scope.sims_transport_route_code_old != $scope.temp.sims_transport_route_code &&
                         (($scope.sims_transport_route_direction_old != $scope.temp.sims_transport_route_direction) ||
                          ($scope.sims_transport_route_direction_old == $scope.temp.sims_transport_route_direction)))) {
                    sub_opr1 = 'J';
                }

                else if ($scope.temp.sims_transport_effective_from != $scope.temp.sims_transport_effective_from1 &&
                      $scope.temp.sims_transport_effective_upto != $scope.temp.sims_transport_effective_upto1 &&
                      $scope.v_Code_old == $scope.temp.v_Code && $scope.sims_transport_route_code_old == $scope.temp.sims_transport_route_code &&
                      $scope.sims_transport_route_direction_old != $scope.temp.sims_transport_route_direction ||

                      ($scope.temp.sims_transport_effective_from != $scope.temp.sims_transport_effective_from1 &&
                      $scope.temp.sims_transport_effective_upto != $scope.temp.sims_transport_effective_upto1 &&
                      $scope.v_Code_old != $scope.temp.v_Code && $scope.sims_transport_route_code_old != $scope.temp.sims_transport_route_code &&
                      (($scope.sims_transport_route_direction_old != $scope.temp.sims_transport_route_direction) ||
                       ($scope.sims_transport_route_direction_old == $scope.temp.sims_transport_route_direction)))) {
                    sub_opr1 = 'K';
                }

                var updaterecord = {

                    sims_transport_academic_year: $scope.temp.sims_academic_year,
                    sims_transport_em_number: $scope.temp.sims_transport_em_number,
                    sims_transport_route_employee_code: $scope.sims_transport_route_employee_code,

                    sims_appl_parameter: sims_appl_parameter,

                    sims_transport_route_code: $scope.temp.sims_transport_route_code,
                    sims_transport_route_code_old: $scope.sims_transport_route_code_old,

                    sims_transport_route_direction: $scope.temp.sims_transport_route_direction,
                    sims_transport_route_direction_old: $scope.sims_transport_route_direction_old,

                    sims_transport_effective_from: $scope.temp.sims_transport_effective_from1,
                    sims_transport_effective_upto: $scope.temp.sims_transport_effective_upto1,

                    sims_transport_effective_from_old: $scope.temp.sims_transport_effective_from,
                    sims_transport_effective_upto_old: $scope.temp.sims_transport_effective_upto,

                    sims_transport_cancel_from_date: $scope.temp.sims_transport_cancel_from_date,

                    sims_transport_pickup_stop_code_old: $scope.sims_transport_pickup_stop_code_old,
                    sims_transport_drop_stop_code_old: $scope.sims_transport_drop_stop_code_old,


                    sims_transport_seat_number: $scope.sims_transport_seat_number,

                    v_Code: $scope.v_Code_old,
                    v_Name_Code: $scope.temp.v_Code,

                    sims_transport_pickup_stop_code: $scope.temp.sims_transport_pickup_stop_code,
                    sims_transport_drop_stop_code: $scope.temp.sims_transport_drop_stop_code,

                    opr: 'U',
                    sub_opr: sub_opr1,

                }
                data1.push(updaterecord);
                $http.post(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/EmployeeUpdateTransportCUD", data1).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Transport Update Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Transport Not Update. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }

                    $('#MyModalEmployee').modal('hide');
                    $scope.searchtable = false;
                    $scope.searchtable_employee = true;

                    $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getEmployeeTransportDetails?acayear=" + $scope.edt.sims_academic_year + "&common_user_code=" + $scope.edt.sims_enroll_number + "&vehicle_code=" + $scope.edt.sims_transport_vehicle_code).then(function (getEmployeeTransportDetails_Data) {
                        $scope.EmployeeTransportDetails_Data = getEmployeeTransportDetails_Data.data;
                        $scope.totalItems1 = $scope.EmployeeTransportDetails_Data.length;
                        $scope.todos1 = $scope.EmployeeTransportDetails_Data;
                        $scope.makeTodos1();
                    });
                });
            }
            else {

                swal({ title: "Alert", text: "Please Select Start Date And End Date for Update Transport", showCloseButton: true, width: 380, });
            }
        }

        $scope.CancelEmployeeTransport = function () {

            var data1 = [];
            var sub_opr1 = null;
            //if ($scope.temp.sims_transport_effective_from == $scope.temp.sims_transport_effective_from1 &&
            //        $scope.temp.sims_transport_effective_upto == $scope.temp.sims_transport_effective_upto1) {
            //    sub_opr1 = 'C';
            //}

            //else if ($scope.temp.sims_transport_effective_from < $scope.temp.sims_transport_effective_from1 &&
            //     $scope.temp.sims_transport_effective_upto == $scope.temp.sims_transport_effective_upto1) {
            //    sub_opr1 = 'D';
            //}

            //else if ($scope.temp.sims_transport_effective_from == $scope.temp.sims_transport_effective_from1 &&
            //     $scope.temp.sims_transport_effective_upto > $scope.temp.sims_transport_effective_upto1) {
            //    sub_opr1 = 'F';
            //}

            //else if ($scope.temp.sims_transport_effective_from < $scope.temp.sims_transport_effective_from1 &&
            //     $scope.temp.sims_transport_effective_upto > $scope.temp.sims_transport_effective_upto1) {
            //    sub_opr1 = 'G';
            //}
            if ($scope.temp.sims_transport_cancel_from_date == null || $scope.temp.sims_transport_cancel_from_date == ''
               ) {

                sub_opr1 = 'T';
            }
            else if ($scope.temp.sims_transport_cancel_from_date != null) {

                sub_opr1 = 'M';
            }
            var cancelrecord = {

                sims_transport_academic_year: $scope.temp.sims_academic_year,
                sims_transport_em_number: $scope.temp.sims_transport_em_number,
                sims_transport_route_employee_code: $scope.sims_transport_route_employee_code,

                sims_appl_parameter: sims_appl_parameter,

                sims_transport_route_code: $scope.temp.sims_transport_route_code,
                sims_transport_route_code_old: $scope.sims_transport_route_code_old,

                sims_transport_route_direction: $scope.temp.sims_transport_route_direction,
                sims_transport_route_direction_old: $scope.sims_transport_route_direction_old,

                sims_transport_effective_from: $scope.temp.sims_transport_effective_from1,
                sims_transport_effective_upto: $scope.temp.sims_transport_effective_upto1,

                sims_transport_effective_from_old: $scope.temp.sims_transport_effective_from,
                sims_transport_effective_upto_old: $scope.temp.sims_transport_effective_upto,

                sims_transport_cancel_from_date: $scope.temp.sims_transport_cancel_from_date,

                sims_transport_pickup_stop_code_old: $scope.sims_transport_pickup_stop_code_old,
                sims_transport_drop_stop_code_old: $scope.sims_transport_drop_stop_code_old,


                sims_transport_seat_number: $scope.sims_transport_seat_number,

                v_Code: $scope.v_Code_old,
                v_Name_Code: $scope.temp.v_Code,

                sims_transport_pickup_stop_code: $scope.temp.sims_transport_pickup_stop_code,
                sims_transport_drop_stop_code: $scope.temp.sims_transport_drop_stop_code,
                opr: 'E',
                sub_opr: sub_opr1,

            }

            data1.push(cancelrecord);
            $http.post(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/EmployeeCancelTransportCUD", data1).then(function (msg) {
                $scope.msg1 = msg.data;

                if ($scope.msg1 == true) {
                    swal({ text: "Transport Cancel Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                }
                else {
                    swal({ text: "Transport Not Cancel. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                }

                $('#MyModalEmployee').modal('hide');
                $scope.searchtable = false;
                $scope.searchtable_employee = true;

                $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getEmployeeTransportDetails?acayear=" + $scope.edt.sims_academic_year + "&common_user_code=" + $scope.edt.sims_enroll_number + "&vehicle_code=" + $scope.edt.sims_transport_vehicle_code).then(function (getEmployeeTransportDetails_Data) {
                    $scope.EmployeeTransportDetails_Data = getEmployeeTransportDetails_Data.data;
                    $scope.totalItems1 = $scope.EmployeeTransportDetails_Data.length;
                    $scope.todos1 = $scope.EmployeeTransportDetails_Data;
                    $scope.makeTodos1();
                });
            });

        }

        /////////////////////////////////////////////////////////////////////////////////////////Apply Directlly To New Transport...

        $scope.CancelProcess = function () {
            checkmonthwise = document.getElementById("chk_term11");
            if (checkmonthwise.checked == true) {
                checkmonthwise.checked = false;
            }
            if ($scope.temp.valid_date != "") {
                $scope.temp.valid_date = '';
            }
            if (chk_term0712_aassign.checked == true) {
                chk_term0712_aassign.checked = false;
                $scope.CheckNewT();

            }
            $scope.CheckMonthWise();
        }

        $scope.CheckNewT = function () {

            //checkmonthwise = document.getElementById("chk_term11");
          
            if (chk_term0712_aassign.checked == true) {
                $scope.hide_feecheck_NT = true;
                $scope.hide_feecheck = true;
                $scope.hide_second_NT = true;
                $scope.end_date_c = true;


                $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getSecondBusCode?V_code=" + $scope.temp.v_Code).then(function (get_VehicleCode_2) {
                    $scope.VehicleCode_2 = get_VehicleCode_2.data;
                    //$scope.temp.sims_transport_vehicle_code_2 = $scope.VehicleCode_2[0].sims_transport_vehicle_code_2;
                })


                $scope.colors_menu = false;
                $scope.cancel_btn = false;
                $scope.valid_date = false;
                $scope.update_btn = false;
                $scope.disable_cancel = false;
                $scope.disable_update = false;
            }

            else {
                $scope.hide_feecheck_NT = false;
                $scope.hide_feecheck = false;
                $scope.end_date_c = false;
                $scope.hide_second_NT = false;
                $scope.hide_second_pick1_NT = false;
                $scope.hide_second_pick2_NT = false;
                $scope.hide_asroute = true;

                $scope.up($scope.restore);
            }

        }

        $scope.direction_one_chege = function (dir_1) {

            if (dir_1 == "03") {
                $scope.hide_pick1 = true;
                $scope.hide_pick2 = true;
                $scope.hide_asroute = true;
            }

            if (dir_1 == "02") {
                $scope.hide_asroute = false;
                $scope.hide_pick1 = false;
                $scope.hide_pick2 = true;
            }
            if (dir_1 == "01") {
                $scope.hide_asroute = false;
                $scope.hide_pick1 = true;
                $scope.hide_pick2 = false;
            }
        }

        $scope.direction_second_chege = function (dir_2) {
            
            if (dir_2 == "02") {
                $scope.hide_second_pick1_NT = false;
                $scope.hide_second_pick2_NT = true;
            }
            if (dir_2 == "01") {
                $scope.hide_second_pick1_NT = true;
                $scope.hide_second_pick2_NT = false;
            }
            $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getStopByRoutedirectioncode?AcaYear=" + $scope.temp.sims_academic_year + "&Vehicle_Code=" + $scope.temp.sims_transport_vehicle_code_2 + "&r_code=" + $scope.temp.sims_transport_route_code_2 + "&d_code=" + $scope.temp.sims_transport_route_direction_2).then(function (getStopByRoute_2) {
                $scope.StopByRoute_2 = getStopByRoute_2.data;
                $scope.temp.sims_transport_stop_code_2 = $scope.StopByRoute_2[0].sims_transport_stop_code_2;
            });
        }

        $scope.getroutebybuscode_2 = function () {
            $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getSecondRouteName?AcaYear=" + $scope.temp.sims_academic_year + "&Vehicle_Code=" + $scope.temp.sims_transport_vehicle_code_2 + "&r_code=" + $scope.temp.sims_transport_route_code + "&d_code=" + $scope.temp.sims_transport_route_direction).then(function (getRouteNameanddirection2) {
                $scope.RouteNameByBusCode2 = getRouteNameanddirection2.data;
                $scope.temp.sims_transport_route_code_2 = $scope.RouteNameByBusCode2[0].sims_transport_route_code_2;
                
                $scope.getStopbyRoute_2($scope.temp.sims_transport_route_code_2, $scope.temp.sims_academic_year);
            });

        }

        $scope.getStopbyRoute_2 = function (route_code, aca_year) {
            $scope.DirectionName_2 = [];
            if ($scope.temp.sims_transport_route_direction == '01') {
                var obj_dir = [];
                obj_dir = {
                    sims_transport_route_direction_2: '02',
                    sims_transport_route_direction_name_2: 'OUT'
                }

                $scope.DirectionName_2.push(obj_dir);
                $scope.temp.sims_transport_route_direction_2 = $scope.DirectionName_2[0].sims_transport_route_direction_2;
                $scope.direction_second_chege($scope.temp.sims_transport_route_direction_2);
            }

            else if ($scope.temp.sims_transport_route_direction == '02') {
                var obj_dir = [];
                obj_dir = {
                    sims_transport_route_direction_2: '01',
                    sims_transport_route_direction_name_2: 'IN'
                }

                $scope.DirectionName_2.push(obj_dir);
                $scope.temp.sims_transport_route_direction_2 = $scope.DirectionName_2[0].sims_transport_route_direction_2;
                $scope.direction_second_chege($scope.temp.sims_transport_route_direction_2);
            }
        }

        $scope.apply_new_tran = function () {
            var data1 = [];
            var updaterecord = {

                sims_transport_academic_year: $scope.temp.sims_academic_year,
                sims_transport_enroll_number: $scope.temp.sims_enroll_number,
                sims_transport_route_student_code: sims_transport_route_student_code,
                sims_appl_parameter: sims_appl_parameter,
                sims_fee_cur_code: $scope.temp.sims_fee_cur_code,
                sims_fee_grade_code: $scope.temp.sims_fee_grade_code,
                sims_fee_section_code: $scope.temp.sims_fee_section_code,
                sims_fee_number: $scope.temp.sims_fee_number,

                sims_transport_route_code_old: $scope.temp.sims_transport_route_code,
                sims_transport_route_code: $scope.temp.sims_transport_route_code_2,
                sims_transport_route_direction_old: $scope.temp.sims_transport_route_direction,
                sims_transport_route_direction: $scope.temp.sims_transport_route_direction_2,

                sims_transport_effective_from: $scope.temp.sims_transport_effective_from1,
                sims_transport_effective_upto: $scope.temp.sims_transport_effective_upto1,
                sims_transport_effective_from_old: $scope.temp.sims_transport_effective_from,
                sims_transport_effective_upto_old: $scope.temp.sims_transport_effective_upto,

                sims_transport_pickup_stop_code_old: $scope.temp.sims_transport_pickup_stop_code,
                sims_transport_drop_stop_code_old: $scope.temp.sims_transport_drop_stop_code,
                sims_transport_pickup_stop_code: $scope.temp.sims_transport_stop_code_2,
                sims_transport_drop_stop_code: $scope.temp.sims_transport_stop_code_2,

                v_Code: $scope.temp.v_Code,
                v_Name_Code: $scope.temp.sims_transport_vehicle_code_2,


                sims_fee_period1: $scope.temp1.sims_fee_period1,
                sims_fee_period2: $scope.temp1.sims_fee_period2,
                sims_fee_period3: $scope.temp1.sims_fee_period3,
                sims_fee_period4: $scope.temp1.sims_fee_period4,
                sims_fee_period5: $scope.temp1.sims_fee_period5,
                sims_fee_period6: $scope.temp1.sims_fee_period6,
                sims_fee_period7: $scope.temp1.sims_fee_period7,
                sims_fee_period8: $scope.temp1.sims_fee_period8,
                sims_fee_period9: $scope.temp1.sims_fee_period9,
                sims_fee_period10: $scope.temp1.sims_fee_period10,
                sims_fee_period11: $scope.temp1.sims_fee_period11,
                sims_fee_period12: $scope.temp1.sims_fee_period12,

                opr: 'U',

            }
            data1.push(updaterecord);
            $http.post(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/ApplyTransportCUD", data1).then(function (msg) {
                $scope.msg1 = msg.data;

                if ($scope.msg1 == true) {
                    swal({ text: "Transport Apply Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                }
                else if ($scope.msg1 == false) {
                    swal({ text: "Transport Not Apply. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                }
                else {
                    swal("Error-"+$scope.msg1)
                }

                $('#MyModal').modal('hide');
                $scope.searchtable = true;
                $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getStudentTransportDetails?cur_code=" + $scope.edt.sims_cur_code + "&acayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&enroll=" + $scope.edt.sims_enroll_number + "&vehicle_code=" + $scope.edt.sims_transport_vehicle_code).then(function (getStudentTransportDetails_Data) {
                    $scope.StudentTransportDetails = getStudentTransportDetails_Data.data;
                    $scope.totalItems = $scope.StudentTransportDetails.length;
                    $scope.todos = $scope.StudentTransportDetails;
                    $scope.makeTodos();
                });
            });
        }

    }]);
})();