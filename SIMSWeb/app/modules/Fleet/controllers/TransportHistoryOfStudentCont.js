﻿(function () {
    'use strict';
    var Student_bacth_code = [], selected_enroll_number = [];
    var main;
    var data1 = [];
    var code = '', enddate;
    var itemset;
    var simsController = angular.module('sims.module.Fleet');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TransportHistoryOfStudentCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {
            $scope.display = true;
            $scope.pager = true;
            $scope.grid = false;
            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.pagesize1 = "10";
            $scope.pageindex1 = "1";
            var route_code_new;
            $scope.student_grid_record = false;
            $scope.employee_grid_record = false;

            $scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');
            itemset = document.getElementById("chk_term10");

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.size = function (str) {
                if (str == "All" || str == "all") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index1 = function (str) {
                $scope.pageindex1 = str;
                $scope.currentPage1 = str; console.log("currentPage1=" + $scope.currentPage1);
                $scope.makeTodos1();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.getalldetais = function () {
                $http.get(ENV.apiUrl + "api/common/getAllAcademicYear").then(function (get_AllAcademicYear) {
                    $scope.ActiveYear = get_AllAcademicYear.data;
                    $scope.edt = { sims_academic_year: $scope.ActiveYear[0].sims_academic_year }

                    $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getRouteNameByBusCode?AcaYear=" + $scope.edt.sims_academic_year + "&Vehicle_Code=" + '').then(function (getRouteNameByBusCode_Data) {
                        $scope.AllRouteName = getRouteNameByBusCode_Data.data;
                    });

                    $http.get(ENV.apiUrl + "api/common/getStopLanLogByDirection?route_code=" + '' + "&aca_year=" + $scope.edt.sims_academic_year).then(function (get_PickupDrop) {
                        $scope.PickupDrop = get_PickupDrop.data;
                    });
                    var direct = [];

                    var dati = ({
                        sims_transport_route_direction_1: 'IN',
                        sims_transport_route_direction: '01',
                    });

                    var dato = ({
                        sims_transport_route_direction_1: 'OUT',
                        sims_transport_route_direction: '02',
                    });

                    direct.push(dati);
                    direct.push(dato);
                    $scope.DirectionName = direct;
                });
            }

            $scope.getalldetais();

            $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getBusCode").then(function (getBusCode_Data) {
                $scope.BusCode_Data = getBusCode_Data.data;
            });

            $scope.getroutebybuscode = function () {

                $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getRouteNameByBusCode?AcaYear=" + $scope.edt.sims_academic_year + "&Vehicle_Code=" + $scope.temp.v_Code).then(function (getRouteNameByBusCode_Data) {
                    $scope.AllRouteName = getRouteNameByBusCode_Data.data;
                    $scope.sims_transport_route_code = [];
                    $scope.sims_transport_route_code = $scope.AllRouteName[0];
                    $scope.getdirectbyroute($scope.sims_transport_route_code.sims_transport_route_code, $scope.edt.sims_academic_year);
                });
            }

            $scope.getdirectbyroute = function (route_code, aca_year) {
                $scope.route_code_new = route_code;
                if (route_code.sims_transport_route_direction != null)
                    route_code_new = route_code.sims_transport_route_code;
                else
                    route_code_new = route_code;
                var direct = [];
                for (var i = 0; i < $scope.AllRouteName.length; i++) {
                    direct.push($scope.AllRouteName[i])
                }
                $scope.DirectionName = direct;
                $scope.edt['sims_transport_route_direction'] = $scope.DirectionName[0].sims_transport_route_direction;
                $scope.getstopbydirection(route_code, $scope.edt.sims_academic_year, $scope.edt.sims_transport_route_direction);
            }

            $scope.getstopbydirection = function (route_code, aca_year, str) {

                $http.get(ENV.apiUrl + "api/common/getStopLanLogByDirection?route_code=" + $scope.sims_transport_route_code.sims_transport_route_code + "&aca_year=" + aca_year).then(function (get_PickupDrop) {
                    $scope.PickupDrop = get_PickupDrop.data;

                    //$scope.edt = {
                    //    sims_academic_year: $scope.edt.sims_academic_year,
                    //    //sims_transport_route_direction: $scope.DirectionName[0].sims_transport_route_direction,
                    //    sims_transport_drop_stop_code: $scope.PickupDrop[0].sims_transport_stop_code,
                    //    sims_transport_pickup_stop_code: $scope.PickupDrop[0].sims_transport_stop_code
                    //};
                    $scope.sims_transport_route_code['sims_transport_route_code'] = $scope.AllRouteName[0].sims_transport_route_code
                })

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.Transport_Details = function () {
                $scope.IP = {};
                $scope.IP.sims_transport_route_direction = $scope.edt.sims_transport_route_direction;
                $scope.IP.sims_transport_pickup_stop_code = $scope.edt.sims_transport_pickup_stop_code;
                $scope.IP.sims_parent_name = $scope.edt.sims_parent_name;
                $scope.IP.sims_parent_id = $scope.edt.sims_parent_id;
                $scope.IP.sims_student_name = $scope.edt.sims_student_name;
                $scope.IP.sims_transport_enroll_number = $scope.edt.sims_student_enroll_number;
                $scope.IP.sims_transport_academic_year = $scope.edt.sims_academic_year;
                $scope.IP.sims_transport_route_code = route_code_new;
                //$scope.IP.V_Code = $scope.temp.v_Code;
                $http.get(ENV.apiUrl + "api/TransRouteStudDetails/getTransportHistory?data=" + JSON.stringify($scope.IP)).then(function (res) {
                    $scope.rows = res.data.table;
                    $scope.totalItems = $scope.rows.length;
                    $scope.todos = $scope.rows;
                    $scope.makeTodos();
                });
            }


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.rows, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.rows;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                return (
                    item.sims_student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_transport_route_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_transport_route_name == toSearch) ? true : false;

            }


            $scope.Reset = function () {

                $scope.edt = {
                    sims_transport_route_direction: '',
                    sims_transport_pickup_stop_code: '',
                    sims_parent_name: '',
                    sims_parent_id: '',
                    sims_student_name: '',
                    sims_student_enroll_number: '',

                }

                $scope.temp = {
                    v_Code: '',
                }
                route_code_new = undefined;
                $scope.sims_transport_route_code = { 'sims_transport_route_code': '' }
                state: 'main.Sim081';
                $scope.filteredTodos = '';
                $scope.getalldetais();
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

        }])
})();
