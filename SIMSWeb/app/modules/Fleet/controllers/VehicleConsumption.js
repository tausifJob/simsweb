﻿(function () {
    'use strict';
    var del = [];
    var simsController = angular.module('sims.module.Fleet');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('VehicleConsumptionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', 'XLSXReaderService', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, XLSXReaderService, $filter) {

            $scope.rgvtbl = false;

          

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);

       

            ///////////////////////////// Upload Doc Code //////////////////////////////////////////

            $scope.display = true;
            $scope.save = true;
            $scope.items = [];

            var date = new Date();
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss');
            $scope.showPreview = false;
            $scope.showJSONPreview = true;
            $scope.json_string = "";
            $scope.itemsPerPage = '50';
            $scope.currentPage = 0;
            $scope.filesize = false;

            $scope.size = function (pagesize) {
                $scope.itemsPerPage = pagesize;

            }

            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;


            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.sheets.Sheet1.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;
            };

            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {
                $scope.flag = 0;
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                    var i = 0;
                    });
            };
            $scope.sheets = [];
            $scope.file_changed = function (element) {
                // SpinnerDialog.show('loading','...');
                var photofile = element.files[0];
                $scope.filename = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;
                    });
                };
                reader.readAsDataURL(photofile);
                var f = document.getElementById('file1').files[0];
                $scope.sheets = [];
                $scope.excelFile = element.files[0];
                XLSXReaderService.readFile($scope.excelFile, $scope.showPreview, $scope.showJSONPreview).then(function (xlsxData) {
                    //console.log(xlsxData.sheets);
                    $scope.display = false;
                    $scope.save = false;
                    //console.log(xlsxData.sheets.Sheet1[0]);
                    var size = Object.keys(xlsxData.sheets.Sheet1[0]).length;
                   
                    // $scope.sheets = [];
                    $scope.sheets = xlsxData.sheets
                    $scope.sheetsData = [];
                    
                    for (var i = 0; i < $scope.sheets.Sheet1.length; i++) {

                        if ($scope.sheets.Sheet1[i].Sale_Time != undefined || $scope.sheets.Sheet1[i].Sale_Time != '') {
                            var date = new Date($scope.sheets.Sheet1[i].Sale_Time);
                            var month = (date.getMonth() + 1);
                            var day = date.getDate();
                            if (month < 10)
                                month = "0" + month;
                            if (day < 10)
                                day = "0" + day;
                            $scope.d = date.getFullYear() + '-' + (month) + '-' + (day);
                        }
                        var data = {
                            sup_code: ($scope.sheets.Sheet1[i].Sup_name == undefined ? '' : $scope.sheets.Sheet1[i].Sup_name),
                            sup_location_code: ($scope.sheets.Sheet1[i].Sup_loc_Name == undefined ? '' : $scope.sheets.Sheet1[i].Sup_loc_Name),
                            im_inv_no: ($scope.sheets.Sheet1[i].Product_Name == undefined ? '' : $scope.sheets.Sheet1[i].Product_Name),
                            vehicle_code: ($scope.sheets.Sheet1[i].License_Plate == undefined ? '' : $scope.sheets.Sheet1[i].License_Plate),
                            sale_time: $scope.d,
                            unit_price: $scope.sheets.Sheet1[i].Unit_Price,
                            sale_qty: $scope.sheets.Sheet1[i].sale_qty,
                            Total_Amount: $scope.sheets.Sheet1[i].Total_Amount,
                            sale_period: $scope.sheets.Sheet1[i].Period
//                            driver_code: ($scope.sheets.Sheet1[i].Driver == undefined ? '' : $scope.sheets.Sheet1[i].Driver)
                        }
                        $scope.sheetsData.push(data);
                    }
                    $scope.busy = true;
                    $scope.errlabel = false;
                    $http.post(ENV.apiUrl + "api/vehicle/VehicleConsumption", $scope.sheetsData).then(function (res) {
                        if (res.data.length > 0) {
                            $scope.busy = false;
                            $scope.rgvtbl = true;
                            $scope.sheets1 = res.data;
                            for (var i = 0; i < $scope.sheets1.length; i++) {
                                if ($scope.sheets1[i].sup_code_flag == false || $scope.sheets1[i].sup_location_code_flag == false
                                    || $scope.sheets1[i].im_inv_no_flag == false || $scope.sheets1[i].vehicle_code_flag == false
                                    ) {
                                   
                                    $scope.savebtn = true;
                                    $scope.errlabel = true;
                                    break;
                                } else {
                                    $scope.savebtn = false;
                                }

                            }
                                    angular.forEach(
                                      angular.element("input[type='file']"),
                                      function (inputElem) {
                                          angular.element(inputElem).val(null);
                                      });
                        }
                    });
                    //$('#uploadModal').modal({ backdrop: 'static', keyboard: false });                   
                });
                // if (element.files[0].size<200000) {
                //var request = {
                //    method: 'POST',
                //    url: ENV.apiUrl + '/api/file/uploadDocument?filename=' + $scope.filename.name + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss') + "&location=" + "Docs/Student",
                //    data: formdata,
                //    headers: {
                //        'Content-Type': undefined
                //    }
                //};
                //$http(request).success(function (d) {
                //    //alert(d);
                //    console.log(d);
                //    //                        swal({ title: "Document Uploaded Successfully", imageUrl: "assets/img/check.png", });
                //    var data = {
                //        sims_admission_user_code: $rootScope.globals.currentUser.username,
                //        sims_admission_doc_path: d
                //    }

                //    $http.post(ENV.apiUrl + "api/document/insertdoc", data).then(function (res) {
                //        $scope.ins = res.data;
                //        if (res.data) {
                //            swal({ title: "Document Uploaded Successfully", imageUrl: "assets/img/check.png", });
                //            $http.get(ENV.apiUrl + "api/document/getDocument?enroll=S1001").then(function (res) {
                //                $scope.items = res.data;
                //                console.log(res.data);
                //            });
                //        } else {
                //            swal({ title: "Document Upload Failed, Check Connection", imageUrl: "assets/img/check.png", });
                //        }

                //    });
                //});
                //   }
            };



            $scope.uploadClick = function () {
                // $scope.filesize = true;
               formdata = new FormData();

            }
            if ($rootScope.globals.studentsLoaded) {
                $scope.getstudentList();
            }


            $scope.insertData = function () {
                $scope.busy = true;
                $http.post(ENV.apiUrl + "api/vehicle/InsertVehicleConsumption", $scope.sheets1).then(function (res) {
                    // $scope.sheets = res.data;
                    if (res.data) {
                        $scope.busy = false;
                        $scope.savebtn = true;
                        $scope.success = true;
                        $scope.fail = false;

                    } else {
                        $scope.busy = false;
                        $scope.savebtn = false;
                        $scope.success = false;
                        $scope.fail = true;

                    }
                });

            }

            $scope.cancel = function () {
                $scope.display = true;
                $scope.sheets = "";
            }


            $scope.showPreviewChanged = function (selectedSheetName) {
                $scope.sheets[$scope.selectedSheetName];
            }

            $scope.textFocus = function (key, val) {
                if (key == "Date" || key == "Emp_No") {
                    $scope.value = true;
                } else {
                    $scope.value = false;
                }
            }

            $scope.textChange = function (item, key, val) {
                if (key == "Clock_In" || key == "Clock_Out") {
                    item[key] = val;
                }

                var main = document.getElementById($scope.chekindex);
                main.checked = false;
                var rm = del.indexOf($scope.chekindex);
                $scope.datasend.splice(rm, 1);
            }
            $scope.$on('student_change', function () {
                $scope.getstudentList();
            });

            $scope.datasend = [];

            $scope.Checkval = function (val, index) {
                val.opr = "Q";
                //                val.status = "";
                $scope.values = val;
                $scope.chekindex = index;
                var main = document.getElementById(index);

                if (main.checked == true) {
                    var data = {
                        sup_code: val.Sup_name,
                        sup_location_code: val.Sup_loc_Name,
                        im_inv_no: val.Product_Name,
                        vehicle_code: val.License_Plate,
                        sale_time: val.Sale_Time,
                        unit_price: val.Unit_Price,
                        sale_qty: val.sale_qty,
                        Total_Amount: val.Total_Amount,
                        sale_period: val.Period
                    }
                    $scope.datasend.push(data);
                    //                    $scope.datasend.push(data);                 
                } else {
                    main.checked = false;
                    var rm = del.indexOf(index);
                    $scope.datasend.splice(rm, 1);
                }
                $scope.filecount = "You Selected " + $scope.datasend.length + " Records";
            }

            $scope.CheckMultiple = function () {
                var main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.sheets.Sheet1.length; i++) {
                    var v = document.getElementById(i);
                    if (main.checked == true) {
                        if (v.checked != true) {
                            v.checked = true;
                            var t = $scope.sheets.Sheet1[i];
                            $scope.Checkval(t, i);
                        }
                    }
                    else {
                        v.checked = false;
                        var t = $scope.sheets.Sheet1[i];
                        main.checked = false;
                        $scope.Checkval(t, i);
                    }
                }

            }


            $scope.downloadFormat = function () {
                $scope.url = "http://dpsmis.mograsys.com/app/modules/Fleet/controllers/vehicle.xls";

                window.open($scope.url, '_self', 'location=yes');

            }

        }]);


    simsController.directive('ngFiles', ['$parse', '$scope', function ($parse, $scope) {
        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])


    simsController.factory("XLSXReaderService", ['$q', '$rootScope',
    function ($q, $rootScope) {
        var service = function (data) {
            angular.extend(this, data);
        }

        service.readFile = function (file, readCells, toJSON) {
            var deferred = $q.defer();

            XLSXReader(file, readCells, toJSON, function (data) {
                $rootScope.$apply(function () {
                    deferred.resolve(data);
                });
            });

            return deferred.promise;
        }


        return service;
    }
    ])



})();