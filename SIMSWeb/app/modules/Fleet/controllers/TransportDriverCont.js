﻿(function () {
    'use strict';
    var opr = '';
    var drivercode = [];
    var main;
    var data1 = [];
    var data = [];

    var simsController = angular.module('sims.module.Fleet');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TransportDriverCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

             $scope.imageUrl = ENV.apiUrl + '/Content/' + $http.defaults.headers.common['schoolId'] + '/Images/DriverImage/';
           // $scope.imageUrl = ENV.apiUrl + '/Content/sjs/Images/DriverImage/';
             $scope.disusr = false;
             $scope.disemp = false;
             $scope.pagesize = '10';
             $scope.pageindex = "0";
             $scope.edt = [];
             $scope.pager = true;
             $scope.table1 = true;
             $scope.driver_email=false;
             $scope.operation = false;
             $scope.editmode = false;
            //$scope.copy1 = 'Employee';
            //$scope.radioclick1 = function (str) {
            //    if (str == 'New') {
            //        $scope.usedis = true;
            //        $scope.edt = '';
            //    }
            //    else {
            //        $scope.usedis = false;
            //    }
            //}
             $scope.user_access = [];

             $scope.appcode = $state.current.name.split('.');
             var user = $rootScope.globals.currentUser.username;

             $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                 debugger;
                 $scope.user_rights = usr_rights.data;

                 for (var i = 0; i < $scope.user_rights.length; i++) {

                     if (i == 0) {
                         $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                         $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                         $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                         $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                         $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                         $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                     }
                 }

                 //console.log($scope.user_access);
             });
            
                $http.get(ENV.apiUrl + "api/TransportCaretaker/getTransportUserCode").then(function (userCodeData) {
                    
                    $scope.edt.sims_user_code = userCodeData.data[0].sims_user_code;
                });

             $scope.get_ischecked = function (str) {
                
                 if (str == "user_true") {
                     $scope.usedis = true;
                     $scope.designation = false;
                     $scope.employeeName = false;
                     $scope.driver_email = true;
                     $scope.driver_phone_no = true;
                     $scope.temp.sims_user = 'user_true';
                 }
                 else {
                     $scope.usedis = false;
                     $scope.designation = true;
                     $scope.employeeName = true;
                     $scope.driver_email = false;
                     $scope.driver_phone_no = false;
                     $scope.temp.sims_user = 'emp_true';
                    
                 }
             }

             $scope.temp =
           {
               sims_user: 'emp_true'

           }

             $http.get(ENV.apiUrl + "api/TransportDriver/getTransportDriver").then(function (TransportDriver_Data) {
                 $scope.TransportDriver = TransportDriver_Data.data;
                 $scope.totalItems = $scope.TransportDriver.length;
                 $scope.todos = $scope.TransportDriver;
                 $scope.makeTodos();

                
             });

             $http.get(ENV.apiUrl + "api/TransportDriver/getPaysDesignation").then(function (getPaysDesignation_Data) {
                 $scope.Designation = getPaysDesignation_Data.data;
             });
            
             $scope.getemployeename = function (desg_code) {
               debugger;
                 $http.get(ENV.apiUrl + "api/TransportDriver/getAllEmpDriverName?desg_code=" + desg_code).then(function (get_AllEmp) {
                     $scope.AllEmpC = get_AllEmp.data;
                   
                 })
             }

            //$http.get(ENV.apiUrl + "api/common/getAllEmpDriver").then(function (get_AllEmp) {
            //    $scope.AllEmpC = get_AllEmp.data;
            //    console.log($scope.AllEmpC);
            //});

             $scope.getdrivername = function (str) {
                 debugger;
                 for (var i = 0; i < $scope.AllEmpC.length; i++) {
                     
                     if ($scope.AllEmpC[i].sims_employee_code == str) {
                         $scope.edt.sims_driver_name = $scope.AllEmpC[i].sims_employee_code_name;
                     }
                 }
                 if (str!=null) {
                     $http.post(ENV.apiUrl + "api/TransportDriver/showwDataAsperEmployee?employeeCode=" + str).then(function (empData) {
                         debugger;
                         var temp_empData = empData.data;
                         $scope.edt.sims_driver_gender = temp_empData[0].em_sex;
                         $scope.edt.sims_driver_mobile_number1 = temp_empData[0].em_mobile;
                         $scope.edt.sims_driver_address = temp_empData[0].addressEmployee;
                         $scope.edt.sims_driver_visa_number = temp_empData[0].em_visa_number;
                         $scope.edt.sims_driver_visa_issuing_authority = temp_empData[0].em_visa_issuing_authority;
                         $scope.edt.sims_driver_visa_issuing_place = temp_empData[0].em_visa_issuing_place;
                         $scope.edt.sims_driver_dot_license_issue_date = temp_empData[0].em_visa_issue_date;
                         $scope.edt.sims_driver_dot_license_expiry_date = temp_empData[0].em_visa_expiry_date;
                         $scope.edt.sims_driver_national_id = temp_empData[0].em_national_id;
                         $scope.edt.sims_driver_national_id_issue_date = temp_empData[0].em_national_id_issue_date;
                         $scope.edt.sims_driver_national_id_expiry_date = temp_empData[0].em_national_id_expiry_date;
                         $scope.edt.sims_driver_date_of_birth = temp_empData[0].em_date_of_birth;
                     });
                 }
                 
             }

             $http.get(ENV.apiUrl + "api/common/getDriverGender").then(function (get_DriverGender) {
                 $scope.DriverGender = get_DriverGender.data;

             })

             $http.get(ENV.apiUrl + "api/common/getDriverType").then(function (getDriver_Type) {
                 $scope.DriverType = getDriver_Type.data;

             })

             $http.get(ENV.apiUrl + "api/common/getVehiclecategory").then(function (get_Vehiclecategory) {
                 $scope.Vehiclecate = get_Vehiclecategory.data;

             })

             $http.get(ENV.apiUrl + "api/common/getVehicleMode").then(function (get_VehicleMode) {
                 $scope.VehicleMode = get_VehicleMode.data;

             })

             $http.get(ENV.apiUrl + "api/common/getVisaType").then(function (get_VisaType) {
                 $scope.VisaType = get_VisaType.data;

             })

             $('*[data-datepicker="true"] input[type="text"]').datepicker({

                 todayBtn: true,
                 orientation: "top left",
                 autoclose: true,
                 todayHighlight: true,
                 format: 'yyyy-mm-dd'
             });

             $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                 $('input[type="text"]', $(this).parent()).focus();
             });



            // $scope.Comparedate = function () {

            //    //if ($scope.temp.sims_transport_vehicle_registration_expiry_date < $scope.temp.sims_transport_vehicle_registration_date)
            //     if (moment($scope.edt.sims_driver_license_expiry_date, "DD-MM-YYYY").format('YYYY-MM-DD') < moment($scope.edt.sims_driver_license_issue_date, "DD-MM-YYYY").format('YYYY-MM-DD'))
            //    {
            //        swal({ title: "Alert", text: "Expiry Date Should be greater than Registration Date", width: 300, height: 200 });
            //        $scope.edt.sims_driver_license_expiry_date = '';
            //    }
            //}


             $scope.check_licence_date = function (issuedt, expdt) {
                 if (issuedt > expdt) {
                     swal({ title: 'Alert', text: "Please select future date", width: 300, height: 200 })
                     $scope.edt.sims_driver_license_expiry_date = '';
                 }
             }

             $scope.check_visa_date = function (vissuedt, vexpdt) {
                 if (vissuedt > vexpdt) {
                     swal({ title: 'Alert', text: "Please select future date", width: 300, height: 200 })
                     $scope.edt.sims_driver_visa_expiry_date = '';
                 }
             }

             $scope.check_national_date = function (nissuedt, nexpdt) {
                 if (nissuedt > nexpdt) {
                     swal({ title: 'Alert', text: "Please select future date", width: 300, height: 200 })
                     $scope.edt.sims_driver_national_id_expiry_date = '';
                 }
             }

             $scope.check_driver_expiry_date = function (issuedt1, expdt1) {
                 if (issuedt1 > expdt1) {
                     swal({ title: 'Alert', text: "Please select future date", width: 300, height: 200 })
                     $scope.edt.sims_driver_dot_license_expiry_date = '';
                 }
             }



             $scope.size = function (str) {
                 
                 //$scope.pagesize = str;
                 //$scope.currentPage = 1;
                 //$scope.numPerPage = str;  $scope.makeTodos();
                 debugger;
                 if (str == "All") {
                     $scope.currentPage = '1';
                     $scope.filteredTodos = $scope.TransportDriver;
                     $scope.pager = false;
                 }
                 else {
                     $scope.pager = true;
                     $scope.pagesize = str;
                     $scope.currentPage = 1;
                     $scope.numPerPage = str;
                     $scope.makeTodos();
                 }

                 $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
             }

             $scope.index = function (str) {
                 $scope.pageindex = str;
                 $scope.currentPage = str;  $scope.makeTodos();
                 $scope.chk={}
                 $scope.chk['check_all'] = false;
                 $scope.row1 = '';
             }

             $scope.New = function () {
                 debugger;
                 if ($scope.user_access.data_insert == false) {
                     swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                 }
                 else {

                 $http.get(ENV.apiUrl + "api/TransportCaretaker/getTransportUserCode").then(function (userCodeData) {
                     $scope.edt.sims_user_code = userCodeData.data[0].sims_user_code;
                 });
                 var img = document.getElementById("file1");
                 img = "";
                 $scope.sims_driver_img = "";
                 $scope.prev_img = "";
                 $scope.disusr = false;
                 $scope.usedis = false;
                 $scope.disemp = false;
                 $scope.designation = true;
                 $scope.employeeName = true;
                 $scope.driver_email = false;
                 $scope.driver_phone_no = false;
                 $scope.temp = {
                     sims_user: 'emp_true'
                 }
                 var dt = new Date();
                 $scope.edt = {
                     dg_code: "",
                     sims_employee_code: "",
                     sims_driver_name: "",
                     sims_driver_type: "",
                     sims_user_code:$scope.edt.sims_user_code,
                     sims_driver_img: "",
                     sims_driver_experience_years: "",

                     sims_driver_gender: "",
                     sims_driver_mobile_number1: "",
                     sims_driver_mobile_number2: "",
                     sims_driver_mobile_number3: "",
                     sims_driver_address: "",
                     //sims_driver_status: "",
                     sims_driver_status: true,
                     sims_driver_date_of_birth: "",


                     sims_driver_driving_license_number: "",
                     //sims_driver_license_issue_date: "",
                     sims_driver_license_issue_date:('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear(),
                     //sims_driver_license_expiry_date: "",
                     sims_driver_license_expiry_date: ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear(),
                     sims_driver_license_place_of_issue: "",
                     sims_driver_license_vehicle_category: "",
                     sims_driver_license_vehicle_mode: "",
                     sims_driver_visa_number: "",

                     sims_driver_visa_type: "",
                     sims_driver_visa_issuing_authority: "",
                     sims_driver_visa_issuing_place: "",
                     sims_driver_visa_issue_date: ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear(),
                     sims_driver_visa_expiry_date: ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear(),
                     sims_driver_national_id: "",
                     sims_driver_national_id_issue_date: ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear(),

                     comn_user_phone_number: '',
                     sims_driver_email:'',
                     sims_driver_national_id_expiry_date: ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear(),
                     sims_driver_dot_license_no: "",
                     sims_driver_dot_license_issue_date: ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear(),
                     sims_driver_dot_license_expiry_date: ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear(),

                 }
                 $scope.check = true;
                 $scope.editmode = false;
                 $scope.newmode = true;
                 $scope.readonly = false;
                 $scope.table1 = false;
                 $scope.operation = true;
                 $scope.savebtn = true;
                 $scope.updatebtn = false;
                 $scope.myForm.$setPristine();
                 $scope.myForm.$setUntouched();
              }
             }

             $scope.up = function (str) {
                 if ($scope.user_access.data_update == false) {
                     swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                 }
                 else {
                 $scope.savebtn = false;
                 $scope.updatebtn = true;
                 $scope.table1 = false;
                 $scope.operation = true;
                 $scope.editmode = true;
                 $scope.newmode = false;
                 $scope.readonly = true;

                 $scope.sims_driver_img = $scope.imageUrl + str.sims_driver_img;
                
                 $scope.edt = {
                     sims_driver_img:str.sims_driver_img,
                     sims_driver_address: str.sims_driver_address,
                     sims_driver_code: str.sims_driver_code,
                     sims_driver_date_of_birth: str.sims_driver_date_of_birth,
                     sims_driver_driving_license_number: str.sims_driver_driving_license_number,
                     sims_driver_experience_years: str.sims_driver_experience_years,

                     sims_driver_gender: str.sims_driver_gender,
                     sims_driver_license_expiry_date: str.sims_driver_license_expiry_date,
                     sims_driver_license_issue_date: str.sims_driver_license_issue_date,
                     sims_driver_license_place_of_issue: str.sims_driver_license_place_of_issue,
                     sims_driver_license_vehicle_category: str.sims_driver_license_vehicle_category,

                     sims_driver_license_vehicle_mode: str.sims_driver_license_vehicle_mode,
                     sims_driver_license_vehicle_mode_name: str.sims_driver_license_vehicle_mode_name,

                     sims_driver_mobile_number1: str.sims_driver_mobile_number1,
                     sims_driver_mobile_number2: str.sims_driver_mobile_number2,
                     sims_driver_mobile_number3: str.sims_driver_mobile_number3,
                     sims_driver_name: str.sims_driver_name,
                     sims_driver_national_id: str.sims_driver_national_id,
                     sims_driver_national_id_expiry_date: str.sims_driver_national_id_expiry_date,
                     sims_driver_national_id_issue_date: str.sims_driver_national_id_issue_date,
                     sims_driver_status: str.sims_driver_status,
                     sims_driver_type: str.sims_driver_type,
                     sims_driver_visa_expiry_date: str.sims_driver_visa_expiry_date,
                     sims_driver_visa_issue_date: str.sims_driver_visa_issue_date,
                     sims_driver_visa_issuing_authority: str.sims_driver_visa_issuing_authority,
                     sims_driver_visa_issuing_place: str.sims_driver_visa_issuing_place,

                     sims_driver_visa_number: str.sims_driver_visa_number,
                     sims_driver_visa_type: str.sims_driver_visa_type,
                     sims_employee_code: str.sims_employee_code,
                     sims_user_code: str.sims_user_code,
                     sims_employee_code_name: str.sims_employee_code_name,

                     sims_driver_email: str.sims_driver_email,
                     comn_user_phone_number: str.comn_user_phone_number,


                     sims_driver_dot_license_no: str.sims_driver_dot_license_no,
                     sims_driver_dot_license_issue_date: str.sims_driver_dot_license_issue_date,
                     sims_driver_dot_license_expiry_date: str.sims_driver_dot_license_expiry_date
               
                 }
                
                 if (str.sims_employee_code == "" || str.sims_employee_code == null) {
                     $scope.temp = {
                         sims_user: 'user_true'
                     }
                     $scope.disusr = true;
                     $scope.disemp = true;
                     $scope.designation = false;
                     $scope.employeeName = false;
                     $scope.driver_email = true;
                     $scope.driver_phone_no = true;
                 }
                 else {
                    
                     $scope.temp = {
                         sims_user: 'emp_true'
                     }
                     $scope.disusr = true;
                     $scope.disemp = true;
                     $scope.designation = false;
                     $scope.employeeName = true;
                     $scope.driver_email = false;
                     $scope.driver_phone_no = false;
                 }
             }
             }

             $scope.cancel = function () {
               
                 var img = document.getElementById("file1");
                 img = "";
                 $scope.sims_driver_img = "";
                 $scope.prev_img = "";
                 $scope.temp = {
                     sims_user: 'emp_true'
                 }
                 $scope.table1 = true;
                 $scope.operation = false;
               
                 $scope.myForm.$setPristine();
                 $scope.myForm.$setUntouched();

                 $scope.edt = {
                     dg_code: "",
                     sims_employee_code: "",
                     sims_driver_name: "",
                     sims_driver_type: "",
                     sims_user_code: "",
                     sims_driver_img: "",
                     sims_driver_experience_years: "",

                     sims_driver_gender: "",
                     sims_driver_mobile_number1: "",
                     sims_driver_mobile_number2: "",
                     sims_driver_mobile_number3: "",
                     sims_driver_address: "",
                     sims_driver_status: "",
                     sims_driver_date_of_birth: "",


                     sims_driver_driving_license_number: "",
                     sims_driver_license_issue_date: "",
                     sims_driver_license_expiry_date: "",
                     sims_driver_license_place_of_issue: "",
                     sims_driver_license_vehicle_category: "",
                     sims_driver_license_vehicle_mode: "",
                     sims_driver_visa_number: "",

                     sims_driver_visa_type: "",
                     sims_driver_visa_issuing_authority: "",
                     sims_driver_visa_issuing_place: "",
                     sims_driver_visa_issue_date: "",
                     sims_driver_visa_expiry_date: "",
                     sims_driver_national_id: "",
                     sims_driver_national_id_issue_date: "",

                     sims_driver_national_id_expiry_date: "",
                     comn_user_phone_number: '',
                     sims_driver_email: '',

                     sims_driver_dot_license_no: "",
                     sims_driver_dot_license_issue_date: "",
                     sims_driver_dot_license_expiry_date: ""

                 }
             }

             $scope.Save = function (myForm) {
                 debugger;
                 if (myForm) {
                     data1 = [];
                     data = [];
                     data = $scope.edt;
                     data.opr = 'I';
                    
                     $scope.exist = false;
                 
                     for (var i = 0; i < $scope.TransportDriver.length; i++) {
                         if ($scope.TransportDriver[i].sims_user_code == data.sims_user_code) {
                             $scope.exist = true;
                         }
                     }
                     if ($scope.exist) {
                         swal({ text: "This User Code Already Exists", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 300, height: 200 })
                     }

                     else {
                         data1.push(data);
                         $http.post(ENV.apiUrl + "api/TransportDriver/CUDTransportDriver", data1).then(function (msg) {
                             $scope.msg1 = msg.data;
                             if ($scope.msg1 != '' || $scope.msg1 != null) {
                                 swal({ title: "Alert", text: "Data added Successfully", width: 300, height: 200 });
                             }
                             else {
                                 swal({ title: "Alert", text: "Data added Successfully", width: 300, height: 200 });
                             }
                             $http.get(ENV.apiUrl + "api/TransportDriver/getTransportDriver").then(function (TransportDriver_Data) {
                                 $scope.TransportDriver = TransportDriver_Data.data;
                                 $scope.totalItems = $scope.TransportDriver.length;
                                 $scope.todos = $scope.TransportDriver;
                                 $scope.makeTodos();
                             });
                         });
                     }
                     $scope.table1 = true;
                     $scope.operation = false;
                 }
             }

             $scope.Update = function (myForm) {
                 if (myForm) {

                     data1 = [];
                     var data = $scope.edt;
                     data.opr = 'U';

                     data1.push(data);
                     $http.post(ENV.apiUrl + "api/TransportDriver/updateTransportDriver", data1).then(function (msg) {
                         $scope.msg1 = msg.data;
                         if ($scope.msg1 == true) {
                             swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                         }
                         else if ($scope.msg1 == false) {
                             swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                         }
                         else {
                             swal("Error-" + $scope.msg1)
                         }
                         $scope.operation = false;
                         $http.get(ENV.apiUrl + "api/TransportDriver/getTransportDriver").then(function (TransportDriver_Data) {
                             $scope.TransportDriver = TransportDriver_Data.data;
                             $scope.totalItems = $scope.TransportDriver.length;
                             $scope.todos = $scope.TransportDriver;
                             $scope.makeTodos();

                         });
                     })
                     $scope.operation = false;
                     $scope.table1 = true;
                 }
             }

             $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

             $scope.makeTodos = function () {
                 var rem = parseInt($scope.totalItems % $scope.numPerPage);
                 if (rem == '0') {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                 }
                 else {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                 }
                 var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                 var end = parseInt(begin) + parseInt($scope.numPerPage);

                 

                 $scope.filteredTodos = $scope.todos.slice(begin, end);
             };

             $scope.CheckAllChecked = function () {
                 main = document.getElementById('mainchk');
                 if (main.checked == true) {
                     for (var i = 0; i < $scope.filteredTodos.length; i++) {
                         var v = document.getElementById($scope.filteredTodos[i].sims_driver_code+i);
                         v.checked = true;
                         $scope.row1 = 'row_selected';
                         $('tr').addClass("row_selected");
                     }
                 }
                 else {

                     for (var i = 0; i < $scope.filteredTodos.length; i++) {
                         var v = document.getElementById($scope.filteredTodos[i].sims_driver_code + i);
                         v.checked = false;
                         main.checked = false;
                         $scope.row1 = '';
                     }
                 }

             }

             $scope.checkonebyonedelete = function () {

                 $("input[type='checkbox']").change(function (e) {
                     if ($(this).is(":checked")) {
                         $(this).closest('tr').addClass("row_selected");
                         $scope.color = '#edefef';
                     } else {
                         $(this).closest('tr').removeClass("row_selected");
                         $scope.color = '#edefef';
                     }
                 });

                 main = document.getElementById('mainchk');
                 if (main.checked == true) {
                     main.checked = false;
                     $("input[type='checkbox']").change(function (e) {
                         if ($(this).is(":checked")) {
                             $(this).closest('tr').addClass("row_selected");
                         }
                         else {
                             $(this).closest('tr').removeClass("row_selected");
                         }
                     });
                 }
             }

             $scope.deleterecord = function () {
                 if ($scope.user_access.data_delete == false) {
                     swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                 }
                 else {
                 drivercode = [];
                 $scope.flag = false;
                 for (var i = 0; i < $scope.filteredTodos.length; i++) {
                     var v = document.getElementById($scope.filteredTodos[i].sims_driver_code + i);
                     if (v.checked == true) {
                         $scope.flag = true;
                         var deleteroutecode = ({
                             'sims_driver_code': $scope.filteredTodos[i].sims_driver_code,

                             opr: 'D'
                         });
                         drivercode.push(deleteroutecode);
                     }
                 }

                 if ($scope.flag) {
                     swal({
                         title: '',
                         text: "Are you sure you want to Delete?",
                         showCloseButton: true,
                         showCancelButton: true,
                         confirmButtonText: 'Yes',
                         width: 380,
                         cancelButtonText: 'No',

                     }).then(function (isConfirm) {
                         if (isConfirm) {
                             $http.post(ENV.apiUrl + "api/TransportDriver/TransportDriverdelete", drivercode).then(function (msg) {
                                 $scope.msg1 = msg.data;
                                 if ($scope.msg1 == true) {
                                     swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                         if (isConfirm) {
                                             $http.get(ENV.apiUrl + "api/TransportDriver/getTransportDriver").then(function (TransportDriver_Data) {
                                                 $scope.TransportDriver = TransportDriver_Data.data;
                                                 $scope.totalItems = $scope.TransportDriver.length;
                                                 $scope.todos = $scope.TransportDriver;
                                                 $scope.makeTodos();
                                             });
                                             main = document.getElementById('mainchk');
                                             if (main.checked == true) {
                                                 main.checked = false;
                                                 {
                                                     $scope.row1 = '';
                                                 }
                                             }
                                         }
                                     });
                                 }
                                 else if ($scope.msg1 == false) {
                                     swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                         if (isConfirm) {
                                             $http.get(ENV.apiUrl + "api/TransportDriver/getTransportDriver").then(function (TransportDriver_Data) {
                                                 $scope.TransportDriver = TransportDriver_Data.data;
                                                 $scope.totalItems = $scope.TransportDriver.length;
                                                 $scope.todos = $scope.TransportDriver;
                                                 $scope.makeTodos();
                                             });
                                             main = document.getElementById('mainchk');
                                             if (main.checked == true) {
                                                 main.checked = false;
                                                 {
                                                     $scope.row1 = '';
                                                 }
                                             }
                                         }
                                     });
                                 }
                                 else {
                                     swal("Error" + $scope.msg1)
                                 }
                             });
                         }
                         else {
                             for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                 var v = document.getElementById($scope.filteredTodos[i].sims_driver_code + i);
                                 if (v.checked == true) {
                                     v.checked = false;
                                     $scope.row1 = '';
                                     main.checked = false;
                                     $('tr').removeClass("row_selected");
                                 }
                             }
                         }
                     });
                 }
                 else {
                     swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                 }

                 $scope.currentPage = true;
             }
             }

             $scope.searched = function (valLists, toSearch) {
                 return _.filter(valLists,
                 function (i) {
                     /* Search Text in all  fields */
                     return searchUtil(i, toSearch);
                 });
             };

             $scope.search = function () {
                 $scope.todos = $scope.searched($scope.TransportDriver, $scope.searchText);
                 $scope.totalItems = $scope.todos.length;
                 $scope.currentPage = '1';
                 if ($scope.searchText == '') {
                     $scope.todos = $scope.TransportDriver;
                 }
                 $scope.makeTodos();
             }

             function searchUtil(item, toSearch) {
                 /* Search Text in all 3 fields */
                
                 return (
                      item.sims_driver_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.sims_user_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_driver_type_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_driver_license_vehicle_categoryname.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_driver_code == toSearch) ? true : false;


             }

             $timeout(function () {
                 $("#fixTable").tableHeadFixer({ 'top': 1 });
             }, 100);

             var formdata = new FormData();

             $scope.getTheFiles = function ($files) {
                 $scope.filesize = true;

                 angular.forEach($files, function (value, key) {
                     formdata.append(key, value);

                     var i = 0;
                     if ($files[i].size > 800000) {
                         $scope.filesize = false;
                         $scope.edt.photoStatus = false;
                         swal({  text: "File Should Not Exceed 800Kb.", imageUrl: "assets/img/notification-alert.png", });
                     }
                     else { }
                 });
             };

             $scope.file_changed = function (element) {

                
                 var photofile = element.files[0];

                 $scope.photo_filename = (photofile.name);

                 $scope.edt['sims_driver_img'] = $scope.photo_filename;

                 $scope.photo_filename = (photofile.type);
                 var reader = new FileReader();
                 reader.onload = function (e) {
                     $scope.$apply(function () {
                         $scope.prev_img = e.target.result;

                     });
                 };
                 reader.readAsDataURL(photofile);
                 if ($scope.edt.sims_employee_code == "" || $scope.edt.sims_employee_code == null)
                 {
                     var request = {
                         method: 'POST',
                         url: ENV.apiUrl + 'api/file/upload?filename=' + $scope.edt.sims_driver_name + '&location=DriverImage',
                         data: formdata,
                         headers: {
                             'Content-Type': undefined
                         }
                     };

                 }
                 else{
                     var request = {
                         method: 'POST',
                         url: ENV.apiUrl + 'api/file/upload?filename=' + $scope.edt.sims_employee_code + '&location=DriverImage',
                         data: formdata,
                         headers: {
                             'Content-Type': undefined
                         }
                     };

                 }
               

                 $http(request).success(function (d) {
                  
                     $scope.edt['sims_driver_img'] = $scope.edt.sims_driver_img;
                     $scope.edt['sims_driver_img'] = d;
                 });


             };


        }])


    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

})();





