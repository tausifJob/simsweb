﻿(function () {
    'use strict';
    var opr = '';
    var routestopcode = [];
    var main;
    var data1 = [];

    var simsController = angular.module('sims.module.Fleet');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TransportRouteStopCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.edt = "";
            $scope.table1 = true;
            $scope.operation = false;

            $scope.propertyName = null;
            $scope.reverse = false;
            
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $http.get(ENV.apiUrl + "api/TransportRouteStop/getRouteStop").then(function (RouteStop_Data) {
                $scope.RouteStopData = RouteStop_Data.data;
                $scope.totalItems = $scope.RouteStopData.length;
                $scope.todos = $scope.RouteStopData;
                $scope.makeTodos();
            });

            $http.get(ENV.apiUrl + "api/common/getAllAcademicYearstop").then(function (get_AllAcademicYear) {
                $scope.ActiveYear = get_AllAcademicYear.data;
                if (get_AllAcademicYear.data.length > 0)
                {
                    $scope.edt.sims_academic_year = get_AllAcademicYear.data[0].sims_academic_year;
                    $scope.getroutecodename($scope.edt.sims_academic_year);
                }
                
            });

            $scope.getroutecodename = function (Acayear) {
                $http.get(ENV.apiUrl + "api/common/getAllRouteName?Acayear=" + Acayear).then(function (get_AllRouteName) {
                    $scope.AllRouteName = get_AllRouteName.data;
                    $scope.getdirection($scope.edt.sims_transport_route_code, Acayear);

                });
                $http.get(ENV.apiUrl + "api/common/getAllStopName?Acayear=" + Acayear).then(function (get_AllStopName) {
                    $scope.AllStopName = get_AllStopName.data;
                });
            }

            $scope.getdirection = function (route_code, aca_year) {
                //$http.get(ENV.apiUrl + "api/common/getDirectionByRouteName?route_code=" + route_code + "&aca_year=" + aca_year).then(function (get_DirectionName) {
                //    $scope.DirectionName = get_DirectionName.data;
                //})

                var temp = [];
                for (var i = 0; i < $scope.AllRouteName.length; i++) {
                    if ($scope.AllRouteName[i].sims_transport_route_code == route_code) {
                        temp.push($scope.AllRouteName[i])
                    }
                }
                $scope.DirectionName = temp;
               
            }

            $scope.size = function (str) {
               
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;  $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.RouteStopData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();
                //$scope.CheckAllChecked();
                $scope.chk = {}
                $scope.chk['check_all'] = false;
                $scope.row1 = '';
            }

            $('.clockpicker').clockpicker({
                autoclose: true
            });

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    $scope.check = true;

                    $scope.editmode = false;
                    $scope.newmode = true;
                    $scope.readonly = false;
                    $scope.table1 = false;
                    $scope.operation = true;
                    $scope.savebtn = true;
                    $scope.updatebtn = false;
                    $scope.myForm.$setPristine();
                    $scope.myForm.$setUntouched();

                    $scope.edt = {
                        sims_transport_stop_code: "",
                        sims_transport_stop_name: "",
                        sims_transport_stop_landmark: "",
                        sims_transport_stop_lat: "",
                        sims_transport_stop_long: "",
                        sims_transport_route_stop_status:true
                    }

                    if ($scope.ActiveYear.length > 0) {
                        $scope.edt.sims_academic_year = $scope.ActiveYear[0].sims_academic_year;
                        $scope.getroutecodename($scope.edt.sims_academic_year);
                    }
                }

            }

            $scope.up = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                
                    $scope.savebtn = false;
                    $scope.updatebtn = true;
                    $scope.table1 = false;
                    $scope.operation = true;
                    $scope.editmode = true;
                    $scope.newmode = false;
                    $scope.newmode1 = true;
                    $scope.readonly = true;

                    $scope.edt = {

                        sims_academic_year: str.sims_academic_year,
                        sims_academic_year_description: str.sims_academic_year_description,

                        sims_transport_route_stop_code: str.sims_transport_route_stop_code,
                        sims_transport_stop_name: str.sims_transport_stop_name,

                        sims_transport_route_code: str.sims_transport_route_code,
                        sims_transport_route_name: str.sims_transport_route_name,

                        sims_transport_route_direction: str.sims_transport_route_direction,
                        sims_transport_route_direction_name: str.sims_transport_route_direction_name,

                        sims_transport_route_stop_expected_time: str.sims_transport_route_stop_expected_time,
                        sims_transport_route_stop_waiting_time: str.sims_transport_route_stop_waiting_time,

                        sims_transport_route_stop_status: str.sims_transport_route_stop_status,
                    }

                    $scope.getroutecodename(str.sims_academic_year);

                }
            }
        

            $scope.cancel = function () {

                $scope.table1 = true;
                $scope.operation = false;


                $scope.edt = {
                    sims_transport_stop_code: "",
                    sims_transport_stop_name: "",
                    sims_transport_stop_landmark: "",
                    sims_transport_stop_lat: "",
                    sims_transport_stop_long: ""
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    data1 = [];
                    var data = $scope.edt;
                    data.opr = 'I';

                    //for (var i = 0; i < $scope.AllRouteName.length; i++) {
                    //    if ($scope.AllRouteName[i].sims_transport_route_name == data.sims_transport_route_code) {
                    //        data.sims_transport_route_code = $scope.AllRouteName[i].sims_transport_route_code;
                    //    }
                    //}


                    $scope.exist = false;
                    for (var i = 0; i < $scope.RouteStopData.length; i++) {
                        if ($scope.RouteStopData[i].sims_academic_year == data.sims_academic_year &&
                            $scope.RouteStopData[i].sims_transport_route_stop_code == data.sims_transport_route_stop_code &&
                            $scope.RouteStopData[i].sims_transport_route_code == data.sims_transport_route_code &&
                            $scope.RouteStopData[i].sims_transport_route_direction == data.sims_transport_route_direction) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ text: "Record Already Exists", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 })
                    }
                    else {
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/TransportRouteStop/CUDRoutetStop", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                            $http.get(ENV.apiUrl + "api/TransportRouteStop/getRouteStop").then(function (RouteStop_Data) {
                                $scope.RouteStopData = RouteStop_Data.data;
                                $scope.totalItems = $scope.RouteStopData.length;
                                $scope.todos = $scope.RouteStopData;
                                $scope.makeTodos();
                            });

                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;

                }

            }

            $scope.Update = function (myForm) {
               
                if (myForm) {
                    data1 = [];
                    var data = $scope.edt;
                    data.opr = 'U';
                    //for (var i = 0; i < $scope.AllRouteName.length; i++) {
                    //    if ($scope.AllRouteName[i].sims_transport_route_name == data.sims_transport_route_code) {
                    //        data.sims_transport_route_code = $scope.AllRouteName[i].sims_transport_route_code;
                    //    }
                    //}
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/TransportRouteStop/CUDRoutetStop", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + scope.msg1)
                        }
                        $scope.operation = false;
                        $http.get(ENV.apiUrl + "api/TransportRouteStop/getRouteStop").then(function (RouteStop_Data) {
                            $scope.RouteStopData = RouteStop_Data.data;
                            $scope.totalItems = $scope.RouteStopData.length;
                            $scope.todos = $scope.RouteStopData;
                            $scope.makeTodos();

                        });
                    })
                    $scope.operation = false;
                    $scope.table1 = true;

                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

               
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
              
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.deleterecord = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                routestopcode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deleteroutecode = ({
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            'sims_transport_route_code': $scope.filteredTodos[i].sims_transport_route_code,
                            'sims_transport_route_direction': $scope.filteredTodos[i].sims_transport_route_direction,
                            'sims_transport_route_stop_code': $scope.filteredTodos[i].sims_transport_route_stop_code,
                            opr: 'D'
                        });
                        routestopcode.push(deleteroutecode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/TransportRouteStop/CUDRoutetStop", routestopcode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/TransportRouteStop/getRouteStop").then(function (RouteStop_Data) {
                                                $scope.RouteStopData = RouteStop_Data.data;
                                                $scope.totalItems = $scope.RouteStopData.length;
                                                $scope.todos = $scope.RouteStopData;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/TransportRouteStop/getRouteStop").then(function (RouteStop_Data) {
                                                $scope.RouteStopData = RouteStop_Data.data;
                                                $scope.totalItems = $scope.RouteStopData.length;
                                                $scope.todos = $scope.RouteStopData;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }

                $scope.currentPage = true;
            }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.RouteStopData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.RouteStopData;
                }
                $scope.makeTodos();

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                
                return (item.sims_academic_year_description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_transport_route_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_transport_route_direction_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_transport_stop_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_transport_route_stop_expected_time.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_transport_route_stop_waiting_time.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 == toSearch) ? true : false;

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


        }])
})();