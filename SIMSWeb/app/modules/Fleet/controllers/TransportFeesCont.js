﻿(function () {
    'use strict';
    var deletetransfeecode = [];
    var routename;
    var main;
    var data1 = [];
    var data = [];
    var simsController = angular.module('sims.module.Fleet');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TransportFeesCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';

            $scope.table1 = true;
            $scope.editmode = false;

            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.countData = [
              { val: 10, data: 10 },
              { val: 20, data: 20 },
              { val: 'All', data: 'All' },

            ]
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            
            $http.get(ENV.apiUrl + "api/TransportFees/getTransportFees").then(function (get_TransportFees) {
                $scope.operation = false;
                $scope.table1 = true;
                $scope.TransportFees_Data = get_TransportFees.data;
                if ($scope.TransportFees_Data.length > 0) {
                    $scope.pager = true;
                    /*if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.TransportFees_Data.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.TransportFees_Data.length, data: 'All' })
                    }*/

                    $scope.totalItems = $scope.TransportFees_Data.length;
                    $scope.todos = $scope.TransportFees_Data;
                    $scope.makeTodos();
                    $scope.table1 = true;
                }
                else {
                    swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    $scope.filteredTodos = [];
                }

            });

            $scope.getgrid = function () {
                $scope.currentPage = 1;

                $http.get(ENV.apiUrl + "api/TransportFees/getTransportFees").then(function (get_TransportFees) {
                    $scope.operation = false;
                    $scope.table1 = true;
                    $scope.TransportFees_Data = get_TransportFees.data;
                    if ($scope.TransportFees_Data.length > 0) {
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.TransportFees_Data.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.TransportFees_Data.length, data: 'All' })
                        }

                        $scope.totalItems = $scope.TransportFees_Data.length;
                        $scope.todos = $scope.TransportFees_Data;
                        $scope.makeTodos();
                        $scope.table1 = true;
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                        $scope.filteredTodos = [];
                    }

                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
                $scope.row1 = '';
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $http.get(ENV.apiUrl + "api/common/getAllAcademicYear").then(function (get_AllAcademicYear) {
                $scope.ActiveYear = get_AllAcademicYear.data;
                $scope.edt = { sims_academic_year: $scope.ActiveYear[0].sims_academic_year };
            });

            $scope.getroutecodename = function (Acayear) {

                $http.get(ENV.apiUrl + "api/TransportFees/getallRouteNameForTransFees?Acayear=" + Acayear).then(function (get_AllRouteName) {
                    $scope.AllRouteName = get_AllRouteName.data;
                })
            }

            $scope.getdirection = function (route_code, aca_year) {

                $http.get(ENV.apiUrl + "api/common/getDirectionByRouteName?route_code=" + route_code + "&aca_year=" + aca_year).then(function (get_DirectionName) {
                    $scope.DirectionName = get_DirectionName.data;
                })

            }

            $http.get(ENV.apiUrl + "api/common/getAllFeeType").then(function (get_AllFeeType) {
                $scope.AllFeeType = get_AllFeeType.data;

               

            });

            $http.get(ENV.apiUrl + "api/common/getFeeFrequency").then(function (get_FeeFrequency) {
                $scope.FeeFrequency_data = get_FeeFrequency.data;
               });

            $http.get(ENV.apiUrl + "api/common/getFeeCategory").then(function (get_FeeCategory) {
                $scope.Fee_Category = get_FeeCategory.data;
            });

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                //$scope.cmb_route_code = true;
                //$scope.cmb_route_direct = true;
                //$scope.sims_transport_route_name = false;
                //$scope.sims_transport_route_name = false;
                $scope.editmode = false;
                $scope.newmode = true;
                $scope.readonly = false;
                $scope.check = true;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {};
                $scope.edt['sims_transport_status'] = true;
                $scope.edt = { sims_academic_year: $scope.ActiveYear[0].sims_academic_year };
                $scope.getroutecodename($scope.edt.sims_academic_year);
              }
            }

            $scope.up = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                //$scope.sims_transport_route_name = true;
                //$scope.sims_transport_direction_name = true;
                //$scope.cmb_route_code = false;
                //$scope.cmb_route_direct = false;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.readonly = true;
                $scope.savebtn = false;
                $scope.updatebtn = true;

                $scope.table1 = false;
                $scope.operation = true;

                $scope.edt = {

                    sims_transport_fee_number: str.sims_transport_fee_number,
                    sims_academic_year: str.sims_academic_year,
                    sims_academic_year_description: str.sims_academic_year_description,
                    sims_transport_route_code: str.sims_transport_route_code,
                    sims_transport_route_name: str.sims_transport_route_name,
                    sims_transport_category: str.sims_transport_category,
                    sims_fee_category_description: str.sims_fee_category_description,
                    sims_transport_route_direction: str.sims_transport_route_direction,
                    sims_transport_route_direction_name: str.sims_transport_route_direction_name,
                    sims_transport_code: str.sims_transport_code,
                    sims_fee_code_description: str.sims_fee_code_description,
                    sims_transport_amount: str.sims_transport_amount,
                    sims_transport_installment_min_amount: str.sims_transport_installment_min_amount,
                    sims_transport_frequency: str.sims_transport_frequency,
                    FeeFrequency: str.FeeFrequency,
                    sims_transport_fee_code_type: str.sims_transport_fee_code_type,
                    FeeCodeType: str.FeeCodeType,

                    transport_fee_period1: str.transport_fee_period1,
                    transport_fee_period2: str.transport_fee_period2,
                    transport_fee_period3: str.transport_fee_period3,
                    transport_fee_period4: str.transport_fee_period4,
                    transport_fee_period5: str.transport_fee_period5,
                    transport_fee_period6: str.transport_fee_period6,
                    transport_fee_period7: str.transport_fee_period7,
                    transport_fee_period8: str.transport_fee_period8,
                    transport_fee_period9: str.transport_fee_period9,
                    transport_fee_period10: str.transport_fee_period10,
                    transport_fee_period11: str.transport_fee_period11,
                    transport_fee_period12: str.transport_fee_period12,
                    sims_transport_status: str.sims_transport_status,
                    sims_transport_installment_mode: str.sims_transport_installment_mode,
                    sims_transport_mandatory: str.sims_transport_mandatory,
                    sims_transport_refundable: str.sims_transport_refundable,
                    sims_transport_manual_receipt: str.sims_transport_manual_receipt
                }


                $scope.getroutecodename(str.sims_academic_year);

                $scope.getdirection(str.sims_transport_route_code, str.sims_academic_year);
              }
            }

            $scope.cancel = function () {

                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.edt = {

                    sims_transport_fee_number: '',
                    sims_academic_year: '',
                    sims_transport_route_code: '',
                    sims_transport_route_name: '',
                    sims_transport_category: '',
                    sims_fee_category_description: '',
                    sims_transport_route_direction: '',
                    sims_transport_route_direction_name: '',
                    sims_transport_code: '',
                    sims_fee_code_description: '',
                    sims_transport_amount: '',
                    sims_transport_installment_min_amount: '',
                    sims_transport_frequency: '',
                    FeeFrequency: '',
                    sims_transport_fee_code_type: '',
                    FeeCodeType: '',

                    transport_fee_period1: '',
                    transport_fee_period2: '',
                    transport_fee_period3: '',
                    transport_fee_period4: '',
                    transport_fee_period5: '',
                    transport_fee_period6: '',
                    transport_fee_period7: '',
                    transport_fee_period8: '',
                    transport_fee_period9: '',
                    transport_fee_period10: '',
                    transport_fee_period11: '',
                    transport_fee_period12: '',
                    sims_transport_status: '',
                    sims_transport_installment_mode: '',
                    sims_transport_mandatory: '',
                    sims_transport_refundable: '',
                    sims_transport_manual_receipt: ''
                }
            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    data1 = [];
                    data = $scope.edt;
                    data.opr = 'I';
                    data1.push(data);
                    $scope.exist = false;
                    for (var i = 0; i < $scope.TransportFees_Data.length; i++) {
                        if ($scope.TransportFees_Data[i].sims_transport_route_code == data.sims_transport_route_code && $scope.TransportFees_Data[i].sims_transport_route_direction == data.sims_transport_route_direction) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ text: "Record Already Exists", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }
                    else {

                        //for (var i = 0; i < $scope.AllRouteName.length; i++)
                        //{
                        //    if ($scope.AllRouteName[i].sims_transport_route_name == $scope.edt.sims_transport_route_name)
                        //        data.sims_transport_route_code = $scope.AllRouteName[i].sims_transport_route_code;
                        //}

                        $http.post(ENV.apiUrl + "api/TransportFees/CUDTransportFees", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                            $scope.operation = false;
                            $http.get(ENV.apiUrl + "api/TransportFees/getTransportFees").then(function (get_TransportFees) {
                                $scope.TransportFees_Data = get_TransportFees.data;
                                $scope.totalItems = $scope.TransportFees_Data.length;
                                $scope.todos = $scope.TransportFees_Data;
                                $scope.makeTodos();
                            });
                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                }
            }

            $scope.Update = function (myForm) {
               
                if (myForm) {
                    data1 = [];
                    data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/TransportFees/CUDTransportFees", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.operation = false;
                        $http.get(ENV.apiUrl + "api/TransportFees/getTransportFees").then(function (get_TransportFees) {
                            $scope.TransportFees_Data = get_TransportFees.data;
                            $scope.totalItems = $scope.TransportFees_Data.length;
                            $scope.todos = $scope.TransportFees_Data;
                            $scope.makeTodos();

                            if ($scope.msg1 == true) {
                                swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                                $http.get(ENV.apiUrl + "api/TransportFees/getStudentTransportData?data=" + JSON.stringify($scope.edt)).then(function (get_StudentTransportFees) {
                                    $scope.Student_TransportFees_Data = get_StudentTransportFees.data.table;
                                    if ($scope.Student_TransportFees_Data.length > 0) {
                                        $('#studentSearch').modal('show');
                                    }
                                });

                            }

                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });

                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                }
            }

            $scope.UpdateStudentFee = function () {
                var data2 = [];


                data2.push($scope.Student_TransportFees_Data);

                var ar = [];
                for (var i = 0; i < $scope.Student_TransportFees_Data.length; i++) {

                    var ob = {};
                    ob.sims_transport_academic_year = $scope.Student_TransportFees_Data[i].sims_transport_academic_year;
                    ob.sims_fee_number = $scope.Student_TransportFees_Data[i].sims_fee_number;
                    ob.sims_fee_cur_code = $scope.Student_TransportFees_Data[i].sims_fee_cur_code;
                    ob.sims_fee_grade_code = $scope.Student_TransportFees_Data[i].sims_fee_grade_code;
                    ob.sims_fee_section_code = $scope.Student_TransportFees_Data[i].sims_fee_section_code;
                    ob.sims_enroll_number = $scope.Student_TransportFees_Data[i].sims_enroll_number;
                    ob.sims_transport_route_code = $scope.Student_TransportFees_Data[i].sims_transport_route_code;
                    ob.sims_transport_route_direction = $scope.Student_TransportFees_Data[i].sims_transport_route_direction;
                    ob.sims_transport_period1 = $scope.Student_TransportFees_Data[i].sims_transport_period1;
                    ob.sims_transport_period2 = $scope.Student_TransportFees_Data[i].sims_transport_period2;
                    ob.sims_transport_period3 = $scope.Student_TransportFees_Data[i].sims_transport_period3;
                    ob.sims_transport_period4 = $scope.Student_TransportFees_Data[i].sims_transport_period4;
                    ob.sims_transport_period5 = $scope.Student_TransportFees_Data[i].sims_transport_period5;
                    ob.sims_transport_period6 = $scope.Student_TransportFees_Data[i].sims_transport_period6;
                    ob.sims_transport_period7 = $scope.Student_TransportFees_Data[i].sims_transport_period7;
                    ob.sims_transport_period8 = $scope.Student_TransportFees_Data[i].sims_transport_period8;
                    ob.sims_transport_period9 = $scope.Student_TransportFees_Data[i].sims_transport_period9;
                    ob.sims_transport_period10 = $scope.Student_TransportFees_Data[i].sims_transport_period10;
                    ob.sims_transport_period11 = $scope.Student_TransportFees_Data[i].sims_transport_period11;
                    ob.sims_transport_period12 = $scope.Student_TransportFees_Data[i].sims_transport_period12;
                    ob.sims_transport_effective_from = $scope.Student_TransportFees_Data[i].sims_transport_effective_from;
                    ob.sims_transport_effective_upto = $scope.Student_TransportFees_Data[i].sims_transport_effective_upto;
                    ar.push(ob);
                }


                $http.post(ENV.apiUrl + "api/TransportFees/CUDStudentTransportFees", ar).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Student Transport Fee Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                    }

                    else if ($scope.msg1 == false) {
                        swal({ text: "Student Transport Fee Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                });
            }

            $scope.size = function (str) {
                /*if (str == 10 || str == 20 || str == 'All') {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();


                */
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }

                if (str == "All") {
                    $scope.currentPage = 1;
                    $scope.numPerPage = $scope.TransportFees_Data.length;
                    $scope.makeTodos();

                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['check_all'] = false;
                $scope.row1 = '';
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    $scope.filteredTodos[i].icon = 'fa fa-plus-circle';
                }
            };

            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_name + i);
                    if (main.checked == true) {
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }

                    else {
                        v.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }

            $scope.deleterecord = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                deletetransfeecode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_name + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletelocacode = ({
                            'sims_transport_fee_number': $scope.filteredTodos[i].sims_transport_fee_number,
                            opr: 'D'
                        });
                        deletetransfeecode.push(deletelocacode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/TransportFees/CUDTransportFees", deletetransfeecode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/TransportFees/getTransportFees").then(function (get_TransportFees) {
                                                $scope.TransportFees_Data = get_TransportFees.data;
                                                $scope.totalItems = $scope.TransportFees_Data.length;
                                                $scope.todos = $scope.TransportFees_Data;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/TransportFees/getTransportFees").then(function (get_TransportFees) {
                                                $scope.TransportFees_Data = get_TransportFees.data;
                                                $scope.totalItems = $scope.TransportFees_Data.length;
                                                $scope.todos = $scope.TransportFees_Data;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }

                $scope.currentPage = true;
             }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.TransportFees_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.TransportFees_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_transport_route_direction_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_transport_route_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.feeFrequency.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_academic_year_description.toLowerCase().indexOf(toSearch.toLowerCase())>-1||
                        item.sims_transport_amount.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_academic_year_description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_transport_fee_number == toSearch) ? true : false;

            }

            $scope.CalculateAmount = function () {

                if ($scope.edt.sims_transport_frequency != undefined) {
                    var decimal = ($scope.edt.sims_transport_amount.toString().split(".")[1]);

                    if (decimal == undefined) {
                        decimal = '00'
                    }
                    //var amount = Math.abs(($scope.edt.sims_transport_amount));
                    var amount = Math.abs(($scope.edt.sims_transport_amount.toString().split(".")[0]))
                    var temp_value1 = 0;
                    temp_value1 = (parseInt(Math.sign(amount / 3) * Math.ceil(Math.abs(amount / 3) / 5) * 5));

                    if ($scope.edt.sims_transport_frequency == 'O' || $scope.edt.sims_transport_frequency == 'Y') {
                        //  var amount = ($scope.search_obj.transport_fee_amount);
                        $scope.edt["transport_fee_period1"] = $scope.edt.sims_transport_amount;
                        $scope.edt["transport_fee_period2"] = '0.0';
                        $scope.edt["transport_fee_period3"] = '0.0';
                        $scope.edt["transport_fee_period4"] = '0.0';
                        $scope.edt["transport_fee_period5"] = '0.0';
                        $scope.edt["transport_fee_period6"] = '0.0';
                        $scope.edt["transport_fee_period7"] = '0.0';
                        $scope.edt["transport_fee_period8"] = '0.0';
                        $scope.edt["transport_fee_period9"] = '0.0';
                        $scope.edt["transport_fee_period10"] = '0.0';
                        $scope.edt["transport_fee_period11"] = '0.0';
                        $scope.edt["transport_fee_period12"] = '0.0';
                    }

                    if ($scope.edt.sims_transport_frequency == 'T') {
                        var temp_value2 = temp_value1;
                        var temp_value3 = (parseInt(amount - (temp_value1 * 2)));
                        if (amount < 6) { temp_value1 = amount; temp_value2 = '0'; temp_value3 = '0'; }
                        if (temp_value3 < 0) {
                            temp_value2 = parseFloat(temp_value2) + parseFloat(temp_value3); if (temp_value2 < 0) { temp_value2 = '0'; }
                            temp_value3 = '0';
                        }
                        //  var amount = parseInt($scope.search_obj.transport_fee_amount);
                        $scope.edt["transport_fee_period1"] = temp_value1;
                        $scope.edt["transport_fee_period2"] = temp_value2;
                        $scope.edt["transport_fee_period3"] = temp_value3 + '.' + decimal;
                        $scope.edt["transport_fee_period4"] = '0.0';
                        //$scope.search_obj["transport_fee_period1"] = Math.ceil(temp_value); $scope.search_obj["transport_fee_period2"] = Math.ceil(temp_value); $scope.search_obj["transport_fee_period3"] = parseInt(temp_value) + '.' + decimal; $scope.search_obj["transport_fee_period4"] = '0'; 
                        $scope.edt["transport_fee_period5"] = '0.0';
                        $scope.edt["transport_fee_period6"] = '0.0';
                        $scope.edt["transport_fee_period7"] = '0.0';
                        $scope.edt["transport_fee_period8"] = '0.0';
                        $scope.edt["transport_fee_period9"] = '0.0';
                        $scope.edt["transport_fee_period10"] = '0.0';
                        $scope.edt["transport_fee_period11"] = '0.0';
                        $scope.edt["transport_fee_period12"] = '0.0';
                    }

                    if ($scope.edt.sims_transport_frequency == 'M') {
                        //  var amount = parseInt($scope.search_obj.transport_fee_amount);
                        var temp_value = parseInt(amount / 10);
                        var rem_amt = parseInt(amount - (temp_value * 10));
                        //var temp_value = (parseInt(Math.sign(amount / 10) * Math.ceil(Math.abs(amount / 10) / 5) * 5)); //alert(temp_value1);

                        //  temp_value = parseInt(Math.sign(amount / 10) * Math.ceil(Math.abs(amount / 3)));
                        //  var dec = parseInt (amount - (temp_value * 9));
                        $scope.edt["transport_fee_period1"] = (temp_value);
                        $scope.edt["transport_fee_period2"] = (temp_value);
                        $scope.edt["transport_fee_period3"] = (temp_value) + '.' + decimal;
                        $scope.edt["transport_fee_period4"] = temp_value;
                        $scope.edt["transport_fee_period5"] = temp_value;
                        $scope.edt["transport_fee_period6"] = temp_value;
                        $scope.edt["transport_fee_period7"] = '0.0';
                        $scope.edt["transport_fee_period8"] = '0.0';
                        $scope.edt["transport_fee_period9"] = temp_value;
                        $scope.edt["transport_fee_period10"] = temp_value;
                        $scope.edt["transport_fee_period11"] = temp_value;
                        $scope.edt["transport_fee_period12"] = temp_value + rem_amt;

                    }
                }
            }

            $scope.Checkfeecal = function () {

                var decimal = ($scope.edt.sims_transport_amount.toString().split(".")[1]);

                if (decimal == undefined) {
                    decimal = '00'
                }
                //var amount = Math.abs(($scope.edt.sims_transport_amount));
                var amount = Math.abs(($scope.edt.sims_transport_amount.toString().split(".")[0]))
                var temp_value1 = 0;
                temp_value1 = (parseInt(Math.sign(amount / 3) * Math.ceil(Math.abs(amount / 3) / 5) * 5));

                if ($scope.edt.sims_transport_frequency == 'O' || $scope.edt.sims_transport_frequency == 'Y') {
                    $scope.edt["transport_fee_period1"] = $scope.edt.sims_transport_amount + '.' + decimal;
                    $scope.edt["transport_fee_period2"] = '0.0';
                    $scope.edt["transport_fee_period3"] = '0.0';
                    $scope.edt["transport_fee_period4"] = '0.0';
                    $scope.edt["transport_fee_period5"] = '0.0';
                    $scope.edt["transport_fee_period6"] = '0.0';
                    $scope.edt["transport_fee_period7"] = '0.0';
                    $scope.edt["transport_fee_period8"] = '0.0';
                    $scope.edt["transport_fee_period9"] = '0.0';
                    $scope.edt["transport_fee_period10"] = '0.0';
                    $scope.edt["transport_fee_period11"] = '0.0';
                    $scope.edt["transport_fee_period12"] = '0.0';
                }

                if ($scope.edt.sims_transport_frequency == 'T') {
                    var temp_value2 = temp_value1;
                    var temp_value3 = (parseInt(amount - (temp_value1 * 2)));
                    if (amount < 6) { temp_value1 = amount; temp_value2 = '0'; temp_value3 = '0'; }
                    if (temp_value3 < 0) {
                        temp_value2 = parseFloat(temp_value2) + parseFloat(temp_value3); if (temp_value2 < 0) { temp_value2 = '0'; }
                        temp_value3 = '0';
                    }
                    $scope.edt["transport_fee_period1"] = temp_value1;
                    $scope.edt["transport_fee_period2"] = temp_value2;
                    $scope.edt["transport_fee_period3"] = temp_value3 + '.' + decimal;
                    $scope.edt["transport_fee_period4"] = '0.0';
                    $scope.edt["transport_fee_period5"] = '0.0';
                    $scope.edt["transport_fee_period6"] = '0.0';
                    $scope.edt["transport_fee_period7"] = '0.0';
                    $scope.edt["transport_fee_period8"] = '0.0';
                    $scope.edt["transport_fee_period9"] = '0.0';
                    $scope.edt["transport_fee_period10"] = '0.0';
                    $scope.edt["transport_fee_period11"] = '0.0';
                    $scope.edt["transport_fee_period12"] = '0.0';
                }

                if ($scope.edt.sims_transport_frequency == 'M') {
                    var temp_value = parseInt(amount / 10);
                    var rem_amt = parseInt(amount - (temp_value * 10));
                    $scope.edt["transport_fee_period1"] = (temp_value); 
                    $scope.edt["transport_fee_period2"] = (temp_value);
                    $scope.edt["transport_fee_period3"] = (temp_value) + '.' + decimal;
                    $scope.edt["transport_fee_period4"] = (temp_value);
                    $scope.edt["transport_fee_period5"] = (temp_value);
                    $scope.edt["transport_fee_period6"] = (temp_value);
                    $scope.edt["transport_fee_period7"] = '0.0';
                    $scope.edt["transport_fee_period8"] = '0.0';
                    $scope.edt["transport_fee_period9"] = (temp_value);
                    $scope.edt["transport_fee_period10"] = (temp_value);
                    $scope.edt["transport_fee_period11"] = (temp_value);
                    $scope.edt["transport_fee_period12"] = (temp_value + rem_amt);
                }
                $scope.edt["sims_transport_amount"] = amount + '.' + decimal;
            }

            var dom;
            $scope.flag = true;
            $scope.expand = function (obj, $event) {
               
                if ($scope.flag == true) {
                    $(dom).remove();
                    obj.icon = "fa fa-minus-circle"
                    dom = $("<tr id='innerRow'><td class='details' colspan='12'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                         "<tr><td class='semi-bold'>" + "Category" + "</td><td class='semi-bold'>" + "Fee type" + "</td> <td class='semi-bold'>" + "Fee Frequency" + " </td></tr>" +

                          "<tr><td>" + obj.sims_fee_category_description + "</td><td>" + obj.sims_fee_code_description + "</td> <td>" + obj.sims_transport_frequency + " </td></tr>" +

                         "<tr><td class='semi-bold'><div class='pull-right'>" + "January" + "</div></td><td class='semi-bold'><div class='pull-right'>" + "February" + "</div></td> <td class='semi-bold'><div class='pull-right'>" + "March" + "</div></td><td class='semi-bold'><div class='pull-right'>" + "April" + "</div></td><td class='semi-bold'><div class='pull-right'>" + "May" + "</div></td> <td class='semi-bold'><div class='pull-right'>" + "June" + "</div></td><td class='semi-bold'><div class='pull-right'>" + "July" + "</div></td><td class='semi-bold'><div class='pull-right'>" + "August" + "</div></td><td class='semi-bold'><div class='pull-right'>" + "September" + "</div></td><td class='semi-bold'><div class='pull-right'>" + "Octomber" + "</div></td><td class='semi-bold'><div class='pull-right'>" + "November" + "</div></td><td class='semi-bold'><div class='pull-right'>" + "December" + "</div></td>" +
                       "</tr>" +

                         "<tr><td>" + '<input disabled="disabled" ng-readonly="readonly" type="text" style="width:70px" class="form-control input-sm"  value=' + obj.transport_fee_period1 + '></input>' + "</td><td>" + '<input disabled="disabled" type="text" style="width:70px" class="form-control input-sm" value=' + obj.transport_fee_period2 + '></input>' + "</td><td>" + '<input disabled="disabled" type="text" style="width:70px" class="form-control input-sm"  value=' + obj.transport_fee_period3 + '></input>' + "</td> <td>" + '<input disabled="disabled" type="text" style="width:70px" class="form-control input-sm"  value=' + obj.transport_fee_period4 + '></input>' + "</td><td>" + '<input disabled="disabled" type="text" style="width:70px" class="form-control input-sm"  value=' + obj.transport_fee_period5 + '></input>' + "</td><td>" + '<input disabled="disabled" type="text" style="width:70px" class="form-control input-sm"  value=' + obj.transport_fee_period6 + '></input>' + "</td><td>" + '<input disabled="disabled" type="text" style="width:70px" class="form-control input-sm"  value=' + obj.transport_fee_period7 + '></input>' + "</td><td>" + '<input disabled="disabled" type="text" style="width:70px" class="form-control input-sm"  value=' + obj.transport_fee_period8 + '></input>' + "</td>" +
                         "<td>" + '<input disabled="disabled" type="text" style="width:70px" class="form-control input-sm"  value=' + obj.transport_fee_period9 + '></input>' + " </td><td>" + '<input disabled="disabled" type="text" style="width:70px" class="form-control input-sm"  value=' + obj.transport_fee_period10 + '></input>' + "</td><td>" + '<input disabled="disabled" type="text" style="width:70px" class="form-control input-sm"  value=' + obj.transport_fee_period11 + '></input>' + "</td><td>" + '<input disabled="disabled" type="text" style="width:70px" class="form-control input-sm"  value=' + obj.transport_fee_period12 + '></input>' + "</td>"
                        + " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);

                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }

            };
        }]
        )
})();