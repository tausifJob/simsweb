﻿(function () {
    'use strict';
    var opr = '';
    var stopcode = [];
    var main;
    var data1 = [];

    var simsController = angular.module('sims.module.Fleet');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TransportStopCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.edt = "";
            $scope.table1 = true;

            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            $http.get(ENV.apiUrl + "api/TransportStop/getTransportStop").then(function (Transport_Stop) {
                $scope.TransportStop = Transport_Stop.data;
                $scope.totalItems = $scope.TransportStop.length;
                $scope.todos = $scope.TransportStop;
                $scope.makeTodos();

            });
            //$scope.Grid_Data();
            //$scope.Grid_Data = function () {
            //    $http.get(ENV.apiUrl + "api/TransportStop/getTransportStop").then(function (Transport_Stop) {
            //        $scope.TransportStop = Transport_Stop.data;
            //        $scope.totalItems = $scope.TransportStop.length;
            //        $scope.todos = $scope.TransportStop;
            //        $scope.makeTodos();

            //    });
            //}
            $http.get(ENV.apiUrl + "api/common/getAllAcademicYearstop").then(function (get_AllAcademicYear) {
                $scope.ActiveYear = get_AllAcademicYear.data;
                //$scope.edt = { sims_academic_year: $scope.ActiveYear.data[0].sims_academic_year, }
            });

            $scope.propertyName = null;
            $scope.reverse = false;
            // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };
            $scope.operation = false;
            $scope.editmode = false;

            $scope.size = function (str) {

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }

                if (str == "All") {
                    $scope.currentPage = 1;
                    $scope.numPerPage = $scope.TransportStop.length;
                    $scope.makeTodos();

                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.edt = "";
                    $scope.check = true;
                    $scope.edt = { sims_academic_year: $scope.ActiveYear[0].sims_academic_year }
                    opr = 'S';
                    $scope.editmode = false;
                    $scope.newmode = true;
                    $scope.readonly = false;
                    $scope.table1 = false;
                    $scope.operation = true;
                    $scope.savebtn = true;
                    $scope.updatebtn = false;
                    $scope.myForm.$setPristine();
                    $scope.myForm.$setUntouched();

                }
            }
            $scope.edt = {};
            $scope.getDataFromGoogle = function () {
                var v = document.getElementById('chk_Latitude');
                if (v.checked == true) {
                  
                    if ($scope.edt.sims_transport_stop_landmark != undefined) {
                        $('#loader0712').modal({ backdrop: 'static', keyboard: false });
                        $scope.edt['sims_transport_stop_long'] = ''
                        $scope.edt['sims_transport_stop_lat'] = ''

                        setTimeout(function () {
                            $("#cmb_uom_code").select2();
                        }, 100);

                        $.ajax({
                            url: "http://maps.googleapis.com/maps/api/geocode/json?address=" + $scope.edt.sims_transport_stop_landmark + "&sensor=false",
                            type: "POST",
                            success: function (res712) {
                                $scope.edt['sims_transport_stop_long'] = res712.results[0].geometry.location.lng,
                                $scope.edt['sims_transport_stop_lat'] = res712.results[0].geometry.location.lat
                                res712 = '';
                                $('#loader0712').modal('hide');
                            }
                        });
                    }
                    else {                       
                        swal({ text: "Please fill the Stop Landmark data.", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        document.getElementById("Text10").focus();
                    }
                }
              

            }

            $scope.getlat = function () {
                debugger;
                if ($scope.edt.sims_transport_stop_landmark != undefined) {
                    swal({
                        title: '',
                        text: "Get details from Google Maps. Might take a while to load details.",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $('#loader0712').modal({ backdrop: 'static', keyboard: false });

                            if ($scope.edt.sims_transport_stop_landmark != undefined) {
                                $scope.edt['sims_transport_stop_long'] = ''
                                $scope.edt['sims_transport_stop_lat'] = ''

                                setTimeout(function () {
                                    $("#cmb_uom_code").select2();
                                }, 100);
                                $.ajax({
                                    //url: "http://maps.googleapis.com/maps/api/geocode/json?address=" + $scope.edt.sims_transport_stop_landmark + "&sensor=false",
                                    url: "https://maps.googleapis.com/maps/api/geocode/json?address=" + $scope.edt.sims_transport_stop_landmark + "&key=AIzaSyDsDs9Tlc6arP1q6PuwVcU_fu6eZoMbKFg",
                                    type: "POST",
                                    success: function (res712) {
                                        console.log("google geolocation data", res712);
                                        console.log("google geolocation data geometry", res712.results[0].geometry);
                                        console.log("google geolocation data lat,long", res712.results[0].geometry.location);

                                        $scope.edt['sims_transport_stop_long'] = res712.results[0].geometry.location.lng,
                                        $scope.edt['sims_transport_stop_lat'] = res712.results[0].geometry.location.lat

                                        console.log("sims_transport_stop_long ", $scope.edt['sims_transport_stop_long'] + ' ' + res712.results[0].geometry.location.lng);
                                        console.log("sims_transport_stop_lat ", $scope.edt['sims_transport_stop_lat'] + ' ' + res712.results[0].geometry.location.lat);

                                        res712 = '';
                                        $('#loader0712').modal('hide');
                                    }
                                });
                            }
                            else
                                $('#loader0712').modal('hide');
                        }
                    });
                }

            }

            $scope.up = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                opr = 'U';
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.readonly = true;
                $scope.edt = "";
                //str.sims_transport_stop_name,
                $scope.edt = {

                    sims_academic_year: str.sims_academic_year,
                    sims_academic_year_description: str.sims_academic_year_description,
                    sims_transport_stop_code: str.sims_transport_stop_code,
                    sims_transport_stop_name: str.sims_transport_stop_name.replace(str.sims_transport_stop_code + ' ' + '-', ''),
                    sims_transport_stop_landmark: str.sims_transport_stop_landmark,

                    sims_transport_stop_lat: str.sims_transport_stop_lat,
                    sims_transport_stop_long: str.sims_transport_stop_long,

                    sims_transport_stop_status: str.sims_transport_stop_status

                }
              }
            }

            $scope.cancel = function () {

                $scope.edt = "";

                $scope.table1 = true;
                $scope.operation = false;

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

                $scope.edt = {
                    sims_transport_stop_code: "",
                    sims_transport_stop_name: "",
                    sims_transport_stop_landmark: "",
                    sims_transport_stop_lat: "",
                    sims_transport_stop_long: ""
                }
            }

            $scope.Save = function (myForm) {

                if (myForm) {
                    data1 = [];
                    var data = $scope.edt;
                    data.opr = 'I';

                    $scope.exist = false;
                    for (var i = 0; i < $scope.TransportStop.length; i++) {
                        if ($scope.TransportStop[i].sims_transport_stop_code == data.sims_transport_stop_code && $scope.TransportStop[i].sims_academic_year == data.sims_academic_year) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ text: "Record Already Exists", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 })
                    }
                    else {
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/TransportStop/CUDTransportStop", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                            $http.get(ENV.apiUrl + "api/TransportStop/getTransportStop").then(function (Transport_Stop) {
                                $scope.TransportStop = Transport_Stop.data;
                                $scope.totalItems = $scope.TransportStop.length;
                                $scope.todos = $scope.TransportStop;
                                $scope.makeTodos();
                            });
                            //$scope.Grid_Data();
                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                }
            }

            $scope.Update = function (myForm) {

                if (myForm) {
                    data1 = [];
                    var data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/TransportStop/CUDTransportStop", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $scope.operation = false;
                        $http.get(ENV.apiUrl + "api/TransportStop/getTransportStop").then(function (Transport_Stop) {
                            $scope.TransportStop = Transport_Stop.data;
                            $scope.totalItems = $scope.TransportStop.length;
                            $scope.todos = $scope.TransportStop;
                            $scope.makeTodos();

                        });
                        //$scope.Grid_Data();
                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_transport_stop_code + i);
                    if (main.checked == true) {
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }

                    else {
                        v.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.deleterecord = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                stopcode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_transport_stop_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deleteroutecode = ({
                            'sims_transport_stop_code': $scope.filteredTodos[i].sims_transport_stop_code,
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            opr: 'D'
                        });
                        stopcode.push(deleteroutecode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/TransportStop/CUDTransportStop", stopcode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/TransportStop/getTransportStop").then(function (Transport_Stop) {
                                                $scope.TransportStop = Transport_Stop.data;
                                                $scope.totalItems = $scope.TransportStop.length;
                                                $scope.todos = $scope.TransportStop;
                                                $scope.makeTodos();
                                            });
                                            //$scope.Grid_Data();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/TransportStop/getTransportStop").then(function (Transport_Stop) {
                                                $scope.TransportStop = Transport_Stop.data;
                                                $scope.totalItems = $scope.TransportStop.length;
                                                $scope.todos = $scope.TransportStop;
                                                $scope.makeTodos();
                                            });
                                            //$scope.Grid_Data();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_transport_stop_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }

                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
               }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.TransportStop, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.TransportStop;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_academic_year_description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_transport_stop_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_transport_stop_landmark.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_transport_stop_lat.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_transport_stop_long.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_transport_stop_code == toSearch) ? true : false;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

           
        }])
})();





