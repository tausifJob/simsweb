﻿(function () {
    'use strict';
    var deletetransfeecode = [];
    var routename;
    var main;
    var data1 = [];
    var data = [];
    var simsController = angular.module('sims.module.Fleet');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TransportConfigureCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.pager = true;
            
            $scope.Tfee = false;
            $scope.Route = false;
            $scope.Stop = false;
            $scope.TRST = false;

            $scope.temp = {};
            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.Createbtn = false;

            // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.countData = [
             { val: 10, data: 10 },
             { val: 20, data: 20 },
             { val: 'All', data: 'All' },
            ]

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.size = function (str) {
                if (str == 10 || str == 20 || str == 'All') {
                    if (str == "All") {
                        $scope.currentPage = '1';
                        $scope.filteredTodos = $scope.Route_Data;
                        $scope.pager = false;
                    }
                    else {
                        $scope.pager = true;
                        $scope.pagesize = str;
                        $scope.currentPage = 1;
                        $scope.numPerPage = str;
                        $scope.makeTodos();
                    }

                }
                $scope.numPerPage = str;
            }
            $scope.change = function (str) {
                debugger;
                console.log(str);
            }
            $scope.Show = function () {

                
                $http.get(ENV.apiUrl + "api/TransportRoute/getAdvanceTransportRoute?ac_year=" + $scope.ActiveYear[0].sims_academic_year + "&module_code=" + $scope.temp.sims_module_code + "&appl_code=" + $scope.temp.sims_application_code).then(function (get_Route_Data) {

                    $scope.Route_Data = get_Route_Data.data;
                    if ($scope.Route_Data.length > 0) {
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                        }
                        $scope.totalItems = $scope.Route_Data.length;
                        $scope.todos = $scope.Route_Data;
                        $scope.makeTodos();

                        if ($scope.temp.sims_application_code == 'Sim079') {
                            $scope.Tfee = true;
                            $scope.Route = false;
                            $scope.Stop = false;
                            $scope.TRST = false;
                            $scope.table1 = true;
                        }
                        else if ($scope.temp.sims_application_code == 'Sim083') {
                            $scope.Route = true;
                            $scope.Tfee = false;
                            $scope.Stop = false;
                            $scope.TRST = false;
                            $scope.table1 = true;
                        }
                        else if ($scope.temp.sims_application_code == 'Sim084') {
                            $scope.Stop = true;
                            $scope.Route = false;
                            $scope.Tfee = false;
                            $scope.TRST = false;
                            $scope.table1 = true;
                        }
                        else if ($scope.temp.sims_application_code == 'Sim085') {
                            $scope.TRST = true;
                            $scope.Route = false;
                            $scope.Tfee = false;
                            $scope.Stop = false;
                            $scope.table1 = true;
                        }
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                        $scope.filteredTodos = [];
                    }

                });
            }

            $http.get(ENV.apiUrl + "api/TransportRoute/getCurrentYear").then(function (get_AllAcademicYear) {
                debugger;
                $scope.ActiveYear = get_AllAcademicYear.data;
            });

            $http.get(ENV.apiUrl + "api/TransportRoute/getAdvanceYear").then(function (get_AllAcademicYear) {
                debugger;
                $scope.ActiveYear1 = get_AllAcademicYear.data;
            });


            $http.get(ENV.apiUrl + "api/TransportRoute/getModuleName").then(function (getModulecode) {
                $scope.ModuleCode = getModulecode.data;
                $scope.temp = {
                    sims_academic_year: $scope.temp.sims_academic_year,
                    sims_module_code: $scope.ModuleCode.sims_module_code
                }                
                $scope.getapplcode();
            })

            $scope.getapplcode = function () {
                $http.get(ENV.apiUrl + "api/TransportRoute/getApplicationName?module_code=" + $scope.temp.sims_module_code).then(function (getApplicationNames) {
                    $scope.ApplicationNames = getApplicationNames.data;
                })
            }

            $scope.getbutton = function () {
                $scope.Createbtn = true;
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.Create = function () {

                if (true) {
                    data1 = [];
                    var data =
                        {
                            opr: 'I',
                            sims_academic_year: $scope.ActiveYear1[0].sims_academic_year,
                            sims_module_code: $scope.temp.sims_module_code,
                            sims_application_code: $scope.temp.sims_application_code
                        }

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/TransportRoute/CUDTransportConfigure", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                            swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 380, });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $scope.getgrid();

                    });

                    $scope.Show();
                }
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Route_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Route_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                return (item.sims_academic_year_description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_transport_route_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_transport_route_short_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_transport_route_direction_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_caretaker_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_transport_route_code == toSearch) ? true : false;


            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            //$scope.CheckAllChecked = function () {

            //    main = document.getElementById('mainchk');
            //    for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //        var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_code + i);
            //        if (main.checked == true) {
            //            v.checked = true;
            //            $('tr').addClass("row_selected");
            //        }

            //        else {
            //            v.checked = false;
            //            $('tr').removeClass("row_selected");
            //        }
            //    }

            //}

            //$scope.checkonebyonedelete = function () {

            //    $("input[type='checkbox']").change(function (e) {
            //        if ($(this).is(":checked")) {
            //            $(this).closest('tr').addClass("row_selected");
            //            $scope.color = '#edefef';
            //        } else {
            //            $(this).closest('tr').removeClass("row_selected");
            //            $scope.color = '#edefef';
            //        }
            //    });

            //    main = document.getElementById('mainchk');
            //    if (main.checked == true) {
            //        main.checked = false;
            //        $("input[type='checkbox']").change(function (e) {
            //            if ($(this).is(":checked")) {
            //                $(this).closest('tr').addClass("row_selected");
            //            }
            //            else {
            //                $(this).closest('tr').removeClass("row_selected");
            //            }
            //        });
            //    }
            //}

            //$scope.deleterecord = function () {
            //    routecode = [];
            //    $scope.flag = false;
            //    for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //        var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_code + i);
            //        if (v.checked == true) {
            //            $scope.flag = true;
            //            var deleteroutecode = ({
            //                'sims_transport_route_code': $scope.filteredTodos[i].sims_transport_route_code,
            //                'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
            //                'sims_transport_route_direction': $scope.filteredTodos[i].sims_transport_route_direction,
            //                opr: 'D'
            //            });
            //            routecode.push(deleteroutecode);
            //        }
            //    }

            //    if ($scope.flag) {
            //        swal({
            //            title: '',
            //            text: "Are you sure you want to Delete?",
            //            showCloseButton: true,
            //            showCancelButton: true,
            //            confirmButtonText: 'Yes',
            //            width: 380,
            //            cancelButtonText: 'No',

            //        }).then(function (isConfirm) {
            //            if (isConfirm) {
            //                $http.post(ENV.apiUrl + "api/TransportRoute/CUDTransportRoute", routecode).then(function (msg) {
            //                    $scope.msg1 = msg.data;
            //                    if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
            //                        swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 380, }).then(function (isConfirm) {
            //                            if (isConfirm) {
            //                                $scope.getgrid();
            //                                main = document.getElementById('mainchk');
            //                                if (main.checked == true) {
            //                                    main.checked = false;
            //                                    {
            //                                        $scope.row1 = '';
            //                                    }
            //                                }
            //                            }
            //                        });
            //                    }
            //                    else {
            //                        swal("Error-" + $scope.msg1)
            //                    }
            //                });
            //            }
            //            else {
            //                for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //                    var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_code + i);
            //                    if (v.checked == true) {
            //                        v.checked = false;
            //                        $scope.row1 = '';
            //                        main.checked = false;
            //                        $('tr').removeClass("row_selected");
            //                    }
            //                }
            //            }
            //        });
            //    }
            //    else {
            //        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
            //    }

            //    $scope.currentPage = true;

            //}


        }])
})();
