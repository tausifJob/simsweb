﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Fleet');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('TransportFeePaymentDetailsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {


            $scope.temp = {};
            $scope.detailsField = true;
            $scope.hideprintpdf = false;
            $scope.decimal_dsc = '';

            $scope.pagesize = '30';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.show_period_by_month = false;
            $scope.how_period_by_term = false;

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['chk_all'] = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 30, $scope.maxSize = 100;

            $scope.makeTodos = function () {
                debugger;
                if ($scope.pagesize == 'All') {
                    $scope.currentPage = 1;
                    $scope.showPager = false;
                    $scope.filteredTodos = $scope.report_data_new;
                }
                else {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                }
            };


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res1) {
                $scope.cur_data = res1.data;
                $scope.temp.sims_cur_code = $scope.cur_data[0].sims_cur_code;
                $scope.getAcademic_year($scope.cur_data[0].sims_cur_code);
            });

            $scope.getAcademic_year = function (cur_code1) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code1).then(function (res1) {
                    $scope.acad_data = res1.data;
                    $scope.temp.sims_academic_year = $scope.acad_data[0].sims_academic_year;

                    $http.get(ENV.apiUrl + "api/FinanceReportRoute/GetFeeFrequency?opr=" + 'A').then(function (res1) {
                        $scope.frequency_data = res1.data;
                        $scope.temp.view = $scope.frequency_data[0].comn_appl_parameter;
                        $scope.getPeriod($scope.temp.view, $scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                    });

                    $http.get(ENV.apiUrl + "api/FinanceReportRoute/GetFeeFrequency?opr=" + 'C').then(function (res1) {
                        debugger
                        $scope.view_report_by = res1.data;
                        $scope.temp.report_type = $scope.view_report_by[0].comn_appl_parameter;
                        $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year, $scope.temp.report_type);
                    });

                    $http.get(ENV.apiUrl + "api/FinanceReportRoute/GetFeeFrequency?opr=" + 'D').then(function (res1) {
                        debugger
                        $scope.status_data = res1.data;
                        $scope.temp.sims_status = $scope.status_data[0].comn_appl_parameter;
                    });

                    
                });
            }

            $scope.getGrade = function (curCode, accYear,report_type) {
                debugger;
                $http.get(ENV.apiUrl + "api/FinanceReportRoute/GetGrade?cur_code=" + curCode + "&acad_year=" + accYear + "&report_type=" + report_type).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $scope.getSection = function (curCode, gradeCode, accYear,report_type) {
                $http.get(ENV.apiUrl + "api/FinanceReportRoute/GetSection?cur_code=" + curCode + "&acad_year=" + accYear + "&grade=" + gradeCode + "&report_type=" + report_type).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }
            $(function () {
                $('#cmb_section_code').multipleSelect({
                    width: '100%'
                });
            });

         
            $scope.getPeriod = function (view,cur,acad) {
                debugger
                $http.get(ENV.apiUrl + "api/FinanceReportRoute/GetPeriod?view=" + view + "&cur_code="+cur+"&acad_year="+acad).then(function (res1) {
                    $scope.period_data = res1.data;
                    $scope.temp.sims_period_code = $scope.period_data[0].comn_appl_parameter;
                });
            }


            //$scope.show_para = function (str) {
            //    debugger;
            //    if (str == 'M') 
            //        {
            //        $scope.show_period_by_month = true;
            //        $scope.show_period_by_term = false;
            //    }
            //    else
            //        {
            //        $scope.show_period_by_term = true;
            //        $scope.show_period_by_month = false;
            //    }
            //}

            $scope.getstudentlist = function () {
                debugger;
                $scope.report_data_new = [];
                $scope.detailsField = true;
                var sims_grade_code = '';
                var sims_section_code = '';
                var obj = {}

                for (var i = 0; i < $scope.temp.sims_grade_code.length; i++)
                {
                    sims_grade_code += $scope.temp.sims_grade_code[i] + ','
                }

                for (var i = 0; i < $scope.temp.sims_section_code.length; i++) {
                    sims_section_code += $scope.temp.sims_section_code[i] + ','
                }

                obj={sims_cur_code:$scope.temp.sims_cur_code,
                    sims_academic_year: $scope.temp.sims_academic_year,
                    sims_grade_code: sims_grade_code,
                    sims_section_code: sims_section_code,
                    view: $scope.temp.view,
                    report_type: $scope.temp.report_type,
                    period_no: $scope.temp.sims_period_code,
                    transport_status: $scope.temp.sims_status
                }

                //(string cur_code, string acad_year, string grade, string section, string age_cal_date, string dob_less, string dob_greater)
                $http.post(ENV.apiUrl + "api/FinanceReportRoute/postgettransportfeedetails", obj).then(function (res1) {
                        if (res1.data.length > 0) {
                            $scope.report_data_new = res1.data;
                            $scope.totalItems = $scope.report_data_new.length;
                            $scope.todos = $scope.report_data_new;
                            $scope.makeTodos();
                        }
                        else {
                            $scope.filteredTodos = [];
                            swal({ title: "Alert", text: " Sorry !!!!Data is not Available", showCloseButton: true, width: 300, height: 200 });
                            $scope.report_data_new = [];
                        }

                    });
            }

            $http.get(ENV.apiUrl + "api/StudentFee/getDecimalPlaces").then(function (getDecimal) {
                debugger;
                $scope.decimal_degit = getDecimal.data;
                $scope.decimal_dsc = $scope.decimal_degit[0].comp_curcy_dec;
            });

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $scope.colsvis = false;


            $scope.exportData = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('pdf_print').innerHTML],
                         {
                             type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                         });
                        $scope.detailsField = false;
                        saveAs(blob, "SubstituteLectureDetailsReport.xls");
                        $scope.colsvis = false;
                        $scope.getsubdetail();

                    }

                });
                $scope.colsvis = true;

            };


            $scope.print = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var docHead = document.head.outerHTML;
                        var printContents = document.getElementById('pdf_print').outerHTML;
                        var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                        var newWin = window.open("", "_blank", winAttr);
                        var writeDoc = newWin.document;
                        writeDoc.open();
                        writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                        writeDoc.close();
                        newWin.focus();
                        $scope.detailsField = false;
                        $scope.colsvis = false;
                        $scope.getsubdetail();

                    }
                });
            };

            $scope.reset_data = function () {
                debugger;
                // $scope.temp.sims_grade_code = '';


                try {
                    $('#bell_box').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#teacher_box').multipleSelect('uncheckAll');
                } catch (e) {

                }
                $scope.temp.sims_bell_code = '';
                $scope.report_data_new = [];


            }

            $scope.ischeck = function () {
                debugger
                $scope.checklick = function () {
                    var v = document.getElementById('expire');
                    if (v.checked == true) {
                        v.checked == false;
                    }
                }
            }

        }])

})();

