﻿(function () {
    'use strict';
    var studentcode = [];
    var data1 = [];
    var main;
    var simsController = angular.module('sims.module.Fleet');
    simsController.controller('ApproveStudentTransportNewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '5';
            $scope.subbtn = false;
            $scope.cancelbtn = false;
            $scope.NodataStudent = false;
            $scope.busdisabled = true;
            $scope.routedisabled = true;
            var username = $rootScope.globals.currentUser.username;

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_student_code);
                    v.checked = false;
                    main.checked = false;
                    //  $scope.row1 = '';
                    $scope.filteredTodos[i]['row1'] = '';

                }
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    if ($scope.filteredTodos[i].sims_transport_route_reject_reason != '') {
                        $scope.filteredTodos[i]['read'] = true;
                    }
                    else {
                        $scope.filteredTodos[i]['read'] = false;

                    }
                    // var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_student_code);
                    // v.checked = true;
                    //$scope.row1 = 'row_selected';
                }
            };

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.edt = {
                    sims_cur_code: $scope.curriculum[0].sims_cur_code
                };
                $scope.getAccYear($scope.curriculum[0].sims_cur_code);
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = {
                        sims_academic_year: $scope.Acc_year[0].sims_academic_year
                    };

                    var direct = [];

                    var dati = ({
                        sims_transport_route_direction_1: 'IN',
                        sims_transport_route_direction: '01',
                    });

                    var dato = ({
                        sims_transport_route_direction_1: 'OUT',
                        sims_transport_route_direction: '02',
                    });

                    var datb = ({
                        sims_transport_route_direction_1: 'BOTH',
                        sims_transport_route_direction: '03',
                    });

                    direct.push(dati);
                    direct.push(dato);
                    direct.push(datb);
                    $scope.DirectionName = direct;



                });
            }

            $scope.getstopbydirection = function (aca_year, str) {
                $http.get(ENV.apiUrl + "api/ApproveStudentTransportDetails/getStopByDirection?direction_code=" + str + "&aca_year=" + aca_year).then(function (get_PickupDrop) {
                    $scope.stopdata = get_PickupDrop.data;
                })
            }

            $scope.getroutebydirection = function (aca_year, str, stop_code) {
                $http.get(ENV.apiUrl + "api/ApproveStudentTransportDetails/getRouteByDirection?direction_code=" + str + "&aca_year=" + aca_year + "&stop_code=" + stop_code).then(function (get_route) {
                    $scope.get_route_data = get_route.data;
                })
            }

            $scope.getvehiclebydirection = function (aca_year, str, stop_code, route_code) {
                $http.get(ENV.apiUrl + "api/ApproveStudentTransportDetails/getVehicleByDirection?direction_code=" + str + "&aca_year=" + aca_year + "&stop_code=" + stop_code + "&route_code=" + route_code).then(function (get_VehicleCode) {
                    $scope.VehicleCode = get_VehicleCode.data;
                })
            }

            $scope.Reset = function () {
                $scope.ApproveDataStudent = false;
                $scope.subbtn = false;
                $scope.cancelbtn = false;
                $scope.edt = {
                    sims_cur_code: '',
                    sims_academic_year: '',
                    sims_grade_code: '',
                    sims_section_code: '',
                    sims_transport_vehicle_code: '',
                    sims_transport_route_reject_reason: ''

                }
            }

            $scope.Cancel = function () {
                $scope.edt = {
                    sims_cur_code: '',
                    sims_grade_code: '',
                    sims_section_code: '',
                    sims_transport_vehicle_code: '',
                    sims_transport_route_reject_reason: ''
                }
                $scope.temp.sims_academic_year = '',
                $scope.ApproveDataStudent = false;
                $scope.NodataStudent = false;
                $scope.cancelbtn = false;
                $scope.subbtn = false;
            }

            $scope.Show_Data = function () {

                $scope.NodataStudent = false;
                $scope.subbtn = true;
                $scope.cancelbtn = true;
                $http.get(ENV.apiUrl + "api/ApproveStudentTransportDetails/getApproveStudentTransportNew?cur_code=" + $scope.edt.sims_cur_code + "&acayear=" + $scope.temp.sims_academic_year + "&direction_code=" + $scope.edt.sims_transport_route_direction + "&stop_code=" + $scope.edt.sims_transport_stop_code).then(function (getApproveStudentTransport_Data) {
                    $scope.ApproveStudentTransport = getApproveStudentTransport_Data.data;
                    $scope.totalItems = $scope.ApproveStudentTransport.length;
                    $scope.todos = $scope.ApproveStudentTransport;
                    $scope.makeTodos();

                    if ($scope.ApproveStudentTransport < 1) {
                        swal({ title: "Alert", text: "Sorry, There Is No Data Found..!", width: 350, height: 200 });
                        $scope.ApproveDataStudent = false;
                        $scope.subbtn = false;
                        $scope.cancelbtn = false;
                        $scope.busdisabled = true;
                        $scope.routedisabled = true;
                    }
                    else {

                        $scope.ApproveDataStudent = true;
                        $scope.NodataStudent = false;
                        $scope.busdisabled = false;
                        $scope.routedisabled = false;
                    }
                });

            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_student_code);
                        if ($scope.filteredTodos[i].read == false) {
                            v.checked = true;
                            $scope.filteredTodos[i]['row1'] = 'row_selected';
                        }
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_student_code);
                        v.checked = false;
                        main.checked = false;
                        //  $scope.row1 = '';
                        $scope.filteredTodos[i]['row1'] = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.insertrecord = function (str) {

                studentcode = [];
                data1 = [];
                var reason = null;

                if (str == undefined || str == '') {
                    reason = null;
                }
                else {
                    reason = str;
                }
                if ($scope.edt.sims_transport_route_code != undefined) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_student_code);

                        if (v.checked == true) {
                            var insertrecord = ({
                                sims_transport_route_student_code: $scope.filteredTodos[i].sims_transport_route_student_code,
                                //sims_transport_route_student_code: studentcode,
                                sims_transport_enroll_number: $scope.filteredTodos[i].sims_transport_enroll_number,
                                sims_cur_code: $scope.filteredTodos[i].sims_cur_code,
                                sims_transport_academic_year: $scope.filteredTodos[i].sims_transport_academic_year,
                                sims_grade_code: $scope.filteredTodos[i].sims_grade_code,
                                sims_section_code: $scope.filteredTodos[i].sims_section_code,
                                sims_transport_route_code: $scope.edt.sims_transport_route_code,
                                C_direction: $scope.edt.sims_transport_route_direction,
                                sims_transport_effective_from: $scope.filteredTodos[i].sims_transport_effective_from,
                                sims_transport_effective_upto: $scope.filteredTodos[i].sims_transport_effective_upto,
                                sims_transport_pickup_stop_code: $scope.edt.sims_transport_stop_code,
                                //sims_transport_drop_stop_code: $scope.filteredTodos[i].sims_transport_drop_stop_code,
                                //sims_transport_vehicle_code: $scope.edt.sims_transport_vehicle_code,
                                sims_transport_route_reject_reason: reason,
                                sims_transport_route_approve_by: username,
                                opr: 'I',

                            });
                            data1.push(insertrecord);
                        }

                    }

                    if (reason != null && reason != '') {
                        $http.post(ENV.apiUrl + "api/ApproveStudentTransportDetails/CUDApproveStudentTransportNew", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: $scope.msg1.strMessage, width: 350, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: $scope.msg1.strMessage, width: 350, height: 200 });
                            }
                            $scope.ApproveDataStudent = false;
                            $scope.cancelbtn = false;

                            $scope.edt = {
                                sims_cur_code: '',
                                sims_academic_year: '',
                                sims_grade_code: '',
                                sims_section_code: '',
                                sims_transport_vehicle_code: '',
                                sims_transport_route_reject_reason: ''

                            }
                            main.checked = false;
                            $http.get(ENV.apiUrl + "api/ApproveStudentTransportDetails/getApproveStudentTransportNew?cur_code=" + $scope.edt.sims_cur_code + "&acayear=" + $scope.temp.sims_academic_year + "&direction_code=" + $scope.edt.sims_transport_route_direction + "&stop_code=" + $scope.edt.sims_transport_stop_code).then(function (getApproveStudentTransport_Data) {
                                $scope.ApproveStudentTransport = getApproveStudentTransport_Data.data;
                                $scope.totalItems = $scope.ApproveStudentTransport.length;
                                $scope.todos = $scope.ApproveStudentTransport;
                                $scope.makeTodos();

                            });
                        });
                    }

                    else {
                        $http.post(ENV.apiUrl + "api/ApproveStudentTransportDetails/CUDApproveStudentTransportNew", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: $scope.msg1.strMessage, width: 350, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: $scope.msg1.strMessage, width: 350, height: 200 });
                            }
                            $scope.ApproveDataStudent = false;
                            $scope.cancelbtn = false;
                            $scope.edt = {
                                sims_cur_code: '',
                                sims_academic_year: '',
                                sims_grade_code: '',
                                sims_section_code: '',

                                sims_transport_vehicle_code: '',
                                sims_transport_route_reject_reason: ''

                            }
                            main.checked = false;
                            $http.get(ENV.apiUrl + "api/ApproveStudentTransportDetails/getApproveStudentTransport?cur_code=" + $scope.edt.sims_cur_code + "&acayear=" + $scope.temp.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&bus_code=" + $scope.edt.sims_transport_vehicle_code).then(function (getApproveStudentTransport_Data) {
                                $scope.ApproveStudentTransport = getApproveStudentTransport_Data.data;
                                $scope.totalItems = $scope.ApproveStudentTransport.length;
                                $scope.todos = $scope.ApproveStudentTransport;
                                $scope.makeTodos();

                            });
                        });
                    }
                    $scope.ApproveDataStudent = false;
                    $scope.cancelbtn = false;
                    $scope.ApproveRecord = false;
                    $scope.subbtn = false;
                    $scope.busdisabled = true;
                    $scope.routedisabled = true;
                }
                else {
                    swal({ title: "Alert", text: 'Please Select Route...', width: 350, height: 200 });
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.ApproveStudentTransport, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.ApproveStudentTransport;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                return (
                     item.sims_transport_route_reject_reason.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_transport_route_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_transport_enroll_number == toSearch) ? true : false;


            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

        }]);
})();



