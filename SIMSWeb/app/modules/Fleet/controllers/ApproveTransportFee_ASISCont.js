﻿/// <reference path="../views/StudentDocumentList.html" />
(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, modulecode = [];
    var simsController = angular.module('sims.module.Fleet');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ApproveTransportFeeASISCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.table = false;
            $scope.table1 = false;
            $scope.pagesize = "10";
            $scope.pageindex = 0;
            console.clear();
            $timeout(function () {
                $("#example").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.size = function (str) {

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }

                if (str == "All") {
                    $scope.currentPage = 1;
                    $scope.numPerPage = $scope.studlist.length;
                    $scope.makeTodos();

                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
            }

            $scope.makeTodos = function () {

                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.getcur = function () {
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                    $scope.curriculum = AllCurr.data;
                    $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                    $scope.getAccYear($scope.edt.sims_cur_code);
                    $scope.getARule($scope.edt.sims_cur_code);
                });

                $http.get(ENV.apiUrl + "api/TransportFees/getStatusnew").then(function (Allnewstatus) {
                    $scope.ststus = Allnewstatus.data;
                });
            }

            $scope.getcur();

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                    $scope.getGrade();
                });
            }

            $scope.getARule = function (curCode) {
                $http.get(ENV.apiUrl + "api/StudentReport/getAttendanceRule?curCode=" + curCode).then(function (attrule) {
                    $scope.arule = attrule.data;
                    $scope.edt = {
                        sims_attendance_code: $scope.arule[0].sims_attendance_code,
                        sims_cur_code: $scope.edt.sims_cur_code
                    };
                });
            }

            $scope.getGrade = function () {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }

            $(function () {
                $('#cmb_grade_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }

            $(function () {
                $('#cmb_section_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.Show_Data = function (str, str1) {
                if ((str != undefined && str1 != undefined) || $scope.edt.sims_cur_code != "" && $scope.temp.sims_academic_year != "") {
                    $http.get(ENV.apiUrl + "api/TransportFees/getStudentTransportDetailsNewFeeStructure_ASIS?cur_code=" + $scope.edt.sims_cur_code + "&acayear=" + $scope.temp.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&enroll=" + $scope.edt.sims_student_enroll_number + "&vehicle_code=" + '' + "&status=" + $scope.edt1.sims_get_status).then(function (res) {
                        $scope.studlist = [];
                        var enrollArray = [];
                        var enrollIncludeArray = [];
                        console.log($scope.studlist);
                        debugger
                        for (var i = 0; i < res.data.length; i++) {
                            var flag = 1;
                            for (var j = 0; j < enrollArray.length; j++) {

                                if (res.data[i].sims_enroll_number == enrollArray[j].sims_enroll_number) {
                                    //sdd
                                    if (enrollArray[j].sims_transport_route_direction_1 == res.data[i].sims_transport_route_direction_1) {
                                         flag = 0;                                         
                                    }
                                    else {                                        
                                        //res.data[i].sims_transport_route_name = res.data[i-1].sims_transport_route_name + '/' + res.data[i].sims_transport_route_name;
                                        //res.data[i].sims_transport_route_direction_1 =  res.data[i-1].sims_transport_route_direction_1 + '/' + res.data[i].sims_transport_route_direction_1;
                                        res.data[i].sims_transport_route_name = enrollArray[j].sims_transport_route_name + '/' + res.data[i].sims_transport_route_name;
                                        res.data[i].sims_transport_route_direction_1 = enrollArray[j].sims_transport_route_direction_1 + '/' + res.data[i].sims_transport_route_direction_1;
                                        res.data[i].sims_paid_total = res.data[i].both_amt;  //parseFloat(enrollArray[j].sims_paid_total) + parseFloat(res.data[i].sims_paid_total);
                                        enrollArray.splice(j, 1);
                                    }

                                }
                                else {
                                   
                                }
                            }
                            enrollArray.push(res.data[i]);

                            //if (flag == 0) {
                            //    enrollArray.splice(i, 1);
                            //}

                            //if (enrollArray.length == 0) {
                            //     //   res.data[i]['index'] = i;
                            //     // if (!enrollIncludeArray.includes(res.data[i].sims_enroll_number)) {
                            //     //   enrollIncludeArray.push(res.data[i].sims_enroll_number);
                            //        enrollArray.push(res.data[i])
                            //     //}                                
                            //}
                        }
                        $scope.studlist = enrollArray;

                        $scope.totalItems = $scope.studlist.length;
                        $scope.todos = $scope.studlist;
                        $scope.makeTodos();
                        if ($scope.studlist.length == 0) {
                            swal({ title: 'Alert', text: "Record Not Found", showCloseButton: true, width: 450, height: 200 });
                        }
                        else {
                            $scope.table = true;
                            $scope.table1 = true;
                        }
                    })
                }

                else {
                    swal({ title: 'Alert', text: "Please Select Curriculum & Academic Year Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {

                $scope.todos = $scope.searched($scope.studlist, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.studlist;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                return (item.student_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_attendance_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_section_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sagar == toSearch) ? true : false;
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyoneselect = function () {

                $("input[type='checkbox']").change(function (e) {

                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.Submit = function () {
                var data1 = [];
                var sub_opr1 = null;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                    if (v.checked == true) {
                        var updaterecord = ({
                            sims_transport_route_student_update_by: $rootScope.globals.currentUser.username,
                            sims_transport_academic_year: $scope.filteredTodos[i].sims_transport_academic_year,
                            sims_transport_enroll_number: $scope.filteredTodos[i].sims_enroll_number,
                            sims_transport_route_student_code: $scope.filteredTodos[i].sims_transport_route_student_code,
                            sims_appl_parameter: $scope.filteredTodos[i].sims_appl_parameter,
                            sims_fee_cur_code: $scope.filteredTodos[i].sims_fee_cur_code,
                            sims_fee_grade_code: $scope.filteredTodos[i].sims_fee_grade_code,
                            sims_fee_section_code: $scope.filteredTodos[i].sims_fee_section_code,
                            sims_fee_number: $scope.filteredTodos[i].sims_fee_number,

                            sims_transport_route_code: $scope.filteredTodos[i].sims_transport_route_code,
                            sims_transport_route_direction: $scope.filteredTodos[i].sims_transport_route_direction,
                            sims_transport_effective_from: $scope.filteredTodos[i].sims_transport_effective_from1,
                            sims_transport_effective_upto: $scope.filteredTodos[i].sims_transport_effective_upto1,

                            sims_transport_route_direction_old: $scope.filteredTodos[i].sims_transport_route_direction_old,
                            sims_transport_effective_from_old: $scope.filteredTodos[i].sims_transport_effective_from,
                            sims_transport_effective_upto_old: $scope.filteredTodos[i].sims_transport_effective_upto,
                            sims_transport_pickup_stop_code_old: $scope.filteredTodos[i].sims_transport_pickup_stop_code_old,
                            sims_transport_drop_stop_code_old: $scope.filteredTodos[i].sims_transport_drop_stop_code_old,
                            sims_transport_route_code_old: $scope.filteredTodos[i].sims_transport_route_code_old,
                            //v_Code: $scope.filteredTodos[i].v_Code_old,
                            v_Name_Code: $rootScope.globals.currentUser.username,//$scope.filteredTodos[i].temp.v_Code,

                            sims_transport_pickup_stop_code: $scope.filteredTodos[i].sims_transport_pickup_stop_code,
                            sims_transport_drop_stop_code: $scope.filteredTodos[i].sims_transport_drop_stop_code,

                            sims_transport_period1: $scope.filteredTodos[i].sims_transport_period1,
                            sims_transport_period2: $scope.filteredTodos[i].sims_transport_period2,
                            sims_transport_period3: $scope.filteredTodos[i].sims_transport_period3,
                            sims_transport_period4: $scope.filteredTodos[i].sims_transport_period4,
                            sims_transport_period5: $scope.filteredTodos[i].sims_transport_period5,
                            sims_transport_period6: $scope.filteredTodos[i].sims_transport_period6,
                            sims_transport_period7: $scope.filteredTodos[i].sims_transport_period7,
                            sims_transport_period8: $scope.filteredTodos[i].sims_transport_period8,
                            sims_transport_period9: $scope.filteredTodos[i].sims_transport_period9,
                            sims_transport_period10: $scope.filteredTodos[i].sims_transport_period10,
                            sims_transport_period11: $scope.filteredTodos[i].sims_transport_period11,
                            sims_transport_period12: $scope.filteredTodos[i].sims_transport_period12,
                            opr: 'G',
                            sims_fee_period1: $scope.filteredTodos[i].sims_fee_period1,
                            sims_fee_period2: $scope.filteredTodos[i].sims_fee_period2,
                            sims_paid_total: $scope.filteredTodos[i].sims_paid_total

                        });
                        data1.push(updaterecord);
                    }
                }
                if (data1.length > 0) {
                    $http.post(ENV.apiUrl + "api/TransportFees/UpdateTransportCUDNewFeeStructure_ASIS", data1).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Transport Fee Update Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Transport Not Update. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        $scope.Show_Data($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);

                        //$http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getStudentTransportDetailsNewFeeStructure?cur_code=" + $scope.edt.sims_cur_code + "&acayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&enroll=" + $scope.edt.sims_enroll_number + "&vehicle_code=" + $scope.edt.sims_transport_vehicle_code).then(function (getStudentTransportDetails_Data) {
                        //    $scope.StudentTransportDetails = getStudentTransportDetails_Data.data;
                        //    $scope.totalItems = $scope.StudentTransportDetails.length;
                        //    $scope.todos = $scope.StudentTransportDetails;
                        //    $scope.makeTodos();
                        //});


                        $('#MyModal').modal('hide');
                        $scope.searchtable = true;
                    });
                }
            }

            $scope.Reset = function () {

                $("#cmb_grade_code").select2("val", "");
                $("#cmb_section_code").select2("val", "");

                $scope.edt = {
                    sims_cur_code: '',
                    sims_student_enroll_number: '',
                }
                $scope.temp = {
                    sims_academic_year: '',
                }

                $scope.table = false;
                $scope.table1 = false;
                $scope.getcur();

            }

            $scope.Clear = function () {

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                    v.checked = false;
                    main.checked = false;
                    $scope.row1 = '';
                    $('tr').removeClass("row_selected");
                }
            }

            $rootScope.visible_stud = true;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = false;
            $rootScope.chkMulti = false;

            $scope.Search = function () {
                $scope.global_Search_click();

                $('#Global_Search_Modal').modal({ backdrop: "static" });
                //  $scope.display = true;
                //$scope.SelectedUserLst = [];
            }

            $scope.$on('global_cancel', function () {
                if ($scope.SelectedUserLst.length > 0) {
                    $scope.edt =
                        {
                            sims_attendance_code: $scope.arule[0].sims_attendance_code,
                            sims_cur_code: $scope.curriculum[0].sims_cur_code,
                            s_name: $scope.SelectedUserLst[0].name,
                            sims_student_enroll_number: $scope.SelectedUserLst[0].s_enroll_no
                        }
                }
                $scope.em_number = $scope.edt.em_number;
            });

            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

        }])
})();
