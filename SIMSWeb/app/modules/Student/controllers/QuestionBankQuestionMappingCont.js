﻿(function () {
    'use strict';
    var del = [];
    var main;
    var imagename = '';
    var formdata = new FormData();
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('QuestionBankQuestionMappingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            //$('#text-editor').wysihtml5();

            $('#text-editor1').wysihtml5();

            $scope.save_btn = true;
            $scope.questionbank_Questions_Details = [];
            $scope.questionbank_Questions_Details1 = [];
            $scope.upload_questionbank_question_code = '';
            $scope.upload_questionbank_question_answer_code = '';
            $scope.sims_questionbank_question_image_path = '';
            $scope.show_paragraph_controls = false;
            $scope.show_para_add_btn = false;
            $scope.show_para_save_btn = false;
            $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/Survey/';
            $scope.temp = {};

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }


            $http.get(ENV.apiUrl + "api/surveyquestionmapping/Get_questionbank_type").then(function (Get_questionbank_type) {
                $scope.questionbank_type = Get_questionbank_type.data;
            });

            $http.get(ENV.apiUrl + "api/surveyquestionmapping/Get_questionbank_skill").then(function (Get_questionbank_type) {
                $scope.questionbank_skill = Get_questionbank_type.data;
            });

            $http.get(ENV.apiUrl + "api/surveyquestionmapping/Get_questionbank_section").then(function (Get_questionbank_type) {
                $scope.questionbank_section = Get_questionbank_type.data;
            });

            $http.get(ENV.apiUrl + "api/surveyquestionmapping/Get_questionbank_subsection").then(function (Get_questionbank_type) {
                $scope.questionbank_subsection = Get_questionbank_type.data;
            });

            $http.get(ENV.apiUrl + "api/surveyquestionmapping/getquestionbankQuestionType").then(function (question) {
                $scope.ques_data = question.data;
            });

            $scope.GetquestionbankDetails = function () {
                $http.get(ENV.apiUrl + "api/surveyquestionmapping/Get_questionbank_details?questionbank_code=" + $scope.temp.sims_questionbank_code).then(function (Get_questionbank_details) {
                    $scope.questionbank_details = Get_questionbank_details.data;

                    if ($scope.questionbank_details.length > 0) {
                        $scope.questionbank_detail_table = true;

                        $scope.GetquestionbankQuestionsdetails($scope.questionbank_details[0]);
                    }
                    else {
                        $scope.questionbank_detail_table = false;
                        $scope.questionbank_questions_table = false;
                        swal({ text: "Record Not Found", width: 300, height: 250, showCloseButton: true });

                    }
                });
            }

            $scope.GetquestionbankQuestionsdetails = function (info) {
                debugger;
                info.sims_check = true;
                $http.get(ENV.apiUrl + "api/surveyquestionmapping/Get_questionbank_Questions_Details?sims_questionbank_code=" + info.sims_questionbank_code).then(function (Get_questionbank_Questions_Details) {
                    $scope.questionbank_Questions_Details = Get_questionbank_Questions_Details.data;
                    $scope.questionbank_Questions_Details1 = angular.copy(Get_questionbank_Questions_Details.data);
                    if ($scope.questionbank_Questions_Details.length > 0) {
                        $scope.questionbank_questions_table = true;
                    } else {
                        $scope.questionbank_questions_table = false;
                        swal({ text: "Record Not Found", width: 300, height: 250, showCloseButton: true });

                    }
                    console.log("$scope.questionbank_Questions_Details", $scope.questionbank_Questions_Details);
                });
            }

            $scope.CheckAllChecked = function (str) {
                for (var i = 0; i < $scope.getquestionbankQuestionOntype.length; i++) {
                    if (str == true) {
                        $scope.getquestionbankQuestionOntype[i].ischecked = true;
                    }
                    else {
                        $scope.getquestionbankQuestionOntype[i].ischecked = false;
                    }
                }
            }

            $scope.SelectQuestionsFormshow = function () {
                debugger
                $scope.temp['sims_questionbank_question_type'] = '';
                $scope.temp['sims_questionbank_question_difficulty_level'] = '';

                $http.get(ENV.apiUrl + "api/surveyquestionmapping/getquestionbankQuestionType").then(function (question) {
                    $scope.ques_data = question.data;
                    $scope.getquestionbankQuestionOntype = [];
                    $('#MyModal').modal('show');
                });
            }

            $scope.getquestiosbyquestiontype = function () {

                $http.get(ENV.apiUrl + "api/surveyquestionmapping/getquestionbankQuestionOntype?sims_questionbank_question_type=" + $scope.temp.sims_questionbank_question_type + "&sims_questionbank_question_difficulty_level=" + $scope.temp.sims_questionbank_question_difficulty_level + "&sims_questionbank_code=" + $scope.temp.sims_questionbank_code + "&sims_questionbank_skill_code=" + $scope.temp.sims_questionbank_skill_code).then(function (getquestionbankQuestionOntype) {
                    $scope.getquestionbankQuestionOntype = getquestionbankQuestionOntype.data;
                    if ($scope.getquestionbankQuestionOntype.length > 0) {
                        $scope.questionbank_questions_for_select = true;
                    } else {
                        $scope.questionbank_questions_for_select = false;
                        swal({ text: "Record Not Found", width: 300, height: 250, showCloseButton: true });

                    }
                });
            }

            $scope.btn_Addselectquestions_update_Click = function () {

                if ($scope.getquestionbankQuestionOntype.length > 0) {
                    for (var k = 0; k < $scope.getquestionbankQuestionOntype.length; k++) {
                        for (var j = 0; j < $scope.questionbank_Questions_Details.length; j++) {
                            if ($scope.questionbank_Questions_Details[j].sims_questionbank_question_code == $scope.getquestionbankQuestionOntype[k].sims_questionbank_question_code) {
                                $scope.getquestionbankQuestionOntype[k].ischecked = false;
                            }
                        }
                    }
                }

                for (var i = 0; i < $scope.getquestionbankQuestionOntype.length; i++) {
                    if ($scope.getquestionbankQuestionOntype[i].ischecked == true) {
                        $scope.questionbank_Questions_Details.push($scope.getquestionbankQuestionOntype[i]);
                    }
                }

                if ($scope.questionbank_Questions_Details.length > 0) {
                    $scope.questionbank_questions_table = true;
                } else {
                    $scope.questionbank_questions_table = false;
                }
            }

            $scope.removequestions = function (info, object, index) {
                object.splice(index, 1);
            }

            $scope.AddnewQuestionstoQuestionnaire_Click = function () {

                $scope.dataobject = [];

                for (var i = 0; i < $scope.questionbank_details.length; i++) {
                    if ($scope.questionbank_details[i].sims_check == true) {
                        for (var j = 0; j < $scope.questionbank_Questions_Details.length; j++) {
                            var data = {};
                            data.sims_questionbank_code = $scope.questionbank_details[i].sims_questionbank_code;
                            data.sims_questionbank_question_code = $scope.questionbank_Questions_Details[j].sims_questionbank_question_code;
                            data.sims_questionbank_question_display_order = $scope.questionbank_Questions_Details[j].sims_questionbank_question_display_order;
                            data.sims_questionbank_question_skip_flag = $scope.questionbank_Questions_Details[j].sims_questionbank_question_skip_flag;
                            data.sims_questionbank_question_status = $scope.questionbank_Questions_Details[j].sims_questionbank_question_status;
                            $scope.dataobject.push(data);
                        }
                    }
                }

                if ($scope.dataobject.length > 0) {
                    $http.post(ENV.apiUrl + "api/surveyquestionmapping/CUDAddnewQuestionstoQuestionnaireQuestoinBank", $scope.dataobject).then(function (res) {
                        $scope.msg = res.data;

                        if ($scope.msg == true) {
                            swal({ text: "Questions Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 250, showCloseButton: true });
                            $scope.GetquestionbankQuestionsdetails($scope.dataobject[0]);
                        }

                    });
                } else {
                    swal({ text: "Select Atleast One questionbank For Insert Questions", width: 300, height: 250, showCloseButton: true });
                }
            }

            $scope.UpdatenewQuestionstoQuestionnaire_Click = function () {
                $http.post(ENV.apiUrl + "api/surveyquestionmapping/CUDAddnewQuestionstoQuestionnaireQuestoinBank", $scope.questionbank_Questions_Details).then(function (res) {
                    $scope.msg = res.data;
                    if ($scope.msg == true) {
                        swal({ text: "Questions Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 250, showCloseButton: true });
                        $scope.GetquestionbankQuestionsdetails($scope.questionbank_Questions_Details[0]);
                    }
                });
            }

            $scope.AddnewQuestionsFormshow = function () {

                $http.get(ENV.apiUrl + "api/surveyquestionmapping/getParagraph").then(function (Get_questionbank_type) {
                    $scope.paragraph_combo_box = Get_questionbank_type.data;
                    $scope.temp['sims_questionbank_question_desc_en'] = '';
                    $scope.temp['sims_questionbank_question_type'] = '';
                    $scope.temp['sims_questionbank_question_difficulty_level'] = '';
                    $scope.temp['sims_questionbank_question_weightage'] = '';
                    $scope.Answerdata = [];
                    $('#MyModal1').modal('show');
                });
            }

            $scope.UpdateQuestionBankFun = function (str) {
                debugger
                $scope.temp = {};
                $scope.Answerdata = [];
                $('.modal-backdrop').remove();
                $('#UpdateQuestionBank').modal('show');
                $http.get(ENV.apiUrl + "api/surveyquestionmapping/getParagraph").then(function (para_data) {
                    $scope.paragraph_combo_box = para_data.data;
                    $scope.temp = {
                        'sims_questionbank_question_type': str.sims_questionbank_question_type,
                        'sims_questionbank_question_difficulty_level': str.sims_questionbank_question_difficulty_level,
                        'sims_questionbank_question_desc_en':str.sims_questionbank_question_desc_en,
                        'sims_questionbank_question_weightage': str.sims_questionbank_question_weightage,
                        //'sims_questionbank_question_desc_en': str.sims_questionbank_question_desc_en,
                        'sims_questionbank_question_answer_desc_en': str.sims_questionbank_question_answer_desc_en,
                        'sims_questionbank_question_correct_answer_flag': str.sims_questionbank_question_correct_answer_flag,
                        'sims_questionbank_skill_code': str.sims_questionbank_skill_code,
                        'sims_questionbank_section_code': str.sims_questionbank_section_code,
                        'sims_questionbank_subsection_code': str.sims_questionbank_subsection_code,
                        'sims_questionbank_no_of_question_available': str.sims_questionbank_no_of_question_available,
                        'sims_questionbank_code':str.sims_questionbank_code,
                        'sims_questionbank_question_code': str.sims_questionbank_question_code,
                        'sims_questionbank_question_answer_explanation': str.sims_questionbank_question_answer_explanation,
                        'sims_questionbank_question_header_code': str.sims_questionbank_question_header_code,
                        'sims_questionbank_question_header_code_description': str.sims_questionbank_question_header_code_description,
                        'sims_questionbank_question_image_path': str.sims_questionbank_question_image_path,
                        'sims_questionbank_question_answer_image': str.sims_questionbank_question_answer_image
                    }

                    $('#text-editor1').data("wysihtml5").editor.clear();
                    $('#text-editor1').data("wysihtml5").editor.setValue(str.sims_questionbank_question_desc_en);

                });

                $scope.Answerdata = angular.copy(str.que_ans);

                
            }

            $scope.updataePara = function (question_code, setcode, sims_questionbank_question_desc_en) {

                $scope.data = {
                    'sims_questionbank_question_code': question_code,
                    'sims_questionbank_question_desc_en':sims_questionbank_question_desc_en
                }
                var lst_t = [];
                lst_t.push($scope.data);

                $http.post(ENV.apiUrl + "api/surveyquestionmapping/CUDQuestionparagraph", lst_t).then(function (msg) {
                    $scope.msg1 = msg.data;
                    console.log("msg", msg);
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Paragraph Updated Succesfully ", width: 300, height: 200 });
                    } else {
                        swal({ title: "Alert", text: "Unable to Update Paragraph", width: 300, height: 200 });
                    }
                });
            }

            $scope.queschange = function (str) {

                $scope.Answerdata = [];
                $scope.ansname = true;
                if (str.sims_questionbank_question_type == '1' || str.sims_questionbank_question_type == '5') {
                    $scope.ans = true;
                    $scope.show_paragraph_controls = false;
                    $scope.show_para_save_btn = false;
                    $scope.show_para_add_btn = false;
                }
                else {
                    $scope.ans = false;
                    $scope.anstable = false;
                    $scope.show_paragraph_controls = false;
                    $scope.show_para_save_btn = false;
                    $scope.show_para_add_btn = false;
                }
                if (str.sims_questionbank_question_type == '4') {
                    $scope.ans1 = true;
                    $scope.anstable1 = true;
                    $scope.show_paragraph_controls = false;
                    $scope.show_para_save_btn = false;
                    $scope.show_para_add_btn = false;
                }
                else {
                    $scope.ans1 = false;
                    $scope.anstable1 = false;
                    $scope.show_paragraph_controls = false;
                    $scope.show_para_save_btn = false;
                    $scope.show_para_add_btn = false;
                }

                if (str.sims_questionbank_question_type == '6') {
                    $scope.show_para_add_btn = true;
                    $scope.AddPara();
                }
            }

            $scope.ratechange = function (str) {

                $scope.ansname = true;
                $http.get(ENV.apiUrl + "api/surveyquestionmapping/getquestionbankQuestionRateDetails?group_code=" + str.sims_questionbank_rating_group_code).then(function (ratingdetails) {
                    $scope.Rating = ratingdetails.data;
                });
            }

            $http.get(ENV.apiUrl + "api/surveyquestionmapping/getquestionbankQuestionRateGroup").then(function (RateGroup) {
                $scope.rate_data = RateGroup.data;
            });

            $http.get(ENV.apiUrl + "api/surveyquestionmapping/Get_question_difficaltyQuestionBank").then(function (Get_question_difficalty) {
                $scope.question_difficalty = Get_question_difficalty.data;
            });

            $scope.Answerdata = [];

            $scope.AddPara = function () {
                debugger;
                if ($scope.temp.sims_questionbank_question_type == '6') {
                    $scope.show_paragraph_controls = true;
                    $scope.show_para_save_btn = true;
                    $scope.show_para_add_btn = false;
                } else {
                    $scope.show_paragraph_controls = false;
                    $scope.clicked_on_add_btn = true;
                    $scope.show_para_save_btn = false;
                    $scope.show_para_add_btn = true;
                }

                
            }

            $scope.SavePara = function () {
                var savedata = [];
                //  $scope.clicked_on_add_btn = true;

                var modulecode = ({
                    'sims_questionbank_question_type': $scope.temp.sims_questionbank_question_type,
                    'sims_questionbank_question_desc_en': $scope.temp.sims_questionbank_question_desc_en,
                });

                savedata.push(modulecode);
                $http.post(ENV.apiUrl + "api/surveyquestionmapping/CUDADDQuestionparagraph", savedata).then(function (msg) {
                    $scope.msg1 = msg.data;
                    console.log("msg", msg);
                    if ($scope.msg1 == true) {
                        $http.get(ENV.apiUrl + "api/surveyquestionmapping/getParagraph").then(function (Get_questionbank_type) {
                            $scope.paragraph_combo_box = Get_questionbank_type.data;
                            $('.modal-backdrop').remove();
                            swal({ title: "Alert", text: "Paragraph Saved Succesfully ", width: 300, height: 200 });
                            //MyModal1
                            $('#MyModal1').modal('show');
                            $scope.temp = {};
                        });
                    } else {
                        swal({ title: "Alert", text: "Unable to Save Paragraph", width: 300, height: 200 });
                    }
                });
            }

            $scope.AddAnswers = function () {

                if ($scope.temp.sims_questionbank_question_answer_desc_en == undefined || $scope.temp.sims_questionbank_question_answer_desc_en == '') {
                    return;
                }
                $scope.anstable = true;
                var v = {
                    'sims_questionbank_question_answer_desc_en': $scope.temp.sims_questionbank_question_answer_desc_en
                }

                $scope.Answerdata.push(v);
                v = '';
                $scope.temp.sims_questionbank_question_answer_desc_en = '';
            }

            $scope.objective_ans = function (event) {

                if (event.key == "Enter") {
                    $scope.AddAnswers();
                }
            }

            $scope.Save = function (Myform) {
                debugger;
                var savefin = [];
                var flag = false;
                if (Myform) {

                    if ($scope.temp.sims_questionbank_question_type == '1' || $scope.temp.sims_questionbank_question_type == '5') {
                        for (var i = 0; i < $scope.Answerdata.length; i++) {
                            var v = document.getElementById($scope.Answerdata[i].sims_questionbank_question_answer_desc_en + i);
                            if (v.checked == true) {
                                var modulecode = ({
                                    'sims_questionbank_question_type': $scope.temp.sims_questionbank_question_type,
                                    'sims_questionbank_question_difficulty_level': $scope.temp.sims_questionbank_question_difficulty_level,
                                    'sims_questionbank_question_weightage': $scope.temp.sims_questionbank_question_weightage,
                                    'sims_questionbank_question_desc_en': $scope.temp.sims_questionbank_question_desc_en,
                                    'sims_questionbank_question_answer_desc_en': $scope.Answerdata[i].sims_questionbank_question_answer_desc_en,
                                    'sims_questionbank_question_correct_answer_flag': true,
                                    'sims_questionbank_question_section': $scope.temp.sims_questionbank_section_code,
                                    'sims_questionbank_question_subsection': $scope.temp.sims_questionbank_subsection_code,
                                    'sims_questionbank_question_skill': $scope.temp.sims_questionbank_skill_code,
                                    'sims_questionbank_question_header_code': $scope.temp.sims_questionbank_question_header_code,
                                });
                            }
                            else {
                                var modulecode = ({
                                    'sims_questionbank_question_type': $scope.temp.sims_questionbank_question_type,
                                    'sims_questionbank_question_difficulty_level': $scope.temp.sims_questionbank_question_difficulty_level,
                                    'sims_questionbank_question_weightage': $scope.temp.sims_questionbank_question_weightage,
                                    'sims_questionbank_question_desc_en': $scope.temp.sims_questionbank_question_desc_en,
                                    'sims_questionbank_question_answer_desc_en': $scope.Answerdata[i].sims_questionbank_question_answer_desc_en,
                                    'sims_questionbank_question_correct_answer_flag': false,
                                    'sims_questionbank_question_section': $scope.temp.sims_questionbank_section_code,
                                    'sims_questionbank_question_subsection': $scope.temp.sims_questionbank_subsection_code,
                                    'sims_questionbank_question_skill': $scope.temp.sims_questionbank_skill_code,
                                    'sims_questionbank_question_header_code': $scope.temp.sims_questionbank_question_header_code,
                                });
                            }
                            flag = true;
                            savefin.push(modulecode);
                        }
                    }

                    //Subjective Question Type
                    if ($scope.temp.sims_questionbank_question_type == '2') {
                        var datacode = ({
                            'sims_questionbank_question_type': $scope.temp.sims_questionbank_question_type,
                            'sims_questionbank_question_desc_en': $scope.temp.sims_questionbank_question_desc_en,
                            'sims_questionbank_question_difficulty_level': $scope.temp.sims_questionbank_question_difficulty_level,
                            'sims_questionbank_question_weightage': $scope.temp.sims_questionbank_question_weightage,
                            'sims_questionbank_question_answer_desc_en': $scope.temp.sims_questionbank_question_answer_desc_en,
                            'sims_questionbank_question_section': $scope.temp.sims_questionbank_section_code,
                            'sims_questionbank_question_subsection': $scope.temp.sims_questionbank_subsection_code,
                            'sims_questionbank_question_skill': $scope.temp.sims_questionbank_skill_code,
                            'sims_questionbank_question_header_code': $scope.temp.sims_questionbank_question_header_code,
                        });
                        flag = true;
                        savefin.push(datacode);
                    }

                    //True/False Question Type
                    if ($scope.temp.sims_questionbank_question_type == '3') {
                        var a = ({
                            'sims_questionbank_question_type': $scope.temp.sims_questionbank_question_type,
                            'sims_questionbank_question_desc_en': $scope.temp.sims_questionbank_question_desc_en,
                            'sims_questionbank_question_difficulty_level': $scope.temp.sims_questionbank_question_difficulty_level,
                            'sims_questionbank_question_weightage': $scope.temp.sims_questionbank_question_weightage,
                            'sims_questionbank_question_answer_desc_en': 'True',
                            'sims_questionbank_question_section': $scope.temp.sims_questionbank_section_code,
                            'sims_questionbank_question_subsection': $scope.temp.sims_questionbank_subsection_code,
                            'sims_questionbank_question_skill': $scope.temp.sims_questionbank_skill_code,
                            'sims_questionbank_question_header_code': $scope.temp.sims_questionbank_question_header_code,
                        });
                        savefin.push(a);
                        var b = ({
                            'sims_questionbank_question_type': $scope.temp.sims_questionbank_question_type,
                            'sims_questionbank_question_difficulty_level': $scope.temp.sims_questionbank_question_difficulty_level,
                            'sims_questionbank_question_weightage': $scope.temp.sims_questionbank_question_weightage,
                            'sims_questionbank_question_desc_en': $scope.temp.sims_questionbank_question_desc_en,
                            'sims_questionbank_question_answer_desc_en': 'False',
                            'sims_questionbank_question_section': $scope.temp.sims_questionbank_section_code,
                            'sims_questionbank_question_subsection': $scope.temp.sims_questionbank_subsection_code,
                            'sims_questionbank_question_skill': $scope.temp.sims_questionbank_skill_code,
                            'sims_questionbank_question_header_code': $scope.temp.sims_questionbank_question_header_code,
                        });
                        flag = true;
                        savefin.push(b);
                    }

                    //Rating Question Type
                    if ($scope.temp.sims_questionbank_question_type == '4') {
                        debugger;
                        for (var i = 0; i < $scope.Rating.length; i++) {
                            var code = ({
                                'sims_questionbank_question_weightage': $scope.temp.sims_questionbank_question_weightage,
                                'sims_questionbank_question_type': $scope.temp.sims_questionbank_question_type,
                                'sims_questionbank_question_desc_en': $scope.temp.sims_questionbank_question_desc_en,
                                'sims_questionbank_question_difficulty_level': $scope.temp.sims_questionbank_question_difficulty_level,
                                'sims_questionbank_question_answer_desc_en': 'Rating',
                                'sims_questionbank_question_rating_group_code': $scope.temp.sims_questionbank_rating_group_code,
                                'sims_questionbank_question_section': $scope.temp.sims_questionbank_section_code,
                                'sims_questionbank_question_subsection': $scope.temp.sims_questionbank_subsection_code,
                                'sims_questionbank_question_skill': $scope.temp.sims_questionbank_skill_code,
                                'sims_questionbank_question_header_code': $scope.temp.sims_questionbank_question_header_code,
                            });
                            flag = true;
                            savefin.push(code);
                        }
                    }

                    $http.post(ENV.apiUrl + "api/surveyquestionmapping/CUDquestionbankQuestionAnswerforquestionbankquestionmapping", savefin).then(function (savedata) {
                        debugger;
                        $scope.newquetionsandansobject = savedata.data;
                        if ($scope.newquetionsandansobject.length > 0) {
                            swal({ text: "Question and Answer Saved Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else {
                            swal({ text: "Sorry! Question and Answer Not Saved", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                    });
                    savefin = [];
                    $scope.ansname = false;

                }
            }

            $scope.btn_Addnewquestions_update_Click = function () {
                debugger
                //for (var i = 0; i < $scope.newquetionsandansobject.length; i++) {
                //    $scope.questionbank_Questions_Details.push($scope.newquetionsandansobject[i]);
                //}

                //if ($scope.questionbank_Questions_Details.length > 0) {
                //    $scope.questionbank_questions_table = true;
                //} else {
                //    $scope.questionbank_questions_table = false;
                //}

                $scope.data = {
                    'sims_questionbank_question_type': $scope.temp.sims_questionbank_question_type,
                    'sims_questionbank_question_difficulty_level': $scope.temp.sims_questionbank_question_difficulty_level,
                    'sims_questionbank_question_desc_en':  document.getElementById('text-editor1').value,
                    'sims_questionbank_question_weightage': $scope.temp.sims_questionbank_question_weightage,
                    //'sims_questionbank_question_desc_en': $scope.temp.sims_questionbank_question_desc_en,
                    'sims_questionbank_question_answer_desc_en': $scope.temp.sims_questionbank_question_answer_desc_en,
                    'sims_questionbank_question_correct_answer_flag': $scope.temp.sims_questionbank_question_answer_desc_en,
                    'sims_questionbank_skill_code': $scope.temp.sims_questionbank_skill_code,
                    'sims_questionbank_section_code': $scope.temp.sims_questionbank_section_code,
                    'sims_questionbank_subsection_code': $scope.temp.sims_questionbank_subsection_code,
                    'sims_questionbank_code': $scope.temp.sims_questionbank_code,
                    'sims_questionbank_question_code': $scope.temp.sims_questionbank_question_code,
                    'sims_questionbank_question_header_code': $scope.temp.sims_questionbank_question_header_code,
                    'sims_questionbank_question_answer_explanation': $scope.temp.sims_questionbank_question_answer_explanation,
                    'sims_questionbank_question_header_code_description': $scope.temp.sims_questionbank_question_header_code_description,
                }

                console.log("temp to update", $scope.data);
                console.log("ansers", $scope.Answerdata);
                console.log("ansers", $scope.Answerdata);

             

                var lst_t = [];
                lst_t.push($scope.data);

                for (var i = 0; i < $scope.Answerdata.length; i++) {
                    $scope.Answerdata[i].sims_questionbank_question_code = lst_t[0].sims_questionbank_question_code;
                }

                $http.post(ENV.apiUrl + "api/surveyquestionmapping/CUDQuestionBank", lst_t).then(function (msg) {
                    $scope.msg1 = msg.data;
                    console.log("msg", msg);
                    if ($scope.msg1 == true) {
                        // $scope.Answerdata.sims_questionbank_question_code = lst_t[0].sims_questionbank_question_code;
                        $http.post(ENV.apiUrl + "api/surveyquestionmapping/CUDQuestionBankANS", $scope.Answerdata).then(function (msg) {
                            $scope.msg1 = msg.data;
                            console.log("msg", msg);
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "QuestionBank Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                $('.modal-backdrop').remove();
                                $scope.getquestiosbyquestiontype();
                            } else {
                                swal({ title: "Alert", text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                        });
                    } else {
                        swal({ title: "Alert", text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                });

            }

            $scope.uploadimageQM = function (str) {
                debugger
                imagename = '';
                $scope.upload_questionbank_question_code = '';
                $scope.upload_questionbank_question_answer_code = '';
                //imagename = str.sims_questionbank_question_code;
                $scope.upload_questionbank_question_code = '';
                $scope.upload_questionbank_question_answer_code = '';
                $scope.upload_questionbank_question_code = str.sims_questionbank_question_code;                

                imagename = $scope.upload_questionbank_question_code;
                $('#UploadImage').modal('show');
            }

            $scope.uploadimageQAM = function (str, str1) {
                debugger
                imagename = '';
                $scope.upload_questionbank_question_code = '';
                $scope.upload_questionbank_question_answer_code = '';
                imagename = str.sims_questionbank_question_code;

                imagename = str.sims_questionbank_question_answer_code;

                $scope.upload_questionbank_question_code = str1;
                $scope.upload_questionbank_question_answer_code = str.sims_questionbank_question_answer_code;

                $('#UploadQUEANSImage').modal('show');
            }

            $scope.closeUploadmodal = function () {
                debugger;
                $('.modal-backdrop').remove();
                $('#UploadImage').modal('hide');
                $('#UpdateQuestionBank').modal('show');
            
            }

            $scope.file_changed = function (element) {

                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;
                    });
                };
                reader.readAsDataURL(photofile);

                $scope.UploadQustionImg = function () {
                    debugger
                    var random1 = Math.random();
                    $scope.prev_img = "";
                    var data = [];
                    var t = "png";//$scope.photo_filename.split("/")[1];
                    console.log(t);
                    data = {
                        sims_questionbank_question_code: imagename,
                        sims_questionbank_question_image_path: imagename + "." + t,
                        opr: "H"
                    }
                    console.log(data);

                    $http.post(ENV.apiUrl + "api/surveyquestionmapping/CUDUpdatePicsF", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Image Upload Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            $('.modal-backdrop').remove();
                            $scope.GetquestionbankDetails();
                            //$('#myModal').modal('hide');
                            //setTimeout(function () {
                            //    $state.reload();
                            //    $scope.Show_Data();
                            //}, 1000);
                        }

                        $('#UploadImage').modal('hide');
                        $('#UpdateQuestionBank').modal('hide');

                        var request = {
                            method: 'POST',
                            url: ENV.apiUrl + '/api/file/upload?filename=' + imagename + "&location=" + "/Survey",
                            data: formdata,
                            headers: {
                                'Content-Type': undefined
                            }
                        };
                        $http(request)
                      .success(function (d) {
                          //$('.modal-backdrop').remove();
                          //var currentPageTemplate = $location.url();
                          //$templateCache.remove(currentPageTemplate);
                          //    $scope.Show_Data();
                          //$route.reload();
                          //.location.reload();
                          //setTimeout(function () {                              
                          //    window.location.reload();
                          //   // $scope.Show_Data();
                          //},300);
                      },
                      function () {
                          alert("Err");
                      });

                    },
                        function () {
                            $('#ErrorMessage').modal({ backdrop: "static" });


                        });

                }

                $scope.UploadQustionAnswerImg = function () {
                    debugger
                    var random1 = Math.random();
                    $scope.prev_img = "";
                    var data = [];
                    var t = "png";//$scope.photo_filename.split("/")[1];
                    console.log(t);
                    data = {
                        sims_questionbank_question_code: $scope.upload_questionbank_question_code,
                        sims_questionbank_question_answer_code: $scope.upload_questionbank_question_answer_code,
                        sims_questionbank_question_image_path: imagename + "." + t,
                        opr: "G"
                    }
                    console.log(data);

                    $http.post(ENV.apiUrl + "api/surveyquestionmapping/CUDUpdatePicsF", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Image Upload Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            $('.modal-backdrop').remove();

                            
                            $scope.GetquestionbankDetails();
                            //setTimeout(function () {
                            //    $state.reload();
                            //    $scope.Show_Data();
                            //}, 1000);

                            //  $scope.Show_Data();
                        }

                        $('#UploadQUEANSImage').modal('hide');
                        $('#UpdateQuestionBank').modal('hide');
                        var request = {
                            method: 'POST',
                            url: ENV.apiUrl + '/api/file/upload?filename=' + imagename + "&location=" + "/Survey",
                            data: formdata,
                            headers: {
                                'Content-Type': undefined
                            }
                        };
                        $http(request)
                      .success(function (d) {
                          $('.modal-backdrop').remove();
                          $scope.GetquestionbankDetails();

                          //var currentPageTemplate = $location.url();
                          //$templateCache.remove(currentPageTemplate);
                          // $scope.Show_Data();
                          //$route.reload();
                          //.location.reload();
                          //setTimeout(function () {                              
                          //    window.location.reload();
                          //   // $scope.Show_Data();
                          //},300);
                      },
                      function () {
                          alert("Err");
                      });

                    },
                        function () {
                            $('#ErrorMessage').modal({ backdrop: "static" });


                        });

                }

                $scope.getTheFiles = function ($files) {
                    $scope.filesize = true;

                    angular.forEach($files, function (value, key) {
                        formdata.append(key, value);

                        var i = 0;
                        if ($files[i].size > 800000) {
                            $scope.filesize = false;
                            $scope.edt.photoStatus = false;

                            swal({ text: "File Should Not Exceed 800Kb.", imageUrl: "assets/img/notification-alert.png" });

                        }
                        else {

                        }

                    });
                };

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });
                $("#Table6").tableHeadFixer({ 'top': 1 });
                $("#fixTable3").tableHeadFixer({ 'top': 1 });

            }, 100);

            $scope.Reset = function () {
                $scope.temp['sims_questionbank_question_difficulty_level'] = '';
                $scope.temp['sims_questionbank_question_type'] = '';
                
                $scope.temp['sims_questionbank_code'] = '';
                $scope.temp['sims_questionbank_skill_code'] = '';
            }
        }]);
})();