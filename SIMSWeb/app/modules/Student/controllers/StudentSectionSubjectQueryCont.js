﻿(function () {
    'use strict';
    var appcode = [];
    var getdata = [];
    var senddata = [], demo = [], demo1 = [];
    var main;
    var simsController = angular.module('sims.module.Student');
    simsController.controller('StudentSectionSubjectQueryCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.edt = {};
            //$scope.edt['sims_subject_code'] = '';
            $scope.edt['sims_subject_code'] = [];
            $scope.simsArray = [];
            $scope.selectedStudentList = [];
            var user = $rootScope.globals.currentUser.username;
            $scope.busyindicator = true;
            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.table = true;
            // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };


            // Change the selector if needed
            var $table = $('table.scroll'),
                $bodyCells = $table.find('tbody tr:first').children(),
                colWidth;

            // Adjust the width of thead cells when window resizes
            $(window).resize(function () {
                // Get the tbody columns width array
                colWidth = $bodyCells.map(function () {
                    return $(this).width();
                }).get();

                // Set the width of thead columns
                $table.find('thead tr').children().each(function (i, v) {
                    $(v).width(colWidth[i]);
                });
            }).resize(); // Trigger resize handler


            //$http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
            //    $scope.curriculum = res.data;

            //    $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;

            //    $scope.getacyr();
            //});

            $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.curriculum = res.data;
                $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr();
            });

            //function getCur(flag, comp_code) {
            //    if (flag) {

            //        $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
            //            $scope.curriculum = res.data;
            //            $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
            //            $scope.getacyr($scope.curriculum[0].sims_cur_code);

            //        });
            //    }
            //    else {

            //        $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
            //            $scope.curriculum = res.data;
            //            $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
            //            $scope.getacyr($scope.curriculum[0].sims_cur_code);
            //        });
            //    }

            //}

            //$http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
            //    $scope.count = res.data;
            //    if ($scope.count) {
            //        getCur(true, $scope.user_details.comp);
            //    }
            //    else {
            //        getCur(false, $scope.user_details.comp)
            //    }

            //});

            $(function () {
                $('#cmb_grade').multipleSelect({
                    width: '100%'
                });
            });

            $scope.getacyr = function () {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.edt.sims_cur_code).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    $scope.edt['sims_academic_year'] = $scope.Academic_year[0].sims_academic_year;
                    $scope.getsections($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);                    
                })
            }

            $scope.getsection = function (str, str1, str2) {
                $http.get(ENV.apiUrl + "api/StudentSectionSubject/GetRCSection?curcode=" + str + "&ayear=" + str1 + "&gradecode=" + str2 + "&teachercode=" + user).then(function (Allsection) {
                    $scope.section1 = Allsection.data;
                    $scope.term_changeNew($scope.section1[0].sims_section_code);
                })
            };


            $scope.term_changeNew = function (section_name) {
                $http.get(ENV.apiUrl + "api/StudentSectionSubject/getTermNew?curcode=" + $scope.edt.sims_cur_code + "&academicyear=" + $scope.edt.sims_academic_year + "&sims_grade_code=" + $scope.edt.sims_grade_code + "&sims_section_code=" + section_name).then(function (terms) {
                    $scope.terms_obj = terms.data;
                    $scope.edt.sims_section_subject_term_code = '';
                });
            };

            $scope.getsections = function (str, str1) {
                $http.get(ENV.apiUrl + "api/StudentSectionSubject/GetRCGrade?curcode=" + str + "&ayear=" + str1 + "&teachercode=" + user).then(function (res) {
                    $scope.grade = res.data;
                })
            };

            $scope.get = function (section,term) {

                //$http.get(ENV.apiUrl + "api/StudentSectionSubject/getSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&login_user=" + user).then(function (getSectionsubject_name) {
                //    $scope.cmb_subject_name = getSectionsubject_name.data;
                //    setTimeout(function () {
                //        $('#cmb_grade').change(function () {
                //            console.log($(this).val());
                //        }).multipleSelect({
                //            width: '100%'
                //        });
                //    }, 1000);
                //});

                $http.get(ENV.apiUrl + "api/StudentSectionSubject/getSectionsubject_nameNew?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&term=" + term + "&login_user=" + user).then(function (getSectionsubject_name) {
                    $scope.cmb_subject_name = getSectionsubject_name.data;
                    console.log("cmb_subject_name",$scope.cmb_subject_name);
                    //$scope.edt['sims_subject_code'] = $scope.cmb_subject_name[0].sims_subject_code;
                    setTimeout(function () {
                        $('#cmb_grade').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }

            $scope.Show_Data = function () {

                $scope.busyindicator = false;
                $scope.info1 = [];
                var demo = [];
                var demo1 = [];
                $scope.secData = [];
                $scope.table = false;
                $scope.subject = false;
                $scope.showSelectedSubject = false;
                $scope.selectedAll = false;
                if ($scope.edt.sims_subject_code == undefined) {
                    // $scope.edt.sims_subject_code = '';
                    $scope.edt.sims_subject_code = '';
                }
                //$scope.edt.sims_subject_code = [];
                var sims_subject_code = "";
                for (var i = 0; i < $scope.edt.sims_subject_code.length; i++) {
                    sims_subject_code = sims_subject_code + $scope.edt.sims_subject_code[i] + ',';
                }

                $http.get(ENV.apiUrl + "api/StudentSectionSubject/getStudentNameCollege?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&subcode=" + sims_subject_code + "&section_term=" + $scope.edt.sims_section_subject_term_code).then(function (allSectionStudent) {
                    $scope.SectionStudent = allSectionStudent.data;
                    $scope.busyindicator = true;

                    if (allSectionStudent.data.length > 0) {
                        //for (var j = 0; j < $scope.SectionStudent[0].sublist.length; j++) {                                                           
                        //    demo = {
                        //        'sims_subject_name_en': $scope.SectionStudent[0].sublist[j].sims_subject_name_en,
                        //        'sims_subject_code': $scope.SectionStudent[0].sublist[j].sims_subject_code
                        //    }
                        //    demo1.push(demo);                                                                              
                        //}
                        for (var j = 0; j < $scope.SectionStudent[0].sublist.length; j++) {
                            for (var i = 0; i < $scope.edt.sims_subject_code.length; i++) {
                                if ($scope.SectionStudent[0].sublist[j].sims_subject_code == $scope.edt.sims_subject_code[i]) {

                                    demo = {
                                        'sims_subject_name_en': ($scope.SectionStudent[0].sublist[j].sims_subject_name_en).toLowerCase(),
                                        'sims_subject_code': $scope.SectionStudent[0].sublist[j].sims_subject_code
                                    }
                                    demo1.push(demo);

                                }
                            }
                        }

                        for (var k = 0; k < $scope.SectionStudent.length; k++) {
                            for (var j = 0; j < $scope.SectionStudent[k].sublist.length; j++) {
                                $scope.flg = false
                                for (var i = 0; i < $scope.edt.sims_subject_code.length; i++) {
                                    if ($scope.edt.sims_subject_code[i] == $scope.SectionStudent[k].sublist[j].sims_subject_code) {
                                        $scope.flg = true;
                                        break;
                                    }
                                }
                                $scope.SectionStudent[k].sublist[j]['is_visible'] = $scope.flg;

                            }
                        }


                        $scope.info1 = demo1;
                        $timeout(function () {
                            $("#fixTable").tableHeadFixer({ 'left': 3, 'z-index': 100 });
                        }, 100);
                    }
                    else {
                        $scope.subject = true;
                        $scope.table = true;
                    }
                });

            }

            var subject1;
            $scope.skipdSubject = [];

            $scope.Check_Single_Subject = function (enroll, subject) {
                console.log(subject, enroll);
                enroll.ischange = true;
                if (subject.sims_status == false) {
                    $http.get(ENV.apiUrl + "api/StudentSectionSubject/getcheckSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&enroll_number=" + enroll.sims_student_enroll_number + "&subject_code=" + subject.sims_subject_code).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1.length > 0) {
                            swal({
                                text: 'This subject is having Marks in Gradebook.Are you sure want to remove subject ?',
                                showCloseButton: true,
                                showCancelButton: true,
                                confirmButtonText: 'Yes',
                                width: 380,
                                cancelButtonText: 'No',
                            }).then(function (isConfirm) {
                                if (isConfirm) {
                                    subject.sims_status = false;
                                }
                                else {
                                    subject.sims_status = true;
                                    var v = document.getElementById(subject.sims_subject_code + enroll.sims_student_enroll_number);
                                    v.checked = true;
                                }
                            })

                        }
                    })
                }
            }

            //$scope.Select_ALL_Subject = function (str) {
            //    debugger
            //    var id = '';
            //    var subject_code = '';
            //    var student_enroll = ''
            //    $scope.ischecked = '';
            //    var check = document.getElementById(str);
            //    $scope.ischecked = str;
            //    $scope.tem = {};
            //    if (check.checked == false) {

            //        for (var i = 0; i < $scope.SectionStudent.length; i++) {
            //            student_enroll = student_enroll + $scope.SectionStudent[i].sims_student_enroll_number + ',';
            //            subject_code = str;
            //        }
            //        $http.get(ENV.apiUrl + "api/StudentSectionSubject/getcheckSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&enroll_number=" + student_enroll + "&subject_code=" + subject_code).then(function (msg) {
            //            $scope.msg1 = msg.data;
            //            if ($scope.msg1.length > 0) {
            //                $('#MyModal').modal({ backdrop: "static" });
            //            } else {

            //                for (var i = 0; i < $scope.SectionStudent.length; i++) {
            //                    $scope.SectionStudent[i].sublist[check.tabIndex].sims_status = check.checked;
            //                    $scope.SectionStudent[i].ischange = true;
            //                }
            //            }
            //        })
            //    }
            //    else {
            //        for (var i = 0; i < $scope.SectionStudent.length; i++) {
            //            $scope.SectionStudent[i].sublist[check.tabIndex].sims_status = check.checked;
            //            $scope.SectionStudent[i].ischange = true;
            //        }
            //    }
            //}



            $scope.Select_ALL_Subject = function (str, obj, index, chk) {
            debugger
                var check = document.getElementById(str);
                var subject_code = '';
                var student_enroll = ''
                $scope.ischecked = '';

                for (var i = 0; i < $scope.SectionStudent.length; i++) {
                    for (var j = 0; j < $scope.SectionStudent[i].sublist.length; j++) {
                        if ($scope.SectionStudent[i].sublist[j].sims_subject_code == obj.sims_subject_code)
                            $scope.SectionStudent[i].sublist[j]['sims_status'] = chk;
                        //$scope.SectionStudent[i]['ischange'] = chk;
                        $scope.SectionStudent[i]['ischange'] = true;
                    }
                }
                if (chk == false) {
                    for (var i = 0; i < $scope.SectionStudent.length; i++) {
                        student_enroll = student_enroll + $scope.SectionStudent[i].sims_student_enroll_number + ',';
                        subject_code = str;
                    }
                    $http.get(ENV.apiUrl + "api/StudentSectionSubject/getcheckSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&enroll_number=" + student_enroll + "&subject_code=" + subject_code).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1.length > 0) {
                            $('#MyModal').modal({ backdrop: "static" });
                        } else {

                            for (var i = 0; i < $scope.SectionStudent.length; i++) {
                                for (var j = 0; j < $scope.SectionStudent[i].sublist.length; j++) {
                                    if ($scope.SectionStudent[i].sublist[j].sims_subject_code == obj.sims_subject_code)
                                        $scope.SectionStudent[i].sublist[j]['sims_status'] = chk;
                                    //$scope.SectionStudent[i]['ischange'] = chk;
                                    $scope.SectionStudent[i]['ischange'] = true;
                                }
                            }
                        }
                        //    for (var i = 0; i < $scope.SectionStudent.length; i++) {
                        //        //$scope.SectionStudent[i].sublist[check.tabIndex].sims_status = chk;
                        //        $scope.SectionStudent[i].ischange = true;
                        //    }
                        //}
                    })
                }
            }

            $scope.btn_Ok_Click = function () {

                var check = document.getElementById($scope.ischecked);
                for (var i = 0; i < $scope.SectionStudent.length; i++) {
                    $scope.SectionStudent[i].sublist[check.tabIndex].sims_status = false;
                    $scope.SectionStudent[i].ischange = true;
                }
            }

            $scope.btn_cancel_Click = function () {

                var check = document.getElementById($scope.ischecked);
                for (var i = 0; i < $scope.SectionStudent.length; i++) {
                    $scope.SectionStudent[i].sublist[check.tabIndex].sims_status = false;
                    $scope.SectionStudent[i].ischange = true;
                }

                for (var i = 0; i < $scope.SectionStudent.length; i++) {
                    var check = document.getElementById($scope.ischecked);
                    for (var j = 0; j < $scope.msg1.length; j++) {
                        if ($scope.SectionStudent[i].sims_student_enroll_number == $scope.msg1[j].sims_student_enroll_number) {
                            $scope.SectionStudent[i].sublist[check.tabIndex].sims_status = true;
                            $scope.SectionStudent[i].ischange = true;
                        }

                    }
                }

            }

            $scope.Select_Student_ALL_Subject = function (str) {
                var id = '';
                var check = document.getElementById(str);
                if (check.checked) {
                    $scope.selectedStudentList.push(str);
                    for (var i = 0; i < $scope.SectionStudent.length; i++) {
                        if ($scope.SectionStudent[i].sims_student_enroll_number == str) {
                            for (var j = 0; j < $scope.SectionStudent[i].sublist.length; j++) {
                                for (var k = 0; k < $scope.info1.length; k++) {
                                    if($scope.info1[k].sims_subject_code == $scope.SectionStudent[i].sublist[j].sims_subject_code) {
                                        $scope.SectionStudent[i].sublist[j].sims_status = check.checked;
                                    }                                    
                                }
                                //$scope.SectionStudent[i].sublist[j].sims_status = check.checked;
                                $scope.SectionStudent[i].ischange = true;
                            }
                        }
                    }
                    if ($scope.selectedStudentList.length == $scope.SectionStudent.length) {
                        $scope.selectedAll = true;
                    }
                }
                else {
                    $scope.selectedAll = false;
                    var index = $scope.selectedStudentList.indexOf(str);
                    $scope.selectedStudentList.splice(index, 1);
                    for (var i = 0; i < $scope.SectionStudent.length; i++) {
                        if ($scope.SectionStudent[i].sims_student_enroll_number == str) {
                            for (var j = 0; j < $scope.SectionStudent[i].sublist.length; j++) {
                                //$scope.SectionStudent[i].sublist[j].sims_status = check.checked;
                                for (var k = 0; k < $scope.info1.length; k++) {
                                    if($scope.info1[k].sims_subject_code == $scope.SectionStudent[i].sublist[j].sims_subject_code) {
                                        $scope.SectionStudent[i].sublist[j].sims_status = check.checked;
                                    }
                                }
                                $scope.SectionStudent[i].ischange = true;
                            }
                        }
                    }
                }

            }

            $scope.isSelectAll = function () {
                $scope.selectedStudentList = [];
                if ($scope.selectedAll) {
                    $scope.selectedAll = true;
                    for (var k = 0; k < $scope.SectionStudent.length; k++) {
                        $scope.selectedStudentList.push($scope.SectionStudent[k].sims_student_enroll_number);
                    }
                }
                else {
                    $scope.selectedAll = false;
                }

                for (var k = 0; k < $scope.SectionStudent.length; k++) {
                    for (var j = 0; j < $scope.SectionStudent[k].sublist.length; j++) {
                        for (var i = 0; i < $scope.edt.sims_subject_code.length; i++) {
                            if ($scope.edt.sims_subject_code[i] == $scope.SectionStudent[k].sublist[j].sims_subject_code) {
                                $scope.SectionStudent[k].sublist[j].sims_status = $scope.selectedAll;
                                $scope.SectionStudent[k].ischange = true;
                                $scope.SectionStudent[k].chkStudent = $scope.selectedAll;
                            }
                        }
                    }
                }

                //for (var i = 0; i < $scope.SectionStudent.length; i++) {                    
                //    for (var j = 0; j < $scope.SectionStudent[i].sublist.length; j++) {
                //        $scope.SectionStudent[i].sublist[j].sims_status = $scope.selectedAll;
                //        $scope.SectionStudent[i].ischange = true;
                //        $scope.SectionStudent[i].chkStudent = $scope.selectedAll;
                //    }                    
                //}
            }

            $scope.Submit = function () {
                //$scope.busyindicator = true;
                //$scope.table = true;
                debugger
                $scope.subject = false;
                getdata = [];
                var Sdata = [{}];
                for (var i = 0; i < $scope.SectionStudent.length; i++) {

                    if ($scope.SectionStudent[i].ischange == true) {
                        var le = Sdata.length;
                        Sdata[le] = {
                            'sims_student_enroll_number': $scope.SectionStudent[i].sims_student_enroll_number,
                            'sims_cur_code': $scope.SectionStudent[i].sims_cur_code,
                            'sims_academic_year': $scope.SectionStudent[i].sims_academic_year,
                            'sims_grade_code': $scope.SectionStudent[i].sims_grade_code,
                            'sims_section_code': $scope.SectionStudent[i].sims_section_code,
                            'sims_section_term': $scope.edt.sims_section_subject_term_code,
                            'sims_subject_name_en': ''
                        };
                        var sbcode = '';
                        var sbcode1 = '';
                        for (var j = 0; j < $scope.SectionStudent[i].sublist.length; j++) {
                            if ($scope.SectionStudent[i].sublist[j].sims_status == true) {
                                sbcode = sbcode + $scope.SectionStudent[i].sublist[j].sims_subject_code + ',';
                            }
                            else {
                                //sbcode1 = sbcode1 + $scope.SectionStudent[i].sublist[j].sims_subject_code + ',';
                                sbcode1 = sbcode1 + $scope.SectionStudent[i].sublist[j].sims_subject_code + ',';
                            }
                        }

                        Sdata[le].sims_subject_name_en = sbcode;
                        Sdata[le].sims_subject_code = sbcode1;
                    }
                }

                Sdata.splice(0, 1);
                var data = Sdata;
                console.log("data",data);
                if (Sdata.length != 0) {
                    $http.post(ENV.apiUrl + "api/StudentSectionSubject/insert_student_section_subject_college", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.selectedAll = false;
                        Sdata = [{}];
                        data = [];
                        $scope.selectedStudentList = [];
                        swal({ text: $scope.msg1.strMessage, width: 380 });
                        $scope.Show_Data();
                        //$scope.busyindicator = false;
                        //$http.get(ENV.apiUrl + "api/StudentSectionSubject/getStudentName?curcode=" + $scope.cur_code + "&gradecode=" + $scope.grade_code2 + "&academicyear=" + $scope.year + "&section=" + $scope.section).then(function (allSectionStudent) {                            
                        //    //$scope.table = false;
                        //    $scope.SectionStudent = allSectionStudent.data;
                        //    demo1 = [];                            
                        //    if (allSectionStudent.data.length > 0) {
                        //        //for (var j = 0; j < $scope.SectionStudent[0].sublist.length; j++) {
                        //        //    demo = {
                        //        //        'sims_subject_name_en': $scope.SectionStudent[0].sublist[j].sims_subject_name_en,
                        //        //        'sims_subject_code': $scope.SectionStudent[0].sublist[j].sims_subject_code
                        //        //    }
                        //        //    demo1.push(demo);
                        //        //}
                        //        for (var j = 0; j < $scope.SectionStudent[0].sublist.length; j++) {
                        //            for (var i = 0; i < $scope.edt.sims_subject_code.length; i++) {
                        //                console.log($scope.edt.sims_subject_code[i]);
                        //                if ($scope.SectionStudent[0].sublist[j].sims_subject_code == $scope.edt.sims_subject_code[i]) {
                        //                    demo = {
                        //                        'sims_subject_name_en':($scope.SectionStudent[0].sublist[j].sims_subject_name_en).toLowerCase(),
                        //                        'sims_subject_code': $scope.SectionStudent[0].sublist[j].sims_subject_code
                        //                    }
                        //                    demo1.push(demo);
                        //                }
                        //            }
                        //        }
                        //        $scope.info1 = demo1;
                        //        $timeout(function () {
                        //            $("#fixTable").tableHeadFixer({ 'left': 3, 'z-index': 40000 });
                        //        }, 100);
                        //        //$scope.table = false;
                        //    }
                        //    else {
                        //        $scope.subject = true;
                        //    }
                        //});
                    });
                    senddata = [];
                    //$scope.table = false;

                    // subject1.checked = false;
                }
                else {
                    swal({ text: $scope.msg1.strMessage, width: 380 });
                }
            }

        }]);

})();



