﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ConsistancyCheck1Cont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.ConsistancyCheck1_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
               
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            $scope.t = false;

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $timeout(function () {
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $http.get(ENV.apiUrl + "api/common/ConsistancyCheck1/getconsistancycheck1").then(function (res) {
               
                $scope.display = false;
                $scope.grid = true;
                $scope.ConsistancyCheck1_data = res.data;
                $scope.totalItems = $scope.ConsistancyCheck1_data.length;
                $scope.todos = $scope.ConsistancyCheck1_data;
                $scope.makeTodos();
                //$scope.grid = true;
            });


           


            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
         var yyyy = today.getFullYear();
          
            $scope.cancel = function () {
                $scope.edt = {};
                $scope.grid = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }
            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.sims_scriptid = true;
                    var autoid;
                     
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = true;
                    $scope.update1 = false;
                    $scope.delete1 = false;
                    $scope.edit_data = true;
                    $scope.edit_code = false;
               
                    $scope.edt = [];
                    $scope.edt =
                   {
                       sims_status: true,
                       sims_startdate: dd + '-' + mm + '-' + yyyy,
                   }
                    
                    $http.get(ENV.apiUrl + "api/common/ConsistancyCheck1/getScriptId").then(function (res) {
                        autoid = res.data;
                        $scope.edt['sims_scriptid'] = autoid;
                    });

                    //frequency//
                    $http.get(ENV.apiUrl + "api/common/ConsistancyCheck1/getalertfrequency").then(function (res) {
                        debugger;
                        $scope.frequency = res.data;
                        console.log('alert freq', $scope.frequency);
                    });
                 }
            }

            //Edit//
            $scope.edit = function (str) {
                debugger;
            if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = false;
                    $scope.update1 = true;
                    $scope.delete1 = false;
                    
                    $scope.edt =
                        {
                            sims_scriptid: str.sims_scriptid,

                            sims_scriptname:str.sims_scriptname,
                            sims_startdate: str.sims_startdate,
                            sims_nextdate: str.sims_nextdate,
                            sims_alertdays:str.sims_alertdays,
                            sims_status: str.sims_status,
                            sims_control_field:str.sims_control_field,
                            sims_alert_name:str.sims_alert_name,
                            sims_alert_desc:str.sims_alert_desc,
                            sims_alert_time:str.sims_alert_time,
                            sims_alert_frequncy:str.sims_alert_frequncy,
                            sims_alert_frequncy_time: str.sims_alert_frequncy_time,
                            sims_alert_due_days: str.sims_alert_due_days,
                        }

                    $scope.edit_code = true;

                }
            }




            //update//
            $scope.Update = function (isvalidate) {
                debugger;
              var data1 = [];
                
                 if (isvalidate) {
                        var data = ({
                        sims_scriptid: $scope.edt.sims_scriptid,
                        sims_scriptname: $scope.edt.sims_scriptname,
                        sims_startdate: $scope.edt.sims_startdate,
                        sims_nextdate: $scope.edt.sims_nextdate,
                        sims_alertdays: $scope.edt.sims_alertdays,
                        sims_status: $scope.edt.sims_status,
                        sims_control_field: $scope.edt.sims_control_field,
                        sims_alert_name: $scope.edt.sims_alert_name,
                        sims_alert_desc: $scope.edt.sims_alert_desc,
                        sims_alert_time: $scope.edt.sims_alert_time,
                        sims_alert_frequncy: $scope.edt.sims_alert_frequncy,
                        sims_alert_frequncy_time: $scope.edt.sims_alert_frequncy_time,
                        sims_alert_due_days: $scope.edt.sims_alert_due_days,
                        opr: 'U'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/ConsistancyCheck1/consistancycheck1in", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });

                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Not Updated Successfully. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }



            $scope.Save = function (isvalidate) {
                var data1 = [];
                
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({
                            sims_scriptid: $scope.edt.sims_scriptid,

                            sims_scriptname: $scope.edt.sims_scriptname,
                            sims_startdate: $scope.edt.sims_startdate,
                            sims_nextdate: $scope.edt.sims_nextdate,
                            sims_alertdays: $scope.edt.sims_alertdays,
                            sims_status: $scope.edt.sims_status,
                            sims_control_field: $scope.edt.sims_control_field,
                            sims_alert_name: $scope.edt.sims_alert_name,
                            sims_alert_desc: $scope.edt.sims_alert_desc,
                            sims_alert_time: $scope.edt.sims_alert_time,
                            sims_alert_frequncy: $scope.edt.sims_alert_frequncy,
                            sims_alert_frequncy_time: $scope.edt.sims_alert_frequncy_time,
                            sims_alert_due_days: $scope.edt.sims_alert_due_days,
                           // sims_alert_time: $scope.edt.sims_alert_time,
                           // sims_alert_time: $scope.edt.sims_alert_time,
                            opr: 'I'
                        });

                        data1.push(data);

                        $http.post(ENV.apiUrl + "api/common/ConsistancyCheck1/consistancycheck1in", data1).then(function (res) {
                            
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            console.log($scope.msg1);
                            if ($scope.msg1 == true) {
                                swal({ text: " Added Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: " Not Added Successfully. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }
            

            $scope.getgrid = function () {
               
                $http.get(ENV.apiUrl + "api/common/ConsistancyCheck1/getconsistancycheck1").then(function (res) {

                    $scope.display = false;
                    $scope.grid = true;
                    $scope.ConsistancyCheck1_data = res.data;
                    $scope.totalItems = $scope.ConsistancyCheck1_data.length;
                    $scope.todos = $scope.ConsistancyCheck1_data;
                    $scope.makeTodos();
                    //$scope.grid = true;
                });



                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                //$scope.Myform.$setPristine();
                //$scope.Myform.$setUntouched();
            }



      //  checkonebyonedelete  //
            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }
            ////Delete////
            $scope.Delete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    var deletecode = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_scriptid + i);

                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodercode = ({
                                'sims_scriptid': $scope.filteredTodos[i].sims_scriptid,
                                'sims_scriptname': $scope.filteredTodos[i].sims_scriptname,
                                'sims_startdate': $scope.filteredTodos[i].sims_startdate,
                                'sims_nextdate': $scope.filteredTodos[i].sims_nextdate,
                                'sims_alertdays': $scope.filteredTodos[i].sims_alertdays,
                                'sims_status': $scope.filteredTodos[i].sims_status,
                                'sims_control_field': $scope.filteredTodos[i].sims_control_field,
                                'sims_alert_name': $scope.filteredTodos[i].sims_alert_name,
                                'sims_alert_desc': $scope.filteredTodos[i].sims_alert_desc,
                                'sims_alert_time': $scope.filteredTodos[i].sims_alert_time,
                                'sims_alert_frequncy_time': $scope.filteredTodos[i].sims_alert_frequncy_time,
                                'sims_alert_due_days': $scope.filteredTodos[i].sims_alert_due_days,

                                'opr': 'D'
                            });
                            deletecode.push(deletemodercode);
                        }
                    }

                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {


                                $http.post(ENV.apiUrl + "api/common/ConsistancyCheck1/consistancycheck1in", deletecode).then(function (res) {
                                    $scope.msg1 = res.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: " Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Record Already Mapped.Can't be Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });


                            }
                            else {
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }

                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    //var v = document.getElementById(i);
                                    var v = document.getElementById($scope.filteredTodos[i].sims_scriptid + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }

                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                }
            }



            $('.clockpicker').clockpicker({
                autoclose: true
            });
           
            $scope.CheckAllChecked = function () {
               
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_scriptid + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_scriptid + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }


            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.ConsistancyCheck1_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

          
            $scope.search = function () {
                debugger;
                $scope.todos = $scope.searched($scope.ConsistancyCheck1_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.ConsistancyCheck1_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_scriptid + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
              }
            function searchUtil(item, toSearch) {
                debugger;
                /* Search Text in all 3 fields */
                return (item.sims_scriptid.toString().toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
                
            }


        }])
})();