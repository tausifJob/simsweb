﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('studentpromotionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.pagesize = "All";
            $scope.pageindex = 0;
            $scope.display = false;
            $scope.table1 = true;
            $scope.fee_no = true;
            $scope.table = true;
            $scope.visit_no = true;
            var check_no = null;
            $scope.busy = false;
            $scope.chku = false;
            $scope.edt = [];
            $scope.sims_all_flag = true;
            //CUR
            //$http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
            //    $scope.curriculum = AllCurr.data;
            //    $scope.sims_cur_code= $scope.curriculum[0].sims_cur_code
            //    $scope.getAccYear();
            //});

            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();

            function getCur(flag, comp_code) {
                if (flag) {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        $scope.curriculum = res.data;
                        $scope.sims_cur_code = $scope.curriculum[0].sims_cur_code
                        $scope.getAccYear();

                    });
                }
                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        $scope.curriculum = res.data;
                        $scope.sims_cur_code = $scope.curriculum[0].sims_cur_code
                        $scope.getAccYear();

                    });


                }

            }

            $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.global_count_comp = res.data;

                if ($scope.global_count_comp) {
                    getCur(true, $scope.user_details.comp);


                }
                else {
                    getCur(false, $scope.user_details.comp)
                }
            });



            $scope.getAccYear = function () {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.sims_cur_code).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;


                    if (Acyear.data.length > 0) {
                        $scope.sims_academic_year = $scope.Acc_year[0].sims_academic_year;
                        $scope.getGrade();
                    }


                    //$scope.temp = {
                    //    'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                    //    'sims_academic_year': $scope.Acc_year[0].sims_academic_year
                    //}
                  
                });
            }

            //Grade
            $scope.getGrade = function () {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.sims_cur_code + "&academic_year=" + $scope.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
                $scope.getSection($scope.sims_cur_code, $scope.temp.sims_grade_code, $scope.sims_academic_year)
            }

            //Section
            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.sims_cur_code + "&grade_code=" + gradeCode + "&academic_year=" + $scope.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }


            //Academic year
            $http.get(ENV.apiUrl + "api/StudentReport/getAcademicYearActive").then(function (AllacademicActive) {
                $scope.getAllactive = AllacademicActive.data;
                if (AllacademicActive.data.length > 0) {
                    $scope.edt = {
                        'sims_academic_years': $scope.getAllactive[0].sims_academic_years
                    }
                }
            });

            $scope.Submit = function (Myform)
            {
                debugger
                if (Myform) {
                    debugger
                    $scope.promotegrade = true;
                    $scope.newpromotegrade = false;
                    $http.get(ENV.apiUrl + "api/StudentReport/getAllTermDetailspromotion?cur=" + $scope.sims_cur_code + "&a_year=" + $scope.sims_academic_year + "&grades=" + $scope.temp.sims_grade_code + "&section=" + $scope.temp.sims_section_code + "&advanced_year=" + $scope.edt.sims_academic_years).then(function (AllTermDetails) {
                        debugger
                        $scope.getAllTerm_details = AllTermDetails.data;
                        $scope.totalItems = $scope.getAllTerm_details.length;
                        $scope.todos = $scope.getAllTerm_details;
                        $scope.makeTodos();
                        $scope.size($scope.pagesize);

                    });
                }
            }

            //$scope.getCombination=function(year)
            //{
            //    $scope.promotegrade = false;
            //    $scope.newpromotegrade = true;
            //    debugger;
            //    $http.get(ENV.apiUrl + "api/StudentReport/getgradesectionCombo?cur_year=" + year).then(function (getCombogrdsec) {
            //        $scope.getCombogrd_sec = getCombogrdsec.data;
            //    });
            //}

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.getAllTerm_details;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.getAllTerm_details, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.getAllTerm_details;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.std_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.parent_name == toSearch) ? true : false;

            }

            $scope.New = function () {

                $scope.temp = '';
                $scope.edt = '';
                $scope.display = true;
                $scope.table = false;
                $scope.table1 = false;
                $scope.instaminimuma = true;
                $scope.update1 = false;

                $scope.Update_btn = false;
                $scope.show_btn = true;


            }


            $scope.savedatapromote = function () {
                $scope.busy = true;
                debugger;
                var deleteintcode = [];
                var Savedata = [];


                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                   // if ($scope.filteredTodos[i].status == true) {
                    var v = document.getElementById("promo-"+i);
                    if (v.checked == true) {
                        var deleteintcode = ({
                            'sims_cur_code': $scope.sims_cur_code,
                            'sims_academic_year': $scope.edt.sims_academic_years,
                            'sims_sims_grades': $scope.filteredTodos[i].promote_code,
                            'sims_enrollment_number': $scope.filteredTodos[i].sims_enroll_number,
                            'sims_academic_year_old': $scope.sims_academic_year,
                            'user_name':user,
                            opr: 'I',
                        });
                        $scope.insert = true;
                        Savedata.push(deleteintcode);
                    }
                }
                if ($scope.insert) {
                    $http.post(ENV.apiUrl + "api/StudentReport/CUDPromoteDetails", Savedata).then(function (msg) {
                        debugger;
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 380, height: 200 });
                               $scope.busy = false;
                            deleteintcode = [];
                            Savedata = [];
                            $scope.Submit();
                        }
                        else {
                            swal({ text: "Student Already Present. " + $scope.msg1, imageUrl: "assets/img/notification-alert.png", width: 380, height: 200 });
                               $scope.busy = false;
                        }

                    });
                }
            }

            $scope.CheckAllChecked = function () {
                debugger
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("promo-"+i);
                        v.checked = true;
                        $scope.filteredTodos[i].status = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    $scope.sims_all_flag = true;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("promo-" + i);
                        v.checked = false;
                        $scope.filteredTodos[i].status = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkPendingAmt = function (str) {
                debugger;
                if (str.promote_code)
                {
                
                    $http.get(ENV.apiUrl + "api/StudentReport/getPendingAmount?sims_cur_code=" + $scope.sims_cur_code + "&sims_academic_year=" + $scope.sims_academic_year + "&sims_grade_code=" + $scope.temp.sims_grade_code + "&sims_section_code=" + $scope.temp.sims_section_code + "&sims_enroll_number=" + str.sims_enroll_number).then(function (msg) {
                         $scope.message=msg.data;
                         if ($scope.message!='') {
                        
                            swal({
                                text: $scope.message,
                                imageUrl: "assets/img/check.png",
                                confirmButtonText: 'OK',
                                cancelButtonText: "Cancel",
                                showCloseButton: true, width: 380,
                                showCancelButton: true,
                            }).then(function (isConfirm) {

                            if (isConfirm) {
                               
                                str.datastatus = true;
                            }
                            else {

                                str.datastatus = false;
                               
                            }
                        });
                    }
                    
                });
              }
              else
                {
                    swal({ text: 'Please select Promotion Grade', imageUrl: "assets/img/check.png", width: 380, height: 200 });
                    str.datastatus=false;
                    return;
                }
                
          }

            $scope.checkonebyonedelete = function (str) {
                debugger
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                        //  str.status = true;
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                        //str.status = false;
                    }
                });
                if (str.datastatus == true) {
                    str.status = true;
                } else {
                    str.status = false;
                }
            }

            //$scope.Cancel = function () {
            //    $scope.edt = '';
            //    $scope.temp = '';
            //}

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.record_grid = true;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

        }])
})();
