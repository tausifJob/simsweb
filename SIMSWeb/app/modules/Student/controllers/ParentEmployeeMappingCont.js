﻿
(function () {
    'use strict';
    var edt = [];
    var temp = [];
    var simsController = angular.module('sims.module.Student');
    var formulaData = [];
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('ParentEmployeeMappingCont',
      ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

          $scope.pagesize = '10';
          $scope.pageindex = "0";
          $scope.search_btn = true;
          $scope.clr_btn = true;
          $scope.pager = true;
          $scope.users = $rootScope.globals.currentUser.username;
          $scope.size = function (str) {


              if (str == "All") {
                  $scope.currentPage = '1';
                  $scope.filteredTodos = $scope.todos;
                  $scope.pager = false;
              }
              else {
                  $scope.pager = true;
                  $scope.pagesize = str;
                  $scope.currentPage = 1;
                  $scope.numPerPage = str;
                  $scope.makeTodos();
              }
          }

          $scope.index = function (str) {
              $scope.pageindex = str;
              $scope.currentPage = str;
              console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
              main.checked = false;
          }

          $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

          $scope.makeTodos = function () {
              var rem = parseInt($scope.totalItems % $scope.numPerPage);
              if (rem == '0') {
                  $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
              }
              else {
                  $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
              }

              var begin = (($scope.currentPage - 1) * $scope.numPerPage);
              var end = parseInt(begin) + parseInt($scope.numPerPage);

              console.log("begin=" + begin); console.log("end=" + end);

              $scope.filteredTodos = $scope.todos.slice(begin, end);

              for (var i = 0; i < $scope.filteredTodos.length; i++) {

                  $scope.filteredTodos[i].icon = 'fa fa-plus-circle';
              }

              $scope.filteredTodos = $scope.todos.slice(begin, end);
          };


          $scope.getcur = function () {
              $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                  $scope.curriculum = AllCurr.data;
                  $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                  $scope.getAccYear($scope.edt.sims_cur_code);
              });
          }

          $scope.getcur();

          $scope.getAccYear = function (curCode) {
              $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                  $scope.Acc_year = Acyear.data;
                  $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                  $scope.getGrade($scope.edt.sims_cur_code, $scope.temp.sims_academic_year);

              });
          }

          $scope.getGrade = function (curCode, accYear) {
              $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                  $scope.Grade_code = Gradecode.data;
                  console.log($scope.Grade_code);

                  setTimeout(function () {
                      $('#Select9').change(function () {
                          console.log($(this).val());
                      }).multipleSelect({
                          width: '100%'
                      });
                  }, 1000);

              });
          }
          $scope.refreshing_data = function () {

              $scope.save_btn = false;
              var data = [];
              var Stud_Details = data;
              $scope.totalItems = Stud_Details.length;
              $scope.todos = Stud_Details;
              $scope.makeTodos();
          }
          var temp_emp_id = [];
          $scope.full_data = {};
          $scope.getEmpID = function (j) {
              try {
                  if (temp_emp_id.length > 0) {
                      temp_emp_id = 0;
                      $scope.full_data = {};
                  }
              } catch (e) {

              }
              $scope.full_data = j;
              temp_emp_id = j.sims_admission_number;
              $rootScope.visible_stud = false;
              $rootScope.visible_parent = false;
              $rootScope.visible_search_parent = false;
              $rootScope.visible_teacher = false;
              $rootScope.visible_User = false;
              $rootScope.visible_Employee = true;
              $rootScope.chkMulti = false;
              $scope.global_Search_click();
              $('#Global_Search_Modal').modal({ backdrop: "static" });
              $scope.temp.em_login_code = '';
              $scope.temp.EmpName = '';
              $scope.showBtn = false;
          }
          $scope.clear_data = function () {
              $scope.filteredTodos = [];
              $scope.pager = false;


          }
          $scope.$on('global_cancel', function (str) {

              console.log($scope.SelectedUserLst);
              if ($scope.SelectedUserLst.length > 0) {
                  $scope.full_data.sims_employee_ID = $scope.SelectedUserLst[0].em_login_code;

              }
          });

          $http.post(ENV.apiUrl + "api/MapingController/SearchEmployee").then(function (dataEMPRecord) {
              $scope.GetAllTeacherName = dataEMPRecord.data;
          });

          $scope.insertDataDetails = function () {
              debugger;
              var temp_sending_data = [];
              var temp = [];
              for (var i = 0; i < $scope.filteredTodos.length; i++) {
                  if ($scope.filteredTodos[i].sims_employee_ID == "") {

                  } else {
                      var temp_employee = $scope.filteredTodos[i].sims_employee_ID.split("/");
                      temp = {
                          created_by: $scope.users,
                          
                          sims_parent_login_code: $scope.filteredTodos[i].sims_parent_login_code,
                          name: $scope.filteredTodos[i].parent_nm,
                          sims_employee_ID: temp_employee[0],
                      }

                      temp_sending_data.push(temp);
                  }
              }


              if (temp_sending_data.length > 0) {

                  $http.post(ENV.apiUrl + "api/MapingController/insertemployeedata", temp_sending_data).then(function (msg) {
                      $scope.msg1 = msg.data;
                      if ($scope.msg1 == true) {
                          swal({ text: "Record Insert Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });

                      }
                      else if ($scope.msg1 == false) {
                          swal({ text: "Record Not Insert Successfully. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                      }
                      else {
                          swal("Error-" + $scope.msg1)
                      }
                  });
              }
          }

          $scope.get_data = function () {
            //  alert("dfghjkl");
              debugger;
              if ($scope.temp.mapping_status) {
                  var status = "Y";
              }
              else
                  var status = "N";

              $http.get(ENV.apiUrl + "api/MapingController/getDetailsParent?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grd_code=" + $scope.temp.sims_grade_code + "&mappingstatus=" + status).then(function (data_details) {
                      var Stud_Details = data_details.data;
                      $scope.totalItems = Stud_Details.length;
                      $scope.todos = Stud_Details;
                      $scope.makeTodos();
                      $scope.save_btn = true;
                  });
               
          }

      }])


})();