﻿
(function () {
    'use strict';
    

    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SurveyRatingCont',
        ['$scope', '$state', '$stateParams', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $stateParams, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {
            //$scope.Rating = "";
            $scope.display = false;
            $scope.table = true;
            $scope.pager1 = false;
            $scope.uploading_doc1 = true;
         
            var rt_groupcode='';
            $scope.getgroupcode = function () {
               
                $http.get(ENV.apiUrl + "api/SurveyRating/getRatingGroupCode").then(function (g_code) {
                    debugger;
                   rt_groupcode = g_code.data[0].ratingGroupCode;
                   console.log(g_code.data);
               })                
            }

            $scope.getgroupcode();
            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.pagesize = "10";
            $scope.Rating =
            {
                statusR : true,
                statusD : true
            };


            $scope.surveyRating = function () {
                console.log($scope.surveyRating)
                debugger;
                var a = { opr: 'S' };
                $http.post(ENV.apiUrl + "api/SurveyRating/RatingCommon", a).then(function (response) {
                    debugger;
                    $scope.surveySettings = response.data.table;
                    console.log($scope.surveySettings)
                    $scope.totalItems = $scope.surveySettings.length;
                    $scope.todos = $scope.surveySettings;
                    $scope.makeTodos();
                  
                });
            }
            $scope.surveyRating();

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseFloat($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseFloat(begin) + parseFloat($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                $scope.selectedAll = false;
                // $scope.isSelectAll();

            };

            $scope.New = function () {
                $scope.display = true;
                $scope.table = false;
                $scope.Table2 = true;
                $scope.save_btn = true;
                $scope.add_btn = true;
                $scope.upd_btn = false;
                $scope.OkDelete = false;
                $scope.Rating = {};
                $scope.ratingArray = [];
                $scope.Rating.StatusR = true;
                $scope.Delete = false;
                $scope.rm_edt = true;
                $scope.AddRating;
                //$scope.Rating = {
                //    statusR: true,
                //    statusD: true
                //};

                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();                
            };
            
            $scope.CheckAllChecked = function () {
                 
                        main = document.getElementById('mainchk');

                    if (main.checked == true) {
                        for (var i = 0; i < $scope.ratingArray.length; i++) {
                            var v = document.getElementById('mainchk' + $scope.ratingArray[i].ratingGroupCode + i);
                            v.checked = true;
                            $('tr').addClass("row_selected");
                        }
                            //$('.sub_checkbox').find('input').prop('checked', true);
                            //$(this).closest('tr').addClass("row_selected");
                            //$scope.color = '#edefef';
                            //for (var i = 0; i < $scope.ratingArray.length; i++) {
                            //    $scope.ratingArray[i].datastatus = true;
                            //}
                    }
                    else {
                        for (var i = 0; i < $scope.ratingArray.length; i++) {
                            var v = document.getElementById('mainchk' + $scope.ratingArray[i].ratingGroupCode + i);
                            v.checked = false;
                            main.checked = false;
                            $scope.row1 = '';
                            $('tr').removeClass("row_selected");
                        }
                            //$('.sub_checkbox').find('input').prop('checked', false);
                            //$(this).closest('tr').addClass("row_selected");
                            //$scope.color = '#edefef';
                    
                }
            }

            var ind;
            var data = [];
            $scope.checkonebyonedelete = function (str,index) {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                        //  str.status = true;
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                        //str.status = false;
                    }
                });
                str.status = str.datastatus;
                ind = index;
                //data = str;
            }

            $scope.Save = function (Myform) {
                debugger;
                $scope.flag = false;
                $scope.Rating['survey_det']=[];
                var rt_code = [];
                console.log($scope.Rating);
                $scope.Rating.opr = 'I';
                $scope.Rating['DescriptionG'] = $scope.Rating.DescriptionG,
                $scope.Rating['StatusR']= $scope.Rating.StatusR,
                $scope.Rating['DescriptionOtherLanguage']= $scope.Rating.DescriptionOtherLanguage
                
                //send.push($scope.Rating);
                // if (Myform) {
               

                    for (var i = 0; i < $scope.ratingArray.length; i++) {
                    var v = document.getElementById('mainchk' + $scope.ratingArray[i].ratingGroupCode + i);

                    if (v.checked == true) {
                        //if ($scope.file_doc.length == 0) {
                        //    rt_code = {
                        //        'RatingCode': $scope.ratingArray[i].ratingCode,
                        //        'RatingName': $scope.ratingArray[i].ratingName,
                        //        'DescriptionD': $scope.ratingArray[i].descriptionD,
                        //        'Point': $scope.ratingArray[i].point,
                        //        'StatusD': $scope.ratingArray[i].statusD,
                        //        'sims_survey_rating_img_path': $scope.ratingArray[i].sims_survey_rating_img_path,
                        //    }
                        //    $scope.Rating['survey_det'].push(rt_code);
                        //}

                        //else {
                            rt_code = {
                                'RatingCode': $scope.ratingArray[i].ratingCode,
                                'RatingName': $scope.ratingArray[i].ratingName,
                                'DescriptionD': $scope.ratingArray[i].descriptionD,
                                'Point': $scope.ratingArray[i].point,
                                'StatusD': $scope.ratingArray[i].statusD,
                                'sims_survey_rating_img_path': $scope.ratingArray[i].sims_survey_rating_img_path,
                            }
                            $scope.Rating['survey_det'].push(rt_code);
                            $scope.flag = true;
                        //}
                    }

                    //}
                           
                            
                   // $scope.flag = true;
                           

                    if ($scope.flag == false) {
                        swal('', 'Please select the records to Insert');
                        return;
                    }
                
            }

                    $http.post(ENV.apiUrl + "api/SurveyRating/CUD_SaveSurveyDetails", $scope.Rating).then(function (res) {
                        console.log(res);
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.surveyRating();
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                    $scope.display = false;
                    $scope.table = true;
                    // $scope.surveyRating();
               // }
            }
           

            $scope.Update = function (Myform) {
                var send = [];
                var Ratingcode = [];
                $scope.flag = false;
                debugger;
                console.log($scope.Rating);
                var group_code = $scope.Rating.RatingGroupCode;
                var group_desc = $scope.Rating.DescriptionG;
                var group_descot = $scope.Rating.DescriptionOtherLanguage;
                var group_status = $scope.Rating.StatusR;
                if (Myform)
                    {
                    for (var i = 0; i < $scope.ratingArray.length; i++) {
                        var v = document.getElementById('mainchk' + $scope.ratingArray[i].ratingGroupCode + i);

                       if (v.checked == true) {
                           if ($scope.file_doc.length == 0) {
                               Ratingcode = {
                                   'opr': 'U',
                                   'RatingGroupCode': group_code,
                                   'DescriptionG': group_desc,
                                   'StatusR': group_status,
                                   'DescriptionOtherLanguage': group_descot,
                                   'RatingCode': $scope.ratingArray[i].ratingCode,
                                   'RatingName': $scope.ratingArray[i].ratingName,
                                   'DescriptionD': $scope.ratingArray[i].descriptionD,
                                   'Point': $scope.ratingArray[i].point,
                                   'StatusD': $scope.ratingArray[i].statusD,
                                   'sims_survey_rating_img_path': $scope.ratingArray[i].sims_survey_rating_img_path,
                               }
                           }
                           else {
                               Ratingcode = {
                                   'opr': 'U',
                                   'RatingGroupCode': group_code,
                                   'DescriptionG': group_desc,
                                   'StatusR': group_status,
                                   'DescriptionOtherLanguage': group_descot,
                                   'RatingCode': $scope.ratingArray[i].ratingCode,
                                   'RatingName': $scope.ratingArray[i].ratingName,
                                   'DescriptionD': $scope.ratingArray[i].descriptionD,
                                   'Point': $scope.ratingArray[i].point,
                                   'StatusD': $scope.ratingArray[i].statusD,
                                   'sims_survey_rating_img_path': $scope.ratingArray[i].sims_survey_rating_img_path,
                               }
                          }
                       //}
                           $scope.flag = true;
                        send.push(Ratingcode);
                        }
                       }
                    if ($scope.flag == false) {
                        swal('', 'Please select the records to update');
                        return;
                    }
                }
                $http.post(ENV.apiUrl + "api/SurveyRating/CUDSurveyRatingSave", send).then(function (res) {
                    $scope.msg1 = res.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        $scope.surveyRating();
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                    $scope.surveyRating();

                });
                $scope.display = false;
                $scope.table = true;
                $scope.save_btn = false;
                $scope.surveyRating();
            }
            
            $scope.edit = function (str) {
                debugger;
                $scope.display = true;
                $scope.table = false;
                $scope.upd_btn = true;
                $scope.save_btn = false;
                $scope.add_btn = true;
                $scope.Table2 = true;
                $scope.Delete = true;
                $scope.rm_edt = false;
                console.log(str);
                $scope.prev_img='';
                //$scope.Rating = {
                //    RatingGroupCode: str.sims_survey_rating_group_code,
                //    DescriptionG: str.sims_survey_rating_group_desc_en,
                //    DescriptionOtherLanguage: str.sims_survey_rating_group_desc_ot,
                //    StatusR: str.sims_survey_rating_group_status,
                //    RatingCode: str.sims_survey_rating_code,
                //    RatingName: str.sims_survey_rating_name,
                //    DescriptionD: str.sims_survey_rating_desc,
                //    Point: str.sims_survey_rating_point,
                //    StatusD: str.sims_survey_rating_status
                //}

                
                    
                var Rating1 = [];
                $http.post(ENV.apiUrl + "api/SurveyRating/RatingDetails?rating=" + str.sims_survey_rating_group_code).then(function (rate_data) {                    
                    Rating1 = rate_data.data;
                    $scope.Rating = {
                        RatingGroupCode: Rating1[0].ratingGroupCode,
                        DescriptionG: Rating1[0].descriptionG,
                        DescriptionOtherLanguage: Rating1[0].descriptionOtherLanguage,
                        StatusR: Rating1[0].statusR,                       
                    }
                    
                    $scope.ratingArray = Rating1;
                    $scope.getData();
                    setTimeout(function () {
                        angular.forEach($scope.ratingArray, function (value, key) {
                            var num = Math.random();
                            var imgSrc = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/Emojis/' + value.sims_survey_rating_img_path + "?v=" + num;
                            //document.getElementById(value.sims_student_enroll_number).setAttribute('src', imgSrc);
                            $("#" + key).attr('src', imgSrc);
                        });
                    }, 1000);
                    
                })
                
            }

           
            //$scope.editrow = function (st, index) {
            //    debugger;
            //    $scope.Rating.ratingName = st.ratingName;
            //    $scope.Rating.DescriptionD = st.descriptionD;
            //    $scope.Rating.Point = st.point;
            //    $scope.Rating.StatusD = st.statusD;
            //    $scope.Rating.ratingCode = st.ratingCode;               
            //}

            $scope.OkDelete = function () {
                debugger;
                
                var deleteArray = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.ratingArray.length; i++)
                {
                    $scope.flag = true;
                //    //var v = document.getElementById('mainchk' + $scope.ratingArray[i].ratingGroupCode + i);
                //    if ($scope.ratingArray[i].datastatus == true)
                       
                    //{
                    var v = document.getElementById('mainchk' + $scope.ratingArray[i].ratingGroupCode + i);
                  
                    if (v.checked == true) {
                        var deletemodulecode = ({                            
                            opr: 'D',                            
                            'RatingGroupCode': $scope.ratingArray[i].ratingGroupCode,
                            'RatingCode': $scope.ratingArray[i].ratingCode,
                            
                        });
                        deleteArray.push(deletemodulecode);
                   }
               }
            

                if ($scope.ratingArray) {
                swal({

                    title: '',
                    text: "Are you sure you want to Delete?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {

                        console.log($scope.Rating);

                        $http.post(ENV.apiUrl + "api/SurveyRating/CUDSurveyRatingSave", deleteArray).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.display = false;
                                        $scope.table = true;
                                        $scope.ratingArray.datastatus == false;
                                        $scope.surveyRating();
                                    }
                                });

                            }
                            else {
                                swal({ text: "Record Already Mapped.Can't be Deleted. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                            
                                    }
                                });
                            }

                        });
                        deleteArray = [];
                    }
                    else {
                        main = document.getElementById('mainchk');
                        if (main.checked == true) {
                            main.checked = false;
                        }

                        for (var i = 0; i < $scope.ratingArray.length; i++) {
                            var v = document.getElementById("test_" + i);
                            if (v.checked == true) {
                                v.checked = false;
                                $('tr').removeClass("row_selected");
                            }
                        }
                    }

                });
                $scope.getData();
            }
            else {
                swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
            }
        }
        

            $scope.getData = function ()
            {
                var a = { opr: 'S' };

                $http.post(ENV.apiUrl + "api/SurveyRating/RatingCommon", a).then(function (res) {
                    $scope.AllData = res.data.table;
                })
            }

            $scope.Cancel = function ()
            {
                $scope.display = false;
                $scope.table = true;
                $scope.Rating = "";
                $scope.ratingArray = [];
                $scope.surveyRating();
                
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.surveySettings, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.surveySettings;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_survey_rating_group_desc_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_survey_rating_group_code == toSearch) ? true : false;
            }

            $scope.ratingArray = [];

            $scope.AddRating = function () {
                debugger;
                var obj = [];

                if ($scope.Rating.ratingName == undefined) {
                    swal('','Please enter Rating Name');
                    return;
                }
                if($scope.Rating.RatingGroupCode==undefined)
                {
                    obj = {                    
                        ratingName: $scope.Rating.ratingName,
                        descriptionD: $scope.Rating.DescriptionD,
                        point: $scope.Rating.Point,
                        statusD: $scope.Rating.StatusD,
                        ratingGroupCode:rt_groupcode,
                    }
                }
                else{
                    obj = {                    
                        ratingName: $scope.Rating.ratingName,
                        descriptionD: $scope.Rating.DescriptionD,
                        point: $scope.Rating.Point,
                        statusD: $scope.Rating.StatusD,
                        ratingGroupCode: $scope.Rating.RatingGroupCode,
                    }
                }

                $scope.ratingArray.push(obj);
                $scope.Rating.ratingName = "";
                $scope.Rating.DescriptionD = "";
                $scope.Rating.Point = "";
                $scope.Rating.StatusD = "";
                
                console.log($scope.ratingArray);
            }
            
            $scope.Remove = function ($event, index, str) {
                str.splice(index, 1);
            }

            $scope.Delete = function () {
                debugger;
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById('checkboxD' + $scope.filteredTodos[i].sims_survey_rating_group_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({                            
                             opr: 'D',                            
                            'RatingGroupCode': $scope.filteredTodos[i].sims_survey_rating_group_code,
                            
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            debugger;
                            $http.post(ENV.apiUrl + "api/SurveyRating/CUDSurveyRatingSave", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.surveyRating();
                                            main = document.getElementById('checkboxR');
                                            if (main.checked == true) {
                                                main.checked = false;
                                            }

                                            $scope.CheckAllChecked();
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.surveyRating();
                                            main.checked = false;
                                            $('tr').removeClass("row_selected");
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                            });
                            deletefin = [];
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById('checkboxR' + $scope.filteredTodos[i].sims_survey_rating_group_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }
            $scope.surveyRating();

            //Image Upload Section//

            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };
            var file_name = '';
            var file_name_name = '';
            $scope.file_doc = [];

            $scope.file_changed = function (element) {
                debugger
                file_name = '';
                file_name_name = '';
                var img_load = '';
                $scope.flag = false;
                var rate_name, rate_point;
                
                for (var i = 0; i < $scope.ratingArray.length; i++) {                   
                    
                        var v = document.getElementById('mainchk' + $scope.ratingArray[i].ratingGroupCode + i);
                        if (v.checked == true) {
                            rate_name = $scope.ratingArray[i].ratingName;
                            rate_point = $scope.ratingArray[i].point;
                            $scope.flag = true;
                        }
                    
                }
                if ($scope.flag == false)
                {
                    swal('','Please select the record');
                    return;
                }
               

                var v = new Date();
                $scope.grid2 = true;
                if ($scope.file_doc == undefined) {
                    $scope.file_doc = '';
                }
                
                if ($scope.file_doc.length > 14) {
                    swal('', 'Upload maximum 15 files.');
                    return;
                } else {
                    var img_code = [];
                    var img_code = $scope.Rating.DescriptionG;
                    
                    file_name = 'Emoji' + '_' + $scope.Rating.DescriptionG + '_' + rate_name + '_' +rate_point;
                    $scope.photofile = element.files[0];
                    file_name_name = $scope.photofile.name;
                    
                    var len = 0;
                    len = file_name_name.split('.');
                    var fortype = file_name_name.split('.')[len.length - 1];
                    var formatbool = false;
                    if (fortype == 'jpg' || fortype == 'png' || fortype == 'jpeg') {
                        formatbool = true;
                    }

                    
                    if (formatbool == true) {
                        if ($scope.photofile.size > 2097152) {
                            swal('', 'File size limit not exceed upto 2 MB.')
                        } else {
                            $scope.uploading_doc1 = false;
                            $scope.photo_filename = ($scope.photofile.type);


                            var reader = new FileReader();
                            reader.onload = function (e) {
                                $scope.$apply(function () {

                                    $scope.prev_img = e.target.result;
                                    $('#' + ind).attr('src', $scope.prev_img)
                                    
                                    var request = {
                                        method: 'POST',
                                        url: ENV.apiUrl + 'api/SurveyRating/upload?filename=' + file_name + '.' + fortype + "&location=" + "Images/Emojis",
                                        data: formdata,
                                        headers: {
                                            'Content-Type': undefined
                                        }
                                    };
                                    $http(request).success(function (d) {


                                        var t = {
                                            sims508_doc: d,
                                            //sims_doc_line: sims_doc_line,
                                            sims_img_path: file_name,
                                        }
                                        $scope.file_doc.push(t);                                     
                                        
                                        for (var i = 0; i < $scope.file_doc.length; i++) {
                                            img_load = $scope.file_doc[i].sims508_doc;
                                        }
                                        for (var i = 0; i < $scope.ratingArray.length; i++) {
                                            var v = document.getElementById('mainchk' + $scope.ratingArray[i].ratingGroupCode + i);
                                            if (img_load != undefined || img_load=='') {
                                                if (v.checked == true) {
                                                    $scope.ratingArray[i].sims_survey_rating_img_path = img_load;
                                                    v.checked = false;
                                                    $scope.row1 = '';
                                                    $('tr').removeClass("row_selected");
                                                }
                                            }
                                        }
                                        v.checked = false;
                                        $scope.row1 = '';
                                        $('tr').removeClass("row_selected");
                                    
                                        //main.checked = false;
                                        //$scope.row1 = '';
                                        //$('tr').removeClass("row_selected");
                                        $scope.uploading_doc1 = true;

                                    });
                                    
                                });
                            };
                            reader.readAsDataURL($scope.photofile);
                        }
                    } else {
                        swal('', '.' + fortype + ' File format not allowed.');
                    }
                }
                //$scope.ratingArray.sims_survey_rating_img_path = img_load;
            }
                        
            $scope.CancelFileUpload = function (idx) {
                $scope.images.splice(idx, 1);

            };

         }]);

    })();