﻿(function () {
    'use strict';

    var opr = '';
    var routecode = [];
    var stopcode = [];
    var routestopcode = [];
    var restore = [];
    var main;
    var mainv;
    var maind;
    var check;
    var simsController = angular.module('sims.module.Student');

    simsController.controller('AdmissionPromoteRemarkCont', ['$scope', '$state', '$rootScope', '$filter', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $filter, $timeout, gettextCatalog, $http, ENV) {

        $scope.pagesize = '10';
        $scope.IP = {};

        $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

        $scope.makeTodos = function () {
            var rem = parseInt($scope.totalItems % $scope.numPerPage);
            if (rem == '0') {
                $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
            }
            else {
                $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
            }
            var begin = (($scope.currentPage - 1) * $scope.numPerPage);
            var end = parseInt(begin) + parseInt($scope.numPerPage);


            $scope.filteredTodos = $scope.todos.slice(begin, end);
            for (var i = 0; i < $scope.filteredTodos.length; i++) {

                $scope.filteredTodos[i].icon = 'fa fa-plus-circle';
            }
        };

        setTimeout(function () {
            $("#Select17,#Select19,#cmb_bus,#Select6").select2();
        }, 100);

        $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
            $scope.curriculum = AllCurr.data;
            $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
            $scope.getAccYear();
        });

        $scope.getAccYear = function (curCode) {
            $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.edt.sims_cur_code).then(function (Acyear) {
                $scope.Acc_year = Acyear.data;
                $scope.edt = {
                    sims_cur_code: $scope.edt.sims_cur_code,//$scope.curriculum[0].sims_cur_code,
                    sims_academic_year: $scope.Acc_year[0].sims_academic_year
                }
                $scope.getGrade();
            });
        }

        $scope.getGrade = function (curCode, accYear) {
            $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Gradecode) {
                $scope.Grade_code = Gradecode.data;
            });
        }

        $scope.getSection = function (curCode, gradeCode, accYear) {
            $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Sectioncode) {
                $scope.Section_code = Sectioncode.data;
            });
        }

        $http.get(ENV.apiUrl + "api/StudentReport/getRegistrationStatus").then(function (res) {
            $scope.Resstatus = res.data;
        });

        $http.get(ENV.apiUrl + "api/StudentReport/getFeedescStatus").then(function (res1) {
            $scope.Feestatus = res1.data;
        });

        $scope.Reset = function () {

            $scope.edt = {
                sims_cur_code: "",
                sims_academic_year: "",
                sims_grade_code: "",
                sims_section_code: "",
                sims_enroll_number: '',

            }
            $("#cmb_bus").select2("val", "");
            $scope.searchtable = false;
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear();
            });
        }

        $scope.Show_Data = function () {

            $scope.IP.cur_shrt_name = $scope.edt.sims_cur_code;
            $scope.IP.sims_academic_year = $scope.edt.sims_academic_year;
            $scope.IP.sims_grade_code = $scope.edt.sims_grade_code;
            $scope.IP.sims_section_code = $scope.edt.sims_section_code;
            $scope.IP.sims_student_enroll_number = $scope.edt.sims_enroll_number;

            $http.get(ENV.apiUrl + "api/StudentReport/getAdmRemarkStudList?data=" + JSON.stringify($scope.IP)).then(function (res) {
                $scope.StudentDetails = res.data.table;
                $scope.totalItems = $scope.StudentDetails.length;
                $scope.todos = $scope.StudentDetails;
                $scope.makeTodos();
                if ($scope.StudentDetails.length > 0)
                    $scope.searchtable = true;
                else
                    $scope.searchtable = false;
            });
        }

        $scope.savedata = function () {
            debugger;
            var data1 = [];
            var data = $scope.filteredTodos.length;
            for (var i = 0; i < data; i++) {
                var data2 = {
                    cur_shrt_name: $scope.edt.sims_cur_code,
                    sims_academic_year: $scope.edt.sims_academic_year,
                    sims_grade_code: $scope.edt.sims_grade_code,
                    sims_section_code: $scope.edt.sims_section_code,
                    sims_student_enroll_number: $scope.filteredTodos[i].sims_enroll_number,
                    sims_student_attribute1: $scope.filteredTodos[i].sims_promote_status,
                    sims_student_attribute3: $scope.filteredTodos[i].sims_fee_paid,
                    sims_student_attribute5: $scope.filteredTodos[i].sims_promote_remark
                };
            data1.push(data2);
            }

            $http.post(ENV.apiUrl + "api/StudentReport/CUDAdmissionstudentpro", data1).then(function (res2) {
                $scope.msg1 = res2.data;
                debugger;
                if ($scope.msg1 == true) {

                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                }
                else {
                    swal({ text: "Record already present. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                }
                
            });
        }

        $scope.size = function (str) {
            if (str == "All" || str == "all") {
                $scope.currentPage = 1;
                $scope.numPerPage = $scope.StudentDetails.length;
                $scope.makeTodos();

            }
            else {
                $scope.pager = true;
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }
        }

        $scope.index = function (str) {
            $scope.pageindex = str;
            $scope.currentPage = str;
            $scope.makeTodos();

            main = document.getElementById('mainchk');
            if (main.checked == true) {
                main.checked = false;
                $scope.row1 = '';
                $scope.color = '#edefef';
            }
        }

        $scope.searched = function (valLists, toSearch) {
            return _.filter(valLists,
            function (i) {
                /* Search Text in all  fields */
                return searchUtil(i, toSearch);
            });
        };

        $scope.search = function () {
            $scope.todos = $scope.searched($scope.StudentDetails, $scope.searchText);
            $scope.totalItems = $scope.todos.length;
            $scope.currentPage = '1';
            if ($scope.searchText == '') {
                $scope.todos = $scope.StudentDetails;
            }
            $scope.makeTodos();
        }

        function searchUtil(item, toSearch) {
            /* Search Text in all 3 fields */

            return (
                 item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                 item.sims_student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                 item.sims_academic_year_description == toSearch) ? true : false;
        }

        $timeout(function () {
            $("#fixTable").tableHeadFixer({ 'top': 1 });
        }, 100);

        $timeout(function () {
            $("#fixTable1").tableHeadFixer({ 'top': 1 });
        }, 100);

     }]);
})();