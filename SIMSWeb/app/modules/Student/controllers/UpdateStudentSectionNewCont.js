﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('UpdateStudentSectionNewController',
      ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

          $scope.edt = {};
          $scope.classwise = false;
          $scope.showDDFlagForNISS = false;



          //$http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
          //    $scope.display = true;
          //    $scope.CurNames_obj = res.data;// console.log($scope.CurNames_obj);
          //    $scope.edt['sims_cur_code'] = $scope.CurNames_obj[0].sims_cur_code;
          //    $scope.getAcdmYr($scope.CurNames_obj[0].sims_cur_code);

          //});


          function getCur(flag, comp_code) {
              if (flag) {

                  $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                      $scope.CurNames_obj = res.data;// console.log($scope.CurNames_obj);
                      $scope.edt['sims_cur_code'] = $scope.CurNames_obj[0].sims_cur_code;
                      $scope.getAcdmYr($scope.CurNames_obj[0].sims_cur_code);

                  });
              }

              else {

                  $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                      $scope.CurNames_obj = res.data;// console.log($scope.CurNames_obj);
                      $scope.edt['sims_cur_code'] = $scope.CurNames_obj[0].sims_cur_code;
                      $scope.getAcdmYr($scope.CurNames_obj[0].sims_cur_code);
                  });
              }

          }

          $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
              $scope.global_count_comp = res.data;

              if ($scope.global_count_comp) {
                  getCur(true, $scope.user_details.comp);


              }
              else {
                  getCur(false, $scope.user_details.comp)
              }
          });



          $scope.propertyName = null;
          $scope.reverse = false;

          $scope.sortBy = function (propertyName) {
              $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
              $scope.propertyName = propertyName;
          };

          if ($http.defaults.headers.common['schoolId'] == 'sms') {
              $scope.showDDFlagForNISS = true;
          }

          $scope.getAcdmYr = function (curcode) {
              $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curcode).then(function (res) {
                  $scope.Academic_yr_obj = res.data; //console.log($scope.Academic_yr_obj);
                  $scope.edt['sims_academic_year'] = $scope.Academic_yr_obj[0].sims_academic_year;
                  $scope.getSectionDetails(curcode, $scope.Academic_yr_obj[0].sims_academic_year);

              });
          }


          $scope.getSectionDetails = function (curname, acdmyr) {

              $http.get(ENV.apiUrl + "api/student/UpdateStudentSection/getSectionDetails?curName=" + curname + "&Acdmyr=" + acdmyr).then(function (res) {
                  $scope.SectionDetails_obj = res.data;
                  $scope.student_table = false;

              });
          }

          $scope.student_table = false;

          $scope.getStudentDetails = function (str) {

              $scope.busy1 = true;
              var data = str;
              $scope.temp = data;
              $http.get(ENV.apiUrl + "api/student/UpdateStudentSection/getStudentDetailsSectionWise?jsonobj=" + JSON.stringify(data)).then(function (res) {
                  $scope.StudentDetails_obj = res.data;
                  $scope.student_table = true;
                  $scope.busy1 = false;
                  //  console.log($scope.StudentDetails_obj);
              });
          }

          $scope.getallsectioncode = function (gr_code) {
              $http.get(ENV.apiUrl + "api/common/GlobalSearch/getAllSectionName?gradecode=" + gr_code).then(function (res) {
                  $scope.All_Section_names = res.data;
              });
          }

          // For NISS
          $scope.getsectionForNIS = function (gr_code) {
              $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + gr_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (res) {
                  $scope.All_Section_names = res.data;
              });
          }

          debugger;
          $scope.getformatedate = function (dt) {
              if (dt != null) {
                  //var d1 = new Date(dt);
                  //var month = d1.getMonth() + 1;
                  //var day = d1.getDate();
                  //if (month < 10)
                  //    month = "0" + month;
                  //if (day < 10)
                  //    day = "0" + day;
                  //var d = month + "/" + (day) + "/" + d1.getFullYear();
                  var d = dt;
                  console.log(d);
                  return d;
              }
          }

          $scope.showStudDetails = function (info) {

              $scope.temp['sims_student_enroll_number'] = info.sims_student_enroll_number;

              //$http.get(ENV.apiUrl + "api/student/UpdateStudentSection/getSectionFeecheck?jsonobj=" + JSON.stringify($scope.temp)).then(function (res) {
              //    $scope.MSG = res.data;

              //    if ($scope.MSG == true) {
              //        swal({ text: 'Attendance is pending for this student.Please update first..', width: 380, showCloseButton: true });
              //    }
              //else {
              var savedata = $scope.temp;
              savedata.student_name = info.student_name;
              savedata.sims_student_enroll_number = info.sims_student_enroll_number;
              savedata.classname = $scope.temp.sims_grade_name_en + " " + $scope.temp.sims_section_name_en;
              $scope.stud_obj = savedata;
              $scope.readonly = true;
              $scope.stud_obj['sims_grade_code_new'] = $scope.temp.sims_grade_code;
              $('#myModal').modal({ backdrop: "static" });

              $http.get(ENV.apiUrl + "api/common/getAllGradesFeePost?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year).then(function (res) {
                  $scope.grade_names_obj = res.data;
                  $http.get(ENV.apiUrl + "api/student/UpdateStudentSection/getSectionFee?jsonobj=" + JSON.stringify($scope.temp)).then(function (res) {
                      $scope.section_fee_obj = res.data;
                      //$scope.getallsectioncode($scope.temp.sims_grade_code);
                      $scope.getsectionForNIS($scope.temp.sims_grade_code);
                  });
              });

              //if ($scope.showDDFlagForNISS == true) {
              //    $http.get(ENV.apiUrl + "api/common/getAllGradesFeePost?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year).then(function (res) {
              //        $scope.grade_names_obj = res.data;
              //        $http.get(ENV.apiUrl + "api/student/UpdateStudentSection/getSectionFee?jsonobj=" + JSON.stringify($scope.temp)).then(function (res) {
              //            $scope.section_fee_obj = res.data;
              //            //$scope.getallsectioncode($scope.temp.sims_grade_code);
              //            $scope.getsectionForNIS($scope.temp.sims_grade_code);
              //        });
              //    })
              //}
              //else {
              //    $http.get(ENV.apiUrl + "api/common/GlobalSearch/getAllGradeName").then(function (res) {
              //        $scope.grade_names_obj = res.data;
              //        $http.get(ENV.apiUrl + "api/student/UpdateStudentSection/getSectionFee?jsonobj=" + JSON.stringify($scope.temp)).then(function (res) {
              //            $scope.section_fee_obj = res.data;
              //            $scope.getallsectioncode($scope.temp.sims_grade_code);
              //        });
              //    })
              //}

              //    }
              //});
          }

          $scope.SaveSectionDetails = function (isvalid) {
              var da = [];
              if (isvalid) {
                  var feelist = '';
                  for (var i = 0; i < $scope.section_fee_obj.length; i++) {
                      if ($scope.section_fee_obj[i].ischecked == true) {
                          feelist = feelist + $scope.section_fee_obj[i].sims_fee_code + ',';
                      }
                  }

                  $scope.stud_obj['fee_code_list'] = feelist;
                  da.push($scope.stud_obj);



                  $('#Div3').modal('show');

                  $http.post(ENV.apiUrl + "api/student/UpdateStudentSection/StudentSectionUpdateNew", da).then(function (res) {
                      $scope.result = res.data;

                      console.log($scope.result);
                      if ($scope.result == true) {
                          $('#myModal').modal('hide');
                          $('#Div3').modal('hide');
                          swal({ text: "Section Updated Successfully", imageUrl: "assets/img/check.png", width: 300, showCloseButton: true });
                          $scope.getSectionDetails($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                      }
                      else if ($scope.result == false) {
                          $('#myModal').modal('hide');
                          $('#Div3').modal('hide');
                          swal({ text: "Section Not Updated. ", imageUrl: "assets/img/close.png", width: 300, showCloseButton: true });
                          $scope.getSectionDetails($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);

                      }
                      else {
                          swal("Error-" + $scope.result)
                      }
                  });
              }
          }

          $timeout(function () {
              $("#Table2").tableHeadFixer({ 'top': 1 });
          }, 100);

          $timeout(function () {
              $("#fixTable").tableHeadFixer({ 'top': 1 });
              $("#example").tableHeadFixer({ 'top': 1 });
          }, 100);

          $timeout(function () {
              $("#fixTable").tableHeadFixer({ 'top': 1 });
          }, 100);
          $timeout(function () {
              $("#Table1").tableHeadFixer({ 'top': 1 });
          }, 100);

          $('*[data-datepicker="true"] input[type="text"]').datepicker({
              todayBtn: true,
              orientation: "top left",
              autoclose: true,
              todayHighlight: true,
              format: "mm/dd/yyyy"
          });

          $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
              $('input[type="text"]', $(this).parent()).focus();
          });

          $scope.ClasswiseStudentshow = function (info) {
              $scope.classwise = true;
              $scope.sectiontable = false;
              $scope.getsections();
          }

          $scope.getsections = function () {
              $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (res) {
                  $scope.grade = res.data;
                  $scope.edt['sims_grade_code'] = $scope.grade[0].sims_grade_code;
                  $scope.getsection();
              })
          };

          $scope.getsection = function () {

              $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Allsection) {
                  $scope.section1 = Allsection.data;
              })
          };

          $scope.BacktoMainForm = function () {
              $scope.Studentdata = [];
              $scope.classwise = false;

          }

          $scope.ShowClassWiseStudent = function () {

              $scope.sectiontable = false;
              $scope.busy = true;
              if ($scope.edt.sims_grade_code != undefined) {
                  $http.post(ENV.apiUrl + "api/student/UpdateStudentSection/classwiseStudentSectionUpdate", $scope.edt).then(function (Studentdata) {
                      $scope.Studentdata1 = Studentdata.data;
                      if ($scope.Studentdata1.length > 0) {
                          $scope.Studentdata = $scope.Studentdata1;
                          $scope.sectiontable = true;
                          $scope.busy = false;
                          $scope.tablehead = $scope.Studentdata[0].section_list;
                          $timeout(function () {
                              $("#example").tableHeadFixer({ 'top': 1 });
                          }, 100);
                      }
                      else {
                          $scope.busy = false;
                          swal({ text: 'Student Not Found', width: 320, showCloseButton: true });
                      }
                  });
              }
              else {
                  $scope.busy = false;
                  swal({ text: 'Select Grade', width: 270, showCloseButton: true });
              }
          }

          $scope.Select_One_Student_For_One_Section = function (enroll_number, section_code, info, info1) {
              for (var j = 0; j < info1.section_list.length; j++) {
                  if (info1.section_list[j].sims_section_code_new == info.sims_section_code_new) {
                      info1.section_list[j].section_status = true;
                      info1.section_list[j].ischange = true;
                  }
                  else {
                      info1.section_list[j].section_status = false;
                  }
              }
          }

          $scope.multipleSaveSectionDetails = function (isvalid) {
              debugger;
              var sectiondataobject = [];
              if (isvalid) {

                  for (var i = 0; i < $scope.Studentdata.length; i++) {
                      for (var j = 0; j < $scope.Studentdata[i].section_list.length; j++) {
                          if ($scope.Studentdata[i].section_list[j].ischange == true && $scope.Studentdata[i].section_list[j].section_status == true) {
                              var data = {};
                              data.sims_academic_year = $scope.Studentdata[i].section_list[j].sims_academic_year;
                              data.sims_cur_code = $scope.Studentdata[i].section_list[j].sims_cur_code;
                              data.sims_section_code_new = $scope.Studentdata[i].section_list[j].sims_section_code_new;
                              data.sims_grade_code_new = $scope.Studentdata[i].section_list[j].sims_grade_code_new;
                              data.sims_student_enroll_number = $scope.Studentdata[i].sims_enroll_number
                              data.effect_from_date = $scope.edt.effect_from_date
                              data.fee_code_list = '';
                              sectiondataobject.push(data);
                          }
                      }
                  }
                  if (sectiondataobject.length > 0) {
                      $scope.classwise = false;
                      $('#Div3').modal({ backdrop: "static" });

                      $http.post(ENV.apiUrl + "api/student/UpdateStudentSection/StudentSectionUpdateNew", sectiondataobject).then(function (res) {
                          $scope.result = res.data;
                          $('#Div3').modal('hide');
                          if ($scope.result == true) {
                              swal({ text: "Section Updated Successfully", imageUrl: "assets/img/check.png", width: 300, showCloseButton: true });
                              $scope.sectiontable = false;
                              $scope.getSectionDetails($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);


                          }
                          else if ($scope.result == false) {
                              $scope.sectiontable = false;
                              swal({ text: "Section Not Updated. ", imageUrl: "assets/img/close.png", width: 300, showCloseButton: true });
                              $scope.getSectionDetails($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                          }
                          else {
                              swal("Error-" + $scope.result)
                          }
                      });
                  }
              }
              else {

                  swal({ text: 'Please Change Atleast One Section To Update', width: 350, showCloseButton: true });
              }
          }

       }]);
})();