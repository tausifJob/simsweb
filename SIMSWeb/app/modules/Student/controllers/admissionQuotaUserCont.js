﻿(function () {
    'use strict';
    var obj1, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('admissionQuotaUserCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.temp = {};
            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            $scope.notshow = false;

            //Get Curriculam and aca year
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (response) {
                debugger;
                $scope.curiculums = response.data;
                // $scope.temp['sims_cur_code'] = $scope.curiculums[0].sims_cur_code;
                // console.log($scope.temp.sims_cur_code);
                $scope.getAcaYear($scope.curiculums[0].sims_cur_code);
            });

            $scope.getAcaYear = function (cur_code) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code).then(function (response) {
                    $scope.acaYears = response.data;
                    $scope.getGradeByCurCode(cur_code, $scope.acaYears[0].sims_academic_year);
                    $scope.getAdminQuotaByCurCode(cur_code, $scope.acaYears[0].sims_academic_year);
                });
            }

            $scope.getGradeByCurCode = function (cur_code, academic) {
                var param = {};
                param.sims_cur_code = cur_code;
                param.sims_academic_year = academic;
                $http.post(ENV.apiUrl + "api/admissionQuotaUser/AllGrades_p", param).then(function (response) {
                    $scope.grades = response.data;
                });
                //$http.get(ENV.apiUrl + "api/common/getAllGradesCommon?cur_code=" + cur_code + "&academic=" + academic).then(function (response) {
                //    $scope.grades = response.data;                    
                //});
                //$scope.getAdminQuotaByCurCode(cur_code, academic);
            }

            $scope.getAdminQuotaByCurCode = function (cur_code, academic) {
                $http.get(ENV.apiUrl + "api/admissionQuotaUser/getAdmissionQuotaFromCur?cur_code=" + cur_code + "&ac_year=" + academic).then(function (response) {
                    $scope.admissionQuotaMaster = response.data;
                });

            }

            $scope.showErr = false;

            $scope.getQuotaStength = function (str) {
                $scope.stregth = str.sims_q_strength;
                $scope.temp.sims_q_strength = str.sims_q_strength;
            }


            $scope.textchange = function (str) {
                if (parseInt(str) > parseInt($scope.stregth)) {
                    $scope.showErr = true;
                }
                else {
                    $scope.showErr = false;
                }
            }

            $scope.newAddForm = function () {

                $scope.temp = {};
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (response) {
                    debugger;
                    $scope.curiculums = response.data;
                    $scope.temp['sims_cur_code'] = $scope.curiculums[0].sims_cur_code;
                    $scope.getAcaYear($scope.curiculums[0].sims_cur_code);
                });

                $scope.temp.admQuotaStatus = true;
                $scope.showAddForm = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.userNameReadOnly = false;
                $scope.curiculumaReadonly = false;
                $scope.acaYearReadonly = false;
                $scope.gradeReadonly = false;
                $scope.quotaReadonly = false;
                $scope.showEditQDesc = false;
              
                $scope.temp = {};
                $scope.temp.admQuotaStatus = true;
            
                if ($scope.curiculums.length > 0) {
                    $scope.temp.sims_cur_code = $scope.curiculums[0].sims_cur_code;
                    $scope.getAcaYear($scope.temp.sims_cur_code);

            }

                if ($scope.acaYears.length > 0) {
                    $scope.temp.sims_academic_year = $scope.acaYears[0].sims_academic_year;
                   // $scope.year($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                }
            }

            $scope.cancelAddForm = function () {
                $scope.temp = {};
                $scope.showAddForm = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.getAdmissionUserQuota = function () {
                $http.get(ENV.apiUrl + "api/admissionQuotaUser/getAllAdmissionQuotaUser").then(function (response) {
                    $scope.admissionUserQuota = response.data;
                    $scope.totalItems = $scope.admissionUserQuota.length;
                    $scope.todos = $scope.admissionUserQuota;
                    $scope.makeTodos();
                });
            };

            $scope.getAdmissionUserQuota();

            //$scope.size = function (str) {
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str;
            //    $scope.makeTodos();
            //}

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.admissionUserQuota;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }


            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                0
                $scope.todos = $scope.searched($scope.admissionUserQuota, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.admissionUserQuota;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_q_academic_year.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_q_user_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.sims_q_status.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_grade_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_q_strength == toSearch) ? true : false;
            }

            //DATA SAVE INSERT
            var data = "";
            $scope.saveAdmissionData = function (Myform) {

                if (Myform) {
                    var data = $scope.temp;
                    data.sims_q_cur_code = $scope.temp.sims_cur_code;
                    data.sims_q_academic_year = $scope.temp.sims_academic_year;
                    data.sims_q_grade_code = $scope.temp.sims_grade_code;
                    data.sims_quota_id = $scope.quota.sims_quota_id;
                    if (data.admQuotaStatus == true) {
                        data.sims_q_status = "true";
                    }
                    else {
                        data.sims_q_status = "false";
                    }

                    $http.post(ENV.apiUrl + "api/admissionQuotaUser/addAdmissionQuotaForUser", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if (msg.status == 200) {
                            $scope.showAddForm = false;
                        }
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();

                        swal({ text: $scope.msg1.strMessage, timer: 5000, width: 320 });
                        $scope.getAdmissionUserQuota();
                    });
                    data = "";
                    $scope.Myform.$setUntouched();
                }

            }

            //DATA EDIT
            $scope.editAdmmissionUser = function (obj) {
                $scope.getAcaYear(obj.sims_q_cur_code);
                $scope.getGradeByCurCode(obj.sims_q_cur_code, obj.sims_q_academic_year)
                $scope.getAdminQuotaByCurCode(obj.sims_q_cur_code, obj.sims_q_academic_year);
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.showAddForm = true;
                $scope.userNameReadOnly = true;
                $scope.curiculumaReadonly = true;
                $scope.acaYearReadonly = true;
                $scope.gradeReadonly = true;
                $scope.quotaReadonly = true;
                $scope.showEditQDesc = true;

                $scope.quotaDesc = obj.sims_quota_desc;
                $scope.quotaId = obj.sims_quota_id;
                $scope.stregth = obj.mq_strength;

                $scope.temp = {
                    sims_cur_code: obj.sims_q_cur_code,
                    sims_academic_year: obj.sims_q_academic_year,
                    sims_grade_code: obj.sims_q_grade_code,
                    sims_q_user_name: obj.sims_q_user_name,
                    sims_q_strength: obj.sims_q_strength,
                };

                if (obj.sims_q_status == "A") {
                    $scope.temp.admQuotaStatus = true;
                }
                else {
                    $scope.temp.admQuotaStatus = false;
                }
            }
            $scope.updateAdmissionData = function (editform) {


                if (editform) {
                    var data = $scope.temp;

                    if (data.admQuotaStatus == true) {
                        $scope.status = "true";
                    }
                    else {
                        $scope.status = "false";
                    }



                    data = {
                        'sims_q_cur_code': $scope.temp.sims_cur_code, 'sims_q_academic_year': $scope.temp.sims_academic_year, 'sims_q_grade_code': $scope.temp.sims_grade_code, 'sims_quota_id': $scope.quotaId,
                        'sims_q_user_name': $scope.temp.sims_q_user_name, 'sims_q_strength': parseInt($scope.temp.sims_q_strength), 'sims_q_status': $scope.status
                    }


                    $http.post(ENV.apiUrl + "api/admissionQuotaUser/updateAdmissionQuotaForUser", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if (msg.status == 200) {
                            $scope.showAddForm = false;
                        }
                        swal({ text: $scope.msg1.strMessage, timer: 5000 ,width:320});
                        $scope.getAdmissionUserQuota();
                    });
                    data = "";
                }

            }

            //GS
            $scope.globalSearch = function () {
                debugger;
                $rootScope.visible_stud = false;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = true;
                $rootScope.chkMulti = false;
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                //$scope.temp.em_login_code = '';
                //$scope.temp.EmpName = '';
                $scope.showBtn = false;
            }

            $scope.$on('global_cancel', function (str) {

                if ($scope.SelectedUserLst.length > 0) {

                   // $scope.temp = {};
                    $scope.temp['enroll_number'] = $scope.SelectedUserLst[0].em_login_code;
                    $scope.temp['name'] = $scope.SelectedUserLst[0].empName;
                    $scope.temp.sims_q_user_name = $scope.SelectedUserLst[0].em_login_code;
                }


                // $scope.getstudentList();
            });

            $scope.searchGlobalClick = function () {
                $scope.Global_Search_by_employee();
            }
         }])

})();
