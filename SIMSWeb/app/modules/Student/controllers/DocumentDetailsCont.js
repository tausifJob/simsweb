﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('DocumentDetailsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            $http.get(ENV.apiUrl + "api/common/Designation/getCompanyName").then(function (res1) {
                $scope.display = false;
                $scope.table = true;
                $scope.comp = res1.data;
                console.log($scope.comp);
            });
            debugger;
            $scope.temp = {};
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                if (AllCurr.data.length > 0) {
                    $scope.temp.sims_cur_code = AllCurr.data[0].sims_cur_code;
                }
                console.log($scope.curriculum);
            });

            $http.get(ENV.apiUrl + "api/DocumentDetail/getModuleName").then(function (modname) {
                $scope.module = modname.data;
                console.log($scope.module);
            });

            $scope.getDocumentN = function (modcode) {
                $http.get(ENV.apiUrl + "api/DocumentDetail/getDocumentName?modcode=" + modcode).then(function (docname) {
                    $scope.documentdetails = docname.data;
                    console.log($scope.documentdetails);
                });
            }

            $http.get(ENV.apiUrl + "api/DocumentDetail/getAllDocument").then(function (res1) {
                $scope.obj1 = res1.data;

                $scope.totalItems = $scope.obj1.length;

                $scope.todos = $scope.obj1;

                $scope.makeTodos();
            });

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.obj1;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj1, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj1;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_doc_mod_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_doc_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_doc_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_doc_srno == toSearch) ? true : false;
            } 

            $scope.New = function () {

                $scope.disabled = false;
                $scope.readonly = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
               $scope.temp = {};
                $scope.temp.sims_doc_status = true;
                debugger;
               
                 if ($scope.curriculum.length > 0) {
                    $scope.temp.sims_cur_code = $scope.curriculum[0].sims_cur_code;
                }

                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            var datasend = [];
            $scope.savedata = function (Myform) {

                if (Myform) {
                    var data = {
                        sims_cur_code: $scope.temp.sims_cur_code
                   , sims_doc_mod_code: $scope.temp.sims_doc_mod_code
                   , sims_doc_code: $scope.temp.sims_doc_code
                   , sims_doc_srno: $scope.temp.sims_doc_srno
                   , sims_doc_desc: $scope.temp.sims_doc_desc
                   , sims_doc_status: $scope.temp.sims_doc_status
                   , opr: 'I'
                    };
                    datasend.push(data);

                    $http.post(ENV.apiUrl + "api/DocumentDetail/CUDDocumentDetails", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        $http.get(ENV.apiUrl + "api/DocumentDetail/getAllDocument").then(function (res1) {
                            $scope.obj1 = res1.data;
                            $scope.totalItems = $scope.obj1.length;
                            $scope.todos = $scope.obj1;
                            $scope.makeTodos();
                        });

                    })
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.fins_comp_code = "";
                $scope.temp.fins_application_code = "";
                $scope.temp.fins_appl_form_field = "";
                $scope.temp.fins_appl_parameter = "";
                $scope.temp.fins_appl_form_field_value1 = "";
                $scope.temp.fins_appl_form_field_value2 = "";
                $scope.temp.fins_appl_form_field_value3 = "";
                $scope.temp.fins_appl_form_field_value4 = "";
            }

            $scope.edit = function (str) {
                debugger;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.getDocumentN(str.sims_doc_mod_code);

                $scope.temp = {
                    sims_cur_code: str.sims_doc_cur_code
                   , sims_doc_mod_code: str.sims_doc_mod_code
                   , sims_doc_code: str.sims_doc_code
                      , sims_doc_name: str.sims_doc_name
                   , sims_doc_srno: str.sims_doc_srno
                   , sims_doc_desc: str.sims_doc_desc
                   , sims_doc_status: str.sims_doc_status
                };

            }

            var dataupdate = [];
            $scope.update = function () {
                dataupdate = [];

                var data = {
                    sims_cur_code: $scope.temp.sims_cur_code
                   , sims_doc_mod_code: $scope.temp.sims_doc_mod_code
                   , sims_doc_code: $scope.temp.sims_doc_code
                   , sims_doc_srno: $scope.temp.sims_doc_srno
                   , sims_doc_desc: $scope.temp.sims_doc_desc
                   , sims_doc_status: $scope.temp.sims_doc_status
                   , opr: 'U'
                };
                dataupdate.push(data);

                $http.post(ENV.apiUrl + "api/DocumentDetail/CUDDocumentDetails", dataupdate).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                    }
                    else
                    {
                        swal("Error-" + $scope.msg1)
                    }

                    $http.get(ENV.apiUrl + "api/DocumentDetail/getAllDocument").then(function (res1) {
                        $scope.obj1 = res1.data;
                        $scope.totalItems = $scope.obj1.length;
                        $scope.todos = $scope.obj1;
                        $scope.makeTodos();
                    });
                });
                dataupdate = [];
                $scope.table = true;
                $scope.display = false;
            }


            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_doc_srno + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_doc_srno + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.OkDelete = function () {
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_doc_srno + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_doc_srno': $scope.filteredTodos[i].sims_doc_srno,
                            'opr': "D"
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/DocumentDetail/CUDDocumentDetails", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/DocumentDetail/getAllDocument").then(function (res1) {
                                                $scope.obj1 = res1.data;
                                                $scope.totalItems = $scope.obj1.length;
                                                $scope.todos = $scope.obj1;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/DocumentDetail/getAllDocument").then(function (res1) {
                                                $scope.obj1 = res1.data;
                                                $scope.totalItems = $scope.obj1.length;
                                                $scope.todos = $scope.obj1;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_doc_srno + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }

         }])

})();
