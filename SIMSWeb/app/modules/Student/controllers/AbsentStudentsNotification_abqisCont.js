﻿
(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, modulecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AbsentStudentsNotification_abqisCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.table = false;
            $scope.table1 = false;
            $scope.pagesize = "All";
            $scope.pageindex = 0;
            console.clear();
            $timeout(function () {
                $("#example").tableHeadFixer({ 'top': 1 });
            }, 100);


            var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');
            $scope.dt = {
                sims_from_date: dateyear,
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd-mm-yyyy'
            });

            $scope.filteredTodos = [], $scope.currentPage = 1; //$scope.numPerPage = 20, $scope.maxSize = 20;

            $scope.size = function (str) {
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }

                if (str == "All") {
                    $scope.currentPage = 1;
                    $scope.numPerPage = $scope.studlist.length;
                    $scope.makeTodos();

                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                //$scope.pageindex = str;
                //$scope.currentPage = str;
                //console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
            }

            $scope.makeTodos = function () {

                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.getcur = function () {
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                    $scope.curriculum = AllCurr.data;
                    $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                    $scope.getAccYear($scope.edt.sims_cur_code);
                    $scope.getARule($scope.edt.sims_cur_code);
                });
            }

            $scope.getcur();

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                    $scope.getGrade();
                });
            }

            $scope.getARule = function (curCode) {
                $http.get(ENV.apiUrl + "api/StudentReport_abqisController/getAttendanceRule?curCode=" + curCode).then(function (attrule) {
                    $scope.arule = attrule.data;
                    $scope.edt = {
                        sims_attendance_code: $scope.arule[0].sims_attendance_code,
                        sims_cur_code: $scope.edt.sims_cur_code
                    };
                });
            }

            $scope.getGrade = function () {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }

            $(function () {
                $('#cmb_grade_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }

            $(function () {
                $('#cmb_section_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.Show_Data = function (str, str1) {
                debugger;
                if ((str != undefined && str1 != undefined) || $scope.edt.sims_cur_code != "" && $scope.temp.sims_academic_year != "") {
                    $http.get(ENV.apiUrl + "api/StudentReport_abqisController/getAbsentStudentList?cur_name=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grd_code=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&student_en=" + $scope.edt.sims_student_enroll_number + "&select_date=" + $scope.dt.sims_from_date + "&att_code=" + $scope.edt.sims_attendance_code).then(function (res) {
                        $scope.studlist = res.data;
                        console.log("studlist", $scope.studlist);
                        $scope.numPerPage = $scope.studlist.length
                        $scope.maxSize = $scope.studlist.length;
                        $scope.totalItems = $scope.studlist.length;
                        $scope.todos = $scope.studlist;
                        $scope.makeTodos();
                        if ($scope.studlist.length == 0) {
                            swal({ text: "Record Not Found...", imageUrl: "assets/img/close.png", showCloseButton: true, width: 450, height: 200 });
                        }
                        else {
                            $scope.table = true;
                            $scope.table1 = true;
                        }
                    })
                }

                else {
                    swal({ title: 'Alert', text: "Please Select Curriculum & Academic Year Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {

                $scope.todos = $scope.searched($scope.studlist, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.studlist;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                return (item.student_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_attendance_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_section_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sagar == toSearch) ? true : false;
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyoneselect = function () {

                $("input[type='checkbox']").change(function (e) {

                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.Submit = function () {
                debugger;

                if ($scope.edt1 != undefined) {
                    debugger;
                    if ($scope.edt1.sims_send_email == true && $scope.edt1.sims_send_email != undefined) {
                        var Savedata = [];
                        var studName = "";
                        for (var i = 0; i < $scope.filteredTodos.length; i++) {
                            var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                            if (v.checked == true) {
                                $scope.flag = true;

                                if ($scope.edt1.sims_communicate_to_father == true) {

                                    var alert_father = ({

                                        'sims_cur_code': $scope.edt.sims_cur_code,
                                        'sims_academic_year': $scope.temp.sims_academic_year,
                                        'sims_grade_name': $scope.filteredTodos[i].sims_grade_name,
                                        'sims_section_name': $scope.filteredTodos[i].sims_section_name,
                                        'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                                        'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                                        'sims_enroll_number': $scope.filteredTodos[i].sims_enroll_number,
                                        'student_full_name': $scope.filteredTodos[i].student_full_name,
                                        'sims_parent_number': $scope.filteredTodos[i].sims_parent_number,
                                        'sims_email_id': $scope.filteredTodos[i].sims_parent_father_email,
                                        'sims_from_date': $scope.dt.sims_from_date,
                                        'doc_path_available_flag': $scope.filteredTodos[i].doc_path_available_flag,
                                        'sims_communicate_to_father':$scope.edt1.sims_communicate_to_father,
                                        opr: 'R'
                                    });
                                    Savedata.push(alert_father);
                                }

                                if ($scope.edt1.sims_communicate_to_mother == true) {

                                    var alert_mother = ({
                                        'sims_cur_code': $scope.edt.sims_cur_code,
                                        'sims_academic_year': $scope.temp.sims_academic_year,
                                        'sims_grade_name': $scope.filteredTodos[i].sims_grade_name,
                                        'sims_section_name': $scope.filteredTodos[i].sims_section_name,
                                        'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                                        'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                                        'sims_enroll_number': $scope.filteredTodos[i].sims_enroll_number,
                                        'student_full_name': $scope.filteredTodos[i].student_full_name,
                                        'sims_parent_number': $scope.filteredTodos[i].sims_parent_number,
                                        'sims_email_id': $scope.filteredTodos[i].sims_parent_mother_email,
                                        'sims_from_date': $scope.dt.sims_from_date,
                                        'doc_path_available_flag': $scope.filteredTodos[i].doc_path_available_flag,
                                        'sims_communicate_to_mother': $scope.edt1.sims_communicate_to_mother,
                                        opr: 'R'
                                    });
                                    Savedata.push(alert_mother);
                                }

                                if ($scope.edt1.sims_communicate_to_guardian == true) {

                                    var alert_guardian = ({
                                        'sims_cur_code': $scope.edt.sims_cur_code,
                                        'sims_academic_year': $scope.temp.sims_academic_year,
                                        'sims_grade_name': $scope.filteredTodos[i].sims_grade_name,
                                        'sims_section_name': $scope.filteredTodos[i].sims_section_name,
                                        'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                                        'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                                        'sims_enroll_number': $scope.filteredTodos[i].sims_enroll_number,
                                        'student_full_name': $scope.filteredTodos[i].student_full_name,
                                        'sims_parent_number': $scope.filteredTodos[i].sims_parent_number,
                                        'sims_email_id': $scope.filteredTodos[i].sims_parent_guardian_email,
                                        'sims_from_date': $scope.dt.sims_from_date,
                                        'doc_path_available_flag': $scope.filteredTodos[i].doc_path_available_flag,
                                        'sims_communicate_to_guardian': $scope.edt1.sims_communicate_to_guardian,
                                        opr: 'R'

                                    });
                                    Savedata.push(alert_guardian);
                                }

                                //var alertsave = ({
                                //    'sims_cur_code': $scope.edt.sims_cur_code,
                                //    'sims_academic_year': $scope.temp.sims_academic_year,
                                //    'sims_grade_name': $scope.filteredTodos[i].sims_grade_name,
                                //    'sims_section_name': $scope.filteredTodos[i].sims_section_name,
                                //    'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                                //    'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                                //    'sims_enroll_number': $scope.filteredTodos[i].sims_enroll_number,
                                //    'student_full_name': $scope.filteredTodos[i].student_full_name,

                                //    'sims_parent_number': $scope.filteredTodos[i].sims_parent_number,

                                //   // 'sims_contact_person': $scope.filteredTodos[i].sims_contact_person,
                                //    'sims_from_date': $scope.dt.sims_from_date,

                                //    'doc_path_available_flag': $scope.filteredTodos[i].doc_path_available_flag,
                                //    opr: 'R'
                                //});
                                //Savedata.push(alertsave);                               
                            }
                        }

                        for (var j = 0; j < $scope.studlist.length; j++) {
                            // studName = studName + $scope.studlist[i].student_full_name + ' of ' + $scope.studlist[i].sims_grade_name + '-' + $scope.studlist[i].sims_section_name + ' marked by ' + $scope.studlist[i].sims_teacher_name + '<br><br>';
                            studName = studName + $scope.studlist[j].sims_enroll_number + ',';
                            console.log("studName :", studName);
                        }

                        var attendance_type = $("#attendance_type option:selected").text();
                        console.log("attendance_name", attendance_type);

                        console.log("Savedata", Savedata);

                        var obj = {
                            stud_name: studName,
                            attendance_type: attendance_type,
                            sims_from_date: $scope.dt.sims_from_date
                        }
                        console.log("obj", obj);

                        $http.post(ENV.apiUrl + "api/StudentReport_abqisController/CUDAbsentStudentList?data1=" + JSON.stringify(obj), Savedata).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: 'Email Sent Successfully', width: 380, height: 200 });
                                $scope.Reset();
                                $scope.currentPage = true;
                            }
                            //if ($scope.msg1.strMessage != undefined) {
                            //    if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                            //        swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                            //        $scope.Reset();
                            //        $scope.currentPage = true;
                            //    }
                            //}
                        });

                    }


                    if ($scope.edt1.sims_send_alert == true && $scope.edt1.sims_send_alert != undefined) {
                        var Savedata = [];
                        for (var i = 0; i < $scope.filteredTodos.length; i++) {
                            var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                            if (v.checked == true) {
                                $scope.flag = true;
                                var alertsave = ({
                                    'sims_cur_code': $scope.edt.sims_cur_code,
                                    'sims_academic_year': $scope.temp.sims_academic_year,
                                    'sims_grade_name': $scope.filteredTodos[i].sims_grade_name,
                                    'sims_section_name': $scope.filteredTodos[i].sims_section_name,
                                    'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                                    'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                                    'sims_enroll_number': $scope.filteredTodos[i].sims_enroll_number,
                                    'student_full_name': $scope.filteredTodos[i].student_full_name,
                                    'sims_parent_number': $scope.filteredTodos[i].sims_parent_number,
                                    'sims_from_date': $scope.dt.sims_from_date,
                                    'sims_contact_person': $rootScope.globals.currentUser.username,
                                    'doc_path_available_flag': $scope.filteredTodos[i].doc_path_available_flag,
                                    opr: 'I'
                                });
                                Savedata.push(alertsave);
                            }
                        }

                        $http.post(ENV.apiUrl + "api/StudentReport_abqisController/CUDAbsentStudentList", Savedata).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1.strMessage != undefined) {
                                if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                    swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                                    $scope.Reset();
                                    $scope.currentPage = true;
                                }
                            }

                        });
                    }

                    if ($scope.edt1.sims_send_sms == true && $scope.edt1.sims_send_sms != undefined) {
                        var Savedata = [];
                        for (var i = 0; i < $scope.filteredTodos.length; i++) {
                            var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                            if (v.checked == true) {
                                $scope.flag = true;

                                if ($scope.edt1.sims_communicate_to_father == true) {
                                    var alert_father = ({
                                        'sims_cur_code': $scope.edt.sims_cur_code,
                                        'sims_academic_year': $scope.temp.sims_academic_year,
                                        'sims_grade_name': $scope.filteredTodos[i].sims_grade_name,
                                        'sims_section_name': $scope.filteredTodos[i].sims_section_name,
                                        'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                                        'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                                        'sims_enroll_number': $scope.filteredTodos[i].sims_enroll_number,
                                        'student_full_name': $scope.filteredTodos[i].student_full_name,
                                        'sims_parent_number': $scope.filteredTodos[i].sims_parent_number,

                                        'sims_contact_number': $scope.filteredTodos[i].sims_parent_father_mobile,
                                        //'sims_contact_number': $scope.filteredTodos[i].sims_contact_number,
                                        'sims_from_date': $scope.dt.sims_from_date,
                                        'doc_path_available_flag': $scope.filteredTodos[i].doc_path_available_flag,
                                        opr: 'M'

                                    });
                                    Savedata.push(alert_father);
                                }

                                if ($scope.edt1.sims_communicate_to_mother == true) {
                                    var alert_mother = ({
                                        'sims_cur_code': $scope.edt.sims_cur_code,
                                        'sims_academic_year': $scope.temp.sims_academic_year,
                                        'sims_grade_name': $scope.filteredTodos[i].sims_grade_name,
                                        'sims_section_name': $scope.filteredTodos[i].sims_section_name,
                                        'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                                        'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                                        'sims_enroll_number': $scope.filteredTodos[i].sims_enroll_number,
                                        'student_full_name': $scope.filteredTodos[i].student_full_name,
                                        'sims_parent_number': $scope.filteredTodos[i].sims_parent_number,

                                        'sims_contact_number': $scope.filteredTodos[i].sims_parent_mother_mobile,
                                        //'sims_contact_number': $scope.filteredTodos[i].sims_contact_number,
                                        'sims_from_date': $scope.dt.sims_from_date,
                                        'doc_path_available_flag': $scope.filteredTodos[i].doc_path_available_flag,
                                        opr: 'M'

                                    });
                                    Savedata.push(alert_mother);
                                }

                                if ($scope.edt1.sims_communicate_to_guardian == true) {
                                    var alert_guardian = ({
                                        'sims_cur_code': $scope.edt.sims_cur_code,
                                        'sims_academic_year': $scope.temp.sims_academic_year,
                                        'sims_grade_name': $scope.filteredTodos[i].sims_grade_name,
                                        'sims_section_name': $scope.filteredTodos[i].sims_section_name,
                                        'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                                        'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                                        'sims_enroll_number': $scope.filteredTodos[i].sims_enroll_number,
                                        'student_full_name': $scope.filteredTodos[i].student_full_name,
                                        'sims_parent_number': $scope.filteredTodos[i].sims_parent_number,


                                        'sims_contact_number': $scope.filteredTodos[i].sims_parent_guardian_mobile,

                                        //'sims_contact_number': $scope.filteredTodos[i].sims_contact_number,
                                        'sims_from_date': $scope.dt.sims_from_date,
                                        'doc_path_available_flag': $scope.filteredTodos[i].doc_path_available_flag,
                                        opr: 'M'

                                    });
                                    Savedata.push(alert_guardian);
                                }

                                //var alertsave = ({
                                //    'sims_cur_code': $scope.edt.sims_cur_code,
                                //    'sims_academic_year': $scope.temp.sims_academic_year,
                                //    'sims_grade_name': $scope.filteredTodos[i].sims_grade_name,
                                //    'sims_section_name': $scope.filteredTodos[i].sims_section_name,
                                //    'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                                //    'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                                //    'sims_enroll_number': $scope.filteredTodos[i].sims_enroll_number,
                                //    'student_full_name': $scope.filteredTodos[i].student_full_name,
                                //    'sims_parent_number': $scope.filteredTodos[i].sims_parent_number,

                                //    //'sims_contact_number': $scope.filteredTodos[i].sims_contact_number,
                                //    'sims_from_date': $scope.dt.sims_from_date,
                                //    'doc_path_available_flag': $scope.filteredTodos[i].doc_path_available_flag,
                                //    opr: 'M'
                                //});
                                //Savedata.push(alertsave);
                            }
                        }

                        $http.post(ENV.apiUrl + "api/StudentReport_abqisController/CUDAbsentStudentList", Savedata).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1.strMessage != undefined) {
                                if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                    swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                                    $scope.Reset();
                                    $scope.currentPage = true;
                                }
                            }

                        });
                    }
                }

                else {
                    swal({ title: 'Alert', text: "Please Select Atleast One Sending Type.", showCloseButton: true, width: 450, height: 200 });
                }
            }

            $scope.Reset = function () {

                $("#cmb_grade_code").select2("val", "");
                $("#cmb_section_code").select2("val", "");

                $scope.edt = {
                    sims_cur_code: '',
                    sims_student_enroll_number: '',
                }
                $scope.temp = {
                    sims_academic_year: '',
                }
                $scope.dt = {
                    sims_from_date: dateyear,
                }
                $scope.edt1 = {
                    sims_send_email: false,
                    sims_send_alert: false,
                    sims_send_sms: false,
                    sims_communicate_to_father: false,
                    sims_communicate_to_mother: false,
                    sims_communicate_to_guardian: false

                }

                $scope.table = false;
                $scope.table1 = false;
                $scope.getcur();

            }

            $scope.Clear = function () {
                $scope.edt1 = {
                    sims_send_email: false,
                    sims_send_alert: false,
                    sims_send_sms: false,
                    sims_communicate_to_father: false,
                    sims_communicate_to_mother: false,
                    sims_communicate_to_guardian: false

                }
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                    v.checked = false;
                    main.checked = false;
                    $scope.row1 = '';
                    $('tr').removeClass("row_selected");
                }
            }

            $rootScope.visible_stud = true;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = false;
            $rootScope.chkMulti = false;

            $scope.Search = function () {
                $scope.global_Search_click();

                $('#Global_Search_Modal').modal({ backdrop: "static" });
                //  $scope.display = true;
                //$scope.SelectedUserLst = [];
            }

            $scope.$on('global_cancel', function () {
                if ($scope.SelectedUserLst.length > 0) {
                    $scope.edt =
                        {
                            sims_attendance_code: $scope.arule[0].sims_attendance_code,
                            sims_cur_code: $scope.curriculum[0].sims_cur_code,
                            s_name: $scope.SelectedUserLst[0].name,
                            sims_student_enroll_number: $scope.SelectedUserLst[0].s_enroll_no
                        }
                }
                $scope.em_number = $scope.edt.em_number;
            });

            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.chk_email = function () {
                $scope.SentM1 = $scope.SentM1 === false ? true : false;

            }

            $scope.chk_alert = function () {
                $scope.SentM1 = $scope.SentM1 === false ? true : false;

            }

            $scope.chk_sms = function () {
                $scope.SentM1 = $scope.SentM1 === false ? true : false;

            }

        }])
})();
