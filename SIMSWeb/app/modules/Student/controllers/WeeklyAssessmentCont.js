﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('WeeklyAssessmentCont',
       ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {


          var user = $rootScope.globals.currentUser.username;

          $scope.edt = {};
          
          $scope.grid = true;
          $scope.NewInsert = true;
          $scope.names = true;
          $scope.deletebuttonshow = true;
          $scope.Co_Scholastic = true;
          $scope.updateshow = false;
          $scope.headcolor = '1';
          $scope.temp = {};
          $scope.Expand1 = true;
          
          $scope.propertyName = null;

          $scope.sortBy = function (propertyName) {
              $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
              $scope.propertyName = propertyName;
          };

          $timeout(function () {
              $("#example2_info").tableHeadFixer({ 'top': 1 }); 
              $("#Table1").tableHeadFixer({ 'top': 1 });
          }, 100);
          $scope.edt = {};
          $scope.updateshow = true;
          $scope.NewInsert = true;
          $scope.updateassign = false;
          $scope.edt.gb_status = true;
          $scope.Assign = {};
          var dt = new Date();
          $scope.Assign.assignment_Date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
          $scope.Assign.assignment_Due_Date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();

          $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
              $scope.curriculum = res.data;
              $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
              $scope.Getyear($scope.curriculum[0].sims_cur_code);
              
          });

          $http.post(ENV.apiUrl + "api/Gradebook/GradeScaleDataOpr?teachercode=" + user + "&opr=SC").then(function (All_Grade) {
              $scope.All_SCALE_GRADE = All_Grade.data;

              console.log($scope.All_SCALE_GRADE);

          });

          $scope.Getyear = function (str) {
              $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                  $scope.getAcademicYear = Academicyear.data;
                  $scope.edt['sims_academic_year'] = $scope.getAcademicYear[0].sims_academic_year;
                  $scope.GetGradecodes($scope.edt.sims_cur_code, $scope.getAcademicYear[0].sims_academic_year);
              });
          }

          $scope.GetGradecodes = function (str, str1) {
              $http.get(ENV.apiUrl + "api/StudentSectionSubject/GetRCGrade?curcode=" + str + "&ayear=" + str1 + "&teachercode=" + user).then(function (res) {
                  $scope.getAllGrades = res.data;
                  $scope.edt['sims_grade_code'] = $scope.getAllGrades[0].sims_grade_code;
                  $scope.getsection(str, str1, $scope.getAllGrades[0].sims_grade_code);
              })
          };

          $scope.getsection = function (str, str1, str2) {
              $http.get(ENV.apiUrl + "api/StudentSectionSubject/GetRCSection?curcode=" + str + "&ayear=" + str1 + "&gradecode=" + str2 + "&teachercode=" + user).then(function (Allsection) {
                  $scope.getSectionFromGrade = Allsection.data;
                  $scope.edt['sims_section_code'] = $scope.getSectionFromGrade[0].sims_section_code;

                  $scope.GradeSectionSubject();
              })
          };

          $scope.GradeSectionSubject = function () {

              $scope.subject_names = [];
              var report1 = {
                  sims_grade_code: $scope.edt.sims_grade_code,
                  sims_academic_year: $scope.edt.sims_academic_year,
                  sims_section_code: $scope.edt.sims_section_code,
                  sims_cur_code: $scope.edt.sims_cur_code,
                  sims_employee_code: user,
                  sims_teacher_code:user
              };

              $scope.Assignment_Form.$setPristine();
              $scope.myForm1.$setUntouched();
              $scope.myForm.$setUntouched();

              $http.post(ENV.apiUrl + "api/Gradebook/AllGradeSection_subject", report1).then(function (allSection_Subject) {
                  $scope.subject_names = allSection_Subject.data;

              });

              $http.post(ENV.apiUrl + "api/WeeklyAssessment/AllGradeBooks", report1).then(function (AllGradeBooks) {
                  $scope.AllGradeBooks = AllGradeBooks.data;

              });
          }

          $http.get(ENV.apiUrl + "api/generatepayroll/GetMonth").then(function (GetMonth) {
              $scope.GetMonth = GetMonth.data;
          });
           
          $scope.WeekObject = [{ week_name: 'Week One', week_code: '1' },
                               { week_name: 'Week Two', week_code: '2' },
                               { week_name: 'Week Three', week_code: '3' },
                               { week_name: 'Week Four', week_code: '4' },
                               { week_name: 'Week Five', week_code: '5' }]
           
          $scope.MonthObject = [{ month_name: 'January', month_code: '1' },
                                { month_name: 'February', month_code: '2' },
                                { month_name: 'March', month_code: '3' },
                                { month_name: 'April', month_code: '4' },
                                { month_name: 'May', month_code: '5' },
                                { month_name: 'June', month_code: '6' },
                                { month_name: 'July', month_code: '7' },
                                { month_name: 'August', month_code: '8' },
                                { month_name: 'September', month_code: '9' },
                                { month_name: 'October', month_code: '10' },
                                { month_name: 'November', month_code: '11' },
                                { month_name: 'December', month_code: '12' }]
           
          $scope.btn_New_Click = function () {

              $scope.grid = false;

          }

          $scope.AssessmentGroupShow = function (color) {
              $scope.headcolor = '1';
              $scope.deletebuttonshow = true;
              $scope.Co_Scholastic = true;
              $scope.Attributes = false;
              $scope.Assessment_week = false;
              $scope.assignmarkupdate = false;

          }

          $scope.AssessmentAttributeShow = function (color) {
              $scope.headcolor = '2';
              $scope.deletebuttonshow = false;
              $scope.Co_Scholastic = false;
              $scope.Attributes = true;
              $scope.Assessment_week = false;
              $scope.assignmarkupdate = false;
          }

          $scope.changemonth = function (info) {

              for (var i = 0; i < $scope.MonthObject.length; i++) {
                  if ($scope.MonthObject[i].month_code == info) {
                      $scope.temp['cat_code'] = $scope.MonthObject[i].month_code;
                      $scope.temp['cat_name'] = $scope.MonthObject[i].month_name;
                  }

              }

          }

          $scope.btn_Assessment_Save_Click = function (isvalid) {

              if (isvalid == true) {
                  $scope.gradebookobject = [];

                  var data = {};
                  data.cur_code = $scope.edt.sims_cur_code;
                  data.academic_year = $scope.edt.sims_academic_year
                  data.grade_code = $scope.edt.sims_grade_code
                  data.section_code = $scope.edt.sims_section_code
                  data.gb_grade_scale = $scope.edt.gb_grade_scale
                  data.teacher_code = user
                  data.gb_status = $scope.edt.gb_status
                  data.Subject_Code = $scope.edt.Subject_Code
                  data.OPR = "GI";
                  $scope.gradebookobject.push(data);

                  $http.post(ENV.apiUrl + "api/WeeklyAssessment/CUDGradeBookDataOpr", $scope.gradebookobject).then(function (msg) {
                      $scope.msg1 = msg.data;
                      if ($scope.msg1 == true) {

                          swal({ text: 'Assessment Gradebook Created Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                          $scope.GradeSectionSubject();
                      }
                      else {

                          swal({ text: 'Assessment Gradebook Not Created. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                      }
                  });
              }
          }

          $scope.edit = function (str, index) {

              $scope.NewInsert = false;

              $scope.Expand = str.gb_number;
              $scope.cur_gb_name = str.gb_number;
              $scope.edt = {
                  'sims_cur_code': str.cur_code,
                  'sims_academic_year': str.academic_year,
                  'gradeCode': str.gb_grade_scale,
                  'gb_name': str.gb_name,
                  'gb_status': str.gb_status,
                  'gb_remark': str.gb_remark,
                  'sims_grade_code': str.grade_code,
                  'sims_section_code': str.section_code,
                  'gb_number': str.gb_number,
                  'gb_grade_scale': str.gb_grade_scale,
                  'Subject_Code': str.subject_Code
              }

          }

          $scope.btn_Assessment_Update_Click = function () {


              $scope.gradebookobject = [];

              var data = {};
              data.cur_code = $scope.edt.sims_cur_code;
              data.academic_year = $scope.edt.sims_academic_year;
              data.grade_code = $scope.edt.sims_grade_code;
              data.section_code = $scope.edt.sims_section_code;
              data.gb_grade_scale = $scope.edt.gb_grade_scale;
              data.Subject_Code = $scope.edt.Subject_Code;
              data.gb_number = $scope.edt.gb_number;
              data.gb_status = $scope.edt.gb_status;
              data.OPR = "GU";
              $scope.gradebookobject.push(data);

              $http.post(ENV.apiUrl + "api/WeeklyAssessment/CUDGradeBookDataOpr", $scope.gradebookobject).then(function (msg) {
                  $scope.msg1 = msg.data;
                  if ($scope.msg1 == true) {

                      $scope.edt['Subject_Code'] = '';
                      $scope.edt['gb_grade_scale'] = '';

                      $scope.NewInsert = true;

                      swal({ text: 'Assessment Gradebook Updated Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                      $scope.GradeSectionSubject();
                  }
                  else if ($scope.msg1 == false) {

                      swal({ text: 'Assessment Gradebook Not Updated. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                  }
                  else
                  {
                      swal("Error-" + $scope.msg1)
                  }
              });

          }

          $scope.btn_Assessment_Category_Save_Click = function (isvalid) {

              if (isvalid) {

                  $scope.gradebookobject = [];

                  for (var i = 0; i < $scope.AllGradeBooks.length; i++) {
                      if ($scope.AllGradeBooks[i].ischecked == true) {

                          var data = {};
                          data.cur_code = $scope.edt.sims_cur_code;
                          data.academic_year = $scope.edt.sims_academic_year;
                          data.grade_code = $scope.edt.sims_grade_code;
                          data.section_code = $scope.edt.sims_section_code;
                          data.gb_grade_scale = $scope.edt.gb_grade_scale
                          data.cat_code = $scope.temp.cat_code;
                          data.cat_name = $scope.temp.cat_name;
                          data.gb_number = $scope.AllGradeBooks[i].gb_number;
                          data.OPR = "CI";
                          $scope.gradebookobject.push(data);
                      }
                  }
                  if ($scope.gradebookobject.length > 0) {
                      $http.post(ENV.apiUrl + "api/WeeklyAssessment/CategoryDataOpr", $scope.gradebookobject).then(function (msg) {
                          $scope.msg1 = msg.data;
                          if ($scope.msg1 == true) {

                              swal({ text: 'Assessment  Category Created Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                              $scope.GradeSectionSubject();
                          }
                          else {

                              swal({ text: 'Assessment Category Not Created. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                          }
                      });
                  } else {

                      swal({ text: 'Select Atleat One Gradebook', imageUrl: "assets/img/notification-alert.png", width: 320, showCloseButton: true });
                  }
              }
          }

          $scope.editCategory = function (str) {

              $scope.updateshow = false;

              $scope.Expand = str.gb_number;
              $scope.catcolor = str.cat_code + str.gb_number;
              $scope.temp = {

                  'cat_code': str.cat_code
                    , 'cat_name': str.cat_name
                    , 'gb_number': str.gb_number
                    , 'sims_grade_code': str.grade_code
                    , 'sims_section_code': str.section_code
                    , 'sims_cur_code': str.cur_code,
                  'sims_academic_year': str.academic_year

              };
          }
          debugger;
          $scope.btn_Save_Click_Weekly_Assessment = function (isvalid) {
              if (isvalid) {

                  $scope.gradebookobject = [];

                  for (var i = 0; i < $scope.AllGradeBooks.length; i++) {
                      for (var j = 0; j < $scope.AllGradeBooks[i].categories.length; j++) {
                          if ($scope.AllGradeBooks[i].categories[j].ischecked == true) {

                              var data = {};
                              debugger;
                              data.Assignment_cur_code = $scope.edt.sims_cur_code;
                              data.Assignment_academic_year = $scope.edt.sims_academic_year;
                              data.Assignment_grade_code = $scope.edt.sims_grade_code;
                              data.Assignment_section_code = $scope.edt.sims_section_code;
                              data.Assignment_gb_number = $scope.AllGradeBooks[i].categories[j].gb_number;
                              data.Assignment_cat_code = $scope.AllGradeBooks[i].categories[j].cat_code;
                              data.Assignment_Name = $scope.Assign.assignment_Name;
                              data.Assignment_Remark = $scope.Assign.assignment_Remark;
                              //data.Assignment_Date = $scope.Assign.assignment_Due_Date;
                              data.Assignment_Date = $scope.Assign.assignment_Date;
                              //data.Assignment_Due_Date = $scope.Assign.assignment_Date;
                              data.Assignment_Due_Date = $scope.Assign.assignment_Due_Date;
                              data.Assignment_Code = $scope.Assign.assignment_Code;
                              data.OPR = 'AI';
                              $scope.gradebookobject.push(data);
                          }
                      }
                  }
                  if ($scope.gradebookobject.length > 0) {
                      $http.post(ENV.apiUrl + "api/WeeklyAssessment/AssignmentDataOpr", $scope.gradebookobject).then(function (msg) {
                          $scope.msg1 = msg.data;
                          if ($scope.msg1 == true) {

                              swal({ text: 'Assessment Assignment Created Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                              $scope.GradeSectionSubject();
                          }
                          else {

                              swal({ text: 'Assessment Assignment Not Created. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                          }
                      });
                  }
                  else {
                      swal({ text: 'Select Atleast One Gradebook Or Categoey', imageUrl: "assets/img/notification-alert.png", width: 320, showCloseButton: true });
                  }

              }
          }

          $scope.checkweeklyassementname = function (info) {
              for (var i = 0; i < $scope.WeekObject.length; i++) {
                  if ($scope.WeekObject[i].week_code == info) {
                      $scope.Assign['assignment_Name'] = $scope.WeekObject[i].week_name;
                  }
              }
          }

          $scope.btn_AssessmentCancel_click = function () {

              $scope.edt['Subject_Code'] = '';
              $scope.edt['gb_grade_scale'] = '';
              $scope.NewInsert = true;
          }

          $scope.btn_category_cancel_click = function () {
              $scope.temp['cat_code'] = '';
              $scope.updateshow = true;
          }
          debugger;
          $scope.EditAssignment = function (info, str, info1) {

              $scope.cur_gb_cat_assign_name = info.assignment_cat_code + info.assignment_gb_number + info.assignment_Code;
              $scope.assignmentcolor = $scope.cur_gb_cat_assign_name;
              $scope.assignmarkupdate = false;
              if ($scope.Co_Scholastic == true) {
                  $scope.updateassign = true;
                  $scope.Assign['assignment_Code'] = info.assignment_Code;
                  $scope.Assign['assignment_Date'] = info.assignment_Date;
                  $scope.Assign['assignment_Due_Date'] = info.assignment_Due_Date;
                  $scope.Assign['assignment_Remark'] = info.assignment_Remark;
                  $scope.Assign['assignment_Name'] = info.assignment_Name;
                  $scope.Assign['assignment_gb_name'] = str.gb_name;
                  $scope.Assign['assignment_cat_name'] = info1.cat_name;
                  $scope.Assign['assignment_gb_code'] = str.gb_number;
                  $scope.Assign['assignment_cat_code'] = info1.cat_code;
              }
              else {

                  var data = {};
                  data.Assignment_Code = info.assignment_Code;
                  data.Assignment_cur_code = info.assignment_cur_code;
                  data.Assignment_academic_year = info.assignment_academic_year;
                  data.Assignment_grade_code = info.assignment_grade_code;
                  data.Assignment_section_code = info.assignment_section_code;
                  data.Assignment_gb_number = info.assignment_gb_number;
                  data.Assignment_cat_code = info.assignment_cat_code;
                  $scope.busy = true;
                  $http.post(ENV.apiUrl + "api/WeeklyAssessment/AssignmentStudent", data).then(function (AssignmentStudent) {
                      $scope.AssignmentStudent = AssignmentStudent.data;
                      $scope.busy = false;
                      if ($scope.AssignmentStudent.length > 0) {
                          $scope.assignmarkupdate = true;

                          for (var i = 0 ; i < $scope.AssignmentStudent.length;i++)
                          {
                              for(var j =0 ;j<$scope.AssignmentStudent[i].scalelist.length;j++)
                              {
                                  if($scope.AssignmentStudent[i].scalelist[j].ischecked==true)
                                  {
                                      $scope.AssignmentStudent[i].scalelist['ischecked1'] = true;
                                  }
                              }
                          }

                              
                      }
                      else {
                          $scope.assignmarkupdate = false;
                          swal({ text: 'Student Not Found', width: 320, showCloseButton: true });
                      }
                  })
              }
          }

          $scope.UpdateAssessmentAssignmentData = function () {

              $scope.gradebookobject = [];
              var data = {};
              debugger;
              data.Assignment_cur_code = $scope.edt.sims_cur_code;
              data.Assignment_academic_year = $scope.edt.sims_academic_year;
              data.Assignment_grade_code = $scope.edt.sims_grade_code;
              data.Assignment_section_code = $scope.edt.sims_section_code;
              data.Assignment_gb_number = $scope.Assign.assignment_gb_code;
              data.Assignment_cat_code = $scope.Assign.assignment_cat_code;
              data.Assignment_Name = $scope.Assign.assignment_Name;
              data.Assignment_Remark = $scope.Assign.assignment_Remark;
              data.Assignment_Date = $scope.Assign.assignment_Date;
              data.Assignment_Due_Date = $scope.Assign.assignment_Due_Date;
              data.Assignment_Code = $scope.Assign.assignment_Code

              data.OPR = 'AU';
              $scope.gradebookobject.push(data);


              if ($scope.gradebookobject.length > 0) {
                  $http.post(ENV.apiUrl + "api/WeeklyAssessment/AssignmentDataOpr", $scope.gradebookobject).then(function (msg) {
                      $scope.msg1 = msg.data;
                      if ($scope.msg1 == true) {
                          $scope.Assign = {};
                          $scope.updateassign = false;
                          $scope.assignmentcolor = '';
                          swal({ text: 'Assessment Updated Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                          $scope.GradeSectionSubject();
                      }
                      else if ($scope.msg1 == false) {

                          swal({ text: 'Assessment Not Updated. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                      }
                      else {
                          swal("Error-" + $scope.msg1)
                      }
                  });
              }
              else {
                  swal({ text: 'Select Atleast One Gradebook Or Categoey', imageUrl: "assets/img/notification-alert.png", width: 320, showCloseButton: true });
              }

          }

          $scope.CancelGradeBooAssignmentData = function () {

              $scope.updateassign = false;

              $scope.cur_gb_cat_assign_name = '';
              $scope.assignmentcolor = '';

              $scope.Assign['assignment_Code'] = '';
              $scope.Assign['assignment_Date'] = '';
              $scope.Assign['assignment_Due_Date'] = '';
              $scope.Assign['assignment_Remark'] = '';

              $scope.Assign['assignment_gb_name'] = '';
              $scope.Assign['assignment_cat_name'] = '';

              $scope.Assign['assignment_gb_code'] = '';
              $scope.Assign['assignment_cat_code'] = '';

          }

          $scope.SaveAssessmentStudentGradeData = function () {

              $scope.studentdatamark = [];

              debugger;

              for (var i = 0; i < $scope.AssignmentStudent.length; i++) {
                  for (var j = 0; j < $scope.AssignmentStudent[i].scalelist.length; j++) {
                      if ($scope.AssignmentStudent[i].scalelist[j].ischecked == true) {
                          var data = {};
                          
                          data.sims_mark_final_grade_code = $scope.AssignmentStudent[i].scalelist[j].sims_mark_grade_code;
                          data.Assignment_cur_code = $scope.AssignmentStudent[i].sims_cur_code;
                          data.Assignment_academic_year = $scope.AssignmentStudent[i].sims_academic_year;
                          data.Assignment_grade_code = $scope.AssignmentStudent[i].sims_grade_code;
                          data.Assignment_section_code = $scope.AssignmentStudent[i].sims_section_code;
                          data.Assignment_gb_number = $scope.AssignmentStudent[i].sims_gb_number;
                          data.Assignment_cat_code = $scope.AssignmentStudent[i].sims_gb_cat_code;
                          data.Assignment_Code = $scope.AssignmentStudent[i].sims_gb_cat_assign_number;
                          data.StudEnroll = $scope.AssignmentStudent[i].sims_enroll_number;

                          $scope.studentdatamark.push(data);
                      }
                      else if($scope.AssignmentStudent[i].scalelist[j].ischange == true)
                      {
                          var data = {};

                          data.sims_mark_final_grade_code = null;
                          data.Assignment_cur_code = $scope.AssignmentStudent[i].sims_cur_code;
                          data.Assignment_academic_year = $scope.AssignmentStudent[i].sims_academic_year;
                          data.Assignment_grade_code = $scope.AssignmentStudent[i].sims_grade_code;
                          data.Assignment_section_code = $scope.AssignmentStudent[i].sims_section_code;
                          data.Assignment_gb_number = $scope.AssignmentStudent[i].sims_gb_number;
                          data.Assignment_cat_code = $scope.AssignmentStudent[i].sims_gb_cat_code;
                          data.Assignment_Code = $scope.AssignmentStudent[i].sims_gb_cat_assign_number;
                          data.StudEnroll = $scope.AssignmentStudent[i].sims_enroll_number;
                          $scope.studentdatamark.push(data);
                      }
                  }
              }

              
              $http.post(ENV.apiUrl + "api/WeeklyAssessment/AssignMarksToStudentsM", $scope.studentdatamark).then(function (msg) {
                      $scope.msg1 = msg.data;
                      if ($scope.msg1 == true) {
                          swal({
                              text: 'Grade Inserted Successfully', imageUrl: "assets/img/check.png",
                              width: 320, showCloseButton: true
                          });
                          $scope.GradeSectionSubject();
                      }
                      else if ($scope.msg1 == false) {

                          swal({
                              text: 'Grade Not Inserted. ', imageUrl: "assets/img/close.png",
                              width: 320, showCloseButton: true
                          });
                      }
                      else {
                          swal("Error-" + $scope.msg1)
                      }
                  });
              
          }
           
          $scope.DeleteGradeBookCateoryAssignment=function(str)
          {
              

              $scope.gradebookobject = [];

              for (var i = 0; i < $scope.AllGradeBooks.length; i++)
              {
                  if($scope.AllGradeBooks[i].categories.length <=0)
                  {
                      $scope.AllGradeBooks[i].categories=[];
                  }
                  for(var j=0;j< $scope.AllGradeBooks[i].categories.length;j++)
                  {
                      if($scope.AllGradeBooks[i].categories[j].assignMents.length <=0)
                      {
                          $scope.AllGradeBooks[i].categories[j].assignMents = [];
                      }

                      for(var k=0;k<$scope.AllGradeBooks[i].categories[j].assignMents.length;k++)
                      {

                          if(  $scope.AllGradeBooks[i].categories[j].assignMents[k].ischecked==true)
                          {
                              var data = {};

                              debugger;

                              data.Assignment_cur_code = $scope.edt.sims_cur_code;
                              data.Assignment_academic_year = $scope.edt.sims_academic_year;
                              data.Assignment_grade_code = $scope.edt.sims_grade_code;
                              data.Assignment_section_code = $scope.edt.sims_section_code;
                              data.Assignment_gb_number = $scope.AllGradeBooks[i].categories[j].assignMents[k].assignment_gb_number;
                              data.Assignment_cat_code =$scope.AllGradeBooks[i].categories[j].assignMents[k].assignment_cat_code;
                              data.Assignment_Code =$scope.AllGradeBooks[i].categories[j].assignMents[k].assignment_Code

                              data.OPR = 'AD';
                              $scope.gradebookobject.push(data);
                          }
                      }

                      }
              }

              if ($scope.gradebookobject.length > 0) {
                  $http.post(ENV.apiUrl + "api/WeeklyAssessment/AssignmentDataOprDelete", $scope.gradebookobject).then(function (msg) {
                      $scope.msg1 = msg.data;
                      if ($scope.msg1 != "") {
                          $scope.gradebookobject = [];
                          $scope.updateassign = false;
                          $scope.assignmentcolor = '';
                          swal({ text: $scope.msg1, width: 320, showCloseButton: true });
                          $scope.GradeSectionSubject();
                      }
                      
                  });
              }
              else {
                  swal({ text: 'Select Atleast One Gradebook Or Categoey', imageUrl: "assets/img/notification-alert.png", width: 320, showCloseButton: true });
              }


          }
           
          $scope.UncheckAllGrade=function(str)
          {
              for (var i = 0; i < str.length; i++)
              {
                  if (str[i].ischecked == true) {
                      str[i].ischecked = false;
                      str[i].ischange = true;
                  }
              }
                 
          }

          $scope.setgradecode=function(str,no)
          {
              for (var i = 0; i < str.length; i++) {
                  if (str[i].ischecked == true && str[i].sims_mark_grade_code==no) {
                      str[i].ischecked = true;
                      
                  }
                  else
                  {
                      str[i].ischecked = false;
                  }
              }

          }
          $('*[data-datepicker="true"] input[type="text"]').datepicker({
              todayBtn: true,
              orientation: "top left",
              autoclose: true,
              todayHighlight: true
          });

          $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
              $('input[type="text"]', $(this).parent()).focus();
          });

       }])
})();