﻿
(function () {
    'use strict';
    var del = [];
    var main;
    var date1, date3, date4;

    var simsController = angular.module('sims.module.Student');
    simsController.controller('Admission_ChristOLCont',
        ['$scope', '$state', '$stateParams', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $stateParams, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.admgrid = false;
            $scope.btnadm = false;
            $scope.btn_Save = false;
            $scope.btn_NewSave = false;
            $scope.btncancel = false;
            $scope.btnok = false;
            $scope.prev = true;
            $scope.details = false;
            $scope.div_view = false;
            $scope.disableparent = true;
            $scope.lbl_show = false;
            $scope.edt1 = [];
            $scope.obj_age = [];
            $scope.div_year = true;
            $scope.div_subject = false;
            $scope.div_fyshow = false;
            $scope.div_syshow = false;
            $scope.scholnm = [], $scope.cur_data = [], $scope.acad_yr = [], $scope.city_lst = [], $scope.caste_lst = [], $scope.maritalstatus_lst = [];
            $scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.edt =
                {
                    admission_date: $scope.ddMMyyyy,
                    tent_join_date: $scope.ddMMyyyy,
                    comm_date: $scope.ddMMyyyy,
                    declaration_status: true,
                }

            var param = $stateParams.admission_num;
            console.log(param);

            $scope.div_edt = true;
            $scope.div_save = false;
            $scope.admgrid = true;
            $scope.btn_Save = true;
            $scope.btn_NewSave = false;
            $scope.selectdisabledata = true;
            $scope.disabledata = false;
            $scope.btncancel = true;

    
                $scope.apiurl = 'https://oa.mograsys.com/christ/';
                $scope.flgsvcc = false;
                $scope.flgdvhss = true;


            $scope.getsubject = function () {
                $http.get($scope.apiurl + "api/common/Admission/GetSubject?stream=" + $scope.edt.sims_student_attribute4).then(function (res) {
                    $scope.subjectgrp_data = res.data;
                    // console.log($scope.stream_data);
                });
            }
            

            $scope.modal_cancel = function () {
                //$('body').addClass('grey condense-menu');
                //$('#main-menu').addClass('mini');
                //$('.page-content').addClass('condensed');
                //$scope.isCondensed = true;
                //$("body").removeClass("modal-open");
                //$("div").removeClass("modal-backdrop in");
                $('#myModal1').modal('hide');

                console.log($stateParams.Class);
                $timeout(function () {
                    if ($http.defaults.headers.common['schoolId'] == 'siso' || $http.defaults.headers.common['schoolId'] == 'asis' || $http.defaults.headers.common['schoolId'] == 'abqis')
                        $state.go("main.Dasiso", { Class: $stateParams.Class });

                    else
                        $state.go("main.Dasdps", { Class: $stateParams.Class });

                }, 300);
            }

            $scope.back = function () {
                $scope.dd = true;
                window.localStorage['Admflag'] = $scope.dd;
                $scope.modal_cancel();
            }



            $scope.getchkAge = function () {
                $scope.edt.sims_student_attribute4 = "";
                $scope.edt.sims_student_attribute11 = "";
                $scope.div_subject = true;

                $http.get($scope.apiurl + "api/Comn/GetSubjectSvcc?cur_code=" + $scope.edt.curr_code + "&acad_yr=" + $scope.edt.academic_year + "&grade=" + $scope.edt.grade_code + "&adm_no=" + $scope.edt['admission_number']).then(function (res) {
                    $scope.subject_lst = res.data;
                });

                $http.get($scope.apiurl + "api/Comn/GetAgecompare?cur_code=" + $scope.edt.curr_code + "&acad_yr=" + $scope.edt.academic_year + "&grade=" + $scope.edt.grade_code).then(function (res) {
                    $scope.obj_age = res.data;
                    // console.log($scope.obj_age);
                });


                //if(
                if ($http.defaults.headers.common['schoolId'] == 'svvmc') {
                    if ($scope.edt.grade_code == '11') {
                        $scope.div_fyshow = false;
                        $scope.div_fyshow = false;
                    }
                    if ($scope.edt.grade_code == '12') {
                        $scope.div_fyshow = true;
                        $scope.div_syshow = false;
                    }
                    if ($scope.edt.grade_code == '13') {
                        $scope.div_fyshow = true;
                        $scope.div_syshow = true;
                    }

                }

                else if ($http.defaults.headers.common['schoolId'] == 'dvhss') {

                    if ($scope.edt.grade_code == '13' || $scope.edt.grade_code == '14') {
                        $scope.div_fyshow = false;
                        $scope.div_syshow = false;
                    }
                    if ($scope.edt.grade_code == '15' || $scope.edt.grade_code == '16') {
                        $scope.div_fyshow = true;
                        $scope.div_syshow = false;
                    }

                }
                //if ($scope.edt.grade_code == '13')
                //{
                //    $scope.div_subject = true;
                //    $http.get($scope.apiurl + "api/common/Admission/GetStreamPrefferd").then(function (res) {
                //        $scope.stream_data = res.data;
                //        // console.log($scope.stream_data);
                //    });
                //}
            }

           // if (param != '[object Object]')
            if (param != '[object Object]')
            {

                debugger
                $scope.btn_back = true;
                $http.get($scope.apiurl + "api/Comn/getAdmissionList").then(function (res) {
                    $scope.feemonth = []; $scope.feecat = [];
                   
                    $scope.feemonth = [
                        { sims_fee_month_code: '1', sims_fee_month_name: 'Jan' },
                        { sims_fee_month_code: '2', sims_fee_month_name: 'Feb' },
                        { sims_fee_month_code: '3', sims_fee_month_name: 'Mar' },
                        { sims_fee_month_code: '4', sims_fee_month_name: 'Apr' },
                        { sims_fee_month_code: '5', sims_fee_month_name: 'May' },
                        { sims_fee_month_code: '6', sims_fee_month_name: 'June' },
                        { sims_fee_month_code: '7', sims_fee_month_name: 'July' },
                        { sims_fee_month_code: '8', sims_fee_month_name: 'Aug' },
                        { sims_fee_month_code: '9', sims_fee_month_name: 'Sept' },
                        { sims_fee_month_code: '10', sims_fee_month_name: 'Oct' },
                        { sims_fee_month_code: '11', sims_fee_month_name: 'Nov' },
                        { sims_fee_month_code: '12', sims_fee_month_name: 'Dec' },
                    ]
                    $scope.obj = res.data;
                    for (var i = 0; i < res.data.length; i++) {

                        if (res.data[i].fee_category_desc != '') {
                            $scope.feecat.push({ fee_category_code: res.data[i].fee_category_code, fee_category_desc: res.data[i].fee_category_desc });
                            $scope.edt['fee_category_code'] = $scope.feecat[0].fee_category_code;
                        }

                        if (res.data[i].school_name != '') {
                            $scope.scholnm.push({ school_name: res.data[i].school_name, school_code: res.data[i].school_code });
                            $scope.edt['school_code'] = $scope.scholnm[0].school_code;
                        }
                        if (res.data[i].curr_name != '') {
                            $scope.cur_data.push({ curr_name: res.data[i].curr_name, curr_code: res.data[i].curr_code });
                            $scope.edt['curr_code'] = $scope.cur_data[0].curr_code;
                        }
                        if (res.data[i].academic_year_desc != '') {
                            $scope.acad_yr.push({ academic_year_desc: res.data[i].academic_year_desc, academic_year: res.data[i].academic_year });
                            $scope.edt['academic_year'] = $scope.acad_yr[0].academic_year;
                        }

                        if (res.data[i].sims_city_code != '') {
                            $scope.city_lst.push({ sims_city_code: res.data[i].sims_city_code, sims_city_state_code: res.data[i].sims_city_state_code, sims_city_name_en: res.data[i].sims_city_name_en });
                            // $scope.edt['academic_year'] = $scope.acad_yr[0].academic_year;
                        }
                        if (res.data[i].sims_appl_parameter_castCatcode != '') {
                            $scope.caste_lst.push({ sims_appl_parameter_castCatcode: res.data[i].sims_appl_parameter_castCatcode, sims_appl_form_field_value1_castcatdesc: res.data[i].sims_appl_form_field_value1_castcatdesc });
                            if ($scope.obj[i].sims_appl_parameter_castCatcode == 'GE') {
                                $scope.edt['sims_student_category'] = $scope.obj[i].sims_appl_parameter_castCatcode;
                            }
                        }
                        if (res.data[i].sims_appl_parameter_marital_statuscode != '') {
                            $scope.maritalstatus_lst.push({ sims_appl_parameter_marital_statuscode: res.data[i].sims_appl_parameter_marital_statuscode, sims_appl_form_field_value1_marital_statusdesc: res.data[i].sims_appl_form_field_value1_marital_statusdesc });
                            if ($scope.obj[i].sims_appl_parameter_marital_statuscode == 'U') {
                                $scope.edt['sims_admission_marital_status'] = $scope.obj[i].sims_appl_parameter_marital_statuscode;
                            }
                        }
                        

                        //if (res.data[i].sims_fee_month_name != '') {
                        //    $scope.feemonth.push({ sims_fee_month_code: res.data[i].sims_fee_month_code, sims_fee_month_name: res.data[i].sims_fee_month_name });
                        //}
                    }

                    $http.get($scope.apiurl + "api/common/Admission/GetTabStudentData?admission_number=" + param).then(function (res) {
                        $scope.edt = res.data;
                        $scope.getchkAge();
                        $scope.getsubject();
                        $scope.edt['comm_date'] = $scope.ddMMyyyy;
                          

                        //student previous exam details
                        $http.get($scope.apiurl + "api/common/Admission/GetStudent_PrevExamDetails?admission_number=" + param).then(function (res) {
                            $scope.edt_exam = res.data;
                            // console.log($scope.edt1);
                            $scope.edt['current_school_name'] = $scope.edt_exam['current_school_name'];
                            $scope.edt['sims_admission_boardname'] = $scope.edt_exam['sims_admission_boardname'];
                            $scope.edt['sims_admission_center_name'] = $scope.edt_exam['sims_admission_center_name'];
                            $scope.edt['sims_admission_passingyear_exam'] = $scope.edt_exam['sims_admission_passingyear_exam'];
                            $scope.edt['sims_admission_seat_no'] = $scope.edt_exam['sims_admission_seat_no'];
                            $scope.edt['sims_admission_no_of_attempts'] = $scope.edt_exam['sims_admission_no_of_attempts'];
                            $scope.edt['sims_admission_marks_obtained'] = $scope.edt_exam['sims_admission_marks_obtained'];
                            $scope.edt['sims_admission_marks_out_of'] = $scope.edt_exam['sims_admission_marks_out_of'];
                            $scope.edt['sims_admission_percentage_obtained'] = $scope.edt_exam['sims_admission_percentage_obtained'];

                            $scope.edt['current_fyschool_name'] = $scope.edt_exam['current_fyschool_name'];
                            $scope.edt['sims_admission_fyboardname'] = $scope.edt_exam['sims_admission_fyboardname'];
                            $scope.edt['sims_admission_fycenter_name'] = $scope.edt_exam['sims_admission_fycenter_name'];
                            $scope.edt['sims_admission_fypassingyear_exam'] = $scope.edt_exam['sims_admission_fypassingyear_exam'];
                            $scope.edt['sims_admission_fyseat_no'] = $scope.edt_exam['sims_admission_fyseat_no'];
                            $scope.edt['sims_fyadmission_no_of_attempts_sem1'] = $scope.edt_exam['sims_fyadmission_no_of_attempts_sem1'];
                            $scope.edt['sims_fyadmission_marks_obtained_sem1'] = $scope.edt_exam['sims_fyadmission_marks_obtained_sem1'];
                            $scope.edt['sims_fyadmission_marks_out_of_Sem1'] = $scope.edt_exam['sims_fyadmission_marks_out_of_Sem1'];
                            $scope.edt['sims_fyadmission_percentage_obtained_sem1'] = $scope.edt_exam['sims_fyadmission_percentage_obtained_sem1'];

                            $scope.edt['sims_fyadmission_no_of_attempts_sem2'] = $scope.edt_exam['sims_fyadmission_no_of_attempts_sem2'];
                            $scope.edt['sims_fyadmission_marks_obtained_sem2'] = $scope.edt_exam['sims_fyadmission_marks_obtained_sem2'];
                            $scope.edt['sims_fyadmission_marks_out_of_Sem2'] = $scope.edt_exam['sims_fyadmission_marks_out_of_Sem2'];
                            $scope.edt['sims_fyadmission_percentage_obtained_sem2'] = $scope.edt_exam['sims_fyadmission_percentage_obtained_sem2'];

                            $scope.edt['current_syschool_name'] = $scope.edt_exam['current_syschool_name'];
                            $scope.edt['sims_admission_syboardname'] = $scope.edt_exam['sims_admission_syboardname'];
                            $scope.edt['sims_admission_sycenter_name'] = $scope.edt_exam['sims_admission_sycenter_name'];
                            $scope.edt['sims_admission_sypassingyear_exam'] = $scope.edt_exam['sims_admission_sypassingyear_exam'];
                            $scope.edt['sims_admission_syseat_no'] = $scope.edt_exam['sims_admission_syseat_no'];
                            $scope.edt['sims_syadmission_no_of_attempts_sem3'] = $scope.edt_exam['sims_syadmission_no_of_attempts_sem3'];
                            $scope.edt['sims_syadmission_marks_obtained_sem3'] = $scope.edt_exam['sims_syadmission_marks_obtained_sem3'];
                            $scope.edt['sims_syadmission_marks_out_of_Sem3'] = $scope.edt_exam['sims_syadmission_marks_out_of_Sem3'];
                            $scope.edt['sims_syadmission_percentage_obtained_sem3'] = $scope.edt_exam['sims_syadmission_percentage_obtained_sem3'];

                            $scope.edt['sims_syadmission_no_of_attempts_sem4'] = $scope.edt_exam['sims_syadmission_no_of_attempts_sem4'];
                            $scope.edt['sims_syadmission_marks_obtained_sem4'] = $scope.edt_exam['sims_syadmission_marks_obtained_sem4'];
                            $scope.edt['sims_syadmission_marks_out_of_sem4'] = $scope.edt_exam['sims_syadmission_marks_out_of_sem4'];
                            $scope.edt['sims_syadmission_percentage_obtained_sem4'] = $scope.edt_exam['sims_syadmission_percentage_obtained_sem4'];
                        });

                    });


                });

            }
            else
            {
                $http.get($scope.apiurl + "api/common/Admission_dpsd/getAdmissionList").then(function (res) {
                    $scope.obj = res.data;
                    console.log($scope.obj);
                    $scope.scholnm = [], $scope.feecat = [], $scope.legalcus = [], $scope.cur_data = [], $scope.feemonth = [], $scope.acad_yr = [];
                  


                    for (var i = 0; i < res.data.length; i++) {

                        if (res.data[i].school_name != '') {
                            $scope.scholnm.push({ school_name: res.data[i].school_name, school_code: res.data[i].school_code });
                            $scope.edt['school_code'] = $scope.scholnm[0].school_code;
                        }
                        if (res.data[i].curr_name != '') {
                            $scope.cur_data.push({ curr_name: res.data[i].curr_name, curr_code: res.data[i].curr_code });
                            //$scope.edt['school_code'] = $scope.cur_data[0].curr_code;
                        }
                        if (res.data[i].fee_category_desc != '') {
                            $scope.feecat.push({ fee_category_code: res.data[i].fee_category_code, fee_category_desc: res.data[i].fee_category_desc });
                            $scope.edt['fee_category_code'] = $scope.feecat[0].fee_category_code;
                        }
                        if (res.data[i].legal_custody_name != '') {
                            $scope.legalcus.push({ legal_custody_code: res.data[i].legal_custody_code, legal_custody_name: res.data[i].legal_custody_name });
                            $scope.edt['primary_contact_pref_code'] = $scope.legalcus[0].legal_custody_code;
                            $scope.edt['legal_custody_code'] = $scope.legalcus[0].legal_custody_code;
                        }
                        if (res.data[i].sims_fee_month_name != '')
                        {
                            $scope.feemonth.push({ sims_fee_month_code: res.data[i].sims_fee_month_code, sims_fee_month_name: res.data[i].sims_fee_month_name });
                        }

                        if (res.data[i].sims_national_id_code == $rootScope.locale.lang)
                        {
                            $scope.lbl_id.national_id = res.data[i].sims_national_id_name;
                        }
                        if ($scope.obj[i].father_salutation_desc != '') {
                            $scope.fathersalutation.push({ father_salutation_desc: $scope.obj[i].father_salutation_desc, father_salutation_code: $scope.obj[i].father_salutation_code });
                            if ($scope.obj[i].father_country == 'Mr.') {
                                $scope.edt['father_salutation_code'] = $scope.obj[i].father_salutation_code;
                            }
                        }
                        if ($scope.obj[i].mother_salutation_desc != '') {
                            $scope.mothersalutation.push({ mother_salutation_desc: $scope.obj[i].mother_salutation_desc, mother_salutation_code: $scope.obj[i].mother_salutation_code });
                            if ($scope.obj[i].father_country == 'Mrs.') {
                                $scope.edt['mother_salutation_code'] = $scope.obj[i].mother_salutation_code;
                            }
                        }
                     
                    }
                   

                });
            }


      
          
            var adm_date = "", grade_name = "";

         


            //admission



            $http.get($scope.apiurl + "api/Comn/GetIncomeSvcc").then(function (res) {
                $scope.income_lst = res.data
            });

            $http.get($scope.apiurl + "api/Comn/getAdmissionList").then(function (res) {
                $scope.obj = res.data;
                $scope.date1 = $scope.obj[0].admission_date;

                if ($scope.date1 <= '2017-12-12') {
                    $scope.btnadm = false;
                }
                else {
                    $scope.btnadm = true;
                }
            });

            $scope.printDetails = function (str) {


                var data = {
                    location: 'Sims.SIMR41SVVM',
                    parameter: { admission_no: str },
                    // state: 'main.Inv035'Sims.SIMR41BRS
                    reg: param,
                    email: email,
                    ready: function () {
                        this.refreshReport();
                    }
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)

                // printDetails
                $state.go('Report');
            }

            $scope.printConfrimDetails = function (str) {


                var data = {
                    location: 'Gradebook.registration_confirmation',
                    parameter: { admission_no: str },
                    state: 'main.Inv035',
                    reg: param,
                    email: email,
                    ready: function () {
                        this.refreshReport();
                    }
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)

                // printDetails
                $state.go('Report');
            }


            $scope.math_pre = function () {
                if (parseFloat($scope.edt.sims_student_attribute1) > 100)
                    $scope.edt['sims_student_attribute1'] = '';
            }

            $scope.sci_pre = function () {
                if (parseFloat($scope.edt.sims_student_attribute2) > 100)
                    $scope.edt['sims_student_attribute2'] = '';
            }

            $scope.math_pre_x = function () {
                if (parseFloat($scope.edt.sims_fyadmission_no_of_attempts_sem2) > 100)
                    $scope.edt['sims_fyadmission_no_of_attempts_sem2'] = '';
            }

            $scope.sci_pre_x = function () {
                if (parseFloat($scope.edt.sims_fyadmission_marks_obtained_sem2) > 100)
                    $scope.edt['sims_fyadmission_marks_obtained_sem2'] = '';
            }



            $scope.max_mark_hsc = function () {

                if (parseInt($scope.edt.sims_admission_marks_obtained) > parseInt($scope.edt.sims_admission_marks_out_of)) {
                    swal({ title: "Alert", text: "Please Enter Valid Marks.", showCloseButton: true, width: 450, });

                    $scope.edt.sims_admission_marks_obtained = '';
                }
            }

            $scope.max_mark_sem1 = function () {

                if (parseInt($scope.edt.sims_fyadmission_marks_obtained_sem1) > parseInt($scope.edt.sims_fyadmission_marks_out_of_Sem1)) {
                    swal({ title: "Alert", text: "Please Enter Valid Marks.", showCloseButton: true, width: 450, });

                    $scope.edt.sims_fyadmission_marks_obtained_sem1 = '';
                }
            }

            $scope.max_mark_sem2 = function () {

                if (parseInt($scope.edt.sims_fyadmission_marks_obtained_sem2) > parseInt($scope.edt.sims_fyadmission_marks_out_of_Sem2)) {
                    swal({ title: "Alert", text: "Please Enter Valid Marks.", showCloseButton: true, width: 450, });

                    $scope.edt.sims_fyadmission_marks_obtained_sem2 = '';
                }
            }

            $scope.max_mark_sem3 = function () {

                if (parseInt($scope.edt.sims_syadmission_marks_obtained_sem3) > parseInt($scope.edt.sims_syadmission_marks_out_of_Sem3)) {
                    swal({ title: "Alert", text: "Please Enter Valid Marks.", showCloseButton: true, width: 450, });

                    $scope.edt.sims_syadmission_marks_obtained_sem3 = '';
                }
            }


            $scope.max_mark_sem4 = function () {

                if (parseInt($scope.edt.sims_syadmission_marks_obtained_sem4) > parseInt($scope.edt.sims_syadmission_marks_out_of_sem4)) {
                    swal({ title: "Alert", text: "Please Enter Valid Marks.", showCloseButton: true, width: 450, });

                    $scope.edt.sims_syadmission_marks_obtained_sem4 = '';
                }
            }






            $scope.EditDetails = function (adm_no) {
                $scope.edt = "";
                $scope.div_edt = true;
                $scope.div_save = false;
                $scope.admgrid = true;
                $scope.btn_Save = true;
                $scope.btn_NewSave = false;

                $http.get($scope.apiurl + "api/Comn/getAdmissionList").then(function (res) {
                    $scope.obj = res.data;
                    for (var i = 0; i < res.data.length; i++) {

                        if (res.data[i].school_name != '') {
                            $scope.scholnm.push({ school_name: res.data[i].school_name, school_code: res.data[i].school_code });
                            $scope.edt['school_code'] = $scope.scholnm[0].school_code;
                        }
                        if (res.data[i].curr_name != '') {
                            $scope.cur_data.push({ curr_name: res.data[i].curr_name, curr_code: res.data[i].curr_code });
                            $scope.edt['curr_code'] = $scope.cur_data[0].curr_code;
                        }
                        if (res.data[i].academic_year_desc != '') {
                            $scope.acad_yr.push({ academic_year_desc: res.data[i].academic_year_desc, academic_year: res.data[i].academic_year });
                            $scope.edt['academic_year'] = $scope.acad_yr[0].academic_year;
                        }

                        if (res.data[i].sims_city_code != '') {
                            $scope.city_lst.push({ sims_city_code: res.data[i].sims_city_code, sims_city_state_code: res.data[i].sims_city_state_code, sims_city_name_en: res.data[i].sims_city_name_en });
                            // $scope.edt['academic_year'] = $scope.acad_yr[0].academic_year;
                        }
                        if (res.data[i].sims_appl_parameter_castCatcode != '') {
                            $scope.caste_lst.push({ sims_appl_parameter_castCatcode: res.data[i].sims_appl_parameter_castCatcode, sims_appl_form_field_value1_castcatdesc: res.data[i].sims_appl_form_field_value1_castcatdesc });
                            if ($scope.obj[i].sims_appl_parameter_castCatcode == 'GE') {
                                $scope.edt['sims_student_category'] = $scope.obj[i].sims_appl_parameter_castCatcode;
                            }
                        }
                        if (res.data[i].sims_appl_parameter_marital_statuscode != '') {
                            $scope.maritalstatus_lst.push({ sims_appl_parameter_marital_statuscode: res.data[i].sims_appl_parameter_marital_statuscode, sims_appl_form_field_value1_marital_statusdesc: res.data[i].sims_appl_form_field_value1_marital_statusdesc });
                            if ($scope.obj[i].sims_appl_parameter_marital_statuscode == 'U') {
                                $scope.edt['sims_admission_marital_status'] = $scope.obj[i].sims_appl_parameter_marital_statuscode;
                            }
                        }
                    }

                    $http.get($scope.apiurl + "api/common/Admission/GetTabStudentData?admission_number=" + adm_no).then(function (res) {
                        $scope.edt = res.data;
                        $scope.getchkAge();
                        $scope.getsubject();

                        //student previous exam details
                        $http.get($scope.apiurl + "api/common/Admission/GetStudent_PrevExamDetails?admission_number=" + adm_no).then(function (res) {
                            $scope.edt_exam = res.data;
                            // console.log($scope.edt1);
                            $scope.edt['current_school_name'] = $scope.edt_exam['current_school_name'];
                            $scope.edt['sims_admission_boardname'] = $scope.edt_exam['sims_admission_boardname'];
                            $scope.edt['sims_admission_center_name'] = $scope.edt_exam['sims_admission_center_name'];
                            $scope.edt['sims_admission_passingyear_exam'] = $scope.edt_exam['sims_admission_passingyear_exam'];
                            $scope.edt['sims_admission_seat_no'] = $scope.edt_exam['sims_admission_seat_no'];
                            $scope.edt['sims_admission_no_of_attempts'] = $scope.edt_exam['sims_admission_no_of_attempts'];
                            $scope.edt['sims_admission_marks_obtained'] = $scope.edt_exam['sims_admission_marks_obtained'];
                            $scope.edt['sims_admission_marks_out_of'] = $scope.edt_exam['sims_admission_marks_out_of'];
                            $scope.edt['sims_admission_percentage_obtained'] = $scope.edt_exam['sims_admission_percentage_obtained'];

                            $scope.edt['current_fyschool_name'] = $scope.edt_exam['current_fyschool_name'];
                            $scope.edt['sims_admission_fyboardname'] = $scope.edt_exam['sims_admission_fyboardname'];
                            $scope.edt['sims_admission_fycenter_name'] = $scope.edt_exam['sims_admission_fycenter_name'];
                            $scope.edt['sims_admission_fypassingyear_exam'] = $scope.edt_exam['sims_admission_fypassingyear_exam'];
                            $scope.edt['sims_admission_fyseat_no'] = $scope.edt_exam['sims_admission_fyseat_no'];
                            $scope.edt['sims_fyadmission_no_of_attempts_sem1'] = $scope.edt_exam['sims_fyadmission_no_of_attempts_sem1'];
                            $scope.edt['sims_fyadmission_marks_obtained_sem1'] = $scope.edt_exam['sims_fyadmission_marks_obtained_sem1'];
                            $scope.edt['sims_fyadmission_marks_out_of_Sem1'] = $scope.edt_exam['sims_fyadmission_marks_out_of_Sem1'];
                            $scope.edt['sims_fyadmission_percentage_obtained_sem1'] = $scope.edt_exam['sims_fyadmission_percentage_obtained_sem1'];

                            $scope.edt['sims_fyadmission_no_of_attempts_sem2'] = $scope.edt_exam['sims_fyadmission_no_of_attempts_sem2'];
                            $scope.edt['sims_fyadmission_marks_obtained_sem2'] = $scope.edt_exam['sims_fyadmission_marks_obtained_sem2'];
                            $scope.edt['sims_fyadmission_marks_out_of_Sem2'] = $scope.edt_exam['sims_fyadmission_marks_out_of_Sem2'];
                            $scope.edt['sims_fyadmission_percentage_obtained_sem2'] = $scope.edt_exam['sims_fyadmission_percentage_obtained_sem2'];

                            $scope.edt['current_syschool_name'] = $scope.edt_exam['current_syschool_name'];
                            $scope.edt['sims_admission_syboardname'] = $scope.edt_exam['sims_admission_syboardname'];
                            $scope.edt['sims_admission_sycenter_name'] = $scope.edt_exam['sims_admission_sycenter_name'];
                            $scope.edt['sims_admission_sypassingyear_exam'] = $scope.edt_exam['sims_admission_sypassingyear_exam'];
                            $scope.edt['sims_admission_syseat_no'] = $scope.edt_exam['sims_admission_syseat_no'];
                            $scope.edt['sims_syadmission_no_of_attempts_sem3'] = $scope.edt_exam['sims_syadmission_no_of_attempts_sem3'];
                            $scope.edt['sims_syadmission_marks_obtained_sem3'] = $scope.edt_exam['sims_syadmission_marks_obtained_sem3'];
                            $scope.edt['sims_syadmission_marks_out_of_Sem3'] = $scope.edt_exam['sims_syadmission_marks_out_of_Sem3'];
                            $scope.edt['sims_syadmission_percentage_obtained_sem3'] = $scope.edt_exam['sims_syadmission_percentage_obtained_sem3'];

                            $scope.edt['sims_syadmission_no_of_attempts_sem4'] = $scope.edt_exam['sims_syadmission_no_of_attempts_sem4'];
                            $scope.edt['sims_syadmission_marks_obtained_sem4'] = $scope.edt_exam['sims_syadmission_marks_obtained_sem4'];
                            $scope.edt['sims_syadmission_marks_out_of_sem4'] = $scope.edt_exam['sims_syadmission_marks_out_of_sem4'];
                            $scope.edt['sims_syadmission_percentage_obtained_sem4'] = $scope.edt_exam['sims_syadmission_percentage_obtained_sem4'];
                        });

                    });


                });

                $scope.selectdisabledata = true;
                $scope.disabledata = false;
                $scope.btncancel = true;

            }

            function convertdate(dt) {
                if (dt != null) {
                    var d1 = new Date(dt);
                    var month = d1.getMonth() + 1;
                    var day = d1.getDate();
                    if (month < 10)
                        month = "0" + month;
                    if (day < 10)
                        day = "0" + day;
                    var d = d1.getFullYear() + "-" + (month) + "-" + (day);
                    return d;
                }
            }

            $scope.calculate_per = function () {

                $scope.edt['sims_admission_percentage_obtained'] = (parseInt($scope.edt.sims_admission_marks_obtained) / parseInt($scope.edt.sims_admission_marks_out_of)) * 100;
            }

            $scope.calculate_fysem1per = function () {

                $scope.edt['sims_fyadmission_percentage_obtained_sem1'] = (parseInt($scope.edt.sims_fyadmission_marks_obtained_sem1) / parseInt($scope.edt.sims_fyadmission_marks_out_of_Sem1)) * 100;
            }

            $scope.calculate_fysem2per = function () {

                $scope.edt['sims_fyadmission_percentage_obtained_sem2'] = (parseInt($scope.edt.sims_fyadmission_marks_obtained_sem2) / parseInt($scope.edt.sims_fyadmission_marks_out_of_Sem2)) * 100;
            }

            $scope.calculate_sysem3per = function () {

                $scope.edt['sims_syadmission_percentage_obtained_sem3'] = (parseInt($scope.edt.sims_syadmission_marks_obtained_sem3) / parseInt($scope.edt.sims_syadmission_marks_out_of_Sem3)) * 100;
            }

            $scope.calculate_sysem4per = function () {
                $scope.edt['sims_syadmission_percentage_obtained_sem4'] = (parseInt($scope.edt.sims_syadmission_marks_obtained_sem4) / parseInt($scope.edt.sims_syadmission_marks_out_of_sem4)) * 100;
            }


            $scope.subject_up = function (obj) {
                for (var i = 0; i < $scope.subject_lst.length; i++) {
                    if ($scope.subject_lst[i].sims_subject_code == obj.sims_subject_code) {
                        var cnt = i - 1;
                        var temp = angular.copy($scope.subject_lst[i]);
                        var temp_pref = angular.copy($scope.subject_lst[cnt].sims_subject_pref)
                        $scope.subject_lst[i] = $scope.subject_lst[cnt];
                        $scope.subject_lst[i].sims_subject_pref = temp.sims_subject_pref;

                        $scope.subject_lst[cnt] = temp;
                        $scope.subject_lst[cnt].sims_subject_pref = temp_pref;

                        break;

                    }
                }

            }

            $scope.subject_down = function (obj) {

                for (var i = 0; i < $scope.subject_lst.length; i++) {
                    if ($scope.subject_lst[i].sims_subject_code == obj.sims_subject_code) {
                        var cnt = i + 1;

                        var temp = angular.copy($scope.subject_lst[i]);
                        var temp_pref = angular.copy($scope.subject_lst[cnt].sims_subject_pref)
                        $scope.subject_lst[i] = $scope.subject_lst[cnt];
                        $scope.subject_lst[i].sims_subject_pref = temp.sims_subject_pref;

                        $scope.subject_lst[cnt] = temp;
                        $scope.subject_lst[cnt].sims_subject_pref = temp_pref;
                        break;

                    }
                }

            }





            //$scope.selectchange = function () {

            //    $("input.prefar").attr("disabled", true);
            //}

            $scope.getchkDOB = function () {

                for (var i = 0; i < $scope.obj_age.length; i++) {
                    if ($scope.edt.grade_code == '01') {
                        if ($scope.edt.birth_date <= $scope.obj_age[i].sims_birth_date_from) {
                            swal({ title: "Alert", text: "For KG 1 admissions, child must be minimum 3.8 year old and can be maximum 4.11 year old as of 2017-03-31.", showCloseButton: true, width: 450, });
                            //swal({ title: "Birth Date", text: "For KG 1 admissions, child must be minimum 3.8 year old and can be maximum 4.11 year old as of 2017-03-31", showCloseButton: true, width: 480, });
                            $scope.edt.birth_date = "";
                        }
                        if ($scope.edt.birth_date >= $scope.obj_age[i].sims_birth_date_to) {
                            swal({ title: "Birth Date", text: "For KG 1 admissions, child must be minimum 3.8 year old and can be maximum 4.11 year old as of 2017-03-31", showCloseButton: true, width: 450, });
                            $scope.edt.birth_date = "";
                        }
                    }
                    if ($scope.edt.grade_code == '02') {
                        if ($scope.edt.birth_date <= $scope.obj_age[i].sims_birth_date_from) {
                            swal({ title: "Birth Date", text: "For KG 2 admissions, child must be minimum 4.8 year old and can be maximum 5.11 year old as of 2017-03-31", showCloseButton: true, width: 450, });
                            $scope.edt.birth_date = "";
                        }
                        if ($scope.edt.birth_date >= $scope.obj_age[i].sims_birth_date_to) {
                            swal({ title: "Birth Date", text: "For KG 2 admissions, child must be minimum 4.8 year old and can be maximum 5.11 year old as of 2017-03-31", showCloseButton: true, width: 450, });
                            $scope.edt.birth_date = "";
                        }
                    }
                    if ($scope.edt.grade_code == '03') {
                        if ($scope.edt.birth_date <= $scope.obj_age[i].sims_birth_date_from) {
                            swal({ title: "Birth Date", text: "For Grade 1 admissions, child must be minimum 5.8 year old and can be maximum 6.11 year old as of 2017-03-31", showCloseButton: true, width: 480, });
                            $scope.edt.birth_date = "";
                        }
                        if ($scope.edt.birth_date >= $scope.obj_age[i].sims_birth_date_to) {
                            swal({ title: "Birth Date", text: "For Grade 1 admissions, child must be minimum 5.8 year old and can be maximum 6.11 year old as of 2017-03-31", showCloseButton: true, width: 480, });
                            $scope.edt.birth_date = "";
                        }
                    }
                }
            }

            $scope.viewDoc = function () {
                $('#Div1').modal('show');
            }

            $scope.Save = function (isvalidate) {
                // console.log(isvalidate);
                if (isvalidate) {
                    // console.log('fff');

                    if ($scope.btn_NewSave == false) {
                        var data = $scope.edt;
                       // data.red_id = param;
                        data.opr = "U";
                        // console.log($scope.edt.admission_date);
                        var subdata = [];
                        $http.post($scope.apiurl + "api/common/Admission/CUDUpdateAdmission", data).then(function (res) {
                            //$scope.lbl_show = true;
                            //$scope.lbl_msg = "Application Updated Sucessfully";
                            swal({ title: "Alert", text: "Application Updated Sucessfully.", showCloseButton: true, width: 380, });
                            for (var i = 0; i < $scope.subject_lst.length; i++) {
                                var subobj = {
                                    sims_admission_number: data.admission_number,
                                    sims_subject_code: $scope.subject_lst[i].sims_subject_code,
                                    sims_subject_pref: $scope.subject_lst[i].sims_subject_pref,
                                    index: i + 1,
                                    opr: 'C'
                                }
                                subdata.push(subobj)
                            }
                            $http.post($scope.apiurl + "api/common/Admission/SubjectCUD", subdata).then(function (res) {
                            });
                            $scope.show_details();
                            // swal({ title: "Alert", text: "Application Updated Sucessfully", imageUrl: "assets/img/notification-alert.png", },
                            // function () {

                            //  });
                        });
                    }
                    else {
                        //console.log('ddddd');
                        $scope.NewSave(isvalidate);
                    }

                }
                //else
                //{
                //    swal({ title: "Alert", text: "Application incomplete, Kindly fill all the Mandatory Fields.", showCloseButton: true, width: 450, });
                //}
            }

            $scope.viewreport = function () {
                $state.go('Report');
            }

            $scope.Cancel = function () {
                $scope.div_edt = false;
                $scope.div_save = true;
                $scope.Insertmsg1 = "";
                $scope.edt = "";
                $scope.admgrid = false;
                $scope.div_view = false;
                $scope.disableparent = true;
                $scope.lbl_msg = "";
                $scope.lbl_show = false;
                // $state.go('Login');
            }

            $scope.getdate = function (str) {
                console.log(str);

            }

            $scope.GetInfo = function (parentid, enrollno) {

                if (parentid != undefined && enrollno != undefined) {

                    $http.post($scope.apiurl + "api/common/Admission/CheckParentCode?parent_id=" + parentid + "&enroll_no=" + enrollno).then(function (res) {
                        $scope.msg1 = res.data;
                        if ($scope.msg1.status == 'false') {
                            swal({ title: "Alert", text: "Invalid Parent ID and Sibling Details", showCloseButton: true, width: 450, });
                            $scope.edt.parent_id = "";
                            $scope.edt.sibling_enroll = "";
                            return;
                        }

                        var data = $scope.edt;
                        $scope.edt.father_salutation_code = $scope.msg1.father_salutation_code;
                        $scope.edt.father_family_name = $scope.msg1.father_family_name;

                        $scope.edt.father_nationality1_code = $scope.msg1.father_nationality1_code;
                        $scope.edt.father_first_name = $scope.msg1.father_first_name;
                        $scope.edt.father_middle_name = $scope.msg1.father_middle_name;
                        $scope.edt.father_last_name = $scope.msg1.father_last_name;
                        $scope.edt.father_summary_address = $scope.msg1.father_summary_address;
                        $scope.edt.sims_admission_father_national_id = $scope.msg1.sims_admission_father_national_id;
                        $scope.edt.father_mobile = $scope.msg1.father_mobile;
                        $scope.edt.father_email = $scope.msg1.father_email;
                        $scope.edt.father_appartment_number = $scope.msg1.father_appartment_number;
                        $scope.edt.father_building_number = $scope.msg1.father_building_number;
                        $scope.edt.father_street_number = $scope.msg1.father_street_number;
                        $scope.edt.father_area_number = $scope.msg1.father_area_number;
                        $scope.edt.father_city = $scope.msg1.father_city;
                        $scope.edt.father_state = $scope.msg1.father_state;
                        $scope.edt.father_country_code = $scope.msg1.father_country_code;
                        $scope.edt.father_po_box = $scope.msg1.father_po_box;
                        $scope.edt.father_phone = $scope.msg1.father_phone;
                        $scope.edt.father_fax = $scope.msg1.father_fax;


                        $scope.edt.mother_salutation_code = $scope.msg1.mother_salutation_code;
                        $scope.edt.mother_family_name = $scope.msg1.mother_family_name;
                        $scope.edt.mother_first_name = $scope.msg1.mother_first_name;
                        $scope.edt.mother_middle_name = $scope.msg1.mother_middle_name;
                        $scope.edt.mother_last_name = $scope.msg1.mother_last_name;
                        $scope.edt.mother_nationality1_code = $scope.msg1.mother_nationality1_code;
                        $scope.edt.mother_summary_address = $scope.msg1.mother_summary_address;
                        $scope.edt.sims_admission_mother_national_id = $scope.msg1.sims_admission_mother_national_id;
                        $scope.edt.mother_mobile = $scope.msg1.mother_mobile;
                        $scope.edt.mother_email = $scope.msg1.mother_email;

                        $scope.edt.mother_appartment_number = $scope.msg1.mother_appartment_number;
                        $scope.edt.mother_building_number = $scope.msg1.mother_building_number;
                        $scope.edt.mother_street_number = $scope.msg1.mother_street_number;
                        $scope.edt.mother_area_number = $scope.msg1.mother_area_number;
                        $scope.edt.mother_city = $scope.msg1.mother_city;
                        $scope.edt.mother_state = $scope.msg1.mother_state;
                        $scope.edt.mother_country_code = $scope.msg1.mother_country_code;
                        $scope.edt.mother_po_box = $scope.msg1.mother_po_box;
                        $scope.edt.mother_phone = $scope.msg1.mother_phone;
                        $scope.edt.mother_fax = $scope.msg1.mother_fax;
                    });
                }
                else {
                    if (parentid == undefined || enrollno == undefined) {
                        $scope.btncancel = true;
                        $scope.btnok = false;
                        swal({ title: "Alert", text: "Parent Id And Enrollment Number is Required", showCloseButton: true, width: 450, });
                        $scope.edt.parent_id = '';
                        $scope.edt.sibling_enroll = '';
                    }
                }
            }

           

            $scope.valandfocus = function (parentid) {
                if (parentid == undefined || parentid == '') {
                    swal({ title: "Alert", text: "Invalid Parent ID", showCloseButton: true, width: 450, });
                    return;
                }
                document.getElementById('txt_enroll_no').focus();
            }

            $scope.GetEMPInfo = function (empid) {

                if (empid != undefined) {
                    $http.post($scope.apiurl + "api/common/Admission/CheckEMPCode?emp_id=" + empid).then(function (res) {
                        $scope.msg1 = res.data;
                        if ($scope.msg1.chkstatus == false) {
                            swal({ title: "Alert", text: "Invalid Employee Code", showCloseButton: true, width: 450, });
                            $scope.edt.employee_code = "";
                            return;
                        }
                        else {
                            swal({ title: "Alert", text: "Employee ID verification successful", showCloseButton: true, width: 450, });
                            return;
                        }
                    });
                }

            }



            $scope.getFatherDetails = function () {
                if ($scope.edt['father_add_status'] == true) {
                    var data = $scope.edt;
                    var v = document.getElementById('txt_mother_aprt');
                    v.value = document.getElementById('txt_father_aprt').value;
                    data.mother_appartment_number = v.value;

                    var v = document.getElementById('txt_mother_build');
                    v.value = document.getElementById('txt_father_build').value;
                    data.mother_building_number = v.value;

                    var v = document.getElementById('txt_mother_street');
                    v.value = document.getElementById('txt_father_Street').value;
                    data.mother_street_number = v.value;

                    var v = document.getElementById('txt_mother_area');
                    v.value = document.getElementById('txt_father_area').value;
                    data.mother_area_number = v.value;

                    var v = document.getElementById('txt_mother_city');
                    v.value = document.getElementById('txt_father_city').value;
                    data.mother_city = v.value;

                    var v = document.getElementById('txt_mother_state');
                    v.value = document.getElementById('txt_father_state').value;
                    data.mother_state = v.value;

                    var v = document.getElementById('cmb_mother_country');
                    v.value = document.getElementById('cmb_father_country').value;
                    data.mother_country_code = v.value;

                    var v = document.getElementById('txt_mother_pobox');
                    v.value = document.getElementById('txt_father_pobox').value;
                    data.mother_po_box = v.value;
                }
                else {
                    $scope.edt['mother_appartment_number'] = '';
                    $scope.edt['mother_building_number'] = '';
                    $scope.edt['mother_street_number'] = '';
                    $scope.edt['mother_area_number'] = '';
                    $scope.edt['mother_city'] = '';
                    $scope.edt['mother_state'] = '';
                    $scope.edt['mother_country_code'] = '';
                    $scope.edt['mother_po_box'] = '';
                }
            }
            var date1;
            var date3;

            //$scope.createdate = function (date) {

            //    console.log(date);
            //    var month = date.split("/")[0];
            //    var day = date.split("/")[1];
            //    var year = date.split("/")[2];
            //    var date1 = year + "/" + month + "/" + day;
            //    console.log(date1);
            //  //  $scope.edt.sims_academic_year_start_date = date1;

            //}


            $scope.createdate = function (date) {
                // console.log(date);
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                date1 = year + "/" + month + "/" + day;
                // console.log(date1);

                $scope.CurrentDate = new Date();
                var now = new Date();
                var month1 = (now.getMonth() + 1);
                var day1 = now.getDate();
                var year1 = now.getFullYear();
                if (month1 < 10)
                    month1 = "0" + month1;
                if (day1 < 10)
                    day1 = "0" + day1;
                date3 = now.getFullYear() + '/' + month1 + '/' + day1;
                var date4 = (now.getFullYear() - 4) + '/' + month1 + '/' + day1;
                //var year2 = (year1 - 4);
                // console.log(date4);

                if (date1 >= date3 || date1 <= date4) {
                    swal({ title: "Birth Date", text: "Age Should Not be less than 4 yrs or greater than current year", imageUrl: "assets/img/notification-alert.png", });
                    var v = document.getElementById('dp_birth_date');
                    v.value = "";
                }
            }

            $scope.chk_applied_date = function () {
                var v = document.getElementById('chk_applied_date');
                if (v.checked == true) {
                    var v = document.getElementById('dp_star_school');
                    v.disabled = false;
                }
                else {
                    var v = document.getElementById('dp_star_school');
                    v.disabled = true;
                }
            }

            $scope.previous_details = function () {
                var v = document.getElementById('chk_prev_school');
                if (v.checked == true) {
                    $scope.prev = false;
                }
                else {
                    $scope.prev = true;
                }
            }

            $scope.chk_years = function () {
                var v = document.getElementById('chk_years');
                if (v.checked == true) {
                    var v = document.getElementById('txt_years');
                    v.disabled = false;

                }
                else {
                    var v = document.getElementById('txt_years');
                    v.disabled = true;
                }
            }

            $scope.chk_gifted = function () {
                var v = document.getElementById('chk_gifted');
                if (v.checked == true) {
                    var v = document.getElementById('txt_gifted');
                    v.disabled = false;

                }
                else {
                    var v = document.getElementById('txt_gifted');
                    v.disabled = true;
                }
            }

            $scope.chk_behaviour_sup = function () {
                var v = document.getElementById('chk_behaviour_sup');
                if (v.checked == true) {
                    var v = document.getElementById('txt_behaviour_sup');
                    v.disabled = false;

                }
                else {
                    var v = document.getElementById('txt_behaviour_sup');
                    v.disabled = true;
                }
            }

            $scope.chk_learn_diff = function () {
                var v = document.getElementById('chk_learn_diff');
                if (v.checked == true) {
                    var v = document.getElementById('txt_learn_diff');
                    v.disabled = false;
                }
                else {
                    var v = document.getElementById('txt_learn_diff');
                    v.disabled = true;
                }
            }

            $scope.chk_medical_cond = function () {
                var v = document.getElementById('chk_medical_cond');
                if (v.checked == true) {
                    var v = document.getElementById('txt_medical_cond');
                    v.disabled = false;
                }
                else {
                    var v = document.getElementById('txt_medical_cond');
                    v.disabled = true;
                }
            }


            $scope.chk_more_sibling = function () {
                var v = document.getElementById('chk_more_sibling');
                if (v.checked == true) {
                    var v = document.getElementById('txt_more_sibling');
                    v.disabled = false;
                }
                else {
                    var v = document.getElementById('txt_more_sibling');
                    v.disabled = true;
                }
            }

            //$scope.chk_employee = function ()
            //{
            //    if ($scope.edt.sims_empchk == true)
            //    {
            //        var v = document.getElementById('txt_emp_code');
            //        v.disabled = false;
            //    }
            //    else
            //    {
            //        var v = document.getElementById('txt_emp_code');
            //        v.disabled = true;
            //    }
            //}


            $scope.NewAdmission = function () {
                $scope.scholnm = [], $scope.cur_data = [], $scope.acad_yr = [];
                $scope.edt = "";
                $scope.div_edt = false;
                $scope.div_save = true;
                $scope.disableparent = true;
                $scope.lbl_msg = "";
                $scope.lbl_show = false;

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.selectdisabledata = false;
                $scope.admgrid = true;
                $scope.btn_Save = false;
                $scope.btn_NewSave = true;

                $scope.edt =
                {
                    sibling_status: false,
                    sims_empchk: false,
                    sims_admission_is_freedom_fighter: 'N',
                    sims_admission_is_physically_handicapped: 'N'

                }
                $scope.chk_Sibling_attend();
                //$scope.chk_employee();




                $http.get($scope.apiurl + "api/Comn/getAdmissionList").then(function (res) {
                    $scope.obj = res.data;
                    $scope.date = $scope.obj[0].admission_date;
                    $scope.edt['admission_date'] = $scope.date;
                    for (var i = 0; i < res.data.length; i++) {

                        if (res.data[i].school_name != '') {
                            $scope.scholnm.push({ school_name: res.data[i].school_name, school_code: res.data[i].school_code });
                            $scope.edt['school_code'] = $scope.scholnm[0].school_code;
                        }
                        if (res.data[i].curr_name != '') {
                            $scope.cur_data.push({ curr_name: res.data[i].curr_name, curr_code: res.data[i].curr_code });
                            $scope.edt['curr_code'] = $scope.cur_data[0].curr_code;
                        }
                        if (res.data[i].academic_year_desc != '') {
                            $scope.acad_yr.push({ academic_year_desc: res.data[i].academic_year_desc, academic_year: res.data[i].academic_year });
                            $scope.edt['academic_year'] = $scope.acad_yr[0].academic_year;
                        }

                        if (res.data[i].sims_city_code != '') {
                            $scope.city_lst.push({ sims_city_code: res.data[i].sims_city_code, sims_city_state_code: res.data[i].sims_city_state_code, sims_city_name_en: res.data[i].sims_city_name_en });
                            // $scope.edt['academic_year'] = $scope.acad_yr[0].academic_year;
                        }
                        if (res.data[i].sims_appl_parameter_castCatcode != '') {
                            $scope.caste_lst.push({ sims_appl_parameter_castCatcode: res.data[i].sims_appl_parameter_castCatcode, sims_appl_form_field_value1_castcatdesc: res.data[i].sims_appl_form_field_value1_castcatdesc });
                            if ($scope.obj[i].sims_appl_parameter_castCatcode == 'GE') {
                                $scope.edt['sims_student_category'] = $scope.obj[i].sims_appl_parameter_castCatcode;
                            }
                        }
                        if (res.data[i].sims_appl_parameter_marital_statuscode != '') {
                            $scope.maritalstatus_lst.push({ sims_appl_parameter_marital_statuscode: res.data[i].sims_appl_parameter_marital_statuscode, sims_appl_form_field_value1_marital_statusdesc: res.data[i].sims_appl_form_field_value1_marital_statusdesc });
                            if ($scope.obj[i].sims_appl_parameter_marital_statuscode == 'U') {
                                $scope.edt['sims_admission_marital_status'] = $scope.obj[i].sims_appl_parameter_marital_statuscode;
                            }
                        }
                    }
                    // console.log($scope.date);
                    // $scope.edt['admission_date'] = moment($scope.date).format("YYYY-DD-MM");
                    //$scope.edt['admission_date'] = $filter('date')($scope.date, 'yyyy-MM-dd');
                    //console.log($scope.obj);
                });





            }


            $scope.chk_Sibling_attend = function () {
                if ($scope.edt.sibling_status == true) {
                    $scope.disableparent = false;
                }
                else {
                    $scope.disableparent = true;
                }
            }

            $scope.viewAdmission = function () {
                $scope.div_view = true;
                $scope.lbl_msg = "";
                $scope.lbl_show = false;
                // $scope.btnview = true;
            }

            $scope.cancelModaldisplay = function () {
                // $scope.btnview = false;
                $scope.div_view = false;
                $scope.selectdisabledata = false;

            }


            $scope.NewSaveF = function () {
             
                var data = $scope.edt;
                data.red_id = param;
                data.email_id = email;
                data.opr = "I";
                var subdata = []
                $http.post($scope.apiurl + "api/common/Admission/CUDInsertAdmission", data).then(function (res) {
                    $scope.Insertmsg1 = res.data;

                    if (res.data) {
                        data.admission_number = $scope.Insertmsg1.admission_number;
                        data.email_id = $scope.Insertmsg1.email_id;
                        data.grade_name = document.getElementById('cmb_grade').options[document.getElementById('cmb_grade').selectedIndex].text;

                       
                        $scope.lbl_show = true;
                        $scope.lbl_msg = "Application Submitted Sucessfully with application number " + data.admission_number;

                        for (var i = 0; i < $scope.subject_lst.length; i++) {
                            var subobj = {
                                sims_admission_number: data.admission_number,
                                sims_subject_code: $scope.subject_lst[i].sims_subject_code,
                                sims_subject_pref: $scope.subject_lst[i].sims_subject_pref,
                                index: i + 1,
                                opr: 'C'
                            }
                            subdata.push(subobj)
                        }
                        $http.post($scope.apiurl + "api/common/Admission/SubjectCUD", subdata).then(function (res) {
                        });
                        // swal({ title: "Alert", text: "Application Submitted Sucessfully", imageUrl: "assets/img/notification-alert.png", },
                        //   function () {
                        $scope.show_details();
                        //   });
                    }
                    $scope.Insertmsg1 = "";
                    $scope.edt = "";
                });

            }
            $scope.NewSave = function (isvalidate) {

                if (isvalidate) {

                    if ($scope.edt.grade_code == '13') {
                        if ($scope.edt.current_fyschool_name == undefined) {
                            swal({ title: "Alert", text: "F.Y Last Institute Name is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.current_fyschool_name == "") {
                            swal({ title: "Alert", text: "F.Y Last Institute Name is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_obtained_sem1 == undefined) {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-I is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_obtained_sem1 == "") {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-I is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_out_of_Sem1 == undefined) {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-I is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_out_of_Sem1 == "") {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-I is Required.", showCloseButton: true, width: 450, });
                        }

                        else if ($scope.edt.sims_fyadmission_marks_obtained_sem2 == undefined) {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-II is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_obtained_sem2 == "") {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-II is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_out_of_Sem2 == undefined) {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-II is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_out_of_Sem2 == "") {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-II is Required.", showCloseButton: true, width: 450, });
                        }

                        else if ($scope.edt.current_syschool_name == undefined) {
                            swal({ title: "Alert", text: "S.Y Last Institute Name is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.current_syschool_name == "") {
                            swal({ title: "Alert", text: "S.Y Last Institute Name is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_syadmission_marks_obtained_sem3 == undefined) {
                            swal({ title: "Alert", text: "S.Y Last Marks Obtained Sem-III is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_syadmission_marks_obtained_sem3 == "") {
                            swal({ title: "Alert", text: "S.Y Last Marks Obtained Sem-III is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_syadmission_marks_obtained_sem4 == undefined) {
                            swal({ title: "Alert", text: "S.Y Last Marks Obtained Sem-IV is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_syadmission_marks_obtained_sem4 == "") {
                            swal({ title: "Alert", text: "S.Y Last Marks Obtained Sem-IV is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_syadmission_marks_out_of_sem4 == undefined) {
                            swal({ title: "Alert", text: "S.Y Last Marks Obtained Sem-IV is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_syadmission_marks_out_of_sem4 == "") {
                            swal({ title: "Alert", text: "S.Y Last Marks Obtained Sem-IV is Required.", showCloseButton: true, width: 450, });
                        }
                        else {
                            $scope.NewSaveF();
                        }

                    }

                    else if ($scope.edt.grade_code == '12') {
                        if ($scope.edt.current_fyschool_name == undefined) {
                            swal({ title: "Alert", text: "F.Y Last Institute Name is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.current_fyschool_name == "") {
                            swal({ title: "Alert", text: "F.Y Last Institute Name is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_obtained_sem1 == undefined) {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-I is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_obtained_sem1 == "") {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-I is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_out_of_Sem1 == undefined) {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-I is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_out_of_Sem1 == "") {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-I is Required.", showCloseButton: true, width: 450, });
                        }

                        else if ($scope.edt.sims_fyadmission_marks_obtained_sem2 == undefined) {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-II is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_obtained_sem2 == "") {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-II is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_out_of_Sem2 == undefined) {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-II is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_out_of_Sem2 == "") {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-II is Required.", showCloseButton: true, width: 450, });
                        }


                        else {
                            $scope.NewSaveF();
                        }

                    }

                    else {
                        $scope.NewSaveF();
                    }

                }
                else {
                    swal({ title: "Alert", text: "Application incomplete, Kindly fill all the Mandatory Fields.", showCloseButton: true, width: 450, });
                }
            }

            $scope.NewSave1 = function (isvalidate) {

                if (isvalidate) {
                    if ($scope.edt.sibling_attend == true) {
                        if ($scope.edt.parent_id == undefined) {
                            swal({ title: "Alert", text: "Parent Id And Enroll No are Required", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.parent_id == "") {
                            swal({ title: "Alert", text: "Parent Id And Enroll No are Required", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sibling_enroll == undefined) {
                            swal({ title: "Alert", text: "Parent Id And Enroll No are Required", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sibling_enroll == "") {
                            swal({ title: "Alert", text: "Parent Id And Enroll No are Required", showCloseButton: true, width: 450, });
                        }
                        else {
                           
                            var data = $scope.edt;
                            data.red_id = param;
                            data.email_id = email;
                            data.opr = "I";

                            $http.post($scope.apiurl + "api/common/Admission/CUDInsertAdmission", data).then(function (res) {
                                $scope.Insertmsg1 = res.data;

                                if (res.status == "Error") {
                                    $scope.lbl_msg = "APPLICATION FORM SUBMITTED SUCCESSFULLY BUT WITH OUT APPLICATION NUMBER";
                                }
                                else {
                                    data.admission_number = $scope.Insertmsg1.admission_number;
                                    data.email_id = $scope.Insertmsg1.email_id;
                                    data.grade_name = document.getElementById('cmb_grade').options[document.getElementById('cmb_grade').selectedIndex].text;

                                    
                                    $scope.lbl_show = true;
                                    $scope.lbl_msg = "Application Submitted Sucessfully with application number " + data.admission_number;
                                    //  swal({ title: "Alert", text: "Application Submitted Sucessfully", imageUrl: "assets/img/notification-alert.png", },
                                    //function ()
                                    //{
                                    $scope.show_details();
                                    // });
                                }
                                $scope.Insertmsg1 = "";
                                $scope.edt = "";
                            });
                        }
                    }
                    else {
                        
                        var data = $scope.edt;
                        data.red_id = param;
                        data.email_id = email;
                        data.opr = "I";

                        var subdata = []



                        $http.post($scope.apiurl + "api/common/Admission/CUDInsertAdmission", data).then(function (res) {
                            $scope.Insertmsg1 = res.data;

                            if (res.data) {
                                data.admission_number = $scope.Insertmsg1.admission_number;
                                data.email_id = $scope.Insertmsg1.email_id;
                                data.grade_name = document.getElementById('cmb_grade').options[document.getElementById('cmb_grade').selectedIndex].text;

                               
                                $scope.lbl_show = true;
                                $scope.lbl_msg = "Application Submitted Sucessfully with application number " + data.admission_number;

                                for (var i = 0; i < $scope.subject_lst.length; i++) {
                                    var subobj = {
                                        sims_admission_number: data.admission_number,
                                        sims_subject_code: $scope.subject_lst[i].sims_subject_code,
                                        sims_subject_pref: $scope.subject_lst[i].sims_subject_pref,
                                        index: i + 1,
                                        opr: 'C'
                                    }
                                    subdata.push(subobj)
                                }
                                $http.post($scope.apiurl + "api/common/Admission/SubjectCUD", subdata).then(function (res) {
                                });
                                // swal({ title: "Alert", text: "Application Submitted Sucessfully", imageUrl: "assets/img/notification-alert.png", },
                                //   function () {
                                $scope.show_details();
                                //   });
                            }
                            $scope.Insertmsg1 = "";
                            $scope.edt = "";
                        });
                    }
                }
                else {
                    // swal({ title: "Alert", text: "Application incomplete, Kindly fill all the Mandatory Fields.", imageUrl: "assets/img/notification-alert.png", });
                    //"Kindly Fill all the requirement details"

                    swal({ title: "Alert", text: "Application incomplete, Kindly fill all the Mandatory Fields.", showCloseButton: true, width: 450, });
                }
            }

            $scope.UploadDocument = function (adm_no) {

                $('#viewdashDetailsModal').modal('show');

                $http.get($scope.apiurl + "api/common/AdmissionDashboard/GetCriteriaName?admission_num=" + adm_no).then(function (res) {
                    $scope.Upload_doc_datails = res.data;
                    $scope.Upload_doc_datails1 = res.data;
                    console.log($scope.Upload_doc_datails1);
                    $scope.totalItems3 = $scope.Upload_doc_datails.length;
                    $scope.todos3 = $scope.Upload_doc_datails;
                    for (var i = 0; i < $scope.totalItems3; i++) {
                        $scope.Upload_doc_datails[i] = res.data[i];
                        $scope.visible = [];
                        $scope.visible = res.data[i].sims_admission_doc_path.split(",");
                        $scope.Upload_doc_datails[i].path = $scope.visible;
                    }
                    $scope.Upload_doc = true;
                    $scope.maingrid1 = false;
                });
            }


            /*start Upload*/

            var formdata = new FormData();

            $scope.getTheFiles = function ($files) {

                $scope.filesize = true;

                angular.forEach($files, function (value, key) {
                    //console.log('VALUES',value);
                    formdata.append(key, value);
                    var i = 0;
                    if ($files[i].size > 200000) {
                        $scope.filesize = false;
                        $scope.edt1.photoStatus = false;
                        swal({ title: "Alert", text: "File Should Not Exceed 200Kb.", imageUrl: "assets/img/notification-alert.png", });
                    }
                });
                console.log('VALUES', formdata.append.length);

            };

            $scope.uploadClick = function (str) {
                console.log(str);
                $scope.filesize = true;
                $scope.edt1 = str;
                console.log($scope.edt1);
                $scope.edt1.photoStatus = true;
                $scope.ins = false;
                formdata = new FormData();
            }

            $scope.file_changed = function (element, str) {
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;
                    });
                };

                reader.readAsDataURL(photofile);

                if (element.files[0].size < 200000) {
                    console.log($scope.edt1.count);
                    if ($scope.edt1.count < 2) {
                        console.log($scope.edt1.sims_admission_doc_path);
                        if ($scope.edt1.sims_admission_doc_path == null || $scope.edt1.sims_admission_doc_path == "") {
                            $scope.edt1.count = $scope.edt1.count;
                        }
                        else {
                            $scope.edt1.count = ($scope.edt1.count) + 1;
                        }

                        // if ($scope.edt1.count < 2)
                        {
                            var request_DATA = {
                                method: 'POST',
                                url: $scope.apiurl + 'api/fileNew/uploadDocument?filename=' + $scope.edt1.admis_num + '_' + $scope.edt1.sims_criteria_code + '_' + $scope.edt1.count + '(' + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss') + ')' + "&location=Docs/Student",
                                data: formdata,
                                headers: {
                                    'Content-Type': undefined
                                }
                            };
                            $http(request_DATA).success(function (d) {
                                var data = {
                                    admis_num: $scope.edt1.admis_num,
                                    sims_criteria_code: $scope.edt1.sims_criteria_code,
                                    sims_admission_doc_path: d,
                                    sims_admission_doc_path_old: $scope.edt1.sims_admission_doc_path
                                }

                                if (data.sims_admission_doc_path != null || data.sims_admission_doc_path != "") {
                                    if ($scope.edt1.sims_admission_doc_path == null || $scope.edt1.sims_admission_doc_path == "") {
                                        console.log($scope.edt1.count);
                                        data.sims_admission_doc_status = 'false';
                                        data.sims_admission_doc_verify = 'false';
                                        data.opr = 'N';

                                        $http.post($scope.apiurl + "api/common/AdmissionDashboard/CUD_Update_Admission_Doc", data).then(function (res) {
                                            $scope.ins = res.data;
                                            if (res.data) {
                                                swal({ title: "Alert", text: "Document Updated Successfully", showCloseButton: true, width: 380, });
                                                $scope.edt1.count = ($scope.edt1.count) + 1;
                                            }

                                            $scope.UploadDocument($scope.edt1.admis_num);
                                        });
                                    }
                                    else {
                                        console.log($scope.edt1.count);
                                        $http.post($scope.apiurl + "api/common/AdmissionDashboard/CUD_Insert_Admission_Doc", data).then(function (res) {
                                            $scope.ins = res.data;
                                            if (res.data) {
                                                swal({ title: "Alert", text: "Document Uploaded Successfully", showCloseButton: true, width: 380, });
                                                $scope.edt1.count = ($scope.edt1.count) + 1;
                                            }

                                            $scope.UploadDocument($scope.edt1.admis_num);
                                        });
                                    }
                                }

                            });
                        }
                    }
                    else {
                        swal({ title: "Alert", text: "You Can Upload Max 2 File Documents For Each Criteria", showCloseButton: true, width: 380, });

                    }

                }
            };

            $scope.add_upload_doc = function () {
                var data = $scope.Upload_doc_datails;
                $http.post($scope.apiurl + "api/common/AdmissionDashboard/CUD_Update_Admission_DocList", data).then(function (res) {
                    $scope.ins = res.data;
                });
            }

            $scope.UploadDocModal_Reset = function () {
                $('#viewdashDetailsModal').modal('hide');
                $scope.maingrid1 = true;
                $scope.Upload_doc = false;
            }

            $scope.doc_delete = function (doc_path, crit_code, adm_no) {

                swal({
                    title: '',
                    text: "Are you sure you want to Delete?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        $http.post($scope.apiurl + "api/common/AdmissionDashboard/CUD_Delete_Admission_Doc?adm_no=" + adm_no + "&criteria_code=" + crit_code + "&doc_path=" + doc_path).then(function (res) {
                            $scope.del = res.data;
                            if (res.data) {
                                swal({ title: "Alert", text: "Document Deleted Successfully", showCloseButton: true, width: 380, });
                            }
                            $scope.UploadDocument(adm_no);
                        });
                    }
                });
            }

            /*End Upload*/

            $scope.show_details = function () {

                $scope.admgrid = false;
                $http.get($scope.apiurl + "api/common/Admission/getAdmissionNoDetails?sims_admission_parent_REG_id=" + param).then(function (res) {
                    $scope.adm = res.data;

                    status = $scope.adm.sims_admission_status;
                    //if ($scope.adm.length <= 0)
                    {
                        $scope.btnadm = true;

                        $scope.strMessage = "";
                        $('#message').modal('hide');
                        $scope.btncancel = true;
                        $scope.btnok = false;
                    }
                    //  else
                    {
                        //$scope.btnadm = false;

                    }
                });
            }

            $("#txt_year,#txt_fyyear,#txt_syyear").datepicker({
                format: "MM-yyyy",
                viewMode: "months",
                minViewMode: "months"
            });


            $("#dp_comm_date,  #dp_admdate, #dp_birth_date").kendoDatePicker({
                format: "yyyy-MM-dd"
                //format: "dd/MM/yyyy"
                //format: "yyyy/MM/dd"
            });



            $scope.GetFatherSumAddr = function () {
                var data = $scope.edt;

                var father_aprt = $("#txt_father_aprt").val().length;
                var father_build = $("#txt_father_build").val().length;
                var father_street = $("#txt_father_Street").val().length;
                var father_area = $("#txt_father_area").val().length;
                var father_city = $("#txt_father_city").val().length;
                var father_state = $("#txt_father_state").val().length;
                var father_country = $("#cmb_father_country").val().length;
                var father_POBox = $("#txt_father_pobox").val().length;

                var country = document.getElementById('cmb_father_country').options[document.getElementById('cmb_father_country').selectedIndex].text;

                document.getElementById('txt_father_summary_add').value = $("#txt_father_aprt").val().length > 0 ? document.getElementById('txt_father_aprt').value + "," : "";
                document.getElementById('txt_father_summary_add').value += $("#txt_father_build").val().length > 0 ? document.getElementById('txt_father_build').value + "," : "";
                document.getElementById('txt_father_summary_add').value += $("#txt_father_Street").val().length > 0 ? document.getElementById('txt_father_Street').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_area > 0 ? document.getElementById('txt_father_area').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_city > 0 ? document.getElementById('txt_father_city').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_state > 0 ? document.getElementById('txt_father_state').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_country > 0 ? country + "," : "";
                document.getElementById('txt_father_summary_add').value += father_POBox > 0 ? document.getElementById('txt_father_pobox').value : "";

                var v = document.getElementById('txt_father_summary_add');
                data.father_summary_address = v.value;
                // console.log(data.father_summary_address);
            }


        


        }]);

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])
})();