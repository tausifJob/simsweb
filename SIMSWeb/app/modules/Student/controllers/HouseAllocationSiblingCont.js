﻿
(function () {
    'use strict';
    var demo = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('HouseAllocationSiblingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.notshow = true;
           
            $scope.pager = true;
            $scope.busyindicator = true;
            $scope.not_student = false;
            $scope.btn_submit = false;
            $scope.edit_code = false;
            $scope.pagesize = "10";
            $scope.pageindex = "0";
            $scope.main = true;
            $scope.grid = false;
            $scope.showdata = false;
            $scope.showhouse = true;
            $scope.temp = {};
            var data1 = [];
            var username = $rootScope.globals.currentUser.username;
            //$http.get(ENV.apiUrl + "api/HouseAllocation/getStudentNameHA?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_code).then(function (allSectionStudent) {
            //    $scope.Studentdata = allSectionStudent.data;

            $scope.showdetails = function () {
               
                debugger;
                $http.get(ENV.apiUrl + "api/HouseAllocation/getallHouseFilterhome?curcode=" + $scope.edt.sims_cur_code + "&academicyear=" + $scope.edt.sims_academic_year + "&search_block=" + $scope.search_block).then(function (res2) {
                    debugger;
                    $scope.Housedata = res2.data;
                    $scope.edt.house_name = $scope.Housedata[0].house_name;
                    console.log($scope.Housedata);
                    $scope.pager = true;

                });
            }


            $http.get(ENV.apiUrl + "api/HouseAllocation/getAllhouseName").then(function (res1) {
                //debugger;
                $scope.AllHousedata = res1.data;

            });
            

            $scope.getdetails = function (str) {
               
                debugger;
                //$http.get(ENV.apiUrl + "api/common/common_seq/getAllAcademicYear?cur=" + cur).then(function (res) {
                $http.get(ENV.apiUrl + "api/HouseAllocation/getallgetHousesibling?studentlist=" + str).then(function (res1) {
                    $scope.Housedata = res1.data;
                    //$scope.Houseinfo = res1.data;
                    $scope.edt.house_name = $scope.Housedata[0].house_name;
                    $scope.pager = true;
                     $scope.todos = $scope.Housedata;
                $scope.makeTodos();
                    
                   
                });
            }
           // $scope.getcur = function () {
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                    $scope.curriculum = AllCurr.data;
                    $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                    $scope.getAccYear($scope.edt.sims_cur_code);
                    $scope.getARule($scope.edt.sims_cur_code);
                });
            //}
            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.edt.sims_cur_code).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;

                    $scope.edt['sims_academic_year'] = $scope.Acc_year[0].sims_academic_year;
                    $scope.getGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                });
            }
            $scope.getGrade = function (curCode, accYear) {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
            } 
            $scope.editdata = function ()
            {
                
                $scope.showdata = true;
                $scope.showhouse = false;
            }
            $scope.edit = function (str) {
                debugger;
                $scope.display = true;
                $scope.grid = true;
                $scope.main = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_code = true;
                $scope.edt =
                   {
                     
                       sims_parent_number: str.sims_parent_number,
                       parent_name: str.parent_name,
                   }
             
                $http.get(ENV.apiUrl + "api/HouseAllocation/getallgetHousesiblingParent?studentlist=" + str.sims_parent_number).then(function (res1) {
                    $scope.Housedata = res1.data;
                    console.log("Housedata", $scope.Housedata);
                });

            }
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 1000;

            $scope.makeTodos = function () {
                alert('maketodos');
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.Homeroom_MultipleDelete();
                }
            };

            $scope.size = function (str) {
                //alert(str);
                debugger;
                if (str == "All") {
                    alert(str);
                    alter($scope.Housedata.length);
                    $scope.currentPage = 1;
                    $scope.numPerPage = $scope.Housedata.length;
                    //alert(numPerPage);

                    $scope.makeTodos();

                }
                else {
                    alert(str);
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                   // alert(numPerPage);
                    $scope.makeTodos();
                    $scope.pager = true;
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();

            }

            
            $scope.UpdateSibling = function () {
                debugger;
                var data1 = [];
               
                    var data = ({
                        house_name: $scope.temp.house_name,
                        //  sims_parent_number: $scope.edt.sims_parent_number,
                        sims_parent_number: $scope.search_block,
                        opr: 'P'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/HouseAllocation/CUDSiblingUpdate", data1).then(function (res) {
                        $scope.main = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "House Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    //$scope.getgrid();
                                   // $scope.main = false;
                                   // $scope.grid = false;
                                    $scope.cancel();
                                }
                            });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "House Not Updated Successfully. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
               
            }
            $scope.cancel = function () {
                $scope.display = false;
                $scope.grid = false;
                $scope.main = true;

                $http.get(ENV.apiUrl + "api/HouseAllocation/getallgetHousesibling?studentlist=" + '').then(function (res1) {
                    $scope.Housedata = res1.data;
                });
            }
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].subject_group_code);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].subject_group_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }



        }]);




})();