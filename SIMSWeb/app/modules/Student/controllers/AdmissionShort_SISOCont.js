﻿
(function () {
    'use strict';
    var del = [];
    var main;
    var date1, date3, date4;

    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AdmissionShort_SISOCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {
            $scope.display = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.grid = true;
            $scope.save1 = true;
            $scope.parent_edit = true;
            $scope.father_edit = true;
            $scope.Chk_parent = false;
            //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');

            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');
            var data2 = [];
            $rootScope.visible_stud = false;
            $rootScope.visible_parent = true;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = false;
            $rootScope.chkMulti = false;
            $scope.chk_searchshow = false;
            $scope.chk_show = true;
            $scope.BUSY = false;
            $scope.msg1 = [];

            $scope.edt =
              {
                  admission_date: $scope.ddMMyyyy,
                  tent_join_date: $scope.ddMMyyyy,
                  comm_date: $scope.ddMMyyyy,
              }

            $http.get(ENV.apiUrl + "api/common/ProspectDashboard/GetPreviousSchool").then(function (res) {
                $scope.school_lst = res.data
            });

            $http.get(ENV.apiUrl + "api/common/Admission/getAdmissionList").then(function (res) {
                $scope.obj = res.data;
                console.log($scope.obj);
                $scope.scholnm = [], $scope.feecat = [], $scope.legalcus = [], $scope.feemonth = [];

                for (var i = 0; i < res.data.length; i++) {
                    if (res.data[i].school_name != '') {
                        $scope.scholnm.push({ school_name: res.data[i].school_name, school_code: res.data[i].school_code });
                        $scope.edt['school_code'] = $scope.scholnm[0].school_code;
                    }
                    if (res.data[i].fee_category_desc != '') {
                        $scope.feecat.push({ fee_category_code: res.data[i].fee_category_code, fee_category_desc: res.data[i].fee_category_desc });
                        $scope.edt['fee_category_code'] = $scope.feecat[0].fee_category_code;
                    }
                    if (res.data[i].legal_custody_name != '') {
                        $scope.legalcus.push({ legal_custody_code: res.data[i].legal_custody_code, legal_custody_name: res.data[i].legal_custody_name });
                        $scope.edt['legal_custody_code'] = $scope.legalcus[0].legal_custody_code;
                    }
                    if (res.data[i].sims_fee_month_name != '') {
                        $scope.feemonth.push({ sims_fee_month_code: res.data[i].sims_fee_month_code, sims_fee_month_name: res.data[i].sims_fee_month_name });
                        // $scope.edt['sims_fee_month_code'] = $scope.feemonth[0].sims_fee_month_code;
                    }
                }


            });

            $scope.chkgetAdmissionList = function ()
            {
                $http.get(ENV.apiUrl + "api/common/Admission/getAdmissionList").then(function (res) {
                    $scope.obj = res.data;
                    console.log($scope.obj);
                    $scope.scholnm = [], $scope.feecat = [], $scope.legalcus = [], $scope.feemonth = [];

                    for (var i = 0; i < res.data.length; i++) {
                        if (res.data[i].school_name != '') {
                            $scope.scholnm.push({ school_name: res.data[i].school_name, school_code: res.data[i].school_code });
                            $scope.edt['school_code'] = $scope.scholnm[0].school_code;
                        }
                        if (res.data[i].fee_category_desc != '') {
                            $scope.feecat.push({ fee_category_code: res.data[i].fee_category_code, fee_category_desc: res.data[i].fee_category_desc });
                            $scope.edt['fee_category_code'] = $scope.feecat[0].fee_category_code;
                        }
                        if (res.data[i].legal_custody_name != '') {
                            $scope.legalcus.push({ legal_custody_code: res.data[i].legal_custody_code, legal_custody_name: res.data[i].legal_custody_name });
                            $scope.edt['legal_custody_code'] = $scope.legalcus[0].legal_custody_code;
                        }
                        if (res.data[i].sims_fee_month_name != '') {
                            $scope.feemonth.push({ sims_fee_month_code: res.data[i].sims_fee_month_code, sims_fee_month_name: res.data[i].sims_fee_month_name });
                            // $scope.edt['sims_fee_month_code'] = $scope.feemonth[0].sims_fee_month_code;
                        }
                    }


                });
            }

            $scope.GetInfo = function (parentid, enrollno) {
                $http.post(ENV.apiUrl + "api/common/Admission/CheckParentCode?parent_id=" + parentid + "&enroll_no=" + enrollno).then(function (res) {
                    $scope.msg1 = res.data;
                    console.log($scope.msg1);

                    $scope.edt.father_first_name = $scope.msg1.father_first_name;
                    $scope.edt.father_middle_name = $scope.msg1.father_middle_name;
                    $scope.edt.father_last_name = $scope.msg1.father_last_name;
                    $scope.edt.father_summary_address = $scope.msg1.father_summary_address;
                    $scope.edt.father_mobile = $scope.msg1.father_mobile;
                    $scope.edt.father_email = $scope.msg1.father_email;
                    $scope.edt.sims_student_enroll_number = $scope.msg1.sims_student_enroll_number;
                    $scope.edt.sims_admission_father_national_id = $scope.msg1.sims_parent_father_national_id;
                    $scope.edt.father_nationality1_code = $scope.msg1.father_nationality1_code;

                    //$scope.edt.mother_first_name = $scope.msg1.mother_first_name;
                    //$scope.edt.mother_middle_name = $scope.msg1.mother_middle_name;
                    //$scope.edt.mother_last_name = $scope.msg1.mother_last_name;
                    //$scope.edt.mother_summary_address = $scope.msg1.mother_summary_address;
                    //$scope.edt.mother_mobile = $scope.msg1.mother_mobile;
                    //$scope.edt.mother_email = $scope.msg1.mother_email;
                    //$scope.edt.sims_admission_mother_national_id = $scope.msg1.sims_parent_mother_national_id;
                    //$scope.edt.mother_nationality1_code = $scope.msg1.mother_nationality1_code;

                    //$scope.edt.guardian_first_name = $scope.msg1.guardian_first_name;
                    //$scope.edt.guardian_middle_name = $scope.msg1.guardian_middle_name;
                    //$scope.edt.guardian_last_name = $scope.msg1.guardian_last_name;
                    //$scope.edt.guardian_summary_address = $scope.msg1.guardian_summary_address;
                    //$scope.edt.guardian_mobile = $scope.msg1.guardian_mobile_no;
                    //$scope.edt.guardian_email = $scope.msg1.guardian_email_id;
                });
            }

            function convertdate(dt) {
                if (dt != null) {
                    var d1 = new Date(dt);
                    var month = d1.getMonth() + 1;
                    var day = d1.getDate();
                    if (month < 10)
                        month = "0" + month;
                    if (day < 10)
                        day = "0" + day;
                    var d = d1.getFullYear() + "-" + (month) + "-" + (day);
                    return d;
                }
            }

            function calculateDate(date) {
                date1 = convertdate(date);

                $scope.CurrentDate = new Date();
                var now = new Date();
                var month = (now.getMonth() + 1);
                var day = now.getDate();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                date3 = now.getFullYear() + '-' + month + '-' + day;
                console.log(date3);
            }

            $scope.createdate = function (date)
            {
                //calculateDate(date);

                var d1 = moment(date, "DD-MM-YYYY").format('YYYY-MM-DD');
                var d2 = moment($scope.ddMMyyyy, "DD-MM-YYYY").format('YYYY-MM-DD');

                if (d1 <= d2)
                {

                    swal({ title: "Alert", text: "Please Select Future Date", showCloseButton: true, width: 380, });
                    $scope.edt.passport_expiry = "";
                }


            }

            $scope.createdate1 = function (date)
            {
                var d1 = moment(date, "DD-MM-YYYY").format('YYYY-MM-DD');
                var d2 = moment($scope.ddMMyyyy, "DD-MM-YYYY").format('YYYY-MM-DD');

                if (d1 <= d2)
                {
                    swal({ title: "Alert", text: "Please Select Future Date", showCloseButton: true, width: 380, });
                    $scope.edt.national_id_expiry_date = "";
                }

            }

            $scope.$on('global_cancel', function (str) {
                console.log($scope.SelectedUserLst);
                $scope.Empmodal_cancel();
            });

            $scope.getparent = function () {
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }

            $scope.Empmodal_cancel = function () {
                for (var i = 0; i < $scope.SelectedUserLst.length; i++) {
                    $scope.edt.sibling_enroll = $scope.SelectedUserLst[i].s_enroll_no;
                    $scope.edt.sims_parent_number = $scope.SelectedUserLst[i].s_parent_id;
                }
                $scope.GetInfo($scope.edt.sims_parent_number, $scope.edt.sibling_enroll);
            }

            $scope.get_legaldetails = function (legal_custody) {

                $scope.msg1 = "";

                if (legal_custody == 'F') {
                    $scope.edt.father_first_name = $scope.msg1.father_first_name;
                    $scope.edt.father_middle_name = $scope.msg1.father_middle_name;
                    $scope.edt.father_last_name = $scope.msg1.father_last_name;
                    $scope.edt.father_summary_address = $scope.msg1.father_summary_address;
                    $scope.edt.father_mobile = $scope.msg1.father_mobile;
                    $scope.edt.father_email = $scope.msg1.father_email;
                    $scope.edt.sims_admission_father_national_id = $scope.msg1.sims_parent_father_national_id;
                    $scope.edt.father_nationality1_code = $scope.msg1.father_nationality1_code;
                }

                if (legal_custody == 'M')
                {
                    $scope.edt.father_first_name = $scope.msg1.mother_first_name;
                    $scope.edt.father_middle_name = $scope.msg1.mother_middle_name;
                    $scope.edt.father_last_name = $scope.msg1.mother_last_name;
                    $scope.edt.father_summary_address = $scope.msg1.mother_summary_address;
                    $scope.edt.father_mobile = $scope.msg1.mother_mobile;
                    $scope.edt.father_email = $scope.msg1.mother_email;
                    $scope.edt.sims_admission_father_national_id = $scope.msg1.sims_parent_mother_national_id;
                    $scope.edt.father_nationality1_code = $scope.msg1.mother_nationality1_code;
                }
                if (legal_custody == 'G') {
                    $scope.edt.father_first_name = $scope.msg1.guardian_first_name;
                    $scope.edt.father_middle_name = $scope.msg1.guardian_middle_name;
                    $scope.edt.father_last_name = $scope.msg1.guardian_last_name;
                    $scope.edt.father_summary_address = $scope.msg1.guardian_summary_address;
                    $scope.edt.father_mobile = $scope.msg1.guardian_mobile_no;
                    $scope.edt.father_email = $scope.msg1.guardian_email_id;
                    $scope.edt.sims_admission_father_national_id = $scope.msg1.sims_parent_guardian_national_id;
                    $scope.edt.father_nationality1_code = $scope.msg1.guardian_nationality1_code;
                }
            }

            $scope.Save = function (isvalidate) {

                var data2 = [];

                if (isvalidate) {
                    $scope.BUSY = true;
                    if ($scope.edt.legal_custody_code == 'F') {
                        $scope.edt.father_first_name = $scope.edt.father_first_name;
                        $scope.edt.father_last_name = $scope.edt.father_last_name;
                        $scope.edt.father_middle_name = $scope.edt.father_middle_name;
                        $scope.edt.father_mobile = $scope.edt.father_mobile;
                        $scope.edt.father_email = $scope.edt.father_email;
                        $scope.edt.father_summary_address = $scope.edt.father_summary_address;
                        $scope.edt.father_nationality1_code = $scope.edt.father_nationality1_code;
                        $scope.edt.sims_parent_father_national_id = $scope.edt.sims_parent_father_national_id;
                    }
                    if ($scope.edt.legal_custody_code == 'M') {
                        $scope.edt.mother_first_name = $scope.edt.father_first_name;
                        $scope.edt.mother_last_name = $scope.edt.father_last_name;
                        $scope.edt.mother_middle_name = $scope.edt.father_middle_name;
                        $scope.edt.mother_mobile = $scope.edt.father_mobile;
                        $scope.edt.mother_email = $scope.edt.father_email;
                        $scope.edt.mother_summary_address = $scope.edt.father_summary_address;
                        $scope.edt.mother_nationality1_code = $scope.edt.father_nationality1_code;
                        $scope.edt.sims_parent_mother_national_id = $scope.edt.sims_parent_father_national_id;
                    }
                    if ($scope.edt.legal_custody_code == 'G') {
                        $scope.edt.guardian_first_name = $scope.edt.father_first_name;
                        $scope.edt.guardian_last_name = $scope.edt.father_last_name;
                        $scope.edt.guardian_middle_name = $scope.edt.father_middle_name;
                        $scope.edt.guardian_mobile = $scope.edt.father_mobile;
                        $scope.edt.guardian_email = $scope.edt.father_email;
                        $scope.edt.guardian_summary_address = $scope.edt.father_summary_address;
                        $scope.edt.guardian_nationality1_code = $scope.edt.father_nationality1_code;
                        $scope.edt.sims_parent_guardian_national_id = $scope.edt.sims_parent_father_national_id;
                    }

                    var data = ({
                        appl_num: $scope.edt.appl_num,
                        comm_date: $scope.edt.comm_date,
                        school_code: $scope.edt.school_code,
                        admission_date: $scope.edt.admission_date,
                        tent_join_date: $scope.edt.tent_join_date,
                        first_name: $scope.edt.first_name,
                        middle_name: $scope.edt.middle_name,
                        last_name: $scope.edt.last_name,
                        birth_country_code: $scope.edt.birth_country_code,
                        gender_code: $scope.edt.gender_code,
                        passport_num: $scope.edt.passport_num,
                        passport_expiry: $scope.edt.passport_expiry,
                        current_school_name: $scope.edt.current_school_name,
                        national_id: $scope.edt.national_id,
                        national_id_expiry_date: $scope.edt.national_id_expiry_date,
                        sims_student_emergency_contact_name1: $scope.edt.sims_student_emergency_contact_name1,
                        sims_student_emergency_contact_number1: $scope.edt.sims_student_emergency_contact_number1,
                        sibling_enroll: $scope.edt.sibling_enroll,
                        parent_id: $scope.edt.sims_parent_number,
                        curr_code: $scope.edt.curr_code,
                        academic_year: $scope.edt.academic_year,
                        grade_code: $scope.edt.grade_code,
                        section_code: $scope.edt.section_code,
                        term_code: $scope.edt.term_code,
                        legal_custody_code: $scope.edt.legal_custody_code,
                        father_first_name: $scope.edt.father_first_name,
                        father_last_name: $scope.edt.father_last_name,
                        father_middle_name: $scope.edt.father_middle_name,
                        father_mobile: $scope.edt.father_mobile,
                        father_email: $scope.edt.father_email,
                        father_summary_address: $scope.edt.father_summary_address,
                        father_nationality1_code: $scope.edt.father_nationality1_code,
                        sims_admission_father_national_id: $scope.edt.sims_admission_father_national_id,
                        mother_first_name: $scope.edt.mother_first_name,
                        mother_last_name: $scope.edt.mother_last_name,
                        mother_middle_name: $scope.edt.mother_middle_name,
                        mother_mobile: $scope.edt.mother_mobile,
                        mother_email: $scope.edt.mother_email,
                        mother_summary_address: $scope.edt.mother_summary_address,
                        mother_nationality1_code: $scope.edt.mother_nationality1_code,
                        sims_admission_mother_national_id: $scope.edt.sims_admission_mother_national_id,
                        guardian_first_name: $scope.edt.guardian_first_name,
                        guardian_last_name: $scope.edt.guardian_last_name,
                        guardian_middle_name: $scope.edt.guardian_middle_name,
                        guardian_mobile: $scope.edt.guardian_mobile,
                        guardian_email: $scope.edt.guardian_email,
                        guardian_summary_address: $scope.edt.guardian_summary_address,
                        guardian_nationality1_code: $scope.edt.guardian_nationality1_code,
                        sims_admission_guardian_national_id: $scope.edt.sims_admission_guardian_national_id,
                        fee_category_code: $scope.edt.fee_category_code,
                        sibling_status: $scope.edt.sibling_status,
                        nationality_code: $scope.edt.nationality_code,
                        birth_date: $scope.edt.birth_date,
                        sims_fee_month_code: $scope.edt.sims_fee_month_code,
                        opr: 'I'
                    });
                    data2.push(data);

                    console.log(data2);

                    //$http.post(ENV.apiUrl + "api/common/Admission/CUDInsertAdmission", data2).then(function (res) {
                    //    $scope.display = true;
                    //    $scope.msg1 = res.data;

                      //  data2.admission_number = $scope.msg1.admission_number;
                        $http.post(ENV.apiUrl + "api/common/Admission/ApproveStudent", data2).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            $scope.BUSY = false;
                            //var enroll = $scope.msg1[0].enroll;

                            if ($scope.msg1.length > 0) {
                                var data1 = $scope.msg1;
                                data1.strMessage = "Sucessfully Admitted \n Enroll='" + data1[0].enroll + "' \n Student Name='" + data1[0].stud_full_name + "'\n Parent Id='" + data1[0].parent_id;
                                swal({ title: "Alert", text: data1.strMessage, showCloseButton: true, width: 380, });

                                $scope.Cancel();
                            }
                        });

                   // });
                }
            }

            $scope.check1 = function () {
                if ($scope.edt.sibling_status == true) {
                    $scope.parent_edit = false;
                    $scope.Chk_parent = true;
                    $scope.chk_searchshow = true;
                    $scope.chk_show = false;
                }
                else {
                    $scope.parent_edit = true;
                    $scope.Chk_parent = false;
                    $scope.chk_searchshow = false;
                    $scope.chk_show = true;
                }
            }

            $scope.birthdate = function (birth_date)
            {
                var d1 = moment(birth_date, "DD-MM-YYYY").format('YYYY-MM-DD');
                var d2 = moment($scope.ddMMyyyy, "DD-MM-YYYY").format('YYYY-MM-DD');

                if (d1 >= d2)
                {

                    swal({ title: "Date", text: "DOB should be less than the Current date.", showCloseButton: true, width: 450, });
                    $scope.edt.birth_date = "";
                }
            }

            $scope.Cancel = function () {
                $scope.edt = [];
                $scope.msg1 = [];
                var data2 = [];
                $scope.BUSY = false;
                $scope.Chk_parent = false;
                $scope.chk_searchshow = false;
                $scope.chk_show = true;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

                $scope.edt =
            {
                admission_date: $scope.ddMMyyyy,
                tent_join_date: $scope.ddMMyyyy,
                comm_date: $scope.ddMMyyyy,
            }

                $scope.chkgetAdmissionList();
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


         }])
})();