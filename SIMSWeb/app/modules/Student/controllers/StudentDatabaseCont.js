﻿(function () {
    'use strict';
    var medical_info;
    var parent_info; var temp, photo_filename;

    var del = [];
    var main;

    var sims_father_image, sims_mother_image, sims_guardian_image, student_image;

    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

    simsController.controller('StudentDatabaseCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.Restriction = true;
            $scope.demo = true;
            $scope.readonlyParentLoginCode = true;
            $scope.table1 = true;
            $scope.hide = true;
            $scope.EmploymentNo = true;
            $scope.editmode = false;
            var formdata = new FormData();
            $scope.read1 = true;
            $scope.read2 = true;
            $scope.read3 = true;
            $scope.read4 = true;
            $scope.read5 = true;
            $scope.Medication = true;
            $scope.Disability = true;
            $scope.Hearing = true;
            $scope.Vision = true;
            $scope.Other = true;
            $scope.student_info = {};
            //var v = document.getElementById('cmb_company_emp');
            // v.disabled = true;
            $scope.dynamicLableFlag = false;
            $scope.sisCollegeLableFlag = false;
            $scope.motherTongueMandatory = false;
            $scope.mobileNoPrefixGES = false;
            $scope.mobileNoPrefixOES = false;

            var lable_flag = ['pearl', 'siscollege'];

            //$('#dp_start_date').data('kendoDatePicker').enable(false);
            //$('#f_date').data('kendoDatePicker').enable(false);


            $scope.img_url_path = 'https://api.mograsys.com/kindoapi/Content/' + $http.defaults.headers.common['schoolId'] + '/';

            //if (lable_flag.includes($http.defaults.headers.common['schoolId'])) {
            //    $scope.dynamicLableFlag = true;
            //    $scope.sisCollegeLableFlag = true;
            //}

            if ($http.defaults.headers.common['schoolId'] == 'sis') {
                $scope.emiratesbtnshow = true
            }

            if ($http.defaults.headers.common['schoolId'] == 'sok') {
                $scope.fullnameshow = true;
            } else {
                $scope.fullnameshow = false;
            }


            if ($http.defaults.headers.common['schoolId'] == 'eiama' || $http.defaults.headers.common['schoolId'] == 'eiam' || $http.defaults.headers.common['schoolId'] == 'eiand' || $http.defaults.headers.common['schoolId'] == 'oes') {
                $scope.joined_date = false;
                $scope.commenced_date = false;
                $('#dp_start_date').data('kendoDatePicker').enable(true);
                $('#f_date').data('kendoDatePicker').enable(true);

            }
            else {
                $scope.joined_date = true;
                $scope.commenced_date = true;
                $('#dp_start_date').data('kendoDatePicker').enable(false);
                $('#f_date').data('kendoDatePicker').enable(false);

            }

            if ($http.defaults.headers.common['schoolId'] == 'siscollege' || $http.defaults.headers.common['schoolId'] == 'svcc') {
                $scope.nationalIdLabelDetails = "Aadhar Id Details";
                $scope.nationalIdLabel = "Aadhar Id";
            }
            else if ($http.defaults.headers.common['schoolId'] == 'pearl') {
                $scope.nationalIdLabelDetails = "Qatar Id Details";
                $scope.nationalIdLabel = "Qatar Id";
            }
            else {
                $scope.nationalIdLabelDetails = "National Id Details";
                $scope.nationalIdLabel = "National Id";
            }

            if ($http.defaults.headers.common['schoolId'] == 'iis') {
                $scope.motherTongueMandatory = true;
            }

            if ($http.defaults.headers.common['schoolId'] == 'ges') {
                $scope.mobileNoPrefixGES = true;
                $scope.mobileNoPrefixOES = false;
            }
            else if ($http.defaults.headers.common['schoolId'] == 'oes') {
                $scope.mobileNoPrefixOES = true;
                $scope.mobileNoPrefixGES = false;
            }
            else {
                $scope.mobileNoPrefixGES = false;
                $scope.mobileNoPrefixOES = false;
            }

            $scope.globalSearch = function () {
                debugger;
                $rootScope.visible_stud = true;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = false;
                $rootScope.chkMulti = false;

                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                $scope.enrollno = '';
            }

            $scope.$on('global_cancel', function (str) {

                console.log($scope.SelectedUserLst);
                if ($scope.SelectedUserLst.length > 0) {
                    $scope.enrollno = $scope.SelectedUserLst[0].s_enroll_no;
                    $scope.SearchStudentData();
                }
            });

            $scope.searchGlobalClick = function () {
                $scope.Global_Search_by_student();
            }


            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;
                console.log($scope.ComboBoxValues)
                $scope.temp = {
                    sims_academic_year: $scope.ComboBoxValues[0].sims_acadmic_year,
                    s_cur_code: $scope.ComboBoxValues[0].sims_cur_code,
                };
            });

            $scope.access_nos = function (event) {
                if (event.key == "Enter") {
                    $scope.SearchStudentData();
                }
            }

            $http.get(ENV.apiUrl + "api/studentdatabase/").then(function (lbl) {
                $scope.lbl = lbl.data;
                $scope.lbl_no = $scope.lbl[0].sims_student_lbl;

            });
            $http.get(ENV.apiUrl + "api/studentdatabase/getSalutation").then(function (sal) {
                $scope.Salutations = sal.data;
            });


            if ($http.defaults.headers.common['schoolId'] == 'siso') {
                $http.get(ENV.apiUrl + "api/studentdatabase/GetPreviousSchool").then(function (result) {
                    $scope.prevSchools = result.data;
                });
            }

            $scope.SearchStudentData = function () {
                debugger;
                $(document).ready(function () {
                    $(window).keydown(function (event) {
                        if (event.keyCode == 13) {
                            event.preventDefault();
                            return false;
                        }
                    });
                });

                if ($scope.enrollno == null || $scope.enrollno == "" || $scope.enrollno == undefined) {
                    $scope.demo = false;
                    $scope.searchtable = false;
                    $scope.readonlyParentLoginCode = true;
                    $('.nav-tabs a[href="#personal_information"]').tab('show')
                    //$('#myModal').modal('show');

                    $scope.Myform.$setPristine();
                    $scope.Myform.$setUntouched();
                    $rootScope.visible_stud = true;
                    $rootScope.visible_parent = false;
                    $rootScope.visible_search_parent = false;
                    $rootScope.visible_teacher = false;
                    $rootScope.visible_User = false;
                    $rootScope.visible_Employee = false;
                    $rootScope.chkMulti = false;
                    $scope.global_Search_click();

                    $('#Global_Search_Modal').modal({ backdrop: "static" });

                    $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                        debugger
                        $scope.ComboBoxValues = AllComboBoxValues.data;
                        $scope.temp = {
                            sims_academic_year: $scope.ComboBoxValues[0].sims_acadmic_year,
                            s_cur_code: $scope.ComboBoxValues[0].sims_cur_code
                        };
                    });

                    $scope.student = [];
                    $scope.enrollno = '';
                    $scope.temp.search_std_grade_name = '';
                    $scope.temp.search_std_section_name = '';
                    $scope.temp.search_std_passport_no = '';
                    $scope.temp.search_std_family_name = '';
                    $scope.temp.sims_nationality_name_en = '';
                    $scope.temp.std_national_id = '';
                    $scope.temp.s_cur_code = '';
                    $scope.temp.sims_academic_year = '';
                }
                else {
                    $scope.demo = false;
                    $scope.readonlyParentLoginCode = true;
                    $scope.parent_info = {};
                    $scope.medical_info = {};
                    $scope.student_info = {};
                    $scope.Sibling_Details = [];
                    $scope.sims_student_image = '';
                    $scope.sims_admisison_father_image = '';
                    $scope.sims_admisison_mother_image = '';
                    $scope.sims_admisison_guardian_image = '';
                    var dt = new Date();
                    var currentDate = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();

                    $http.get(ENV.apiUrl + "api/studentdatabase/get_Sims_Student_Details?enroll_no=" + $scope.enrollno).then(function (Student_Details) {
                        debugger
                        $scope.student_data = Student_Details.data;
                        console.log("Student_Details.data", Student_Details.data);

                        //$(document).ready(function () {
                        //    var selectIds = $('#collapseOne,#collapseTwo,#collapseThree,#collapseFour,#collapseFive,#collapseSix,#collapseSeven');
                        //    selectIds.removeClass("in").addClass("in");
                        //    $("#personal_info_icon").removeClass('glyphicon-plus').addClass('glyphicon-minus');                            
                        //});

                        if ($scope.student_data.length == 0) {
                            swal({ text: 'Student ID not found', width: 300, showCloseButton: true, timer: 4000 });
                            return false;
                        }
                        $scope.student_info = $scope.student_data[0];
                        console.log("student_info", $scope.student_info);

                        $scope.sims_student_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/' + $scope.student_info['sims_student_image'];

                        var school_flag = ['sms', 'sis'];

                        if (school_flag.includes($http.defaults.headers.common['schoolId'])) {
                            if ($scope.student_data[0].sims_student_primary_contact_pref == undefined || $scope.student_data[0].sims_student_primary_contact_pref == "" || $scope.student_data[0].sims_student_primary_contact_pref == null) {
                                $scope.student_info['sims_student_primary_contact_pref'] = "F";
                            }
                            else {
                                $scope.student_info['sims_student_primary_contact_pref'] = $scope.student_data[0].sims_student_primary_contact_pref;
                            }
                        }


                        //if ($scope.student_data[0].sims_student_dob == undefined || $scope.student_data[0].sims_student_dob == "" || $scope.student_data[0].sims_student_dob == null) {
                        //    $scope.student_info.sims_student_dob = currentDate;
                        //}
                        //else {
                        //    $scope.student_info.sims_student_dob = $scope.student_data[0].sims_student_dob;
                        //}

                        //if ($scope.student_data[0].sims_student_date == undefined || $scope.student_data[0].sims_student_date == "" || $scope.student_data[0].sims_student_date == null) {
                        //    $scope.student_info.sims_student_date = currentDate;
                        //}
                        //else {
                        //    $scope.student_info.sims_student_date = $scope.student_data[0].sims_student_date;
                        //}
                        //if ($scope.student_data[0].sims_student_commence_date == undefined || $scope.student_data[0].sims_student_commence_date == "" || $scope.student_data[0].sims_student_commence_date == null) {
                        //    $scope.student_info.sims_student_commence_date = currentDate;
                        //}
                        //else {
                        //    $scope.student_info.sims_student_commence_date = $scope.student_data[0].sims_student_commence_date;
                        //}
                        //if (isNaN($scope.student_data[0].sims_student_passport_issue_date) || $scope.student_data[0].sims_student_passport_issue_date == undefined || $scope.student_data[0].sims_student_passport_issue_date == "" || $scope.student_data[0].sims_student_passport_issue_date == null) {
                        //    $scope.student_info.sims_student_passport_issue_date = currentDate;
                        //}
                        //else {
                        //    $scope.student_info.sims_student_passport_issue_date = $scope.student_data[0].sims_student_passport_issue_date;
                        //}
                        //if (isNaN($scope.student_data[0].sims_student_passport_expiry_date) || $scope.student_data[0].sims_student_passport_expiry_date == undefined || $scope.student_data[0].sims_student_passport_expiry_date == "" || $scope.student_data[0].sims_student_passport_expiry_date == null) {
                        //    $scope.student_info.sims_student_passport_expiry_date = currentDate;
                        //}
                        //else {
                        //    $scope.student_info.sims_student_passport_expiry_date = $scope.student_data[0].sims_student_passport_expiry_date;
                        //}
                        //console.log("sims_student_passport_expiry_date", $scope.student_info.sims_student_passport_expiry_date);

                        //if (isNaN($scope.student_data[0].sims_student_visa_issue_date) || $scope.student_data[0].sims_student_visa_issue_date == undefined || $scope.student_data[0].sims_student_visa_issue_date == "" || $scope.student_data[0].sims_student_visa_issue_date == null) {
                        //    $scope.student_info.sims_student_visa_issue_date = currentDate;
                        //}
                        //else {
                        //    $scope.student_info.sims_student_visa_issue_date = $scope.student_data[0].sims_student_visa_issue_date;
                        //}
                        //if (isNaN($scope.student_data[0].sims_student_visa_expiry_date) || $scope.student_data[0].sims_student_visa_expiry_date == undefined || $scope.student_data[0].sims_student_visa_expiry_date == "" || $scope.student_data[0].sims_student_visa_expiry_date == null) {
                        //    $scope.student_info.sims_student_visa_expiry_date = currentDate;
                        //}
                        //else {
                        //    $scope.student_info.sims_student_visa_expiry_date = $scope.student_data[0].sims_student_visa_expiry_date;
                        //}

                        //if (isNaN($scope.student_data[0].sims_student_national_id_issue_date) || $scope.student_data[0].sims_student_national_id_issue_date == undefined || $scope.student_data[0].sims_student_national_id_issue_date == "" || $scope.student_data[0].sims_student_national_id_issue_date == null) {
                        //    $scope.student_info.sims_student_national_id_issue_date = currentDate;
                        //}
                        //else {
                        //    $scope.student_info.sims_student_national_id_issue_date = $scope.student_data[0].sims_student_national_id_issue_date;
                        //}

                        //if (isNaN($scope.student_data[0].sims_student_national_id_expiry_date) || $scope.student_data[0].sims_student_national_id_expiry_date == undefined || $scope.student_data[0].sims_student_national_id_expiry_date == "" || $scope.student_data[0].sims_student_national_id_expiry_date == null) {
                        //    $scope.student_info.sims_student_national_id_expiry_date = currentDate;
                        //}
                        //else {
                        //    $scope.student_info.sims_student_national_id_expiry_date = $scope.student_data[0].sims_student_national_id_expiry_date;
                        //}

                        //if (isNaN($scope.student_data[0].sims_student_ea_registration_date) || $scope.student_data[0].sims_student_ea_registration_date == undefined || $scope.student_data[0].sims_student_ea_registration_date == "" || $scope.student_data[0].sims_student_ea_registration_date == null) {
                        //    $scope.student_info.sims_student_ea_registration_date = currentDate;
                        //}
                        //else {
                        //    $scope.student_info.sims_student_ea_registration_date = $scope.student_data[0].sims_student_ea_registration_date;
                        //}

                        $scope.GiftedStatus($scope.student_info.sims_student_gifted_status);
                        $scope.MusicProficiency($scope.student_info.sims_student_music_status);
                        $scope.SportStatus($scope.student_info.sims_student_sports_status);
                        $scope.LanguageSupportStatus($scope.student_info.sims_student_language_support_status);
                        $scope.BehaviourStatus($scope.student_info.sims_student_behaviour_status);

                    });


                    $http.get(ENV.apiUrl + "api/studentdatabase/get_Sims_Sibling_Details?enroll_no=" + $scope.enrollno).then(function (SiblingDetails) {
                        $scope.Sibling_Details = SiblingDetails.data;


                    });


                    $http.get(ENV.apiUrl + "api/studentdatabase/get_Student_Medical_Details?enroll_no=" + $scope.enrollno).then(function (Student_Medical_Details) {
                        $scope.Medical_Details = Student_Medical_Details.data;
                        $scope.medical_info = {};
                        if ($scope.Medical_Details.length > 0) {
                            $scope.medical_info = $scope.Medical_Details[0];
                        }
                        else {
                            $scope.medical_info.student_enroll_no = $scope.enrollno;
                        }

                        $scope.MedicationStatus($scope.medical_info.sims_medication_status);

                        $scope.DisabilityStatus($scope.medical_info.sims_disability_status);

                        $scope.RestrictionStatus($scope.medical_info.student_health_restriction_status);

                        $scope.HearingStatus($scope.medical_info.sims_health_hearing_status);

                        $scope.VisionStatus($scope.medical_info.sims_health_vision_status);

                        $scope.OtherStatus($scope.medical_info.sims_health_other_status);




                    });

                    debugger
                    $http.get(ENV.apiUrl + "api/studentdatabase/getParentDetails?enroll_no=" + $scope.enrollno).then(function (ParentDetails) {
                        $scope.Parent_Details = ParentDetails.data;
                        $scope.parent_info = ParentDetails.data[0];
                        debugger
                        $scope.getstatefather();
                        $scope.getcityfather();

                        //$scope.getstatemother($scope.parent_info.sims_admission_mother_country_code);
                        //$scope.getcitymother($scope.parent_info.sims_admission_mother_state);

                        //$scope.getstateguardian($scope.parent_info.sims_admission_guardian_country_code);
                        //$scope.getcityguardian($scope.parent_info.sims_admission_guardian_state);


                        $scope.isparentschoolemployee($scope.parent_info.sims_parent_is_employment_status);

                        $scope.student_father_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.parent_info.sims_admisison_father_image;
                        $scope.student_mother_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.parent_info.sims_admisison_mother_image;
                        $scope.student_guardian_image1 = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.parent_info.sims_admisison_guardian_image;

                        if ($scope.Parent_Details[0].sims_admission_father_salutation_code == undefined || $scope.Parent_Details[0].sims_admission_father_salutation_code == "" || $scope.Parent_Details[0].sims_admission_father_salutation_code == null) {
                            $scope.parent_info['sims_admission_father_salutation_code'] = 'Mr.';
                        }
                        else {
                            $scope.parent_info['sims_admission_father_salutation_code'] = $scope.Parent_Details[0].sims_admission_father_salutation_code;
                        }
                        if ($scope.Parent_Details[0].sims_admission_mother_salutation_code == undefined || $scope.Parent_Details[0].sims_admission_mother_salutation_code == "" || $scope.Parent_Details[0].sims_admission_mother_salutation_code == null) {
                            $scope.parent_info['sims_admission_mother_salutation_code'] = "Mrs.";
                        }
                        else {
                            $scope.parent_info['sims_admission_mother_salutation_code'] = $scope.Parent_Details[0].sims_admission_mother_salutation_code;
                        }
                        if ($scope.Parent_Details[0].sims_admission_guardian_salutation_code == undefined || $scope.Parent_Details[0].sims_admission_guardian_salutation_code == "" || $scope.Parent_Details[0].sims_admission_guardian_salutation_code == null) {
                            $scope.parent_info['sims_admission_guardian_salutation_code'] = "Mr.";
                        }
                        else {
                            $scope.parent_info['sims_admission_guardian_salutation_code'] = $scope.Parent_Details[0].sims_admission_guardian_salutation_code;
                        }
                        $scope.parent_info['sims_admission_guardian_area_number'] = $scope.Parent_Details[0].sims_admission_guardian_area_number;
                        $scope.parent_info['sims_municipality_name_en'] = $scope.Parent_Details[0].sims_admission_mother_area_number;

                        //if (isNaN($scope.Parent_Details[0].sims_parent_father_passport_issue_date) || $scope.Parent_Details[0].sims_parent_father_passport_issue_date == undefined || $scope.Parent_Details[0].sims_parent_father_passport_issue_date == "" || $scope.Parent_Details[0].sims_parent_father_passport_issue_date == null) {
                        //    $scope.parent_info.sims_parent_father_passport_issue_date = currentDate;
                        //}
                        //else {
                        //    $scope.parent_info.sims_parent_father_passport_issue_date = $scope.Parent_Details[0].sims_parent_father_passport_issue_date;
                        //}

                        //if (isNaN($scope.Parent_Details[0].sims_parent_father_passport_expiry_date) || $scope.Parent_Details[0].sims_parent_father_passport_expiry_date == undefined || $scope.Parent_Details[0].sims_parent_father_passport_expiry_date == "" || $scope.Parent_Details[0].sims_parent_father_passport_expiry_date == null) {
                        //    $scope.parent_info.sims_parent_father_passport_expiry_date = currentDate;
                        //}
                        //else {
                        //    $scope.parent_info.sims_parent_father_passport_expiry_date = $scope.Parent_Details[0].sims_parent_father_passport_expiry_date;
                        //}

                        //if (isNaN($scope.Parent_Details[0].sims_parent_father_national_id_issue_date) || $scope.Parent_Details[0].sims_parent_father_national_id_issue_date == undefined || $scope.Parent_Details[0].sims_parent_father_national_id_issue_date == "" || $scope.Parent_Details[0].sims_parent_father_national_id_issue_date == null) {
                        //    $scope.parent_info.sims_parent_father_national_id_issue_date = currentDate;
                        //}
                        //else {
                        //    $scope.parent_info.sims_parent_father_national_id_issue_date = $scope.Parent_Details[0].sims_parent_father_national_id_issue_date;
                        //}

                        //if (isNaN($scope.Parent_Details[0].sims_parent_father_national_id_expiry_date) || $scope.Parent_Details[0].sims_parent_father_national_id_expiry_date == undefined || $scope.Parent_Details[0].sims_parent_father_national_id_expiry_date == "" || $scope.Parent_Details[0].sims_parent_father_national_id_expiry_date == null) {
                        //    $scope.parent_info.sims_parent_father_national_id_expiry_date = currentDate;
                        //}
                        //else {
                        //    $scope.parent_info.sims_parent_father_national_id_expiry_date = $scope.Parent_Details[0].sims_parent_father_national_id_expiry_date;
                        //}

                        //if (isNaN($scope.Parent_Details[0].sims_parent_mother_passport_issue_date) || $scope.Parent_Details[0].sims_parent_mother_passport_issue_date == undefined || $scope.Parent_Details[0].sims_parent_mother_passport_issue_date == "" || $scope.Parent_Details[0].sims_parent_mother_passport_issue_date == null) {
                        //    $scope.parent_info.sims_parent_mother_passport_issue_date = currentDate;
                        //}
                        //else {
                        //    $scope.parent_info.sims_parent_mother_passport_issue_date = $scope.Parent_Details[0].sims_parent_mother_passport_issue_date;
                        //}

                        //if (isNaN($scope.Parent_Details[0].sims_parent_mother_passport_expiry_date) || $scope.Parent_Details[0].sims_parent_mother_passport_expiry_date == undefined || $scope.Parent_Details[0].sims_parent_mother_passport_expiry_date == "" || $scope.Parent_Details[0].sims_parent_mother_passport_expiry_date == null) {
                        //    $scope.parent_info.sims_parent_mother_passport_expiry_date = currentDate;
                        //}
                        //else {
                        //    $scope.parent_info.sims_parent_mother_passport_expiry_date = $scope.Parent_Details[0].sims_parent_mother_passport_expiry_date;
                        //}

                        //if (isNaN($scope.Parent_Details[0].sims_parent_mother_national_id_issue_date) || $scope.Parent_Details[0].sims_parent_mother_national_id_issue_date == undefined || $scope.Parent_Details[0].sims_parent_mother_national_id_issue_date == "" || $scope.Parent_Details[0].sims_parent_mother_national_id_issue_date == null) {
                        //    $scope.parent_info.sims_parent_mother_national_id_issue_date = currentDate;
                        //}
                        //else {
                        //    $scope.parent_info.sims_parent_mother_national_id_issue_date = $scope.Parent_Details[0].sims_parent_mother_national_id_issue_date;
                        //}

                        //if (isNaN($scope.Parent_Details[0].sims_parent_mother_national_id_expiry_date) || $scope.Parent_Details[0].sims_parent_mother_national_id_expiry_date == undefined || $scope.Parent_Details[0].sims_parent_mother_national_id_expiry_date == "" || $scope.Parent_Details[0].sims_parent_mother_national_id_expiry_date == null) {
                        //    $scope.parent_info.sims_parent_mother_national_id_expiry_date = currentDate;
                        //}
                        //else {
                        //    $scope.parent_info.sims_parent_mother_national_id_expiry_date = $scope.Parent_Details[0].sims_parent_mother_national_id_expiry_date;
                        //}
                        //if (isNaN($scope.Parent_Details[0].sims_parent_guardian_passport_issue_date) || $scope.Parent_Details[0].sims_parent_guardian_passport_issue_date == undefined || $scope.Parent_Details[0].sims_parent_guardian_passport_issue_date == "" || $scope.Parent_Details[0].sims_parent_guardian_passport_issue_date == null) {
                        //    $scope.parent_info.sims_parent_guardian_passport_issue_date = currentDate;
                        //}
                        //else {
                        //    $scope.parent_info.sims_parent_guardian_passport_issue_date = $scope.Parent_Details[0].sims_parent_guardian_passport_issue_date;
                        //}
                        //if (isNaN($scope.Parent_Details[0].sims_parent_guardian_passport_expiry_date) || $scope.Parent_Details[0].sims_parent_guardian_passport_expiry_date == undefined || $scope.Parent_Details[0].sims_parent_guardian_passport_expiry_date == "" || $scope.Parent_Details[0].sims_parent_guardian_passport_expiry_date == null) {
                        //    $scope.parent_info.sims_parent_guardian_passport_expiry_date = currentDate;
                        //}
                        //else {
                        //    $scope.parent_info.sims_parent_guardian_passport_expiry_date = $scope.Parent_Details[0].sims_parent_guardian_passport_expiry_date;
                        //}

                        //if (isNaN($scope.Parent_Details[0].sims_parent_guardian_national_id_issue_date) || $scope.Parent_Details[0].sims_parent_guardian_national_id_issue_date == undefined || $scope.Parent_Details[0].sims_parent_guardian_national_id_issue_date == "" || $scope.Parent_Details[0].sims_parent_guardian_national_id_issue_date == null) {
                        //    $scope.parent_info.sims_parent_guardian_national_id_issue_date = currentDate;
                        //}
                        //else {
                        //    $scope.parent_info.sims_parent_guardian_national_id_issue_date = $scope.Parent_Details[0].sims_parent_guardian_national_id_issue_date;
                        //}
                        //if (isNaN($scope.Parent_Details[0].sims_parent_guardian_national_id_expiry_date) || $scope.Parent_Details[0].sims_parent_guardian_national_id_expiry_date == undefined || $scope.Parent_Details[0].sims_parent_guardian_national_id_expiry_date == "" || $scope.Parent_Details[0].sims_parent_guardian_national_id_expiry_date == null) {
                        //    $scope.parent_info.sims_parent_guardian_national_id_expiry_date = currentDate;
                        //}
                        //else {
                        //    $scope.parent_info.sims_parent_guardian_national_id_expiry_date = $scope.Parent_Details[0].sims_parent_guardian_national_id_expiry_date;
                        //}

                    });
                }
            }

            //setTimeout(function () {
            //$scope.setNewdate = function (id, dateObj) {
            //    debugger
            //        console.log(id, dateObj);
            //        $('#' + id).datepicker('update', new Date(dateObj));
            //    };

            //}, 2000);


            $scope.SaveStudent_data = function (Myform) {
                debugger;
                if ($scope.student_info != undefined) {
                    if (Myform) {
                        $('.nav-tabs a[href="#personal_information"]').tab('show')
                        if ($http.defaults.headers.common['schoolId'] == 'sok') {
                            $scope.methodname = 'UpdateSims_Student_DetailsSOK'
                        } else {
                            $scope.methodname = 'UpdateSims_Student_Details'
                        }

                        $http.post(ENV.apiUrl + "api/studentdatabase/" + $scope.methodname, $scope.student_info).then(function (MSG11) {
                            $scope.msgstr = MSG11.data;
                        });


                        $http.post(ENV.apiUrl + "api/studentdatabase/update_Student_Medical_Details", $scope.medical_info).then(function (msg3) {
                            $scope.msgstr3 = msg3.data;
                        });

                        $http.post(ENV.apiUrl + "api/studentdatabase/UpdateSims_Parent_Details", $scope.parent_info).then(function (msg2) {
                            $scope.msgstr2 = msg2.data;
                            setTimeout(function () {
                                if ($scope.msgstr == true && $scope.msgstr2 == true && $scope.msgstr3 == true) {
                                    swal({ text: 'Student Data Updated Successfully ' + '  ' + ' Parent Data Updated Successfully ' + ' ' + 'Student Medical Data Updated Successfully', width: 420, showCloseButton: true, timer: 3000 });
                                }
                                else if ($scope.msgstr == false && $scope.msgstr2 == false && $scope.msgstr3 == false) {
                                    swal({ text: 'Student Data Not Updated' + '  ' + ' Parent Data Not Updated' + ' ' + 'Student Medical Data Not Updated', width: 420, showCloseButton: true, timer: 3000 });
                                }
                                else if ($scope.msgstr == true && $scope.msgstr2 == true && $scope.msgstr3 == false) {
                                    swal({ text: 'Student Data  Updated Successfully' + '  ' + 'Parent Data Updated Successfully' + ' ' + 'Student Medical Data Not Updated', width: 420, showCloseButton: true, timer: 3000 });
                                }

                                else if ($scope.msgstr == false && $scope.msgstr2 == true && $scope.msgstr3 == false) {
                                    swal({ text: 'Student Data Not Updated' + '  ' + 'Parent Data Updated Successfully' + ' ' + 'Student Medical Data Not Updated', width: 420, showCloseButton: true, timer: 3000 });
                                }

                                else if ($scope.msgstr == false && $scope.msgstr2 == false && $scope.msgstr3 == true) {
                                    swal({ text: 'Student Data Not Updated' + '  ' + 'Parent Data Not Updated' + ' ' + 'Student Medical Data  Updated Successfully', width: 420, showCloseButton: true, timer: 3000 });
                                }
                                else if ($scope.msgstr == true && $scope.msgstr2 == false && $scope.msgstr3 == true) {
                                    swal({ text: 'Student Data  Updated Successfully' + '  ' + 'Parent Data Not Updated' + ' ' + 'Student Medical Data  Updated Successfully', width: 420, showCloseButton: true, timer: 3000 });
                                }
                                else if ($scope.msgstr == true && $scope.msgstr2 == false && $scope.msgstr3 == false) {
                                    swal({ text: 'Student Data  Updated Successfully' + '  ' + 'Parent Data Not Updated' + ' ' + 'Student Medical Data  Not Updated', width: 420, showCloseButton: true, timer: 3000 });
                                }
                                else if ($scope.msgstr == false && $scope.msgstr2 == true && $scope.msgstr3 == true) {
                                    swal({ text: 'Student Data  Updated' + '  ' + 'Parent Data  Updated  Successfully' + ' ' + 'Student Medical Data   Updated  Successfully', width: 420, showCloseButton: true, timer: 3000 });
                                }
                            }, 10);
                            $scope.student_info = {};
                            $scope.parent_info = {};
                            $scope.medical_info = {};
                            //setTimeout(function () {
                            //   $state.go($state.current, {}, { reload: true });
                            //},2000);
                            $scope.Myform.$setPristine();
                            $scope.Myform.$setUntouched();
                            $scope.parent_info = [];
                            $scope.medical_info = [];
                            $scope.student_info = [];
                            $scope.Sibling_Details = [];
                            $scope.father_photo = "";
                            $scope.mother_photo = "";
                            $scope.guardian_photo = "";
                            $scope.enrollno = '';
                            $scope.sims_student_photo = "";
                            $scope.sims_admisison_father_image = "";
                            $scope.sims_admisison_mother_image = "";
                            $scope.sims_admisison_guardian_image = "";
                            $scope.sims_student_image = "";
                            $scope.student_info.sims_student_image = "";
                            $scope.parent_info.sims_admisison_father_image = "";
                            $scope.parent_info.sims_admisison_guardian_image = "";
                            $scope.parent_info.sims_admisison_mother_image = "";
                            $scope.demo = true;
                            var c = document.getElementById('chk_company_emp');
                            c.checked == false;
                        });
                    }
                    else {
                        swal({ text: 'Fill required fields', width: 300, showCloseButton: true });
                    }
                }
                else {

                    swal({ text: 'Fields Are Empty', width: 300, showCloseButton: true });

                }

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.IsCompanyEmployee = function () {

                var c = document.getElementById('chk_company_emp');
                if (c.checked == true) {
                    var v = document.getElementById('cmb_company_emp');
                    v.disabled = false;
                }
                else {
                    var v = document.getElementById('cmb_company_emp');
                    v.disabled = true;

                    var cmb = document.getElementById('')
                }

            }

            $scope.GiftedStatus = function (Gifted_status) {

                if (Gifted_status) {

                    //txtgiftedstatus.disabled = false;
                    $scope.read1 = false;
                }
                else {

                    //txtgiftedstatus.disabled = true;
                    $scope.read1 = true;

                }

            }

            $scope.MusicProficiency = function (Music_Proficiency) {

                if (Music_Proficiency) {

                    $scope.read2 = false;
                }
                else {

                    $scope.read2 = true;
                }
            }

            $scope.SportStatus = function (Sport_Status) {
                if (Sport_Status) {

                    $scope.read3 = false;
                }
                else {

                    $scope.read3 = true;
                }

            }

            $scope.LanguageSupportStatus = function (Language_Support_Status) {
                if (Language_Support_Status) {

                    $scope.read4 = false;
                }
                else {

                    $scope.read4 = true;
                }

            }

            $scope.BehaviourStatus = function (Behaviour_Status) {
                if (Behaviour_Status) {

                    $scope.read5 = false;
                }
                else {

                    $scope.read5 = true;
                }

            }

            $scope.MedicationStatus = function (Medication_Status) {
                if (Medication_Status) {

                    $scope.Medication = false;
                }
                else {

                    $scope.Medication = true;
                }

            }

            $scope.DisabilityStatus = function (Disability_Status) {
                if (Disability_Status) {

                    $scope.Disability = false;
                }
                else {

                    $scope.Disability = true;
                }

            }

            $scope.RestrictionStatus = function (Restriction_Status) {
                if (Restriction_Status) {

                    $scope.Restriction = false;
                }
                else {

                    $scope.Restriction = true;
                }

            }

            $scope.HearingStatus = function (Hearing_Status) {
                if (Hearing_Status) {

                    $scope.Hearing = false;
                }
                else {

                    $scope.Hearing = true;
                }

            }

            $scope.VisionStatus = function (Vision_Status) {
                if (Vision_Status) {

                    $scope.Vision = false;
                }
                else {

                    $scope.Vision = true;
                }

            }

            $scope.OtherStatus = function (Other_Status) {
                if (Other_Status) {

                    $scope.Other = false;
                }
                else {

                    $scope.Other = true;
                }

            }

            //$http.get(ENV.apiUrl + "api/studentdatabase/getmunicipalityZone").then(function (municipalityZone) {
            // $scope.municipalityZone = municipalityZone.data;
            // });


            $scope.getstatefather = function () {
                debugger;
                //$http.get(ENV.apiUrl + "api/HRMS/emp/getState?em_Country_Code=" + country).then(function (res) {
                //    $scope.stt = res.data;
                //});
                $http.get(ENV.apiUrl + "api/State/getState").then(function (get_State) {
                    $scope.stt = get_State.data;
                });
            }

            $scope.getcityfather = function () {
                debugger;
                //$http.get(ENV.apiUrl + "api/HRMS/emp/getCity?em_State_Code=" + state).then(function (res) {
                //    $scope.ctt = res.data;
                //});
                $http.get(ENV.apiUrl + "api/CityMaster/getCity").then(function (get_City) {
                    $scope.ctt = get_City.data;
                });
            }
            $scope.getstatemother = function (country) {
                $http.get(ENV.apiUrl + "api/HRMS/emp/getState?em_Country_Code=" + country).then(function (res) {
                    $scope.stt = res.data;
                });
            }

            $scope.getcitymother = function (state) {
                $http.get(ENV.apiUrl + "api/HRMS/emp/getCity?em_State_Code=" + state).then(function (res) {
                    $scope.ctt = res.data;
                });
            }
            $scope.getstateguardian = function (country) {
                $http.get(ENV.apiUrl + "api/HRMS/emp/getState?em_Country_Code=" + country).then(function (res) {
                    $scope.stt = res.data;
                });
            }

            $scope.getcityguardian = function (state) {
                $http.get(ENV.apiUrl + "api/HRMS/emp/getCity?em_State_Code=" + state).then(function (res) {
                    $scope.ctt = res.data;
                });
            }

            //$scope.getstatefather = function (code) {

            //    var country_code = '';
            //    for (var i = 0; i < $scope.ComboBoxValues.length; i++) {

            //        if ($scope.ComboBoxValues[i].sims_country_name_en == code) {
            //            country_code = $scope.ComboBoxValues[i].sims_country_code;
            //        }
            //    }

            //    $http.get(ENV.apiUrl + "api/studentdatabase/getStateName?country_name=" + country_code + "&father=" + true + "&mother=" + false).then(function (state) {
            //        $scope.state_info = state.data;
            //        //console.log($scope.state_info);

            //    })
            //}

            //$scope.getcityname = function (code) {

            //    $http.get(ENV.apiUrl + "api/studentdatabase/getCity?state_name=" + code + "&father=" + true + "&mother=" + false).then(function (city) {
            //        $scope.city_info = city.data;
            //        //console.log($scope.city_info);

            //    })


            //}

            //$scope.getstatemother = function (code) {
            //    $http.get(ENV.apiUrl + "api/studentdatabase/getStateName?country_name=" + code + "&father=" + false + "&mother=" + true).then(function (state) {
            //        $scope.mother_state_info = state.data;
            //        //console.log($scope.mother_state_info);

            //    })
            //}

            //$scope.getmothercityname = function (code) {

            //    $http.get(ENV.apiUrl + "api/studentdatabase/getCity?state_name=" + code + "&father=" + false + "&mother=" + true).then(function (city) {
            //        $scope.mother_city_info = city.data;
            //        //console.log($scope.mother_city_info);

            //    })


            //}

            //$scope.getstateguardian = function (code) {
            //    $http.get(ENV.apiUrl + "api/studentdatabase/getStateName?country_name=" + code + "&father=" + false + "&mother=" + false).then(function (state) {
            //        $scope.guardian_state_info = state.data;
            //        //console.log($scope.mother_state_info);

            //    })
            //}

            //$scope.getguardiancityname = function (code) {

            //    $http.get(ENV.apiUrl + "api/studentdatabase/getCity?state_name=" + code + "&father=" + false + "&mother=" + false).then(function (city) {
            //        $scope.guardian_city_info = city.data;
            //        //console.log($scope.mother_city_info);

            //    })
            //}

            $scope.isparentschoolemployee = function (isparent_school_employee) {
                if (isparent_school_employee) {
                    $scope.hide = false;
                    $scope.EmploymentNo = false
                }
                else {

                    $scope.hide = true;
                    $scope.EmploymentNo = true;
                }
            }

            $scope.createdate = function (exp_date_pass, issue_date_pass, name) {


                //var month1 = exp_date_pass.split("/")[0];
                //var day1 = exp_date_pass.split("/")[1];
                //var year1 = exp_date_pass.split("/")[2];
                //var new_exp_date_pass = year1 + "/" + month1 + "/" + day1;
                var new_exp_date_pass = exp_date_pass;

                //var year = issue_date_pass.split("/")[0];
                //var month = issue_date_pass.split("/")[1];
                //var day = issue_date_pass.split("/")[2];
                //var new_issue_date_pass = year + "/" + month + "/" + day;
                var new_issue_date_pass = issue_date_pass;

                if (new_exp_date_pass < new_issue_date_pass) {

                    swal({ text: "Please Select Future Date", width: 300, showCloseButton: true });

                    $scope.student_info[name] = '';

                }
                else {


                    $scope.student_info[name] = new_exp_date_pass;

                }
            }

            $scope.createdate1 = function (exp_date_pass, issue_date_pass, name1) {

                debugger;
                //var month1 = exp_date_pass.split("/")[0];
                //var day1 = exp_date_pass.split("/")[1];
                //var year1 = exp_date_pass.split("/")[2];
                //var new_exp_date_pass = year1 + "/" + month1 + "/" + day1;
                var new_exp_date_pass = exp_date_pass;

                //var year = issue_date_pass.split("/")[0];
                //var month = issue_date_pass.split("/")[1];
                //var day = issue_date_pass.split("/")[2];
                //var new_issue_date_pass = year + "/" + month + "/" + day;
                var new_issue_date_pass = issue_date_pass;

                if (new_exp_date_pass < new_issue_date_pass) {

                    $rootScope.strMessage = "Please Select Future Date";
                    $('#message').modal('show');

                    $scope.parent_info[name1] = '';
                }
                else {

                    $scope.parent_info[name1] = new_exp_date_pass;
                }
            }

            $scope.showdate = function (date, name) {

                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //$scope.student_info[name] = year + "/" + month + "/" + day;
                $scope.student_info[name] = date;

            }

            $scope.showdate1 = function (date, name1) {
                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //$scope.parent_info[name1] = year + "/" + month + "/" + day;
                $scope.parent_info[name1] = date;

            }

            $scope.showdatemedical = function (date, name1) {

                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];

                //$scope.medical_info[name1] = year + "/" + month + "/" + day;
                $scope.medical_info[name1] = date;
            }

            $scope.Healthdatecheck1 = function (exp_date_pass, issue_date_pass, name1) {


                //var month1 = exp_date_pass.split("/")[0];
                //var day1 = exp_date_pass.split("/")[1];
                //var year1 = exp_date_pass.split("/")[2];
                //var new_exp_date_pass = year1 + "/" + month1 + "/" + day1;
                var new_exp_date_pass = exp_date_pass;

                //var year = issue_date_pass.split("/")[0];
                //var month = issue_date_pass.split("/")[1];
                //var day = issue_date_pass.split("/")[2];
                //var new_issue_date_pass = year + "/" + month + "/" + day;
                var new_issue_date_pass = issue_date_pass;

                if (new_exp_date_pass < new_issue_date_pass) {

                    $rootScope.strMessage = "Please Select Future Date";
                    $('#message').modal('show');

                    $scope.parent_info[name1] = '';
                }
                else {



                    $scope.parent_info[name1] = new_exp_date_pass;
                }
            }

            $scope.file_changed = function (element, name) {

                var str = '';

                str = $scope.enrollno

                var photofile = element.files[0];
                photo_filename = (photofile.type)
                $scope.student_info[name] = str + '.' + photo_filename.split("/")[1];
                student_image = photo_filename.replace(photo_filename, str);

                var reader = new FileReader();

                reader.onload = function (e) {
                    $scope.$apply(function () {

                        $scope.sims_student_photo = e.target.result;


                    });
                };
                reader.readAsDataURL(photofile);

                console.log("student_image", student_image);

                var request = {
                    method: 'POST',
                    //url: ENV.apiUrl + 'api/file/upload?filename=' + student_image + '&location=StudentImages',
                    url: ENV.apiUrl + '/api/file/upload?filename=' + student_image + "&location=" + "/StudentImages",
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request)
                       .success(function (d) {

                       }, function () {
                           alert("Err");
                       });


            };

            $scope.searchstudent = function () {
                $scope.busy = true;
                $scope.searchtable = false;
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.temp)).then(function (Allstudent) {

                    $scope.student = Allstudent.data;

                    $scope.busy = false;

                    $scope.searchtable = true;
                });
            }

            $scope.edit = function (info) {
                $scope.enrollno = info.s_enroll_no;
                $scope.SearchStudentData();
            }

            $scope.getsectioncode = function (cur, year, grade) {

                $http.get(ENV.apiUrl + "api/common/SectionTerm/getAllSections?cur_code=" + cur + "&ac_year=" + year + "&g_code=" + grade).then(function (res1) {
                    $scope.sections = res1.data;
                });


            }

            $scope.getTheFiles = function ($files) {

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.file_mother = function (element, name) {

                var str = '';
                str = 'M_' + $scope.parent_info.sims_parent_number;

                var photofile = element.files[0];
                photo_filename = (photofile.type);
                $scope.parent_info[name] = str + '.' + photo_filename.split("/")[1];
                sims_mother_image = photo_filename.replace(photo_filename, str);

                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.mother_photo = e.target.result;
                        //$scope.temp.em_img = element.files[0].name;
                        //console.log($scope.temp.em_img);
                    });
                };
                reader.readAsDataURL(photofile);

                var request = {
                    method: 'POST',
                    url: ENV.apiUrl + '/api/file/upload?filename=' + sims_mother_image.split(".")[0] + '&location=ParentImages',
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request)
                       .success(function (d) {

                       }, function () {
                           alert("Err");
                       });
            };

            $scope.file_father = function (element, name) {

                var str = '';
                str = 'F_' + $scope.parent_info.sims_parent_number;


                var photofile = element.files[0];
                photo_filename = (photofile.type);
                $scope.parent_info[name] = str + '.' + photo_filename.split("/")[1];
                sims_father_image = photo_filename.replace(photo_filename, str);

                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.father_photo = e.target.result;
                        //$scope.temp.em_img = element.files[0].name;
                        //console.log($scope.temp.em_img);
                    });
                };
                reader.readAsDataURL(photofile);

                var request = {
                    method: 'POST',
                    url: ENV.apiUrl + '/api/file/upload?filename=' + sims_father_image.split(".")[0] + '&location=ParentImages',
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request)
                       .success(function (d) {
                       }, function () {
                           alert("Err");
                       });
            };

            $scope.getTheFiles_mother = function ($files) {

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.getTheFiles_father = function ($files) {

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });

            }

            $scope.file_guardian = function (element, name) {

                var str = '';
                str = 'G_' + $scope.parent_info.sims_parent_number;

                var photofile = element.files[0];
                photo_filename = (photofile.type);
                $scope.parent_info[name] = str + '.' + photo_filename.split("/")[1];
                sims_guardian_image = photo_filename.replace(photo_filename, str);

                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.guardian_photo = e.target.result;
                        //$scope.temp.em_img = element.files[0].name;
                        //console.log($scope.temp.em_img);
                    });
                };
                reader.readAsDataURL(photofile);

                var request = {
                    method: 'POST',
                    url: ENV.apiUrl + '/api/file/upload?filename=' + sims_guardian_image.split(".")[0] + '&location=ParentImages',
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request)
                       .success(function (d) {
                           mother = d;
                       }, function () {
                           alert("Err");
                       });

            };

            $scope.getTheFiles_guardian = function ($files) {

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };


            //$('*[data-datepicker="true"] input[type="text"]').datepicker({

            //    todayBtn: true,
            //    orientation: "top left",
            //    autoclose: true,
            //    todayHighlight: true                
            //});

            //$(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
            //     $('input[type="text"]', $(this).parent()).focus();

            //});
            //$scope.SearchStudentData();


            $(document).keydown(function (e) {
                // ESCAPE key pressed
                if (e.keyCode == 27) {
                    //   $('#myModal').modal('hide');
                }
            });

            $scope.complete = function () {
                //for (var k = 0; k < $scope.AreaDetails.length; k++) {
                $("#txt_father_area,#txt_mother_area,#txt_guardian_area").autocomplete({
                    source: $scope.Area
                });
                //}
            }
            $scope.Area = [];
            $http.get(ENV.apiUrl + "api/studentdatabase/getAreaDetails").then(function (res) {
                debugger;
                console.log(res.data);
                $scope.AreaDetails = res.data;
                if ($scope.AreaDetails.length > 0) {
                    for (var k = 0; k < $scope.AreaDetails.length; k++) {
                        $scope.Area.push($scope.AreaDetails[k].sims_municipality_name_en);
                    }
                }
            });
        }]);
})();



