﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AdmissionDashboard_dvpsCont',
        ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.display = true;
            $scope.grid = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.pagesize1 = "5";
            $scope.pageindex1 = "1";
            $scope.pagesize2 = "5";
            $scope.pageindex2 = "1";
            $scope.pagesize3 = "5";
            $scope.pageindex3 = "1";
            $scope.pagesize4 = "5";
            $scope.pageindex4 = "1";
            $scope.pagesize5 = "5";
            $scope.pageindex5 = "1";
            $scope.flag = true;
            $scope.btn_selected = false;
            var str, cnt;
            $scope.dash = [];
            $scope.view = [];
            $scope.screen_datails = [];
            $scope.Upload_doc_datails = [];
            var admdetails = [];
            $scope.btn_prospect = true;
            $scope.btn_edit = true;
            $scope.btn_reject = true;
            $scope.click = false;

            $scope.btn_visible = null;
            $scope.gender = null;
            $scope.btn_nationality = null;
            $scope.btn_emp = [];
            $scope.btn_siblingenroll = [];
            $scope.maingrid2 = true;
            $scope.temp1 = {
                adm_no: '',
                first_name1: ''
            };

            


            var del = [];
            var t = false;
            var main, section = "", fee_category = "";
            $scope.filesize = true;
            var date = new Date();
            //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');
            $scope.email = { 'sims_msg_subject': undefined }
            //$scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss');
            $scope.once = true;
            var param = $stateParams.Class;

            $scope.filteredTodos1 = [], $scope.currentPage1 = 1, $scope.numPerPage1 = 5, $scope.maxSize1 = 5;
            $scope.filteredTodos2 = [], $scope.currentPage2 = 1, $scope.numPerPage2 = 5, $scope.maxSize2 = 5;
            $scope.filteredTodos3 = [], $scope.currentPage3 = 1, $scope.numPerPage3 = 5, $scope.maxSize3 = 5;
            $scope.filteredTodos4 = [], $scope.currentPage4 = 1, $scope.numPerPage4 = 5, $scope.maxSize4 = 5;
            $scope.filteredTodos5 = [], $scope.currentPage5 = 1, $scope.numPerPage5 = 5, $scope.maxSize5 = 5;

            $scope.docurl = ENV.apiUrl + '/content/' + $http.defaults.headers.common['schoolId'];


            if ($http.defaults.headers.common['schoolId'] == 'geis') {
                $scope.lbl_nationalid = "Civil ID";
            }
            else {
                $scope.lbl_nationalid = "National ID";
            }

            $scope.exportData = function () {

                var check = true;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {//exportable
                            var blob = new Blob([document.getElementById('Div21').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, "Report.xls");


                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Report Not Save", showCloseButton: true, width: 380, })
                }
            };

            $timeout(function () {
                $("#fixedtable,#fixedtable4").tableHeadFixer({ 'top': 1 });
            }, 100);


            // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {

                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;

                $scope.propertyName = propertyName;
            };
            //Sorting

            if (param != "") {
                $scope.edt_dash =
                    {
                        curr_code: param.curr_code,
                        academic_year: param.academic_year,
                        grade_code: param.grade_code,
                        sims_appl_parameter_reg: param.sims_appl_parameter_reg,
                        //sims_flag: 
                    };
            }


            $scope.printDetails = function (str) {
                //$('#viewdashDetailsModal').modal('hide');

                $scope.maingrid2 = false;
                $scope.maingrid1 = false;
                $scope.report_show = true;

                setTimeout(function () {

                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/GetAdm_dashRpt").then(function (res) {
                        $scope.reportparameter = res.data;
                        //var data = {
                        //    location: res.data,
                        //    parameter: {
                        //        admission_no: str,
                        //    },
                        //    state: 'main.DsDmc.OldmcNew',
                        //    ready: function () {
                        //        this.refreshReport();
                        //    },
                        //}

                        $scope.parameter = {
                            admission_no: str,
                        };

                        var s;

                        s = "SimsReports." + res.data + ",SimsReports";
                        var url = window.location.href;
                        var domain = url.substring(0, url.indexOf(':'));
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';

                        $("#reportViewerAdm")
                                 .telerik_ReportViewer({
                                     //serviceUrl: ENV.apiUrl + "api/reports/",
                                     serviceUrl: service_url,

                                     viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                     scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                     // Zoom in and out the report using the scale
                                     // 1.0 is equal to 100%, i.e. the original size of the report
                                     scale: 1.0,
                                     ready: function () {
                                         //this.refreshReport();
                                     }

                                 });

                        var reportViewer = $("#reportViewerAdm").data("telerik_ReportViewer");
                        reportViewer.reportSource({
                            report: s,
                            parameters: $scope.parameter,
                        });

                        setInterval(function () {

                            $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                            $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                        }, 1000);


                        $timeout(function () {
                            $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                            $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });


                        }, 100);

                        //window.localStorage["ReportDetails"] = JSON.stringify(data)
                        //$state.go('main.ReportCardParameter')


                    });
                }, 1000);

            }


            $scope.ConfirmprintDetails = function (str) {
                console.log(str);
                var data = {
                    location: 'Sims.SMR107',
                    parameter: {
                        AdmissionNo: str,
                    },
                    state: 'main.Dasdps',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                console.log(data);

                window.localStorage["ReportDetails"] = JSON.stringify(data);
                $state.go('main.ReportCardParameter');
            }

            /*start_paging_gridview*/

            $scope.size3 = function (pagesize) {
                $scope.itemsPerPage3 = pagesize;
            }

            $scope.range3 = function () {
                var rangeSize3 = 5;
                var ret3 = [];
                var start3;

                start3 = $scope.currentPage3;
                if (start3 > $scope.pageCount3() - rangeSize3) {
                    start3 = $scope.pageCount3() - rangeSize3 + 1;
                }

                for (var i = start3; i < start3 + rangeSize3; i++) {
                    if (i >= 0)
                        ret3.push(i);
                }
                return ret3;
            }

            $scope.prevPage3 = function () {
                if ($scope.currentPage3 > 0) {
                    $scope.currentPage3--;
                }
            }

            $scope.prevPageDisabled3 = function () {
                return $scope.currentPage3 === 0 ? "disabled" : "";
            };

            $scope.pageCount3 = function () {
                return Math.ceil($scope.Upload_doc_datails.length / $scope.itemsPerPage3) - 1;
            };

            $scope.nextPage3 = function () {
                if ($scope.currentPage3 < $scope.pageCount3()) {
                    $scope.currentPage3++;
                }
            };

            $scope.nextPageDisabled3 = function () {
                return $scope.currentPage3 === $scope.pageCount3() ? "disabled" : "";
            };

            $scope.setPage3 = function (n) {
                $scope.currentPage3 = n;
            };

            /*End Region*/

            $scope.Cancel = function () {

                if ($scope.report_show) {
                    $scope.maingrid1 = true;
                    $scope.maingrid2 = true;
                    $scope.report_show = false;

                }

                else {
                    $scope.view = "";
                    $scope.maingrid1 = false;
                    // $scope.maingrid2 = false;
                    //$scope.maingrid1 = true;
                    $scope.Screening = false;
                    $scope.Upload_doc = false;
                    $scope.div_Communication = false;
                    $scope.div_CommunicationHist = false;
                    $scope.commcancel_btn();
                    $scope.temp1 = {};
                }
            }



            $scope.link = function (str) {

                $scope.show_detals_flg_n = true;
               // $scope.maingrid1 = false;
               // $scope.maingrid2 = false;
                  $scope.show_form = true;

               // $('#viewdashDetailsModal').modal('hide');
                //$http.defaults.headers.common['schoolId'] = 'geis';

                if ($http.defaults.headers.common['schoolId'] == 'geis') {
                    setTimeout(function () {
                        $state.go("main.Olgeis", { admission_num: str, Class: $scope.edt_dash });
                        //$state.go("main.OlAdps", { admission_num: str, Class: $scope.edt_dash });
                    }, 500);
                }
                else if ($http.defaults.headers.common['schoolId'] == 'siso') {
                    setTimeout(function () {
                        $state.go("main.AdSISO", { admission_num: str, Class: $scope.edt_dash });
                    }, 500);
                }

                else if ($http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'dvhss') {
                    setTimeout(function () {
                        $state.go("main.OlSvc", { admission_num: str, Class: $scope.edt_dash });
                    }, 500);
                }
                //else if ($http.defaults.headers.common['schoolId'] == 'dvps') {
                //    setTimeout(function () {
                //        $state.go("main.Oldvps", { admission_num: str, Class: $scope.edt_dash });
                //    }, 500);
                //}

                else {
                    setTimeout(function () {
                      //  $state.go("main.DsDmc.OldmcNew", { admission_num: str, Class: $scope.edt_dash });
                        //$state.go("main.OlAdps", { admission_num: str, Class: $scope.edt_dash });
                        $scope.admission_num_new = angular.copy( str);
                        $rootScope.$broadcast('application-chage', { data: str });
                    }, 500);
                }
            }

           

            $scope.back = function () {

                $scope.show_detals_flg_n = false;
                $scope.show_form = false;

            }

            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.obj2 = res.data;
                $scope.edt_dash['curr_code'] = $scope.obj2[0].sims_attendance_cur_code;
                $scope.curr_code = $scope.obj2[0].sims_attendance_cur_code;

                $scope.getCur($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.edt_dash.sims_appl_parameter_reg);

            });

            $scope.getCur = function (cur_code, acad_yr, grade, reg) {
                debugger
                if (param.grade_code == "" && param.sims_appl_parameter_reg == "") {
                    $scope.edt_dash.grade_code = "";
                    $scope.edt_dash.sims_appl_parameter_reg = "";
                    $http.get(ENV.apiUrl + "api/common/getAllAcademicYearDash?cur_code=" + cur_code).then(function (res) {
                        $scope.obj1 = res.data;

                        //if ($scope.edt_dash['academic_year'] == "")
                        {
                            $scope.edt_dash['academic_year'] = JSON.stringify(parseInt($scope.obj1[0].sims_academic_year) + 1).toString();
                            $scope.acad_yr =  parseInt($scope.obj1[0].sims_academic_year)+1;
                            $scope.GetGrade(cur_code, $scope.acad_yr, $scope.edt_dash.grade_code, $scope.edt_dash.sims_appl_parameter_reg);
                        }



                    });
                }
                else {
                    $http.get(ENV.apiUrl + "api/common/getAllAcademicYearDash?cur_code=" + cur_code).then(function (res) {
                        $scope.obj1 = res.data;
                        $scope.edt_dash['academic_year'] = param.academic_year;

                        $scope.GetGrade(cur_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.edt_dash.sims_appl_parameter_reg);
                    });
                }



                $scope.GetInfo(cur_code, acad_yr, grade, reg);

            }

            $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/getRegistration").then(function (res) {
                $scope.reg = res.data;
            });

            //Get Dashboard Details
            var da = false
            if (window.localStorage['Admflag'] != "true") {
                window.localStorage.removeItem("Admflag");
            }

            function labelFormatter(label, series) {
                return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>"
            //+ label + "<br/>" 
                    + Math.round(series.percent) + "</div>";
            }

            $scope.GetInfo = function (cur_code, AcadmicYear, gradeCode, reg) {
                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/GetDashTilesDetails?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode).then(function (res) {
                    $scope.dashTiles = res.data;
                    console.log(res.data);
                });

                if (cur_code === '' && AcadmicYear === '' && gradeCode === '' && reg === '') {
                }
                else {

                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_asd/Getdirectadmissioncnt?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode).then(function (res) {
                        $scope.dirctadmCnt_deatails = res.data;
                    });

                    //+ "&admission_status=" + reg
                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/GetEmployeeCodecntDetails?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode).then(function (res) {
                        $scope.empCnt_details = res.data;
                        if ($scope.empCnt_details.count == 0) {
                            swal({ title: "Alert", text: "No Record Found", showCloseButton: true, width: 380, });
                        }
                        $scope.grid = true;
                    });

                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/GetsiblingcntDetails?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode).then(function (res) {
                        $scope.siblingCnt_details = res.data;
                        if ($scope.siblingCnt_details.count == 0) {
                            swal({ title: "Alert", text: "No Record Found", showCloseButton: true, width: 380, });
                        }
                        $scope.grid = true;
                    });

                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/GetGendercntDetails?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode).then(function (res) {
                        $scope.genderCnt_deatails = res.data;
                        $scope.grid = true;

                        $scope.labels = [];
                        $scope.data = [];
                        $scope.value = [];

                        var doughnutData = [];

                        for (var i = 0; i < $scope.genderCnt_deatails.length; i++) {
                            $scope.labels.push($scope.genderCnt_deatails[i].gender);
                            $scope.data.push(parseInt($scope.genderCnt_deatails[i].gender_cnt));
                            $scope.value.push($scope.genderCnt_deatails[i].gender_code)
                        }

                        $scope.grid = true;

                    });

                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/GetNationalityCntDetails?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode).then(function (res) {
                        $scope.nationalityCnt_deatails = res.data;
                        $scope.grid = true;

                        $scope.chartLabels = [];
                        $scope.chartData = [];
                        $scope.chartColor = [];

                        var pieChartData = [];

                        for (var i = 0; i < $scope.nationalityCnt_deatails.length; i++) {
                            var color = Math.floor(Math.random() * 16777216).toString(16);
                            color: '#000000'.slice(0, -color.length) + color;
                            $scope.nationalityCnt_deatails[i].color = color;

                            $scope.chartLabels.push($scope.nationalityCnt_deatails[i].nationality_name);
                            $scope.chartData.push(parseInt($scope.nationalityCnt_deatails[i].nationality_code));
                            $scope.chartColor.push($scope.nationalityCnt_deatails[i].color);


                        }


                    });
                }

                if (window.localStorage['Admflag'] == "true") {
                    $scope.getalldata(param.curr_code, param.academic_year, param.grade_code, param.sims_appl_parameter_reg, null, null, null, null, $scope.paid_status, '');
                    window.localStorage.removeItem("Admflag");
                }
            }

            $scope.GetGrade = function (cur, acad_yr, grade, reg) {
                debugger

                $scope.filteredTodos = [];
                // $scope.edt_dash.sims_appl_parameter_reg = "";
                if (param.grade_code == "" && param.sims_appl_parameter_reg == "") {
                    $scope.edt_dash.grade_code = "";
                    $scope.edt_dash.sims_appl_parameter_reg = "";
                    $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + cur + "&academic_year=" + acad_yr).then(function (res) {
                        $scope.obj3 = res.data;
                        setTimeout(function () {
                            $('#cmb_grade_code').change(function () {
                                console.log($(this).val());
                            }).multipleSelect({
                                width: '100%'
                            });
                            $("#cmb_grade_code").multipleSelect("checkAll");
                        }, 1000);
                        //$scope.grade = $scope.obj3[0].sims_grade_code;
                        //$scope.GetGradechage($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.edt_dash.sims_appl_parameter_reg);

                        if ($scope.edt_dash['grade_code'] == "") {
                            $scope.edt_dash['grade_code'] = $scope.obj3[0].sims_grade_code;
                            $scope.grade = $scope.obj3[0].sims_grade_code;
                            $scope.GetGradechage(cur, acad_yr, $scope.grade, $scope.edt_dash.sims_appl_parameter_reg);
                            $scope.GetInfo(cur, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.edt_dash.sims_appl_parameter_reg);
                        }
                    });
                }
                else {

                    reg = "";
                    $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + cur + "&academic_year=" + acad_yr).then(function (res) {
                        $scope.obj3 = res.data;
                        $scope.edt_dash['grade_code'] = param.grade_code;
                        setTimeout(function () {
                            $('#cmb_grade_code').change(function () {
                                console.log($(this).val());
                            }).multipleSelect({
                                width: '100%'
                            });
                            $("#cmb_grade_code").multipleSelect("checkAll");
                        }, 1000);

                    });
                }

                $scope.GetInfo(cur, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.edt_dash.sims_appl_parameter_reg);
            }

            $scope.GetGradechage = function (cur, acad_yr, grade, reg) {
                $scope.edt_dash.sims_appl_parameter_reg = "";
                reg = "";

                $scope.GetInfo(cur, acad_yr, grade, reg);
            }

            //Show_Tile_Details

            $scope.getalldata = function (cur_code, AcadmicYear, gradeCode, adm_status, gender, nationality, employee_code, sibling_enroll, paid_status1, sen_att) {
                $scope.temp1 = {};
                $scope.btn_visible = adm_status;
                $scope.paid_status = paid_status1;
                $scope.sen_status = sen_att;
                $scope.maingrid2 = true;
                $scope.maingrid1 = false;
                $('#viewdashDetailsModal').modal({ backdrop: 'static', keyboard: true });
                $scope.show_detals_flg_n = false;
                $scope.show_form = false;

            }

            $scope.getsearchAllData = function (adm_no, adm_name) {
                $scope.maingrid2 = true;
                if ($scope.paid_status == "" || $scope.paid_status == undefined) {
                    $scope.viewdashDetailsModal($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.btn_visible, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll, adm_no, adm_name, $scope.sen_status);
                }
                if ($scope.paid_status == 'P') {
                    $scope.showPaid();
                }
                if ($scope.paid_status == 'U') {
                    $scope.showUnPaid();
                }
            }

            $scope.hide_model = function () {
                if ($scope.Upload_doc) {
                    $scope.maingrid2 = true;
                    $scope.maingrid1 = true;
                    $scope.Upload_doc = false;
                }
                else if ($scope.report_show) {
                 $scope.maingrid2 = true;
                $scope.maingrid1 = true;
                $scope.report_show = false;
                }
                else
                $('#viewdashDetailsModal').modal('hide');
                
            }

            $scope.reject_label = 'Un-reject';
            $scope.viewdashDetailsModal = function (cur_code, AcadmicYear, gradeCode, adm_status, gender, nationality, employee_code, sibling_enroll, adm_no, adm_name, sen_status) {
                debugger;

                $scope.Upload_doc = false;
                $scope.btn_visible = adm_status;
                $scope.gender = gender;
                $scope.btn_nationality = nationality;
                $scope.btn_emp = employee_code;
                $scope.btn_siblingenroll = sibling_enroll;

                //if (sen_status == '1' && adm_status == 'W')
                //{
                //    $scope.btn_prospect = true;
                //    $scope.btn_reject = true;
                //    $scope.btn_unreject = false;
                //}

                $scope.reject_label = 'Un-reject';

                if (adm_status == 'S') {
                    $scope.btn_nationality = null;
                    $scope.gender = null;
                    $scope.btn_emp = null;
                    $scope.btn_siblingenroll = null;
                    $scope.btn_prospect = false;
                    $scope.btn_reject = false;
                    $scope.btn_unreject = true;
                    $scope.btn_selected = true;
                    $scope.reject_label = 'Un-Select';
                }

                if (adm_status == 'W') {
                    $scope.btn_nationality = null;
                    $scope.gender = null;
                    $scope.btn_emp = null;
                    $scope.btn_siblingenroll = null;

                    $scope.btn_prospect = false;
                    $scope.btn_reject = false;
                    $scope.btn_unreject = false;
                    $scope.btn_selected = false;
                }
                if (adm_status == 'R') {
                    $scope.btn_nationality = null;
                    $scope.gender = null;
                    $scope.btn_emp = null;
                    $scope.btn_siblingenroll = null;

                    $scope.btn_prospect = true;
                    $scope.btn_reject = true;
                    $scope.btn_unreject = false;
                    $scope.btn_selected = true;
                }
                if (adm_status == 'C') {
                    $scope.btn_nationality = null;
                    $scope.gender = null;
                    $scope.btn_emp = null;
                    $scope.btn_siblingenroll = null;
                    $scope.btn_prospect = true;
                    $scope.btn_reject = true;
                    $scope.btn_unreject = false;
                    $scope.btn_selected = true;
                }
                if (adm_status == '') {
                    $scope.btn_prospect = true;
                    $scope.btn_reject = true;
                    $scope.btn_unreject = false;
                }
                if (adm_status == '1') {
                    adm_status = 'W';
                }
                if (adm_status == '2') {
                    adm_status = 'R';
                }

                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/GetStudentsDmc?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode + "&admission_status=" + adm_status + "&gender_code=" + gender + "&nationality_code=" + nationality + "&employee_code=" + employee_code + "&sibling_enroll=" + sibling_enroll + "&adm_no=" + adm_no + "&adm_name=" + adm_name + "&sen_status=" + $scope.sen_status).then(function (res) {
                    $scope.dash_data = res.data;
                    $scope.grid = true;
                    $scope.maingrid1 = true;
                    if ($scope.dash_data.length > 0) {
                        //$('#viewdashDetailsModal').modal({ backdrop: 'static', keyboard: true });
                    }
                    else {
                        swal({ title: "Alert", text: "No Record Found", showCloseButton: true, width: 380, });
                    }
                });

            }

            $scope.showPaid = function () {
                var ob =
                    {
                        curr_code: $scope.edt_dash.curr_code,
                        AcadmicYear: $scope.edt_dash.academic_year,
                        gradeCode: $scope.edt_dash.grade_code,
                        admission_status: 'W',
                        gender_code: '',
                        nationality_code: '',
                        employee_code: '',
                        sibling_enroll: '',
                        adm_no: $scope.temp1.adm_no,
                        adm_name: $scope.temp1.first_name1

                        // prospect_fee_status:'Paid'
                    }

                if (ob.admission_status == 'S') {
                    $scope.btn_prospect = false;
                    $scope.btn_reject = false;
                    $scope.btn_unreject = false;
                    $scope.btn_selected = true;
                }

                if (ob.admission_status == 'W') {
                    $scope.btn_prospect = false;
                    $scope.btn_reject = false;
                    $scope.btn_unreject = true;
                    $scope.btn_selected = false;
                }
                if (ob.admission_status == 'R') {
                    $scope.btn_prospect = true;
                    $scope.btn_reject = true;
                    $scope.btn_unreject = false;
                    $scope.btn_selected = false;
                }
                if (ob.admission_status == 'C') {
                    $scope.btn_prospect = true;
                    $scope.btn_reject = true;
                    $scope.btn_unreject = true;
                    $scope.btn_selected = true;
                }
                if (ob.admission_status == '') {
                    $scope.btn_prospect = true;
                    $scope.btn_reject = true;
                    $scope.btn_unreject = false;
                }
                if (ob.admission_status == '1') {
                    ob.admission_status = 'W';
                }
                if (ob.admission_status == '2') {
                    ob.admission_status = 'R';
                }


                $http.post(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/waitingPaidStudents", ob).then(function (res) {
                    $scope.dash_data = res.data;
                    $scope.grid = true;
                    $scope.maingrid1 = true;
                    if ($scope.dash_data.length > 0) {
                        $('#viewdashDetailsModal').modal({ backdrop: 'static', keyboard: true });
                    }
                    else {
                        swal({ title: "Alert", text: "No Record Found", showCloseButton: true, width: 380, });
                    }
                });
            }

            $scope.showUnPaid = function () {
                var ob = {
                    curr_code: $scope.edt_dash.curr_code,
                    AcadmicYear: $scope.edt_dash.academic_year,
                    gradeCode: $scope.edt_dash.grade_code,
                    admission_status: 'W',
                    gender_code: '',
                    nationality_code: '',
                    employee_code: '',
                    sibling_enroll: '',
                    adm_no: $scope.temp1.adm_no,
                    adm_name: $scope.temp1.first_name1
                    //prospect_fee_status: 'Unpaid'
                }

                if (ob.admission_status == 'S') {
                    $scope.btn_prospect = false;
                    $scope.btn_reject = false;
                    $scope.btn_unreject = false;
                    $scope.btn_selected = true;
                }

                if (ob.admission_status == 'W') {
                    $scope.btn_prospect = false;
                    $scope.btn_reject = false;
                    $scope.btn_unreject = true;
                    $scope.btn_selected = false;
                }
                if (ob.admission_status == 'R') {
                    $scope.btn_prospect = true;
                    $scope.btn_reject = true;
                    $scope.btn_unreject = false;
                    $scope.btn_selected = false;
                }
                if (ob.admission_status == 'C') {
                    $scope.btn_prospect = true;
                    $scope.btn_reject = true;
                    $scope.btn_unreject = true;
                    $scope.btn_selected = true;
                }
                if (ob.admission_status == '') {
                    $scope.btn_prospect = true;
                    $scope.btn_reject = true;
                    $scope.btn_unreject = false;
                }
                if (ob.admission_status == '1') {
                    ob.admission_status = 'W';
                }
                if (ob.admission_status == '2') {
                    ob.admission_status = 'R';
                }

                $scope.btn_prospect = true;
                $scope.btn_unreject = true;


                $http.post(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/waitingUnPaidStudents", ob).then(function (res) {
                    $scope.dash_data = res.data;
                    $scope.grid = true;
                    $scope.maingrid1 = true;
                    if ($scope.dash_data.length > 0) {
                        $('#viewdashDetailsModal').modal({ backdrop: 'static', keyboard: true });
                    }
                    else {
                        swal({ title: "Alert", text: "No Record Found", showCloseButton: true, width: 380, });
                    }
                });
            }

            $scope.getEmpDetails = function () {
                $scope.gender = null;
                $scope.btn_nationality = null;
                $scope.btn_emp = [];
                $scope.btn_siblingenroll = [];
                var empDetails = [];


                for (var i = 0; i < $scope.empCnt_details.length; i++) {
                    empDetails = empDetails + $scope.empCnt_details[i].empl_code + ',';
                }

                if (empDetails.length == 0) {
                    //$scope.viewdashDetailsModal(null, null, null, null, null, null, empDetails, null);
                }
                else {//viewdashDetailsModal
                    $scope.btn_emp = empDetails;
                    $scope.getalldata($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.btn_visible, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll, $scope.paid_status, '');
                }
            }

            $scope.getSiblingDetails = function () {
                $scope.gender = null;
                $scope.btn_nationality = null;
                $scope.btn_emp = [];
                $scope.btn_siblingenroll = [];
                var siblingDetails = [];


                for (var j = 0; j < $scope.siblingCnt_details.length; j++) {
                    siblingDetails = siblingDetails + $scope.siblingCnt_details[j].sibling_enroll + ',';
                }

                if (siblingDetails.length == 0) {
                    //$scope.viewdashDetailsModal(null, null, null, null, null, null, null, siblingDetails);
                }
                else {
                    $scope.btn_siblingenroll = siblingDetails;
                    //consle.log(siblingDetails);
                    $scope.getalldata($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.btn_visible, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll, $scope.paid_status, '');
                    // $scope.viewdashDetailsModal($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.btn_visible, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll);
                }
            }

            $scope.chartLabels = ['Series A', 'Series B', 'Series C'];
            $scope.chartData = [65, 59, 20];
            $scope.chartColor = ['#CCCCCC', '#CCCCCC', '#CCCCCC'];



            $(document).keyup(function (e) {
                if (e.keyCode == 27) { // escape key maps to keycode `27`
                    // <DO YOUR WORK HERE>

                    $('#viewdashDetailsModal').modal('hide');
                    $scope.Cancel();
                }
            });

            $scope.onClickSlice = function (points, evt) {
                $scope.gender = null;
                $scope.btn_nationality = null;
                $scope.btn_emp = [];
                $scope.btn_siblingenroll = [];
                // $scope.maingrid2 = true;

                $scope.btn_prospect = true;
                $scope.btn_reject = true;
                $scope.btn_unreject = false;
                $scope.btn_selected = true;

                //  console.log(points, evt);
                $scope.btn_nationality = points[0]._model.label;
                $scope.getalldata($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, null, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll, $scope.paid_status, '');
            };

            $scope.labels = ["ABC", "XYZ"];
            $scope.data = [300, 500];

            $scope.onClick = function (points, evt) {
                $scope.gender = null;
                $scope.btn_nationality = null;
                $scope.btn_emp = [];
                $scope.btn_siblingenroll = [];

                $scope.btn_prospect = true;
                $scope.btn_reject = true;
                $scope.btn_unreject = false;
                $scope.btn_selected = true;
                // $scope.maingrid2 = true;

                // console.log(points, evt);
                $scope.gender = points[0]._model.label;
                //console.log(points[0]._model.label);
                $scope.getalldata($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, null, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll, $scope.paid_status, '');
            };

            // $scope.getCur(param.curr_code);

            //  $scope.GetGrade(param.curr_code, param.academic_year, param.grade_code, param.sims_appl_parameter_reg);

            $scope.GetInfo(param.curr_code, param.academic_year, param.grade_code, param.sims_appl_parameter_reg);

            $scope.check1 = function (dash) {
                $scope.sims_admission_father_mail = dash.father_email;
                $scope.sims_admission_father_mobile = dash.father_mobile;
                $scope.sims_admission_number = dash.admission_number;

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

                admdetails = dash.admission_number;
                var v = document.getElementById(dash.admission_number);
                if (v.checked == true)
                    // if (dash.admission_number1==true)
                {
                    //$scope.btn_edit = false;
                    // $scope.btn_reject = false;
                    //if (dash.curr_code == '' || dash.academic_year == '' || dash.grade_code == '' || dash.section_code == '' || dash.fee_category == '')
                    //{
                    //    swal({ title: "Alert", text: "Please Check Whether Section and Fee Category are Selected for this Students", showCloseButton: true, width: 380, });
                    //    v.checked = false;
                    //    var index = del.indexOf(dash.admission_number);

                    //    if (index > -1) {
                    //        del.splice(index, 3);
                    //    }
                    //}
                    //else {
                    del.push(dash.admission_number, dash.section_code, dash.fee_category);
                    $scope.row1 = '';
                    //}
                }
                else {
                    //$scope.btn_edit = true;

                    v.checked = false;
                    var index = del.indexOf(dash.admission_number);

                    if (index > -1) {
                        del.splice(index, 3);
                    }
                }
            }

            $scope.check = function () {
                main = document.getElementById('mainchk');
                del = [];
                if (main.checked == true) {
                    debugger;
                    for (var i = 0; i < $scope.dash_data.length; i++) {
                        $scope.dash_data[i].admission_number1 = true;
                        var v = document.getElementById($scope.dash_data[i].admission_number);
                        // console.log($scope.dash_data[i].curr_code);
                        //if ($scope.dash_data[i].curr_code == '' || $scope.dash_data[i].academic_year == '' || $scope.dash_data[i].grade_code == '' || $scope.dash_data[i].section_code == '' || $scope.dash_data[i].fee_category == '')
                        //{
                        //    v.checked = false;
                        //    var index = del.indexOf($scope.dash_data[i].admission_number);

                        //    if (index > -1) {
                        //        del.splice(index, 3);
                        //    }
                        //}
                        //else
                        //{

                        // if ($scope.dash_data[i].admission_number1 == true)
                        //{

                        v.checked = true;
                        del.push($scope.dash_data[i].admission_number, $scope.dash_data[i].section_code, $scope.dash_data[i].fee_category);
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                        //}
                        //}
                    }
                }
                else {
                    for (var i = 0; i < $scope.dash_data.length; i++) {
                        var t = $scope.dash_data[i].admission_number;

                        var v = document.getElementById(t, $scope.dash_data[i].section_code, $scope.dash_data[i].fee_category);
                        $scope.dash_data[i].admission_number1 = false;
                        v.checked = false;
                        del.pop(t);
                        $scope.row1 = '';
                    }
                }
            }


            //Promote the Admissions
            $scope.getPromote = function () {
                $scope.click1 = false;
                if ($scope.click == false) {
                    $scope.click = true;
                    admdetails = [];
                    for (var i = 0; i < $scope.dash_data.length; i++) {
                        var t = $scope.dash_data[i].admission_number;
                        var v = document.getElementById(t);

                        //if (v.checked == true) || $scope.dash_data[i].prospect_fee_status == 'Unpaid'
                        if ($scope.dash_data[i].admission_number1) {


                            if ($scope.dash_data[i].curr_code == '' || $scope.dash_data[i].academic_year == '' || $scope.dash_data[i].grade_code == '' || $scope.dash_data[i].section_code == '' || $scope.dash_data[i].fee_category == '') {
                                //if ($scope.dash_data[i].section_code == '')
                                //if ($scope.dash_data[i].term_code == '')
                                //    $scope.dash_data[i].term_code = '01';
                                //if ($scope.dash_data[i].fee_category == '')
                                //    $scope.dash_data[i].fee_category = '01';
                                //if ($scope.dash_data[i].sims_fee_month_code == '')
                                //    $scope.dash_data[i].sims_fee_month_code = '4';

                                swal({ title: "Alert", text: "Please Check Whether Section and Category are Selected for these Students.", showCloseButton: true, width: 380, });
                                $scope.click1 = true;

                                v.checked = false;
                                $scope.click = false;
                                $scope.row1 = '';
                                $('tr').removeClass("row_selected");

                                admdetails = admdetails + $scope.dash_data[i].admission_number + ',';
                            }
                            else {
                                admdetails = admdetails + $scope.dash_data[i].admission_number + ',';
                            }
                        }
                    }
                    //if (admdetails.length == 0) {
                    //    swal({ title: "Alert", text: "Nothing Selected", showCloseButton: true, width: 380, });
                    //}

                    if ($scope.click1 != true) {
                        if (admdetails.length > 0) {
                            var data = $scope.edt_dash;
                            data.admission_number = admdetails;
                            data.user_code = $rootScope.globals.currentUser.username;
                            console.log(data);
                            if (admdetails.length > 0) {
                                $http.post(ENV.apiUrl + "api/common/Admission_dpsd/ApproveMultiple", data).then(function (res) {
                                    $scope.promote = res.data;
                                    console.log($scope.promote);
                                });
                                $scope.GetInfo($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.btn_visible);
                                $scope.getalldata($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.btn_visible, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll, $scope.paid_status, '');
                                $scope.getsearchAllData('', '');
                                //$scope.ConfirmprintDetails(admdetails);
                            }
                        }
                    }
                }
                else {
                    $scope.edt_dash = [];
                }

            }

            //Reject the Admissions
            $scope.getReject = function () {
                admdetails = [];
                for (var i = 0; i < $scope.dash_data.length; i++) {
                    var t = $scope.dash_data[i].admission_number;
                    var v = document.getElementById(t);

                    //if (v.checked == true)
                    if ($scope.dash_data[i].admission_number1) {
                        admdetails = admdetails + $scope.dash_data[i].admission_number + ',';
                    }
                }
                if (admdetails.length == 0) {
                    swal({ title: "Alert", text: "Please select atleast 1 applicant", showCloseButton: true, width: 380, });
                    //$('#message').modal({ backdrop: 'static', keyboard: true });
                }
                else {
                    swal({
                        title: '',
                        text: "The selected applications will be rejected",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.OkRejectadm();
                        }
                    });
                }
            }

            $scope.OkRejectadm = function () {
                var data = $scope.edt_dash;
                data.admission_number = admdetails;
                data.user_code = $rootScope.globals.currentUser.username;
                data.status = 'R';
                data.opr = 'J';

                $http.post(ENV.apiUrl + "api/common/Admission_dpsd/AdmissionStatusOpr", data).then(function (res) {
                    $scope.display = true;
                    $scope.msg1 = res.data;
                    console.log($scope.msg1);

                    if ($scope.msg1 > 0) {

                    }
                });

                $scope.getalldata($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.btn_visible, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll, $scope.paid_status, '');
                $scope.GetInfo($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.btn_visible);
                $scope.getsearchAllData('', '');
            }

            //UnReject the Admissions
            $scope.getUnReject = function () {
                admdetails = [];
                for (var i = 0; i < $scope.dash_data.length; i++) {
                    var t = $scope.dash_data[i].admission_number;
                    var v = document.getElementById(t);

                    //if (v.checked == true)
                    if ($scope.dash_data[i].admission_number1) {
                        admdetails = admdetails + $scope.dash_data[i].admission_number + ',';
                    }
                }
                if (admdetails.length == 0) {
                    swal({ title: "Alert", text: "Please select atleast 1 applicant", showCloseButton: true, width: 380, });
                    //$('#message').modal({ backdrop: 'static', keyboard: true });
                }
                else {
                    swal({
                        title: '',
                        text: "The selected applications will be " + $scope.reject_label,
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.OkUnRejectadm();
                        }
                    });
                }
            }

            $scope.OkUnRejectadm = function () {
                var data = $scope.edt_dash;
                data.admission_number = admdetails;
                data.user_code = $rootScope.globals.currentUser.username;
                data.status = 'W';
                data.opr = 'J';

                $http.post(ENV.apiUrl + "api/common/Admission_dpsd/AdmissionStatusOpr", data).then(function (res) {
                    $scope.display = true;
                    $scope.msg1 = res.data;
                    console.log($scope.msg1);

                    if ($scope.msg1 > 0) {

                    }
                });

                $scope.getalldata($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.btn_visible, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll, $scope.paid_status, '');
                $scope.GetInfo($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.btn_visible);
                $scope.getsearchAllData('', '');
            }



            //getSelected
            $scope.getSelected = function () {
                admdetails = [];
                // $scope.btn_selected = true;
                $scope.btn_prospect = false;
                for (var i = 0; i < $scope.dash_data.length; i++) {
                    var t = $scope.dash_data[i].admission_number;
                    var v = document.getElementById(t);

                    if ($scope.dash_data[i].admission_number1) {
                        admdetails = admdetails + $scope.dash_data[i].admission_number + ',';
                    }
                }
                if (admdetails.length == 0) {
                    swal({ title: "Alert", text: "Please select atleast 1 applicant", showCloseButton: true, width: 380, });
                }
                else {
                    swal({
                        title: '',
                        text: "The applicants will be Selected",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.OkselectedAdm();
                        }
                    });
                }
            }

            $scope.OkselectedAdm = function () {

                var data = $scope.edt_dash;
                data.admission_number = admdetails;
                data.user_code = $rootScope.globals.currentUser.username;
                data.status = 'S';
                data.opr = 'J';

                console.log(data);

                $http.post(ENV.apiUrl + "api/common/Admission_dpsd/AdmissionStatusOpr", data).then(function (res) {
                    $scope.display = true;
                    $scope.msg1 = res.data;
                    //console.log($scope.msg1);

                    if ($scope.msg1 > 0) {

                        swal({ text: "Updated Successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });

                        //$('#viewdashDetailsModal1').modal({ backdrop: 'static', keyboard: true });
                        //$scope.get_msgtypechecked($scope.edt_dash1.sims_Email);


                        //if ($scope.sen_status != '1') {
                        //    $http.get(ENV.apiUrl + "api/common/Admission_dpsd/GetemailSubjectforInviting").then(function (res) {
                        //        $scope.emailSubject_datails = res.data;
                        //        console.log($scope.emailSubject_datails);
                        //        $scope.edt_dash1['sims_recepient_id'] = $scope.sims_admission_father_mail;
                        //        $scope.edt_dash1['sims_subject'] = $scope.emailSubject_datails[0].mailmsg_subject;
                        //        $scope.edt_dash1['sims_body'] = $scope.emailSubject_datails[0].mailmsg_body;
                        //        //$scope.edt_dash1['admission_date1'] = $scope.ddMMyyyy;
                        //        //$scope.edt_dash1['msg_time'] = "12:00 AM";

                        //        var v = document.getElementById('text-editor1');
                        //        v.value = $scope.emailSubject_datails[0].mailmsg_body;
                        //        $scope.edt_dash1['sims_body'] = v.value;
                        //        $('#text-editor1').data("wysihtml5").editor.setValue($scope.edt_dash1['sims_body']);

                        //        // console.log($scope.emailSubject_datails[0].mailmsg_body);

                        //        // $scope.test = $scope.edt_dash1.sims_body.split(' ');

                        //        // for (var i = 0; i < $scope.test.length; i++)
                        //        // {
                        //        //     if ($scope.test[i] == '{appl_num}')
                        //        //     {
                        //        //         $scope.test[i] = $scope.sims_admission_number;
                        //        //     }
                        //        //     if ($scope.test[i] == '{appl_date}') {
                        //        //         $scope.test[i] = $scope.edt_dash1['admission_date1'];
                        //        //     }
                        //        //     if ($scope.test[i] == '{app_time}') {
                        //        //         $scope.test[i] = $scope.edt_dash1['msg_time'];
                        //        //     }

                        //        // }
                        //        //// $scope.edt_dash1['sims_body'] = $scope.test.replace(/[, ]+/g, " ").trim();
                        //        // $scope.edt_dash1['sims_body'] = $scope.test;

                        //        // $('#text-editor1').data("wysihtml5").editor.setValue($scope.edt_dash1['sims_body']);
                        //    });
                        //}

                    }
                });

                $scope.getalldata($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.btn_visible, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll, $scope.paid_status, '');
                $scope.GetInfo($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.btn_visible);
                $scope.getsearchAllData('', '');
                //$scope.maingrid1 = false;
                //$scope.maingrid2 = false;
            }

            $scope.getadmissionDate = function (admdate) {
                $scope.edt_dash1['admission_date1'] = admdate;
                $('#text-editor1').data("wysihtml5").editor.setValue($scope.edt_dash1['sims_body']);
                // $scope.edt_dash1['msg_time'] = "12:00 AM";
            }

            $scope.makeTodos1 = function () {
                var rem = parseInt($scope.totalItems1 % $scope.numPerPage1);
                if (rem == '0') {
                    $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1);
                }
                else {
                    $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1) + 1;
                }
                var begin1 = (($scope.currentPage1 - 1) * $scope.numPerPage1);
                var end1 = parseInt(begin1) + parseInt($scope.numPerPage1);

                $scope.filteredTodos1 = $scope.todos1.slice(begin1, end1);
            };

            $scope.size1 = function (str) {
                console.log(str);
                $scope.pagesize1 = str;
                $scope.currentPage1 = 1;
                $scope.numPerPage1 = str;
                console.log("numPerPage=" + $scope.numPerPage1);
                $scope.makeTodos1();
            }

            $scope.index1 = function (str) {
                $scope.pageindex1 = str;
                $scope.currentPage1 = str;
                console.log("currentPage1=" + $scope.currentPage1);
                $scope.makeTodos1();
            }

            $scope.searched1 = function (valLists, toSearch) {

                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil1(i, toSearch);
                });
            };

            $scope.search1 = function () {
                $scope.todos1 = $scope.searched1($scope.view, $scope.searchText1);
                $scope.totalItems1 = $scope.todos1.length;
                $scope.currentPage1 = '1';
                if ($scope.searchText1 == '') {
                    $scope.todos1 = $scope.view;
                }
                $scope.makeTodos1();
            }

            function searchUtil1(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.admission_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.stud_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            //Upload_document
            $scope.UploadDocument = function (adm_no) {
                //  $scope.admission_no = adm_no;
                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/GetCriteriaName?admission_num=" + adm_no).then(function (res) {
                    $scope.Upload_doc_datails = res.data;
                    $scope.totalItems3 = $scope.Upload_doc_datails.length;
                    $scope.todos3 = $scope.Upload_doc_datails;
                    for (var i = 0; i < $scope.totalItems3; i++) {
                        $scope.Upload_doc_datails[i] = res.data[i];
                        //  $scope.todos = res.data[i];
                        $scope.visible = [];
                        $scope.visible = res.data[i].sims_admission_doc_path.split(",");
                        $scope.Upload_doc_datails[i].path = $scope.visible;
                    }
                    $scope.makeTodos3();
                    console.log($scope.Upload_doc_datails);
                    $scope.Upload_doc = true;
                    $scope.maingrid1 = false;
                    $scope.maingrid2 = false;
                    $scope.edt_dash.sims_admission_doc_path1 = "NO";
                    // $('#UploadDocModal').modal({ backdrop: 'static', keyboard: true });
                });

            }

            $scope.makeTodos3 = function () {
                var rem = parseInt($scope.totalItems3 % $scope.numPerPage3);
                if (rem == '0') {
                    $scope.pagersize3 = parseInt($scope.totalItems3 / $scope.numPerPage3);
                }
                else {
                    $scope.pagersize3 = parseInt($scope.totalItems3 / $scope.numPerPage3) + 1;
                }
                var begin3 = (($scope.currentPage3 - 1) * $scope.numPerPage3);
                var end3 = parseInt(begin3) + parseInt($scope.numPerPage3);
                $scope.filteredTodos3 = $scope.todos3.slice(begin3, end3);
            };

            $scope.size3 = function (str) {
                console.log(str);
                $scope.pagesize3 = str;
                $scope.currentPage3 = 1;
                $scope.numPerPage3 = str;
                console.log("numPerPage3=" + $scope.numPerPage3);
                $scope.makeTodos3();
            }

            $scope.index3 = function (str) {
                $scope.pageindex3 = str;
                $scope.currentPage3 = str;
                console.log("currentPage3=" + $scope.currentPage3);
                $scope.makeTodos3();
            }

            $scope.searched3 = function (valLists, toSearch) {

                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil3(i, toSearch);
                });
            };

            $scope.search3 = function () {
                $scope.todos3 = $scope.searched3($scope.Upload_doc_datails, $scope.searchText3);
                $scope.totalItems3 = $scope.todos3.length;
                $scope.currentPage3 = '1';
                if ($scope.searchText3 == '') {
                    $scope.todos3 = $scope.Upload_doc_datails;
                }
                $scope.makeTodos3();
            }

            function searchUtil3(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_criteria_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_criteria_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            //screeningMarks
            $scope.screeningMarks = function (adm_no) {
                //$scope.admission_no = adm_no;

                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/GetcriteriaMarks?admission_num=" + adm_no).then(function (res) {
                    $scope.screen_datails = res.data;
                    $scope.totalItems2 = $scope.screen_datails.length;
                    $scope.todos2 = $scope.screen_datails;
                    $scope.makeTodos2();
                    $scope.Screening = true;
                    $scope.maingrid1 = false;
                    $scope.maingrid2 = false;
                    $scope.t = true;
                    //$('#screeningModal').modal({ backdrop: 'static', keyboard: true });
                });
            }

            $scope.makeTodos2 = function () {
                var rem = parseInt($scope.totalItems2 % $scope.numPerPage2);
                if (rem == '0') {
                    $scope.pagersize2 = parseInt($scope.totalItems2 / $scope.numPerPage2);
                }
                else {
                    $scope.pagersize2 = parseInt($scope.totalItems2 / $scope.numPerPage2) + 1;
                }
                var begin2 = (($scope.currentPage2 - 1) * $scope.numPerPage2);
                var end2 = parseInt(begin2) + parseInt($scope.numPerPage2);

                $scope.filteredTodos2 = $scope.todos2.slice(begin2, end2);
            };

            $scope.size2 = function (str) {
                console.log(str);
                $scope.pagesize2 = str;
                $scope.currentPage2 = 1;
                $scope.numPerPage2 = str; console.log("numPerPage2=" + $scope.numPerPage2); $scope.makeTodos2();
            }

            $scope.index2 = function (str) {
                $scope.pageindex2 = str;
                $scope.currentPage2 = str; console.log("currentPage2=" + $scope.currentPage2); $scope.makeTodos2();
            }

            $scope.searched2 = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil2(i, toSearch);
                });
            };

            $scope.search2 = function () {
                $scope.todos2 = $scope.searched2($scope.screen_datails, $scope.searchText2);
                $scope.totalItems2 = $scope.todos2.length;
                $scope.currentPage2 = '1';
                if ($scope.searchText2 == '') {
                    $scope.todos2 = $scope.screen_datails;
                }
                $scope.makeTodos2();
            }

            function searchUtil2(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_admission_criteria_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_criteria_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            //Update_screening_details
            $scope.screeningModal_Cancel = function () {
                var data = $scope.screen_datails;
                $http.post(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/CUD_UpdateAdmissionMarks", data).then(function (res) {
                    $scope.display = true;
                    $scope.msg1 = res.data;
                    console.log($scope.msg1);
                    if ($scope.msg1.messageType > 0) {
                        swal({ title: "Admission Marks", text: "'" + $scope.msg1.strMessage + "'", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.Screening = false;
                                $scope.maingrid1 = true;
                                // $('#screeningModal').modal('hide');
                                $scope.getalldata($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.btn_visible, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll, $scope.paid_status, '');
                                // $scope.GetInfo($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.edt_dash.sims_appl_parameter_reg);
                            }
                        });

                    }
                });
            }

            $scope.screeningModal_Reset = function () {
                $scope.Screening = false;
                $scope.maingrid1 = true;
                $scope.maingrid2 = true;
            }

            //HTML5 editor

            $scope.apiurl1 = 'https://oa.mograsys.com/' + $http.defaults.headers.common['schoolId'] + '/';
            $scope.label_hsc = 'H.S.S.C.E';
            $scope.ViewDetails = function (str) {

                $scope.single_o = str;
                $scope.show_detals_flg = true;
                //   $('#view_details').modal({ backdrop: 'static', keyboard: true });

              
                $http.get($scope.apiurl1 + "api/common/Admission/GetStudent_PrevExamDetails?admission_number=" + str.admission_number).then(function (res) {
                    $scope.edt_dash_exam = res.data;
                   $scope.edt_n = {};
                    // console.log($scope.edt_dash1);

                   $scope.edt_n['grade_code'] = str.grade_code;

                   $scope.fy_flg = false;
                   $scope.sy_flg = false;
                   $scope.flg_stream = false;

                   if ($http.defaults.headers.common['schoolId'] == 'facp') {
                       if ($scope.edt_n['grade_code'] == '05' || $scope.edt_n['grade_code'] == '08' || $scope.edt_n['grade_code'] == '11') {
                           $scope.fy_flg = true;
                       }

                       if ($scope.edt_n['grade_code'] == '06' || $scope.edt_n['grade_code'] == '09' || $scope.edt_n['grade_code'] == '12') {
                           $scope.fy_flg = true;
                           $scope.sy_flg = true;
                       }
                   }
                   if ($http.defaults.headers.common['schoolId'] == 'dmc') {

                    

                       if ($scope.edt_n['grade_code'] == '28' || $scope.edt_n['grade_code'] == '29' || $scope.edt_n['grade_code'] == '30' || $scope.edt_n['grade_code'] == '31') {
                           $scope.label_hsc = 'Graduation';
                           $scope.flg_stream = true;
                       }

                       if ($scope.edt_n['grade_code'] == '14' || $scope.edt_n['grade_code'] == '17' || $scope.edt_n['grade_code'] == '20' || $scope.edt_n['grade_code'] == '23' || $scope.edt_n['grade_code'] == '26' || $scope.edt_n['grade_code'] == '29' || $scope.edt_n['grade_code'] == '31') {
                           $scope.fy_flg = true;
                       }

                       if ($scope.edt_n['grade_code'] == '15' || $scope.edt_n['grade_code'] == '18' || $scope.edt_n['grade_code'] == '21' || $scope.edt_n['grade_code'] == '24' || $scope.edt_n['grade_code'] == '27') {
                           $scope.fy_flg = true;
                           $scope.sy_flg = true;
                       }

                   }
                   if ($http.defaults.headers.common['schoolId'] == 'cesc') {

                       if ($scope.edt_n['grade_code'] == '12' || $scope.edt_n['grade_code'] == '15') {
                           $scope.fy_flg = true;
                       }

                       if ($scope.edt_n['grade_code'] == '13' || $scope.edt_n['grade_code'] == '16') {
                           $scope.fy_flg = true;
                           $scope.sy_flg = true;
                       }
                   }

                    $scope.edt_n['current_school_name'] = $scope.edt_dash_exam['current_school_name'];
                    $scope.edt_n['sims_admission_boardname'] = $scope.edt_dash_exam['sims_admission_boardname'];
                    $scope.edt_n['sims_admission_center_name'] = $scope.edt_dash_exam['sims_admission_center_name'];
                    $scope.edt_n['sims_admission_passingyear_exam'] = $scope.edt_dash_exam['sims_admission_passingyear_exam'];
                    $scope.edt_n['sims_admission_seat_no'] = $scope.edt_dash_exam['sims_admission_seat_no'];
                    $scope.edt_n['sims_admission_no_of_attempts'] = $scope.edt_dash_exam['sims_admission_no_of_attempts'];
                    $scope.edt_n['sims_admission_marks_obtained'] = $scope.edt_dash_exam['sims_admission_marks_obtained'];
                    $scope.edt_n['sims_admission_marks_out_of'] = $scope.edt_dash_exam['sims_admission_marks_out_of'];
                    $scope.edt_n['sims_admission_percentage_obtained'] = $scope.edt_dash_exam['sims_admission_percentage_obtained'];

                    $scope.edt_n['current_fyschool_name'] = $scope.edt_dash_exam['current_fyschool_name'];
                    $scope.edt_n['sims_admission_fyboardname'] = $scope.edt_dash_exam['sims_admission_fyboardname'];
                    $scope.edt_n['sims_admission_fycenter_name'] = $scope.edt_dash_exam['sims_admission_fycenter_name'];
                    $scope.edt_n['sims_admission_fypassingyear_exam'] = $scope.edt_dash_exam['sims_admission_fypassingyear_exam'];
                    $scope.edt_n['sims_admission_fyseat_no'] = $scope.edt_dash_exam['sims_admission_fyseat_no'];
                    $scope.edt_n['sims_fyadmission_no_of_attempts_sem1'] = $scope.edt_dash_exam['sims_fyadmission_no_of_attempts_sem1'];
                    $scope.edt_n['sims_fyadmission_marks_obtained_sem1'] = $scope.edt_dash_exam['sims_fyadmission_marks_obtained_sem1'];
                    $scope.edt_n['sims_fyadmission_marks_out_of_Sem1'] = $scope.edt_dash_exam['sims_fyadmission_marks_out_of_Sem1'];
                    $scope.edt_n['sims_fyadmission_percentage_obtained_sem1'] = $scope.edt_dash_exam['sims_fyadmission_percentage_obtained_sem1'];

                    $scope.edt_n['sims_fyadmission_no_of_attempts_sem2'] = $scope.edt_dash_exam['sims_fyadmission_no_of_attempts_sem2'];
                    $scope.edt_n['sims_fyadmission_marks_obtained_sem2'] = $scope.edt_dash_exam['sims_fyadmission_marks_obtained_sem2'];
                    $scope.edt_n['sims_fyadmission_marks_out_of_Sem2'] = $scope.edt_dash_exam['sims_fyadmission_marks_out_of_Sem2'];
                    $scope.edt_n['sims_fyadmission_percentage_obtained_sem2'] = $scope.edt_dash_exam['sims_fyadmission_percentage_obtained_sem2'];

                    $scope.edt_n['current_syschool_name'] = $scope.edt_dash_exam['current_syschool_name'];
                    $scope.edt_n['sims_admission_syboardname'] = $scope.edt_dash_exam['sims_admission_syboardname'];
                    $scope.edt_n['sims_admission_sycenter_name'] = $scope.edt_dash_exam['sims_admission_sycenter_name'];
                    $scope.edt_n['sims_admission_sypassingyear_exam'] = $scope.edt_dash_exam['sims_admission_sypassingyear_exam'];
                    $scope.edt_n['sims_admission_syseat_no'] = $scope.edt_dash_exam['sims_admission_syseat_no'];
                    $scope.edt_n['sims_syadmission_no_of_attempts_sem3'] = $scope.edt_dash_exam['sims_syadmission_no_of_attempts_sem3'];
                    $scope.edt_n['sims_syadmission_marks_obtained_sem3'] = $scope.edt_dash_exam['sims_syadmission_marks_obtained_sem3'];
                    $scope.edt_n['sims_syadmission_marks_out_of_Sem3'] = $scope.edt_dash_exam['sims_syadmission_marks_out_of_Sem3'];
                    $scope.edt_n['sims_syadmission_percentage_obtained_sem3'] = $scope.edt_dash_exam['sims_syadmission_percentage_obtained_sem3'];

                    $scope.edt_n['sims_syadmission_no_of_attempts_sem4'] = $scope.edt_dash_exam['sims_syadmission_no_of_attempts_sem4'];
                    $scope.edt_n['sims_syadmission_marks_obtained_sem4'] = $scope.edt_dash_exam['sims_syadmission_marks_obtained_sem4'];
                    $scope.edt_n['sims_syadmission_marks_out_of_sem4'] = $scope.edt_dash_exam['sims_syadmission_marks_out_of_sem4'];
                    $scope.edt_n['sims_syadmission_percentage_obtained_sem4'] = $scope.edt_dash_exam['sims_syadmission_percentage_obtained_sem4'];

                    $scope.edt_n['sims_student_attribute6'] = $scope.edt_dash_exam['sims_student_attribute6'];


                  //  $scope.$apply();

                });

                $http.get($scope.apiurl1 + "api/Comn/GetSubjectDMC?cur_code=" + str.curr_code + "&acad_yr=" + str.academic_year + "&grade=" + str.grade_code + "&adm_no=" + str.admission_number).then(function (mmmm) {
                    $scope.subject_lst = mmmm.data;
                });

            }

            $scope.ViewDetails_hide = function () {
                $scope.show_detals_flg = false;
            }

            $scope.getCommunicate = function () {
                console.log('ppp');
                admdetails = [];
                var adm_data = [];


                for (var i = 0; i < $scope.dash_data.length; i++) {
                    // var v = document.getElementById($scope.dash_data[i].admission_number);
                    // if ($scope.dash_data[i].admission_number1 == true)

                    var t = $scope.dash_data[i].admission_number;
                    var v = document.getElementById(t);

                    if ($scope.dash_data[i].admission_number1) {
                        adm_data.push($scope.dash_data[i].admission_number);
                        admdetails = admdetails + $scope.dash_data[i].admission_number + ',';
                    }
                }


                //if (adm_data.length <= 1)
                // {
                console.log(admdetails);
                $scope.div_Communication = true;
                $scope.maingrid1 = false;
                $scope.maingrid2 = false;
                $scope.email = [];
                // $('#commnModal').modal({ backdrop: 'static', keyboard: true });

                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/GetAdmissionTemplates").then(function (res) {
                    $scope.template_data = res.data;
                    for (var i = 0; i < res.data.length; i++) {
                        if ($scope.template_data[i].sims_msg_subject == 'DPS-MIS Admission Department') {
                            $scope.email['sims_msg_subject'] = $scope.template_data[i].sims_msg_subject;
                        }
                    }
                    $scope.getbody($scope.email['sims_msg_subject']);
                });

                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/GetAdmission_EmailIds?admission_nos=" + admdetails).then(function (res) {
                    $scope.emailId_data = res.data;
                });

            }

            $('#text-editor').wysihtml5();
            $('#text-editor1').wysihtml5();

            $scope.getbody = function (msg_type) {
                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/GetAdmissionTemplatesBody?template_subject=" + msg_type).then(function (res) {
                    $scope.tempBody_data = res.data;
                    var body = $scope.tempBody_data.sims_msg_body;
                    var v = document.getElementById('text-editor');
                    v.value = body;
                    $scope.email.msgbody = v.value;
                    $scope.flag = false;
                    console.log($scope.tempBody_data.sims_msg_sr_no);
                    $('#text-editor').data("wysihtml5").editor.setValue($scope.email.msgbody);

                    $scope.email['email_subject'] = msg_type;

                    $http.get(ENV.apiUrl + "api/common/Email/GetcheckEmailProfile?sr_no=" + $scope.tempBody_data.sims_msg_sr_no).then(function (res) {
                        $scope.emailProfile_data = res.data;
                        console.log($scope.emailProfile_data);
                    });
                });
            }

            var data1 = [];
            var lst_cc = [];

            $scope.sendMail = function () {

                $scope.email_exists = false;

                var define_admission = '';

                if ($scope.email.sims_msg_subject == undefined) {
                    swal({ title: "Alert", text: "Please Select Template to Sent Mail.", showCloseButton: true, width: 380, });
                    return;
                }
                else {

                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/GetAdmissionTemplatesBody?template_subject=" + $scope.email.sims_msg_subject).then(function (res) {
                        $scope.TemplatesBody_data = res.data;

                        $scope.sims_msg_body = $scope.TemplatesBody_data.sims_msg_body;
                        $scope.sims_msg_signature = $scope.TemplatesBody_data.sims_msg_signature;

                        var msgbody = $('#text-editor').val();
                        // console.log(msgbody);

                        define_admission = admdetails;
                        var adm_number = admdetails;

                        for (var i = 0; i < $scope.emailId_data.length; i++) {
                            var adm_no = adm_number.indexOf(',')
                            var admission_num = adm_number.substr(0, adm_no);
                            adm_number = adm_number.substr(adm_no + 1);

                            if ($scope.emailId_data[i].chk_email == true) {
                                //var adm_number = admdetails;


                                var data =
                                   ({
                                       emailsendto: $scope.emailId_data[i].emailid,
                                       body: msgbody,
                                       subject: $scope.email.email_subject,
                                       // subject: $scope.email.sims_msg_subject,
                                       comm_desc: msgbody,
                                       admis_num: admission_num,
                                       enrollnumber: admission_num,
                                       comm_method: 'E',
                                       comm_date: $scope.ddMMyyyy,
                                       sender_emailid: $scope.emailProfile_data,
                                       sims_recepient_id: '0'
                                   });

                                data1.push(data);

                                $scope.email_exists = true;
                                //console.log(data1);
                            }
                        }

                        if (($scope.email.ccTo == "" || $scope.email.ccTo == undefined) && ($scope.email_exists == false)) {
                            swal({ title: "Alert", text: "Unable to Sent Mail", showCloseButton: true, width: 380, });
                        }

                        if ($scope.email.ccTo != "" || $scope.email.ccTo != undefined) {
                            var v = [];
                            var s = $scope.email.ccTo;
                            if (s != null) {
                                v = s.split(',');
                            }

                            //var adm_number = admdetails;
                            //var adm_no = adm_number.indexOf(',')
                            //var admission_num = adm_number.substr(0, adm_no);

                            for (var i = 0; i < v.length; i++) {
                                var data =
                                  ({
                                      emailsendto: v[i],
                                      body: msgbody,
                                      subject: $scope.email.email_subject,
                                      // subject: $scope.email.sims_msg_subject,
                                      comm_desc: msgbody,
                                      admis_num: admission_num,
                                      enrollnumber: '',
                                      comm_method: 'E',
                                      comm_date: $scope.ddMMyyyy,
                                      sender_emailid: $scope.emailProfile_data,
                                      sims_recepient_id: '1'
                                  });

                                data1.push(data);
                            }

                            var data2 =
                                {
                                    attFilename: ''
                                }
                            lst_cc.push(data2);

                        }

                        // console.log(data1);

                        $http.post(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/CUDCommunication", data1).then(function (res) {
                            $scope.Admcomm_data = res.data;

                            if ($scope.Admcomm_data == true) {

                                $http.post(ENV.apiUrl + "api/common/Email/ScheduleMails_dpsd?filenames=" + JSON.stringify(lst_cc), data1).then(function (res) {
                                    $scope.ScheduleMails_data = res.data;

                                    if ($scope.ScheduleMails_data == true) {
                                        swal({ text: "The email has been sent successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                                    }
                                    else {
                                        $scope.commnModaldisplay();
                                    }

                                    $scope.commnModaldisplay();
                                });
                            }
                        });
                    });
                }

            }


            $scope.commnModaldisplay = function () {
                $scope.email = [];
                $scope.emailId_data = [];
                $scope.maingrid1 = true;
                $scope.maingrid2 = true;
                // $('#commnModal').modal('hide');
                $('#text-editor').data("wysihtml5").editor.clear();
                $scope.div_Communication = false;
            }

            $scope.CancelEmailId = function (indx) {
                console.log(indx);
                $scope.emailId_data.splice(indx, 1);
                console.log($scope.emailId_data.splice(indx, 1));
            }



            /*start Upload*/

            var formdata = new FormData();

            $scope.getTheFiles = function ($files) {
                $scope.filesize = true;
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                    var i = 0;
                    if ($files[i].size > 500000) {
                        $scope.filesize = false;
                        $scope.edt_dash.photoStatus = false;
                        swal({ text: "File Should Not Exceed 500Kb.", imageUrl: "assets/img/notification-alert.png" });
                    }
                });

            };

            $scope.uploadClick = function (str) {
                $scope.filesize = true;
                $scope.edt_dash1 = str;
                console.log($scope.edt_dash1);
                $scope.edt_dash.photoStatus = true;
                $scope.ins = false;
                formdata = new FormData();
            }

            $scope.file_changed = function (element, str) {
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;
                    });
                };

                reader.readAsDataURL(photofile);

                if (element.files[0].size < 500000) {
                    console.log($scope.edt_dash1.count);
                    if ($scope.edt_dash1.count < 2) {
                        console.log($scope.edt_dash1.sims_admission_doc_path);
                        if ($scope.edt_dash1.sims_admission_doc_path == null || $scope.edt_dash1.sims_admission_doc_path == "") {
                            $scope.edt_dash1.count = $scope.edt_dash1.count;
                        }
                        else {
                            $scope.edt_dash1.count = ($scope.edt_dash1.count) + 1;
                        }

                        // if ($scope.edt_dash1.count < 2)
                        {
                            var request = {
                                method: 'POST',
                                url: ENV.apiUrl + 'api/fileNew/uploadDocument?filename=' + $scope.edt_dash1.admis_num + '_' + $scope.edt_dash1.sims_criteria_code + '_' + $scope.edt_dash1.count + '(' + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss') + ')' + "&location=" + "Docs/Student",
                                data: formdata,
                                headers: {
                                    'Content-Type': undefined
                                }
                            };
                            $http(request).success(function (d) {
                                var data = {
                                    admis_num: $scope.edt_dash1.admis_num,
                                    sims_criteria_code: $scope.edt_dash1.sims_criteria_code,
                                    sims_admission_doc_path: d,
                                    sims_admission_doc_path_old: $scope.edt_dash1.sims_admission_doc_path
                                }

                                if (data.sims_admission_doc_path != null || data.sims_admission_doc_path != "") {
                                    if ($scope.edt_dash1.sims_admission_doc_path == null || $scope.edt_dash1.sims_admission_doc_path == "") {
                                        console.log($scope.edt_dash1.count);
                                        data.sims_admission_doc_status = 'false';
                                        data.sims_admission_doc_verify = 'false';
                                        data.opr = 'N';

                                        console.log(data);

                                        $http.post(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/CUD_Update_Admission_Doc", data).then(function (res) {
                                            $scope.ins = res.data;
                                            if (res.data) {
                                                swal({ text: "Document Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                                                $scope.edt_dash1.count = ($scope.edt_dash1.count) + 1;
                                            }

                                            $scope.UploadDocument($scope.edt_dash1.admis_num);
                                        });
                                    }
                                    else {
                                        console.log($scope.edt_dash1.count);
                                        $http.post(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/CUD_Insert_Admission_Doc", data).then(function (res) {
                                            $scope.ins = res.data;
                                            if (res.data) {
                                                swal({ text: "Document Uploaded Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                                                $scope.edt_dash1.count = ($scope.edt_dash1.count) + 1;
                                            }

                                            $scope.UploadDocument($scope.edt_dash1.admis_num);
                                        });
                                    }
                                }

                            });
                        }
                    }
                    else {
                        swal({ title: "Alert", text: "You Can Upload Max 2 File Documents For Each Criteria", showCloseButton: true, width: 380, });

                    }

                }
            };

            $scope.add_upload_doc = function () {
                var data = $scope.Upload_doc_datails;
                $http.post(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/CUD_Update_Admission_DocList", data).then(function (res) {
                    $scope.ins = res.data;
                    if (res.data) {
                    }
                    $scope.maingrid1 = true;
                    $scope.Upload_doc = false;
                    //$scope.GetInfo($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.edt_dash.sims_appl_parameter_reg);
                    $scope.getalldata($scope.edt_dash.curr_code, $scope.edt_dash.academic_year, $scope.edt_dash.grade_code, $scope.btn_visible, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll, $scope.paid_status);
                });
            }

            $scope.UploadDocModal_Reset = function () {
                $scope.maingrid1 = true;
                $scope.maingrid2 = true;
                $scope.Upload_doc = false;
            }

            $scope.doc_delete = function (doc_path, crit_code, adm_no) {
                swal({
                    title: '',
                    text: "Are you sure you want to Delete?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        $http.post(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/CUD_Delete_Admission_Doc?adm_no=" + adm_no + "&criteria_code=" + crit_code + "&doc_path=" + doc_path).then(function (res) {
                            $scope.del = res.data;
                            if (res.data) {
                                swal({ text: "Document Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                            }
                            $scope.UploadDocument(adm_no);
                        });
                    }
                });


            }

            /*End Upload*/

            /*start History Communication*/

            $scope.getcommunicationhistory = function (adm_No) {
                $scope.AdmissionNo = adm_No;
                $scope.div_CommunicationHist = true;
                $scope.maingrid1 = false;
                $scope.maingrid2 = false;
                // $('#commhistModal').modal({ backdrop: 'static', keyboard: true });

                $http.get(ENV.apiUrl + "api/common/ProspectDashboard/GetCommMethods").then(function (res) {
                    $scope.method_data = res.data;
                });

                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/getCommunication?adm_no=" + $scope.AdmissionNo).then(function (res) {
                    $scope.comm_data = res.data;
                    $scope.totalItems4 = $scope.comm_data.length;
                    $scope.todos4 = $scope.comm_data;
                    $scope.makeTodos4();
                    $scope.grid1 = true;
                    $scope.display1 = false;
                });

            }

            $scope.makeTodos4 = function () {
                var rem = parseInt($scope.totalItems4 % $scope.numPerPage4);
                if (rem == '0') {
                    $scope.pagersize4 = parseInt($scope.totalItems4 / $scope.numPerPage4);
                }
                else {
                    $scope.pagersize4 = parseInt($scope.totalItems4 / $scope.numPerPage4) + 1;
                }
                var begin4 = (($scope.currentPage4 - 1) * $scope.numPerPage4);
                var end4 = parseInt(begin4) + parseInt($scope.numPerPage4);

                $scope.filteredTodos4 = $scope.todos4.slice(begin4, end4);
            };

            $scope.size4 = function (str) {
                console.log(str);
                $scope.pagesize1 = str;
                $scope.currentPage4 = 1;
                $scope.numPerPage4 = str;
                console.log("numPerPage=" + $scope.numPerPage4);
                $scope.makeTodos4();
            }

            $scope.index4 = function (str) {
                $scope.pageindex4 = str;
                $scope.currentPage4 = str;
                console.log("currentPage4=" + $scope.currentPage4);
                $scope.makeTodos4();
            }

            $scope.New = function () {
                $scope.display1 = true;
                $scope.grid1 = false;
                $scope.temp = "";
            }



            $scope.CommHist_Cancel = function () {
                $scope.div_CommunicationHist = false;
                $scope.maingrid1 = true;
                $scope.maingrid2 = true;
            }

            $scope.commcancel_btn = function () {
                $scope.grid1 = true;
                $scope.display1 = false;
                $scope.myForm1.$setPristine();
                $scope.myForm1.$setUntouched();
            }

            $scope.Save = function (isvalidate) {

                var commdata1 = [];
                var commdata = [];

                if (isvalidate) {
                    var commdata = ({
                        admis_num: $scope.AdmissionNo,
                        comm_method: $scope.temp.comm_method,
                        comm_date: $scope.temp.comm_date,
                        comm_desc: $scope.temp.comm_desc,
                        enq_rem: $scope.temp.enq_rem,
                        status: $scope.temp.status,
                        opr: 'I'
                    });

                    commdata1.push(commdata);

                    $http.post(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/CUDCommunication", commdata1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Admission Communication Added Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getcommunicationhistory($scope.AdmissionNo);
                                }
                            });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Admission Communication Not Added Successfully. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getcommunicationhistory($scope.AdmissionNo);
                                }
                            });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                    $scope.myForm1.$setPristine();
                    $scope.myForm1.$setUntouched();
                }
            }

            $scope.searched4 = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil4(i, toSearch);
                });
            };

            $scope.search4 = function () {
                $scope.todos4 = $scope.searched4($scope.comm_data, $scope.searchText4);
                $scope.totalItems4 = $scope.todos4.length;
                $scope.currentPage4 = '1';
                if ($scope.searchText4 == '') {
                    $scope.todos4 = $scope.comm_data;
                }
                $scope.makeTodos4();
            }

            function searchUtil4(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.comm_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.comm_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            /* End History Communication*/



            /* start communication for sending mail/sms*/
            $scope.chk_email = true;
            $scope.chk_sms = false;

            $scope.edt_dash1 =
            {
                sims_Email: "true",
                sims_sms_char: '0',
                sims_sms_length: '0'
            };

            $scope.send_mail_details = function () {
                /* EMAIL*/
                if ($scope.edt_dash1.sims_Email == "true") {
                    swal({ title: "Alert", text: "Do You Want to Send Emails?", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.sendMaildetails();
                        }
                    });
                }
                if ($scope.edt_dash1.sims_Email == "false") {
                    swal({
                        title: '',
                        text: "Do You Want to Send SMS?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.OnsendSMSdetails();
                        }
                    });
                }
            }


            $http.get(ENV.apiUrl + "api/common/EmailSms/GetcheckEmailProfile").then(function (res) {
                $scope.getProfile = res.data;
                // console.log($scope.getProfile);
            });

            $scope.get_msgtypechecked = function (msg_type) {


                if (msg_type == "true") {
                    $scope.edt_dash1['sims_recepient_id'] = $scope.sims_admission_father_mail;
                    $scope.chk_email = true;
                    $scope.chk_sms = false;
                }
                else if (msg_type == "false") {
                    $scope.edt_dash1['sims_recepient_id'] = $scope.sims_admission_father_mobile;
                    $scope.chk_email = false;
                    $scope.chk_sms = true;
                    $scope.edt_dash1.sims_sms_msg = "Enter your message below ( max 1000 chars.1 sms is 160 characters.)";
                    $scope.edt_dash1.sims_sms_subjectblock = "These special characters ' ][}{|~^ ' will be treated as two character";
                }

            }

            $scope.msg_details = function () {
                if ($scope.edt_dash1.sims_smstext != "" && $scope.edt_dash1.sims_smstext != undefined) {
                    var q = 0, cnt = 0;
                    $scope.edt_dash1.sims_sms_char = 0;

                    var v = document.getElementById('txt_smstext');
                    var r = v.value.length;
                    var sms = $scope.edt_dash1.sims_smstext;
                    //console.log(v.value.length);

                    var nameList = new Array('[', ']', '{', '}');
                    if (sms != "") {
                        for (var x = 0, c = ''; c = sms.charAt(x) ; x++) {
                            for (var i = 0; i < nameList.length; i++) {
                                if (nameList[i] === c) {
                                    q = q + 1;
                                }
                            }
                        }
                        var ss = q * 2;
                        var smsLen = r;
                        cnt = (smsLen - q) + ss;
                        $scope.edt_dash1.sims_sms_char = cnt;
                    }

                    if ($scope.edt_dash1.sims_sms_char <= 160 && $scope.edt_dash1.sims_sms_char > 0)
                        $scope.edt_dash1.sims_sms_length = "1";
                    if ($scope.edt_dash1.sims_sms_char <= 320 && $scope.edt_dash1.sims_sms_char > 160)
                        $scope.edt_dash1.sims_sms_length = "2";
                    if ($scope.edt_dash1.sims_sms_char <= 480 && $scope.edt_dash1.sims_sms_char > 320)
                        $scope.edt_dash1.sims_sms_length = "3";
                    if ($scope.edt_dash1.sims_sms_char <= 640 && $scope.edt_dash1.sims_sms_char > 480)
                        $scope.edt_dash1.sims_sms_length = "4";
                    if ($scope.edt_dash1.sims_sms_char <= 800 && $scope.edt_dash1.sims_sms_char > 640)
                        $scope.edt_dash1.sims_sms_length = "5";
                    if ($scope.edt_dash1.sims_sms_char <= 960 && $scope.edt_dash1.sims_sms_char > 800)
                        $scope.edt_dash1.sims_sms_length = "6";
                    if ($scope.edt_dash1.sims_sms_char <= 1000 && $scope.edt_dash1.sims_sms_char > 960)
                        $scope.edt_dash1.sims_sms_length = "7";
                }
                else {
                    $scope.edt_dash1.sims_sms_char = "0";
                    $scope.edt_dash1.sims_sms_length = "0";
                }
            }

            $scope.sendMaildetails = function () {
                var data = [];
                $scope.edt_dash1.body = document.getElementById('text-editor1').value;

                var flag = true;
                /* EMAIL*/
                if ($scope.edt_dash1.sims_Email == "true") {
                    if (($scope.edt_dash1.sims_subject == "" || $scope.edt_dash1.sims_subject == undefined) && ($scope.edt_dash1.body == "" || $scope.edt_dash1.body == undefined)) {
                        flag = false;
                        swal({ title: "Alert", text: "Subject & Message Body are Required", showCloseButton: true, width: 380, });
                    }

                    if (flag == true) {
                        $scope.BUSY = true;
                        var data1 =
                            {
                                subject: $scope.edt_dash1.sims_subject,
                                body: $scope.edt_dash1.body,
                                sender_emailid: $scope.getProfile,
                                emailsendto: $scope.edt_dash1.sims_recepient_id
                            }
                        data.push(data1);

                        var data2 =
                                    {
                                        attFilename: ''
                                    }
                        lst_cc.push(data2);

                        $http.post(ENV.apiUrl + "api/common/Email/ScheduleMails_dpsd?filenames=" + JSON.stringify(lst_cc), data).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Mail Sent Successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                            }
                            else {
                                swal({ text: "Mail Not Sent.", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });

                            }
                        });
                    }
                }
            }

            $scope.username = $rootScope.globals.currentUser.username;

            $scope.OnsendSMSdetails = function () {
                var data3 = [];
                var smssendto = "";
                $scope.BUSY = true;

                var data2 =
                {
                    sims_smstext: $scope.edt_dash1.sims_smstext,
                    usercode: $scope.username,
                    smssendto: $scope.edt_dash1.sims_recepient_id
                }
                data3.push(data2);

                $http.post(ENV.apiUrl + "api/common/EmailSms/CUD_Insert_SMS_Schedule", data3).then(function (res) {
                    $scope.display = true;
                    $scope.msg1 = res.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "SMS sent successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                    }
                    else {
                        swal({ text: "SMS Not Sent", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                    }
                });
            }

            $scope.cancel_mail_details = function () {
                $scope.edt_dash1 = "";
                $scope.edt_dash1['sims_Email'] = "true";
                $scope.chk_email = true;
                $scope.chk_sms = false;
                $scope.get_msgtypechecked($scope.edt_dash1['sims_Email']);
            }

            /**/


            $scope.getDirectAdmDetails = function () {
                $('#viewdashDetailsModal').modal({ backdrop: 'static', keyboard: true });
                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_asd/Get_directadmissionDetails?cur_code=" + $scope.edt_dash.curr_code + "&acad_yr=" + $scope.edt_dash.academic_year + "&grade_code=" + $scope.edt_dash.grade_code).then(function (res) {
                    $scope.dirctadm_deatails = res.data;
                    $scope.totalItems5 = $scope.dirctadm_deatails.length;
                    $scope.todos5 = $scope.dirctadm_deatails;
                    $scope.makeTodos5();
                    $scope.admdetails = true;
                    $scope.maingrid1 = false;
                });
            }

            $scope.makeTodos5 = function () {
                var rem = parseInt($scope.totalItems5 % $scope.numPerPage5);
                if (rem == '0') {
                    $scope.pagersize5 = parseInt($scope.totalItems5 / $scope.numPerPage5);
                }
                else {
                    $scope.pagersize5 = parseInt($scope.totalItems5 / $scope.numPerPage5) + 1;
                }
                var begin5 = (($scope.currentPage5 - 1) * $scope.numPerPage5);
                var end5 = parseInt(begin5) + parseInt($scope.numPerPage5);

                $scope.filteredTodos5 = $scope.todos5.slice(begin5, end5);
            };

            $scope.size5 = function (str) {
                console.log(str);
                $scope.pagesize5 = str;
                $scope.currentPage5 = 1;
                $scope.numPerPage5 = str; console.log("numPerPage5=" + $scope.numPerPage5); $scope.makeTodos5();
            }

            $scope.index5 = function (str) {
                $scope.pageindex5 = str;
                $scope.currentPage5 = str; console.log("currentPage5=" + $scope.currentPage5); $scope.makeTodos5();
            }

            $scope.searched5 = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil5(i, toSearch);
                });
            };

            $scope.search5 = function () {
                $scope.todos5 = $scope.searched5($scope.dirctadm_deatails, $scope.searchText5);
                $scope.totalItems5 = $scope.todos5.length;
                $scope.currentPage5 = '1';
                if ($scope.searchText5 == '') {
                    $scope.todos5 = $scope.dirctadm_deatails;
                }
                $scope.makeTodos5();
            }

            function searchUtil5(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_student_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sibling_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
        }])

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])
})();