﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeAssesmentScheduleCont',
        ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.display = true;
            $scope.grid = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.flag = true;
            var str, cnt;
            var main;

            var admdetails = [];
            $scope.filesize = true;
            var date = new Date();
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');

            $scope.obj1 = [], $scope.obj3 = [];

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.edt =
                {
                    sims_assesment_date: $scope.ddMMyyyy,
                    from_date: $scope.ddMMyyyy,
                    to_date: $scope.ddMMyyyy,
                    sims_assesment_time: "12:00 AM"
                }

            // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {

                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;

                $scope.propertyName = propertyName;
            };

            $scope.countData = [

                  { val: 5, data: 5 },
                  { val: 10, data: 10 },
                  { val: 15, data: 15 },

            ]


            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.obj2 = res.data;
                $scope.edt['curr_code'] = $scope.obj2[0].sims_attendance_cur_code;
                $scope.curr_code = $scope.obj2[0].sims_attendance_cur_code;
                $scope.getCur($scope.edt.curr_code);
            });

            $http.get(ENV.apiUrl + "api/StudentAssessmentSchedule/GetAssesmentTeacher").then(function (res) {
                $scope.teach_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/StudentAssessmentSchedule/GetEmployeeApplicant").then(function (res) {
                $scope.applicant_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/StudentAssessmentSchedule/GetEmployeeRound").then(function (res) {
                $scope.emp_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/EmpProfile/GetEPData").then(function (res) {
                $scope.obj = res.data;
                
            });
            $scope.empoloyeeInfo = function (k) {
                debugger;
                $http.get(ENV.apiUrl + "api/EmpProfile/GetEPData").then(function (res) {
                    $scope.obj = res.data;
                
                });

                $http.get(ENV.apiUrl + "api/StudentAssessmentSchedule/GetEmployeeProfileData?sims_pros_no=" + k.sims_pros_no).then(function (res) {
                    $scope.temp1 = res.data;

                    $scope.temp = {
                        em_login_code: $scope.temp1[0].em_login_code,
                        em_number: $scope.temp1[0].em_number,
                        em_service_status_code: $scope.temp1[0].em_service_status_code,
                        em_status_code: $scope.temp1[0].em_status_code,
                        em_apartment_number: $scope.temp1[0].em_apartment_number,
                        em_family_name: $scope.temp1[0].em_family_name,
                        em_street_number: $scope.temp1[0].em_street_number,
                        em_area_number: $scope.temp1[0].em_area_number,
                        em_phone: $scope.temp1[0].em_phone,
                        em_fax: $scope.temp1[0].em_fax,
                        em_post: $scope.temp1[0].em_post,
                        em_emergency_contact_name2: $scope.temp1[0].em_emergency_contact_name2,
                        em_emergency_contact_number2: $scope.temp1[0].em_emergency_contact_number2,
                        em_joining_ref: $scope.temp1[0].em_joining_ref,
                        em_ethnicity_code: $scope.temp1[0].em_ethnicity_code,
                        em_handicap_status: $scope.temp1[0].em_handicap_status,
                        em_stop_salary_indicator: $scope.temp1[0].em_stop_salary_indicator,
                        em_agreement: $scope.temp1[0].em_agreement,
                        em_punching_status: $scope.temp1[0].em_punching_status,
                        em_bank_cash_tag: $scope.temp1[0].em_bank_cash_tag,
                        em_citi_exp_tag: $scope.temp1[0].em_citi_exp_tag,
                        em_leave_tag: $scope.temp1[0].em_leave_tag,
                        em_passport_issue_place: $scope.temp1[0].em_passport_issue_place,
                        em_passport_issuing_authority: $scope.temp1[0].em_passport_issuing_authority,
                        em_pssport_exp_rem_date: $scope.temp1[0].em_pssport_exp_rem_date,
                        em_visa_issuing_place: $scope.temp1[0].em_visa_issuing_place,
                        em_visa_issuing_authority: $scope.temp1[0].em_visa_issuing_authority,
                        em_visa_exp_rem_date: $scope.temp1[0].em_visa_exp_rem_date,
                        em_visa_type_code: $scope.temp1[0].em_visa_type_code,
                        em_agreement_start_date: $scope.temp1[0].em_agreement_start_date,
                        em_agreement_exp_date: $scope.temp1[0].em_agreement_exp_date,
                        em_agreemet_exp_rem_date: $scope.temp1[0].em_agreemet_exp_rem_date,
                        em_punching_id: $scope.temp1[0].em_punching_id,
                        em_dependant_full: $scope.temp1[0].em_dependant_full,
                        em_dependant_half: $scope.temp1[0].em_dependant_half,
                        em_dependant_infant: $scope.temp1[0].em_dependant_infant,
                        em_Bank_Code: $scope.temp1[0].em_Bank_Code,
                        em_bank_ac_no: $scope.temp1[0].em_bank_ac_no,
                        em_iban_no: $scope.temp1[0].em_iban_no,
                        em_route_code: $scope.temp1[0].em_route_code,
                        em_bank_swift_code: $scope.temp1[0].em_bank_swift_code,
                        em_gpf_ac_no: $scope.temp1[0].em_gpf_ac_no,
                        em_pan_no: $scope.temp1[0].em_pan_no,
                        em_labour_card_no: $scope.temp1[0].em_labour_card_no,
                        em_gosi_ac_no: $scope.temp1[0].em_gosi_ac_no,
                        em_gosi_start_date: $scope.temp1[0].em_gosi_start_date,
                        em_national_id: $scope.temp1[0].em_national_id,
                        em_secret_question_code: $scope.temp1[0].em_secret_question_code,
                        em_secret_answer: $scope.temp1[0].em_secret_answer,

                        em_building_number: $scope.temp1[0].em_building_number,
                        em_Company_Code: $scope.temp1[0].em_Company_Code,
                        em_Dept_Code: $scope.temp1[0].em_Dept_Code,
                        em_Designation_Code: $scope.temp1[0].em_Designation_Code,
                        em_Dest_Code: $scope.temp1[0].em_Dest_Code,
                        em_Grade_Code: $scope.temp1[0].em_Grade_Code,
                        em_Marital_Status_Code: $scope.temp1[0].em_Marital_Status_Code,
                        em_Nation_Code: $scope.temp1[0].em_Nation_Code,
                        em_Country_Code: $scope.temp1[0].em_Country_Code,
                        em_State_Code: $scope.temp1[0].em_State_Code,
                        em_City_Code: $scope.temp1[0].em_City_Code,
                        em_Religion_Code: $scope.temp1[0].em_Religion_Code,
                        em_Salutation_Code: $scope.temp1[0].em_Salutation_Code,
                        em_Sex_Code: $scope.temp1[0].em_Sex_Code,
                        em_Staff_Type_Code: $scope.temp1[0].em_Staff_Type_Code,
                        em_blood_group_code: $scope.temp1[0].em_blood_group_code,
                        em_date_of_birth: $scope.temp1[0].em_date_of_birth,
                        em_date_of_join: $scope.temp1[0].em_date_of_join,
                        em_dept_effect_from: $scope.temp1[0].em_dept_effect_from,
                        em_email: $scope.temp1[0].em_email,
                        em_emergency_contact_name1: $scope.temp1[0].em_emergency_contact_name1,
                        em_emergency_contact_number1: $scope.temp1[0].em_emergency_contact_number1,
                        em_first_name: $scope.temp1[0].em_first_name,
                        em_grade_effect_from: $scope.temp1[0].em_grade_effect_from,
                        em_last_name: $scope.temp1[0].em_last_name,
                        em_middle_name: $scope.temp1[0].em_middle_name,
                        em_mobile: $scope.temp1[0].em_mobile,
                        em_name_ot: $scope.temp1[0].em_name_ot,
                        em_national_id_expiry_date: $scope.temp1[0].em_national_id_expiry_date,
                        em_national_id_issue_date: $scope.temp1[0].em_national_id_issue_date,
                        em_passport_exp_date: $scope.temp1[0].em_passport_exp_date,
                        em_passport_issue_date: $scope.temp1[0].em_passport_issue_date,
                        em_passport_no: $scope.temp1[0].em_passport_no,
                        em_summary_address: $scope.temp1[0].em_summary_address,
                        em_summary_address_local_language: $scope.temp1[0].em_summary_address_local_language,
                        em_visa_exp_date: $scope.temp1[0].em_visa_exp_date,
                        em_stop_salary_from: $scope.temp1[0].em_stop_salary_from,
                        em_visa_issue_date: $scope.temp1[0].em_visa_issue_date,
                        em_visa_no: $scope.temp1[0].em_visa_no,
                        em_img: $scope.temp1[0].em_img,
                        em_resident_in_qatar_since: $scope.temp1[0].em_resident_in_qatar_since,
                        em_remarks_awards: $scope.temp1[0].em_remarks_awards,
                        em_name_of_spouse: $scope.temp1[0].em_name_of_spouse,
                        em_Spouse_Designation_Code: $scope.temp1[0].em_Spouse_Designation_Code,
                        em_spouse_organization: $scope.temp1[0].em_spouse_organization,
                        em_spouse_qatar_id: $scope.temp1[0].em_spouse_qatar_id,
                        em_spouse_contact_number: $scope.temp1[0].em_spouse_contact_number,
                        pays_work_type_code: $scope.temp1[0].pays_work_type_code,
                        pays_work_type_name: $scope.temp1[0].pays_work_type_name,
                        em_labour_card_no: $scope.temp1[0].em_labour_card_no,
                        em_work_permit_issue_date: $scope.temp1[0].em_work_permit_issue_date,
                        em_work_permit_expiry_date: $scope.temp1[0].em_work_permit_expiry_date,
                        em_personalemail: $scope.temp1[0].em_personalemail                        
                    }
                   
                $('#UploadDocModal').modal({ backdrop: 'static', keyboard: true });
                });
            }

      


            $scope.getAssessmentscheduleData = function () {
                $scope.filteredTodos = [];
                //if ($scope.edt.em_round == undefined) {
                //    swal({ title: "Alert", text: "Please select Round", showCloseButton: true, width: 380, });
                //}
                //else{
                    debugger;
                    $http.get(ENV.apiUrl + "api/StudentAssessmentSchedule/GetEmployeeAssesmentSchedule?from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date + "&round=" + $scope.edt.em_round + "&em_applicant_id=" + $scope.edt.em_applicant_id).then(function (res) {
                    $scope.assessmentscheduleData = res.data;
                    if ($scope.assessmentscheduleData.length > 0) {
                        $scope.table = true;
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.assessmentscheduleData.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.assessmentscheduleData.length, data: 'All' })
                        }
                        $scope.totalItems = $scope.assessmentscheduleData.length;
                        $scope.todos = $scope.assessmentscheduleData;
                        $scope.grid = true;
                        $scope.makeTodos();
                    }
                    else {
                        $scope.table = false;
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                        $scope.filteredTodos = [];
                    }

                });

            //}
        }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }


            $scope.check1 = function (dash) {

                $scope.sims_admission_father_mail = dash.sims_admission_father_email;
                $scope.sims_admission_father_mobile = dash.sims_admission_father_mobile;
                $scope.sims_admission_number = dash.sims_pros_no;
                $scope.sims_assesment_date = dash.sims_assesment_date;
                $scope.sims_assesment_time = dash.sims_assesment_time;

                console.log(dash);
                admdetails = dash.sims_pros_no;
                var v = document.getElementById(dash.sims_pros_no);

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }

                });
                // }

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.check = function () {
                debugger;
                main = document.getElementById('mainchk');
                del = [];
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_pros_no);
                        v.checked = true;
                        del.push($scope.filteredTodos[i].sims_pros_no);
                        $('tr').addClass("row_selected");
                        // }
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sims_pros_no;
                        var v = document.getElementById(t);
                        v.checked = false;
                        del.pop(t);
                        // $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }
            }

            $scope.cancel = function () {
                $scope.filteredTodos = [];
                $scope.pager = false;
            }


            $scope.save = function () {
                debugger;
                if ($scope.edt.em_round == undefined)
                {
                    swal({ title: "Alert", text: "Please select Round", showCloseButton: true, width: 380, });
                }
                else
                {
                    var data2 = [];
                    var send_data2 = [];

                    admdetails = [];

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        if ($scope.filteredTodos[i].pros_num1 == true) {
                            admdetails = admdetails + $scope.filteredTodos[i].sims_pros_no + ',';
                        }
                    }

                    if (admdetails.length == 0) {
                        swal({ title: "Alert", text: "Please select atleast 1 applicant", showCloseButton: true, width: 380, });
                        //$('#message').modal({ backdrop: 'static', keyboard: false });
                    }
                    else {
                        if ($scope.edt.sims_teacher_code == null) {
                            swal({ title: "Alert", text: "Please select Assesment Teacher.", showCloseButton: true, width: 380, });
                        }
                        else {
                            var data = ({
                                sims_teacher_code: $scope.edt.sims_teacher_code,
                                sims_assesment_date: $scope.edt.sims_assesment_date,
                                sims_prosno_list: admdetails,
                                sims_assessment_created_user: $rootScope.globals.currentUser.username,
                                sims_assesment_time: $scope.edt.sims_assesment_time,
                                em_round: $scope.edt.em_round,
                                opr: 'I'
                            });

                            data2.push(data);

                            var data_obj = ({
                                sims_teacher_code: $scope.edt.sims_teacher_code,
                                sims_assesment_date: $scope.edt.sims_assesment_date,
                                sims_prosno_list: admdetails,
                                sims_assessment_created_user: $rootScope.globals.currentUser.username,
                                sims_assesment_time: $scope.edt.sims_assesment_time,
                                em_round: $scope.edt.em_round,
                                opr: 'B'
                            });

                            send_data2.push(data_obj);


                            var copy_obj = angular.copy(send_data2);

                            $http.post(ENV.apiUrl + "api/StudentAssessmentSchedule/CUDInsertEmployeeScheduleDetails", data2).then(function (res) {
                                $scope.msg1 = res.data;
                                console.log($scope.msg1);

                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Mapped.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            swal({ title: "Email", text: "Do you want Send Email.", showCloseButton: true, showCancelButton: true, width: 380, }).then(function (isConfirm) {
                                                if (isConfirm) {

                                                    $http.post(ENV.apiUrl + "api/StudentAssessmentSchedule/CUDsend_email", send_data2).then(function (res) {
                                                    });
                                                    $scope.getAssessmentscheduleData();
                                                }
                                                else {
                                                    $scope.getAssessmentscheduleData();
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    }
                }
            }
            //new code for communication


            $('#text-editor1').wysihtml5();
            $scope.chk_email = true;
            $scope.chk_sms = false;

            $scope.edt1 =
            {
                sims_Email: "true",
                sims_sms_char: '0',
                sims_sms_length: '0'
            };

            $http.get(ENV.apiUrl + "api/common/EmailSms/GetcheckEmailProfile").then(function (res) {
                $scope.getProfile = res.data;
                // console.log($scope.getProfile);
            });

            $scope.send_mail_details = function () {
                /* EMAIL*/
                if ($scope.edt1.sims_Email == "true") {
                    swal({ title: "Alert", text: "Do You Want to Send Emails?", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.sendMaildetails();
                        }
                    });
                }
                if ($scope.edt1.sims_Email == "false") {
                    swal({
                        title: '',
                        text: "Do You Want to Send SMS?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.OnsendSMSdetails();
                        }
                    });
                }
            }

            $scope.get_msgtypechecked = function (msg_type) {


                if (msg_type == "true") {
                    $scope.edt1['sims_recepient_id'] = $scope.sims_admission_father_mail;
                    $scope.edt1['sims_body'] = $scope.body;
                    $scope.chk_email = true;
                    $scope.chk_sms = false;
                }
                else if (msg_type == "false") {
                    $scope.edt1['sims_recepient_id'] = $scope.sims_admission_father_mobile;
                    $scope.edt1['sims_smstext'] = $scope.txt4;
                    $scope.msg_details($scope.edt1.sims_smstext);
                    $scope.chk_email = false;
                    $scope.chk_sms = true;
                    $scope.edt1.sims_sms_msg = "Enter your message below ( max 1000 chars.1 sms is 160 characters.)";
                    $scope.edt1.sims_sms_subjectblock = "These special characters ' ][}{|~^ ' will be treated as two character";
                }

            }

            $scope.getcommunicate = function (dash) {

                $scope.sims_admission_father_mail = dash.sims_admission_father_email;
                $scope.sims_admission_father_mobile = dash.sims_admission_father_mobile;
                $scope.sims_admission_number = dash.sims_pros_no;
                $scope.sims_assesment_date = dash.sims_assesment_date;
                $scope.sims_assesment_time = dash.sims_assesment_time;

                $('#viewdashDetailsModal1').modal({ backdrop: 'static', keyboard: true });
                $scope.get_msgtypechecked($scope.edt1.sims_Email);



                $http.get(ENV.apiUrl + "api/common/Admission_dpsd/GetemailSubjectforInviting").then(function (res) {
                    $scope.emailSubject_datails = res.data;
                    // console.log($scope.emailSubject_datails);
                    $scope.edt1['sims_subject'] = $scope.emailSubject_datails[0].mailmsg_subject;
                    $scope.edt1['sims_body'] = $scope.emailSubject_datails[0].mailmsg_body;

                    var v = document.getElementById('text-editor1');
                    v.value = $scope.emailSubject_datails[0].mailmsg_body;
                    $scope.edt1['sims_body'] = v.value;
                    $('#text-editor1').data("wysihtml5").editor.setValue($scope.edt1['sims_body']);


                    $scope.old_str = angular.copy($scope.edt1.sims_body);

                    var txt = $scope.old_str.replace(/{appl_num}/g, $scope.sims_admission_number);
                    var txt1 = txt.replace(/{appl_date}/g, $scope.sims_assesment_date);
                    var txt2 = txt1.replace(/{app_time}/g, $scope.sims_assesment_time);

                    $scope.body = txt2;
                    $scope.edt1['sims_body'] = $scope.body;

                    $('#text-editor1').data("wysihtml5").editor.setValue($scope.edt1['sims_body']);
                });


                $http.get(ENV.apiUrl + "api/common/Admission_dpsd/GetSmsSubjectforInviting").then(function (res) {
                    $scope.smsSubject_datails = res.data;
                    $scope.edt1['sims_smstext'] = $scope.smsSubject_datails[0].mailmsg_body;

                    $scope.old_str1 = angular.copy($scope.edt1.sims_smstext);

                    var txtt = $scope.old_str1.replace(/{appl_num}/g, $scope.sims_admission_number);
                    var txtt1 = txtt.replace(/{appl_date}/g, $scope.sims_assesment_date);
                    var txtt2 = txtt1.replace(/{app_time}/g, $scope.sims_assesment_time);
                    $scope.txt4 = txtt2;
                    $scope.edt1['sims_smstext'] = $scope.txt4;
                    $scope.msg_details($scope.edt1.sims_smstext);
                });
            }

            $scope.msg_details = function (sims_smstext) {
                if (sims_smstext != "" && sims_smstext != undefined) {
                    var q = 0, cnt = 0;
                    $scope.edt1.sims_sms_char = 0;

                    var v = document.getElementById('txt_smstext');
                    var r = v.value.length;
                    // var sms = $scope.edt1.sims_smstext;
                    var sms = sims_smstext;
                    //console.log(v.value.length);

                    var nameList = new Array('[', ']', '{', '}');
                    if (sms != "") {
                        for (var x = 0, c = ''; c = sms.charAt(x) ; x++) {
                            for (var i = 0; i < nameList.length; i++) {
                                if (nameList[i] === c) {
                                    q = q + 1;
                                }
                            }
                        }
                        var ss = q * 2;
                        var smsLen = r;
                        cnt = (smsLen - q) + ss;
                        $scope.edt1.sims_sms_char = cnt;
                    }

                    if ($scope.edt1.sims_sms_char <= 160 && $scope.edt1.sims_sms_char > 0)
                        $scope.edt1.sims_sms_length = "1";
                    if ($scope.edt1.sims_sms_char <= 320 && $scope.edt1.sims_sms_char > 160)
                        $scope.edt1.sims_sms_length = "2";
                    if ($scope.edt1.sims_sms_char <= 480 && $scope.edt1.sims_sms_char > 320)
                        $scope.edt1.sims_sms_length = "3";
                    if ($scope.edt1.sims_sms_char <= 640 && $scope.edt1.sims_sms_char > 480)
                        $scope.edt1.sims_sms_length = "4";
                    if ($scope.edt1.sims_sms_char <= 800 && $scope.edt1.sims_sms_char > 640)
                        $scope.edt1.sims_sms_length = "5";
                    if ($scope.edt1.sims_sms_char <= 960 && $scope.edt1.sims_sms_char > 800)
                        $scope.edt1.sims_sms_length = "6";
                    if ($scope.edt1.sims_sms_char <= 1000 && $scope.edt1.sims_sms_char > 960)
                        $scope.edt1.sims_sms_length = "7";
                }
                else {
                    $scope.edt1.sims_sms_char = "0";
                    $scope.edt1.sims_sms_length = "0";
                }
            }

            var data1 = [];
            var lst_cc = [];

            $scope.sendMaildetails = function () {
                var data = [];
                $scope.edt1.body = document.getElementById('text-editor1').value;

                var flag = true;
                /* EMAIL*/
                if ($scope.edt1.sims_Email == "true") {
                    if (($scope.edt1.sims_subject == "" || $scope.edt1.sims_subject == undefined) && ($scope.edt1.body == "" || $scope.edt1.body == undefined)) {
                        flag = false;
                        swal({ title: "Alert", text: "Subject & Message Body are Required", showCloseButton: true, width: 380, });
                    }

                    if (flag == true) {
                        $scope.BUSY = true;
                        var data1 =
                            {
                                subject: $scope.edt1.sims_subject,
                                body: $scope.edt1.body,
                                sender_emailid: $scope.getProfile,
                                emailsendto: $scope.edt1.sims_recepient_id
                            }
                        data.push(data1);

                        var data2 =
                                    {
                                        attFilename: ''
                                    }
                        lst_cc.push(data2);

                        $http.post(ENV.apiUrl + "api/common/Email/ScheduleMails_dpsd?filenames=" + JSON.stringify(lst_cc), data).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Mail Sent Successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                            }
                            else {
                                swal({ text: "Mail Not Sent.", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });

                            }
                        });
                    }
                }
            }

            $scope.username = $rootScope.globals.currentUser.username;

            $scope.OnsendSMSdetails = function () {
                var data3 = [];
                var smssendto = "";
                $scope.BUSY = true;

                var data2 =
                {
                    sims_smstext: $scope.edt1.sims_smstext,
                    usercode: $scope.username,
                    smssendto: $scope.edt1.sims_recepient_id
                }
                data3.push(data2);

                $http.post(ENV.apiUrl + "api/common/EmailSms/CUD_Insert_SMS_Schedule", data3).then(function (res) {
                    $scope.display = true;
                    $scope.msg1 = res.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "SMS sent successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                    }
                    else {
                        swal({ text: "SMS Not Sent", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                    }
                });
            }

            $scope.cancel_mail_details = function () {
                $scope.edt1 = "";
                $scope.edt1['sims_Email'] = "true";
                $scope.chk_email = true;
                $scope.chk_sms = false;
                $scope.get_msgtypechecked($scope.edt1['sims_Email']);
            }

            /*end*/

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
        }])

})();