﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudDashAnalysisReportCont',
       ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV',
           function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

               $scope.modalGraphId='';
               $scope.edt = [];
               $http.get(ENV.apiUrl + "api/analysisreport/getCuriculum").then(function (res) {
                   $scope.curiculums=res.data;
                   debugger;
                   $scope.edt['cur_code_comp'] = $scope.curiculums[0].cur_code;
                   $scope.edt['cur_code_stat']=$scope.curiculums[0].cur_code;
                   $scope.edt['cur_code_nation'] = $scope.curiculums[0].cur_code; 
                   $scope.edt['cur_code_avail']=$scope.curiculums[0].cur_code;
                   $scope.edt['cur_code_other'] = $scope.curiculums[0].cur_code;

                   $scope.getfirstAcademicYear($scope.edt['cur_code_comp']);
                   $scope.getSecondAcademicYear($scope.edt['cur_code_stat']);
                   $scope.getThirdAcademicYear($scope.edt['cur_code_nation']);
                   $scope.getFourthAcademicYear($scope.edt['cur_code_avail']);
                   $scope.getFifthAcademicYear($scope.edt['cur_code_other']);
               })


               // 1. COMPARATIVE ANALYSIS OF STUDENT INTAKE//////////
               $scope.hideGraph=true;
        
  
               $scope.getfirstAcademicYear = function (curCode) {
                   $http.get(ENV.apiUrl + "api/analysisreport/getAcademicYear?cur_code=" + curCode).then(function (res) {
                       $scope.firstacademicYears = res.data;
                       $scope.edt['academic_year_comp'] = $scope.firstacademicYears[0].academic_year;
                       $scope.getDatafirstGraph($scope.edt['cur_code_comp'], $scope.edt['academic_year_comp']);
                       setTimeout(function () {
                           var e = document.getElementById('comp_acad');
                           $scope.selected = e.options[e.selectedIndex].text;
                       }, 300)
                   })
               }

               $scope.showcompBarChartModal=function(){
                   if (window.bar3 != undefined) {
                       window.bar3.destroy();
                   }
                   if (window.bar4 != undefined) {
                       window.bar4.destroy();
                   }
                   $scope.dash1_recieved_lst=[];
                   $scope.dash1_total_lst=[];
                   $('#firstgraphModal').modal({backdrop: 'static', keyboard: false});

                   $http.get(ENV.apiUrl + "api/analysisreport/getStudentIntakeData?curCode="+$scope.edt['cur_code_comp'] +"&acaYear="+$scope.edt['academic_year_comp']).then(function (res) {
                       $scope.studentIntakeData=res.data;

                       console.log($scope.studentIntakeData);
                       for(var i=0;$scope.studentIntakeData.length>i;i++){
                           if ($scope.studentIntakeData[i].enquiry_cnt == 0 && $scope.studentIntakeData[i].reg_stud == 0 && $scope.studentIntakeData[i].paid_stud == 0) {
                               $scope.errormsg = "Data Not Available";
                           } else {
                               $scope.dash1_total_lst.push($scope.studentIntakeData[i].enquiry_cnt);
                               $scope.dash1_total_lst.push($scope.studentIntakeData[i].reg_stud);
                               $scope.dash1_total_lst.push($scope.studentIntakeData[i].paid_stud);
                           }
                           if ($scope.studentIntakeData[i].per_stud_reg_to_enq == 0 && $scope.studentIntakeData[i].per_stud_paid_to_enq == 0 && $scope.studentIntakeData[i].per_stud_paid_to_reg) {
                               $scope.errormsg = "Data Not Available";
                           } else {
                               $scope.dash1_recieved_lst.push($scope.studentIntakeData[i].per_stud_paid_to_enq);
                               $scope.dash1_recieved_lst.push($scope.studentIntakeData[i].per_stud_reg_to_enq);
                               $scope.dash1_recieved_lst.push($scope.studentIntakeData[i].per_stud_paid_to_reg);
                           }
                       }
                       setTimeout(function(){
                           var ctx = document.getElementById("compBarChart2").getContext("2d");

                           var data = {
                               labels: ["Enq.Recieved", "Stud Reg", "Paid Fee"],
                               datasets: [{
                                   label: "Total No.",
                                   backgroundColor: ['#FF5A5E','#5AD3D1','#9467bd'],
                                   borderColor: ['#FF5A5E','#5AD3D1','#9467bd'],
                                   data: $scope.dash1_total_lst
                               }]
                           };
               
                           window.bar3 = new Chart(ctx, {
                               type: 'bar',
                               data: data,
                               options: {
                                   legend: {
                                       display: false,
                                   },
                                   title: {
                                       display: true,
                                       text: 'Total No. Of Student',
                                       position:'bottom'
                                   }
                               }
                           });

                       },300)

                       var ctx = document.getElementById("compBarChart4").getContext("2d");
                       var data = {
                           labels: ['Reg.Student enq. recieved(%)','Student paid to enq. recieved(%)', 'Student paid to reg.students(%)'],
                           datasets:[{  
                               data: $scope.dash1_recieved_lst,
                               backgroundColor: ['#FF5A5E','#5AD3D1','#9467bd']
                           }]
                       }


                       window.bar4 = new Chart(ctx, {
                           type: 'pie',
                           data: data,
                           options: {
                               legend: {
                                   display: true,                           
                                   labels: {
                                       boxWidth:10,
                                       fontSize:10,
                                   }
                               },
                               title: {
                                   display: true,
                                   text: 'Percentage Of Student',
                                   position:'bottom'
                               }
                           }
                       });  
                   })
               }
            
    
               $scope.getDatafirstGraph = function (cur, year) {
                   $scope.dash1_recieved_lst = [];
                   $scope.dash1_total_lst = [];
                   if (window.bar != undefined) {
                       window.bar.destroy();
                   }
                   if (window.bar1 != undefined) {
                       window.bar1.destroy();
                   }
                   $http.get(ENV.apiUrl + "api/analysisreport/getStudentIntakeData?curCode=" + cur + "&acaYear=" + year).then(function (res) {
                       $scope.studentIntakeData=res.data;
                       if($scope.studentIntakeData.length>0){
                           for(var i=0;$scope.studentIntakeData.length>i;i++){
                               if($scope.studentIntakeData[i].enquiry_cnt==0 && $scope.studentIntakeData[i].reg_stud==0 && $scope.studentIntakeData[i].paid_stud==0){
                                   $scope.errormsg="Data Not Available";
                               }else{
                                   $scope.dash1_total_lst.push($scope.studentIntakeData[i].enquiry_cnt);
                                   $scope.dash1_total_lst.push($scope.studentIntakeData[i].reg_stud);
                                   $scope.dash1_total_lst.push($scope.studentIntakeData[i].paid_stud);                        
                               }
                      
                               if($scope.studentIntakeData[i].per_stud_reg_to_enq==0 && $scope.studentIntakeData[i].per_stud_paid_to_enq ==0 && $scope.studentIntakeData[i].per_stud_paid_to_reg){
                                   $scope.errormsg="Data Not Available";
                               }else{
                                   $scope.dash1_recieved_lst.push($scope.studentIntakeData[i].per_stud_paid_to_enq);
                                   $scope.dash1_recieved_lst.push($scope.studentIntakeData[i].per_stud_reg_to_enq);
                                   $scope.dash1_recieved_lst.push($scope.studentIntakeData[i].per_stud_paid_to_reg);                          
                               }
                           }

                           var ctx = document.getElementById("compBarChart").getContext("2d");

                           ctx.canvas.parentNode.style.height = '120px';
                    
                           var data = {
                               labels: ["Enq Rec", "Stud Reg", "Paid Fee"],
                               datasets: [{
                                   label: "Total No.",
                                   backgroundColor: ['#FF5A5E','#5AD3D1','#9467bd'],
                                   borderColor: ['#FF5A5E','#5AD3D1','#9467bd'],
                                   data: $scope.dash1_total_lst
                               }]
                           };



                           window.bar = new Chart(ctx, {
                               type: 'bar',
                               data: data,
                               options: {
                                   legend: {
                                       display: false,
                                   },
                                   title: {
                                       display: true,
                                       text: 'Total No. Of Student',
                                       position:'bottom'
                                   }
                               }
                           });

                           var ctx3 = document.getElementById("compBarChart3").getContext("2d");

                           ctx3.canvas.parentNode.style.height = '128px';

                           var data = {
                               labels: ['Reg.Student enq. recieved(%)','Student paid to enq. recieved(%)', 'Student paid to reg.students(%)'],
                               datasets:[{  
                                   data: $scope.dash1_recieved_lst,
                                   backgroundColor: ['#FF5A5E','#5AD3D1','#9467bd']
                               }]
                           }

                           window.bar1 = new Chart(ctx3, {
                               type: 'pie',
                               data: data,
                               options: {
                                   legend: {
                                       display: false,
                                       labels: {
                                           boxWidth:10,
                                           fontSize:10,
                                           position:'bottom'
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: 'Percentage Of Student',
                                       position:'bottom'
                                   }
                               }
                           });  

                       }else{
                           $scope.errormsg="Data Not Available";
                       } 
                   })            
               }
            

               $scope.showTableData=function(){
                   $scope.hideGraph=false;
               }
               $scope.showGraph=function(){
                   $scope.hideGraph=true;
               }



               ////////// end  //////////

               // 2. STUDENT STATUS //////////////////


               $scope.showsecondgraphTable=function(){
                   $scope.hideGraph=false;
               }
               $scope.showsecondgraph=function(){
                   $scope.hideGraph=true;
               }

               $scope.getSecondAcademicYear = function (curCode) {
                   $http.get(ENV.apiUrl + "api/analysisreport/getAcademicYear?cur_code="+curCode).then(function (res) {
                       $scope.secondacademicYears=res.data;
                       $scope.edt['academic_year_stat'] = $scope.secondacademicYears[0].academic_year;
                       $scope.graphStudentStatus($scope.edt['cur_code_stat'], $scope.edt['academic_year_stat']);
                   })
               }


               $scope.graphStudentStatus = function (cur, year) {
                   $scope.dash2_grades_lst = [];
                   $scope.dash2_present_class_lst = [];
                   $scope.dash2_class_capacity_lst = [];
                   $scope.dash2_curr_capacity_lst = [];
                   $scope.dash2_active_students_lst = [];
                   $scope.dash2_withdrawn_students_lst = [];
                   $scope.dash2_present_attendees_lst = [];
                   $scope.dash2_vacancy_lst = [];

                   if (window.bar5 != undefined) {
                       window.bar5.destroy();
                   }

                   $http.get(ENV.apiUrl + "api/analysisreport/getStudentStatusData?curCode=" + cur + "&acaYear=" + year).then(function (res) {
                       console.log(res.data);    
                       $scope.studStsChartData=res.data;        
                       for (var i = 0; i < res.data.length; i++) {
                           if(res.data[i].grades!="Grand Total"){
                               $scope.dash2_grades_lst.push(res.data[i].grades);                    
                               $scope.dash2_present_class_lst.push(res.data[i].present_class);
                               $scope.dash2_class_capacity_lst.push(res.data[i].class_capacity);
                               $scope.dash2_curr_capacity_lst.push(res.data[i].curr_capacity);
                               $scope.dash2_active_students_lst.push(res.data[i].active_students);
                               $scope.dash2_withdrawn_students_lst.push(res.data[i].withdrawn_students);
                               $scope.dash2_present_attendees_lst.push(res.data[i].present_attendees);
                               $scope.dash2_vacancy_lst.push(res.data[i].vacancy);
                           }

                       }

                       var ctxs = document.getElementById("studStsChart").getContext("2d");

                     //  ctxs.canvas.parentNode.style.width = '528px';
                       ctxs.canvas.parentNode.style.height = '400px';

                       var StatusData = {
                           labels: $scope.dash2_grades_lst,
                           datasets: [
                           //    {
                           //    label: "Class Capacity",
                           //    backgroundColor: "#1f77b4",
                           //    borderColor: "#1f77b4",
                           //    data: $scope.dash2_class_capacity_lst
                           //},
                           //{
                           //    label: "Present Classrooms",
                           //    backgroundColor: "#e377c2",
                           //    borderColor: "#e377c2",
                           //    data: $scope.dash2_present_class_lst
                           //},
                             {
                                 label: "Current Capacity",
                                 backgroundColor: "#7f7f7f",
                                 borderColor: "#7f7f7f",
                                 data: $scope.dash2_curr_capacity_lst
                             },
                             //{
                             //    label: "Registered Students",
                             //    backgroundColor: "#8c564b",
                             //    borderColor: "#8c564b",
                             //    data: [0, 0, 0, 0, 0, 0, 0]
                             //},
                             {
                                 label: "Active Students",
                                 backgroundColor: "#9467bd",
                                 borderColor: "#9467bd",
                                 data: $scope.dash2_active_students_lst
                             },
                             {
                                 label: "No.of Withdrawn",
                                 backgroundColor: "#2ca02c",
                                 borderColor: "#2ca02c",
                                 data: $scope.dash2_withdrawn_students_lst
                             },
                               {
                                   label: "Present Attendees",
                                   backgroundColor: "#d62728",
                                   borderColor: "#d62728",
                                   data: $scope.dash2_present_attendees_lst
                               },
                               {
                                   label: "Vacancy Per Class",
                                   backgroundColor: "#ff7f0e",
                                   borderColor: "#ff7f0e",
                                   data: $scope.dash2_vacancy_lst
                               }
                           ]
                       };



                       setTimeout(function() {
                           window.bar5 = new Chart(ctxs, {
                               type: 'bar',
                               data: StatusData,
                               options: {
                                   responsive: true,
                                   maintainAspectRatio: false,
                                   //scales: {
                                   //    xAxes: [{
                                   //        stacked: true
                                   //    }],
                                   //    yAxes: [{
                                   //        stacked: true
                                   //    }]
                                   //},
                                   legend: {
                                       display: true,
                                       labels: {
                                           boxWidth:10,
                                           fontSize:10,
                                           position:'bottom'
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: 'Student Status',
                                       position:'bottom'
                                   }
                               }
                           });

                       }, 300);

                   });
               }


               $scope.showGraphModal=function(){
                   $('#secondgraphModal').modal({backdrop: 'static', keyboard: false});
                   $scope.dash2_grades_lst=[];
                   $scope.dash2_present_class_lst=[];
                   $scope.dash2_class_capacity_lst=[];
                   $scope.dash2_curr_capacity_lst=[];
                   $scope.dash2_active_students_lst=[];
                   $scope.dash2_withdrawn_students_lst=[];
                   $scope.dash2_present_attendees_lst=[];
                   $scope.dash2_vacancy_lst=[];
                   if (window.bar6 != undefined) {
                       window.bar6.destroy();
                   }
                   $http.get(ENV.apiUrl + "api/analysisreport/getStudentStatusData?curCode="+$scope.edt['cur_code_stat']+"&acaYear="+$scope.edt['academic_year_stat']).then(function (res) {                    
                       for (var i = 0; i < res.data.length; i++) {
                           if(res.data[i].grades!="Grand Total"){
                               $scope.dash2_grades_lst.push(res.data[i].grades);                    
                               $scope.dash2_present_class_lst.push(res.data[i].present_class);
                               $scope.dash2_class_capacity_lst.push(res.data[i].class_capacity);
                               $scope.dash2_curr_capacity_lst.push(res.data[i].curr_capacity);
                               $scope.dash2_active_students_lst.push(res.data[i].active_students);
                               $scope.dash2_withdrawn_students_lst.push(res.data[i].withdrawn_students);
                               $scope.dash2_present_attendees_lst.push(res.data[i].present_attendees);
                               $scope.dash2_vacancy_lst.push(res.data[i].vacancy);
                           }
                       }
     
                       var ctx = document.getElementById("studStsChart2").getContext("2d");
                       ctx.canvas.parentNode.style.height = '400px';
                       var modaldata = {
                           labels: $scope.dash2_grades_lst,
                           datasets: [
                           //    {
                           //    label: "Class Capacity",
                           //    backgroundColor: "#1f77b4",
                           //    borderColor: "#1f77b4",
                           //    data: $scope.dash2_class_capacity_lst
                           //}, {
                           //    label: "Present Classrooms",
                           //    backgroundColor: "#e377c2",
                           //    borderColor: "#e377c2",
                           //    data: $scope.dash2_present_class_lst
                           //},
                             {
                                 label: "Current Capacity",
                                 backgroundColor: "#7f7f7f",
                                 borderColor: "#7f7f7f",
                                 data: $scope.dash2_curr_capacity_lst
                             },
                             //{
                             //    label: "Registered Students",
                             //    backgroundColor: "#8c564b",
                             //    borderColor: "#8c564b",
                             //    data: [0,0,0,0,0,0,0]
                             //},
                             {
                                 label: "Active Students",
                                 backgroundColor: "#9467bd",
                                 borderColor: "#9467bd",
                                 data: $scope.dash2_active_students_lst
                             },
                             {
                                 label: "No.of Withdrawn",
                                 backgroundColor: "#2ca02c",
                                 borderColor: "#2ca02c",
                                 data: $scope.dash2_withdrawn_students_lst
                             },
                               {
                                   label: "Present Attendees",
                                   backgroundColor: "#d62728",
                                   borderColor: "#d62728",
                                   data: $scope.dash2_present_attendees_lst
                               },
                               {
                                   label: "Vacancy Per Class",
                                   backgroundColor: "#ff7f0e",
                                   borderColor: "#ff7f0e",
                                   data: $scope.dash2_vacancy_lst
                               }
                           ]
                       };


                       setTimeout(function() {
                           window.bar6 = new Chart(ctx, {
                               type: 'bar',
                               data: modaldata,
                               options: {
                                   responsive: true,
                                   maintainAspectRatio: false,
                                   //scales: {
                                   //    xAxes: [{
                                   //        stacked: true
                                   //    }],
                                   //    yAxes: [{
                                   //        stacked: true
                                   //    }]
                                   //},
                                   legend: {
                                       display: true,
                                       labels: {
                                           boxWidth:10,
                                           fontSize:10
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: 'Student Status',
                                       position:'bottom'
                                   }
                               }
                           });

                       }, 300);

                   });
               }

               //////////// end /////////////////            

               // 3. NATIONAITY OF ACTIVE STUDENTS /////////
        
               var ctx = document.getElementById("nationChart").getContext("2d");
               var data = {}
               $scope.shownationChartModal=function(){
                   $('#thirdgraphModal').modal({backdrop: 'static', keyboard: false});
                   $scope.dash3_nationality_lst=[];
                   $scope.dash3_male_lst=[];
                   $scope.dash3_female_lst=[];
                   $scope.dash3_total_lst=[];
                   $scope.dash3_percentage_lst=[];
                   $scope.nationChart = [];
                   if (window.bar8 != undefined) {
                       window.bar8.destroy();
                   }

                   $http.get(ENV.apiUrl + "api/analysisreport/getStudentNationalityData?curCode="+$scope.edt['cur_code_nation']+"&acaYear="+$scope.edt['academic_year_nation']).then(function (res) {     
                       $scope.nationChart = res.data;
                       for (var i = 0; i < res.data.length; i++) {
                           if(res.data[i].country!='Grand Total'){
                               $scope.dash3_nationality_lst.push(res.data[i].country)                    
                               $scope.dash3_male_lst.push(res.data[i].male)
                               $scope.dash3_female_lst.push(res.data[i].female)
                               $scope.dash3_total_lst.push(res.data[i].total);
                               $scope.dash3_percentage_lst.push(res.data[i].percentage);
                           }

                       };

                       var ctx = document.getElementById("nationChart2").getContext("2d");
                       ctx.canvas.parentNode.style.height = '400px';
                       var data = {
                           labels: $scope.dash3_nationality_lst,
                           datasets: [{
                               label: "Female",
                               backgroundColor: "#e74e81",
                               borderColor: "#e74e81",
                               pointBackgroundColor:"#e74e81",
                               data:  $scope.dash3_female_lst
                           },{
                               label: "Male",
                               backgroundColor: "#4365B0",
                               borderColor: "#4365B0",
                               pointBackgroundColor:"#4365B0",
                               data:  $scope.dash3_male_lst
                           },
                               {
                                   label: "Total",
                                   backgroundColor: "#5AD3D1",
                                   borderColor: "#5AD3D1",
                                   pointBackgroundColor:"#5AD3D1",                    
                                   data: $scope.dash3_total_lst
                               },
                              {
                                  label: "Percentage(%)",
                                  backgroundColor: "#D00",
                                  borderColor: "#D00",
                                  pointBackgroundColor:"#D00",                    
                                  data: $scope.dash3_percentage_lst
                              }]
                       };

                       window.bar8 = new Chart(ctx, {
                           type: 'bar',
                           data: data,
                           options: {
                               responsive: true,
                               maintainAspectRatio: false,
                               legend: {
                                   display: true,
                               },
                               title: {
                                   display: true,
                                   text: 'Nationality Of Active Students',
                                   position:'bottom'
                               },
                               pointRadius:[15,10],
                               pointHoverRadius:[40,40]  
                           }
                       });
                   })

               }

               $scope.getThirdAcademicYear=function(curCode){
                   $http.get(ENV.apiUrl + "api/analysisreport/getAcademicYear?cur_code="+curCode).then(function (res) {
                       $scope.thirdacademicYears=res.data;
                       $scope.edt['academic_year_nation'] = $scope.thirdacademicYears[0].academic_year;
                       $scope.getDataThirdGraph($scope.edt['cur_code_nation'], $scope.edt['academic_year_nation']);
                   })
               }
               $scope.getDataThirdGraph = function (cur, year) {
                   $scope.dash3_nationality_lst = [];
                   $scope.dash3_male_lst = [];
                   $scope.dash3_female_lst = [];
                   $scope.dash3_total_lst = [];
                   $scope.dash3_percentage_lst = [];
                   if (window.bar7 != undefined) {
                       window.bar7.destroy();
                   }
                   $http.get(ENV.apiUrl + "api/analysisreport/getStudentNationalityData?curCode=" + cur + "&acaYear=" + year).then(function (res) {
                       $scope.nationChart=res.data;       
                       for (var i = 0; i < res.data.length; i++) {
                           if(res.data[i].country!='Grand Total'){
                               $scope.dash3_nationality_lst.push(res.data[i].country)                    
                               $scope.dash3_male_lst.push(res.data[i].male)
                               $scope.dash3_female_lst.push(res.data[i].female)
                               $scope.dash3_total_lst.push(res.data[i].total);
                               $scope.dash3_percentage_lst.push(res.data[i].percentage);
                           }

                       };

                       var ctxn = document.getElementById("Canvas1").getContext("2d");
                       ctxn.canvas.parentNode.style.height = '400px';

                       data = {
                           labels: $scope.dash3_nationality_lst,
                           datasets: [{
                               label: "Female",
                               backgroundColor: "#e74e81",
                               borderColor: "#e74e81",
                               pointBackgroundColor:"#e74e81",
                               data:  $scope.dash3_female_lst
                           },{
                               label: "Male",
                               backgroundColor: "#4365B0",
                               borderColor: "#4365B0",
                               pointBackgroundColor:"#4365B0",
                               data:  $scope.dash3_male_lst
                           },
                               {
                                   label: "Total",
                                   backgroundColor: "#5AD3D1",
                                   borderColor: "#5AD3D1",
                                   pointBackgroundColor:"#5AD3D1",                    
                                   data: $scope.dash3_total_lst
                               },
                              {
                                  label: "Percentage(%)",
                                  backgroundColor: "#D00",
                                  borderColor: "#D00",
                                  pointBackgroundColor:"#D00",                    
                                  data: $scope.dash3_percentage_lst
                              }]
                       };

                     
                       window.bar7 = new Chart(ctxn, {
                           type: 'bar',
                           data: data,
                           options: {
                               responsive: true,
                               maintainAspectRatio: false,
                               legend: {
                                   display: true,
                                   labels: {
                                       boxWidth: 10,
                                       fontSize: 10
                                   },
                               },
                               title: {
                                   display: true,
                                   text: 'Nationality Of Active Students',
                                   position:'bottom'
                               },
                               pointRadius:[15,10],
                               pointHoverRadius:[40,40]           
                           }
                       });
                   })
               }

               $scope.showthirdgraphTable = function () {
                   $scope.hideGraph = false;
               }
               $scope.showthirdgraph = function () {
                   $scope.hideGraph = true;
               }

          
               /////////// end ////////////////


               // 4. OTHER STATISTICS  /////////

               $scope.showotherStatChartModal=function(){
                   $('#fourthgraphModal').modal({ backdrop: 'static', keyboard: false });
                   $http.get(ENV.apiUrl + "api/analysisreport/getOtherStatistics?curCode="+$scope.edt['cur_code_other']+"&acaYear="+$scope.edt['academic_year_other']).then(function (res) {     
                       $scope.otherstat = res.data;
                       if (window.bar10 != undefined) {
                           window.bar10.destroy();
                       }
                       for(var i=0;$scope.otherstat.length>i;i++){
                           var o=$scope.otherstat[i].avg_expected +','+$scope.otherstat[i].avg_concession+','+$scope.otherstat[i].avg_net_tution
                           var b = o.split(',').map(function(item) {
                               return parseInt(item, 10);
                           });
                           var ctx = document.getElementById("otherStatChart2").getContext("2d");
                           var data = {
                               labels: ['Avg. Gross Tuition Fee','Avg. Discount', 'Avg. Net Tuition Fee'],
                               datasets:[{  
                                   data: b,
                                   backgroundColor: ['#FF5A5E','#5AD3D1','#9467bd']
                               }]
                           }

                           window.bar10 = new Chart(ctx, {
                               type: 'doughnut',
                               data: data,
                               options: {
                                   legend: {
                                       display: true,
                                       labels: {
                                           boxWidth:10,
                                           fontSize:10
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: 'Other Statistics',
                                       position:'bottom'
                                   }, tooltips: {
                                       mode: 'index',
                                       callbacks: {
                                           // Use the footer callback to display the sum of the items showing in the tooltip
                                           footer: function(tooltipItems, data) {
                                               var sum = 0;

                                               tooltipItems.forEach(function(tooltipItem) {
                                                   sum += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                               });
                                               return 'AED: ' + sum;
                                           },
                                       },
                                       footerFontStyle: 'normal'
                                   }
                               }
                           });  
                       }
                   })
               }
               $scope.getFifthAcademicYear = function (curCode) {
                   $http.get(ENV.apiUrl + "api/analysisreport/getAcademicYear?cur_code=" + curCode).then(function (res) {
                       $scope.statacademicYears = res.data;
                       $scope.edt['academic_year_other'] = $scope.statacademicYears[0].academic_year;
                       $scope.getDataOtherStat( $scope.edt['cur_code_other'],$scope.edt['academic_year_other'])
                   })
               }
               $scope.getDataOtherStat=function(cur,year){
                   $http.get(ENV.apiUrl + "api/analysisreport/getOtherStatistics?curCode="+cur+"&acaYear="+year).then(function (res) {     
                       $scope.otherstat = res.data;
                       if (window.bar9 != undefined) {
                           window.bar9.destroy();
                       }
                       if($scope.otherstat.length>0){
                           for(var i=0;$scope.otherstat.length>i;i++){
                               var o=$scope.otherstat[i].avg_expected +','+$scope.otherstat[i].avg_concession+','+$scope.otherstat[i].avg_net_tution
                               var b = o.split(',').map(function(item) {
                                   return parseInt(item, 10);
                               });
                               var ctxo = document.getElementById("otherStatChart").getContext("2d");
                               ctxo.canvas.parentNode.style.height = '128px';
                               var data = {
                                   labels: ['Avg. Gross Tuition Fee','Avg. Discount', 'Avg. Net Tuition Fee'],
                                   datasets:[{  
                                       data: b,
                                       backgroundColor: ['#FF5A5E','#5AD3D1','#9467bd']
                                   }]
                               }

                               window.bar9 = new Chart(ctxo, {
                                   type: 'doughnut',
                                   data: data,
                                   options: {
                                       legend: {
                                           display: false,
                                           labels: {
                                               boxWidth:10,
                                               fontSize:10
                                           }
                                       },
                                       title: {
                                           display: true,
                                           text: 'Other Statistics',
                                           position:'bottom'
                                       }, tooltips: {
                                           mode: 'index',
                                           callbacks: {
                                               // Use the footer callback to display the sum of the items showing in the tooltip
                                               footer: function(tooltipItems, data) {
                                                   var sum = 0;

                                                   tooltipItems.forEach(function(tooltipItem) {
                                                       sum += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                                   });
                                                   return 'AED: ' + sum;
                                               },
                                           },
                                           footerFontStyle: 'normal'
                                       }
                                   }
                               });  
                           }
                       }else{
                           $scope.errorMessage="Other Statistics Data Not Available"
                       }
                   })               
                       
               }       

               /////////// end //////////////// 


               // 5. STAFF DETAILS

               if ($http.defaults.headers.common['schoolId'] == 'siso') {
                   $scope.currStaffType = 'AA';                   
                   $scope.staffdropdown = true;
                   $scope.staffType = [{ key: 'AA', value: 'All' }, { key: 'AC', value: 'Academic' }, { key: 'AD', value: 'Administration' }, { key: 'OT', value: 'Other' }]

               } else {
                   $scope.staffdropdown = false;
               }

               $scope.staffSelect2 = function (stafftype) {
                   $scope.currStaffType = stafftype;
                   $scope.showStaffDetailsiso();
               }
               $scope.showStaffDetailsiso = function () {              

                   $http.get(ENV.apiUrl + "api/analysisreport/getNewStaffDetailsData?sopr=" + $scope.currStaffType).then(function (res) {
                   console.log(res.data);
                   $scope.staffData = res.data;
                   $scope.legends = [];
                   $scope.labels = [];
                   if (window.bar11 != undefined) {
                       window.bar11.destroy();
                   }
                   var ctxst = document.getElementById("staffDetailChart").getContext("2d");
                 //  ctxst.canvas.parentNode.style.width = '528px';
                   ctxst.canvas.parentNode.style.height = '400px';

                   angular.forEach($scope.staffData, function (value, key) {
                       angular.forEach(value, function (value1, key1) {
                           if (key == 0) {
                               if (key1 != 'nationality') {
                                   $scope.legends.push(key1);
                               }
                           }
                           if (key1 == 'nationality') {
                               $scope.labels.push(value1);
                           }
                       })
                   })
                   $scope.dataset = [];
                   console.log($scope.legends)
                   angular.forEach($scope.legends, function (value, key) {
                       var color = Math.floor(Math.random() * 16777216).toString(16);
                       var backcolor = '#000000'.slice(0, -color.length) + color;
                       $scope.dataset.push({ label: value, backgroundColor: backcolor, data: [] })
                   });

                   angular.forEach($scope.staffData, function (value, key) {
                       angular.forEach(value, function (value1, key1) {
                           angular.forEach($scope.legends, function (value2, key2) {
                               if (key1 != 'nationality') {
                                   if (key1 == value2) {
                                       $scope.dataset[key2]['data'].push(value1);
                                   }
                               }
                           });
                       })
                   })


                   var modaldata = {
                       labels: $scope.labels,
                       datasets: $scope.dataset
                   };


                   setTimeout(function () {
                       window.bar11 = new Chart(ctxst, {
                           type: 'bar',
                           data: modaldata,
                           options: {
                               responsive: true,
                               maintainAspectRatio: false,
                               legend: {
                                   display: true,
                                   labels: {
                                       boxWidth: 10,
                                       fontSize: 10
                                   }
                               },
                               title: {
                                   display: true,
                                   text: 'Staff Details',
                                   position: 'bottom'
                               }
                           }
                       });

                   }, 300);

               })
               }



                $scope.showstaffDetailModal=function(){
                    if ($http.defaults.headers.common['schoolId'] == 'siso') {                        
                        $scope.currStaffType = 'AA';
                        $scope.staffdropdown = true;
                        $scope.staffType = [{ key: 'AA', value: 'All' }, { key: 'AC', value: 'Academic' }, { key: 'AD', value: 'Administration' }, { key: 'OT', value: 'Other' }]
                        $('#fifthgraphModal').modal({ backdrop: 'static', keyboard: false });
                        $scope.staffSelect($scope.currStaffType);
                    } else {
                        $scope.staffdropdown = false;                   
                    $('#fifthgraphModal').modal({ backdrop: 'static', keyboard: false });
                    $http.get(ENV.apiUrl + "api/analysisreport/getStaffDetailsData").then(function (res) {
                        console.log(res.data);
                        $scope.staffData=res.data;            
                        $scope.legends=[];
                        $scope.labels=[];
                        if (window.bar12 != undefined) {
                            window.bar12.destroy();
                        }
                        var ctx = document.getElementById("staffDetailChart2").getContext("2d");
                        ctx.canvas.parentNode.style.height = '400px';
                        angular.forEach($scope.staffData,function(value,key){
                            angular.forEach(value,function(value1,key1){
                                if(key==0){
                                    if(key1!='nationality'){                    
                                        $scope.legends.push(key1);                        
                                    }
                                }
                                if(key1=='nationality'){
                                    $scope.labels.push(value1);    
                                }
                            })
                        })
                        $scope.dataset=[];
                        console.log($scope.legends)
                        angular.forEach($scope.legends,function(value,key){
                            var color = Math.floor(Math.random() * 16777216).toString(16);
                            var backcolor = '#000000'.slice(0, -color.length) + color;
                            $scope.dataset.push({label:value,backgroundColor:backcolor,data: []})
                        });

                        angular.forEach($scope.staffData,function(value,key){
                            angular.forEach(value,function(value1,key1){                  
                                angular.forEach($scope.legends,function(value2,key2){                    
                                    if(key1!='nationality'){                    
                                        if(key1==value2){
                                            $scope.dataset[key2]['data'].push(value1);
                                        }                     
                                    }
                                });  
                            })
                        })

                        var modaldata = {
                            labels: $scope.labels,
                            datasets: $scope.dataset
                        };

                
                        setTimeout(function() {
                            window.bar12 = new Chart(ctx, {
                                type: 'bar',
                                data: modaldata,
                                options: {
                                    responsive: true,
                                    maintainAspectRatio: false,
                                    legend: {
                                        display: true,
                                        labels: {
                                            boxWidth:10,
                                            fontSize:10
                                        }
                                    },
                                    title: {
                                        display: true,
                                        text: 'Staff Details',
                                        position:'bottom'
                                    }
                                }
                            });

                        }, 300);
                    })
                    }
                }

                $scope.staffSelect = function (stafftype) {
                    $scope.currStaffType = stafftype;
                    $http.get(ENV.apiUrl + "api/analysisreport/getNewStaffDetailsData?sopr=" + $scope.currStaffType).then(function (res) {
                        console.log(res.data);
                        $scope.staffData = res.data;
                        $scope.legends = [];
                        $scope.labels = [];
                        if (window.bar12 != undefined) {
                            window.bar12.destroy();
                        }
                        var ctx = document.getElementById("staffDetailChart2").getContext("2d");

                        angular.forEach($scope.staffData, function (value, key) {
                            angular.forEach(value, function (value1, key1) {
                                if (key == 0) {
                                    if (key1 != 'nationality') {
                                        $scope.legends.push(key1);
                                    }
                                }
                                if (key1 == 'nationality') {
                                    $scope.labels.push(value1);
                                }
                            })
                        })
                        $scope.dataset = [];
                        console.log($scope.legends)
                        angular.forEach($scope.legends, function (value, key) {
                            var color = Math.floor(Math.random() * 16777216).toString(16);
                            var backcolor = '#000000'.slice(0, -color.length) + color;
                            $scope.dataset.push({ label: value, backgroundColor: backcolor, data: [] })
                        });

                        angular.forEach($scope.staffData, function (value, key) {
                            angular.forEach(value, function (value1, key1) {
                                angular.forEach($scope.legends, function (value2, key2) {
                                    if (key1 != 'nationality') {
                                        if (key1 == value2) {
                                            $scope.dataset[key2]['data'].push(value1);
                                        }
                                    }
                                });
                            })
                        })

                        var modaldata = {
                            labels: $scope.labels,
                            datasets: $scope.dataset
                        };


                        setTimeout(function () {
                            window.bar12 = new Chart(ctx, {
                                type: 'bar',
                                data: modaldata,
                                options: {
                                    legend: {
                                        display: true,
                                        labels: {
                                            boxWidth: 10,
                                            fontSize: 10
                                        }
                                    },
                                    title: {
                                        display: true,
                                        text: 'Staff Details',
                                        position: 'bottom'
                                    }
                                }
                            });

                        }, 300);
                    })
                }

                $http.get(ENV.apiUrl + "api/analysisreport/getStaffDetailsData").then(function (res) {
                    console.log(res.data);
                    $scope.staffData=res.data;            
                    $scope.legends=[];
                    $scope.labels=[];
                    if (window.bar11 != undefined) {
                        window.bar11.destroy();
                    }
                    var ctxst = document.getElementById("staffDetailChart").getContext("2d");
                 //   ctxst.canvas.parentNode.style.width = '528px';
                    ctxst.canvas.parentNode.style.height = '400px';

                    angular.forEach($scope.staffData,function(value,key){
                        angular.forEach(value,function(value1,key1){
                            if(key==0){
                                if(key1!='nationality'){                    
                                    $scope.legends.push(key1);                        
                                }
                            }
                            if(key1=='nationality'){
                                $scope.labels.push(value1);    
                            }
                        })
                    })
                    $scope.dataset=[];
                    console.log($scope.legends)
                    angular.forEach($scope.legends,function(value,key){
                        var color = Math.floor(Math.random() * 16777216).toString(16);
                        var backcolor = '#000000'.slice(0, -color.length) + color;
                        $scope.dataset.push({label:value,backgroundColor:backcolor,data: []})
                    });

                    angular.forEach($scope.staffData,function(value,key){
                        angular.forEach(value,function(value1,key1){                  
                            angular.forEach($scope.legends,function(value2,key2){                    
                                if(key1!='nationality'){                                                
                                    if(key1==value2){
                                        $scope.dataset[key2]['data'].push(value1);
                                    }                     
                                }
                            });  
                        })
                    })


                    var modaldata = {
                        labels: $scope.labels,
                        datasets: $scope.dataset
                    };

                    
                    setTimeout(function() {
                        window.bar11 = new Chart(ctxst, {
                            type: 'bar',
                            data: modaldata,
                            options: {
                                responsive: true,
                                maintainAspectRatio: false,
                                legend: {
                                    display: true,
                                    labels: {
                                        boxWidth:10,
                                        fontSize:10
                                    }
                                },
                                title: {
                                    display: true,
                                    text: 'Staff Details',
                                    position:'bottom'
                                }
                            }
                        });

                    }, 300);

                })
                  

               ////////// end ////////////////

               // 6. DETAILS OF DISCOUNT AVAILED BY STUDENTS  /////////


               $scope.showavailStudChartModal=function(){
                   $('#sixthgraphModal').modal({backdrop: 'static', keyboard: false});
                   $scope.stud_concession_description_lst=[];
                   $scope.stud_discount_value_lst=[];
                   $scope.stud_cnt_lst=[];
                   $scope.dataset2 = [];
                   if (window.bar14 != undefined) {
                       window.bar14.destroy();
                   }
                   $http.get(ENV.apiUrl + "api/analysisreport/getStudentDiscount?curCode="+$scope.edt['cur_code_avail']+"&acaYear="+$scope.edt['academic_year_avail']).then(function (res) {     
                       $scope.stud_discount=res.data;
                       for (var i = 0; res.data.length > i; i++) {
                           if (res.data[i].discount != '' || res.data[i].student != '') {
                               if (res.data[i].concession != 'Total') {
                                   var color = Math.floor(Math.random() * 16777216).toString(16);
                                   var backcolor = '#000000'.slice(0, -color.length) + color;
                                   $scope.dataset2.push({
                                       label: res.data[i].concession, fill: false,
                                       backgroundColor: backcolor, borderColor: backcolor, data: []
                                   })
                                   $scope.stud_cnt_lst = res.data[i].discount + ',' + res.data[i].student;
                                   var b = $scope.stud_cnt_lst.split(',').map(function (item) {
                                       return parseInt(item, 10);
                                   });

                                   $scope.dataset2[i]['data'] = b;
                               }
                           } else {
                               $scope.errmsgg = "Data Not Available"
                           }
                       }


                       console.log($scope.dataset2);
                       var ctx = document.getElementById("availStudChart2").getContext("2d");
                       var data = {
                           labels: ["Discount(%)","Students(count)"],
                           datasets: $scope.dataset2
                       };

                       window.bar14 = new Chart(ctx, {
                           type:'line',
                           data: data,
                           options: {
                               legend: {
                                   display: true,
                                   labels: {
                                       boxWidth:10,
                                       fontSize:10
                                   }
                               },
                               title: {
                                   display: true,
                                   text: 'Details of discount availed by Students',
                                   position:'bottom'
                               },
                           }
                       });

                   });
               }
               $scope.getFourthAcademicYear = function (curCode) {
                   $http.get(ENV.apiUrl + "api/analysisreport/getAcademicYear?cur_code=" + curCode).then(function (res) {
                       $scope.fourthacademicYears = res.data;
                       $scope.edt['academic_year_avail'] = $scope.fourthacademicYears[0].academic_year;
                       $scope.getDataDiscountStud($scope.edt['cur_code_avail'], $scope.edt['academic_year_avail'])
                   });
               }

               $scope.getDataDiscountStud = function (cur, year) {
                   $scope.stud_concession_description_lst=[];
                   $scope.stud_discount_value_lst=[];
                   $scope.stud_cnt_lst=[];
                   $scope.dataset2 = [];
                   if (window.bar13 != undefined) {
                       window.bar13.destroy();
                   }
                   $http.get(ENV.apiUrl + "api/analysisreport/getStudentDiscount?curCode="+cur+"&acaYear="+year).then(function (res) {     
                       $scope.stud_discount=res.data;                      

                       for (var i = 0; res.data.length > i; i++) {
                           if (res.data[i].discount != '' || res.data[i].student != '') {
                               if (res.data[i].concession != 'Total') {
                                   var color = Math.floor(Math.random() * 16777216).toString(16);
                                   var backcolor = '#000000'.slice(0, -color.length) + color;
                                   $scope.dataset2.push({
                                       label: res.data[i].concession, fill: false,
                                       backgroundColor: backcolor, borderColor: backcolor, data: []
                                   })
                                   $scope.stud_cnt_lst = res.data[i].discount + ',' + res.data[i].student;
                                   var b = $scope.stud_cnt_lst.split(',').map(function (item) {
                                       return parseInt(item, 10);
                                   });
                                   $scope.dataset2[i]['data'] = b;
                               }
                           } else {
                               $scope.errmsgg = "Data Not Available"
                           }
                       }


                       console.log($scope.dataset2);
                       var ctxav = document.getElementById("availStudChart").getContext("2d");
                       ctxav.canvas.parentNode.style.height = '128px';
                       //                      ctxav.canvas.parentNode.style.height = '50px';

                       var data = {
                           labels: ["Discount(%)","Students(count)"],
                           datasets: $scope.dataset2
                       };


                       window.bar13 = new Chart(ctxav, {
                           type:'line',
                           data: data,
                           options: {
                               legend: {
                                   display: true,
                                   labels: {
                                       boxWidth:10,
                                       fontSize:10
                                   }
                               },
                               title: {
                                   display: true,
                                   text: 'Details of discount availed by Students',
                                   position:'bottom'
                               },
                           }
                       });

                   });
               }                          



               /////////// end //////////////// 

               // 7. DETAILS OF MOVEMENT DURING THE MONTH

               $scope.moveData=[]

               $scope.showmoveofMonthChartModal=function(){
                   $('#seventhgraphModal').modal({backdrop: 'static', keyboard: false});
                   $scope.moveData=[]
                   $http.get(ENV.apiUrl + "api/analysisreport/getMovementOfMonth").then(function (res) {
                       console.log(res.data);
                       $scope.movementData=res.data;
                       var v=0;
                       var l= 0;

                       for(var i=0;res.data.length>i;i++){
                           if(res.data[i].staff_status=='V'){
                               v++;                     
                           }else if(res.data[i].staff_status=='L'){
                               l++;
                           }
                       }
                       $scope.moveData.push(v)     
                       $scope.moveData.push(l);                                                    
                       if (window.bar16 != undefined) {
                           window.bar16.destroy();
                       }
                       var ctx = document.getElementById("moveofMonthChart2").getContext("2d");
                       var data = {
                           labels: ['Joined','Resigned'],
                           datasets:[{  
                               data: $scope.moveData,
                               backgroundColor: ['#5AD3D1','#FF5A5E']
                           }]
                       }

                       window.bar16 = new Chart(ctx, {
                           type: 'pie',
                           data: data,
                           options: {
                               legend: {
                                   display: true,
                                   labels: {
                                       fontSize:10
                                   }
                               },
                               title: {
                                   display: true,
                                   text: 'Details Of Movement During The Year',
                                   position:'bottom'
                               }
                           }
                       });  
                   })
               }

               $http.get(ENV.apiUrl + "api/analysisreport/getMovementOfMonth").then(function (res) {
                   console.log(res.data);

                   var v=0;
                   var l= 0;
                   if(res.data.length>0){   
                       for(var i=0;res.data.length>i;i++){
                           if(res.data[i].staff_status=='V'){
                               v++;                     
                           }else if(res.data[i].staff_status=='L'){
                               l++;
                           }
                       }
                       $scope.moveData.push(v)     
                       $scope.moveData.push(l);                                                    
                       if (window.bar15 != undefined) {
                           window.bar15.destroy();
                       }
                       var ctxmo = document.getElementById("moveofMonthChart").getContext("2d");
                       //ctxmo.canvas.parentNode.style.width = '128px';

                       var data = {
                           labels: ['Joined','Resigned'],
                           datasets:[{  
                               data: $scope.moveData,
                               backgroundColor: ['#5AD3D1','#FF5A5E']
                           }]
                       }
                       window.bar15 = new Chart(ctxmo, {
                           type: 'pie',
                           data: data,
                           options: {
                               legend: {
                                   display: false,
                                   labels: {
                                       fontSize:10
                                   }
                               },
                               title: {
                                   display: true,
                                   text: 'Details Of Movement During The Year',
                                   position:'bottom'
                               }
                           }
                       });  
                   }else{
                       $scope.ermesg="Data Not Available"
                   }
               })

               ////////////////end ///////////

               /////  Export Excel ///////

               $scope.exportTable = function (tablename,id) {
                   var blob = new Blob([document.getElementById(id).innerHTML], {
                       type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                   });
                   saveAs(blob,tablename + ".xls");
               }
               /// end //

                $scope.printGraph = function (curgraph) {
                    var options = {
                    };
                    var pdf = new jsPDF('p', 'pt', 'a4');
                    pdf.setTextColor(100)
//                    pdf.setFontSize(30)
//                    pdf.addImage(img, 'JPEG', 15, 40, 50, 50)
                    pdf.text(20, 20, $scope.user_details['lic_school_name']);
                    pdf.addHTML($("#" + curgraph), 30, 30, options, function () {
                        pdf.save( curgraph+'.pdf');
                    });
//                    addPage()
                 //   pdf.autoPrint();
                }
                
    }]);

    simsController.filter('titleCase', function () {
            return function (input) {
            input = input || '';
            return input.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
        };
    });

})();