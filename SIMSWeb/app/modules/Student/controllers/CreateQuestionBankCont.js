﻿(function () {
    'use strict';

    var main, temp, del = [];
    var grade = "", section = "", desgination = "", departt = "", user_grp = "";

    var simsController = angular.module('sims.module.Student');
    simsController.controller('CreateQuestionBankCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.div_staff = false;
            $scope.div_parent = false;
            var data1 = [];
            $scope.surveyDetails = [];
            var deletecode = [];
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.edit_code = false;
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            $scope.user_group_code = [], $scope.department_code = [], $scope.grade_code = [], $scope.section_code = [];

            var surValues = [], deptValues = [], gradeValues = [], sectionValues = [];

            $(function () {
                $('#cmb_grade').multipleSelect({
                    width: '100%',
                    placeholder: "Select Grade"
                });

                $('#cmb_section').multipleSelect({
                    width: '100%',
                    placeholder: "Select Section"
                });

                $('#cmb_dept').multipleSelect({
                    width: '100%',
                    placeholder: "Select Department"
                });

                $('#cmb_group').multipleSelect({
                    width: '100%',
                    placeholder: "Select Group"
                });


            });

            $http.get(ENV.apiUrl + "api/ERP/SurveyDetails/getSurveyStatus").then(function (status_res) {
                $scope.status_data = status_res.data;
            });

            $scope.edit1 = function (str) {
                str.status = true;
            }

            $scope.getSurveyStatusChange = function (survey_code, survey_status) {

            }

            $scope.update1 = function (str) {
                debugger
                //$scope.getSurveyStatusChange(str.sims_survey_code, str.sims_survey_status);

                $http.post(ENV.apiUrl + "api/ERP/SurveyDetails/updateQuestionBankStatus?survey_code=" + str.sims_questionbank_code + "&sims_survey_status=" + str.sims_survey_status).then(function (res) {
                    $scope.msg1 = res.data;
                    $scope.getgrid();
                });
                str.status = false;
            }

            $scope.cancel1 = function (str) {
                str.status = false;
            }

            $scope.New = function () {
                $scope.edt = "";
                $scope.edit_code = false;
                $scope.dis1 = false;
                $scope.dis2 = true;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = {};
                var dt = new Date();
                $scope.edt.sims_questionbank_start_date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                $scope.edt.sims_questionbank_end_date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();

                $http.get(ENV.apiUrl + "api/ERP/SurveyDetails/getAcademicYear").then(function (acadYr_res) {
                    $scope.acadYr_data = acadYr_res.data;
                });

                $http.get(ENV.apiUrl + "api/ERP/SurveyDetails/getGender").then(function (gender_res) {
                    $scope.gender_data = gender_res.data;
                });

                $http.get(ENV.apiUrl + "api/ERP/SurveyDetails/getSurveyType").then(function (surveyType_res) {
                    $scope.surveyType_data = surveyType_res.data;

                });

                $http.get(ENV.apiUrl + "api/ERP/SurveyDetails/getSurveyGroupType").then(function (usergrp_res) {
                    $scope.group_data = usergrp_res.data;
                    setTimeout(function () {
                        $('#cmb_group').change(function () {
                            user_grp = $(this).val();
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });

                $http.get(ENV.apiUrl + "api/common/getDepartmentName").then(function (res) {
                    $scope.department = res.data;
                    setTimeout(function () {
                        $('#cmb_dept').change(function () {
                            departt = $(this).val();
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });

                $scope.getGrade = function (acadYR) {
                    if (acadYR != "" || acadYR != undefined) {
                        // if (gradeValues.length < 0) 
                        {
                            $http.get(ENV.apiUrl + "api/ERP/SurveyDetails/Get_Grade_CodebyCuriculum?acad_yr=" + acadYR).then(function (res) {
                                $scope.grade = res.data;
                                setTimeout(function () {
                                    $('#cmb_grade').change(function () {
                                        grade = $(this).val();
                                    }).multipleSelect({
                                        width: '100%',
                                    });
                                }, 1000);
                            });
                        }
                    }

                }

                $scope.getSection = function (acadYR, grade) {
                    if (acadYR != "" && grade != "") {
                        //  if (sectionValues.length < 0)
                        {
                            $http.get(ENV.apiUrl + "api/ERP/SurveyDetails/Get_Section_CodebyCuriculum?acad_yr=" + acadYR + "&grade_code=" + grade).then(function (res) {
                                $scope.section = res.data;
                                setTimeout(function () {
                                    $('#cmb_section').change(function () {
                                        section = $(this).val();
                                    }).multipleSelect({
                                        multiple: true,
                                        width: '100%'
                                    });
                                    // $("#cmb_section").multipleSelect("checkAll");
                                }, 1000);
                            });
                        }
                    }
                }
            }



            $scope.getGroupdetails = function (group_code) {
                for (var i = 0; i < group_code.length; i++) {
                    if (group_code[i] == "03") {
                        $scope.div_staff = true;
                    }
                    if (group_code[i] == "04" || group_code[i] == "05") {
                        $scope.div_parent = true;
                    }
                    if (group_code[i] == "A") {
                        $scope.div_staff = false;
                        $scope.div_parent = false;
                    }
                }
            }

            $scope.Save = function (isvalidate) {

                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        if ($scope.edt['sims_chkgender'] == true) {
                            if ($scope.edt.sims_survey_gender_code == "") {
                                swal({ title: "Alert", text: "Please select Gender.", showCloseButton: true, width: 380, });
                                return;
                            }
                        }
                        var dept_code, grp_code, section_code, grade_code;

                        var group_code = $scope.edt.sims_survey_user_group_code;
                        grp_code = grp_code + ',' + group_code;
                        var groupCode = grp_code.substr(grp_code.indexOf(',') + 1);

                        var dept = $scope.edt.sims_survey_dept_no;
                        dept_code = dept_code + ',' + dept;
                        var department = dept_code.substr(dept_code.indexOf(',') + 1);

                        var grade = $scope.edt.sims_survey_grade_code;
                        grade_code = grade_code + ',' + grade;
                        var gradeCode = grade_code.substr(grade_code.indexOf(',') + 1);

                        var sec = $scope.edt.sims_survey_section_code;
                        section_code = section_code + ',' + sec;
                        var sectionCode = section_code.substr(section_code.indexOf(',') + 1);

                        var data = ({
                            sims_questionbank_type: $scope.edt.sims_questionbank_type,
                            sims_questionbank_subject: $scope.edt.sims_questionbank_subject,
                            sims_questionbank_subject_ot: $scope.edt.sims_questionbank_subject_ot,
                            sims_questionbank_desc_en: $scope.edt.sims_questionbank_desc_en,
                            sims_questionbank_desc_ot: $scope.edt.sims_questionbank_desc_ot,
                            sims_questionbank_start_date: $scope.edt.sims_questionbank_start_date,
                            sims_questionbank_end_date: $scope.edt.sims_questionbank_end_date,

                            sims_questionbank_created_by_user: $rootScope.globals.currentUser.username,
                            sims_questionbank_no_of_question_available: $scope.edt.sims_questionbank_no_of_question_available,
                            sims_questionbank_no_of_question_to_attempt: $scope.edt.sims_questionbank_no_of_question_to_attempt,
                            sims_questionbank_max_allowed_time_in_minute: $scope.edt.sims_questionbank_max_allowed_time_in_minute,
                            sims_questionbank_max_attempt_count: $scope.edt.sims_questionbank_max_attempt_count,
                            sims_questionbank_end_of_questionbank_rating_flag: $scope.edt.sims_questionbank_end_of_questionbank_rating_flag,

                            sims_questionbank_end_of_questionbank_suggestion_flag: $scope.edt.sims_questionbank_end_of_questionbank_suggestion_flag,
                            sims_questionbank_end_of_questionbank_remark_flag: $scope.edt.sims_questionbank_end_of_questionbank_remark_flag,
                            sims_questionbank_multiple_attempt_flag: $scope.edt.sims_questionbank_multiple_attempt_flag,
                            sims_questionbank_erp_flag: $scope.edt.sims_questionbank_erp_flag,
                            sims_questionbank_ppn_flag: $scope.edt.sims_questionbank_ppn_flag,
                            sims_questionbank_annonimity_flag: $scope.edt.sims_questionbank_annonimity_flag,
                            sims_survey_status: $scope.edt.sims_survey_status,

                            sims_survey_user_group_code: groupCode,
                            sims_survey_dept_no: department,
                            sims_survey_acad_year: $scope.edt.sims_survey_acad_year,
                            sims_survey_grade_code: gradeCode,
                            sims_survey_section_code: sectionCode,
                            sims_survey_gender_code: $scope.edt.sims_survey_gender_code,
                            opr: 'I'
                        });

                        data1.push(data);
                        console.log(data1);

                        $http.post(ENV.apiUrl + "api/ERP/SurveyDetails/CUDInsertBankDetails", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            $rootScope.strMessage = $scope.msg1.strMessage;
                            if ($scope.msg1 == true) {
                                swal({ text: "Survey Created Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal({ text: "Survey Already Exists", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }

            }

            ///old code

            $timeout(function () {
                $("#example_wrapper").scrollbar();
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.countData = [

                  { val: 5, data: 5 },
                  { val: 10, data: 10 },
                  { val: 15, data: 15 },

            ]

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $http.get(ENV.apiUrl + "api/ERP/SurveyDetails/getQuestionBankDetails").then(function (res) {
                debugger
                $scope.dis1 = true;
                $scope.dis2 = false;
                $scope.surveyDetails = res.data;
                if ($scope.surveyDetails.length > 0) {
                    $scope.table = true;
                    $scope.pager = true;
                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.surveyDetails.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.surveyDetails.length, data: 'All' })
                    }
                    $scope.totalItems = $scope.surveyDetails.length;
                    $scope.todos = $scope.surveyDetails;
                    $scope.makeTodos();
                    $scope.grid = true;
                }


            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };





            $scope.edit = function (str) {
                debugger
                $scope.New();
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_code = true;

                $scope.edt =
                    {
                        sims_survey_srl_no: str.sims_survey_srl_no,
                        sims_questionbank_code: str.sims_questionbank_code,
                        sims_questionbank_type: str.sims_questionbank_type,
                        sims_questionbank_subject: str.sims_questionbank_subject,
                        sims_questionbank_subject_ot: str.sims_questionbank_subject_ot,
                        sims_questionbank_desc_en: str.sims_questionbank_desc_en,
                        sims_questionbank_desc_ot: str.sims_questionbank_desc_ot,
                        sims_questionbank_start_date: str.sims_questionbank_start_date,
                        sims_questionbank_end_date: str.sims_questionbank_end_date,
                        sims_questionbank_annonimity_flag: str.sims_questionbank_annonimity_flag,
                        sims_questionbank_ppn_flag: str.sims_questionbank_ppn_flag,
                        sims_questionbank_erp_flag: str.sims_questionbank_erp_flag,
                        sims_questionbank_multiple_attempt_flag: str.sims_questionbank_multiple_attempt_flag,
                        sims_questionbank_end_of_questionbank_remark_flag: str.sims_questionbank_end_of_questionbank_remark_flag,
                        sims_questionbank_end_of_questionbank_suggestion_flag: str.sims_questionbank_end_of_questionbank_suggestion_flag,
                        sims_questionbank_end_of_questionbank_rating_flag: str.sims_questionbank_end_of_questionbank_rating_flag,
                        sims_questionbank_max_attempt_count: str.sims_questionbank_max_attempt_count,
                        sims_questionbank_max_allowed_time_in_minute: str.sims_questionbank_max_allowed_time_in_minute,
                        sims_questionbank_no_of_question_to_attempt: str.sims_questionbank_no_of_question_to_attempt,
                        sims_questionbank_no_of_question_available: str.sims_questionbank_no_of_question_available,
                        sims_questionbank_created_by_user: str.sims_questionbank_created_by_user,
                        sims_survey_creation_time: str.sims_survey_creation_time,
                        sims_survey_status: str.sims_survey_status,
                        sims_survey_gender_code: str.sims_survey_gender_code,


                        //survey details
                        sims_survey_user_group_code: str.sims_survey_user_group_code,
                        sims_survey_dept_no: str.sims_survey_dept_no,
                        sims_survey_grade_code: str.sims_survey_grade_code,
                        sims_survey_section_code: str.sims_survey_section_code,
                        sims_survey_acad_year: str.sims_survey_acad_year,
                        sims_survey_gradesection_code: str.sims_survey_gradesection_code
                    }

                $scope.user_group_code = str.sims_survey_user_group_code.split(',');

                $scope.department_code = str.sims_survey_dept_no.split(',');

                $scope.grade_code = str.sims_survey_grade_code.split(',');

                $scope.section_code = str.sims_survey_gradesection_code.split(',');


                for (var i = 0; i < $scope.user_group_code.length; i++) {
                    surValues.push($scope.user_group_code[i]);
                }

                for (var i = 0; i < $scope.department_code.length; i++) {
                    deptValues.push($scope.department_code[i]);
                }

                for (var i = 0; i < $scope.grade_code.length; i++) {
                    gradeValues.push($scope.grade_code[i]);
                }

                for (var i = 0; i < $scope.section_code.length; i++) {
                    sectionValues.push($scope.section_code[i]);
                }


                $http.get(ENV.apiUrl + "api/ERP/SurveyDetails/getSurveyGroupType").then(function (usergrp_res) {
                    $scope.group_data = usergrp_res.data;
                    setTimeout(function () {
                        $('#cmb_group').change(function () {
                            user_grp = $(this).val();
                        }).multipleSelect({
                            width: '100%'
                        });

                        $("#cmb_group").multipleSelect("setSelects", surValues);
                    }, 1000);

                });

                $http.get(ENV.apiUrl + "api/common/getDepartmentName").then(function (res) {
                    $scope.department = res.data;
                    setTimeout(function () {
                        $('#cmb_dept').change(function () {
                            departt = $(this).val();
                        }).multipleSelect({
                            width: '100%'
                        });

                        $("#cmb_dept").multipleSelect("setSelects", deptValues);

                    }, 1000);
                });

                $http.get(ENV.apiUrl + "api/ERP/SurveyDetails/Get_Grade_CodebyCuriculum?acad_yr=" + str.sims_survey_acad_year).then(function (res) {
                    $scope.grade = res.data;
                    setTimeout(function () {
                        $('#cmb_grade').change(function () {
                            grade = $(this).val();
                        }).multipleSelect({
                            width: '100%',
                        });

                        $("#cmb_grade").multipleSelect("setSelects", gradeValues);
                    }, 1000);
                });

                $http.get(ENV.apiUrl + "api/ERP/SurveyDetails/Get_Section_CodebyCuriculum?acad_yr=" + str.sims_survey_acad_year + "&grade_code=" + gradeValues).then(function (res) {
                    $scope.section = res.data;
                    console.log($scope.section);
                    setTimeout(function () {
                        $('#cmb_section').change(function () {
                            section = $(this).val();
                        }).multipleSelect({
                            multiple: true,
                            width: '100%'
                        });
                        $("#cmb_section").multipleSelect("setSelects", sectionValues);
                    }, 1000);
                });

            }


            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/ERP/SurveyDetails/getQuestionBankDetails").then(function (res) {
                    $scope.dis1 = true;
                    $scope.dis2 = false;
                    $scope.surveyDetails = res.data;
                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.surveyDetails.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.surveyDetails.length, data: 'All' })
                    }
                    $scope.totalItems = $scope.surveyDetails.length;
                    $scope.todos = $scope.surveyDetails;
                    $scope.makeTodos();
                    $scope.grid = true;
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function () {
                var data1 = [];

                    var dept_code, grp_code, section_code, grade_code;

                    var group_code = $scope.edt.sims_survey_user_group_code;
                    grp_code = grp_code + ',' + group_code;
                    var groupCode = grp_code.substr(grp_code.indexOf(',') + 1);

                    var dept = $scope.edt.sims_survey_dept_no;
                    dept_code = dept_code + ',' + dept;
                    var department = dept_code.substr(dept_code.indexOf(',') + 1);

                    var grade = $scope.edt.sims_survey_grade_code;
                    grade_code = grade_code + ',' + grade;
                    var gradeCode = grade_code.substr(grade_code.indexOf(',') + 1);

                    var sec = $scope.edt.sims_survey_section_code;
                    section_code = section_code + ',' + sec;
                    var sectionCode = section_code.substr(section_code.indexOf(',') + 1);

                    var data = ({
                        sims_survey_srl_no: $scope.edt.sims_survey_srl_no,
                        sims_questionbank_code: $scope.edt.sims_questionbank_code,
                        sims_questionbank_type: $scope.edt.sims_questionbank_type,
                        sims_questionbank_subject: $scope.edt.sims_questionbank_subject,
                        sims_questionbank_subject_ot: $scope.edt.sims_questionbank_subject_ot,
                        sims_questionbank_desc_en: $scope.edt.sims_questionbank_desc_en,
                        sims_questionbank_desc_ot: $scope.edt.sims_questionbank_desc_ot,
                        sims_questionbank_start_date: $scope.edt.sims_questionbank_start_date,
                        sims_questionbank_end_date: $scope.edt.sims_questionbank_end_date,

                        sims_questionbank_created_by_user: $rootScope.globals.currentUser.username,
                        sims_questionbank_no_of_question_available: $scope.edt.sims_questionbank_no_of_question_available,
                        sims_questionbank_no_of_question_to_attempt: $scope.edt.sims_questionbank_no_of_question_to_attempt,
                        sims_questionbank_max_allowed_time_in_minute: $scope.edt.sims_questionbank_max_allowed_time_in_minute,
                        sims_questionbank_max_attempt_count: $scope.edt.sims_questionbank_max_attempt_count,
                        sims_questionbank_end_of_questionbank_rating_flag: $scope.edt.sims_questionbank_end_of_questionbank_rating_flag,

                        sims_questionbank_end_of_questionbank_suggestion_flag: $scope.edt.sims_questionbank_end_of_questionbank_suggestion_flag,
                        sims_questionbank_end_of_questionbank_remark_flag: $scope.edt.sims_questionbank_end_of_questionbank_remark_flag,
                        sims_questionbank_multiple_attempt_flag: $scope.edt.sims_questionbank_multiple_attempt_flag,
                        sims_questionbank_erp_flag: $scope.edt.sims_questionbank_erp_flag,
                        sims_questionbank_ppn_flag: $scope.edt.sims_questionbank_ppn_flag,
                        sims_questionbank_annonimity_flag: $scope.edt.sims_questionbank_annonimity_flag,
                        sims_survey_status: $scope.edt.sims_survey_status,

                        sims_survey_user_group_code: groupCode,
                        sims_survey_dept_no: department,
                        sims_survey_acad_year: $scope.edt.sims_survey_acad_year,
                        sims_survey_grade_code: gradeCode,
                        sims_survey_section_code: sectionCode,
                        sims_survey_gender_code: $scope.edt.sims_survey_gender_code,
                        opr: 'U'
                    });

                    data1.push(data);
                    console.log(data1);

                    $http.post(ENV.apiUrl + "api/ERP/SurveyDetails/CUDQuestionBankDetailsUpdate", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        $rootScope.strMessage = $scope.msg1.strMessage;
                        if ($scope.msg1 == true) {
                            swal({ text: "Survey Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal({ text: "Survey Not Updated.", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                    });

                }
        

            $scope.cancel = function () {

                $scope.div_staff = false;
                $scope.div_parent = false;
                $scope.dis1 = true;
                $scope.dis2 = false;
                $scope.edt = "";
                try {
                    $('#cmb_group').multipleSelect('uncheckAll');
                } catch (e) { }

                try {
                    $('#cmb_grade').multipleSelect('uncheckAll');
                } catch (e) { }

                try {
                    $('#cmb_section').multipleSelect('uncheckAll');
                } catch (e) { }

                try {
                    $('#cmb_dept').multipleSelect('uncheckAll');
                } catch (e) { }

                $scope.user_group_code = []; $scope.department_code = []; $scope.grade_code = []; $scope.section_code = [];

                surValues = [];
                deptValues = [];
                gradeValues = [];
                sectionValues = [];
                $scope.getGroupdetails($scope.edt['sims_survey_user_group_code']);

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test_" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test_" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function (info) {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }

            $scope.size = function (str) {
                //if (str == 5 || str == 10 || str == 15) {
                //    $scope.pager = true;
                //}
                //else {
                //    $scope.pager = false;
                //}
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.surveyDetails;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.surveyDetails, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.surveyDetails;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_survey_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.survey_type_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_questionbank_subject.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }]);
})();

