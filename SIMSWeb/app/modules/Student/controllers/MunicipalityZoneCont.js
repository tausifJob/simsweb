﻿(function () {

    var simsController = angular.module('sims.module.Student');

    simsController.controller('MunicipalityZoneCont', ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

        $scope.display = false;
        $scope.table = true;


        $scope.pagesize = '10';
        $scope.pageindex = "0";
        $scope.pager = true;
        $timeout(function () {
            $("#fixTable").tableHeadFixer({ 'top': 1 });
        }, 100);
        $http.get(ENV.apiUrl + "api/MunicipalityZone/GetMunicipalityName").then(function (res) {
            debugger;
            $scope.name = res.data;
        });

        $http.get(ENV.apiUrl + "api/MunicipalityZone/getMunicipalityZoneDetails").then(function (res1) {

            $scope.CreDiv = res1.data;
            $scope.totalItems = $scope.CreDiv.length;
            $scope.todos = $scope.CreDiv;
            $scope.makeTodos();

        });

        $scope.size = function (str) {
            //console.log(str);
            //$scope.pagesize = str;
            //$scope.currentPage = 1;
            //$scope.numPerPage = str;
            debugger;
            if (str == "All") {
                $scope.currentPage = '1';
                $scope.filteredTodos = $scope.CreDiv;
                $scope.pager = false;
            }
            else {
                $scope.pager = true;
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }
            console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
        }

        $scope.index = function (str) {
            $scope.pageindex = str;
            $scope.currentPage = str;
            console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            main.checked = false;
            $scope.CheckAllChecked();
        }

        $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

        $scope.makeTodos = function () {
            var rem = parseInt($scope.totalItems % $scope.numPerPage);
            if (rem == '0') {
                $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
            }
            else {
                $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
            }

            var begin = (($scope.currentPage - 1) * $scope.numPerPage);
            var end = parseInt(begin) + parseInt($scope.numPerPage);

            console.log("begin=" + begin); console.log("end=" + end);

            $scope.filteredTodos = $scope.todos.slice(begin, end);
        };

        $scope.searched = function (valLists, toSearch) {
            return _.filter(valLists,

            function (i) {
                /* Search Text in all  fields */
                return searchUtil(i, toSearch);
            });
        };
        $scope.search = function () {
            debugger;
            $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
            $scope.totalItems = $scope.todos.length;
            $scope.currentPage = '1';
            if ($scope.searchText == '') {
                $scope.todos = $scope.CreDiv;
            }
            $scope.makeTodos();
            main.checked = false;
            $scope.CheckAllChecked()

        }

        function searchUtil(item, toSearch) {
            /* Search Text in all 3 fields */
            return (item.english_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.arabic_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.acno == toSearch) ? true : false;
        }

        //  new
        $scope.New = function () {
            var datasend = [];
            $scope.int = "";
            $scope.table = false;
            $scope.display = true;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.move = true;
            $scope.temp = "";

            $scope.temp = {};
            $scope.temp['municipalityZone_status'] = true;
            $scope.temp["english_name"] = "";
            $scope.temp["arabic_name"] = "";
            $scope.temp["other_name"] = "";


            $scope.Myform1.$setPristine();
            $scope.Myform1.$setUntouched();
        }

        //cancle
        $scope.Cancel = function () {
            $scope.temp = "";
            $scope.table = true;
            $scope.display = false;
            $scope.int = " ";
        }

        //DATA Save

        var datasend = [];
        $scope.savedata = function (MyForm1) {
            debugger;
            if (MyForm1) {
                var data = $scope.temp;
                data.opr = 'I';
                datasend.push(data);
                $http.post(ENV.apiUrl + "api/MunicipalityZone/CUDMunicipalityZoneDetails", datasend).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                    }
                    else {
                        swal({ text: "Record Not Inserted " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                    }

                    $http.get(ENV.apiUrl + "api/MunicipalityZone/getMunicipalityZoneDetails").then(function (res1) {
                        $scope.CreDiv = res1.data;
                        $scope.totalItems = $scope.CreDiv.length;
                        $scope.todos = $scope.CreDiv;
                        $scope.makeTodos();
                    });
                });
                datasend = [];
                $scope.table = true;
                $scope.display = false;
            }
        }

        //update
        var dataforUpdate = [];
        $scope.update = function (MyForm1) {
            debugger;
            if (MyForm1) {
                var data = $scope.temp;
                data.opr = 'U';
                dataforUpdate.push(data);
                $http.post(ENV.apiUrl + "api/MunicipalityZone/CUDMunicipalityZoneDetails", dataforUpdate).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Updated ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }

                    $http.get(ENV.apiUrl + "api/MunicipalityZone/getMunicipalityZoneDetails").then(function (res1) {
                        $scope.CreDiv = res1.data;
                        $scope.totalItems = $scope.CreDiv.length;
                        $scope.todos = $scope.CreDiv;
                        $scope.makeTodos();

                    });
                });
                $scope.dataforUpdate = [];
                $scope.table = true;
                $scope.display = false;
            }
        }

        //DATA EDIT
        $scope.edit = function (str) {
            debugger;
            $scope.gdisabled = false;
            $scope.aydisabled = true;
            $scope.table = false;
            $scope.display = true;
            $scope.save_btn = false;
            $scope.Update_btn = true;
            $scope.divcode_readonly = true;
            //$scope.temp = str;

            
            $scope.temp = {
                municipalityZone_code: str.municipalityZone_code,
                english_name: str.english_name,
                arabic_name: str.arabic_name,
                other_name: str.other_name,
                municipalityZone_status: str.municipalityZone_status,
                municipality_code: str.municipality_code,
                municipality_name: str.municipality_name
            }

            $scope.Myform1.$setPristine();
            $scope.Myform1.$setUntouched();
        }

        //Delete

        $scope.CheckAllChecked = function () {
            debugger;
            main = document.getElementById('mainchk');
            if (main.checked == true) {
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i + 10);
                    v.checked = true;
                    $scope.row1 = 'row_selected';
                    $('tr').addClass("row_selected");
                }
            }
            else {

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i + 10);
                    v.checked = false;
                    main.checked = false;
                    $scope.row1 = '';
                    $('tr').removeClass("row_selected");
                }
            }

        }

        $scope.checkonebyonedelete = function () {
            debugger;
            $("input[type='checkbox']").change(function (e) {
                if ($(this).is(":checked")) {
                    $(this).closest('tr').addClass("row_selected");
                    $scope.color = '#edefef';
                } else {
                    $(this).closest('tr').removeClass("row_selected");
                    $scope.color = '#edefef';
                }
            });

            main = document.getElementById('mainchk');
            if (main.checked == true) {
                main.checked = false;
                $scope.color = '#edefef';
                $scope.row1 = '';
            }
        }

        $scope.OkDelete = function () {
            debugger;
            deletefin = [];
            $scope.flag = false;
            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                var v = document.getElementById(i + 10);
                if (v.checked == true) {
                    $scope.flag = true;
                    var deletemodulecode = ({
                        'municipalityZone_code': $scope.filteredTodos[i].municipalityZone_code,
                        opr: 'D'
                    });
                    deletefin.push(deletemodulecode);
                }
            }
            if ($scope.flag) {
                swal({
                    title: '',
                    text: "Are you sure you want to Delete?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {

                    if (isConfirm) {

                        $http.post(ENV.apiUrl + "api/MunicipalityZone/CUDMunicipalityZoneDetails", deletefin).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $http.get(ENV.apiUrl + "api/MunicipalityZone/getMunicipalityZoneDetails").then(function (res1) {
                                            $scope.CreDiv = res1.data;
                                            $scope.totalItems = $scope.CreDiv.length;
                                            $scope.todos = $scope.CreDiv;
                                            $scope.makeTodos();
                                        });
                                        main = document.getElementById('mainchk');
                                        if (main.checked == true) {
                                            main.checked = false;
                                            {
                                                $scope.row1 = '';
                                            }
                                        }
                                    }
                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $http.get(ENV.apiUrl + "api/MunicipalityZone/getMunicipalityZoneDetails").then(function (res1) {
                                            $scope.CreDiv = res1.data;
                                            $scope.totalItems = $scope.CreDiv.length;
                                            $scope.todos = $scope.CreDiv;
                                            $scope.makeTodos();
                                        });
                                        main = document.getElementById('mainchk');
                                        if (main.checked == true) {
                                            main.checked = false;
                                            {
                                                $scope.row1 = '';
                                            }
                                        }
                                    }
                                });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });
                    }
                    else {
                        for (var i = 0; i < $scope.filteredTodos.length; i++) {
                            var v = document.getElementById(i + 10);
                            if (v.checked == true) {
                                v.checked = false;
                                //$scope.row1 = '';
                                main.checked = false;
                                $('tr').removeClass("row_selected");
                            }
                        }
                    }
                });
            }
            else {
                swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
            }
            $scope.currentPage = true;
        }

    }])
})();