﻿(function () {
    'use strict';
    var del = [];
    var main;
    var imagename = '';
    var formdata = new FormData();
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TestQuizeSubmissionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.save_btn = true;
            $scope.questionbank_Questions_Details = [];
            $scope.questionbank_Questions_Details1 = [];
            $scope.upload_questionbank_question_code = '';
            $scope.upload_questionbank_question_answer_code = '';
            $scope.sims_questionbank_question_image_path = '';

            $scope.temp = {};


         //   $scope.GetquestionbankDetails = function () {
            $http.get(ENV.apiUrl + "api/surveyquestionmapping/getTestSubmist?sims_questionbank_code=" + '' + "&sims_questionbank_user_response_user_code=" + '').then(function (Get_questionbank_details) {
                debugger
                $scope.questionbank_details = Get_questionbank_details.data;
                $scope.temp = {
                    'total_questionbank_question': $scope.questionbank_details[0].total_questionbank_question,
                    'total_question_attempted': $scope.questionbank_details[0].total_question_attempted,
                    'left_question': $scope.questionbank_details[0].left_question,
                    'total_correct_answer': $scope.questionbank_details[0].total_correct_answer,
                    'total_wrong_answer': $scope.questionbank_details[0].total_wrong_answer,
                    'maximum_marks': $scope.questionbank_details[0].maximum_marks,
                    'correct_marks': $scope.questionbank_details[0].correct_marks,
                    'wrong_marks': $scope.questionbank_details[0].wrong_marks,
                    'total_marks': $scope.questionbank_details[0].total_marks,
                };

                $scope.labels = [];
                $scope.data = [];
                $scope.value = [];

                var doughnutData = [];

                for (var i = 0; i < $scope.questionbank_details.length; i++) {
                    $scope.labels.push($scope.questionbank_details[i].total_correct_answer);
                    $scope.labels.push($scope.questionbank_details[i].total_questionbank_question);
                    $scope.data.push(parseInt($scope.questionbank_details[i].total_correct_answer));
                    $scope.data.push(parseInt($scope.questionbank_details[i].total_questionbank_question));
                    $scope.value.push($scope.questionbank_details[i].total_correct_answer)
                }

                $scope.labels1 = [];
                $scope.questionattempted = [];
                $scope.value1 = [];

                var doughnutData = [];

                for (var i = 0; i < $scope.questionbank_details.length; i++) {
                    $scope.labels1.push($scope.questionbank_details[i].total_question_attempted);
                    $scope.labels1.push($scope.questionbank_details[i].total_questionbank_question);
                    $scope.questionattempted.push(parseInt($scope.questionbank_details[i].total_question_attempted));
                    $scope.questionattempted.push(parseInt($scope.questionbank_details[i].total_questionbank_question));
                    $scope.value1.push($scope.questionbank_details[i].total_question_attempted)
                }

                $scope.labels2 = [];
                $scope.timedata = [];
                $scope.value2 = [];

                var doughnutData = [];

                for (var i = 0; i < $scope.questionbank_details.length; i++) {
                    $scope.labels2.push(3.12);
                    $scope.labels2.push(10);
                    $scope.timedata.push(parseInt(3.12));
                    $scope.timedata.push(parseInt(10));
                    $scope.value2.push($scope.questionbank_details[i].total_question_attempted)
                }


                });
           // }
        }]);
})();