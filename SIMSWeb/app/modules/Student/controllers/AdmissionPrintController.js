﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AdmissionPrintController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
          // Pre-Required Functions and Variables
 
            /* PAGER FUNCTIONS*/

            $scope.search_txt = '';
            $scope.show_flag = false;
            function _onFetchData() {
                $scope.admissionlist = [];
                $scope.show_flag = false;
                if ($scope.search_txt == undefined)
                    $scope.search_txt = '';
                $http.get(ENV.apiUrl + "api/common/Admission?search=" + $scope.search_txt).then(function (admissionData) {
                    $scope.admissionData = admissionData.data;
                    });
            }
          
            $scope.fetchDatakeyPress = function ($event) {
                if ($event.keyCode == 13) {
                    _onFetchData();
                }
            }
            $scope.btnPreview_click = function () {
                _onFetchData();

            }
            
            $scope.getAdmissionLst = function (info) {
                $http.get(ENV.apiUrl + "api/common/Admission?reg_id=" + info.sims_admission_parent_REG_id).then(function (admissionlist) {
                    $scope.admissionlist = admissionlist.data;
                    if ($scope.admissionlist.length > 0)
                        $scope.show_flag = true;
                    else {
                        $scope.show_flag = false;
                        swal(
                            {
                                showCloseButton: true,
                                text: 'Admission(s) Or Prospect(s) Infornation Not Found.',
                                width: 350,
                                showCloseButon: true
                            });
                    }
                });
            }
            $scope.btnReset_click = function () {
                $scope.search_txt = '';
                $scope.show_flag = false;
                $scope.admissionData = [];
                $scope.admissionlist = [];

            }

        //Events End
         }])
})();

