﻿(function () {
    'use strict';
    var formdata = new FormData();
    var imagename = '';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudentOnlyViewImageCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$route', '$templateCache', '$location', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $route, $templateCache, $location) {
            $scope.pagesize = '5';
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {                
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.Show_Data();
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;

                $scope.edt = {
                    sims_cur_code: $scope.curriculum[0].sims_cur_code
                }
                $scope.getacyr($scope.edt.sims_cur_code);
            });

            $scope.getacyr = function (str) {
                debugger;
                $scope.cur_code = str;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    debugger;
                    $scope.Academic_year = Academicyear.data;
                    $scope.edt = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                        'sims_academic_year': $scope.Academic_year[0].sims_academic_year,
                    }
                    $scope.getGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                });
            }

            $scope.getGrade = function (str, str1) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str + "&academic_year=" + str1).then(function (res) {
                    $scope.grade = res.data;
                })
            }

            $scope.getsection = function (cur_code, grade_code, academic_year) {

                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + cur_code + "&grade_code=" + grade_code + "&academic_year=" + academic_year).then(function (Allsection) {

                    $scope.section1 = Allsection.data;
                })
            };

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 8;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.Show_Data = function () {
                $scope.StudenteData = [];
                debugger;
                if ($scope.edt.sims_grade_code === undefined || $scope.edt.sims_section_name === undefined) {
                    swal({ title: "Alert", text: "Please select Grade & Section", width: 300, height: 200 });
                    $scope.table1 = false;
                }
                else {
                    $scope.table1 = true;
                    $scope.ImageView = false;
                    $scope.imageUrl = '';

                    $http.get(ENV.apiUrl + "api/student/studentImage/getStudentsDetails?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name).then(function (Student_Data) {
                        $scope.StudenteData = Student_Data.data;
                        $scope.totalItems = $scope.StudenteData.length;
                        //$scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/';
                        $scope.todos = $scope.StudenteData;
                        $scope.makeTodos();

                        setTimeout(function () {
                            angular.forEach($scope.StudenteData, function (value, key) {
                                var num = Math.random();
                                var imgSrc = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/' + value.sims_student_image + "?v=" + num;
                                document.getElementById(value.sims_student_enroll_number).setAttribute('src', imgSrc);

                            });
                        }, 1000);

                        if (Student_Data.data.length > 0) { }

                        else {
                            $scope.ImageView = true;
                        }
                    });
                }

            }

            $scope.Reset = function () {

                $scope.edt.sims_cur_code = "";
                $scope.edt.sims_academic_year = "";
                $scope.edt.sims_grade_code = "";
                $scope.edt.sims_section_name = "";
                $scope.table1 = false;

            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.StudenteData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.StudenteData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in 2 fields */
                return (item.sims_student_passport_full_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_student_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_student_enroll_number == toSearch) ? true : false;
            }

            $scope.UploadImageModal = function (str) {

                $scope.edt['sims_student_image'] = "";
                $scope.prev_img = "";

                imagename = str.sims_student_enroll_number;
                $('#myModal').modal('show');
            }

            $scope.file_changed = function (element) {

                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;
                    });
                };
                reader.readAsDataURL(photofile);

                $scope.Upload = function () {
                    $scope.prev_img = "";
                    var data = [];
                    var t = $scope.photo_filename.split("/")[1];                    
                    data = {
                        sims_student_enroll_number: imagename,
                        sims_student_image: imagename + "." + t,
                        opr: "W"
                    }
                    
                    $http.post(ENV.apiUrl + "api/student/studentImage/CUDUpdatePics", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Image Upload Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }

                        $('#myModal').modal('hide');
                        var request = {
                            method: 'POST',
                            url: ENV.apiUrl + '/api/file/upload?filename=' + data.sims_student_enroll_number + "&location=" + "/StudentImages",
                            data: formdata,
                            headers: {
                                'Content-Type': undefined
                            }
                        };
                        $http(request)
                      .success(function (d) {
                          //var currentPageTemplate = $location.url();
                          //$templateCache.remove(currentPageTemplate);
                          $scope.Show_Data();
                          //$route.reload();
                          //.location.reload();
                          //setTimeout(function () {
                          //    window.location.reload();
                          //   // $scope.Show_Data();
                          //},300);
                      },
                      function () {
                          alert("Err");
                      });

                    },
                        function () {
                            $('#ErrorMessage').modal({ backdrop: "static" });


                        });

                }

                $scope.getTheFiles = function ($files) {
                    $scope.filesize = true;

                    angular.forEach($files, function (value, key) {
                        formdata.append(key, value);

                        var i = 0;
                        if ($files[i].size > 800000) {
                            $scope.filesize = false;
                            $scope.edt.photoStatus = false;

                            swal({  text: "File Should Not Exceed 800Kb.", imageUrl: "assets/img/notification-alert.png" });

                        }
                        else {

                        }

                    });
                };
            }

            $scope.delete = function (obj) {
                $scope.flag = true;
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            var deleteStudentDetail = ({
                                'sims_student_enroll_number': obj.sims_student_enroll_number,
                                'sims_student_img': obj.sims_student_image,
                                'opr': 'D'
                            });


                            $http.post(ENV.apiUrl + "api/student/studentImage/CUDStudentsDetails", deleteStudentDetail).then(function (msg) {
                                $scope.msg1 = msg.data;
                                debugger
                                if ($scope.msg1 == true) {
                                    swal({ text: "Image Deleted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                    $scope.imageUrl = '';
                                    $scope.Show_Data();
                                }
                                else {
                                    swal({ text: "Image Not Deleted", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                            });
                        }
                    });
                    $http.get(ENV.apiUrl + "api/student/studentImage/getStudentsDetails").then(function (Student_Data) {
                        $scope.AttendanceCode_Data = Student_Data.data;
                        $scope.totalItems = $scope.AttendanceCode_Data.length;
                        $scope.todos = $scope.AttendanceCode_Data;
                        $scope.makeTodos();

                    });
                }


            }


            $scope.downloaddoc = function (str) {                                
                $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/';
                $scope.url = $scope.imageUrl + str.sims_student_image;
                //$scope.url = 'http://api.mograsys.com/APIERP/Content/adis/Images/StudentImages/' + str.sims_student_image;
                window.open($scope.url, '_new');
            }

            simsController.directive('ngFiles', ['$parse', function ($parse) {

                function fn_link(scope, element, attrs) {
                    var onChange = $parse(attrs.ngFiles);
                    element.on('change', function (event) {
                        onChange(scope, { $files: event.target.files });
                    });
                };

                return {
                    link: fn_link
                }
            }])
        }])
})();
