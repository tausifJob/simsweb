﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var report_card_code;
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('DetentionThresholdCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {


            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            //Select Data SHOW


            //Bind Combo
            $scope.temp = {};
            $scope.oldtemp = {};

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCurriculum) {
                $scope.Curriculum = getCurriculum.data;
                if ($scope.Curriculum.length > 0) {
                    $scope.temp.sims_cur_code = $scope.Curriculum[0].sims_cur_code;
                    $scope.getacademicYear($scope.temp.sims_cur_code);
                }
                //$scope.temp['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                //$scope.getacademicyear($scope.curriculum[0].sims_cur_code);
                console.log($scope.Curriculum);
            });

            $http.get(ENV.apiUrl + "api/Student/getDetentionThreshold").then(function (res1) {

                $scope.DetentionThresholdData = res1.data;
                $scope.CountAll = $scope.DetentionThresholdData.length;
                $scope.totalItems = $scope.DetentionThresholdData.length;
                $scope.todos = $scope.DetentionThresholdData;
                $scope.makeTodos();

            });

           

            //$scope.GetCombo = function (curcode, year, grade, section) {
            //    debugger;
            //    $http.get(ENV.apiUrl + "api/ReportCard/getReportCardLevelName?cur_code=" + curcode + "&academic_year=" + year + "&grade_code=" + grade + "&section_name=" + section).then(function (res) {
            //        debugger;
            //        $scope.levelNameData = res.data;

            //    });


            //    $http.get(ENV.apiUrl + "api/ReportCard/getTermDetails?cur_code=" + curcode + "&academic_year=" + year).then(function (res) {
            //        $scope.TermData = res.data;

            //    });


            //    $http.get(ENV.apiUrl + "api/ReportCard/getParamDesc?cur_code=" + curcode + "&academic_year=" + year + "&grade_code=" + grade + "&section_name=" + section).then(function (res) {
            //        $scope.ParamDescData = res.data;

            //    });
            //}

            $scope.getacademicYear = function (str) {
                $scope.curriculum_code = str;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (getAcademicYear) {
                    $scope.AcademicYear = getAcademicYear.data;
                    if ($scope.AcademicYear.length > 0) {
                        $scope.temp.sims_academic_year = $scope.AcademicYear[0].sims_academic_year;
                        $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                    }
                    //$scope.temp['sims_academic_year'] = $scope.AcademicYear[0].sims_academic_year;
                    // $scope.getGrade($scope.Curriculum[0].sims_cur_code,$scope.AcademicYear[0].sims_academic_year)
                    console.log($scope.AcademicYear);
                })
            }


            $scope.getGrade = function (str1, str2) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str1 + "&academic_year=" + str2).then(function (getAllGrades) {
                    $scope.AllGrades = getAllGrades.data;


                })
            }

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.DetentionThresholdData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                //$scope.CheckAllChecked();
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.DetentionThresholdData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.DetentionThresholdData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_section_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 
                        ) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
                debugger;
                $scope.disabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = [];
                $scope.CurriculumReadonly = false;
                $scope.AcademicYearReadonly = false;
                $scope.GradeReadonly = false;
                $scope.SectionReadonly = false;
               
                $scope.temp = {
                    //declaredate: yyyy + '-' + mm + '-' + dd,
                    declaredate: dd + '-' + mm + '-' + yyyy,
                    //publishdate: yyyy + '-' + mm + '-' + dd,
                   
                    status: true,
                }
                $scope.edt = {
                    username: "Admin",
                }
                debugger;
                //curriculum
                if ($scope.Curriculum.length > 0) {
                    $scope.temp.sims_cur_code = $scope.Curriculum[0].sims_cur_code;
                    $scope.getacademicYear($scope.temp.sims_cur_code);
                }

                // Academic Year
                if ($scope.AcademicYear.length > 0) {
                    $scope.temp.sims_academic_year = $scope.AcademicYear[0].sims_academic_year;
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                }

                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            $scope.myFunct = function (keyEvent) {
                debugger;
                if (keyEvent.which == 13)
                    //$scope.temp.declaredate = yyyy + '-' + mm + '-' + dd;
                    $scope.temp.declaredate = dd + '-' + mm + '-' + yyyy;
            }

            $scope.myFunct1 = function (keyEvent) {
                debugger;
                if (keyEvent.which == 13)
                    //$scope.temp.publishdate = yyyy + '-' + mm + '-' + dd;
                    $scope.temp.publishdate = dd + '-' + mm + '-' + yyyy;
            }


            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = [];
                $scope.table = true;
                $scope.display = false;
                $scope.edt = [];
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.sectionrbt = false;
                $scope.sectiontxt = true;
                $scope.Update_btn = true;
                $scope.readonlyAdjustableFeeType = true;
                $scope.CurriculumReadonly = true;
                $scope.AcademicYearReadonly = true;
                $scope.GradeReadonly = true;
                $scope.SectionReadonly = true;


                $scope.edt = {
                    username: str.user_name,
                }
               
                $scope.getacademicYear(str.sims_cur_code);
                $scope.getGrade(str.sims_cur_code, str.sims_academic_year);
                $scope.getsection(str.sims_cur_code, str.sims_grade_code, str.sims_academic_year);
            //    $scope.GetCombo(str.sims_cur_code, str.sims_academic_year, str.sims_grade_code, str.sims_section_code);
                $scope.temp = {
                    status: str.sims_detention_threshold_status,
                    sims_cur_code: str.sims_cur_code,
                    sims_academic_year: str.sims_academic_year,
                    sims_grade_code: str.sims_grade_code,
                    sims_section_code: str.sims_section_code,                                 
                    declaredate: str.sims_detention_threshold_created_date,
                    sims_detention_threshold1: str.sims_detention_threshold1,
                    sims_detention_threshold2: str.sims_detention_threshold2,
                    sims_detention_threshold3: str.sims_detention_threshold3,
                    username: str.sims_detention_threshold_created_user_code,
                   

                };

                $scope.oldtemp = {
                    status: str.sims_detention_threshold_status,
                    sims_cur_code: str.sims_cur_code,
                    sims_academic_year: str.sims_academic_year,
                    sims_grade_code: str.sims_grade_code,
                    sims_section_code: str.sims_section_code,
                    declaredate: str.sims_detention_threshold_created_date,
                    sims_detention_threshold1: str.sims_detention_threshold1,
                    sims_detention_threshold2: str.sims_detention_threshold2,
                    sims_detention_threshold3: str.sims_detention_threshold3,
                    username: str.sims_detention_threshold_created_user_code,


                };

            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger;

                if (Myform) {

                    datasend = [];
                    data = [];
        
                    var data = {
                        sims_cur_code: $scope.temp.sims_cur_code,
                        sims_academic_year: $scope.temp.sims_academic_year,
                        sims_grade_code: $scope.temp.sims_grade_code,
                        sims_section_code: $scope.temp.sims_section_code,                      
                        sims_detention_threshold_created_date: $scope.temp.declaredate,                   
                        sims_detention_threshold_created_user_code: $scope.edt.username,
                        sims_detention_threshold_status: $scope.temp.status,
                        sims_detention_threshold1: $scope.temp.sims_detention_threshold1,
                        sims_detention_threshold2: $scope.temp.sims_detention_threshold2,
                        sims_detention_threshold3: $scope.temp.sims_detention_threshold3,
                    }
                    data.opr = 'I';
                    datasend.push(data);

                    $http.post(ENV.apiUrl + "api/Student/InsertUpdateDeleteDetentionThreshold", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 380 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 380 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $scope.getgrid();
                    });
                    $scope.table = true;
                    $scope.display = false;
                }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                debugger;
                $scope.flag1 = false;
                if (Myform) {
                    var sectioncode1;
                    var sectioncode = [];


                    dataforUpdate = [];

                    var data = {
                        sims_cur_code: $scope.temp.sims_cur_code,
                        sims_academic_year: $scope.temp.sims_academic_year,
                        sims_grade_code: $scope.temp.sims_grade_code,
                        sims_section_code: $scope.temp.sims_section_code,
                        sims_detention_threshold_created_date: $scope.temp.declaredate,
                        sims_detention_threshold_created_user_code: $scope.edt.username,
                        sims_detention_threshold_status: $scope.temp.status,
                        sims_detention_threshold1: $scope.temp.sims_detention_threshold1,
                        sims_detention_threshold2: $scope.temp.sims_detention_threshold2,
                        sims_detention_threshold3: $scope.temp.sims_detention_threshold3,

                        sims_detention_threshold1old: $scope.oldtemp.sims_detention_threshold1,
                        sims_detention_threshold2old: $scope.oldtemp.sims_detention_threshold2,
                        sims_detention_threshold3old: $scope.oldtemp.sims_detention_threshold3,


                    }

                    data.opr = "U";

                    dataforUpdate.push(data);

                    $http.post(ENV.apiUrl + "api/Student/InsertUpdateDeleteDetentionThreshold", dataforUpdate).then(function (msg) {
                        debugger;
                        $scope.updateResult = msg.data;
                        if ($scope.updateResult == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 380 });
                        }
                        else if ($scope.updateResult == false) {
                            swal({ text: "Record Not Updated.", imageUrl: "assets/img/close.png", width: 380 });
                        }
                        else {
                            swal("Error-" + $scope.updateResult)
                        }
                        $scope.getgrid();
                    });

                    $scope.table = true;
                    $scope.display = false;
                }
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_report_card_name + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_report_card_name + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/Student/getDetentionThreshold").then(function (res1) {

                    $scope.DetentionThresholdData = res1.data;
                    $scope.totalItems = $scope.DetentionThresholdData.length;
                    $scope.todos = $scope.DetentionThresholdData;
                    $scope.makeTodos();

                });
            }

            $scope.Delete = function () {
                debugger;
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    //var t = $scope.filteredTodos[i].uom_code;
                      var v = document.getElementById('test_' + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                            'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                          
                            'opr': 'D',

                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {

                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/Student/InsertUpdateDeleteDetentionThreshold", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;



                                        }
                                    });
                                }
                                else {
                                    swal({ text: "Record Already Mapped.Can't be Deleted. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }

                            });
                        }
                        else {

                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_report_card_name + i);
                                //var t = $scope.filteredTodos[i].sims_fee_category;
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");


                                }


                            }

                        }

                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

                $scope.currentPage = str;
            }

            $(function () {
                $('#app_on1').multipleSelect({
                    width: '100%'
                });
            });



            $scope.getsection = function (str, str1, str2) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + str + "&grade_code=" + str1 + "&academic_year=" + str2).then(function (Allsection) {
                    $scope.getSectionFromGrade = Allsection.data;
                    setTimeout(function () {
                        $('#app_on1').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                })
            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


         }])

})();
