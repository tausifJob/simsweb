﻿/// <reference path="ParentRegistrationCont.js" />
(function () {
    'use strict';
    var parent_info; var temp, photo_filename;
    var sims_father_image, sims_mother_image, sims_guardian_image;
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ParentRegistrationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {


            var formdata = new FormData();
            var formdatamother = new FormData();
            var formdataguardian = new FormData();

            $scope.showEditBtn = false;
            $scope.showSaveBtn = true;
            $scope.showNextParentID = false;
            $scope.student_father_image ='';
            $scope.student_guardian_image='';
            $scope.student_mother_image ='';

            $scope.GlobeSearch = function () {
                debugger;
                $rootScope.visible_stud = false;
                $rootScope.visible_parent = true;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = false;
                $rootScope.chkMulti = false;
                
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                //$scope.temp.em_login_code = '';
                //$scope.temp.EmpName = '';
                $scope.showBtn = false;

            }

            $scope.$on('global_cancel', function (str) {

                if ($scope.SelectedUserLst.length > 0) {

                    $scope.edt = {};
                    $scope.edt = {
                        'parentNumber': $scope.SelectedUserLst[0].s_parent_id
                    }                    
                    $scope.parentSearch($scope.SelectedUserLst[0].s_parent_id);
                    $scope.showNextParentID = false;
                }
            });

            $scope.searchGlobalClick = function () {
                $scope.Global_Search_by_employee();
            }

            var nextParent = ['ajb', 'aji', 'zps'];

            if (nextParent.includes($http.defaults.headers.common['schoolId'])) {
                $scope.showNextParentID = true;
            }

            $scope.getParentId = function () {
                $http.get(ENV.apiUrl + "api/ParentRegistration/getParentID").then(function (res) {
                    if (res.data.length > 0) {
                        $scope.nextParentId = res.data[0].user_code;                       
                    }                    
                });
            }
            $scope.getParentId();

            $http.get(ENV.apiUrl + "api/State/getState").then(function (get_State) {
                $scope.stt = get_State.data;
            });            
            $http.get(ENV.apiUrl + "api/CityMaster/getCity").then(function (get_City) {
                $scope.ctt = get_City.data;
            });
            
            $scope.parentSearch = function (emplogcode) {
                debugger;
                $http.get(ENV.apiUrl + "api/ParentRegistration/getAll_Parent_Details?sims_parent_number=" + emplogcode).then(function (res) {
                    $scope.temp1 = res.data;
                    if ($scope.temp1.length > 0) {
                        $scope.showNextParentID = false;
                        $scope.showEditBtn = true;
                        $scope.showSaveBtn = false;
                        $scope.edt = {
                            parentNumber: emplogcode,
                            sims_parent_number: $scope.temp1[0].sims_parent_number,
                            sims_parent_login_code: $scope.temp1[0].sims_parent_login_code,
                            father_building_number: $scope.temp1[0].sims_parent_father_building_number,

                            father_salutation_code: $scope.temp1[0].sims_parent_father_salutation_code,
                            father_first_name: $scope.temp1[0].sims_parent_father_first_name,
                            father_middle_name: $scope.temp1[0].sims_parent_father_middle_name,
                            father_last_name: $scope.temp1[0].sims_parent_father_last_name,
                            father_family_name: $scope.temp1[0].sims_parent_father_family_name,
                            father_family_name_ot: $scope.temp1[0].sims_parent_father_name_ot,

                            father_nationality1_code: $scope.temp1[0].sims_parent_father_nationality1_code,
                            father_nationality2_code: $scope.temp1[0].sims_parent_father_nationality2_code,
                            father_appartment_number: $scope.temp1[0].sims_parent_father_appartment_number,
                            father_street_number: $scope.temp1[0].sims_parent_father_street_number,
                            father_area_number: $scope.temp1[0].sims_parent_father_area_number,
                            father_summary_address: $scope.temp1[0].sims_parent_father_summary_address,
                            father_city: $scope.temp1[0].sims_parent_father_city1,
                            father_state: $scope.temp1[0].sims_parent_father_state1,
                            father_country_code: $scope.temp1[0].sims_parent_father_country_code1,
                            father_phone: $scope.temp1[0].sims_parent_father_phone,
                            father_mobile: $scope.temp1[0].sims_parent_father_mobile,
                            father_email: $scope.temp1[0].sims_parent_father_email,
                            father_fax: $scope.temp1[0].sims_parent_father_fax,
                            father_po_box: $scope.temp1[0].sims_parent_father_po_box,
                            father_occupation: $scope.temp1[0].sims_parent_father_occupation,
                            //sims_parent_father_occupation_local_language:$scope.temp1[0].sims_parent_father_occupation_local_language,
                            //sims_parent_father_occupation_location_local_language: $scope.temp1[0].sims_parent_father_occupation_location_local_language,
                            father_passport_number: $scope.temp1[0].sims_parent_father_passport_number,
                            sims_parent_father_img:$scope.temp1[0].sims_parent_father_img,

                            mother_salutation_code: $scope.temp1[0].sims_parent_mother_salutation_code,
                            mother_first_name: $scope.temp1[0].sims_parent_mother_first_name,
                            mother_middle_name: $scope.temp1[0].sims_parent_mother_middle_name,
                            mother_last_name: $scope.temp1[0].sims_parent_mother_last_name,
                            mother_family_name: $scope.temp1[0].sims_parent_mother_family_name,
                            mother_name_ot: $scope.temp1[0].sims_parent_mother_name_ot,
                            mother_nationality1_code: $scope.temp1[0].sims_parent_mother_nationality1_code,
                            mother_appartment_number: $scope.temp1[0].sims_parent_mother_appartment_number,
                            mother_nationality2_code: $scope.temp1[0].sims_parent_mother_nationality2_code,
                            mother_building_number: $scope.temp1[0].sims_parent_mother_building_number,
                            mother_area_number: $scope.temp1[0].sims_parent_mother_area_number,
                            mother_street_number: $scope.temp1[0].sims_parent_mother_street_number,
                            mother_summary_address: $scope.temp1[0].sims_parent_mother_summary_address,
                            mother_city: $scope.temp1[0].sims_parent_mother_city1,
                            mother_state: $scope.temp1[0].sims_parent_mother_state1,
                            mother_phone: $scope.temp1[0].sims_parent_mother_phone,
                            mother_country_code: $scope.temp1[0].sims_parent_mother_country_code1,
                            mother_mobile: $scope.temp1[0].sims_parent_mother_mobile,
                            mother_email: $scope.temp1[0].sims_parent_mother_email,
                            mother_fax: $scope.temp1[0].sims_parent_mother_fax,
                            mother_po_box: $scope.temp1[0].sims_parent_mother_po_box,
                            mother_occupation: $scope.temp1[0].sims_parent_mother_occupation,
                            //sims_parent_mother_occupation_local_language:$scope.temp1[0].sims_parent_mother_occupation_local_language,
                            mother_company: $scope.temp1[0].sims_parent_mother_company,
                            mother_passport_number: $scope.temp1[0].sims_parent_mother_passport_number,
                            sims_parent_mother_img: $scope.temp1[0].sims_parent_mother_img,

                            guardian_salutation_code: $scope.temp1[0].sims_parent_guardian_salutation_code,
                            guardian_first_name: $scope.temp1[0].sims_parent_guardian_first_name,
                            guardian_middle_name: $scope.temp1[0].sims_parent_guardian_middle_name,
                            guardian_last_name: $scope.temp1[0].sims_parent_guardian_last_name,
                            guardian_family_name: $scope.temp1[0].sims_parent_guardian_family_name,
                            guardian_nationality1_code: $scope.temp1[0].sims_parent_guardian_nationality1_code,
                            guardian_name_ot: $scope.temp1[0].sims_parent_guardian_name_ot,
                            guardian_nationality2_code: $scope.temp1[0].sims_parent_guardian_nationality2_code,
                            guardian_appartment_number: $scope.temp1[0].sims_parent_guardian_appartment_number,
                            guardian_building_number: $scope.temp1[0].sims_parent_guardian_building_number,
                            guardian_street_number: $scope.temp1[0].sims_parent_guardian_street_number,
                            guardian_summary_address: $scope.temp1[0].sims_parent_guardian_summary_address,
                            guardian_area_number: $scope.temp1[0].sims_parent_guardian_area_number,
                            guardian_city: $scope.temp1[0].sims_parent_guardian_city1,
                            guardian_state: $scope.temp1[0].sims_parent_guardian_state1,
                            guardian_country_code: $scope.temp1[0].sims_parent_guardian_country_code1,
                            guardian_phone: $scope.temp1[0].sims_parent_guardian_phone,
                            guardian_mobile: $scope.temp1[0].sims_parent_guardian_mobile,
                            guardian_email: $scope.temp1[0].sims_parent_guardian_email,
                            guardian_fax: $scope.temp1[0].sims_parent_guardian_fax,
                            guardian_po_box: $scope.temp1[0].sims_parent_guardian_po_box,
                            guardian_occupation: $scope.temp1[0].sims_parent_guardian_occupation,
                            sims_parent_guardian_img:$scope.temp1[0].sims_parent_guardian_img,
                            //sims_parent_guardian_occupation_local_language:$scope.temp1[0].sims_parent_guardian_occupation_local_language,
                            //sims_parent_guardian_occupation_location_local_language :temp1[0].sims_parent_guardian_occupation_location_local_language,
                            guardian_company: $scope.temp1[0].sims_parent_guardian_company,
                            //guardian_relationship_code: $scope.temp1[0].sims_parent_guardian_salutation_code,
                            guardian_passport_number: $scope.temp1[0].sims_parent_guardian_passport_number,
                            //sims_parent_is_employment_status: $scope.temp1[0].sims_parent_is_employment_status,
                            //sims_parent_is_employement_comp_code1: $scope.temp1[0].sims_parent_is_employement_comp_code1,
                            //sims_parent_is_employment_number: $scope.temp1[0].sims_parent_is_employment_number,
                            //sims_parent_father_national_id: $scope.temp1[0].sims_parent_father_national_id__expiry_date,
                            sims_parent_father_passport_issue_date: $scope.temp1[0].sims_parent_father_passport_issue_date,
                            sims_parent_father_passport_expiry_date: $scope.temp1[0].sims_parent_father_passport_expiry_date,
                            sims_parent_father_national_id_issue_date: $scope.temp1[0].sims_parent_father_national_id_issue_date,
                            sims_parent_father_national_id__expiry_date: $scope.temp1[0].sims_parent_father_national_id__expiry_date,


                            sims_parent_mother_passport_issue_date: $scope.temp1[0].sims_parent_mother_passport_issue_date,
                            sims_parent_mother_passport_expiry_date: $scope.temp1[0].sims_parent_mother_passport_expiry_date,
                            sims_parent_mother_national_id_issue_date: $scope.temp1[0].sims_parent_mother_national_id_issue_date,
                            sims_parent_mother_national_id_expiry_date: $scope.temp1[0].sims_parent_mother_national_id_expiry_date,

                            sims_parent_gua_passport_issue_date: $scope.temp1[0].sims_parent_guardian_passport_issue_date,
                            sims_parent_gua_passport_expiry_date: $scope.temp1[0].sims_parent_guardian_passport_expiry_date2,
                            sims_parent_gua_national_id: $scope.temp1[0].sims_parent_guardian_national_id_issue_date,
                            sims_parent_gua_national_id_expiry_date: $scope.temp1[0].sims_parent_guardian_national_id_expiry_date,
                        }

                        $scope.student_father_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.temp1[0].sims_parent_father_img;
                        $scope.student_guardian_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.temp1[0].sims_parent_guardian_img;
                        $scope.student_mother_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.temp1[0].sims_parent_mother_img;
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Found", width: 300,timer:4000});
                        $scope.showEditBtn = false;
                        $scope.showSaveBtn = true;
                    }
                    
                });
            }



            $timeout(function () {
                $("#example_wrapper").scrollbar();
                $("#example_wrapper1").scrollbar();
                //$(".scroll-wrapper").css({ 'height': '300px' });
                $("#fixtable").tableHeadFixer({ 'top': 1 });
                $("#fixtable1").tableHeadFixer({ 'top': 1 });
            }, 100);
            $scope.parent_info = [];
            $scope.imgUpload = function () {

            }
            $scope.getTheFiles = function ($files) {

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.file_mother = function (element, name) {

                var str = '';
                str = 'M_' + $scope.edt.parentNumber;

                var photofile = element.files[0];
                photo_filename = (photofile.type);
                $scope.parent_info.sims_admisison_mother_image = str + '.' + photo_filename.split("/")[1];
                str = $scope.parent_info.sims_admisison_mother_image;
                sims_mother_image = photo_filename.replace(photo_filename, str);
                $scope.edt.sims_parent_mother_img = photo_filename.replace(photo_filename, str);

                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.mother_photo = e.target.result;
                        //$scope.temp.em_img = element.files[0].name;
                        //console.log($scope.temp.em_img);
                    });
                };
                reader.readAsDataURL(photofile);


            };

            $scope.file_father = function (element, name) {


                var str = '';
                //str = 'F_' + $scope.parent_info.sims_parent_number;
                str = 'F_' + $scope.edt.parentNumber;

                var photofile = element.files[0];
                photo_filename = (photofile.type);
                $scope.parent_info.sims_admisison_father_image = str + '.' + photo_filename.split("/")[1];
                str = $scope.parent_info.sims_admisison_father_image;
                sims_father_image = photo_filename.replace(photo_filename, str);
                $scope.edt.sims_parent_father_img = photo_filename.replace(photo_filename, str);

                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.father_photo = e.target.result;
                        //$scope.temp.em_img = element.files[0].name;
                        //console.log($scope.temp.em_img);
                    });
                };
                reader.readAsDataURL(photofile);

            };

            $scope.getTheFiles_mother = function ($files) {

                angular.forEach($files, function (value, key) {
                    formdatamother.append(key, value);
                });
            };

            $scope.getTheFiles_father = function ($files) {


                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });

            }

            $scope.file_guardian = function (element, name) {

                var str = '';
                str = 'G_' + $scope.edt.parentNumber;

                var photofile = element.files[0];
                photo_filename = (photofile.type);
                $scope.parent_info.sims_admisison_guardian_image = str + '.' + photo_filename.split("/")[1];
                str = $scope.parent_info.sims_admisison_guardian_image;
                sims_guardian_image = photo_filename.replace(photo_filename, str);
                $scope.edt.sims_parent_guardian_img = photo_filename.replace(photo_filename, str);

                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.guardian_photo = e.target.result;
                        //$scope.temp.em_img = element.files[0].name;
                        //console.log($scope.temp.em_img);
                    });
                };
                reader.readAsDataURL(photofile);
            };

            $scope.getTheFiles_guardian = function ($files) {

                angular.forEach($files, function (value, key) {
                    formdataguardian.append(key, value);
                });
            };

            $scope.Cancel = function () {
                $scope.edt = [];
                sims_father_image = '';
                sims_guardian_image = '';
                sims_mother_image = '';
                $scope.father_photo = '';
                $scope.mother_photo = '';
                $scope.guardian_photo = '';
                $scope.myForm2.$setPristine();
                $scope.myForm2.$setUntouched();
                $scope.myForm3.$setPristine();
                $scope.myForm3.$setUntouched();
                $scope.myForm4.$setPristine();
                $scope.myForm4.$setUntouched();
                document.getElementById('txt_guar_addr').value = '';
                document.getElementById('txt_mother_addr').value = '';
                document.getElementById('txt_father_summary_add').value = '';
                if (nextParent.includes($http.defaults.headers.common['schoolId'])) {
                    $scope.showNextParentID = true;
                }
                else {
                    $scope.showNextParentID = false;
                }

                $("#fathertImage").attr('src', '');
                $("#guradianImage").attr('src', '');
                $("#studentMotherImg").attr('src', '');
                $scope.student_father_image = '';
                $scope.student_guardian_image = '';
                $scope.student_mother_image = '';
                
            }

            $scope.saveParent = function (isvalidate) {

                var datasend = [];
                if (isvalidate) {
                    var data = ({

                        parent_id: $scope.edt.parent_id,
                        father_salutation_code: $scope.edt.father_salutation_code,
                        father_first_name: $scope.edt.father_first_name,
                        father_middle_name: $scope.edt.father_middle_name,
                        father_last_name: $scope.edt.father_last_name,
                        father_family_name: $scope.edt.father_family_name,
                        father_family_name_ot: $scope.edt.father_family_name_ot,
                        father_nationality1_code: $scope.edt.father_nationality1_code,
                        father_nationality2_code: $scope.edt.father_nationality2_code,
                        father_appartment_number: $scope.edt.father_appartment_number,
                        father_building_number: $scope.edt.father_building_number,
                        father_street_number: $scope.edt.father_street_number,
                        father_area_number: $scope.edt.father_area_number,
                        father_city: $scope.edt.father_city,
                        father_state: $scope.edt.father_state,
                        father_country_code: $scope.edt.father_country_code,
                        father_summary_address: $scope.edt.father_summary_address,
                        father_po_box: $scope.edt.father_po_box,
                        father_phone: $scope.edt.father_phone,
                        father_email: $scope.edt.father_email,
                        father_mobile: $scope.edt.father_mobile,
                        father_fax: $scope.edt.father_fax,
                        father_occupation: $scope.edt.father_occupation,
                        father_company: $scope.edt.father_company,
                        father_passport_number: $scope.edt.father_passport_number,

                        //sims_parent_father_img: sims_father_image,
                        sims_parent_father_img: $scope.edt.sims_parent_father_img,

                        sims_parent_father_passport_issue_date: $scope.edt.sims_parent_father_passport_issue_date,
                        sims_parent_father_passport_expiry_date: $scope.edt.sims_parent_father_passport_expiry_date,
                        sims_parent_father_national_id: $scope.edt.sims_parent_father_national_id,
                        sims_parent_father_national_id_issue_date: $scope.edt.sims_parent_father_national_id_issue_date,
                        sims_parent_father_national_id_expiry_date: $scope.edt.sims_parent_father_national_id__expiry_date,

                        guardian_salutation_code: $scope.edt.guardian_salutation_code,
                        guardian_first_name: $scope.edt.guardian_first_name,
                        guardian_middle_name: $scope.edt.guardian_middle_name,
                        guardian_last_name: $scope.edt.guardian_last_name,
                        guardian_family_name: $scope.edt.guardian_family_name,
                        guardian_nationality1_code: $scope.edt.guardian_nationality1_code,
                        guardian_nationality2_code: $scope.edt.guardian_nationality2_code,
                        guardian_appartment_number: $scope.edt.guardian_appartment_number,
                        guardian_building_number: $scope.edt.guardian_building_number,
                        guardian_street_number: $scope.edt.guardian_street_number,
                        guardian_area_number: $scope.edt.guardian_area_number,
                        guardian_city: $scope.edt.guardian_city,
                        guardian_state: $scope.edt.guardian_state,
                        guardian_country_code: $scope.edt.guardian_country_code,
                        guardian_summary_address: $scope.edt.guardian_summary_address,
                        guardian_po_box: $scope.edt.guardian_po_box,
                        guardian_phone: $scope.edt.guardian_phone,
                        guardian_mobile: $scope.edt.guardian_mobile,
                        guardian_fax: $scope.edt.guardian_fax,
                        guardian_email: $scope.edt.guardian_email,
                        guardian_occupation: $scope.edt.guardian_occupation,
                        guardian_company: $scope.edt.guardian_company,
                        guardian_passport_number: $scope.edt.guardian_passport_number,
                        guardian_relationship_code: $scope.edt.guardian_relationship_code,
                        sims_parent_guardian_img: $scope.edt.sims_parent_guardian_img,

                        sims_parent_gua_passport_issue_date: $scope.edt.sims_parent_gua_passport_issue_date,
                        sims_parent_gua_passport_expiry_date: $scope.edt.sims_parent_gua_passport_expiry_date,
                        sims_parent_gua_national_id: $scope.edt.sims_parent_gua_national_id,
                        sims_parent_gua_national_id_issue_date: $scope.edt.sims_parent_gua_national_id_issue_date,
                        sims_parent_gua_national_id_expiry_date: $scope.edt.sims_parent_gua_national_id_expiry_date,

                        sims_parent_mother_passport_issue_date: $scope.edt.sims_parent_mother_passport_issue_date,
                        sims_parent_mother_passport_expiry_date: $scope.edt.sims_parent_mother_passport_expiry_date,
                        sims_parent_mother_national_id: $scope.edt.sims_parent_mother_national_id,
                        sims_parent_mother_national_id_issue_date: $scope.edt.sims_parent_mother_national_id_issue_date,
                        sims_parent_mother_national_id_expiry_date: $scope.edt.sims_parent_mother_national_id_expiry_date,


                        mother_salutation_code: $scope.edt.mother_salutation_code,
                        mother_first_name: $scope.edt.mother_first_name,
                        mother_middle_name: $scope.edt.mother_middle_name,
                        mother_last_name: $scope.edt.mother_last_name,
                        mother_family_name: $scope.edt.mother_family_name,
                        mother_name_ot: $scope.edt.mother_name_ot,
                        mother_nationality1_code: $scope.edt.mother_nationality1_code,
                        mother_nationality2_code: $scope.edt.mother_nationality2_code,
                        mother_appartment_number: $scope.edt.mother_appartment_number,
                        mother_building_number: $scope.edt.mother_building_number,
                        mother_street_number: $scope.edt.mother_street_number,
                        mother_area_number: $scope.edt.mother_area_number,
                        mother_city: $scope.edt.mother_city,
                        mother_state: $scope.edt.mother_state,
                        mother_country_code: $scope.edt.mother_country_code,
                        mother_summary_address: $scope.edt.mother_summary_address,
                        mother_po_box: $scope.edt.mother_po_box,
                        mother_phone: $scope.edt.mother_phone,
                        mother_mobile: $scope.edt.mother_mobile,
                        mother_fax: $scope.edt.mother_fax,
                        mother_email: $scope.edt.mother_email,
                        mother_occupation: $scope.edt.mother_occupation,
                        mother_passport_number: $scope.edt.mother_passport_number,
                        //sims_parent_mother_img: sims_mother_image,
                        sims_parent_mother_img: $scope.edt.sims_parent_mother_img,
                        opr: 'I'
                    });
                    datasend.push(data);

                    //$scope.test = [{section_code:'01'}];
                    $http.post(ENV.apiUrl + "api/ParentRegistration/CUDParent", datasend).then(function (msg) {

                        $scope.parentcode = msg.data;

                        if ($scope.parentcode.length != 0) {

                            if ($scope.parentcode[0].sims_parent_father_img != '') {



                                var request = {
                                    method: 'POST',
                                    url: ENV.apiUrl + '/api/file/upload?filename=' + $scope.parentcode[0].sims_parent_father_img.split('.')[0] + '&location=ParentImages',
                                    data: formdata,
                                    headers: {
                                        'Content-Type': undefined
                                    }
                                };

                                $http(request)
                                     .success(function (d) {
                                     }, function () {
                                         alert("Err");
                                     });
                            }

                            if ($scope.parentcode[0].sims_parent_mother_img != '') {
                                var request = {
                                    method: 'POST',
                                    url: ENV.apiUrl + '/api/file/upload?filename=' + $scope.parentcode[0].sims_parent_mother_img.split('.')[0] + '&location=ParentImages',
                                    data: formdatamother,
                                    headers: {
                                        'Content-Type': undefined
                                    }
                                };

                                $http(request)
                                     .success(function (d) {
                                     }, function () {
                                         alert("Err");
                                     });
                            }
                            if ($scope.parentcode[0].sims_parent_guardian_img != '') {
                                var request = {
                                    method: 'POST',
                                    url: ENV.apiUrl + '/api/file/upload?filename=' + $scope.parentcode[0].sims_parent_guardian_img.split('.')[0] + '&location=ParentImages',
                                    data: formdataguardian,
                                    headers: {
                                        'Content-Type': undefined
                                    }
                                };

                                $http(request)
                                     .success(function (d) {
                                     }, function () {
                                         alert("Err");
                                     });
                            }


                            swal({ text: "Parent Registration Successfully\n Parent Code is=" + $scope.parentcode[0].user_code, imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            $scope.Cancel();
                            $scope.getParentId();

                        }
                        else {
                            swal({  text: "Parent Registration failed", imageUrl: "assets/img/close.png", width: 300, height: 200 });

                        }


                    });
                }

            }

            $scope.update = function () {
                debugger
                var datasend = [];                
                //if (myForm2) {
                    var data = ({
                        parent_id: $scope.edt.parentNumber,
                        father_salutation_code: $scope.edt.father_salutation_code,
                        father_first_name: $scope.edt.father_first_name,
                        father_middle_name: $scope.edt.father_middle_name,
                        father_last_name: $scope.edt.father_last_name,
                        father_family_name: $scope.edt.father_family_name,
                        father_family_name_ot: $scope.edt.father_family_name_ot,
                        father_nationality1_code: $scope.edt.father_nationality1_code,
                        father_nationality2_code: $scope.edt.father_nationality2_code,
                        father_appartment_number: $scope.edt.father_appartment_number,
                        father_building_number: $scope.edt.father_building_number,
                        father_street_number: $scope.edt.father_street_number,
                        father_area_number: $scope.edt.father_area_number,
                        father_city: $scope.edt.father_city,
                        father_state: $scope.edt.father_state,
                        father_country_code: $scope.edt.father_country_code,
                        father_summary_address: $scope.edt.father_summary_address,
                        father_po_box: $scope.edt.father_po_box,
                        father_phone: $scope.edt.father_phone,
                        father_email: $scope.edt.father_email,
                        father_mobile: $scope.edt.father_mobile,
                        father_fax: $scope.edt.father_fax,
                        father_occupation: $scope.edt.father_occupation,
                        father_company: $scope.edt.father_company,
                        father_passport_number: $scope.edt.father_passport_number,

                        //sims_parent_father_img: sims_father_image,
                        sims_parent_father_img: $scope.edt.sims_parent_father_img,

                        sims_parent_father_passport_issue_date: $scope.edt.sims_parent_father_passport_issue_date,
                        sims_parent_father_passport_expiry_date: $scope.edt.sims_parent_father_passport_expiry_date,
                        sims_parent_father_national_id: $scope.edt.sims_parent_father_national_id,
                        sims_parent_father_national_id_issue_date: $scope.edt.sims_parent_father_national_id_issue_date,
                        sims_parent_father_national_id_expiry_date: $scope.edt.sims_parent_father_national_id__expiry_date,

                        guardian_salutation_code: $scope.edt.guardian_salutation_code,
                        guardian_first_name: $scope.edt.guardian_first_name,
                        guardian_middle_name: $scope.edt.guardian_middle_name,
                        guardian_last_name: $scope.edt.guardian_last_name,
                        guardian_family_name: $scope.edt.guardian_family_name,
                        guardian_nationality1_code: $scope.edt.guardian_nationality1_code,
                        guardian_nationality2_code: $scope.edt.guardian_nationality2_code,
                        guardian_appartment_number: $scope.edt.guardian_appartment_number,
                        guardian_building_number: $scope.edt.guardian_building_number,
                        guardian_street_number: $scope.edt.guardian_street_number,
                        guardian_area_number: $scope.edt.guardian_area_number,
                        guardian_city: $scope.edt.guardian_city,
                        guardian_state: $scope.edt.guardian_state,
                        guardian_country_code: $scope.edt.guardian_country_code,
                        guardian_summary_address: $scope.edt.guardian_summary_address,
                        guardian_po_box: $scope.edt.guardian_po_box,
                        guardian_phone: $scope.edt.guardian_phone,
                        guardian_mobile: $scope.edt.guardian_mobile,
                        guardian_fax: $scope.edt.guardian_fax,
                        guardian_email: $scope.edt.guardian_email,
                        guardian_occupation: $scope.edt.guardian_occupation,
                        guardian_company: $scope.edt.guardian_company,
                        guardian_passport_number: $scope.edt.guardian_passport_number,
                        guardian_relationship_code: $scope.edt.guardian_relationship_code,
                        //sims_parent_guardian_img: sims_guardian_image,
                        sims_parent_guardian_img: $scope.edt.sims_parent_guardian_img,

                        sims_parent_gua_passport_issue_date: $scope.edt.sims_parent_gua_passport_issue_date,
                        sims_parent_gua_passport_expiry_date: $scope.edt.sims_parent_gua_passport_expiry_date,
                        sims_parent_gua_national_id: $scope.edt.sims_parent_gua_national_id,
                        sims_parent_gua_national_id_issue_date: $scope.edt.sims_parent_gua_national_id_issue_date,
                        sims_parent_gua_national_id_expiry_date: $scope.edt.sims_parent_gua_national_id_expiry_date,

                        sims_parent_mother_passport_issue_date: $scope.edt.sims_parent_mother_passport_issue_date,
                        sims_parent_mother_passport_expiry_date: $scope.edt.sims_parent_mother_passport_expiry_date,
                        sims_parent_mother_national_id: $scope.edt.sims_parent_mother_national_id,
                        sims_parent_mother_national_id_issue_date: $scope.edt.sims_parent_mother_national_id_issue_date,
                        sims_parent_mother_national_id_expiry_date: $scope.edt.sims_parent_mother_national_id_expiry_date,


                        mother_salutation_code: $scope.edt.mother_salutation_code,
                        mother_first_name: $scope.edt.mother_first_name,
                        mother_middle_name: $scope.edt.mother_middle_name,
                        mother_last_name: $scope.edt.mother_last_name,
                        mother_family_name: $scope.edt.mother_family_name,
                        mother_name_ot: $scope.edt.mother_name_ot,
                        mother_nationality1_code: $scope.edt.mother_nationality1_code,
                        mother_nationality2_code: $scope.edt.mother_nationality2_code,
                        mother_appartment_number: $scope.edt.mother_appartment_number,
                        mother_building_number: $scope.edt.mother_building_number,
                        mother_street_number: $scope.edt.mother_street_number,
                        mother_area_number: $scope.edt.mother_area_number,
                        mother_city: $scope.edt.mother_city,
                        mother_state: $scope.edt.mother_state,
                        mother_country_code: $scope.edt.mother_country_code,
                        mother_summary_address: $scope.edt.mother_summary_address,
                        mother_po_box: $scope.edt.mother_po_box,
                        mother_phone: $scope.edt.mother_phone,
                        mother_mobile: $scope.edt.mother_mobile,
                        mother_fax: $scope.edt.mother_fax,
                        mother_email: $scope.edt.mother_email,
                        mother_occupation: $scope.edt.mother_occupation,
                        mother_passport_number: $scope.edt.mother_passport_number,
                        //sims_parent_mother_img: sims_mother_image,
                        sims_parent_mother_img: $scope.edt.sims_parent_mother_img,
                        opr: 'U'
                    });
                    datasend.push(data);

                    console.log("post", data);

                    //$scope.test = [{section_code:'01'}];
                    $http.post(ENV.apiUrl + "api/ParentRegistration/CUDParent", datasend).then(function (msg) {

                        $scope.parentcode = msg.data;

                        if ($scope.parentcode.length != 0) {

                            if ($scope.parentcode[0].sims_parent_father_img != '') {

                                var request = {
                                    method: 'POST',
                                    url: ENV.apiUrl + '/api/file/upload?filename=' + $scope.parentcode[0].sims_parent_father_img.split('.')[0] + '&location=ParentImages',
                                    data: formdata,
                                    headers: {
                                        'Content-Type': undefined
                                    }
                                };

                                $http(request)
                                     .success(function (d) {
                                     }, function () {
                                         alert("Err");
                                     });
                            }

                            if ($scope.parentcode[0].sims_parent_mother_img != '') {
                                var request = {
                                    method: 'POST',
                                    url: ENV.apiUrl + '/api/file/upload?filename=' + $scope.parentcode[0].sims_parent_mother_img.split('.')[0] + '&location=ParentImages',
                                    data: formdatamother,
                                    headers: {
                                        'Content-Type': undefined
                                    }
                                };

                                $http(request)
                                     .success(function (d) {
                                     }, function () {
                                         alert("Err");
                                     });
                            }
                            if ($scope.parentcode[0].sims_parent_guardian_img != '') {
                                var request = {
                                    method: 'POST',
                                    url: ENV.apiUrl + '/api/file/upload?filename=' + $scope.parentcode[0].sims_parent_guardian_img.split('.')[0] + '&location=ParentImages',
                                    data: formdataguardian,
                                    headers: {
                                        'Content-Type': undefined
                                    }
                                };

                                $http(request)
                                     .success(function (d) {
                                     }, function () {
                                         alert("Err");
                                     });
                            }

                            swal({ text: "Parent Info Updated Successfully", imageUrl: "assets/img/check.png", width: 350, height: 200 });
                            $scope.Cancel();
                        }
                        else {
                            swal({ text: "Parent Info Not Updated. " + $scope.parentcode, imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                    });
                //}
            }

            $scope.chk_copy_mother_add = function () {
                if ($scope.edt.copy_add == true) {
                    debugger
                    $scope.edt.mother_appartment_number = $scope.edt.father_appartment_number;
                    $scope.edt.mother_area_number = $scope.edt.father_area_number;
                    $scope.edt.mother_building_number = $scope.edt.father_building_number;
                    $scope.edt.mother_street_number = $scope.edt.father_street_number;
                    $scope.edt.mother_city = $scope.edt.father_city;
                    $scope.edt.mother_state = $scope.edt.father_state;
                    $scope.edt.mother_country_code = $scope.edt.father_country_code;
                    $scope.edt.mother_po_box = $scope.edt.father_po_box;
                    if ($scope.edt.father_nationality1_code == 'undefined' || $scope.edt.father_nationality1_code == undefined || $scope.edt.father_nationality1_code == null || $scope.edt.father_nationality1_code == "") {
                       // $scope.edt.mother_nationality1_code = $scope.edt.father_nationality1_code;
                    } else {
                        $scope.edt.mother_nationality1_code = $scope.edt.father_nationality1_code;
                    }
                    if ($scope.edt.father_nationality2_code == 'undefined' || $scope.edt.father_nationality2_code == undefined || $scope.edt.father_nationality2_code == null || $scope.edt.father_nationality2_code == "") {
                        // $scope.edt.mother_nationality1_code = $scope.edt.father_nationality1_code;
                    } else {
                        $scope.edt.mother_nationality2_code = $scope.edt.father_nationality2_code;
                    }
                    
                    $scope.edt.mother_summary_address = $scope.edt.father_summary_address;
                    $scope.edt.mother_phone = $scope.edt.father_phone;
                    $scope.edt.mother_po_box = $scope.edt.father_po_box;
                    $scope.edt.mother_mobile = $scope.edt.father_mobile;
                    $scope.edt.mother_occupation = $scope.edt.father_occupation;
                    $scope.edt.mother_email = $scope.edt.father_email;
                    $scope.edt.mother_company = $scope.edt.father_company;
                }
                //else {
                //    $scope.edt.mother_appartment_number = "";
                //    $scope.edt.mother_area_number = "";
                //    $scope.edt.mother_building_number = "";
                //    $scope.edt.mother_street_number = "";
                //    $scope.edt.mother_city = "";
                //    $scope.edt.mother_state = "";
                //    $scope.edt.mother_country_code = "";
                //    $scope.edt.mother_po_box = "";
                //    $scope.edt.mother_nationality1_code = "";
                //    $scope.edt.mother_nationality2_code = "";
                //    $scope.edt.mother_summary_address = "";
                //    $scope.edt.mother_phone = "";
                //    $scope.edt.mother_po_box = "";
                //    $scope.edt.mother_mobile = "";
                //    $scope.edt.mother_occupation = "";
                //    $scope.edt.mother_email = "";
                //    $scope.edt.mother_company = "";
                //}

            }

            $scope.chk_copy_guar_add1 = function () {
                if ($scope.edt.copy_add1 == true) {

                    $scope.edt.guardian_appartment_number = $scope.edt.father_appartment_number;
                    $scope.edt.guardian_area_number = $scope.edt.father_area_number;
                    $scope.edt.guardian_building_number = $scope.edt.father_building_number;
                    $scope.edt.guardian_street_number = $scope.edt.father_street_number;
                    $scope.edt.guardian_city = $scope.edt.father_city;
                    $scope.edt.guardian_state = $scope.edt.father_state;
                    $scope.edt.guardian_country_code = $scope.edt.father_country_code;
                    $scope.edt.guardian_po_box = $scope.edt.father_po_box;
                    if ($scope.edt.father_nationality1_code == 'undefined' || $scope.edt.father_nationality1_code == undefined || $scope.edt.father_nationality1_code == null || $scope.edt.father_nationality1_code == "") {                        
                    } else {
                        $scope.edt.guardian_nationality1_code = $scope.edt.father_nationality1_code;
                    }
                    if ($scope.edt.father_nationality2_code == 'undefined' || $scope.edt.father_nationality2_code == undefined || $scope.edt.father_nationality2_code == null || $scope.edt.father_nationality2_code == "") {
                    } else {
                        $scope.edt.guardian_nationality2_code = $scope.edt.father_nationality2_code;
                    }
                    
                    $scope.edt.guardian_summary_address = $scope.edt.father_summary_address;
                    $scope.edt.guardian_phone = $scope.edt.father_phone;
                    $scope.edt.guardian_po_box = $scope.edt.father_po_box;
                    $scope.edt.guardian_mobile = $scope.edt.father_mobile;
                    $scope.edt.guardian_occupation = $scope.edt.father_occupation;
                    $scope.edt.guardian_email = $scope.edt.father_email;
                    $scope.edt.guardian_company = $scope.edt.father_company;
                }
                //else {
                //    $scope.edt.guardian_appartment_number = "";
                //    $scope.edt.guardian_area_number = "";
                //    $scope.edt.guardian_building_number = "";
                //    $scope.edt.guardian_street_number = "";
                //    $scope.edt.guardian_city = "";
                //    $scope.edt.guardian_state = "";
                //    $scope.edt.guardian_country_code = "";
                //    $scope.edt.guardian_po_box = "";
                //    $scope.edt.guardian_nationality1_code = "";
                //    $scope.edt.guardian_nationality2_code = "";
                //    $scope.edt.guardian_summary_address = "";
                //    $scope.edt.guardian_phone = "";
                //    $scope.edt.guardian_po_box = "";
                //    $scope.edt.guardian_mobile = "";
                //    $scope.edt.guardian_occupation = "";
                //    $scope.edt.guardian_email = "";
                //    $scope.edt.guardian_company = "";
                //}

            }

            $scope.GetFatherSumAddr = function () {
                var country = document.getElementById('cmb_father_country').options[document.getElementById('cmb_father_country').selectedIndex].text;

                var father_aprt = $("#txt_father_aprt").val().length;
                var father_build = $("#txt_father_build").val().length;
                var father_street = $("#txt_father_Street").val().length;
                var father_area = $("#txt_father_area").val().length;
                var father_city = $("#txt_father_city").val().length;
                var father_state = $("#txt_father_state").val().length;
                var father_country = $("#cmb_father_country").val().length;
                var father_POBox = $("#txt_father_pobox").val().length;

                document.getElementById('txt_father_summary_add').value = $("#txt_father_aprt").val().length > 0 ? document.getElementById('txt_father_aprt').value + "," : "";
                document.getElementById('txt_father_summary_add').value += $("#txt_father_build").val().length > 0 ? document.getElementById('txt_father_build').value + "," : "";
                document.getElementById('txt_father_summary_add').value += $("#txt_father_Street").val().length > 0 ? document.getElementById('txt_father_Street').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_area > 0 ? document.getElementById('txt_father_area').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_city > 0 ? document.getElementById('txt_father_city').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_state > 0 ? document.getElementById('txt_father_state').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_country > 0 ? country + "," : "";
                document.getElementById('txt_father_summary_add').value += father_POBox > 0 ? document.getElementById('txt_father_pobox').value : "";
            }

            $scope.GetMotherSumAddr = function () {
                var mother_aprt = $("#txt_mother_aprt").val().length;
                var mother_build = $("#txt_mother_build").val().length;
                var mother_street = $("#txt_mother_street").val().length;
                var mother_area = $("#txt_mother_area").val().length;
                var mother_city = $("#txt_mother_city").val().length;
                var mother_state = $("#txt_mother_state").val().length;
                var mother_country = $("#cmb_mother_country").val().length;
                var mother_POBox = $("#txt_mother_pobox").val().length;

                var country = document.getElementById('cmb_mother_country').options[document.getElementById('cmb_mother_country').selectedIndex].text;

                document.getElementById('txt_mother_addr').value = mother_aprt > 0 ? document.getElementById('txt_mother_aprt').value + "," : "";
                document.getElementById('txt_mother_addr').value += mother_build > 0 ? document.getElementById('txt_mother_build').value + "," : "";
                document.getElementById('txt_mother_addr').value += mother_street > 0 ? document.getElementById('txt_mother_street').value + "," : "";
                document.getElementById('txt_mother_addr').value += mother_area > 0 ? document.getElementById('txt_mother_area').value + "," : "";
                document.getElementById('txt_mother_addr').value += mother_city > 0 ? document.getElementById('txt_mother_city').value + "," : "";
                document.getElementById('txt_mother_addr').value += mother_state > 0 ? document.getElementById('txt_mother_state').value + "," : "";
                document.getElementById('txt_mother_addr').value += mother_country > 0 ? country + "," : "";
                document.getElementById('txt_mother_addr').value += mother_POBox > 0 ? document.getElementById('txt_mother_pobox').value : "";
            }

            $scope.GetGuardianSumAddr = function () {
                var guar_aprt = $("#txt_guar_aprt").val().length;
                var guar_build = $("#txt_guar_build").val().length;
                var guar_street = $("#txt_guar_street").val().length;
                var guar_area = $("#txt_guar_area").val().length;
                var guar_city = $("#txt_guar_city").val().length;
                var guar_state = $("#txt_guar_state").val().length;
                var guar_country = $("#cmb_gar_country").val().length;
                var guar_POBox = $("#txt_guar_pobox").val().length;

                var country = document.getElementById('cmb_gar_country').options[document.getElementById('cmb_gar_country').selectedIndex].text;

                document.getElementById('txt_guar_addr').value = guar_aprt > 0 ? document.getElementById('txt_guar_aprt').value + "," : "";
                document.getElementById('txt_guar_addr').value += guar_build > 0 ? document.getElementById('txt_guar_build').value + "," : "";
                document.getElementById('txt_guar_addr').value += guar_street > 0 ? document.getElementById('txt_guar_street').value + "," : "";
                document.getElementById('txt_guar_addr').value += guar_area > 0 ? document.getElementById('txt_guar_area').value + "," : "";
                document.getElementById('txt_guar_addr').value += guar_city > 0 ? document.getElementById('txt_guar_city').value + "," : "";
                document.getElementById('txt_guar_addr').value += guar_state > 0 ? document.getElementById('txt_guar_state').value + "," : "";
                document.getElementById('txt_guar_addr').value += guar_country > 0 ? country + "," : "";
                document.getElementById('txt_guar_addr').value += guar_POBox > 0 ? document.getElementById('txt_guar_pobox').value : "";
            }


            $http.get(ENV.apiUrl + "api/ParentRegistration/getAdmissionList").then(function (res) {
                $scope.obj = res.data;                
            });

            $scope.ChkDate1 = function (noissueafter) {

                //var date = new Date();
                //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy/MM/dd');
                //var year1 = noissueafter.split("-")[0];
                //var month1 = noissueafter.split("-")[1];
                //var day1 = noissueafter.split("-")[2];
                //var new_end_date = year1 + "-" + month1 + "-" + day1;
                var new_end_date = noissueafter;

                if (new_end_date < $scope.edt.sims_parent_father_passport_issue_date) {
                    //$rootScope.strMessage = "Please Select Today or Next Date";
                    //$('#message').modal('show');
                    swal({ title: "Alert", text: "Please Select Future Date", showCloseButton: true, width: 380, });
                    $scope.edt.sims_parent_father_passport_expiry_date = "";
                }

            }

            $scope.ChkDate2 = function (noissueafter) {

                //var date = new Date();
                //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy/MM/dd');
                //var year1 = noissueafter.split("-")[0];
                //var month1 = noissueafter.split("-")[1];
                //var day1 = noissueafter.split("-")[2];
                //var new_end_date = year1 + "-" + month1 + "-" + day1;
                var new_end_date = noissueafter;

                if (new_end_date < $scope.edt.sims_parent_father_national_id_issue_date) {
                    //$rootScope.strMessage = "Please Select Today or Next Date";
                    //$('#message').modal('show');
                    swal({ title: "Alert", text: "Please Select Future Date", showCloseButton: true, width: 380, });
                    $scope.edt.sims_parent_father_national_id_expiry_date = "";
                }

            }

            $scope.ChkDate3 = function (noissueafter) {

                //var date = new Date();
                //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy/MM/dd');
                //var year1 = noissueafter.split("-")[0];
                //var month1 = noissueafter.split("-")[1];
                //var day1 = noissueafter.split("-")[2];
                //var new_end_date = year1 + "-" + month1 + "-" + day1;
                var new_end_date = noissueafter;

                if (new_end_date < $scope.edt.sims_parent_mother_passport_issue_date) {
                    //$rootScope.strMessage = "Please Select Today or Next Date";
                    //$('#message').modal('show');
                    swal({ title: "Alert", text: "Please Select Future Date", showCloseButton: true, width: 380, });
                    $scope.edt.sims_parent_mother_passport_expiry_date = "";
                }

            }

            $scope.ChkDate4 = function (noissueafter) {

                //var date = new Date();
                //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy/MM/dd');
                //var year1 = noissueafter.split("-")[0];
                //var month1 = noissueafter.split("-")[1];
                //var day1 = noissueafter.split("-")[2];
                //var new_end_date = year1 + "-" + month1 + "-" + day1;
                var new_end_date = noissueafter;

                if (new_end_date < $scope.edt.sims_parent_mother_national_id_issue_date) {
                    //$rootScope.strMessage = "Please Select Today or Next Date";
                    //$('#message').modal('show');
                    swal({ title: "Alert", text: "Please Select Future Date", showCloseButton: true, width: 380, });
                    $scope.edt.sims_parent_mother_national_id_expiry_date = "";
                }

            }

            $scope.ChkDate5 = function (noissueafter) {

                //var date = new Date();
                //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy/MM/dd');
                //var year1 = noissueafter.split("-")[0];
                //var month1 = noissueafter.split("-")[1];
                //var day1 = noissueafter.split("-")[2];
                //var new_end_date = year1 + "-" + month1 + "-" + day1;
                var new_end_date = noissueafter;

                if (new_end_date < $scope.edt.sims_parent_gua_passport_issue_date) {
                    //$rootScope.strMessage = "Please Select Today or Next Date";
                    //$('#message').modal('show');
                    swal({ title: "Alert", text: "Please Select Future Date", showCloseButton: true, width: 380, });
                    $scope.edt.sims_parent_gua_passport_expiry_date = "";
                }

            }

            $scope.ChkDate6 = function (noissueafter) {

                //var date = new Date();
                //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy/MM/dd');
                //var year1 = noissueafter.split("-")[0];
                //var month1 = noissueafter.split("-")[1];
                //var day1 = noissueafter.split("-")[2];
                //var new_end_date = year1 + "-" + month1 + "-" + day1;
                var new_end_date = noissueafter;

                if (new_end_date < $scope.edt.sims_parent_gua_national_id_issue_date) {
                    //$rootScope.strMessage = "Please Select Today or Next Date";
                    //$('#message').modal('show');
                    swal({ title: "Alert", text: "Please Select Future Date", showCloseButton: true, width: 380, });
                    $scope.edt.sims_parent_gua_national_id_expiry_date = "";
                }

            }



            //$(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
            //    $('input[type="text"]', $(this).parent()).focus();
            //});

            //$('*[data-datepicker="true"] input[type="text"]').datepicker({
            //    todayBtn: true,
            //    orientation: "top left",
            //    autoclose: true,
            //    todayHighlight: true,
            //    format: "yyyy-mm-dd"
            //});



        }])

})();
