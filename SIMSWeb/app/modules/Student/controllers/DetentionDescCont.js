﻿(function () {
    'use strict';
    var obj1, temp, opr, comp, str;
    var main, deletefin = [];
    var report_card_code;
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('DetentionDescCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {


            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            //Select Data SHOW



            //Bind Combo
            $scope.temp = {};
            $http.get(ENV.apiUrl + "api/Student/getDetentionLevelName").then(function (getLevel) {
                $scope.Level = getLevel.data;
                //if ($scope.Level.length > 0)
                //{
                //    $scope.temp.sims_detention_level_code = $scope.Level[0].sims_detention_level_code;
                //}
                //console.log($scope.Level);
            });

            $http.get(ENV.apiUrl + "api/Student/getDetentionDesc").then(function (res1) {
                
                $scope.DetentionDesc = res1.data;
                $scope.CountAll = $scope.DetentionDesc.length;
                $scope.totalItems = $scope.DetentionDesc.length;
                $scope.todos = $scope.DetentionDesc;
                $scope.makeTodos();

            });

            $scope.GetCombo = function () {
                $http.get(ENV.apiUrl + "api/Student/getDetentionLevelName").then(function (res) {
                    $scope.levelNameData = res.data;
                });
            }

     
            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.DetentionDesc;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                //$scope.CheckAllChecked();
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.DetentionDesc, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.DetentionDesc;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_section_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_report_card_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_level_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_report_card_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.term_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_fee_amount == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
                debugger;
                $scope.disabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = [];        
                $scope.LevelReadonly = false;
                $scope.temp = {
                    //declaredate: yyyy + '-' + mm + '-' + dd,
                    sims_detention_desc_created_date: dd + '-' + mm + '-' + yyyy,
                    sims_detention_desc_status: true,
                    username: "Admin",
                }
                
                debugger;
                //curriculum
                //$http.get(ENV.apiUrl + "api/Student/getDetentionLevelName").then(function (getDetentionLevelName) {
                //    $scope.Level = getDetentionLevelName.data;
                //    if ($scope.Level.length > 0) {
                //        $scope.temp.sims_detention_level_code = $scope.Level[0].sims_detention_level_code;
                //    }

                //});

                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            $scope.myFunct = function (keyEvent) {
                debugger;
                if (keyEvent.which == 13)
                    //$scope.temp.declaredate = yyyy + '-' + mm + '-' + dd;
                    $scope.temp.declaredate = dd + '-' + mm + '-' + yyyy;
            }

            $scope.myFunct1 = function (keyEvent) {
                debugger;
                if (keyEvent.which == 13)
                    //$scope.temp.publishdate = yyyy + '-' + mm + '-' + dd;
                    $scope.temp.publishdate = dd + '-' + mm + '-' + yyyy;
            }


            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = [];
                $scope.table = true;
                $scope.display = false;
                $scope.edt = [];
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;     
                $scope.Update_btn = true;
                $scope.LevelReadonly = true;
                //$http.get(ENV.apiUrl + "api/Student/getDetentionLevelName").then(function (getDetentionLevelName) {
                //    $scope.Level = getDetentionLevelName.data;
                //    if ($scope.Level.length > 0) {
                //        $scope.temp.sims_detention_level_code = $scope.Level[0].sims_detention_level_code;
                //    }
                //});
                //$scope.temp.sims_detention_level_code = str.sims_detention_level_code;
                $scope.temp = {
                    sims_detention_desc_status: str.sims_detention_desc_status,
                    sims_detention_desc_code: str.sims_detention_desc_code,
                    username: str.username,
                    sims_detention_description_ot: str.sims_detention_description_ot,
                    sims_detention_description: str.sims_detention_description,
                    sims_detention_desc_created_date: str.sims_detention_desc_created_date,
                    sims_display_order: str.sims_display_order,
                    sims_detention_level_code: str.sims_detention_level_code
                };
            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger;

                if (Myform) {

                    datasend = [];
                    data = [];

                    var data = {
                        sims_detention_level_code: $scope.temp.sims_detention_level_code,
                        sims_detention_desc_code: $scope.temp.sims_detention_desc_code,
                        sims_detention_description: $scope.temp.sims_detention_description,
                        sims_detention_description_ot: $scope.temp.sims_detention_description_ot,
                        sims_display_order: $scope.temp.sims_display_order,
                        sims_detention_desc_status: $scope.temp.sims_detention_desc_status,
                        sims_detention_desc_created_user_code: $scope.temp.sims_detention_desc_created_user_code,
                        sims_detention_desc_created_date: $scope.temp.sims_detention_desc_created_date,
                       
                    }
                    data.opr = 'I';
                    datasend.push(data);

                    $http.post(ENV.apiUrl + "api/Student/InsertUpdateDeleteDetentionDesc", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 380 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 380 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $scope.getgrid();
                    });
                    $scope.table = true;
                    $scope.display = false;
                }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                debugger;
                $scope.flag1 = false;
                if (Myform) {
           

                    dataforUpdate = [];

                    var data = {
                        sims_detention_level_code: $scope.temp.sims_detention_level_code,
                        sims_detention_desc_code: $scope.temp.sims_detention_desc_code,
                        sims_detention_description: $scope.temp.sims_detention_description,
                        sims_detention_description_ot: $scope.temp.sims_detention_description_ot,
                        sims_display_order: $scope.temp.sims_display_order,
                        sims_detention_desc_status: $scope.temp.sims_detention_desc_status,
                        sims_detention_desc_created_user_code: $scope.temp.sims_detention_desc_created_user_code,
                        sims_detention_desc_created_date: $scope.temp.sims_detention_desc_created_date,
                    }

                    data.opr = "U";

                    dataforUpdate.push(data);

                    $http.post(ENV.apiUrl + "api/Student/InsertUpdateDeleteDetentionDesc", dataforUpdate).then(function (msg) {
                        debugger;
                        $scope.updateResult = msg.data;
                        if ($scope.updateResult == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 380 });
                        }
                        else if ($scope.updateResult == false) {
                            swal({ text: "Record Not Updated.", imageUrl: "assets/img/close.png", width: 380 });
                        }
                        else {
                            swal("Error-" + $scope.updateResult)
                        }
                        $scope.getgrid();
                    });

                    $scope.table = true;
                    $scope.display = false;
                }
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_detention_level_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_detention_level_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/Student/getDetentionDesc").then(function (res1) {

                    $scope.DetentionDesc = res1.data;
                    $scope.totalItems = $scope.DetentionDesc.length;
                    $scope.todos = $scope.DetentionDesc;
                    $scope.makeTodos();

                });
            }

            $scope.Delete = function () {
                debugger;
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    //var t = $scope.filteredTodos[i].uom_code;
                    var v = document.getElementById('test_' + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sims_detention_level_code': $scope.filteredTodos[i].sims_detention_level_code,
                            'sims_detention_desc_code': $scope.filteredTodos[i].sims_detention_desc_code,
                            'opr': 'D',

                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {

                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/Student/InsertUpdateDeleteDetentionDesc", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;



                                        }
                                    });
                                }
                                else {
                                    swal({ text: "Record Already Mapped.Can't be Deleted. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }

                            });
                        }
                        else {

                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_report_card_name + i);
                                //var t = $scope.filteredTodos[i].sims_fee_category;
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");


                                }


                            }

                        }

                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                }

                $scope.currentPage = str;
            }

            $(function () {
                $('#app_on1').multipleSelect({
                    width: '100%'
                });
            });



           

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


         }])

})();
