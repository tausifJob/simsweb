﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, modulecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AdmissionEmailHistoryCont',
         ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {

             $timeout(function () {
                 $("#example").tableHeadFixer({ 'top': 1 });
             }, 100);

             $scope.pagesize = "10";
             $scope.pageindex = 0;
             $scope.busyindicator = false;
             $scope.ShowCheckBoxes = true;

             $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');

             function getCur(flag, comp_code) {
                 if (flag) {

                     $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                         $scope.curriculum = res.data;
                         $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                         $scope.getAccYear($scope.edt.sims_cur_code);

                     });
                 }

                 else {

                     $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                         $scope.curriculum = res.data;
                         $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                         $scope.getAccYear($scope.edt.sims_cur_code);

                     });
                 }

             }

             $scope.getuser = function () {
                 $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                     $scope.global_count_comp = res.data;

                     if ($scope.global_count_comp) {
                         getCur(true, $scope.user_details.comp);


                     }
                     else {
                         getCur(false, $scope.user_details.comp)
                     }
                 });
             }

             $scope.getuser();

             $scope.getAccYear = function (curCode) {
                 $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                     $scope.Acc_year = Acyear.data;
                     $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                     $scope.getGrade(curCode, $scope.temp.sims_academic_year);
                 });
             }

             $scope.GetGrade = function () {
                 $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                     $scope.Grade_code = Gradecode.data;
                     setTimeout(function () {
                         $('#cmb_grade_code').change(function () {
                             console.log($(this).val());
                         }).multipleSelect({
                             width: '100%'
                         });
                     }, 1000);

                 });
             }

             $scope.getGrade = function (curCode, accYear) {
                 $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                     $scope.Grade_code = Gradecode.data;
                     setTimeout(function () {
                         $('#cmb_grade_code').change(function () {
                             console.log($(this).val());
                         }).multipleSelect({
                             width: '100%'
                         });
                     }, 1000);

                 });
             }

             $(function () {
                 $('#cmb_grade_code').multipleSelect({
                     width: '100%'
                 });
             });

             $scope.getSection = function (curCode, gradeCode, accYear) {
                 $http.get(ENV.apiUrl + "api/StudentReport/getAdmissionTemplateFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                     $scope.Section_code = Sectioncode.data;
                     setTimeout(function () {
                         $('#cmb_section_code').change(function () {
                             console.log($(this).val());
                         }).multipleSelect({
                             width: '100%'
                         });
                     }, 1000);

                 });
             }

             $(function () {
                 $('#cmb_section_code').multipleSelect({
                     width: '100%'
                 });
             });

             $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

             $scope.size = function (str) {
                 //$scope.pagesize = str;
                 //$scope.currentPage = 1;
                 //$scope.numPerPage = str;
                 //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();

                 main = document.getElementById('mainchk');
                 if (main.checked == true) {
                     main.checked = false;
                     $scope.row1 = '';
                     $scope.color = '#edefef';
                 }

                 if (str == "All") {
                     $scope.currentPage = 1;
                     $scope.numPerPage = $scope.studlist.length;
                     $scope.makeTodos();

                 }
                 else {
                     $scope.pagesize = str;
                     $scope.currentPage = 1;
                     $scope.numPerPage = str;
                     $scope.makeTodos();
                 }
             }

             $scope.index = function (str) {
                 $scope.pageindex = str;
                 $scope.currentPage = str;
                 $scope.makeTodos();

                 main = document.getElementById('mainchk');
                 if (main.checked == true) {
                     main.checked = false;
                     $scope.row1 = '';
                     $scope.color = '#edefef';
                 }
             }

             $scope.makeTodos = function () {

                 var rem = parseInt($scope.totalItems % $scope.numPerPage);
                 if (rem == '0') {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                 }
                 else {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                 }

                 var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                 var end = parseInt(begin) + parseInt($scope.numPerPage);

                 console.log("begin=" + begin); console.log("end=" + end);

                 $scope.filteredTodos = $scope.todos.slice(begin, end);
             };

             $scope.Show_Data = function (str, str1) {
                 if (str != undefined && str1 != undefined) {

                     $http.get(ENV.apiUrl + "api/StudentReport/getAdmissionEmailSchedule?cur_name=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grd_code=" + $scope.edt.sims_grade_code + "&email_code=" + $scope.edt.sims_msg_subject + "&from_date=" + $scope.dt.sims_from_date + "&to_date=" + $scope.dt.sims_to_date).then(function (res) {
                         $scope.studlist = res.data;
                         $scope.totalItems = $scope.studlist.length;
                         $scope.todos = $scope.studlist;
                         $scope.makeTodos();
                         if ($scope.studlist.length == 0) {
                             swal({ title: 'Alert', text: "Record Not Found...", showCloseButton: true, width: 450, height: 200 });

                         }
                         else {
                             $scope.table = true;
                             $scope.busyindicator = false;
                         }
                     })

                 }
                 else {
                     swal({ title: 'Alert', text: "Please Select Curriculum & Academic Year Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                 }
             }

             $scope.Resend = function (tre) {
                 var Savedata = [];
                 var Save_sa = ({
                     'cur_shrt_name': $scope.edt.sims_cur_code,
                     'sims_academic_year': $scope.temp.sims_academic_year,
                     'sims_grade_code': $scope.edt.sims_grade_code,
                     'sims_student_attribute1': tre.sims_student_attribute1,
                     'sims_student_attribute3': tre.sims_student_attribute3,
                     'sims_student_attribute5': tre.sims_student_attribute5,
                     'sims_MOE_Number': tre.sims_MOE_Number,
                     'sims_student_attribute4': tre.sims_student_attribute4,
                     'sims_parent_father_passport_expiry_date': tre.sims_parent_father_passport_expiry_date
                 });
                 Savedata.push(Save_sa);

                 $http.post(ENV.apiUrl + "api/StudentReport/CUDAdmissionEmailSchedule", Savedata).then(function (msg) {
                     $scope.msg1 = msg.data;
                     if ($scope.msg1.strMessage != undefined) {
                         if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                             swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 380, });
                         }
                     }
                     else {
                         swal({ title: "Alert", text: "Record Not Inserted...", showCloseButton: true, width: 380, });
                     }
                 });
             }

             $scope.PrintEmail = function (std) {
                 $scope.t = 'Sims.EmailPrint';
                 var data = {
                     location: $scope.t,
                     parameter: {
                         parent_no: std.sims_student_attribute4,
                         sub: std.sims_MOE_Number,
                     },
                     state: 'main.SimAEH'
                 }
                 window.localStorage["ReportDetails"] = JSON.stringify(data)
                 $state.go('main.ReportCardParameter')
             }

             $scope.back = function () {
                 debugger;
                 $scope.table = false;
                 $scope.busyindicator = false;
                 $scope.ShowCheckBoxes = true;
             }

             $scope.Reset = function () {
                 $scope.select_all = false;
                 $scope.edt = {
                     sims_cur_code: '',
                     sims_grade_code: '',
                     sims_section_code: '',
                 }
                 $scope.temp = {
                     sims_academic_year: '',
                 }
                 $scope.table = false;
                 $scope.busyindicator = false;
                 $scope.getuser();

                 $scope.dt = {
                     'sims_from_date': $scope.ddMMyyyy,
                     'sims_to_date': $scope.ddMMyyyy,
                 }
                 $("#cmb_grade_code").select2("val", "");
                 $("#cmb_section_code").select2("val", "");
             }

             $scope.dt = {
                 'sims_from_date': $scope.ddMMyyyy,
                 'sims_to_date': $scope.ddMMyyyy,
             }

             $scope.searched = function (valLists, toSearch) {
                 return _.filter(valLists,

                 function (i) {
                     /* Search Text in all  fields */
                     return searchUtil(i, toSearch);
                 });
             };

             $scope.search = function () {
                 $scope.todos = $scope.searched($scope.studlist, $scope.searchText);
                 $scope.totalItems = $scope.todos.length;
                 $scope.currentPage = '1';
                 if ($scope.searchText == '') {
                     $scope.todos = $scope.obj;
                 }
                 $scope.makeTodos();
             }

             function searchUtil(item, toSearch) {
                 return (item.sims_student_attribute1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.father_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                     || item.student_name_.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_parent_father_email.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                     || item.sims_parent_father_passport_expiry_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
             }

          }])
})();