﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, savefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

    simsController.controller('SurveyQuestionsCont',
   ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

       $scope.pagesize = "5";
       $scope.pageindex = 0;
       $scope.save_btn = true;
       $scope.pager = false;
       $scope.pageno = false;
       var dataforSave = [];
       $scope.display = false;
       $scope.table = true;
       $scope.searchtable = false;
       $scope.ans = false;
       $scope.anstable = false;
       $scope.ansname = false;       

       var user = $rootScope.globals.currentUser.username;

       //var dt = new Date();
       //$scope.sims_library_joining_date = dt.getDate() + "-" + (dt.getMonth() + 1) + "-" + dt.getFullYear();

       $timeout(function () {
           $("#fixTable").tableHeadFixer({ 'top': 1 });
       }, 100);

       $('*[data-datepicker="true"] input[type="text"]').datepicker({
           todayBtn: true,
           orientation: "top left",
           autoclose: true,
           todayHighlight: true
       });

       $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
           $('input[type="text"]', $(this).parent()).focus();
       });

       
       //function shuffle(array) {
       //    var currentIndex = array.length, temporaryValue, randomIndex;
       //    debugger;
       //    // While there remain elements to shuffle...
       //    while (0 !== currentIndex) {
       //        debugger;
       //        // Pick a remaining element...
       //        randomIndex = Math.floor(Math.random() * currentIndex);
       //        currentIndex -= 1;

       //        // And swap it with the current element.
       //        temporaryValue = array[currentIndex];
       //        array[currentIndex] = array[randomIndex];
       //        array[randomIndex] = temporaryValue;
       //    }

       //    return array;           
       //}

       $http.get(ENV.apiUrl + "api/SurveyQuestionAnswer/getSurveyQuestionType").then(function (question) {
           debugger;
           
           $scope.ques_data = question.data;          
           //shuffle($scope.ques_data);           
           //console.log($scope.ques_data);
       });

       $http.get(ENV.apiUrl + "api/SurveyQuestionAnswer/getSurveyQuestionRateGroup").then(function (RateGroup) {
           $scope.rate_data = RateGroup.data;           
       });

       $scope.queschange = function (str) {
           debugger;
           $scope.Answerdata = [];
           $scope.ansname = true;
           if (str.sims_survey_question_type == '1') {
               $scope.ans = true;
               //$scope.anstable = true;
           }
           else {
               $scope.ans = false;
               $scope.anstable = false;
           }
           if (str.sims_survey_question_type == '4') {
               $scope.ans1 = true;
               $scope.anstable1 = true;
           }
           else {
               $scope.ans1 = false;
               $scope.anstable1 = false;
           }
       }

       $scope.ratechange = function (str) {
          
           $scope.ansname = true;
           $http.get(ENV.apiUrl + "api/SurveyQuestionAnswer/getSurveyQuestionRateDetails?group_code=" + str.sims_survey_rating_group_code).then(function (ratingdetails) {
               $scope.Rating = ratingdetails.data;
           });
       }

       $scope.checkonebyonedelete = function () {

           $("input[type='checkbox']").change(function (e) {
               if ($(this).is(":checked")) { //If the checkbox is checked
                   $(this).closest('tr').addClass("row_selected");
                   $scope.color = '#edefef';
               }
               else {
                   $(this).closest('tr').removeClass("row_selected");

                   $scope.color = '#edefef';
               }
           });

           main = document.getElementById('mainchk');
           if (main.checked == true) {
               main.checked = false;
               $("input[type='checkbox']").change(function (e) {
                   if ($(this).is(":checked")) {
                       $(this).closest('tr').addClass("row_selected");
                   }
                   else {
                       $(this).closest('tr').removeClass("row_selected");
                   }
               });
           }
       }

       $scope.RemoveAnswer = function ($event, index, str) {
           str.splice(index, 1);
       }

       //Add Answers
       $scope.Answerdata = [];
       $scope.AddAnswers = function () {
           debugger;
           if ($scope.temp.sims_survey_question_answer_desc_en == undefined || $scope.temp.sims_survey_question_answer_desc_en == '') {
               return;
           }
           $scope.anstable = true;
           var v = {
               'sims_survey_question_answer_desc_en':$scope.temp.sims_survey_question_answer_desc_en
           }

           $scope.Answerdata.push(v);
           v = '';
           $scope.temp.sims_survey_question_answer_desc_en = '';
       }

       $scope.objective_ans = function (event) {
           debugger;
           if (event.key == "Enter") {
               $scope.AddAnswers();
           }
       }
              
       //Form Close
       $scope.close = function () {
           $scope.table = true;
           $scope.display = false;
           $scope.grid2 = false;
           $scope.grid1 = false;
           $scope.sims_library_renewed_date = '';
           // $scope.save_btn = false;
       }

       $scope.Save = function (Myform) {
           debugger;
           savefin = [];
           var flag = false;           
           if (Myform) {
               //Objective Question Type
               if ($scope.temp.sims_survey_question_type == '1') {
                   for (var i = 0; i < $scope.Answerdata.length; i++) {
                       var v = document.getElementById($scope.Answerdata[i].sims_survey_question_answer_desc_en + i);
                       if (v.checked == true) {
                           var modulecode = ({
                               'sims_survey_question_type': $scope.temp.sims_survey_question_type,
                               'sims_survey_question_desc_en': $scope.temp.sims_survey_question_desc_en,
                               'sims_survey_question_answer_desc_en': $scope.Answerdata[i].sims_survey_question_answer_desc_en,
                               'sims_survey_question_correct_answer_flag': true
                           });
                       }
                       else {
                           var modulecode = ({
                               'sims_survey_question_type': $scope.temp.sims_survey_question_type,
                               'sims_survey_question_desc_en': $scope.temp.sims_survey_question_desc_en,
                               'sims_survey_question_answer_desc_en': $scope.Answerdata[i].sims_survey_question_answer_desc_en,
                               'sims_survey_question_correct_answer_flag': false
                           });
                       }
                       flag = true;
                       savefin.push(modulecode);
                   }
               }

               //Subjective Question Type
               if ($scope.temp.sims_survey_question_type == '2') {
                   var datacode = ({
                       'sims_survey_question_type': $scope.temp.sims_survey_question_type,
                       'sims_survey_question_desc_en': $scope.temp.sims_survey_question_desc_en,
                       'sims_survey_question_answer_desc_en': $scope.temp.sims_survey_question_answer_desc_en,
                   });
                   flag = true;
                   savefin.push(datacode);
               }

               //True/False Question Type
               if ($scope.temp.sims_survey_question_type == '3') {
                   var a = ({
                       'sims_survey_question_type': $scope.temp.sims_survey_question_type,
                       'sims_survey_question_desc_en': $scope.temp.sims_survey_question_desc_en,
                       'sims_survey_question_answer_desc_en': 'True',
                   });
                   savefin.push(a);
                   var b = ({
                       'sims_survey_question_type': $scope.temp.sims_survey_question_type,
                       'sims_survey_question_desc_en': $scope.temp.sims_survey_question_desc_en,
                       'sims_survey_question_answer_desc_en': 'False',
                   });
                   flag = true;
                   savefin.push(b);
               }

               //Rating Question Type
               if ($scope.temp.sims_survey_question_type == '4') {
                   for (var i = 0; i < $scope.Rating.length; i++) {
                       var code = ({
                           'sims_survey_question_type': $scope.temp.sims_survey_question_type,
                           'sims_survey_question_desc_en': $scope.temp.sims_survey_question_desc_en,
                           //'sims_survey_rating_name': $scope.Rating[i].sims_survey_rating_name,
                           'sims_survey_question_answer_desc_en': $scope.Rating[i].sims_survey_rating_desc,
                       });
                       flag = true;
                       savefin.push(code);
                   }
               }

               $http.post(ENV.apiUrl + "api/SurveyQuestionAnswer/CUDSurveyQuestionAnswer", savefin).then(function (savedata) {
                   $scope.msg1 = savedata.data;
                   if ($scope.msg1 == true) {
                       swal({ text: "Question and Answer Saved Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                       savefin = [];
                   }
                   else {
                       swal({ text: "Sorry! Question and Answer Not Saved", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                   }
               });
               savefin = [];
               $('tr').removeClass("row_selected");
               main.checked = false;
               $scope.ansname = false;
               $scope.Reset();
           }
       }

       //Cancel Button
       $scope.Reset = function () {
           $scope.temp = {};
           $scope.Answerdata = [];
           $scope.ans = false;
           $scope.anstable = false;
           $scope.ansname = false;
           }
    }])
})();