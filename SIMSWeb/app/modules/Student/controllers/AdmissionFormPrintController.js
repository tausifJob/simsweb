﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AdmissionFormPrintController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
          // Pre-Required Functions and Variables
            $scope.showEmpty = true;
            $scope.search = '';
            /* PAGER FUNCTIONS*/
            $http.get(ENV.apiUrl + "api/common/Admission?search="+$scope.search+"&flag=Y").then(function (admissiondata) {
                $scope.admissiondata = admissiondata.data;
            });

        //Events End
         }])
})();

