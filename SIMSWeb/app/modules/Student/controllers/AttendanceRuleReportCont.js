﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AttendanceRuleReportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            // Start
            //$scope.name = "Subject Teacher List";
            $scope.temp = {};


            $scope.getAllGrades = function (cur_code, academic_year) {
                $http.get(ENV.apiUrl + "api/Report/SubjectTeacherList/getAllGrades?cur_code=" + cur_code + "&academic_year=" + academic_year).then(function (res1) {
                    $scope.grade_code = res1.data;
                    setTimeout(function () {
                        $('#grade_box').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                        $("#grade_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }

            $scope.getAccYear = function (cur_code) {
                $http.get(ENV.apiUrl + "api/Report/SubjectTeacherList/getAcademicYear?curCode=" + cur_code).then(function (res1) {
                    $scope.academic_year = res1.data;
                    if (res1.data.length > 0) {
                        $scope.temp['sims_academic_year'] = res1.data[0].sims_academic_year;
                        $scope.getAllGrades(cur_code, res1.data[0].sims_academic_year)
                    }
                });
            }

            $http.get(ENV.apiUrl + "api/Report/SubjectTeacherList/getCuriculum").then(function (res1) {
                $scope.sims_cur = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_cur_code'] = res1.data[0].sims_cur_code;

                    $scope.getAccYear(res1.data[0].sims_cur_code);
                }
            });

            $(function () {
                $('#grade_box').multipleSelect({ width: '100%' });
                $('#section_box').multipleSelect({ width: '100%' });
                $('#rule_box').multipleSelect({ width: '100%' });

            });


            $scope.getSectionFromGrade = function (cur_code, academic_year, grade_code) {
                $http.get(ENV.apiUrl + "api/Report/SubjectTeacherList/getSectionFromGrade?cur_code=" + cur_code + "&academic_year=" + academic_year + "&grade_code=" + grade_code).then(function (res1) {
                    $scope.section_code = res1.data;

                    setTimeout(function () {
                        $('#section_box').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                        $("#section_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }


            $http.get(ENV.apiUrl + "api/Report/SubjectTeacherList/getAttendanceRule").then(function (res1) {
                $scope.rule_type = res1.data;
                console.log($scope.rule_type)
                setTimeout(function () {
                    $('#rule_box').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                    $("#rule_box").multipleSelect("checkAll");
                }, 1000);
            });

            $scope.AttendanceRuleReport = function () {
                $scope.colsvis = false;
                $http.get(ENV.apiUrl + "api/Report/SubjectTeacherList/getAttendanceRule_Rpt?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&rule_Code=" + $scope.temp.sims_attendance_rule_code).then(function (res1) {
                    $scope.report_data = res1.data;
                    //angular.forEach($scope.report_data, function (f) {
                    //    f.student_cnt = parseFloat(f.student_cnt);
                    //});
                    //angular.forEach($scope.report_data, function (f) {
                    //    f.sims_bell_lecture_per_week = parseFloat(f.sims_bell_lecture_per_week);
                    //});
                    console.log($scope.report_data);
                });
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }




            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.colsvis = false;


            $scope.exportData = function () {
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('rpt_data').innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        saveAs(blob, "AttendanceRuleReport.xls");
                        $scope.colsvis = false;

                    }

                });
            };

         }])

})();

