﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CancelAdmissionApprovalCont',
           ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {
               $scope.searchResult = [];
               console.clear();
               $scope.edts = {};
               $scope.pagesize = "5";
               $scope.pageindex = "1";
               $scope.month_view = true;
               $scope.table1 = false;
               // $scope.global_Search_click1 = function () {
               $rootScope.visible_stud = true;
               //$rootScope.visible_parent = false;
               //$rootScope.visible_search_parent = false;
               //$rootScope.visible_teacher = false;
               //$rootScope.visible_User = true;
               //$rootScope.visible_Employee = false;
               $rootScope.chkMulti = false;
               $scope.username = $rootScope.globals.currentUser.username;

               var dt = new Date();
               //var date = dt.getDate() + "-" + (dt.getMonth() + 1) + "-" + dt.getFullYear();
               var date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
               $timeout(function () {
                   $("#table").tableHeadFixer({ 'top': 1 });
               }, 100);

               $timeout(function () {
                   $("#fixTable").tableHeadFixer({ 'top': 1 });
               }, 100);

               //var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
               var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');
               $scope.dt = {
                   sims_tc_certificate_request_date: dateyear,
               }

               $scope.size = function (str) {
                   $scope.pagesize = str;
                   $scope.currentPage = 1;
                   $scope.numPerPage = str;
                   console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
               }

               $scope.index = function (str) {
                   $scope.pageindex = str;
                   $scope.currentPage = str;
                   console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                   main.checked = false;
                   $scope.CheckAllChecked();
               }

               $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

               $scope.makeTodos = function () {
                   var rem = parseInt($scope.totalItems % $scope.numPerPage);
                   if (rem == '0') {
                       $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                   }
                   else {
                       $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                   }

                   var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                   var end = parseInt(begin) + parseInt($scope.numPerPage);

                   console.log("begin=" + begin); console.log("end=" + end);

                   $scope.filteredTodos = $scope.todos.slice(begin, end);
               };

               $('*[data-datepicker="true"] input[type="text"]').datepicker({
                   todayBtn: true,
                   orientation: "top left",
                   autoclose: true,
                   todayHighlight: true,
                   format: 'yyyy-mm-dd'
               });

               //$scope.showdata = function () {
               //    $http.get(ENV.apiUrl + "api/CancelAdmissionApproval/getCancelAdmissionApprovalReqList?user=" + $scope.username).then(function (res) {
               //        $scope.requestList = res.data;
               //        $scope.pendingList = [];
               //        $scope.requestList.forEach(function (cr) {
               //            var tempFiltered = $scope.pendingList.find(function (cri) { return cri.sims_enroll_number === cr.sims_enroll_number })
               //            cr.sims_tc_certificate_request_status === 'P' && tempFiltered === undefined && ($scope.pendingList[$scope.pendingList.length] = cr)
               //        })
               //        $scope.tablehide = true;
               //        debugger
                        
               //    })
               //}

               //$scope.showdata();

               $scope.shownew = function (str) {
                   debugger;
                   $http.get(ENV.apiUrl + "api/CancelAdmissionApproval/getCancelAdmissionApprovalRequestList?user=" + $scope.username + "&status=" + str).then(function (res1) {
                       $scope.pendingList = res1.data;
                       $scope.totalItems = $scope.pendingList.length;
                       $scope.todos = $scope.pendingList;
                       $scope.makeTodos();
                       if ($scope.pendingList.length == 0) {
                           $scope.tablehide = true;
                       }
                       else {
                           $scope.tablehide = true;
                       }
                       $scope.currentPage = true;
                   });
               }
               $scope.shownew('P');

               $scope.Show = function () {
                   $http.get(ENV.apiUrl + "api/StudentReport/getTcCertificateRequiestDate?date_wisesearch=" + $scope.dt.sims_tc_certificate_request_date).then(function (res1) {
                       $scope.obj = res1.data;
                       $scope.totalItems = $scope.obj.length;
                       $scope.todos = $scope.obj;
                       $scope.makeTodos();
                       if ($scope.obj.length == 0) {
                           swal({ title: 'Alert', text: "No Record Found...", showCloseButton: true, width: 450, height: 200 });
                           $scope.tablehide = false;

                       }
                       else {
                           $scope.tablehide = true;
                       }
                   })
               }

               $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                   $('input[type="text"]', $(this).parent()).focus();
               });

               $scope.SearchEnroll = function () {
                   $scope.global_Search_click();
                   $('#Global_Search_Modal').modal({ backdrop: "static" });
               }

               $scope.$on('global_cancel', function () {
                   
                   if ($scope.SelectedUserLst.length > 0) {
                       $scope.edt =
                           {
                               sims_enroll_number: $scope.SelectedUserLst[0].s_enroll_no,
                               sims_student_name: $scope.SelectedUserLst[0].name,
                           }
                   }
               });

               $scope.Save = function (MyForm) {
                   
                   if (MyForm) {
                       var user = $rootScope.globals.currentUser.username;
                       var Savedata = [];
                       var obj = ({
                           'sims_enroll_number': $scope.edt.sims_enroll_number,
                           'sims_tc_certificate_reason': $scope.edt.sims_tc_certificate_reason,
                           'sims_tc_certificate_requested_by': user,
                           'sims_tc_certificate_request_date': $scope.dt.sims_tc_certificate_request_date,
                           opr: 'I',
                       });

                       Savedata.push(obj);

                       $http.post(ENV.apiUrl + "api/StudentReport/CUDTccertificaterequiest", Savedata).then(function (msg) {

                           $scope.msg1 = msg.data;
                           if ($scope.msg1.strMessage != undefined) {
                               if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                   swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                                   $scope.showdata();
                                   $scope.currentPage = true;
                               }
                           }
                           else {
                               swal({ title: "Alert", text: "Record Not Inserted..." , showCloseButton: true, width: 380, });
                           }
                       });

                   }
               }

               $scope.searched = function (valLists, toSearch) {
                   return _.filter(valLists,
                   function (i) {
                       return searchUtil(i, toSearch);
                   });
               };

               $scope.search = function () {
                   $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                   $scope.totalItems = $scope.todos.length;
                   $scope.currentPage = '1';
                   if ($scope.searchText == '') {
                       $scope.todos = $scope.obj;
                   }
                   $scope.makeTodos();
               }

               function searchUtil(item, toSearch) {
                   return (item.sims_tc_certificate_request_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                          item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                          item.sims_tc_certificate_request_status.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                          item.sager == toSearch) ? true : false;
               }

               $scope.clear = function () {
                   $scope.edt = {
                       sims_enroll_number: '',
                       sims_tc_certificate_reason: '',
                   }
                   $scope.searchText = '';
               }

               $scope.Reset = function () {
                   
                   $scope.clear();
                   $scope.tablehide = false;
               }

               //Pending, Cancelled and Approved

               $scope.cancel_adm = function (info) {
                   
                   $state.go('main.CancelAdmissionClearFees', { data: info });
               }

               $scope.check1 = function () {
                   
                   $scope.fee['isSelected'] = fee.isSelected;
                   $scope.fee.isChecked = true;

                   //if (fee.isSelected = true) {
                   //    var fee_user = $scope.username;
                   //}
               }
               $scope.check2 = function () {
                   
                   incidence['isSelected'] = incidence.isSelected;
               }
               $scope.check3 = function () {
                   library['isSelected'] = library.isSelected;
               }
               $scope.check4 = function () {
                   exam['isSelected'] = exam.isSelected;
               }
               $scope.check5 = function () {
                   inventory['isSelected'] = inventory.isSelected;
               }

               var datasend = [];
               $scope.submit = function () {
                   debugger;
                                    
                   $http.get(ENV.apiUrl + "api/StudentReport/getResionsupdate?enroll=" + $scope.copy_clear.sims_enroll_number + "&year=" + $scope.copy_clear.sims_tc_certificate_academic_year + "&duedate=" + $scope.duedate).then(function (res) {
                   });


                   console.log($scope.enroll);
                   $scope.chk = [];
                   $http.get(ENV.apiUrl + "api/CancelAdmissionApproval/getApprovalDeptNew?user=" + $scope.username).then(function (check) {
                       
                       $scope.chk = check.data;
                       $scope.chkshow = $scope.chk[0];
                   })
                   //console.log($scope.chk);
                   //console.log($scope.chkshow);
                  

                   //if ($scope.chkshow.sims_acad_clr_status == true && $scope.chkshow.sims_acad_clr_status.isChecked == true) {
                   //    var acad_user = $scope.username;
                   //    var approve_date1 = date;
                   //}
                   //if ($scope.chkshow.sims_fee_clr_status == true && $scope.chkshow.sims_fee_clr_status.isChecked == true) {
                   //    var fee_user = $scope.username;
                   //    var approve_date2 = date;
                   //}
                   //if ($scope.chkshow.sims_finn_clr_status == true && $scope.chkshow.sims_finn_clr_status.isChecked == true) {
                   //    var finn_user = $scope.username;
                   //    var approve_date3 = date;
                   //}
                   //if ($scope.chkshow.sims_inv_clr_status == true && $scope.chkshow.sims_inv_clr_status.isChecked == true) {
                   //    var inv_user = $scope.username;
                   //    var approve_date4 = date;
                   //}
                   //if ($scope.chkshow.sims_inci_clr_status == true && $scope.chkshow.sims_inci_clr_status.isChecked == true) {
                   //    var inci_user = $scope.username;
                   //    var approve_date5 = date;
                   //}
                   //if ($scope.chkshow.sims_lib_clr_status == true && $scope.chkshow.sims_lib_clr_status.isChecked == true) {
                   //    var lib_user = $scope.username;
                   //    var approve_date6 = date;
                   //}
                   //if ($scope.chkshow.sims_trans_clr_status == true && $scope.chkshow.sims_trans_clr_status.isChecked == true) {
                   //    var trans_user = $scope.username;
                   //    var approve_date7 = date;
                   //}
                   //if ($scope.chkshow.sims_admin_clr_status == true && $scope.chkshow.sims_admin_clr_status.isChecked == true) {
                   //    var admin_user = $scope.username;
                   //    var approve_date8 = date;
                   //}
                   //if ($scope.chkshow.sims_other1_clr_status == true && $scope.chkshow.sims_other1_clr_status.isChecked == true) {
                   //    var other1_user = $scope.username;
                   //    var approve_date9 = date;
                   //}
                   //if ($scope.chkshow.sims_other2_clr_status == true && $scope.chkshow.sims_other2_clr_status.isChecked == true) {
                   //    var other2_user = $scope.username;
                   //    var approve_date10 = date;
                   //}
                   //if ($scope.chkshow.sims_other3_clr_status == true && $scope.chkshow.sims_other3_clr_status.isChecked == true) {
                   //    var other3_user = $scope.username;
                   //    var approve_date11 = date;
                   //}
                   // console.log($scope.chkshow);
                   
                   var count1 = 0;
                   if ($scope.chkshow.sims_acad_clr_status == true) {
                       var v1 = document.getElementById('chk_term7');
                       if (v1.checked == true) {
                           var acad_user = $scope.username;
                           var approve_date1 = date;
                           count1=count1+1;
                       }
                       
                   }
                   if ($scope.chkshow.sims_fee_clr_status == true) {
                       var v2 = document.getElementById('chk_term1');
                       if(v2.checked == true){
                           var fee_user = $scope.username;
                           var approve_date2 = date;

                           count1=count1+1;
                       }
                       for (var i = 0; i < $scope.searchResult.length; i++) {
                           var fee_data = {
                               'sims_fee_period1_paid':$scope.searchResult[i].sims_fee_period1_paid,
                               'sims_fee_period2_paid':$scope.searchResult[i].sims_fee_period2_paid,
                               'sims_fee_period3_paid':$scope.searchResult[i].sims_fee_period3_paid,
                               'sims_fee_period4_paid':$scope.searchResult[i].sims_fee_period4_paid,
                               'sims_fee_period5_paid':$scope.searchResult[i].sims_fee_period5_paid,
                               'sims_fee_period6_paid':$scope.searchResult[i].sims_fee_period6_paid,
                               'sims_fee_period7_paid':$scope.searchResult[i].sims_fee_period7_paid,
                               'sims_fee_period8_paid':$scope.searchResult[i].sims_fee_period8_paid,
                               'sims_fee_period9_paid':$scope.searchResult[i].sims_fee_period9_paid,
                               'sims_fee_period10_paid':$scope.searchResult[i].sims_fee_period10_paid,
                               'sims_fee_period11_paid':$scope.searchResult[i].sims_fee_period11_paid,
                               'sims_fee_period12_paid': $scope.searchResult[i].sims_fee_period12_paid,
                               'sims_enroll_number': $scope.enroll,
                               'sims_academic_year': $scope.grd_sec.sims_academic_year,
                               'sims_cur_code': $scope.grd_sec.sims_cur_code,
                               'sims_grade_code': $scope.grd_sec.sims_grade_code,
                               'sims_section_code': $scope.grd_sec.sims_section_code
                           }
                           datasend.push(fee_data);
                       }
                      
                       $http.post(ENV.apiUrl + "api/CancelAdmissionApproval/UpdateFeeDetails", datasend).then(function (msg) {
                               
                               $scope.msg1 = msg.data;
                               fee_data = [];
                       });                       
                   }
                   if ($scope.chkshow.sims_finn_clr_status == true) {
                       var v3 = document.getElementById('chk_term2');
                       if (v3.checked == true) {
                           var finn_user = $scope.username;
                           var approve_date3 = date;
                           count1=count1+1;
                       }
                   }
                   if ($scope.chkshow.sims_inv_clr_status == true) {
                       var v4 = document.getElementById('chk_term3');
                       if (v4.checked == true) {
                           var inv_user = $scope.username;
                           var approve_date4 = date;
                           count1=count1+1;
                       }
                   }
                   if ($scope.chkshow.sims_inci_clr_status == true) {
                       var v5 = document.getElementById('chk_term4');
                       if (v5.checked == true) {
                           var inci_user = $scope.username;
                           var approve_date5 = date;
                           count1=count1+1;
                       }
                   }
                   if ($scope.chkshow.sims_lib_clr_status == true) {
                       var v6 = document.getElementById('chk_term5');
                       if (v6.checked == true) {
                           var lib_user = $scope.username;
                           var approve_date6 = date;
                           count1=count1+1;
                       }
                   }
                   if ($scope.chkshow.sims_trans_clr_status == true) {
                       var v7 = document.getElementById('chk_term6');
                       if (v7.checked == true) {
                           var trans_user = $scope.username;
                           var approve_date7 = date;
                           count1=count1+1;
                       }
                   }
                   if ($scope.chkshow.sims_admin_clr_status == true) {
                       var v8 = document.getElementById('chk_term8');
                       if (v8.checked == true) {
                           var admin_user = $scope.username;
                           var approve_date8 = date;
                           count1=count1+1;
                       }
                   }
                   if ($scope.chkshow.sims_other1_clr_status == true) {
                       var other1_user = $scope.username;
                       var approve_date9 = date;                     
                   }
                   if ($scope.chkshow.sims_other2_clr_status == true) {
                       var other2_user = $scope.username;
                       var approve_date10 = date;
                   }
                   if ($scope.chkshow.sims_other3_clr_status == true) {
                       var other3_user = $scope.username;
                       var approve_date11 = date;
                   }
                   if ($scope.chkshow.sims_clr_super_user_flag == true) {
                       var v9 = document.getElementById('chk_term12');
                       if (v9.checked == true) {                           
                           count1 = count1 + 1;
                       }
                   }

                   if (count1 == 0) {
                       swal('', 'No departments are Selected/Cleared.');
                       return;
                   }
                   else {

                       debugger;
                       var data = [];
                       if ($scope.chkshow.sims_fee_clr_status == true) {
                           var senddata = {
                               'opr': 'I',
                               'sims_enroll_number': $scope.enroll,
                                
                               'sims_clr_tran_status': $scope.edts.sims_fee_clr_status,
                               'sims_clr_tran_remark': $scope.sims_fee_clr_remark,
                               'sims_clr_tran_employee_id': fee_user,
                               'sims_clr_tran_created_date': approve_date2,
                               'sims_clr_tran_access_code': '1'
                           }
                           data.push(senddata);
                       }
                       //if ($scope.chkshow.sims_finn_clr_status == true) {
                       //    var senddata = {
                       //        'opr': 'I',
                       //        'sims_enroll_number': $scope.enroll,
                       //        'sims_finn_clr_status': $scope.edts.sims_finn_clr_status,
                       //        'sims_finn_clr_remark': $scope.sims_finn_clr_remark,
                       //        'sims_finn_clr_by': finn_user,
                       //        'sims_finn_clr_date': approve_date3,
                       //        'sims_clr_tran_access_code': '2'
                       //    }
                       //    data.push(senddata);
                       //}

                           //'sims_inv_clr_status': $scope.edts.sims_inv_clr_status,
                           //'sims_inv_clr_remark': $scope.sims_inv_clr_remark,
                           //'sims_inv_clr_by': inv_user,
                           //'sims_inv_clr_date': approve_date4,

                           //'sims_inci_clr_status': $scope.edts.sims_inci_clr_status,
                           //'sims_inci_clr_remark': $scope.sims_inci_clr_remark,
                           //'sims_inci_clr_by': inci_user,
                       //'sims_inci_clr_date': approve_date5,

                       if ($scope.chkshow.sims_lib_clr_status == true) {
                           var senddata = {
                               'opr': 'I',
                               'sims_enroll_number': $scope.enroll,
                               'sims_clr_tran_status': $scope.edts.sims_lib_clr_status,
                               'sims_clr_tran_remark': $scope.sims_lib_clr_remark,
                               'sims_clr_tran_employee_id': lib_user,
                               'sims_clr_tran_created_date': approve_date6,
                               
                               'sims_clr_tran_access_code': '7'
                           }
                           data.push(senddata);
                       }

                           //'sims_trans_clr_status': $scope.edts.sims_trans_clr_status,
                           //'sims_trans_clr_remark': $scope.sims_trans_clr_remark,
                           //'sims_trans_clr_by': trans_user,
                           //'sims_trans_clr_date': approve_date7,

                           //'sims_acad_clr_status': $scope.edts.sims_acad_clr_status,
                           //'sims_acad_clr_remark': $scope.sims_acad_clr_remark,
                           //'sims_acad_clr_by': acad_user,
                           //'sims_acad_clr_date': approve_date1,

                           //'sims_admin_clr_status': $scope.edts.sims_admin_clr_status,
                           //'sims_admin_clr_remark': $scope.sims_admin_clr_remark,
                           //'sims_admin_clr_by': admin_user,
                           //'sims_admin_clr_date': approve_date8,

                           //'sims_other1_clr_status': $scope.edts.sims_other1_clr_status,
                           //'sims_other1_clr_remark': $scope.sims_other1_clr_remark,
                           //'sims_other1_clr_by': other1_user,
                           //'sims_other1_clr_date': approve_date9,

                           //'sims_other2_clr_status': $scope.edts.sims_other2_clr_status,
                           //'sims_other2_clr_remark': $scope.sims_other2_clr_remark,
                           //'sims_other2_clr_by': other2_user,
                           //'sims_other2_clr_date': approve_date10,

                           //'sims_other3_clr_status': $scope.edts.sims_other3_clr_status,
                           //'sims_other3_clr_remark': $scope.sims_other3_clr_remark,
                           //'sims_other3_clr_by': other3_user,
                           //'sims_other3_clr_date': approve_date11,

                           //'sims_clr_status': $scope.edts.sims_clr_status
                       
                     
                       console.log($scope.senddata);
                       
                       $http.post(ENV.apiUrl + "api/CancelAdmissionApproval/InsertClearStatusNew", data).then(function (msg) {
                           
                           $scope.msg1 = msg.data;
                           if ($scope.msg1 == true) {
                               $('#myModal1').modal('hide');
                               swal({ text: "Record Inserted/Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 }).then(function (isConfirm) {
                                   $scope.showdata();
                               });
                           }
                           else {
                               swal({ text: "Record Not Inserted", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                           }
                       });
                       data = [];
                       if ($scope.chkshow.sims_fee_clr_status == true) {
                           $scope.table1 = false;
                           var v2 = document.getElementById('chk_term1');
                           if (v2.checked == true) {
                               v2.checked = false;
                           }
                       }
                   }
               }

               $scope.allow = function () {
                   
                   if ($scope.edts.sims_clr_status == true) {
                       swal('', 'Clearance will be directly given if you select this and later you wont be able to approve other departments.');                       
                   }
               }

               $scope.modal_cancel = function () {
                   //$scope.j = '';
                   $scope.clear = '';
                   $scope.showdata();
               }

               //$scope.viewDetails = function () {
               //    $http.get(ENV.apiUrl + "api/StudentReport/AllRequestDetailsList?enroll=" + clear.sims_enroll_number + "&year=" + clear.sims_tc_certificate_academic_year).then(function (res) {
               //        $scope.duedate = res.data;
               //    });
               //}
               $scope.viewDetails = function (clear) {
                   debugger;
                   $scope.copy_clear = angular.copy(clear);
                   $http.get(ENV.apiUrl + "api/StudentReport/getdatemonth?enroll=" + clear.sims_enroll_number + "&year=" + clear.sims_tc_certificate_academic_year).then(function (res) {
                       $scope.duedate = res.data;
                   });

                   $http.get(ENV.apiUrl + "api/CancelAdmissionApproval/getgradesaction?enroll=" + clear.sims_enroll_number + "&year=" + clear.sims_tc_certificate_academic_year).then(function (sec_details) {
                       $scope.grd_sec = sec_details.data[0];
                       console.log($scope.grd_sec);
                   });

                   $http.get(ENV.apiUrl + "api/StudentReport/GetStatusforStudentNew?enroll_no=" + clear.sims_enroll_number).then(function (res) {
                       $scope.clearence_new = res.data;
                   });

                   //$state.go('main.CancelAdmissionClearFees', { data: $scope.info });
                   $scope.enroll = clear.sims_enroll_number
                   $('#myModal1').modal('show');
                   $http.post(ENV.apiUrl + "api/common/CancelAdmission/CancelAdmission?enroll_no=" + clear.sims_enroll_number + "&cur_code=" + clear.sims_tc_certificate_cur_code + "&acad_yr=" + clear.sims_tc_certificate_academic_year).then(function (res) {
                       $scope.edt = res.data;
                       $scope.clearance = true;
                       console.log($scope.edt);
                   });

                   $scope.chk = [];
                   $http.get(ENV.apiUrl + "api/CancelAdmissionApproval/getApprovalDeptNew?user=" + $scope.username).then(function (check) {
                       $scope.chk = check.data;
                       $scope.chkshow = $scope.chk[0];
                   })

                   $http.post(ENV.apiUrl + "api/CancelAdmissionApproval/AdmCheckedDept?enroll=" + $scope.enroll).then(function (chkdept) {
                       $scope.edts = chkdept.data[0];
                       //$scope.chkshow = $scope.chk[0];                                    
                   })
               }

               $scope.getFeesDetails = function () {
                   if ($scope.chkshow.sims_fee_clr_status == true) {
                       var v2 = document.getElementById('chk_term1');
                       if (v2.checked == true) {
                           //var len;
                           $scope.table1 = false;
                           $scope.busyindicator = false;
                           $scope.show_Update_table = false;

                       }
                       else {
                           $scope.table1 = false;
                           $scope.busyindicator = false;
                           $scope.show_Update_table = true;
                           return;
                       }
                       console.log($scope.grd_sec);
                       console.log($scope.clear);
                       console.log($scope.copy_clear);
                       $http.get(ENV.apiUrl + "api/CancelAdmissionApproval/getsectionfeedetails?enroll=" + $scope.copy_clear.sims_enroll_number + "&cur=" + $scope.grd_sec.sims_cur_code + "&ac_year=" + $scope.grd_sec.sims_academic_year + "&sec=" + $scope.grd_sec.sims_section_code).then(function (fee_info) {
                           
                           $scope.searchResult = fee_info.data;
                           $scope.clearance = true;
                           console.log($scope.edt);
                       });
                   }
               }

               //reject user wise cancel 
               $scope.reject = function (str) {
                   debugger;
                   swal({
                       title: '',
                       text: "Are you sure you want to Reject?",
                       showCloseButton: true,
                       confirmButtonText: 'Yes',
                       showCancelButton: true,
                       width: 380,
                       cancelButtonText: 'No',

                   }).then(function (isConfirm) {
                       if (isConfirm) {
                           //sims_tc_certificate_request_number
                           var user = $rootScope.globals.currentUser.username;
                           var Savedata = [];
                           var obj = ({
                               
                               'sims_tc_certificate_cur_code' : str.sims_tc_certificate_cur_code,
                               'sims_tc_certificate_academic_year': str.sims_tc_certificate_academic_year,
                               'sims_enroll_number': str.sims_enroll_number,
                               'sims_tc_certificate_requested_by': user,
                               'sims_tc_certificate_request_number': str.sims_tc_certificate_request_number,
                               'sims_tc_certificate_request_date': str.sims_tc_certificate_request_date,
                               'sims_tc_certificate_reason' : 'Reject',
                               opr: 'U',
                           });

                           Savedata.push(obj);

                           $http.post(ENV.apiUrl + "api/CancelAdmissionApproval/CUDTccertificaterequiestUpdate", Savedata).then(function (msg) {

                               $scope.msg1 = msg.data;
                               if (msg.data == true) {
                                   swal({ text: "Record  Rejected Successfully...", imageUrl: "assets/img/check.png", width: 380, height: 200 });
                                   $scope.showdata();
                                   $scope.Reset();
                                   $scope.currentPage = true;

                               }
                               else if (msg.data == false) {
                                   swal({ text: "Record Not Rejected...", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                               }
                               else {
                                   swal("Error-" + $scope.msg1)
                               }
                           });
                       }
                   });

               }

               // $scope.main
               //$scope.getAllSectionFees = function () {
               //    
               //    //if ($scope.chkshow.sims_fee_clr_status == true) {
               //    //    var v2 = document.getElementById('chk_term1');
               //    //    if(v2.checked == true){
               //    //        var fee_user = $scope.username;
               //    //        var approve_date2 = date;
               //    //        count1=count1+1;
               //    //    }
               //    //}
               //    var v2 = document.getElementById('chk_term1');
               //    if (v2.checked == true) {
               //        var len;
               //        $scope.table1 = true;
               //        $scope.busyindicator = true;
               //        $scope.show_Update_table = false;

               //        $http.get(ENV.apiUrl + "api/common/SectionFee/getSectionFee?cur_name=" + $scope.grd_sec.sims_cur_code + "&academic_year=" + $scope.grd_sec.sims_academic_year
               //            + "&grade_name=" + $scope.grd_sec.sims_grade_code + "&section_name=" + $scope.grd_sec.sims_section_code
               //            + "&fee_category=" + '01').then(function (res) {

               //                $scope.searhcResult = res.data;
               //                $scope.len = $scope.searhcResult.length;
               //                $scope.show_Update_table = true;
               //                $scope.show_Insert_table = false;
               //                $scope.show_ins_table = false;

               //                if ($scope.searhcResult.length > 0) {
               //                    $scope.table1 = true;
               //                    $scope.busyindicator = false;
               //                }
               //                else {
               //                    $scope.table1 = false;
               //                    $scope.busyindicator = false;

               //                    swal({ text: 'Fee Not Found', width: 320, showCloseButton: true });
               //                }
               //                if ($scope.len == 0) {
               //                    $scope.get();
               //                }
               //            });
               //    }
               //}
            }]);
})();