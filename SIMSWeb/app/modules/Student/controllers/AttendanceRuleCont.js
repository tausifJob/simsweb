﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AttendanceRuleCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.grid = true;
            $scope.edit_data = false;
            $scope.attrule_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            $timeout(function () {
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $http.get(ENV.apiUrl + "api/common/AttendanceRule/getAttendanceRule").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.attrule_data = res.data;
                $scope.totalItems = $scope.attrule_data.length;
                $scope.todos = $scope.attrule_data;
                $scope.makeTodos();
                $scope.grid = true;

                for (var i = 0; i < $scope.totalItems; i++) {
                    $scope.attrule_data[i].icon = "fa fa-plus-circle";
                }
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edit = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = false;
                    $scope.update1 = true;
                    $scope.delete1 = false;
                    $scope.edit_data = true;
                    //$scope.edt = str;
                    $scope.edt =
                        {
                            sims_academic_year: str.sims_academic_year,
                            sims_cur_code: str.sims_cur_code,
                            sims_grade_code: str.sims_grade_code,
                            sims_attendance_attendance_code: str.sims_attendance_attendance_code,
                            sims_attendance_category_code: str.sims_attendance_category_code,
                            sims_attendance_consequence_code: str.sims_attendance_consequence_code,
                            sims_attendance_rule_code: str.sims_attendance_rule_code,
                            sims_attendance_rule_name: str.sims_attendance_rule_name,
                            sims_attendance_threshold_value1: str.sims_attendance_threshold_value1,
                            sims_attendance_threshold_value2: str.sims_attendance_threshold_value2,
                            sims_attendance_threshold_value3: str.sims_attendance_threshold_value3,
                            sims_attendance_threshold_value4: str.sims_attendance_threshold_value4,
                            sims_attendance_threshold_value5: str.sims_attendance_threshold_value5,
                            sims_attendance_report_card_flag: str.sims_attendance_report_card_flag,
                            sims_attendance_academic_year_flag: str.sims_attendance_academic_year_flag,
                            sims_attendance_term_flag: str.sims_attendance_term_flag,
                        }
                    $scope.GetAacd_yr($scope.edt.sims_cur_code);
                    $scope.GetGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                }
            }
            $scope.edt = {};
            debugger;
            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.cur_data = res.data;
                if (res.data.length > 0) {
                    $scope.edt.sims_cur_code = res.data[0].sims_attendance_cur_code;
                    $scope.GetAacd_yr($scope.edt.sims_cur_code);
                }
            });
            debugger;
            $scope.GetAacd_yr = function (cur) {
                if (cur != undefined) {
                    $http.get(ENV.apiUrl + "api/common/Attendance/getAcademicYear?curCode=" + cur).then(function (res) {
                        $scope.acad = res.data;
                        if (res.data.length > 0) {
                            $scope.edt.sims_academic_year = res.data[0].sims_academic_year;
                            $scope.GetGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                        }
                    });

                    $http.get(ENV.apiUrl + "api/common/AttendanceRule/getConsequence?cur_name=" + cur).then(function (res) {
                        $scope.consequence_data = res.data;
                    });

                    $http.get(ENV.apiUrl + "api/common/AttendanceRule/getAttendance?cur_name=" + cur).then(function (res) {
                        $scope.attendance_data = res.data;
                    });
                }
            }

            $scope.GetGrade = function (cur, acad_yr) {
                $http.get(ENV.apiUrl + "api/common/ModeratorUser/getAllGrades?cur_code=" + cur + "&ac_year=" + acad_yr).then(function (res) {
                    $scope.grade = res.data;
                });
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    var autoid;
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = true;
                    $scope.update1 = false;
                    $scope.delete1 = false;
                    $scope.edit_data = false;
                    $scope.edt = [];
                    $scope.edt = {};
                    debugger;
                    $scope.edt.sims_attendance_report_card_flag = true;
                    $scope.edt.sims_attendance_term_flag = true;
                    $scope.edt.sims_attendance_academic_year_flag = true;

                    if ($scope.cur_data.length > 0) {
                        $scope.edt.sims_cur_code = $scope.cur_data[0].sims_attendance_cur_code;
                        $scope.GetAacd_yr($scope.edt.sims_cur_code);
                    }

                    if ($scope.acad.length > 0) {
                        $scope.edt.sims_academic_year = $scope.acad[0].sims_academic_year;
                        $scope.GetGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                    }

                    $http.get(ENV.apiUrl + "api/common/AttendanceRule/getAutoGenerateRuleCode").then(function (res) {
                        autoid = res.data;
                        $scope.edt['sims_attendance_rule_code'] = autoid;
                    });
                }
            }

            $scope.Save = function (isvalidate) {
                debugger
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({
                            sims_academic_year: $scope.edt.sims_academic_year,
                            sims_cur_code: $scope.edt.sims_cur_code,
                            sims_grade_code: $scope.edt.sims_grade_code,
                            sims_attendance_attendance_code: $scope.edt.sims_attendance_attendance_code,
                            sims_attendance_category_code: $scope.edt.sims_attendance_category_code,
                            sims_attendance_consequence_code: $scope.edt.sims_attendance_consequence_code,
                            sims_attendance_rule_code: $scope.edt.sims_attendance_rule_code,
                            sims_attendance_rule_name: $scope.edt.sims_attendance_rule_name,
                            sims_attendance_threshold_value1: $scope.edt.sims_attendance_threshold_value1,
                            sims_attendance_threshold_value2: $scope.edt.sims_attendance_threshold_value2,
                            sims_attendance_threshold_value3: $scope.edt.sims_attendance_threshold_value3,
                            sims_attendance_threshold_value4: $scope.edt.sims_attendance_threshold_value4,
                            sims_attendance_threshold_value5: $scope.edt.sims_attendance_threshold_value5,
                            sims_attendance_report_card_flag: $scope.edt.sims_attendance_report_card_flag,
                            sims_attendance_academic_year_flag: $scope.edt.sims_attendance_academic_year_flag,
                            sims_attendance_term_flag: $scope.edt.sims_attendance_term_flag,
                            opr: 'I'
                        });

                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/common/AttendanceRule/CUDAttendanceRule", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            console.log($scope.msg1);
                            if ($scope.msg1 == true) {
                                swal({ text: "Attendance rule added successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Attendance rule not added successfully. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/common/AttendanceRule/getAttendanceRule").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.attrule_data = res.data;
                    $scope.totalItems = $scope.attrule_data.length;
                    $scope.todos = $scope.attrule_data;
                    $scope.makeTodos();
                    $scope.grid = true;

                    for (var i = 0; i < $scope.totalItems; i++) {
                        $scope.attrule_data[i].icon = "fa fa-plus-circle";
                    }


                });

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }


            $scope.Update = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    var data = ({

                        sims_academic_year: $scope.edt.sims_academic_year,
                        sims_cur_code: $scope.edt.sims_cur_code,
                        sims_grade_code: $scope.edt.sims_grade_code,
                        sims_attendance_attendance_code: $scope.edt.sims_attendance_attendance_code,
                        sims_attendance_category_code: $scope.edt.sims_attendance_category_code,
                        sims_attendance_consequence_code: $scope.edt.sims_attendance_consequence_code,
                        sims_attendance_rule_code: $scope.edt.sims_attendance_rule_code,
                        sims_attendance_rule_name: $scope.edt.sims_attendance_rule_name,
                        sims_attendance_threshold_value1: $scope.edt.sims_attendance_threshold_value1,
                        sims_attendance_threshold_value2: $scope.edt.sims_attendance_threshold_value2,
                        sims_attendance_threshold_value3: $scope.edt.sims_attendance_threshold_value3,
                        sims_attendance_threshold_value4: $scope.edt.sims_attendance_threshold_value4,
                        sims_attendance_threshold_value5: $scope.edt.sims_attendance_threshold_value5,
                        sims_attendance_report_card_flag: $scope.edt.sims_attendance_report_card_flag,
                        sims_attendance_academic_year_flag: $scope.edt.sims_attendance_academic_year_flag,
                        sims_attendance_term_flag: $scope.edt.sims_attendance_term_flag,
                        opr: 'U'
                    });

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/common/AttendanceRule/CUDAttendanceRule", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Attendance rule updated successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });

                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Attendance rule not updated successfully. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Delete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    var deletecode = [];
                    $scope.flag = false;

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_attendance_rule_code + i);

                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodercode = ({
                                'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
                                'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                                'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                                'sims_attendance_rule_code': $scope.filteredTodos[i].sims_attendance_rule_code,
                                'opr': 'D'
                            });
                            deletecode.push(deletemodercode);
                        }
                    }

                    if ($scope.flag) {

                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $http.post(ENV.apiUrl + "api/common/AttendanceRule/CUDAttendanceRule", deletecode).then(function (res) {
                                    $scope.msg1 = res.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Attendance rule deleted successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Attendance rule not deleted successfully. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }

                                });
                            }
                            else {
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }

                                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                                    var v = document.getElementById($scope.filteredTodos[i].sims_attendance_rule_code + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Please select atleast one record", showCloseButton: true, width: 380, });
                    }
                }
            }

            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_attendance_rule_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_attendance_rule_code + i);
                        v.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }


            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.attrule_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.attrule_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.attrule_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_attendance_rule_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_attendance_rule_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }


            var dom;
            $scope.flag = true;
            $scope.icon = "fa fa-plus-circle";

            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr  id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                          "<tr> <td class='semi-bold'>" + "Threshold Value 1" + "</td><td class='semi-bold'>" + "Threshold Value 2" + " </td><td class='semi-bold'>" + "Threshold Value 3" + "</td><td class='semi-bold'>" + "Threshold Value 4" + "</td><td class='semi-bold'>" + "Threshold Value 5" + "</td><td class='semi-bold'>" + "Report Card Flag" + "</td> <td class='semi-bold'>" + "Attendance Term Flag" + " </td><td class='semi-bold'>" + "Academic Year Flag" + "</td>" +
                            "</tr>" +
                              "<tr><td>" + (info.sims_attendance_threshold_value1) + "</td> <td>" + (info.sims_attendance_threshold_value2) + " </td><td>" + (info.sims_attendance_threshold_value3) + "</td><td>" + (info.sims_attendance_threshold_value4) + "</td><td>" + (info.sims_attendance_threshold_value5) + "</td><td>" + (info.sims_attendance_report_card_flag) + "</td> <td>" + (info.sims_attendance_term_flag) + " </td><td>" + (info.sims_attendance_academic_year_flag) + "</td>" +
                            "</tr>" +

                        "</tbody>" +
                        " </table></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    info.icon = "fa fa-plus-circle";
                    $scope.flag = true;
                }
            };



        }])
})();