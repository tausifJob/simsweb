﻿
(function () {
    'use strict';
    var del = [];
    var main;
    var date1, date3, date4;

    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ProspectAdmissionNISCont',
        ['$scope', '$state', '$stateParams', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $stateParams, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.display = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.school_edit = true;
            $scope.Study_edit = true;
            $scope.desc_edit = true;
            $scope.confirm_edit = true;
            $scope.emp_edit = true;
            $scope.hearing_edit = true;
            $scope.music_edit = true;
            $scope.sport_edit = true;
            $scope.other_edit = true;
            $scope.circum_edit = true;
            $scope.disab_edit = true;
            $scope.healthres_edit = true;
            $scope.health_vision_edit = true;
            // $scope.tick_edit = true;
            $scope.pricont_edit = true;
            $scope.transreq_edit = true;
            $scope.lang_sup_edit = true;
            $scope.behaviour_edit = true;
            $scope.gifted_edit = true;
            $scope.med_edit = true;
            $scope.grid = true;
            $scope.save1 = true;
            $scope.update1 = false;
            $scope.t = true;
            $scope.btn_back = false;

            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');

          
            $scope.edt = [];
            var data2 = [];

            $scope.edt =
                {
                    admission_date: $scope.ddMMyyyy,
                    tent_join_date: $scope.ddMMyyyy,
                    comm_date: $scope.ddMMyyyy,
                    declaration_status: true
                }

            var param = $stateParams.Pros_num;
            console.log(param);

            $(document).ready(function () {
                $('#t2').addClass('disableTab');
                $('#t3').addClass('disableTab');
                $('#t4').addClass('disableTab');
                $('#t5').addClass('disableTab');
                $('#t6').addClass('disableTab');
                $('#t7').addClass('disableTab');
                $('#t8').addClass('disableTab');
            });

            if (param != '[object Object]') {
                $http.get(ENV.apiUrl + "api/common/ProspectDashboard/GetTabStudentData?prospect_number=" + param).then(function (res) {
                    $scope.edt = res.data;
                    console.log($scope.edt);
                    $scope.edt.admission_date = $scope.ddMMyyyy;
                    $scope.edt.tent_join_date = $scope.ddMMyyyy;
                    $scope.edt.comm_date = $scope.ddMMyyyy;
                    $scope.edt.birth_date = $scope.ddMMyyyy;
                    $scope.edt.passport_issue_date = $scope.ddMMyyyy;
                    $scope.edt.passport_expiry = $scope.ddMMyyyy;
                    $scope.edt.visa_issue_date = $scope.ddMMyyyy;
                    $scope.edt.visa_expiry_date = $scope.ddMMyyyy;
                    $scope.edt.national_id_issue_date = $scope.ddMMyyyy;
                    $scope.edt.national_id_expiry_date = $scope.ddMMyyyy;
                    $scope.edt.sibling_dob = $scope.ddMMyyyy;
                    $scope.edt.current_school_from_date = $scope.ddMMyyyy;
                    $scope.edt.current_school_to_date = $scope.ddMMyyyy;
                    $scope.edt.health_card_issue_date = $scope.ddMMyyyy;
                    $scope.edt.health_card_expiry_date = $scope.ddMMyyyy;
                    
                    $scope.update1 = true;
                    $scope.save1 = false;
                    $scope.btn_back = true;
                });
            }

            $http.get(ENV.apiUrl + "api/common/Admission/getAdmissionList").then(function (res) {

                $scope.obj = res.data;
                console.log($scope.obj);
                $scope.scholnm = [], $scope.feecat = [], $scope.legalcus = [], $scope.curCode = [],$scope.acad_yr=[];
                for (var i = 0; i < res.data.length; i++) {
                    if (res.data[i].school_name != '') {
                        $scope.scholnm.push({ school_name: res.data[i].school_name, school_code: res.data[i].school_code });
                        $scope.edt['school_code'] = $scope.scholnm[0].school_code;
                    }
                    if (res.data[i].legal_custody_name != '') {
                        $scope.legalcus.push({ legal_custody_code: res.data[i].legal_custody_code, legal_custody_name: res.data[i].legal_custody_name });
                        $scope.edt['primary_contact_pref_code'] = $scope.legalcus[0].legal_custody_code;
                    }
                    if (res.data[i].curr_name != '') {
                        $scope.curCode.push({ curr_code: res.data[i].curr_code, curr_name: res.data[i].curr_name });
                        $scope.edt['curr_code'] = $scope.curCode[0].curr_code;
                    }
                    if (res.data[i].academic_year_desc != '') {
                        $scope.acad_yr.push({ academic_year: res.data[i].academic_year, academic_year_desc: res.data[i].academic_year_desc });
                        $scope.edt['academic_year'] = $scope.acad_yr[0].academic_year;
                    }
                }

            });

            $scope.GetInfo = function (parentid, enrollno) {
                console.log(parentid, enrollno);
                if (enrollno != null) {
                    $http.post(ENV.apiUrl + "api/common/Admission/CheckParentCode?parent_id=" + parentid + "&enroll_no=" + enrollno).then(function (res) {
                        $scope.msg1 = res.data;
                        console.log($scope.msg1);
                        $scope.edt.parent_id = $scope.msg1.parent_id;
                        $scope.edt.sibling_enroll = $scope.msg1.sims_student_enroll_number;
                        $scope.edt.sibling_name = $scope.msg1.sibling_name;
                    });
                }
            }

            $scope.Getstep1 = function (isvalidate) {
                if (isvalidate) {
                    $('.nav-tabs a[href="#tab2"]').tab('show');
                }
            }

            $scope.getPrevStep2 = function () {
                $('.nav-tabs a[href="#tab1"]').tab('show');
            }

            $scope.getPrevStep3 = function () {
                $('.nav-tabs a[href="#tab2"]').tab('show');
            }

            $scope.getPrevStep4 = function () {
                $('.nav-tabs a[href="#tab3"]').tab('show');
            }

            $scope.getPrevStep5 = function () {
                $('.nav-tabs a[href="#tab4"]').tab('show');
            }

            $scope.getPrevStep6 = function () {
                if ($scope.edt.sibling_status == true) {
                    $('.nav-tabs a[href="#tab2"]').tab('show');
                }
                else {
                    $('.nav-tabs a[href="#tab5"]').tab('show');
                }
            }

            $scope.getPrevStep7 = function () {
                $('.nav-tabs a[href="#tab6"]').tab('show');
            }

            $scope.getPrevStep8 = function () {
                $('.nav-tabs a[href="#tab7"]').tab('show');
            }

            $scope.Getstep2 = function (isvalidate) {
                if (isvalidate) {
                    console.log($scope.edt.sibling_status);
                    // if ($scope.edt.sibling_status == true)
                    if ($scope.edt.sibling_status == true) {
                        if (($scope.edt.parent_id == undefined || $scope.edt.parent_id == '') && ($scope.edt.sibling_enroll == undefined || $scope.edt.sibling_enroll == '')) {

                            swal({ title: "Alert", text: "Please Enter Sibling Details", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {

                                }
                                else {

                                }
                            });

                        }
                        else {
                            $('.nav-tabs a[href="#tab6"]').tab('show');
                        }

                    }
                    else {
                        $('.nav-tabs a[href="#tab3"]').tab('show');
                    }
                }
            }

            $scope.Getstep3 = function (isvalidate) {
                if (isvalidate) {
                    $('.nav-tabs a[href="#tab4"]').tab('show')
                }
            }

            $scope.Getstep4 = function (isvalidate) {
                if (isvalidate) {
                    $('.nav-tabs a[href="#tab5"]').tab('show')
                }
            }

            $scope.Getstep5 = function (isvalidate) {
                if (isvalidate) {
                    $('.nav-tabs a[href="#tab6"]').tab('show')
                }
            }

            $scope.Getstep6 = function (isvalidate) {
                if (isvalidate) {
                    $('.nav-tabs a[href="#tab7"]').tab('show')
                }
            }

            $scope.Getstep7 = function (isvalidate) {
                if (isvalidate) {
                    $('.nav-tabs a[href="#tab8"]').tab('show')
                }
            }

            $scope.Save = function (isvalidate) {
                $scope.save1 = true;
                var data2 = [];
                $scope.BUSY = true;

                if (isvalidate) {
                    if ($scope.update1 == false) {

                        var data = ({
                            admission_number: $scope.edt.admission_number,
                            appl_num: $scope.edt.appl_num,
                            pros_num: $scope.edt.pros_num,
                            pros_appl_num: $scope.edt.pros_appl_num,
                            admission_date: $scope.edt.admission_date,
                            school_code: $scope.edt.school_code,
                            curr_code: $scope.edt.curr_code,
                            academic_year: $scope.edt.academic_year,
                            grade_code: $scope.edt.grade_code,
                            section_code: $scope.edt.section_code,
                            term_code: $scope.edt.term_code,
                            tent_join_date: $scope.edt.tent_join_date,
                            sims_student_emergency_contact_number1: $scope.edt.sims_student_emergency_contact_number1,
                            sims_student_emergency_contact_name1: $scope.edt.sims_student_emergency_contact_name1,
                            first_name: $scope.edt.first_name,
                            middle_name: $scope.edt.middle_name,
                            last_name: $scope.edt.last_name,
                            family_name: $scope.edt.family_name,
                            first_name_ot: $scope.edt.first_name_ot,
                            midd_name_ot: $scope.edt.midd_name_ot,
                            last_name_ot: $scope.edt.last_name_ot,
                            family_name_ot: $scope.edt.family_name_ot,
                            nicke_name: $scope.edt.nicke_name,
                            birth_date: $scope.edt.birth_date,
                            comm_date: $scope.edt.comm_date,
                            birth_country_code: $scope.edt.birth_country_code,
                            nationality_code: $scope.edt.nationality_code,
                            ethinicity_code: $scope.edt.ethinicity_code,
                            gender_code: $scope.edt.gender_code,
                            religion_code: $scope.edt.religion_code,
                            passport_num: $scope.edt.passport_num,
                            passport_issue_date: $scope.edt.passport_issue_date,
                            passport_expiry: $scope.edt.passport_expiry,
                            passport_issue_auth: $scope.edt.passport_issue_auth,
                            passport_issue_place: $scope.edt.passport_issue_place,
                            visa_number: $scope.edt.visa_number,
                            visa_type: $scope.edt.visa_type,
                            visa_issuing_authority: $scope.edt.visa_issuing_authority,
                            visa_issue_date: $scope.edt.visa_issue_date,
                            visa_expiry_date: $scope.edt.visa_expiry_date,
                            visa_issuing_place: $scope.edt.visa_issuing_place,
                            national_id: $scope.edt.national_id,
                            national_id_issue_date: $scope.edt.national_id_issue_date,
                            national_id_expiry_date: $scope.edt.national_id_expiry_date,
                            sibling_status: $scope.edt.sibling_status,
                            parent_id: $scope.edt.parent_id,
                            sibling_enroll: $scope.edt.sibling_enroll,
                            sibling_name: $scope.edt.sibling_name,
                            sibling_dob: $scope.edt.sibling_dob,
                            sibling_school_code: $scope.edt.sibling_school_code,
                            employee_type: $scope.edt.employee_type,
                            employee_code: $scope.edt.employee_code,
                            employee_school_code: $scope.edt.employee_school_code,
                            motherTounge_language_code: $scope.edt.motherTounge_language_code,
                            main_language_code: $scope.edt.main_language_code,
                            main_language_r_code: $scope.edt.main_language_r_code,
                            main_language_w_code: $scope.edt.main_language_w_code,
                            main_language_s_code: $scope.edt.main_language_s_code,
                            primary_contact_code: $scope.edt.primary_contact_code,
                            primary_contact_pref_code: $scope.edt.primary_contact_pref_code,
                            fee_payment_contact_pref_code: $scope.edt.fee_payment_contact_pref_code,
                            transport_status: $scope.edt.transport_status,
                            transport_desc: $scope.edt.transport_desc,
                            father_salutation_code: $scope.edt.father_salutation_code,
                            father_first_name: $scope.edt.father_first_name,
                            father_middle_name: $scope.edt.father_middle_name,
                            father_last_name: $scope.edt.father_last_name,
                            father_family_name: $scope.edt.father_family_name,
                            father_family_name_ot: $scope.edt.father_family_name_ot,
                            father_nationality1_code: $scope.edt.father_nationality1_code,
                            father_nationality2_code: $scope.edt.father_nationality2_code,
                            father_appartment_number: $scope.edt.father_appartment_number,
                            father_building_number: $scope.edt.father_building_number,
                            father_street_number: $scope.edt.father_street_number,
                            father_area_number: $scope.edt.father_area_number,
                            father_city: $scope.edt.father_city,
                            father_state: $scope.edt.father_state,
                            father_country_code: $scope.edt.father_country_code,
                            father_summary_address: $scope.edt.father_summary_address,
                            father_po_box: $scope.edt.father_po_box,
                            father_phone: $scope.edt.father_phone,
                            father_email: $scope.edt.father_email,
                            father_mobile: $scope.edt.father_mobile,
                            father_fax: $scope.edt.father_fax,
                            father_occupation: $scope.edt.father_occupation,
                            father_company: $scope.edt.father_company,
                            father_passport_number: $scope.edt.father_passport_number,
                            sims_admission_father_national_id: $scope.edt.sims_admission_father_national_id,
                            guardian_salutation_code: $scope.edt.guardian_salutation_code,
                            guardian_first_name: $scope.edt.guardian_first_name,
                            guardian_middle_name: $scope.edt.guardian_middle_name,
                            guardian_last_name: $scope.edt.guardian_last_name,
                            guardian_family_name: $scope.edt.guardian_family_name,
                            guardian_nationality1_code: $scope.edt.guardian_nationality1_code,
                            guardian_nationality2_code: $scope.edt.guardian_nationality2_code,
                            guardian_appartment_number: $scope.edt.guardian_appartment_number,
                            guardian_building_number: $scope.edt.guardian_building_number,
                            guardian_street_number: $scope.edt.guardian_street_number,
                            guardian_area_number: $scope.edt.guardian_area_number,
                            guardian_city: $scope.edt.guardian_city,
                            guardian_state: $scope.edt.guardian_state,
                            guardian_country_code: $scope.edt.guardian_country_code,
                            guardian_summary_address: $scope.edt.guardian_summary_address,
                            guardian_po_box: $scope.edt.guardian_po_box,
                            guardian_phone: $scope.edt.guardian_phone,
                            guardian_mobile: $scope.edt.guardian_mobile,
                            guardian_fax: $scope.edt.guardian_fax,
                            guardian_email: $scope.edt.guardian_email,
                            guardian_occupation: $scope.edt.guardian_occupation,
                            guardian_company: $scope.edt.guardian_company,
                            guardian_passport_number: $scope.edt.guardian_passport_number,
                            guardian_relationship_code: $scope.edt.guardian_relationship_code,
                            mother_salutation_code: $scope.edt.mother_salutation_code,
                            mother_first_name: $scope.edt.mother_first_name,
                            mother_middle_name: $scope.edt.mother_middle_name,
                            mother_last_name: $scope.edt.mother_last_name,
                            mother_family_name: $scope.edt.mother_family_name,
                            mother_name_ot: $scope.edt.mother_name_ot,
                            mother_nationality1_code: $scope.edt.mother_nationality1_code,
                            mother_nationality2_code: $scope.edt.mother_nationality2_code,
                            mother_appartment_number: $scope.edt.mother_appartment_number,
                            mother_building_number: $scope.edt.mother_building_number,
                            mother_street_number: $scope.edt.mother_street_number,
                            mother_area_number: $scope.edt.mother_area_number,
                            mother_city: $scope.edt.mother_city,
                            mother_state: $scope.edt.mother_state,
                            mother_country_code: $scope.edt.mother_country_code,
                            mother_summary_address: $scope.edt.mother_summary_address,
                            mother_po_box: $scope.edt.mother_po_box,
                            mother_phone: $scope.edt.mother_phone,
                            mother_mobile: $scope.edt.mother_mobile,
                            mother_fax: $scope.edt.mother_fax,
                            mother_email: $scope.edt.mother_email,
                            mother_occupation: $scope.edt.mother_occupation,
                            mother_passport_number: $scope.edt.mother_passport_number,
                            current_school_status: $scope.edt.current_school_status,
                            current_school_name: $scope.edt.current_school_name,
                            current_school_enroll_number: $scope.edt.current_school_enroll_number,
                            current_school_grade: $scope.edt.current_school_grade,
                            current_school_cur: $scope.edt.current_school_cur,
                            current_school_from_date: $scope.edt.current_school_from_date,
                            current_school_to_date: $scope.edt.current_school_to_date,
                            current_school_language: $scope.edt.current_school_language,
                            current_school_head_teacher: $scope.edt.current_school_head_teacher,
                            current_school_phone: $scope.edt.current_school_phone,
                            current_school_fax: $scope.edt.current_school_fax,
                            current_school_city: $scope.edt.current_school_city,
                            current_school_country_code: $scope.edt.current_school_country_code,
                            current_school_address: $scope.edt.current_school_address,
                            marketing_code: $scope.edt.marketing_code,
                            marketing_description: $scope.edt.marketing_description,
                            parent_status_code: $scope.edt.parent_status_code,
                            legal_custody_code: $scope.edt.legal_custody_code,
                            health_card_number: $scope.edt.health_card_number,
                            health_card_issue_date: $scope.edt.health_card_issue_date,
                            health_card_issuing_authority: $scope.edt.health_card_issuing_authority,
                            blood_group_code: $scope.edt.blood_group_code,
                            medication_status: $scope.edt.medication_status,
                            medication_desc: $scope.edt.medication_desc,
                            disability_status: $scope.edt.disability_status,
                            disability_desc: $scope.edt.disability_desc,
                            behaviour_status: $scope.edt.behaviour_status,
                            behaviour_desc: $scope.edt.behaviour_desc,
                            health_restriction_status: $scope.edt.health_restriction_status,
                            health_restriction_desc: $scope.edt.health_restriction_desc,
                            health_hearing_status: $scope.edt.health_hearing_status,
                            health_hearing_desc: $scope.edt.health_hearing_desc,
                            health_vision_status: $scope.edt.health_vision_status,
                            health_vision_desc: $scope.edt.health_vision_desc,
                            health_other_status: $scope.edt.health_other_status,
                            health_other_desc: $scope.edt.health_other_desc,
                            gifted_status: $scope.edt.gifted_status,
                            gifted_desc: $scope.edt.gifted_desc,
                            music_status: $scope.edt.music_status,
                            music_desc: $scope.edt.music_desc,
                            sports_status: $scope.edt.sports_status,
                            sports_desc: $scope.edt.sports_desc,
                            language_support_status: $scope.edt.language_support_status,
                            language_support_desc: $scope.edt.language_support_desc,
                            declaration_status: $scope.edt.declaration_status,
                            fees_paid_status: $scope.edt.fees_paid_status,
                            fee_category_code: $scope.edt.fee_category_code,
                            ip: $scope.edt.ip,
                            dns: $scope.edt.dns,
                            user_code: $scope.edt.user_code,
                            status: $scope.edt.status,
                            sims_student_attribute1: $scope.edt.sims_student_attribute1,
                            sims_student_attribute2: $scope.edt.sims_student_attribute2,
                            sims_student_attribute3: $scope.edt.sims_student_attribute3,
                            sims_student_attribute4: $scope.edt.sims_student_attribute4,
                            sims_student_health_respiratory_status: $scope.edt.sims_student_health_respiratory_status,
                            sims_student_health_respiratory_desc: $scope.edt.sims_student_health_respiratory_desc,
                            sims_student_health_hay_fever_status: $scope.edt.sims_student_health_hay_fever_status,
                            sims_student_health_hay_fever_desc: $scope.edt.sims_student_health_hay_fever_desc,
                            sims_student_health_epilepsy_status: $scope.edt.sims_student_health_epilepsy_status,
                            sims_student_health_epilepsy_desc: $scope.edt.sims_student_health_epilepsy_desc,
                            sims_student_health_skin_status: $scope.edt.sims_student_health_skin_status,
                            sims_student_health_skin_desc: $scope.edt.sims_student_health_skin_desc,
                            sims_student_health_diabetes_status: $scope.edt.sims_student_health_diabetes_status,
                            sims_student_health_diabetes_desc: $scope.edt.sims_student_health_diabetes_desc,
                            sims_student_health_surgery_status: $scope.edt.sims_student_health_surgery_status,
                            sims_student_health_surgery_desc: $scope.edt.sims_student_health_surgery_desc,
                            opr: 'I'
                        });

                        data2.push(data);
                        console.log(data2);

                        $http.post(ENV.apiUrl + "api/common/Admission/CUDInsertAdmission", data2).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            console.log($scope.msg1);

                            data.admission_number = $scope.msg1.admission_number;
                            $http.post(ENV.apiUrl + "api/common/Admission/ApproveStudent", data2).then(function (res) {
                                $scope.display = true;
                                $scope.msg1 = res.data;
                                $scope.BUSY = false;
                                var enroll = $scope.msg1[0].enroll;

                                var data1 = $scope.msg1;
                                data1.strMessage = "Sucessfully Admitted Enroll='" + enroll + "' \n Student Name='" + data1[0].stud_full_name + "'\n Parent Id='" + data1[0].parent_id;

                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: data1.strMessage, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                                $scope.edt = [];
                                $scope.myForm.$setPristine();
                                $scope.myForm.$setUntouched();
                                //$rootScope.strMessage = data1.strMessage;
                                //console.log($scope.data1);
                                //$('#message').modal({ backdrop: 'static', keyboard: false });


                            });

                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $scope.edt = [];
                $scope.msg1 = "";

                $('.nav-tabs a[href="#tab1"]').tab('show');
            }

            $scope.Update = function (isvalidate) {
                var data2 = [];
                if (isvalidate) {
                    var data = ({
                        admission_number: $scope.edt.admission_number,
                        appl_num: $scope.edt.appl_num,
                        pros_num: $scope.edt.pros_num,
                        pros_appl_num: $scope.edt.pros_appl_num,
                        admission_date: $scope.edt.admission_date,
                        school_code: $scope.edt.school_code,
                        curr_code: $scope.edt.curr_code,
                        academic_year: $scope.edt.academic_year,
                        grade_code: $scope.edt.grade_code,
                        section_code: $scope.edt.section_code,
                        term_code: $scope.edt.term_code,
                        tent_join_date: $scope.edt.tent_join_date,
                        sims_student_emergency_contact_number1: $scope.edt.sims_student_emergency_contact_number1,
                        sims_student_emergency_contact_name1: $scope.edt.sims_student_emergency_contact_name1,
                        first_name: $scope.edt.first_name,
                        middle_name: $scope.edt.middle_name,
                        last_name: $scope.edt.last_name,
                        family_name: $scope.edt.family_name,
                        first_name_ot: $scope.edt.first_name_ot,
                        midd_name_ot: $scope.edt.midd_name_ot,
                        last_name_ot: $scope.edt.last_name_ot,
                        family_name_ot: $scope.edt.family_name_ot,
                        nicke_name: $scope.edt.nicke_name,
                        birth_date: $scope.edt.birth_date,
                        comm_date: $scope.edt.comm_date,
                        birth_country_code: $scope.edt.birth_country_code,
                        nationality_code: $scope.edt.nationality_code,
                        ethinicity_code: $scope.edt.ethinicity_code,
                        gender_code: $scope.edt.gender_code,
                        religion_code: $scope.edt.religion_code,
                        passport_num: $scope.edt.passport_num,
                        passport_issue_date: $scope.edt.passport_issue_date,
                        passport_expiry: $scope.edt.passport_expiry,
                        passport_issue_auth: $scope.edt.passport_issue_auth,
                        passport_issue_place: $scope.edt.passport_issue_place,
                        visa_number: $scope.edt.visa_number,
                        visa_type: $scope.edt.visa_type,
                        visa_issuing_authority: $scope.edt.visa_issuing_authority,
                        visa_issue_date: $scope.edt.visa_issue_date,
                        visa_expiry_date: $scope.edt.visa_expiry_date,
                        visa_issuing_place: $scope.edt.visa_issuing_place,
                        national_id: $scope.edt.national_id,
                        national_id_issue_date: $scope.edt.national_id_issue_date,
                        national_id_expiry_date: $scope.edt.national_id_expiry_date,
                        sibling_status: $scope.edt.sibling_status,
                        parent_id: $scope.edt.parent_id,
                        sibling_enroll: $scope.edt.sibling_enroll,
                        sibling_name: $scope.edt.sibling_name,
                        sibling_dob: $scope.edt.sibling_dob,
                        sibling_school_code: $scope.edt.sibling_school_code,
                        employee_type: $scope.edt.employee_type,
                        employee_code: $scope.edt.employee_code,
                        employee_school_code: $scope.edt.employee_school_code,
                        motherTounge_language_code: $scope.edt.motherTounge_language_code,
                        main_language_code: $scope.edt.main_language_code,
                        main_language_r_code: $scope.edt.main_language_r_code,
                        main_language_w_code: $scope.edt.main_language_w_code,
                        main_language_s_code: $scope.edt.main_language_s_code,
                        primary_contact_code: $scope.edt.primary_contact_code,
                        primary_contact_pref_code: $scope.edt.primary_contact_pref_code,
                        fee_payment_contact_pref_code: $scope.edt.fee_payment_contact_pref_code,
                        transport_status: $scope.edt.transport_status,
                        transport_desc: $scope.edt.transport_desc,
                        father_salutation_code: $scope.edt.father_salutation_code,
                        father_first_name: $scope.edt.father_first_name,
                        father_middle_name: $scope.edt.father_middle_name,
                        father_last_name: $scope.edt.father_last_name,
                        father_family_name: $scope.edt.father_family_name,
                        father_family_name_ot: $scope.edt.father_family_name_ot,
                        father_nationality1_code: $scope.edt.father_nationality1_code,
                        father_nationality2_code: $scope.edt.father_nationality2_code,
                        father_appartment_number: $scope.edt.father_appartment_number,
                        father_building_number: $scope.edt.father_building_number,
                        father_street_number: $scope.edt.father_street_number,
                        father_area_number: $scope.edt.father_area_number,
                        father_city: $scope.edt.father_city,
                        father_state: $scope.edt.father_state,
                        father_country_code: $scope.edt.father_country_code,
                        father_summary_address: $scope.edt.father_summary_address,
                        father_po_box: $scope.edt.father_po_box,
                        father_phone: $scope.edt.father_phone,
                        father_email: $scope.edt.father_email,
                        father_mobile: $scope.edt.father_mobile,
                        father_fax: $scope.edt.father_fax,
                        father_occupation: $scope.edt.father_occupation,
                        father_company: $scope.edt.father_company,
                        father_passport_number: $scope.edt.father_passport_number,
                        sims_admission_father_national_id: $scope.edt.sims_admission_father_national_id,
                        guardian_salutation_code: $scope.edt.guardian_salutation_code,
                        guardian_first_name: $scope.edt.guardian_first_name,
                        guardian_middle_name: $scope.edt.guardian_middle_name,
                        guardian_last_name: $scope.edt.guardian_last_name,
                        guardian_family_name: $scope.edt.guardian_family_name,
                        guardian_nationality1_code: $scope.edt.guardian_nationality1_code,
                        guardian_nationality2_code: $scope.edt.guardian_nationality2_code,
                        guardian_appartment_number: $scope.edt.guardian_appartment_number,
                        guardian_building_number: $scope.edt.guardian_building_number,
                        guardian_street_number: $scope.edt.guardian_street_number,
                        guardian_area_number: $scope.edt.guardian_area_number,
                        guardian_city: $scope.edt.guardian_city,
                        guardian_state: $scope.edt.guardian_state,
                        guardian_country_code: $scope.edt.guardian_country_code,
                        guardian_summary_address: $scope.edt.guardian_summary_address,
                        guardian_po_box: $scope.edt.guardian_po_box,
                        guardian_phone: $scope.edt.guardian_phone,
                        guardian_mobile: $scope.edt.guardian_mobile,
                        guardian_fax: $scope.edt.guardian_fax,
                        guardian_email: $scope.edt.guardian_email,
                        guardian_occupation: $scope.edt.guardian_occupation,
                        guardian_company: $scope.edt.guardian_company,
                        guardian_passport_number: $scope.edt.guardian_passport_number,
                        guardian_relationship_code: $scope.edt.guardian_relationship_code,
                        mother_salutation_code: $scope.edt.mother_salutation_code,
                        mother_first_name: $scope.edt.mother_first_name,
                        mother_middle_name: $scope.edt.mother_middle_name,
                        mother_last_name: $scope.edt.mother_last_name,
                        mother_family_name: $scope.edt.mother_family_name,
                        mother_name_ot: $scope.edt.mother_name_ot,
                        mother_nationality1_code: $scope.edt.mother_nationality1_code,
                        mother_nationality2_code: $scope.edt.mother_nationality2_code,
                        mother_appartment_number: $scope.edt.mother_appartment_number,
                        mother_building_number: $scope.edt.mother_building_number,
                        mother_street_number: $scope.edt.mother_street_number,
                        mother_area_number: $scope.edt.mother_area_number,
                        mother_city: $scope.edt.mother_city,
                        mother_state: $scope.edt.mother_state,
                        mother_country_code: $scope.edt.mother_country_code,
                        mother_summary_address: $scope.edt.mother_summary_address,
                        mother_po_box: $scope.edt.mother_po_box,
                        mother_phone: $scope.edt.mother_phone,
                        mother_mobile: $scope.edt.mother_mobile,
                        mother_fax: $scope.edt.mother_fax,
                        mother_email: $scope.edt.mother_email,
                        mother_occupation: $scope.edt.mother_occupation,
                        mother_passport_number: $scope.edt.mother_passport_number,
                        current_school_status: $scope.edt.current_school_status,
                        current_school_name: $scope.edt.current_school_name,
                        current_school_enroll_number: $scope.edt.current_school_enroll_number,
                        current_school_grade: $scope.edt.current_school_grade,
                        current_school_cur: $scope.edt.current_school_cur,
                        current_school_from_date: $scope.edt.current_school_from_date,
                        current_school_to_date: $scope.edt.current_school_to_date,
                        current_school_language: $scope.edt.current_school_language,
                        current_school_head_teacher: $scope.edt.current_school_head_teacher,
                        current_school_phone: $scope.edt.current_school_phone,
                        current_school_fax: $scope.edt.current_school_fax,
                        current_school_city: $scope.edt.current_school_city,
                        current_school_country_code: $scope.edt.current_school_country_code,
                        current_school_address: $scope.edt.current_school_address,
                        marketing_code: $scope.edt.marketing_code,
                        marketing_description: $scope.edt.marketing_description,
                        parent_status_code: $scope.edt.parent_status_code,
                        legal_custody_code: $scope.edt.legal_custody_code,
                        health_card_number: $scope.edt.health_card_number,
                        health_card_issue_date: $scope.edt.health_card_issue_date,
                        health_card_issuing_authority: $scope.edt.health_card_issuing_authority,
                        blood_group_code: $scope.edt.blood_group_code,
                        medication_status: $scope.edt.medication_status,
                        medication_desc: $scope.edt.medication_desc,
                        disability_status: $scope.edt.disability_status,
                        disability_desc: $scope.edt.disability_desc,
                        behaviour_status: $scope.edt.behaviour_status,
                        behaviour_desc: $scope.edt.behaviour_desc,
                        health_restriction_status: $scope.edt.health_restriction_status,
                        health_restriction_desc: $scope.edt.health_restriction_desc,
                        health_hearing_status: $scope.edt.health_hearing_status,
                        health_hearing_desc: $scope.edt.health_hearing_desc,
                        health_vision_status: $scope.edt.health_vision_status,
                        health_vision_desc: $scope.edt.health_vision_desc,
                        health_other_status: $scope.edt.health_other_status,
                        health_other_desc: $scope.edt.health_other_desc,
                        gifted_status: $scope.edt.gifted_status,
                        gifted_desc: $scope.edt.gifted_desc,
                        music_status: $scope.edt.music_status,
                        music_desc: $scope.edt.music_desc,
                        sports_status: $scope.edt.sports_status,
                        sports_desc: $scope.edt.sports_desc,
                        language_support_status: $scope.edt.language_support_status,
                        language_support_desc: $scope.edt.language_support_desc,
                        declaration_status: $scope.edt.declaration_status,
                        fees_paid_status: $scope.edt.fees_paid_status,
                        fee_category_code: $scope.edt.fee_category_code,
                        ip: $scope.edt.ip,
                        dns: $scope.edt.dns,
                        user_code: $scope.edt.user_code,
                        status: $scope.edt.status,
                        sims_student_attribute1: $scope.edt.sims_student_attribute1,
                        sims_student_attribute2: $scope.edt.sims_student_attribute2,
                        sims_student_attribute3: $scope.edt.sims_student_attribute3,
                        sims_student_attribute4: $scope.edt.sims_student_attribute4,
                        sims_student_health_respiratory_status: $scope.edt.sims_student_health_respiratory_status,
                        sims_student_health_respiratory_desc: $scope.edt.sims_student_health_respiratory_desc,
                        sims_student_health_hay_fever_status: $scope.edt.sims_student_health_hay_fever_status,
                        sims_student_health_hay_fever_desc: $scope.edt.sims_student_health_hay_fever_desc,
                        sims_student_health_epilepsy_status: $scope.edt.sims_student_health_epilepsy_status,
                        sims_student_health_epilepsy_desc: $scope.edt.sims_student_health_epilepsy_desc,
                        sims_student_health_skin_status: $scope.edt.sims_student_health_skin_status,
                        sims_student_health_skin_desc: $scope.edt.sims_student_health_skin_desc,
                        sims_student_health_diabetes_status: $scope.edt.sims_student_health_diabetes_status,
                        sims_student_health_diabetes_desc: $scope.edt.sims_student_health_diabetes_desc,
                        sims_student_health_surgery_status: $scope.edt.sims_student_health_surgery_status,
                        sims_student_health_surgery_desc: $scope.edt.sims_student_health_surgery_desc,
                        quota_code: $scope.edt.quota_code,
                        opr: 'O'
                    });

                    data2.push(data);

                    $http.post(ENV.apiUrl + "api/common/ProspectDashboard/CUDUpdateProspectAdmission", data2).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        console.log($scope.msg1);
                        if ($scope.msg1.status == true) {
                            swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            });
                        }
                    });
                }
            }

            $scope.modal_cancel = function () {

                $('body').addClass('grey condense-menu');
                $('#main-menu').addClass('mini');
                $('.page-content').addClass('condensed');
                $scope.isCondensed = true;
                $("body").removeClass("modal-open");
                $("div").removeClass("modal-backdrop in");

                console.log($stateParams.Class);
                $state.go("main.Sim567", { Class: $stateParams.Class });

            }

            $scope.back = function () {
                $scope.modal_cancel();
            }

            //checkbox_visibility_start

            $scope.schoolcheck = function () {
                if ($scope.edt.current_school_status == true) {
                    $scope.school_edit = false;
                }
                else {
                    $scope.school_edit = true;
                }
            }

            $scope.chk_desc = function () {
                if ($scope.edt.marketing_code == true) {
                    $scope.desc_edit = false;
                }
                else {
                    $scope.desc_edit = true;
                }
            }

            $scope.chk_cicumstances = function () {
                if ($scope.edt.parent_status_code == true) {
                    $scope.circum_edit = false;
                }
                else {
                    $scope.circum_edit = true;
                }
            }

            $scope.chk_disab_status = function () {
                if ($scope.edt.disability_status == true) {
                    $scope.disab_edit = false;
                }
                else {
                    $scope.disab_edit = true;
                }
            }

            $scope.chk_health_res = function () {
                if ($scope.edt.health_restriction_status == true) {
                    $scope.healthres_edit = false;
                }
                else {
                    $scope.healthres_edit = true;
                }

            }

            $scope.chk_medic_status = function () {
                if ($scope.edt.medication_status == true) {
                    $scope.med_edit = false;
                }
                else {
                    $scope.med_edit = true;
                }
            }

            $scope.chk_other = function () {
                if ($scope.edt.health_other_status == true) {
                    $scope.other_edit = false;
                }
                else {
                    $scope.other_edit = true;
                }
            }

            $scope.chk_hearing = function () {
                if ($scope.edt.health_hearing_status == true) {
                    $scope.hearing_edit = false;
                }
                else {
                    $scope.hearing_edit = true;
                }
            }

            $scope.chk_vision = function () {
                if ($scope.edt.health_vision_status == true) {
                    $scope.health_vision_edit = false;
                }
                else {
                    $scope.health_vision_edit = true;
                }
            }

            $scope.chk_behaviour = function () {
                if ($scope.edt.behaviour_status == true) {
                    $scope.behaviour_edit = false;
                }
                else {
                    $scope.behaviour_edit = true;
                }
            }

            $scope.chk_gifted_status = function () {
                if ($scope.edt.gifted_status == true) {
                    $scope.gifted_edit = false;
                }
                else {
                    $scope.gifted_edit = true;
                }
            }

            $scope.chk_music_pro = function () {
                if ($scope.edt.music_status == true) {
                    $scope.music_edit = false;
                }
                else {
                    $scope.music_edit = true;
                }
            }

            $scope.chk_sport_pro = function () {
                if ($scope.edt.sports_status == true) {
                    $scope.sport_edit = false;
                }
                else {
                    $scope.sport_edit = true;
                }
            }

            $scope.chk_lang_sup = function () {
                if ($scope.edt.language_support_status == true) {
                    $scope.lang_sup_edit = false;
                }
                else {
                    $scope.lang_sup_edit = true;
                }
            }

            $scope.chk_Pri_contact = function () {
                if ($scope.edt.primary_contact_code == true) {
                    $scope.pricont_edit = false;
                }
                else {
                    $scope.pricont_edit = true;
                }
            }

            $scope.chk_transport_requin = function () {
                if ($scope.edt.transport_status == true) {
                    $scope.transreq_edit = false;
                }
                else {
                    $scope.transreq_edit = true;
                }
            }

            $scope.chk_employee = function () {
                if ($scope.edt.employee_type == true) {
                    $scope.emp_edit = false;
                }
                else {
                    $scope.emp_edit = true;
                }
            }

            $scope.chk_studyingin = function () {
                if ($scope.edt.sibling_status == true) {
                    $scope.Study_edit = false;
                    var v = document.getElementById('t3');
                    var v = document.getElementById('t4');
                    var v = document.getElementById('t5');
                    $scope.t = false;
                }
                else {
                    $scope.Study_edit = true;
                    var v = document.getElementById('t3');
                    var v = document.getElementById('t4');
                    var v = document.getElementById('t5');
                    $scope.t = true;
                }
            }

            $scope.chk_ticking = function () {
                if ($scope.edt.declaration_status == true) {
                    $scope.tick_edit = false;
                    $scope.confirm_edit = true;
                }
                else {
                    $scope.tick_edit = true;
                    $scope.confirm_edit = false;
                }
            }

            $scope.chk_copy_mother_add = function () {
                if ($scope.edt.copy_add == true) {
                    $scope.edt.mother_appartment_number = $scope.edt.father_appartment_number;
                    $scope.edt.mother_area_number = $scope.edt.father_area_number;
                    $scope.edt.mother_building_number = $scope.edt.father_building_number;
                    $scope.edt.mother_street_number = $scope.edt.father_street_number;
                    $scope.edt.mother_city = $scope.edt.father_city;
                    $scope.edt.mother_state = $scope.edt.father_state;
                    $scope.edt.mother_country_code = $scope.edt.father_country_code;
                    $scope.edt.mother_po_box = $scope.edt.father_po_box;
                    $scope.edt.mother_nationality1_code = $scope.edt.father_nationality1_code;
                    $scope.edt.mother_nationality2_code = $scope.edt.father_nationality2_code;
                    $scope.edt.mother_summary_address = $scope.edt.father_summary_address;
                    $scope.edt.mother_phone = $scope.edt.father_phone;
                    $scope.edt.mother_po_box = $scope.edt.father_po_box;
                    $scope.edt.mother_mobile = $scope.edt.father_mobile;
                    $scope.edt.mother_occupation = $scope.edt.father_occupation;
                    $scope.edt.mother_email = $scope.edt.father_email;
                    $scope.edt.mother_company = $scope.edt.father_company;
                }
                else {
                    $scope.edt.mother_appartment_number = "";
                    $scope.edt.mother_area_number = "";
                    $scope.edt.mother_building_number = "";
                    $scope.edt.mother_street_number = "";
                    $scope.edt.mother_city = "";
                    $scope.edt.mother_state = "";
                    $scope.edt.mother_country_code = "";
                    $scope.edt.mother_po_box = "";
                    $scope.edt.mother_nationality1_code = "";
                    $scope.edt.mother_nationality2_code = "";
                    $scope.edt.mother_summary_address = "";
                    $scope.edt.mother_phone = "";
                    $scope.edt.mother_po_box = "";
                    $scope.edt.mother_mobile = "";
                    $scope.edt.mother_occupation = "";
                    $scope.edt.mother_email = "";
                    $scope.edt.mother_company = "";
                }

            }

            $scope.chk_copy_guar_add1 = function () {
                if ($scope.edt.copy_add1 == true) {

                    $scope.edt.guardian_appartment_number = $scope.edt.father_appartment_number;
                    $scope.edt.guardian_area_number = $scope.edt.father_area_number;
                    $scope.edt.guardian_building_number = $scope.edt.father_building_number;
                    $scope.edt.guardian_street_number = $scope.edt.father_street_number;
                    $scope.edt.guardian_city = $scope.edt.father_city;
                    $scope.edt.guardian_state = $scope.edt.father_state;
                    $scope.edt.guardian_country_code = $scope.edt.father_country_code;
                    $scope.edt.guardian_po_box = $scope.edt.father_po_box;
                    $scope.edt.guardian_nationality1_code = $scope.edt.father_nationality1_code;
                    $scope.edt.guardian_nationality2_code = $scope.edt.father_nationality2_code;
                    $scope.edt.guardian_summary_address = $scope.edt.father_summary_address;
                    $scope.edt.guardian_phone = $scope.edt.father_phone;
                    $scope.edt.guardian_po_box = $scope.edt.father_po_box;
                    $scope.edt.guardian_mobile = $scope.edt.father_mobile;
                    $scope.edt.guardian_occupation = $scope.edt.father_occupation;
                    $scope.edt.guardian_email = $scope.edt.father_email;
                    $scope.edt.guardian_company = $scope.edt.father_company;
                }
                else {
                    $scope.edt.guardian_appartment_number = "";
                    $scope.edt.guardian_area_number = "";
                    $scope.edt.guardian_building_number = "";
                    $scope.edt.guardian_street_number = "";
                    $scope.edt.guardian_city = "";
                    $scope.edt.guardian_state = "";
                    $scope.edt.guardian_country_code = "";
                    $scope.edt.guardian_po_box = "";
                    $scope.edt.guardian_nationality1_code = "";
                    $scope.edt.guardian_nationality2_code = "";
                    $scope.edt.guardian_summary_address = "";
                    $scope.edt.guardian_phone = "";
                    $scope.edt.guardian_po_box = "";
                    $scope.edt.guardian_mobile = "";
                    $scope.edt.guardian_occupation = "";
                    $scope.edt.guardian_email = "";
                    $scope.edt.guardian_company = "";
                }

            }

            $scope.GetFatherSumAddr = function () {
                var country = document.getElementById('cmb_father_country').options[document.getElementById('cmb_father_country').selectedIndex].text;

                var father_aprt = $("#txt_father_aprt").val().length;
                var father_build = $("#txt_father_build").val().length;
                var father_street = $("#txt_father_Street").val().length;
                var father_area = $("#txt_father_area").val().length;
                var father_city = $("#txt_father_city").val().length;
                var father_state = $("#txt_father_state").val().length;
                var father_country = $("#cmb_father_country").val().length;
                var father_POBox = $("#txt_father_pobox").val().length;

                document.getElementById('txt_father_summary_add').value = $("#txt_father_aprt").val().length > 0 ? document.getElementById('txt_father_aprt').value + "," : "";
                document.getElementById('txt_father_summary_add').value += $("#txt_father_build").val().length > 0 ? document.getElementById('txt_father_build').value + "," : "";
                document.getElementById('txt_father_summary_add').value += $("#txt_father_Street").val().length > 0 ? document.getElementById('txt_father_Street').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_area > 0 ? document.getElementById('txt_father_area').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_city > 0 ? document.getElementById('txt_father_city').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_state > 0 ? document.getElementById('txt_father_state').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_country > 0 ? country + "," : "";
                document.getElementById('txt_father_summary_add').value += father_POBox > 0 ? document.getElementById('txt_father_pobox').value : "";
            }

            $scope.GetMotherSumAddr = function () {
                var mother_aprt = $("#txt_mother_aprt").val().length;
                var mother_build = $("#txt_mother_build").val().length;
                var mother_street = $("#txt_mother_street").val().length;
                var mother_area = $("#txt_mother_area").val().length;
                var mother_city = $("#txt_mother_city").val().length;
                var mother_state = $("#txt_mother_state").val().length;
                var mother_country = $("#cmb_mother_country").val().length;
                var mother_POBox = $("#txt_mother_pobox").val().length;

                var country = document.getElementById('cmb_mother_country').options[document.getElementById('cmb_mother_country').selectedIndex].text;

                document.getElementById('txt_mother_addr').value = mother_aprt > 0 ? document.getElementById('txt_mother_aprt').value + "," : "";
                document.getElementById('txt_mother_addr').value += mother_build > 0 ? document.getElementById('txt_mother_build').value + "," : "";
                document.getElementById('txt_mother_addr').value += mother_street > 0 ? document.getElementById('txt_mother_street').value + "," : "";
                document.getElementById('txt_mother_addr').value += mother_area > 0 ? document.getElementById('txt_mother_area').value + "," : "";
                document.getElementById('txt_mother_addr').value += mother_city > 0 ? document.getElementById('txt_mother_city').value + "," : "";
                document.getElementById('txt_mother_addr').value += mother_state > 0 ? document.getElementById('txt_mother_state').value + "," : "";
                document.getElementById('txt_mother_addr').value += mother_country > 0 ? country + "," : "";
                document.getElementById('txt_mother_addr').value += mother_POBox > 0 ? document.getElementById('txt_mother_pobox').value : "";
            }

            $scope.GetGuardianSumAddr = function () {
                var guar_aprt = $("#txt_guar_aprt").val().length;
                var guar_build = $("#txt_guar_build").val().length;
                var guar_street = $("#txt_guar_street").val().length;
                var guar_area = $("#txt_guar_area").val().length;
                var guar_city = $("#txt_guar_city").val().length;
                var guar_state = $("#txt_guar_state").val().length;
                var guar_country = $("#cmb_gar_country").val().length;
                var guar_POBox = $("#txt_guar_pobox").val().length;

                var country = document.getElementById('cmb_gar_country').options[document.getElementById('cmb_gar_country').selectedIndex].text;

                document.getElementById('txt_guar_addr').value = guar_aprt > 0 ? document.getElementById('txt_guar_aprt').value + "," : "";
                document.getElementById('txt_guar_addr').value += guar_build > 0 ? document.getElementById('txt_guar_build').value + "," : "";
                document.getElementById('txt_guar_addr').value += guar_street > 0 ? document.getElementById('txt_guar_street').value + "," : "";
                document.getElementById('txt_guar_addr').value += guar_area > 0 ? document.getElementById('txt_guar_area').value + "," : "";
                document.getElementById('txt_guar_addr').value += guar_city > 0 ? document.getElementById('txt_guar_city').value + "," : "";
                document.getElementById('txt_guar_addr').value += guar_state > 0 ? document.getElementById('txt_guar_state').value + "," : "";
                document.getElementById('txt_guar_addr').value += guar_country > 0 ? country + "," : "";
                document.getElementById('txt_guar_addr').value += guar_POBox > 0 ? document.getElementById('txt_guar_pobox').value : "";
            }

            //checkbox_visibility_end

            $scope.Cancel = function () {
                $scope.edt = "";
                $scope.msg1 = "";
                data = "";
                data1 = "";
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
        }]);

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
     }])
})();