﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SectionElectiveSubjectDetailsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {


            $scope.temp = {};
            $scope.table1 = true;
            $scope.pager = true;
            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.gradeEmpty = false;
            $scope.isEdit = false;


            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.sectionElectiveSubject;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
                 main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.sectionElectiveSubject, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.sectionElectiveSubject;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_language_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_language_desc_remark.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_language_code == toSearch) ? true : false;
            }

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (response) {
                $scope.curiculums = response.data;
                $scope.getGrade($scope.temp.sims_cur_code);
            });

            $scope.getAcaYear = function (cur_code) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code).then(function (response) {
                    $scope.acaYears = response.data;
                    $scope.getGrade($scope.temp.sims_cur_code,$scope.temp.sims_academic_year);
                });
            }
      
            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.grade = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade').change(function () {
                            $scope.selectedGrade = $(this).val();

                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                    if ($scope.isEdit) {
                    debugger;
                        $http.get(ENV.apiUrl + "api/sectionElectiveSubject/getElectiveGrdSecSubject?langcode=" + $scope.lanuague_code).then(function (response) {
                            $scope.grdsecsub = response.data;
                            setTimeout(function () {
                                $("#cmb_grade").multipleSelect("setSelects", $scope.grdsecsub.gradelist);
                            }, 1000);

                        });
                    }

                });
            }

            $(function () {
                $('#cmb_grade').multipleSelect({
                    width: '100%'
                });
            });

            $scope.getSection = function (curCode,accYear, gradeCode) {
                $http.get(ENV.apiUrl + "api/sectionElectiveSubject/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                    $scope.section = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section').change(function () {
                            $scope.selectedSection = $(this).val();
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1500);
                    if ($scope.isEdit) {
                        setTimeout(function () {
                            $('#cmb_section').multipleSelect("setSelects", $scope.grdsecsub.sectionlist);
                        }, 2000);

                        setTimeout(function () {
                            $('#cmb_subject').multipleSelect("setSelects", $scope.grdsecsub.subjectlist);
                        }, 1000);
                    }
                    
                });
            }

            $(function () {
                $('#cmb_section').multipleSelect({
                    width: '100%'
                });

                $('#cmb_subject').multipleSelect({
                    width: '100%',
                    filter:true
                });
            });
            $scope.selectedsubs = [];

            $http.get(ENV.apiUrl + "api/sectionElectiveSubject/getSubject").then(function (res) {
                $scope.subjects = res.data;
                setTimeout(function () {
                            $('#cmb_subject').change(function () {
                                $scope.selectedsubs = $(this).val();                               
                            }).multipleSelect({
                                width: '100%',
                            });                    
                }, 500);
            });

            $scope.setStatus=function(status){
                $scope.temp.sims_status=status;
            }

            function nonEmpty(arr) {
                return !!arr && arr.length > 0;
            }

            $scope.newAddForm = function () {
                $scope.temp = {};
                $scope.isEdit = false;
                $scope.showAddForm = true;
                $scope.table1 = false;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.quotaDescReadOnly = false;
                $scope.yearReadonly = false;
                $scope.curiculamReadonly = false;
                $scope.languageReadonly = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                if ($scope.curiculums.length > 0) {
                    $scope.temp.sims_cur_code = $scope.curiculums[0].sims_cur_code;
                }
                $scope.temp.sims_status=true;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.temp.sims_cur_code).then(function (response) {
                    $scope.acaYears = response.data;
                    if ($scope.acaYears.length > 0) {
                        $scope.temp.sims_academic_year = $scope.acaYears[0].sims_academic_year;
                    }
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);

                });
                try {$('#cmb_grade').multipleSelect('uncheckAll');} catch (e) {}
                try {$('#cmb_section').multipleSelect('uncheckAll');} catch (e) {}
                try {$('#cmb_subject').multipleSelect('uncheckAll');} catch (e) {}
            }

            $scope.cancelAddForm = function () {
                $scope.temp = {};
                $scope.showAddForm = false;
                $scope.table1 = true;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.getAllSectionElectiveSubject = function () {
                $http.get(ENV.apiUrl + "api/sectionElectiveSubject/getAllSectionElectiveSubject").then(function (response) {
                    $scope.sectionElectiveSubject = response.data;
                    console.log($scope.sectionElectiveSubject);
                    $scope.totalItems = $scope.sectionElectiveSubject.length;
                    $scope.todos = $scope.sectionElectiveSubject;
                    $scope.makeTodos();
                });
            };

            $scope.getAllSectionElectiveSubject();

            $scope.saveSectionElectiveSubject = function (Myform) {
                    if (nonEmpty($scope.selectedGrade)) {
                        if (nonEmpty($scope.selectedSection)) {
                            if (nonEmpty($scope.selectedsubs)) {
                                if ($scope.selectedsubs.length > $scope.temp.sims_elective_subject) {
                                    swal('Select Max' + '&nbsp;' + $scope.temp.sims_elective_subject + '&nbsp;' + 'Subject');
                                    $scope.temp.subject_code = '';
                                    //$('#cmb_subject').multipleSelect("setSelects", ''); 
                                        try {
                                            $('#cmb_subject').multipleSelect('uncheckAll');
                                        } catch (e) {

                                        }
                                } else {
                                    if ($scope.selectedsubs.length < $scope.temp.sims_compulsory_subject) {
                                        swal('Select Compulsory' + '&nbsp;' + $scope.temp.sims_compulsory_subject + '&nbsp;' + 'Subject');
                                    } else {
                                        if (Myform) {
                                            var maindatasend = [];
                                            var demo1 = [];
                                            for (var i = 0; i < $scope.temp.sims_section_code.length; i++) {
                                                var d = {
                                                    sims_grade_code: $scope.temp.sims_section_code[i],
                                                };
                                                demo1.push(d);
                                            }
                                            var demo = [];
                                            for (var i = 0; i < $scope.temp.subject_code.length; i++) {
                                                var d = { 'sims_subject_code': $scope.temp.subject_code[i] }
                                                demo.push(d);
                                            }
                                            for (var i = 0; i < demo1.length; i++) {
                                                for (var j = 0; j < demo.length; j++) {
                                                    var data = {};
                                                    data = {
                                                        sims_grade_code: demo1[i].sims_grade_code,
                                                        subject_code: demo[j].sims_subject_code,
                                                        sims_cur_code: $scope.temp.sims_cur_code,
                                                        sims_academic_year: $scope.temp.sims_academic_year,
                                                        sims_language_code: $scope.temp.sims_language_code,
                                                        sims_language_desc_remark: $scope.temp.sims_language_desc_remark,
                                                        sims_elective_subject: $scope.temp.sims_elective_subject,
                                                        sims_compulsory_subject: $scope.temp.sims_compulsory_subject,
                                                        sims_status:$scope.temp.sims_status
                                                    }
                                                    maindatasend.push(data);
                                                }
                                            } 
                                            $http.post(ENV.apiUrl + "api/sectionElectiveSubject/addSectionElectiveSubject", maindatasend).then(function (msg) {
                                                $scope.msg1 = msg.data;
                                                swal({ text: $scope.msg1.strMessage });
                                                $scope.temp = '';
                                                $scope.getAllSectionElectiveSubject()
                                                $scope.table1 = true;
                                                $scope.showAddForm = false;
                                            });
                                        }
                                    }
                                }
                            } else {
                                swal('Please select Subject');
                            }
                        } else {
                            swal('Please select Section');
                        }
                    } else {
                        swal('Please select Grade');
                    }

            }
         
            $scope.editSectionElectiveSubject = function (edt) {
                
                $scope.isEdit = true;
                $scope.editobj = edt;
                $scope.lanuague_code = edt.sims_language_code;
                console.log(edt)
                $scope.temp = {
                    sims_cur_code: edt.sims_cur_code,
                    sims_academic_year: edt.sims_academic_year,
                    sims_language_code: edt.sims_language_desc,
                    sims_language_desc_remark: edt.sims_language_desc_remark,
                    sims_elective_subject: edt.sims_elective_subject,
                    sims_compulsory_subject: edt.sims_compulsory_subject,
                    sims_status: edt.sims_status
                };
                $scope.getAcaYear(edt.sims_cur_code);
               $scope.quotaDescReadOnly = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.showAddForm = true;
                $scope.table1 = false;
                $scope.yearReadonly = true;
                $scope.curiculamReadonly = true;
                $scope.languageReadonly = true;
             
            }

            $scope.updateAdmissionData = function (Myform) {
                if (nonEmpty($scope.selectedGrade)) {
                    if (nonEmpty($scope.selectedSection)) {
                        if (nonEmpty($scope.selectedsubs)) {
                            if ($scope.selectedsubs.length > $scope.temp.sims_elective_subject) {
                                swal('Select Max' + '&nbsp;' + $scope.temp.sims_elective_subject + '&nbsp;' + 'Subject');
                                $scope.temp.subject_code = '';
                                $('#cmb_subject').multipleSelect("setSelects", '');
                            } else {
                                if ($scope.selectedsubs.length < $scope.temp.sims_compulsory_subject) {
                                    swal('Select Compulsory' + '&nbsp;' + $scope.temp.sims_compulsory_subject + '&nbsp;' + 'Subject');
                                } else {
                                    if (Myform) {
                                        var maindatasend = [];
                                        var demo1 = [];
                                        for (var i = 0; i < $scope.temp.sims_section_code.length; i++) {
                                            var d = {
                                                sims_grade_code: $scope.temp.sims_section_code[i],
                                            };
                                            demo1.push(d);
                                        }
                                        var demo = [];
                                        for (var i = 0; i < $scope.temp.subject_code.length; i++) {
                                            var d = { 'sims_subject_code': $scope.temp.subject_code[i] }
                                            demo.push(d);
                                        }
                                        for (var i = 0; i < demo1.length; i++) {
                                            for (var j = 0; j < demo.length; j++) {
                                                var data = {};
                                                data = {
                                                    sims_grade_code: demo1[i].sims_grade_code,
                                                    subject_code: demo[j].sims_subject_code,
                                                    sims_cur_code: $scope.temp.sims_cur_code,
                                                    sims_academic_year: $scope.temp.sims_academic_year,
                                                    sims_language_code: $scope.temp.sims_language_code,
                                                    sims_language_desc_remark: $scope.temp.sims_language_desc_remark,
                                                    sims_elective_subject: $scope.temp.sims_elective_subject,
                                                    sims_compulsory_subject: $scope.temp.sims_compulsory_subject,
                                                    sims_status: $scope.temp.sims_status
                                                }
                                                maindatasend.push(data);
                                            }
                                        }
                                        $http.post(ENV.apiUrl + "api/sectionElectiveSubject/updateSectionElectiveSubject", maindatasend).then(function (msg) {
                                            $scope.msg1 = msg.data;
                                            swal({ text: $scope.msg1.strMessage });
                                            $scope.getAllSectionElectiveSubject()
                                            $scope.table1 = true;
                                            $scope.showAddForm = false;

                                        });

                                    } else {

                                    }
                                }
                            }
                        } else {
                            swal('Please select Subject');
                        }
                    } else {
                        swal('Please select Section');
                    }
                } else {
                    swal('Please select Grade');
                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_language_code);// + $scope.filteredTodos[i].comn_app_mode
                    if (main.checked == true) {
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                    else {
                        v.checked = false;
                        main.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }
            }

            $scope.checkValue = function (comp, max) {
                var a = parseInt(comp)
                var b = parseInt(max)
                if (a > b) {
                    swal('Compulsory selection is must be less than or same as Max selection');
                    $scope.temp.sims_compulsory_subject = '';
                }
            }

            $scope.Delete = function () {
                var DeleteData = '';
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = document.getElementById($scope.filteredTodos[i].sims_language_code);
                    if (t.checked == true) {
                        $scope.flag = true;
                        DeleteData = DeleteData + $scope.filteredTodos[i].sims_language_code + ',';
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            var data = { sims_language_code: DeleteData };                           
                            $http.post(ENV.apiUrl + "api/sectionElectiveSubject/SectionElectiveSubjectDelete?sims_language_code=" + DeleteData).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1) {
                                    swal({ text: $scope.msg1.strMessage });
                                    main = document.getElementById('mainchk');
                                    if (main.checked == true) {
                                        main.checked = false;
                                    }
                                    $scope.getAllSectionElectiveSubject();
                                }
                                else {
                                    swal({ text: $scope.msg1.strMessage });
                                }
                            });
                        }
                        else {

                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            DeleteData = '';
                            $scope.CheckAllChecked();
                        }
                    });
                }
                else {
                    DeleteData = '';
                    swal({ text: 'Select At Least One Record To Delete', width: 380, showCloseButton: true });
                }
            }
         }])
})();
