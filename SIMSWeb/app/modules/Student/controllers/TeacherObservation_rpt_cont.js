﻿(function () {
    'use strict';
    var obj1, temp, opr, comp, flag, current_date;
    var main, deletefin = [], temp = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TeacherObservation_rpt_cont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Update_btn = true;
            $scope.display = true;
            $scope.table = true;
            $scope.agenda_grid = false;
            $scope.PageSize_Search = false;
            $scope.pageSize_txt = false;
            $scope.search_txt = false;
            $scope.hideGraph = true;
            
          
            var username = $rootScope.globals.currentUser.username;

            $scope.CurrentDate = new Date();
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();
            current_date = yyyy + '-' + mm + '-' + dd;


            console.log("Current Date");
            console.log(current_date);
            // var cur_year = yyyy;

            var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');
            $scope.edt = {
                sims_agenda_sdate: dateyear,
                sims_agenda_edate: dateyear
            }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd-mm-yyyy'
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#test").tableHeadFixer({ 'top': 1 });
            }, 100);
            $scope.downloaddoc = function (str) {
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Agenda/' + str;
                window.open($scope.url);
            }

            $scope.compareDate = function () {
                if ($scope.temp.sims_agenda_sdate > $scope.temp.sims_agenda_edate) {
                    swal({ title: "Alert", text: "End date should be greater than Start date", showCloseButton: true, width: 300, height: 200 });
                    $scope.temp.sims_agenda_edate = "";
                }
            }

            $scope.clear = function () {
                $scope.temp = '';
                $scope.filteredTodos = [];
                $scope.agenda_grid = false;
                $scope.PageSize_Search = false;
                $scope.pageSize_txt = false;
                $scope.search_txt = false;
            }

            $http.get(ENV.apiUrl + "api/Agenda/getAll_Agenda_Details").then(function (AllagendaData) {
                $scope.agenda_Details = AllagendaData.data;
                $scope.totalItems = $scope.agenda_Details.length;
                $scope.todos = $scope.agenda_Details;
                $scope.makeTodos();
                console.log($scope.agenda_Details);
            });

            $scope.printDiv = function (div) {

                var docHead = document.head.outerHTML;
                var printContents = document.getElementById(div).outerHTML;
                var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

                var newWin = window.open("", "_blank", winAttr);
                var writeDoc = newWin.document;
                writeDoc.open();
                writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                writeDoc.close();
                newWin.focus();
            }

            $scope.exportAction = function () {
                $('#test').tableExport({ type: 'pdf', pdfFontSize: '7', escape: 'false' });
            }

            $scope.exportData = function () {
                swal({
                    title: '',
                    text: "Are you sure you want to Save?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {//exportable
                        var blob = new Blob([document.getElementById('example_wrapper').innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        saveAs(blob, "Report.xls");
                    }
                });
            };

            $scope.expand = function (j, $event) {
                //console.log(j);
                if (j.sims_isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.sims_isexpanded = "grid";
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.sims_isexpanded = "none";
                }
            }

            $scope.teacher_data = function (cur, acYear, gCode, sCode, subCode, sdt, edt) {
               
                $http.get(ENV.apiUrl + "api/getComnFile/getTeacherObservation_ASD?cur_code=" + cur + "&acad_year=" + acYear + "&grade=" + gCode + "&section=" + sCode + "&subject=" + subCode + "&from_date=" + sdt + "&to_date=" + edt).then(function (teacher_Details) {
                    debugger;
                    $scope.teacher_Details = teacher_Details.data;
                   
                    if ($scope.teacher_Details.length > 0) {
                        $scope.totalItems = $scope.teacher_Details.length;
                        $scope.todos = $scope.teacher_Details;
                        $scope.makeTodos();
                        $scope.PageSize_Search = true;
                        $scope.pageSize_txt = true;
                        $scope.search_txt = true;
                        $scope.agenda_grid = true;
                    }
                    else {
                        swal({ title: "Alert", text: "No Records Found", width: 300, height: 200 });
                    }
                });

                

            }

            $scope.close = function () {
                debugger;
                alert('hi');
                $scope.hide_teacher =false;
            }

            $scope.show_Description = function (str, str1,grade,section,sdate,edate, str2,dispaly_code) {
                $scope.hideGraph = false;
                //$('#teacher_desc').modal('show');
                $('#teacher_desc').modal({ backdrop: 'static', keyboard: false });
                debugger;
                $http.get(ENV.apiUrl + "api/getComnFile/getTeacherObservationLink_ASD?sims_cur_code=" + str + "&sims_academic_year=" + str1 + "&grade=" + grade + "&section=" + section + "&from_date=" + sdate + "&to_date=" + edate + "&sims_lesson_subject_code=" + str2 + "&display_code=" + dispaly_code).then(function (teacher_desc) {
                    $scope.teacher_desc = teacher_desc.data;
                });
            }

           

            function getCur(flag, comp_code) {
                if (flag) {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        $scope.curriculum = res.data;
                        $scope.temp = {
                            'sims_cur_code': $scope.curriculum[0].sims_cur_code
                        }
                        $scope.getAccYear($scope.curriculum[0].sims_cur_code);
                    });
                }
                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        $scope.curriculum = res.data;
                        $scope.temp = {
                            'sims_cur_code': $scope.curriculum[0].sims_cur_code
                        }
                        $scope.getAccYear($scope.curriculum[0].sims_cur_code);

                    });


                }

            }

            $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.global_count_comp = res.data;

                if ($scope.global_count_comp) {
                    getCur(true, $scope.user_details.comp);


                }
                else {
                    getCur(false, $scope.user_details.comp)
                }
            });













            $http.get(ENV.apiUrl + "api/common/Subject/getSubject").then(function (Allsubjects) {
                debugger;
                $scope.All_subjects = Allsubjects.data;
                setTimeout(function () {
                    $('#Sub_id').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%',
                    });
                    $("#Sub_id").multipleSelect("checkAll");
                }, 1000);
            });
            
            

            $scope.getAccYear = function (cur_code) {

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    //$scope.temp['sims_academic_year'] = $scope.Acc_year[0].sims_academic_year;
                    $scope.temp.sims_academic_year = $scope.Acc_year[0].sims_academic_year;
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                });
            }

            $scope.getGrade = function (curCode, accYear) {

                    $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                        $scope.Grade_code = Gradecode.data;
                        setTimeout(function () {
                            $('#grade_id').change(function () {
                                console.log($(this).val());
                            }).multipleSelect({
                                width: '100%',
                            });
                            $("#grade_id").multipleSelect("checkAll");
                        }, 1000);
            
                    });
            }
            $(function () {
                $('#grade_id').multipleSelect({
                    width: '100%'
                });
            });

            $scope.getSection = function (curCode, gradeCode, accYear) {
               
                    $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                        $scope.Section_code = Sectioncode.data;

                        setTimeout(function () {
                            $('#sec_id').change(function () {
                                console.log($(this).val());
                            }).multipleSelect({
                                width: '100%',
                            });
                            $("#sec_id").multipleSelect("checkAll");
                        }, 1000);
                    });

                             
            }




            $scope.showGraphModal = function (cur, acYear, gCode, sCode, subCode, sdt, edt) {
                debugger;
                $scope.hideGraph = true;
                $('#secondgraphModal').modal({ backdrop: 'static', keyboard: false });
                $scope.dash2_observation = [];
                $scope.dash2_outstanding = [];
                $scope.dash2_very_good = [];
                $scope.dash2_good = [];
                $scope.dash2_acceptable = [];
                $scope.dash2_very_weak = [];
                $scope.dash2_weak = [];
                $scope.dash2_subject = [];
               

                $http.get(ENV.apiUrl + "api/getComnFile/getTeacherObservation_ASD?cur_code=" + cur + "&acad_year=" + acYear + "&grade=" + gCode + "&section=" + sCode + "&subject=" + subCode + "&from_date=" + sdt + "&to_date=" + edt).then(function (res) {
                    for (var i = 0; i < res.data.length; i++) {
                        
                        $scope.dash2_observation.push(res.data[i].observation);
                        $scope.dash2_outstanding.push(res.data[i].outstanding);
                        $scope.dash2_very_good.push(res.data[i].very_good);
                        $scope.dash2_good.push(res.data[i].good);
                        $scope.dash2_acceptable.push(res.data[i].acceptable);
                        $scope.dash2_very_weak.push(res.data[i].very_weak);
                        $scope.dash2_weak.push(res.data[i].weak);
                        $scope.dash2_subject.push(res.data[i].sims_subject_name_en);
                            
                       
                    }

                    var ctx = document.getElementById("studStsChart2").getContext("2d");
                    ctx.canvas.parentNode.style.height = '400px';
                    var modaldata = {
                        labels: $scope.dash2_observation,
                        datasets: [
               
                          {
                              label: "Outstanding",
                              backgroundColor: "#7f7f7f",
                              borderColor: "#7f7f7f",
                              data: $scope.dash2_outstanding
                          },
                          
                          {
                              label: "Very Good",
                              backgroundColor: "#9467bd",
                              borderColor: "#9467bd",
                              data: $scope.dash2_very_good
                          },
                          {
                              label: "Good",
                              backgroundColor: "#2ca02c",
                              borderColor: "#2ca02c",
                              data: $scope.dash2_good
                          },
                            {
                                label: "Acceptable",
                                backgroundColor: "#d62728",
                                borderColor: "#d62728",
                                data: $scope.dash2_acceptable
                            },
                            {
                                label: "Very Weak",
                                backgroundColor: "#ff7f0e",
                                borderColor: "#ff7f0e",
                                data: $scope.dash2_very_weak
                            },

                            {
                                label: "Weak",
                                backgroundColor: "#1f77b4",
                                borderColor: "#1f77b4",
                                data: $scope.dash2_weak
                            },
                        ]
                    };


                    setTimeout(function () {
                        window.bar6 = new Chart(ctx, {
                            type: 'bar',
                            data: modaldata,
                            options: {
                                responsive: true,
                                maintainAspectRatio: false,
                                //scales: {
                                //    xAxes: [{
                                //        stacked: true
                                //    }],
                                //    yAxes: [{
                                //        stacked: true
                                //    }]
                                //},
                                legend: {
                                    display: true,
                                    labels: {
                                        boxWidth: 10,
                                        fontSize: 10
                                    }
                                },
                                title: {
                                    display: true,
                                    text: $scope.dash2_subject,
                                    position: 'bottom'
                                   
                                }
                            }
                        });

                    }, 300);

                });
            }


       





            

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    $scope.filteredTodos[i].sims_icon = 'fa fa-plus-circle';
                }
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.agenda_Details, $scope.searchText);
                $scope.totalItems = $scope.agenda_Details.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.agenda_Details;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_agenda_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_subject_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_agenda_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_agenda_doc == toSearch) ? true : false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd",
            });
            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
        }])

    simsController.filter('titleCase', function () {
        return function (input) {
            input = input || '';
            return input.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
        };
    });

})();
