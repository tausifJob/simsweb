﻿(function () {
    'use strict';
    var teacher_code = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });



    simsController.controller('sibling_detailsCont',
    ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
        $scope.display = true;
        $scope.busyindicator = false;
        $scope.show_ins_table_loading = false;

        $http.get(ENV.apiUrl + "api/LibraryAttribute/getCuriculum").then(function (res1) {
            $scope.cur_data = res1.data;
            $scope.sims_Cur = $scope.cur_data[0].sims_cur_code;
            $scope.getAcademic_year($scope.cur_data[0].sims_Cur);
        });
               $scope.getAcademic_year = function (cur_code1) {
                   debugger
                   $http.get(ENV.apiUrl + "api/LibraryAttribute/getAcademicYear?curCode=" + cur_code1).then(function (res1) {
                       $scope.acad_data = res1.data;
                       $scope.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                   });
            }

        $http.get(ENV.apiUrl + "api/LibraryAttribute/getshow").then(function (res1) {
            $scope.show_data = res1.data;
            $scope.sims_appl_parameter = $scope.show_data[0].sims_cur_code;
        });
      
        $scope.getload_data = function (show_report1, cur_code1, acad_year1, student_count1,search1) {
           
            $http.get(ENV.apiUrl + "api/LibraryAttribute/getload_data?show_report=" + show_report1 + "&cur_code=" + cur_code1 + "&acad_year=" + acad_year1 + "&student_count=" + student_count1 + "&search=" + search1).then(function (res1) {
                $scope.stud_data = res1.data;
                console.log("mydata", $scope.stud_data);
            });
        }

        $timeout(function () {
            $("#customers").tableHeadFixer({ 'top': 1 });
        }, 100);

        $scope.exportData = function () {
            swal({
                title: "Alert",
                text: "Do you want to Export in MS-Excel?",
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonText: 'Yes',
                width: 380,
                cancelButtonText: 'No',
            }).then(function (isConfirm) {
                if (isConfirm) {
                    var blob = new Blob([document.getElementById('Div1').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, "Report.xls");
                }
            });
        };
     }]);
})();