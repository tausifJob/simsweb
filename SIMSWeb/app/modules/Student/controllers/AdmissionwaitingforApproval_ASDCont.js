﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AdmissionwaitingforApproval_ASDCont',
        ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.display = true;
            $scope.grid = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.pagesize1 = "5";
            $scope.pageindex1 = "1";
            $scope.flag = true;
            var str, cnt;
            $scope.dash = [];
            $scope.view = [];
            var admdetails = [];
            $scope.btn_edit = true;
            var del = [];
            var t = false;
            $scope.comm_btn = true;
            $scope.pros_btn = true;
            $scope.display1 = false;
            $scope.grid1 = true;
            var main, section = "", fee_category = "";
            $scope.filesize = true;
            var date = new Date();
            $scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.once = true;
            var param = $stateParams.Class;

            var grade = "";
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 20, $scope.maxSize = 20;
            $scope.filteredTodos1 = [], $scope.currentPage1 = 1, $scope.numPerPage1 = 5, $scope.maxSize1 = 5;

            $timeout(function () {
                $("#fixedtable,#fixedtable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            if (param != undefined) {
                $scope.edt =
                    {
                        curr_code: param.curr_code,
                        academic_year: param.academic_year,
                        grade_code: param.grade_code,
                        sims_appl_parameter_reg: param.sims_appl_parameter_reg
                    };
            }

            $(function () {
                $('#cmb_grade').multipleSelect({
                    width: '100%',
                    placeholder: "Select Grade"
                });
            });

            // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {

                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;

                $scope.propertyName = propertyName;
            };

            $scope.countData = [

                  { val: 5, data: 5 },
                  { val: 10, data: 10 },
                  { val: 15, data: 15 },

            ]

            $scope.Cancel = function () {
                $scope.view = "";
            }

            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.obj2 = res.data;
                $scope.edt['curr_code'] = $scope.obj2[0].sims_attendance_cur_code;
                $scope.curr_code = $scope.obj2[0].sims_attendance_cur_code;
                $scope.getCur($scope.edt.curr_code);
            });

            $scope.getCur = function (cur_code) {
                $http.get(ENV.apiUrl + "api/common/getAllAcademicYearDash?cur_code=" + cur_code).then(function (res) {
                    $scope.obj1 = res.data;
                    $scope.edt['academic_year'] = $scope.obj1[0].sims_academic_year;
                    $scope.acad_yr = $scope.obj1[0].sims_academic_year;
                    $scope.GetGrade(cur_code, $scope.acad_yr, $scope.edt.grade_code, $scope.edt.sims_appl_parameter_reg);
                });
            }



            //Get Dashboard Details

            $scope.GetInfo = function (cur_code, AcadmicYear, gradeCode, reg) {// + "&gender_code=" + '' + "&nationality_code=" + '' + "&employee_code=" + '' + "&sibling_enroll=" + ''
                $scope.filteredTodos = [];
                $scope.dash = [];
                debugger;
                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/GetStudents?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode + "&admission_status=" + 'S').then(function (res) {
                    $scope.dash = res.data;
                    console.log('length'+ $scope.dash.length);
                    if ($scope.dash.length > 0) {
                        $scope.table = true;
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.dash.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.dash.length, data: 'All' })
                        }
                        $scope.totalItems = $scope.dash.length;
                        $scope.todos = $scope.dash;
                        $scope.grid = true;
                        $scope.makeTodos();
                    }
                    else {
                        $scope.table = false;
                        $scope.todos = [];
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                        $scope.filteredTodos = [];
                    }
                });

            }



            $scope.GetGrade = function (cur, acad_yr, grade, reg) {
                reg = "";
                //$http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + cur + "&academic_year=" + acad_yr).then(function (res) {
                //    $scope.obj3 = res.data;
                //});

                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/Get_Grade_CodebyCuriculum?cur_code=" + cur + "&acad_yr=" + acad_yr).then(function (res) {
                    $scope.grade = res.data;
                    setTimeout(function () {
                        $('#cmb_grade').change(function () {
                            //  console.log($(this).val());
                            grade = $(this).val();
                        }).multipleSelect({
                            width: '100%',
                        });
                    }, 1000);
                });

            }

            $scope.GetGrade(param.curr_code, param.academic_year);

            $scope.GetGradechage = function (cur, acad_yr, grade, reg) {
                $scope.edt.sims_appl_parameter_reg = "";
                reg = "";


            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                // main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.dash, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.dash;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.admission_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.stud_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            //$scope.GetInfo(param.curr_code, param.academic_year, param.grade_code, param.sims_appl_parameter_reg);
            $scope.temp1 = [];
            $scope.check1 = function (dash) {
                console.log(dash);
                $scope.temp1 = dash;
                admdetails = dash.admission_number;
                var v = document.getElementById(dash.admission_number);


                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked

                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                        // }
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });
                // }

            }

            var data2 = [];
            var data;
            //Promote the Admissions
            $scope.getPromote = function () {
                debugger
                admdetails = [];
                data2 = [];
                var cnt = 0;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].admission_number1 == true) {
                        //admdetails = admdetails + $scope.filteredTodos[i].admission_number + ',';
                        admdetails =$scope.filteredTodos[i].admission_number ;

                        cnt = cnt + 1;
                    }
                }
                if (admdetails.length == 0) {
                    swal({ title: "Alert", text: "Nothing Selected", showCloseButton: true, width: 380, });
                    return;
                }
                else if (cnt == 1) {
                    data = $scope.temp1;
                    data.admission_number = admdetails;
                    data2.push(data);
                    //  console.log(data);
                    if (admdetails.length > 0) {
                        $http.post(ENV.apiUrl + "api/common/Admission_dpsd/ApproveStudent", data2).then(function (res) {
                            $scope.msg1 = res.data;
                            //   console.log($scope.promote);
                        });

                        $scope.grid1 = true;
                        $scope.div_okcancel = false;
                        $('#myModal1').modal({ backdrop: 'static', keyboard: true });
                    }
                }
                else if (cnt > 1) {
                    swal({ title: "Alert", text: "Please select atleast one Record.", showCloseButton: true, width: 380, });
                    return;
                }
                $scope.GetInfo($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.edt.sims_appl_parameter_reg);
            }


            //confirm and collect fee
            $scope.get_studentfee = function () {
                var cnt = 0;
                admdetails = [];
                data2 = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].admission_number1 == true) {
                        admdetails = admdetails + $scope.filteredTodos[i].admission_number + ',';
                        cnt = cnt + 1;

                    }
                }
                if (admdetails.length == 0) {
                    swal({ title: "Alert", text: "Nothing Selected", showCloseButton: true, width: 380, });
                    return;
                }
                else if (cnt == 1) {
                    data = $scope.temp1;
                    data.admission_number = admdetails;
                    data2.push(data);
                    console.log(data2);
                    if (admdetails.length > 0) {
                        $http.post(ENV.apiUrl + "api/common/Admission_dpsd/ApproveStudent", data2).then(function (res) {
                            $scope.msg1 = res.data;
                            console.log($scope.msg1);
                        });

                        $scope.grid1 = true;
                        $scope.div_okcancel = true;
                        $('#myModal1').modal({ backdrop: 'static', keyboard: true });
                    }
                }
                else if (cnt > 1) {
                    swal({ title: "Alert", text: "Please select atleast one Record.", showCloseButton: true, width: 380, });
                    return;
                }

            }

            $scope.modal_okcancel_new = function () {
                $('#myModal1').modal('hide');
            }

            $scope.modal_okcancel = function () {
                $('#myModal1').modal('hide');

                if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                    $timeout(function () {
                        $state.go("main.SiVATN");
                    }, 100);
                }
             else   if ($http.defaults.headers.common['schoolId'] == 'geis') {
                    $timeout(function () {
                        $state.go("main.Sim043");
                    }, 100);
                }

             else if ($http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvps') {
                    $timeout(function () {
                        $state.go("main.Sim43N");
                    }, 100);
                }
                else {
                    $timeout(function () {
                        $state.go("main.SiVATN");
                    }, 100);
                }
            }

            $scope.New = function () {
                $scope.display1 = true;
                $scope.grid1 = false;
                $scope.temp = "";
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
        }])


})();