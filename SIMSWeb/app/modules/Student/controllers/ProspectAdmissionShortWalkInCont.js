﻿
(function () {
    'use strict';
    var del = [];
    var main;
    var date1, date3, date4;

    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ProspectAdmissionShortWalkInCont',['$scope', '$state', '$stateParams', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $stateParams, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {
          
        $scope.save1 = true;
        $scope.Previous_School = true;

        if ($http.defaults.headers.common['schoolId'] == 'tiadxb' || $http.defaults.headers.common['schoolId'] == 'tiashj') {
            $scope.Previous_School = false;

        }

        $scope.back = function () {
            $scope.modal_cancel();
        }


        $scope.modal_cancel = function () {

            console.log($stateParams.Class);
            $state.go("main.Prsiso", { Class: $stateParams.Class });

        }

            $scope.obj_age = [];
          
        
            
            $scope.edt = {};

            $http.get(ENV.apiUrl + "api/common/ProspectDashboard/GetAcademicYear").then(function (res) {
                $scope.academicYears = res.data
                $scope.edt['academic_year'] = res.data[0].academic_year;
                $scope.getAcademicYear($scope.edt['academic_year']);
            });


            $http.get(ENV.apiUrl + "api/common/ProspectDashboard/GetAboutUsData").then(function (res) {
                $scope.about_lst = res.data
            });

            $http.get(ENV.apiUrl + "api/common/ProspectDashboard/GetPreviousSchool").then(function (res) {
                $scope.school_lst = res.data
            });

            $scope.getAcademicYear = function (currYear) {
                $http.get(ENV.apiUrl + "api/common/ProspectDashboard/GetAllGradeSec?currYear=" + currYear).then(function (res) {
                    $scope.allgradesSection = res.data
                });
            }

            var param = $stateParams.Pros_num;
            $scope.copy_per = angular.copy($stateParams.Pros_num);

            if (param != '[object Object]') {
                $http.get(ENV.apiUrl + "api/common/ProspectDashboard/GetTabStudentData?prospect_number=" + $scope.copy_per).then(function (res) {
                    debugger
                   
                        $scope.pro_data = res.data;

                        $scope.edt = {
                            sname: $scope.pro_data.first_name,
                            gender: $scope.pro_data.gender_code,
                            dob1: $scope.pro_data.birth_date,
                            grade: $scope.pro_data.grade_code,
                            pname: $scope.pro_data.father_first_name,
                            mobile: $scope.pro_data.father_mobile,
                            email: $scope.pro_data.father_email,
                            academic_year: $scope.pro_data.academic_year,
                            referal_code: $scope.pro_data.marketing_description,
                            school_code: $scope.pro_data.current_school_name,
                            pros_number: $scope.pro_data.admission_number,
                            

                        }
                        $scope.getAcademicYear($scope.pro_data.academic_year);

                    console.log(res.data);
                
                    $scope.update1 = true;
                    $scope.save1 = false;
                    $scope.btn_back = true;
                });
            }



            $scope.RegisterUser = function (edt,flg) {
                //  $scope.getPassword()
                if (flg) {
                    $scope.sendData = [];
                    console.log(edt);
                    console.log($scope.password);;

                    var day = edt.dob1.split('-')[0];
                    var month = edt.dob1.split('-')[1];
                    var year = edt.dob1.split('-')[2];

                    edt.dob = year + '-' + month + '-' + day;
                    // edt.password = $scope.password;

                    $http.post(ENV.apiUrl + "api/common/ProspectDashboard/AddEnquirySisoNew", edt).success(function (data) {
                        $scope.p_data = data;

                        if ($scope.p_data) {
                            // swal({ text: " Sucessfully" });
                            $scope.edt = {}
                            swal({ title: "Registered Successfully", text: "Parent will be getting an email from the school shortly. ", imageUrl: "assets/img/check.png", width: 300, height: 200  });

                            $scope.myForm.$setPristine();
                            $scope.myForm.$setUntouched();
                        } else {
                            // swal({ text: "Not Registered" });
                            swal({ title: "Alert", text: " Not Registered", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });

                        }

                    });
                }
            }


            $scope.UpdateUser = function (edt, flg) {
                //  $scope.getPassword()
                if (flg) {
                    $scope.sendData = [];
                    console.log(edt);
                    console.log($scope.password);;

                    var day = edt.dob1.split('-')[0];
                    var month = edt.dob1.split('-')[1];
                    var year = edt.dob1.split('-')[2];

                    edt.dob = year + '-' + month + '-' + day;
                    // edt.password = $scope.password;

                    $http.post(ENV.apiUrl + "api/common/ProspectDashboard/UpdateEnquirySisoNew", edt).success(function (data) {
                        $scope.p_data = data;

                        if ($scope.p_data) {
                            // swal({ text: " Sucessfully" });
                           // $scope.edt = {}
                            swal({ title: "Alert", text: "Updated Successfully. ", imageUrl: "assets/img/check.png", width: 300, height: 200 });

                            //$scope.myForm.$setPristine();
                            //$scope.myForm.$setUntouched();
                        } else {
                            // swal({ text: "Not Registered" });
                            swal({ title: "Alert", text: " Not Updated", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });

                        }

                    });
                }
            }


            $scope.Cancel = function () {
                $scope.edt = {}
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.getchkAge = function () {
                $http.get(ENV.apiUrl + "api/common/GetAgecompare?cur_code=" + "01" + "&acad_yr=" + $scope.edt.academic_year + "&grade=" + $scope.edt.grade).then(function (res) {
                    $scope.obj_age = res.data;
                    $scope.datechange($scope.edt['dob1']);
                    console.log($scope.obj_age);
                });
            }

            $scope.datechange = function (str) {
                

                if (str != '') {
                    var day = str.split('-')[0];
                    var month = str.split('-')[1];
                    var year = str.split('-')[2];
                    $scope.edt.dobn = year + '-' + month + '-' + day;
                }
                //$scope.edt.dobn = str;

                for (var i = 0; i < $scope.obj_age.length; i++) {
                    if (new Date($scope.edt.dobn) < new Date($scope.obj_age[i].sims_birth_date_from)) {
                        $scope.edt['dob1'] = "";
                        str = '';
                        // swal({ title: "Alert", text: "Age criteria is not met.", showCloseButton: true, width: 450, });
                        swal({ title: "Alert", text: "Age criteria is not met.", showCloseButton: true, width: 450, });

                    }
                    else if (new Date($scope.edt.dobn) > new Date($scope.obj_age[i].sims_birth_date_to)) {
                        $scope.edt['dob1'] = "";
                        str = '';
                        swal({ title: "Alert", text: "Age criteria is not met.", showCloseButton: true, width: 450, });


                    }
                }

            }

            $scope.mobile_change = function () {

                $http.get(ENV.apiUrl + "api/common/ProspectDashboard/GetMobileDetails?mobile=" + $scope.edt.mobile).then(function (res) {
                    $scope.mobile_lst = res.data;

                    if (res.data.length > 0) {
                        $('#myModalPros').modal({ backdrop: 'static', keyboard: true });
                    }

                });
            }
 

           
            jQuery('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd-mm-yyyy'
            });
            jQuery(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                jQuery('input[type="text"]', jQuery(this).parent()).focus();
            });

       
        }]);

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])
})();