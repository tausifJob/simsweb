﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var curriculum_code, ac_year, grade, admino;
    var grandtotal = 0;
    var finanacecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ProspectFeesCont',
        ['$scope', '$state', '$rootScope', '$filter', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $filter, $timeout, gettextCatalog, $http, ENV) {

            var username = $rootScope.globals.currentUser.username;
            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.display = true;
            //$scope.checked = true;
            var dataforSave = [];
            $scope.table = false;
            $scope.chequeno = true;
            $scope.temp = [];
            $scope.grandtotal = 0;
            $scope.chequeno = true;
            $scope.referenceno = false;
            $scope.readonlybankname = true;
            $scope.readonlychequeno = true;
            $scope.readonlychequedate = true;

            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //Select Data SHOW

            $scope.countData = [
                    { val: 5, data: 5 },
                    { val: 10, data: 10 },
                    { val: 15, data: 15 },

            ]


            $scope.size = function (str) {
                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }

                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';

            }


            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;
            $scope.AdmissionGradeFeesData = [];

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.print = function (str) {
                debugger;
                $http.get(ENV.apiUrl + "api/ProspectFees/getprospectReceipt").then(function (res) {
                    
                    $scope.reportparameter = res.data;
                    var data = {
                        location: res.data,
                        parameter: {
                            fee_rec_no: str.sims_doc_no,
                        },
                        state: 'main.Sim565',
                        ready: function () {
                            this.refreshReport();
                        },
                    }
                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                    $state.go('main.ReportCardParameter')
                });
               
            }
            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //CheckBox

            $scope.CheckAllChecked = function () {
                $scope.grandtotal = 0;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.AdmissionGradeFeesData.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $scope.grandtotal = parseInt($scope.grandtotal) + parseInt($scope.AdmissionGradeFeesData[i].sims_grade_fee_amount);
                    }
                    $scope.temp.grandtotal = $scope.grandtotal;
                }
                else {
                    for (var i = 0; i < $scope.AdmissionGradeFeesData.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $scope.grandtotal = 0;
                        $scope.temp.grandtotal = 0;
                    }
                }

            }


            $scope.checkonebyonedelete = function (str) {




                if (str.no == true) {

                    $scope.grandtotal = parseInt($scope.grandtotal) + parseInt(str.sims_grade_fee_amount);

                }
                else {

                    $scope.grandtotal = parseInt($scope.grandtotal) - parseInt(str.sims_grade_fee_amount);

                }
                $scope.temp.grandtotal = $scope.grandtotal;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';

                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';

                    }
                });
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }


            //Bind Curriculum,Academic Year section
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCurriculum) {
                debugger;
                $scope.Curriculum = getCurriculum.data;
                $scope.temp['sims_cur_code'] = $scope.Curriculum[0].sims_cur_code
                $scope.getacademicYear($scope.Curriculum[0].sims_cur_code);

            });

            $scope.getBankDetails = function () {
                $http.get(ENV.apiUrl + "api/AdmissionFees/getBankDeatails").then(function (getBankDetails) {
                    $scope.BankDetails = getBankDetails.data;

                })
            }

            $scope.getacademicYear = function (str) {
                debugger;
                $scope.curriculum_code = str;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (getAcademicYear) {
                    $scope.AcademicYear = getAcademicYear.data;

                })
            }


            $scope.getGrade = function (str1, str2) {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str1 + "&academic_year=" + str2).then(function (getAllGrades) {
                    $scope.AllGrades = getAllGrades.data;

                })
            }

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.AdmissionFeesData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.AdmissionFeesData;
                }
                $scope.makeTodos();
                main.checked = false;

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_pros_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_student_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_parent_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_grade_fee_amount == toSearch) ? true : false;
            }


            //DATA CANCEL
            $scope.reset = function () {

                $scope.table = false;
                $scope.display = true;
                $scope.temp = [];
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.getgrid();
            }

            $scope.cancel = function () {

                debugger;
                $scope.temp = {
                    sims_cur_code: $scope.curriculum_code,
                    sims_academic_year: $scope.ac_year,
                    sims_grade_code: $scope.grade,
                    admissionNo: $scope.admino,

                };

                $scope.temp.Checkno = "";
                $scope.temp.referenceno = "";
                $scope.temp.slma_pty_bank_id = "";
                $scope.temp.chequedate = "";
                $scope.temp.grandtotal = "";
                $scope.getgrid($scope.curriculum_code, $scope.ac_year, $scope.grade, $scope.admino);
                $scope.row1 = '';
                $scope.edit = {
                    copy1: 'Ca'
                }
                main = document.getElementById('mainchk');
                main.checked = false;

                $scope.CheckAllChecked();
                $scope.AdmissionGradeFeesData = [];

                // $scope.getgrid($scope.curriculum_code, $scope.ac_year, $scope.grade, $scope.admino);
            }

            //total
            $scope.total = function (str) {
                var v = document.getElementById(i);
                if (v.checked == true) {
                    $scope.grandtotal = parseInt($scope.grandtotal) + parseInt(str.sims_grade_fee_amount);
                }
                else {
                    $scope.grandtotal = parseInt($scope.grandtotal) - parseInt(str.sims_grade_fee_amount);
                }
                $scope.temp.grandtotal = $scope.grandtotal;
            }

            // Submit Fees

            $scope.PrintReport = function () {

                $http.get(ENV.apiUrl + "api/ProspectFees/getprospectReceipt").then(function (res) {
                    $scope.reportparameter = res.data;
                    var data = {
                        location: res.data,
                        parameter: {
                            fee_rec_no: $scope.docno,
                        },
                        state: 'main.Sim565',
                        ready: function () {
                            this.refreshReport();
                        },
                    }
                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                    $state.go('main.ReportCardParameter')
                });

            }

            $scope.clear1 = function () {
                debugger;
                $scope.grandtotal = 0;
                $scope.temp.grandtotal = angular.copy($scope.grandtotal);
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
                $scope.currentPage = true;
                for (var i = 0; i <= $scope.AdmissionGradeFeesData.length; i++) {
                    var v = document.getElementById(i);

                    if (v.checked == true) {
                        v.checked = false;
                        main.checked = false;
                        $('tr').removeClass("row_selected");
                    }


                }
            }
            $scope.submitadmissionfee = function () {

                var Subfee = [];
                $scope.flag = false;
                $scope.flag1 = false;

                if ($scope.edt.copy1 == 'Ch' || $scope.edt.copy1 == 'BT') {
                    if ($scope.edt.copy1 == 'Ch') {
                        if ($scope.temp.slma_pty_bank_id == undefined || $scope.temp.slma_pty_bank_id == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please select bank name", showCloseButton: true, width: 380, });

                        }
                        else if ($scope.temp.chequedate == null || $scope.temp.chequedate == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Enter Cheque Date", showCloseButton: true, width: 380, });
                        }
                        else if ($scope.temp.Checkno == null || $scope.temp.Checkno == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Enter Cheque Number", showCloseButton: true, width: 380, });
                        }
                        else {
                            $scope.flag1 = false;
                        }
                    }

                    else {
                        if ($scope.temp.referenceno == null || $scope.temp.referenceno == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Enter Reference Number", showCloseButton: true, width: 380, });
                        }
                        else {
                            $scope.flag1 = false;
                        }

                    }

                }
                if (!$scope.flag1) {
                    if ($scope.temp.Checkno == null || $scope.temp.Checkno == "") {
                        $scope.sims_fee_cheque_number = $scope.temp.referenceno;
                    }
                    else {
                        $scope.sims_fee_cheque_number = $scope.temp.Checkno;
                    }

                    for (var i = 0; i < $scope.AdmissionGradeFeesData.length; i++) {
                        //var t = $scope.filteredTodos[i].uom_code;
                        var v = document.getElementById(i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var Subfeeentry = ({
                                'sims_cur_code': $scope.AdmissionGradeFeesData[i].sims_cur_code,
                                'sims_academic_year': $scope.AdmissionGradeFeesData[i].sims_academic_year,
                                'sims_grade_code': $scope.AdmissionGradeFeesData[i].sims_grade_code,
                                'sims_fee_code': $scope.AdmissionGradeFeesData[i].sims_fee_code,
                                'sims_grade_fee_amount': $scope.AdmissionGradeFeesData[i].sims_grade_fee_amount,
                                'sims_pros_number': $scope.temp.sims_pros_number,

                                'sims_fee_payment_mode': $scope.edt.copy1,
                                'sims_fee_cheque_bank_code': $scope.temp.slma_pty_bank_id,
                                'sims_user_name': username,
                                'sims_fee_cheque_number': $scope.sims_fee_cheque_number,
                                'sims_fee_cheque_date': $scope.temp.chequedate,

                            });
                            Subfee.push(Subfeeentry);
                        }
                    }


                    if ($scope.flag) {

                        swal({
                            title: '',
                            text: "Are you sure you want to submit fees?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                $http.post(ENV.apiUrl + "api/ProspectFees/insertProspectFees", Subfee).then(function (res) {
                                    debugger;
                                    $scope.docno = res.data;
                                    var cnt = 0;
                                    if (!$scope.docno == "") {
                                        $scope.AdmissionGradeFeesData1 = angular.copy($scope.AdmissionGradeFeesData);
                                        for (var i = 0; i < $scope.AdmissionGradeFeesData.length; i++) {
                                            cnt++;
                                            var v = document.getElementById(i);
                                            if (v.checked == true) {
                                                $scope.AdmissionGradeFeesData1.splice(cnt - 1, 1);
                                                cnt--;

                                            }
                                        }

                                        $scope.AdmissionGradeFeesData = angular.copy($scope.AdmissionGradeFeesData1);

                                        $scope.printreceipt = true;
                                        swal({
                                            title: '',
                                            text: "Prospect Fee Collection Successfully.\n Do you want to promote/move this student to admission? ",
                                            showCloseButton: true,
                                            showCancelButton: true,
                                            confirmButtonText: 'Yes',
                                            width: 380,
                                            cancelButtonText: 'No',

                                        }).then(function (isConfirm) {

                                            if (isConfirm) {

                                                $http.post(ENV.apiUrl + "api/ProspectFees/promotetoadmission?prospectno=" + $scope.temp.sims_pros_number).then(function (res1) {
                                                    $scope.promotestudent = res1.data;
                                                    if ($scope.promotestudent == true) {
                                                        swal({ text: "Prospect collected successfully ", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                                            if (isConfirm) {

                                                                $scope.clear1();

                                                            }
                                                            else {
                                                                $scope.clear1();
                                                            }

                                                        });
                                                    }
                                                });


                                            }

                                            else {
                                                $scope.clear1();
                                            }
                                        });

                                    }
                                    else {
                                        swal({ text: "Fee Not Submited", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.temp = {
                                                    sims_cur_code: $scope.curriculum_code,
                                                    sims_academic_year: $scope.ac_year,
                                                    sims_grade_code: $scope.grade,
                                                    admissionNo: $scope.admino

                                                };

                                                $scope.getgrid($scope.curriculum_code, $scope.ac_year, $scope.grade, $scope.admino);
                                            }

                                        });
                                    }

                                });
                            }
                            else {
                                $scope.clear1();

                            }

                        });

                    }
                    else {
                        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                    }
                }

                $scope.currentPage = str;
            }

            $scope.ChkDate = function (noissueafter) {

                var date = new Date();
                $scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy/MM/dd');
                var month1 = noissueafter.split("/")[0];
                var day1 = noissueafter.split("/")[1];
                var year1 = noissueafter.split("/")[2];
                var new_end_date = year1 + "/" + month1 + "/" + day1;

                if (new_end_date < $scope.ddMMyyyy) {
                    //$rootScope.strMessage = "Please Select Today or Next Date";
                    //$('#message').modal('show');
                    swal({ title: "Alert", text: "Please Select Today or Next Date", showCloseButton: true, width: 380, });
                    $scope.temp.chequedate = "";
                }

            }


            //DATA EDIT
            $scope.edit = function (str) {

                $scope.temp = {
                    sims_student_full_name: str.sims_student_full_name,
                    sims_grade_name_en: str.sims_grade_name_en,
                    sims_pros_number: str.sims_pros_number
                };

            }

            $scope.edt =
                {
                    copy1: 'Ca'
                }



            $scope.collectshow = function (str) {

                $scope.printreceipt = false;
                if (str.sims_doc_status_content == 'Collect') {

                    $scope.temp = {
                        Checkno: "",
                        referenceno: "",
                        slma_pty_bank_id: "",
                        chequedate: "",

                    }
                    grandtotal = 0;
                    $scope.grandtotal = 0;
                    $scope.temp = {
                        sims_student_full_name: str.sims_student_full_name,
                        sims_grade_name_en: str.sims_grade_name_en,
                        sims_pros_number: str.sims_pros_number,
                        sims_cur_code: str.sims_cur_code,
                        sims_academic_year: str.sims_academic_year,
                        sims_grade_code: str.sims_grade_code
                    };
                    $scope.edt =
               {
                   copy1: 'Ca'
               }

                    $scope.radioclick('Ca');


                    $http.get(ENV.apiUrl + "api/ProspectFees/getProspectGradeFees?cur_code=" + str.sims_cur_code + "&academic_year=" + str.sims_academic_year + "&grade_code=" + str.sims_grade_code + "&prospect_no_name=" + str.sims_pros_number).then(function (res1) {

                        $scope.AdmissionGradeFeesData = res1.data;

                    });

                    $scope.getBankDetails();
                    //$scope.copy1.checked = true;
                    $('#myModal1').modal('show');
                }
                else {
                    $('#myModal1').modal('show');
                }
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                formate: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                $scope.temp[name1] = year + "/" + month + "/" + day;
            }


            $scope.radioclick = function (str) {

                if (str == 'Ca') {
                    $scope.chequeno = true;
                    $scope.referenceno = false;

                    $scope.readonlybankname = true;
                    $scope.readonlychequeno = true;
                    $scope.readonlychequedate = true;
                }

                else if (str == 'Ch') {
                    $scope.readonlybankname = false;
                    $scope.readonlychequeno = false;
                    $scope.readonlychequedate = false;
                    $scope.chequeno = true;
                    $scope.referenceno = false;

                }

                else if (str == 'BT') {
                    $scope.chequeno = false;
                    $scope.referenceno = true;
                    $scope.readonlybankname = true;
                    $scope.readonlychequeno = true;
                    $scope.readonlychequedate = true;
                }

            }

            $scope.getgrid = function (cur_code, acad_yr, grade_code, adm_no) {

                $scope.ac_year = acad_yr;
                $scope.grade = grade_code;
                $scope.admino = adm_no;
                $http.get(ENV.apiUrl + "api/ProspectFees/getProspectFees?cur_code=" + cur_code + "&academic_year=" + acad_yr + "&grade_code=" + grade_code + "&prospect_no_name=" + adm_no).then(function (res1) {

                    $scope.AdmissionFeesData = res1.data;
                    if ($scope.AdmissionFeesData.length > 0) {
                        $scope.table = true;
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.AdmissionFeesData.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.AdmissionFeesData.length, data: 'All' })
                        }
                        $scope.totalItems = $scope.AdmissionFeesData.length;
                        $scope.todos = $scope.AdmissionFeesData;
                        $scope.makeTodos();
                    }
                    else {
                        $scope.table = false;
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    }


                });

            }

            $scope.getgrid1 = function () {


                $http.get(ENV.apiUrl + "api/ProspectFees/getProspectGradeFees?cur_code=" + temp.sims_cur_code + "&academic_year=" + temp.sims_academic_year + "&grade_code=" + temp.sims_grade_code + "&prospect_no_name=" + temp.sims_pros_number).then(function (res1) {

                    $scope.AdmissionGradeFeesData = res1.data;
                    //$scope.totalItems = $scope.AdmissionGradeFeesData.length;
                    //$scope.todos = $scope.AdmissionGradeFeesData;
                    //$scope.makeTodos();


                });

            }
         }])

})();
