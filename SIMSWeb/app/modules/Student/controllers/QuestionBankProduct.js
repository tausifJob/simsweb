﻿(function () {

    var simsController = angular.module('sims.module.Student');

    simsController.controller('QuestionBankProductCont', ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

        $scope.display = false;
        $scope.table = true;


        $scope.pagesize = '10';
        $scope.pageindex = "0";
        $scope.pager = true;
        $scope.showcodeMsg = false;
        $scope.ImgLoaded = false;
        $scope.costDisabled = false;
        $scope.int = {};
        $scope.int.sims_questionbank_code = [];
        var user = $rootScope.globals.currentUser.username;
        $scope.gradecode = '';

        $timeout(function () {
            $("#fixTable").tableHeadFixer({ 'top': 1 });
        }, 100);


        $(function () {
            $('#quest_bank_code').multipleSelect({
                width: '100%'
            });
        });

        $http.get(ENV.apiUrl + "api/QuestionBankProduct/getAllGrades").then(function (res) {
            $scope.All_grade_names = res.data;
            $scope.gradecode = res.data[0].sims_grade_code;
            $scope.getQuestionBankDetails($scope.gradecode);
        });

        $scope.getQuestionBankDetails = function (gradecode) {
            $http.get(ENV.apiUrl + "api/QuestionBankProduct/getQuestionBankDetails?grade_code=" + gradecode).then(function (res1) {
                debugger
                $scope.QuestionBankDetails = res1.data;
                console.log("QuestionBankDetails", $scope.QuestionBankDetails);
                setTimeout(function () {
                    $('#quest_bank_code').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });
        }
        
        $http.get(ENV.apiUrl + "api/QuestionBankProduct/getCurrencyDetails").then(function (res) {
            $scope.CurrencyDetails = res.data;
            $scope.curency_code = res.data[0].comp_curcy_code;
            $scope.comp_curcy_dec = res.data[0].comp_curcy_dec;
            console.log("curency_code",$scope.curency_code);
        });

        $scope.size = function (str) {
            //console.log(str);
            //$scope.pagesize = str;
            //$scope.currentPage = 1;
            //$scope.numPerPage = str;
            debugger;
            if (str == "All") {
                $scope.currentPage = '1';
                $scope.filteredTodos = $scope.CreDiv;
                $scope.pager = false;
            }
            else {
                $scope.pager = true;
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }
            console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
        }

        var file_name = '';
        var file_name_name = '';
        var fortype = '';
        var forName = '';

        var formdata = new FormData();
        $scope.getTheFiles = function ($files) {
            angular.forEach($files, function (value, key) {
                formdata.append(key, value);
            });
        };
        $scope.file_changed = function (element) {
            debugger
            var photofile = element.files[0];
            $scope.photo_filename = (photofile.type);
            var v = new Date();
            file_name_name = photofile.name;
            var len = 0;
            len = file_name_name.split('.');
            forName = file_name_name.split('.')[0] + '_' + v.getHours() + v.getSeconds();
            fortype = file_name_name.split('.')[len.length - 1];

            if ($scope.photo_filename != undefined || $scope.photo_filename != "") {
                var imageType = $scope.photo_filename.split('/');
                if (fortype.toLowerCase() == 'jpeg' || fortype.toLowerCase() == 'jpg' || fortype.toLowerCase() == 'png') {

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $scope.$apply(function () {
                            $scope.prev_img = e.target.result;
                        });
                    };
                    reader.readAsDataURL(photofile);
                }
                else {
                    swal({ text: "Only jpeg and png image format supported." });
                    $('#empimg').attr("src", '');
                    return;
                }
            }
            else {
                swal({ text: "Image format not supported." });
                $('#empimg').attr("src", '');
                return;
            }
        };

        $scope.CheckAllChecked = function () {
            main = document.getElementById('mainchk');
            if (main.checked == true) {
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i + $scope.filteredTodos[i].sims_questionbank_set_no);
                    v.checked = true;
                    $scope.row1 = 'row_selected';
                    $('tr').addClass("row_selected");
                }
            }
            else {

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i + $scope.filteredTodos[i].sims_questionbank_set_no);
                    v.checked = false;
                    main.checked = false;
                    $scope.row1 = '';
                    $('tr').removeClass("row_selected");
                }
            }

        }


        $scope.index = function (str) {
            $scope.pageindex = str;
            $scope.currentPage = str;
            console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            main.checked = false;
            $scope.CheckAllChecked();
        }

        $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

        $scope.makeTodos = function () {
            var rem = parseInt($scope.totalItems % $scope.numPerPage);
            if (rem == '0') {
                $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
            }
            else {
                $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
            }

            var begin = (($scope.currentPage - 1) * $scope.numPerPage);
            var end = parseInt(begin) + parseInt($scope.numPerPage);

            console.log("begin=" + begin); console.log("end=" + end);

            $scope.filteredTodos = $scope.todos.slice(begin, end);
        };

        $scope.searched = function (valLists, toSearch) {
            return _.filter(valLists,

            function (i) {
                /* Search Text in all  fields */
                return searchUtil(i, toSearch);
            });
        };

        function searchUtil(item, toSearch) {
            /* Search Text in all 3 fields */
            return (item.sims_questionbank_set_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                item.sims_questionbank_set_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                item.sims_questionbank_set_cost.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 
              ) ? true : false;
        }

        $scope.search = function () {
            $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
            $scope.totalItems = $scope.todos.length;
            $scope.currentPage = '1';
            if ($scope.searchText == '') {
                $scope.todos = $scope.CreDiv;
            }
            $scope.makeTodos();
        }

        //  new
        $scope.New = function () {
            var datasend = [];
            $scope.int = {};
            $scope.table = false;
            $scope.display = true;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.move = true;
            $scope.showcodeMsg = false;
            $scope.int.sims_questionbank_set_status = true;
            $scope.ImgLoaded = false;
            $scope.costDisabled = false;
            $("#file1").val('');
            $scope.Myform1.$setPristine();
            $scope.Myform1.$setUntouched();
        }

        //cancle
        $scope.Cancel = function () {
            $scope.temp = "";
            $scope.table = true;
            $scope.display = false;
            $scope.showcodeMsg = false;
            $scope.int = {};
            $scope.int.sims_questionbank_set_status = true;
            $scope.photo_filename = '';
            $scope.ImgLoaded = false;
            $scope.costDisabled = false;
            file_name = '';
            file_name_name = '';
            fortype = '';
            forName = '';
            $scope.gradecode = '';
            $("#file1").val('');
            try {
                $("#quest_bank_code").multipleSelect("uncheckAll");
            }
            catch (e) {

            }
            $scope.Myform1.$setPristine();
            $scope.Myform1.$setUntouched();
        }

        //DATA EDIT  
        $scope.edit = function (str) {
            debugger
            $scope.Update_btn = true;
            $scope.gdisabled = false;
            $scope.aydisabled = true;
            $scope.table = false;
            $scope.display = true;
            $scope.save_btn = false;
            $scope.divcode_readonly = true;
            debugger
            $scope.int.sims_questionbank_code = [];
            
            $scope.int.sims_questionbank_set_name = str.sims_questionbank_set_name;
            $scope.int.sims_questionbank_set_cost = str.sims_questionbank_set_cost;
            $scope.int.sims_questionbank_set_status = str.sims_questionbank_set_status;
            $scope.int.sims_questionbank_set_no = str.sims_questionbank_set_no;
            $scope.int.sims_questionbank_set_free_status = str.sims_questionbank_set_free_status;
            $scope.int.sims_questionbank_set_logo_path = str.sims_questionbank_set_logo_path;
            var question_code = str.sims_questionbank_code.split(',');
                        
            $scope.int.sims_questionbank_code = question_code.concat();;

            setTimeout(function () {      
                $("#quest_bank_code").multipleSelect("setSelects", $scope.int.sims_questionbank_code);
            }, 500);

            $scope.setFree();

            console.log("sims_questionbank_code",$scope.int.sims_questionbank_code);
        }
        // grid data
        $scope.getGrid = function () {

            $http.get(ENV.apiUrl + "api/QuestionBankProduct/getQuestionBankProductDetails").then(function (res1) {
                debugger
                $scope.CreDiv = res1.data;
                $scope.totalItems = $scope.CreDiv.length;
                // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                $scope.todos = $scope.CreDiv;
                $scope.makeTodos();
            });
        }
        $scope.getGrid();

        //insert data 
        $scope.Save = function (Myform) {
            var datasend = [];
            debugger
         
            if (Myform) {
                $scope.data = $scope.int;

                var question_bank_code = "";
                for (var i = 0; i < $scope.int.sims_questionbank_code.length; i++) {
                    question_bank_code = question_bank_code + $scope.int.sims_questionbank_code[i] + ',';
                }
                $scope.data['sims_questionbank_code'] = question_bank_code;
                $scope.data['opr'] = 'I';
                $scope.data['sims_questionbank_set_created_by'] = $rootScope.globals.currentUser.username;
                if($scope.photo_filename === undefined || $scope.photo_filename === "" || $scope.photo_filename === null) {
                    $scope.data['sims_questionbank_set_logo_path'] = '';
                }
                else {
                    $scope.data['sims_questionbank_set_logo_path'] = forName + '.png';
                }

                datasend.push($scope.data);
                $http.post(ENV.apiUrl + "api/QuestionBankProduct/CUDQuestionBankProductSet", datasend).then(function (res) {

                    $scope.msg1 = res.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });

                        debugger;
                        if ($scope.ImgLoaded == true) {
                            var request = {
                                method: 'POST',
                                url: ENV.apiUrl + '/api/file/Imageupload?filename=' + forName + "&location=" + "/Product",
                                data: formdata,
                                headers: {
                                    'Content-Type': undefined
                                }
                            };
                            $http(request).success(function (d) {
                                //alert(d);
                            });
                        }


                        $state.reload();
                        //$scope.getGrid();
                        $scope.int = {};
                        $scope.Cancel();

                    }
                    else {
                        //swal({ title: "Alert", text: "Record NOt Inserted ", showCloseButton: true, width: 300, height: 200 });
                        swal('', 'Record not inserted');

                    }
                });
            }
        }

        //DATA UPDATE
        var dataforUpdate = [];
        $scope.update = function (Myform) {

            if (Myform) {
                debugger;
                var data = $scope.int;                
                var question_bank_code = "";
                for (var i = 0; i < $scope.int.sims_questionbank_code.length; i++) {
                    question_bank_code = question_bank_code + $scope.int.sims_questionbank_code[i] + ',';
                }
                data['sims_questionbank_code'] = question_bank_code;
                data.opr = "U";
                data['sims_questionbank_set_created_by'] = $rootScope.globals.currentUser.username;
                if ($scope.photo_filename === undefined || $scope.photo_filename === "" || $scope.photo_filename === null) {
                }
                else {
                    data['sims_questionbank_set_logo_path'] = forName + '.png' ;
                }
                

                dataforUpdate.push(data);
                //dataupdate.push(data);
                $http.post(ENV.apiUrl + "api/QuestionBankProduct/CUDQuestionBankProductSet", dataforUpdate).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                      

                        debugger;
                        if ($scope.ImgLoaded == true) {
                            var request = {
                                method: 'POST',
                                url: ENV.apiUrl + '/api/file/Imageupload?filename=' +  forName + "&location=" + "/Product",
                                data: formdata,
                                headers: {
                                    'Content-Type': undefined
                                }
                            };
                            $http(request).success(function (d) {
                                //alert(d);
                            });
                        }

                        $state.reload();
                        //$state.go('main.qunpro');
                        //$scope.getGrid();
                        $scope.Cancel();
                    }
                    else {
                        swal({ text: "Record Not Updated", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                    }                  
                });
                dataforUpdate = [];
                $scope.table = true;
                $scope.display = false;
            }
        }
        // Data DELETE RECORD

        $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/Product/';

        
        $scope.image_click = function () {
            $scope.ImgLoaded = true;
        }

        $scope.downloaddoc1 = function (str) {
           
            $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/Product/' + str;
            window.open($scope.url);
        }

        $scope.setFree = function () {
            if ($scope.int.sims_questionbank_set_free_status == true) {
                $scope.costDisabled = true;
            }
            else {
                $scope.costDisabled = false;
            }
        };
        
        $scope.checkonebyonedelete = function () {

            $("input[type='checkbox']").change(function (e) {
                if ($(this).is(":checked")) {
                    $(this).closest('tr').addClass("row_selected");
                    $scope.color = '#edefef';
                } else {
                    $(this).closest('tr').removeClass("row_selected");
                    $scope.color = '#edefef';
                }
            });

            main = document.getElementById('mainchk');
            if (main.checked == true) {
                main.checked = false;
                $scope.color = '#edefef';
                $scope.row1 = '';
            }
        }

        //delete
        $scope.OkDelete = function () {
            debugger
            deletefin = [];
            $scope.flag = false;
            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                var v = document.getElementById(i + $scope.filteredTodos[i].sims_questionbank_set_no);
                if (v.checked == true) {
                    $scope.flag = true;
                    var deletemodulecode = {
                        'sims_questionbank_set_no': $scope.filteredTodos[i].sims_questionbank_set_no,
                        opr: 'D'
                    }
                    deletefin.push(deletemodulecode);
                }
            }
            if ($scope.flag) {
                swal({
                    title: '',
                    text: "Are you sure you want to Delete? Delete this field Related all record",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {

                    if (isConfirm) {
                        debugger;
                        $http.post(ENV.apiUrl + "api/QuestionBankProduct/CUDQuestionBankProductSet", deletefin).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getGrid();
                                        main = document.getElementById('mainchk');
                                        if (main.checked == true) {
                                            main.checked = false;
                                        }

                                        $scope.CheckAllChecked();
                                    }
                                    $scope.currentPage = true;
                                });
                            }
                            else {
                                swal({ text: "Record Not Deleted", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getGrid();
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                });
                            }

                        });
                        deletefin = [];
                    }
                    else {
                        for (var i = 0; i < $scope.filteredTodos.length; i++) {
                            var v = document.getElementById(i + $scope.filteredTodos[i].sims_questionbank_set_no );
                            if (v.checked == true) {
                                v.checked = false;
                                main.checked = false;
                                $('tr').removeClass("row_selected");
                            }
                        }
                    }
                });
            }
            else {
                swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
            }
            $scope.currentPage = true;
        }
    }])

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

})();