﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, modulecode = [];
    var data = [], data1 = [];
    var simsController = angular.module('sims.module.Student');

    simsController.controller('StudentBehaviourTargetCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {


            $scope.busyindicator = false;
            $scope.hide = true;
            $scope.hide2 = false;
            console.clear();

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear($scope.edt.sims_cur_code);
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                    $scope.getGrade(curCode, $scope.temp.sims_academic_year);

                    $http.get(ENV.siteUrl + "StudentReport.json").then(function (col) {
                        $scope.columns = col.data;
                        console.log($scope.columns);
                    });

                });
            }

            $scope.GetGrade = function () {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $scope.getGrade = function (curCode, accYear) {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $(function () {
                $('#cmb_grade_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/StudentReport/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $(function () {
                $('#cmb_section_code').multipleSelect({
                    width: '100%'
                });
            });

            $http.get(ENV.apiUrl + "api/StudentReport/gettargetcodes").then(function (targets) {
                $scope.targetdatas = targets.data;
                setTimeout(function () {
                    $('#cmb_target_code').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });

            $(function () {
                $('#cmb_target_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.getlinedesc = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/StudentReport/gettargetcodesdetails?targetcode=" + $scope.edt.sims_target_code).then(function (targetdata) {
                    $scope.searchitems = targetdata.data;

                    setTimeout(function () {
                        $('#cmd_line_disc').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }

            $(function () {
                $('#cmd_line_disc').multipleSelect({
                    width: '100%'
                });
            });

            $scope.Show_Data = function () {
                
                var fd=null,ud=null;

                if($scope.dt.sims_from_date==undefined)
                    fd=null
                else
                    fd=$scope.dt.sims_from_date;

                if($scope.dt1.sims_upto_date==undefined)
                    ud=null
                else
                    ud=$scope.dt1.sims_upto_date;

                var objnew = {
                    sims_cur_code: $scope.edt.sims_cur_code,
                    sims_academic_year: $scope.temp.sims_academic_year,
                    sims_grade_code: $scope.edt.sims_grade_code + '',
                    sims_section_code: $scope.edt.sims_section_code + '',
                    sims_student_enroll_number: $scope.edt.sims_student_enroll_number,
                    sims_target_code: $scope.edt.sims_target_code + '',
                    sims_target_code_line_no: $scope.target.sims_target_code_line_no + '',
                    sims_from_date: fd,
                    sims_upto_date: ud,
                }
                $http.post(ENV.apiUrl + "api/StudentReport/Targetcodesdetailstudent", objnew).then(function (res) {

                    $scope.rows = res.data.table;
                    $scope.Allrows = res.data.table1;
                    $scope.Allrowslin = res.data.table2;
                    if ($scope.Allrowslin.length <= 0) {
                        swal({ title: "Alert", text: "Records not found", showCloseButton: true, width: 380, });
                        $scope.hide2 = false;
                    }
                    else {
                        for (var r in $scope.rows) {
                            $scope.rows[r]['sims_icon'] = "fa fa-plus-circle";
                            $scope.rows[r]['isexpanded'] = "none";
                            $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].sims_student_behaviour_enroll_number);
                        }
                        $scope.hide2 = true;
                    }
                    console.clear();
                });
            }

            function getSubitems(dno) {
                var arr = [];
                for (var i = 0; i < $scope.Allrows.length; i++) {
                    if ($scope.Allrows[i].sims_student_behaviour_enroll_number == dno) {
                        $scope.Allrows[i]['sims_icon'] = "fa fa-plus-circle";
                        $scope.Allrows[i]['isexpanded'] = "none";
                        $scope.Allrows[i]['subItems1'] = getSubitems1(dno, $scope.Allrows[i].sims_student_behaviour_target_no);
                        arr.push($scope.Allrows[i]);
                    }
                }
                return arr;
            }

            function getSubitems1(dno, tno) {
                var arr1 = [];
                for (var i = 0; i < $scope.Allrowslin.length; i++) {
                    if ($scope.Allrowslin[i].sims_student_behaviour_enroll_number == dno && $scope.Allrowslin[i].sims_student_behaviour_target_no==tno) {
                        arr1.push($scope.Allrowslin[i]);
                    }
                }
                return arr1;
            }

            $scope.ordclick = function (gh) {
                var l = gh.subItems.length;
                for (var i = 0; i < l; i++) {
                    gh.subItems[i]['isChecked'] = gh.isChecked;
                }
            }

            $scope.ordlineclick = function (r, m) {
                var l = r.subItems.length;
                var f = false;
                if (m) {
                    f = true;
                    for (var i = 0; i < l; i++) {
                        f = f && r.subItems[i]['isChecked'];
                    }
                }
                r.isChecked = f;
            }

            $scope.expand = function (j, $event) {
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none";
                    if ($scope.Allrowslin.length <= 0) {
                        swal({ title: "Alert", text: "Records not found", showCloseButton: true, width: 380, });
                        $scope.hide2 = false;
                    }
                    else {
                        for (var r in $scope.rows) {
                            $scope.rows[r]['sims_icon'] = "fa fa-plus-circle";
                            $scope.rows[r]['isexpanded'] = "none";
                            $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].sims_student_behaviour_enroll_number);
                        }
                        $scope.hide2 = true;
                    }
                    console.clear();
                }
            }

            $scope.expand1 = function (j, $event) {
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none";
                }
            }

            var datasend = [];
            var datasend1 = [];

            $scope.Save = function () {
                debugger;
                data = {
                    'opr': 'C',
                    'sims_cur_code': $scope.edt.sims_cur_code,
                    'sims_academic_year': $scope.temp.sims_academic_year,
                    'sims_student_enroll_number': $scope.edt.sims_student_enroll_number,
                    'sims_target_code': $scope.edt.sims_target_code + '',
                    'sims_target_code_line_no': $scope.target.sims_target_code_line_no + '',
                    'sims_from_date': $scope.dt.sims_from_date,
                    'sims_upto_date': $scope.dt1.sims_upto_date,
                    'sims_student_behaviour_target_status': true,
                    'sims_grade_code': $scope.edt.sims_grade_code + '',
                    'sims_section_code': $scope.edt.sims_section_code + '',
                }
                datasend.push(data);
                $http.post(ENV.apiUrl + "api/StudentReport/CUDTtargetdetailstudent?targetcode=" + '' + "&enroll_number=" + '', datasend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 != null || $scope.msg1 != undefined) {
                        var m = $scope.msg1.strMessage;
                        swal({ title: "Alert", text: m, width: 450, height: 200 }); //
                        //$scope.gettargetcode();
                    }
                });
                datasend = [];
                datasend1 = [];
            }



            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.dt = {
                sims_from_date: dd + '-' + mm + '-' + yyyy,
                sims_upto_date: dd + '-' + mm + '-' + yyyy,

            }
            $scope.dt1 = {
                sims_from_date: dd + '-' + mm + '-' + yyyy,
                sims_upto_date: dd + '-' + mm + '-' + yyyy,

            }



            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });
        }
         ])
})();