﻿(function () {
    'use strict';
    var appcode = [];
    var getdata = [];
    var senddata = [], demo = [], demo1 = [];
    var main;
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ADMCancellationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.busyindicator = true;
            $scope.submit2 = false;
            $scope.submit1 = true;
            $scope.chkbox = false;
            $scope.edt = [];
            $scope.grades_data = "";
            var g = false;

            var range = [];
            for (var i = 1; i <= 10; i++) {
                range[range.length] = {
                    sims_clr_preference_order: i
                };
            }
            $scope.range = range;

            $scope.listOfPreference = range;

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
                $scope.edt["sims_cur_code"] = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr($scope.curriculum[0].sims_cur_code);
            })

            $http.get(ENV.apiUrl + "api/common/getModulesTcWorkFlow").then(function (res1) {
                $scope.moduleList = res1.data;
                $scope.moduleList.forEach(function (cr) {
                    cr.checked = false;
                    cr.sims_clr_preference_order = "";
                })
                $scope.accessDetails();
            })

            $timeout(function () {
                $(document).ready(function () {
                    $("#from_date, #to_date").kendoDatePicker({
                        format: "dd-MM-yyyy",
                        value: ''
                    });
                });
            }, 500);

            $scope.getsections = function () {
                $scope.chkbox = true;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (res1) {
                    console.log(res1.data);
                    $scope.grades_data = "";
                    $scope.grades_data = res1.data;
                    $scope.grades_data.forEach(function (cr) {
                        cr.checked = false;
                    })
                    //$("#grade_module").jqxListBox({ source: '', displayMember: '', valueMember: "", width: '100%', height: 300, theme: 'energyblue', multiple: true });
                    //$("#grade_module").jqxListBox({ source: $scope.grades_data, displayMember: 'sims_grade_name', valueMember: "sims_grade_code", width: '100%', height: 300, theme: 'energyblue', multiple: true });
                })
            };

            $scope.getacyr = function (str) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    $scope.edt["sims_academic_year"] = $scope.Academic_year[0].sims_academic_year;
                    $scope.getsections();
                });
            }

            $('#grade_module').bind('select', function (event) {
                var args = event.args;
                // var items = $('#grade_module').jqxListBox('getItems', args.index);
                var items = $("#grade_module").jqxListBox('getSelectedItems');

                if (items.length > 0) {
                    $scope.edt.sims_grade_list = "";
                    for (var i = 0; i < items.length; i++) {
                        $scope.edt.sims_grade_list += items[i].originalItem.sims_grade_code;
                        if (i < items.length - 1) $scope.edt.sims_grade_list += ",";
                    }
                    //alert(labels);
                }

            });

            $scope.onSuperUserClick = function (value) {
                !value && $scope.moduleList.forEach(function (cr) { cr.checked = false; cr.sims_clr_preference_order = ""; })
            }

            $scope.savedata = function (opr) {
                var datasend = [];
                if ($scope.edt.empName != undefined && $('#from_date').val() != "" && $('#to_date').val() != "") {
                    var gradeListJoin = $scope.grades_data.filter(function (cr) { return cr.checked === true }).map(function (i) { return i.sims_grade_code }).join(',');
                    if (gradeListJoin !== "") {
                        var t = new Date();
                        var tempCreated = t.getDate() + '-' + (t.getMonth() + 1) + '-' + t.getFullYear();
                        if ($scope.isSuperUser) {
                            var tempObj = {
                                opr: opr,
                                sims_clr_cur_code: $scope.edt.sims_cur_code,
                                sims_clr_academic_year: $scope.edt.sims_academic_year,
                                sims_clr_employee_id: $scope.edt.em_number,
                                sims_clr_access_code: "",
                                sims_clr_class_code: gradeListJoin,
                                sims_clr_preference_order: '1',
                                sims_clr_status: 'A',
                                sims_clr_created_by: $rootScope.globals.currentUser.username,
                                sims_clr_created_date: tempCreated.toString(),
                                sims_clr_access_from_date: $('#from_date').val(),
                                sims_clr_access_to_date: $('#to_date').val(),
                                sims_clr_is_super_user: 'Y'
                            }
                            datasend[0] = tempObj;
                        }
                        else {
                            var temp = $scope.moduleList.filter(function (cr) { return cr.checked === true || cr.isAlreadyCheck === true })
                            temp.forEach(function (cr) {
                                var tempObj = {
                                    opr: opr,
                                    sims_clr_cur_code: $scope.edt.sims_cur_code,
                                    sims_clr_academic_year: $scope.edt.sims_academic_year,
                                    sims_clr_employee_id: $scope.edt.em_number,
                                    sims_clr_access_code: cr.comn_mod_code,
                                    sims_clr_class_code: gradeListJoin,
                                    sims_clr_preference_order: cr.sims_clr_preference_order.toString(),
                                    sims_clr_status: 'A',
                                    sims_clr_created_by: $rootScope.globals.currentUser.username,
                                    sims_clr_created_date: tempCreated.toString(),
                                    sims_clr_access_from_date: $('#from_date').val(),
                                    sims_clr_access_to_date: $('#to_date').val(),
                                    sims_clr_is_super_user: 'N'
                                }
                                cr.isAlreadyCheck && cr.checked === false && (tempObj.sims_clr_status = 'I')
                                datasend[datasend.length] = tempObj;
                            })
                        }
                        $http.post(ENV.apiUrl + "api/common/CUDCancelAdmission", datasend).then(function (res) {
                            datasend = [];
                            if (res.data.flag == '1') {
                                swal({ text: res.data.msg, imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                $scope.clearEmp();
                                $scope.accessDetails();
                            }
                            else if (res.data.flag == '0' || res.data.flag == '-1') {
                                swal({ text: res.data.msg, imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Select grade(s)", width: 300, height: 200 });
                    }
                }
                else {
                    swal({ title: "Alert", text: "Select Employee, From date, To date", width: 300, height: 200 });
                }
            }

            $scope.accessDetails = function () {
                $http.get(ENV.apiUrl + "api/common/getAccessDetails?user=" + $rootScope.globals.currentUser.username + "&empId=" + $scope.edt.em_number).then(function (res11) {
                    $scope.accessDetailsList = res11.data;
                })
            }

            $scope.clearEmp = function () {
                $scope.edt.em_number = undefined;
                $scope.edt.empNameDis = "";
                $scope.edt.empName = undefined;
                $scope.edt.from_date = "";
                $scope.edt.to_date = "";
                $scope.edt.sims_cur_code = "";
                $scope.edt.sims_academic_year = "";
                $scope.moduleList.forEach(function (cr) { cr.checked = false, cr.sims_clr_preference_order = "" })
                $scope.grades_data.forEach(function (cr) { cr.checked = false })
                $scope.isSuperUser = false;
            }

            $scope.btncancel = function () {
                $scope.submit1 = true;
                $scope.clearEmp();
            }

            $scope.bindAccessDetailsOnRowClick = function (x) {
                $scope.submit1 = false;
                $scope.moduleList.forEach(function (cr) { cr.checked = false, cr.isAlreadyCheck = false, cr.sims_clr_preference_order = "" })
                $scope.grades_data.forEach(function (cr) { cr.checked = false })
                x.sims_clr_is_super_user === 'Y' ? $scope.isSuperUser = true : $scope.isSuperUser = false 
                $scope.moduleList.forEach(function (cr) {
                    cr.comn_mod_code == x.sims_clr_access_code && (cr.checked = true, cr.isAlreadyCheck = true, cr.sims_clr_preference_order = +x.sims_clr_preference_order)
                })
                $scope.edt.em_number = x.sims_clr_employee_id;
                $scope.edt.sims_cur_code = x.sims_cur_code;
                $scope.edt.sims_academic_year = x.sims_academic_year;
                $scope.edt.from_date = x.sims_clr_access_from_date;
                $scope.edt.to_date = x.sims_clr_access_to_date;
                $scope.edt.empNameDis = x.sims_clr_employee_id + '-' + x.em_first_name;;
                $scope.edt.empName = x.em_first_name;
                var temp = x.sims_clr_class_code.split(',');
                temp.forEach(function (cr) {
                    $scope.grades_data.forEach(function (cri) {
                        cr === cri.sims_grade_code && (cri.checked = true)
                    })
                })
                //res1.data.forEach(function (cr, ind) {
                //    $scope.moduleList.forEach(function (cri) {
                //        cr.sims_clr_access_code === cri.comn_mod_code && (cri.checked = true)
                //    })
                //})
            }

            $scope.Submit2 = function () {
                
                $scope.chkbox = false;
                var data = $scope.edt;
                datasend.push(data);
                $http.post(ENV.apiUrl + "api/common/CUDCancelAdmissionshow", datasend).then(function (msg) {
                    datasend = [];
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        $scope.edt["sims_cur_code"] = '';
                        $scope.edt["sims_academic_year"] = '';
                        $scope.edt["empName"] = '';
                        $scope.edt["sims_fee_clr_status"] = '',
                        $scope.edt["sims_finn_clr_status"] = '',
                        $scope.edt["sims_inv_clr_status"] = '',
                        $scope.edt["sims_inci_clr_status"] = '',
                        $scope.edt["sims_lib_clr_status"] = '',
                        $scope.edt["sims_trans_clr_status"] = '',
                        $scope.edt["sims_acad_clr_status"] = '',
                        $scope.edt["sims_admin_clr_status"] = '',
                        $scope.edt["sims_other1_clr_status"] = '',
                        $scope.edt["sims_other2_clr_status"] = '',
                        $scope.edt["sims_other3_clr_status"] = '',
                        $scope.edt["sims_clr_emp_status"] = '',
                         $scope.edt["sims_clr_emp_status"] = '',
                         $("#grade_module").jqxListBox('selectItem', "");
                        $scope.grade_s = '';
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                });
            }

            $scope.Fetch = function () {

            }

            $scope.Fetch1 = function () {
                if ($scope.edt.empName != undefined && $scope.edt.sims_academic_year != undefined && $scope.edt.sims_cur_code != undefined && $scope.edt.empName != "") {
                    $scope.submit2 = true;
                    $scope.submit1 = false;
                    $scope.chkbox = true;
                    $http.get(ENV.apiUrl + "api/common/getAllADMData?Empnum=" + $scope.em_number + "&sims_cur_code=" + $scope.edt.sims_cur_code + "&sims_academic_year=" + $scope.edt.sims_academic_year).then(function (res1) {
                        $scope.CreDiv = res1.data;

                        res1.data.forEach(function (cr, ind) {
                            $scope.moduleList.forEach(function (cri) {
                                cr.sims_clr_access_code === cri.comn_mod_code && (cri.checked = true)
                            })
                        })

                        //var temp = $scope.CreDiv[0].grades.split(',');
                        //temp.forEach(function (cr) {
                        //    $scope.grades_data.forEach(function (cri) {
                        //        cr === cri.sims_grade_code && (cri.checked = true)
                        //    })
                        //})

                        //$scope.edt = {
                        //    'sims_sr_no': $scope.CreDiv[0].sims_sr_no,
                        //    'sims_acad_clr_status': $scope.CreDiv[0].sims_acad_clr_status,
                        //    'sims_admin_clr_status': $scope.CreDiv[0].sims_admin_clr_status,
                        //    'sims_clr_emp_status': $scope.CreDiv[0].sims_clr_emp_status,
                        //    'sims_fee_clr_status': $scope.CreDiv[0].sims_fee_clr_status,
                        //    'sims_finn_clr_status': $scope.CreDiv[0].sims_finn_clr_status,
                        //    'sims_inci_clr_status': $scope.CreDiv[0].sims_inci_clr_status,
                        //    'sims_inv_clr_status': $scope.CreDiv[0].sims_inv_clr_status,
                        //    'sims_lib_clr_status': $scope.CreDiv[0].sims_lib_clr_status,
                        //    'sims_other1_clr_status': $scope.CreDiv[0].sims_other1_clr_status,
                        //    'sims_other2_clr_status': $scope.CreDiv[0].sims_other2_clr_status,
                        //    'sims_other3_clr_status': $scope.CreDiv[0].sims_other3_clr_status,
                        //    'sims_status': $scope.CreDiv[0].sims_status,
                        //    'sims_trans_clr_status': $scope.CreDiv[0].sims_trans_clr_status,
                        //    'sims_clr_super_user_flag': $scope.CreDiv[0].sims_clr_super_user_flag,
                        //    'sims_academic_year': $scope.CreDiv[0].sims_academic_year,
                        //    'sims_cur_code': $scope.CreDiv[0].sims_cur_code,
                        //    'empName': $scope.CreDiv[0].sims_clr_emp_id,
                        //    'empNameDis': $scope.edt.empName + '-' + $scope.edt.em_number
                        //}
                        //$scope.grade_s = $scope.CreDiv[0].grades;
                        //$scope.cur_code = $scope.CreDiv[0].sims_cur_code;


                        //if ($scope.grade_s != undefined) {
                        //    var d = $scope.grade_s.split(',');
                        //    for (var i = 0; i < d.length - 1; i++) {
                        //        var jqItem = $("#grade_module").jqxListBox('getItemByValue', d[i]);
                        //        $("#grade_module").jqxListBox('selectItem', jqItem);
                        //    }
                        //}

                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Employee,Curriculum,Academic year", width: 300, height: 200 });
                }
            }
            $rootScope.visible_stud = false;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = true;
            $rootScope.chkMulti = false;
            $scope.Search = function () {
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                //  $scope.display = true;
                //$scope.SelectedUserLst = [];
            }

            $scope.$on('global_cancel', function () {
                if ($scope.SelectedUserLst.length > 0) {
                    $scope.edt.empName = $scope.SelectedUserLst[0].empName;
                    $scope.edt.em_number = $scope.SelectedUserLst[0].em_number;
                    $scope.edt.empNameDis = $scope.SelectedUserLst[0].em_number + '-' + $scope.SelectedUserLst[0].empName
                }
                $scope.em_number = $scope.edt.em_number;
            });
        }]);
})();



