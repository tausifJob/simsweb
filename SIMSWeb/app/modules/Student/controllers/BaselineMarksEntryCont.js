﻿(function () {
    'use strict';
    var appcode = [];
    var getdata = [];
    var senddata = [], demo = [], demo1 = [];
    var main;
    var simsController = angular.module('sims.module.Student');
    simsController.controller('BaselineMarksEntryCont',
          ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

              $scope.edt = {};
              //$scope.edt['sims_subject_code'] = '';
              $scope.edt['sims_subject_code'] = [];
              $scope.simsArray = [];
              $scope.selectedStudentList = [];
              var user = $rootScope.globals.currentUser.username;
              $scope.busyindicator = true;
              $scope.propertyName = null;
              $scope.reverse = false;
             
              $scope.table = true;
              // propertyName = null; reverse = false
              $scope.sortBy = function (propertyName) {
                  $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                  $scope.propertyName = propertyName;
              };


              // Change the selector if needed
              var $table = $('table.scroll'),
                  $bodyCells = $table.find('tbody tr:first').children(),
                  colWidth;

              // Adjust the width of thead cells when window resizes
              $(window).resize(function () {
                  // Get the tbody columns width array
                  colWidth = $bodyCells.map(function () {
                      return $(this).width();
                  }).get();

                  $table.find('thead tr').children().each(function (i, v) {
                      $(v).width(colWidth[i]);
                  });
              }).resize(); // Trigger resize handler


              //$http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
              //    $scope.curriculum = res.data;

              //    $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;

              //    $scope.getacyr();
              //});

              $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                  $scope.curriculum = res.data;
                  $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                  $scope.getacyr();
              });

              //function getCur(flag, comp_code) {
              //    if (flag) {

              //        $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
              //            $scope.curriculum = res.data;
              //            $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
              //            $scope.getacyr($scope.curriculum[0].sims_cur_code);

              //        });
              //    }
              //    else {

              //        $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
              //            $scope.curriculum = res.data;
              //            $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
              //            $scope.getacyr($scope.curriculum[0].sims_cur_code);
              //        });
              //    }

              //}

              //$http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
              //    $scope.count = res.data;
              //    if ($scope.count) {
              //        getCur(true, $scope.user_details.comp);
              //    }
              //    else {
              //        getCur(false, $scope.user_details.comp)
              //    }

              //});

              $(function () {
                  $('#cmb_grade').multipleSelect({
                      width: '100%'
                  });
              });

              $scope.getacyr = function () {
                  $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.edt.sims_cur_code).then(function (Academicyear) {
                      $scope.Academic_year = Academicyear.data;
                      $scope.edt['sims_academic_year'] = $scope.Academic_year[0].sims_academic_year;
                      $scope.getsections($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                  })
              }

              $scope.getsection = function (str, str1, str2) {
                  $http.get(ENV.apiUrl + "api/StudentSectionSubject/GetRCSection?curcode=" + str + "&ayear=" + str1 + "&gradecode=" + str2 + "&teachercode=" + user).then(function (Allsection) {
                      $scope.section1 = Allsection.data;
                  })
              };


              $scope.getsections = function (str, str1) {
                  $http.get(ENV.apiUrl + "api/StudentSectionSubject/GetRCGrade?curcode=" + str + "&ayear=" + str1 + "&teachercode=" + user).then(function (res) {
                      $scope.grade = res.data;
                  })
              };

              $scope.get = function (section) {

                  $http.get(ENV.apiUrl + "api/StudentSectionSubject/getSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&login_user=" + user).then(function (getSectionsubject_name) {
                      $scope.cmb_subject_name = getSectionsubject_name.data;
                      setTimeout(function () {
                          $('#cmb_grade').change(function () {
                              console.log($(this).val());
                          }).multipleSelect({
                              width: '100%'
                          });
                      }, 1000);
                  });
              }
              $scope.checkmark = function (str) {
                  debugger;
                  //if (str.sims_mark_code > 100 || str.sims_mark_code < 0 || str.sims_mark_code==undefined) {
                  //    str.sims_mark_code = '';
                  //    swal({ text:"Please Enter  Mark", width: 380 });
                  //}

                  $http.get(ENV.apiUrl + "api/BaselineMarksEntry/getmark_grade_name?sims_mark_code=" + str.sims_mark_code).then(function (markGrade_name) {
                      $scope.Grade_name = markGrade_name.data;
                      str.sims_mark_grade_name = $scope.Grade_name[0].sims_mark_grade_name;


                  });

              }
              $scope.Show_Data = function () {
                  debugger;
                  $scope.busyindicator = false;
                  $scope.info1 = [];
                  var demo = [];
                  var demo1 = [];
                  $scope.secData = [];
                  $scope.table = false;
                  $scope.subject = false;
                  $scope.showSelectedSubject = false;
                  $scope.selectedAll = false;
                  if ($scope.edt.sims_subject_code == undefined) {
                      // $scope.edt.sims_subject_code = '';
                      $scope.edt.sims_subject_code = '';
                  }
                  //$scope.edt.sims_subject_code = [];
                  var sims_subject_code = "";
                  for (var i = 0; i < $scope.edt.sims_subject_code.length; i++) {
                      sims_subject_code = sims_subject_code + $scope.edt.sims_subject_code[i] + ',';
                  }

                  $http.get(ENV.apiUrl + "api/BaselineMarksEntry/getStudentName?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&subcode=" + sims_subject_code).then(function (allSectionStudent) {
                      $scope.SectionStudent = allSectionStudent.data;
                      $scope.busyindicator = true;
                      console.log("$scope.SectionStudent",$scope.SectionStudent);
                      if (allSectionStudent.data.length > 0) {
                          //for (var j = 0; j < $scope.SectionStudent[0].sublist.length; j++) {                                                           
                          //    demo = {
                          //        'sims_subject_name_en': $scope.SectionStudent[0].sublist[j].sims_subject_name_en,
                          //        'sims_subject_code': $scope.SectionStudent[0].sublist[j].sims_subject_code
                          //    }
                          //    demo1.push(demo);                                                                              
                          //}

 

                          console.log("sims_subject_code",$scope.edt.sims_subject_code);
                          debugger
                          for (var j = 0; j < $scope.SectionStudent[0].sublist.length; j++) {
                              for (var i = 0; i < $scope.edt.sims_subject_code.length; i++) {
                                  if ($scope.SectionStudent[0].sublist[j].sims_subject_code == $scope.edt.sims_subject_code[i]) {

                                      demo = {
                                          'sims_subject_name_en': ($scope.SectionStudent[0].sublist[j].sims_subject_name_en).toLowerCase(),
                                          'sims_subject_code': $scope.SectionStudent[0].sublist[j].sims_subject_code,
                                          'sims_mark_grade_high_marks': $scope.SectionStudent[0].sublist[j].sims_mark_grade_high_marks
                                      }
                                      demo1.push(demo);

                                  }
                              }
                          }

                          /// new chalu

                          //for (var j = 0; j < $scope.cmb_subject_name.length; j++) {
                          //        for (var i = 0; i < $scope.edt.sims_subject_code.length; i++) {
                          //            if ($scope.cmb_subject_name[j].sims_subject_code == $scope.edt.sims_subject_code[i]) {

                          //                demo = {
                          //                    'sims_subject_name_en': ($scope.cmb_subject_name[j].sims_subject_name_en).toLowerCase(),
                          //                    'sims_subject_code': $scope.cmb_subject_name[j].sims_subject_code
                          //                }
                          //                demo1.push(demo);

                          //            }
                          //        }
                          //}
                          console.log("demo1", demo1);
                          $scope.info1 = demo1;
                          console.log("$scope.info1", $scope.info1);

                          for (var k = 0; k < $scope.SectionStudent.length; k++) {
                              for (var j = 0; j < $scope.SectionStudent[k].sublist.length; j++) {
                                  $scope.flg = false
                                  for (var i = 0; i < $scope.edt.sims_subject_code.length; i++) {
                                      if ($scope.edt.sims_subject_code[i] == $scope.SectionStudent[k].sublist[j].sims_subject_code) {
                                          $scope.flg = true;
                                          break;
                                      }
                                  }
                                  $scope.SectionStudent[k].sublist[j]['is_visible'] = $scope.flg;

                              }
                          }


                        
                          $timeout(function () {
                              $("#fixTable").tableHeadFixer({ 'left': 3, 'z-index': 100 });
                          }, 100);
                      }
                      else {
                          $scope.subject = true;
                          $scope.table = true;
                      }
                  });

              }

              var subject1;
              $scope.skipdSubject = [];

              $scope.Check_Single_Subject = function (enroll, subject) {
                  console.log(subject, enroll);
                  enroll.ischange = true;
                  if (subject.sims_status == false) {
                      $http.get(ENV.apiUrl + "api/StudentSectionSubject/getcheckSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&enroll_number=" + enroll.sims_student_enroll_number + "&subject_code=" + subject.sims_subject_code).then(function (msg) {
                          $scope.msg1 = msg.data;
                          if ($scope.msg1.length > 0) {
                              swal({
                                  text: 'This Subject Having Mark In Gradebook ,Are You Want Be Remove Subject?',
                                  showCloseButton: true,
                                  showCancelButton: true,
                                  confirmButtonText: 'Yes',
                                  width: 380,
                                  cancelButtonText: 'No',
                              }).then(function (isConfirm) {
                                  if (isConfirm) {
                                      subject.sims_status = false;
                                  }
                                  else {
                                      subject.sims_status = true;
                                      var v = document.getElementById(subject.sims_subject_code + enroll.sims_student_enroll_number);
                                      v.checked = true;
                                  }
                              })

                          }
                      })
                  }
              }



              $scope.getGrPts = function (str) {
                                   $http.get(ENV.apiUrl + "api/BaselineMarksEntry/getGradePoints?grCode=" + str.sims_mark_grade_code).then(function (res) {
                                       debugger;
                                       $scope.grade_Point = res.data;
                                       str.sims_mark_grade_high = $scope.grade_Point[0].sims_mark_grade_high;
                                   })
                               }


              //$scope.Select_ALL_Subject = function (str) {
              //    debugger
              //    var id = '';
              //    var subject_code = '';
              //    var student_enroll = ''
              //    $scope.ischecked = '';
              //    var check = document.getElementById(str);
              //    $scope.ischecked = str;
              //    $scope.tem = {};
              //    if (check.checked == false) {

              //        for (var i = 0; i < $scope.SectionStudent.length; i++) {
              //            student_enroll = student_enroll + $scope.SectionStudent[i].sims_student_enroll_number + ',';
              //            subject_code = str;
              //        }
              //        $http.get(ENV.apiUrl + "api/StudentSectionSubject/getcheckSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&enroll_number=" + student_enroll + "&subject_code=" + subject_code).then(function (msg) {
              //            $scope.msg1 = msg.data;
              //            if ($scope.msg1.length > 0) {
              //                $('#MyModal').modal({ backdrop: "static" });
              //            } else {

              //                for (var i = 0; i < $scope.SectionStudent.length; i++) {
              //                    $scope.SectionStudent[i].sublist[check.tabIndex].sims_status = check.checked;
              //                    $scope.SectionStudent[i].ischange = true;
              //                }
              //            }
              //        })
              //    }
              //    else {
              //        for (var i = 0; i < $scope.SectionStudent.length; i++) {
              //            $scope.SectionStudent[i].sublist[check.tabIndex].sims_status = check.checked;
              //            $scope.SectionStudent[i].ischange = true;
              //        }
              //    }
              //}



              $scope.Select_ALL_Subject = function (str, obj, index, chk) {
                  var check = document.getElementById(str);
                  var subject_code = '';
                  var student_enroll = ''
                  $scope.ischecked = '';

                  for (var i = 0; i < $scope.SectionStudent.length; i++) {
                      for (var j = 0; j < $scope.SectionStudent[i].sublist.length; j++) {
                          if ($scope.SectionStudent[i].sublist[j].sims_subject_code == obj.sims_subject_code)
                              $scope.SectionStudent[i].sublist[j]['sims_status'] = chk;
                          $scope.SectionStudent[i]['ischange'] = chk;
                      }
                  }
                  if (chk == false) {
                      for (var i = 0; i < $scope.SectionStudent.length; i++) {
                          student_enroll = student_enroll + $scope.SectionStudent[i].sims_student_enroll_number + ',';
                          subject_code = str;
                      }
                      $http.get(ENV.apiUrl + "api/StudentSectionSubject/getcheckSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&enroll_number=" + student_enroll + "&subject_code=" + subject_code).then(function (msg) {
                          $scope.msg1 = msg.data;
                          if ($scope.msg1.length > 0) {
                              $('#MyModal').modal({ backdrop: "static" });
                          } else {

                              for (var i = 0; i < $scope.SectionStudent.length; i++) {
                                  $scope.SectionStudent[i].sublist[check.tabIndex].sims_status = chk;
                                  $scope.SectionStudent[i].ischange = true;
                              }
                          }
                      })
                  }
              }

              $scope.btn_Ok_Click = function () {

                  var check = document.getElementById($scope.ischecked);
                  for (var i = 0; i < $scope.SectionStudent.length; i++) {
                      $scope.SectionStudent[i].sublist[check.tabIndex].sims_status = false;
                      $scope.SectionStudent[i].ischange = true;
                  }
              }

              $scope.btn_cancel_Click = function () {

                  var check = document.getElementById($scope.ischecked);
                  for (var i = 0; i < $scope.SectionStudent.length; i++) {
                      $scope.SectionStudent[i].sublist[check.tabIndex].sims_status = false;
                      $scope.SectionStudent[i].ischange = true;
                  }

                  for (var i = 0; i < $scope.SectionStudent.length; i++) {
                      var check = document.getElementById($scope.ischecked);
                      for (var j = 0; j < $scope.msg1.length; j++) {
                          if ($scope.SectionStudent[i].sims_student_enroll_number == $scope.msg1[j].sims_student_enroll_number) {
                              $scope.SectionStudent[i].sublist[check.tabIndex].sims_status = true;
                              $scope.SectionStudent[i].ischange = true;
                          }

                      }
                  }

              }

              $scope.Select_Student_ALL_Subject = function (str) {
                  debugger
                  var id = '';
                  var check = document.getElementById(str);
                  if (check.checked) {
                      $scope.selectedStudentList.push(str);
                      for (var i = 0; i < $scope.SectionStudent.length; i++) {
                          if ($scope.SectionStudent[i].sims_student_enroll_number == str) {
                              for (var j = 0; j < $scope.SectionStudent[i].sublist.length; j++) {
                                  $scope.SectionStudent[i].sublist[j].sims_status = check.checked;
                                  $scope.SectionStudent[i].ischange = true;
                              }
                          }
                      }
                      if ($scope.selectedStudentList.length == $scope.SectionStudent.length) {
                          $scope.selectedAll = true;
                      }
                  }
                  else {
                      $scope.selectedAll = false;
                      var index = $scope.selectedStudentList.indexOf(str);
                      $scope.selectedStudentList.splice(index, 1);
                      for (var i = 0; i < $scope.SectionStudent.length; i++) {
                          if ($scope.SectionStudent[i].sims_student_enroll_number == str) {
                              for (var j = 0; j < $scope.SectionStudent[i].sublist.length; j++) {
                                  $scope.SectionStudent[i].sublist[j].sims_status = check.checked;
                                  $scope.SectionStudent[i].ischange = true;
                              }
                          }
                      }
                  }

              }

              $scope.isSelectAll = function () {
                  $scope.selectedStudentList = [];
                  if ($scope.selectedAll) {
                      $scope.selectedAll = true;
                      for (var k = 0; k < $scope.SectionStudent.length; k++) {
                          $scope.selectedStudentList.push($scope.SectionStudent[k].sims_student_enroll_number);
                      }
                  }
                  else {
                      $scope.selectedAll = false;
                  }

                  for (var k = 0; k < $scope.SectionStudent.length; k++) {
                      for (var j = 0; j < $scope.SectionStudent[k].sublist.length; j++) {
                          for (var i = 0; i < $scope.edt.sims_subject_code.length; i++) {
                              if ($scope.edt.sims_subject_code[i] == $scope.SectionStudent[k].sublist[j].sims_subject_code) {
                                  $scope.SectionStudent[k].sublist[j].sims_status = $scope.selectedAll;
                                  $scope.SectionStudent[k].ischange = true;
                                  $scope.SectionStudent[k].chkStudent = $scope.selectedAll;
                              }
                          }
                      }
                  }

                  //for (var i = 0; i < $scope.SectionStudent.length; i++) {                    
                  //    for (var j = 0; j < $scope.SectionStudent[i].sublist.length; j++) {
                  //        $scope.SectionStudent[i].sublist[j].sims_status = $scope.selectedAll;
                  //        $scope.SectionStudent[i].ischange = true;
                  //        $scope.SectionStudent[i].chkStudent = $scope.selectedAll;
                  //    }                    
                  //}
              }

              $scope.Submit = function () {
                  debugger;
                  //$scope.busyindicator = true;
                  //$scope.table = true;
                  $scope.subject = false;
                  getdata = [];
                  var Sdata = [];
                  var data = {}
                  //for (var i = 0; i < $scope.SectionStudent.length; i++) {

                  //    if ($scope.SectionStudent[i].chkStudent == true) {

                  //        Sdata.push($scope.SectionStudent[i])
                  //    }
                  //}
                  for (var k = 0; k < $scope.SectionStudent.length; k++) {
                      for (var j = 0; j < $scope.SectionStudent[k].sublist.length; j++) {
                          if ($scope.SectionStudent[k].chkStudent == true) {

                              if ($scope.SectionStudent[k].sublist[j].colour == "#ff00006b") {
                                  swal({ text: 'Please check Marks. should not be greater than high marks', width: 380 });
                                  return;
                              }
                              var le = Sdata.length;
                              Sdata[le] = {
                                  'sims_cur_code': $scope.SectionStudent[k].sims_cur_code,
                                  'sims_academic_year': $scope.SectionStudent[k].sims_academic_year,
                                  'sims_grade_code': $scope.SectionStudent[k].sims_grade_code,
                                  'sims_section_code': $scope.SectionStudent[k].sims_section_code,
                                  'sims_student_enroll_number': $scope.SectionStudent[k].sims_student_enroll_number,


                                  'sims_subject_code': $scope.SectionStudent[k].sublist[j].sims_subject_code,
                                  'sims_mark_code': $("#" + $scope.SectionStudent[k].sims_student_enroll_number + $scope.SectionStudent[k].sublist[j].sims_subject_code).val() //$scope.SectionStudent[k].sublist[j].sims_mark_code
                              };
                          };
                      };
                  }
                  console.log("Sdata",Sdata);
                  
                  if (Sdata.length != 0) {
                      $http.post(ENV.apiUrl + "api/BaselineMarksEntry/insert_student_section_subject", Sdata).then(function (msg) {
                          $scope.msg1 = msg.data;
                          $scope.selectedAll = false;
                          Sdata = [{}];
                          data = [];
                          $scope.selectedStudentList = [];
                          swal({ text: 'Insert Succesfully', width: 380 });
                          $scope.Show_Data();
                          //$scope.busyindicator = false;
                          //$http.get(ENV.apiUrl + "api/StudentSectionSubject/getStudentName?curcode=" + $scope.cur_code + "&gradecode=" + $scope.grade_code2 + "&academicyear=" + $scope.year + "&section=" + $scope.section).then(function (allSectionStudent) {                            
                          //    //$scope.table = false;
                          //    $scope.SectionStudent = allSectionStudent.data;
                          //    demo1 = [];                            
                          //    if (allSectionStudent.data.length > 0) {
                          //        //for (var j = 0; j < $scope.SectionStudent[0].sublist.length; j++) {
                          //        //    demo = {
                          //        //        'sims_subject_name_en': $scope.SectionStudent[0].sublist[j].sims_subject_name_en,
                          //        //        'sims_subject_code': $scope.SectionStudent[0].sublist[j].sims_subject_code
                          //        //    }
                          //        //    demo1.push(demo);
                          //        //}
                          //        for (var j = 0; j < $scope.SectionStudent[0].sublist.length; j++) {
                          //            for (var i = 0; i < $scope.edt.sims_subject_code.length; i++) {
                          //                console.log($scope.edt.sims_subject_code[i]);
                          //                if ($scope.SectionStudent[0].sublist[j].sims_subject_code == $scope.edt.sims_subject_code[i]) {
                          //                    demo = {
                          //                        'sims_subject_name_en':($scope.SectionStudent[0].sublist[j].sims_subject_name_en).toLowerCase(),
                          //                        'sims_subject_code': $scope.SectionStudent[0].sublist[j].sims_subject_code
                          //                    }
                          //                    demo1.push(demo);
                          //                }
                          //            }
                          //        }
                          //        $scope.info1 = demo1;
                          //        $timeout(function () {
                          //            $("#fixTable").tableHeadFixer({ 'left': 3, 'z-index': 40000 });
                          //        }, 100);
                          //        //$scope.table = false;
                          //    }
                          //    else {
                          //        $scope.subject = true;
                          //    }
                          //});
                      });
                      senddata = [];
                      
                  }
                  else {
                      swal({ text: 'Not Insert Recored', width: 380 });
                  }
              }

              $scope.save1 = false;
              $scope.studesubmark = function (str) {
                  debugger;
                  console.log('DATA' + str)


                  var cnt = 0;

                  if (str.sims_mark_code == undefined) {
                      str.sims_mark_code = '';

                  }
                  if (parseInt(str.sims_mark_grade_high_marks) <= parseInt(str.sims_mark_code)) {

                      //str.sims_mark_code = '';
                      str['colour'] = "#ff00006b";

                      cnt = cnt + 1;
                      $scope.save1 = true;
                  } else {
                      str['colour'] = null;
                      cnt = cnt - 1;

                      $scope.save1 = false;
                  }
              
               
                  //for (var i = 0; i <= str.length - 1; i++) {

                  //    for (var j = 0; j < str[i].sublist.length; j++) {

                      
                  //        if (str[i].sublist[j].sims_mark_code == undefined) {
                  //            str[i].sublist[j].sims_mark_code = 0;
                  //           }

                  //        if (parseInt(str[i].sublist[j].sims_mark_grade_high_marks) <= parseInt(str[i].sublist[j].sims_mark_code) || str[i].sublist[j].sims_mark_code == undefined) {
                  //            str[i].sublist[j]['colour'] = "red";

                  //            cnt = cnt + 1;
                  //          }
                  //else {
                              
                  //            str[i].sublist[j]['colour'] = "null";
                  //   // cnt = cnt - 1;
                  //    //str[i].sublist[j].sims_mark_code = 0;
                      
                  //    //break;
                  //}
                  //}
                  //}




                  //if (cnt>0) {
                  //    $scope.save1 = false;
                  //} else {
                  //    $scope.save1 = true;
                       
                  //}
              }
          }]);

    //old code
    //['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

    //    $scope.edt = {};
    //    $scope.edt['sims_subject_code'] = [];
    //    $scope.simsArray = [];
    //    $scope.selectedStudentList = [];
    //    var user = $rootScope.globals.currentUser.username;
    //    $scope.busyindicator = true;
    //    $scope.propertyName = null;
    //    $scope.reverse = false;
    //    $scope.grade_new = false;
    //    $scope.marks_new = false;
    //    $scope.busyindicator = true;

    //    // propertyName = null; reverse = false
    //    $scope.sortBy = function (propertyName) {
    //        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
    //        $scope.propertyName = propertyName;
    //    };


    //    // Change the selector if needed
    //    var $table = $('table.scroll'),
    //        $bodyCells = $table.find('tbody tr:first').children(),
    //        colWidth;

    //    // Adjust the width of thead cells when window resizes
    //    $(window).resize(function () {
    //        // Get the tbody columns width array
    //        colWidth = $bodyCells.map(function () {
    //            return $(this).width();
    //        }).get();

    //        // Set the width of thead columns
    //        $table.find('thead tr').children().each(function (i, v) {
    //            $(v).width(colWidth[i]);
    //        });
    //    }).resize(); // Trigger resize handler


    //    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum?username=" + $rootScope.globals.currentUser.username).then(function (res) {
    //        $scope.curriculum = res.data;
    //        $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
    //        $scope.getacyr();
    //    });

    //    $(function () {
    //        $('#cmb_grade').multipleSelect({
    //            width: '100%'
    //        });
    //    });

    //    $scope.getacyr = function () {
    //        $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.edt.sims_cur_code).then(function (Academicyear) {
    //            $scope.Academic_year = Academicyear.data;
    //            $scope.edt['sims_academic_year'] = $scope.Academic_year[0].sims_academic_year;
    //            $scope.getsections($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
    //        })
    //    }

    //    $scope.getsection = function (str, str1, str2) {
    //        $http.get(ENV.apiUrl + "api/StudentSectionSubject/GetRCSection?curcode=" + str + "&ayear=" + str1 + "&gradecode=" + str2 + "&teachercode=" + user).then(function (Allsection) {
    //            $scope.section1 = Allsection.data;
    //        })
    //    };

    //    $scope.getgrade = function (str, str1) {
    //        $http.get(ENV.apiUrl + "api/StudentSectionSubject/GetRCGrade?curcode=" + str + "&ayear=" + str1 + "&teachercode=" + user).then(function (res) {
    //            $scope.grades = res.data;
    //        })
    //    };

    //    $scope.get = function (section) {

    //        $http.get(ENV.apiUrl + "api/BaselineMarksEntry/getSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&login_user=" + user).then(function (getSectionsubject_name) {
    //            $scope.cmb_subject_name = getSectionsubject_name.data;
    //            setTimeout(function () {
    //                $('#cmb_grade').change(function () {
    //                    console.log($(this).val());
    //                }).multipleSelect({
    //                    width: '100%'
    //                });
    //            }, 1000);
    //        });
    //    }

    //    $scope.Show_Data = function () {
    //        debugger;
    //        $scope.busyindicator = false;
    //        $scope.info1 = [];
    //        var demo = [];
    //        var demo1 = [];
    //        $scope.secData = [];
    //        $scope.table = false;
    //        $scope.subject = false;
    //        $scope.showSelectedSubject = false;
    //        $scope.selectedAll = false;

    //        if ($scope.edt.sims_subject_code == undefined) {

    //            $scope.edt.sims_subject_code = '';
    //        }

    //        var sims_subject_code = "";

    //        for (var i = 0; i < $scope.edt.sims_subject_code.length; i++) {
    //            sims_subject_code = sims_subject_code + $scope.edt.sims_subject_code[i] + ',';
    //        }

    //        $http.get(ENV.apiUrl + "api/BaselineMarksEntry/getStudentName?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&subcode=" + sims_subject_code).then(function (allSectionStudent) {
    //            $scope.SectionStudent = allSectionStudent.data;
    //            $scope.busyindicator = true;
    //            debugger;
    //            if (allSectionStudent.data.length > 0) {
    //                for (var j = 0; j < $scope.SectionStudent[0].sublist.length; j++) {
    //                    for (var i = 0; i < $scope.edt.sims_subject_code.length; i++) {
    //                        if ($scope.SectionStudent[0].sublist[j].sims_subject_code == $scope.edt.sims_subject_code[i]) {

    //                            demo = {
    //                                'sims_subject_name_en': ($scope.SectionStudent[0].sublist[j].sims_subject_name_en).toLowerCase(),
    //                                'sims_subject_code': $scope.SectionStudent[0].sublist[j].sims_subject_code
    //                            }
    //                            demo1.push(demo);

    //                        }
    //                    }
    //                }

    //                $scope.marks = false;
    //                $scope.grade = false;

    //                $http.get(ENV.apiUrl + "api/BaselineMarksEntry/getType?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&acyear=" + $scope.edt.sims_academic_year + "&sectcode=" + $scope.edt.sims_section_name + "&subcode=" + sims_subject_code).then(function (res) {
    //                    debugger;
    //                    $scope.baselineType = res.data;

    //                    for (var i = 0; i < baselineType.length; i++) {
    //                        if ($scope.baselineType[i].sims_subject_baseline_score_type == 'M') {
    //                            $scope.marks_new = true;
    //                            $scope.grade_new = false;
    //                        }
    //                        else

    //                            if ($scope.baselineType[i].sims_subject_baseline_score_type == 'G') {
    //                                $scope.grade_new = true;
    //                                $scope.marks_new = false;
    //                            }
    //                    }
    //                });

    //                //var mark1 = parseFloat(info.sims_subject_baseline_max_score);
    //                //var maxmark = (mark1 / 10) * 100;
    //                //$scope.BaseGrade = function (str, info) { 
    //                //     if ($scope.baselineType.length > 0) {
    //                //         var str1 = parseFloat(str);
    //                //         var mark1 = parseFloat(info.sims_subject_baseline_max_score);
    //                //         var maxmark = (mark1 / 10) * 100;
    //                //         var maxmark1 = Math.round((str1 * 100) / mark1);

    //                //         if (str1 <= mark1) {
    //                //             for (var i = 0; i < $scope.baselineType.length; i++) {
    //                //                 if (maxmark1 <= parseFloat($scope.baselineType[i].sims_mark_grade_high) && maxmark1 >= parseFloat($scope.baselineType[i].sims_mark_grade_low)) {
    //                //                     if (info.sims_subject_baseline_score_type == $scope.baselineType[i].gradeGroupCode) 
    //                //                     {
    //                //                         info.sims_mark_grade_code = $scope.baselineType[i].sims_mark_grade_code;
    //                //                         info.sims_mark_grade_name = $scope.baselineType[i].sims_mark_grade_name;                                   
    //                //                     }
    //                //                 }
    //                //             }
    //                //         }
    //                //   $scope.BaseMark = function (str, info) {

    //                //                 if (str != '') {
    //                //                     if ($scope.baselineType.length > 0) {
    //                //                         for (var i = 0; i < $scope.baselineType.length; i++) {
    //                //                             if ($scope.baselineType[i].sims_mark_grade_name == str && info.sims_gb_grade_scale == $scope.baselineType[i].gradeGroupCode) {
    //                //                                 for (var j = 0; j < $scope.student_data.length; j++) {
    //                //                                     info.sims_mark_grade_name = $scope.Grade_Scale_Data[i].sims_mark_grade_name;
    //                //                                     info.sims_mark_grade_point = $scope.Grade_Scale_Data[i].sims_mark_grade_point;
    //                //                                 }
    //                //                             }
    //                //                         } 
    //                //                     }

    //                $http.get(ENV.apiUrl + "api/BaselineMarksEntry/getGrade").then(function (res) {
    //                    debugger;
    //                    $scope.grade1 = res.data;
    //                    $scope.getGrPts($scope.edt.sims_mark_grade_code);
    //                });

    //                $scope.getGrPts = function (str) {
    //                    $http.get(ENV.apiUrl + "api/BaselineMarksEntry/getGradePoints?grCode=" + str.sims_mark_grade_code).then(function (res) {
    //                        debugger;
    //                        $scope.grade_Point = res.data;
    //                        str.sims_mark_grade_high = $scope.grade_Point[0].sims_mark_grade_high;
    //                    })
    //                }

    //                $scope.info1 = demo1;
    //                $timeout(function () {
    //                    $("#fixTable").tableHeadFixer({ 'left': 3, 'z-index': 100 });
    //                }, 100);

    //                $scope.table = true;

    //            }

    //        });

    //    }
    //    $scope.skipdSubject = [];

    //    $scope.Check_Single_Subject = function (enroll, subject) {
    //        console.log(subject, enroll);
    //        enroll.ischange = true;
    //        if (subject.sims_status == false) {
    //            $http.get(ENV.apiUrl + "api/BaselineMarksEntry/getcheckSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&enroll_number=" + enroll.sims_student_enroll_number + "&subject_code=" + subject.sims_subject_code).then(function (msg) {
    //                $scope.msg1 = msg.data;
    //                if ($scope.msg1.length > 0) {
    //                    swal({
    //                        text: 'This Subject Having Mark In Gradebook ,Are You Want Be Remove Subject?',
    //                        showCloseButton: true,
    //                        showCancelButton: true,
    //                        confirmButtonText: 'Yes',
    //                        width: 380,
    //                        cancelButtonText: 'No',
    //                    }).then(function (isConfirm) {
    //                        if (isConfirm) {
    //                            subject.sims_status = false;
    //                        }
    //                        else {
    //                            subject.sims_status = true;
    //                            var v = document.getElementById(subject.sims_subject_code + enroll.sims_student_enroll_number);
    //                            v.checked = true;
    //                        }
    //                    })

    //                }
    //            })
    //        }
    //    }

    //    $scope.Select_ALL_Subject = function (str, obj, index, chk) {
    //        var check = document.getElementById(str);
    //        var subject_code = '';
    //        var student_enroll = ''
    //        $scope.ischecked = '';

    //        for (var i = 0; i < $scope.SectionStudent.length; i++) {
    //            for (var j = 0; j < $scope.SectionStudent[i].sublist.length; j++) {
    //                if ($scope.SectionStudent[i].sublist[j].sims_subject_code == obj.sims_subject_code)
    //                    $scope.SectionStudent[i].sublist[j]['sims_status'] = chk;
    //                $scope.SectionStudent[i]['ischange'] = chk;
    //            }
    //        }
    //        if (chk == false) {
    //            for (var i = 0; i < $scope.SectionStudent.length; i++) {
    //                student_enroll = student_enroll + $scope.SectionStudent[i].sims_student_enroll_number + ',';
    //                subject_code = str;
    //            }
    //            $http.get(ENV.apiUrl + "api/BaselineMarksEntry/getcheckSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&enroll_number=" + student_enroll + "&subject_code=" + subject_code).then(function (msg) {
    //                $scope.msg1 = msg.data;
    //                if ($scope.msg1.length > 0) {
    //                    $('#MyModal').modal({ backdrop: "static" });
    //                } else {

    //                    for (var i = 0; i < $scope.SectionStudent.length; i++) {
    //                        $scope.SectionStudent[i].sublist[check.tabIndex].sims_status = chk;
    //                        $scope.SectionStudent[i].ischange = true;
    //                    }
    //                }
    //            })
    //        }
    //    }

    //    $scope.Select_Student_ALL_Subject = function (str) {
    //        var id = '';
    //        var check = document.getElementById(str);
    //        if (check.checked) {
    //            $scope.selectedStudentList.push(str);
    //            for (var i = 0; i < $scope.SectionStudent.length; i++) {
    //                if ($scope.SectionStudent[i].sims_student_enroll_number == str) {
    //                    for (var j = 0; j < $scope.SectionStudent[i].sublist.length; j++) {
    //                        $scope.SectionStudent[i].sublist[j].sims_status = check.checked;
    //                        $scope.SectionStudent[i].ischange = true;
    //                    }
    //                }
    //            }
    //            if ($scope.selectedStudentList.length == $scope.SectionStudent.length) {
    //                $scope.selectedAll = true;
    //            }
    //        }
    //        else {
    //            $scope.selectedAll = false;
    //            var index = $scope.selectedStudentList.indexOf(str);
    //            $scope.selectedStudentList.splice(index, 1);
    //            for (var i = 0; i < $scope.SectionStudent.length; i++) {
    //                if ($scope.SectionStudent[i].sims_student_enroll_number == str) {
    //                    for (var j = 0; j < $scope.SectionStudent[i].sublist.length; j++) {
    //                        $scope.SectionStudent[i].sublist[j].sims_status = check.checked;
    //                        $scope.SectionStudent[i].ischange = true;
    //                    }
    //                }
    //            }
    //        }

    //    }

    //    $scope.isSelectAll = function () {
    //        $scope.selectedStudentList = [];
    //        if ($scope.selectedAll) {
    //            $scope.selectedAll = true;SectionStudent
    //            for (var k = 0; k < $scope.SectionStudent.length; k++) {
    //                $scope.selectedStudentList.push($scope.SectionStudent[k].sims_student_enroll_number);
    //            }
    //        }
    //        else {
    //            $scope.selectedAll = false;
    //        }

    //        for (var k = 0; k < $scope.SectionStudent.length; k++) {
    //            for (var j = 0; j < $scope.SectionStudent[k].sublist.length; j++) {
    //                for (var i = 0; i < $scope.edt.sims_subject_code.length; i++) {
    //                    if ($scope.edt.sims_subject_code[i] == $scope.SectionStudent[k].sublist[j].sims_subject_code) {
    //                        $scope.SectionStudent[k].sublist[j].sims_status = $scope.selectedAll;
    //                        $scope.SectionStudent[k].ischange = true;
    //                        $scope.SectionStudent[k].chkStudent = $scope.selectedAll;
    //                    }
    //                }
    //            }
    //        }
    //    }

    //    $scope.Submit = function () {
    //        $scope.subject = false;
    //        getdata = [];
    //        var Sdata = [{}];
    //        for (var i = 0; i < $scope.SectionStudent.length; i++) {

    //            if ($scope.SectionStudent[i].ischange == true) {
    //                var le = Sdata.length;
    //                Sdata[le] = {
    //                    'sims_student_enroll_number': $scope.SectionStudent[i].sims_student_enroll_number,
    //                    'sims_cur_code': $scope.SectionStudent[i].sims_cur_code,
    //                    'sims_academic_year': $scope.SectionStudent[i].sims_academic_year,
    //                    'sims_grade_code': $scope.SectionStudent[i].sims_grade_code,
    //                    'sims_section_code': $scope.SectionStudent[i].sims_section_code,
    //                    'sims_subject_name_en': ''

    //                };
    //                var sbcode = '';
    //                var sbcode1 = '';
    //                for (var j = 0; j < $scope.SectionStudent[i].sublist.length; j++) {
    //                    if ($scope.SectionStudent[i].sublist[j].sims_status == true) {
    //                        sbcode = sbcode + $scope.SectionStudent[i].sublist[j].sims_subject_code + ',';
    //                    }
    //                    else {

    //                        sbcode1 = sbcode1 + $scope.SectionStudent[i].sublist[j].sims_subject_code + ',';
    //                    }
    //                }

    //                Sdata[le].sims_subject_name_en = sbcode;
    //                Sdata[le].sims_subject_code = sbcode1;
    //            }
    //        }

    //        Sdata.splice(0, 1);
    //        var data = Sdata;
    //        if (Sdata.length != 0) {
    //            $http.post(ENV.apiUrl + "api/StudentSectionSubject/insert_student_section_subject", data).then(function (msg) {
    //                $scope.msg1 = msg.data;
    //                $scope.selectedAll = false;
    //                Sdata = [{}];
    //                data = [];
    //                $scope.selectedStudentList = [];
    //                swal({ text: $scope.msg1.strMessage, width: 380 });
    //                $scope.Show_Data();

    //            });
    //            senddata = [];
    //            //$scope.table = false;

    //            // subject1.checked = false;
    //        }
    //        else {
    //            swal({ text: $scope.msg1.strMessage, width: 380 });
    //        }
    //    }
    //    $scope.Reset = function () {
    //        $scope.edt = {
    //            sims_cur_code: '',
    //            sims_academic_year: '',
    //            sims_grade_code: '',
    //            sims_section_name: '',
    //            sims_subject_code: ''
    //        }
    //        $scope.table = false;

    //    }
    //}]);

    
})();



