﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SurveyRatingEvaluationCont',
        ['$scope', '$state', '$rootScope', '$timeout', '$stateParams','gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, $stateParams, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = true;
            $scope.table = true;
            //$scope.grid = false;
            //$scope.cmbstatus = true;
            //$scope.checked = false;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100)

            //Select Data SHOW
            $http.get(ENV.apiUrl + "api/Survey/get_allSurvey").then(function (res) {
                debugger;
                $scope.survey_names = res.data;
            });

            $scope.exportData = function () {
                var check = true;
                debugger;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Export " + $scope.obj1.lic_school_name + " Survey Rating Report.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger;
                            var blob = new Blob([document.getElementById('dataexport').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " Survey Report" + ".xls");                            
                        }
                    });
                }
                else {
                    swal({ text: "Report Not Saved", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, })
                }

            }

            $scope.savedata = function () {
                debugger;
                $scope.grid = true;
                $http.post(ENV.apiUrl + "api/SurveyRatingEval/SurveyRatingDetails?sur_code=" + $scope.temp.sims_survey_code + "&sdate=" + $scope.temp.start_date + "&edate=" + $scope.temp.end_date).then(function (res1) {

                    $scope.CreDiv = res1.data;
                    $scope.totalItems = $scope.CreDiv.length;
                    // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                    $scope.todos = $scope.CreDiv;
                    $scope.makeTodos();
                    if ($scope.CreDiv.length < 0) {
                        swal({ text: "No records found.", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                    }
                });
            }
            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.CreDiv;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                debugger;
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_survey_user_response_user_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_survey_student_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_survey_question_desc_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_survey_rating_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = {};
                $scope.filteredTodos = [];
                $scope.grid = false;
                
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }
            
        }])

})();