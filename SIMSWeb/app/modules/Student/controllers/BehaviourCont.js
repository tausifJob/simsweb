﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('BehaviourCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.temp = {};
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $scope.beha_url = ENV.apiUrl + 'Content' + '/' + $http.defaults.headers.common['schoolId'] + "/Docs/Behaviour/";
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            //Select Data SHOW
            $http.get(ENV.apiUrl + "api/Behaviour/getAllBehaviour").then(function (res1) {
                $scope.studDevice = res1.data;
                $scope.totalItems = $scope.studDevice.length;
                $scope.todos = $scope.studDevice;
                $scope.makeTodos();
            });


            //$http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
            //    $scope.curriculum = AllCurr.data;
            //    $scope.temp = {
            //        'sims_behaviour_cur_code': $scope.curriculum[0].sims_cur_code
            //    }
            //  //  $scope.getAccYear($scope.temp.sims_cur_code);
            //});


            //$http.get(ENV.apiUrl + "api/StudentDevice/GetDeviceType").then(function (res1) {
            //    $scope.studDeviceType = res1.data;
            //});

            

            $http.get(ENV.apiUrl + "api/Behaviour/GetBehaviourType").then(function (res3) {
                $scope.behaviourType = res3.data;
            });

            $scope.size = function (str) {
               if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.studDevice;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


           

            //*******************************MODEL BOX CODE***********************************
            debugger;
            $scope.temp = {};
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                if (AllCurr.data.length > 0) {
                    $scope.temp = {
                     'sims_behaviour_cur_code': $scope.curriculum[0].sims_cur_code
                         }
                    $scope.getCurrLevel($scope.temp.sims_behaviour_cur_code);
                }
                console.log("Cur Code:",$scope.curriculum);
            });


            function getCur(flag, comp_code) {
                if (flag) {
                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        $scope.curriculum = res.data;
                        $scope.temp = {
                                   'sims_behaviour_cur_code': $scope.curriculum[0].sims_cur_code
                                       }
                        $scope.getCurrLevel($scope.curriculum[0].sims_cur_code);
                    });
                }
                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        $scope.curriculum = res.data;
                        $scope.temp = {
                            'sims_behaviour_cur_code': $scope.curriculum[0].sims_cur_code
                        }
                        $scope.getCurrLevel($scope.curriculum[0].sims_cur_code);
                    });
                }

            }

            $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.count = res.data;
                if ($scope.count) {
                    getCur(true, $scope.user_details.comp);
                }
                else {
                    getCur(false, $scope.user_details.comp)
                }

            });



            debugger;
            $scope.getCurrLevel = function (curCode) {
                $http.get(ENV.apiUrl + "api/Behaviour/getCuriculumLevel?curCode=" + curCode).then(function (res2) {
                    $scope.curLevel = res2.data;
                    if (res2.data.length > 0) {
                        $scope.temp.sims_behaviour_cur_level_code = res2.data[0].sims_cur_code;
                    }
                    console.log("Curr Level:",$scope.curLevel);
                });
            }

            //$scope.getAccYear = function (curCode) {
            //    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
            //        $scope.Acc_year = Acyear.data;
            //        if (Acyear.data.length > 0) {
            //            $scope.temp.sims_academic_year = Acyear.data[0].sims_academic_year;
            //            $scope.getGrade($scope.temp.s_cur_code, $scope.temp.sims_academic_year);
            //        }
            //        console.log($scope.Acc_year);
            //    });
            //}

            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    console.log($scope.Grade_code);
                });
            }

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    console.log($scope.Section_code);
                });
            }



            $scope.Search = function () {
                $scope.temp = {};
                debugger;
                if ($scope.curriculum.length > 0) {
                    $scope.temp.s_cur_code = $scope.curriculum[0].sims_cur_code;
                    $scope.getAccYear($scope.temp.s_cur_code);
                }
                if ($scope.Acc_year.length > 0) {
                    $scope.temp.sims_academic_year = $scope.Acc_year[0].sims_academic_year;
                    $scope.getGrade($scope.temp.s_cur_code, $scope.temp.sims_academic_year);
                }

                $('#MyModal').modal('show');
            }
            $scope.SearchSudent = function () {

                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    main.checked = false;
                }
                $scope.searchtable = false;
                $scope.busy = true;
                $scope.sibling_result;
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.temp)).then(function (Allstudent) {

                    $scope.student = Allstudent.data;
                    $scope.searchtable = true;
                    $scope.busy = false;
                });
            }

            $scope.DataEnroll = function () {
                for (var i = 0; i < $scope.student.length; i++) {
                    var t = $scope.student[i].s_enroll_no;
                    var v = document.getElementById(t);
                    if (v.checked == true)

                        $scope.temp =
                       {
                           enroll_number: $scope.student[i].s_enroll_no
                       };
                }
            }

            //*****************************************************************************
           


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.studDevice, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.studDevice;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked()

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_cur_level_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_behaviour_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_appl_form_field_value1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_behaviour_description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_behaviour_points == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {

                $scope.disabled = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.search_btn = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.divcode_readonly = false;
                $scope.temp = "";
                $scope.temp = {};
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
               
                $scope.temp = {
                    'sims_behaviour_cur_code': $scope.curriculum[0].sims_cur_code
                }
                $scope.temp.sims_behaviour_status = true;
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.behaviour_photo = '';
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger;
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.divcode_readonly = true;
                // $scope.temp = str;


                $scope.temp = {
                    'sims_behaviour_cur_code': str.sims_behaviour_cur_code,
                    'sims_behaviour_cur_level_code': str.sims_behaviour_cur_level_code, 
                    'sims_behaviour_code': str.sims_behaviour_code,
                    'sims_behaviour_type': str.sims_behaviour_type,
                    'sims_behaviour_description': str.sims_behaviour_description,
                    'sims_behaviour_points': str.sims_behaviour_points,
                    'sims_behaviour_img': str.sims_behaviour_img,
                    'sims_behaviour_status': str.sims_behaviour_status
                }
                //'sims_behaviour_img': str.sims_behaviour_img,

                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            $scope.downloaddoc = function (str) {
                $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/';
                $scope.url = $scope.imageUrl + str.sims_behaviour_img;
                $scope.url = 'http://api.mograsys.com/APIERP/Content/adis/Images/StudentImages/' + str.sims_behaviour_img;
                window.open($scope.url, '_new');
            }


            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {
                $scope.filesize = true;

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);

                    var i = 0;
                    //if ($files[i].size > 200000) {
                    //    $scope.filesize = false;
                    //    $scope.edt.photoStatus = false;

                    //    swal({ title: "Alert", text: "File Should Not Exceed 200Kb.", imageUrl: "assets/img/notification-alert.png", });

                    //}
                    //else {




                    //}

                });
            };
            
            var fortype = '';
            var photofile;
            $scope.file_changed = function (element, str) {
                debugger;
                photofile = element.files[0];


                $scope.photo_filename = (photofile.name);

                var len = 0;
                len = $scope.photo_filename.split('.');
                fortype = $scope.photo_filename.split('.')[len.length - 1];
               // $scope.temp.sims_behaviour_img = $scope.photo_filename;
                $scope.edt1 = { sims_behaviour_img: $scope.photo_filename }
                $.extend($scope.temp, $scope.edt1)
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;

                    });
                };
                reader.readAsDataURL(photofile);


            };


            $scope.uploadClick = function (str) {
                $scope.filesize = true;
                //$scope.edt = str;
                //$scope.edt.photoStatus = true;
                //$scope.ins = false;
                formdata = new FormData();
            }


            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger;
                if (Myform) {
                    var data = $scope.temp;
                    $scope.ImageView = false;
                    $scope.imageUrl = '';
                    var v = new Date();
                  var  file_name = 'Behaviour' + v.getDay() + '_' + v.getMonth() + '_' + v.getYear() + '_' + v.getHours() + '_' + v.getMonth() + '_' + v.getSeconds();

                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + 'api/file/uploadDocument_new?filename=' + file_name + "&location=" + "Docs/Behaviour/",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    $http(request).success(function (d) {

                       data.opr = 'I';
                       data['sims_behaviour_img'] = d;
                       datasend.push(data);
                    $http.post(ENV.apiUrl + "api/Behaviour/CUDInsertBehaviour", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        }
                        else {
                            swal({ text: "Record Not Inserted. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }

                        $http.get(ENV.apiUrl + "api/Behaviour/getAllBehaviour").then(function (res1) {
                            $scope.studDevice = res1.data;
                            $scope.totalItems = $scope.studDevice.length;
                            $scope.todos = $scope.studDevice;
                            $scope.makeTodos();

                    
                        });

                    });

                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                    });

                }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                if (Myform) {
                    debugger;
                    var data = $scope.temp;

                    $scope.ImageView = false;
                    $scope.imageUrl = '';
                    var v = new Date();
                    var  file_name = 'Behaviour' + v.getDay() + '_' + v.getMonth() + '_' + v.getYear() + '_' + v.getHours() + '_' + v.getMonth() + '_' + v.getSeconds();

                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + 'api/file/uploadDocument_new?filename=' + file_name + "&location=" + "Docs/Behaviour/",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    $http(request).success(function (d) {

                        data.opr = "U";
                        data['sims_behaviour_img'] = d;
                    dataforUpdate.push(data);
                    //dataupdate.push(data);
                    $http.post(ENV.apiUrl + "api/Behaviour/CUDInsertBehaviour", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        }
                        else {
                            swal({ text: "Record Not Updated. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }
                        $http.get(ENV.apiUrl + "api/Behaviour/getAllBehaviour").then(function (res1) {
                            $scope.studDevice = res1.data;
                            $scope.totalItems = $scope.studDevice.length;
                            $scope.todos = $scope.studDevice;
                            $scope.makeTodos();
                        });

                    });
                    dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                    });
                }
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('abc' + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('abc' + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }


            $scope.OkDelete = function () {
                debugger;
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById('abc' + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_behaviour_code': $scope.filteredTodos[i].sims_behaviour_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            debugger;
                            $http.post(ENV.apiUrl + "api/Behaviour/CUDInsertBehaviour", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Behaviour/getAllBehaviour").then(function (res1) {
                                                $scope.studDevice = res1.data;
                                                $scope.totalItems = $scope.studDevice.length;
                                                $scope.todos = $scope.studDevice;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal({ text: "Record Not Deleted. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Behaviour/getAllBehaviour").then(function (res1) {
                                                $scope.studDevice = res1.data;
                                                $scope.totalItems = $scope.studDevice.length;
                                                $scope.todos = $scope.studDevice;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById('abc' + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    //$scope.row1 = '';
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            simsController.directive('ngFiles', ['$parse', function ($parse) {

                function fn_link(scope, element, attrs) {
                    var onChange = $parse(attrs.ngFiles);
                    element.on('change', function (event) {
                        onChange(scope, { $files: event.target.files });
                    });
                };

                return {
                    link: fn_link
                }
            }])


         }])

})();
