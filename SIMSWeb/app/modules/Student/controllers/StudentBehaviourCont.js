﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudentBehaviourCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.display = false;
            $scope.table1 = true;
            $scope.fee_no = true;
            $scope.table = true;
            $scope.visit_no = true;
            var check_no = null;
            $scope.busy = false;
            $scope.chku = false;
            $scope.edt = [];
            $scope.showSaveBtn = false;
            $scope.showoOnCopy = false;
            $scope.temp = {};
            $scope.temp.sims_behaviour_date = $filter('date')(new Date(), 'dd-MM-yyyy');

            //CUR
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.temp['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.getAccYear($scope.temp.sims_cur_code)
            });


            function getCur(flag, comp_code) {
                if (flag) {
                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        $scope.curriculum = res.data;
                        $scope.temp['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                        $scope.getAccYear($scope.temp.sims_cur_code)
                    });
                }
                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        $scope.curriculum = res.data;                       
                        $scope.temp['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                        $scope.getAccYear($scope.temp.sims_cur_code)
                    });
                }

            }

            $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.count = res.data;
                if ($scope.count) {
                    getCur(true, $scope.user_details.comp);
                }
                else {
                    getCur(false, $scope.user_details.comp)
                }

            });



            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                    $scope.temp['sims_academic_year'] = $scope.Acc_year[0].sims_academic_year;
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                });

            }

          
            //Grade
            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
                $scope.getSection($scope.temp.sims_cur_code, $scope.temp.sims_grade_code, $scope.temp.sims_academic_year)
            }

            //Section
            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }


            //Academic year
            //$http.get(ENV.apiUrl + "api/StudentBehaviour/getAcademicYearActive").then(function (AllacademicActive) {
            //    $scope.getAllactive = AllacademicActive.data;
            //    if (AllacademicActive.data.length > 0) {
            //        $scope.edt = {
            //            'sims_academic_years': $scope.getAllactive[0].sims_academic_years
            //        }
            //    }
            //});

            //$http.get(ENV.apiUrl + "api/StudentBehaviour/getBehaviourComment?cur=" + $scope.temp.sims_cur_code + "&a_year=" + $scope.temp.sims_academic_year + "&grades=" + $scope.temp.sims_grade_code).then(function (res) { //+ "&section=" + $scope.temp.sims_section_code
            //    $scope.behaviourCode = res.data;               
            //});

            $scope.Submit = function (Myform)
            {
                if (Myform) {
                    debugger
                    $scope.promotegrade = true;
                    $scope.newpromotegrade = false;
                    $http.get(ENV.apiUrl + "api/StudentBehaviour/getAllStudentBehaviour?cur=" + $scope.temp.sims_cur_code + "&a_year=" + $scope.temp.sims_academic_year + "&grades=" + $scope.temp.sims_grade_code + "&section=" + $scope.temp.sims_section_code).then(function (AllTermDetails) { //+ "&advanced_year=" + $scope.edt.sims_academic_years
                        $scope.getAllTerm_details = AllTermDetails.data;                        
                        $scope.totalItems = $scope.getAllTerm_details.length;
                        $scope.todos = $scope.getAllTerm_details;
                        $scope.makeTodos();
                        $scope.size($scope.pagesize);

                        if ($scope.getAllTerm_details.length > 0) {
                            $scope.showSaveBtn = true;
                        }
                        else {
                            $scope.showSaveBtn = false;
                        }


                    });


                    $http.get(ENV.apiUrl + "api/StudentBehaviour/getBehaviourComment?cur=" + $scope.temp.sims_cur_code + "&a_year=" + $scope.temp.sims_academic_year + "&grades=" + $scope.temp.sims_grade_code).then(function (res) { //+ "&section=" + $scope.temp.sims_section_code
                        $scope.behaviourCode = res.data;                       
                    });
                }
            }

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.getAllTerm_details;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.getAllTerm_details, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.getAllTerm_details;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.std_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.New = function () {
                $scope.temp = {};
                $scope.edt = '';
                $scope.display = true;
                $scope.table = false;
                $scope.table1 = false;
                $scope.instaminimuma = true;
                $scope.update1 = false;
                $scope.Update_btn = false;
                $scope.show_btn = true;
            }

            $scope.savedata = function () {                
                debugger;
                var deleteintcode = [];
                var Savedata = [];
                $scope.flag = false;                
                if ($scope.temp.copyAll == true) {
                    if ($scope.temp.sims_behaviour_code == undefined || $scope.temp.sims_behaviour_code=='') {
                        swal({ title: 'Alert', text: 'Please select behaviour', width: 380, height: 200 });
                        return;
                    }
                    else if ($scope.temp.sims_behaviour_note == undefined || $scope.temp.sims_behaviour_note == '') {
                        swal({ title: 'Alert', text: 'Please enter behaviour description', width: 380, height: 200 });
                        return;
                    }
                    else {
                        $scope.busy = true;
                        for (var i = 0; i < $scope.getAllTerm_details.length; i++) {
                            var deleteintcode = ({
                                'sims_behaviour_enroll_number': $scope.getAllTerm_details[i].sims_enroll_number,
                                'sims_behaviour_code': $scope.temp.sims_behaviour_code,
                                'sims_behaviour_note': $scope.temp.sims_behaviour_note,
                                'sims_behaviour_date': $scope.temp.sims_behaviour_date,
                                opr: 'I',
                            });
                            Savedata.push(deleteintcode);
                        }

                        $http.post(ENV.apiUrl + "api/StudentBehaviour/CUDInsertStudentBehaviour", Savedata).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 380, height: 200 });
                                $scope.busy = false;
                                deleteintcode = [];
                                Savedata = [];
                                $scope.Submit();
                                $scope.Cancel();
                            }
                            else {
                                $scope.busy = false;
                                swal({ text: "Record Inserted Successfully. ", imageUrl: "assets/img/check.png", width: 380, height: 200 });
                                $scope.Cancel();
                            }
                            $http.get(ENV.apiUrl + "api/StudentBehaviour/getAllStudentBehaviour?cur=" + $scope.temp.sims_cur_code + "&a_year=" + $scope.temp.sims_academic_year + "&grades=" + $scope.temp.sims_grade_code + "&section=" + $scope.temp.sims_section_code).then(function (AllTermDetails) { //+ "&advanced_year=" + $scope.edt.sims_academic_years                                
                                $scope.getAllTerm_details = AllTermDetails.data;
                                $scope.totalItems = $scope.getAllTerm_details.length;
                                $scope.todos = $scope.getAllTerm_details;
                                $scope.makeTodos();
                                $scope.size($scope.pagesize);
                            });

                        });
                    }

                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("promo-" + i);
                        if (v.checked == true) {
                            $scope.busy = true;
                            if ($scope.filteredTodos[i].datastatus == true) {
                                var deleteintcode = ({
                                    'sims_behaviour_enroll_number': $scope.filteredTodos[i].sims_enroll_number,
                                    'sims_behaviour_code': $scope.filteredTodos[i].sims_behaviour_code,
                                    'sims_behaviour_note': $scope.filteredTodos[i].sims_behaviour_note,
                                    'sims_behaviour_date': $scope.temp.sims_behaviour_date,
                                    opr: 'I',
                                });
                                $scope.insert = true;
                                Savedata.push(deleteintcode);
                                $scope.flag = true;
                            }
                        }

                    }

                    if ($scope.flag) {
                        $http.post(ENV.apiUrl + "api/StudentBehaviour/CUDInsertStudentBehaviour", Savedata).then(function (msg) {                            
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 380, height: 200 });
                                $scope.busy = false;
                                deleteintcode = [];
                                Savedata = [];
                                $scope.Submit();
                                $scope.Cancel()
                            }
                            else {
                                //swal({ title: "Alert", text: "Student Already Present. " + $scope.msg1, width: 380, height: 200 });
                                $scope.busy = false;

                                swal({ text: "Record Inserted Successfully. ", imageUrl: "assets/img/check.png", width: 380, height: 200 });
                                $scope.Cancel();

                            }



                            $http.get(ENV.apiUrl + "api/StudentBehaviour/getAllStudentBehaviour?cur=" + $scope.temp.sims_cur_code + "&a_year=" + $scope.temp.sims_academic_year + "&grades=" + $scope.temp.sims_grade_code + "&section=" + $scope.temp.sims_section_code).then(function (AllTermDetails) { //+ "&advanced_year=" + $scope.edt.sims_academic_years                                
                                $scope.getAllTerm_details = AllTermDetails.data;
                                $scope.totalItems = $scope.getAllTerm_details.length;
                                $scope.todos = $scope.getAllTerm_details;
                                $scope.makeTodos();
                                $scope.size($scope.pagesize);
                            });


                        });
                    }
                    else {
                        swal({ title: 'Alert', text: 'Please select student', width: 380, height: 200 });
                    }

                }//copyALL

            }

            $scope.CheckAllChecked = function () {
                debugger
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("promo-"+i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("promo-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }


            $scope.checkonebyonedelete = function (str) {
                debugger
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                        //  str.status = true;
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                        //str.status = false;
                    }
                });
                str.status = str.datastatus;
            }

            $scope.Cancel = function () {
                $scope.edt = {};
                $scope.temp = {};
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.temp['sims_academic_year'] = $scope.Acc_year[0].sims_academic_year;
                $scope.temp.sims_behaviour_date = $filter('date')(new Date(), 'dd-MM-yyyy');
                $scope.showSaveBtn = false;
                $scope.showoOnCopy = false;
            }

            $scope.displayOnCopy = function () {
                if ($scope.temp.copyAll == true) {
                    $scope.showoOnCopy = true;
                }
                else {
                    $scope.showoOnCopy = false;
                }                
            }
            
         }])
})();
