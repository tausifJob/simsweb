﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Student');

    simsController.controller('StudentClearanceStatusCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.deleteList = [];
            $scope.studentData = [];

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.getStudentClearanceStatus = function () {
                $http.get(ENV.apiUrl + "api/StudentClearanceStatus/getStudentClearanceStatus").then(function (response) {
                    $scope.studentData = response.data;                    
                    $scope.totalItems = $scope.studentData.length;
                    $scope.todos = $scope.studentData;
                    $scope.makeTodos();                  
                });
            }
            $scope.getStudentClearanceStatus();

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseFloat($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseFloat(begin) + parseFloat($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                $scope.selectedAll = false;
                $scope.isSelectAll();

            };

            $scope.edt = {};
            $scope.grid = true;
            $scope.display = false;
        
            $scope.isSelectAll = function () {
                $scope.deleteList = [];
                if ($scope.selectedAll) {
                    $scope.selectedAll = true;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.deleteList.push($scope.filteredTodos[i].sims_mom_type_code);
                    }
                    $scope.row1 = 'row_selected';
                }
                else {
                    $scope.selectedAll = false;
                    $scope.row1 = '';
                }

                angular.forEach($scope.filteredTodos, function (item) {
                    item.ischecked = $scope.selectedAll;
                });

            }

            $scope.checkIfAllSelected = function () {
                var id = event.target.id;
                var value = event.target.value;
                if (document.getElementById(id).checked) {
                    $scope.deleteList.push(value);
                    if ($scope.deleteList.length == $scope.filteredTodos.length) {
                        $scope.selectedAll = true;
                    }
                    $("#" + id).closest('tr').addClass("row_selected");
                } else {
                    $scope.selectedAll = false;
                    $scope.row1 = '';
                    var index = $scope.deleteList.indexOf(value);
                    $scope.deleteList.splice(index, 1);
                    $("#" + id).closest('tr').removeClass("row_selected");
                }
            };

            
            $scope.size = function (str) {

                if (str == 'All') {

                    $scope.totalItems = $scope.studentData.length;
                    $scope.todos = $scope.studentData;
                    $scope.filteredTodos = $scope.studentData;
                    $scope.numPerPage = $scope.studentData.length;
                    $scope.maxSize = $scope.studentData.length
                    $scope.makeTodos();
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.studentData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.studentData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in 2 fields */
                return (item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

         }])
})();