﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('DefineQuota',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.tr_screen_display = false;
            $scope.grid_display = true;
            $scope.Academic_year = {};
            $scope.edt = { 'cur_Code': null, 'academic_year': null, 'status': null };
            $scope.loadD = false;
            $http.get(ENV.apiUrl + "api/common/Admission/getAllQuotaDetails").then(function (getAllQuotaDesc) {
                $scope.AllQuota = getAllQuotaDesc.data;
                $scope.totalItems = $scope.AllQuota.length;
                $scope.todos = $scope.AllQuota;
                $scope.makeTodos();
            })

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                if ($scope.curriculum.length == 1) {
                    $scope.loadD = true;
                    $scope.edt.cur_Code = $scope.curriculum[0].sims_cur_code;
                }
            });

            $http.get(ENV.apiUrl + "api/common/Designation/getAllDesignation").then(function (Desg_data) {
                console.log(Desg_data.data);
                $scope.desg_data = Desg_data.data;
            })

            $scope.getAY = function () {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.edt.cur_Code).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    //if ($scope.Academic_year.length == 1)
                    //    $scope.edt['academic_year'] = $scope.Academic_year[0].sims_academic_year;
                })
            }

            $scope.getGrd = function () {
                $http.get(ENV.apiUrl + "api/common/SectionTerm/getAllGrades?cur_code=" + $scope.edt.cur_Code + "&ac_year=" + $scope.edt.academic_year).then(function (Grade_data) {
                    $scope.grade_data = Grade_data.data;
                })
            }

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
            }

            $scope.Quota_edit = function (str) {
                $scope.tr_screen_display = true;
                $scope.grid_display = false;
                $scope.btn_save = false;
                $scope.btn_update = true;
                $scope.btn_delete = false;
                console.log($scope.edt);
                $scope.loadD == true;
                $scope.edt.cur_Code = str.cur_Code;
                $scope.edt.cur_Name = str.cur_Name;
                $scope.edt.sr_no = str.sr_no;
                $scope.edt.academic_year = str.academic_year;
                $scope.edt.academic_year_Desc = str.academic_year_Desc;
                $scope.edt.grade_code = str.grade_code;
                $scope.edt.grade_Name = str.grade_Name;
                $scope.edt.desg_code = str.desg_code;
                $scope.edt.desg_desc = str.desg_desc;
                $scope.edt.quotaDesc = str.quotaDesc;
                $scope.edt.strength = str.strength;
                $scope.edt.status = str.status == "A";
                $scope.edt.opr = 'I';
            }

            $scope.Quota_Update = function () {
                var data = $scope.edt;
                data.opr = 'U';
                $http.post(ENV.apiUrl + "api/common/Admission/InsertUpdateQuotaDetails", data).then(function (msg) {
                    $scope.msg1 = msg.data;
                    $scope.tr_screen_display = false;
                    $scope.grid_display = true;
                    if ($scope.msg1 == true) {
                        swal({
                            text: 'Information Updated Successfully',
                            imageUrl: "assets/img/check.png",
                            width: 380
                            , showCloseButton: true
                        });
                        $http.get(ENV.apiUrl + "api/common/Admission/getAllQuotaDetails").then(function (Disdata) {
                            $scope.AllQuota = Disdata.data;
                            $scope.totalItems = $scope.AllQuota.length;
                            $scope.todos = $scope.AllQuota;
                            $scope.makeTodos();
                        })
                    }
                    else if ($scope.msg1 == false) {
                        swal({
                            text: 'Information Not Updated. ',
                            imageUrl: "assets/img/close.png",
                            width: 380, showCloseButton: true
                        });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                })
            }

            $scope.DefQuota_New = function () {
                $scope.tr_screen_display = true;
                $scope.grid_display = false;
                $scope.btn_save = true;
                $scope.btn_update = false;
                $scope.btn_delete = false;
                $scope.edt['status'] = true;
                $scope.edt['desg_desc'] = '';
                $scope.edt['grade_code'] = undefined;
                $scope.edt['quotaDesc'] = '';
                $scope.edt['desg_code'] = undefined;
                $scope.edt['strength'] = 0;
            }

            $scope.DefQuota_Delete = function () {
                var t = false;
                var t1 = false;
                var sr_nos = '';
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    t = $scope.filteredTodos[i].isChecked;
                    if (t == true) {
                        t1 = true;
                        sr_nos = sr_nos + $scope.filteredTodos[i].sr_no + ',';
                    }
                }

                var homeroomcode = ({
                    'status': sr_nos,
                    'opr': 'D'
                });

                if (t1 == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            console.log(homeroomcode);
                            $http.post(ENV.apiUrl + "api/common/Admission/DelQuotaDetails", homeroomcode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                $scope.tr_screen_display = false;
                                $scope.grid_display = true;

                                if ($scope.msg1 > 0) {

                                    swal({ text: $scope.msg1 + ' Records Deleted Successfully', imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                                    $http.get(ENV.apiUrl + "api/common/Admission/getAllQuotaDetails").then(function (getAllHomeroom) {
                                        $scope.isAllChecked = false;
                                        $scope.AllQuota = getAllHomeroom.data;
                                        $scope.row1 = "";
                                        $scope.currentPage = 1;
                                        $scope.totalItems = $scope.AllQuota.length;
                                        $scope.todos = $scope.AllQuota;
                                        $scope.makeTodos();
                                    });
                                }
                                else if ($scope.msg1 ==false) 
                                    //else
                                {

                                    swal({ text: 'Information Not Deleted. ', imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                                }
                               else{
                                swal("Error-"+ $scope.msg1)
                                }
                                $scope.isAllChecked = false;
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    $scope.filteredTodos[i].isChecked = false;
                                }
                            })
                        }
                        else {
                            $scope.isAllChecked = false;
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                $scope.filteredTodos[i].isChecked = false;
                            }
                        }
                    })
                }
                else {
                    swal({ text: 'Select At Least One Record To Delete', width: 380 });
                }
            }

            $scope.cancel = function () {
                $scope.grid_display = true;
                $scope.tr_screen_display = false;
                //$scope.edt = { 'cur_Code': null, 'academic_year': null, 'status': null };
            }

            $scope.Quota_MultipleSelect = function () {
                for (var v = 0; v < $scope.filteredTodos.length; v++) {
                    $scope.filteredTodos[v].isChecked = $scope.isAllChecked;
                }
            }

            $scope.Quota_SingleSelect = function (str) {
                if (str.isChecked == false)
                    $scope.isAllChecked = false;
                else {
                    var t = true;
                    for (var v = 0; v < $scope.filteredTodos.length; v++) {
                        t = t && $scope.filteredTodos[v].isChecked;
                    }
                    $scope.isAllChecked = t;
                }
            }

            $scope.SaveData = function (isvalid) {
                if (isvalid) {
                    var data = $scope.edt;
                    data.opr = 'I';
                    console.log(data);
                    $http.post(ENV.apiUrl + "api/common/Admission/InsertUpdateQuotaDetails", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.tr_screen_display = false;
                        $scope.grid_display = true;
                        if ($scope.msg1 == true) {
                            swal({
                                text: 'Information Inserted Successfully',
                                imageUrl: "assets/img/check.png",
                                width: 380, showCloseButton: true
                            });
                            $http.get(ENV.apiUrl + "api/common/Admission/getAllQuotaDetails").then(function (getAllHomeroom) {
                                $scope.AllQuota = getAllHomeroom.data;
                                $scope.totalItems = $scope.AllQuota.length;
                                $scope.todos = $scope.AllQuota;
                                $scope.makeTodos();
                            });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: 'Information Not Inserted. ', imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    })
                    $scope.edt = { 'cur_Code': null, 'academic_year': null, 'status': null };
                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.AllQuota, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.AllQuota;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.academic_year_Desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.desg_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.grade_Name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.quotaDesc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.reload = function () {
                $scope.isAllChecked = false;
                $http.get(ENV.apiUrl + "api/common/Admission/getAllQuotaDetails").then(function (getAllHomeroom) {
                    $scope.AllQuota = getAllHomeroom.data;
                    $scope.totalItems = $scope.AllQuota.length;
                    $scope.todos = $scope.AllQuota;
                    $scope.makeTodos();
                });
            }

            $scope.$watch('edt.cur_Code', function (n, o) {
                if ($scope.loadD == true) {
                    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.edt.cur_Code).then(function (Academicyear) {
                        $scope.Academic_year = Academicyear.data;
                        if ($scope.Academic_year.length == 1) {
                            $scope.loadD = true;
                            $scope.edt['academic_year'] = $scope.Academic_year[0].sims_academic_year;
                        }
                    })
                }
            })

            $scope.$watch('edt.academic_year', function (n, o) {
                if ($scope.loadD == true) {
                    $http.get(ENV.apiUrl + "api/common/SectionTerm/getAllGrades?cur_code=" + $scope.edt.cur_Code + "&ac_year=" + $scope.edt.academic_year).then(function (Grade_data) {
                        $scope.grade_data = Grade_data.data;
                        $scope.loadD == false;
                    })
                }
            })
         }])
})();