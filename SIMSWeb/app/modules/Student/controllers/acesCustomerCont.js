﻿(function () {
    'use strict';
    var main, temp = [];
    var data1 = [];
    var data2 = [];
    var block1 = [];
    var status = "";
    var msg_flag1 = false;
    var prdocdata = [];
    var acconcode = "";
    var cmbvalue = "";
    var comp_code = "1";
    var chk;
    var data = [];
    var simsController = angular.module('sims.module.Student');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('acesCustomerCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$interval', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $interval) {
            $http.get(ENV.apiUrl + "api/AgendaCreationController/GetAgendaDoc").then(function (GetAgendaDoc) {
                $scope.file_upload_details = GetAgendaDoc.data;
            });
            $http({
                method: 'post',
                url: ENV.apiUrl + 'api/student/fileUpload/SDFUselectAll'
            }).then(function (res) {
                $scope.SDFUselectAll = res.data;
            }, function (err) { console.log(err); })
            $scope.getFormatedDate = function (dt, whichFormat, whatToAdd, addSubValue) {
                var rFs = "";
                if (whatToAdd !== undefined) {
                    switch (whatToAdd) {
                        case 'yy': dt = new Date(dt.setFullYear(dt.getFullYear() + addSubValue)); break;
                        case 'mm': dt = new Date(dt.setMonth(dt.getMonth() + addSubValue)); break;
                        case 'dd': dt = new Date(dt.setDate(dt.getDate() + addSubValue)); break;
                        case 'hh': dt = new Date(dt.setHours(dt.getHours() + addSubValue)); break;
                        case 'ms': dt = new Date(dt.setMinutes(dt.getMinutes() + addSubValue)); break;
                        case 'ss': dt = new Date(dt.setSeconds(dt.getSeconds() + addSubValue)); break;
                        case 'ms': dt = new Date(dt.setMilliseconds(dt.getMilliseconds() + addSubValue)); break;
                    };
                }

                var ad = dt.toLocaleString().split(' ');
                var ary = whichFormat.split('.');
                var len = ary.length;
                var mdy = ad[0].replace(',', '').split('/');
                var mhs = ad[1].split(':');
                var ampm = ad[2];
                for (var i = 0; i < len; i++) {
                    var sel = ary[i];
                    switch (sel) {
                        case 'yy': rFs += mdy[2].substring(2); break;
                        case 'yyyy': rFs += mdy[2]; break;
                        case 'mm': rFs += mdy[0]; break;
                        case 'MM': rFs += dt.toString().substring(4, 7); break;
                        case 'dd': rFs += mdy[1]; break;
                        case 'DD': rFs += dt.toString().substring(0, 3); break;
                        case 'hh': rFs += mhs[0]; break;
                        case 'mn': rFs += mhs[1]; break;
                        case 'ss': rFs += mhs[2]; break;
                        case 'ap': rFs += ampm; break;
                        case '/': rFs += '/'; break;
                        case ':': rFs += ':'; break;
                        case '-': rFs += '-'; break;
                        case '_': rFs += '_'; break;
                        case ' ': rFs += ' '; break;
                    }
                }
                return rFs;
            }
            $scope.onClickPrintExcel = function () {
                var columns = [{ title: "Sr.", dataKey: "RowNumber" },
                        { title: "Enroll no.", dataKey: "sims_student_enroll_number" },
                        { title: "Student", dataKey: "student_name" },
                        { title: "Father", dataKey: "father_name" },
                        { title: "Mother", dataKey: "mother_name" },
                        { title: "Parent mob.", dataKey: "sims_parent_father_mobile" },
                        { title: "Parent email", dataKey: "sims_parent_guardian_email" },
                        { title: "Academic year", dataKey: "sims_academic_year" }];
                var currentDate = new Date();
                $scope.currentDate = $scope.getFormatedDate(currentDate, 'dd./.mm./.yyyy. .hh.:.mn. .ap');

                $scope.printExcel("Student Details", $scope.currentDate, [], columns, $scope.trans_data);
            }
            $scope.base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            },
            $scope.format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                })
            }
            $scope.printExcel = function (pageTitle, printDate, filters, columns, data, totalOfRows) {
                var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>';
                var columnCount = columns.length;
                var tempTable = '<tr><th style="font-size:24px; text-align:center; padding:4px;" colspan="' + columnCount + '">' + pageTitle + '</th></tr><tr><td colspan="' + columnCount + '"></td></tr>';

                filters.forEach(function (cr) { tempTable += '<tr><td style="font-size:16px; text-align:center; padding:4px;" colspan="' + columnCount + '">' + cr + '</td></tr>' })

                tempTable += '<tr><th style="text-align:right;" colspan="' + columnCount + '">Date: ' + printDate + '</th></tr>';
                tempTable += '<tr><th colspan="' + columnCount + '"></th></tr>';
                tempTable += '<table border="1"><thead><tr>'
                for (var i = 0; i < columnCount; i++) {
                    tempTable += '<th style="font-size:14px; text-align:center; padding:8px;">' + columns[i].title + '</th>'
                }
                tempTable += '</tr></thead><tbody>';

                data.forEach(function (d) {
                    tempTable += '<tr>';
                    for (var i = 0; i < columnCount; i++) {
                        tempTable += '<td style="font-size:14px; text-align:center; padding:8px;">' + d[columns[i].dataKey] + '</td>'
                    }
                    tempTable += '</tr>';
                })

                if (totalOfRows !== false && totalOfRows !== undefined) {
                    tempTable += totalOfRows;
                }

                tempTable += '</tbody></table>';
                var ctx = { worksheet: pageTitle || 'Worksheet', table: tempTable };
                var uri = 'data:application/vnd.ms-excel;base64,';

                var a = document.createElement('a');
                a.href = uri + $scope.base64($scope.format(template, ctx));
                var ToDate = new Date();
                a.download = pageTitle.replace(/ /g, '_') + '_' + $scope.getFormatedDate(ToDate, 'dd./.mm./.yyyy. .hh.:.mn.:.ss. .ap').replace(/ /g, '_') + '.xls';
                a.click();
            }
            $scope.ExcelToJSON = function (file) {
                this.parseExcel = function (file) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var data = e.target.result;
                        var workbook = XLSX.read(data, {
                            type: 'binary'
                        });
                        workbook.SheetNames.forEach(function (sheetName) {
                            // Here is your object
                            var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                            var json_object = JSON.stringify(XL_row_object);
                            console.log(JSON.parse(json_object));
                            jQuery('#xlx_json').val(json_object);
                        })
                    };
                    reader.onerror = function (ex) {
                        console.log(ex);
                    };
                    reader.readAsBinaryString(file);
                };
            };
            $scope.errorListFun = function (d) {
                $scope.errorList[$scope.errorList.length] = d;
                debugger
            }
            $scope.uploadfile = function () {
                $scope.errorList = [];
                $scope.tble_show = false;
                var filesData = $('#inputGroupFile01')[0].files;
                $scope.isLoading = true;
                var reader = new FileReader();
                reader.onload = function (e) {
                    var data = e.target.result;
                    var workbook = XLSX.read(data, {
                        type: 'binary'
                    });
                    workbook.SheetNames.forEach(function (sheetName, ind) {
                        if (ind === 0) {
                            var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                            var errorMsg = '';
                            var isErrorMsg = false;
                            var errorCheckCount = 0;
                            $scope.records = [];
                            XL_row_object.some(function (cr, ind) {
                                $scope.records[$scope.records.length] = {
                                    country_code: cr['Country code'] || undefined,
                                    state_code: cr['State code'] || undefined,
                                    region_code: cr['Region code'] || undefined,
                                    district_code: cr['District code'] || undefined,
                                    city_code: cr['City code'] || undefined,
                                    taluka_code: cr['Taluka code'] || undefined,
                                    area_code: cr['Area code'] || undefined,
                                    school_code: cr['School code'] || undefined,
                                    school_type: cr['School type'] || undefined,
                                    school_name: cr['School name'] || undefined,
                                    school_short_name: cr['School short name'] || undefined,
                                    school_other_name: cr['School other name'] || undefined,
                                    school_logo: cr['School logo'] || undefined,
                                    school_address: cr['School address'] || undefined,
                                    contact_person: cr['Contact person'] || undefined,
                                    fax_no: cr['Fax no'] || undefined,
                                    tel_no: cr['Tel no'] || undefined,
                                    email: cr['Email'] || undefined,
                                    website_url: cr['Website URL'] || undefined,
                                    school_curriculum: cr['School curriculum'] || undefined,
                                    agreement_date: cr['Agreement date(YYYY-MM-DD)'].split('/').join('-') || undefined,
                                    subscription_date: cr['Subscription date(YYYY-MM-DD)'].split('/').join('-') || undefined,
                                    subscription_expiry_date: cr['Subscription expiry date(YYYY-MM-DD)'].split('/').join('-') || undefined,
                                    grace_30: cr['Grace 30'] || undefined,
                                    grace_60: cr['Grace 60'] || undefined,
                                    grace_90: cr['Grace 90'] || undefined,
                                    status: cr['Status'] || undefined,
                                    time_zone: cr['Time zone(+/-MM:HH)'] || undefined,
                                    latitude: cr['latitude'] || undefined,
                                    longitude: cr['longitude'] || undefined
                                }
                                //var tempGradeCode = $scope.records[ind].sims_admission_grade_code;
                                //var tempInd = ind + 1;
                                //$scope.records[ind].sims_admission_date === undefined && (isErrorMsg = true, $scope.errorListFun('Row ' + tempInd + '  : Enter admission date'))
                                //!isErrorMsg && tempGradeCode === undefined && (isErrorMsg = true, $scope.errorListFun('Row ' + tempInd + '  : Enter Grade code'))
                                //tempGradeCode = $scope.grade.find(function (cr) { return cr.sims_grade_code === tempGradeCode })
                                //tempGradeCode === undefined && (isErrorMsg = true, $scope.errorListFun('Row ' + tempInd + '  : Enter Correct Grade code'))
                                //$scope.records[ind].sims_admission_passport_first_name_en === undefined && (isErrorMsg = true, $scope.errorListFun('Row ' + tempInd + '  : Enter Student First Name'))
                                //$scope.records[ind].sims_admission_passport_middle_name_en === undefined && (isErrorMsg = true, $scope.errorListFun('Row ' + tempInd + '  : Enter Student Middle Name'))
                                //$scope.records[ind].sims_admission_passport_last_name_en === undefined && (isErrorMsg = true, $scope.errorListFun('Row ' + tempInd + '  : Enter Student Last Name'))
                                //$scope.records[ind].sims_admission_gender === undefined && (isErrorMsg = true, $scope.errorListFun('Row ' + tempInd + '  : Enter Gender'))
                                //$scope.records[ind].sims_admission_dob === undefined && (isErrorMsg = true, $scope.errorListFun('Row ' + tempInd + '  : Enter DOB(YYYY-MM-DD)'))
                                //$scope.records[ind].sims_admission_father_first_name === undefined && (isErrorMsg = true, $scope.errorListFun('Row ' + tempInd + '  : Enter Father First Name'))
                                //$scope.records[ind].sims_admission_father_last_name === undefined && (isErrorMsg = true, $scope.errorListFun('Row ' + tempInd + '  : Enter Father Last Name'))
                                //$scope.records[ind].sims_admission_father_mobile === undefined && (isErrorMsg = true, $scope.errorListFun('Row ' + tempInd + '  : Enter Father Mobile'))
                                //var tempR = $scope.SDFUselectAll.find(function (cr) {
                                //    return cr.sims_student_passport_first_name_en === $scope.records[ind].sims_admission_passport_first_name_en &&
                                //    cr.sims_student_passport_last_name_en === $scope.records[ind].sims_admission_passport_last_name_en &&
                                //    cr.sims_parent_father_first_name === $scope.records[ind].sims_admission_father_first_name &&
                                //    cr.sims_parent_father_last_name === $scope.records[ind].sims_admission_father_last_name &&
                                //    cr.sims_parent_father_mobile === $scope.records[ind].sims_admission_father_mobile
                                //})

                                //var tempR1 = $scope.records.filter(function (cr) {
                                //    return cr.sims_admission_passport_first_name_en === $scope.records[ind].sims_admission_passport_first_name_en &&
                                //    cr.sims_admission_passport_last_name_en === $scope.records[ind].sims_admission_passport_last_name_en &&
                                //    cr.sims_admission_father_first_name === $scope.records[ind].sims_admission_father_first_name &&
                                //    cr.sims_admission_father_last_name === $scope.records[ind].sims_admission_father_last_name &&
                                //    cr.sims_admission_father_mobile === $scope.records[ind].sims_admission_father_mobile
                                //})
                                //if (tempR !== undefined || tempR1.length > 1) {
                                //    isErrorMsg = true;
                                //    $scope.errorListFun('Row ' + tempInd + '  : Duplicate record of student : ' + cr['Student First Name'] + ' ' + cr['Student Last Name']);
                                //}
                                errorCheckCount++;
                            })
                            var tempInt = $interval(function () {
                                if (errorCheckCount === XL_row_object.length) {
                                    isErrorMsg ? ($scope.isErrorList = true, $scope.isSaveBtn = false) : ($scope.isErrorList = false, $scope.isSaveBtn = true)
                                    $scope.tble_show = true;
                                    $scope.isLoading = false;
                                    $scope.isClearBtn = true;
                                    $interval.cancel(tempInt);
                                }
                            }, 500)
                        }
                    })
                }
                reader.onerror = function (ex) {
                    console.log(ex);
                };
                if (filesData.length > 0) {
                    reader.readAsBinaryString(filesData[0]);
                }
            }
            $scope.btnClear = function () {
                $scope.isLoading = false;
                var input = $('#inputGroupFile01');
                input.replaceWith(input.val('').clone(true));
                $scope.isErrorList = false;
                $scope.records.length = 0;
                $scope.tble_show = false;
                $scope.isClearBtn = false;
            }
            $scope.btnSave = function () {
                $scope.isLoading = true;
                $http({
                    method: 'post',
                    url: ENV.apiUrl + 'api/student/fileUpload/AcesCustomer_insert',
                    data: $scope.records
                }).then(function (res) {
                    $scope.isLoading = false;
                    $scope.isProcessBtn = true;
                    $scope.isSaveBtn = false;
                    var input = $('#inputGroupFile01');
                    input.replaceWith(input.val('').clone(true));
                    $scope.isErrorList = false;
                    $scope.records.length = 0;
                    $scope.tble_show = false;
                    $scope.isClearBtn = false;
                    swal({ text: "Record inserted successfully", imageUrl: "assets/img/check.png" });
                    //alert('File uploaded successfully');
                },
                function (err) {
                    $scope.isLoading = false;
                    swal({ text: "Proccess failed", imageUrl: "assets/img/close.png" });
                })
            }
            $scope.btnProcess = function () {
                var obj = {
                    sims_cur_code: $scope.s_cur_code,
                    sims_academic_year: $scope.sims_academic_year,
                    sims_sr_no: $scope.s_cur_code,
                }
                $http({
                    method: 'post',
                    url: ENV.apiUrl + 'api/student/fileUpload/SDFU_process',
                    data: obj
                }).then(function (res) {
                    swal({ text: "Proccess done successfully", imageUrl: "assets/img/check.png" });
                    //console.log('Proccess successfully done.')
                }, function () {
                    swal({ text: "Proccess failed", imageUrl: "assets/img/close.png" });
                })
            }
            $scope.do_file = (function () {
                var rABS = typeof FileReader !== "undefined" && (FileReader.prototype || {}).readAsBinaryString;
                var domrabs = document.getElementsByName("userabs")[0];
                if (!rABS) domrabs.disabled = !(domrabs.checked = false);
                var use_worker = typeof Worker !== 'undefined';
                var domwork = document.getElementsByName("useworker")[0];
                if (!use_worker) domwork.disabled = !(domwork.checked = false);

                var xw = function xw(data, cb) {
                    var worker = new Worker(XW.worker);
                    worker.onmessage = function (e) {
                        switch (e.data.t) {
                            case 'ready': break;
                            case 'e': console.error(e.data.d); break;
                            case XW.msg: cb(JSON.parse(e.data.d)); break;
                        }
                    };
                    worker.postMessage({ d: data, b: rABS ? 'binary' : 'array' });
                };
                return function do_file(files) {
                    rABS = domrabs.checked;
                    use_worker = domwork.checked;
                    var f = files[0];
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        if (typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
                        var data = e.target.result;
                        if (!rABS) data = new Uint8Array(data);
                        if (use_worker) xw(data, process_wb);
                        else process_wb(X.read(data, { type: rABS ? 'binary' : 'array' }));
                    };
                    if (rABS) reader.readAsBinaryString(f);
                    else reader.readAsArrayBuffer(f);
                };
            })();
            $scope.getViewDetails = function () {
                $('#loader').modal({ backdrop: 'static', keyboard: false });

                var data = {
                    cur_code: $scope.s_cur_code || null,
                    academic_year: $scope.sims_academic_year || null,
                    grade_code: $scope.sims_grade_code || null,
                    section_code: $scope.sims_section_code || null,
                    search_flag: $scope.sims_search_code || null,
                    search_text: $scope.search_txt || null
                }

                $scope.s_cur_code = ($scope.s_cur_code || '');
                $scope.sims_academic_year = ($scope.sims_academic_year || '');
                $scope.sims_grade_code = ($scope.sims_grade_code || '');
                $scope.sims_section_code = ($scope.sims_section_code || '');
                $scope.sims_search_code = ($scope.sims_search_code || '');
                $scope.search_flag = ($scope.search_flag || '');
                $scope.search_txt = ($scope.search_txt || '');

                $http.get(ENV.apiUrl + "api/studentdatabase/getSimsStudentExportDetails?cur_code=" + $scope.s_cur_code + "&academic_year=" + $scope.sims_academic_year + "&grade_code=" + $scope.sims_grade_code + "&section_code=" + $scope.sims_section_code + "&search_flag=" + $scope.sims_search_code + "&search_text=" + $scope.search_txt).then(function (res) {
                    debugger
                    $scope.trans_data = res.data;
                    $('#loader').modal('hide');
                    $scope.tble_show = true;
                });
            }
        }])
})();
