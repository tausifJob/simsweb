﻿(function () {
    'use strict';
    var main, temp = [];
    var data1 = [];
    var data2 = [];
    var block1 = [];
    var status = "";
    var msg_flag1 = false;
    var prdocdata = [];
    var acconcode = "";
    var cmbvalue = "";
    var comp_code = "1";
    var chk;
    var data = [];
    var simsController = angular.module('sims.module.Student');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('studentDetailsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$interval', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $interval) {
            $scope.getsection = function (str, str1, str2) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + str + "&grade_code=" + str1 + "&academic_year=" + str2).then(function (Allsection) {
                    $scope.All_Section_names = Allsection.data;
                    //   $scope.sims_section_name = $scope.section1[0].sims_section_code;
                    //  $scope.Show_Data();
                })
            };
            $scope.getGrade = function (str1, str2) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str1 + "&academic_year=" + str2).then(function (getAllGrades) {
                    $scope.grade = getAllGrades.data;
                    $scope.getsection($scope.temp.s_cur_code, str1, str2)
                })
            }
            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.temp.s_cur_code).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = {
                        s_cur_code: $scope.temp.s_cur_code,
                        sims_academic_year: $scope.Acc_year[0].sims_academic_year
                    }
                    $scope.getGrade($scope.temp.s_cur_code, $scope.temp.sims_academic_year);
                });
            }
            $http.get(ENV.apiUrl + "api/Fee/SFS/getSeachOptions").then(function (searchoptions) {
                $scope.searchoptions = searchoptions.data;
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                    $scope.curriculum = res.data;
                    $scope.temp = { s_cur_code: $scope.curriculum[0].sims_cur_code };
                    $scope.getAccYear();
                })
            });
            $http.get(ENV.apiUrl + "api/AgendaCreationController/GetAgendaDoc").then(function (GetAgendaDoc) {
                $scope.file_upload_details = GetAgendaDoc.data;
            });
            $http({
                method: 'post',
                url: ENV.apiUrl + 'api/student/fileUpload/SDFUselectAll'
            }).then(function (res) {
                $scope.SDFUselectAll = res.data;
            },
            function (err) {
                console.log(err);
            })
            $scope.getFormatedDate = function (dt, whichFormat, whatToAdd, addSubValue) {
                var rFs = "";
                if (whatToAdd !== undefined) {
                    switch (whatToAdd) {
                        case 'yy': dt = new Date(dt.setFullYear(dt.getFullYear() + addSubValue)); break;
                        case 'mm': dt = new Date(dt.setMonth(dt.getMonth() + addSubValue)); break;
                        case 'dd': dt = new Date(dt.setDate(dt.getDate() + addSubValue)); break;
                        case 'hh': dt = new Date(dt.setHours(dt.getHours() + addSubValue)); break;
                        case 'ms': dt = new Date(dt.setMinutes(dt.getMinutes() + addSubValue)); break;
                        case 'ss': dt = new Date(dt.setSeconds(dt.getSeconds() + addSubValue)); break;
                        case 'ms': dt = new Date(dt.setMilliseconds(dt.getMilliseconds() + addSubValue)); break;
                    };
                }

                var ad = dt.toLocaleString().split(' ');
                var ary = whichFormat.split('.');
                var len = ary.length;
                var mdy = ad[0].replace(',', '').split('/');
                var mhs = ad[1].split(':');
                var ampm = ad[2];
                for (var i = 0; i < len; i++) {
                    var sel = ary[i];
                    switch (sel) {
                        case 'yy': rFs += mdy[2].substring(2); break;
                        case 'yyyy': rFs += mdy[2]; break;
                        case 'mm': rFs += mdy[0]; break;
                        case 'MM': rFs += dt.toString().substring(4, 7); break;
                        case 'dd': rFs += mdy[1]; break;
                        case 'DD': rFs += dt.toString().substring(0, 3); break;
                        case 'hh': rFs += mhs[0]; break;
                        case 'mn': rFs += mhs[1]; break;
                        case 'ss': rFs += mhs[2]; break;
                        case 'ap': rFs += ampm; break;
                        case '/': rFs += '/'; break;
                        case ':': rFs += ':'; break;
                        case '-': rFs += '-'; break;
                        case '_': rFs += '_'; break;
                        case ' ': rFs += ' '; break;
                    }
                }
                return rFs;
            }
            $scope.onClickPrintExcel = function () {
                var columns = [{ title: "Sr.", dataKey: "RowNumber" },
                        { title: "Enroll no.", dataKey: "sims_student_enroll_number" },
                        { title: "Student", dataKey: "student_name" },
                        { title: "Father", dataKey: "father_name" },
                        { title: "Mother", dataKey: "mother_name" },
                        { title: "Parent mob.", dataKey: "sims_parent_father_mobile" },
                        { title: "Parent email", dataKey: "sims_parent_guardian_email" },
                        { title: "Academic year", dataKey: "sims_academic_year" }];
                var currentDate = new Date();
                $scope.currentDate = $scope.getFormatedDate(currentDate, 'dd./.mm./.yyyy. .hh.:.mn. .ap');

                $scope.printExcel("Student Details", $scope.currentDate, [], columns, $scope.trans_data);
            }
            $scope.base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            },
            $scope.format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                })
            }
            $scope.printExcel = function (pageTitle, printDate, filters, columns, data, totalOfRows) {
                var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>';
                var columnCount = columns.length;
                var tempTable = '<tr><th style="font-size:24px; text-align:center; padding:4px;" colspan="' + columnCount + '">' + pageTitle + '</th></tr><tr><td colspan="' + columnCount + '"></td></tr>';

                filters.forEach(function (cr) { tempTable += '<tr><td style="font-size:16px; text-align:center; padding:4px;" colspan="' + columnCount + '">' + cr + '</td></tr>' })

                tempTable += '<tr><th style="text-align:right;" colspan="' + columnCount + '">Date: ' + printDate + '</th></tr>';
                tempTable += '<tr><th colspan="' + columnCount + '"></th></tr>';
                tempTable += '<table border="1"><thead><tr>'
                for (var i = 0; i < columnCount; i++) {
                    tempTable += '<th style="font-size:14px; text-align:center; padding:8px;">' + columns[i].title + '</th>'
                }
                tempTable += '</tr></thead><tbody>';

                data.forEach(function (d) {
                    tempTable += '<tr>';
                    for (var i = 0; i < columnCount; i++) {
                        tempTable += '<td style="font-size:14px; text-align:center; padding:8px;">' + d[columns[i].dataKey] + '</td>'
                    }
                    tempTable += '</tr>';
                })

                if (totalOfRows !== false && totalOfRows !== undefined) {
                    tempTable += totalOfRows;
                }

                tempTable += '</tbody></table>';
                var ctx = { worksheet: pageTitle || 'Worksheet', table: tempTable };
                var uri = 'data:application/vnd.ms-excel;base64,';

                var a = document.createElement('a');
                a.href = uri + $scope.base64($scope.format(template, ctx));
                var ToDate = new Date();
                a.download = pageTitle.replace(/ /g, '_') + '_' + $scope.getFormatedDate(ToDate, 'dd./.mm./.yyyy. .hh.:.mn.:.ss. .ap').replace(/ /g, '_') + '.xls';
                a.click();
            }
            $scope.ExcelToJSON = function (file) {
                this.parseExcel = function (file) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var data = e.target.result;
                        var workbook = XLSX.read(data, {
                            type: 'binary'
                        });
                        workbook.SheetNames.forEach(function (sheetName) {
                            // Here is your object
                            var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                            var json_object = JSON.stringify(XL_row_object);
                            console.log(JSON.parse(json_object));
                            jQuery('#xlx_json').val(json_object);
                        })
                    };
                    reader.onerror = function (ex) {
                        console.log(ex);
                    };
                    reader.readAsBinaryString(file);
                };
            };
            $scope.errorListFun = function (d) {
                $scope.errorList[$scope.errorList.length] = d;
            }
            $scope.uploadfile = function () {
                if ($scope.s_cur_code === undefined) {
                    var input = $('#inputGroupFile01');
                    input.replaceWith(input.val('').clone(true));
                    alert('Select curriculam');
                    return false
                }
                if ($scope.sims_academic_year === undefined) {
                    var input = $('#inputGroupFile01');
                    input.replaceWith(input.val('').clone(true));
                    alert('Select  academic year');
                    return false
                }
                $scope.errorList = [];
                $scope.tble_show = false;
                var filesData = $('#inputGroupFile01')[0].files;
                $scope.isLoading = true;
                var reader = new FileReader();
                reader.onload = function (e) {
                    var data = e.target.result;
                    var workbook = XLSX.read(data, {
                        type: 'binary'
                    });
                    workbook.SheetNames.forEach(function (sheetName) {
                        var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                        var errorMsg = '';
                        var isErrorMsg = false;
                        var errorCheckCount = 0;
                        $scope.records = [];
                        XL_row_object.some(function (cr, ind) {
                            $scope.records[$scope.records.length] = {
                                sims_sr_no: '',
                                sims_admission_application_number: (ind + 1),
                                sims_admission_date: cr['Admission date(YYYY-MM-DD)'].split('/').join('-') || undefined,
                                sims_admission_cur_code: $scope.s_cur_code || undefined,
                                sims_admission_academic_year: $scope.sims_academic_year || undefined,
                                sims_admission_grade_code: cr['Grade code'] || undefined,
                                sims_admission_tentative_joining_date: cr['Joining date(YYYY-MM-DD)'].split('/').join('-') || undefined,
                                sims_admission_passport_first_name_en: cr['Student First Name'] || undefined,
                                sims_admission_passport_middle_name_en: cr['Student Middle Name'] || undefined,
                                sims_admission_passport_last_name_en: cr['Student Last Name'] || undefined,
                                sims_admission_gender: cr['Gender'] || undefined,
                                sims_admission_dob: cr['DOB(YYYY-MM-DD)'].split('/').join('-') || undefined,
                                sims_admission_father_first_name: cr['Father First Name'] || undefined,
                                sims_admission_father_middle_name: cr['Father Middle Name'] || undefined,
                                sims_admission_father_last_name: cr['Father Last Name'] || undefined,
                                sims_admission_father_mobile: cr['Father Mobile'] || undefined,
                                sims_admission_father_email: cr['Father Email'] || undefined,
                                sims_admission_mother_first_name: cr['Mother First Name'] || undefined,
                                sims_admission_mother_middle_name: cr['Mother Middle Name'] || undefined,
                                sims_admission_mother_last_name: cr['Mother Last Name'] || undefined,
                                sims_admission_mother_mobile: cr['Mother Mobile'] || undefined,
                                sims_admission_mother_email: cr['Mother Email'] || undefined
                            }
                            var tempGradeCode = $scope.records[ind].sims_admission_grade_code;
                            var tempInd = ind + 1;
                            $scope.records[ind].sims_admission_date === undefined && (isErrorMsg = true, $scope.errorListFun('Row no.- ' + tempInd + ': Enter admission date'))
                            !isErrorMsg && tempGradeCode === undefined && (isErrorMsg = true, $scope.errorListFun('Row no.- ' + tempInd + ': Enter Grade code'))
                            tempGradeCode = $scope.grade.find(function (cr) { return cr.sims_grade_code === tempGradeCode })
                            tempGradeCode === undefined && (isErrorMsg = true, $scope.errorListFun('Row no.- ' + tempInd + ': Enter Correct Grade code'))
                            //tempGradeCode !== undefined && ($scope.records[$scope.records.length].sims_section_code = tempGradeCode.sims_section_code)
                            $scope.records[ind].sims_admission_passport_first_name_en === undefined && (isErrorMsg = true, $scope.errorListFun('Row no.- ' + tempInd + ': Enter Student First Name'))
                            $scope.records[ind].sims_admission_passport_middle_name_en === undefined && (isErrorMsg = true, $scope.errorListFun('Row no.- ' + tempInd + ': Enter Student Middle Name'))
                            $scope.records[ind].sims_admission_passport_last_name_en === undefined && (isErrorMsg = true, $scope.errorListFun('Row no.- ' + tempInd + ': Enter Student Last Name'))
                            $scope.records[ind].sims_admission_gender === undefined && (isErrorMsg = true, $scope.errorListFun('Row no.- ' + tempInd + ': Enter Gender'))
                            $scope.records[ind].sims_admission_dob === undefined && (isErrorMsg = true, $scope.errorListFun('Row no.- ' + tempInd + ': Enter DOB(YYYY-MM-DD)'))
                            $scope.records[ind].sims_admission_father_first_name === undefined && (isErrorMsg = true, $scope.errorListFun('Row no.- ' + tempInd + ': Enter Father First Name'))
                            $scope.records[ind].sims_admission_father_last_name === undefined && (isErrorMsg = true, $scope.errorListFun('Row no.- ' + tempInd + ': Enter Father Last Name'))
                            $scope.records[ind].sims_admission_father_mobile === undefined && (isErrorMsg = true, $scope.errorListFun('Row no.- ' + tempInd + ': Enter Father Mobile'))
                            var tempR = $scope.SDFUselectAll.find(function (cr) {
                                return cr.sims_student_passport_first_name_en === $scope.records[ind].sims_admission_passport_first_name_en &&
                                cr.sims_student_passport_last_name_en === $scope.records[ind].sims_admission_passport_last_name_en &&
                                cr.sims_parent_father_first_name === $scope.records[ind].sims_admission_father_first_name &&
                                cr.sims_parent_father_last_name === $scope.records[ind].sims_admission_father_last_name &&
                                cr.sims_parent_father_mobile === $scope.records[ind].sims_admission_father_mobile
                            })
                            if (tempR !== undefined) {
                                //var input = $('#inputGroupFile01');
                                //input.replaceWith(input.val('').clone(true));
                                //$scope.isLoading = false;
                                //alert('Student details of ' + cr.sims_student_passport_first_name_en + ' ' + cr.sims_student_passport_last_name_en + ' at row no. ' + (ind + 1));
                                $scope.errorList[$scope.errorList.length] = $scope.errorListFun('Row no.- ' + tempInd + ': Duplicate record of student' + cr.sims_student_passport_first_name_en + ' ' + cr.sims_student_passport_last_name_en);
                                //return true;
                            }
                            errorCheckCount++;
                        })
                        var tempInt = $interval(function () {
                            if (errorCheckCount === XL_row_object.length) {
                                isErrorMsg ? ($scope.isErrorList = true, $scope.isSaveBtn = false) : ($scope.isErrorList = false, $scope.isSaveBtn = true)
                                $scope.tble_show = true;
                                $scope.isLoading = false;
                                $interval.cancel(tempInt);
                            }
                        }, 50)


                        //var tempInt = setInterval(function () {
                        //    if (filesData.length && !isErrorMsg && errorCheckCount === XL_row_object.length) {
                        //        clearInterval(tempInt);
                        //        var files = new FormData();
                        //        var v = new Date();
                        //        var file_name = 'SDFU_' + v.getHours() + '_' + v.getMonth() + '_' + v.getSeconds() + '_' + v.getMilliseconds() + '_' + Math.floor((Math.random() * 1000) + 1);
                        //        $scope.file_upload_details.ftppassword = $scope.file_upload_details.ftppassword.replace('&', '%26');
                        //        for (var i = 0; i < filesData.length; i++) {
                        //            var fileNamedata = filesData[i].name;
                        //            var filenamExtension = fileNamedata.split('.');
                        //            if ((filenamExtension[filenamExtension.length - 1]).toUpperCase() == 'XLS' || (filenamExtension[filenamExtension.length - 1]).toUpperCase() == "XLSX") {
                        //                files.append(filesData[i].name, filesData[i], 'Web\\' + filesData[i].name.replace(/\s/g, ""));
                        //            } else {
                        //                alert('Please Select Valid File Format.. eg XLS, XLSX');
                        //                return false
                        //            }
                        //            var request = {
                        //                method: 'POST',
                        //                url: ENV.apiUrl + 'api/AgendaCreationController/SDFileUpload?filename=' + file_name + '.' + filenamExtension[1] +
                        //                 "&newloc=" + $scope.file_upload_details.newpath +
                        //                 "&oldloc=" + $scope.file_upload_details.oldpath +
                        //                 "&uname=" + $scope.file_upload_details.ftpusername +
                        //                 "&pass=" + $scope.file_upload_details.ftppassword,
                        //                data: files,
                        //                headers: {
                        //                    'Content-Type': undefined
                        //                }
                        //            };
                        //            $http(request).success(function (d) {
                        //                var obj = {
                        //                    sims_cur_code: $scope.s_cur_code,
                        //                    sims_academic_year: $scope.sims_academic_year,
                        //                    sims_file_uploaded_by: $rootScope.globals.currentUser.username,
                        //                    sims_file_path: d.split('***')[1]
                        //                }
                        //                $http({
                        //                    method: 'post',
                        //                    url: ENV.apiUrl + 'api/student/fileUpload/SDFU_insert',
                        //                    data: obj
                        //                }).then(function (res) {
                        //                    records.forEach(function (cr) { cr.sims_sr_no = res.data })
                        //                    $http({
                        //                        method: 'post',
                        //                        url: ENV.apiUrl + 'api/student/fileUpload/SDFUDetails_insert',
                        //                        data: records
                        //                    }).then(function (res) {
                        //                        alert('File uploaded, records inserted.');
                        //                        $scope.isLoading = false;
                        //                    },
                        //                    function (err) {
                        //                        debugger
                        //                    })
                        //                },
                        //                function (err) {
                        //                    debugger
                        //                })
                        //            });
                        //        }
                        //    }
                        //}, 250)
                    })
                }
                reader.onerror = function (ex) {
                    console.log(ex);
                };
                reader.readAsBinaryString(filesData[0]);
            }
            $scope.btnSave = function () {
                $scope.isLoading = true;
                var filesData = $('#inputGroupFile01')[0].files;
                var files = new FormData();
                var v = new Date();
                var file_name = 'SDFU_' + v.getHours() + '_' + v.getMonth() + '_' + v.getSeconds() + '_' + v.getMilliseconds() + '_' + Math.floor((Math.random() * 1000) + 1);
                $scope.file_upload_details.ftppassword = $scope.file_upload_details.ftppassword.replace('&', '%26');
                for (var i = 0; i < filesData.length; i++) {
                    var fileNamedata = filesData[i].name;
                    var filenamExtension = fileNamedata.split('.');
                    if ((filenamExtension[filenamExtension.length - 1]).toUpperCase() == 'XLS' || (filenamExtension[filenamExtension.length - 1]).toUpperCase() == "XLSX") {
                        files.append(filesData[i].name, filesData[i], 'Web\\' + filesData[i].name.replace(/\s/g, ""));
                    } else {
                        alert('Please Select Valid File Format.. eg XLS, XLSX');
                        return false
                    }
                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + 'api/AgendaCreationController/SDFileUpload?filename=' + file_name + '.' + filenamExtension[1] +
                         "&newloc=" + $scope.file_upload_details.newpath +
                         "&oldloc=" + $scope.file_upload_details.oldpath +
                         "&uname=" + $scope.file_upload_details.ftpusername +
                         "&pass=" + $scope.file_upload_details.ftppassword,
                        data: files,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    $http(request).success(function (d) {
                        var obj = {
                            sims_cur_code: $scope.s_cur_code,
                            sims_academic_year: $scope.sims_academic_year,
                            sims_file_uploaded_by: $rootScope.globals.currentUser.username,
                            sims_file_path: d.split('***')[1]
                        }
                        $http({
                            method: 'post',
                            url: ENV.apiUrl + 'api/student/fileUpload/SDFU_insert',
                            data: obj
                        }).then(function (res) {
                            $scope.records.forEach(function (cr) { cr.sims_sr_no = res.data })
                            $http({
                                method: 'post',
                                url: ENV.apiUrl + 'api/student/fileUpload/SDFUDetails_insert',
                                data: $scope.records
                            }).then(function (res) {
                                $scope.isLoading = false;
                                var input = $('#inputGroupFile01');
                                input.replaceWith(input.val('').clone(true));
                                $scope.isErrorList = false;
                                $scope.records.length = 0;
                                $scope.tble_show = false;
                                alert('File uploaded successfully');
                            },
                            function (err) {
                                $scope.isLoading = false;
                                alert('Error in processing');
                            })
                        },
                        function (err) {
                            $scope.isLoading = false;
                            alert('Error in processing');
                        })
                    });
                }
            }
            $scope.do_file = (function () {
                var rABS = typeof FileReader !== "undefined" && (FileReader.prototype || {}).readAsBinaryString;
                var domrabs = document.getElementsByName("userabs")[0];
                if (!rABS) domrabs.disabled = !(domrabs.checked = false);
                var use_worker = typeof Worker !== 'undefined';
                var domwork = document.getElementsByName("useworker")[0];
                if (!use_worker) domwork.disabled = !(domwork.checked = false);

                var xw = function xw(data, cb) {
                    var worker = new Worker(XW.worker);
                    worker.onmessage = function (e) {
                        switch (e.data.t) {
                            case 'ready': break;
                            case 'e': console.error(e.data.d); break;
                            case XW.msg: cb(JSON.parse(e.data.d)); break;
                        }
                    };
                    worker.postMessage({ d: data, b: rABS ? 'binary' : 'array' });
                };
                return function do_file(files) {
                    rABS = domrabs.checked;
                    use_worker = domwork.checked;
                    var f = files[0];
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        if (typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
                        var data = e.target.result;
                        if (!rABS) data = new Uint8Array(data);
                        if (use_worker) xw(data, process_wb);
                        else process_wb(X.read(data, { type: rABS ? 'binary' : 'array' }));
                    };
                    if (rABS) reader.readAsBinaryString(f);
                    else reader.readAsArrayBuffer(f);
                };
            })();
            $scope.getViewDetails = function () {

                //$scope.acct_name = $("#cmb_acc_Code3 option:selected").text();
                //$scope.from = $scope.block1.from_date;
                //$scope.to = $scope.block1.to_date;
                //$scope.tble_show = true;

                //if ($scope.block1.gldd_acct_code != "" && $scope.block1.gldd_acct_code != undefined)
                //    $scope.msg1 = '';
                //else {
                //    $scope.msg1 = 'Field is Required';
                //    return;
                //}

                $('#loader').modal({ backdrop: 'static', keyboard: false });

                var data = {
                    cur_code: $scope.s_cur_code || null,
                    academic_year: $scope.sims_academic_year || null,
                    grade_code: $scope.sims_grade_code || null,
                    section_code: $scope.sims_section_code || null,
                    search_flag: $scope.sims_search_code || null,
                    search_text: $scope.search_txt || null
                    //gltr_comp_code: comp_code,
                    //gltr_doc_code: ($scope.block1['gltd_doc_code'] != undefined || $scope.block1['gltd_doc_code'] != "") ? $scope.block1['gltd_doc_code'] : null,
                    //gltr_acct_code: ($scope.block1.gldd_acct_code != undefined || $scope.block1.gldd_acct_code != "") ? $scope.block1.gldd_acct_code : null,
                    //gltr_pstng_date: ($scope.block1.from_date != undefined || $scope.block1.from_date != "") ? $scope.block1.from_date : null,
                    //gltr_doc_date: ($scope.block1.to_date != undefined || $scope.block1.to_date != "") ? $scope.block1.to_date : null,
                    //glco_cost_centre_code: ($scope.block1['glco_cost_centre_code'] != undefined || $scope.block1['glco_cost_centre_code'] != "") ? $scope.block1['glco_cost_centre_code'] : null,
                }
                //$http({
                //    method: 'post',
                //    url: ENV.apiUrl + 'api/studentdatabase/getSimsStudentExportDetails',
                //    data: data
                //}).then(function (res) {
                //    debugger
                //},
                //function (err) {
                //    debugger
                //})
                $scope.s_cur_code = ($scope.s_cur_code || '');
                $scope.sims_academic_year = ($scope.sims_academic_year || '');
                $scope.sims_grade_code = ($scope.sims_grade_code || '');
                $scope.sims_section_code = ($scope.sims_section_code || '');
                $scope.sims_search_code = ($scope.sims_search_code || '');
                $scope.search_flag = ($scope.search_flag || '');
                $scope.search_txt = ($scope.search_txt || '');

                $http.get(ENV.apiUrl + "api/studentdatabase/getSimsStudentExportDetails?cur_code=" + $scope.s_cur_code + "&academic_year=" + $scope.sims_academic_year + "&grade_code=" + $scope.sims_grade_code + "&section_code=" + $scope.sims_section_code + "&search_flag=" + $scope.sims_search_code + "&search_text=" + $scope.search_txt).then(function (res) {
                    debugger
                    $scope.trans_data = res.data;
                    $('#loader').modal('hide');
                    $scope.tble_show = true;
                    //if ($scope.block1.sltr_yob_amt > 0) {
                    //    $scope.opbal = $scope.block1.sltr_yob_amt;
                    //    $scope.cr = ' dr';
                    //}
                    //else if ($scope.block1.sltr_yob_amt < 0) {
                    //    $scope.opbal = Math.abs($scope.block1.sltr_yob_amt);
                    //    $scope.cr = ' cr';
                    //}
                    //else
                    //    $scope.opbal = $scope.block1.sltr_yob_amt;

                });

                //var close_amt;
                //$scope.closebal = 0;
                //$http.post(ENV.apiUrl + "api/GLSLQuery/list_All_GL_Query", data).then(function (res1) {
                //    $scope.trans_data = res1.data;


                //    $('#loader').modal('hide');
                //    $(".modal-backdrop").removeClass("modal-backdrop");

                //    if ($scope.trans_data.length > 0) {
                //        $scope.length = $scope.trans_data.length - 1;

                //        //if ($scope.block1.sltr_yob_amt > 0) {
                //        //    $scope.trans_data[$scope.length].gltr_total_dr_amount = parseFloat($scope.trans_data[$scope.length].gltr_total_dr_amount);//+ $scope.block1.sltr_yob_amt;
                //        //}
                //        //else if ($scope.block1.sltr_yob_amt < 0) {
                //        //    $scope.trans_data[$scope.length].gltr_total_cr_amount = parseFloat($scope.trans_data[$scope.length].gltr_total_cr_amount);//+Math.abs($scope.block1.sltr_yob_amt);
                //        //}
                //        close_amt = parseFloat($scope.trans_data[$scope.length].gltr_total_dr_amount) - parseFloat($scope.trans_data[$scope.length].gltr_total_cr_amount);


                //        if ($scope.block1.sltr_yob_amt >= 0) {
                //            close_amt = close_amt + parseFloat($scope.block1.sltr_yob_amt);
                //        }
                //        else if ($scope.block1.sltr_yob_amt < 0) {
                //            close_amt = parseFloat(close_amt) + parseFloat($scope.block1.sltr_yob_amt);
                //        }


                //        if (close_amt >= 0) {
                //            $scope.closebal = close_amt;
                //            $scope.dr = ' dr';
                //        }
                //        else if (close_amt < 0) {
                //            $scope.closebal = Math.abs(close_amt);
                //            $scope.dr = ' cr';
                //        }
                //        else
                //            $scope.closebal = close_amt;
                //    }

                //});

            }
        }]
        )
})();
