﻿(function () {
    'use strict';
    var obj1, temp, opr, comp, edt;
    var main, deletefin = [];
    var deletecode = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CertificateTCParameterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$stateParams', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $stateParams) {

            $scope.pagesize = "10";
            $scope.pageindex = 1;
            $scope.search = true;
            var dataforSave = [];
            $scope.reset = true;
            $scope.display = true;
            $scope.table = true;
            $scope.tcdetails = true;
            $scope.save_btn = true;
            $scope.savecancel_btn = true;
            $scope.Update_btn = false;
            $scope.print_btn = false;
            $scope.divcode_readonly = true;
            $scope.temp = {};
            $scope.edtt = {};
            $scope.enrollno_data = false;
            //$scope.temp['sims_certificate_fee_paid'] = true;

            if ($http.defaults.headers.common['schoolId'] == 'tsis')
                $scope.report_name = 'Certificate.CERR12TSIS';

            else  if ($http.defaults.headers.common['schoolId'] == 'iis')
                 $scope.report_name = 'Certificate.CERR002IIS_TC';
            else  if ($http.defaults.headers.common['schoolId'] == 'vision')
                 $scope.report_name = 'Certificate.CERR002DVHSTCNEW';
            else  if ($http.defaults.headers.common['schoolId'] == 'dvhs')
                 $scope.report_name = 'Certificate.CERR002DVHSTCNEW';
            else  if ($http.defaults.headers.common['schoolId'] == 'dvhss')
                $scope.report_name = 'Certificate.CERR002DVHSSTCNEW';
                 else  if ($http.defaults.headers.common['schoolId'] == 'dvps')
                 $scope.report_name = 'Certificate.CERR002DVHSTCNEW';
            else
                $scope.report_name = 'Certificate.CERR27ADIS';



            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100)

            $timeout(function () {
                $("#fixTable2").tableHeadFixer({ 'top': 1 });
            }, 100)
            var user = $rootScope.globals.currentUser.username;

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.edits = {};
            var dt = new Date();
            $scope.edits.tC_Date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
            //$scope.edts.sims_certificate_date_of_issue = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
            $(function () {
                $('#cmb_Subjects').multipleSelect({
                    width: '100%'
                });
            });

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                debugger
                $scope.ComboBoxValues = AllComboBoxValues.data;
                //$scope.temp.sims_academic_year = $scope.ComboBoxValues[0].sims_acadmic_year;
                //$scope.temp.s_cur_code = $scope.ComboBoxValues[0].sims_cur_code;
                $scope.edt = {
                    sims_academic_year: $scope.ComboBoxValues[0].sims_acadmic_year,
                    s_cur_code: $scope.ComboBoxValues[0].sims_cur_code,
                };
            });

            $http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (res) {
                debugger;
                $scope.cmbCur = res.data;
                if ($scope.cmbCur.length > 0) {
                    $scope.edt.s_cur_code = $scope.cmbCur[0].sims_cur_code;
                    $scope.getAcadm_yr($scope.cmbCur[0].sims_cur_code)

                }
            })


            $scope.getAcadm_yr = function (cur_code) {
                debugger;
                $scope.curriculum_code = cur_code;
                $http.get(ENV.apiUrl + "api/attendance/getAttendanceyear?cur_code=" + cur_code).then(function (res) {
                    $scope.Acdm_year = res.data; console.log($scope.Acdm_year);

                    if ($scope.cmbCur.length > 0) {
                        $scope.edt['sims_academic_year'] = res.data[0].sims_academic_year;
                        $scope.academic_change(res.data[0].sims_academic_year);
                    }
                });
            }


            $scope.academic_change = function (acd) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.global_Search.global_curriculum_code + "&academic_year=" + acd).then(function (res) {
                    $scope.All_grade_names = res.data;
                })
            }


            //$scope.academic_change = function (acd) {

            //    $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.global_Search.global_curriculum_code + "&academic_year=" + acd).then(function (res) {
            //        $scope.All_grade_names = res.data;
            //    })
            //}

            //Navigated from Cancel Admission 
            //debugger;
            //console.log($stateParams.edt);
            //var cadmin = $stateParams.edt;
            //var opr = 'P';
            //$http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Student_details?enroll_No=" + cadmin.enroll + "&opr=" + opr).then(function (certificate) {

            //    $scope.savedata = certificate.data;
            //    $scope.temp = $scope.savedata[0];
            //    //var dt = new Date();
            //    //$scope.edts['tC_Date'] = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
            //});

            //$http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Stud_subject?enroll_No=" + cadmin.enroll).then(function (subject) {
            //    $scope.subject_studied = subject.data;
            //    console.log($scope.subject_studied);
            //    setTimeout(function () {
            //        $('#cmb_Subjects').change(function () {
            //            console.log($(this).val());
            //        }).multipleSelect({
            //            width: '100%'
            //        });
            //    }, 1000);
            //});

            //Select Data SHOW (Combo Box)

            $http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_certificate_no").then(function (certificate) {
                $scope.certificate_data = certificate.data;
                $scope.edtt.sims_certificate_number = $scope.certificate_data[0].sims_certificate_number;
            });

            //$http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Stream").then(function (get_stream) {
            //    $scope.stream_data = get_stream.data;
            //    $scope.edts = {
            //        stream_code: $scope.stream_data[0].stream_code
            //    };
            //});

            $http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_status_code").then(function (get_stream) {
                debugger
                $scope.status_data = get_stream.data;
                
            });

            $scope.reset = function () {

                debugger;
                $scope.Update_btn = false;
                $scope.print_btn = false;
                $scope.save_btn = true;
                $scope.savecancel_btn = true;
                $scope.temp = {};
                //$scope.edt.sims_certificate_number = "";
                $scope.temp.sims_certificate_enroll_number = "";
                $scope.temp.stud_name = "";
                $scope.temp.nationality = "";
                $scope.temp.father_name = "";
                $scope.temp.religion = "";
                $scope.temp.date_of_birth = "";
                $scope.temp.date_of_admission = "";
                $scope.temp.dobw = "";
                $scope.temp.present_class = "";
                $scope.temp.present_sec = "";
                $scope.temp.admitted_class = "";
                $scope.temp.result_end_acad_yr = "";
                $scope.temp.promoted_class = "";
                $scope.temp.promoted_acad_yr = "";
                $scope.temp.admitted_class = "";
                $scope.temp.sims_certificate_subject_studied = '';
                $scope.temp.subject_studied = [];
                $scope.subject_studied = [];
                $scope.temp.present_days = "";
                $scope.temp.total_days = "";
                $scope.temp.credits = "";
                $scope.temp.sims_certificate_general_conduct = "";
                $scope.temp.sims_certificate_remark = "";
                $scope.temp.registrar = "";
                $scope.temp.principle = "";
                $scope.temp.sims_certificate_registration_register_no = "";
                $scope.temp.sims_certificate_registration_serial_no = "";
                $scope.temp.sims_certificate_date_of_leaving = "";
                $scope.temp.sims_certificate_reason_of_leaving = "";
                $scope.temp.sims_certificate_academic_progress = "";
                $scope.temp.sims_certificate_fee_paid = "";
                $scope.temp.sims_certificate_sc_st_status = "";
                //$scope.temp.sims_student_promoted_to = "Incomplete";
                //$scope.temp.sims_certificate_general_conduct = "Good";
                $scope.temp.sims_student_exam_appeared = "";
                $scope.enrollno_data = false;
                $scope.temp.sims_certificate_fee_paid = true;
                var v = document.getElementById('yes');
                var c = document.getElementById('no');
                v.checked = false;
                c.checked = false;
                $scope.ret_chk_label = false;
                $scope.ret_chk = false;
                subject_code = [];
               // $("#cmb_Subjects").multipleSelect("UncheckAll");
                setTimeout(function () {
                    debugger;
                    $('#cmb_Subjects').change(function () {
                       // console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 800);
            }

           

            $scope.stud_details = function () {

                $scope.Update_btn = false;
                $scope.print_btn = false;
                $scope.save_btn = true;
                $scope.savecancel_btn = true;

                if ($scope.temp.sims_certificate_enroll_number == null || $scope.temp.sims_certificate_enroll_number == "") {

                    $scope.searchtable = false;
                    $('#myModal2').modal('show');

                }
                else {
                    var opr = 'P';
                    //$http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Student_details?enroll_no=" + $scope.temp.sims_certificate_enroll_number + "&opr=" + opr).then(function (Student_Details) {
                    //    $scope.msg = Student_Details.data;                       
                    //        $scope.savedata = Student_Details.data;
                    //        $scope.temp = $scope.savedata[0];
                    //        console.log($scope.temp);                        
                    //});

                    $http.post(ENV.apiUrl + "api/CertificateTCParameter/Check_Tc_details?userid=" + $scope.temp.sims_certificate_enroll_number).then(function (check) {

                        $scope.msg = check.data;
                       
                        //$scope.msg1 = $scope.msg;
                        //if ($scope.msg == true) {
                        //    swal('', 'TC Already Issued for this Student');
                        //}
                        //else {                           
                            $http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Student_details?enroll_No=" + $scope.temp.sims_certificate_enroll_number + "&opr=" + opr).then(function (certificate) {
                                debugger
                                $scope.savedata = certificate.data;
                                $scope.temp = $scope.savedata[0];
                                $scope.temp.sims_certificate_exam_year = $scope.savedata[0].exam_year;
                                $scope.temp.sims_certificate_fee_paid= true;
                              //  $scope.temp.sims_student_exam_appeared=$scope.savedata.sims
                            });
                            
                            //$scope.temp.sims_certificate_enroll_number = info.s_enroll_no;
                            $http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Stud_subject?enroll_No=" + $scope.temp.sims_certificate_enroll_number).then(function (subject) {
                                debugger;
                                $scope.subject_studied = subject.data;
                                console.log($scope.subject_studied);
                                setTimeout(function () {
                                    debugger;
                                    $('#cmb_Subjects').change(function () {
                                        //console.log($(this).val());
                                    }).multipleSelect({
                                        width: '100%'
                                       
                                    });
                                    $("#cmb_Subjects").multipleSelect("checkAll");
                                   
                                }, 1000);
                               


                            });

                       // }//
                        //flag = false;

                    });


                }
            }

            $scope.searchstudent = function () {
                debugger;
                $scope.busy = true;
                $scope.searchtable = false;

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.edt)).then(function (Allstudent) {

                    $scope.student = Allstudent.data;

                    $scope.busy = false;

                    $scope.searchtable = true;
                });
            }

            $scope.edit = function (info) {
                debugger;
                $scope.temp = {};
                $http.post(ENV.apiUrl + "api/CertificateTCParameter/Check_Tc_details?userid=" + info.s_enroll_no).then(function (certi) {

                    $scope.msg = certi.data;
                    //$scope.msg1 = $scope.msg;
                    if ($scope.msg == true) {
                        swal('', 'TC Already Issued for this Student');
                    }
                    else {
                        var opr = 'R';
                        $http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Student_details?enroll_No=" + info.s_enroll_no + "&opr=" + opr + "&academic_year=" + $scope.edt.sims_academic_year).then(function (certificate) {
                            debugger;
                            $scope.savedata = certificate.data;
                            $scope.temp = $scope.savedata[0];
                            $scope.temp.sims_certificate_fee_paid = true;
                            //var dt = new Date();
                            //$scope.edts['tC_Date'] = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();

                        });

                        $scope.enrollno = true;
                        $http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_doc_details?enroll_No=" + $scope.temp.sims_certificate_enroll_number).then(function (certificatedata) {
                            debugger;
                            $scope.Studentdata = certificatedata.data;
                            // $scope.temp = $scope.savedata[0];
                        });

                        $scope.temp.sims_certificate_enroll_number = info.s_enroll_no;
                        $http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Stud_subject?enroll_No=" + $scope.temp.sims_certificate_enroll_number).then(function (subject1) {
                            debugger;
                            $scope.subject_studied = subject1.data;
                            console.log($scope.subject_studied);
                            setTimeout(function () {
                                $('#cmb_Subjects').change(function () {
                                    console.log($(this).val());
                                }).multipleSelect({
                                    width: '100%'
                                });
                                $("#cmb_Subjects").multipleSelect("checkAll");
                            }, 1000);
                        });

                    }
                    //flag = false;
                });

               $scope.stud_details();
                
                
            }

            $scope.getsectioncode = function (cur, year, grade) {

                $http.get(ENV.apiUrl + "api/common/SectionTerm/getAllSections?cur_code=" + cur + "&ac_year=" + year + "&g_code=" + grade).then(function (res1) {
                    $scope.sections = res1.data;
                });
            }

            //Ceritficate Search Submit Button(Modal 1)
            var cer_no = '';
            $scope.submit = function () {
                debugger;
                cer_no = '';
                for (var j = 0; j < $scope.filteredTodos.length; j++) {
                    var v = document.getElementById($scope.filteredTodos[j].sims_certificate_number + j);
                    if (v.checked == true) {
                        $scope.savedata = $scope.filteredTodos[j];
                        $scope.temp = $scope.savedata;
                        cer_no = $scope.temp.sims_certificate_number;
                        //console.log($scope.savedata.sims_certificate_subject_studied)
                        //var array = $scope.savedata.sims_certificate_subject_studied.split(',');
                        //$scope.temp['sims_certificate_subject_studied']=array;
                    }
                }

                $scope.enrollno_data = true;
                $http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_doc_details?enroll_No=" + $scope.temp.sims_certificate_enroll_number).then(function (certificatedata) {

                    $scope.Studentdata = certificatedata.data;
                    // $scope.temp = $scope.savedata[0];
                });

                $http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Stud_subject?enroll_No=" + $scope.temp.sims_certificate_enroll_number).then(function (subj) {
                    debugger
                    $scope.subject_studied = subj.data;
                    console.log($scope.subject_studied);
                    setTimeout(function () {
                        debugger;
                        $('#cmb_Subjects').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                        $("#cmb_Subjects").multipleSelect("checkAll");
                    }, 1000);


                    var add = [];

                    //setTimeout(function () {
                    //    //add.push('08')

                    //    $scope.test = $scope.savedata.sims_certificate_subject_studied.substring(0, $scope.savedata.sims_subject_code.length - 1);
                    //    $scope.cls = $scope.test.split(",");
                    //    for (var i = 0; i < $scope.cls.length; i++) {
                    //        add.push($scope.cls[i])

                    //    }
                    //    try {
                    //        $("#cmb_Subjects").multipleSelect("setSelects", add);
                    //    }
                    //    catch (e) {

                    //    }
                    //}, 1000);

                  

                });
                $('#myModal1').modal('hide');
                debugger;
                //var a = document.getElementById('yes');
                //var b = document.getElementById('no');
                //if ($scope.subject_studied.sims_student_promoted_to = 'Incomplete') {
                //    a.checked = true;
                //}
                //else {
                //    b.checked = true;
                //}

                $scope.Update_btn = true;
                $scope.print_btn = true;
                $scope.save_btn = false;
                $scope.savecancel_btn = false;

            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.display = true;
                $scope.table = true;
            }

            $scope.tcdetails = function () {

                $scope.certificate = true;
                $('#myModal1').modal('show');

				$scope.Reset_grid = function () {
                    $scope.edt.enroll = '';
                    $scope.filteredTodos = '';
                }
				
                $scope.cert_show = function () {
                    debugger
                    //if ($scope.edt.enroll = 'undefined' || $scope.edt.enroll == "") {
                    //    $scope.edt.enroll = null;
                    //}
                    $http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Search_Tc_details?enroll_no=" + $scope.edt.enroll).then(function (res1) {

                        $scope.CreDiv = res1.data;
                        $scope.totalItems = $scope.CreDiv.length;
                        $scope.todos = $scope.CreDiv;
                        $scope.makeTodos();
                        console.log(res1.data);
                    });
                }

                $scope.size = function (str) {
                    console.log(str);
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                }

                $scope.index = function (str) {
                    $scope.pageindex = str;
                    $scope.currentPage = str;
                    console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                    main.checked = false;
                    $scope.CheckAllChecked();
                }

                $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

                $scope.makeTodos = function () {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }

                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                };

                $scope.searched = function (valLists, toSearch) {
                    return _.filter(valLists,

                    function (i) {
                        /* Search Text in all  fields */
                        return searchUtil(i, toSearch);
                    });
                };

                //Search
                $scope.search = function () {
                    $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                    $scope.totalItems = $scope.todos.length;
                    $scope.currentPage = '1';
                    if ($scope.searchText == '') {
                        $scope.todos = $scope.CreDiv;
                    }
                    $scope.makeTodos();
                }

                function searchUtil(item, toSearch) {
                    /* Search Text in all 3 fields */
                    return (item.stud_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                            item.sims_certificate_enroll_number == toSearch) ? true : false;
                }

            }

            //DATA SAVE INSERT
            var datasend = [];
            var subject_code = [];
            $scope.SaveData = function () {
                debugger;
                console.log($scope.temp);
                console.log($scope.edt);
                debugger;
                // if (Myform) {
                if ($scope.temp.sims_certificate_reason_of_leaving == null || $scope.temp.sims_certificate_reason_of_leaving == undefined) {
                    swal('', 'Please enter reason of leaving');
                    return;
                }

                //if (cadmin == '' || cadmin == null) {
                var subject = $scope.temp.sims_certificate_subject_studied;
                
                subject_code = subject_code + ',' + subject;
                var str2 = subject_code.substr(subject_code.indexOf(',') + 1);
                
                var data = {
                    'opr': 'I',
                    'sims_certificate_number': $scope.edtt.sims_certificate_number,
                    'sims_certificate_enroll_number': $scope.temp.sims_certificate_enroll_number,
                    'sims_certificate_date_of_leaving': $scope.temp.sims_certificate_date_of_leaving,
                    'sims_certificate_reason_of_leaving': $scope.temp.sims_certificate_reason_of_leaving,
                    'sims_certificate_general_conduct': $scope.temp.sims_certificate_general_conduct,
                    'sims_certificate_academic_progress': $scope.temp.sims_certificate_academic_progress,
                    'sims_certificate_subject_studied': str2,
                    'sims_certificate_remark': $scope.temp.sims_certificate_remark,
                    'sims_certificate_attendance_remark': $scope.temp.sims_certificate_attendance_remark,
                    'sims_certificate_qualified_for_promotion': $scope.temp.sims_certificate_qualified_for_promotion,
                    'sims_certificate_date_of_issue': $scope.edits.tC_Date,
                    'sims_certificate_fee_paid': $scope.temp.sims_certificate_fee_paid,
                    'sims_certificate_sc_st_status': $scope.temp.sims_certificate_sc_st_status,
                    'sims_certificate_field1': $scope.temp.sims_certificate_field1,
                    'sims_certificate_field2': $scope.temp.sims_certificate_field2,
                    'sims_certificate_field3': $scope.temp.sims_certificate_field3,
                    'sims_certificate_field4': $scope.temp.sims_certificate_field4,
                    'sims_certificate_field5': $scope.temp.sims_certificate_field5,
                    'sims_certificate_registration_register_no': $scope.temp.sims_certificate_registration_register_no,
                    'sims_certificate_registration_serial_no': $scope.temp.sims_certificate_registration_serial_no,
                    'sims_certificate_result_register_no': $scope.temp.sims_certificate_result_register_no,
                    'sims_certificate_result_serial_no': $scope.temp.sims_certificate_result_serial_no,                    
                    'sims_certificate_academic_year': $scope.temp.academic_year,
                    'sims_certificate_exam_year': $scope.temp.sims_certificate_exam_year,
                    'sims_student_promoted_to': $scope.temp.sims_student_promoted_to,
                    //'promoted_acad_yr': $scope.temp.promoted_acad_yr,
                    'sims_student_fees_paid_lastmonth': $scope.temp.sims_student_fees_paid_lastmonth,
                    'sims_student_exam_result': $scope.temp.sims_student_exam_result,
                    'sims_student_exam_appeared': $scope.temp.sims_student_exam_appeared,                    
                }
                datasend.push(data);
                $http.post(ENV.apiUrl + "api/CertificateTCParameter/CUDCertificate_Tc_Parameter_Saveonly", datasend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    debugger;
                    if ($scope.msg1 == true) {
                       
                        //$scope.OkCancelAdmission();
                        swal({
                            title: "Alert",
                            text: "Admission Not Cancelled and Record Saved Successfully!!Do You Want To Print Report?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                //swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                                var data = {
                                    location: $scope.report_name,
                                    parameter: {
                                        roll_no: $scope.temp.sims_certificate_enroll_number,

                                        acad:$scope.temp.academic_year
                                    },
                                    state: 'main.Sim505',
                                    ready: function () {
                                        this.refreshReport();
                                    },
                                }
                                window.localStorage["ReportDetails"] = JSON.stringify(data)
                                $state.go('main.ReportCardParameter')//Certificate.CERR27ADIS
                                $scope.Update_btn = false;
                                $scope.print_btn = false;
                                $scope.save_btn = true;
                                $scope.savecancel_btn = true;
                                $scope.currentPage = true;
                                $scope.reset();
                            }
                            else {
                                $scope.reset();
                            }
                        });
                    }
                    else {
                        //swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                        $scope.Update_btn = false;
                        $scope.print_btn = false;
                        $scope.save_btn = true;
                        $scope.savecancel_btn = true;
                        $scope.currentPage = true;
                        $scope.reset();
                    }
                });
               // $scope.reset();
                datasend = [];
                $scope.table = true;
                $scope.display = true;
                //}
            }

            $scope.SaveCancel = function () {
                console.log($scope.temp);
                console.log($scope.edt);
                debugger;
                // if (Myform) {
                //if ($scope.temp.sims_certificate_field3 == null || $scope.temp.sims_certificate_field3 == undefined) {
                //    swal('', 'Please Select Student Status');
                //    return;
                //}

                //if (cadmin == '' || cadmin == null) {
                if ($scope.temp.sims_certificate_reason_of_leaving == undefined || $scope.temp.sims_certificate_reason_of_leaving == null) {
                    swal('', 'Please provide reason for leaving');
                    return;
                }
                var subject = $scope.temp.sims_certificate_subject_studied;
                subject_code = subject_code + ',' + subject;
                var str2 = subject_code.substr(subject_code.indexOf(',') + 1);
                var data = {
                    'opr': 'I',
                    'sims_certificate_number': $scope.edtt.sims_certificate_number,
                    'sims_certificate_enroll_number': $scope.temp.sims_certificate_enroll_number,
                    'sims_certificate_date_of_leaving': $scope.temp.sims_certificate_date_of_leaving,
                    'sims_certificate_reason_of_leaving': $scope.temp.sims_certificate_reason_of_leaving,
                    'sims_certificate_general_conduct': $scope.temp.sims_certificate_general_conduct,
                    'sims_certificate_academic_progress': $scope.temp.sims_certificate_academic_progress,
                    'sims_certificate_subject_studied': str2,
                    'sims_certificate_remark': $scope.temp.sims_certificate_remark,
                    'sims_certificate_attendance_remark': $scope.temp.sims_certificate_attendance_remark,
                    'sims_certificate_qualified_for_promotion': $scope.temp.sims_certificate_qualified_for_promotion,
                    'sims_certificate_date_of_issue': $scope.edits.tC_Date,
                    'sims_certificate_fee_paid': $scope.temp.sims_certificate_fee_paid,
                    'sims_certificate_sc_st_status': $scope.temp.sims_certificate_sc_st_status,
                    'sims_certificate_field1': $scope.temp.sims_certificate_field1,
                    'sims_certificate_field2': $scope.temp.sims_certificate_field2,
                    'sims_certificate_field3': $scope.temp.sims_certificate_field3,
                    'sims_certificate_field4': $scope.temp.sims_certificate_field4,
                    'sims_certificate_field5': $scope.temp.sims_certificate_field5,
                    'sims_certificate_registration_register_no': $scope.temp.sims_certificate_registration_register_no,
                    'sims_certificate_registration_serial_no': $scope.temp.sims_certificate_registration_serial_no,
                    'sims_certificate_result_register_no': $scope.temp.sims_certificate_result_register_no,
                    'sims_certificate_result_serial_no': $scope.temp.sims_certificate_result_serial_no,
                    'sims_certificate_academic_year': $scope.temp.academic_year,
                    'sims_certificate_exam_year': $scope.temp.sims_certificate_exam_year,
                    'sims_student_promoted_to': $scope.temp.sims_student_promoted_to,
                    //'promoted_acad_yr': $scope.temp.promoted_acad_yr,
                    'sims_student_fees_paid_lastmonth': $scope.temp.sims_student_fees_paid_lastmonth,
                    'sims_student_exam_result': $scope.temp.sims_student_exam_result,
                    'sims_student_exam_appeared': $scope.temp.sims_student_exam_appeared,
                    'sims_certificate_req_status': 'I'
                }
                datasend.push(data);
                $http.post(ENV.apiUrl + "api/CertificateTCParameter/CUDCertificate_Tc_Parameter", datasend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    debugger;
                    if ($scope.msg1 == true) {

                        //$scope.OkCancelAdmission();
                        swal({
                            title: "Alert",
                            text: "Admission Cancelled and Record Saved Successfully!!Do You Want To Print Report?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                //swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                                var data = {
                                    location: $scope.report_name,
                                    parameter: {
                                        roll_no: $scope.temp.sims_certificate_enroll_number,

                                        acad: $scope.temp.academic_year
                                    },
                                    state: 'main.Sim505',
                                    ready: function () {
                                        this.refreshReport();
                                    },
                                }
                                window.localStorage["ReportDetails"] = JSON.stringify(data)
                                $state.go('main.ReportCardParameter')//Certificate.CERR27ADIS
                                $scope.Update_btn = false;
                                $scope.print_btn = false;
                                $scope.save_btn = true;
                                $scope.savecancel_btn = true;
                                $scope.currentPage = true;
                                $scope.reset();
                            }
                            else {
                                $scope.reset();
                            }
                        });
                    }
                    else {
                        //swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                        $scope.Update_btn = false;
                        $scope.print_btn = false;
                        $scope.save_btn = true;
                        $scope.savecancel_btn = true;
                        $scope.currentPage = true;
                        //$scope.reset();
                    }
                });
                $scope.reset();
                datasend = [];
                $scope.table = true;
                $scope.display = true;
                //}
            }

            //DATA UPDATE
            var dataforUpdate = [];
            $scope.update = function () {
                debugger
                //if (Myform) {
                //var data = $scope.temp;
                //data.opr = "U";
                //dataforUpdate.push(data);
                if ($scope.temp.sims_certificate_reason_of_leaving == undefined || $scope.temp.sims_certificate_reason_of_leaving == null) {
                    swal('', 'Please provide reason for leaving');
                    return;
                }
                var subject = $scope.temp.sims_certificate_subject_studied;
                subject_code = subject_code + ',' + subject;
                var str2 = subject_code.substr(subject_code.indexOf(',') + 1);

                var data = {
                    'opr': 'U',
                    'sims_certificate_number': cer_no,
                    'sims_certificate_enroll_number': $scope.temp.sims_certificate_enroll_number,
                    'sims_certificate_date_of_leaving': $scope.temp.sims_certificate_date_of_leaving,
                    'sims_certificate_reason_of_leaving': $scope.temp.sims_certificate_reason_of_leaving,
                    'sims_certificate_general_conduct': $scope.temp.sims_certificate_general_conduct,
                    'sims_certificate_academic_progress': $scope.temp.sims_certificate_academic_progress,
                    'sims_certificate_subject_studied': str2,
                    'sims_certificate_remark': $scope.temp.sims_certificate_remark,
                    'sims_certificate_attendance_remark': $scope.temp.sims_certificate_attendance_remark,
                    'sims_certificate_qualified_for_promotion': $scope.temp.sims_certificate_qualified_for_promotion,
                    'sims_certificate_date_of_issue': $scope.edits.tC_Date,
                    'sims_certificate_fee_paid': $scope.temp.sims_certificate_fee_paid,
                    'sims_certificate_sc_st_status': $scope.temp.sims_certificate_sc_st_status,
                    'sims_certificate_field1': $scope.temp.sims_certificate_field1,
                    'sims_certificate_field2': $scope.temp.sims_certificate_field2,
                    'sims_certificate_field3': $scope.temp.sims_certificate_field3,
                    'sims_certificate_field4': $scope.temp.sims_certificate_field4,
                    'sims_certificate_field5': $scope.temp.sims_certificate_field5,
                    'sims_certificate_registration_register_no': $scope.temp.sims_certificate_registration_register_no,
                    'sims_certificate_registration_serial_no': $scope.temp.sims_certificate_registration_serial_no,
                    'sims_certificate_result_register_no': $scope.temp.sims_certificate_result_register_no,
                    'sims_certificate_result_serial_no': $scope.temp.sims_certificate_result_serial_no,
                    //'stream': $scope.edts.stream_code,
                    //'Credits': $scope.temp.credits,
                    'sims_certificate_academic_year': $scope.temp.sims_certificate_academic_year,
                    'sims_certificate_exam_year': $scope.temp.sims_certificate_exam_year,
                    'sims_student_promoted_to': $scope.temp.sims_student_promoted_to,
                    //'promoted_acad_yr': $scope.temp.promoted_acad_yr,
                    'sims_student_fees_paid_lastmonth': $scope.temp.sims_student_fees_paid_lastmonth,
                    'sims_student_exam_result': $scope.temp.sims_student_exam_result,
                    'sims_student_exam_appeared': $scope.temp.sims_student_exam_appeared,
                    'sims_certificate_req_status': 'I'
                }

                dataforUpdate.push(data);
                $http.post(ENV.apiUrl + "api/CertificateTCParameter/CUDCertificate_Tc_Parameter", dataforUpdate).then(function (msg) {

                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({
                            title: "Alert",
                            text: "Updated Successfully!!Do You Want To Print Report?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                debugger;
                                //swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                                var data = {
                                    location: $scope.report_name,
                                    parameter: {
                                        roll_no: $scope.temp.sims_certificate_enroll_number,
                                        acad: $scope.temp.academic_year
                                    },
                                    state: 'main.Sim505',
                                    ready: function () {
                                        this.refreshReport();
                                    },
                                }
                                window.localStorage["ReportDetails"] = JSON.stringify(data)
                                $state.go('main.ReportCardParameter') //Certificate.CERR27ADIS
                                $scope.Update_btn = false;
                                $scope.print_btn = false;
                                $scope.save_btn = true;
                                $scope.savecancel_btn = true;
                                $scope.currentPage = true;
                                $scope.reset();
                            }
                        });
                    }
                    else {
                        debugger
                        $scope.Update_btn = true;
                        $scope.print_btn = false;
                        $scope.save_btn = false;
                        $scope.savecancel_btn = false;
                        $scope.currentPage = true;
                        $scope.reset();
                    }
                });
                //$scope.reset();
                dataforUpdate = [];
                $scope.table = true;
                $scope.display = true;
                //} 
            }

            //Print Certificate/Report Generation 
            $scope.print = function () {
                debugger

                console.log($scope.temp.academic_year);
                var data = {
                    location: $scope.report_name,
                    parameter: {
                        roll_no: $scope.temp.sims_certificate_enroll_number,
                      //  acad:'2017'


                    },
                    state: 'main.Sim505',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')//Certificate.CERR27ADIS
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_certificate_number + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_certificate_number + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function (info) {
                debugger
                $('tr').removeClass("row_selected");
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_certificate_number + i);
                    if (v.checked == true) {
                        $scope.filteredTodos[i].ischange = true;
                    }
                    else {
                        $scope.filteredTodos[i].ischange = false;
                    }
                }

                $("input[type='radio']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                //main = document.getElementById('$index');
                //if (main.checked == true) {

                //    main.checked = false;
                //    $scope.color = '#edefef';
                //    $scope.row1 = '';
                //    $('tr').removeClass("row_selected");
                //}
            }

            //Radio Button Click 
            $scope.radio1Click = function () {
                //$scope.ret_chk_label = false;
                //$scope.ret_chk = false;
                debugger;
                if ($scope.temp.sims_certificate_enroll_number == undefined) {
                    var v = document.getElementById('yes');
                    var c = document.getElementById('no');
                    v.checked = false;
                    c.checked = false;
                    return;
                }
                $scope.temp.sims_student_exam_result = "Incomplete";
                $scope.temp.sims_certificate_general_conduct = "Good"
                $scope.temp.sims_student_exam_appeared = "";
              //  $scope.temp.sims_certificate_exam_year = "";
                var class_nm=$scope.temp.present_class.split(' ');
                if (class_nm.length > 0) {
                    if ( class_nm[0] == '12') {
                        $scope.temp.sims_student_exam_appeared = "All India Senior School Certifcate Examination (AISSCE) conducted by CBSE Delhi,India";// + $scope.Studentdata.sims_academic_year;
                    }
                    else if (class_nm[0] == '10') {
                        $scope.temp.sims_student_exam_appeared = "All India Secondary School  Examination (AISSCE) conducted by CBSE Delhi,India";// + $scope.Studentdata.sims_academic_year;

                    }
                    else {
                        $scope.temp.sims_student_exam_appeared = "CBSE Delhi,India";// + $scope.Studentdata.sims_academic_year;

                    }
                }

            }

            $scope.radio2Click = function () {
                debugger
                if ($scope.temp.sims_certificate_enroll_number == undefined) {
                    var v = document.getElementById('yes');
                    var c = document.getElementById('no');
                    v.checked = false;
                    c.checked = false;
                    return;
                }
                //$scope.ret_chk_label = true;
                //$scope.ret_chk = true;
                $scope.yeardata = [];
                $scope.temp.sims_student_exam_result = "Passed";
                $scope.temp.sims_certificate_general_conduct = "Good"
              //  $scope.temp.sims_student_exam_appeared = "All India Senior School Certifcate Examination (AISSCE) conducted by CBSE Delhi,India";// + $scope.Studentdata.sims_academic_year;
                var class_nm = $scope.temp.present_class.split(' ');
                if (class_nm.length > 0) {
                    if (class_nm[0] == '11' || class_nm[0] == '12') {
                        $scope.temp.sims_student_exam_appeared = "All India Senior School Certifcate Examination (AISSCE) conducted by CBSE Delhi,India";// + $scope.Studentdata.sims_academic_year;
                    }
                    else if (class_nm[0] == '10') {
                        $scope.temp.sims_student_exam_appeared = "All India Secondary School  Examination (AISSCE) conducted by CBSE Delhi,India";// + $scope.Studentdata.sims_academic_year;

                    }
                    else {
                        $scope.temp.sims_student_exam_appeared = "CBSE Delhi,India";// + $scope.Studentdata.sims_academic_year;

                    }
                }

                //$http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Student_details?enroll_No=" + $scope.temp.sims_certificate_enroll_number + "&opr=" + 'P').then(function (certificate) {
                //    debugger;
                //    $scope.yeardata = certificate.data;
                //    $scope.temp.sims_certificate_exam_year = $scope.yeardata[0].sims_certificate_exam_year;
                //});               
            }

            $scope.Reset_grid = function () {
                $scope.edt = {};
                $scope.reset();
            }

            $scope.downloaddoc1 = function (str) {
                debugger;
                //  $scope.url = "http://localhost:90/SIMSAPI/Content/Assignment/TeacherDoc/" + str;
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/TCDocs/' + str.sims_certificate_doc_path_name;
                window.open($scope.url);
            }
         }])
})();