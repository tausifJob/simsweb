﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudentAssignRemark_ahgsCont',
        ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.display = true;
            $scope.grid = true;
            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.flag = true;
            var str, cnt;
            var main;
            var grade = "";
            var del = [];
           
            var admdetails = [],remark=[];
            $scope.filesize = true;
            $scope.edt ={};
            
            
            $scope.obj1 = [], $scope.obj3 = [];

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

          
             // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {
               
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;

                $scope.propertyName = propertyName;
            };

            $scope.countData = [

                  { val: 5, data: 5 },
                  { val: 10, data: 10 },
                  { val: 15, data: 15 },

            ]

            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.obj2 = res.data;
                $scope.edt['curr_code'] = $scope.obj2[0].sims_attendance_cur_code;
                $scope.curr_code = $scope.obj2[0].sims_attendance_cur_code;
                $scope.getCur($scope.edt.curr_code);
            });

            $scope.getCur = function (cur_code)
            {
                $http.get(ENV.apiUrl + "api/common/getAllAcademicYearAss?cur_code=" + cur_code).then(function (res) {
                    $scope.obj1 = res.data;
                    console.log($scope.obj1[0].sims_academic_year);
                    $scope.edt['academic_year'] = $scope.obj1[0].sims_academic_year;
                    $scope.acad_yr = $scope.obj1[0].sims_academic_year;
                    $scope.GetGrade(cur_code, $scope.acad_yr);
                });
            }

            $scope.GetGrade = function (cur, acad_yr)
            {
                if (cur != "" && acad_yr != "")
                {
                    $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + cur + "&academic_year=" + acad_yr).then(function (res) {
                        $scope.obj3 = res.data;
                        // $scope.edt['grade_code'] = $scope.obj3[0].sims_grade_code;
                        // $scope.grade = $scope.obj3[0].sims_grade_code;
                        setTimeout(function () {
                            $('#cmb_grade').change(function () {
                                //  console.log($(this).val());
                                grade = $(this).val();
                            }).multipleSelect({
                                width: '100%',
                            });
                        }, 1000);
                    });
                }
            }

            $scope.getSection = function (cur,acad_yr, grade)
            {
                 if (cur != "" && acad_yr  !="" && grade != "")
                    {
                     $http.get(ENV.apiUrl + "api/StudentAssignRemark/Get_Section_CodebyCuriculum?cur_code=" + cur + "&acad_yr=" + acad_yr + "&grade_sec=" + grade).then(function (res) {
                            $scope.section = res.data;
                        });
                    }
            }

            $scope.getAssignRemarkData = function ()
            {
                $scope.filteredTodos = [];
                $http.get(ENV.apiUrl + "api/StudentAssignRemark/GetStudentAssignRemark?cur_code=" + $scope.edt.curr_code + "&acad_yr=" + $scope.edt.academic_year + "&grade_sec=" + $scope.edt.grade_section_code).then(function (res) {
                        $scope.assessmentscheduleData = res.data;
                        if ($scope.assessmentscheduleData.length > 0) {
                            $scope.table = true;
                            $scope.pager = true;
                            if ($scope.countData.length > 3) {
                                $scope.countData.splice(3, 1);
                                $scope.countData.push({ val: $scope.assessmentscheduleData.length, data: 'All' })
                            }
                            else
                            {
                                $scope.countData.push({ val: $scope.assessmentscheduleData.length, data: 'All' })
                            }
                            $scope.totalItems = $scope.assessmentscheduleData.length;
                            $scope.todos = $scope.assessmentscheduleData;
                            $scope.grid = true;
                            $scope.makeTodos();
                        }
                        else {
                            $scope.table = false;
                            swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                            $scope.filteredTodos = [];
                        }

                    });
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.check1 = function (dash)
            {
                var v = document.getElementById(dash.sims_enroll_no);
                
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }

                });
                // }

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.check = function () {
                main = document.getElementById('mainchk');
                del = [];
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_enroll_no);
                        v.checked = true;
                        del.push($scope.filteredTodos[i].sims_enroll_no);
                        $('tr').addClass("row_selected");
                        // }
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sims_enroll_no;
                        var v = document.getElementById(t);
                        v.checked = false;
                        del.pop(t);
                        // $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }
            }

            $scope.cancel = function ()
            {
                $scope.filteredTodos = [];
                $scope.pager = false;
            }

            $scope.save = function ()
            {
                var data2 = [];
                var send_data2 = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++)
                {
                    if ($scope.filteredTodos[i].sims_enroll_no1 == true)
                    {

                        var data = ({
                            sims_cur_code: $scope.edt.curr_code,
                            sims_acad_yr: $scope.edt.academic_year,
                            sims_grade_code: $scope.edt.grade_code,
                            grade_section_code: $scope.edt.grade_section_code,
                            sims_student_remark: $scope.filteredTodos[i].sims_student_remark,
                            sims_enroll_no: $scope.filteredTodos[i].sims_enroll_no,
                            opr: 'U'
                        });

                        data2.push(data);
                        $scope.flag=true;
                    }
                }

                if ($scope.flag == true)
                {
                    $http.post(ENV.apiUrl + "api/StudentAssignRemark/CUDInsertAssignRemarkDetails", data2).then(function (res) {
                        $scope.msg1 = res.data;
                        // console.log($scope.msg1);

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "RF ID Updated", showCloseButton: true, width: 380, });
                        }
                        $scope.getAssignRemarkData();
                    });
                }
            }

         }])

})();