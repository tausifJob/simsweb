﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Student');
    simsController.controller('MarkEntryViewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.display = false;
            $scope.table1 = true;
            $scope.fee_no = true;
            $scope.table = true;
            $scope.visit_no = true;
            var check_no = null;
            $scope.chku = false;
            $scope.edt = [];

            var user = $rootScope.globals.currentUser.username;

            //CUR
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.temp = {
                    'sims_cur_code': $scope.curriculum[0].sims_cur_code
                }
                $scope.getAccYear($scope.temp.sims_cur_code)
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    debugger;
                    $scope.temp = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                        'sims_academic_year': $scope.Acc_year[0].sims_academic_year
                    }

                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year)
                  
                });

            }

            //
            //Grade
            $scope.getGrade = function (curCode, accYear) {
                debugger
                $http.get(ENV.apiUrl + "api/StudentReport/getgradesectionComboMarkEntry?curCode=" + $scope.temp.sims_cur_code + "&accYear=" + $scope.temp.sims_academic_year + "&user=" + user).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
              //  $scope.getSection($scope.temp.sims_cur_code, $scope.temp.sims_grade_code, $scope.temp.sims_academic_year)
            }

            //Section
            $scope.getSection = function (curCode, accYear,grade_code) {
                debugger
                $http.get(ENV.apiUrl + "api/StudentReport/getsectionComboMarkEntry?curCode=" + curCode + "&accYear=" + accYear + "&grade_code=" + grade_code + "&user=" + user).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }

            $http.get(ENV.apiUrl + "api/common/getAllTerms").then(function (term) {
                debugger;
                $scope.getterm = term.data;
            });

            $scope.getSubject = function (curCode, accYear, grade_code,term,section) {
                debugger
                $http.get(ENV.apiUrl + "api/StudentReport/getsubjectComboMarkEntry?curCode=" + curCode + "&accYear=" + accYear + "&grade_code=" + grade_code + "&term=" + term + "&section=" + section).then(function (Subjectcode) {
                    $scope.Subject_code = Subjectcode.data;
                });
            }


            $scope.getCategory = function (curCode, accYear, grade_code, section, gbnumber) {
                debugger
                $http.get(ENV.apiUrl + "api/StudentReport/getCategoryComboMarkEntry?curCode=" + curCode + "&accYear=" + accYear + "&grade_code=" + grade_code + "&section=" + section + "&gbnumber=" + gbnumber).then(function (Categorycode) {
                    $scope.Category_code = Categorycode.data;
                });
            }


            $scope.Submit = function () {
                debugger
               // var f = str.req_no;
                $scope.t = 'GBR002SISQATAR';

                var data = {
                    location: $scope.t,
                    parameter: {
                             cur_details:$scope.temp.sims_cur_code,
                             acad_year:$scope.temp.sims_academic_year,
                             grade_details:$scope.temp.sims_grade_code,
                             section_details:$scope.temp.sims_section_code,
                             term_details:$scope.temp.sims_term_code,
                             subject_details:$scope.temp.sims_gb_number,
                             category_details: $scope.temp.sims_gb_number+$scope.temp.sims_gb_cat_code,
                             page_setting:'A4'
                    },
                    state: 'main.Sim766'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

         }])
})();
