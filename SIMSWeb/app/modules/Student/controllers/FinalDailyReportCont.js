﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('FinalDailyReportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.temp = {};
            $scope.datasend1 = [];
            $scope.pagesize = "15";
            $scope.sims_freq = "0";
            $scope.pageindex = "1";

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.temp = {};
            $scope.display = true;
            $scope.add_btn = false;
            $scope.any_incidence = true;
            //$scope.save_btn = true;
            var dataforSave = [];
            var user = $rootScope.globals.currentUser.username;
            $scope.edt = {};

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 15, $scope.maxSize = 15;


            $(function () {
                $('#grade_box').multipleSelect({ width: '100%' });
                $('#section_box').multipleSelect({ width: '100%' });

            });

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.edt.mom_start_date = dd + '-' + mm + '-' + yyyy;
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;



            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#chkmarks1").attr("disabled", "disabled");
                        $scope.any_incidence = 'Yes';

                    } else {

                        $("#chkmarks1").removeAttr("disabled");
                        $("#chkmarks1").focus();
                        $scope.any_incidence = 'No';
                    }
                });
            });



            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_end_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });

            $("#from_date").kendoDatePicker({
                format: "dd-MM-yyyy",
                min: new Date(1900, 1, 1),
                max: new Date()
            });


            var today = new Date();

            today = ('0' + today.getDate()).slice(-2) + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + today.getFullYear();
            $scope.mom_start_date = today;

            $http.get(ENV.apiUrl + "api/DailyReport/getCuriculum").then(function (res1) {
                $scope.sims_cur = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_cur_code'] = res1.data[0].sims_cur_code;
                    $scope.getAccYear(res1.data[0].sims_cur_code);
                }
            });

            $scope.getAccYear = function (cur_code) {
                debugger
                $http.get(ENV.apiUrl + "api/DailyReport/getAcademicYear?curCode=" + cur_code).then(function (res1) {
                    $scope.academic_year = res1.data;
                    if (res1.data.length > 0) {
                        $scope.temp['sims_academic_year'] = res1.data[0].sims_academic_year;
                        $scope.getAllGrades(cur_code, res1.data[0].sims_academic_year)

                    }
                });

            }

            $scope.getAllGrades = function (cur_code, academic_year) {
                debugger
                $http.get(ENV.apiUrl + "api/DailyReport/getAllGradesNew?cur_code=" + cur_code + "&academic_year=" + academic_year + "&user=" + user).then(function (res1) {
                    $scope.grade_code = res1.data;
                    setTimeout(function () {
                        $('#grade_box').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }

            $scope.getSectionFromGrade = function (cur_code, academic_year, grade_code) {
                debugger;
                $http.get(ENV.apiUrl + "api/DailyReport/getSectionFromGradeNew?cur_code=" + cur_code + "&academic_year=" + academic_year + "&grade_code=" + grade_code + "&user=" + user).then(function (res1) {
                    $scope.section_code = res1.data;

                    setTimeout(function () {
                        $('#section_box').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }

            $scope.daily_report = function () {
                debugger
                $http.get(ENV.apiUrl + "api/DailyReport/getFinalDailyReport?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&date=" + $scope.mom_start_date).then(function (res1) {

                    if (res1.data.length > 0) {
                        debugger;
                        $scope.report_data = res1.data;
                        $scope.pager = true;
                        $scope.display = true;
                        $scope.add_btn = false;
                        $scope.totalItems = $scope.report_data.length;
                        $scope.todos = $scope.report_data;
                        $scope.makeTodos();

                        if (parseInt($scope.report_data[0].flag) > 0) {
                            debugger;
                            $scope.edt.view_button == true;
                        }
                        else {
                            $scope.edt.view_button == false;

                        }

                    }
                    else {
                        swal({ title: "Alert", text: " Data is not Available for selected date", showCloseButton: true, width: 300, height: 200 });
                        $scope.pager = false;
                        $scope.filteredTodos = [];
                    }
                    //angular.forEach($scope.report_data, function (f) {
                    //    f.sims_bell_lecture_per_week = parseFloat(f.sims_bell_lecture_per_week);
                    //});
                    //$scope.filteredTodos = [];
                    console.log($scope.report_data);
                });
            }

            $http.get(ENV.apiUrl + "api/DailyReport/getFeeling").then(function (res1) {
                $scope.sims_feelings = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['feeling_code'] = res1.data[0].feeling_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport/getActivity").then(function (res1) {
                $scope.sims_activity = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['activity_code'] = res1.data[0].activity_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport/getFrequency").then(function (res1) {
                debugger;
                $scope.sims_freq = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['frequency_code'] = res1.data[0].frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport/getQuantity").then(function (res1) {
                debugger;
                $scope.sims_qty = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['quantity_code'] = res1.data[0].quantity_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport/getPlzProvide").then(function (res1) {
                debugger;
                $scope.sims_plz_prov = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['please_prov_code'] = res1.data[0].please_prov_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport/getPlzProvideQty").then(function (res1) {
                debugger;
                $scope.sims_plz_prov_qty = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['please_prov_qty_code'] = res1.data[0].please_prov_qty_code;

                }
            });

            $scope.checkNappyChange = function (str, type) {
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_student_nappy_changing = "";
                }
                else {
                    $scope.edt.sims_student_nappy_changing = "Nappy Change";
                }

                if (type == "Dry") {
                    $scope.edt.sims_student_nappy_changing_dry = "Dry";

                }
                else {
                    $scope.edt.sims_student_nappy_changing_dry = null;
                }

            }

            $scope.checkNappyChange1 = function (str, type) {
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_student_nappy_changing = "";
                }
                else {
                    $scope.edt.sims_student_nappy_changing = "Nappy Change";
                }

                if (type == "Wet") {
                    $scope.edt.sims_student_nappy_changing_wet = "Wet";

                }
                else {
                    $scope.edt.sims_student_nappy_changing_wet = null;
                }

            }

            $scope.checkNappyChange2 = function (str, type) {
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_student_nappy_changing = null;
                }
                else {
                    $scope.edt.sims_student_nappy_changing = "Nappy Change";
                }

                if (type == "BM") {
                    $scope.edt.sims_student_nappy_changing_bm = "BM";
                }
                else {
                    $scope.edt.sims_student_nappy_changing_bm = null;
                }
            }

            $scope.checkTiletTraining = function (str, type) {
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_student_toilet_training = null;
                }
                else {
                    $scope.edt.sims_student_toilet_training = "Toilet Training";
                }

                if (type == "Number1") {
                    $scope.edt.sims_student_toilet_training_Number1 = "Number1";

                }
                else {
                    $scope.edt.sims_student_toilet_training_Number1 = null;
                }

            }

            $scope.checkTiletTraining1 = function (str, type) {
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_student_toilet_training = null;
                }
                else {
                    $scope.edt.sims_student_toilet_training = "Toilet Training";
                }


                if (type == "Number2") {
                    $scope.edt.sims_student_toilet_training_Number2 = "Number2";
                }
                else {
                    $scope.edt.sims_student_toilet_training_Number2 = null;
                }

            }

            $scope.checkTiletTraining2 = function (str, type) {
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_student_toilet_training = null;
                }
                else {
                    $scope.edt.sims_student_toilet_training = "Toilet Training";
                }

                if (type == "Number3") {
                    $scope.edt.sims_student_toilet_training_Askedtogo = "Asked to go";
                }
                else {
                    $scope.edt.sims_student_toilet_training_Askedtogo = null;
                }
            }

            $scope.checkFluidIntake = function (str, type) {
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_student_fluid_intake = "";
                }
                else {
                    $scope.edt.sims_student_fluid_intake = "Fluid Intake";
                }

                if (type == "Milk") {
                    $scope.edt.sims_student_fluid_intake_Milk = "Milk";

                }
                else {
                    $scope.edt.sims_student_fluid_intake_Milk = null;
                }

            }

            $scope.checkFluidIntake1 = function (str, type) {
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_student_fluid_intake = "";
                }
                else {
                    $scope.edt.sims_student_fluid_intake = "Fluid Intake";
                }

                if (type == "Water") {
                    $scope.edt.sims_student_fluid_intake_Water = "Water";
                }
                else {
                    $scope.edt.sims_student_fluid_intake_Water = null;
                }

            }

            $scope.checkFood = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_student_food = null;
                }
                else {
                    $scope.edt.sims_student_food = "Food";
                }

                if (type == "Morningsnack") {
                    $scope.edt.sims_student_food_Morning_snack = "Morning Snack";

                }
                else {
                    $scope.edt.sims_student_food_Morning_snack = null;
                }


            }

            $scope.checkFood1 = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_student_food = null;
                }
                else {
                    $scope.edt.sims_student_food = "Food";
                }


                if (type == "Lunch") {
                    $scope.edt.sims_student_food_Lunch = "Lunch";
                }
                else {
                    $scope.edt.sims_student_food_Lunch = null;
                }


            }

            $scope.checkFood2 = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_student_food = null;
                }
                else {
                    $scope.edt.sims_student_food = "Food";
                }

                if (type == "Afternoonsnac") {
                    $scope.edt.sims_student_food_Afternoon_snack = "Afternoon Snack";
                }
                else {
                    $scope.edt.sims_student_food_Afternoon_snack = null;
                }

            }

            $scope.provide_type = function (str,type) {
                debugger;
                

                if (type == "Diapers") {
                    $scope.edt.sims_student_please_provide_diaper = "Diapers/Pull-Ups";
                }
                else {
                    $scope.edt.sims_student_please_provide_diaper = null;
                }

            }

            $scope.provide_type1 = function (str,type) {
                debugger;
                

                if (type == "Wetwipes") {
                    $scope.edt.sims_student_please_provide_Wet_wipes = "Wet Wipes";
                }
                else {
                    $scope.edt.sims_student_please_provide_Wet_wipes = null;
                }

            }

            $scope.provide_type2 = function (str, type) {
                debugger;


                if (type == "Formula") {
                    $scope.edt.sims_student_please_provide_Wet_Formula = "Formula";
                }
                else {
                    $scope.edt.sims_student_please_provide_Wet_Formula = null;
                }

            }

            $scope.provide_type3 = function (str, type) {
                debugger;


                if (type == "Clothing") {
                    $scope.edt.sims_student_please_provide_Wet_Clothing = "Clothing";
                }
                else {
                    $scope.edt.sims_student_please_provide_Wet_Clothing = null;
                }

            }

            $scope.provide_type4 = function (str, type) {
                debugger;


                if (type == "Others") {
                    $scope.edt.sims_student_please_provide_Wet_Others = "Others";
                }
                else {
                    $scope.edt.sims_student_please_provide_Wet_Others = null;
                }

            }




            var datasend = [];
            $scope.save_btn_function = function (Myform) {
                debugger
                console.log($scope.datasend1);
                if (Myform) {
                    var data = $scope.edt;
                    data.opr = 'I';

                    //$scope.datasend1.push($scope.data);
                    data.sims_academic_year = $scope.datasend1[0].sims_academic_year;
                    data.sims_enroll_number = $scope.datasend1[0].sims_enroll_number;
                    data.sims_cur_code = $scope.datasend1[0].sims_cur_code;
                    data.sims_grade_code = $scope.datasend1[0].sims_grade_code;
                    data.sims_section_code = $scope.datasend1[0].sims_section_code;
                    data.mom_start_date = $scope.mom_start_date;
                    data.sims_student_created_by = $rootScope.globals.currentUser.username;
                    //  data.sims_academic_year = $scope.str.sims_academic_year;
                    // data.sims_enroll_number = $scope.str.sims.sims_enroll_number;
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/DailyReport/CUDDetails", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.daily_report();
                            $scope.back_date ();
                        }
                        else {
                            swal({ text: "Record Not Inserted " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.back_date();
                        }

                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = true;
                    $scope.add_btn = false;
                    $update_btn = false;
                    $scope.edt = {};

                }

            }



            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.report_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            var data_send = [];
            var data1 = [];

            $scope.show_incidenc_remark = function () {
                debugger;
                if ($scope.edt.sims_student_any_incidence = 'Yes') {
                    $scope.any_incidence = true;
                }
                else {
                    $scope.any_incidence = false;
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }





            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            //$scope.colsvis = false;

            $scope.back_date = function () {
                debugger;
                $scope.display = true;
                $scope.add_btn = false;
                $scope.edt = {};
                $scope.datasend1 = [];

            }

            $scope.reset_data = function () {
                debugger;
                $scope.temp.sims_grade_code = '';

                $scope.pager = false;
                $scope.filteredTodos = [];


                

            }


            $scope.add_details = function (str) {

                debugger;

                $scope.edt["mom_start_date"] = $scope.sdate;
                //$scope.edt.mom_start_date = $scope.sdate;
                $scope.datasend1 = [];
                $scope.data = [];
               
                console.log(str);

                $scope.edt.sims_enroll_number = str.sims_enroll_number;
                $scope.edt.sims_grade_name_en = str.sims_grade_name_en;
                $scope.edt.sims_section_name_en = str.sims_section_name_en;
                $scope.edt.student_name = str.student_name;
                $scope.edt.parent_name = str.parent_name;
                $scope.edt.sims_student_img = str.sims_student_img;

                $scope.data = {
                    sims_academic_year: str.sims_academic_year,
                    sims_enroll_number: str.sims_enroll_number,
                    sims_cur_code: str.sims_cur_code,
                    sims_grade_code: str.sims_grade_code,
                    sims_section_code: str.sims_section_code

                }

                $scope.datasend1.push($scope.data);

                //$scope.temp = {};

                if (parseInt(str.flag) > 0) {
                    //update
                    

                    swal({
                        title: '',
                        text: "Details are Present!!!Do you want to update?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            $scope.display = false;
                            $scope.add_btn = true;
                            //update
                            $scope.update_btn = true;
                            $scope.save_btn = false;

                            $http.get(ENV.apiUrl + "api/DailyReport/geteditedData?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grade_code=" + str.sims_grade_code + "&section_code=" + str.sims_section_code + "&enroll_number=" + str.sims_enroll_number + "&date=" + $scope.mom_start_date).then(function (res1) {
                                debugger;
                                if (res1.data.length > 0) {
                                    
                                    var temp_report_data = res1.data;
                                    console.log(temp_report_data);
                                    $scope.edt.sims_student_feeling = temp_report_data[0].sims_student_feeling;
                                    $scope.edt.sims_student_activity = temp_report_data[0].sims_student_activity;
                                    $scope.edt.sims_student_nappy_changing_dry_frequency = temp_report_data[0].sims_student_nappy_changing_dry_frequency;
                                    $scope.edt.sims_student_nappy_changing_dry_Remark = temp_report_data[0].sims_student_nappy_changing_dry_Remark;
                                    $scope.edt.sims_student_nappy_changing_wet = temp_report_data[0].sims_student_nappy_changing_wet,
                                    $scope.edt.sims_student_nappy_changing_wet_frequency = temp_report_data[0].sims_student_nappy_changing_wet_frequency;
                                    $scope.edt.sims_student_nappy_changing_wet_Remark = temp_report_data[0].sims_student_nappy_changing_wet_Remark;
                                    $scope.edt.sims_student_nappy_changing_bm = temp_report_data[0].sims_student_nappy_changing_bm;
                                    $scope.edt.sims_student_nappy_changing_bm_frequency = temp_report_data[0].sims_student_nappy_changing_bm_frequency;
                                    $scope.edt.sims_student_nappy_changing_bm_Remark = temp_report_data[0].sims_student_nappy_changing_bm_Remark;
                                    $scope.edt.sims_student_toilet_training = temp_report_data[0].sims_student_toilet_training;
                                    $scope.edt.sims_student_toilet_training_Number1 = temp_report_data[0].sims_student_toilet_training_Number1;
                                    $scope.edt.sims_student_toilet_training_Number1_frequency = temp_report_data[0].sims_student_toilet_training_Number1_frequency;
                                    $scope.edt.sims_student_toilet_training_Number1_Remark = temp_report_data[0].sims_student_toilet_training_Number1_Remark;
                                    $scope.edt.sims_student_toilet_training_Number2 = temp_report_data[0].sims_student_toilet_training_Number2;
                                    $scope.edt.sims_student_toilet_training_Number2_frequency = temp_report_data[0].sims_student_toilet_training_Number2_frequency;
                                    $scope.edt.sims_student_toilet_training_Number2_Remark = temp_report_data[0].sims_student_toilet_training_Number2_Remark;
                                    $scope.edt.sims_student_toilet_training_Askedtogo = temp_report_data[0].sims_student_toilet_training_Askedtogo;
                                    $scope.edt.sims_student_toilet_training_Askedtogo_frequency = temp_report_data[0].sims_student_toilet_training_Askedtogo_frequency;
                                    $scope.edt.sims_student_toilet_training_Askedtogo_Remark = temp_report_data[0].sims_student_toilet_training_Askedtogo_Remark;
                                    $scope.edt.sims_student_fluid_intake = temp_report_data[0].sims_student_fluid_intake;
                                    $scope.edt.sims_student_fluid_intake_Milk = temp_report_data[0].sims_student_fluid_intake_Milk;
                                    $scope.edt.sims_student_fluid_intake_Milk_qty = temp_report_data[0].sims_student_fluid_intake_Milk_qty;
                                    $scope.edt.sims_student_fluid_intake_Milk_frequency = temp_report_data[0].sims_student_fluid_intake_Milk_frequency;
                                    $scope.edt.sims_student_fluid_intake_Milk_remark = temp_report_data[0].sims_student_fluid_intake_Milk_remark;
                                    $scope.edt.sims_student_fluid_intake_Water = temp_report_data[0].sims_student_fluid_intake_Water;
                                    $scope.edt.sims_student_fluid_intake_Water_qty = temp_report_data[0].sims_student_fluid_intake_Water_qty;
                                    $scope.edt.sims_student_fluid_intake_water_frequency = temp_report_data[0].sims_student_fluid_intake_water_frequency;
                                    $scope.edt.sims_student_fluid_intake_Water_remark = temp_report_data[0].sims_student_fluid_intake_Water_remark;
                                    $scope.edt.sims_student_food = temp_report_data[0].sims_student_food;
                                    $scope.edt.sims_student_food_Morning_snack = temp_report_data[0].sims_student_food_Morning_snack;
                                    $scope.edt.sims_student_food_Morning_snack_quantity = temp_report_data[0].sims_student_food_Morning_snack_quantity;
                                    $scope.edt.sims_student_food_Morning_snack_remark = temp_report_data[0].sims_student_food_Morning_snack_remark;
                                    $scope.edt.sims_student_food_Lunch = temp_report_data[0].sims_student_food_Lunch;
                                    $scope.edt.sims_student_food_Lunch_quantity = temp_report_data[0].sims_student_food_Lunch_quantity;
                                    $scope.edt.sims_student_food_Lunch_remark = temp_report_data[0].sims_student_food_Lunch_remark;
                                    $scope.edt.sims_student_food_Afternoon_snack = temp_report_data[0].sims_student_food_Afternoon_snack;
                                    $scope.edt.sims_student_food_Afternoon_snack_quantity = temp_report_data[0].sims_student_food_Afternoon_snack_quantity;
                                    $scope.edt.sims_student_food_Afternoon_snack_remark = temp_report_data[0].sims_student_food_Afternoon_snack_remark;
                                    $scope.edt.sims_student_slep_time_am = temp_report_data[0].sims_student_slep_time_am;
                                    $scope.edt.sims_student_slep_time_pm = temp_report_data[0].sims_student_slep_time_pm;
                                    $scope.edt.sims_student_any_incidence = temp_report_data[0].sims_student_any_incidence;
                                    $scope.edt.sims_student_incidence_remark = temp_report_data[0].sims_student_incidence_remark;
                                    $scope.edt.sims_student_provide_type = temp_report_data[0].sims_student_provide_type;
                                    $scope.edt.sims_student_provide_quantity = temp_report_data[0].sims_student_provide_quantity;
                                    $scope.edt.sims_student_teacher_comment = temp_report_data[0].sims_student_teacher_comment;
                                    $scope.edt.sims_student_parent_comment = temp_report_data[0].sims_student_parent_comment;
                                    $scope.edt.sims_student_please_provide_diaper = temp_report_data[0].sims_student_please_provide_diaper;
                                    $scope.edt.sims_student_please_provide_diaper_qty = temp_report_data[0].sims_student_please_provide_diaper_qty;
                                    $scope.edt.sims_student_please_provide_diaper_remark = temp_report_data[0].sims_student_please_provide_diaper_remark;
                                    $scope.edt.sims_student_please_provide_Wet_wipes = temp_report_data[0].sims_student_please_provide_Wet_wipes;
                                    $scope.edt.sims_student_please_provide_Wet_wipes_qty = temp_report_data[0].sims_student_please_provide_Wet_wipes_qty;
                                    $scope.edt.sims_student_please_provide_Wet_wipes_remark = temp_report_data[0].sims_student_please_provide_Wet_wipes_remark;
                                    $scope.edt.sims_student_please_provide_Wet_Formula = temp_report_data[0].sims_student_please_provide_Wet_Formula;
                                    $scope.edt.sims_student_please_provide_Wet_Formula_qty = temp_report_data[0].sims_student_please_provide_Wet_Formula_qty;
                                    $scope.edt.sims_student_please_provide_Wet_Formula_remark = temp_report_data[0].sims_student_please_provide_Wet_Formula_remark;
                                    $scope.edt.sims_student_please_provide_Wet_Clothing = temp_report_data[0].sims_student_please_provide_Wet_Clothing;
                                    $scope.edt.sims_student_please_provide_Wet_Clothing_qty = temp_report_data[0].sims_student_please_provide_Wet_Clothing_qty;
                                    $scope.edt.sims_student_please_provide_Wet_Clothing_remark = temp_report_data[0].sims_student_please_provide_Wet_Clothing_remark;
                                    $scope.edt.sims_student_please_provide_Wet_Others = temp_report_data[0].sims_student_please_provide_Wet_Others;
                                    $scope.edt.sims_student_please_provide_Wet_Others_qty = temp_report_data[0].sims_student_please_provide_Wet_Others_qty;
                                    $scope.edt.sims_student_please_provide_Wet_Others_remark = temp_report_data[0].sims_student_please_provide_Wet_Others_remark;

                                }
                                console.log(temp_report_data);
                            });


                        }
                    })

                    
                }
                else {
                    //insert
                    $scope.display = false;
                    $scope.add_btn = true;
                    $scope.update_btn = false;
                    $scope.save_btn = true;
                }

            }

            
            $scope.getReport = function (str) {
                debugger
                console.log(str);
                var t = moment($scope.mom_start_date, 'DD-MM-YYYY').format('YYYY-MM-DD')

                var data = {

                    location: 'Sims.SMR150LWG',
                    parameter: {
                        cur_code: str.sims_cur_code,
                        acad_year: str.sims_academic_year,
                        grade_code: str.sims_grade_code,
                        section_code: str.sims_section_code,
                        enroll_number: str.sims_enroll_number,
                        date :t,

                    },
                    state: 'main.Dfr001',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                console.log("data", data);
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter');
            }

            //$scope.update_function = function (str) {
            //    debugger;
            //    var data = str;
            //    data.opr = "U";
            //    dataforUpdate.push(data);
            //    $http.post(ENV.apiUrl + "api/DailyReport/CUDDetails", dataforUpdate).then(function (msg) {
            //        $scope.msg1 = msg.data;

            //        if ($scope.msg1 == true) {
            //            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
            //        }
            //        else {
            //            swal({ text: "Not Updated " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
            //        }

            //    });
            //}

            var dataforUpdate = [];

            $scope.update = function () {
                debugger
                var data = $scope.edt;
                data.sims_academic_year = $scope.datasend1[0].sims_academic_year;
                data.sims_enroll_number = $scope.datasend1[0].sims_enroll_number;
                data.sims_cur_code = $scope.datasend1[0].sims_cur_code;
                data.sims_grade_code = $scope.datasend1[0].sims_grade_code;
                data.sims_section_code = $scope.datasend1[0].sims_section_code;
                data.mom_start_date = $scope.mom_start_date;
                data.opr = "U";
                dataforUpdate.push(data);
                //dataupdate.push(data);
                $http.post(ENV.apiUrl + "api/DailyReport/CUDDetails", dataforUpdate).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        $scope.daily_report();
                        $scope.back_date();
                    }
                    else {
                        swal({ text: "Not Updated " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        $scope.daily_report();
                        $scope.back_date();
                    }

                    $scope.maindata();
                });
                dataforUpdate = [];
                $scope.display = false;
                $scope.add_btn = true;


            }



            $(document).ready(function () {
                $("#sleep_time_am").kendoDateTimePicker({
                    format: "hh:mm tt"
                });
            });
            $('.clockpicker').clockpicker();


        }])

})();

