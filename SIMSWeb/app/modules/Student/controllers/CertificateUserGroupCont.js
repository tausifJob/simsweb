﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CertificateUserGroupCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            
            //$scope.cmbstatus = true;
            //$scope.checked = false;


            //$timeout(function () {
            //    $("#fixTable").tableHeadFixer({ 'top': 1 });
            //}, 100);


            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/CertificateUserGroup/getCertificateUserGroup").then(function (getCertificateUserGroup_Data) {


                $scope.getCertificateUserGroup = getCertificateUserGroup_Data.data;
                $scope.totalItems = $scope.getCertificateUserGroup.length;
                $scope.todos = $scope.getCertificateUserGroup;
                $scope.makeTodos();
                // $scope.makeTodos();

            });


            // Bind Value to the DropDown Curriculum Name 



            $http.get(ENV.apiUrl + "api/CertificateUserGroup/getCurriculumName").then(function (getCurriculumName_Data) {
                $scope.temp = {};
                $scope.getCurriculumName = getCurriculumName_Data.data;
                if (getCurriculumName_Data.data.length > 0) {
                    $scope.temp.sims_cur_code = getCurriculumName_Data.data[0].sims_cur_code;
                }
                console.log($scope.getCurriculumName);
            });

            // Bind Value to the DropDown Certificate Name 

            $http.get(ENV.apiUrl + "api/CertificateUserGroup/getCertificateName").then(function (getCertificateName_Data) {

                $scope.getCertificateName = getCertificateName_Data.data;
                console.log($scope.getCertificateName);
            });

            // Bind Value to the DropDown User Group Name

            $http.get(ENV.apiUrl + "api/CertificateUserGroup/getUserGroupName").then(function (getUserGroupName_Data) {

                $scope.getUserGroupName = getUserGroupName_Data.data;
                console.log($scope.getUserGroupName);
            });



            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);

                });
            };

            //Search
            $scope.search = function () {

                $scope.todos = $scope.searched($scope.getCertificateUserGroup, $scope.searchText);   // DeliveryMode bind here
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.getCertificateUserGroup;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                debugger;
                return (item.sims_cur_full_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_certificate_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.comn_user_group_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_user_group_enable == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
                debugger;
                $scope.disabled = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.divcode_readonly = false;
                $scope.temp = {};
                $scope.temp.sims_user_group_enable = true;
                $scope.Curname = false;
                $scope.certifNm = false;
                $scope.userGrpNm = false;
                if ($scope.getCurriculumName.length > 0) {
                    $scope.temp.sims_cur_code = $scope.getCurriculumName[0].sims_cur_code;
                }
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

              //  $scope.temp = "";

            }

            //DATA CANCEL
            $scope.Cancel = function () {
                debugger;
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();


            }

            //DATA EDIT
            $scope.edit = function (str) {

                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.divcode_readonly = true;
                $scope.Curname = true;
                $scope.certifNm = true;
                $scope.userGrpNm = true;

                $scope.temp = str;

            }

            //DATA SAVE INSERT

            $scope.savedata = function (Myform) {
                
                debugger;

                var datasend = [];
                if (Myform) {
                    var data = $scope.temp;
                    data.opr = 'I';
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/CertificateUserGroup/CUDCertificateUserGroup", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 380, height: 200 });

                            $http.get(ENV.apiUrl + "api/CertificateUserGroup/getCertificateUserGroup").then(function (getCertificateUserGroup_Data) {

                                $scope.getCertificateUserGroup = getCertificateUserGroup_Data.data;
                                $scope.totalItems = $scope.getCertificateUserGroup.length;
                                $scope.todos = $scope.getCertificateUserGroup;
                                $scope.makeTodos();
                                // $scope.makeTodos();
                            });

                        }
                        else {
                            swal({ text: "This value is allready present. " + $scope.msg1, imageUrl: "assets/img/notification-alert.png", width: 380, height: 200 });
                        }

                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                if (Myform) {

                    debugger;
                    var data = $scope.temp;
                    data.opr = "U";
                    dataforUpdate.push(data);

                    $http.post(ENV.apiUrl + "api/CertificateUserGroup/CUDCertificateUserGroup", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {

                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });

                            $http.get(ENV.apiUrl + "api/CertificateUserGroup/getCertificateUserGroup").then(function (getCertificateUserGroup_Data) {

                                $scope.getCertificateUserGroup = getCertificateUserGroup_Data.data;
                                $scope.totalItems = $scope.getCertificateUserGroup.length;
                                $scope.todos = $scope.getCertificateUserGroup;
                                $scope.makeTodos();
                                // $scope.makeTodos();

                            });

                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                    });
                    dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('test-' + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('test-' + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {



                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById('test-' + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({

                            'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
                            'sims_certificate_number': $scope.filteredTodos[i].sims_certificate_number,
                            'sims_user_group_code': $scope.filteredTodos[i].sims_user_group_code,

                            opr: 'D'

                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/CertificateUserGroup/CUDCertificateUserGroup", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {

                                            $http.get(ENV.apiUrl + "api/CertificateUserGroup/getCertificateUserGroup").then(function (getCertificateUserGroup_Data) {

                                                debugger;
                                                $scope.getCertificateUserGroup = getCertificateUserGroup_Data.data;
                                                $scope.totalItems = $scope.getCertificateUserGroup.length;
                                                $scope.todos = $scope.getCertificateUserGroup;
                                                $scope.makeTodos();
                                                // $scope.makeTodos();

                                            });


                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }

                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {


                                            $http.get(ENV.apiUrl + "api/CertificateUserGroup/getCertificateUserGroup").then(function (getCertificateUserGroup_Data) {

                                                debugger;
                                                $scope.getCertificateUserGroup = getCertificateUserGroup_Data.data;
                                                $scope.totalItems = $scope.getCertificateUserGroup.length;
                                                $scope.todos = $scope.getCertificateUserGroup;
                                                $scope.makeTodos();
                                                // $scope.makeTodos();

                                            });


                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            //main.checked = false;
                                            // $('tr').removeClass("row_selected");
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById('test-' + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                    //  $scope.row1 = '';

                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.getCertificateUserGroup;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();

                main.checked = false;
                $scope.row1 = '';
                // $scope.CheckAllChecked();

            }


         }])

})();
