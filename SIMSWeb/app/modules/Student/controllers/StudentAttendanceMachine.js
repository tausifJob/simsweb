﻿(function () {
    'use strict';
    var subject_list = [];
    var section_list = [];
    var main;
    var grade_code, SectionSubject5 = [], subject_code = [], subject_code1 = [];

    var cur_code;
    var section_code, sectioncodemodal1;
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudentAttendanceMachineCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.display = false;
            $scope.grid = true
            $scope.show = false;
            $scope.table = false;
           // $scope.btn_submit = false;
            $scope.edt = {};
           
            $scope.getAccYear = function (curCode) {

                $scope.edt.sims_grade_code = '';
                $scope.edt.sims_section_code = '';

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.edt.sims_cur_code).then(function (Acyear) {
                    
                    $scope.getAcademicYear = Acyear.data;

                    $scope.edt['sims_academic_year'] = $scope.getAcademicYear[0].sims_academic_year;
                    $scope.getgrades($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);

                });
            }

            //$scope.getacyr = function (str) {
               

            //    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
            //        $scope.getAcademicYear = Academicyear.data;
            //        $scope.getsections(str, $scope.getAcademicYear[0].sims_academic_year)
                    
            //        $scope.getsection($scope.curriculum[0].edt.sims_grade_code);
            //    });
            //}

            $scope.getgrades = function (str, str1) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str + "&academic_year=" + str1).then(function (res) {
                    $scope.getAllGrades = res.data;
                    //$scope.edt = {
                    //    sims_cur_code: str,
                    //    sims_academic_year: str1,
                    //    //sims_grade_code: $scope.getAllGrades[0].sims_grade_code,
                    //};
                    //$scope.getsection(str, str1, $scope.getAllGrades[0].sims_grade_code);


                })
            };

            $scope.getsection = function (str, str1, str2) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + str + "&grade_code=" + str2 + "&academic_year=" + str1).then(function (Allsection) {
                    $scope.getSectionFromGrade = Allsection.data;
                })
            };


            $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                debugger;
                
                $scope.curriculum = res.data;
                $scope.edt["sims_cur_code"] = $scope.curriculum[0].sims_cur_code;
                $scope.getAccYear($scope.curriculum[0].sims_cur_code);
                
            });


             $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
             };

             //function getCur(flag, comp_code) {
             //    if (flag) {

             //        $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
             //            $scope.curriculum = res.data;
             //            $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
             //            $scope.getAccYear($scope.curriculum[0].sims_cur_code);
                         
                        
             //        });
             //    }
             //    else {

             //        $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
             //            $scope.curriculum = res.data;
             //            $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
             //            $scope.getAccYear($scope.curriculum[0].sims_cur_code);
             //            $scope.getsection($scope.curriculum[0].edt.sims_cur_code);
             //            $scope.getsection($scope.curriculum[0].edt.sims_academic_year);
             //            $scope.getsection($scope.curriculum[0].edt.sims_grade_code);

             //        });


             //    }

             //}



            //$scope.getSectionGradesWise = function () {
            //    $scope.table = false;
            //    $scope.SectionSubject = [];
                
            //    if ($scope.edt.sims_cur_code != "" && $scope.edt.sims_cur_code != undefined)
            //        $scope.msg1 = '';
            //    else {
            //        $scope.msg1 = 'Field is Required';
            //        return;
            //    }
            //    if ($scope.edt.sims_grade_code != "" && $scope.edt.sims_grade_code != undefined)
            //        $scope.msg3 = '';
            //    else {
            //        $scope.msg3 = 'Field is Required';
            //        return;
            //    }
            //    if ($scope.edt.sims_academic_year != "" && $scope.edt.sims_academic_year != undefined)
            //        $scope.msg2 = '';
            //    else {
            //        $scope.msg2 = 'Field is Required';
            //        return;
            //    }
            //    if ($scope.edt.sims_section_code != "" && $scope.edt.sims_section_code != undefined)
            //        $scope.msg4 = '';
            //    else {
            //        $scope.msg4 = 'Field is Required';
            //        return;
            //    }
                
            //    $http.get(ENV.apiUrl + "api/AllocationRollNo/getStudentName?curcode=" + $scope.edt.sims_cur_code + "&academicyear=" + $scope.edt.sims_academic_year + "&gradecode=" + $scope.edt.sims_grade_code + "&sectioncode=" + $scope.edt.sims_section_code)
            //        .then(function (allSectionSubject) {
            //            $scope.AllStudentData = allSectionSubject.data;
            //            console.log("$scope.AllStudentData value", $scope.AllStudentData);
            //            $($scope.AllStudentData).each(function (k,v) {
            //                v.sims_roll_number = v.sims_roll_number == undefined ? '' : v.sims_roll_number;
            //            });
            //            $scope.table = true;
                      

            //        });
    
            //}

            $scope.checkNo = function (str, index) {
                debugger;
                if (str != '') {
                    for (var i = 0; i < $scope.AllStudentData.length; i++)
                        if (i != index) {
                            if ($scope.AllStudentData[i].sims_roll_number == str) {
                                swal({ text: 'Roll number is  already exists', width: 300, showCloseButton: true });
                                break;
                                str = '';
                                //$scope.$digest();
                                $scope.$apply();
                            }
                        }
                   

                }
            }

            //$scope.Update = function () {
            //    debugger;
            //    var datacoll = [];
            //    var collenroll = '';

            //    for (var i = 0; i < $scope.AllStudentData.length; i++) {
            //        if ($scope.AllStudentData[i].sims_roll_number != "" && $scope.AllStudentData[i].sims_roll_number != undefined)
                        
            //            collenroll = collenroll+$scope.AllStudentData[i].sims_student_enroll_number + '/' + $scope.AllStudentData[i].sims_roll_number+','
                      
            //    }

            //    if(collenroll.length>0)
            //          var data = {
            //                sims_cur_code: $scope.AllStudentData[0].sims_cur_code,
            //                sims_academic_year: $scope.AllStudentData[0].sims_academic_year,
            //                sims_grade_code: $scope.AllStudentData[0].sims_grade_code,
            //                sims_section_code: $scope.AllStudentData[0].sims_section_code,
            //                //sims_student_enroll_number: $scope.AllStudentData[i].sims_student_enroll_number,
            //                //sims_roll_number: $scope.AllStudentData[i].sims_roll_number,
            //                sims_enroll_roll: collenroll,

            //                //string: $scope.AllStudentData[i].string 
            //            }
            //        datacoll.push(data);
                

            //    $http.post(ENV.apiUrl + "api/AllocationRollNo/updateRollNo", datacoll).then(function (msg1) {
            //        $scope.msg = msg1.data;

            //        if ($scope.msg) {
            //            swal('', 'Record Updated Successfully.');
            //            $scope.reset_form();
            //        }
            //        //else {
            //        //    swal('','Roll Number Already Mapped/Data Not Updated');
                       
            //        //}


            //    });

            //}


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);

           
            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = 'All';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;


            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
            };

            
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.curriculum_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.curriculum_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].curriculum_code;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }

                }


            }
            

            $scope.Reset = function () {
                $scope.table = false;
                $scope.btn_submit = false;
                $scope.edt.sims_grade_code = '';
                $scope.edt.sims_section_code = '';


                //$scope.edt = {
                //    sims_grade_code: '',
                //    sims_section_name: ''
                //}
            }
            



        }])
})();