﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    
    simsController.controller('SurveyCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = true;
            $scope.table = true;
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            var user = $rootScope.globals.currentUser.username;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100)            

            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/Survey/get_allSurvey").then(function (res) {
                debugger;
                $scope.survey = res.data;                
            });
         
            function shuffle(array) {
                var currentIndex = array.length, temporaryValue, randomIndex;
             
                // While there remain elements to shuffle...
                while (0 !== currentIndex) {
                    debugger;
                    // Pick a remaining element...
                    randomIndex = Math.floor(Math.random() * currentIndex);
                    currentIndex -= 1;

                    // And swap it with the current element.
                    temporaryValue = array[currentIndex];
                    array[currentIndex] = array[randomIndex];
                    array[randomIndex] = temporaryValue;
                }
                return array;           
            }

            $http.get(ENV.apiUrl + "api/Survey/get_SurveyQuestion?string sims_survey_code=" + str.sims_survey_code).then(function (que) {
                debugger;
                $scope.questiondata = que.data;
                //shuffle($scope.questiondata);                
                console.log($scope.questiondata);

                setTimeout(function () {
                    debugger;
                    angular.forEach($scope.questiondata, function (value, key) {
                        angular.forEach(value.answers, function (val, k) {

                            var num = Math.random();
                            var imgSrc = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/Emojis/' + val.sims_survey_rating_img_path + "?v=" + num;
                            //document.getElementById(value.sims_student_enroll_number).setAttribute('src', imgSrc);
                            $("#" + value.sims_survey_question_code + k).attr('src', imgSrc);
                            console.log(imgSrc);
                       });
                        
                        console.log(value);
                       
                    });
                }, 500);
            });

            //$http.get(ENV.apiUrl + "api/Survey/get_SurveyAnswer").then(function (ans) {
            //    debugger;
            //    $scope.answerdata = ans.data;
            //});
        
            $scope.radioclick = function (str, str1, str2) {
                debugger;
                for (var j = 0; j < str.answers.length; j++) {                    
                    if (str.answers[j].sims_survey_question_answer_code == str1 && str.sims_survey_question_code==str2) {
                        str.answers[j].isChecked = true;
                    }
                    else {
                        str.answers[j].isChecked = false;
                    }
                }
            }
            
            $scope.radioclick1 = function (str, str1, str2) {
                debugger;
                for (var j = 0; j < str.answers.length; j++) {
                    if (str.answers[j].sims_survey_rating_code == str1 && str.sims_survey_question_code == str2) {
                        str.answers[j].isChecked = true;
                    }
                    else {
                        str.answers[j].isChecked = false;
                    }
                }
            }

            $scope.anscheck = function (ans, ans1, ans2, index) {
                debugger;
                main = document.getElementById('survey' + index + ans1);
                if (main.checked == true) {
                    for (var i = 0; i < ans.answers.length; i++) {
                        if (ans.answers[i].sims_survey_question_answer_code == ans1 && ans.sims_survey_question_code == ans2) {
                            ans.answers[i].isChecked = true;
                        }                       
                    }
                }               
            }

            //$scope.textans = function (text, text1, text2)
            //{
            //    var textjoin = [];
            //    for (var k = 0; k < text.answers.length; k++)
            //    {
            //        if (text.answers[k].sims_survey_question_answer_code == text1 && text.answers[k].sims_survey_user_response_answer_text == text2)
            //            textjoin = text1 + text2;
            //    }
            //}

            //DATA SAVE INSERT
            var datasend = [];
            var quest = [];
            var ans = [];
            var documentlist = [];
            $scope.savedata = function (Myform) {
                debugger;
                //var count1 = 0;
                //var count2 = 0;
                //for (var i = 0; i < $scope.questiondata.length; i++) {
                //    count1= count1 + 1;
                //   for (var j = 0; j < $scope.questiondata[i].answers.length; j++) {
                //       if ($scope.questiondata[i].answers[j].isChecked) {
                //           count2 = count2 + 1;                          
                //       }
                //   }
                //}
     
                //if (count1 != count2 || count2== 0) {
                    //swal('', 'Please answer all the questions');
                    //return;
                //}
                
                //else {
                 //   if (Myform) {
                        quest = [];
                        ans = [];
                        var join = [];
                        for (var i = 0; i < $scope.questiondata.length; i++)
                        {
                            for (var j = 0; j < $scope.questiondata[i].answers.length; j++) {                                
                                if ($scope.questiondata[i].answers[j].isChecked && $scope.questiondata[i].answers[j].sims_survey_rating_code == '') {
                                    //quest = quest + $scope.questiondata[i].sims_survey_question_code + ',';
                                    //ans = ans + $scope.questiondata[i].answers[j].sims_survey_question_answer_code + ',';
                                    join = join + $scope.questiondata[i].sims_survey_question_code + $scope.questiondata[i].answers[j].sims_survey_question_answer_code + ',';
                                }
                                if ($scope.questiondata[i].answers[j].sims_survey_user_response_answer_text != undefined) {
                                    join = join + $scope.questiondata[i].sims_survey_question_code + $scope.questiondata[i].answers[j].sims_survey_question_answer_code + $scope.questiondata[i].answers[j].sims_survey_user_response_answer_text + ',';
                                }
                                if ($scope.questiondata[i].answers[j].isChecked && $scope.questiondata[i].answers[j].sims_survey_rating_code != '') {
                                    join = join + $scope.questiondata[i].sims_survey_question_code + $scope.questiondata[i].answers[j].sims_survey_rating_code + ',';
                                }
                            }
                        }
                        console.log(join);
                        documentlist = {
                            'sims_survey_code': $scope.survey[0].sims_survey_code,
                            'questionanswer': join,
                            'sims_survey_user_response_user_code': user,
                            //'sims_survey_user_response_answer_text': null,                           
                            'opr': 'I',
                        }
                        
                        datasend.push(documentlist);
                        $http.post(ENV.apiUrl + "api/Survey/CUD_Survey", documentlist).then(function (msg) {
                            $scope.msg1 = msg.data;

                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Done!! Your answers submitted successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                $state.go($state.current, {}, { reload: true });
                            }
                            else {
                                swal({ text: "Sorry!! Answers cannot be inserted, Survey already done", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                            }
                                                     
                        });
                   // }
               // }
            }
        }])
})();