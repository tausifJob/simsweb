﻿(function () {
    'use strict';
    var main, temp = [];
    var data1 = [];
    var data2 = [];
    var block1 = [];
    var status = "";
    var msg_flag1 = false;
    var prdocdata = [];
    var acconcode = "";
    var cmbvalue = "";
    var comp_code = "1";
    var chk;
    var data = [];
    var simsController = angular.module('sims.module.Student');
    //simsController.directive('ngFiles', ['$parse', function ($parse) {
    //    function fn_link(scope, element, attrs) {
    //        var onChange = $parse(attrs.ngFiles);
    //        element.on('change', function (event) {
    //            onChange(scope, { $files: event.target.files });
    //        });
    //    };
    //    return {
    //        link: fn_link
    //    }
    //}])
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('campReportNewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$interval', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $interval) {

            $scope.getCamp = function (curCode, accCode) {
                if (accCode === null) {
                    $scope.campDetails && ($scope.campDetails.length = 0)
                    return false
                }
                $http.get(ENV.apiUrl + "api/CampTransaction/getCampDetails?cur_code=" + $scope.s_cur_code + "&academic_year=" + $scope.sims_academic_year + "&username=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.campDetails = res.data;
                })
            }

            $scope.getAccYear = function (curCode) {
                $scope.Acc_year && ($scope.Acc_year.length = 0)
                $scope.campDetails && ($scope.campDetails.length = 0)
                if (curCode === null) {
                    return false
                }
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                })
            }

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
                $scope.getAccYear($scope.curriculum[0].sims_cur_code);
            })

            $scope.onSearch = function () {
                if ($scope.s_cur_code === undefined) {
                    alert('Select curriculam');
                    return false
                }
                if ($scope.sims_academic_year === undefined) {
                    alert('Select academic year');
                    return false
                }
                if ($scope.sims_health_camp === undefined) {
                    alert('Select camp');
                    return false
                }
                $scope.isLoading = true;
                $http.get(ENV.apiUrl + "api/CampTransaction/getStudentsforDashboard?cur_code=" + $scope.s_cur_code + "&academic_year=" + $scope.sims_academic_year + "&camp_code=" + $scope.sims_health_camp).then(function (res) {
                    $scope.isLoading = false;
                    if (res.data.length === 0) {
                        $scope.isDatafound = false;
                        alert('Data not found');
                        return false
                    }
                    $scope.isDatafound = true;
                    $scope.studentDetails = res.data;
                },
                function (err) {
                    $scope.isLoading = false;
                })
            }

            $scope.drawGender = function (value) {
                var tooltext = '';
                switch (value) {
                    case "M": value = "0.5"; tooltext = "Male"; $scope.genderImg = "male.png"; break;
                    case "T": value = "1.5"; tooltext = "Transgender"; $scope.genderImg = "transgender.png"; break;
                    case "F": value = "2.5"; tooltext = "Female"; $scope.genderImg = "female.png"; break;
                }
                var genderSource = {
                    chart: {
                        caption: "Gender : " + tooltext,
                        subcaption: "A",
                        subCaptionFontColor: "#FFFFFF",
                        subCaptionFontSize: "12",
                        captionontop: "0",
                        origw: "380",
                        origh: "250",
                        gaugestartangle: "135",
                        gaugeendangle: "45",
                        gaugeoriginx: "190",
                        gaugeoriginy: "220",
                        gaugeouterradius: "190",
                        theme: "fusion",
                        showvalue: "1",
                        numbersuffix: "",
                        showTickMarks: "0",
                        showTickValues: "0"
                        //visualizationObj.Legend.Show = false;
                    },
                    colorrange: {
                        color: [
                          {
                              minvalue: "0",
                              maxvalue: "1",
                              code: "#97ca3d"
                          },
                          {
                              minvalue: "1",
                              maxvalue: "2",
                              code: "#fde70d"
                          },
                          {
                              minvalue: "2",
                              maxvalue: "3",
                              code: "#2baae2"
                          }
                        ]
                    },
                    dials: {
                        dial: [
                          {
                              showValue: "0",
                              value: value,
                              tooltext: tooltext
                          }
                        ]
                    }
                };
                FusionCharts.ready(function () {
                    var genderChart = new FusionCharts({
                        type: "angulargauge",
                        renderAt: "genderChart",
                        width: "100%",
                        height: "250",
                        dataFormat: "json",
                        dataSource: genderSource
                    }).render();
                })
            }

            $scope.drawBMI_weight = function (cr) {
                var value = +cr.sims_health_camp_parameter_check_value;
                //$scope.weightChartcomment = cr.sims_health_camp_parameter_check_comment;
                var temp = '';
                if (value < 18.5) {
                    $scope.weightChartAnnotation = "underweight.png"
                    temp = 0.5;
                }
                else if (value > 18.5 && value <= 23) {
                    $scope.weightChartAnnotation = "normal1.png"
                    temp = 1.5;
                }
                else if (value > 23 && value <= 27.5) {
                    $scope.weightChartAnnotation = "overweight1.png"
                    temp = 2.5;
                }
                else if (value > 27.5) {
                    $scope.weightChartAnnotation = "obese1.png"
                    temp = 3.5;
                }

                var weightSource = {
                    chart: {
                        caption: "Weight (" + value + "Kg)",
                        subcaption: cr.sims_health_camp_parameter_check_comment,
                        captionOnTop: "0",
                        lowerlimitdisplay: "0kg",
                        upperlimitdisplay: "max",
                        numbersuffix: "Kg",
                        gaugefillmix: "{dark-20},{light+70},{dark-10}",
                        theme: "fusion",
                        showvalue: "1",
                        showTickMarks: "0",
                        showTickValues: "0"
                        //valueFontColor: "#ffffff"
                    },
                    colorrange: {
                        color: [
                          //{
                          //    minvalue: "0",
                          //    maxvalue: "18.5",
                          //    label: "Underweight",
                          //    code: "#46d1f0"
                          //},
                          //{
                          //    minvalue: "18.5",
                          //    maxvalue: "23",
                          //    label: "Healthy range",
                          //    code: "#34a549"
                          //},
                          //{
                          //    minvalue: "23",
                          //    maxvalue: "27.5",
                          //    label: "Over weight",
                          //    code: "#f3c915"
                          //},
                          //{
                          //    minvalue: "27.5",
                          //    maxvalue: "32",
                          //    label: "Obese",
                          //    code: "#d82403"
                          //}

                        {
                            minvalue: "0",
                            maxvalue: "1",
                            label: "Underweight",
                            code: "#46d1f0"
                        },
                        {
                            minvalue: "1",
                            maxvalue: "2",
                            label: "Healthy range",
                            code: "#34a549"
                        },
                        {
                            minvalue: "2",
                            maxvalue: "3",
                            label: "Over weight",
                            code: "#f3c915"
                        },
                        {
                            minvalue: "3",
                            maxvalue: "4",
                            label: "Obese",
                            code: "#f18930"
                        }
                        ]
                    },
                    pointers: {
                        pointer: [
                          {
                              showValue: "0",
                              value: temp
                          }
                        ]
                    }
                };
                FusionCharts.ready(function () {
                    var weightChart = new FusionCharts({
                        type: "hlineargauge",
                        renderAt: "weightChart",
                        width: "75%",
                        height: "175",
                        dataFormat: "json",
                        dataSource: weightSource
                    }).render();
                })
                
            }
            $scope.drawBMI_height = function (value) {
                var heightSource = {
                    chart: {
                        caption: "Height (" + value + "cm)",
                        xaxisname: "",
                        yaxisname: "",
                        numbersuffix: "cm",
                        theme: "fusion",
                        captionOnTop:"0"
                    },
                    data: [{
                          value: value
                      }]
                };

                FusionCharts.ready(function() {
                    var heightChart = new FusionCharts({
                        type: "column2d",
                        renderAt: "heightChart",
                        width: "200",
                        height: "300",
                        dataFormat: "json",
                        dataSource: heightSource
                        }).render();
                });





                //FusionCharts.ready(function () {
                //    var yValue = 0;
                //    value < 10 && (yValue = value * 6.2)
                //    value > 10 && value <= 20 && (yValue = value * 5)
                //    value > 20 && value <= 30 && (yValue = value * 4.8)
                //    value > 30 && value <= 40 && (yValue = value * 4.3)
                //    value > 40 && value <= 50 && (yValue = value * 4.2)
                //    value > 50 && value <= 60 && (yValue = value * 4.1)
                //    value > 60 && value <= 70 && (yValue = value * 4.04)
                //    value > 70 && value <= 80 && (yValue = value * 3.99)
                //    value > 80 && value <= 90 && (yValue = value * 3.95)
                //    value > 90 && value <= 100 && (yValue = value * 3.9)
                //    value > 100 && (yValue = value * 3.5)
                //    var heightSource = new FusionCharts({
                //        type: 'column2d',
                //        renderAt: 'heightChart',
                //        width: '200',
                //        height: '405',
                //        dataFormat: 'json',
                //        dataSource: {
                //            "chart": {
                //                "yAxisName": "Heigth (" + value + "cm)",
                //                "yAxisMaxValue": "100",
                //                "yAxisNameFontSize": "14",
                //                "yAxisNameFontBold": "1",
                //                "numberPrefix": "",
                //                "theme": "fusion",
                //                "PlotfillAlpha": "0",
                //                "placeValuesInside": "0",
                //                "rotateValues": "0",
                //                "valueFontColor": "#333333",
                //                "fontWeight": "1"
                //            },
                //            "annotations": {
                //                "width": "200",
                //                "height": "405",
                //                "autoScale": "1",
                //                "groups": [{
                //                    "id": "user-images",
                //                    "xScale_": "20",
                //                    "yScale_": "20",
                //                    "items": [{
                //                        "id": "butterFinger-icon",
                //                        "type": "image",
                //                        "url": "../SIMSWeb/assets/img/heightmeter.png",
                //                        "x": "$xaxis.label.0.x + 75",
                //                        "y": "$xaxis.label.0.x + 405 - " + (yValue),
                //                        "xScale": "40",
                //                        "yScale": value,
                //                        "fill": "#f1f1f1"
                //                    }]
                //                }]
                //            },
                //            "data": [{
                //                "value": value
                //            }]
                //        }
                //    }).render();
                //})


                //var heightSource = {
                //    chart: {
                //        //caption: "Height (cm)",
                //        lowerlimitdisplay: "0cm",
                //        upperlimitdisplay: "180cm",
                //        numbersuffix: "cm",
                //        chartbottommargin: "5",
                //        theme: "fusion",
                //        showvalue: "1",
                //        showLegend: "1",
                //        //bgImage: "../SIMSWeb/assets/img/male.png"
                //    },
                //    colorrange: {
                //        color: [
                //          {
                //              minvalue: "145",
                //              maxvalue: "180"
                //          }
                //        ]
                //    },
                //    //value: value,
                //    pointers: {
                //        pointer: [{
                //            value: value
                //        }]
                //    },
                //    annotations: {
                //        width: "500",
                //        height: "300",
                //        autoScale: "1",
                //        groups: [{
                //            id: "user-images",
                //            xScale_: "20",
                //            yScale_: "20",
                //            items: [{
                //                id: "butterFinger-icon",
                //                type: "image",
                //                url: "../SIMSWeb/assets/img/male.png",
                //                x: "$xaxis.label.0.x - 10",
                //                y: "$canvasEndY - 10",
                //                xScale: "30",
                //                yScale: "20",
                //            }]
                //        }]
                //    },
                //    data: [{
                //        "label": "Butterfinger",
                //        value: value
                //    }]
                //annotations: {
                //    showbelow: "1",
                //    groups: [
                //      {
                //          id: "indicator",
                //          items: [
                //            {
                //                id: "message",
                //                align: "CENTER",
                //                bold: "1",
                //                type: "text",
                //                text: "",
                //                color: "#000000",
                //                x: "$chartCenterX",
                //                y: "$chartEndY-35"
                //            }
                //          ]
                //      }
                //    ]
                //}
                //};
                //FusionCharts.ready(function () {
                //    var heightChart = new FusionCharts({
                //        type: "vled",
                //        renderAt: "heightChart",
                //        width: "55%",
                //        height: "260",
                //        dataFormat: "json",
                //        dataSource: heightSource
                //    }).render();
                //})
            }
            $scope.drawBMI_nailHy = function (cr) {
                var tooltext = 'Nail hygiene: ';
                var value = cr.sims_health_camp_parameter_check_code;
                switch (value) {
                    case "0004": value = "0.5"; tooltext += "Not done"; break;
                    case "0003": value = "1.5"; tooltext += "Poor"; $scope.nailImg = 'nailpoor.png'; break;
                    case "0002": value = "2.5"; tooltext += "Fair"; $scope.nailImg = 'nailfair.png'; break;
                    case "0001": value = "3.5"; tooltext += "Good"; $scope.nailImg = 'nailnormal.png'; break;
                }
                var nailhygieneSource = {
                    chart: {
                        caption: tooltext,
                        subcaption: cr.sims_health_camp_parameter_check_comment,
                        subCaptionFontSize: "12",
                        captionontop: "0",
                        origw: "380",
                        origh: "250",
                        gaugestartangle: "135",
                        gaugeendangle: "45",
                        gaugeoriginx: "190",
                        gaugeoriginy: "220",
                        gaugeouterradius: "190",
                        theme: "fusion",
                        showvalue: "1",
                        numbersuffix: "",
                        showTickMarks: "0",
                        showTickValues: "0",
                        exportEnabled:"1" 
                    },
                    colorrange: {
                        color: [
                          {
                              minvalue: "0",
                              maxvalue: "1",
                              code: "#cc3333"
                          },
                          {
                              minvalue: "1",
                              maxvalue: "2",
                              code: "#ddaa33"
                          },
                          {
                              minvalue: "2",
                              maxvalue: "3",
                              code: "#11ddaa"
                          },
                          {
                              minvalue: "3",
                              maxvalue: "4",
                              code: "#009933"
                          }
                        ]
                    },
                    dials: {
                        dial: [
                          {
                              showValue: "0",
                              value: value,
                              tooltext: tooltext
                          }
                        ]
                    }
                };
                FusionCharts.ready(function () {
                    var nailhygieneChart = new FusionCharts({
                        type: "angulargauge",
                        renderAt: "nailhygieneChart",
                        width: "100%",
                        height: "250",
                        dataFormat: "json",
                        dataSource: nailhygieneSource
                    }).render();
                })
            }
            $scope.drawBMI_hairHy = function (cr) {
                var tooltext = 'Hair hygiene: ';
                var value = cr.sims_health_camp_parameter_check_code;
                switch (value) {
                    case "0004": value = "0.5"; tooltext += "Not done"; break;
                    case "0003": value = "1.5"; tooltext += "Poor"; break;
                    case "0002": value = "2.5"; tooltext += "Fair"; break;
                    case "0001": value = "3.5"; tooltext += "Good"; break;
                }
                var hairhygieneSource = {
                    chart: {
                        caption: tooltext,
                        subcaption: cr.sims_health_camp_parameter_check_comment,
                        subCaptionFontSize: "12",
                        captionontop: "0",
                        origw: "380",
                        origh: "250",
                        gaugestartangle: "135",
                        gaugeendangle: "45",
                        gaugeoriginx: "190",
                        gaugeoriginy: "220",
                        gaugeouterradius: "190",
                        theme: "fusion",
                        showvalue: "1",
                        numbersuffix: "",
                        showTickMarks: "0",
                        showTickValues: "0"
                    },
                    colorrange: {
                        color: [
                          {
                              minvalue: "0",
                              maxvalue: "1",
                              code: "#cc3333"
                          },
                          {
                              minvalue: "1",
                              maxvalue: "2",
                              code: "#ddaa33"
                          },
                          {
                              minvalue: "2",
                              maxvalue: "3",
                              code: "#11ddaa"
                          },
                          {
                              minvalue: "3",
                              maxvalue: "4",
                              code: "#009933"
                          }
                        ]
                    },
                    dials: {
                        dial: [
                          {
                              showValue: "0",
                              value: value,
                              tooltext: tooltext
                          }
                        ]
                    }
                };
                FusionCharts.ready(function () {
                    var hairhygieneChart = new FusionCharts({
                        type: "angulargauge",
                        renderAt: "hairhygieneChart",
                        width: "100%",
                        height: "250",
                        dataFormat: "json",
                        dataSource: hairhygieneSource
                    }).render();
                })
            }
            $scope.drawBMI_pulseR = function (cr) {
                var pluseRateSource = {
                    chart: {
                        caption: "Pulse rate (BPM)",
                        captionontop: "0",
                        origw: "80",
                        origh: "360",
                        gaugestartangle: "220",
                        gaugeendangle: "-40",
                        gaugeoriginx: "250",
                        gaugeoriginy: "170",
                        gaugeouterradius: "140",
                        theme: "fusion",
                        showvalue: "1",
                        numbersuffix: " BPM",
                        valuefontsize: "25",
                        showLegend: "1",
                        majorTMNumber: "6"
                    },
                    colorrange: {
                        color: [
                          {
                              //minvalue: +cr.sims_health_parameter_check_min_value,
                              //maxvalue: +cr.sims_health_parameter_check_max_value,
                              minvalue: 0,
                              maxvalue: 120,
                              code: "#bda7f6"
                          }
                        ]
                    },
                    dials: {
                        dial: [
                          {
                              value: +cr.sims_health_camp_parameter_check_value,
                              tooltext: cr.sims_health_camp_parameter_check_comment
                          }
                        ]
                    }
                };

                FusionCharts.ready(function () {
                    var pluseRateChart = new FusionCharts({
                        type: "angulargauge",
                        renderAt: "pluseRateChart",
                        width: "350",
                        height: "250",
                        dataFormat: "json",
                        dataSource: pluseRateSource
                    }).render();
                })
            }
            $scope.drawBMI_oxymet = function (cr) {
                var oxymetrySource = {
                    chart: {
                        caption: "Oxymetry (%)",
                        captionontop: "0",
                        origw: "80",
                        origh: "360",
                        gaugestartangle: "220",
                        gaugeendangle: "-40",
                        gaugeoriginx: "250",
                        gaugeoriginy: "170",
                        gaugeouterradius: "140",
                        theme: "fusion",
                        showvalue: "1",
                        numbersuffix: "%",
                        valuefontsize: "25",
                        showLegend: "1",
                        majorTMNumber: "6",
                    },
                    colorrange: {
                        color: [
                          {
                              //minvalue: +cr.sims_health_parameter_check_min_value,
                              //maxvalue: +cr.sims_health_parameter_check_max_value,
                              minvalue: 0,
                              maxvalue: 120,
                              code: "#b4d8f3"
                          }
                        ]
                    },
                    dials: {
                        dial: [
                          {
                              value: +cr.sims_health_camp_parameter_check_value,
                              tooltext: cr.sims_health_camp_parameter_check_comment
                          }
                        ]
                    }
                };
                FusionCharts.ready(function () {
                    var oximetrySource = new FusionCharts({
                        type: "angulargauge",
                        renderAt: "oxymetryChart",
                        width: "350",
                        height: "250",
                        dataFormat: "json",
                        dataSource: oxymetrySource
                    }).render();
                })
            }

            $scope.drawBIO_anemic = function (value) {
                //value = value * 10;
                //var yValue = 0;
                //value < 10 && (yValue = value * 6.2)
                //value > 10 && value <= 20 && (yValue = value * 5)
                //value > 20 && value <= 30 && (yValue = value * 4.5)
                //value > 30 && value <= 40 && (yValue = value * 4.3)
                //value > 40 && value <= 50 && (yValue = value * 4.2)
                //value > 50 && value <= 60 && (yValue = value * 4.1)
                //value > 60 && value <= 70 && (yValue = value * 4.04)
                //value > 70 && value <= 80 && (yValue = value * 3.99)
                //value > 80 && value <= 90 && (yValue = value * 4.04)
                //value > 90 && value <= 100 && (yValue = value * 3.9)
                //var anemicSource = new FusionCharts({
                //    type: 'column2d',
                //    renderAt: 'anemicChart',
                //    width: '200',
                //    height: '405',
                //    dataFormat: 'json',
                //    dataSource: {
                //        "chart": {
                //            "yAxisName": "Heigth (CM)",
                //            "yAxisMaxValue": "14",
                //            "numberPrefix": "",
                //            "theme": "fusion",
                //            "PlotfillAlpha": "0",
                //            "placeValuesInside": "0",
                //            "rotateValues": "0",
                //            "valueFontColor": "#333333",
                //            "fontWeight": "1",
                //            "bgImage": "../SIMSWeb/assets/img/anemicchart.png",
                //            "majorTMNumber": "7"
                //        },
                //        "annotations": {
                //            "width": "200",
                //            "height": "405",
                //            "autoScale": "1",
                //            "groups": [{
                //                "id": "user-images",
                //                "xScale_": "20",
                //                "yScale_": "20",
                //                "items": [{
                //                    "id": "butterFinger-icon",
                //                    "type": "image",
                //                    "url": "../SIMSWeb/assets/img/anemicchart.png",
                //                    "x": "$xaxis.label.0.x + 75",
                //                    "y": "$xaxis.label.0.x + 405 - " + (yValue),
                //                    "xScale": "100",
                //                    "yScale": value,
                //                    "fill": "#f1f1f1"
                //                }]
                //            }]
                //        },
                //        "data": [{
                //            "value": value
                //        }]
                //    }
                //}).render();



                var anemicSource = {
                    chart: {
                        caption: "",
                        lowerlimitdisplay: "0g/dl",
                        upperlimitdisplay: "16g/dl",
                        numbersuffix: "g/dl",
                        chartbottommargin: "0",
                        theme: "fusion",
                        showvalue: "1",
                        majorTMNumber: "7",
                        ticksOnRight: "0"
                    },
                    colorrange: {
                        color: [
                          {
                              minvalue: "0",
                              maxvalue: "2",
                              code: "#fc4544"
                          },
                          {
                              minvalue: "2",
                              maxvalue: "4",
                              code: "#f12b2a"
                          },
                          {
                              minvalue: "4",
                              maxvalue: "6",
                              code: "#ea2521"
                          },
                          {
                              minvalue: "6",
                              maxvalue: "8",
                              code: "#e11312"
                          },
                          {
                              minvalue: "8",
                              maxvalue: "10",
                              code: "#cb0e05"
                          },
                          {
                              minvalue: "10",
                              maxvalue: "12",
                              code: "#b30e0e"
                          },
                          {
                              minvalue: "12",
                              maxvalue: "14",
                              code: "#92060c"
                          },
                          {
                              minvalue: "14",
                              maxvalue: "16",
                              code: "#92060c"
                          }
                        ]
                    },
                    value: value,
                    annotations: {
                        showbelow: "1",
                        groups: [
                          {
                              id: "indicator",
                              items: [
                                {
                                    id: "message",
                                    align: "CENTER",
                                    bold: "1",
                                    type: "text",
                                    text: "",
                                    color: "#000000",
                                    x: "$chartCenterX",
                                    y: "$chartEndY-35"
                                }
                              ]
                          }
                        ]
                    }
                };
                FusionCharts.ready(function () {
                    var anemicChart = new FusionCharts({
                        type: "vled",
                        renderAt: "anemicChart",
                        width: "150",
                        height: "250",
                        dataFormat: "json",
                        dataSource: anemicSource
                    }).render();
                })
            }
            $scope.drawBIO_spirom = function (cr) {
                var spirometrySource = {
                    chart: {
                        caption: "Spirometry : " + cr.sims_health_camp_parameter_check_comment,
                        showpercentvalues: "0",
                        defaultcenterlabel: cr.sims_health_camp_parameter_check_value + " {br} L/M",
                        aligncaptionwithcanvas: "0",
                        captionontop: "0",
                        decimals: "0",
                        theme: "fusion",
                        centerLabelFontSize: "18",
                        centerLabelBold: "1"
                    },
                    data: [
                      {
                          showLabel: "0",
                          showValue:"0",
                          value: cr.sims_health_camp_parameter_check_value
                      }
                    ]
                };
                FusionCharts.ready(function () {
                    var spirometryChart = new FusionCharts({
                        type: "doughnut2d",
                        renderAt: "spirometryChart",
                        width: "100%",
                        height: "300",
                        dataFormat: "json",
                        dataSource: spirometrySource
                    }).render();
                })
            }

            $scope.drawENT_Hearin = function (cr) {
                var tooltext = ': ';
                var value = cr.sims_health_camp_parameter_check_code;
                switch (value) {
                    case "0001": value = "0.5"; tooltext += "Normal"; break;
                    case "0002": value = "1.5"; tooltext += "Abnormal"; break;
                    case "0003": value = "2.5"; tooltext += "Not done"; break;
                }
                var hearingSource = {
                    chart: {
                        caption: "Hearing",
                        captionontop: "0",
                        subcaption: cr.sims_health_camp_parameter_check_comment == "" ? "-" : cr.sims_health_camp_parameter_check_comment,
                        lowerlimitdisplay: "0",
                        upperlimitdisplay: "3",
                        numbersuffix: "",
                        gaugefillmix: "{dark-20},{light+70},{dark-10}",
                        theme: "fusion",
                        showTickMarks: "0",
                        showTickValues: "0"
                    },
                    colorrange: {
                        color: [
                          {
                              minvalue: "0",
                              maxvalue: "1",
                              label: "Normal",
                              code: "#34a549"
                          },
                          {
                              minvalue: "1",
                              maxvalue: "2",
                              label: "Abnormal",
                              code: "#f12b2a"
                          },
                          {
                              minvalue: "2",
                              maxvalue: "3",
                              label: "Not done",
                              code: "#46d1f0"
                          }
                        ]
                    },
                    pointers: {
                        pointer: [
                          {
                              value: value,
                              showValue: "0"
                          }
                        ]
                    }
                };
                FusionCharts.ready(function () {
                    var hearingSensitivityChart = new FusionCharts({
                        type: "hlineargauge",
                        renderAt: "hearingSensitivityChart",
                        width: "100%",
                        height: "175",
                        dataFormat: "json",
                        dataSource: hearingSource
                    }).render();
                })

                //if (value < 18.5) {
                //    $scope.weightChartAnnotation = "underweight.png"
                //}
                //else if (value > 18.5 && value <= 23) {
                //    $scope.weightChartAnnotation = "normal1.png"
                //}
                //else if (value > 23 && value <= 27.5) {
                //    $scope.weightChartAnnotation = "overweight1.png"
                //}
                //else if (value > 27.5) {
                //    $scope.weightChartAnnotation = "obese1.png"
                //}
            }
            $scope.drawENT_Earing = function (cr) {
                var tooltext = ': ';
                var value = cr.sims_health_camp_parameter_check_code;
                switch (value) {
                    case "0001": value = "0.5"; tooltext += "Good"; break;
                    case "0002": value = "1.5"; tooltext += "Fair"; break;
                    case "0003": value = "2.5"; tooltext += "Poor"; break;
                    case "0004": value = "3.5"; tooltext += "Not done"; break;
                }
                var earingSource = {
                    chart: {
                        caption: "Ear",
                        captionontop: "0",
                        subcaption: cr.sims_health_camp_parameter_check_comment == "" ? "-" : cr.sims_health_camp_parameter_check_comment,
                        lowerlimitdisplay: "0",
                        upperlimitdisplay: "3",
                        numbersuffix: "",
                        gaugefillmix: "{dark-20},{light+70},{dark-10}",
                        theme: "fusion",
                        showTickMarks: "0",
                        showTickValues: "0"
                    },
                    colorrange: {
                        color: [
                          {
                              minvalue: "0",
                              maxvalue: "1",
                              label: "Good",
                              code: "#34a549"
                          },
                          {
                              minvalue: "1",
                              maxvalue: "2",
                              label: "Fair",
                              code: "#f3c915"
                          },
                          {
                              minvalue: "2",
                              maxvalue: "3",
                              label: "Poor",
                              code: "#f12b2a"
                          },
                          {
                              minvalue: "3",
                              maxvalue: "4",
                              label: "Not done",
                              code: "#46d1f0"
                          }
                        ]
                    },
                    pointers: {
                        pointer: [
                          {
                              value: value,
                              showValue: "0"
                          }
                        ]
                    }
                };
                FusionCharts.ready(function () {
                    var earChart = new FusionCharts({
                        type: "hlineargauge",
                        renderAt: "earChart",
                        width: "100%",
                        height: "175",
                        dataFormat: "json",
                        dataSource: earingSource
                    }).render();
                })
            }
            $scope.drawENT_Throat = function (cr) {
                var tooltext = ': ';
                var value = cr.sims_health_camp_parameter_check_code;
                switch (value) {
                    case "0001": value = "0.5"; tooltext += "Normal"; break;
                    case "0002": value = "1.5"; tooltext += "Abnormal"; break;
                    case "0003": value = "2.5"; tooltext += "Not done"; break;
                }
                var throatSource = {
                    chart: {
                        caption: "Throat",
                        captionontop: "0",
                        subcaption: cr.sims_health_camp_parameter_check_comment == "" ? "-" : cr.sims_health_camp_parameter_check_comment,
                        lowerlimitdisplay: "0",
                        upperlimitdisplay: "3",
                        numbersuffix: "",
                        gaugefillmix: "{dark-20},{light+70},{dark-10}",
                        theme: "fusion",
                        showTickMarks: "0",
                        showTickValues: "0"
                    },
                    colorrange: {
                        color: [
                          {
                              minvalue: "0",
                              maxvalue: "1",
                              label: "Normal",
                              //alpha: "50",
                              code: "#34a549"
                          },
                          {
                              minvalue: "1",
                              maxvalue: "2",
                              label: "Abnormal",
                              //alpha: "50",
                              code: "#f12b2a"
                          },
                          {
                              minvalue: "2",
                              maxvalue: "3",
                              label: "Not done",
                              //alpha: "50",
                              code: "#46d1f0"
                          }
                        ]
                    },
                    pointers: {
                        pointer: [
                          {
                              value: value,
                              showValue: "0"
                          }
                        ]
                    }
                };
                FusionCharts.ready(function () {
                    var throathChart = new FusionCharts({
                        type: "hlineargauge",
                        renderAt: "throatChart",
                        width: "100%",
                        height: "175",
                        dataFormat: "json",
                        dataSource: throatSource
                    }).render();
                })
            }

            $scope.drawDentalPitN = function (value) {
                var tooltext = 'Pit & Fissure caries: ';
                switch (value) {
                    case "0001": $scope.PitText = "Status: Found"; $scope.PitNImage = 'dentalPitYes.jpg'; break;
                    case "0002": $scope.PitText = "Status: Not found"; $scope.PitNImage = 'dentalNo.jpg'; break;
                    case "0003": $scope.PitText = "Status: Not done"; $scope.PitNImage = 'notDone.png'; break;
                }
                value === "" ? $scope.isPit = false : $scope.isPit = true
            }
            $scope.drawDentalNurs = function (value) {
                switch (value) {
                    case "0001": $scope.NursText = "Status: Found"; $scope.NursImage = 'dentalNursYes.jpg'; break;
                    case "0002": $scope.NursText = "Status: Not found"; $scope.NursImage = 'dentalNo.jpg'; break;
                    case "0003": $scope.NursText = "Status: Not done"; $scope.NursImage = 'notDone.png'; break;
                }
                value === "" ? $scope.isNurs = false : $scope.isNurs = true
            }
            $scope.drawDentalGumI = function (value) {
                switch (value) {
                    case "0001": $scope.GumIText = "Status: Found"; $scope.GumIImage = 'dentalGumYes.jpg'; break;
                    case "0002": $scope.GumIText = "Status: Not found"; $scope.GumIImage = 'dentalNo.jpg'; break;
                    case "0003": $scope.GumIText = "Status: Not done"; $scope.GumIImage = 'notDone.png'; break;
                }
                value === "" ? $scope.isGum = false : $scope.isGum = true
            }
            $scope.drawDentalBlee = function (value) {
                switch (value) {
                    case "0001": $scope.BleeText = "Status: Found"; $scope.BleedingImage = 'dentalBeedlingYes.jpg'; break;
                    case "0002": $scope.BleeText = "Status: Not found"; $scope.BleedingImage = 'dentalNo.jpg'; break;
                    case "0003": $scope.BleeText = "Status: Not done"; $scope.BleedingImage = 'notDone.png'; break;
                }
                value === "" ? $scope.isBleeding = false : $scope.isBleeding = true
            }
            $scope.drawDentalTart = function (value) {
                switch (value) {
                    case "0001": $scope.tartarText = "Status: Found"; $scope.TartarImage = 'dentalTartarYes.jpg'; break;
                    case "0002": $scope.tartarText = "Status: Not found"; $scope.TartarImage = 'dentalNo.jpg'; break;
                    case "0003": $scope.tartarText = "Status: Not done"; $scope.TartarImage = 'notDone.png'; break;
                }
                value === "" ? $scope.isTartar = false : $scope.isTartar = true
            }
            $scope.drawDentalPlaq = function (value) {
                switch (value) {
                    case "0001": $scope.plaqueText = "Status: Found"; $scope.PlaqueImage = 'dentalPlaqueYes.png'; break;
                    case "0002": $scope.plaqueText = "Status: Not found"; $scope.PlaqueImage = 'dentalNo.jpg'; break;
                    case "0003": $scope.plaqueText = "Status: Not done"; $scope.PlaqueImage = 'notDone.png'; break;
                }
                value === "" ? $scope.isPlaque = false : $scope.isPlaque = true
            }
            $scope.drawDentalOral = function (value) {
                var tooltext = 'Oral hygiene: ';
                switch (value) {
                    case "0004": value = "0.5"; tooltext += "Not done"; break;
                    case "0003": value = "1.5"; tooltext += "Poor"; $scope.nailImg = 'nailpoor.png'; break;
                    case "0002": value = "2.5"; tooltext += "Fair"; $scope.nailImg = 'nailfair.png'; break;
                    case "0001": value = "3.5"; tooltext += "Good"; $scope.nailImg = 'nailnormal.png'; break;
                }
                var oralhygieneSource = {
                    chart: {
                        caption: tooltext,
                        captionontop: "0",
                        origw: "380",
                        origh: "250",
                        gaugestartangle: "135",
                        gaugeendangle: "45",
                        gaugeoriginx: "190",
                        gaugeoriginy: "220",
                        gaugeouterradius: "190",
                        theme: "fusion",
                        showvalue: "1",
                        numbersuffix: "",
                        valuefontsize: "18",
                        legendPosition: "right",
                        showLegend: "1"
                    },
                    colorrange: {
                        color: [
                          {
                              minvalue: "0",
                              maxvalue: "1",
                              code: "#cc3333"
                          },
                          {
                              minvalue: "1",
                              maxvalue: "2",
                              code: "#ddaa33"
                          },
                          {
                              minvalue: "2",
                              maxvalue: "3",
                              code: "#11ddaa"
                          },
                          {
                              minvalue: "3",
                              maxvalue: "4",
                              code: "#009933"
                          }
                        ]
                    },
                    dials: {
                        dial: [
                          {
                              value: value,
                              tooltext: tooltext
                          }
                        ]
                    }
                };
                FusionCharts.ready(function () {
                    var nailhygieneChart = new FusionCharts({
                        type: "angulargauge",
                        renderAt: "oralhygiene",
                        width: "100%",
                        height: "250",
                        dataFormat: "json",
                        dataSource: oralhygieneSource
                    }).render();
                })
            }
            $scope.drawDentalCari = function (cmt) {
                for (var i = 11; i <= 85; i++) {
                    if ((i > 18 && i < 21) || (i > 28 && i < 31) || (i > 38 && i < 41) || (i > 48 && i < 51) || (i > 55 && i < 61) || (i > 65 && i < 71) || (i > 75 && i < 81)) {
                    }
                    else {
                        var tempId = '#teeth' + i + ' path';
                        $(tempId).attr('fill', '#FFFFFF');
                    }
                }
                if (cmt !== "Absent") {
                    cmt !== '' && (cmt = cmt.substr(0, cmt.length - 1), cmt = cmt.split(','))
                    $timeout(function () {
                        cmt.forEach(function (cr) {
                            var temp = cr.split('-');
                            var tempId = '#teeth' + temp[1] + ' path';
                            switch (temp[0]) {
                                case 'D': $(tempId).attr('fill', '#FF0000'); break;
                                case 'F': $(tempId).attr('fill', '#008000'); break;
                                case 'M': $(tempId).attr('fill', '#FFFF00'); break;
                            }
                            //$scope.teethDetails[$scope.teethDetails.length] = { teethNo: temp[1], isFilled: true, teethType: temp[0] };
                        })
                        $scope.isOral = true;
                    }, 2000)
                }
                else {
                    $scope.isOral = false;
                }
            }
            $scope.drawDentalCalc = function (value) {
                switch (value) {
                    case "0001": $scope.calculusText = "Status: Found"; $scope.CalculusImage = 'dentalCalculusYes.png'; break;
                    case "0002": $scope.calculusText = "Status: Not found"; $scope.CalculusImage = 'dentalNo.jpg'; break;
                    case "0003": $scope.calculusText = "Status: Not done"; $scope.CalculusImage = 'notDone.png'; break;
                }
            }

            $scope.drawEntExamination = function (cr) {
                cr.parameter_lst.length > 0 &&
                (cr.parameter_lst.map(function (cri) {
                    switch (cri.sims_health_parameter_code) {
                        case "0009": $scope.drawENT_Hearin(cri.check_lst[0]); break; // Hearing
                        case "0010": $scope.drawENT_Earing(cri.check_lst[0]); break; // Earing
                        case "0011": $scope.drawENT_Throat(cri.check_lst[0]); break; // Throath
                    }
                }), $scope.isENT = true)
            }

            $scope.drawBMI = function (cr) {
                cr.parameter_lst.length > 0 &&
                (cr.parameter_lst.map(function (cri) {
                    //var tempPara = cri.sims_health_parameter_isNumeric === "N" ? cri.sims_health_camp_parameter_check_comment : cri.sims_health_camp_parameter_check_value;
                    switch (cri.sims_health_parameter_code) {
                        case "0024": $scope.drawBMI_weight(cri.check_lst[0]); break; // Weight
                        case "0025": $scope.drawBMI_height(+cri.sims_health_camp_parameter_check_value); break; // Height
                        case "0028": $scope.drawBMI_nailHy(cri.check_lst[0]); break; // Nail hygiene
                        case "0029": $scope.drawBMI_hairHy(cri.check_lst[0]); break; // Hair hygiene
                        case "0030": $scope.drawBMI_pulseR(cri.check_lst[0]); break; // Pluse rate
                        case "0031": $scope.drawBMI_oxymet(cri.check_lst[0]); break; // oxymetry
                    }
                }), $scope.isBMI = true)
            }
            $scope.drawBiometric = function (cr) {
                cr.parameter_lst.length > 0 &&
                (cr.parameter_lst.map(function (cri) {
                    switch (cri.sims_health_parameter_code) {
                        case "0027": $scope.drawBIO_anemic(+cri.check_lst[0].sims_health_camp_parameter_check_value); break; // Anemic
                        case "0030": $scope.drawBMI_pulseR(cri.check_lst[0]); break; // Pluse rate
                        case "0031": $scope.drawBMI_oxymet(cri.check_lst[0]); break; // Oxymetry
                        case "0033": $scope.drawBIO_spirom(cri.check_lst[0]); break; // Spirometry
                            //case "0029": $scope.drawBMI_pulseR(cri); break; // Pluse rate
                            //case "0029": $scope.drawBMI_oximet(cri); break; // oximetry
                    }
                }), $scope.isBiomertics = true, $scope.isPluseRateNoxy = true)
                cr.parameter_lst.length == 0 && ($scope.isPluseRateNoxy = false)
            }
            $scope.drawVision = function (cr) {
                $scope.isVision = true;
            }
            $scope.drawDental = function (cr) {
                cr.parameter_lst.length > 0 &&
                (cr.parameter_lst.map(function (cri) {
                    switch (cri.sims_health_parameter_code) {
                        case "0001": $scope.drawDentalPitN(cri.check_lst[0].sims_health_camp_parameter_check_code); break; // "Pit & Fissure caries"
                        case "0002": $scope.drawDentalNurs(cri.check_lst[0].sims_health_camp_parameter_check_code); break; // "Nursing Bottle Caries"
                        case "0003": $scope.drawDentalGumI(cri.check_lst[0].sims_health_camp_parameter_check_code); break; // "Gum Inflamed"
                        case "0004": $scope.drawDentalBlee(cri.check_lst[0].sims_health_camp_parameter_check_code); break; // "Bleeding"
                        case "0005": $scope.drawDentalTart(cri.check_lst[0].sims_health_camp_parameter_check_code); break; // "Tartar"
                        case "0006": $scope.drawDentalPlaq(cri.check_lst[0].sims_health_camp_parameter_check_code); break; // "Plaque"
                        case "0007": $scope.drawDentalOral(cri.check_lst[0].sims_health_camp_parameter_check_code); break; // "Oral hygiene"
                        case "0044": $scope.drawDentalCari(cri.check_lst[0].sims_health_camp_parameter_check_comment); break; // "Caries"
                    }
                }), $scope.isDental = true)
            }
            $scope.onClickRow = function (x) {
                $scope.isLoading = true;
                var obj = {
                    sims_health_package_code: x.sims_health_package_code,
                    sims_health_enroll_number: x.sims_student_enroll_number,
                    sims_health_camp_code: x.sims_health_camp_code,
                }
                $http({
                    method: 'post',
                    url: ENV.apiUrl + 'api/CampTransaction/CUAllListforDashboard',
                    data: obj
                }).then(function (res) {
                    $scope.drawGender(x.sims_student_gender);
                    res.data.test_lst.length > 0 &&
                    (res.data.test_lst.map(function (cr) {
                        switch (cr.sims_health_test_code) {
                            case "0002": $scope.drawBMI(cr); break;
                            case "0003": $scope.drawBiometric(cr); break;
                            case "0005": cr.parameter_lst.length > 0 && ($scope.generalPhysicalTests = cr.parameter_lst, $scope.isGeneralPhysical = true); break;
                            case "0006": $scope.drawEntExamination(cr); break;
                            case "0007": cr.parameter_lst.length > 0 && ($scope.OtherStatsTests = cr.parameter_lst, $scope.isOtherStats = true); break;
                            case "0008": $scope.drawDental(cr); break;
                            case "0010": $scope.drawVision(cr); break;
                        }
                    }), $scope.isDatafound = true)
                    $scope.isLoading = false;
                },
                function (err) {
                    $scope.isLoading = false;
                    $scope.isDatafound = false;
                    debugger
                })
            }
        }])
})();
