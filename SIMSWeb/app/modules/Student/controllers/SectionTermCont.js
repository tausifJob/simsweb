﻿(function () {
    'use strict';
    var obj1;
    var obj2; var temp;
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SectionTermCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.table1 = true;
            $scope.editmode = false;
            $scope.operation = false;
            $scope.edt = {};
            $scope.edt['section_term_status'] = true;
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {
                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            $http.get(ENV.apiUrl + "api/common/SectionTerm/getAllSectionTerm").then(function (res1) {
                $scope.termData = res1.data;
                $scope.table1 = true;
                $scope.result = [];

                $scope.totalItems = $scope.termData.length;
                $scope.todos = $scope.termData;

                $scope.makeTodos();

            });

            $scope.size = function (str) {

                if (str != 'All') {

                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                } else {

                    $scope.pagesize = $scope.termData.length;
                    $scope.currentPage = 1;
                    $scope.numPerPage = $scope.termData.length;
                    $scope.pagesize = 'All'
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            var dom;
            $scope.flag = true;

            $scope.expand = function (info, $event) {

                if (info.section_term6_end_date == undefined) {
                    info.section_term6_end_date = null;
                }
                if (info.section_term5_end_date == undefined) {
                    info.section_term5_end_date = null;
                }
                if (info.section_term4_end_date == undefined) {
                    info.section_term4_end_date = null;
                }
                if (info.section_term3_end_date == undefined) {
                    info.section_term3_end_date = null;
                }

                if (info.section_term6_start_date == undefined) {
                    info.section_term6_start_date = null;
                }
                if (info.section_term5_start_date == undefined) {
                    info.section_term5_start_date = null;
                }
                if (info.section_term4_start_date == undefined) {
                    info.section_term4_start_date = null;
                }
                if (info.section_term3_start_date == undefined) {
                    info.section_term3_start_date = null;
                }

                if (info.section_term2_start_date == undefined) {
                    info.section_term2_start_date = null;
                }
                if (info.section_term2_end_date == undefined) {
                    info.section_term2_end_date = null;
                }

                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='12'>" +
                "<table class='inner-table1' cellpadding='5' cellspacing='0'  style='width:100%;border:solid;border-width:02px'>" +
                "<tbody>" +
                "<tr> <td class='semi-bold'>" + "Term Name " + "</td> <td class='semi-bold'>" + "Start" + " </td><td class='semi-bold'>" + "End" + "</td><td class='semi-bold'>" + " Term Name" + "</td><td class='semi-bold'>" + "Start" + " </td>" +
                "<td class='semi-bold'>" + "End" + "</td><td class='semi-bold'>" + "Term Name" + "</td><td class='semi-bold'>" + "Start " + "</td><td class='semi-bold'>" + "End" + "</td></tr>" +
                "<tr><td>" + (info.section_term2_name) + "</td> <td>" + (info.section_term2_start_date) + " </td><td>" + (info.section_term2_end_date) + "</td><td>" + (info.section_term3_name) + "</td><td>" + (info.section_term3_start_date) + " </td>" +
                "<td>" + (info.section_term3_end_date) + "</td><td>" + (info.section_term4_name) + "</td><td>" + (info.section_term4_start_date) + "</td><td>" + (info.section_term4_end_date) + "</td></tr>" +
                "<tr><td class='semi-bold'>" + "Term Name " + "</td> <td class='semi-bold'>" + "Start" + " </td><td class='semi-bold'>" + "End" + "</td><td class='semi-bold'>" + " Term Name" + "</td><td class='semi-bold'>" + "Start" + " </td>" +
                "<td class='semi-bold'>" + "End" + "</td><td></td><td></td><td></td></tr>" +
                "<tr><td>" + (info.section_term5_name) + "</td> <td>" + (info.section_term5_start_date) + " </td><td>" + (info.section_term5_end_date) + "</td><td>" + (info.section_term6_name) + "</td><td>" + (info.section_term6_start_date) + " </td>" +
                "<td>" + (info.section_term6_end_date) + "</td><td class='semi-bold'>&nbsp;</td><td class='semi-bold'>&nbsp;</td><td class='semi-bold'>&nbsp;</td></tr>" +
                " </table></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    //$(dom).remove();
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }
            }

            $scope.term1edt = function (term_code, term2_code, term3_code, term4_code, term5_code, cur_code, sims_academic_year) {


                $scope.tempterm = [];
                $scope.allterm2 = [];
                $scope.allterm3 = [];
                $scope.allterm4 = [];
                $scope.allterm5 = [];
                $scope.allterm6 = [];
                for (var i = 0; i < $scope.allterm.length; i++) {
                    if ($scope.allterm[i].section_term_code != term_code) {
                        $scope.tempterm.push($scope.allterm[i]);
                    }
                }

                $scope.allterm2 = angular.copy($scope.tempterm);
                $scope.tempterm = [];
                for (var i = 0; i < $scope.allterm.length; i++) {
                    if ($scope.allterm[i].section_term_code != term_code && $scope.allterm[i].section_term_code != term2_code && $scope.allterm[i].section_term_code != undefined && term2_code != undefined && term2_code != '') {
                        $scope.tempterm.push($scope.allterm[i]);
                    }
                }
                $scope.allterm3 = angular.copy($scope.tempterm);


                $scope.tempterm = [];
                for (var i = 0; i < $scope.allterm.length; i++) {
                    if ($scope.allterm[i].section_term_code != term_code && $scope.allterm[i].section_term_code != term2_code && term2_code != undefined && term2_code != '' && $scope.allterm[i].section_term_code != undefined && $scope.allterm[i].section_term_code != term3_code && term3_code != undefined && term3_code != '') {
                        $scope.tempterm.push($scope.allterm[i]);
                    }
                }
                $scope.allterm4 = angular.copy($scope.tempterm);

                $scope.tempterm = [];
                for (var i = 0; i < $scope.allterm.length; i++) {
                    if ($scope.allterm[i].section_term_code != term_code && $scope.allterm[i].section_term_code != term2_code && term2_code != undefined && term2_code != '' && $scope.allterm[i].section_term_code != term3_code && term3_code != undefined && term3_code != '' && $scope.allterm[i].section_term_code != undefined && $scope.allterm[i].section_term_code != term4_code && term4_code != undefined && term4_code != '') {
                        $scope.tempterm.push($scope.allterm[i]);
                    }
                }
                $scope.allterm5 = angular.copy($scope.tempterm);

                $scope.tempterm = [];
                for (var i = 0; i < $scope.allterm.length; i++) {
                    if ($scope.allterm[i].section_term_code != term_code && $scope.allterm[i].section_term_code != term2_code && term2_code != '' && term2_code != undefined && $scope.allterm[i].section_term_code != term3_code && term3_code != undefined && term3_code != '' && $scope.allterm[i].section_term_code != undefined && $scope.allterm[i].section_term_code != term4_code && term4_code != undefined && term4_code != '' && $scope.allterm[i].section_term_code != term5_code && term5_code != undefined && term5_code != '') {
                        $scope.tempterm.push($scope.allterm[i]);
                    }
                }
                $scope.allterm6 = angular.copy($scope.tempterm);

            }

            function getCur(flag, comp_code) {
                if (flag) {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        $scope.currData = res.data;
                        $scope.edt["sims_cur_code"] = $scope.currData[0].sims_cur_code;
                        $scope.currCode($scope.currData[0].sims_cur_code)

                    });
                }
                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        $scope.currData = res.data;
                        $scope.edt["sims_cur_code"] = $scope.currData[0].sims_cur_code;
                        $scope.currCode($scope.currData[0].sims_cur_code)
                    });
                }

            }

            $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.count = res.data;
                if ($scope.count) {
                    getCur(true, $scope.user_details.comp);
                }
                else {
                    getCur(false, $scope.user_details.comp)
                }

            });

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    debugger
                    $scope.newmode1 = true;
                    $scope.newmode2 = true;
                    $scope.newmode3 = true;
                    $scope.term1 = false;
                    $scope.term2 = false;
                    $scope.term3 = false;
                    $scope.editmode = false;
                    $scope.newmode = true;
                    $scope.table1 = false;
                    $scope.operation = true;
                    $scope.savebtn = true;
                    $scope.updatebtn = false;

                    $scope.edt = {
                        section_term_start_date: dd + '-' + mm + '-' + yyyy,
                        section_term2_start_date: dd + '-' + mm + '-' + yyyy,
                        section_term3_start_date: dd + '-' + mm + '-' + yyyy,
                        section_term4_start_date: dd + '-' + mm + '-' + yyyy,
                        section_term5_start_date: dd + '-' + mm + '-' + yyyy,
                        section_term6_start_date: dd + '-' + mm + '-' + yyyy,

                        section_term_end_date: dd + '-' + mm + '-' + yyyy,
                        section_term2_end_date: dd + '-' + mm + '-' + yyyy,
                        section_term3_end_date: dd + '-' + mm + '-' + yyyy,
                        section_term4_end_date: dd + '-' + mm + '-' + yyyy,
                        section_term5_end_date: dd + '-' + mm + '-' + yyyy,
                        section_term6_end_date: dd + '-' + mm + '-' + yyyy

                    }
                    $scope.edt['section_term_status'] = true;

                    if ($scope.count) {
                        getCur(true, $scope.user_details.comp);

                        // console.log($scope.user_details.comp);
                    }
                    else {
                        getCur(false, $scope.user_details.comp)
                        //  $scope.user_details.comp
                    }
                    $scope.Date();
                    //$http.get(ENV.apiUrl + "api/common/SectionTerm/getCuriculum").then(function (res1) {
                    //    $scope.currData = res1.data;
                    //    $scope.edt['sims_cur_code'] = $scope.currData[0].sims_cur_code;
                    //    $scope.currCode($scope.edt.sims_cur_code)
                    //});
                }

            }

            $scope.currCode = function (curr) {
                $scope.curname = curr;
                $http.get(ENV.apiUrl + "api/common/SectionTerm/getAllPriandcurAcademicYear?cur_code=" + curr).then(function (res1) {
                    $scope.years = res1.data;
                    $scope.edt['sims_academic_year'] = $scope.years[0].sims_academic_year;
                    $scope.year($scope.edt.sims_cur_code, $scope.edt.sims_academic_year)
                });
            }

            $scope.year = function (cur, year) {

                $http.get(ENV.apiUrl + "api/common/SectionTerm/getAllCurLevelName?cur_code=" + cur + "&academic_year=" + year).then(function (res1) {
                    $scope.obj1 = res1.data;
                });

                $http.get(ENV.apiUrl + "api/common/SectionTerm/getAllGrades?cur_code=" + cur + "&ac_year=" + year).then(function (res1) {
                    $scope.grades = res1.data;
                });

                $http.get(ENV.apiUrl + "api/common/SectionTerm/getAllTermDescription?cur_code=" + cur + "&academic_year=" + year).then(function (res1) {
                    $scope.allterm = res1.data;
                });
            }

            $scope.getcurlevelgradecode = function (cur, year, cur_level) {

                $http.get(ENV.apiUrl + "api/common/SectionTerm/getAllCurLevelGradeName?cur_code=" + cur + "&academic_year=" + year + "&cur_level_code=" + cur_level).then(function (res1) {
                    $scope.obj = res1.data;
                });
            }

            $scope.getsectioncode = function (cur, year, grade) {

                $http.get(ENV.apiUrl + "api/common/SectionTerm/getAllSections?cur_code=" + cur + "&ac_year=" + year + "&g_code=" + grade).then(function (res1) {
                    $scope.sections = res1.data;
                });


            }
            $scope.allterm = [];
            $scope.up = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                debugger;
                $http.get(ENV.apiUrl + "api/common/SectionTerm/getAllTermDescription?cur_code=" + str.sims_cur_code + "&academic_year=" + str.sims_academic_year).then(function (res1) {
                    $scope.allterm = res1.data;
                    $scope.editmode = true;
                    $scope.newmode = false;
                    $scope.savebtn = false;
                    $scope.updatebtn = true;
                    $scope.table1 = false;
                    $scope.operation = true;
                    $scope.edt = angular.copy(str);

                    $scope.edt['section_term2_name'] = str.section_term2_code;
                    $scope.edt['section_term3_name'] = str.section_term3_code;
                    $scope.edt['section_term4_name'] = str.section_term4_code;
                    $scope.edt['section_term5_name'] = str.section_term5_code;
                    $scope.edt['section_term6_name'] = str.section_term6_code;
                    $scope.edt['section_term_name'] = str.section_term_code;
                    $scope.edt['section_term_status'] = str.section_term_status;
                    $scope.term1edt($scope.edt.section_term_name, $scope.edt.section_term2_name, $scope.edt.section_term3_name, $scope.edt.section_term4_name, $scope.edt.section_term5_name, str.sims_cur_code, str.sims_academic_year);
                });
            }
            }

            function convertdate(dt) {
                if (dt != null) {
                    var d1 = new Date(dt);
                    var month = d1.getMonth() + 1;
                    var day = d1.getDate();
                    if (month < 10)
                        month = "0" + month;
                    if (day < 10)
                        day = "0" + day;
                    var d = d1.month + "/" + (day) + "/" + getFullYear();
                    return d;
                }
            }

            $scope.termonechanged = function (term_code, cur_code, academic_year) {

                $http.get(ENV.apiUrl + "api/common/SectionTerm/getSection_Term?cur_code=" + cur_code + "&academic_year=" + academic_year + "&term_code=" + term_code).then(function (res1) {
                    $scope.termdata = res1.data;
                    for (var i = 0; i < $scope.termdata.length; i++) {
                        if (term_code == $scope.termdata[i].sims_term_code) {
                            $scope.edt.section_term_start_date = $scope.termdata[i].sims_term_start_date;
                            $scope.edt.section_term_end_date = $scope.termdata[i].sims_term_end_date;
                            $scope.edt.section_term2_start_date = '';
                            $scope.edt.section_term2_end_date = '';
                            $scope.edt.section_term3_start_date = '';
                            $scope.edt.section_term3_end_date = '';
                            $scope.edt.section_term4_start_date = '';
                            $scope.edt.section_term4_end_date = '';
                            $scope.edt.section_term5_start_date = '';
                            $scope.edt.section_term5_end_date = '';
                            $scope.edt.section_term6_start_date = '';
                            $scope.edt.section_term6_end_date = '';
                            $scope.edt.section_term2_name = '';
                            $scope.edt.section_term3_name = '';
                            $scope.edt.section_term4_name = '';
                            $scope.edt.section_term5_name = '';
                            $scope.edt.section_term6_name = '';
                        }
                    }

                    $scope.tempterm = [];
                    $scope.allterm2 = [];
                    $scope.allterm3 = [];
                    $scope.allterm4 = [];
                    $scope.allterm5 = [];
                    $scope.allterm6 = [];

                    for (var i = 0; i < $scope.allterm.length; i++) {
                        if ($scope.allterm[i].section_term_code != $scope.edt.section_term_name) {
                            $scope.tempterm.push($scope.allterm[i])
                        }
                    }

                    $scope.allterm2 = $scope.tempterm;
                    $scope.table1 = false;
                    $scope.operation = true;

                });
            }

            $scope.termtwochanged = function (term_code, cur_code, academic_year) {

                $http.get(ENV.apiUrl + "api/common/SectionTerm/getSection_Term?cur_code=" + cur_code + "&academic_year=" + academic_year + "&term_code=" + term_code).then(function (res1) {

                    $scope.termdata = res1.data;

                    for (var i = 0; i < $scope.termdata.length; i++) {

                        if (term_code == $scope.termdata[i].sims_term_code) {
                            $scope.edt.section_term2_start_date = $scope.termdata[i].sims_term_start_date;
                            $scope.edt.section_term2_end_date = $scope.termdata[i].sims_term_end_date;
                            $scope.edt.section_term3_start_date = '';
                            $scope.edt.section_term3_end_date = '';
                            $scope.edt.section_term4_start_date = '';
                            $scope.edt.section_term4_end_date = '';
                            $scope.edt.section_term5_start_date = '';
                            $scope.edt.section_term5_end_date = '';
                            $scope.edt.section_term6_start_date = '';
                            $scope.edt.section_term6_end_date = '';
                        }

                    }

                    $scope.tempterm = [];
                    $scope.allterm3 = [];
                    $scope.allterm4 = [];
                    $scope.allterm5 = [];
                    $scope.allterm6 = [];
                    for (var i = 0; i < $scope.allterm.length; i++) {
                        if ($scope.allterm[i].section_term_code != $scope.edt.section_term_name && $scope.allterm[i].section_term_code != $scope.edt.section_term2_name) {
                            $scope.tempterm.push($scope.allterm[i])
                        }
                    }

                    $scope.allterm3 = $scope.tempterm;

                    $scope.table1 = false;
                    $scope.operation = true;

                });
            }

            $scope.termthreechanged = function (term_code, cur_code, academic_year) {

                $http.get(ENV.apiUrl + "api/common/SectionTerm/getSection_Term?cur_code=" + cur_code + "&academic_year=" + academic_year + "&term_code=" + term_code).then(function (res1) {

                    $scope.termdata = res1.data;

                    for (var i = 0; i < $scope.termdata.length; i++) {
                        if (term_code == $scope.termdata[i].sims_term_code) {
                            $scope.edt.section_term3_start_date = $scope.termdata[i].sims_term_start_date;
                            $scope.edt.section_term3_end_date = $scope.termdata[i].sims_term_end_date;
                            $scope.edt.section_term4_start_date = '';
                            $scope.edt.section_term4_end_date = '';
                            $scope.edt.section_term5_start_date = '';
                            $scope.edt.section_term5_end_date = '';
                            $scope.edt.section_term6_start_date = '';
                            $scope.edt.section_term6_end_date = '';
                        }

                    }


                    $scope.tempterm = [];
                    $scope.allterm4 = [];
                    $scope.allterm5 = [];
                    $scope.allterm6 = [];

                    for (var i = 0; i < $scope.allterm.length; i++) {
                        if ($scope.allterm[i].section_term_code != $scope.edt.section_term_name && $scope.allterm[i].section_term_code != $scope.edt.section_term2_name && $scope.allterm[i].section_term_code != $scope.edt.section_term3_name) {
                            $scope.tempterm.push($scope.allterm[i])
                        }
                    }
                    $scope.allterm4 = $scope.tempterm;

                    $scope.table1 = false;
                    $scope.operation = true;

                });
            }

            $scope.termfourchanged = function (term_code, cur_code, academic_year) {

                $http.get(ENV.apiUrl + "api/common/SectionTerm/getSection_Term?cur_code=" + cur_code + "&academic_year=" + academic_year + "&term_code=" + term_code).then(function (res1) {

                    $scope.termdata = res1.data;

                    for (var i = 0; i < $scope.termdata.length; i++) {
                        if (term_code == $scope.termdata[i].sims_term_code) {
                            $scope.edt.section_term4_start_date = $scope.termdata[i].sims_term_start_date;
                            $scope.edt.section_term4_end_date = $scope.termdata[i].sims_term_end_date;
                            $scope.edt.section_term5_start_date = '';
                            $scope.edt.section_term5_end_date = '';
                            $scope.edt.section_term6_start_date = '';
                            $scope.edt.section_term6_end_date = '';
                        }

                    }


                    $scope.allterm5 = [];
                    $scope.allterm6 = [];
                    $scope.tempterm = [];
                    for (var i = 0; i < $scope.allterm.length; i++) {
                        if ($scope.allterm[i].section_term_code != $scope.edt.section_term_name && $scope.allterm[i].section_term_code != $scope.edt.section_term2_name && $scope.edt.section_term3_name != $scope.allterm[i].section_term_code && $scope.edt.section_term4_name != $scope.allterm[i].section_term_code) {
                            $scope.tempterm.push($scope.allterm[i])
                        }
                    }
                    $scope.allterm5 = $scope.tempterm;

                    $scope.table1 = false;
                    $scope.operation = true;

                });
            }

            $scope.Date = function () {
                debugger;
                setTimeout(function () {
                    $(document).ready(function () {
                        $("#term_1_start_dt, #term_2_start_dt,#term_3_start_dt,#term_4_start_dt,#term_5_start_dt,#term_6_start_dt,#term_1_end_dt,#term_2_end_dt,#term_3_end_dt,#term_4_end_dt,#term_5_end_dt,#term_6_end_dt").kendoDatePicker({ format: "dd-MM-yyyy", value: '' });
                    });
                }, 700);

            }



            $scope.termfivechanged = function (term_code, cur_code, academic_year) {

                $http.get(ENV.apiUrl + "api/common/SectionTerm/getSection_Term?cur_code=" + cur_code + "&academic_year=" + academic_year + "&term_code=" + term_code).then(function (res1) {

                    $scope.termdata = res1.data;

                    for (var i = 0; i < $scope.termdata.length; i++) {
                        if (term_code == $scope.termdata[i].sims_term_code) {
                            $scope.edt.section_term3_start_date = $scope.termdata[i].sims_term_start_date;
                            $scope.edt.section_term5_end_date = $scope.termdata[i].sims_term_end_date;
                            $scope.edt.section_term6_start_date = '';
                            $scope.edt.section_term6_end_date = '';
                        }

                    }
                    $scope.allterm6 = [];
                    $scope.tempterm = [];

                    for (var i = 0; i < $scope.allterm.length; i++) {
                        if ($scope.allterm[i].section_term_code != $scope.edt.section_term_name && $scope.allterm[i].section_term_code != $scope.edt.section_term2_name && $scope.edt.section_term3_name != $scope.allterm[i].section_term_code && $scope.edt.section_term4_name != $scope.allterm[i].section_term_code && $scope.edt.section_term5_name != $scope.allterm[i].section_term_code) {
                            $scope.isterm = true;
                            $scope.tempterm.push($scope.allterm[i])
                        }
                    }
                    $scope.allterm6 = $scope.tempterm;

                    $scope.table1 = false;
                    $scope.operation = true;

                });
            }

            $scope.termsixchanged = function (term_code, cur_code, academic_year) {

                $http.get(ENV.apiUrl + "api/common/SectionTerm/getSection_Term?cur_code=" + cur_code + "&academic_year=" + academic_year + "&term_code=" + term_code).then(function (res1) {

                    $scope.termdata = res1.data;

                    for (var i = 0; i < $scope.termdata.length; i++) {
                        if (term_code == $scope.termdata[i].sims_term_code) {
                            $scope.edt.section_term6_start_date = $scope.termdata[i].sims_term_start_date;
                            $scope.edt.section_term6_end_date = $scope.termdata[i].sims_term_end_date;
                        }

                    }
                    $scope.table1 = false;
                    $scope.operation = true;

                });
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.edt = {};
            }

            $scope.Save = function () {


                if ($scope.edt.section_term_name == undefined && $scope.edt.section_term2_name == undefined && $scope.edt.section_term3_name == undefined && $scope.edt.section_term4_name == undefined && $scope.edt.section_term5_name == undefined && $scope.edt.section_term6_name == undefined) {
                    $scope.operation = true;
                    $scope.table1 = false;
                    swal({ text: 'Record Cannot be Insert', width: 300, showCloseButton: true });
                }
                else {

                    $scope.operation = false;

                    $http.post(ENV.apiUrl + "api/common/SectionTerm/InsertSims_Section_Term?simsobj=" + JSON.stringify($scope.edt)).then(function (msg) {

                        $scope.msg1 = msg.data;
                        swal({ text: $scope.msg1.strMessage, width: 300, showCloseButton: true });

                        $http.get(ENV.apiUrl + "api/common/SectionTerm/getAllSectionTerm").then(function (AllSectionTerm) {
                            $scope.termData = AllSectionTerm.data;
                            $scope.table1 = true;
                            $scope.result = [];
                            $scope.totalItems = $scope.termData.length;
                            $scope.todos = $scope.termData;
                            $scope.makeTodos();
                            $scope.operation = false;

                        });

                    });
                    $scope.table1 = true;
                    $scope.operation = false;
                }
            }

            $scope.Update = function () {
                debugger;
                $scope.operation = false;
                
                //if ($scope.edt.section_term_name.includes("0")) {
                //    $scope.edt.section_term_name= $scope.edt.section_term_name.substring(1,$scope.edt.section_term_name.length)
                //}
                //if ($scope.edt.section_term2_name.includes("0")) {
                //    $scope.edt.section_term2_name = $scope.edt.section_term2_name.substring(1, $scope.edt.section_term2_name.length)
                //}
                //if ($scope.edt.section_term3_name.includes("0")) {
                //    $scope.edt.section_term3_name=$scope.edt.section_term3_name.substring(1, $scope.edt.section_term3_name.length)
                //}
                //if ($scope.edt.section_term4_name.includes("0")) {
                //    $scope.edt.section_term4_name=$scope.edt.section_term4_name.substring(1, $scope.edt.section_term4_name.length)
                //}
                //if ($scope.edt.section_term5_name.includes("0")) {
                //    $scope.edt.section_term5_name=$scope.edt.section_term5_name.substring(1, $scope.edt.section_term5_name.length)
                //}
                //if ($scope.edt.section_term6_name.includes("0")) {
                //    $scope.edt.section_term6_name=$scope.edt.section_term6_name.substring(1, $scope.edt.section_term6_name.length)
                //}
                $http.post(ENV.apiUrl + "api/common/SectionTerm/Update_section_term?gradeobj=" + JSON.stringify($scope.edt)).then(function (msg) {
                    $scope.msg1 = msg.data;
                    swal({ text: $scope.msg1.strMessage, width: 300, showCloseButton: true });
                    $http.get(ENV.apiUrl + "api/common/SectionTerm/getAllSectionTerm").then(function (AllSectionTerm) {
                        $scope.termData = AllSectionTerm.data;
                        $scope.result = [];
                        $scope.totalItems = $scope.termData.length;
                        $scope.todos = $scope.termData;
                        $scope.makeTodos();
                        $scope.table1 = true;
                        $scope.operation = false;
                    })
                });
                $scope.operation = false;
                $scope.table1 = true;

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.deletemultiple();
                }
            };

            $scope.deletemultiple = function () {

                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if (main.checked == true) {
                        $scope.filteredTodos[i].ischecked = true;
                        $('tr').addClass("row_selected");
                    }
                    else {
                        main.checked = false;
                        $('tr').removeClass("row_selected");
                        $scope.filteredTodos[i].ischecked = false;
                    }
                }
            }

            $scope.singledelete = function (str) {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
                var v = document.getElementById(str);
                if (v.checked == true) {
                    del.push({ id: str });
                }
                else {
                    v.checked = false;
                    var index = del.indexOf(str);
                    del.splice(index, 1);
                }

            }

            $scope.deleterecord = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    $scope.deletedatasend = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        if ($scope.filteredTodos[i].ischecked == true) {
                            $scope.flag = true;
                            $scope.deletedatasend.push($scope.filteredTodos[i]);
                        }
                    }

                    if ($scope.flag == true) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $http.post(ENV.apiUrl + "api/common/SectionTerm/cudSims_Section_Term", $scope.deletedatasend).then(function (msg) {

                                    $scope.msg1 = msg.data;
                                    $scope.table1 = true;
                                    $scope.operation = false;
                                    swal({ text: $scope.msg1.strMessage, width: 300, showCloseButton: true });
                                    $http.get(ENV.apiUrl + "api/common/SectionTerm/getAllSectionTerm").then(function (AllSectionTerm) {
                                        $scope.termData = AllSectionTerm.data;
                                        $scope.result = [];
                                        del = [];
                                        $scope.totalItems = $scope.termData.length;
                                        $scope.todos = $scope.termData;
                                        $scope.makeTodos();

                                    })
                                });
                            }
                            else {

                                main = document.getElementById('mainchk');
                                main.checked = false;
                                $scope.deletemultiple();

                            }
                        })
                    }
                    else {
                        swal({ text: 'Select At Least One Record To Delete', width: 380, showCloseButton: true });
                    }
                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
                //format: 'dd-mm-yyyy'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.termData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.termData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.section_term_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_section_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }


            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            // $scope.sdate = dd + '-' + mm + '-' + yyyy;
            //$scope.edate = dd + '-' + mm + '-' + yyyy;
            debugger
            $scope.edt = {
                section_term_start_date: dd + '-' + mm + '-' + yyyy,
                section_term2_start_date: dd + '-' + mm + '-' + yyyy,
                section_term3_start_date: dd + '-' + mm + '-' + yyyy,
                section_term4_start_date: dd + '-' + mm + '-' + yyyy,
                section_term5_start_date: dd + '-' + mm + '-' + yyyy,
                section_term6_start_date: dd + '-' + mm + '-' + yyyy,

                section_term_end_date: dd + '-' + mm + '-' + yyyy,
                section_term2_end_date: dd + '-' + mm + '-' + yyyy,
                section_term3_end_date: dd + '-' + mm + '-' + yyyy,
                section_term4_end_date: dd + '-' + mm + '-' + yyyy,
                section_term5_end_date: dd + '-' + mm + '-' + yyyy,
                section_term6_end_date: dd + '-' + mm + '-' + yyyy

            }


         }])
})();
