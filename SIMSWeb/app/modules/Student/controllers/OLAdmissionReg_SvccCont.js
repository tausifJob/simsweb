﻿
(function () {
    'use strict';
    var del = [];
    var main;
    var date1, date3, date4;

    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('OLAdmissionReg_SvccCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {
           
            $scope.save1 = true;

            $scope.Save = function (isvalidate) {

                var data2 = [];

                if (isvalidate) 
                {
                   // 
                    
                    var data = ({
                        sims_admission_parent_first_name: $scope.edt.sims_admission_parent_first_name,
                        prospect_no: $scope.edt.prospect_no,
                        sims_admission_parent_national_id: $scope.edt.sims_admission_parent_national_id,
                        sims_admission_parent_mobile:$scope.edt.sims_admission_parent_mobile,
                        opr: 'R'
                    });

                    $http.post(ENV.apiUrl + "api/common/Admission/CheckAdharcardID",data).then(function (res) {
                        if (res.data == "true")
                        {
                            $scope.edt.sims_admission_parent_national_id = "";
                            $scope.edt.prospect_no = "";
                            $scope.edt.sims_admission_parent_first_name = "";
                            swal({  text: "Already Exist", width: 380, imageUrl: "assets/img/notification-alert.png" });
                            return;
                            $scope.BUSY = false;
                        }
                        else
                        {
                            if (res.data == "false")
                            {
                                $scope.BUSY = true;
                                $http.post(ENV.apiUrl + "api/common/Admission/Register?data=" + JSON.stringify(data)).then(function (res) {
                                       // $scope.display = true;
                                        $scope.msg1 = res.data;
                                        $scope.BUSY = false;


                                        if ($scope.msg1.length > 0)
                                        {
                                            $scope.edt = [];
                                            swal({ text: "Student registration successful.", imageUrl: "assets/img/check.png" });
                                            return;

                                        }
                                 });
                            }
                            $scope.myForm.$setPristine();
                            $scope.myForm.$setUntouched();
                        }
                    });
                    
                }
            }

            $scope.check1 = function () {
                if ($scope.edt.sibling_status == true) {
                    $scope.parent_edit = false;
                    $scope.Chk_parent = true;
                    $scope.chk_searchshow = true;
                    $scope.chk_show = false;
                }
                else {
                    $scope.parent_edit = true;
                    $scope.Chk_parent = false;
                    $scope.chk_searchshow = false;
                    $scope.chk_show = true;
                }
            }

            $scope.birthdate = function (birth_date)
            {
                var d1 = moment(birth_date, "DD-MM-YYYY").format('YYYY-MM-DD');
                var d2 = moment($scope.ddMMyyyy, "DD-MM-YYYY").format('YYYY-MM-DD');

                if (d1 >= d2)
                {

                    swal({ title: "Date", text: "DOB should be less than the Current date.", showCloseButton: true, width: 450, });
                    $scope.edt.birth_date = "";
                }
            }

            $scope.Cancel = function () {
                $scope.edt = [];
                $scope.msg1 = [];
                var data2 = [];
                $scope.BUSY = false;
                $scope.Chk_parent = false;
                $scope.chk_searchshow = false;
                $scope.chk_show = true;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

                $scope.edt =
            {
                admission_date: $scope.ddMMyyyy,
                tent_join_date: $scope.ddMMyyyy,
                comm_date: $scope.ddMMyyyy,
            }

                $scope.chkgetAdmissionList();
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


        }])
})();