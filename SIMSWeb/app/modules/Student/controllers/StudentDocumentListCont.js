﻿/// <reference path="../views/StudentDocumentList.html" />
(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, modulecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('StudentDocumentListCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.table = false;
            $scope.table1 = false;
            $scope.pagesize = "5";
            $scope.pageindex = 0;

            $timeout(function () {
                $("#example").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                debugger;
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear($scope.edt.sims_cur_code);
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                    $scope.getGrade(curCode, $scope.temp.sims_academic_year);
                });
            }

            $scope.getGrade = function (curCode, accYear) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
            }

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }

            $scope.Show_Data = function (str, str1) {
                debugger;
                if ((str != undefined && str1 != undefined) || $scope.edt.sims_cur_code != "" && $scope.temp.sims_academic_year != "") {
                    $http.get(ENV.apiUrl + "api/StudentReport/getStudentDocumentList?cur_name=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grd_code=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&student_en=" + $scope.edt.sims_student_enroll_number).then(function (res) {
                        $scope.studlist = res.data;
                        $scope.totalItems = $scope.studlist.length;
                        $scope.todos = $scope.studlist;
                        $scope.makeTodos();
                        if ($scope.studlist.length == 0) {
                            swal({ title: 'Alert', text: "Please Try Again Record Not Found...", showCloseButton: true, width: 450, height: 200 });
                            $scope.ShowCheckBoxes = true;
                        }
                        else {
                            $scope.table = true;
                            $scope.table1 = true;
                        }
                    })
                }

                else {
                    swal({ title: 'Alert', text: "Please Select Curriculum & Academic Year Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                }
            }

            $scope.Reset = function () {
                $scope.edt = {
                    sims_cur_code: '',
                    sims_grade_code: '',
                    sims_section_code: '',
                }
                $scope.temp = {
                    sims_academic_year: '',
                }
                $scope.table = false;
                $scope.table1 = false;
            }

            $scope.downloaddoc = function (str) {
                debugger;
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Docs/Student/' + str;
                window.open($scope.url);
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                debugger;
                $scope.todos = $scope.searched($scope.studlist, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.studlist;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                return (item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_section_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sager == toSearch) ? true : false;
            }

         }])
})();
