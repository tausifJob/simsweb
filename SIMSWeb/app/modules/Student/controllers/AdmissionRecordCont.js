﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AdmissionRecordCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.pagesize = "All";
            $scope.pageindex = 0;
            $scope.display = false;
            $scope.table1 = true;
            $scope.fee_no = true;
            $scope.table = true;
            $scope.visit_no = true;
            var check_no = null;
            $scope.busy = false;
            $scope.chku = false;
            $scope.edt = [];

            //CUR
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.temp = {
                    'sims_cur_code': $scope.curriculum[0].sims_cur_code
                }
                $scope.getAccYear($scope.temp.sims_cur_code)
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;

                    $scope.temp = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                        'sims_academic_year': $scope.Acc_year[0].sims_academic_year
                    }
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                });

            }



            //Grade
            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/AdmissionSubject/getAllGrades?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&user_name=" + $rootScope.globals.currentUser.username).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
                $scope.getSection($scope.temp.sims_cur_code, $scope.temp.sims_grade_code, $scope.temp.sims_academic_year)
            }

            //Section
            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/AdmissionSubject/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear + "&user_name=" + $rootScope.globals.currentUser.username).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }


            //Academic year
            //$http.get(ENV.apiUrl + "api/StudentReport/getAcademicYearActive").then(function (AllacademicActive) {
            //    $scope.getAllactive = AllacademicActive.data;
            //    if (AllacademicActive.data.length > 0) {
            //        $scope.edt = {
            //            'sims_academic_years': $scope.getAllactive[0].sims_academic_years
            //        }
            //    }
            //});



            $scope.Submit = function () {

                $scope.promotegrade = true;
                $scope.newpromotegrade = false;
                $http.get(ENV.apiUrl + "api/common/AdmissionSubject/Get_subjectMapping?cur_code=" + $scope.temp.sims_cur_code + "&acad_yr=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code).then(function (AllTermDetails) {
                    $scope.getAllTerm_details = AllTermDetails.data;
                    $scope.totalItems = $scope.getAllTerm_details.length;
                    $scope.todos = $scope.getAllTerm_details;
                    $scope.makeTodos();
                    $scope.size($scope.pagesize);

                    $http.get(ENV.apiUrl + "api/common/AdmissionSubject/Get_secondlangDetails?cur_code=" + $scope.temp.sims_cur_code + "&acad_yr=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code).then(function (secondlang) {
                        $scope.secondlang_detail = secondlang.data;
                    });

                    $http.get(ENV.apiUrl + "api/common/AdmissionSubject/Get_thirdlangDetails?cur_code=" + $scope.temp.sims_cur_code + "&acad_yr=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code).then(function (thirdlang) {
                        $scope.thirdlang_detail = thirdlang.data;
                    });
                    //$scope.getCombination=function(year)
                });
            }

            $scope.size = function (str) {

                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.getAllTerm_details;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.getAllTerm_details, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.getAllTerm_details;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.std_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;

            }

            $scope.New = function () {

                $scope.temp = '';
                $scope.edt = '';
                $scope.display = true;
                $scope.table = false;
                $scope.table1 = false;
                $scope.instaminimuma = true;
                $scope.update1 = false;

                $scope.Update_btn = false;
                $scope.show_btn = true;


            }


            $scope.savedatapromote = function () {


                //var deleteintcode = [];
                var Savedata = [];
                $scope.insert = false;
                // $scope.filteredTodos = [];

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    // if ($scope.filteredTodos[i].status == true) {
                    var v = document.getElementById("enroll_number-" + i);
                    if (v.checked == true) {
                        $scope.busy = true;
                        var deleteintcode = ({
                            'sims_cur_code': $scope.temp.sims_cur_code,
                            'sims_academic_year': $scope.temp.sims_academic_year,
                            'sims_sims_grades': $scope.temp.sims_grade_code,
                            'sims_sims_section': $scope.temp.sims_section_code,
                            'sims_enrollment_number': $scope.filteredTodos[i].enroll_number,
                            //'sims_academic_year_old': $scope.temp.sims_academic_years,
                            'sims_student_attribute1': $scope.filteredTodos[i].sims_subject_code,
                            'sims_student_attribute2': $scope.filteredTodos[i].sims_admission_third_lang_code,
                            'sims_student_attribute3': $scope.filteredTodos[i].sims_student_attribute3,
                            'sims_student_attribute4': $scope.filteredTodos[i].sims_student_attribute4,
                            opr: 'I',
                        });
                        $scope.insert = true;

                        Savedata.push(deleteintcode);
                    }

                }
                if ($scope.insert) {
                    $http.post(ENV.apiUrl + "api/common/AdmissionSubject/CUDsubject", Savedata).then(function (msg) {

                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully.", imageUrl: "assets/img/check.png", width: 380, height: 200 });
                            $scope.busy = false;
                            deleteintcode = [];
                            Savedata = [];
                            $scope.Submit();
                        }
                        else {
                            swal({ text: "Record Already Mapped.", imageUrl: "assets/img/notification-alert.png", width: 380, height: 200 });
                            $scope.busy = false;
                        }

                    });
                }
                else {
                    swal({ title: "Alert", text: "Please select Atleast one record.", width: 380, height: 200 });
                    return;
                }
            }



            $scope.CheckAllChecked = function () {
                debugger
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("enroll_number-" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("enroll_number-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }


            $scope.checkonebyonedelete = function (str) {
                debugger
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                        //  str.status = true;
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                        //str.status = false;
                    }
                });
                str.status = str.datastatus;
            }



            $scope.Cancel = function () {
                // $scope.edt = '';
                // $scope.temp = '';
                $scope.temp.sims_grade_code = '';
                $scope.temp.sims_section_code = '';
                $scope.getAllTerm_details = [];
                $scope.filteredTodos = [];
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

         }])
})();
