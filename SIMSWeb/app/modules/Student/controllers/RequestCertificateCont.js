﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var deletecode = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('RequestCertificateCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.pager = true;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.enrollno = false;
            $scope.display = false;
            $scope.table = true;
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100)

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100)

            $timeout(function () {
                $("#fixTable2").tableHeadFixer({ 'top': 1 });
            }, 100)

            $timeout(function () {
                $("#fixTable3").tableHeadFixer({ 'top': 1 });
            }, 100)

            $timeout(function () {
                $("#fixTable4").tableHeadFixer({ 'top': 1 });
            }, 100)

            var user = $rootScope.globals.currentUser.username;

            //$('*[data-datepicker="true"] input[type="text"]').datepicker({
            //    todayBtn: true,
            //    orientation: "top left",
            //    autoclose: true,
            //    todayHighlight: true,
            //    format: "yyyy-mm-dd"
            //});

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/RequestCertificate/GetRequestCertificate").then(function (res1) {

                $scope.CreDiv = res1.data;
                $scope.totalItems = $scope.CreDiv.length;
                $scope.todos = $scope.CreDiv;
                $scope.makeTodos();
            });

            $http.get(ENV.apiUrl + "api/RequestCertificate/GetAllCertificate").then(function (certificate) {
                $scope.certificate_no = certificate.data;
            });

            $http.get(ENV.apiUrl + "api/RequestCertificate/GetAllUserType").then(function (user) {
                $scope.usertype = user.data;
            });

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.CreDiv;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search1 = function () {
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_certificate_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_certificate_requested_by.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
                $scope.disabled = false;
                $scope.temp = {};
                $scope.global_search_result_lst = {};
                $scope.global_search_result = {};
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.search_btn = false;
                $scope.u_id = false;
                $scope.id_label = false;
                $scope.id_label1 = false;
                $scope.enroll_no = false;
                $scope.search_btn1 = false;
                $scope.divcode_readonly = false;
                var datepicker = $("#cmb_create_date").data("kendoDatePicker");
                datepicker.readonly(false);
                $scope.enrollno = false;
                
                var dt = new Date();
                var datepicker = $("#cmb_create_date").data("kendoDatePicker");
                datepicker.readonly(false);
                $scope.temp.sims_certificate_request_date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                $scope.temp.sims_certificate_expected_date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                //$scope.temp = "";
                //$scope.temp = {};
                //$scope.temp['sims_library_attribute_status'] = true;
            }

            //View user buttons
            $scope.userselect = function () {
                debugger;
                
                if ($scope.temp.user_type == 'Student') {
                    //$scope.search_btn1 = true;
                    //$scope.search_btn = false;
                    $scope.studflg = false;
                    $scope.empflg = true;
                    $scope.parentflg = true;                    
                    
                    $scope.enroll_no = true;
                    $scope.u_id = false;
                    $scope.id_label = false;
                    $scope.id_label1 = true;
                    
                    $scope.student_class = true;
                    $scope.parent_class = false;   
                    $scope.emp_class = false;

                    $('#eid').removeClass('active');                    
                    $('#tab6').removeClass('active');
                    $('#pid').removeClass('active');
                    $('#tab2').removeClass('active');                     
                    $('#tab1').addClass('active');
                    $('#sid').addClass('active');

                    $scope.global_parent_table = false;
                    $scope.global_student_table = true;
                }
                if ($scope.temp.user_type == 'Employee') {

                    $scope.studflg = true;
                    $scope.parentflg = true;
                    $scope.empflg = false;
                    $scope.enroll_no = false;
                    
                    $scope.u_id = true;
                    $scope.id_label = true;
                    $scope.id_label1 = false;

                    $scope.student_class = false;
                    $scope.parent_class = false;
                    $scope.emp_class = true;

                    $('#sid').removeClass('active');
                    $('#tab1').removeClass('active');
                    $('#pid').removeClass('active');                    
                    $('#tab2').removeClass('active');                    
                    $('#eid').addClass('active');
                    $('#tab6').addClass('active');
                    $scope.global_parent_table = true;
                    $scope.global_student_table = false;
                }
                if ($scope.temp.user_type == 'Parent') {
                    debugger;
                    $scope.studflg = true;
                    $scope.parentflg = false;
                    $scope.empflg = true;
                    $scope.enroll_no = false;
                    
                    $scope.u_id = true;
                    $scope.id_label = true;
                    $scope.id_label1 = false;

                    $scope.student_class = false;
                    $scope.parent_class = true;
                    $scope.emp_class = false;

                    $('#sid').removeClass('active');
                    $('#tab1').removeClass('active');
                    $('#pid').addClass('active');
                    $('#tab2').addClass('active');
                    $('#eid').removeClass('active');
                    $('#tab6').removeClass('active');
                    $scope.global_parent_table = true;
                    $scope.global_student_table = false;
                }
               
            }

            $scope.RemoveEnrollMentNo = function ($event, index, str) {
                str.splice(index, 1);
            }

            //Select Student Enroll No


            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.search_btn = false;
                $scope.u_id = true;
                $scope.id_label = true;
                $scope.id_label1 = true;
                $scope.enroll_no = true;
                $scope.search_btn1 = false;
                $scope.enrollno = false;
                $scope.divcode_readonly = true;

                var datepicker = $("#cmb_create_date").data("kendoDatePicker");
                datepicker.readonly();
                
                var datepicker = $("#cmb_create_date").data("kendoDatePicker");
                datepicker.readonly();

                $scope.temp = {
                    sims_certificate_request_number: str.sims_certificate_request_number,
                    sims_certificate_number: str.sims_certificate_number,
                    sims_certificate_name: str.sims_certificate_name,
                    sims_certificate_request_date: str.sims_certificate_request_date,
                    sims_certificate_expected_date: str.sims_certificate_expected_date,
                    user_type: str.user_type,
                    sims_certificate_requested_by: str.sims_certificate_requested_by,
                    sims_certificate_enroll_number: str.sims_certificate_enroll_number,
                    sims_certificate_reason: str.sims_certificate_reason
                };
            }

            //DATA SAVE INSERT
            var datasend = [];
            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                debugger
                $scope.ComboBoxValues = AllComboBoxValues.data;
                $scope.global_Search = {
                    global_acdm_yr: $scope.ComboBoxValues[0].sims_acadmic_year,
                    global_curriculum_code: $scope.ComboBoxValues[0].sims_cur_code,
                };
            });

            $scope.global_Search = {};
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                debugger
                $scope.cmbCur = res.data;
                if (res.data.length > 0) {
                    $scope.global_Search.global_curriculum_code = res.data[0].sims_cur_code;
                    $scope.getAcadm_yr($scope.global_Search.global_curriculum_code);
                }
            })

            $scope.getAcadm_yr = function (cur_code) {
                $scope.curriculum_code = cur_code;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code).then(function (res) {
                    $scope.Acdm_year = res.data; 
                    if (res.data.length > 0) {
                        $scope.global_Search.global_acdm_yr = res.data[0].sims_academic_year;
                    }
                    $scope.getGrade(cur_code, res.data[0].sims_academic_year)
                    console.log($scope.Acdm_year);
                });
            }
            $scope.getGrade = function (str1, str2) {
                //for (var i = 0; i < $scope.AcademicYear.length; i++) {
                //    if ($scope.AcademicYear[i].sims_academic_year == str2) {
                //        year_desc = $scope.AcademicYear[i].sims_academic_year_desc
                //    }
                //}


                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str1 + "&academic_year=" + str2).then(function (getAllGrades) {
                    $scope.All_grade_names = getAllGrades.data;
                    $scope.getsection(str1, $scope.All_grade_names[0].sims_grade_code, str2);
                })

            }

            $scope.getsection = function (str, str1, str2) {

                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + str + "&grade_code=" + str1 + "&academic_year=" + str2).then(function (Allsection) {
                    $scope.All_Section_names = Allsection.data;
                })
            };
            $scope.modal_cancel = function () {
                debugger;
                  $scope.All_grade_names = '';
                $scope.All_Nationality_names = '';
                  $scope.Cur_Names = '';
                $scope.glbl_obj = '';
                $scope.global_Search = '';
                $scope.global_search_result = '';
                $scope.global_Search = "";
            }

            $scope.global_Search = "";

            $scope.Global_Search_by_student = function () {
                //debugger

                $scope.global_student_table = false;
                $scope.busy = true;
                if ($scope.global_Search == "") {
                    $scope.glbl_obj = $scope.global_Search;
                }
                else {
                    $scope.glbl_obj = {
                        s_cur_code: $scope.global_Search.global_curriculum_code,
                        search_std_enroll_no: $scope.global_Search.glbl_enroll_num,
                        search_std_name: $scope.global_Search.glbl_name,
                        search_std_grade_name: $scope.global_Search.gradecode,
                        search_std_section_name: $scope.global_Search.sectioncode,
                        search_std_passport_no: $scope.global_Search.glbl_pass_num,
                        search_std_family_name: $scope.global_Search.glbl_family_name,
                        sims_academic_year: $scope.global_Search.global_acdm_yr,
                        sims_nationality_name_en: $scope.global_Search.nationalitycode,
                        std_national_id: $scope.global_Search.nationalityID
                    }
                }
                //myarray = []; $scope.global_search_result = "";

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.glbl_obj)).then(function (res) {
                    debugger
                    $scope.global_search_result_lst = res.data; console.log($scope.global_search_result_lst);
                    $scope.global_student_table = true;
                    $scope.busy = false;
                });
            }

            $scope.Global_Search_by_parent = function () {

                $scope.global_parent_table = false;
                $scope.busy = true;
                //$scope.add_btn = true;
                $scope.searchbyparent = {
                    search_std_grade_name: $scope.global_Search.gradecode2,
                    search_std_section_name: $scope.global_Search.sectioncode2,
                    search_parent_id: $scope.global_Search.search_parent_id,
                    search_parent_name: $scope.global_Search.search_parent_name,
                    search_parent_email_id: $scope.global_Search.search_parent_email_id,
                    search_parent_mobile_no: $scope.global_Search.search_parent_mobile_no
                }

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchParent?data=" + JSON.stringify($scope.searchbyparent)).then(function (res) {
                    $scope.global_search_result = res.data;
                    $scope.global_parent_table = true;
                    $scope.busy = false;
                    console.log($scope.global_search_result);
                });
            }

            $scope.Global_Search_SearchParentSectionWise = function () {
                $scope.global_parent_table = false;
                $scope.busy = true;

                $scope.searchbyparent = {
                    search_std_grade_name: $scope.global_Search.gradecode2,
                    search_std_section_name: $scope.global_Search.sectioncode2,
                }

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchParentSectionWise?data=" + JSON.stringify($scope.searchbyparent)).then(function (res) {
                    $scope.global_search_result = res.data;
                    $scope.global_parent_table = true;
                    $scope.busy = false;
                    console.log($scope.global_search_result);
                });
            }

            $scope.Global_Search_by_Teacher = function () {
                $scope.global_teacher_table = false;
                $scope.busy = true;

                $scope.searchbyparent = {
                    search_teacher_id: $scope.global_Search.search_teacher_id,
                    search_teacher_name: $scope.global_Search.search_teacher_name
                }

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchTeacher?data=" + JSON.stringify($scope.searchbyparent)).then(function (res) {
                    $scope.global_search_result = res.data;
                    $scope.global_teacher_table = true;
                    $scope.busy = false;
                    console.log($scope.global_search_result);
                });
            }

            $scope.Global_Search_by_User = function () {
                $scope.Allusers = [];
                $scope.currentPage = 0;
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchUser?data=" + JSON.stringify($scope.temp1)).then(function (users) {
                    $scope.Allusers = users.data;
                    $scope.table = true;
                    $scope.busy = false;
                })
            }

            $scope.Global_Search_by_employee = function () {
                $scope.NodataEmployee = false;
                $http.post(ENV.apiUrl + "api/common/GlobalSearch/SearchEmployee?data=" + JSON.stringify($scope.temp)).then(function (SearchEmployee_Data) {
                    $scope.EmployeeDetails = SearchEmployee_Data.data;
                    console.log($scope.EmployeeDetails);
                    $scope.EmployeeTable = true;

                    if (SearchEmployee_Data.data.length > 0) {
                        //   $scope.EmployeeTable = true;
                    }
                    else {
                        $scope.NodataEmployee = true;
                        //  $scope.EmployeeTable = false;
                    }
                });
            }

            $scope.search = function () {
                $scope.userselect();             
                $('#Global_Search_Modal1').modal('show');
            }

            $scope.search_stud = function () {
                debugger;
                $scope.userselect();
                //$('#tab1').addClass('active');
                //$('#tab2').removeClass('active');
                //$('#tab6').removeClass('active');

                //$scope.studflg = false;
                //$scope.empflg = true;
                //$scope.parentflg = true;
                $scope.global_Search = {};
                try {
                    if ($scope.cmbCur.length > 0) {
                        $scope.global_Search.global_curriculum_code = $scope.cmbCur[0].sims_cur_code;
                        $scope.getAcadm_yr($scope.global_Search.global_curriculum_code);
                    }

                    if ($scope.Acdm_year.length > 0) {
                        $scope.global_Search.global_acdm_yr = $scope.Acdm_year[0].sims_academic_year;
                    }
                } catch (e) {

                }
                $('#Global_Search_Modal1').modal('show');
                
            }

            $scope.Gbl_checkbox_click = function () {
                debugger
                for (var j = 0; j < $scope.global_search_result_lst.length; j++) {
                    var v = document.getElementById($scope.global_search_result_lst[j].s_enroll_no);
                    if (v.checked == true) {
                        $scope.temp.sims_certificate_enroll_number = $scope.global_search_result_lst[j].s_enroll_no;
                    }
                }
                $scope.glbl_obj = '';
                $scope.global_Search = '';
                $scope.global_search_result_lst = '';
                $('#Global_Search_Modal1').modal('hide');
            }
            //$scope.Studentdata = [];
            $scope.Gbl_checkbox_click_parent = function (info) {
                //    $scope.Studentdata.push(info);
                //    $scope.glbl_obj = '';
                //    $scope.global_Search = '';
                //    $scope.global_search_result = '';
                //    $('#Global_Search_Modal').modal('hide');
            }

            $scope.Studentdata = [];
            $scope.Add_parent = function () {
                debugger
                $scope.enrollno = true;
                $scope.Studentdata = [];
                for (var i = 0; i < $scope.global_search_result.length; i++) {
                    var a = document.getElementById($scope.global_search_result[i].s_parent_id + i)
                    if (a.checked == true) {
                        $scope.Studentdata.push($scope.global_search_result[i]);
                        $scope.temp.sims_certificate_requested_by = $scope.global_search_result[i].s_parent_id;
                    }
                }
                $scope.glbl_obj = '';
                $scope.global_Search = '';
                $scope.global_search_result = '';
                $('#Global_Search_Modal1').modal('hide');
            }

            $scope.Gbl_checkbox_click_emp = function () {
                for (var j = 0; j < $scope.EmployeeDetails.length; j++) {
                    var v = document.getElementById($scope.EmployeeDetails[j].em_login_code);
                    if (v.checked == true) {
                        $scope.temp.sims_certificate_requested_by = $scope.EmployeeDetails[j].em_login_code;
                        // $scope.temp.sims_certificate_enroll_number = $scope.global_search_result[j].s_enroll_no;
                    }
                }
                $scope.id_label1 = false;
                $scope.enroll_no = false;
                $scope.search_btn1 = false;
                $scope.glbl_obj = '';
                $scope.global_Search = '';
                $scope.global_search_result = '';
                $('#Global_Search_Modal1').modal('hide');
            }

            //Insert Record
            $scope.savedata = function (Myform) {
                debugger
                if (Myform) {
                    
                    if ($scope.temp.sims_certificate_number == "" || $scope.temp.sims_certificate_number == null || $scope.temp.sims_certificate_number =='undefined') {
                        swal('', 'Please Select Required Certificate');
                        return;
                    }
                    //if ($scope.temp.sims_certificate_expected_date > $scope.temp.sims_certificate_request_date) {
                    //    swal('', 'Expected date must be after requested date');
                    //    return;
                    //}
                    else if ($scope.temp.user_type == 'Student' && $scope.temp.sims_certificate_enroll_number == undefined) {
                        swal('', 'Please Select Enroll No');
                        return;
                    }
                    else if ($scope.temp.user_type == 'Parent' && $scope.temp.sims_certificate_requested_by == undefined) {
                        swal('', 'Please Select Requested by and ID');
                        return;
                    }
                    else if ($scope.temp.user_type == 'Employee' && $scope.temp.sims_certificate_requested_by == undefined) {
                        swal('', 'Please Select Requested by and ID');
                        return;
                    }

                    
                        if ($scope.temp.user_type == 'Parent') {
                            for (var j = 0; j < $scope.Studentdata.length; j++) {
                                var code = {
                                    opr: 'I',
                                    sims_certificate_request_date: $scope.temp.sims_certificate_request_date,
                                    sims_certificate_requested_by: $scope.temp.sims_certificate_requested_by,
                                    sims_certificate_number: $scope.temp.sims_certificate_number,
                                    sims_certificate_reason: $scope.temp.sims_certificate_reason,
                                    sims_certificate_expected_date: $scope.temp.sims_certificate_expected_date,
                                    sims_certificate_enroll_number: $scope.Studentdata[j].s_enroll_no,
                                    sims_certificate_issued_by: user,
                                    sims_certificate_request_status: 'I',
                                }
                                datasend.push(code);
                            }
                            
                        }
                        if ($scope.temp.user_type == 'Employee') {
                            var data = {
                                opr: 'I',
                                //sims_certificate_request_number: $scope.temp.sims_certificate_request_number,
                                sims_certificate_request_date: $scope.temp.sims_certificate_request_date,
                                sims_certificate_requested_by: $scope.temp.sims_certificate_requested_by,
                                sims_certificate_number: $scope.temp.sims_certificate_number,
                                sims_certificate_reason: $scope.temp.sims_certificate_reason,
                                sims_certificate_expected_date: $scope.temp.sims_certificate_expected_date,
                                sims_certificate_enroll_number: $scope.temp.sims_certificate_enroll_number,
                                sims_certificate_issued_by: user,
                                sims_certificate_request_status: 'I',
                            }
                            datasend.push(data);
                        }
                        if ($scope.temp.user_type == 'Student') {
                            var data = {
                                opr: 'I',
                                //sims_certificate_request_number: $scope.temp.sims_certificate_request_number,
                                sims_certificate_request_date: $scope.temp.sims_certificate_request_date,
                                sims_certificate_requested_by: $scope.temp.sims_certificate_enroll_number,
                                sims_certificate_number: $scope.temp.sims_certificate_number,
                                sims_certificate_reason: $scope.temp.sims_certificate_reason,
                                sims_certificate_expected_date: $scope.temp.sims_certificate_expected_date,
                                sims_certificate_enroll_number: $scope.temp.sims_certificate_enroll_number,
                                sims_certificate_issued_by: user,
                                sims_certificate_request_status: 'I',
                            }
                            datasend.push(data);
                        }
                        $http.post(ENV.apiUrl + "api/RequestCertificate/CUDRequestCertificate", datasend).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                $scope.getgrid();
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });
                        //$scope.getgrid();
                        datasend = [];
                        $scope.Studentdata = [];
                        $scope.table = true;
                        $scope.display = false;
                    
                }
            }

            //DATA UPDATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                debugger
                if (Myform) {
                    debugger
                    if ($scope.temp.sims_certificate_expected_date < $scope.temp.sims_certificate_request_date) {
                        swal('', 'Expected date must be after requested date');
                        return;
                    }
                    var data = {
                        opr: 'U',
                        sims_certificate_request_number: $scope.temp.sims_certificate_request_number,
                        sims_certificate_request_date: $scope.temp.sims_certificate_request_date,
                        sims_certificate_requested_by: $scope.temp.sims_certificate_requested_by,
                        sims_certificate_number: $scope.temp.sims_certificate_number,
                        sims_certificate_reason: $scope.temp.sims_certificate_reason,
                        sims_certificate_expected_date: $scope.temp.sims_certificate_expected_date,
                        sims_certificate_enroll_number: $scope.temp.sims_certificate_enroll_number,
                        sims_certificate_issued_by: user,
                        sims_certificate_request_status: 'I',
                    }
                    dataforUpdate.push(data);
                    //dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/RequestCertificate/CUDRequestCertificate", dataforUpdate).then(function (msg) {

                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            $scope.getgrid();
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                    $scope.table = true;
                    $scope.display = false;
                    dataforUpdate = [];
                }
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_certificate_request_number + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_certificate_request_number + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = ''; 
                        $('tr').removeClass("row_selected");
                    }
                }
            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");

                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                deletefin = [];
                $scope.flag = false;
                debugger;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_certificate_request_number + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_certificate_request_number': $scope.filteredTodos[i].sims_certificate_request_number,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/RequestCertificate/CUDRequestCertificate", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                debugger;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                            }

                                            $scope.CheckAllChecked();
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main.checked = false;
                                            $('tr').removeClass("row_selected");
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            debugger
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_certificate_request_number + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.row1 = '';
                $scope.currentPage = str;
                main.checked = false;
                // deletefin = [];
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/RequestCertificate/GetRequestCertificate").then(function (res1) {

                    $scope.CreDiv = res1.data;
                    $scope.totalItems = $scope.CreDiv.length;
                    $scope.todos = $scope.CreDiv;
                    $scope.makeTodos();
                });
            }
         }])
})();