﻿/// <reference path="Admission_dpsdOLCont.js" />

(function () {
    'use strict';
    var del = [];
    var main;
    var date1, date3, date4;

    var simsController = angular.module('sims.module.Student');
    simsController.controller('Admission_asdOLCont',
        ['$scope', '$state', '$stateParams', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $stateParams, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.display = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.school_edit = true;
            $scope.Study_edit = true;
            $scope.desc_edit = true;
            $scope.confirm_edit = true;
            $scope.emp_edit = true;
            $scope.hearing_edit = true;
            $scope.music_edit = true;
            $scope.sport_edit = true;
            $scope.other_edit = true;
            $scope.circum_edit = true;
            $scope.disab_edit = true;
            $scope.healthres_edit = true;
            $scope.health_vision_edit = true;
            $scope.allAYs = [];
           // $scope.tick_edit = true;
            $scope.pricont_edit = true;
            $scope.transreq_edit = true;
            $scope.lang_sup_edit = true;
            $scope.behaviour_edit = true;
            $scope.gifted_edit = true;
            $scope.med_edit = true;
            $scope.learnther_edit = true;
            $scope.spec_edu_edit = true;
            $scope.lang_sup_edit = true;
            $scope.falledgrade_edit = true;
            $scope.behaviour_edit = true;
            $scope.commun_edit = true;
            $scope.specActiv_edit = true;
            $scope.disableparent = true;
            $scope.grid = true;
            $scope.save1 = true;
            $scope.update1 = false;
            $scope.t = true;
            $scope.btn_back = false;
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');
           // $scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.edt = [];
            var data2 = [];
            $scope.BUSY = false;
            $scope.fathersalutation = [], $scope.mothersalutation = [];
            $scope.section = "New";

            $timeout(function () {
                $("#fixedtable4").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.edt =
                {
                    admission_date: $scope.ddMMyyyy,
                    tent_join_date: $scope.ddMMyyyy,
                    comm_date: $scope.ddMMyyyy,
                    declaration_status: true,
                }
            debugger
            var param = $stateParams.admission_num;
            console.log(param);


            $http.get(ENV.apiUrl + "api/common/ProspectDashboard/GetAboutUsData").then(function (res) {
                $scope.about_lst = res.data
            });

            $http.get(ENV.apiUrl + "api/common/ProspectDashboard/GetPreviousSchool").then(function (res) {
                $scope.school_lst = res.data
            });
            $scope.getNatArab = function (nationality_code) {
                if (nationality_code == '784') {
                    $scope.edt.sims_student_attribute3 = 'Y';

                }
                else {
                    $scope.edt.sims_student_attribute3 = 'N';
                }
                
             if(nationality_code == '12' || nationality_code == '48' || nationality_code == '174' || nationality_code == '262' || nationality_code == '818' || nationality_code == '368' || nationality_code == '400' || nationality_code == '414' || nationality_code == '422' || nationality_code == '434' || nationality_code == '478' || nationality_code == '504' || nationality_code == '512' || nationality_code == '972' || nationality_code == '634' || nationality_code == '682' || nationality_code == '706' || nationality_code == '728' || nationality_code == '736' || nationality_code == '760' || nationality_code == '788' || nationality_code == '887' || nationality_code == '784')
                {
                    $scope.edt.sims_student_attribute5 = 'Y';
                }
            else{
                 $scope.edt.sims_student_attribute5 = 'N';
                }
            }
           

            $(document).ready(function () {
                $('#t1').addClass('disableTab');
                $('#t2').addClass('disableTab');
                $('#t3').addClass('disableTab');
                $('#t4').addClass('disableTab');
                $('#t5').addClass('disableTab');
                $('#t6').addClass('disableTab');
                $('#t7').addClass('disableTab');
                $('#t8').addClass('disableTab');
            });

           // if (param != '[object Object]')
            if (param != '[object Object]')
            {
                $http.get(ENV.apiUrl + "api/common/Admission_dpsd/getAdmissionList").then(function (res) {
                    $scope.obj = res.data;
                    console.log($scope.obj);
                    $scope.scholnm = [], $scope.feecat = [], $scope.legalcus = [], $scope.cur_data = [], $scope.feemonth = [], $scope.acad_yr = [];



                    for (var i = 0; i < res.data.length; i++)
                    {

                        if (res.data[i].school_name != '') {
                            $scope.scholnm.push({ school_name: res.data[i].school_name, school_code: res.data[i].school_code });
                            $scope.edt['school_code'] = $scope.scholnm[0].school_code;
                        }
                        if (res.data[i].curr_name != '') {
                            $scope.cur_data.push({ curr_name: res.data[i].curr_name, curr_code: res.data[i].curr_code });
                            //$scope.edt['school_code'] = $scope.cur_data[0].curr_code;
                        }
                        if (res.data[i].fee_category_desc != '') {
                            $scope.feecat.push({ fee_category_code: res.data[i].fee_category_code, fee_category_desc: res.data[i].fee_category_desc });
                            $scope.edt['fee_category_code'] = $scope.feecat[0].fee_category_code;
                        }
                        if (res.data[i].legal_custody_name != '') {
                            $scope.legalcus.push({ legal_custody_code: res.data[i].legal_custody_code, legal_custody_name: res.data[i].legal_custody_name });
                            $scope.edt['primary_contact_pref_code'] = $scope.legalcus[0].legal_custody_code;
                            $scope.edt['legal_custody_code'] = $scope.legalcus[0].legal_custody_code;
                        }
                        if (res.data[i].sims_fee_month_name != '') {
                            $scope.feemonth.push({ sims_fee_month_code: res.data[i].sims_fee_month_code, sims_fee_month_name: res.data[i].sims_fee_month_name });
                        }

                        if (res.data[i].sims_national_id_code == $rootScope.locale.lang)
                        {
                            $scope.lbl_id.national_id = res.data[i].sims_national_id_name;
                        }

                        if ($scope.obj[i].father_salutation_desc != '')
                        {
                            $scope.fathersalutation.push({ father_salutation_desc: $scope.obj[i].father_salutation_desc, father_salutation_code: $scope.obj[i].father_salutation_code });
                            if ($scope.obj[i].father_country == 'Mr.') {
                                $scope.edt['father_salutation_code'] = $scope.obj[i].father_salutation_code;
                            }
                        }
                        if ($scope.obj[i].mother_salutation_desc != '')
                        {
                            $scope.mothersalutation.push({ mother_salutation_desc: $scope.obj[i].mother_salutation_desc, mother_salutation_code: $scope.obj[i].mother_salutation_code });
                            if ($scope.obj[i].father_country == 'Mrs.') {
                                $scope.edt['mother_salutation_code'] = $scope.obj[i].mother_salutation_code;
                            }
                        }
                        //if ($scope.obj[i].section_name != '')
                        //{
                        //   // $scope.feemonth.push({ sims_fee_month_code: res.data[i].sims_fee_month_code, sims_fee_month_name: res.data[i].sims_fee_month_name });
                        //    if ($scope.section == $scope.obj[i].section_name)
                        //    {
                        //        $scope.edt['section_code'] = $scope.obj[i].section_code;
                        //        $scope.section = $scope.edt['section_code'];

                        //    }
                        //}

                    }

                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard_dpsdNew/GetTabStudentData?admission_number=" + param).then(function (res) {
                        $scope.edt = res.data;
                        console.log($scope.edt);

                        if ($scope.edt['admission_date']!=null)
                          //  $scope.edt.admission_date = $scope.ddMMyyyy;
                        if ($scope.edt['tent_join_date'] == null)
                            $scope.edt.tent_join_date = $scope.ddMMyyyy;
                        if ($scope.edt['comm_date'] == null)
                            $scope.edt.comm_date = $scope.ddMMyyyy;
                        if ($scope.edt['term_code'] == "")
                            $scope.edt['term_code'] = '01';
                        if ($scope.edt['fee_category_code'] == "")
                            $scope.edt['fee_category_code'] = '01';
                        if ($scope.edt['sims_fee_month_code'] == null)
                            $scope.edt['sims_fee_month_code'] = '4';
                        //if ($scope.edt['section_code'] == "")
                        //    $scope.edt['section_code'] = $scope.section;
                        $scope.update1 = true;
                        $scope.save1 = false;
                        $scope.btn_back = true;

                        if ($http.defaults.headers.common['schoolId'] == 'tiadxb' || $http.defaults.headers.common['schoolId'] == 'tiashj' || $http.defaults.headers.common['schoolId'] == 'tosdxb' || $http.defaults.headers.common['schoolId'] == 'aisdxb') {
                            $scope.btn_back = false;

                        }

                        $scope.chk_disab_status();
                        $scope.chk_health_res();
                        $scope.chk_medic_status();
                        $scope.chk_other();
                        $scope.chk_hearing();
                        $scope.chk_vision();
                        $scope.chk_spec_activies();
                        $scope.chk_behaviour();
                        $scope.chk_lang_sup();
                        $scope.chk_learn_ther();
                        $scope.chk_spec_edu();
                        $scope.chk_falled_grade();
                        $scope.chk_commun();
                        $scope.chk_Sibling_attend();
                        $scope.chk_ticking();
                        //$scope.getstream($scope.edt.grade_code);
                        //$scope.getSubject($scope.edt['sims_student_attribute9']);
                        $scope.getchkstrength($scope.edt.section_code);
                        if ($scope.edt.status == 'W' || $scope.edt.status == 'S')
                            $scope.update1 = true;
                        else
                            $scope.update1 = false;

                    });


                });
            }
            else
            {
                $http.get(ENV.apiUrl + "api/common/Admission_dpsd/getAdmissionList").then(function (res) {
                    $scope.obj = res.data;
                    console.log($scope.obj);
                    $scope.scholnm = [], $scope.feecat = [], $scope.legalcus = [], $scope.cur_data = [], $scope.feemonth = [], $scope.acad_yr = [];
                  


                    for (var i = 0; i < res.data.length; i++) {

                        if (res.data[i].school_name != '') {
                            $scope.scholnm.push({ school_name: res.data[i].school_name, school_code: res.data[i].school_code });
                            $scope.edt['school_code'] = $scope.scholnm[0].school_code;
                        }
                        if (res.data[i].curr_name != '') {
                            $scope.cur_data.push({ curr_name: res.data[i].curr_name, curr_code: res.data[i].curr_code });
                            //$scope.edt['school_code'] = $scope.cur_data[0].curr_code;
                        }
                        if (res.data[i].fee_category_desc != '') {
                            $scope.feecat.push({ fee_category_code: res.data[i].fee_category_code, fee_category_desc: res.data[i].fee_category_desc });
                            $scope.edt['fee_category_code'] = $scope.feecat[0].fee_category_code;
                        }
                        if (res.data[i].legal_custody_name != '') {
                            $scope.legalcus.push({ legal_custody_code: res.data[i].legal_custody_code, legal_custody_name: res.data[i].legal_custody_name });
                            $scope.edt['primary_contact_pref_code'] = $scope.legalcus[0].legal_custody_code;
                            $scope.edt['legal_custody_code'] = $scope.legalcus[0].legal_custody_code;
                        }
                        if (res.data[i].sims_fee_month_name != '')
                        {
                            $scope.feemonth.push({ sims_fee_month_code: res.data[i].sims_fee_month_code, sims_fee_month_name: res.data[i].sims_fee_month_name });
                        }

                        if (res.data[i].sims_national_id_code == $rootScope.locale.lang)
                        {
                            $scope.lbl_id.national_id = res.data[i].sims_national_id_name;
                        }
                        if ($scope.obj[i].father_salutation_desc != '') {
                            $scope.fathersalutation.push({ father_salutation_desc: $scope.obj[i].father_salutation_desc, father_salutation_code: $scope.obj[i].father_salutation_code });
                            if ($scope.obj[i].father_country == 'Mr.') {
                                $scope.edt['father_salutation_code'] = $scope.obj[i].father_salutation_code;
                            }
                        }
                        if ($scope.obj[i].mother_salutation_desc != '') {
                            $scope.mothersalutation.push({ mother_salutation_desc: $scope.obj[i].mother_salutation_desc, mother_salutation_code: $scope.obj[i].mother_salutation_code });
                            if ($scope.obj[i].father_country == 'Mrs.') {
                                $scope.edt['mother_salutation_code'] = $scope.obj[i].mother_salutation_code;
                            }
                        }
                     
                    }
                   

                });
            }

            $scope.lbl_id =
                {
                    national_id:''
                }

            $scope.removeNull = function (itm) {
                return itm.profiles;
            }
            

            $scope.getstream = function (grade_code) {

                if (grade_code == '03' || grade_code == '04' || grade_code == '05' || grade_code == '06' || grade_code == '07' || grade_code == '08' || grade_code == '09' || grade_code == '10' || grade_code == '11' || grade_code == '12')
                {
                    $scope.div_second = true;
                    $scope.div_third = false;
                    $http.get(ENV.apiUrl + "api/common/Admission_dpsd/Get_secondlangDetails?cur_code=" + $scope.edt.curr_code + "&acad_yr=" + $scope.edt.academic_year + "&grade_code=" + $scope.edt.grade_code).then(function (secondlang_res) {
                        $scope.secondlang_data = secondlang_res.data;
                        //console.log($scope.secondlang_data);
                    });
                }
                if (grade_code == '05' || grade_code == '06' || grade_code == '07' || grade_code == '08' || grade_code == '09' || grade_code == '10' || grade_code == '11' || grade_code == '12') {
                    $scope.div_second = true;
                    $scope.div_third = true;
                    $http.get(ENV.apiUrl + "api/common/Admission_dpsd/Get_thirdlangDetails?cur_code=" + $scope.edt.curr_code + "&acad_yr=" + $scope.edt.academic_year + "&grade_code=" + $scope.edt.grade_code).then(function (thirdlang_res) {
                        $scope.thirdlang_data = thirdlang_res.data;
                        // console.log($scope.thirdlang_data);
                    });
                }
                if (grade_code == '13' || grade_code == '14')
                {
                    $scope.div_subject = true;
                    $http.get(ENV.apiUrl + "api/common/Admission_dpsd/GetStreamPrefferd").then(function (res) {
                        $scope.stream_data = res.data;
                    });
                }
            }

            $scope.getSubject = function (stream)
            {
                $http.get(ENV.apiUrl + "api/common/Admission_dpsd/GetMandetorySubject?stream=" + stream).then(function (Mansubject_res) {
                    $scope.Mandetorysubject_data = Mansubject_res.data;
                    //   $scope.edt['sims_admission_second_lang_code'] = $scope.Mandetorysubject_data[0].subject_code;
                    // console.log($scope.Mandetorysubject_data);
                });

                $http.get(ENV.apiUrl + "api/common/Admission_dpsd/GetOptionalSubject?stream=" + stream).then(function (Opsubject_res) {
                    $scope.Optionalsubject_data = Opsubject_res.data;
                    //  $scope.edt['sims_admission_third_lang_code'] = $scope.Optionalsubject_data[0].subject_code;
                    //console.log($scope.Mandetorysubject_data);
                });
            }

            $scope.GetInfo = function (parentid, enrollno)
            {
                console.log(parentid, enrollno);
                if (enrollno != null) {
                    $http.post(ENV.apiUrl + "api/common/Admission_dpsd/CheckParentCode?parent_id=" + parentid + "&enroll_no=" + enrollno).then(function (res) {
                        $scope.msg1 = res.data;
                        console.log($scope.msg1);
                        $scope.edt.parent_id = $scope.msg1.parent_id;
                        $scope.edt.sibling_enroll = $scope.msg1.sims_student_enroll_number;
                        $scope.edt.sibling_name = $scope.msg1.sibling_name;
                    });
                }
            }

            $scope.valandfocus = function (parentid) {
                if (parentid == undefined || parentid == '') {
                    swal({ title: "Alert", text: "Invalid Parent ID", showCloseButton: true, width: 450, });
                    return;
                }
                document.getElementById('txt_enroll_no').focus();
            }

            

            $scope.Save = function (isvalidate) {
                $scope.save1 = true;
                var data2 = [];
                $scope.BUSY = true;

                if (isvalidate)
                {
                    if ($scope.update1 == false)
                    {
                        var data = ({
                            admission_number: $scope.edt.admission_number,
                            appl_num: $scope.edt.appl_num,
                            pros_num: $scope.edt.pros_num,
                            pros_appl_num: $scope.edt.pros_appl_num,
                            admission_date: $scope.edt.admission_date,
                            school_code: $scope.edt.school_code,
                            curr_code: $scope.edt.curr_code,
                            academic_year: $scope.edt.academic_year,
                            grade_code: $scope.edt.grade_code,
                            section_code: $scope.edt.section_code,
                            term_code: $scope.edt.term_code,
                            tent_join_date: $scope.edt.tent_join_date,
                            sims_student_emergency_contact_number1: $scope.edt.sims_student_emergency_contact_number1,
                            sims_student_emergency_contact_name1: $scope.edt.sims_student_emergency_contact_name1,
                            first_name: $scope.edt.first_name,
                            middle_name: $scope.edt.middle_name,
                            last_name: $scope.edt.last_name,
                            family_name: $scope.edt.family_name,
                            first_name_ot: $scope.edt.first_name_ot,
                            midd_name_ot: $scope.edt.midd_name_ot,
                            last_name_ot: $scope.edt.last_name_ot,
                            family_name_ot: $scope.edt.family_name_ot,
                            nicke_name: $scope.edt.nicke_name,
                            birth_date: $scope.edt.birth_date,
                            comm_date: $scope.edt.comm_date,
                            birth_country_code: $scope.edt.birth_country_code,
                            nationality_code: $scope.edt.nationality_code,
                            ethinicity_code: $scope.edt.ethinicity_code,
                            gender_code: $scope.edt.gender_code,
                            religion_code: $scope.edt.religion_code,
                            passport_num: $scope.edt.passport_num,
                            passport_issue_date: $scope.edt.passport_issue_date,
                            passport_expiry: $scope.edt.passport_expiry,
                            passport_issue_auth: $scope.edt.passport_issue_auth,
                            passport_issue_place: $scope.edt.passport_issue_place,
                            visa_number: $scope.edt.visa_number,
                            visa_type: $scope.edt.visa_type,
                            visa_issuing_authority: $scope.edt.visa_issuing_authority,
                            visa_issue_date: $scope.edt.visa_issue_date,
                            visa_expiry_date: $scope.edt.visa_expiry_date,
                            visa_issuing_place: $scope.edt.visa_issuing_place,
                            national_id: $scope.edt.national_id,
                            national_id_issue_date: $scope.edt.national_id_issue_date,
                            national_id_expiry_date: $scope.edt.national_id_expiry_date,
                            sibling_status: $scope.edt.sibling_status,
                            parent_id: $scope.edt.parent_id,
                            sibling_enroll: $scope.edt.sibling_enroll,
                            sibling_name: $scope.edt.sibling_name,
                            sibling_dob: $scope.edt.sibling_dob,
                            sibling_school_code: $scope.edt.sibling_school_code,
                            employee_type: $scope.edt.employee_type,
                            employee_code: $scope.edt.employee_code,
                            employee_school_code: $scope.edt.employee_school_code,
                            motherTounge_language_code: $scope.edt.motherTounge_language_code,
                            main_language_code: $scope.edt.main_language_code,
                            main_language_r_code: $scope.edt.main_language_r_code,
                            main_language_w_code: $scope.edt.main_language_w_code,
                            main_language_s_code: $scope.edt.main_language_s_code,
                            primary_contact_code: $scope.edt.primary_contact_code,
                            primary_contact_pref_code: $scope.edt.primary_contact_pref_code,
                            fee_payment_contact_pref_code: $scope.edt.fee_payment_contact_pref_code,
                            transport_status: $scope.edt.transport_status,
                            transport_desc: $scope.edt.transport_desc,
                            father_salutation_code: $scope.edt.father_salutation_code,
                            father_first_name: $scope.edt.father_first_name,
                            father_middle_name: $scope.edt.father_middle_name,
                            father_last_name: $scope.edt.father_last_name,
                            father_family_name: $scope.edt.father_family_name,
                            father_family_name_ot: $scope.edt.father_family_name_ot,
                            father_nationality1_code: $scope.edt.father_nationality1_code,
                            father_nationality2_code: $scope.edt.father_nationality2_code,
                            father_appartment_number: $scope.edt.father_appartment_number,
                            father_building_number: $scope.edt.father_building_number,
                            father_street_number: $scope.edt.father_street_number,
                            father_area_number: $scope.edt.father_area_number,
                            father_city: $scope.edt.father_city,
                            sims_student_attribute12:$scope.edt.sims_student_attribute12,

                            father_state: $scope.edt.father_state,
                            father_country_code: $scope.edt.father_country_code,
                            father_summary_address: $scope.edt.father_summary_address,
                            father_po_box: $scope.edt.father_po_box,
                            father_phone: $scope.edt.father_phone,
                            father_email: $scope.edt.father_email,
                            father_mobile: $scope.edt.father_mobile,
                            father_fax: $scope.edt.father_fax,
                            father_occupation: $scope.edt.father_occupation,
                            father_company: $scope.edt.father_company,
                            father_passport_number: $scope.edt.father_passport_number,
                            sims_admission_father_national_id: $scope.edt.sims_admission_father_national_id,
                            guardian_salutation_code: $scope.edt.guardian_salutation_code,
                            guardian_first_name: $scope.edt.guardian_first_name,
                            guardian_middle_name: $scope.edt.guardian_middle_name,
                            guardian_last_name: $scope.edt.guardian_last_name,
                            guardian_family_name: $scope.edt.guardian_family_name,
                            guardian_nationality1_code: $scope.edt.guardian_nationality1_code,
                            guardian_nationality2_code: $scope.edt.guardian_nationality2_code,
                            guardian_appartment_number: $scope.edt.guardian_appartment_number,
                            guardian_building_number: $scope.edt.guardian_building_number,
                            guardian_street_number: $scope.edt.guardian_street_number,
                            guardian_area_number: $scope.edt.guardian_area_number,
                            guardian_city: $scope.edt.guardian_city,
                            guardian_state: $scope.edt.guardian_state,
                            guardian_country_code: $scope.edt.guardian_country_code,
                            guardian_summary_address: $scope.edt.guardian_summary_address,
                            guardian_po_box: $scope.edt.guardian_po_box,
                            guardian_phone: $scope.edt.guardian_phone,
                            guardian_mobile: $scope.edt.guardian_mobile,
                            guardian_fax: $scope.edt.guardian_fax,
                            guardian_email: $scope.edt.guardian_email,
                            guardian_occupation: $scope.edt.guardian_occupation,
                            guardian_company: $scope.edt.guardian_company,
                            guardian_passport_number: $scope.edt.guardian_passport_number,
                            guardian_relationship_code: $scope.edt.guardian_relationship_code,
                            mother_salutation_code: $scope.edt.mother_salutation_code,
                            mother_first_name: $scope.edt.mother_first_name,
                            mother_middle_name: $scope.edt.mother_middle_name,
                            mother_last_name: $scope.edt.mother_last_name,
                            mother_family_name: $scope.edt.mother_family_name,
                            mother_name_ot: $scope.edt.mother_name_ot,
                            mother_nationality1_code: $scope.edt.mother_nationality1_code,
                            mother_nationality2_code: $scope.edt.mother_nationality2_code,
                            mother_appartment_number: $scope.edt.mother_appartment_number,
                            mother_building_number: $scope.edt.mother_building_number,
                            mother_street_number: $scope.edt.mother_street_number,
                            mother_area_number: $scope.edt.mother_area_number,
                            mother_city: $scope.edt.mother_city,
                            mother_state: $scope.edt.mother_state,
                            mother_country_code: $scope.edt.mother_country_code,
                            mother_summary_address: $scope.edt.mother_summary_address,
                            mother_po_box: $scope.edt.mother_po_box,
                            mother_phone: $scope.edt.mother_phone,
                            mother_mobile: $scope.edt.mother_mobile,
                            mother_fax: $scope.edt.mother_fax,
                            mother_email: $scope.edt.mother_email,
                            mother_occupation: $scope.edt.mother_occupation,
                            mother_passport_number: $scope.edt.mother_passport_number,
                            current_school_status: $scope.edt.current_school_status,
                            current_school_name: $scope.edt.current_school_name,
                            current_school_enroll_number: $scope.edt.current_school_enroll_number,
                            current_school_grade: $scope.edt.current_school_grade,
                            current_school_cur: $scope.edt.current_school_cur,
                            current_school_from_date: $scope.edt.current_school_from_date,
                            current_school_to_date: $scope.edt.current_school_to_date,
                            current_school_language: $scope.edt.current_school_language,
                            current_school_head_teacher: $scope.edt.current_school_head_teacher,
                            current_school_phone: $scope.edt.current_school_phone,
                            current_school_fax: $scope.edt.current_school_fax,
                            current_school_city: $scope.edt.current_school_city,
                            current_school_country_code: $scope.edt.current_school_country_code,
                            current_school_address: $scope.edt.current_school_address,
                            marketing_code: $scope.edt.marketing_code,
                            marketing_description: $scope.edt.marketing_description,
                            parent_status_code: $scope.edt.parent_status_code,
                            legal_custody_code: $scope.edt.legal_custody_code,
                            health_card_number: $scope.edt.health_card_number,
                            health_card_issue_date: $scope.edt.health_card_issue_date,
                            health_card_issuing_authority: $scope.edt.health_card_issuing_authority,
                            blood_group_code: $scope.edt.blood_group_code,
                            //sims_student_attribute12: $scope.edt.sims_student_attribute12,
                            medication_status: $scope.edt.medication_status,
                            medication_desc: $scope.edt.medication_desc,
                            disability_status: $scope.edt.disability_status,
                            disability_desc: $scope.edt.disability_desc,

                            behaviour_status: $scope.edt.behaviour_status,
                            behaviour_desc: $scope.edt.behaviour_desc,
                            health_restriction_status: $scope.edt.health_restriction_status,
                            health_restriction_desc: $scope.edt.health_restriction_desc,
                            health_hearing_status: $scope.edt.health_hearing_status,
                            health_hearing_desc: $scope.edt.health_hearing_desc,
                            health_vision_status: $scope.edt.health_vision_status,
                            health_vision_desc: $scope.edt.health_vision_desc,
                            health_other_status: $scope.edt.health_other_status,
                            health_other_desc: $scope.edt.health_other_desc,
                            gifted_status: $scope.edt.gifted_status,
                            gifted_desc: $scope.edt.gifted_desc,
                            music_status: $scope.edt.music_status,
                            music_desc: $scope.edt.music_desc,
                            sports_status: $scope.edt.sports_status,
                            sports_desc: $scope.edt.sports_desc,
                            language_support_status: $scope.edt.language_support_status,
                            language_support_desc: $scope.edt.language_support_desc,
                            declaration_status: $scope.edt.declaration_status,
                            fees_paid_status: $scope.edt.fees_paid_status,
                            fee_category_code: $scope.edt.fee_category_code,
                            ip: $scope.edt.ip,
                            dns: $scope.edt.dns,
                            user_code: $scope.edt.user_code,
                            status: $scope.edt.status,
                            sims_student_attribute1: $scope.edt.sims_student_attribute1,
                            sims_student_attribute2: $scope.edt.sims_student_attribute2,
                            sims_student_attribute3: $scope.edt.sims_student_attribute3,
                            sims_student_attribute4: $scope.edt.sims_student_attribute4,
                            sims_student_health_respiratory_status: $scope.edt.sims_student_health_respiratory_status,
                            sims_student_health_respiratory_desc: $scope.edt.sims_student_health_respiratory_desc,
                            sims_student_health_hay_fever_status: $scope.edt.sims_student_health_hay_fever_status,
                            sims_student_health_hay_fever_desc: $scope.edt.sims_student_health_hay_fever_desc,
                            sims_student_health_epilepsy_status: $scope.edt.sims_student_health_epilepsy_status,
                            sims_student_health_epilepsy_desc: $scope.edt.sims_student_health_epilepsy_desc,
                            sims_student_health_skin_status: $scope.edt.sims_student_health_skin_status,
                            sims_student_health_skin_desc: $scope.edt.sims_student_health_skin_desc,
                            sims_student_health_diabetes_status: $scope.edt.sims_student_health_diabetes_status,
                            sims_student_health_diabetes_desc: $scope.edt.sims_student_health_diabetes_desc,
                            sims_student_health_surgery_status: $scope.edt.sims_student_health_surgery_status,
                            sims_student_health_surgery_desc: $scope.edt.sims_student_health_surgery_desc,
                            sims_admission_recommendation: $scope.edt.sims_admission_recommendation,
                            //sims_admission_father_national_id:$scope.edt.sims_admission_father_national_id,
                            sims_admission_mother_national_id: $scope.edt.sims_admission_mother_national_id,
                            sims_admission_guardian_national_id: $scope.edt.sims_admission_guardian_national_id,
                            sims_fee_month_code: $scope.edt.sims_fee_month_code,
                            sims_student_attribute5: $scope.edt.sims_student_attribute5,
                            sims_student_attribute6: $scope.edt.sims_student_attribute6,
                            sims_student_attribute7: $scope.edt.sims_student_attribute7,
                            sims_student_attribute8: $scope.edt.sims_student_attribute8,
                            sims_student_attribute9: $scope.edt.sims_student_attribute9,
                            sims_student_attribute10: $scope.edt.sims_student_attribute10,
                            sims_student_attribute11: $scope.edt.sims_student_attribute11,
                           // sims_student_attribute12: $scope.edt.sims_student_attribute12,

                            home_appartment_number: $scope.edt.home_appartment_number,
                            home_building_number: $scope.edt.home_building_number,
                            home_street_number: $scope.edt.home_street_number,
                            home_area_number: $scope.edt.home_area_number,
                            home_city: $scope.edt.home_city,
                            home_state: $scope.edt.home_state,
                            home_country_code: $scope.edt.home_country_code,
                            home_summary_address: $scope.edt.home_summary_address,
                            home_po_box: $scope.edt.home_po_box,
                            home_landmark: $scope.edt.home_landmark,
                            home_country_isdcode: $scope.edt.home_country_isdcode,
                            home_phone: $scope.edt.home_country_isdcode + '-' + $scope.edt.home_phone,
                            //sims_house_code: $scope.edt.sims_house_code,
                            opr: 'I'
                        });

                        data2.push(data);
                        console.log(data2);

                        //$http.post(ENV.apiUrl + "api/common/Admission_dpsd/CUDInsertAdmission", data2).then(function (res) {
                        //    $scope.display = true;
                        //    $scope.msg1 = res.data;
                        //    console.log($scope.msg1);

                        //    data.admission_number = $scope.msg1.admission_number;
                            $http.post(ENV.apiUrl + "api/common/Admission_dpsd/ApproveStudent", data2).then(function (res) {
                                $scope.display = true;
                                $scope.msg1 = res.data;
                                $scope.BUSY = false;
                                //var enroll = $scope.msg1[0].enroll;

                                if ($scope.msg1.length > 0)
                                {
                                    var data1 = $scope.msg1;
                                    data1.strMessage = "Sucessfully Admitted Enroll='" + data1[0].enroll + "' \n Student Name='" + data1[0].stud_full_name + "'\n Parent Id='" + data1[0].parent_id;

                                    swal({ title: "Alert", text: data1.strMessage, showCloseButton: true, width: 380, });
                                    $scope.getgrid();
                                }
                               
                                
                                //$rootScope.strMessage = data1.strMessage;
                                //console.log($scope.data1);
                                //$('#message').modal({ backdrop: 'static', keyboard: false });

                               
                            });

                        //});
                    }
                    else
                    {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function ()
            {
                $scope.edt = [];
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.msg1 = "";
             
                $('.nav-tabs a[href="#admtab1"]').tab('show');
            }

            /*start_Date code*/

            function convertdate(dt) {
                if (dt != null) {
                    //var d1 = new Date(dt);
                    //var month = d1.getMonth() + 1;
                    //var day = d1.getDate();
                    //if (month < 10)
                    //    month = "0" + month;
                    //if (day < 10)
                    //    day = "0" + day;
                    //var d = d1.getFullYear() + "-" + (month) + "-" + (day);
                    var d = dt;
                    return d;
                }
            }

           

            //*start_new*//

            $scope.getchkstrength = function (sectioncode)
            {
                if (sectioncode != undefined)
                {
                    $http.get(ENV.apiUrl + "api/common/Admission_dpsd/getsectionList?cur_code=" + $scope.edt.curr_code + "&acad_yr=" + $scope.edt.academic_year + "&grade_code=" + $scope.edt.grade_code + "&section_code=" + sectioncode).then(function (res) {
                        $scope.sectionlst1 = res.data;
                       // $scope.sectionlst = $scope.sectionlst1;
                    });
                }

                $('#cmb_section').css('background-color', '#FFF');
            }

            $scope.view_section_strength = function ()
            {
                    $http.get(ENV.apiUrl + "api/common/Admission_dpsd/getsectionList?cur_code=" + $scope.edt.curr_code + "&acad_yr=" + $scope.edt.academic_year + "&grade_code=" + $scope.edt.grade_code + "&section_code=" + undefined).then(function (res) {
                        $scope.sectionlst = res.data;
                        $scope.section = true;
                        $('#SectionListModal').modal({ backdrop: 'static', keyboard: true });

                        for (var i = 0; i < $scope.sectionlst.length; i++)
                        {
                            if ($scope.sectionlst[i].section_code == $scope.edt.section_code) {
                                setTimeout(function () {
                                   // $scope.test = '#' + i+1;
                                    $(('#'+i)).addClass('row_selected');
                                }, 100);
                                break;
                            }
                        }
                    });
            }

           
            //*end_new*//

            $scope.Update = function (isvalidate)
            {
                var data2 = [];
                if (isvalidate)
                {
                    //if ($scope.edt.grade_code == '03' || $scope.edt.grade_code == '04') {
                    //    if ($scope.edt.sims_admission_second_lang_code == null)
                    //        $scope.edt.sims_admission_second_lang_code = "";

                    //    if ($scope.edt.sims_admission_second_lang_code == "") {
                    //        swal({ title: "Alert", text: "Please Select Second Lang.", showCloseButton: true, width: 380, });
                    //        return;
                    //    }
                    //}

                    //if ($scope.edt.grade_code == '05' || $scope.edt.grade_code == '06' || $scope.edt.grade_code == '07' || $scope.edt.grade_code == '08' || $scope.edt.grade_code == '09' ||
                    //    $scope.edt.grade_code == '10' || $scope.edt.grade_code == '11' || $scope.edt.grade_code == '12') {
                    //    if ($scope.edt.sims_admission_third_lang_code == null)
                    //        $scope.edt.sims_admission_third_lang_code = "";

                    //    if ($scope.edt.sims_admission_second_lang_code == null)
                    //        $scope.edt.sims_admission_second_lang_code = "";

                    //    if ($scope.edt.sims_admission_third_lang_code == "") {
                    //        if ($scope.edt.sims_admission_second_lang_code == "") {
                    //            swal({ title: "Alert", text: "Please Select Second and third Lang.", showCloseButton: true, width: 380, });
                    //            return;
                    //        }
                    //    }

                    //    if ($scope.edt.sims_admission_third_lang_code == "") {
                    //        if ($scope.edt.sims_admission_second_lang_code != "") {
                    //            swal({ title: "Alert", text: "Please Select Second and third Lang.", showCloseButton: true, width: 380, });
                    //            return;
                    //        }
                    //    }

                    //    if ($scope.edt.sims_admission_third_lang_code != "") {
                    //        if ($scope.edt.sims_admission_second_lang_code == "") {
                    //            swal({ title: "Alert", text: "Please Select Second and third Lang.", showCloseButton: true, width: 380, });
                    //            return;
                    //        }
                    //    }


                    //}

                    //if ($scope.edt.grade_code == '13' || $scope.edt.grade_code == '14') {
                    //    if ($scope.edt.sims_admission_third_lang_code == null)
                    //        $scope.edt.sims_admission_third_lang_code = "";

                    //    if ($scope.edt.sims_admission_second_lang_code == null)
                    //        $scope.edt.sims_admission_second_lang_code = "";

                    //    if ($scope.edt.sims_admission_third_lang_code == "") {
                    //        if ($scope.edt.sims_admission_second_lang_code == "") {
                    //            swal({ title: "Alert", text: "Please Select Mandetory & Optional Subject.", showCloseButton: true, width: 380, });
                    //            return;
                    //        }
                    //    }

                    //    if ($scope.edt.sims_admission_third_lang_code == "") {
                    //        if ($scope.edt.sims_admission_second_lang_code != "") {
                    //            swal({ title: "Alert", text: "Please Select Mandetory & Optional Subject.", showCloseButton: true, width: 380, });
                    //            return;
                    //        }
                    //    }

                    //    if ($scope.edt.sims_admission_third_lang_code != "") {
                    //        if ($scope.edt.sims_admission_second_lang_code == "") {
                    //            swal({ title: "Alert", text: "Please Select Mandetory & Optional Subject.", showCloseButton: true, width: 380, });
                    //            return;
                    //        }
                    //    }
                    //}

                    //if ($scope.edt.home_country_isdcode == null)
                    //    $scope.edt.home_country_isdcode = "";
                    //if ($scope.edt.home_phone == null)
                    //    $scope.edt.home_phone = "";


                    var data = ({
                        admission_number: $scope.edt.admission_number,
                        appl_num: $scope.edt.appl_num,
                        pros_num: $scope.edt.pros_num,
                        pros_appl_num: $scope.edt.pros_appl_num,
                        admission_date: $scope.edt.admission_date,
                        school_code: $scope.edt.school_code,
                        curr_code: $scope.edt.curr_code,
                        academic_year: $scope.edt.academic_year,
                        grade_code: $scope.edt.grade_code,
                        section_code: $scope.edt.section_code,
                        term_code: $scope.edt.term_code,
                        tent_join_date: $scope.edt.tent_join_date,
                        sims_student_emergency_contact_number1: $scope.edt.sims_student_emergency_contact_number1,
                        sims_student_emergency_contact_name1: $scope.edt.sims_student_emergency_contact_name1,
                        first_name: $scope.edt.first_name,
                        middle_name: $scope.edt.middle_name,
                        last_name: $scope.edt.last_name,
                        family_name: $scope.edt.family_name,
                        first_name_ot: $scope.edt.first_name_ot,
                        midd_name_ot: $scope.edt.midd_name_ot,
                        last_name_ot: $scope.edt.last_name_ot,
                        family_name_ot: $scope.edt.family_name_ot,
                        nicke_name: $scope.edt.nicke_name,
                        birth_date: $scope.edt.birth_date,
                        comm_date: $scope.edt.comm_date,
                        birth_country_code: $scope.edt.birth_country_code,
                        nationality_code: $scope.edt.nationality_code,
                        ethinicity_code: $scope.edt.ethinicity_code,
                        gender_code: $scope.edt.gender_code,
                        religion_code: $scope.edt.religion_code,
                        passport_num: $scope.edt.passport_num,
                        passport_issue_date: $scope.edt.passport_issue_date,
                        passport_expiry: $scope.edt.passport_expiry,
                        passport_issue_auth: $scope.edt.passport_issue_auth,
                        passport_issue_place: $scope.edt.passport_issue_place,
                        visa_number: $scope.edt.visa_number,
                        visa_type: $scope.edt.visa_type,
                        visa_issuing_authority: $scope.edt.visa_issuing_authority,
                        visa_issue_date: $scope.edt.visa_issue_date,
                        visa_expiry_date: $scope.edt.visa_expiry_date,
                        visa_issuing_place: $scope.edt.visa_issuing_place,
                        national_id: $scope.edt.national_id,
                        national_id_issue_date: $scope.edt.national_id_issue_date,
                        national_id_expiry_date: $scope.edt.national_id_expiry_date,
                        sibling_status: $scope.edt.sibling_status,
                        parent_id: $scope.edt.parent_id,
                        sibling_enroll: $scope.edt.sibling_enroll,
                        sibling_name: $scope.edt.sibling_name,
                        sibling_dob: $scope.edt.sibling_dob,
                        sibling_school_code: $scope.edt.sibling_school_code,
                        employee_type: $scope.edt.employee_type,
                        employee_code: $scope.edt.employee_code,
                        employee_school_code: $scope.edt.employee_school_code,
                        motherTounge_language_code: $scope.edt.motherTounge_language_code,
                        main_language_code: $scope.edt.main_language_code,
                        main_language_r_code: $scope.edt.main_language_r_code,
                        main_language_w_code: $scope.edt.main_language_w_code,
                        main_language_s_code: $scope.edt.main_language_s_code,
                        primary_contact_code: $scope.edt.primary_contact_code,
                        primary_contact_pref_code: $scope.edt.primary_contact_pref_code,
                        fee_payment_contact_pref_code: $scope.edt.fee_payment_contact_pref_code,
                        transport_status: $scope.edt.transport_status,
                        transport_desc: $scope.edt.transport_desc,
                        father_salutation_code: $scope.edt.father_salutation_code,
                        father_first_name: $scope.edt.father_first_name,
                        father_middle_name: $scope.edt.father_middle_name,
                        father_last_name: $scope.edt.father_last_name,
                        father_family_name: $scope.edt.father_family_name,
                        father_family_name_ot: $scope.edt.father_family_name_ot,
                        father_nationality1_code: $scope.edt.father_nationality1_code,
                        father_nationality2_code: $scope.edt.father_nationality2_code,
                        father_appartment_number: $scope.edt.father_appartment_number,
                        father_building_number: $scope.edt.father_building_number,
                        father_street_number: $scope.edt.father_street_number,
                        father_area_number: $scope.edt.father_area_number,
                        father_city: $scope.edt.father_city,
                       
                        father_state: $scope.edt.father_state,
                        father_country_code: $scope.edt.father_country_code,
                        father_summary_address: $scope.edt.father_summary_address,
                        father_po_box: $scope.edt.father_po_box,
                        father_phone: $scope.edt.father_phone,
                        father_email: $scope.edt.father_email,
                        father_mobile: $scope.edt.father_mobile,
                        father_fax: $scope.edt.father_fax,
                        father_occupation: $scope.edt.father_occupation,
                        father_company: $scope.edt.father_company,
                        father_passport_number: $scope.edt.father_passport_number,
                        sims_admission_father_national_id: $scope.edt.sims_admission_father_national_id,
                        guardian_salutation_code: $scope.edt.guardian_salutation_code,
                        guardian_first_name: $scope.edt.guardian_first_name,
                        guardian_middle_name: $scope.edt.guardian_middle_name,
                        guardian_last_name: $scope.edt.guardian_last_name,
                        guardian_family_name: $scope.edt.guardian_family_name,
                        guardian_nationality1_code: $scope.edt.guardian_nationality1_code,
                        guardian_nationality2_code: $scope.edt.guardian_nationality2_code,
                        guardian_appartment_number: $scope.edt.guardian_appartment_number,
                        guardian_building_number: $scope.edt.guardian_building_number,
                        guardian_street_number: $scope.edt.guardian_street_number,
                        guardian_area_number: $scope.edt.guardian_area_number,
                        guardian_city: $scope.edt.guardian_city,
                        guardian_state: $scope.edt.guardian_state,
                        guardian_country_code: $scope.edt.guardian_country_code,
                        guardian_summary_address: $scope.edt.guardian_summary_address,
                        guardian_po_box: $scope.edt.guardian_po_box,
                        guardian_phone: $scope.edt.guardian_phone,
                        guardian_mobile: $scope.edt.guardian_mobile,
                        guardian_fax: $scope.edt.guardian_fax,
                        guardian_email: $scope.edt.guardian_email,
                        guardian_occupation: $scope.edt.guardian_occupation,
                        guardian_company: $scope.edt.guardian_company,
                        guardian_passport_number: $scope.edt.guardian_passport_number,
                        guardian_relationship_code: $scope.edt.guardian_relationship_code,
                        mother_salutation_code: $scope.edt.mother_salutation_code,
                        mother_first_name: $scope.edt.mother_first_name,
                        mother_middle_name: $scope.edt.mother_middle_name,
                        mother_last_name: $scope.edt.mother_last_name,
                        mother_family_name: $scope.edt.mother_family_name,
                        mother_name_ot: $scope.edt.mother_name_ot,
                        mother_nationality1_code: $scope.edt.mother_nationality1_code,
                        mother_nationality2_code: $scope.edt.mother_nationality2_code,
                        mother_appartment_number: $scope.edt.mother_appartment_number,
                        mother_building_number: $scope.edt.mother_building_number,
                        mother_street_number: $scope.edt.mother_street_number,
                        mother_area_number: $scope.edt.mother_area_number,
                        mother_city: $scope.edt.mother_city,
                        mother_state: $scope.edt.mother_state,
                        mother_country_code: $scope.edt.mother_country_code,
                        mother_summary_address: $scope.edt.mother_summary_address,
                        mother_po_box: $scope.edt.mother_po_box,
                        mother_phone: $scope.edt.mother_phone,
                        mother_mobile: $scope.edt.mother_mobile,
                        mother_fax: $scope.edt.mother_fax,
                        mother_email: $scope.edt.mother_email,
                        mother_occupation: $scope.edt.mother_occupation,
                        mother_passport_number: $scope.edt.mother_passport_number,
                        current_school_status: $scope.edt.current_school_status,
                        current_school_name: $scope.edt.current_school_name,
                        current_school_enroll_number: $scope.edt.current_school_enroll_number,
                        current_school_grade: $scope.edt.current_school_grade,
                        current_school_cur: $scope.edt.current_school_cur,
                        current_school_from_date: $scope.edt.current_school_from_date,
                        current_school_to_date: $scope.edt.current_school_to_date,
                        current_school_language: $scope.edt.current_school_language,
                        current_school_head_teacher: $scope.edt.current_school_head_teacher,
                        current_school_phone: $scope.edt.current_school_phone,
                        current_school_fax: $scope.edt.current_school_fax,
                        current_school_city: $scope.edt.current_school_city,
                        current_school_country_code: $scope.edt.current_school_country_code,
                        current_school_address: $scope.edt.current_school_address,
                        marketing_code: $scope.edt.marketing_code,
                        marketing_description: $scope.edt.marketing_description,
                        parent_status_code: $scope.edt.parent_status_code,
                        legal_custody_code: $scope.edt.legal_custody_code,
                        health_card_number: $scope.edt.health_card_number,
                        health_card_issue_date: $scope.edt.health_card_issue_date,
                        health_card_issuing_authority: $scope.edt.health_card_issuing_authority,
                        blood_group_code: $scope.edt.blood_group_code,
                        medication_status: $scope.edt.medication_status,
                        medication_desc: $scope.edt.medication_desc,
                        disability_status: $scope.edt.disability_status,
                        disability_desc: $scope.edt.disability_desc,
                        behaviour_status: $scope.edt.behaviour_status,
                        behaviour_desc: $scope.edt.behaviour_desc,
                        health_restriction_status: $scope.edt.health_restriction_status,
                        health_restriction_desc: $scope.edt.health_restriction_desc,
                        health_hearing_status: $scope.edt.health_hearing_status,
                        health_hearing_desc: $scope.edt.health_hearing_desc,
                        health_vision_status: $scope.edt.health_vision_status,
                        health_vision_desc: $scope.edt.health_vision_desc,
                        health_other_status: $scope.edt.health_other_status,
                        health_other_desc: $scope.edt.health_other_desc,
                        gifted_status: $scope.edt.gifted_status,
                        gifted_desc: $scope.edt.gifted_desc,
                        music_status: $scope.edt.music_status,
                        music_desc: $scope.edt.music_desc,
                        sports_status: $scope.edt.sports_status,
                        sports_desc: $scope.edt.sports_desc,
                        language_support_status: $scope.edt.language_support_status,
                        language_support_desc: $scope.edt.language_support_desc,
                        declaration_status: $scope.edt.declaration_status,
                        fees_paid_status: $scope.edt.fees_paid_status,
                        fee_category_code: $scope.edt.fee_category_code,
                        ip: $scope.edt.ip,
                        dns: $scope.edt.dns,
                        user_code: $scope.edt.user_code,
                        status: $scope.edt.status,
                        sims_student_attribute1: $scope.edt.sims_student_attribute1,
                        sims_student_attribute2: $scope.edt.sims_student_attribute2,
                        sims_student_attribute3: $scope.edt.sims_student_attribute3,
                        sims_student_attribute4: $scope.edt.sims_student_attribute4,
                        sims_student_health_respiratory_status: $scope.edt.sims_student_health_respiratory_status,
                        sims_student_health_respiratory_desc: $scope.edt.sims_student_health_respiratory_desc,
                        sims_student_health_hay_fever_status: $scope.edt.sims_student_health_hay_fever_status,
                        sims_student_health_hay_fever_desc: $scope.edt.sims_student_health_hay_fever_desc,
                        sims_student_health_epilepsy_status: $scope.edt.sims_student_health_epilepsy_status,
                        sims_student_health_epilepsy_desc: $scope.edt.sims_student_health_epilepsy_desc,
                        sims_student_health_skin_status: $scope.edt.sims_student_health_skin_status,
                        sims_student_health_skin_desc: $scope.edt.sims_student_health_skin_desc,
                        sims_student_health_diabetes_status: $scope.edt.sims_student_health_diabetes_status,
                        sims_student_health_diabetes_desc: $scope.edt.sims_student_health_diabetes_desc,
                        sims_student_health_surgery_status: $scope.edt.sims_student_health_surgery_status,
                        sims_student_health_surgery_desc: $scope.edt.sims_student_health_surgery_desc,
                        sims_admission_recommendation: $scope.edt.sims_admission_recommendation,
                        sims_admission_mother_national_id: $scope.edt.sims_admission_mother_national_id,
                        sims_admission_guardian_national_id: $scope.edt.sims_admission_guardian_national_id,
                        sims_fee_month_code: $scope.edt.sims_fee_month_code,
                        sims_student_attribute5: $scope.edt.sims_student_attribute5,
                        sims_student_attribute6: $scope.edt.sims_student_attribute6,
                        sims_student_attribute7: $scope.edt.sims_student_attribute7,
                        sims_student_attribute8: $scope.edt.sims_student_attribute8,
                        sims_student_attribute9: $scope.edt.sims_student_attribute9,
                        sims_student_attribute10: $scope.edt.sims_student_attribute10,
                        sims_student_attribute11: $scope.edt.sims_student_attribute11,
                        sims_student_attribute12: $scope.edt.sims_student_attribute12,
                        sims_admission_specialAct_status: $scope.edt.sims_admission_specialAct_status,
                        sims_admission_specialAct_desc:$scope.edt.sims_admission_specialAct_desc,
                        sims_admission_communication_status: $scope.edt.sims_admission_communication_status,
                        sims_admission_communication_desc:$scope.edt.sims_admission_communication_desc,
                        sims_admission_falled_grade_status: $scope.edt.sims_admission_falled_grade_status,
                        sims_admission_falled_grade_desc:$scope.edt.sims_admission_falled_grade_desc,
                        sims_admission_learning_therapy_status:$scope.edt.sims_admission_learning_therapy_status,
                        sims_admission_learning_therapy_desc:$scope.edt.sims_admission_learning_therapy_desc,
                        sims_admission_special_education_status:$scope.edt.sims_admission_special_education_status,
                        sims_admission_special_education_desc:$scope.edt.sims_admission_special_education_desc,
                        //new second & third lang code
                        sims_admission_second_lang_code: $scope.edt.sims_admission_second_lang_code,
                        sims_admission_third_lang_code: $scope.edt.sims_admission_third_lang_code,
                        sims_admission_subject_code: $scope.edt.sims_admission_second_lang_code + ',' + $scope.edt.sims_admission_third_lang_code,

                        //home country details code
                        //home_appartment_number: $scope.edt.home_appartment_number,
                        //home_building_number: $scope.edt.home_building_number,
                        //home_street_number: $scope.edt.home_street_number,
                        //home_area_number: $scope.edt.home_area_number,
                        //home_city: $scope.edt.home_city,
                        //home_state: $scope.edt.home_state,
                        //home_country_code: $scope.edt.home_country_code,
                        //home_summary_address: $scope.edt.home_summary_address,
                        //home_po_box: $scope.edt.home_po_box,
                        //home_landmark: $scope.edt.home_landmark,
                        //home_country_isdcode: $scope.edt.home_country_isdcode,
                        //home_phone: $scope.edt.home_country_isdcode + '-' + $scope.edt.home_phone,

                        //sims_house_code: $scope.edt.sims_house_code,
                        opr: 'U'
                    });

                    data2.push(data);

                    $http.post(ENV.apiUrl + "api/common/Admission_dpsd/CUDUpdateAdmission", data2).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        console.log($scope.msg1);
                        if ($scope.msg1.status == true) {
                            //$rootScope.strMessage = $scope.msg1.strMessage;
                            swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm)
                                {
                                    $scope.modal_cancel();
                                }
                            });
                        }
                    });
                }
            }

            $scope.Confirm = function (isvalidate)
            {
                //var data = $scope.edt;

                // data.admission_number = param;
                var data2 = [];
                if (isvalidate)
                {
                   // if ($scope.sectionlst1.length > 0)
                    //  {



                    if ($scope.sectionlst1.current_strength > $scope.sectionlst1.section_strength)
                    {
                            swal({ title: "Alert", text: "Section Strength is Full.If You still want to Continue?", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm)
                                {
                                    $scope.adm_update();
                                }
                                else
                                {
                                    $('#cmb_section').css('background-color', '#E5E9EC');
                                    $('.nav-tabs a[href="#admtab1"]').tab('show');
                                }
                            });
                    }
                    else
                    {
                        $scope.adm_update();
                    }
                   // }
                }
                   
                //$state.go('main.AdmissionDashboard');
            }

            //admission update

            $scope.adm_update = function ()
            {

                //if ($scope.edt.grade_code == '03' || $scope.edt.grade_code == '04')
                //{
                //    if ($scope.edt.sims_admission_second_lang_code == null)
                //        $scope.edt.sims_admission_second_lang_code = "";

                //    if($scope.edt.sims_admission_second_lang_code=="")
                //    {
                //        swal({ title: "Alert", text: "Please Select Second Lang.", showCloseButton: true, width: 380, });
                //        return;
                //    }
                //}

                //if ($scope.edt.grade_code == '05' || $scope.edt.grade_code == '06' || $scope.edt.grade_code == '07' || $scope.edt.grade_code == '08' || $scope.edt.grade_code == '09'||
                //    $scope.edt.grade_code == '10' || $scope.edt.grade_code == '11' || $scope.edt.grade_code == '12')
                //{
                //    if ($scope.edt.sims_admission_third_lang_code == null)
                //        $scope.edt.sims_admission_third_lang_code = "";

                //    if ($scope.edt.sims_admission_second_lang_code == null)
                //        $scope.edt.sims_admission_second_lang_code = "";

                //    if ($scope.edt.sims_admission_third_lang_code == "")
                //    {
                //        if ($scope.edt.sims_admission_second_lang_code == "")
                //        {
                //            swal({ title: "Alert", text: "Please Select Second and third Lang.", showCloseButton: true, width: 380, });
                //            return;
                //        }
                //    }

                //    if ($scope.edt.sims_admission_third_lang_code == "") {
                //        if ($scope.edt.sims_admission_second_lang_code != "") {
                //            swal({ title: "Alert", text: "Please Select Second and third Lang.", showCloseButton: true, width: 380, });
                //            return;
                //        }
                //    }

                //    if ($scope.edt.sims_admission_third_lang_code != "") {
                //        if ($scope.edt.sims_admission_second_lang_code == "") {
                //            swal({ title: "Alert", text: "Please Select Second and third Lang.", showCloseButton: true, width: 380, });
                //            return;
                //        }
                //    }

                   
                //}

                //if ($scope.edt.grade_code == '13' || $scope.edt.grade_code == '14') {
                //    if ($scope.edt.sims_admission_third_lang_code == null)
                //        $scope.edt.sims_admission_third_lang_code = "";

                //    if ($scope.edt.sims_admission_second_lang_code == null)
                //        $scope.edt.sims_admission_second_lang_code = "";

                //    if ($scope.edt.sims_admission_third_lang_code == "") {
                //        if ($scope.edt.sims_admission_second_lang_code == "") {
                //            swal({ title: "Alert", text: "Please Select Mandetory & Optional Subject.", showCloseButton: true, width: 380, });
                //            return;
                //        }
                //    }

                //    if ($scope.edt.sims_admission_third_lang_code == "") {
                //        if ($scope.edt.sims_admission_second_lang_code != "") {
                //            swal({ title: "Alert", text: "Please Select Mandetory & Optional Subject.", showCloseButton: true, width: 380, });
                //            return;
                //        }
                //    }

                //    if ($scope.edt.sims_admission_third_lang_code != "") {
                //        if ($scope.edt.sims_admission_second_lang_code == "") {
                //            swal({ title: "Alert", text: "Please Select Mandetory & Optional Subject.", showCloseButton: true, width: 380, });
                //            return;
                //        }
                //    }
                //}

                if ($scope.edt.home_country_isdcode == null)
                    $scope.edt.home_country_isdcode = "";
                if ($scope.edt.home_phone == null)
                    $scope.edt.home_phone = "";


                var data = ({
                    admission_number: param,
                    appl_num: $scope.edt.appl_num,
                    pros_num: $scope.edt.pros_num,
                    pros_appl_num: $scope.edt.pros_appl_num,
                    admission_date: $scope.edt.admission_date,
                    school_code: $scope.edt.school_code,
                    curr_code: $scope.edt.curr_code,
                    academic_year: $scope.edt.academic_year,
                    grade_code: $scope.edt.grade_code,
                    section_code: $scope.edt.section_code,
                    term_code: $scope.edt.term_code,
                    tent_join_date: $scope.edt.tent_join_date,
                    sims_student_emergency_contact_number1: $scope.edt.sims_student_emergency_contact_number1,
                    sims_student_emergency_contact_name1: $scope.edt.sims_student_emergency_contact_name1,
                    first_name: $scope.edt.first_name,
                    middle_name: $scope.edt.middle_name,
                    last_name: $scope.edt.last_name,
                    family_name: $scope.edt.family_name,
                    first_name_ot: $scope.edt.first_name_ot,
                    midd_name_ot: $scope.edt.midd_name_ot,
                    last_name_ot: $scope.edt.last_name_ot,
                    family_name_ot: $scope.edt.family_name_ot,
                    nicke_name: $scope.edt.nicke_name,
                    birth_date: $scope.edt.birth_date,
                    comm_date: $scope.edt.comm_date,
                    birth_country_code: $scope.edt.birth_country_code,
                    nationality_code: $scope.edt.nationality_code,
                    ethinicity_code: $scope.edt.ethinicity_code,
                    gender_code: $scope.edt.gender_code,
                    religion_code: $scope.edt.religion_code,
                    passport_num: $scope.edt.passport_num,
                    passport_issue_date: $scope.edt.passport_issue_date,
                    passport_expiry: $scope.edt.passport_expiry,
                    passport_issue_auth: $scope.edt.passport_issue_auth,
                    passport_issue_place: $scope.edt.passport_issue_place,
                    visa_number: $scope.edt.visa_number,
                    visa_type: $scope.edt.visa_type,
                    visa_issuing_authority: $scope.edt.visa_issuing_authority,
                    visa_issue_date: $scope.edt.visa_issue_date,
                    visa_expiry_date: $scope.edt.visa_expiry_date,
                    visa_issuing_place: $scope.edt.visa_issuing_place,
                    national_id: $scope.edt.national_id,
                    national_id_issue_date: $scope.edt.national_id_issue_date,
                    national_id_expiry_date: $scope.edt.national_id_expiry_date,
                    sibling_status: $scope.edt.sibling_status,
                    parent_id: $scope.edt.parent_id,
                    sibling_enroll: $scope.edt.sibling_enroll,
                    sibling_name: $scope.edt.sibling_name,
                    sibling_dob: $scope.edt.sibling_dob,
                    sibling_school_code: $scope.edt.sibling_school_code,
                    employee_type: $scope.edt.employee_type,
                    employee_code: $scope.edt.employee_code,
                    employee_school_code: $scope.edt.employee_school_code,
                    motherTounge_language_code: $scope.edt.motherTounge_language_code,
                    main_language_code: $scope.edt.main_language_code,
                    main_language_r_code: $scope.edt.main_language_r_code,
                    main_language_w_code: $scope.edt.main_language_w_code,
                    main_language_s_code: $scope.edt.main_language_s_code,
                    primary_contact_code: $scope.edt.primary_contact_code,
                    primary_contact_pref_code: $scope.edt.primary_contact_pref_code,
                    fee_payment_contact_pref_code: $scope.edt.fee_payment_contact_pref_code,
                    transport_status: $scope.edt.transport_status,
                    transport_desc: $scope.edt.transport_desc,
                    father_salutation_code: $scope.edt.father_salutation_code,
                    father_first_name: $scope.edt.father_first_name,
                    father_middle_name: $scope.edt.father_middle_name,
                    father_last_name: $scope.edt.father_last_name,
                    father_family_name: $scope.edt.father_family_name,
                    father_family_name_ot: $scope.edt.father_family_name_ot,
                    father_nationality1_code: $scope.edt.father_nationality1_code,
                    father_nationality2_code: $scope.edt.father_nationality2_code,
                    father_appartment_number: $scope.edt.father_appartment_number,
                    father_building_number: $scope.edt.father_building_number,
                    father_street_number: $scope.edt.father_street_number,
                    father_area_number: $scope.edt.father_area_number,
                    father_city: $scope.edt.father_city,
                    
                    father_state: $scope.edt.father_state,
                    father_country_code: $scope.edt.father_country_code,
                    father_summary_address: $scope.edt.father_summary_address,
                    father_po_box: $scope.edt.father_po_box,
                    father_phone: $scope.edt.father_phone,
                    father_email: $scope.edt.father_email,
                    father_mobile: $scope.edt.father_mobile,
                    father_fax: $scope.edt.father_fax,
                    father_occupation: $scope.edt.father_occupation,
                    father_company: $scope.edt.father_company,
                    father_passport_number: $scope.edt.father_passport_number,
                    sims_admission_father_national_id: $scope.edt.sims_admission_father_national_id,
                    guardian_salutation_code: $scope.edt.guardian_salutation_code,
                    guardian_first_name: $scope.edt.guardian_first_name,
                    guardian_middle_name: $scope.edt.guardian_middle_name,
                    guardian_last_name: $scope.edt.guardian_last_name,
                    guardian_family_name: $scope.edt.guardian_family_name,
                    guardian_nationality1_code: $scope.edt.guardian_nationality1_code,
                    guardian_nationality2_code: $scope.edt.guardian_nationality2_code,
                    guardian_appartment_number: $scope.edt.guardian_appartment_number,
                    guardian_building_number: $scope.edt.guardian_building_number,
                    guardian_street_number: $scope.edt.guardian_street_number,
                    guardian_area_number: $scope.edt.guardian_area_number,
                    guardian_city: $scope.edt.guardian_city,
                    guardian_state: $scope.edt.guardian_state,
                    guardian_country_code: $scope.edt.guardian_country_code,
                    guardian_summary_address: $scope.edt.guardian_summary_address,
                    guardian_po_box: $scope.edt.guardian_po_box,
                    guardian_phone: $scope.edt.guardian_phone,
                    guardian_mobile: $scope.edt.guardian_mobile,
                    guardian_fax: $scope.edt.guardian_fax,
                    guardian_email: $scope.edt.guardian_email,
                    guardian_occupation: $scope.edt.guardian_occupation,
                    guardian_company: $scope.edt.guardian_company,
                    guardian_passport_number: $scope.edt.guardian_passport_number,
                    guardian_relationship_code: $scope.edt.guardian_relationship_code,
                    mother_salutation_code: $scope.edt.mother_salutation_code,
                    mother_first_name: $scope.edt.mother_first_name,
                    mother_middle_name: $scope.edt.mother_middle_name,
                    mother_last_name: $scope.edt.mother_last_name,
                    mother_family_name: $scope.edt.mother_family_name,
                    mother_name_ot: $scope.edt.mother_name_ot,
                    mother_nationality1_code: $scope.edt.mother_nationality1_code,
                    mother_nationality2_code: $scope.edt.mother_nationality2_code,
                    mother_appartment_number: $scope.edt.mother_appartment_number,
                    mother_building_number: $scope.edt.mother_building_number,
                    mother_street_number: $scope.edt.mother_street_number,
                    mother_area_number: $scope.edt.mother_area_number,
                    mother_city: $scope.edt.mother_city,
                    mother_state: $scope.edt.mother_state,
                    mother_country_code: $scope.edt.mother_country_code,
                    mother_summary_address: $scope.edt.mother_summary_address,
                    mother_po_box: $scope.edt.mother_po_box,
                    mother_phone: $scope.edt.mother_phone,
                    mother_mobile: $scope.edt.mother_mobile,
                    mother_fax: $scope.edt.mother_fax,
                    mother_email: $scope.edt.mother_email,
                    mother_occupation: $scope.edt.mother_occupation,
                    mother_passport_number: $scope.edt.mother_passport_number,
                    current_school_status: $scope.edt.current_school_status,
                    current_school_name: $scope.edt.current_school_name,
                    current_school_enroll_number: $scope.edt.current_school_enroll_number,
                    current_school_grade: $scope.edt.current_school_grade,
                    current_school_cur: $scope.edt.current_school_cur,
                    current_school_from_date: $scope.edt.current_school_from_date,
                    current_school_to_date: $scope.edt.current_school_to_date,
                    current_school_language: $scope.edt.current_school_language,
                    current_school_head_teacher: $scope.edt.current_school_head_teacher,
                    current_school_phone: $scope.edt.current_school_phone,
                    current_school_fax: $scope.edt.current_school_fax,
                    current_school_city: $scope.edt.current_school_city,
                    current_school_country_code: $scope.edt.current_school_country_code,
                    current_school_address: $scope.edt.current_school_address,
                    marketing_code: $scope.edt.marketing_code,
                    marketing_description: $scope.edt.marketing_description,
                    parent_status_code: $scope.edt.parent_status_code,
                    legal_custody_code: $scope.edt.legal_custody_code,
                    health_card_number: $scope.edt.health_card_number,
                    health_card_issue_date: $scope.edt.health_card_issue_date,
                    health_card_issuing_authority: $scope.edt.health_card_issuing_authority,
                    blood_group_code: $scope.edt.blood_group_code,
                    medication_status: $scope.edt.medication_status,
                    medication_desc: $scope.edt.medication_desc,
                    disability_status: $scope.edt.disability_status,
                    disability_desc: $scope.edt.disability_desc,
                    behaviour_status: $scope.edt.behaviour_status,
                    behaviour_desc: $scope.edt.behaviour_desc,
                    health_restriction_status: $scope.edt.health_restriction_status,
                    health_restriction_desc: $scope.edt.health_restriction_desc,
                    health_hearing_status: $scope.edt.health_hearing_status,
                    health_hearing_desc: $scope.edt.health_hearing_desc,
                    health_vision_status: $scope.edt.health_vision_status,
                    health_vision_desc: $scope.edt.health_vision_desc,
                    health_other_status: $scope.edt.health_other_status,
                    health_other_desc: $scope.edt.health_other_desc,
                    gifted_status: $scope.edt.gifted_status,
                    gifted_desc: $scope.edt.gifted_desc,
                    music_status: $scope.edt.music_status,
                    music_desc: $scope.edt.music_desc,
                    sports_status: $scope.edt.sports_status,
                    sports_desc: $scope.edt.sports_desc,
                    language_support_status: $scope.edt.language_support_status,
                    language_support_desc: $scope.edt.language_support_desc,
                    declaration_status: $scope.edt.declaration_status,
                    fees_paid_status: $scope.edt.fees_paid_status,
                    fee_category_code: $scope.edt.fee_category_code,
                    ip: $scope.edt.ip,
                    dns: $scope.edt.dns,
                    user_code: $scope.edt.user_code,
                    status: $scope.edt.status,
                    sims_student_attribute1: $scope.edt.sims_student_attribute1,
                    sims_student_attribute2: $scope.edt.sims_student_attribute2,
                    sims_student_attribute3: $scope.edt.sims_student_attribute3,
                    sims_student_attribute4: $scope.edt.sims_student_attribute4,
                    sims_student_health_respiratory_status: $scope.edt.sims_student_health_respiratory_status,
                    sims_student_health_respiratory_desc: $scope.edt.sims_student_health_respiratory_desc,
                    sims_student_health_hay_fever_status: $scope.edt.sims_student_health_hay_fever_status,
                    sims_student_health_hay_fever_desc: $scope.edt.sims_student_health_hay_fever_desc,
                    sims_student_health_epilepsy_status: $scope.edt.sims_student_health_epilepsy_status,
                    sims_student_health_epilepsy_desc: $scope.edt.sims_student_health_epilepsy_desc,
                    sims_student_health_skin_status: $scope.edt.sims_student_health_skin_status,
                    sims_student_health_skin_desc: $scope.edt.sims_student_health_skin_desc,
                    sims_student_health_diabetes_status: $scope.edt.sims_student_health_diabetes_status,
                    sims_student_health_diabetes_desc: $scope.edt.sims_student_health_diabetes_desc,
                    sims_student_health_surgery_status: $scope.edt.sims_student_health_surgery_status,
                    sims_student_health_surgery_desc: $scope.edt.sims_student_health_surgery_desc,
                    sims_admission_recommendation: $scope.edt.sims_admission_recommendation,
                    sims_admission_mother_national_id: $scope.edt.sims_admission_mother_national_id,
                    sims_admission_guardian_national_id: $scope.edt.sims_admission_guardian_national_id,
                    sims_fee_month_code: $scope.edt.sims_fee_month_code,
                    sims_student_attribute5: $scope.edt.sims_student_attribute5,
                    sims_student_attribute6: $scope.edt.sims_student_attribute6,
                    sims_student_attribute7: $scope.edt.sims_student_attribute7,
                    sims_student_attribute8: $scope.edt.sims_student_attribute8,
                    sims_student_attribute9: $scope.edt.sims_student_attribute9,
                    sims_student_attribute10: $scope.edt.sims_student_attribute10,
                    sims_student_attribute11: $scope.edt.sims_student_attribute11,
                    sims_student_attribute12: $scope.edt.sims_student_attribute12,

                    //new second & third language details 
                    sims_admission_second_lang_code: $scope.edt.sims_admission_second_lang_code,
                    sims_admission_third_lang_code: $scope.edt.sims_admission_third_lang_code,
                    sims_admission_subject_code: $scope.edt.sims_admission_second_lang_code + ',' + $scope.edt.sims_admission_third_lang_code,
                    //sims_house_code: $scope.edt.sims_house_code,

                    ////new code
                    //home_appartment_number: $scope.edt.home_appartment_number,
                    //home_building_number: $scope.edt.home_building_number,
                    //home_street_number: $scope.edt.home_street_number,
                    //home_area_number: $scope.edt.home_area_number,
                    //home_city: $scope.edt.home_city,
                    //home_state: $scope.edt.home_state,
                    //home_country_code: $scope.edt.home_country_code,
                    //home_summary_address: $scope.edt.home_summary_address,
                    //home_po_box: $scope.edt.home_po_box,
                    //home_landmark: $scope.edt.home_landmark,
                    //home_country_isdcode: $scope.edt.home_country_isdcode,
                    //home_phone: $scope.edt.home_country_isdcode + '-' + $scope.edt.home_phone,

                });

                data2.push(data);

                $http.post(ENV.apiUrl + "api/common/Admission_dpsd/ApproveStudent", data2).then(function (res) {
                    $scope.display = true;
                    $scope.msg1 = res.data;
                    console.log($scope.msg1);
                });

                $scope.grid1 = true;
                $('#myModal1').modal({ backdrop: 'static', keyboard: true });
            }

            $scope.modal_cancel = function ()
            {
                //$('body').addClass('grey condense-menu');
                //$('#main-menu').addClass('mini');
                //$('.page-content').addClass('condensed');
                //$scope.isCondensed = true;
                //$("body").removeClass("modal-open");
                //$("div").removeClass("modal-backdrop in");
                $('#myModal1').modal('hide');

                console.log($stateParams.Class);
                $timeout(function () {
                  
                        if ($http.defaults.headers.common['schoolId'] == 'tiadxb' || $http.defaults.headers.common['schoolId'] == 'tiashj' || $http.defaults.headers.common['schoolId'] == 'tosdxb' || $http.defaults.headers.common['schoolId'] == 'aisdxb') 
                      
                            $state.go("main.dashL", { Class: $stateParams.Class });

                    else
                            $state.go("main.dashL", { Class: $stateParams.Class });

                }, 300);
            }


            $scope.get_studentfee = function ()
            {
                $state.go("main.Sim043");
            }

            $scope.back = function ()
            {
                $scope.dd = true;
                window.localStorage['Admflag'] = $scope.dd;
                $scope.modal_cancel();
            }

            //checkbox_visibility_start

            $scope.schoolcheck = function () {
                if ($scope.edt.current_school_status == true)
                {
                    $scope.school_edit = false;
                }
                else
                {
                    $scope.school_edit = true;
                }
            }

            $scope.chk_desc = function ()
            {
                if ($scope.edt.marketing_code == true)
                {
                    $scope.desc_edit = false;
                }
                else
                {
                    $scope.desc_edit = true;
                }
            }

            $scope.chk_cicumstances = function ()
            {
                if ($scope.edt.parent_status_code == true)
                {
                    $scope.circum_edit = false;
                }
                else
                {
                    $scope.circum_edit = true;
                }
            }

            $scope.chk_disab_status = function () {
                if ($scope.edt.disability_status == true) {
                    $scope.disab_edit = false;
                }
                else {
                    $scope.disab_edit = true;
                }
            }

            $scope.chk_health_res = function () {
                if ($scope.edt.health_restriction_status == true) {
                    $scope.healthres_edit = false;
                }
                else {
                    $scope.healthres_edit = true;
                }

            }

            $scope.chk_medic_status = function () {
                if ($scope.edt.medication_status == true) {
                    $scope.med_edit = false;
                }
                else {
                    $scope.med_edit = true;
                }
            }

            $scope.chk_other = function () {
                if ($scope.edt.health_other_status == true) {
                    $scope.other_edit = false;
                }
                else {
                    $scope.other_edit = true;
                }
            }

            $scope.chk_hearing = function () {
                if ($scope.edt.health_hearing_status == true) {
                    $scope.hearing_edit = false;
                }
                else {
                    $scope.hearing_edit = true;
                }
            }

            $scope.chk_vision = function () {
                if ($scope.edt.health_vision_status == true) {
                    $scope.health_vision_edit = false;
                }
                else {
                    $scope.health_vision_edit = true;
                }
            }

            $scope.chk_behaviour = function () {
                if ($scope.edt.behaviour_status == true) {
                    $scope.behaviour_edit = false;
                }
                else {
                    $scope.behaviour_edit = true;
                }
            }

            $scope.chk_gifted_status = function () {
                if ($scope.edt.gifted_status == true) {
                    $scope.gifted_edit = false;
                }
                else {
                    $scope.gifted_edit = true;
                }
            }

            $scope.chk_music_pro = function () {
                if ($scope.edt.music_status == true) {
                    $scope.music_edit = false;
                }
                else {
                    $scope.music_edit = true;
                }
            }

            $scope.chk_sport_pro = function () {
                if ($scope.edt.sports_status == true) {
                    $scope.sport_edit = false;
                }
                else {
                    $scope.sport_edit = true;
                }
            }

            $scope.chk_lang_sup = function () {
                if ($scope.edt.language_support_status == true) {
                    $scope.lang_sup_edit = false;
                }
                else {
                    $scope.lang_sup_edit = true;
                }
            }

            $scope.chk_Pri_contact = function () {
                if ($scope.edt.primary_contact_code == true) {
                    $scope.pricont_edit = false;
                }
                else {
                    $scope.pricont_edit = true;
                }
            }

            $scope.chk_transport_requin = function () {
                if ($scope.edt.transport_status == true) {
                    $scope.transreq_edit = false;
                }
                else {
                    $scope.transreq_edit = true;
                }
            }

            $scope.chk_employee = function () {
                var v = document.getElementById('chk_employee');
                if (v.checked == true) {
                    var v = document.getElementById('txt_emp_code');
                    v.disabled = false;
                }
                else {
                    var v = document.getElementById('txt_emp_code');
                    v.disabled = true;
                }
            }

            $scope.chk_Sibling_attend = function () 
            {
               
                if ($scope.edt.sibling_status == true)
                {
                    $scope.disableparent = false;
                }
                else {
                    $scope.disableparent = true;
                }
            }



            $scope.chk_learn_ther = function () {
                if ($scope.edt.sims_admission_learning_therapy_status == true) {
                    $scope.learnther_edit = false;
                }
                else {
                    $scope.learnther_edit = true;
                }
            }

            $scope.chk_spec_edu = function () {
                if ($scope.edt.sims_admission_special_education_status == true) {
                    $scope.spec_edu_edit = false;
                }
                else {
                    $scope.spec_edu_edit = true;
                }
            }

            $scope.chk_falled_grade = function () {
                if ($scope.edt.sims_admission_falled_grade_status == true) {
                    $scope.falledgrade_edit = false;
                }
                else {
                    $scope.falledgrade_edit = true;
                }
            }

            $scope.chk_commun = function () {
                if ($scope.edt.sims_admission_communication_status == true) {
                    $scope.commun_edit = false;
                }
                else {
                    $scope.commun_edit = true;
                }
            }

            $scope.chk_spec_activies = function () {
                if ($scope.edt.sims_admission_specialAct_status == true) {
                    $scope.specActiv_edit = false;
                }
                else {
                    $scope.specActiv_edit = true;
                }
            }

            $scope.chk_studyingin = function () {
                if ($scope.edt.sibling_status == true) {
                    $scope.Study_edit = false;
                    var v = document.getElementById('t3');
                    var v = document.getElementById('t4');
                    var v = document.getElementById('t5');
                    $scope.t = false;
                }
                else {
                    $scope.Study_edit = true;
                    var v = document.getElementById('t3');
                    var v = document.getElementById('t4');
                    var v = document.getElementById('t5');
                    $scope.t = true;
                }
            }

            $scope.chk_ticking = function ()
            {
               // var v = document.getElementById('chk_ticking');
                if ($scope.edt.declaration_status == true)
                {
                    $scope.tick_edit = false;
                    $scope.confirm_edit = false;
                    // var v = document.getElementById('save');
                    // v.disabled = false;
                }
                else
                {
                    $scope.tick_edit = true;
                    $scope.confirm_edit = true;
                    // var v = document.getElementById('save');
                    // v.disabled = true;
                }
            }

            $scope.chk_copy_mother_add = function () {
                if ($scope.edt.copy_add == true) {
                    $scope.edt.mother_appartment_number = $scope.edt.father_appartment_number;
                    $scope.edt.mother_area_number = $scope.edt.father_area_number;
                    $scope.edt.mother_building_number = $scope.edt.father_building_number;
                    $scope.edt.mother_street_number = $scope.edt.father_street_number;
                    $scope.edt.mother_city = $scope.edt.father_city;
               
                    $scope.edt.mother_state = $scope.edt.father_state;
                    $scope.edt.mother_country_code = $scope.edt.father_country_code;
                    $scope.edt.mother_po_box = $scope.edt.father_po_box;
                    $scope.edt.mother_nationality1_code = $scope.edt.father_nationality1_code;
                    $scope.edt.mother_nationality2_code = $scope.edt.father_nationality2_code;
                    $scope.edt.mother_summary_address = $scope.edt.father_summary_address;
                    $scope.edt.mother_phone = $scope.edt.father_phone;
                    $scope.edt.mother_po_box = $scope.edt.father_po_box;
                    $scope.edt.mother_mobile = $scope.edt.father_mobile;
                    $scope.edt.mother_occupation = $scope.edt.father_occupation;
                    $scope.edt.mother_email = $scope.edt.father_email;
                    $scope.edt.mother_company = $scope.edt.father_company;
                }
                else {
                    $scope.edt.mother_appartment_number = "";
                    $scope.edt.mother_area_number = "";
                    $scope.edt.mother_building_number = "";
                    $scope.edt.mother_street_number = "";
                    $scope.edt.mother_city = "";
                    $scope.edt.mother_state = "";
                    $scope.edt.mother_country_code = "";
                    $scope.edt.mother_po_box = "";
                    $scope.edt.mother_nationality1_code = "";
                    $scope.edt.mother_nationality2_code = "";
                    $scope.edt.mother_summary_address = "";
                    $scope.edt.mother_phone = "";
                    $scope.edt.mother_po_box = "";
                    $scope.edt.mother_mobile = "";
                    $scope.edt.mother_occupation = "";
                    $scope.edt.mother_email = "";
                    $scope.edt.mother_company = "";
                }

            }

            $scope.chk_copy_guar_add1 = function () {
                if ($scope.edt.copy_add1 == true) {

                    $scope.edt.guardian_appartment_number = $scope.edt.father_appartment_number;
                    $scope.edt.guardian_area_number = $scope.edt.father_area_number;
                    $scope.edt.guardian_building_number = $scope.edt.father_building_number;
                    $scope.edt.guardian_street_number = $scope.edt.father_street_number;
                    $scope.edt.guardian_city = $scope.edt.father_city;
                    $scope.edt.guardian_state = $scope.edt.father_state;
                    $scope.edt.guardian_country_code = $scope.edt.father_country_code;
                    $scope.edt.guardian_po_box = $scope.edt.father_po_box;
                    $scope.edt.guardian_nationality1_code = $scope.edt.father_nationality1_code;
                    $scope.edt.guardian_nationality2_code = $scope.edt.father_nationality2_code;
                    $scope.edt.guardian_summary_address = $scope.edt.father_summary_address;
                    $scope.edt.guardian_phone = $scope.edt.father_phone;
                    $scope.edt.guardian_po_box = $scope.edt.father_po_box;
                    $scope.edt.guardian_mobile = $scope.edt.father_mobile;
                    $scope.edt.guardian_occupation = $scope.edt.father_occupation;
                    $scope.edt.guardian_email = $scope.edt.father_email;
                    $scope.edt.guardian_company = $scope.edt.father_company;
                }
                else {
                    $scope.edt.guardian_appartment_number = "";
                    $scope.edt.guardian_area_number = "";
                    $scope.edt.guardian_building_number = "";
                    $scope.edt.guardian_street_number = "";
                    $scope.edt.guardian_city = "";
                    $scope.edt.guardian_state = "";
                    $scope.edt.guardian_country_code = "";
                    $scope.edt.guardian_po_box = "";
                    $scope.edt.guardian_nationality1_code = "";
                    $scope.edt.guardian_nationality2_code = "";
                    $scope.edt.guardian_summary_address = "";
                    $scope.edt.guardian_phone = "";
                    $scope.edt.guardian_po_box = "";
                    $scope.edt.guardian_mobile = "";
                    $scope.edt.guardian_occupation = "";
                    $scope.edt.guardian_email = "";
                    $scope.edt.guardian_company = "";
                }

            }

            $scope.GetFatherSumAddr = function () {
                var country = document.getElementById('cmb_father_country').options[document.getElementById('cmb_father_country').selectedIndex].text;

                var father_aprt = $("#txt_father_aprt").val().length;
                var father_build = $("#txt_father_build").val().length;
                var father_street = $("#txt_father_Street").val().length;
                var father_area = $("#txt_father_area").val().length;
                var father_city = $("#txt_father_city").val().length;
                var father_state = $("#txt_father_state").val().length;
                var father_country = $("#cmb_father_country").val().length;
                var father_POBox = $("#txt_father_pobox").val().length;

                document.getElementById('txt_father_summary_add').value = $("#txt_father_aprt").val().length > 0 ? document.getElementById('txt_father_aprt').value + "," : "";
                document.getElementById('txt_father_summary_add').value += $("#txt_father_build").val().length > 0 ? document.getElementById('txt_father_build').value + "," : "";
                document.getElementById('txt_father_summary_add').value += $("#txt_father_Street").val().length > 0 ? document.getElementById('txt_father_Street').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_area > 0 ? document.getElementById('txt_father_area').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_city > 0 ? document.getElementById('txt_father_city').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_state > 0 ? document.getElementById('txt_father_state').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_country > 0 ? country + "," : "";
                document.getElementById('txt_father_summary_add').value += father_POBox > 0 ? document.getElementById('txt_father_pobox').value : "";
            }

            $scope.GetMotherSumAddr = function () {
                var mother_aprt = $("#txt_mother_aprt").val().length;
                var mother_build = $("#txt_mother_build").val().length;
                var mother_street = $("#txt_mother_street").val().length;
                var mother_area = $("#txt_mother_area").val().length;
                var mother_city = $("#txt_mother_city").val().length;
                var mother_state = $("#txt_mother_state").val().length;
                var mother_country = $("#cmb_mother_country").val().length;
                var mother_POBox = $("#txt_mother_pobox").val().length;

                var country = document.getElementById('cmb_mother_country').options[document.getElementById('cmb_mother_country').selectedIndex].text;

                document.getElementById('txt_mother_addr').value = mother_aprt > 0 ? document.getElementById('txt_mother_aprt').value + "," : "";
                document.getElementById('txt_mother_addr').value += mother_build > 0 ? document.getElementById('txt_mother_build').value + "," : "";
                document.getElementById('txt_mother_addr').value += mother_street > 0 ? document.getElementById('txt_mother_street').value + "," : "";
                document.getElementById('txt_mother_addr').value += mother_area > 0 ? document.getElementById('txt_mother_area').value + "," : "";
                document.getElementById('txt_mother_addr').value += mother_city > 0 ? document.getElementById('txt_mother_city').value + "," : "";
                document.getElementById('txt_mother_addr').value += mother_state > 0 ? document.getElementById('txt_mother_state').value + "," : "";
                document.getElementById('txt_mother_addr').value += mother_country > 0 ? country + "," : "";
                document.getElementById('txt_mother_addr').value += mother_POBox > 0 ? document.getElementById('txt_mother_pobox').value : "";
            }

            $scope.GetGuardianSumAddr = function () {
                var guar_aprt = $("#txt_guar_aprt").val().length;
                var guar_build = $("#txt_guar_build").val().length;
                var guar_street = $("#txt_guar_street").val().length;
                var guar_area = $("#txt_guar_area").val().length;
                var guar_city = $("#txt_guar_city").val().length;
                var guar_state = $("#txt_guar_state").val().length;
                var guar_country = $("#cmb_gar_country").val().length;
                var guar_POBox = $("#txt_guar_pobox").val().length;

                var country = document.getElementById('cmb_gar_country').options[document.getElementById('cmb_gar_country').selectedIndex].text;

                document.getElementById('txt_guar_addr').value = guar_aprt > 0 ? document.getElementById('txt_guar_aprt').value + "," : "";
                document.getElementById('txt_guar_addr').value += guar_build > 0 ? document.getElementById('txt_guar_build').value + "," : "";
                document.getElementById('txt_guar_addr').value += guar_street > 0 ? document.getElementById('txt_guar_street').value + "," : "";
                document.getElementById('txt_guar_addr').value += guar_area > 0 ? document.getElementById('txt_guar_area').value + "," : "";
                document.getElementById('txt_guar_addr').value += guar_city > 0 ? document.getElementById('txt_guar_city').value + "," : "";
                document.getElementById('txt_guar_addr').value += guar_state > 0 ? document.getElementById('txt_guar_state').value + "," : "";
                document.getElementById('txt_guar_addr').value += guar_country > 0 ? country + "," : "";
                document.getElementById('txt_guar_addr').value += guar_POBox > 0 ? document.getElementById('txt_guar_pobox').value : "";
            }


            ///
            $scope.chk_more_sibling = function () {
                var v = document.getElementById('chk_more_sibling');
                if (v.checked == true) {
                    var v = document.getElementById('txt_more_sibling');
                    v.disabled = false;
                }
                else {
                    var v = document.getElementById('txt_more_sibling');
                    v.disabled = true;
                }
            }

            $scope.chk_employee = function () {
                var v = document.getElementById('chk_employee');
                if (v.checked == true) {
                    var v = document.getElementById('txt_emp_code');
                    v.disabled = false;
                }
                else {
                    var v = document.getElementById('txt_emp_code');
                    v.disabled = true;
                }
            }

           
            $scope.getFatherDetails = function () {
                var data = $scope.edt;
                var v = document.getElementById('txt_mother_aprt');
                v.value = document.getElementById('txt_father_aprt').value;
                data.mother_appartment_number = v.value;

                var v = document.getElementById('txt_mother_build');
                v.value = document.getElementById('txt_father_build').value;
                data.mother_building_number = v.value;

                var v = document.getElementById('txt_mother_street');
                v.value = document.getElementById('txt_father_Street').value;
                data.mother_street_number = v.value;

                var v = document.getElementById('txt_mother_area');
                v.value = document.getElementById('txt_father_area').value;
                data.mother_area_number = v.value;

                var v = document.getElementById('txt_mother_city');
                v.value = document.getElementById('txt_father_city').value;
                data.mother_city = v.value;

                var v = document.getElementById('txt_mother_state');
                v.value = document.getElementById('txt_father_state').value;
                data.mother_state = v.value;

                var v = document.getElementById('cmb_mother_country');
                v.value = document.getElementById('cmb_father_country').value;
                data.mother_country_code = v.value;

                var v = document.getElementById('txt_mother_pobox');
                v.value = document.getElementById('txt_father_pobox').value;
                data.mother_po_box = v.value;
            }


            $scope.GetFatherSumAddr = function ()
            {
                var data = $scope.edt;

                var father_aprt = $("#txt_father_aprt").val().length;
                var father_build = $("#txt_father_build").val().length;
                var father_street = $("#txt_father_Street").val().length;
                var father_area = $("#txt_father_area").val().length;
                var father_university = $("#txt_father_uni").val().length;
                var father_city = $("#txt_father_city").val().length;
                var father_state = $("#txt_father_state").val().length;
                var father_country = $("#cmb_father_country").val().length;
                var father_POBox = $("#txt_father_pobox").val().length;

                var country = document.getElementById('cmb_father_country').options[document.getElementById('cmb_father_country').selectedIndex].text;
                document.getElementById('txt_father_summary_add').value = $("#txt_father_aprt").val().length > 0 ? document.getElementById('txt_father_aprt').value + "," : "";
                document.getElementById('txt_father_summary_add').value += $("#txt_father_build").val().length > 0 ? document.getElementById('txt_father_build').value + "," : "";
                document.getElementById('txt_father_summary_add').value += $("#txt_father_Street").val().length > 0 ? document.getElementById('txt_father_Street').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_university > 0 ? document.getElementById('txt_father_uni').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_area > 0 ? document.getElementById('txt_father_area').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_city > 0 ? document.getElementById('txt_father_city').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_state > 0 ? document.getElementById('txt_father_state').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_country > 0 ? country + "," : "";
                document.getElementById('txt_father_summary_add').value += father_POBox > 0 ? document.getElementById('txt_father_pobox').value : "";

                var v = document.getElementById('txt_father_summary_add');
                data.father_summary_address = v.value;
                console.log(data.father_summary_address);
            }


            //checkbox_visibility_end

            $scope.Cancel = function ()
            {
                $scope.edt = "";
                $scope.msg1 = "";
                data = "";
                data1 = "";
                //var v = document.getElementById('txt_first_name1');
                //v.value = "";
                //var v = document.getElementById('txt_last_name1');
                //v.value = "";
                //var v = document.getElementById('txt_middle_name1');
                //v.value = "";
                //var v = document.getElementById('txt_mobile');
                //v.value = "";
                //var v = document.getElementById('txt_email');
                //v.value = "";
                //var v = document.getElementById('txt_addr');
                //v.value = "";
                //var v = document.getElementById('txt_national_id');
                //v.value = "";

            }


            //upload image code start

            var formdata = new FormData();

            $scope.getTheFiles = function ($files) {
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.file_changedF = function (element) {
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;
                    });
                };
                reader.readAsDataURL(photofile);

            };

            $scope.uploadImgClickF = function () {
                $scope.uploadImgClickFShow = true;
            }

            $scope.uploadImgFCancel = function () {
                $scope.uploadImgClickFShow = false;
            }

            $scope.uploadImgF = function () {
                var request = {
                    method: 'POST',
                    url: ENV.apiUrl + '/api/fileNew/upload?filename=' + param + "_f" + "&location=" + "Images/ParentImage",
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request).success(function (d) {

                    var data = {
                        admission_number: pros_num,
                        sims_father_img: d,
                        opr: 'F2'

                    }

                    $http.post(ENV.apiUrl + "api/common/Admission_dpsd/CUDUpdateAdm_Studentimage", data).then(function (res) {
                        $scope.ins = res.data;
                    });

                    $scope.uploadImgClickFShow = false;


                });



            }

            //mother_image upload
            $scope.file_changedM = function (element) {
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_imgM = e.target.result;
                    });
                };
                reader.readAsDataURL(photofile);
            };

            $scope.uploadImgClickM = function () {
                $scope.uploadImgClickMShow = true;
            }

            $scope.uploadImgMCancel = function () {
                $scope.uploadImgClickMShow = false;
            }

            $scope.uploadImgM = function () {
                var request = {
                    method: 'POST',
                    url: ENV.apiUrl + '/api/fileNew/upload?filename=' + param + "_m" + "&location=" + "Images/ParentImage",
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request).success(function (d) {
                    $scope.loadingscr = false;
                    //alert(mother);
                    var data = {
                        admission_number: pros_num,
                        sims_mother_img: d,
                        opr: 'F3'

                    }

                    $http.post(ENV.apiUrl + "api/common/Admission_dpsd/CUDUpdateAdm_Studentimage", data).then(function (res) {
                        $scope.ins = res.data;
                    });

                    $scope.loadingscr = true;
                    $scope.uploadImgClickMShow = false;


                });



            }

            //student_image upload
            $scope.file_changedS = function (element) {
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_imgS = e.target.result;
                    });
                };
                reader.readAsDataURL(photofile);
            };

            $scope.uploadImgClickS = function () {
                $scope.uploadImgClickSShow = true;
            }

            $scope.uploadImgSCancel = function () {
                $scope.uploadImgClickSShow = false;
            }

            $scope.uploadImgS = function () {
                $scope.loadingscr = true;
                var request = {
                    method: 'POST',
                    url: ENV.apiUrl + '/api/fileNew/upload?filename=' + param + "_s" + "&location=" + "Images/StudentImage",
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request).success(function (d) {

                    //alert(mother);
                    var data = {
                        admission_number: pros_num,
                        //sims_mother_img: d,
                        sims_student_img: d,
                        opr: 'F1'

                    }

                    $http.post(ENV.apiUrl + "api/common/Admission_dpsd/CUDUpdateAdm_Studentimage", data).then(function (res) {
                        $scope.ins = res.data;
                    });
                    $scope.loadingscr = true;

                    $scope.uploadImgClickSShow = false;


                });



            }




            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
        }]);

    //simsController.directive('ngFiles', ['$parse', function ($parse) {

    //    function fn_link(scope, element, attrs) {
    //        var onChange = $parse(attrs.ngFiles);
    //        element.on('change', function (event) {
    //            onChange(scope, { $files: event.target.files });
    //        });
    //    };

    //    return {
    //        link: fn_link
    //    }
    //}])


    simsController.directive('filedirective', ['$parse', function ($parse) {

        return {
            restrict: 'EA',
            link: function (scope, element, attrs) {
                var onChange = $parse(attrs.ngFiles);
                element.on('change', function (event) {
                    onChange(scope, { $files: event.target.files });
                });

                element.on('submit', function () {
                    console.log(element[0]);
                    // find the first invalid element
                    var firstInvalid = element[0].querySelector('.ng-invalid');

                    // if we find one, set focus
                    if (firstInvalid) {
                        firstInvalid.focus();
                        console.log(firstInvalid);
                        return;
                    }
                });
            }

        }
     }])

})();