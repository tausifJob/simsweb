﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudentEANumberCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            $scope.search_btn = true;
            $scope.clr_btn = true;
            $scope.Update_btn = false;
            $scope.newdisplay = false;
            $scope.table = true;
            $scope.grid = false;
            var fetchedRecords = [];

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.temp = {
                    'sims_cur_code': $scope.curriculum[0].sims_cur_code
                }
                $scope.getAccYear($scope.temp.sims_cur_code);
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                        'sims_academic_year': $scope.Acc_year[0].sims_academic_year
                    }
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                });
            }

            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
            }

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }

            $scope.stud_StudEA_Details_fun = function (curcode, acYear, gCode, sCode) {
                $scope.grid = true;
                $http.get(ENV.apiUrl + "api/StudentEA/getStudentEA?cur_code=" + curcode + "&academic_year=" + acYear + "&grade_code=" + gCode + "&section_code=" + sCode).then(function (StudEADetails) {
                    $scope.StudEA_Details = StudEADetails.data;
                    $scope.fetchedRecords = StudEADetails.data;
                    if ($scope.StudEA_Details.length > 0) {
                        $scope.table = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.StudEA_Details.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.StudEA_Details.length, data: 'All' })

                        }
                    }
                    else {
                        swal({ text: "Records not found", width: 300, showCloseButton: true });
                        $scope.grid = false;
                        $scope.table = true;
                    }

                });
            }

            $scope.countData = [
                    { val: 5, data: 5 },
                    { val: 10, data: 10 },
                    { val: 15, data: 15 },
            ]

            $scope.size = function (str) {
                //if (str == 5 || str == 10 || str == 15) {
                //    $scope.pager = true;
                //}
                //else {
                //    $scope.pager = false;
                //}
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    $scope.filteredTodos[i].icon = 'fa fa-plus-circle';
                }


            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                debugger;
                $scope.StudEA_Details = $scope.searched($scope.StudEA_Details, $scope.searchText);
                if ($scope.searchText != '') {
                    $scope.StudEA_Details = $scope.StudEA_Details;
                }
                else { $scope.StudEA_Details = $scope.fetchedRecords; }
               
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_enrollment_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_student_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_student_ea_number == toSearch) ? true : false;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.newdisplay = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.dest_code = "";
                $scope.temp.country_code = "";
                $scope.temp.dest_name = "";
            }

            $scope.clear = function () {
                $scope.temp = '';
                $scope.grid = true;
                $scope.table = true;
            }

            $scope.updatedata = function (ea) {
                ea.isChange = true;
            }

            $scope.update = function () {

                var dataupdate = [];
                var data = {};
                for (var k = 0; k < $scope.StudEA_Details.length; k++) {
                    if ($scope.StudEA_Details[k].isChange == true) {
                        data =
                       {
                           sims_enrollment_number: $scope.StudEA_Details[k].sims_enrollment_number,
                           sims_student_ea_number: $scope.StudEA_Details[k].sims_student_ea_number,
                           opr: 'U',
                       }
                        dataupdate.push(data);
                    }

                }

                if (dataupdate.length > 0) {
                    $http.post(ENV.apiUrl + "api/StudentEA/CUDStudentEA", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, showCloseButton: true });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, showCloseButton: true });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    })
                    $scope.grid = true;
                    $scope.table = true;
                    $scope.newdisplay = false;
                    dataupdate = [];

                }
                else {
                    swal({ text: "Change Atleast one Record to Update", width: 380, showCloseButton: true });
                }
            }
         }])
})();
