﻿
(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [];
    var doc_no;
    var simsController = angular.module('sims.module.Student');
    simsController.controller('PaymentApproval_ABQCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.pager = true;
            $scope.failure_payment = true;
            $scope.failure = false;
            $scope.pagggg = false;
            var user = $rootScope.globals.currentUser.username;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            $scope.temp = {}



            $scope.size = function (str) {

                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.FeeReceiptData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $http.get(ENV.apiUrl + "api/Fee/StudentFee_New/GetPaymentMode").then(function (res1) {
                debugger;
                $scope.PaymentModes = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp.temp_dd_payment_type = res1.data[1].pmDescShort;
                   
                }
                setTimeout(function () {
                    $('#dd_payment_type').multipleSelect({
                     
                    })
                }, 100);

            });
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            }
            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.FeeReceiptData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.FeeReceiptData;
                }

                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.temp_dd_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.total_amt.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.temp_dd_fee_transaction_id.toLowerCase().indexOf(toSearch.toLowerCase()) > -1||
                    item.temp_dd_doc_date_time.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.temp_dd_error_messsage.toLowerCase().indexOf(toSearch.toLowerCase()) > -1||
                    item.temp_dd_doc_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.paginationview1 = document.getElementById("paginationview");
            //date dd-MM-yyyy formate
            $scope.showdate = function (date, name) {
                debugger
                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //$scope.edt[name] = year + "/" + month + "/" + day;
                $scope.edt[name] = date;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.FeeReceiptData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.FeeReceiptData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked()

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.doc_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }


            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }

            var yyyy = today.getFullYear();
            //$scope.sdate = yyyy + '-' + mm + '-' + dd;
            //$scope.edate = yyyy + '-' + mm + '-' + dd;
            //$scope.edt = {
            //    from_date: yyyy + '-' + mm + '-' + dd,
            //    to_date: yyyy + '-' + mm + '-' + dd,

            //}

            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.edt = {
                from_date: dd + '-' + mm + '-' + yyyy,
                to_date: dd + '-' + mm + '-' + yyyy,

            }
            $scope.temp = {
                sims_realizedate: dd + '-' + mm + '-' + yyyy,
            
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.onKeyPress = function ($event) {
                debugger;
                if ($event.keyCode == 13) {

                    //  $scope.Show($scope.edt.from_date, $scope.edt.to_date, $scope.edt.search);
                    // console.log("enetr");
                }
            };

            $scope.check_all_check = function (str) {


                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if (str) {
                        $scope.filteredTodos[i]['approve_status'] = true;
                    }
                    else {
                        $scope.filteredTodos[i]['approve_status'] = false;

                    }
                }
            }
            var pm, pm_type;
           
            $scope.FAILURE_Data = function () {
                debugger
                $scope.failure = true;
                $scope.failure_payment = false;
                $scope.table1 = true;
                $scope.pagggg = true;
                $scope.ImageView = false;
                pm_type = [];
                
                //if ($scope.edt.temp_dd_payment_type.length == 0 || $scope.edt == undefined) {
                if ($scope.edt.temp_dd_payment_type == undefined || $scope.edt.temp_dd_payment_type == "" || $scope.edt == undefined) {
                    pm_type = '';
                }
                else {
                    pm = $scope.edt.temp_dd_payment_type;
                    pm_type = pm_type + ',' + pm;
                }
                $scope.edt.temp_dd_payment_type = pm_type;


                $http.get(ENV.apiUrl + "api/FeeReceipt/gettransactiondetail_IIS_FAILURE?frmdate=" + $scope.edt.from_date + "&todate=" + $scope.edt.to_date + "&auth_code=" + $scope.edt.temp_dd_fee_auth_code + "&enroll=" + $scope.edt.enroll + "&amt=" + $scope.edt.temp_dd_fee_amount + "&temp_dd_payment_type=" + $scope.edt.temp_dd_payment_type + "&temp_dd_type=" + $scope.edt.temp_dd_type).then(function (FeeReceipt_Data) {
                    $scope.FeeReceiptData = FeeReceipt_Data.data;
                    $scope.totalItems = $scope.FeeReceiptData.length;
                    $scope.todos = $scope.FeeReceiptData;
                    $scope.makeTodos();
                    console.log($scope.FeeReceiptData);
                    if (FeeReceipt_Data.data.length > 0) { }
                    else {
                        $scope.ImageView = true;
                        $scope.pager = false;
                    }
                });
            }
            $scope.approve_Data = function (str) {
                debugger
                var data_lst = [];
                if ($scope.temp['sims_realizedate'] == undefined)
                    $scope.temp['sims_realizedate'] = '';
                if ($scope.temp['sims_realizedate'] == '') {
                    swal({ title: "Alert", text: "Please Select Realize Date.", width: 380, height: 200 });

                }
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i]['approve_status']) {
                        var ins_obj = {
                            temp_dd_doc_no: $scope.filteredTodos[i].temp_dd_doc_no,
                            temp_dd_realize_date: $scope.temp.sims_realizedate,
                            temp_dd_creation_user: $rootScope.globals.currentUser.username
                        }

                        data_lst.push(ins_obj);
                    }
                }
                if (data_lst.length > 0) {
                    $http.post(ENV.apiUrl + "api/common/UserApplicationsController/CUD_fee_doc", data_lst).then(function (msg) {

                        if (msg.data) {
                            swal({ text: "Receipt Created..", imageUrl: "assets/img/check.png", width: 380, height: 200 });
                        }
                        else {
                            swal({ text: "Receipt Not Created..", imageUrl: "assets/img/close.png", width: 380, height: 200 });


                        }
                        pm_type = [];

                        //if ($scope.edt.temp_dd_payment_type.length == 0 || $scope.edt == undefined) {
                        if ($scope.edt.temp_dd_payment_type == undefined || $scope.edt.temp_dd_payment_type == "" || $scope.edt == undefined) {
                            pm_type = '';
                        }
                        else {
                            pm = $scope.edt.temp_dd_payment_type;
                            pm_type = pm_type + ',' + pm;
                        }
                        $scope.edt.temp_dd_payment_type = pm_type;


                        $http.get(ENV.apiUrl + "api/FeeReceipt/gettransactiondetail?frmdate=" + $scope.edt.from_date + "&todate=" + $scope.edt.to_date + "&auth_code=" + $scope.edt.temp_dd_fee_auth_code + "&enroll=" + $scope.edt.enroll + "&amt=" + $scope.edt.temp_dd_fee_amount + "&temp_dd_payment_type=" + $scope.edt.temp_dd_payment_type + "&temp_dd_type=" + $scope.edt.temp_dd_type).then(function (FeeReceipt_Data) {
                            $scope.FeeReceiptData = FeeReceipt_Data.data;
                            $scope.totalItems = $scope.FeeReceiptData.length;
                            $scope.todos = $scope.FeeReceiptData;
                            $scope.makeTodos();
                            console.log($scope.FeeReceiptData);
                            if (FeeReceipt_Data.data.length > 0) { }
                            else {
                                $scope.ImageView = true;
                                $scope.pager = false;
                            }
                        });


                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select at least one record.", width: 380, height: 200 });
                }
            }
            var data = [];
            
            $scope.Show = function () {

                debugger
                $scope.failure_payment = true;
                $scope.failure = false;
                $scope.table1 = true;
                $scope.pagggg = true;
                $scope.ImageView = false;
                pm_type = [];
              
                //if ($scope.edt.temp_dd_payment_type.length == 0 || $scope.edt == undefined) {
                if ($scope.edt.temp_dd_payment_type == undefined || $scope.edt.temp_dd_payment_type == "" || $scope.edt == undefined) {
                    pm_type = '';
                }
                else {
                    pm = $scope.edt.temp_dd_payment_type;
                    pm_type = pm_type + ',' + pm;
                }
                $scope.edt.temp_dd_payment_type = pm_type;


                $http.get(ENV.apiUrl + "api/FeeReceipt/gettransactiondetail?frmdate=" + $scope.edt.from_date + "&todate=" + $scope.edt.to_date + "&auth_code=" + $scope.edt.temp_dd_fee_auth_code + "&enroll=" + $scope.edt.enroll + "&amt=" + $scope.edt.temp_dd_fee_amount + "&temp_dd_payment_type=" + $scope.edt.temp_dd_payment_type + "&temp_dd_type=" + $scope.edt.temp_dd_type).then(function (FeeReceipt_Data) {
                    $scope.FeeReceiptData = FeeReceipt_Data.data;
                    $scope.totalItems = $scope.FeeReceiptData.length;
                    $scope.todos = $scope.FeeReceiptData;
                    $scope.makeTodos();
                    console.log($scope.FeeReceiptData);
                    if (FeeReceipt_Data.data.length > 0) { }
                    else {
                        $scope.ImageView = true;
                        $scope.pager = false;
                    }
                });
                //}
            }

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#from_date").attr("disabled", "disabled");
                        $scope.edt["from_date"] = '';
                    } else {

                        $("#from_date").removeAttr("disabled");
                        $("#from_date").focus();
                        //$scope.edt = {
                        //    from_date: yyyy + '-' + mm + '-' + dd,
                        //    to_date: yyyy + '-' + mm + '-' + dd,
                        //}
                        $scope.edt = {
                            from_date: dd + '-' + mm + '-' + yyyy,
                            to_date: dd + '-' + mm + '-' + yyyy,
                        }

                    }
                });
            });


            $scope.Report = function (str) {
                $http.get(ENV.apiUrl + "api/StudentFee/GetReceipt").then(function (res) {
                    var rname = res.data;

                    var data = {
                        //location: 'Sims.SIMR51FeeRec',
                        location: rname,
                        parameter: {
                            fee_rec_no: str.doc_no,
                        },
                        state: 'main.Sim615',
                        ready: function () {
                            this.refreshReport();
                        },
                    }
                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                    $state.go('main.ReportCardParameter')
                });
            }

        }]
        )
})();