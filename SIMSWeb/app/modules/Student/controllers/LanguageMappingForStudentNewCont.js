﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LanguageMappingForStudentNewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.pagesize = "All";
            $scope.pageindex = 0;
            $scope.display = false;
            $scope.table1 = true;
            $scope.fee_no = true;
            $scope.table = true;
            $scope.visit_no = true;
            var check_no = null;
            $scope.busy = false;
            $scope.chku = false;
            $scope.edt = [];
            $scope.div_lang = false;
            $scope.temp = {};

            //CUR
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.temp['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.getAccYear($scope.temp.sims_cur_code)
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp['sims_academic_year']= $scope.Acc_year[0].sims_academic_year;
                  
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                });

            }

            //Grade
            $scope.getGrade = function (curCode, accYear)
            {
                $http.get(ENV.apiUrl + "api/common/AdmissionSubject/Get_langDetails?cur_code=" + $scope.temp.sims_cur_code + "&acad_yr=" + $scope.temp.sims_academic_year).then(function (langcode) {
                    $scope.lang_data = langcode.data;
                });

                $http.get(ENV.apiUrl + "api/common/AdmissionSubject/getAllGrades?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&user_name=" + $rootScope.globals.currentUser.username).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
                $scope.getSection($scope.temp.sims_cur_code, $scope.temp.sims_grade_code, $scope.temp.sims_academic_year)
            }

            //Section
            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/AdmissionSubject/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear + "&user_name=" + $rootScope.globals.currentUser.username).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }

            //Academic year
            //$http.get(ENV.apiUrl + "api/StudentReport/getAcademicYearActive").then(function (AllacademicActive) {
            //    $scope.getAllactive = AllacademicActive.data;
            //    if (AllacademicActive.data.length > 0) {
            //        $scope.edt = {
            //            'sims_academic_years': $scope.getAllactive[0].sims_academic_years
            //        }
            //    }
            //});

            var subjectValues = [];
            $scope.getAllTerm_details = [];

            $scope.secondlang_detail = [];

            $scope.getlangdata = function ()
            {
                $scope.div_lang = false;

                $http.get(ENV.apiUrl + "api/common/AdmissionSubject/Get_subjectMappingN?cur_code=" + $scope.temp.sims_cur_code + "&acad_yr=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&lang_code=" + $scope.temp.sims_language_code).then(function (AllTermDetails) {
                            $scope.getAllTerm_details = AllTermDetails.data;
                            console.log($scope.getAllTerm_details);
                            $scope.totalItems = $scope.getAllTerm_details.length;
                            $scope.todos = $scope.getAllTerm_details;
                            $scope.makeTodos();
                            $scope.size($scope.pagesize);

                        //    $http.get(ENV.apiUrl + "api/common/AdmissionSubject/Get_secondlangDetails?cur_code=" + $scope.temp.sims_cur_code + "&acad_yr=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&lang_code=" + $scope.temp.sims_language_code).then(function (secondlang) {
                        //        $scope.secondlang_detail = secondlang.data;
                        //        if ($scope.secondlang_detail.length > 0)
                        //        {
                        //            for (var i = 0; i < $scope.getAllTerm_details.length; i++) {
                        //                var dd = '#cmb_Subject_code' + i;
                        //                try {
                        //                    $(dd).multipleSelect({ width: '100%', placeholder: "Select Language" });
                        //                }
                        //                catch (ex) {
                        //                }
                        //            }
                        //        }

                        //});
                   
                   
                });
            }

            $scope.Submit = function ()
            {
                $scope.promotegrade = true;
                $scope.newpromotegrade = false;
               
                $http.get(ENV.apiUrl + "api/common/AdmissionSubject/Get_subjectMappingN?cur_code=" + $scope.temp.sims_cur_code + "&acad_yr=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&lang_code=" + $scope.temp.sims_language_code).then(function (AllTermDetails) {
                    $scope.getAllTerm_details = AllTermDetails.data;
                    console.log($scope.getAllTerm_details);
                    $scope.totalItems = $scope.getAllTerm_details.length;
                    $scope.todos = $scope.getAllTerm_details;
                    $scope.makeTodos();
                    $scope.size($scope.pagesize);
                    $scope.div_lang = true;
                    //$http.get(ENV.apiUrl + "api/common/AdmissionSubject/Get_secondlangDetails?cur_code=" + $scope.temp.sims_cur_code + "&acad_yr=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&lang_code=" + $scope.temp.sims_language_code).then(function (secondlang) {
                    //    $scope.secondlang_detail = secondlang.data;

                    //    if ($scope.secondlang_detail.length > 0)
                    //    {
                    //        $scope.div_lang = true;
                    //        for (var i = 0; i < $scope.getAllTerm_details.length; i++) {

                    //            // for (var j = 0; j < $scope.secondlang_detail.length; j++)
                    //            //{
                    //            var dd = '#cmb_Subject_code' + i;
                    //            try {
                    //                $(dd).multipleSelect({ width: '100%', placeholder: "Select Language" });

                    //            }
                    //            catch (ex) {
                    //            }

                    //            try {
                    //                // setTimeout(function () {
                    //                $(dd).multipleSelect("setSelects", $scope.getAllTerm_details[i]['subjectDet1']);
                    //                // console.log($scope.getAllTerm_details[i]['subjectDet1']);
                    //                //  }, 1000);
                    //            }
                    //            catch (ex1) {
                    //            }
                    //            //}
                    //        }
                    //    }
                    //    else
                    //    {
                    //        swal({ title: "Alert", text: "Please define Elective subjects to map to students.", width: 380, height: 200 });
                    //        return;
                    //    }
                    //});
                   
                    
                    //$http.get(ENV.apiUrl + "api/common/AdmissionSubject/Get_thirdlangDetails?cur_code=" + $scope.temp.sims_cur_code + "&acad_yr=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code).then(function (thirdlang) {
                    //    $scope.thirdlang_detail = thirdlang.data;
                    //});
                    //$scope.getCombination=function(year)
                });
            }

            $scope.size = function (str) {

                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.getAllTerm_details;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                $scope.copy_filter_todo = angular.copy($scope.filteredTodos);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.getAllTerm_details, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.getAllTerm_details;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.std_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;

            }

            $scope.New = function () {
                $scope.temp = '';
                $scope.edt = '';
                $scope.display = true;
                $scope.table = false;
                $scope.table1 = false;
                $scope.instaminimuma = true;
                $scope.update1 = false;
                $scope.Update_btn = false;
                $scope.show_btn = true;
            }

            $scope.savedatapromote = function () {
                //var deleteintcode = [];
                debugger
                var Savedata = [];
                $scope.insert = false;
                // $scope.filteredTodos = [];

                for (var i = 0; i < $scope.filteredTodos.length; i++)
                {
                    
                    var subj_lst = '';
                    for (var j = 0; j < $scope.filteredTodos[i].sublist.length; j++) {
                        //if ($scope.filteredTodos[i].sublist[j].sims_status == true)
                        //{
                        //    subj_lst = subj_lst + ' ' + $scope.filteredTodos[i].sublist[j].sims_subject_code + ','

                        //}

                        $scope.busy = true;
                        $scope.insert = true;

                       // if (subj_lst != '' && $scope.filteredTodos[i].sublist.length == (j + 1)) {
                            var deleteintcode = ({
                                'sims_cur_code': $scope.temp.sims_cur_code,
                                'sims_academic_year': $scope.temp.sims_academic_year,
                                'sims_sims_grades': $scope.temp.sims_grade_code,
                                'sims_sims_section': $scope.temp.sims_section_code,
                                'sims_enrollment_number': $scope.filteredTodos[i].enroll_number,
                                //'sims_academic_year_old': $scope.temp.sims_academic_years,
                                'sims_student_attribute1': $scope.filteredTodos[i].sublist[j].sims_subject_code,
                                'sims_student_attribute2': $scope.filteredTodos[i].sublist[j].sims_status ? 'Y':'N',
                                // 'sims_student_attribute3': subjectCode1,
                                'sims_language_code': $scope.temp.sims_language_code,
                                opr: 'J',
                            });


                            Savedata.push(deleteintcode);
                        }

                 //   }



                }
                if ($scope.insert) 
                {
                    $http.post(ENV.apiUrl + "api/common/AdmissionSubject/CUDsubjectNew", Savedata).then(function (msg) {

                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Records Mapped Successfully.", imageUrl: "assets/img/check.png", width: 380, height: 200 });
                            $scope.busy = false;
                            deleteintcode = [];
                            Savedata = [];
                            $scope.Submit();
                        }
                        else {
                            swal({
                                text: "No content changes found, Please map at least one record.",
                                imageUrl: "assets/img/notification-alert.png", width: 380, height: 200
                            });
                            $scope.busy = false;
                        }

                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select at least One Record.", width: 380, height: 200 });
                    return;
                }
            }

            $scope.getLanguageCount = function (lang_cnt)
            {
               // console.log(lang_cnt);

                for (var i = 0; i < $scope.lang_data.length; i++)
                {

                    if ($scope.lang_data[i].sims_language_code === lang_cnt)
                    {
                        $scope.subleng = $scope.lang_data[i].sims_compulsory_subject;
                        //console.log($scope.subleng);
                        break;
                    }
                }
                $scope.getlangdata();
            }

            $scope.getsubjectcount = function (subject_code,old_subject,index,str)
            {
                var oldsub = '';
                 oldsub = $scope.copy_filter_todo[index].sims_subject_code;

                 $scope.sub = [];
                

                if (subject_code.length > $scope.subleng)
                {
                    
                    $scope.sub = oldsub.split(',');
                   
                    var dd1;
                    dd1 = '#cmb_Subject_code' + index;
                  
                    swal({ title: "Alert", text: "Please select max " + $scope.subleng + " compulsory subject.", width: 380, height: 200 });
                    $(dd1).multipleSelect("setSelects", $scope.sub);
                }
            };

            $scope.CheckAllChecked = function () {
                debugger
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("enroll_number-" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("enroll_number-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function (str) {
                debugger
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                        //  str.status = true;
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                        //str.status = false;
                    }
                });
                str.status = str.datastatus;
            }

            $scope.Check_Single_Subject = function (ob, mob) {
                debugger
                var cnt = 0;
                for (var i = 0; i < mob.length; i++) {
                    if (mob[i].sims_status) {
                        cnt = cnt + 1;
                    }

                }
                if (cnt > parseFloat(ob.sims_compulsory_subject)) {
                    swal({ title: "Alert", text: "Please select max " + ob.sims_compulsory_subject + " compulsory subject.", width: 380, height: 200 });
                    ob.sims_status = false;
                }

            }

            $scope.Cancel = function () {
                // $scope.edt = '';
                // $scope.temp = '';
                $scope.temp.sims_grade_code = '';
                $scope.temp.sims_section_code = '';
                $scope.getAllTerm_details = [];
                $scope.filteredTodos = [];
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

         }])
})();
