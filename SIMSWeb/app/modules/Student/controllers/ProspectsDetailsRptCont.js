﻿

(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('ProspectsDetailsRptCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.temp = {};
            $scope.detailsField = true;
            $scope.hideprintpdf = false;

            $scope.pagesize = '30';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.mom_end_date_check = true;
            $scope.mom_start_date_check = true;
            $scope.temp = {};
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.mom_start_date = '';
            $scope.mom_end_date = '';

            $(function () {
                $('#bell_box').multipleSelect({ width: '100%' });
                $('#teacher_box').multipleSelect({ width: '100%', filter: true });

            });

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_start_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });
            if ($scope.mom_start_date_check == true) {
                $scope.mom_start_date = '';
            }
            else {
                $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
            }

            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_end_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });
            if ($scope.mom_end_date_check == true) {
                $scope.mom_end_date = '';
            }
            else {
                $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
            }
            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {

                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['chk_all'] = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 30, $scope.maxSize = 100;

            $scope.makeTodos = function () {
                debugger;
                if ($scope.pagesize == 'All') {
                    $scope.currentPage = 1;
                    $scope.showPager = false;
                    $scope.filteredTodos = $scope.report_data;
                }
                else {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                }
            };


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res1) {
                $scope.cur_data = res1.data;
                $scope.temp.sims_cur_code = $scope.cur_data[0].sims_cur_code;
                $scope.getAcademic_year($scope.cur_data[0].sims_cur_code);
            });

            $scope.getAcademic_year = function (cur_code1) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code1).then(function (res1) {
                    $scope.acad_data = res1.data;
                    $scope.temp.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);

                });
            }


            $scope.getGrade = function (cur_code, acad_year) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }



            $scope.parameter = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/classlist/getastatus").then(function (res1) {
                    $scope.stat_for = res1.data;
                    $scope.temp.comn_appl_parameter = $scope.stat_for[0].comn_appl_parameter;
                    setTimeout(function () {
                        $('#pros').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }






            //$scope.getdetails = function () {
            //    debugger;
            //    $http.get(ENV.apiUrl + "api/classlist/getlistbyteacher?cur_code=" + $scope.temp.sims_cur_code + "&acad_year=" + $scope.temp.sims_academic_year + "&grade_code =" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code).then(function (res1) {
            //        $scope.report_data = res1.data;


            //    });
            //}

            $scope.getdetails = function () {
                debugger;
                $scope.colsvis = false;
                $http.get(ENV.apiUrl + "api/classlist/getprospects?cur_code=" + $scope.temp.sims_cur_code + "&acad_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code +
                      "&st_date=" + $scope.mom_start_date + "&end_date=" + $scope.mom_end_date + "&pros_status=" + $scope.temp.comn_appl_parameter).then(function (res1) {
                          if (res1.data.length > 0) {
                              $scope.report_data = res1.data;
                              $scope.totalItems = $scope.report_data.length;
                              $scope.todos = $scope.report_data;
                              $scope.makeTodos();

                          }
                          else {
                              swal({ title: "Alert", text: "Data is not Available", showCloseButton: true, width: 300, height: 200 });
                              $scope.report_data = [];

                          }



                      });
            }



            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };
            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $scope.colsvis = false;


            $scope.exportData = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('rpt_data1').innerHTML],
                         {
                             type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                         });
                        $scope.detailsField = false;
                        saveAs(blob, "OnlineAdmissionStdListCont.xls");
                        $scope.colsvis = false;
                        $scope.getsubdetail();

                    }
                    $scope.getdetails();
                });

            };


            $scope.print = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var docHead = document.head.outerHTML;
                        var printContents = document.getElementById('rpt_data1').outerHTML;
                        var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                        var newWin = window.open("", "_blank", winAttr);
                        var writeDoc = newWin.document;
                        writeDoc.open();
                        writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                        writeDoc.close();
                        newWin.focus();
                        $scope.detailsField = false;
                        $scope.colsvis = false;
                        $scope.getsubdetail();

                    }

                });
                $scope.getdetails();
            };

            $scope.reset_data = function () {
                debugger;
                $scope.temp.sims_grade_code = '';
                $scope.filteredTodos = [];
                $scope.pager = false;


                try {
                    $('#cmb_grade_code').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_section_code').multipleSelect('uncheckAll');
                } catch (e) {

                }
                $scope.temp.sims_section_code = '';
                $scope.filteredTodos = [];


            }

            $scope.ischeck = function () {
                debugger
                $scope.checklick = function () {
                    var v = document.getElementById('expire');
                    if (v.checked == true) {
                        v.checked == false;
                    }
                }
            }


        }])

})();

