﻿(function () {
    'use strict';
    var formdata = new FormData();
    var imagename = '';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('AcceptanceCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.pagesize = '10';
            $scope.pager = true;
            $scope.pagesize3 = "10";
            $scope.pageindex3 = "1";
            $scope.filteredTodos3 = [], $scope.currentPage3 = 1, $scope.numPerPage3 = 10, $scope.maxSize3 = 10;
            $scope.url_new = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Docs/employee/confirmationLetter/';
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size3 = function (pagesize) {
                $scope.itemsPerPage3 = pagesize;
            }

            $scope.busyindicator = true;
            $scope.temp = {};

            $scope.range3 = function () {
                var rangeSize3 = 10;
                var ret3 = [];
                var start3;
                start3 = $scope.currentPage3;
                if (start3 > $scope.pageCount3() - rangeSize3) {
                    start3 = $scope.pageCount3() - rangeSize3 + 1;
                }

                for (var i = start3; i < start3 + rangeSize3; i++) {
                    if (i >= 0)
                        ret3.push(i);
                }
                return ret3;
            }

            $scope.prevPage3 = function () {
                if ($scope.currentPage3 > 0) {
                    $scope.currentPage3--;
                }
            }

            $scope.prevPageDisabled3 = function () {
                return $scope.currentPage3 === 0 ? "disabled" : "";
            };

            $scope.pageCount3 = function () {
                return Math.ceil($scope.Upload_doc_datails.length / $scope.itemsPerPage3) - 1;
            };

            $scope.nextPage3 = function () {
                if ($scope.currentPage3 < $scope.pageCount3()) {
                    $scope.currentPage3++;
                }
            };

            $scope.nextPageDisabled3 = function () {
                return $scope.currentPage3 === $scope.pageCount3() ? "disabled" : "";
            };

            $scope.setPage3 = function (n) {
                $scope.currentPage3 = n;
            };
            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.StudenteData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.StudenteData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.StudenteData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_student_passport_full_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_student_enroll_number == toSearch) ? true : false;
            }

            //$http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
            //    $scope.curriculum = res.data;
            //    $scope.temp.s_cur_code = $scope.curriculum[0].sims_cur_code;
            //    $scope.getacyr($scope.temp.s_cur_code);
            //})


            $scope.getAssesmentEmployeeDate = function () {
                $http.get(ENV.apiUrl + "api/TeacherAssessmentEntry/GetAssesmentEmployeeDate").then(function (res) {
                    $scope.assesmentDate_data = res.data;
                });
            }
            $scope.getAssesmentEmployeeDate();



   
          

        
            $scope.Reset = function () {
                $scope.edt.sims_grade_code = '';
                $scope.edt.sims_section_code = '';
                $scope.edt.sims_student_enroll_number = '';
                $scope.table1 = false;
                $scope.StudenteData = [];
            }

            $scope.Show_Data = function () {
                debugger;
                $scope.table1 = true;
                $scope.ImageView = false;
                $http.get(ENV.apiUrl + "api/TeacherAssessmentEntry/GetEmployeeAssesmentSchedule?date=" + $scope.edt.sims_assesment_date).then(function (res) {
                    $scope.StudenteData = res.data;
                    $scope.totalItems = $scope.StudenteData.length;
                    $scope.imageUrl = ENV.apiUrl + '/Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/';
                    // $scope.imageUrl = ENV.apiUrl + 'Content/sjs/Images/StudentImages/';
                    $scope.todos = $scope.StudenteData;
                    $scope.makeTodos();
                    if ($scope.StudenteData.length > 0) { }
                    else {
                        $scope.ImageView = true;
                    }
                });
            }


            $scope.makeTodos3 = function () {
                var rem = parseInt($scope.totalItems3 % $scope.numPerPage3);
                if (rem == '0') {
                    $scope.pagersize3 = parseInt($scope.totalItems3 / $scope.numPerPage3);
                }
                else {
                    $scope.pagersize3 = parseInt($scope.totalItems3 / $scope.numPerPage3) + 1;
                }
                var begin3 = (($scope.currentPage3 - 1) * $scope.numPerPage3);
                var end3 = parseInt(begin3) + parseInt($scope.numPerPage3);
                $scope.filteredTodos3 = $scope.todos3.slice(begin3, end3);
            };

            $scope.size3 = function (str) {
                $scope.pagesize3 = str;
                $scope.currentPage3 = 1;
                $scope.numPerPage3 = str;
                $scope.makeTodos3();
            }

            $scope.index3 = function (str) {
                $scope.pageindex3 = str;
                $scope.currentPage3 = str;
                $scope.makeTodos3();
            }

            $scope.searched3 = function (valLists, toSearch) {

                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil3(i, toSearch);
                });
            };

            $scope.search3 = function () {
                $scope.todos3 = $scope.searched3($scope.Upload_doc_datails, $scope.searchText3);
                $scope.totalItems3 = $scope.todos3.length;
                $scope.currentPage3 = '1';
                if ($scope.searchText3 == '') {
                    $scope.todos3 = $scope.Upload_doc_datails;
                }
                $scope.makeTodos3();
            }

            function searchUtil3(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_criteria_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_criteria_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.UploadDocument = function (str) {
                debugger
                $scope.busyindicator = true;

                $scope.grade_section = {
                    sims_grade_name: str.sims_grade_name,
                    sims_section_name: str.sims_section_name,
                    sims_admission_enroll_number: str.sims_student_enroll_number,
                    sims_admission_grade_code: str.sims_grade_code,
                    sims_admission_section_code: str.sims_section_code
                }

                //  $scope.admission_no = adm_no;
                $http.get(ENV.apiUrl + "api/student/UploadRemainingDocument/GetCriteriaName?student_roll_number=" + str.sims_student_enroll_number).then(function (res) {
                    $scope.Upload_doc_datails = res.data;
                    $scope.totalItems3 = $scope.Upload_doc_datails.length;
                    $scope.todos3 = $scope.Upload_doc_datails;
                    for (var i = 0; i < $scope.totalItems3; i++) {
                        $scope.Upload_doc_datails[i] = res.data[i];
                        //  $scope.todos = res.data[i];
                        $scope.visible = [];
                        $scope.visible = res.data[i].sims_admission_doc_path.split(",");
                        $scope.Upload_doc_datails[i].path = $scope.visible;
                    }
                    $scope.makeTodos3();
                    $scope.Upload_doc = true;
                    $scope.edt.sims_admission_doc_path1 = "NO";
                    $scope.edt = {
                        sims_student_passport_full_name_en: str.sims_student_passport_full_name_en,
                        sims_student_enroll_number: str.sims_student_enroll_number
                    }
                    $('#UploadDocModal').modal({ backdrop: 'static', keyboard: true });
                });
            }

            $scope.UploadImageModal = function (str) {

                imagename = str.sims_student_enroll_number;

                $('#myModal').modal('show');
            }

            simsController.directive('ngFiles', ['$parse', function ($parse) {

                function fn_link(scope, element, attrs) {
                    var onChange = $parse(attrs.ngFiles);
                    element.on('change', function (event) {
                        onChange(scope, { $files: event.target.files });
                    });
                };

                return {
                    link: fn_link
                }
            }])


            //upload file Acceptance
            $scope.uploadClick = function (str) {
                debugger;
                $scope.filesize = true;
                $scope.edt1 = str;
                $scope.edt.photoStatus = true;
                $scope.ins = false;
                formdata = new FormData();
            }

            $scope.getTheFiles = function ($files) {
                $scope.filesize = true;
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                    //var i = 0;
                    //if ($files[i].size > 2048) {
                    //    $scope.filesize = false;                        
                    //    swal({ title: "Alert", text: "File Should Not Exceed 2 MB."});
                    //}
                });
            };

            //upload file Acceptance
            $scope.file_changed = function (element, str) {
                debugger;
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;
                    });
                };

                reader.readAsDataURL(photofile);

                $scope.busyindicator = false;
                $scope.object = {};

                //$scope.object.sims_admission_enroll_number = $scope.grade_section.sims_admission_enroll_number;
                //$scope.object.sims_admission_cur_code = $scope.temp.s_cur_code;
                //$scope.object.sims_admission_academic_year = $scope.temp.sims_academic_year;
                //$scope.object.sims_admission_grade_code = $scope.grade_section.sims_admission_grade_code;
                //$scope.object.sims_admission_section_code = $scope.grade_section.sims_admission_section_code;
            

                if (element.files[0].size < 1048576) {

                    var request = {
                        method: 'POST',
                        //url: ENV.apiUrl + 'api/student/UploadRemainingDocument/uploadDocument?filename=' + $scope.edt1.admis_num + '_' + $scope.edt1.sims_criteria_code + '_' + $scope.edt1.count + '(' + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss') + ')' + "&location=" + "Docs/Student",
                        url: ENV.apiUrl + 'api/file/uploadDocument_new?filename=' + $scope.edt1.em_applicant_id + '(' + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss') + ')' + "&location=" + "Docs/employee/confirmationLetter",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    $http(request).success(function (d) {
                        debugger;
                        var data = {
                            em_applicant_id: $scope.edt1.em_applicant_id,
                            em_doc_path:d

                        }
                        debugger;
                        if (data.em_doc_path != null || data.em_doc_path != "") {
                            if ($scope.edt1.em_doc_path == null || $scope.edt1.em_doc_path == "") {
                               
                                data.opr = 'L';

                                $http.post(ENV.apiUrl + "api/TeacherAssessmentEntry/CUD_Update_Employee_Doc", data).then(function (res) {
                                    $scope.ins = res.data;
                                    $scope.busyindicator = true;
                                    if (res.data) {
                                        swal({ text: "Document Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                                        $scope.edt1.count = ($scope.edt1.count) + 1;

                                        $scope.Show_Data();
                                    }

                                    //$scope.UploadDocument($scope.edt1.admis_num);
                                    //$('#UploadDocModal').modal('hide');
                                });
                            }
                            else {

                                $http.post(ENV.apiUrl + "api/common/AdmissionDashboard/CUD_Insert_Admission_Doc", data).then(function (res) {
                                    $scope.ins = res.data;
                                    if (res.data) {
                                        swal({ text: "Document Uploaded Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                                        $scope.edt1.count = ($scope.edt1.count) + 1;

                                        $scope.getUploadDocumnetDetails();
                                    }

                                    //$scope.UploadDocument($scope.edt1.admis_num);
                                    //$('#UploadDocModal').modal('hide');
                                });
                            }
                        }

                    });
                    //}
                    //}
                    //else {
                    //    swal({ title: "Alert", text: "You Can Upload Max 2 File Documents For Each Criteria", showCloseButton: true, width: 380, });
                    //    $scope.busyindicator = true;
                    //}
                }
                else {
                    swal({ title: "Alert", text: "Please upload file less than 2 mb", showCloseButton: true, width: 380, });
                    $scope.busyindicator = true;
                }

            };

            $scope.downloaddoc = function (str) {
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Docs/Student/' + str;
                window.open($scope.url);
            }

            $scope.updatedata = function (ea) {
                debugger;
                ea.isChange = true;
            }

            $scope.add_upload_doc = function () {
                debugger;
                var dataupdate = [];
                var data = {};
                for (var k = 0; k < $scope.Upload_doc_datails.length; k++) {
                    if ($scope.Upload_doc_datails[k].isChange == true) {
                        debugger;
                        data =
                       {
                           sims_admission_doc_status: $scope.Upload_doc_datails[k].sims_admission_doc_status,
                           sims_admission_doc_verify: $scope.Upload_doc_datails[k].sims_admission_doc_verify,
                           admis_num: $scope.Upload_doc_datails[k].admis_num,
                           sims_criteria_code: $scope.Upload_doc_datails[k].sims_criteria_code,
                           sims_admission_doc_path: $scope.Upload_doc_datails[k].sims_admission_doc_path,
                           opr: 'MMM',
                       }
                        dataupdate.push(data);
                    }

                }

                if (dataupdate.length > 0) {
                    $http.post(ENV.apiUrl + "api/student/UploadRemainingDocument/CUD_Update_Admission_DocList", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, showCloseButton: true });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated", imageUrl: "assets/img/close.png", width: 300, showCloseButton: true });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                    })
                    $scope.grid = true;
                    $scope.table = true;
                    $scope.newdisplay = false;
                    dataupdate = [];

                }
                else {
                    swal({ text: "Change Atleast one Record to Update", width: 380, showCloseButton: true });
                }
            }

            //$scope.add_upload_doc = function () {
            //    debugger
            //    var data = $scope.Upload_doc_datails;

            //    if ($scope.Upload_doc_datails != undefined) {
            //        $http.post(ENV.apiUrl + "api/student/UploadRemainingDocument/CUD_Update_Admission_DocList", data).then(function (res) {
            //            $scope.ins = res.data;
            //            if ($scope.ins == true) {
            //                swal({ title: "Alert", text: "Document Updated Successfully", showCloseButton: true, width: 380, });
            //            }

            //        });
            //    }
            //    else {

            //        swal({ title: "Alert", text: "Document not Updated", showCloseButton: true, width: 380, });
            //    }


            //}


            //$scope.add_upload_doc = function () {
            //    debugger
            //    var data = $scope.Upload_doc_datails;

            //    if ($scope.Upload_doc_datails != undefined) {
            //        $http.post(ENV.apiUrl + "api/student/UploadRemainingDocument/CUD_Update_Admission_DocList", data).then(function (res) {
            //            $scope.ins = res.data;
            //            if ($scope.ins==true) {
            //                swal({ title: "Alert", text: "Document Updated Successfully", showCloseButton: true, width: 380, });
            //            }

            //        });
            //    }
            //    else {

            //                swal({ title: "Alert", text: "Document not Updated", showCloseButton: true, width: 380, });
            //            }


            //    }

            $scope.getUploadDocumnetDetails = function () {

                $http.get(ENV.apiUrl + "api/student/UploadRemainingDocument/GetCriteriaName?student_roll_number=" + $scope.grade_section.sims_admission_enroll_number).then(function (res) {
                    $scope.Upload_doc_datails = res.data;
                    $scope.totalItems3 = $scope.Upload_doc_datails.length;
                    $scope.todos3 = $scope.Upload_doc_datails;
                    for (var i = 0; i < $scope.totalItems3; i++) {
                        $scope.Upload_doc_datails[i] = res.data[i];
                        $scope.visible = [];
                        $scope.visible = res.data[i].sims_admission_doc_path.split(",");
                        $scope.Upload_doc_datails[i].path = $scope.visible;
                    }
                    $scope.makeTodos3();
                    $scope.Upload_doc = true;
                    $scope.edt.sims_admission_doc_path1 = "NO";
                    $scope.edt = {
                        //sims_student_passport_full_name_en: str.sims_student_passport_full_name_en,
                        sims_student_enroll_number: $scope.grade_section.sims_admission_enroll_number
                    }
                });
            }

            $scope.doc_delete = function (doc_path, em_app_id) {
                debugger
                swal({
                    title: '',
                    text: "Are you sure you want to Delete?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    debugger
                    if (isConfirm) {
                        $http.post(ENV.apiUrl + "api/TeacherAssessmentEntry/CUD_Delete_Employee_Doc?em_app_id=" + em_app_id + "&doc_path=" + doc_path).then(function (res) {
                                debugger;
                                $scope.del = res.data;
                                if ($scope.del.strMessage != undefined || $scope.del.strMessage != "" || $scope.del.strMessage != null) {
                                    swal({ text: "Document Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                                    //$('#UploadDocModal').modal('hide');
                                    $scope.Show_Data();
                                }
                                //$scope.UploadDocument(adm_no);
                            });
                    }
                });
            }

            //$scope.Upload = function () {
            //    debugger
            //    var data = [];
            //    var t = $scope.photo_filename.split("/")[1];
            //    console.log(t);
            //    data = {
            //        sims_student_enroll_number: imagename,
            //        sims_student_image: imagename + "." + t,
            //        opr: "W"
            //    }
            //    console.log(data);
            //    debugger
            //    $http.post(ENV.apiUrl + "api/student/UploadRemainingDocument/UpdateStudentDocumentCUD", data).then(function (res) {
            //        $scope.result = res.data;
            //        $('#myModal').modal('hide');

            //        var request = {
            //            method: 'POST',
            //            url: ENV.apiUrl + '/api/file/upload?filename=' + data.sims_student_enroll_number + "&location=" + "StudentImages",
            //            data: formdata,
            //            headers: {
            //                'Content-Type': undefined
            //            }
            //        };
            //        $http(request)
            //      .success(function (d) {
            //      }, function () {
            //          alert("Err");
            //      });
            //        if ($scope.result == true) {
            //            swal({ title: "Alert", text: "Document Upload Successfully", width: 300, height: 200 });
            //            $scope.terminal = document.getElementById("file1");
            //            $scope.terminal = '';
            //        }
            //    });
            //}

        }])
})();
