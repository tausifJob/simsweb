﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AdmissionDmcOLCont',
        ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {
           

            $scope.admgrid = false;
            $scope.btnadm = false;
            $scope.btn_Save = false;
            $scope.btn_NewSave = false;
            $scope.btncancel = false;
            $scope.btnok = false;
            $scope.prev = true;
            $scope.details = false;
            $scope.div_view = false;
            $scope.disableparent = true;
            $scope.lbl_show = false;
            $scope.edt1 = [];
            $scope.obj_age = [];
            $scope.div_year = true;
            $scope.div_subject = false;
            $scope.div_fyshow = false;
            $scope.div_syshow = false;
            $scope.scholnm = [], $scope.cur_data = [], $scope.acad_yr = [], $scope.city_lst = [], $scope.caste_lst = [], $scope.maritalstatus_lst = [];
            var adm_date = "", grade_name = "";


           
            $scope.div_edt = true;
            $scope.div_save = false;
            $scope.admgrid = true;
            $scope.btn_Save = true;
            $scope.btn_NewSave = false;
            $scope.btn_back = true;

            $scope.scholnm = [], $scope.cur_data = [], $scope.acad_yr = [], $scope.city_lst = [], $scope.caste_lst = [], $scope.maritalstatus_lst = [], $scope.feecat = [];
            var param = $stateParams.admission_num;
            var adm_no = $stateParams.admission_num;

            //var school_code = $http.defaults.headers.common['schoolId'];


            $scope.apiurl = 'https://oa.mograsys.com/' + $http.defaults.headers.common['schoolId'] + '/';

            $scope.label_hsc = 'H.S.S.C.E';

           

            $scope.$on('application-chage', function () {
                $scope.scholnm = [], $scope.cur_data = [], $scope.acad_yr = [], $scope.city_lst = [], $scope.caste_lst = [], $scope.maritalstatus_lst = [], $scope.feecat = [];

                $scope.edt = {}
                $http.get($scope.apiurl + "api/Comn/getAdmissionList").then(function (res) {
                    $scope.obj = res.data;

                    for (var i = 0; i < res.data.length; i++) {

                        //if (res.data[i].fee_category_desc != '') {
                        //    $scope.feecat.push({ fee_category_code: res.data[i].fee_category_code, fee_category_desc: res.data[i].fee_category_desc });
                        //    $scope.edt['fee_category_code'] = $scope.feecat[0].fee_category_code;
                        //}

                        if (res.data[i].school_name != '') {
                            $scope.scholnm.push({ school_name: res.data[i].school_name, school_code: res.data[i].school_code });
                            $scope.edt['school_code'] = $scope.scholnm[0].school_code;
                        }
                        if (res.data[i].curr_name != '') {
                            $scope.cur_data.push({ curr_name: res.data[i].curr_name, curr_code: res.data[i].curr_code });
                            $scope.edt['curr_code'] = $scope.cur_data[0].curr_code;
                        }
                        if (res.data[i].academic_year_desc != '') {
                            $scope.acad_yr.push({ academic_year_desc: res.data[i].academic_year_desc, academic_year: res.data[i].academic_year });
                            $scope.edt['academic_year'] = $scope.acad_yr[0].academic_year;
                        }

                        if (res.data[i].sims_city_code != '') {
                            $scope.city_lst.push({ sims_city_code: res.data[i].sims_city_code, sims_city_state_code: res.data[i].sims_city_state_code, sims_city_name_en: res.data[i].sims_city_name_en });
                            // $scope.edt['academic_year'] = $scope.acad_yr[0].academic_year;
                        }
                        if (res.data[i].sims_appl_parameter_castCatcode != '') {
                            $scope.caste_lst.push({ sims_appl_parameter_castCatcode: res.data[i].sims_appl_parameter_castCatcode, sims_appl_form_field_value1_castcatdesc: res.data[i].sims_appl_form_field_value1_castcatdesc });
                            if ($scope.obj[i].sims_appl_parameter_castCatcode == 'GE') {
                                $scope.edt['sims_student_category'] = $scope.obj[i].sims_appl_parameter_castCatcode;
                            }
                        }
                        if (res.data[i].sims_appl_parameter_marital_statuscode != '') {
                            $scope.maritalstatus_lst.push({ sims_appl_parameter_marital_statuscode: res.data[i].sims_appl_parameter_marital_statuscode, sims_appl_form_field_value1_marital_statusdesc: res.data[i].sims_appl_form_field_value1_marital_statusdesc });
                            if ($scope.obj[i].sims_appl_parameter_marital_statuscode == 'U') {
                                $scope.edt['sims_admission_marital_status'] = $scope.obj[i].sims_appl_parameter_marital_statuscode;
                            }
                        }


                    }

                    $http.get($scope.apiurl + "api/common/Admission/GetTabStudentData?admission_number=" + $scope.admission_num_new).then(function (res_new) {
                        debugger;
                        $scope.edt = res_new.data;

                        var d1 = new Date();
                        var month = d1.getMonth() + 1;
                        var day = d1.getDate();
                        if (month < 10)
                            month = "0" + month;
                        if (day < 10)
                            day = "0" + day;
                        var d = d1.getFullYear() + "-" + (month) + "-" + (day);

                        $scope.edt['tent_join_date'] = d;


                        $scope.fy_flg = false;
                        $scope.sy_flg = false;
                        $scope.flg_stream = false;

                        if ($http.defaults.headers.common['schoolId'] == 'facp') {
                            if ($scope.edt['grade_code'] == '05' || $scope.edt['grade_code'] == '08' || $scope.edt['grade_code'] == '11') {
                                $scope.fy_flg = true;
                            }

                            if ($scope.edt['grade_code'] == '06' || $scope.edt['grade_code'] == '09' || $scope.edt['grade_code'] == '12') {
                                $scope.fy_flg = true;
                                $scope.sy_flg = true;
                            }
                        }
                        if ($http.defaults.headers.common['schoolId'] == 'dmc') {

                            if ($scope.edt['grade_code'] == '28' || $scope.edt['grade_code'] == '29' || $scope.edt['grade_code'] == '30' || $scope.edt['grade_code'] == '31') {
                                $scope.label_hsc = 'Graduation';
                                $scope.flg_stream = true;
                            }

                            if ($scope.edt['grade_code'] == '14' || $scope.edt['grade_code'] == '17' || $scope.edt['grade_code'] == '20' || $scope.edt['grade_code'] == '23' || $scope.edt['grade_code'] == '26' || $scope.edt['grade_code'] == '29' || $scope.edt['grade_code'] == '31') {
                                $scope.fy_flg = true;
                            }

                            if ($scope.edt['grade_code'] == '15' || $scope.edt['grade_code'] == '18' || $scope.edt['grade_code'] == '21' || $scope.edt['grade_code'] == '24' || $scope.edt['grade_code'] == '27') {
                                $scope.fy_flg = true;
                                $scope.sy_flg = true;
                            }

                        }
                        if ($http.defaults.headers.common['schoolId'] == 'cesc') {

                            if ($scope.edt['grade_code'] == '12' || $scope.edt['grade_code'] == '15') {
                                $scope.fy_flg = true;
                            }

                            if ($scope.edt['grade_code'] == '13' || $scope.edt['grade_code'] == '16') {
                                $scope.fy_flg = true;
                                $scope.sy_flg = true;
                            }
                        }

                          //  setTimeout(function () {
                        $http.get($scope.apiurl + "api/common/Admission/GetStudent_PrevExamDetails?admission_number=" + $scope.admission_num_new).then(function (resss) {
                            debugger
                            $scope.edt_exam_new = resss.data;
                            console.log($scope.edt_exam_new);
                            $scope.edt['current_school_name'] = $scope.edt_exam_new['current_school_name'];
                            $scope.edt['sims_admission_boardname'] = $scope.edt_exam_new['sims_admission_boardname'];
                            $scope.edt['sims_admission_center_name'] = $scope.edt_exam_new['sims_admission_center_name'];
                            $scope.edt['sims_admission_passingyear_exam'] = $scope.edt_exam_new['sims_admission_passingyear_exam'];
                            $scope.edt['sims_admission_seat_no'] = $scope.edt_exam_new['sims_admission_seat_no'];
                            $scope.edt['sims_admission_no_of_attempts'] = $scope.edt_exam_new['sims_admission_no_of_attempts'];
                            $scope.edt['sims_admission_marks_obtained'] = $scope.edt_exam_new['sims_admission_marks_obtained'];
                            $scope.edt['sims_admission_marks_out_of'] = $scope.edt_exam_new['sims_admission_marks_out_of'];
                            $scope.edt['sims_admission_percentage_obtained'] = $scope.edt_exam_new['sims_admission_percentage_obtained'];

                            $scope.edt['current_fyschool_name'] = $scope.edt_exam_new['current_fyschool_name'];
                            $scope.edt['sims_admission_fyboardname'] = $scope.edt_exam_new['sims_admission_fyboardname'];
                            $scope.edt['sims_admission_fycenter_name'] = $scope.edt_exam_new['sims_admission_fycenter_name'];
                            $scope.edt['sims_admission_fypassingyear_exam'] = $scope.edt_exam_new['sims_admission_fypassingyear_exam'];
                            $scope.edt['sims_admission_fyseat_no'] = $scope.edt_exam_new['sims_admission_fyseat_no'];
                            $scope.edt['sims_fyadmission_no_of_attempts_sem1'] = $scope.edt_exam_new['sims_fyadmission_no_of_attempts_sem1'];
                            $scope.edt['sims_fyadmission_marks_obtained_sem1'] = $scope.edt_exam_new['sims_fyadmission_marks_obtained_sem1'];
                            $scope.edt['sims_fyadmission_marks_out_of_Sem1'] = $scope.edt_exam_new['sims_fyadmission_marks_out_of_Sem1'];
                            $scope.edt['sims_fyadmission_percentage_obtained_sem1'] = $scope.edt_exam_new['sims_fyadmission_percentage_obtained_sem1'];

                            $scope.edt['sims_fyadmission_no_of_attempts_sem2'] = $scope.edt_exam_new['sims_fyadmission_no_of_attempts_sem2'];
                            $scope.edt['sims_fyadmission_marks_obtained_sem2'] = $scope.edt_exam_new['sims_fyadmission_marks_obtained_sem2'];
                            $scope.edt['sims_fyadmission_marks_out_of_Sem2'] = $scope.edt_exam_new['sims_fyadmission_marks_out_of_Sem2'];
                            $scope.edt['sims_fyadmission_percentage_obtained_sem2'] = $scope.edt_exam_new['sims_fyadmission_percentage_obtained_sem2'];

                            $scope.edt['current_syschool_name'] = $scope.edt_exam_new['current_syschool_name'];
                            $scope.edt['sims_admission_syboardname'] = $scope.edt_exam_new['sims_admission_syboardname'];
                            $scope.edt['sims_admission_sycenter_name'] = $scope.edt_exam_new['sims_admission_sycenter_name'];
                            $scope.edt['sims_admission_sypassingyear_exam'] = $scope.edt_exam_new['sims_admission_sypassingyear_exam'];
                            $scope.edt['sims_admission_syseat_no'] = $scope.edt_exam_new['sims_admission_syseat_no'];
                            $scope.edt['sims_syadmission_no_of_attempts_sem3'] = $scope.edt_exam_new['sims_syadmission_no_of_attempts_sem3'];
                            $scope.edt['sims_syadmission_marks_obtained_sem3'] = $scope.edt_exam_new['sims_syadmission_marks_obtained_sem3'];
                            $scope.edt['sims_syadmission_marks_out_of_Sem3'] = $scope.edt_exam_new['sims_syadmission_marks_out_of_Sem3'];
                            $scope.edt['sims_syadmission_percentage_obtained_sem3'] = $scope.edt_exam_new['sims_syadmission_percentage_obtained_sem3'];

                            $scope.edt['sims_syadmission_no_of_attempts_sem4'] = $scope.edt_exam_new['sims_syadmission_no_of_attempts_sem4'];
                            $scope.edt['sims_syadmission_marks_obtained_sem4'] = $scope.edt_exam_new['sims_syadmission_marks_obtained_sem4'];
                            $scope.edt['sims_syadmission_marks_out_of_sem4'] = $scope.edt_exam_new['sims_syadmission_marks_out_of_sem4'];
                            $scope.edt['sims_syadmission_percentage_obtained_sem4'] = $scope.edt_exam_new['sims_syadmission_percentage_obtained_sem4'];

                            console.log($scope.edt);
                        });
                          //}, 50000);

                        $scope.section_changed();
                        $scope.getchkAge();
                        //student previous exam details

                        console.log($scope.edt);


                    });


                });

                $http.get($scope.apiurl + "api/Comn/GetIncomeSvcc").then(function (res) {
                    $scope.income_lst = res.data
                });
            });


            $scope.section_changed = function () {
                debugger
                $http.get(ENV.apiUrl + "api/common/Admission_dpsd/getCategory?cur=" + $scope.edt['curr_code'] + "&academic=" + $scope.edt['academic_year'] + "&grade=" + $scope.edt['grade_code'] + "&section=" + $scope.edt['section_code']).then(function (res) {
                    $scope.feecat = res.data
                });
            }
            
            $scope.selectdisabledata = true;
            $scope.disabledata = false;
            $scope.btncancel = true;

            function convertdate(dt) {
                if (dt != null) {
                    var d1 = new Date(dt);
                    var month = d1.getMonth() + 1;
                    var day = d1.getDate();
                    if (month < 10)
                        month = "0" + month;
                    if (day < 10)
                        day = "0" + day;
                    var d = d1.getFullYear() + "-" + (month) + "-" + (day);
                    return d;
                }
            }


            $scope.getFatherDetails = function () {
                if ($scope.edt['father_add_status'] == true) {
                    var data = $scope.edt;
                    var v = document.getElementById('txt_mother_aprt');
                    v.value = document.getElementById('txt_father_aprt').value;
                    data.mother_appartment_number = v.value;

                    var v = document.getElementById('txt_mother_build');
                    v.value = document.getElementById('txt_father_build').value;
                    data.mother_building_number = v.value;

                    var v = document.getElementById('txt_mother_street');
                    v.value = document.getElementById('txt_father_Street').value;
                    data.mother_street_number = v.value;

                    var v = document.getElementById('txt_mother_area');
                    v.value = document.getElementById('txt_father_area').value;
                    data.mother_area_number = v.value;

                    var v = document.getElementById('cmb_mother_country');
                    v.value = document.getElementById('cmb_father_country').value;
                    data.mother_country_code = v.value;


                    var v = document.getElementById('txt_mother_state');
                    v.value = document.getElementById('txt_father_state').value;
                    data.mother_state = v.value;



                    //var v = document.getElementById('txt_mother_city');
                    //v.value = $scope.edt.father_city
                    //document.getElementById('txt_father_city').value;
                    data.mother_city = $scope.edt.father_city;

                    var v = document.getElementById('txt_mother_pobox');
                    v.value = document.getElementById('txt_father_pobox').value;
                    data.mother_po_box = v.value;
                }
                else {
                    $scope.edt['mother_appartment_number'] = '';
                    $scope.edt['mother_building_number'] = '';
                    $scope.edt['mother_street_number'] = '';
                    $scope.edt['mother_area_number'] = '';
                    $scope.edt['mother_city'] = '';
                    $scope.edt['mother_state'] = '';
                    $scope.edt['mother_country_code'] = '';
                    $scope.edt['mother_po_box'] = '';
                }
            }




            $scope.getchkAge = function () {
                $scope.edt.sims_student_attribute4 = "";
                $scope.edt.sims_student_attribute11 = "";
                $scope.div_subject = true;

                $http.get($scope.apiurl + "api/Comn/GetSubjectDMC?cur_code=" + $scope.edt.curr_code + "&acad_yr=" + $scope.edt.academic_year + "&grade=" + $scope.edt.grade_code + "&adm_no=" + $scope.admission_num_new).then(function (mmmm) {
                    //$scope.subject_lst1 = mmmm.data;
                    $scope.subject_lst1 = [];

                    console.log(mmmm.data);

                    for (var i = 0; i < mmmm.data.length; i++) {
                        if (mmmm.data[i].sims_compulsory_subject == '') {
                            mmmm.data[i]['sublist'] = [];
                            mmmm.data[i]['selected_count'] = 0;
                            $scope.subject_lst1.push(mmmm.data[i])

                        }

                        else {
                            var flg = false;
                            for (var j = 0; j < $scope.subject_lst1.length; j++) {
                                if ($scope.subject_lst1[j].sims_language_desc == mmmm.data[i].sims_language_desc) {
                                    flg = true;
                                    //$scope.subject_lst[j]['sublist'] = [];
                                    $scope.subject_lst1[j]['sublist'].push(mmmm.data[i])
                                }

                            }
                            if (flg == false) {
                                mmmm.data[i]['selected_count'] = 0;
                                $scope.subject_lst1.push(mmmm.data[i]);
                                $scope.subject_lst1[$scope.subject_lst1.length - 1]['sublist'] = [];
                                $scope.subject_lst1[$scope.subject_lst1.length - 1]['sublist'].push(mmmm.data[i]);
                            }


                        }
                    }
                    console.log($scope.subject_lst1);



                });

                $http.get($scope.apiurl + "api/Comn/GetAgecompare?cur_code=" + $scope.edt.curr_code + "&acad_yr=" + $scope.edt.academic_year + "&grade=" + $scope.edt.grade_code).then(function (res) {
                    $scope.obj_age = res.data;
                    // console.log($scope.obj_age);
                });

                if ($scope.edt.grade_code == '13' || $scope.edt.grade_code == '14') {
                    $scope.div_fyshow = false;
                    $scope.div_syshow = false;
                }
                if ($scope.edt.grade_code == '15' || $scope.edt.grade_code == '16') {
                    $scope.div_fyshow = true;
                    $scope.div_syshow = false;
                }



                //if ($scope.edt.grade_code == '13')
                //{
                //    $scope.div_subject = true;
                //    $http.get($scope.apiurl + "api/common/Admission/GetStreamPrefferd").then(function (res) {
                //        $scope.stream_data = res.data;
                //        // console.log($scope.stream_data);
                //    });
                //}
            }


            $scope.getchkDOB = function () {

                for (var i = 0; i < $scope.obj_age.length; i++) {
                    if ($scope.edt.grade_code == '01') {
                        if ($scope.edt.birth_date <= $scope.obj_age[i].sims_birth_date_from) {
                            swal({ title: "Alert", text: "For KG 1 admissions, child must be minimum 3.8 year old and can be maximum 4.11 year old as of 2017-03-31.", showCloseButton: true, width: 450, });
                            //swal({ title: "Birth Date", text: "For KG 1 admissions, child must be minimum 3.8 year old and can be maximum 4.11 year old as of 2017-03-31", showCloseButton: true, width: 480, });
                            $scope.edt.birth_date = "";
                        }
                        if ($scope.edt.birth_date >= $scope.obj_age[i].sims_birth_date_to) {
                            swal({ title: "Birth Date", text: "For KG 1 admissions, child must be minimum 3.8 year old and can be maximum 4.11 year old as of 2017-03-31", showCloseButton: true, width: 450, });
                            $scope.edt.birth_date = "";
                        }
                    }
                    if ($scope.edt.grade_code == '02') {
                        if ($scope.edt.birth_date <= $scope.obj_age[i].sims_birth_date_from) {
                            swal({ title: "Birth Date", text: "For KG 2 admissions, child must be minimum 4.8 year old and can be maximum 5.11 year old as of 2017-03-31", showCloseButton: true, width: 450, });
                            $scope.edt.birth_date = "";
                        }
                        if ($scope.edt.birth_date >= $scope.obj_age[i].sims_birth_date_to) {
                            swal({ title: "Birth Date", text: "For KG 2 admissions, child must be minimum 4.8 year old and can be maximum 5.11 year old as of 2017-03-31", showCloseButton: true, width: 450, });
                            $scope.edt.birth_date = "";
                        }
                    }
                    if ($scope.edt.grade_code == '03') {
                        if ($scope.edt.birth_date <= $scope.obj_age[i].sims_birth_date_from) {
                            swal({ title: "Birth Date", text: "For Grade 1 admissions, child must be minimum 5.8 year old and can be maximum 6.11 year old as of 2017-03-31", showCloseButton: true, width: 480, });
                            $scope.edt.birth_date = "";
                        }
                        if ($scope.edt.birth_date >= $scope.obj_age[i].sims_birth_date_to) {
                            swal({ title: "Birth Date", text: "For Grade 1 admissions, child must be minimum 5.8 year old and can be maximum 6.11 year old as of 2017-03-31", showCloseButton: true, width: 480, });
                            $scope.edt.birth_date = "";
                        }
                    }
                }
            }
            $scope.createdate = function (date) {
                // console.log(date);
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                date1 = year + "/" + month + "/" + day;
                // console.log(date1);

                $scope.CurrentDate = new Date();
                var now = new Date();
                var month1 = (now.getMonth() + 1);
                var day1 = now.getDate();
                var year1 = now.getFullYear();
                if (month1 < 10)
                    month1 = "0" + month1;
                if (day1 < 10)
                    day1 = "0" + day1;
                date3 = now.getFullYear() + '/' + month1 + '/' + day1;
                var date4 = (now.getFullYear() - 4) + '/' + month1 + '/' + day1;
                //var year2 = (year1 - 4);
                // console.log(date4);

                if (date1 >= date3 || date1 <= date4) {
                    swal({ title: "Birth Date", text: "Age Should Not be less than 4 yrs or greater than current year", imageUrl: "assets/img/notification-alert.png", });
                    var v = document.getElementById('dp_birth_date');
                    v.value = "";
                }
            }



            $scope.NewSaveF = function () {
                $rootScope.BUSY.IsBusy = true;
                var data = $scope.edt;
                data.red_id = param;
                data.email_id = email;
                data.opr = "I";
                var subdata = []
                $http.post($scope.apiurl + "api/common/Admission/CUDInsertAdmission", data).then(function (res) {
                    $scope.Insertmsg1 = res.data;

                    if (res.data) {
                        data.admission_number = $scope.Insertmsg1.admission_number;
                        data.email_id = $scope.Insertmsg1.email_id;
                        data.grade_name = document.getElementById('cmb_grade').options[document.getElementById('cmb_grade').selectedIndex].text;

                        $rootScope.BUSY.IsBusy = false;
                        $scope.lbl_show = true;
                        $scope.lbl_msg = "Application Submitted Sucessfully with application number " + data.admission_number;

                        //for (var i = 0; i < $scope.subject_lst.length; i++) {
                        //    var subobj = {
                        //        sims_admission_number: data.admission_number,
                        //        sims_subject_code: $scope.subject_lst[i].sims_subject_code,
                        //        sims_subject_pref: $scope.subject_lst[i].sims_subject_pref,
                        //        index: i + 1,
                        //        opr: 'C'
                        //    }
                        //    subdata.push(subobj)
                        //}
                        //$http.post($scope.apiurl + "api/common/Admission/SubjectCUD", subdata).then(function (res) {
                        //});
                        // swal({ title: "Alert", text: "Application Submitted Sucessfully", imageUrl: "assets/img/notification-alert.png", },
                        //   function () {
                        $scope.show_details();
                        //   });
                    }
                    $scope.Insertmsg1 = "";
                    $scope.edt = "";
                });

            }
            $scope.NewSave = function (isvalidate) {

                if (isvalidate) {

                    //if ($scope.edt.grade_code == '15' || $scope.edt.grade_code == '16') {
                    //    if ($scope.edt.current_fyschool_name == undefined) {
                    //        swal({ title: "Alert", text: "F.Y Last Institute Name is Required.", showCloseButton: true, width: 450, });
                    //    }
                    //    else if ($scope.edt.current_fyschool_name == "") {
                    //        swal({ title: "Alert", text: "F.Y Last Institute Name is Required.", showCloseButton: true, width: 450, });
                    //    }
                    //    else if ($scope.edt.sims_fyadmission_marks_obtained_sem1 == undefined) {
                    //        swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-I is Required.", showCloseButton: true, width: 450, });
                    //    }
                    //    else if ($scope.edt.sims_fyadmission_marks_obtained_sem1 == "") {
                    //        swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-I is Required.", showCloseButton: true, width: 450, });
                    //    }
                    //    else if ($scope.edt.sims_fyadmission_marks_out_of_Sem1 == undefined) {
                    //        swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-I is Required.", showCloseButton: true, width: 450, });
                    //    }
                    //    else if ($scope.edt.sims_fyadmission_marks_out_of_Sem1 == "") {
                    //        swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-I is Required.", showCloseButton: true, width: 450, });
                    //    }

                    //    else if ($scope.edt.sims_fyadmission_marks_obtained_sem2 == undefined) {
                    //        swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-II is Required.", showCloseButton: true, width: 450, });
                    //    }
                    //    else if ($scope.edt.sims_fyadmission_marks_obtained_sem2 == "") {
                    //        swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-II is Required.", showCloseButton: true, width: 450, });
                    //    }
                    //    else if ($scope.edt.sims_fyadmission_marks_out_of_Sem2 == undefined) {
                    //        swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-II is Required.", showCloseButton: true, width: 450, });
                    //    }
                    //    else if ($scope.edt.sims_fyadmission_marks_out_of_Sem2 == "") {
                    //        swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-II is Required.", showCloseButton: true, width: 450, });
                    //    }

                    //    else if ($scope.edt.current_syschool_name == undefined) {
                    //        swal({ title: "Alert", text: "S.Y Last Institute Name is Required.", showCloseButton: true, width: 450, });
                    //    }
                    //    else if ($scope.edt.current_syschool_name == "") {
                    //        swal({ title: "Alert", text: "S.Y Last Institute Name is Required.", showCloseButton: true, width: 450, });
                    //    }
                    //    else if ($scope.edt.sims_syadmission_marks_obtained_sem3 == undefined) {
                    //        swal({ title: "Alert", text: "S.Y Last Marks Obtained Sem-III is Required.", showCloseButton: true, width: 450, });
                    //    }
                    //    else if ($scope.edt.sims_syadmission_marks_obtained_sem3 == "") {
                    //        swal({ title: "Alert", text: "S.Y Last Marks Obtained Sem-III is Required.", showCloseButton: true, width: 450, });
                    //    }
                    //    else if ($scope.edt.sims_syadmission_marks_obtained_sem4 == undefined) {
                    //        swal({ title: "Alert", text: "S.Y Last Marks Obtained Sem-IV is Required.", showCloseButton: true, width: 450, });
                    //    }
                    //    else if ($scope.edt.sims_syadmission_marks_obtained_sem4 == "") {
                    //        swal({ title: "Alert", text: "S.Y Last Marks Obtained Sem-IV is Required.", showCloseButton: true, width: 450, });
                    //    }
                    //    else if ($scope.edt.sims_syadmission_marks_out_of_sem4 == undefined) {
                    //        swal({ title: "Alert", text: "S.Y Last Marks Obtained Sem-IV is Required.", showCloseButton: true, width: 450, });
                    //    }
                    //    else if ($scope.edt.sims_syadmission_marks_out_of_sem4 == "") {
                    //        swal({ title: "Alert", text: "S.Y Last Marks Obtained Sem-IV is Required.", showCloseButton: true, width: 450, });
                    //    }
                    //    else {
                    //        $scope.NewSaveF();
                    //    }

                    //}

                    //else
                    if ($scope.edt.grade_code == '15' || $scope.edt.grade_code == '16') {
                        if ($scope.edt.current_fyschool_name == undefined) {
                            swal({ title: "Alert", text: "F.Y Last Institute Name is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.current_fyschool_name == "") {
                            swal({ title: "Alert", text: "F.Y Last Institute Name is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_obtained_sem1 == undefined) {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-I is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_obtained_sem1 == "") {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-I is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_out_of_Sem1 == undefined) {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-I is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_out_of_Sem1 == "") {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-I is Required.", showCloseButton: true, width: 450, });
                        }

                        else if ($scope.edt.sims_fyadmission_marks_obtained_sem2 == undefined) {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-II is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_obtained_sem2 == "") {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-II is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_out_of_Sem2 == undefined) {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-II is Required.", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sims_fyadmission_marks_out_of_Sem2 == "") {
                            swal({ title: "Alert", text: "F.Y Last Marks Obtained Sem-II is Required.", showCloseButton: true, width: 450, });
                        }


                        else {
                            $scope.NewSaveF();
                        }

                    }

                    else {
                        $scope.NewSaveF();
                    }

                }
                else {
                    swal({ title: "Alert", text: "Application incomplete, Kindly fill all the Mandatory Fields.", showCloseButton: true, width: 450, });
                }
            }

            $scope.NewSave1 = function (isvalidate) {

                if (isvalidate) {
                    if ($scope.edt.sibling_attend == true) {
                        if ($scope.edt.parent_id == undefined) {
                            swal({ title: "Alert", text: "Parent Id And Enroll No are Required", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.parent_id == "") {
                            swal({ title: "Alert", text: "Parent Id And Enroll No are Required", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sibling_enroll == undefined) {
                            swal({ title: "Alert", text: "Parent Id And Enroll No are Required", showCloseButton: true, width: 450, });
                        }
                        else if ($scope.edt.sibling_enroll == "") {
                            swal({ title: "Alert", text: "Parent Id And Enroll No are Required", showCloseButton: true, width: 450, });
                        }
                        else {
                            $rootScope.BUSY.IsBusy = true;
                            var data = $scope.edt;
                            data.red_id = param;
                            data.email_id = email;
                            data.opr = "I";

                            $http.post($scope.apiurl + "api/common/Admission/CUDInsertAdmission", data).then(function (res) {
                                $scope.Insertmsg1 = res.data;

                                if (res.status == "Error") {
                                    $scope.lbl_msg = "APPLICATION FORM SUBMITTED SUCCESSFULLY BUT WITH OUT APPLICATION NUMBER";
                                }
                                else {
                                    data.admission_number = $scope.Insertmsg1.admission_number;
                                    data.email_id = $scope.Insertmsg1.email_id;
                                    data.grade_name = document.getElementById('cmb_grade').options[document.getElementById('cmb_grade').selectedIndex].text;

                                    $rootScope.BUSY.IsBusy = false;
                                    $scope.lbl_show = true;
                                    $scope.lbl_msg = "Application Submitted Sucessfully with application number " + data.admission_number;
                                    //  swal({ title: "Alert", text: "Application Submitted Sucessfully", imageUrl: "assets/img/notification-alert.png", },
                                    //function ()
                                    //{
                                    $scope.show_details();
                                    // });
                                }
                                $scope.Insertmsg1 = "";
                                $scope.edt = "";
                            });
                        }
                    }
                    else {
                        $rootScope.BUSY.IsBusy = true;
                        var data = $scope.edt;
                        data.red_id = param;
                        data.email_id = email;
                        data.opr = "I";

                        var subdata = []



                        $http.post($scope.apiurl + "api/common/Admission/CUDInsertAdmission", data).then(function (res) {
                            $scope.Insertmsg1 = res.data;

                            if (res.data) {
                                data.admission_number = $scope.Insertmsg1.admission_number;
                                data.email_id = $scope.Insertmsg1.email_id;
                                data.grade_name = document.getElementById('cmb_grade').options[document.getElementById('cmb_grade').selectedIndex].text;

                                $rootScope.BUSY.IsBusy = false;
                                $scope.lbl_show = true;
                                $scope.lbl_msg = "Application Submitted Sucessfully with application number " + data.admission_number;

                                //for (var i = 0; i < $scope.subject_lst.length; i++) {
                                //    var subobj = {
                                //        sims_admission_number: data.admission_number,
                                //        sims_subject_code: $scope.subject_lst[i].sims_subject_code,
                                //        sims_subject_pref: $scope.subject_lst[i].sims_subject_pref,
                                //        index: i + 1,
                                //        opr: 'C'
                                //    }
                                //    subdata.push(subobj)
                                //}
                                //$http.post($scope.apiurl + "api/common/Admission/SubjectCUD", subdata).then(function (res) {
                                //});
                                // swal({ title: "Alert", text: "Application Submitted Sucessfully", imageUrl: "assets/img/notification-alert.png", },
                                //   function () {
                                $scope.show_details();
                                //   });
                            }
                            $scope.Insertmsg1 = "";
                            $scope.edt = "";
                        });
                    }
                }
                else {
                    // swal({ title: "Alert", text: "Application incomplete, Kindly fill all the Mandatory Fields.", imageUrl: "assets/img/notification-alert.png", });
                    //"Kindly Fill all the requirement details"

                    swal({ title: "Alert", text: "Application incomplete, Kindly fill all the Mandatory Fields.", showCloseButton: true, width: 450, });
                }
            }

            $scope.GetFatherSumAddr = function () {
                var data = $scope.edt;

                var father_aprt = $("#txt_father_aprt").val().length;
                var father_build = $("#txt_father_build").val().length;
                var father_street = $("#txt_father_Street").val().length;
                var father_area = $("#txt_father_area").val().length;
                var father_city = $("#txt_father_city").val().length;
                var father_state = $("#txt_father_state").val().length;
                var father_country = $("#cmb_father_country").val().length;
                var father_POBox = $("#txt_father_pobox").val().length;

                var country = document.getElementById('cmb_father_country').options[document.getElementById('cmb_father_country').selectedIndex].text;
                var state = document.getElementById('txt_father_state').options[document.getElementById('txt_father_state').selectedIndex].text;
                var city = document.getElementById('txt_father_city').options[document.getElementById('txt_father_city').selectedIndex].text;

                document.getElementById('txt_father_summary_add').value = $("#txt_father_aprt").val().length > 0 ? document.getElementById('txt_father_aprt').value + "," : "";
                document.getElementById('txt_father_summary_add').value += $("#txt_father_build").val().length > 0 ? document.getElementById('txt_father_build').value + "," : "";
                document.getElementById('txt_father_summary_add').value += $("#txt_father_Street").val().length > 0 ? document.getElementById('txt_father_Street').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_area > 0 ? document.getElementById('txt_father_area').value + "," : "";
                document.getElementById('txt_father_summary_add').value += father_city > 0 ? city + "," : "";
                document.getElementById('txt_father_summary_add').value += father_state > 0 ? state + "," : "";
                document.getElementById('txt_father_summary_add').value += father_country > 0 ? country + "," : "";
                document.getElementById('txt_father_summary_add').value += father_POBox > 0 ? document.getElementById('txt_father_pobox').value : "";

                var v = document.getElementById('txt_father_summary_add');
                data.father_summary_address = v.value;
                // console.log(data.father_summary_address);
            }



            $scope.Save = function (isvalidate) {
                if (isvalidate) {
                    debugger;
                    if ($scope.btn_NewSave == false) {
                        var data = $scope.edt;
                        data.red_id = $scope.admission_num_new;
                        data.opr = "U";
                        $scope.flg_sub = false;
                        // console.log($scope.edt.admission_date);
                        var subdata = [];

                        if ($scope.subject_lst1.length > 0) {
                            for (var i = 0; i < $scope.subject_lst1.length; i++) {
                                $scope.subject_lst1[i]['selected_count'] = 0;
                            }
                            for (var i = 0; i < $scope.subject_lst1.length; i++) {
                                $scope.subject_lst1[i]['selected_count'] = 0;
                            }

                            for (var i = 0; i < $scope.subject_lst1.length; i++) {
                                if ($scope.subject_lst1[i]['sublist'].length > 0) {

                                    for (var j = 0; j < $scope.subject_lst1[i]['sublist'].length; j++) {


                                        if ($scope.subject_lst1[i].sublist[j].sims_status) {
                                            $scope.subject_lst1[i].sublist[j]['sims_subject_pref'] = '1';
                                            $scope.subject_lst1[i]['selected_count'] = $scope.subject_lst1[i]['selected_count'] + 1;
                                        }
                                        var subobj = {
                                            sims_admission_number: data.admission_number,
                                            sims_subject_code: $scope.subject_lst1[i].sublist[j].sims_subject_code,
                                            sims_subject_pref: $scope.subject_lst1[i].sublist[j].sims_subject_pref,
                                            index: i,
                                            opr: 'C'
                                        }


                                        subdata.push(subobj);

                                    }
                                }
                                else {

                                    var subobj = {
                                        sims_admission_number: data.admission_number,
                                        sims_subject_code: $scope.subject_lst1[i].sims_subject_code,
                                        sims_subject_pref: $scope.subject_lst1[i].sims_subject_pref,
                                        index: i,
                                        opr: 'C'
                                    }

                                    subdata.push(subobj)
                                }

                            }



                            for (var i = 0; i < $scope.subject_lst1.length; i++) {
                                if ($scope.subject_lst1[i].sims_compulsory_subject != '') {
                                    if (parseInt($scope.subject_lst1[i].sims_compulsory_subject) != parseInt($scope.subject_lst1[i].selected_count)) {
                                        $scope.flg_sub = true;
                                        swal({ title: "Alert", text: "Select the provisional ELECTIVE from the group " + $scope.subject_lst1[i].sims_language_desc, showCloseButton: true, width: 450, });
                                        break;
                                    }
                                }
                            }
                        }

                        if ($scope.flg_sub == false) {
                            $http.post($scope.apiurl + "api/common/Admission/CUDUpdateAdmission", data).then(function (res) {
                                debugger;
                                //$rootScope.BUSY.IsBusy = false;
                                //$scope.lbl_show = true;
                                //$scope.lbl_msg = "Application Updated Sucessfully";

                                // $scope.show_details();
                                swal({ title: "Alert", text: "Application Updated Sucessfully", imageUrl: "assets/img/notification-alert.png", },
                                function () {

                                });

                                $http.post($scope.apiurl + "api/common/Admission/SubjectCUD", subdata).then(function (res) {
                                });
                            });

                        }
                    }
                    else {
                        //console.log('ddddd');
                        //$scope.NewSave(isvalidate);
                    }

                }

            }

            $scope.back = function () {
                $scope.dd = true;
                window.localStorage['Admflag'] = $scope.dd;
                $scope.modal_cancel();
            }

            $scope.modal_cancel = function () {
                //$('body').addClass('grey condense-menu');
                //$('#main-menu').addClass('mini');
                //$('.page-content').addClass('condensed');
                //$scope.isCondensed = true;
                //$("body").removeClass("modal-open");
                //$("div").removeClass("modal-backdrop in");
                $('#myModal1').modal('hide');

                console.log($stateParams.Class);
                $timeout(function () {
                    if ($http.defaults.headers.common['schoolId'] == 'siso' || $http.defaults.headers.common['schoolId'] == 'asis' || $http.defaults.headers.common['schoolId'] == 'abqis')
                        $state.go("main.Dasiso", { Class: $stateParams.Class });

                    else
                        $state.go("main.DsDmc", { Class: $stateParams.Class });

                }, 300);
            }

            $("#txt_year,#txt_fyyear,#txt_syyear").datepicker({
                format: "MM-yyyy",
                viewMode: "months",
                minViewMode: "months"
            });


            $("#dp_comm_date,  #dp_admdate, #dp_birth_date").kendoDatePicker({
                format: "yyyy-MM-dd"
                //format: "dd/MM/yyyy"
                //format: "yyyy/MM/dd"
            });

        }])


})();