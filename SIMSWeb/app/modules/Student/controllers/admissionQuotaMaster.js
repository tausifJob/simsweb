﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('admissionQuotaMasterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.temp = {};
            $scope.table1 = true;
            $scope.pager = true;
            //Get Curriculam and aca year
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (response) {
                $scope.curiculums = response.data;
                $scope.temp.sims_cur_code = $scope.curiculums[0].sims_cur_code;
                $scope.getAcaYear($scope.temp.sims_cur_code);
            });

            $scope.getAcaYear = function (cur_code) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code).then(function (response) {
                    $scope.acaYears = response.data;
                    $scope.temp.sims_academic_year = $scope.acaYears[0].sims_academic_year;
                });
            }

            $scope.pagesize = "10";
            $scope.pageindex = 0;

            $scope.newAddForm = function () {
                $scope.temp = {};
                $scope.temp.admQuotaStatus = true;
                $scope.showAddForm = true;
                $scope.table1 = false;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.quotaDescReadOnly = false;
                $scope.yearReadonly = false;
                $scope.curiculamReadonly = false;
                if ($scope.curiculums.length > 0) {
                    $scope.temp.sims_cur_code = $scope.curiculums[0].sims_cur_code;
                }

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.temp.sims_cur_code).then(function (response) {
                    $scope.acaYears = response.data;
                    if ($scope.acaYears.length > 0) {
                        $scope.temp.sims_academic_year = $scope.acaYears[0].sims_academic_year;
                    }

                });
            }

            $scope.cancelAddForm = function () {
                $scope.showAddForm = false;
                $scope.table1 = true;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            debugger;
            $scope.getAdmissionQuota = function () {
                $http.get(ENV.apiUrl + "api/admissionQuota/getAllAdmissionQuota").then(function (response) {
                    $scope.admissionQuota = response.data;
                    $scope.totalItems = $scope.admissionQuota.length;
                    $scope.todos = $scope.admissionQuota;
                    $scope.makeTodos();

                });
            };

            $scope.getAdmissionQuota();

            $scope.size = function (str) {
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //$scope.makeTodos();
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.admissionQuota;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.admissionQuota, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.admissionQuota;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_academic_year.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_quota_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_q_strength == toSearch) ? true : false;
            }

            $scope.checkQutaExists = function () {
                angular.forEach($scope.admissionQuota, function (value, key) {
                    if (value.sims_quota_desc == $scope.temp.sims_quota_desc) {
                        swal({ text: "Admission quota description already exists", imageUrl: "assets/img/notification-alert.png", timer: 5000, width: 380 });
                        $scope.temp.sims_quota_desc = "";
                    }
                });
            }

            //DATA SAVE INSERT
            var data = "";
            $scope.saveAdmissionData = function (Myform) {
                //$scope.table1 = true;
                var postArray = [];
                if (Myform) {
                    var data = $scope.temp;
                    if (data.admQuotaStatus == true) {
                        data.sims_q_status = "true";
                    }
                    else {
                        data.sims_q_status = "false";
                    }
                    data.opr = 'I';
                    postArray.push(data);

                    $http.post(ENV.apiUrl + "api/admissionQuota/addUpdateAdmissionQuota", postArray).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if (msg.status == 200) {
                            //$scope.showAddForm = false;
                        }                        
                        if ($scope.msg1 == true) {
                            swal({ text: "Admission quota added successfully", imageUrl: "assets/img/check.png", timer: 5000, width: 380 });
                        }
                        else {
                            swal({ text: "Admission quota not added", imageUrl: "assets/img/close.png", timer: 5000, width: 380 });
                        }

                        $scope.cancelAddForm();
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                        
                        $scope.getAdmissionQuota();
                    });
                    data = "";
                }

            }


            //DATA EDIT
            $scope.editAdmmission = function (obj) {
               debugger
                $scope.quotaDescReadOnly = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.showAddForm = true;
                
                $scope.table1 = false;
                $scope.yearReadonly = true;
                $scope.curiculamReadonly = true;

                $scope.temp = {
                    sims_cur_code: obj.sims_cur_code,
                    sims_academic_year: obj.sims_academic_year,
                    sims_quota_desc: obj.sims_quota_desc,
                    sims_q_strength: obj.sims_q_strength,
                    sims_quota_id: obj.sims_quota_id
                };

                if (obj.sims_q_status == "A") {
                    $scope.temp.admQuotaStatus = true;
                }
                else {
                    $scope.temp.admQuotaStatus = false;
                }

            }

            $scope.updateAdmissionData = function (Myform) {
                //$scope.table1 = true;
                debugger
                var postArray = [];
                if (Myform) {
                    var data = $scope.temp;
                    if (data.admQuotaStatus == true) {
                        data.sims_q_status = "true";
                    }
                    else {
                        data.sims_q_status = "false";
                    }
                    data.opr = 'U';

                    postArray.push(data);

                    $http.post(ENV.apiUrl + "api/admissionQuota/addUpdateAdmissionQuota", postArray).then(function (msg) {
                        $scope.msg1 = msg.data;
                        console.log($scope.msg1);
                        if ($scope.msg1 == true) {
                            swal({ text: "Admission quota updated successfully", imageUrl: "assets/img/check.png", timer: 5000, width: 380 });
                        }
                        else {
                            swal({ text: "Admission quota not updated", imageUrl: "assets/img/close.png", timer: 5000, width: 380 });
                        }
                        $scope.cancelAddForm();                       
                        $scope.getAdmissionQuota();
                    });
                    data = "";
                }

            }

         }])

})();
