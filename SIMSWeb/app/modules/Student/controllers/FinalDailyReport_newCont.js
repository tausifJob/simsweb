﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('FinalDailyReport_newCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.temp = {};
            $scope.datasend1 = [];
            $scope.pagesize = "25";
            $scope.sims_freq = "0";
            $scope.pageindex = "1";
            $scope.home_checking = true;
            $scope.hide_grade = false;
            $scope.hide_section = false;
            $scope.hide_Homerm = true;
            $scope.submite_room = true;
            $scope.submite_section = false;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.temp = {};
            $scope.display = true;
            $scope.add_btn = false;
            $scope.any_incidence = true;
            //$scope.save_btn = true;
            var dataforSave = [];
            var user = $rootScope.globals.currentUser.username;
            $scope.edt = {};

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 25, $scope.maxSize = 25;


            $(function () {
                $('#grade_box').multipleSelect({ width: '100%' });
                $('#section_box').multipleSelect({ width: '100%' });

            });

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.edt.mom_start_date = dd + '-' + mm + '-' + yyyy;
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;



            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#chkmarks1").attr("disabled", "disabled");
                        $scope.any_incidence = 'Yes';

                    } else {

                        $("#chkmarks1").removeAttr("disabled");
                        $("#chkmarks1").focus();
                        $scope.any_incidence = 'No';
                    }
                });
            });
            $scope.home_room = function () {
                debugger;
                $scope.hide_grade = false;
                $scope.hide_section = false;
                $scope.hide_Homerm = true;
                $scope.submite_room = true;
                $scope.submite_section = false;
                $scope.DailyTable = false;
                $scope.RoomTable = true;
            }

            //class hide
            $scope.class_hide = function () {
                debugger;
                $scope.hide_grade = true;
                $scope.hide_section = true;
                $scope.hide_Homerm = false;
                $scope.submite_section = true;
                $scope.submite_room = false;
                $scope.DailyTable = true;
                $scope.RoomTable = false;
                $scope.temp.sims_homeroom_code = '';
            }
            

            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_end_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });

            $("#from_date").kendoDatePicker({
                format: "dd-MM-yyyy",
                min: new Date(1900, 1, 1),
                max: new Date()
            });


            var today = new Date();

            today = ('0' + today.getDate()).slice(-2) + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + today.getFullYear();
            $scope.mom_start_date = today;




            $http.get(ENV.apiUrl + "api/DailyReport_new/getCuriculum").then(function (res1) {
                $scope.sims_cur = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_cur_code'] = res1.data[0].sims_cur_code;
                    $scope.getAccYear(res1.data[0].sims_cur_code);
                }
            });

            $scope.getAccYear = function (cur_code) {
                debugger
                $http.get(ENV.apiUrl + "api/DailyReport_new/getAcademicYear?curCode=" + cur_code).then(function (res1) {
                    $scope.academic_year = res1.data;
                    if (res1.data.length > 0) {
                        $scope.temp['sims_academic_year'] = res1.data[0].sims_academic_year;
                        $scope.getAllGrades(cur_code, res1.data[0].sims_academic_year)
                        $scope.acdm_yr_change(cur_code, res1.data[0].sims_academic_year)
                    }
                });

            }

            $scope.acdm_yr_change = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/DailyReport_new/getHomeRoomBatch?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&userName=" + user).then(function (grades) {
                    $scope.getsec = grades.data;
                    $scope.edt.sims_homeroom_code = grades.data[0].sims_homeroom_code;
                    console.log($scope.getsec);
                });
            }

            $scope.getAllGrades = function (cur_code, academic_year) {
                debugger
                $http.get(ENV.apiUrl + "api/DailyReport_new/getAllGradesNew?cur_code=" + cur_code + "&academic_year=" + academic_year + "&user=" + user).then(function (res1) {
                    $scope.grade_code = res1.data;
                    setTimeout(function () {
                        $('#grade_box').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }

            $scope.getSectionFromGrade = function (cur_code, academic_year, grade_code) {
                debugger;
                $http.get(ENV.apiUrl + "api/DailyReport_new/getSectionFromGradeNew?cur_code=" + cur_code + "&academic_year=" + academic_year + "&grade_code=" + grade_code + "&user=" + user).then(function (res1) {
                    $scope.section_code = res1.data;

                    setTimeout(function () {
                        $('#section_box').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }

            $scope.daily_report = function () {
                debugger
                $http.get(ENV.apiUrl + "api/DailyReport_new/getFinalDailyReport?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&date=" + $scope.mom_start_date).then(function (res1) {

                    if (res1.data.length > 0) {
                        debugger;
                        $scope.report_data = res1.data;
                        $scope.pager = true;
                        $scope.display = true;
                        $scope.add_btn = false;
                        $scope.totalItems = $scope.report_data.length;
                        $scope.todos = $scope.report_data;
                        $scope.makeTodos();

                        if (parseInt($scope.report_data[0].flag) > 0) {
                            debugger;
                            $scope.edt.view_button == true;
                        }
                        else {
                            $scope.edt.view_button == false;

                        }

                    }
                    else {
                        swal({ title: "Alert", text: " Data is not Available", showCloseButton: true, width: 300, height: 200 });
                        $scope.pager = false;
                        $scope.filteredTodos = [];
                    }
                    //angular.forEach($scope.report_data, function (f) {
                    //    f.sims_bell_lecture_per_week = parseFloat(f.sims_bell_lecture_per_week);
                    //});
                    //$scope.filteredTodos = [];
                    console.log($scope.report_data);
                });
            }

            $scope.home_report = function () {
                debugger
                $http.get(ENV.apiUrl + "api/DailyReport_new/getHomeRoomReport?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&homerum_code=" + $scope.temp.sims_homeroom_code + "&date=" + $scope.mom_start_date + "&user=" + user).then(function (res5) {

                    if (res5.data.length > 0) {
                        debugger;
                        $scope.homeroom_data = res5.data;
                        $scope.pager = true;
                        $scope.RoomTable = true;
                        $scope.add_btn = false;
                        $scope.totalItems = $scope.homeroom_data.length;
                        $scope.todos = $scope.homeroom_data;
                        $scope.makeTodos();

                         }
                    else {
                        swal({ title: "Alert", text: " Data is not Available !!!!", showCloseButton: true, width: 300, height: 200 });
                        $scope.pager = false;
                        $scope.filteredTodos = [];
                    }
                    //angular.forEach($scope.report_data, function (f) {
                    //    f.sims_bell_lecture_per_week = parseFloat(f.sims_bell_lecture_per_week);
                    //});
                    //$scope.filteredTodos = [];
                    console.log($scope.homeroom_data);
                });
            }
            $http.get(ENV.apiUrl + "api/DailyReport_new/getFeeling").then(function (res1) {
                $scope.sims_feelings = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_subcode2'] = res1.data[0].sims_obsprog_subcode2;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getActivity").then(function (res1) {
                $scope.sims_activity = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_subcode2'] = res1.data[0].sims_obsprog_subcode2;

                }
                setTimeout(function () {
                    $('#Select6').change(function () {

                    }).multipleSelect({
                        width: '100%'
                    });
                    //$("#show_box").multipleSelect("checkAll");
                }, 1000);

            });



            $http.get(ENV.apiUrl + "api/DailyReport_new/getFrequencydry").then(function (res1) {
                debugger;
                $scope.sims_freq_dry = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getFrequencywet").then(function (res1) {
                debugger;
                $scope.sims_freq_wet = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getFrequencybm").then(function (res1) {
                debugger;
                $scope.sims_freq_bm = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getFrequencyn1").then(function (res1) {
                debugger;
                $scope.sims_freq_n1 = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getFrequencyn2").then(function (res1) {
                debugger;
                $scope.sims_freq_n2 = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getFrequencyask").then(function (res1) {
                debugger;
                $scope.sims_freq_ask = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getFrequencymilk").then(function (res1) {
                debugger;
                $scope.sims_freq_milk = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_quantity_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getFrequencywat").then(function (res1) {
                debugger;
                $scope.sims_freq_wat = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_quantity_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getFrequencyjui").then(function (res1) {
                debugger;
                $scope.sims_freq_jui = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_quantity_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getqtymilk").then(function (res1) {
                debugger;
                $scope.sims_qty_milk = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getqtywat").then(function (res1) {
                debugger;
                $scope.sims_qty_wat = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getqtyjui").then(function (res1) {
                debugger;
                $scope.sims_qty_jui = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getFrequencymor").then(function (res1) {
                debugger;
                $scope.sims_freq_mor = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['mor_frequency_code'] = res1.data[0].mor_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getFrequencylunch").then(function (res1) {
                debugger;
                $scope.sims_freq_lunch = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['lunch_frequency_code'] = res1.data[0].lunch_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getFrequencyaft").then(function (res1) {
                debugger;
                $scope.sims_freq_aft = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['aft_frequency_code'] = res1.data[0].aft_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getqtymor").then(function (res1) {
                debugger;
                $scope.sims_qty_mor = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getqtylunch").then(function (res1) {
                debugger;
                $scope.sims_qty_lunch = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getqtyaft").then(function (res1) {
                debugger;
                $scope.sims_qty_aft = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getFrequencyclothing").then(function (res1) {
                debugger;
                $scope.sims_freq_clothing = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getFrequencydiaper").then(function (res1) {
                debugger;
                $scope.sims_freq_diaper = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getFrequencyformula").then(function (res1) {
                debugger;
                $scope.sims_freq_formula = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getFrequencywetwipes").then(function (res1) {
                debugger;
                $scope.sims_freq_wetwipes = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getFrequencyothers").then(function (res1) {
                debugger;
                $scope.sims_freq_others = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_code_tag_parameter_frequency_code'] = res1.data[0].sims_obsprog_code_tag_parameter_frequency_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getQuantity").then(function (res1) {
                debugger;
                $scope.sims_qty = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['quantity_code'] = res1.data[0].quantity_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getPlzProvide").then(function (res1) {
                debugger;
                $scope.sims_plz_prov = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['please_prov_code'] = res1.data[0].please_prov_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getPlzProvideQty").then(function (res1) {
                debugger;
                $scope.sims_plz_prov_qty = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['please_prov_qty_code'] = res1.data[0].please_prov_qty_code;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getsleeptime").then(function (res1) {
                debugger;
                $scope.sims_sleeptime = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_subcode2'] = res1.data[0].sims_obsprog_subcode2;

                }
            });

            $http.get(ENV.apiUrl + "api/DailyReport_new/getwaketime").then(function (res1) {
                debugger;
                $scope.sims_waketime = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_obsprog_subcode2'] = res1.data[0].sims_obsprog_subcode2;

                }
            });


            $scope.feeling_type = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_feel = null;
                    $scope.edt.sims_obsprog_subcode1_feel = null;

                }
                else {
                    $scope.edt.sims_obsprog_code_feel = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_feel = type[0].sims_obsprog_subcode1;
                }

            }

            $scope.activity_type = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_activity = null;
                    $scope.edt.sims_obsprog_subcode1_activity = null;

                }
                else {
                    $scope.edt.sims_obsprog_code_activity = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_activity = type[0].sims_obsprog_subcode1;
                }

            }

            $scope.checkNappyChange = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_dry = null;
                    $scope.edt.sims_obsprog_subcode1_dry = null;
                    $scope.edt.sims_obsprog_code_tag_code_dry = null

                }
                else {
                    $scope.edt.sims_obsprog_code_dry = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_dry = type[0].sims_obsprog_subcode1;
                    $scope.edt.sims_obsprog_code_tag_code_dry = type[0].sims_obsprog_code_tag_code;
                }

            }

            $scope.checkNappyChange1 = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_wet = null;
                    $scope.edt.sims_obsprog_subcode1_wet = null;
                    $scope.edt.sims_obsprog_code_tag_code_wet = null;

                }
                else {
                    $scope.edt.sims_obsprog_code_wet = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_wet = type[0].sims_obsprog_subcode1;
                    $scope.edt.sims_obsprog_code_tag_code_wet = type[0].sims_obsprog_code_tag_code;
                }

            }

            $scope.checkNappyChange2 = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_bm = null;
                    $scope.edt.sims_obsprog_subcode1_bm = null;
                    $scope.edt.sims_obsprog_code_tag_code_bm = null;

                }
                else {
                    $scope.edt.sims_obsprog_code_bm = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_bm = type[0].sims_obsprog_subcode1;
                    $scope.edt.sims_obsprog_code_tag_code_bm = type[0].sims_obsprog_code_tag_code;
                }

            }

            $scope.checkTiletTraining = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_n1 = null;
                    $scope.edt.sims_obsprog_subcode1_n1 = null;
                    $scope.edt.sims_obsprog_code_tag_code_n1 = null;

                }
                else {
                    $scope.edt.sims_obsprog_code_n1 = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_n1 = type[0].sims_obsprog_subcode1;
                    $scope.edt.sims_obsprog_code_tag_code_n1 = type[0].sims_obsprog_code_tag_code;
                }

            }

            $scope.checkTiletTraining1 = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code = null;
                    $scope.edt.sims_obsprog_subcode1 = null;
                    $scope.edt.sims_obsprog_code_tag_code = null;

                }
                else {
                    $scope.edt.sims_obsprog_code_n2 = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_n2 = type[0].sims_obsprog_subcode1;
                    $scope.edt.sims_obsprog_code_tag_code_n2 = type[0].sims_obsprog_code_tag_code;
                }

            }

            $scope.checkTiletTraining2 = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_ask = null;
                    $scope.edt.sims_obsprog_subcode1_ask = null;
                    $scope.edt.sims_obsprog_code_tag_code_ask = null;

                }
                else {
                    $scope.edt.sims_obsprog_code_ask = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_ask = type[0].sims_obsprog_subcode1;
                    $scope.edt.sims_obsprog_code_tag_code_ask = type[0].sims_obsprog_code_tag_code;
                }

            }

            $scope.checkFluidIntake = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_milk = null;
                    $scope.edt.sims_obsprog_subcode1_milk = null;
                    $scope.edt.sims_obsprog_code_tag_code_milk = null;

                }
                else {
                    $scope.edt.sims_obsprog_code_milk = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_milk = type[0].sims_obsprog_subcode1;
                    $scope.edt.sims_obsprog_code_tag_code_milk = type[0].sims_obsprog_code_tag_code;
                }

            }

            $scope.checkFluidIntake1 = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_water = null;
                    $scope.edt.sims_obsprog_subcode1_water = null;
                    $scope.edt.sims_obsprog_code_tag_code_water = null;

                }
                else {
                    $scope.edt.sims_obsprog_code_water = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_water = type[0].sims_obsprog_subcode1;
                    $scope.edt.sims_obsprog_code_tag_code_water = type[0].sims_obsprog_code_tag_code;
                }

            }

            $scope.checkFluidIntake2 = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_juice = null;
                    $scope.edt.sims_obsprog_subcode1_juice = null;
                    $scope.edt.sims_obsprog_code_tag_code_juice = null;

                }
                else {
                    $scope.edt.sims_obsprog_code_juice = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_juice = type[0].sims_obsprog_subcode1;
                    $scope.edt.sims_obsprog_code_tag_code_juice = type[0].sims_obsprog_code_tag_code;
                }

            }

            $scope.checkFood = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_mor = null;
                    $scope.edt.sims_obsprog_subcode1_mor = null;
                    $scope.edt.sims_obsprog_code_tag_code_mor = null;

                }
                else {
                    $scope.edt.sims_obsprog_code_mor = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_mor = type[0].sims_obsprog_subcode1;
                    $scope.edt.sims_obsprog_code_tag_code_mor = type[0].sims_obsprog_code_tag_code;
                }

            }

            $scope.checkFood1 = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_lunch = null;
                    $scope.edt.sims_obsprog_subcode1_lunch = null;
                    $scope.edt.sims_obsprog_code_tag_code_lunch = null;

                }
                else {
                    $scope.edt.sims_obsprog_code_lunch = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_lunch = type[0].sims_obsprog_subcode1;
                    $scope.edt.sims_obsprog_code_tag_code_lunch = type[0].sims_obsprog_code_tag_code;
                }

            }

            $scope.checkFood2 = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_aft = null;
                    $scope.edt.sims_obsprog_subcode1_aft = null;
                    $scope.edt.sims_obsprog_code_tag_code_aft = null;

                }
                else {
                    $scope.edt.sims_obsprog_code_aft = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_aft = type[0].sims_obsprog_subcode1;
                    $scope.edt.sims_obsprog_code_tag_code_aft = type[0].sims_obsprog_code_tag_code;
                }

            }

            $scope.provide_type = function (str, type) {
                debugger;


                if (type == "Diapers") {
                    $scope.edt.sims_student_please_provide_diaper = "Diapers/Pull-Ups";
                }
                else {
                    $scope.edt.sims_student_please_provide_diaper = null;
                }

            }

            $scope.provide_type1 = function (str, type) {
                debugger;


                if (type == "Wetwipes") {
                    $scope.edt.sims_student_please_provide_Wet_wipes = "Wet Wipes";
                }
                else {
                    $scope.edt.sims_student_please_provide_Wet_wipes = null;
                }

            }

            $scope.provide_type2 = function (str, type) {
                debugger;


                if (type == "Formula") {
                    $scope.edt.sims_student_please_provide_Wet_Formula = "Formula";
                }
                else {
                    $scope.edt.sims_student_please_provide_Wet_Formula = null;
                }

            }

            $scope.provide_type3 = function (str, type) {
                debugger;


                if (type == "Clothing") {
                    $scope.edt.sims_student_please_provide_Wet_Clothing = "Clothing";
                }
                else {
                    $scope.edt.sims_student_please_provide_Wet_Clothing = null;
                }

            }

            $scope.provide_type4 = function (str, type) {
                debugger;


                if (type == "Others") {
                    $scope.edt.sims_student_please_provide_Wet_Others = "Others";
                }
                else {
                    $scope.edt.sims_student_please_provide_Wet_Others = null;
                }

            }

            $scope.checkpleaseprovide = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_clothing = null;
                    $scope.edt.sims_obsprog_subcode1_clothing = null;
                    $scope.edt.sims_obsprog_code_tag_code_clothing = null

                }
                else {
                    $scope.edt.sims_obsprog_code_clothing = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_clothing = type[0].sims_obsprog_subcode1;
                    $scope.edt.sims_obsprog_code_tag_code_clothing = type[0].sims_obsprog_code_tag_code;
                }

            }

            $scope.checkpleaseprovide1 = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_diaper = null;
                    $scope.edt.sims_obsprog_subcode1_diaper = null;
                    $scope.edt.sims_obsprog_code_tag_code_diaper = null

                }
                else {
                    $scope.edt.sims_obsprog_code_diaper = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_diaper = type[0].sims_obsprog_subcode1;
                    $scope.edt.sims_obsprog_code_tag_code_diaper = type[0].sims_obsprog_code_tag_code;
                }

            }

            $scope.checkpleaseprovide2 = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_formula = null;
                    $scope.edt.sims_obsprog_subcode1_formula = null;
                    $scope.edt.sims_obsprog_code_tag_code_formula = null

                }
                else {
                    $scope.edt.sims_obsprog_code_formula = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_formula = type[0].sims_obsprog_subcode1;
                    $scope.edt.sims_obsprog_code_tag_code_formula = type[0].sims_obsprog_code_tag_code;
                }

            }

            $scope.checkpleaseprovide3 = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_wetwipes = null;
                    $scope.edt.sims_obsprog_subcode1_wetwipes = null;
                    $scope.edt.sims_obsprog_code_tag_code_wetwipes = null

                }
                else {
                    $scope.edt.sims_obsprog_code_wetwipes = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_wetwipes = type[0].sims_obsprog_subcode1;
                    $scope.edt.sims_obsprog_code_tag_code_wetwipes = type[0].sims_obsprog_code_tag_code;
                }

            }

            $scope.checkpleaseprovide4 = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_others = null;
                    $scope.edt.sims_obsprog_subcode1_others = null;
                    $scope.edt.sims_obsprog_code_tag_code_others = null

                }
                else {
                    $scope.edt.sims_obsprog_code_others = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_others = type[0].sims_obsprog_subcode1;
                    $scope.edt.sims_obsprog_code_tag_code_others = type[0].sims_obsprog_code_tag_code;
                }

            }

            $scope.checksleep = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_sleep = null;
                    $scope.edt.sims_obsprog_subcode1_sleep = null;
                    $scope.edt.sims_obsprog_subcode2_sleep = null;

                }
                else {
                    $scope.edt.sims_obsprog_code_sleep = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_sleep = type[0].sims_obsprog_subcode1;
                    $scope.edt.sims_obsprog_subcode2_sleep = type[0].sims_obsprog_subcode2;
                }

            }

            $scope.checkwakeup = function (str, type) {
                debugger;
                if (str == undefined || str == "" || str == null) {
                    $scope.edt.sims_obsprog_code_wakeup = null;
                    $scope.edt.sims_obsprog_subcode1_wakeup = null;
                    $scope.edt.sims_obsprog_subcode2_wakeup = null;

                }
                else {
                    $scope.edt.sims_obsprog_code_wakeup = type[0].sims_obsprog_code;
                    $scope.edt.sims_obsprog_subcode1_wakeup = type[0].sims_obsprog_subcode1;
                    $scope.edt.sims_obsprog_subcode2_wakeup = type[0].sims_obsprog_subcode2;
                }

            }


            var datasend = [];
            $scope.save_btn_function = function (Myform) {
                debugger
                console.log($scope.datasend1);
                if (Myform) {
                    var data = $scope.edt;
                    data.opr = 'I';

                    //$scope.datasend1.push($scope.data);
                    data.sims_academic_year = $scope.datasend1[0].sims_academic_year;
                    data.sims_enroll_number = $scope.datasend1[0].sims_enroll_number;
                    data.sims_cur_code = $scope.datasend1[0].sims_cur_code;
                    data.sims_grade_code = $scope.datasend1[0].sims_grade_code;
                    data.sims_section_code = $scope.datasend1[0].sims_section_code;
                    data.mom_start_date = $scope.mom_start_date;
                    data.sims_student_created_by = $rootScope.globals.currentUser.username;
                    datasend.push(data);


                    /// split

                    var t = [];
                    var obj = {
                        opr: 'I',
                        sims_academic_year: $scope.datasend1[0].sims_academic_year,
                        sims_enroll_number: $scope.datasend1[0].sims_enroll_number,
                        sims_cur_code: $scope.datasend1[0].sims_cur_code,
                        sims_grade_code: $scope.datasend1[0].sims_grade_code,
                        sims_section_code: $scope.datasend1[0].sims_section_code,
                        mom_start_date: $scope.mom_start_date,
                        sims_student_created_by: $rootScope.globals.currentUser.username,

                    }

                    var obj_feel = angular.copy(obj);
                    obj_feel['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_feel;
                    obj_feel['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_feel;
                    obj_feel['sims_obsprog_subcode2'] = $scope.edt.sims_student_feeling;
                    obj_feel['sims_student_parent_comment'] = $scope.edt.sims_feelingt_parent_comment,
                    obj_feel['sims_student_teacher_comment'] = $scope.edt.sims_feeling_teacher_comment,
                    //obj_feel['sims_obsprog_subcode3'] = '';
                    //obj_feel['sims_obsprog_code_tag_code'] = '';
                    //obj_feel['sims_obsprog_code_tag_parameter_frequency_code'] = '';
                    //obj_feel['sims_obsprog_code_tag_parameter_frequency_description'] = '';
                    //obj_feel['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = '';
                    //obj_feel['sims_obsprog_code_tag_parameter_frequency_quantity_code_description'] = null;
                    t.push(obj_feel);
                    debugger;
                    try {
                        for (var i = 0; i < $scope.edt.sims_student_activity.length; i++) {                            
                            var obj_activity = angular.copy(obj);
                            obj_activity['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_activity;
                            obj_activity['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_activity;
                            obj_activity['sims_obsprog_subcode2'] = $scope.edt.sims_student_activity[i]
                            obj_activity['sims_student_parent_comment'] = $scope.edt.sims_activity_parent_comment,
                            obj_activity['sims_student_teacher_comment'] = $scope.edt.sims_activity_teacher_comment,
                            t.push(obj_activity);
                        }
                    } catch (e) {

                    }

                 

                    var obj_nappy_dry = angular.copy(obj);
                    obj_nappy_dry['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_dry;
                    obj_nappy_dry['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_dry;
                    obj_nappy_dry['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_dry;
                    obj_nappy_dry['sims_obsprog_code_tag_parameter_frequency_code'] = $scope.edt.sims_student_nappy_changing_dry_frequency;
                    obj_nappy_dry['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_nappy_changing_dry_Remark;
                    obj_nappy_dry['sims_student_parent_comment'] = $scope.edt.sims_student_nappy_changing_dry_parent_Remark,
                    obj_nappy_dry['sims_student_teacher_comment'] = $scope.edt.sims_student_nappy_changing_dry_teacher_Remark,
                    //obj_activity['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = '';
                    t.push(obj_nappy_dry);

                    var obj_nappy_wet = angular.copy(obj);
                    obj_nappy_wet['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_wet;
                    obj_nappy_wet['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_wet;
                    obj_nappy_wet['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_wet;
                    obj_nappy_wet['sims_obsprog_code_tag_parameter_frequency_code'] = $scope.edt.sims_student_nappy_changing_wet_frequency;
                    obj_nappy_wet['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_nappy_changing_wet_Remark;
                    obj_nappy_wet['sims_student_parent_comment'] = $scope.edt.sims_student_nappy_changing_wet_parent_Remark,
                    obj_nappy_wet['sims_student_teacher_comment'] = $scope.edt.sims_student_nappy_changing_wet_teacher_Remark,
                    t.push(obj_nappy_wet);

                    var obj_nappy_bm = angular.copy(obj);
                    obj_nappy_bm['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_bm;
                    obj_nappy_bm['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_bm;
                    obj_nappy_bm['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_bm;
                    obj_nappy_bm['sims_obsprog_code_tag_parameter_frequency_code'] = $scope.edt.sims_student_nappy_changing_bm_frequency;
                    obj_nappy_bm['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_nappy_changing_bm_Remark;
                    obj_nappy_bm['sims_student_parent_comment'] = $scope.edt.sims_student_nappy_changing_bm_parent_Remark,
                    obj_nappy_bm['sims_student_teacher_comment'] = $scope.edt.sims_student_nappy_changing_bm_teacher_Remark,
                    t.push(obj_nappy_bm);

                    var obj_toilet_n1 = angular.copy(obj);
                    obj_toilet_n1['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_n1;
                    obj_toilet_n1['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_n1;
                    obj_toilet_n1['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_n1;
                    obj_toilet_n1['sims_obsprog_code_tag_parameter_frequency_code'] = $scope.edt.sims_student_toilet_training_Number1_frequency;
                    obj_toilet_n1['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_toilet_training_Number1_Remark;
                    obj_toilet_n1['sims_student_parent_comment'] = $scope.edt.sims_student_toilet_n1_parent_Remark,
                    obj_toilet_n1['sims_student_teacher_comment'] = $scope.edt.sims_student_toilet_n1_teacher_Remark,
                    t.push(obj_toilet_n1);

                    var obj_toilet_n2 = angular.copy(obj);
                    obj_toilet_n2['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_n2;
                    obj_toilet_n2['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_n2;
                    obj_toilet_n2['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_n2;
                    obj_toilet_n2['sims_obsprog_code_tag_parameter_frequency_code'] = $scope.edt.sims_student_toilet_training_Number2_frequency;
                    obj_toilet_n2['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_toilet_training_Number2_Remark;
                    obj_toilet_n2['sims_student_parent_comment'] = $scope.edt.sims_student_toilet_n2_parent_Remark,
                    obj_toilet_n2['sims_student_teacher_comment'] = $scope.edt.sims_student_toilet_n2_teacher_Remark,
                    t.push(obj_toilet_n2);

                    var obj_toilet_ask = angular.copy(obj);
                    obj_toilet_ask['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_ask;
                    obj_toilet_ask['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_ask;
                    obj_toilet_ask['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_ask;
                    obj_toilet_ask['sims_obsprog_code_tag_parameter_frequency_code'] = $scope.edt.sims_student_toilet_training_Askedtogo_frequency;
                    obj_toilet_ask['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_toilet_training_Askedtogo_Remark;
                    obj_toilet_ask['sims_student_parent_comment'] = $scope.edt.sims_student_toilet_ask_parent_Remark,
                    obj_toilet_ask['sims_student_teacher_comment'] = $scope.edt.sims_student_toilet_ask_teacher_Remark,
                    t.push(obj_toilet_ask);



                    var obj_fluid_milk = angular.copy(obj);
                    obj_fluid_milk['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_milk;
                    obj_fluid_milk['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_milk;
                    obj_fluid_milk['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_milk;
                    obj_fluid_milk['sims_obsprog_code_tag_parameter_frequency_code'] = $scope.edt.sims_student_fluid_intake_Milk_frequency;
                    obj_fluid_milk['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_fluid_intake_Milk_qty;
                    obj_fluid_milk['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_fluid_intake_Milk_remark;
                    obj_fluid_milk['sims_student_parent_comment'] = $scope.edt.sims_student_fluid_milk_parent_Remark,
                    obj_fluid_milk['sims_student_teacher_comment'] = $scope.edt.sims_student_fluid_milk_teacher_Remark,
                    t.push(obj_fluid_milk);

                    var obj_fluid_water = angular.copy(obj);
                    obj_fluid_water['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_water;
                    obj_fluid_water['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_water;
                    obj_fluid_water['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_water;
                    obj_fluid_water['sims_obsprog_code_tag_parameter_frequency_code'] = $scope.edt.sims_student_fluid_intake_water_frequency;
                    obj_fluid_water['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_fluid_intake_Water_qty;
                    obj_fluid_water['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_fluid_intake_Water_remark;
                    obj_fluid_water['sims_student_parent_comment'] = $scope.edt.sims_student_fluid_water_parent_Remark,
                    obj_fluid_water['sims_student_teacher_comment'] = $scope.edt.sims_student_fluid_water_teacher_Remark,
                    t.push(obj_fluid_water);

                    var obj_fluid_juice = angular.copy(obj);
                    obj_fluid_juice['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_juice;
                    obj_fluid_juice['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_juice;
                    obj_fluid_juice['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_juice;
                    obj_fluid_juice['sims_obsprog_code_tag_parameter_frequency_code'] = $scope.edt.sims_student_fluid_intake_juice_frequency;
                    obj_fluid_juice['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_fluid_intake_juice_qty;
                    obj_fluid_juice['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_fluid_intake_juice_remark;
                    obj_fluid_juice['sims_student_parent_comment'] = $scope.edt.sims_student_fluid_juice_parent_Remark,
                    obj_fluid_juice['sims_student_teacher_comment'] = $scope.edt.sims_student_fluid_juice_teacher_Remark,
                    t.push(obj_fluid_juice);

                    var obj_food_mor = angular.copy(obj);
                    obj_food_mor['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_mor;
                    obj_food_mor['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_mor;
                    obj_food_mor['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_mor;
                    obj_food_mor['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_food_Morning_snack_quantity;
                    obj_food_mor['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_food_Morning_snack_remark;
                    obj_food_mor['sims_student_parent_comment'] = $scope.edt.sims_student_food_mor_parent_Remark,
                    obj_food_mor['sims_student_teacher_comment'] = $scope.edt.sims_student_food_mor_teacher_Remark,
                    t.push(obj_food_mor);

                    var obj_food_lunch = angular.copy(obj);
                    obj_food_lunch['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_lunch;
                    obj_food_lunch['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_lunch;
                    obj_food_lunch['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_lunch;
                    obj_food_lunch['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_food_Lunch_quantity;
                    obj_food_lunch['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_food_Lunch_remark;
                    obj_food_lunch['sims_student_parent_comment'] = $scope.edt.sims_student_food_lunch_parent_Remark,
                    obj_food_lunch['sims_student_teacher_comment'] = $scope.edt.sims_student_food_lunch_teacher_Remark,
                    t.push(obj_food_lunch);

                    var obj_food_aft = angular.copy(obj);
                    obj_food_aft['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_aft;
                    obj_food_aft['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_aft;
                    obj_food_aft['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_aft;
                    obj_food_aft['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_food_Afternoon_snack_quantity;
                    obj_food_aft['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_food_Afternoon_snack_remark;
                    obj_food_aft['sims_student_parent_comment'] = $scope.edt.sims_student_food_aft_parent_Remark,
                    obj_food_aft['sims_student_teacher_comment'] = $scope.edt.sims_student_food_aft_teacher_Remark,
                    t.push(obj_food_aft);

                    var obj_sleep = angular.copy(obj);
                    obj_sleep['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_sleep;
                    obj_sleep['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_sleep;
                    obj_sleep['sims_obsprog_subcode2'] = $scope.edt.sims_obsprog_subcode2_sleep;
                    obj_sleep['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_slep_time;
                    t.push(obj_sleep);


                    var obj_watch_wake = angular.copy(obj);
                    obj_watch_wake['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_wakeup;
                    obj_watch_wake['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_wakeup;
                    obj_watch_wake['sims_obsprog_subcode2'] = $scope.edt.sims_obsprog_subcode2_wakeup;
                    obj_watch_wake['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_wake_up_time;
                    t.push(obj_watch_wake);

                    var obj_diaper = angular.copy(obj);
                    obj_diaper['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_diaper;
                    obj_diaper['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_diaper;
                    obj_diaper['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_diaper;
                    obj_diaper['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_please_provide_diaper_qty;
                    obj_diaper['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_please_provide_diaper_remark;
                    obj_diaper['sims_student_parent_comment'] = $scope.edt.sims_student_diaper_parent_Remark,
                    obj_diaper['sims_student_teacher_comment'] = $scope.edt.sims_student_diaper_teacher_Remark,
                    t.push(obj_diaper);

                    var obj_clothing = angular.copy(obj);
                    obj_clothing['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_clothing;
                    obj_clothing['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_clothing;
                    obj_clothing['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_clothing;
                    obj_clothing['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_please_provide_Clothing_qty;
                    obj_clothing['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_please_provide_Wet_Clothing_remark;
                    obj_clothing['sims_student_parent_comment'] = $scope.edt.sims_student_clothing_parent_Remark,
                    obj_clothing['sims_student_teacher_comment'] = $scope.edt.sims_student_clothing_teacher_Remark,
                    t.push(obj_clothing);

                    var obj_wetwipes = angular.copy(obj);
                    obj_wetwipes['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_wetwipes;
                    obj_wetwipes['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_wetwipes;
                    obj_wetwipes['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_wetwipes;
                    obj_wetwipes['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_please_provide_Wet_wipes_qty;
                    obj_wetwipes['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_please_provide_Wet_wipes_remark;
                    obj_wetwipes['sims_student_parent_comment'] = $scope.edt.sims_student_wet_wipes_parent_Remark,
                    obj_wetwipes['sims_student_teacher_comment'] = $scope.edt.sims_student_wet_wipes_teacher_Remark,
                    t.push(obj_wetwipes);

                    var obj_formula = angular.copy(obj);
                    obj_formula['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_formula;
                    obj_formula['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_formula;
                    obj_formula['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_formula;
                    obj_formula['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_please_provide_Formula_qty;
                    obj_formula['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_please_provide_Wet_Formula_remark;
                    obj_formula['sims_student_parent_comment'] = $scope.edt.sims_student_formula_parent_Remark,
                    obj_formula['sims_student_teacher_comment'] = $scope.edt.sims_student_formula_teacher_Remark,
                    t.push(obj_formula);

                    var obj_other = angular.copy(obj);
                    obj_other['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_others;
                    obj_other['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_others;
                    obj_other['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_others;
                    obj_other['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_please_provide_Others_qty;
                    obj_other['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_please_provide_Wet_Others_remark;
                    obj_other['sims_student_parent_comment'] = $scope.edt.sims_student_other_parent_Remark,
                    obj_other['sims_student_teacher_comment'] = $scope.edt.sims_student_other_teacher_Remark,
                    t.push(obj_other);

                  
                    var t1 = [];
                    var obj1 = {
                        opr1: '9',
                        sims_academic_year1: $scope.datasend1[0].sims_academic_year,
                        sims_enroll_number1: $scope.datasend1[0].sims_enroll_number,
                        sims_cur_code1: $scope.datasend1[0].sims_cur_code,
                        sims_grade_code1: $scope.datasend1[0].sims_grade_code,
                        sims_section_code1: $scope.datasend1[0].sims_section_code,
                        mom_start_date1: $scope.mom_start_date,
                        sims_student_created_by1: $rootScope.globals.currentUser.username,
                        sims_obsprog_incidence_desc: $scope.edt.sims_incidence_desc,
                        sims_obsprog_incidence_location: $scope.edt.sims_incidence_location,
                        sims_obsprog_incidence_parent_remark: $scope.edt.sims_incidence_parent_comment,
                        sims_obsprog_incidence_teacher_remark: $scope.edt.sims_incidence_teacher_comment,
                    }

                    t1.push(obj1);

                   
                  

                    $http.post(ENV.apiUrl + "api/DailyReport_new/CUDDetails", t).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {

                            $http.post(ENV.apiUrl + "api/DailyReport_new/CUDDetails1", t1).then(function (msg) {
                                $scope.msg2 = msg.data;

                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });

                                    if ($scope.temp.sims_homeroom_code != '') {
                                        $scope.home_report(); 
                                    } else {
                                        $scope.daily_report();
                                    }
                                    //$scope.daily_report();
                                    $scope.back_date();
                                }
                                else {
                                    swal({ text: "Record Not Inserted " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                                    $scope.back_date();
                                }

                            });

                        }
                        else {
                            swal({ text: "Record Not Inserted " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.back_date();
                        }

                    });




                   
                    datasend = [];
                    $scope.table = true;
                    $scope.display = true;
                    $scope.add_btn = false;
                    $scope.$update_btn = false;
                    $scope.edt = {};

                }

            }




            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.report_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            var data_send = [];
            var data1 = [];

            $scope.show_incidenc_remark = function () {
                debugger;
                if ($scope.edt.sims_student_any_incidence = 'Yes') {
                    $scope.any_incidence = true;
                }
                else {
                    $scope.any_incidence = false;
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }





            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            //$scope.colsvis = false;

            $scope.back_date = function () {
                debugger;
                $scope.display = true;
                $scope.add_btn = false;
                $scope.edt = {};
                $scope.datasend1 = [];

            }

            $scope.reset_data = function () {
                debugger;
                // $scope.temp.sims_grade_code = '';

               
                try {
                    $('#grade_box').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#section_box').multipleSelect('uncheckAll');
                } catch (e) {

                    try {
                        $('#Select6').multipleSelect('uncheckAll');
                    } catch (e) {

                    }

                }
                $scope.edt.sims_homeroom_code = '';
                $scope.pager = false;
                $scope.filteredTodos = [];




            }

            //KISHOR

            $scope.add_details = function (str) {

                debugger;

                $scope.edt["mom_start_date"] = $scope.sdate;
                //$scope.edt.mom_start_date = $scope.sdate;
                $scope.datasend1 = [];
                $scope.data = [];
                 
                
                console.log(str);

                $scope.edt.sims_enroll_number = str.sims_enroll_number;
                $scope.edt.sims_grade_name_en = str.sims_grade_name_en;
                $scope.edt.sims_section_name_en = str.sims_section_name_en;
                $scope.edt.student_name = str.student_name;
                $scope.edt.parent_name = str.parent_name;
                $scope.edt.sims_student_img = str.sims_student_img;
                $scope.edt.sims_homeroom_name = str.sims_homeroom_name;
             
                $scope.data = {
                    sims_academic_year: str.sims_academic_year,
                    sims_enroll_number: str.sims_enroll_number,
                    sims_cur_code: str.sims_cur_code,
                    sims_grade_code: str.sims_grade_code,
                    sims_section_code: str.sims_section_code,
                    HomeroomCode: str.homeroomCode
                }

                $scope.datasend1.push($scope.data);

                //$scope.temp = {};

                if (parseInt(str.flag) > 0) {
                    //update

                    var t = [];

                    swal({
                        title: '',
                        text: "Details are Present!!!Do you want to update?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            $scope.display = false;
                            $scope.add_btn = true;
                            //update
                            $scope.update_btn = true;
                            $scope.save_btn = false;
                            debugger;
                            $http.get(ENV.apiUrl + "api/DailyReport_new/geteditedData?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&enroll_number=" + $scope.data.sims_enroll_number + "&date=" + $scope.mom_start_date).then(function (res1) {
                                debugger;

                                if (res1.data.length > 0) {
                                    for (var i = 0; i < res1.data.length; i++) {
                                        $scope.edt.sims_incidence_desc = res1.data[i].sims_obsprog_incidence_desc;
                                        $scope.edt.sims_incidence_location = res1.data[i].sims_obsprog_incidence_location;
                                        $scope.edt.sims_incidence_teacher_comment = res1.data[i].sims_obsprog_incidence_teacher_remark;
                                        $scope.edt.sims_incidence_parent_comment = res1.data[i].sims_obsprog_incidence_parent_remark;
                                        if (res1.data[i].sims_obsprog_subcode1 == 'Fee') {
                                            $scope.edt.sims_obsprog_code_feel = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_subcode1_feel = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_student_feeling = res1.data[i].sims_obsprog_subcode2;
                                            $scope.edt.sims_feelingt_parent_comment = res1.data[i].sims_obsprog_parent_remark;
                                            $scope.edt.sims_feeling_teacher_comment = res1.data[i].sims_obsprog_teacher_remark;
                                        }

                                        if (res1.data[i].sims_obsprog_subcode1 == 'ACT') {
                                            $scope.edt.sims_obsprog_code_activity = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_subcode1_activity = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_student_activity = res1.data[i].sims_obsprog_subcode2;
                                            $scope.edt.sims_activity_parent_comment = res1.data[i].sims_obsprog_parent_remark;
                                            $scope.edt.sims_activity_teacher_comment = res1.data[i].sims_obsprog_teacher_remark;

                                            
                                            t.push(res1.data[i].sims_obsprog_subcode2)
                                           
                                        }

                                        if (res1.data[i].sims_obsprog_subcode1 == 'NAP' && res1.data[i].sims_obsprog_code_tag_code == 'DRY') {
                                            $scope.edt.sims_obsprog_code_dry = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_code_tag_code_dry = res1.data[i].sims_obsprog_code_tag_code;
                                            $scope.edt.sims_obsprog_subcode1_dry = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_student_nappy_changing_dry_frequency = res1.data[i].sims_obsprog_code_tag_parameter_frequency_code;
                                            $scope.edt.sims_student_nappy_changing_dry_Remark = res1.data[i].sims_obsprog_code_tag_remark;
                                            $scope.edt.sims_student_nappy_changing_dry_parent_Remark = res1.data[i].sims_obsprog_parent_remark;
                                            $scope.edt.sims_student_nappy_changing_dry_teacher_Remark = res1.data[i].sims_obsprog_teacher_remark;
                                        }

                                        if (res1.data[i].sims_obsprog_subcode1 == 'NAP' && res1.data[i].sims_obsprog_code_tag_code == 'WET') {
                                            $scope.edt.sims_obsprog_code_wet = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_code_tag_code_wet = res1.data[i].sims_obsprog_code_tag_code;
                                            $scope.edt.sims_obsprog_subcode1_wet = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_student_nappy_changing_wet_frequency = res1.data[i].sims_obsprog_code_tag_parameter_frequency_code;
                                            $scope.edt.sims_student_nappy_changing_wet_Remark = res1.data[i].sims_obsprog_code_tag_remark;
                                            $scope.edt.sims_student_nappy_changing_wet_teacher_Remark = res1.data[i].sims_obsprog_teacher_remark;
                                            $scope.edt.sims_student_nappy_changing_wet_parent_Remark = res1.data[i].sims_obsprog_parent_remark;
                                        }
                                        if (res1.data[i].sims_obsprog_subcode1 == 'NAP' && res1.data[i].sims_obsprog_code_tag_code == 'BM') {
                                            $scope.edt.sims_obsprog_code_bm = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_code_tag_code_bm = res1.data[i].sims_obsprog_code_tag_code;
                                            $scope.edt.sims_obsprog_subcode1_bm = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_student_nappy_changing_bm_frequency = res1.data[i].sims_obsprog_code_tag_parameter_frequency_code;
                                            $scope.edt.sims_student_nappy_changing_bm_Remark = res1.data[i].sims_obsprog_code_tag_remark;
                                            $scope.edt.sims_student_nappy_changing_bm_teacher_Remark = res1.data[i].sims_obsprog_teacher_remark;
                                            $scope.edt.sims_student_nappy_changing_bm_parent_Remark = res1.data[i].sims_obsprog_parent_remark;
                                        }

                                        if (res1.data[i].sims_obsprog_subcode1 == 'TOI' && res1.data[i].sims_obsprog_code_tag_code == 'NO1') {
                                            $scope.edt.sims_obsprog_code_n1 = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_code_tag_code_n1 = res1.data[i].sims_obsprog_code_tag_code;
                                            $scope.edt.sims_obsprog_subcode1_n1 = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_student_toilet_training_Number1_frequency = res1.data[i].sims_obsprog_code_tag_parameter_frequency_code;
                                            $scope.edt.sims_student_toilet_training_Number1_Remark = res1.data[i].sims_obsprog_code_tag_remark;
                                            $scope.edt.sims_student_toilet_n1_teacher_Remark = res1.data[i].sims_obsprog_teacher_remark;
                                            $scope.edt.sims_student_toilet_n1_parent_Remark = res1.data[i].sims_obsprog_parent_remark;
                                        }

                                        if (res1.data[i].sims_obsprog_subcode1 == 'TOI' && res1.data[i].sims_obsprog_code_tag_code == 'NO2') {
                                            $scope.edt.sims_obsprog_code_n2 = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_code_tag_code_n2 = res1.data[i].sims_obsprog_code_tag_code;
                                            $scope.edt.sims_obsprog_subcode1_n2 = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_student_toilet_training_Number2_frequency = res1.data[i].sims_obsprog_code_tag_parameter_frequency_code;
                                            $scope.edt.sims_student_toilet_training_Number2_Remark = res1.data[i].sims_obsprog_code_tag_remark;
                                            $scope.edt.sims_student_toilet_n2_teacher_Remark = res1.data[i].sims_obsprog_teacher_remark;
                                            $scope.edt.sims_student_toilet_n2_parent_Remark = res1.data[i].sims_obsprog_parent_remark;
                                        }

                                        if (res1.data[i].sims_obsprog_subcode1 == 'TOI' && res1.data[i].sims_obsprog_code_tag_code == 'ASK') {
                                            $scope.edt.sims_obsprog_code_ask = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_code_tag_code_ask = res1.data[i].sims_obsprog_code_tag_code;
                                            $scope.edt.sims_obsprog_subcode1_ask = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_student_toilet_training_Askedtogo_frequency = res1.data[i].sims_obsprog_code_tag_parameter_frequency_code;
                                            $scope.edt.sims_student_toilet_training_Askedtogo_Remark = res1.data[i].sims_obsprog_code_tag_remark;
                                            $scope.edt.sims_student_toilet_ask_teacher_Remark = res1.data[i].sims_obsprog_teacher_remark;
                                            $scope.edt.sims_student_toilet_ask_parent_Remark = res1.data[i].sims_obsprog_parent_remark;
                                        }

                                        if (res1.data[i].sims_obsprog_subcode1 == 'FLU' && res1.data[i].sims_obsprog_code_tag_code == 'MIL') {
                                            $scope.edt.sims_obsprog_code_milk = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_code_tag_code_milk = res1.data[i].sims_obsprog_code_tag_code;
                                            $scope.edt.sims_obsprog_subcode1_milk = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_student_fluid_intake_Milk_qty = res1.data[i].sims_obsprog_code_tag_parameter_frequency_quantity_code
                                            $scope.edt.sims_student_fluid_intake_Milk_frequency = res1.data[i].sims_obsprog_code_tag_parameter_frequency_code;
                                            $scope.edt.sims_student_fluid_intake_Milk_remark = res1.data[i].sims_obsprog_code_tag_remark;
                                            $scope.edt.sims_student_fluid_milk_teacher_Remark = res1.data[i].sims_obsprog_teacher_remark;
                                            $scope.edt.sims_student_fluid_milk_parent_Remark = res1.data[i].sims_obsprog_parent_remark;
                                        }

                                        if (res1.data[i].sims_obsprog_subcode1 == 'FLU' && res1.data[i].sims_obsprog_code_tag_code == 'WAT') {
                                            $scope.edt.sims_obsprog_code_water = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_code_tag_code_water = res1.data[i].sims_obsprog_code_tag_code;
                                            $scope.edt.sims_obsprog_subcode1_water = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_student_fluid_intake_Water_qty = res1.data[i].sims_obsprog_code_tag_parameter_frequency_quantity_code
                                            $scope.edt.sims_student_fluid_intake_water_frequency = res1.data[i].sims_obsprog_code_tag_parameter_frequency_code;
                                            $scope.edt.sims_student_fluid_intake_Water_remark = res1.data[i].sims_obsprog_code_tag_remark;
                                            $scope.edt.sims_student_fluid_water_teacher_Remark = res1.data[i].sims_obsprog_teacher_remark;
                                            $scope.edt.sims_student_fluid_water_parent_Remark = res1.data[i].sims_obsprog_parent_remark;
                                        }

                                        if (res1.data[i].sims_obsprog_subcode1 == 'FLU' && res1.data[i].sims_obsprog_code_tag_code == 'JUI') {
                                            $scope.edt.sims_obsprog_code_juice = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_code_tag_code_juice = res1.data[i].sims_obsprog_code_tag_code;
                                            $scope.edt.sims_obsprog_subcode1_juice = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_student_fluid_intake_juice_qty = res1.data[i].sims_obsprog_code_tag_parameter_frequency_quantity_code
                                            $scope.edt.sims_student_fluid_intake_juice_frequency = res1.data[i].sims_obsprog_code_tag_parameter_frequency_code;
                                            $scope.edt.sims_student_fluid_intake_juice_remark = res1.data[i].sims_obsprog_code_tag_remark;
                                            $scope.edt.sims_student_fluid_juice_teacher_Remark = res1.data[i].sims_obsprog_teacher_remark;
                                            $scope.edt.sims_student_fluid_juice_parent_Remark = res1.data[i].sims_obsprog_parent_remark;
                                        }

                                        if (res1.data[i].sims_obsprog_subcode1 == 'FOO' && res1.data[i].sims_obsprog_code_tag_code == 'MOR') {
                                            $scope.edt.sims_obsprog_code_mor = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_code_tag_code_mor = res1.data[i].sims_obsprog_code_tag_code;
                                            $scope.edt.sims_obsprog_subcode1_mor = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_student_food_Morning_snack_quantity = res1.data[i].sims_obsprog_code_tag_parameter_frequency_quantity_code
                                            $scope.edt.sims_student_food_Morning_snack_remark = res1.data[i].sims_obsprog_code_tag_remark;
                                            $scope.edt.sims_student_food_mor_teacher_Remark = res1.data[i].sims_obsprog_teacher_remark;
                                            $scope.edt.sims_student_food_mor_parent_Remark = res1.data[i].sims_obsprog_parent_remark;
                                        }

                                        if (res1.data[i].sims_obsprog_subcode1 == 'FOO' && res1.data[i].sims_obsprog_code_tag_code == 'LUN') {
                                            $scope.edt.sims_obsprog_code_lunch = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_code_tag_code_lunch = res1.data[i].sims_obsprog_code_tag_code;
                                            $scope.edt.sims_obsprog_subcode1_lunch = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_student_food_Lunch_quantity = res1.data[i].sims_obsprog_code_tag_parameter_frequency_quantity_code
                                            $scope.edt.sims_student_food_Lunch_remark = res1.data[i].sims_obsprog_code_tag_remark;
                                            $scope.edt.sims_student_food_lunch_teacher_Remark = res1.data[i].sims_obsprog_teacher_remark;
                                            $scope.edt.sims_student_food_lunch_parent_Remark = res1.data[i].sims_obsprog_parent_remark;
                                        }

                                        if (res1.data[i].sims_obsprog_subcode1 == 'FOO' && res1.data[i].sims_obsprog_code_tag_code == 'AFT') {
                                            $scope.edt.sims_obsprog_code_aft = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_code_tag_code_aft = res1.data[i].sims_obsprog_code_tag_code;
                                            $scope.edt.sims_obsprog_subcode1_aft = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_student_food_Afternoon_snack_quantity = res1.data[i].sims_obsprog_code_tag_parameter_frequency_quantity_code
                                            $scope.edt.sims_student_food_Afternoon_snack_remark = res1.data[i].sims_obsprog_code_tag_remark;
                                            $scope.edt.sims_student_food_aft_teacher_Remark = res1.data[i].sims_obsprog_teacher_remark;
                                            $scope.edt.sims_student_food_aft_parent_Remark = res1.data[i].sims_obsprog_parent_remark;
                                        }

                                        if (res1.data[i].sims_obsprog_subcode1 == 'SLE' && res1.data[i].sims_obsprog_subcode2 == '01') {
                                            $scope.edt.sims_obsprog_code_sleep = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_subcode1_sleep = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_obsprog_subcode2_sleep = res1.data[i].sims_obsprog_subcode2;
                                            $scope.edt.sims_student_slep_time = res1.data[i].sims_obsprog_code_tag_parameter_frequency_quantity_code
                                        }

                                        if (res1.data[i].sims_obsprog_subcode1 == 'SLE' && res1.data[i].sims_obsprog_subcode2 == '02') {
                                            $scope.edt.sims_obsprog_code_wakeup = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_subcode1_wakeup = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_obsprog_subcode2_wakeup = res1.data[i].sims_obsprog_subcode2;
                                            $scope.edt.sims_student_wake_up_time = res1.data[i].sims_obsprog_code_tag_parameter_frequency_quantity_code
                                        }
                                        if (res1.data[i].sims_obsprog_subcode1 == 'DIA' && res1.data[i].sims_obsprog_code_tag_code == 'DIA') {
                                            $scope.edt.sims_obsprog_code_diaper = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_code_tag_code_diaper = res1.data[i].sims_obsprog_code_tag_code;
                                            $scope.edt.sims_obsprog_subcode1_diaper = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_student_please_provide_diaper_qty = res1.data[i].sims_obsprog_code_tag_parameter_frequency_quantity_code
                                            $scope.edt.sims_student_please_provide_diaper_remark = res1.data[i].sims_obsprog_code_tag_remark;
                                            $scope.edt.sims_student_diaper_teacher_Remark = res1.data[i].sims_obsprog_teacher_remark;
                                            $scope.edt.sims_student_diaper_parent_Remark = res1.data[i].sims_obsprog_parent_remark;
                                        }
                                        if (res1.data[i].sims_obsprog_subcode1 == 'CLO' && res1.data[i].sims_obsprog_code_tag_code == 'CLO') {
                                            $scope.edt.sims_obsprog_code_clothing = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_code_tag_code_clothing = res1.data[i].sims_obsprog_code_tag_code;
                                            $scope.edt.sims_obsprog_subcode1_clothing = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_student_please_provide_Clothing_qty = res1.data[i].sims_obsprog_code_tag_parameter_frequency_quantity_code
                                            $scope.edt.sims_student_please_provide_Wet_Clothing_remark = res1.data[i].sims_obsprog_code_tag_remark;
                                            $scope.edt.sims_student_clothing_teacher_Remark = res1.data[i].sims_obsprog_teacher_remark;
                                            $scope.edt.sims_student_clothing_parent_Remark = res1.data[i].sims_obsprog_parent_remark;
                                        }

                                        if (res1.data[i].sims_obsprog_subcode1 == 'WET' && res1.data[i].sims_obsprog_code_tag_code == 'WET') {
                                            $scope.edt.sims_obsprog_code_wetwipes = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_code_tag_code_wetwipes = res1.data[i].sims_obsprog_code_tag_code;
                                            $scope.edt.sims_obsprog_subcode1_wetwipes = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_student_please_provide_Wet_wipes_qty = res1.data[i].sims_obsprog_code_tag_parameter_frequency_quantity_code
                                            $scope.edt.sims_student_please_provide_Wet_wipes_remark = res1.data[i].sims_obsprog_code_tag_remark;
                                            $scope.edt.sims_student_wet_wipes_teacher_Remark = res1.data[i].sims_obsprog_teacher_remark;
                                            $scope.edt.sims_student_wet_wipes_parent_Remark = res1.data[i].sims_obsprog_parent_remark;
                                        }
                                        if (res1.data[i].sims_obsprog_subcode1 == 'FOR' && res1.data[i].sims_obsprog_code_tag_code == 'FOR') {
                                            $scope.edt.sims_obsprog_code_formula = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_code_tag_code_formula = res1.data[i].sims_obsprog_code_tag_code;
                                            $scope.edt.sims_obsprog_subcode1_formula = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_student_please_provide_Formula_qty = res1.data[i].sims_obsprog_code_tag_parameter_frequency_quantity_code
                                            $scope.edt.sims_student_please_provide_Wet_Formula_remark = res1.data[i].sims_obsprog_code_tag_remark;
                                            $scope.edt.sims_student_formula_teacher_Remark = res1.data[i].sims_obsprog_teacher_remark;
                                            $scope.edt.sims_student_formula_parent_Remark = res1.data[i].sims_obsprog_parent_remark;
                                        }
                                        if (res1.data[i].sims_obsprog_subcode1 == 'OTH' && res1.data[i].sims_obsprog_code_tag_code == 'OTH') {
                                            $scope.edt.sims_obsprog_code_others = res1.data[i].sims_obsprog_code;
                                            $scope.edt.sims_obsprog_code_tag_code_others = res1.data[i].sims_obsprog_code_tag_code;
                                            $scope.edt.sims_obsprog_subcode1_others = res1.data[i].sims_obsprog_subcode1;
                                            $scope.edt.sims_student_please_provide_Others_qty = res1.data[i].sims_obsprog_code_tag_parameter_frequency_quantity_code
                                            $scope.edt.sims_student_please_provide_Wet_Others_remark = res1.data[i].sims_obsprog_code_tag_remark;
                                            $scope.edt.sims_student_other_teacher_Remark = res1.data[i].sims_obsprog_teacher_remark;
                                            $scope.edt.sims_student_other_parent_Remark = res1.data[i].sims_obsprog_parent_remark;
                                        }


                                    }

                                }

                                try {
                                    $scope.edt.sims_student_activity = t;
                                    $("#Select6").multipleSelect("setSelects", t);
                                }
                                catch (e) {
                                }
                                console.log(res1.data[i]);
                            });


                        }
                    })


                }
                else {
                    //insert
                    $scope.display = false;
                    $scope.add_btn = true;
                    $scope.update_btn = false;
                    $scope.save_btn = true;
                    try {
                        $('#Select6').multipleSelect('uncheckAll');
                    } catch (e) {

                    }
                }

            }


            $scope.getReport = function (str) {
                debugger
                console.log(str);
                var t = moment($scope.mom_start_date, 'DD-MM-YYYY').format('YYYY-MM-DD')

                var data = {

                    location: 'Sims.SMR150LWG',
                    parameter: {
                        cur_code: str.sims_cur_code,
                        acad_year: str.sims_academic_year,
                        grade_code: str.sims_grade_code,
                        section_code: str.sims_section_code,
                        enroll_number: str.sims_enroll_number,
                        date: t,

                    },
                    state: 'main.Dfrn01',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                console.log("data", data);
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter');
            }

            var datasend = [];
            $scope.update = function (Myform) {
                debugger
                console.log($scope.datasend1);
                if (Myform) {
                    debugger;
                    var data = $scope.edt;
                    data.opr = 'U';

                    //$scope.datasend1.push($scope.data);
                    data.sims_academic_year = $scope.datasend1[0].sims_academic_year;
                    data.sims_enroll_number = $scope.datasend1[0].sims_enroll_number;
                    data.sims_cur_code = $scope.datasend1[0].sims_cur_code;
                    data.sims_grade_code = $scope.datasend1[0].sims_grade_code;
                    data.sims_section_code = $scope.datasend1[0].sims_section_code;
                    data.mom_start_date = $scope.mom_start_date;
                    data.sims_student_created_by = $rootScope.globals.currentUser.username;
                    datasend.push(data);


                    /// split

                    var t = [];
                    var obj = {

                        opr: 'I',
                        sims_academic_year: $scope.datasend1[0].sims_academic_year,
                        sims_enroll_number: $scope.datasend1[0].sims_enroll_number,
                        sims_cur_code: $scope.datasend1[0].sims_cur_code,
                        sims_grade_code: $scope.datasend1[0].sims_grade_code,
                        sims_section_code: $scope.datasend1[0].sims_section_code,
                        mom_start_date: $scope.mom_start_date,
                        sims_student_created_by: $rootScope.globals.currentUser.username,

                    }

                    var obj_feel = angular.copy(obj);
                    obj_feel['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_feel;
                    obj_feel['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_feel;
                    obj_feel['sims_obsprog_subcode2'] = $scope.edt.sims_student_feeling;
                    obj_feel['sims_student_parent_comment'] = $scope.edt.sims_feelingt_parent_comment,
                    obj_feel['sims_student_teacher_comment'] = $scope.edt.sims_feeling_teacher_comment,
                    //obj_feel['sims_obsprog_subcode3'] = '';
                    //obj_feel['sims_obsprog_code_tag_code'] = '';
                    //obj_feel['sims_obsprog_code_tag_parameter_frequency_code'] = '';
                    //obj_feel['sims_obsprog_code_tag_parameter_frequency_description'] = '';
                    //obj_feel['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = '';
                    //obj_feel['sims_obsprog_code_tag_parameter_frequency_quantity_code_description'] = null;
                    t.push(obj_feel);

                    debugger;
                    try {
                        for (var i = 0; i < $scope.edt.sims_student_activity.length; i++) {
                            var obj_activity = angular.copy(obj);
                            obj_activity['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_activity;
                            obj_activity['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_activity;
                            obj_activity['sims_obsprog_subcode2'] = $scope.edt.sims_student_activity[i]
                            obj_activity['sims_student_parent_comment'] = $scope.edt.sims_activity_parent_comment,
                            obj_activity['sims_student_teacher_comment'] = $scope.edt.sims_activity_teacher_comment,
                            t.push(obj_activity);
                        }
                    } catch (e) {

                    }
                    //var obj_activity = angular.copy(obj);
                    //obj_activity['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_activity;
                    //obj_activity['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_activity;
                    //obj_activity['sims_obsprog_subcode2'] = $scope.edt.sims_student_activity
                    //obj_activity['sims_student_parent_comment'] = $scope.edt.sims_activity_parent_comment,
                    //obj_activity['sims_student_teacher_comment'] = $scope.edt.sims_activity_teacher_comment,
                    //t.push(obj_activity);

                    var obj_nappy_dry = angular.copy(obj);
                    obj_nappy_dry['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_dry;
                    obj_nappy_dry['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_dry;
                    obj_nappy_dry['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_dry;
                    obj_nappy_dry['sims_obsprog_code_tag_parameter_frequency_code'] = $scope.edt.sims_student_nappy_changing_dry_frequency;
                    obj_nappy_dry['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_nappy_changing_dry_Remark;
                    obj_nappy_dry['sims_student_parent_comment'] = $scope.edt.sims_student_nappy_changing_dry_parent_Remark,
                    obj_nappy_dry['sims_student_teacher_comment'] = $scope.edt.sims_student_nappy_changing_dry_teacher_Remark,
                    //obj_activity['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = '';
                    t.push(obj_nappy_dry);

                    var obj_nappy_wet = angular.copy(obj);
                    obj_nappy_wet['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_wet;
                    obj_nappy_wet['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_wet;
                    obj_nappy_wet['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_wet;
                    obj_nappy_wet['sims_obsprog_code_tag_parameter_frequency_code'] = $scope.edt.sims_student_nappy_changing_wet_frequency;
                    obj_nappy_wet['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_nappy_changing_wet_Remark;
                    obj_nappy_wet['sims_student_parent_comment'] = $scope.edt.sims_student_nappy_changing_wet_parent_Remark,
                    obj_nappy_wet['sims_student_teacher_comment'] = $scope.edt.sims_student_nappy_changing_wet_teacher_Remark,
                    t.push(obj_nappy_wet);

                    var obj_nappy_bm = angular.copy(obj);
                    obj_nappy_bm['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_bm;
                    obj_nappy_bm['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_bm;
                    obj_nappy_bm['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_bm;
                    obj_nappy_bm['sims_obsprog_code_tag_parameter_frequency_code'] = $scope.edt.sims_student_nappy_changing_bm_frequency;
                    obj_nappy_bm['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_nappy_changing_bm_Remark;
                    obj_nappy_bm['sims_student_parent_comment'] = $scope.edt.sims_student_nappy_changing_bm_parent_Remark,
                    obj_nappy_bm['sims_student_teacher_comment'] = $scope.edt.sims_student_nappy_changing_bm_teacher_Remark,
                    t.push(obj_nappy_bm);

                    var obj_toilet_n1 = angular.copy(obj);
                    obj_toilet_n1['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_n1;
                    obj_toilet_n1['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_n1;
                    obj_toilet_n1['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_n1;
                    obj_toilet_n1['sims_obsprog_code_tag_parameter_frequency_code'] = $scope.edt.sims_student_toilet_training_Number1_frequency;
                    obj_toilet_n1['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_toilet_training_Number1_Remark;
                    obj_toilet_n1['sims_student_parent_comment'] = $scope.edt.sims_student_toilet_n1_parent_Remark,
                    obj_toilet_n1['sims_student_teacher_comment'] = $scope.edt.sims_student_toilet_n1_teacher_Remark,
                    t.push(obj_toilet_n1);

                    var obj_toilet_n2 = angular.copy(obj);
                    obj_toilet_n2['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_n2;
                    obj_toilet_n2['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_n2;
                    obj_toilet_n2['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_n2;
                    obj_toilet_n2['sims_obsprog_code_tag_parameter_frequency_code'] = $scope.edt.sims_student_toilet_training_Number2_frequency;
                    obj_toilet_n2['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_toilet_training_Number2_Remark;
                    obj_toilet_n2['sims_student_parent_comment'] = $scope.edt.sims_student_toilet_n2_parent_Remark,
                    obj_toilet_n2['sims_student_teacher_comment'] = $scope.edt.sims_student_toilet_n2_teacher_Remark,
                    t.push(obj_toilet_n2);

                    var obj_toilet_ask = angular.copy(obj);
                    obj_toilet_ask['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_ask;
                    obj_toilet_ask['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_ask;
                    obj_toilet_ask['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_ask;
                    obj_toilet_ask['sims_obsprog_code_tag_parameter_frequency_code'] = $scope.edt.sims_student_toilet_training_Askedtogo_frequency;
                    obj_toilet_ask['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_toilet_training_Askedtogo_Remark;
                    obj_toilet_ask['sims_student_parent_comment'] = $scope.edt.sims_student_toilet_ask_parent_Remark,
                    obj_toilet_ask['sims_student_teacher_comment'] = $scope.edt.sims_student_toilet_ask_teacher_Remark,
                    t.push(obj_toilet_ask);



                    var obj_fluid_milk = angular.copy(obj);
                    obj_fluid_milk['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_milk;
                    obj_fluid_milk['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_milk;
                    obj_fluid_milk['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_milk;
                    obj_fluid_milk['sims_obsprog_code_tag_parameter_frequency_code'] = $scope.edt.sims_student_fluid_intake_Milk_frequency;
                    obj_fluid_milk['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_fluid_intake_Milk_qty;
                    obj_fluid_milk['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_fluid_intake_Milk_remark;
                    obj_fluid_milk['sims_student_parent_comment'] = $scope.edt.sims_student_fluid_milk_parent_Remark,
                    obj_fluid_milk['sims_student_teacher_comment'] = $scope.edt.sims_student_fluid_milk_teacher_Remark,
                    t.push(obj_fluid_milk);

                    var obj_fluid_water = angular.copy(obj);
                    obj_fluid_water['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_water;
                    obj_fluid_water['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_water;
                    obj_fluid_water['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_water;
                    obj_fluid_water['sims_obsprog_code_tag_parameter_frequency_code'] = $scope.edt.sims_student_fluid_intake_water_frequency;
                    obj_fluid_water['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_fluid_intake_Water_qty;
                    obj_fluid_water['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_fluid_intake_Water_remark;
                    obj_fluid_water['sims_student_parent_comment'] = $scope.edt.sims_student_fluid_water_parent_Remark,
                    obj_fluid_water['sims_student_teacher_comment'] = $scope.edt.sims_student_fluid_water_teacher_Remark,
                    t.push(obj_fluid_water);

                    var obj_fluid_juice = angular.copy(obj);
                    obj_fluid_juice['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_juice;
                    obj_fluid_juice['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_juice;
                    obj_fluid_juice['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_juice;
                    obj_fluid_juice['sims_obsprog_code_tag_parameter_frequency_code'] = $scope.edt.sims_student_fluid_intake_juice_frequency;
                    obj_fluid_juice['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_fluid_intake_juice_qty;
                    obj_fluid_juice['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_fluid_intake_juice_remark;
                    obj_fluid_juice['sims_student_parent_comment'] = $scope.edt.sims_student_fluid_juice_parent_Remark,
                    obj_fluid_juice['sims_student_teacher_comment'] = $scope.edt.sims_student_fluid_juice_teacher_Remark,
                    t.push(obj_fluid_juice);

                    var obj_food_mor = angular.copy(obj);
                    obj_food_mor['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_mor;
                    obj_food_mor['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_mor;
                    obj_food_mor['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_mor;
                    obj_food_mor['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_food_Morning_snack_quantity;
                    obj_food_mor['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_food_Morning_snack_remark;
                    obj_food_mor['sims_student_parent_comment'] = $scope.edt.sims_student_food_mor_parent_Remark,
                    obj_food_mor['sims_student_teacher_comment'] = $scope.edt.sims_student_food_mor_teacher_Remark,
                    t.push(obj_food_mor);

                    var obj_food_lunch = angular.copy(obj);
                    obj_food_lunch['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_lunch;
                    obj_food_lunch['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_lunch;
                    obj_food_lunch['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_lunch;
                    obj_food_lunch['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_food_Lunch_quantity;
                    obj_food_lunch['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_food_Lunch_remark;
                    obj_food_lunch['sims_student_parent_comment'] = $scope.edt.sims_student_food_lunch_parent_Remark,
                    obj_food_lunch['sims_student_teacher_comment'] = $scope.edt.sims_student_food_lunch_teacher_Remark,
                    t.push(obj_food_lunch);

                    var obj_food_aft = angular.copy(obj);
                    obj_food_aft['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_aft;
                    obj_food_aft['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_aft;
                    obj_food_aft['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_aft;
                    obj_food_aft['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_food_Afternoon_snack_quantity;
                    obj_food_aft['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_food_Afternoon_snack_remark;
                    obj_food_aft['sims_student_parent_comment'] = $scope.edt.sims_student_food_aft_parent_Remark,
                    obj_food_aft['sims_student_teacher_comment'] = $scope.edt.sims_student_food_aft_teacher_Remark,
                    t.push(obj_food_aft);

                    var obj_sleep = angular.copy(obj);
                    obj_sleep['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_sleep;
                    obj_sleep['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_sleep;
                    obj_sleep['sims_obsprog_subcode2'] = $scope.edt.sims_obsprog_subcode2_sleep;
                    obj_sleep['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_slep_time;
                    t.push(obj_sleep);


                    var obj_watch_wake = angular.copy(obj);
                    obj_watch_wake['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_wakeup;
                    obj_watch_wake['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_wakeup;
                    obj_watch_wake['sims_obsprog_subcode2'] = $scope.edt.sims_obsprog_subcode2_wakeup;
                    obj_watch_wake['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_wake_up_time;
                    t.push(obj_watch_wake);

                    var obj_diaper = angular.copy(obj);
                    obj_diaper['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_diaper;
                    obj_diaper['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_diaper;
                    obj_diaper['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_diaper;
                    obj_diaper['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_please_provide_diaper_qty;
                    obj_diaper['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_please_provide_diaper_remark;
                    obj_diaper['sims_student_parent_comment'] = $scope.edt.sims_student_diaper_parent_Remark,
                    obj_diaper['sims_student_teacher_comment'] = $scope.edt.sims_student_diaper_teacher_Remark,
                    t.push(obj_diaper);

                    var obj_clothing = angular.copy(obj);
                    obj_clothing['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_clothing;
                    obj_clothing['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_clothing;
                    obj_clothing['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_clothing;
                    obj_clothing['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_please_provide_Clothing_qty;
                    obj_clothing['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_please_provide_Wet_Clothing_remark;
                    obj_clothing['sims_student_parent_comment'] = $scope.edt.sims_student_clothing_parent_Remark,
                    obj_clothing['sims_student_teacher_comment'] = $scope.edt.sims_student_clothing_teacher_Remark,
                    t.push(obj_clothing);

                    var obj_wetwipes = angular.copy(obj);
                    obj_wetwipes['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_wetwipes;
                    obj_wetwipes['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_wetwipes;
                    obj_wetwipes['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_wetwipes;
                    obj_wetwipes['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_please_provide_Wet_wipes_qty;
                    obj_wetwipes['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_please_provide_Wet_wipes_remark;
                    obj_wetwipes['sims_student_parent_comment'] = $scope.edt.sims_student_wet_wipes_parent_Remark,
                    obj_wetwipes['sims_student_teacher_comment'] = $scope.edt.sims_student_wet_wipes_teacher_Remark,
                    t.push(obj_wetwipes);

                    var obj_formula = angular.copy(obj);
                    obj_formula['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_formula;
                    obj_formula['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_formula;
                    obj_formula['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_formula;
                    obj_formula['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_please_provide_Formula_qty;
                    obj_formula['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_please_provide_Wet_Formula_remark;
                    obj_formula['sims_student_parent_comment'] = $scope.edt.sims_student_formula_parent_Remark,
                    obj_formula['sims_student_teacher_comment'] = $scope.edt.sims_student_formula_teacher_Remark,
                    t.push(obj_formula);

                    var obj_other = angular.copy(obj);
                    obj_other['sims_obsprog_code'] = $scope.edt.sims_obsprog_code_others;
                    obj_other['sims_obsprog_subcode1'] = $scope.edt.sims_obsprog_subcode1_others;
                    obj_other['sims_obsprog_code_tag_code'] = $scope.edt.sims_obsprog_code_tag_code_others;
                    obj_other['sims_obsprog_code_tag_parameter_frequency_quantity_code'] = $scope.edt.sims_student_please_provide_Others_qty;
                    obj_other['sims_obsprog_code_tag_remark'] = $scope.edt.sims_student_please_provide_Wet_Others_remark;
                    obj_other['sims_student_parent_comment'] = $scope.edt.sims_student_other_parent_Remark,
                    obj_other['sims_student_teacher_comment'] = $scope.edt.sims_student_other_teacher_Remark,
                    t.push(obj_other);

                    var t1 = [];
                    var obj1 = {
                        opr1: '9',
                        sims_academic_year1: $scope.datasend1[0].sims_academic_year,
                        sims_enroll_number1: $scope.datasend1[0].sims_enroll_number,
                        sims_cur_code1: $scope.datasend1[0].sims_cur_code,
                        sims_grade_code1: $scope.datasend1[0].sims_grade_code,
                        sims_section_code1: $scope.datasend1[0].sims_section_code,
                        mom_start_date1: $scope.mom_start_date,
                        sims_student_created_by1: $rootScope.globals.currentUser.username,
                        sims_obsprog_incidence_desc: $scope.edt.sims_incidence_desc,
                        sims_obsprog_incidence_location: $scope.edt.sims_incidence_location,
                        sims_obsprog_incidence_parent_remark: $scope.edt.sims_incidence_parent_comment,
                        sims_obsprog_incidence_teacher_remark: $scope.edt.sims_incidence_teacher_comment,
                    }
                    t1.push(obj1);
                    debugger;
                    $http.post(ENV.apiUrl + "api/DailyReport_new/recordsd?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&sims_enroll_number=" + $scope.datasend1[0].sims_enroll_number + "&sims_obsprog_date=" + $scope.mom_start_date).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            $http.post(ENV.apiUrl + "api/DailyReport_new/CUDDetails", t).then(function (msg) {
                                $scope.msg1 = msg.data;

                                if ($scope.msg1 == true) {

                                    $http.post(ENV.apiUrl + "api/DailyReport_new/CUDDetails1", t1).then(function (msg) {
                                        $scope.msg2 = msg.data;

                                        if ($scope.msg1 == true) {
                                            swal({ text: "Record updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                                            
                                            if ($scope.edt.sims_homeroom_name == undefined) {
                                                $scope.daily_report();
                                            } else {
                                                $scope.home_report();
                                            }
                                            //$state.reload();
                                            //$scope.daily_report();
                                           // $scope.home_report();
                                            $scope.back_date();

                                            datasend = [];
                                            $scope.table = true;
                                            $scope.display = true;
                                            $scope.add_btn = false;
                                            $scope.$update_btn = false;
                                            $scope.edt = {};
                                        }
                                        else {
                                            swal({ text: "Record Not updated " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                                            $scope.back_date();
                                        }

                                    });

                                }

                            });
                        }
                    });

                    //$http.post(ENV.apiUrl + "api/DailyReport_new/delete_records?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&sims_enroll_number=" + $scope.datasend1[0].sims_enroll_number + "&sims_obsprog_date=" + $scope.mom_start_date).then(function (msg) {
                                          

                    //});

                    //$http.post(ENV.apiUrl + "api/DailyReport_new/CUDDetails", t).then(function (msg) {
                    //    $scope.msg1 = msg.data;

                    //    if ($scope.msg1 == true) {

                    //        $http.post(ENV.apiUrl + "api/DailyReport_new/CUDDetails1", t1).then(function (msg) {
                    //            $scope.msg2 = msg.data;

                    //            if ($scope.msg1 == true) {
                    //                swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                    //                $scope.daily_report();
                    //                $scope.back_date();
                    //            }
                    //            else {
                    //                swal({ text: "Record Not Updated " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                    //                $scope.back_date();
                    //            }

                    //        });

                    //    }
                    //    else {
                    //        swal({ text: "Record Not Updated " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                    //        $scope.back_date();
                    //    }

                    //});


                 

                }

            }



            $(document).ready(function () {
                $("#sleep_time_am").kendoDateTimePicker({
                    format: "hh:mm tt"
                });
            });
            $('.clockpicker').clockpicker();


        }])

})();

