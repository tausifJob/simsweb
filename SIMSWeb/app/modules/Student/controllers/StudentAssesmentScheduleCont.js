﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudentAssesmentScheduleCont',
        ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.display = true;
            $scope.grid = true;
            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.flag = true;
            var str, cnt;
            var main;
           
            var admdetails = [];
            $scope.filesize = true;
            var date = new Date();
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');
            
            $scope.obj1 = [], $scope.obj3 = [];

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 10;

            $scope.edt =
                {
                    sims_assesment_date: $scope.ddMMyyyy,
                    sims_assesment_time:"12:00 AM"
                }

             // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {
               
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;

                $scope.propertyName = propertyName;
            };

            $scope.countData = [

                  
                  { val: 10, data: 10 },
                  { val: 15, data: 15 },
                   

            ]


            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.obj2 = res.data;
                $scope.edt['curr_code'] = $scope.obj2[0].sims_attendance_cur_code;
                $scope.curr_code = $scope.obj2[0].sims_attendance_cur_code;
                $scope.getCur($scope.edt.curr_code);
            });

            $http.get(ENV.apiUrl + "api/StudentAssessmentSchedule/GetAssesmentTeacher").then(function (res) {
                $scope.teach_data = res.data;
            });

            $scope.getCur = function (cur_code)
            {
                $http.get(ENV.apiUrl + "api/common/getAllAcademicYearAss?cur_code=" + cur_code).then(function (res) {
                    $scope.obj1 = res.data;
                    console.log($scope.obj1[0].sims_academic_year);
                    $scope.edt['academic_year'] = $scope.obj1[0].sims_academic_year;
                    $scope.acad_yr = $scope.obj1[0].sims_academic_year;
                    $scope.GetGrade(cur_code, $scope.acad_yr);
                });
            }

            $scope.GetGrade = function (cur, acad_yr)
            {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + cur + "&academic_year=" + acad_yr).then(function (res) {
                    $scope.obj3 = res.data;
                    $scope.edt['grade_code'] = $scope.obj3[0].sims_grade_code;
                    $scope.grade = $scope.obj3[0].sims_grade_code;
                   
                });
            }


            $scope.getAssessmentscheduleData = function ()
            {
                $scope.filteredTodos = [];
                 
                $http.get(ENV.apiUrl + "api/StudentAssessmentSchedule/GetAssesmentSchedule?cur_code=" + $scope.edt.curr_code + "&acad_yr=" + $scope.edt.academic_year + "&grade_code=" + $scope.edt.grade_code).then(function (res) {
                        $scope.assessmentscheduleData = res.data;
                        if ($scope.assessmentscheduleData.length > 0) {
                            $scope.table = true;
                            $scope.pager = true;
                            if ($scope.countData.length > 3) {
                                $scope.countData.splice(3, 1);
                                $scope.countData.push({ val: $scope.assessmentscheduleData.length, data: 'All' })
                            }
                            else
                            {
                                $scope.countData.push({ val: $scope.assessmentscheduleData.length, data: 'All' })
                            }
                            $scope.totalItems = $scope.assessmentscheduleData.length;
                            $scope.todos = $scope.assessmentscheduleData;
                            $scope.grid = true;
                            $scope.makeTodos();
                        }
                        else {
                            $scope.table = false;
                            swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                            $scope.filteredTodos = [];
                        }

                    });
              
            }


            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                if ( str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            //check all 
            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_pros_no);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_pros_no);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.check1 = function (dash) {

                $scope.sims_admission_father_mail = dash.sims_admission_father_email;
                $scope.sims_admission_father_mobile = dash.sims_admission_father_mobile;
                $scope.sims_admission_number = dash.sims_pros_no;
                $scope.sims_assesment_date = dash.sims_assesment_date;
                $scope.sims_assesment_time = dash.sims_assesment_time;

                console.log(dash);
                admdetails = dash.sims_pros_no;
                var v = document.getElementById(dash.sims_pros_no);
                
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }

                });
                // }

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.check = function () {
                debugger;
                main = document.getElementById('mainchk');
                del = [];
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_pros_no);
                        v.checked = true;
                        del.push($scope.filteredTodos[i].sims_pros_no);
                        $('tr').addClass("row_selected");
                        // }
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sims_pros_no;
                        var v = document.getElementById(t);
                        v.checked = false;
                        del.pop(t);
                        // $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }
            }

            $scope.cancel = function ()
            {
                $scope.filteredTodos = [];
                $scope.pager = false;
            }


            $scope.save = function ()
            {
                var data2 = [];
                var send_data2 = [];

                admdetails = [];

                for (var i = 0; i < $scope.filteredTodos.length; i++)
                {
                    if ($scope.filteredTodos[i].pros_num1 == true)
                    {
                        admdetails = admdetails + $scope.filteredTodos[i].sims_pros_no + ',';
                    }
                }

                if (admdetails.length == 0) {
                    swal({ title: "Alert", text: "Please select atleast 1 applicant", showCloseButton: true, width: 380, });
                    //$('#message').modal({ backdrop: 'static', keyboard: false });
                }
                else
                {
                    if ($scope.edt.sims_teacher_code == null)
                    {
                        swal({ title: "Alert", text: "Please select Assesment Teacher.", showCloseButton: true, width: 380, });
                    }
                    else
                    {
                        var data = ({
                            sims_cur_code: $scope.edt.curr_code,
                            sims_acad_yr: $scope.edt.academic_year,
                            sims_grade_code: $scope.edt.grade_code,
                            sims_teacher_code: $scope.edt.sims_teacher_code,
                            sims_assesment_date: $scope.edt.sims_assesment_date,
                            sims_prosno_list: admdetails,
                            sims_assessment_created_user: $rootScope.globals.currentUser.username,
                            sims_assesment_time:$scope.edt.sims_assesment_time,
                            opr: 'I'
                        });

                        data2.push(data);

                        var data_obj = ({
                            sims_cur_code: $scope.edt.curr_code,
                            sims_acad_yr: $scope.edt.academic_year,
                            sims_grade_code: $scope.edt.grade_code,
                            sims_teacher_code: $scope.edt.sims_teacher_code,
                            sims_assesment_date: $scope.edt.sims_assesment_date,
                            sims_prosno_list: admdetails,
                            sims_assessment_created_user: $rootScope.globals.currentUser.username,
                            sims_assesment_time: $scope.edt.sims_assesment_time,
                            opr: 'B'
                        });

                        send_data2.push(data_obj);
                        
                      
                        var copy_obj = angular.copy(send_data2);

                        $http.post(ENV.apiUrl + "api/StudentAssessmentSchedule/CUDInsertAssessmentScheduleDetails", data2).then(function (res) {
                            $scope.msg1 = res.data;
                            console.log($scope.msg1);

                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Mapped.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        swal({ title: "Email", text: "Do you want Send Email.", showCloseButton: true, showCancelButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {

                                                $http.post(ENV.apiUrl + "api/StudentAssessmentSchedule/CUDsend_email", send_data2).then(function (res) {
                                                });
                                                $scope.getAssessmentscheduleData();
                                            }
                                            else {
                                                $scope.getAssessmentscheduleData();
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            }

            //new code for communication


            $('#text-editor1').wysihtml5();
            $scope.chk_email = true;
            $scope.chk_sms = false;

            $scope.edt1 =
            {
                sims_Email: "true",
                sims_sms_char: '0',
                sims_sms_length: '0'
            };

            $http.get(ENV.apiUrl + "api/common/EmailSms/GetcheckEmailProfile").then(function (res) {
                $scope.getProfile = res.data;
                // console.log($scope.getProfile);
            });

            $scope.send_mail_details = function () {
                /* EMAIL*/
                if ($scope.edt1.sims_Email == "true") {
                    swal({ title: "Alert", text: "Do You Want to Send Emails?", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.sendMaildetails();
                        }
                    });
                }
                if ($scope.edt1.sims_Email == "false") {
                    swal({
                        title: '',
                        text: "Do You Want to Send SMS?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.OnsendSMSdetails();
                        }
                    });
                }
            }

            $scope.get_msgtypechecked = function (msg_type) {


                if (msg_type == "true") {
                    $scope.edt1['sims_recepient_id'] = $scope.sims_admission_father_mail;
                    $scope.edt1['sims_body'] = $scope.body;
                    $scope.chk_email = true;
                    $scope.chk_sms = false;
                }
                else if (msg_type == "false") {
                    $scope.edt1['sims_recepient_id'] = $scope.sims_admission_father_mobile;
                    $scope.edt1['sims_smstext'] = $scope.txt4;
                    $scope.msg_details($scope.edt1.sims_smstext);
                    $scope.chk_email = false;
                    $scope.chk_sms = true;
                    $scope.edt1.sims_sms_msg = "Enter your message below ( max 1000 chars.1 sms is 160 characters.)";
                    $scope.edt1.sims_sms_subjectblock = "These special characters ' ][}{|~^ ' will be treated as two character";
                }

            }

            $scope.getcommunicate = function (dash)
            {
               
                $scope.sims_admission_father_mail = dash.sims_admission_father_email;
                $scope.sims_admission_father_mobile = dash.sims_admission_father_mobile;
                $scope.sims_admission_number = dash.sims_pros_no;
                $scope.sims_assesment_date = dash.sims_assesment_date;
                $scope.sims_assesment_time = dash.sims_assesment_time;

                $('#viewdashDetailsModal1').modal({ backdrop: 'static', keyboard: true });
                $scope.get_msgtypechecked($scope.edt1.sims_Email);

               

                $http.get(ENV.apiUrl + "api/common/Admission_dpsd/GetemailSubjectforInviting").then(function (res) {
                    $scope.emailSubject_datails = res.data;
                   // console.log($scope.emailSubject_datails);
                    $scope.edt1['sims_subject'] = $scope.emailSubject_datails[0].mailmsg_subject;
                    $scope.edt1['sims_body'] = $scope.emailSubject_datails[0].mailmsg_body;

                    var v = document.getElementById('text-editor1');
                    v.value = $scope.emailSubject_datails[0].mailmsg_body;
                    $scope.edt1['sims_body'] = v.value;
                    $('#text-editor1').data("wysihtml5").editor.setValue($scope.edt1['sims_body']);


                    $scope.old_str = angular.copy($scope.edt1.sims_body);

                    var txt = $scope.old_str.replace(/{appl_num}/g, $scope.sims_admission_number);
                    var txt1 = txt.replace(/{appl_date}/g, $scope.sims_assesment_date);
                    var txt2 = txt1.replace(/{app_time}/g, $scope.sims_assesment_time);

                    $scope.body = txt2;
                    $scope.edt1['sims_body'] = $scope.body;

                     $('#text-editor1').data("wysihtml5").editor.setValue($scope.edt1['sims_body']);
                });


                $http.get(ENV.apiUrl + "api/common/Admission_dpsd/GetSmsSubjectforInviting").then(function (res) {
                    $scope.smsSubject_datails = res.data;
                    $scope.edt1['sims_smstext'] = $scope.smsSubject_datails[0].mailmsg_body;

                    $scope.old_str1 = angular.copy($scope.edt1.sims_smstext);

                    var txtt = $scope.old_str1.replace(/{appl_num}/g, $scope.sims_admission_number);
                    var txtt1 = txtt.replace(/{appl_date}/g, $scope.sims_assesment_date);
                    var txtt2 = txtt1.replace(/{app_time}/g, $scope.sims_assesment_time);
                    $scope.txt4 = txtt2;
                    $scope.edt1['sims_smstext'] = $scope.txt4;
                    $scope.msg_details($scope.edt1.sims_smstext);
                });
            }

            $scope.msg_details = function (sims_smstext)
            {
                if (sims_smstext != "" && sims_smstext != undefined) {
                    var q = 0, cnt = 0;
                    $scope.edt1.sims_sms_char = 0;

                    var v = document.getElementById('txt_smstext');
                    var r = v.value.length;
                    // var sms = $scope.edt1.sims_smstext;
                    var sms = sims_smstext;
                    //console.log(v.value.length);

                    var nameList = new Array('[', ']', '{', '}');
                    if (sms != "") {
                        for (var x = 0, c = ''; c = sms.charAt(x) ; x++) {
                            for (var i = 0; i < nameList.length; i++) {
                                if (nameList[i] === c) {
                                    q = q + 1;
                                }
                            }
                        }
                        var ss = q * 2;
                        var smsLen = r;
                        cnt = (smsLen - q) + ss;
                        $scope.edt1.sims_sms_char = cnt;
                    }

                    if ($scope.edt1.sims_sms_char <= 160 && $scope.edt1.sims_sms_char > 0)
                    $scope.edt1.sims_sms_length = "1";
                    if ($scope.edt1.sims_sms_char <= 320 && $scope.edt1.sims_sms_char > 160)
                    $scope.edt1.sims_sms_length = "2";
                    if ($scope.edt1.sims_sms_char <= 480 && $scope.edt1.sims_sms_char > 320)
                    $scope.edt1.sims_sms_length = "3";
                    if ($scope.edt1.sims_sms_char <= 640 && $scope.edt1.sims_sms_char > 480)
                    $scope.edt1.sims_sms_length = "4";
                    if ($scope.edt1.sims_sms_char <= 800 && $scope.edt1.sims_sms_char > 640)
                    $scope.edt1.sims_sms_length = "5";
                    if ($scope.edt1.sims_sms_char <= 960 && $scope.edt1.sims_sms_char > 800)
                    $scope.edt1.sims_sms_length = "6";
                    if ($scope.edt1.sims_sms_char <= 1000 && $scope.edt1.sims_sms_char > 960)
                    $scope.edt1.sims_sms_length = "7";
                }
                else {
                    $scope.edt1.sims_sms_char = "0";
                    $scope.edt1.sims_sms_length = "0";
                }
            }

             var data1 = [];
             var lst_cc = [];

            $scope.sendMaildetails = function ()
            {
                var data = [];
                $scope.edt1.body = document.getElementById('text-editor1').value;
               
                var flag = true;
                /* EMAIL*/
                if ($scope.edt1.sims_Email == "true")
                {
                    if (($scope.edt1.sims_subject == "" || $scope.edt1.sims_subject == undefined) && ($scope.edt1.body == "" || $scope.edt1.body == undefined)) {
                        flag = false;
                        swal({ title: "Alert", text: "Subject & Message Body are Required", showCloseButton: true, width: 380, });
                    }

                    if (flag == true)
                    {
                        $scope.BUSY = true;
                        var data1 =
                            {
                                subject: $scope.edt1.sims_subject,
                                body: $scope.edt1.body,
                                sender_emailid: $scope.getProfile,
                                emailsendto: $scope.edt1.sims_recepient_id
                            }
                        data.push(data1);

                        var data2 =
                                    {
                                        attFilename: ''
                                    }
                        lst_cc.push(data2);

                        $http.post(ENV.apiUrl + "api/common/Email/ScheduleMails_dpsd?filenames=" + JSON.stringify(lst_cc), data).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Mail Sent Successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                            }
                            else {
                                swal({ text: "Mail Not Sent.", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });

                            }
                        });
                    }
                }
            }

            $scope.username = $rootScope.globals.currentUser.username;

            $scope.OnsendSMSdetails = function ()
            {
                var data3 = [];
                var smssendto = "";
                    $scope.BUSY = true;
                        
                    var data2=
                    {
                        sims_smstext : $scope.edt1.sims_smstext,
                        usercode : $scope.username,
                        smssendto : $scope.edt1.sims_recepient_id
                    }
                    data3.push(data2);
          
                    $http.post(ENV.apiUrl + "api/common/EmailSms/CUD_Insert_SMS_Schedule", data3).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true)
                        {
                            swal({ text: "SMS sent successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                        }
                        else
                        {
                            swal({ text: "SMS Not Sent", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                        }
                    });
            }

            $scope.cancel_mail_details = function ()
            {
                $scope.edt1 = "";
                $scope.edt1['sims_Email'] = "true";
                $scope.chk_email = true;
                $scope.chk_sms = false;
                $scope.get_msgtypechecked($scope.edt1['sims_Email']);
            }

            /*end*/

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
         }])

})();