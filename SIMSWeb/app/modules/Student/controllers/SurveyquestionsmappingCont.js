﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SurveyquestionsmappingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.save_btn = true;
            $scope.Survey_Questions_Details = [];
            $scope.Survey_Questions_Details1 = [];
            $scope.temp = {};

            $http.get(ENV.apiUrl + "api/surveyquestionmapping/Get_survey_type").then(function (Get_survey_type) {
                $scope.survey_type = Get_survey_type.data;
            });

            $http.get(ENV.apiUrl + "api/SurveyQuestionAnswer/getSurveyQuestionType").then(function (question) {
                $scope.ques_data = question.data;
            });

            $scope.GetSurveyDetails = function () {
                $http.get(ENV.apiUrl + "api/surveyquestionmapping/Get_survey_details?survey_code=" + $scope.temp.sims_survey_code).then(function (Get_survey_details) {
                    $scope.survey_details = Get_survey_details.data;

                    if ($scope.survey_details.length > 0) {
                        $scope.survey_detail_table = true;

                        $scope.GetsurveyQuestionsdetails($scope.survey_details[0]);
                    }
                    else {
                        $scope.survey_detail_table = false;
                        $scope.survey_questions_table = false;
                        swal({ text: "Record Not Found", width: 300, height: 250, showCloseButton: true });

                    }
                });
            }


            $scope.UpdateQuestionBankFun = function (str) {
                debugger
                $scope.temp = {};
                $scope.Answerdata = [];
                $('.modal-backdrop').remove();
                $('#Updatesurvey').modal('show');
              //  $http.get(ENV.apiUrl + "api/surveyquestionmapping/getParagraph").then(function (para_data) {
                 //   $scope.paragraph_combo_box = para_data.data;
                    $scope.temp = {
                        'sims_survey_question_type': str.sims_survey_question_type,
                        'sims_survey_question_difficulty_level': str.sims_survey_question_difficulty_level,
                        'sims_survey_question_desc_en': str.sims_survey_question_desc_en,
                        'sims_survey_question_weightage': str.sims_survey_question_weightage,
                        'sims_survey_question_answer_desc_en': str.sims_survey_question_answer_desc_en,
                        'sims_survey_question_correct_answer_flag': str.sims_survey_question_correct_answer_flag,
                        'sims_survey_skill_code': str.sims_survey_skill_code,
                        'sims_survey_section_code': str.sims_survey_section_code,
                        'sims_survey_subsection_code': str.sims_survey_subsection_code,
                        'sims_survey_no_of_question_available': str.sims_survey_no_of_question_available,
                        'sims_survey_code': str.sims_survey_code,
                        'sims_survey_question_code': str.sims_survey_question_code,
                        'sims_survey_question_answer_explanation': str.sims_survey_question_answer_explanation,
                        'sims_survey_question_header_code': str.sims_survey_question_header_code,
                        'sims_survey_question_header_code_description': str.sims_survey_question_header_code_description,
                        'sims_survey_question_image_path': str.sims_survey_question_image_path,
                        'sims_survey_question_answer_image': str.sims_survey_question_answer_image
                    }

                    //$('#text-editor1').data("wysihtml5").editor.clear();
                    //$('#text-editor1').data("wysihtml5").editor.setValue(str.sims_survey_question_desc_en);

                //});

                    $scope.Answerdata = angular.copy(str.que_ans);


            }


            $scope.btn_Addnewquestions_update_ClickNew = function () {
                debugger
                

                $scope.data = {
                    'sims_survey_question_type': $scope.temp.sims_survey_question_type,
                    'sims_survey_question_difficulty_level': $scope.temp.sims_survey_question_difficulty_level,
                    'sims_survey_question_desc_en':  $scope.temp.sims_survey_question_desc_en,
                    'sims_survey_question_weightage': $scope.temp.sims_survey_question_weightage,
                    //'sims_survey_question_desc_en': $scope.temp.sims_survey_question_desc_en,
                    'sims_survey_question_answer_desc_en': $scope.temp.sims_survey_question_answer_desc_en,
                    'sims_survey_question_correct_answer_flag': $scope.temp.sims_survey_question_answer_desc_en,
                    'sims_survey_skill_code': $scope.temp.sims_survey_skill_code,
                    'sims_survey_section_code': $scope.temp.sims_survey_section_code,
                    'sims_survey_subsection_code': $scope.temp.sims_survey_subsection_code,
                    'sims_survey_code': $scope.temp.sims_survey_code,
                    'sims_survey_question_code': $scope.temp.sims_survey_question_code,
                    'sims_survey_question_header_code': $scope.temp.sims_survey_question_header_code,
                    'sims_survey_question_answer_explanation': $scope.temp.sims_survey_question_answer_explanation,
                    'sims_survey_question_header_code_description': $scope.temp.sims_survey_question_header_code_description,
                }

                //console.log("temp to update", $scope.data);
                //console.log("ansers", $scope.Answerdata);
                //console.log("ansers", $scope.Answerdata);



                var lst_t = [];
                lst_t.push($scope.data);

                for (var i = 0; i < $scope.Answerdata.length; i++) {
                    $scope.Answerdata[i].sims_survey_question_code = lst_t[0].sims_survey_question_code;
                }

                $http.post(ENV.apiUrl + "api/surveyquestionmapping/CUDSurveyNew", lst_t).then(function (msg) {
                    $scope.msg1 = msg.data;
                    console.log("msg", msg);
                    if ($scope.msg1 == true) {
                      
                        $http.post(ENV.apiUrl + "api/surveyquestionmapping/CUDSurveyANSNew", $scope.Answerdata).then(function (msg) {
                            $scope.msg1 = msg.data;
                           
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Survey Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                $('.modal-backdrop').remove();
                                  $scope.GetSurveyDetails();
                            } else {
                                swal({ title: "Alert", text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                        });
                    } else {
                        swal({ title: "Alert", text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                });

            }


            $scope.GetsurveyQuestionsdetails = function (info) {
               
                info.sims_check = true;
                $http.get(ENV.apiUrl + "api/surveyquestionmapping/Get_Survey_Questions_DetailsNew?sims_survey_code=" + info.sims_survey_code).then(function (Get_Survey_Questions_Details) {
                    $scope.Survey_Questions_Details = Get_Survey_Questions_Details.data;
                    $scope.Survey_Questions_Details1 = angular.copy(Get_Survey_Questions_Details.data);
                    if ($scope.Survey_Questions_Details.length > 0) {
                        $scope.survey_questions_table = true;
                    } else {
                        $scope.survey_questions_table = false;
                        swal({ text: "Record Not Found", width: 300, height: 250, showCloseButton: true });

                    }

                });
            }

            $scope.CheckAllChecked = function (str) {
                for (var i = 0; i < $scope.getSurveyQuestionOntype.length; i++) {
                    if (str == true) {
                        $scope.getSurveyQuestionOntype[i].ischecked = true;
                    }
                    else {
                        $scope.getSurveyQuestionOntype[i].ischecked = false;
                    }
                }
            }

            $scope.SelectQuestionsFormshow = function () {
                $scope.temp['sims_survey_question_type'] = '';
                $scope.temp['sims_survey_question_difficulty_level'] = '';

                $scope.getSurveyQuestionOntype = [];
                $('#MyModal').modal('show');
            }

            $scope.getquestiosbyquestiontype = function () {

                $http.get(ENV.apiUrl + "api/surveyquestionmapping/getSurveyQuestionOntype?sims_survey_question_type=" + $scope.temp.sims_survey_question_type + "&sims_survey_question_difficulty_level=" +$scope.temp.sims_survey_question_difficulty_level).then(function (getSurveyQuestionOntype) {
                    $scope.getSurveyQuestionOntype = getSurveyQuestionOntype.data;
                    if ($scope.getSurveyQuestionOntype.length > 0) {
                        $scope.survey_questions_for_select = true;
                    } else {
                        $scope.survey_questions_for_select = false;
                        swal({ text: "Record Not Found", width: 300, height: 250, showCloseButton: true });

                    }
                });
            }

            $scope.btn_Addselectquestions_update_Click = function () {

                if ($scope.getSurveyQuestionOntype.length > 0) {
                    for (var k = 0; k < $scope.getSurveyQuestionOntype.length; k++) {
                        for (var j = 0; j < $scope.Survey_Questions_Details.length; j++) {
                            if ($scope.Survey_Questions_Details[j].sims_survey_question_code == $scope.getSurveyQuestionOntype[k].sims_survey_question_code) {
                                $scope.getSurveyQuestionOntype[k].ischecked = false;
                            }
                        }
                    }
                }

                for (var i = 0; i < $scope.getSurveyQuestionOntype.length; i++) {
                    if ($scope.getSurveyQuestionOntype[i].ischecked == true) {
                        $scope.Survey_Questions_Details.push($scope.getSurveyQuestionOntype[i]);
                    }
                }

                if ($scope.Survey_Questions_Details.length > 0) {
                    $scope.survey_questions_table = true;
                } else {
                    $scope.survey_questions_table = false;
                }
            }

            $scope.removequestions = function (info, object, index) {
                object.splice(index, 1);
            }

            $scope.AddnewQuestionstoQuestionnaire_Click = function () {

                $scope.dataobject = [];

                for (var i = 0; i < $scope.survey_details.length; i++) {
                    if ($scope.survey_details[i].sims_check == true) {
                        for (var j = 0; j < $scope.Survey_Questions_Details.length; j++) {
                            var data = {};
                            data.sims_survey_code = $scope.survey_details[i].sims_survey_code;
                            data.sims_survey_question_code = $scope.Survey_Questions_Details[j].sims_survey_question_code;
                            data.sims_survey_question_display_order = $scope.Survey_Questions_Details[j].sims_survey_question_display_order;
                            data.sims_survey_question_skip_flag = $scope.Survey_Questions_Details[j].sims_survey_question_skip_flag;
                            data.sims_survey_question_status = $scope.Survey_Questions_Details[j].sims_survey_question_status;
                            $scope.dataobject.push(data);
                        }
                    }
                }

                if ($scope.dataobject.length > 0) {
                    $http.post(ENV.apiUrl + "api/surveyquestionmapping/CUDAddnewQuestionstoQuestionnaire", $scope.dataobject).then(function (res) {
                        $scope.msg = res.data;

                        if ($scope.msg == true) {
                            swal({ text: "Questions Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 250, showCloseButton: true });
                            $scope.GetsurveyQuestionsdetails($scope.dataobject[0]);
                        }

                    });
                }else
                {
                    swal({ text: "Select Atleast One Survey For Insert Questions", width: 300, height: 250, showCloseButton: true });
                }
            }

            $scope.UpdatenewQuestionstoQuestionnaire_Click = function () {
                $http.post(ENV.apiUrl + "api/surveyquestionmapping/CUDAddnewQuestionstoQuestionnaire", $scope.Survey_Questions_Details).then(function (res) {
                        $scope.msg = res.data;
                        if ($scope.msg == true) {
                            swal({ text: "Questions Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 250, showCloseButton: true });
                            $scope.GetsurveyQuestionsdetails($scope.Survey_Questions_Details[0]);
                        }
                    });
            }
            //add new question
            $scope.AddnewQuestionsFormshow = function () {
                $scope.temp['sims_survey_question_desc_en'] = '';
                $scope.temp['sims_survey_question_desc_ot'] = '';
                $scope.temp['sims_survey_question_type'] = '';
                $scope.temp['sims_survey_question_difficulty_level'] = '';
                $scope.temp['sims_survey_question_weightage'] = '';
                $scope.Answerdata = [];
                $('#MyModal1').modal('show');
            }

            $scope.queschange = function (str) {

                $scope.Answerdata = [];
                $scope.ansname = true;
                if (str.sims_survey_question_type == '1' || str.sims_survey_question_type == '5') {
                    $scope.ans = true;
                    //$scope.anstable = true;
                }
                else {
                    $scope.ans = false;
                    $scope.anstable = false;
                }
                if (str.sims_survey_question_type == '4') {
                    $scope.ans1 = true;
                    $scope.anstable1 = true;
                }
                else {
                    $scope.ans1 = false;
                    $scope.anstable1 = false;
                }
            }

            $scope.ratechange = function (str) {

                $scope.ansname = true;
                $http.get(ENV.apiUrl + "api/SurveyQuestionAnswer/getSurveyQuestionRateDetails?group_code=" + str.sims_survey_rating_group_code).then(function (ratingdetails) {
                    $scope.Rating = ratingdetails.data;
                });
            }

            $http.get(ENV.apiUrl + "api/SurveyQuestionAnswer/getSurveyQuestionRateGroup").then(function (RateGroup) {
                $scope.rate_data = RateGroup.data;
            });

            $http.get(ENV.apiUrl + "api/surveyquestionmapping/Get_question_difficalty").then(function (Get_question_difficalty) {
                $scope.question_difficalty = Get_question_difficalty.data;
            });

            $scope.Answerdata = [];

            $scope.AddAnswers = function () {

                if ($scope.temp.sims_survey_question_answer_desc_en == undefined || $scope.temp.sims_survey_question_answer_desc_en == '') {
                    return;
                }
                $scope.anstable = true;
                var v = {
                    'sims_survey_question_answer_desc_en': $scope.temp.sims_survey_question_answer_desc_en
                }

                $scope.Answerdata.push(v);
                v = '';
                $scope.temp.sims_survey_question_answer_desc_en = '';
            }

            $scope.objective_ans = function (event) {

                if (event.key == "Enter") {
                    $scope.AddAnswers();
                }
            }

            $scope.Save = function (Myform) {
                debugger;
                var savefin = [];
                var flag = false;
                if (Myform) {

                    if ($scope.temp.sims_survey_question_type == '1' || $scope.temp.sims_survey_question_type == '5') {
                        for (var i = 0; i < $scope.Answerdata.length; i++) {
                            var v = document.getElementById($scope.Answerdata[i].sims_survey_question_answer_desc_en + i);
                            if (v.checked == true) {
                                var modulecode = ({
                                    'sims_survey_question_type': $scope.temp.sims_survey_question_type,
                                    'sims_survey_question_difficulty_level': $scope.temp.sims_survey_question_difficulty_level,
                                    'sims_survey_question_weightage':$scope.temp.sims_survey_question_weightage,
                                    'sims_survey_question_desc_en': $scope.temp.sims_survey_question_desc_en,
                                    'sims_survey_question_desc_ot': $scope.temp.sims_survey_question_desc_ot,
                                    'sims_survey_question_answer_desc_en': $scope.Answerdata[i].sims_survey_question_answer_desc_en,
                                    'sims_survey_question_correct_answer_flag': true
                                });
                            }
                            else {
                                var modulecode = ({
                                    'sims_survey_question_type': $scope.temp.sims_survey_question_type,
                                    'sims_survey_question_difficulty_level': $scope.temp.sims_survey_question_difficulty_level,
                                    'sims_survey_question_weightage': $scope.temp.sims_survey_question_weightage,
                                    'sims_survey_question_desc_en': $scope.temp.sims_survey_question_desc_en,
                                    'sims_survey_question_desc_ot': $scope.temp.sims_survey_question_desc_ot,
                                    'sims_survey_question_answer_desc_en': $scope.Answerdata[i].sims_survey_question_answer_desc_en,
                                    'sims_survey_question_correct_answer_flag': false
                                });
                            }
                            flag = true;
                            savefin.push(modulecode);
                        }
                    }

                    //Subjective Question Type
                    if ($scope.temp.sims_survey_question_type == '2') {
                        var datacode = ({
                            'sims_survey_question_type': $scope.temp.sims_survey_question_type,
                            'sims_survey_question_desc_en': $scope.temp.sims_survey_question_desc_en,
                            'sims_survey_question_desc_ot': $scope.temp.sims_survey_question_desc_ot,
                            'sims_survey_question_difficulty_level': $scope.temp.sims_survey_question_difficulty_level,
                            'sims_survey_question_weightage': $scope.temp.sims_survey_question_weightage,
                            'sims_survey_question_answer_desc_en': $scope.temp.sims_survey_question_answer_desc_en,
                        });
                        flag = true;
                        savefin.push(datacode);
                    }

                    //True/False Question Type
                    if ($scope.temp.sims_survey_question_type == '3') {
                        var a = ({
                            'sims_survey_question_type': $scope.temp.sims_survey_question_type,
                            'sims_survey_question_desc_en': $scope.temp.sims_survey_question_desc_en,
                            'sims_survey_question_desc_ot': $scope.temp.sims_survey_question_desc_ot,
                            'sims_survey_question_difficulty_level': $scope.temp.sims_survey_question_difficulty_level,
                            'sims_survey_question_weightage': $scope.temp.sims_survey_question_weightage,
                            'sims_survey_question_answer_desc_en': 'True',
                        });
                        savefin.push(a);
                        var b = ({
                            'sims_survey_question_type': $scope.temp.sims_survey_question_type,
                            'sims_survey_question_difficulty_level': $scope.temp.sims_survey_question_difficulty_level,
                            'sims_survey_question_weightage': $scope.temp.sims_survey_question_weightage,
                            'sims_survey_question_desc_en': $scope.temp.sims_survey_question_desc_en,
                            'sims_survey_question_desc_ot': $scope.temp.sims_survey_question_desc_ot,
                            'sims_survey_question_answer_desc_en': 'False',
                        });
                        flag = true;
                        savefin.push(b);
                    }

                    //Rating Question Type
                    if ($scope.temp.sims_survey_question_type == '4') {
                        debugger;
                        for (var i = 0; i < $scope.Rating.length; i++) {
                            var code = ({
                                'sims_survey_question_weightage': $scope.temp.sims_survey_question_weightage,
                                'sims_survey_question_type': $scope.temp.sims_survey_question_type,
                                'sims_survey_question_desc_en': $scope.temp.sims_survey_question_desc_en,
                                'sims_survey_question_desc_ot': $scope.temp.sims_survey_question_desc_ot,
                                'sims_survey_question_difficulty_level': $scope.temp.sims_survey_question_difficulty_level,
                                'sims_survey_question_answer_desc_en': 'Rating',
                                'sims_survey_question_rating_group_code': $scope.temp.sims_survey_rating_group_code,
                            });
                            flag = true;
                            savefin.push(code);
                        }
                    }

                    $http.post(ENV.apiUrl + "api/surveyquestionmapping/CUDSurveyQuestionAnswerforsurveyquestionmapping", savefin).then(function (savedata) {
                        debugger;
                        $scope.newquetionsandansobject = savedata.data;
                        if ($scope.newquetionsandansobject.length > 0) {
                            swal({ text: "Question and Answer Saved Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else {
                            swal({ text: "Sorry! Question and Answer Not Saved", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                    });
                    savefin = [];
                    $scope.ansname = false;

                }
            }

            $scope.btn_Addnewquestions_update_Click = function () {
                for (var i = 0; i < $scope.newquetionsandansobject.length; i++) {
                    $scope.Survey_Questions_Details.push($scope.newquetionsandansobject[i]);
                }

                if ($scope.Survey_Questions_Details.length > 0) {
                    $scope.survey_questions_table = true;
                } else {
                    $scope.survey_questions_table = false;
                }
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });
                $("#Table6").tableHeadFixer({ 'top': 1 });
                $("#fixTable3").tableHeadFixer({ 'top': 1 });

            }, 100);

            $scope.Reset=function()
            {
                $scope.temp['sims_survey_question_difficulty_level'] = '';
                $scope.temp['sims_survey_question_type'] = '';
            }
         }]);
})(); 