﻿(function () {

    var main;
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SENApproveCont',
      ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.pagesize = 'All';
            $scope.pageindex = "0";
            $scope.table = true;
            $scope.search_btn = true;
            $scope.clr_btn = true;
            $scope.pager = false;
            $scope.save_btn = true;
            $scope.save_image = [];
            $scope.image_arr = [];
            $scope.tr_len = "";
            $scope.url_new = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Docs/Student/';
            $scope.users = $rootScope.globals.currentUser.username;
            $scope.temp = {}
            $scope.temp['value'] = '0';
            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 50, $scope.maxSize = 50;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    $scope.filteredTodos[i].icon = 'fa fa-plus-circle';
                }

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Stud_Details, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Stud_Details;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }


            //check all 
            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk11');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            //checked one by one
            var ind;
            var data = [];
            $scope.checkonebyonedelete = function (str, index) {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                        //str.status = true;
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                        //str.status = false;
                    }
                });
                str.status = str.datastatus;
                ind = index;
                //data = str;
            }



            $scope.getcur = function () {
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                    $scope.curriculum = AllCurr.data;
                    $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                    $scope.getAccYear($scope.edt.sims_cur_code);
                });
            }

            $scope.getcur();

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                    $scope.getGrade($scope.edt.sims_cur_code, $scope.temp.sims_academic_year);
                    //$scope.health();
                });
            }

            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    console.log($scope.Grade_code);
                    $scope.temp['value'] = '0';
                });
            }

            $scope.stud_Details_fun = function (curcode, acYear, gCode, gFilter) {
                debugger;
               // $scope.temp = {}
                
                cur = $scope.edt.sims_cur_code;
                year = $scope.temp.sims_academic_year;
                grade = $scope.temp.sims_grade_code;
                section = $scope.temp.sims_section_code;
                filter = $scope.temp.value;
                $http.get(ENV.apiUrl + "api/studentApprove/getStudSearch?sims_cur_code=" + curcode + "&sims_academic_year=" + acYear + "&sims_grade_code=" + gCode + "&value=" + gFilter).then(function (StudDetails) {
                    $scope.Stud_Details = StudDetails.data;
                    $scope.totalItems = $scope.Stud_Details.length;
                    $scope.todos = $scope.Stud_Details;
                    $scope.makeTodos();
                });
            }


            // update
            $scope.update = function () {
                debugger;
                var send = [];
                code = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i]['value'] = $scope.temp.value;
                    $scope.filteredTodos[i]['user_name'] = $scope.users;
                }

                $http.post(ENV.apiUrl + "api/studentApprove/CUDStudentSenApprove", $scope.filteredTodos).then(function (msg) {


                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }

                    $scope.stud_Details_fun($scope.edt.sims_cur_code, $scope.temp.sims_academic_year, $scope.temp.sims_grade_code, $scope.temp.value);
                });

            }

            $scope.call_modal = function (str) {
                debugger
                $(document).ready(function () {
                    $('#MyModal5').fadeIn('fast');
                });

                $scope.dataupdatesend = [];
                data = {
                    sims_admission_sr_no: str.sims_admission_sr_no,
                    sims_admission_number: str.sims_admission_number,
                    sims_sen_rejected_by: $scope.users,
                }
                $scope.dataupdatesend.push(data);

                $scope.reject = function () {
                    debugger
                    data_remark = {
                        sims_admission_sr_no:$scope.dataupdatesend[0].sims_admission_sr_no,
                        sims_admission_number:$scope.dataupdatesend[0].sims_admission_number,
                        sims_sen_rejected_by:$scope.dataupdatesend[0].sims_sen_rejected_by,
                        sims_sen_rejected_remark: $scope.sims_sen_rejected_remark
                    
                    }

                    $scope.dataupdatesend = [];

                    $scope.dataupdatesend.push(data_remark);

                    console.log($scope.dataupdatesend);
                    swal({
                        title: "Alert",
                        text: "Do you want to reject This Student?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/studentApprove/CUDStudentSenRejectApproveList", $scope.dataupdatesend).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Rejected Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                    $scope.dataupdatesend = [];
                                    $scope.Close_modal();
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Unable To Reject. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                                $scope.stud_Details_fun($scope.edt.sims_cur_code, $scope.temp.sims_academic_year, $scope.temp.sims_grade_code, $scope.temp.value);
                            });
                        }
                    });
                }

            }


            $scope.Close_modal = function () {
                $scope.sims_sen_rejected_remark = "";
                $(document).ready(function () {
                    $('#MyModal5').fadeOut('fast');
                });
              }
            $scope.UploadDocumentView = function (str) {
                debugger
                $scope.save_image = [];
                data_image = {
                    sims_sen_document_admission_number: str.sims_admission_number,
                    sims_sen_document_transaction_number: str.sims_admission_sr_no,
                    sims_sen_document_created_by: $scope.users,
                }
                $scope.save_image.push(data_image);

                $scope.getDocDetailsFun(str.sims_admission_sr_no, str.sims_admission_number);
                $scope.busyindicator = true;
                $('#UploadDocModal').modal({ backdrop: 'static', keyboard: true });
            }

            $scope.getDocDetailsFun = function (sims_admission_sr_no, sims_admission_number) {
                debugger
                $http.get(ENV.apiUrl + "api/student/getDoclist?sims_sen_document_transaction_number=" + sims_admission_sr_no + "&sims_sen_document_admission_number=" + sims_admission_number).then(function (docDetails) {
                    $scope.doc_details = docDetails.data;
                    $scope.tr_len = $scope.doc_details.length;
                    $scope.tr_len = parseInt($scope.tr_len) + 1;
                });
            }

            $scope.uploadClick = function (str) {
                debugger
                $scope.filesize = true;
                $scope.edt1 = str;
                $scope.edt.photoStatus = true;
                $scope.ins = false;
                formdata = new FormData();
            }

            $scope.getTheFiles = function ($files) {
                $scope.filesize = true;
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.file_changed = function (element, str) {
                debugger;
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                $scope.adm_number = $scope.save_image[0].sims_sen_document_admission_number
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;
                    });
                };

                reader.readAsDataURL(photofile);

                $scope.busyindicator = false;
                $scope.object = {};
                if (element.files[0].size < 1048576) {


                    var request = {

                        method: 'POST',

                        url: ENV.apiUrl + 'api/file/uploadDocument_new?filename=' + $scope.adm_number + '_' + '(' + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss') + ')' + "&location=" + "Docs/Student/",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    $http(request).success(function (d) {
                        $scope.filename = d;
                        save_final_data = {
                            sims_sen_document_admission_number: $scope.save_image[0].sims_sen_document_admission_number,
                            sims_sen_document_transaction_number: $scope.save_image[0].sims_sen_document_transaction_number,
                            sims_sen_document_created_by: $scope.save_image[0].sims_sen_document_created_by,
                            sims_sen_document_remark: $scope.sims_sen_document_remark,
                            sims_sen_document_name: $scope.filename
                        }
                        $scope.image_arr.push(save_final_data);


                        $http.post(ENV.apiUrl + "api/student/CIImgUpl", $scope.image_arr).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Uploaded Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                $scope.sims_sen_document_remark = "";
                                $scope.sims_sen_document_name = "";
                                $scope.getDocDetailsFun($scope.image_arr[0].sims_sen_document_transaction_number, $scope.image_arr[0].sims_sen_document_admission_number);
                                $scope.save_image = [];
                                $scope.image_arr = [];
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Something Went Wrong", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });



                    });
                }
                else {
                    swal({ title: "Alert", text: "Please upload file less than 2 mb", showCloseButton: true, width: 380, });
                    $scope.busyindicator = true;
                }
            }

            $scope.clear = function () {
                
                $scope.temp.sims_grade_code = '';
                $scope.temp.value = '';
                $scope.filteredTodos = '';
                $scope.pager = false;
            }
      }])

       simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };
        return {
            link: fn_link
        }
    }])
})();
