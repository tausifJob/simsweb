﻿(function () {
    'use strict';
    var main = '';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('UpdateFeeCategoryCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.busyindicator = false;
            $scope.grid = true;
            $scope.color = '#edefef';
            
            
            
            

            $http.get(ENV.apiUrl + "api/UpdateFeeCatController/GetCur").then(function (GetCur) {
                $scope.GetCur = GetCur.data;
              //  $scope.edt = { cur: $scope.GetCur[0].cur }
            });

          
           
            $scope.select_cur = function () {
                $http.get(ENV.apiUrl + "api/UpdateFeeCatController/GetAca").then(function (GetAca) {
                    $scope.GetAca = GetAca.data;
                    debugger;
                    //$scope.edt = { aca: $scope.GetAca[0].aca }

                    //$scope.select_aca($scope.edt.aca);
                });
            }


            $scope.select_aca = function (aca)
            {
                debugger;
                
                $http.get(ENV.apiUrl + "api/UpdateFeeCatController/Getgrd?cur=" + $scope.edt.cur + "&aca=" + aca).then(function (Getgrd) {
                    $scope.Getgrd = Getgrd.data;
                });
                
            }

            $scope.select_grd = function (grd) {
                debugger;
                $http.get(ENV.apiUrl + "api/UpdateFeeCatController/Getsec?cur=" + $scope.edt.cur + "&aca=" + $scope.edt.aca + "&grd=" + grd).then(function (Getsec) {
                    $scope.Getsec = Getsec.data;
                });

            }

            $scope.Show_data = function ()
            {
                debugger;
                if ($scope.edt.cur == undefined || $scope.edt.cur == '' || $scope.edt.aca == undefined || $scope.edt.aca == '') {
                    swal('', 'Curriculum And Academic Year Are Mandatory...!!!');
                }
                else {
                    if ($scope.edt.em_login_code == undefined || $scope.edt.em_login_code == '') {
                        $scope.edt.em_login_code = '';
                    }
                    if ($scope.edt.grade == undefined || $scope.edt.grade == '') {
                        $scope.edt.grade = '';
                    }
                    if ($scope.edt.sec == undefined || $scope.edt.sec == '') {
                        $scope.edt.sec = '';
                    }

                    $scope.busy = true;
                    $scope.rgvtbl = false;

                    $http.get(ENV.apiUrl + "api/UpdateFeeCatController/GetStudent?cur=" + $scope.edt.cur + "&aca=" + $scope.edt.aca + "&grade=" + $scope.edt.grade + "&sec=" + $scope.edt.sec + "&enroll=" + $scope.edt.em_login_code).then(function (GetStudent) {
                        $scope.GetStudent = GetStudent.data;
                        $scope.busy = false;
                        $scope.rgvtbl = true;
                    });

                    $http.get(ENV.apiUrl + "api/UpdateFeeCatController/GetFeeCategory").then(function (GetFeeCategory) {
                        $scope.GetFeeCategory = GetFeeCategory.data;
                    });
                }
            }

            $scope.reset_data = function () {
                $scope.edt.cur = '';
                $scope.edt.aca = '';
                $scope.edt.grade = '';
                $scope.edt.sec = '';
                $scope.edt.em_login_code = '';
                $scope.GetStudent = '';
                $scope.busy = false;
                $scope.rgvtbl = true;
            }


            $scope.update_details = function () {
                $scope.busy = true;
                $scope.rgvtbl = false;
                var datasend = [];
                for (var i = 0; i < $scope.GetStudent.length; i++) {
                    if ($scope.GetStudent[i].fee_code!='')
                    {
                        var data = {
                        enroll: $scope.GetStudent[i].enroll
                            , fee_code: $scope.GetStudent[i].fee_code
                            , aca: $scope.edt.aca
                    };
                    datasend.push(data);
                }
                }
                debugger;
                $http.post(ENV.apiUrl + "api/UpdateFeeCatController/Update_Stud_fee_category", datasend).then(function (abcd) {

                    if (abcd.data == true)
                    {
                        swal('','Record Is Successfully Updated...!!!');
                    }
                    else if (abcd.data == false)
                        swal('', 'Record Is Not Updated...!!!');
                    else {
                        swal("Error-" + abcd.data)
                    }

                  

                });
                console.log(datasend);
                $scope.reset_data();
            }

            $(function () {
                $('#cmb_grade').multipleSelect({
                    width: '100%'
                });
            });


            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.CheckMultiple();
                }
            };


            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

         }])
})();