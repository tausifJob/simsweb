﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var curriculum_code, ac_year, grade, admino;
    var grandtotal = 0;
    var finanacecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AdmissionFeesGeisCont',
        ['$scope', '$state', '$rootScope', '$filter', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $filter, $timeout, gettextCatalog, $http, ENV) {

            var username = $rootScope.globals.currentUser.username;
            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.display = true;
            //$scope.checked = true;
            var dataforSave = [];
            $scope.table = false;
            $scope.chequeno = true;
            $scope.temp = [];
            $scope.grandtotal = 0;
            $scope.chequeno = true;
            $scope.referenceno = false;
            $scope.readonlybankname = true;
            $scope.readonlychequeno = true;
            $scope.readonlychequedate = true;




            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //Select Data SHOW


            $scope.countData = [
                    { val: 5, data: 5 },
                    { val: 10, data: 10 },
                    { val: 15, data: 15 },

            ]


            //$scope.size = function (str) {
            //    if (str == 5 || str == 10 || str == 15) {
            //        $scope.pager = true;
            //    }
            //    else {
            //        $scope.pager = false;
            //    }
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str;
            //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.AdmissionFeesData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }


            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            $scope.AdmissionGradeFeesData = [];

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //CheckBox

            $scope.CheckAllChecked = function () {
                debugger;
                $scope.grandtotal = 0;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.AdmissionGradeFeesData.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $scope.grandtotal = parseFloat($scope.grandtotal) + parseFloat($scope.AdmissionGradeFeesData[i].sims_grade_fee_amount);
                    }
                    $scope.temp.grandtotal = $scope.grandtotal;
                }
                else {
                    for (var i = 0; i < $scope.AdmissionGradeFeesData.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $scope.grandtotal = 0;
                        $scope.temp.grandtotal = 0;
                    }
                }

            }

            $scope.report_show = true;
            $scope.print = function (str) {
                debugger;
                //var data = {
                //    location: $scope.prov_doc_url,
                //    parameter: {
                //        fee_rec_no: str.sims_doc_no,

                //    },
                //    state: 'main.Sim521'
                //}
                //window.localStorage["ReportDetails"] = JSON.stringify(data)
                //$state.go('main.ReportCardParameter')

                $('#feemodal').modal('show');

                $scope.report_show = false;
               
                var data = {
                    location: $scope.prov_doc_url,
                    parameter: {
                        fee_rec_no: str.sims_doc_no,

                    },
                    state: 'main.Sim521'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)

                var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';

                $scope.parameters = {}

                $("#reportViewer1")
                               .telerik_ReportViewer({
                                   //serviceUrl: ENV.apiUrl + "api/reports/",
                                   serviceUrl: service_url,

                                   viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                   scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                   // Zoom in and out the report using the scale
                                   // 1.0 is equal to 100%, i.e. the original size of the report
                                   scale: 1.0,
                                   ready: function () {
                                       //this.refreshReport();
                                   }

                               });

                var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                reportViewer.reportSource({
                    report: "SimsReports." + $scope.prov_doc_url + ",SimsReports",
                    parameters: {
                        fee_rec_no: str.sims_doc_no,

                    },
                });

                //var rv = $("#reportViewer1").data("telerik_ReportViewer");
                //rv.commands.print.exec();

                setInterval(function () {

                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                    //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

                }, 1000);


                $timeout(function () {
                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                    //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


                }, 100)


                  
            }
            $scope.grandtotal = '0';
            $scope.checkonebyonedelete = function (str) {

                debugger;
                if (str.paying == '0' || str.paying == "" || str.paying == undefined) {
                    swal({ title: "Alert", text: "Please Enter value", showCloseButton: true, width: 380, });


                }
                
                if (str.no == true) {
                   // $scope.grandtotal = '0';
                    $scope.grandtotal = parseFloat($scope.grandtotal) + parseFloat(str.paying);

                }
                else {
                    $scope.grandtotal = parseFloat($scope.grandtotal) - parseFloat(str.paying);
                    str.paying = '0';
                }
                $scope.temp.grandtotal = $scope.grandtotal;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });
                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

          



            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=AdmFee").then(function (res) {
                $scope.prov_doc_url = res.data;
                console.log($scope.prov_doc_url);
            });


            $scope.getBankDetails = function () {
                $http.get(ENV.apiUrl + "api/AdmissionFees/getBankDeatails").then(function (getBankDetails) {
                    $scope.BankDetails = getBankDetails.data;
                    console.log($scope.BankDetails);
                })
            }


            //Bind Curriculum,Academic Year section
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCurriculum) {
                $scope.Curriculum = getCurriculum.data;
                $scope.temp['sims_cur_code'] = $scope.Curriculum[0].sims_cur_code
                $scope.getacademicYear($scope.Curriculum[0].sims_cur_code);
            });

            $scope.getacademicYear = function (str) {
                $scope.curriculum_code = str;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (getAcademicYear) {
                    $scope.AcademicYear = getAcademicYear.data;
                    $scope.temp['sims_academic_year'] = $scope.AcademicYear[0].sims_academic_year
                    console.log($scope.AcademicYear);
                    $scope.getGrade(str, $scope.AcademicYear[0].sims_academic_year);
                })
            }


            $scope.getGrade = function (str1, str2) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str1 + "&academic_year=" + str2).then(function (getAllGrades) {
                    $scope.AllGrades = getAllGrades.data;
                    console.log($scope.AllGrades);
                })
            }

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.AdmissionFeesData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.AdmissionFeesData;
                }
                $scope.makeTodos();
                main.checked = false;

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_admission_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_student_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_parent_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_grade_fee_amount == toSearch) ? true : false;
            }


            //DATA CANCEL
            $scope.reset = function () {

                $scope.table = false;
                $scope.display = true;
                $scope.temp = [];
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.getgrid();
            }

            $scope.cancel = function () {


                $scope.temp = {
                    sims_cur_code: $scope.curriculum_code,
                    sims_academic_year: $scope.ac_year,
                    sims_grade_code: $scope.grade,
                    admissionNo: $scope.admino,

                };
                $scope.temp.Checkno = "";
                $scope.temp.referenceno = "";
                $scope.temp.slma_pty_bank_id = "";
                $scope.temp.chequedate = "";
                $scope.temp.grandtotal = "";
                $scope.getgrid($scope.curriculum_code, $scope.ac_year, $scope.grade, $scope.admino);
                $scope.row1 = '';
                $scope.edit = {
                    copy1: 'Ca'
                }
                main = document.getElementById('mainchk');
                main.checked = false;

                $scope.CheckAllChecked();
                $scope.AdmissionGradeFeesData = [];

                // $scope.getgrid($scope.curriculum_code, $scope.ac_year, $scope.grade, $scope.admino);
            }

            //total
            $scope.total = function (str) {
                var v = document.getElementById(i);
                if (v.checked == true) {
                    $scope.grandtotal = parseInt($scope.grandtotal) + parseInt(str.sims_grade_fee_amount);
                }
                else {
                    $scope.grandtotal = parseInt($scope.grandtotal) - parseInt(str.sims_grade_fee_amount);
                }
                $scope.temp.grandtotal = $scope.grandtotal;
            }

            $scope.PrintReport = function () {
                debugger;

                //var data = {
                //    location: $scope.prov_doc_url,
                //    parameter: {
                //        fee_rec_no: $scope.docno,

                //    },
                //    state: 'main.Sim521'
                //}
                //window.localStorage["ReportDetails"] = JSON.stringify(data)
                //$state.go('main.ReportCardParameter')

                //$('#feemodal').modal({ backdrop: 'static', keyboard: false });
                $('#feemodal').modal('show');

                $scope.report_show = false;
                var rname = $scope.prov_doc_url;
                    var data = {
                        //location: 'Sims.SIMR51FeeRec',
                        location: rname,
                        parameter: {
                            fee_rec_no: $scope.docno,                            
                        },
                        state: 'main.Sim529',
                        ready: function () {
                            this.refreshReport();
                        },
                    }

                    window.localStorage["ReportDetails"] = JSON.stringify(data)

                    // });

                    //For Report Tool////////////////////////////////////////////////////////////////////////////////


                    try {
                        $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                        $scope.location = $scope.rpt.location;
                        $scope.parameter = $scope.rpt.parameter;
                        $scope.state = $scope.rpt.state;
                    }
                    catch (ex) {
                    }
                    var s;

                    //if ($http.defaults.headers.common['schoolId'] == 'dpsmis')
                    //    s = "SimsReports." + $scope.location + ",SimsReports";


                    //else
                    //    s = "SimsReports." + $scope.location + ",SimsReports" + $http.defaults.headers.common['schoolId'];


                    //  window.localStorage["Finn_comp"] = JSON.stringify(data)

                    s = "SimsReports." + $scope.location + ",SimsReports";

                    var url = window.location.href;
                    var domain = url.substring(0, url.indexOf(':'))
                    if ($http.defaults.headers.common['schoolId'] == 'sms') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    }

                    else {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    }
                    console.log(service_url);


                    $scope.parameters = {}

                    $("#reportViewer1")
                                   .telerik_ReportViewer({
                                       //serviceUrl: ENV.apiUrl + "api/reports/",
                                       serviceUrl: service_url,

                                       viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                       scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                       // Zoom in and out the report using the scale
                                       // 1.0 is equal to 100%, i.e. the original size of the report
                                       scale: 1.0,
                                       ready: function () {
                                           //this.refreshReport();
                                       }

                                   });





                    var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                    reportViewer.reportSource({
                        report: s,
                        parameters: data.parameter,
                    });

                    //var rv = $("#reportViewer1").data("telerik_ReportViewer");
                    //rv.commands.print.exec();

                    setInterval(function () {

                        $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                        $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                        //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

                    }, 1000);


                    $timeout(function () {
                        $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                        $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                        //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


                    }, 100)


    

            }

            $scope.clear1 = function () {
                debugger;
                //$scope.grandtotal = "";
                //$scope.temp.grandtotal = "";
                $scope.grandtotal = 0;
                $scope.temp.grandtotal = angular.copy($scope.grandtotal);
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
                $scope.currentPage = true;
                for (var i = 0; i < $scope.AdmissionGradeFeesData.length; i++) {
                    var v = document.getElementById(i);

                    if (v.checked == true) {
                        v.checked = false;
                        main.checked = false;
                        $('tr').removeClass("row_selected");
                    }


                }
            }
            // Submit Fees



            $scope.submitadmissionfeeCancel = function () {
                debugger;
                var Subfee = [];
                $scope.flag = false;
                $scope.flag1 = false;
                 
                if ($scope.temp.receiptdate == 'undefined') {

                    swal({ text: "Please Enter Receipt Date", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, })
                }


                if (!$scope.flag1) {


                    for (var i = 0; i < $scope.AdmissionGradeFeesData.length; i++) {
                        //var t = $scope.filteredTodos[i].uom_code;
                        var v = document.getElementById(i);
                        if (v.checked == true) {
                            $scope.flag = true;

                            
                            var Subfeeentry = ({
                                'sims_cur_code': $scope.AdmissionGradeFeesData[i].sims_cur_code,
                                'sims_academic_year': $scope.AdmissionGradeFeesData[i].sims_academic_year,
                                'sims_grade_code': $scope.AdmissionGradeFeesData[i].sims_grade_code,
                                'sims_fee_code': $scope.AdmissionGradeFeesData[i].sims_fee_code,
                                'sims_admission_no': $scope.AdmissionGradeFeesData[i].sims_admission_no,
                                'sims_doc_no': $scope.AdmissionGradeFeesData[i].sims_doc_no,

                                'sims_fee_amount': $scope.AdmissionGradeFeesData[i].paying,
                                'sims_user_name': username,
                                'sims_doc_date': $scope.temp.receiptdate,
                                'sims_fee_payment_mode': $scope.edt.copy1,
                                'sims_fee_cheque_bank_code': $scope.temp.slma_pty_bank_id,
                                'sims_fee_cheque_number': $scope.temp.Checkno,
                                'sims_fee_cheque_date': $scope.temp.chequedate,
                            });
                            Subfee.push(Subfeeentry);
                        }
                    }

                     
                    if ($scope.flag) {

                        swal({
                            title: '',
                            text: "Are you sure you want to cancel receipt ?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                $http.post(ENV.apiUrl + "api/AdmissionFees/insertAdmissionFeesCancel", Subfee).then(function (res) {
                                    $scope.docno = res.data;
                                    var cnt = 0;
                                    if ($scope.docno) {
                                        $scope.btn_cancel_dis = true;
                                        swal({ text: "Receipt Cancelled Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid($scope.curriculum_code, $scope.ac_year, $scope.grade, $scope.admino);

                                                $scope.clear1();
                                                $('#cancel_model').modal('hide');

                                            }


                                        });
                                    }
                                    else {
                                        $scope.btn_cancel_dis = false;
                                        swal({
                                            text: "Receipt Not Cancelled. ", imageUrl: "assets/img/close.png",
                                            showCloseButton: true, width: 380,
                                        }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.clear1();


                                            }

                                        });
                                    }

                                });
                            }
                            else {
                                $scope.grandtotal = 0;
                                $scope.temp.grandtotal = 0;
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }
                                for (var i = 0; i < $scope.AdmissionGradeFeesData.length; i++) {
                                    var v = document.getElementById(i);

                                    if (v.checked == true) {
                                        v.checked = false;
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }


                                }

                            }

                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                    }
                }

                $scope.currentPage = str;
            }



            $scope.submitadmissionfee = function () {

                var Subfee = [];
                $scope.flag = false;
                $scope.flag1 = false;

                if ($scope.temp.receiptdate == null || $scope.temp.receiptdate == undefined) {
                    swal({ title: "Alert", text: "Please select Receipt Date", showCloseButton: true, width: 380, });
                    return;
                }

                if ($scope.edt.copy1 == 'Ch' || $scope.edt.copy1 == 'BT' || $scope.edt.copy1 == 'Cc') {
                    if ($scope.edt.copy1 == 'Ch') {
                        if ($scope.temp.slma_pty_bank_id == undefined || $scope.temp.slma_pty_bank_id == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please select bank", showCloseButton: true, width: 380, });
                            return;

                        }
                        else if ($scope.temp.chequedate == null || $scope.temp.chequedate == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Enter Cheque Date", showCloseButton: true, width: 380, });
                            return;
                        }
                        else if ($scope.temp.Checkno == null || $scope.temp.Checkno == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Enter Cheque Number", showCloseButton: true, width: 380, });
                            return;
                        }
                        else {
                            $scope.flag1 = false;
                        }
                    }
                    else if ($scope.edt.copy1 == 'Cc') {
                        if ($scope.temp.slma_pty_bank_id == undefined || $scope.temp.slma_pty_bank_id == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please select bank", showCloseButton: true, width: 380, });
                            return;

                        }
                        else if ($scope.temp.credit_card_no == null || $scope.temp.credit_card_no == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Enter Transaction Number", showCloseButton: true, width: 380, });
                            return;
                        }
                        else {
                            $scope.flag1 = false;
                        }
                    }
                    else {
                        if ($scope.temp.referenceno == null || $scope.temp.referenceno == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Enter Reference Number", showCloseButton: true, width: 380, });
                            return;
                        }
                        else {
                            $scope.flag1 = false;
                        }

                    }

                }
                if (!$scope.flag1) {
                    if ($scope.edt.copy1 == 'BT') {
                        $scope.sims_fee_cheque_number = $scope.temp.referenceno;
                    }
                    else if ($scope.edt.copy1 == 'Cc') {
                        $scope.sims_fee_cheque_number = $scope.temp.credit_card_no;
                    }
                    else if ($scope.edt.copy1 == 'Ch') {
                        $scope.sims_fee_cheque_number = $scope.temp.Checkno;
                    }
                    else { $scope.sims_fee_cheque_number = null; }


                    for (var i = 0; i < $scope.AdmissionGradeFeesData.length; i++) {
                        //var t = $scope.filteredTodos[i].uom_code;
                        var v = document.getElementById(i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var Subfeeentry = ({
                                'sims_cur_code': $scope.AdmissionGradeFeesData[i].sims_cur_code,
                                'sims_academic_year': $scope.AdmissionGradeFeesData[i].sims_academic_year,
                                'sims_grade_code': $scope.AdmissionGradeFeesData[i].sims_grade_code,
                                'sims_fee_code': $scope.AdmissionGradeFeesData[i].sims_fee_code,
                                'sims_grade_fee_amount': $scope.AdmissionGradeFeesData[i].sims_grade_fee_amount,
                                'sims_admission_no': $scope.temp.sims_admission_no,

                                'sims_fee_payment_mode': $scope.edt.copy1,
                                'sims_fee_cheque_bank_code': $scope.temp.slma_pty_bank_id,
                                'sims_user_name': username,
                                'sims_fee_cheque_number': $scope.sims_fee_cheque_number,
                                'sims_fee_cheque_date': $scope.temp.chequedate,
                                'sims_doc_date': $scope.temp.receiptdate,

                            });
                            Subfee.push(Subfeeentry);
                        }
                    }


                    if ($scope.flag) {

                        swal({
                            title: '',
                            text: "Are you sure you want to submit fees?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                $http.post(ENV.apiUrl + "api/AdmissionFees/insertAdmissionFees", Subfee).then(function (res) {
                                    $scope.docno = res.data;
                                    var cnt = 0;
                                    if (!$scope.docno == "") {
                                        $scope.AdmissionGradeFeesData1 = angular.copy($scope.AdmissionGradeFeesData);
                                        for (var i = 0; i < $scope.AdmissionGradeFeesData.length; i++) {
                                            cnt++;
                                            var v = document.getElementById(i);
                                            if (v.checked == true) {
                                                $scope.AdmissionGradeFeesData1.splice(cnt - 1, 1);
                                                cnt--;

                                            }
                                        }

                                        $scope.AdmissionGradeFeesData = angular.copy($scope.AdmissionGradeFeesData1);

                                        $scope.printreceipt = true;
                                        $scope.btn_sub_dis = true;
                                        swal({ text: "Fee Submited Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.clear1();
                                            }

                                        });
                                    }
                                    else {
                                        $scope.btn_sub_dis = false;
                                        swal({ text: "Fee Not Submited. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.clear1();

                                            }

                                        });
                                    }

                                });
                            }
                            else {
                                $scope.grandtotal = 0;
                                $scope.temp.grandtotal = 0;
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }
                                for (var i = 0; i < $scope.AdmissionGradeFeesData.length; i++) {
                                    var v = document.getElementById(i);

                                    if (v.checked == true) {
                                        v.checked = false;
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }


                                }

                            }

                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                    }
                }

                $scope.currentPage = str;
            }

            $scope.ChkDate = function (noissueafter) {

                var date = new Date();
                $scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy/MM/dd');
                var month1 = noissueafter.split("/")[0];
                var day1 = noissueafter.split("/")[1];
                var year1 = noissueafter.split("/")[2];
                var new_end_date = year1 + "/" + month1 + "/" + day1;

                if (new_end_date < $scope.ddMMyyyy) {
                    //$rootScope.strMessage = "Please Select Today or Next Date";
                    //$('#message').modal('show');
                    swal({ title: "Alert", text: "Please Select Today or Next Date", showCloseButton: true, width: 380, });
                    $scope.temp.chequedate = "";
                }

            }


            //DATA EDIT
            $scope.edit = function (str) {



                $scope.temp = {
                    sims_student_full_name: str.sims_student_full_name,
                    sims_grade_name_en: str.sims_grade_name_en,
                    sims_admission_no: str.sims_admission_no,
                };

            }

            $scope.edt =
                {
                    copy1: 'Ca'
                }



            $scope.collectshow = function (str) {
                $scope.printreceipt = false;

                var date = new Date();
                var month = (date.getMonth() + 1);
                var day = date.getDate();

                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                //$scope.receipt_date= date.getFullYear() + '-' + (month) + '-' + (day);
                $scope.receipt_date = (day) + '-' + (month) + '-' + date.getFullYear();

                if (str.sims_doc_status_content == 'Collect') {

                    $scope.temp = {
                        Checkno: "",
                        referenceno: "",
                        slma_pty_bank_id: "",
                        chequedate: "",

                    }
                    grandtotal = 0;
                    $scope.grandtotal = 0;
                    $scope.temp = {
                        sims_student_full_name: str.sims_student_full_name,
                        sims_grade_name_en: str.sims_grade_name_en,
                        sims_admission_no: str.sims_admission_no,
                        sims_cur_code: str.sims_cur_code,
                        sims_academic_year: str.sims_academic_year,
                        sims_grade_code: str.sims_grade_code,
                        receiptdate: $scope.receipt_date
                    };
                    $scope.edt =
               {
                   copy1: 'Ca'
               }

                    $scope.radioclick('Ca');


                    $http.get(ENV.apiUrl + "api/AdmissionFees/getAdmissionGradeFees?cur_code=" + str.sims_cur_code + "&academic_year=" + str.sims_academic_year + "&grade_code=" + str.sims_grade_code + "&admission_no_name=" + str.sims_admission_no).then(function (res1) {

                        $scope.AdmissionGradeFeesData = res1.data;

                    });

                    $scope.getBankDetails();
                    //$scope.copy1.checked = true;
                    $('#myModal1').modal('show');
                    $scope.btn_sub_dis = false;
                }
                else {
                    $('#myModal1').modal('show');
                }
            }



            $scope.cancel_addmission = function (str) {
                $scope.printreceipt = false;
                debugger;
                var date = new Date();
                var month = (date.getMonth() + 1);
                var day = date.getDate();

                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                //$scope.receipt_date= date.getFullYear() + '-' + (month) + '-' + (day);
                $scope.receipt_date = (day) + '-' + (month) + '-' + date.getFullYear();
                //if (str.sims_doc_status_content == 'Collect')
                {

                    $scope.temp = {
                        Checkno: "",
                        referenceno: "",
                        slma_pty_bank_id: "",
                        chequedate: ""
                        

                    }
                    grandtotal = 0;
                    $scope.grandtotal = 0;
                    $scope.temp = {
                        sims_student_full_name: str.sims_student_full_name,
                        sims_grade_name_en: str.sims_grade_name_en,
                        sims_admission_no: str.sims_admission_no,
                        sims_cur_code: str.sims_cur_code,
                        sims_academic_year: str.sims_academic_year,
                        sims_grade_code: str.sims_grade_code,
                        receiptdate: $scope.receipt_date
                    };
                    $scope.edt =
               {
                   copy1: 'Ca'
               }

                    $scope.radioclick('Ca');


                    $http.get(ENV.apiUrl + "api/AdmissionFees/getAdmissionGradeFeesCancel?cur_code=" + str.sims_cur_code + "&academic_year=" + str.sims_academic_year + "&grade_code=" + str.sims_grade_code + "&admission_no_name=" + str.sims_admission_no).then(function (res1) {

                        $scope.AdmissionGradeFeesData = res1.data;

                    });

                    $scope.getBankDetails();
                    //$scope.copy1.checked = true;
                    $('#cancel_model').modal('show');
                    $scope.btn_cancel_dis = false;
                }
                //else {
                //    $('#myModal1').modal('show');
                //}
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });



            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                $scope.temp[name1] = year + "/" + month + "/" + day;
            }


            $scope.radioclick = function (str) {

                if (str == 'Ca') {
                    $scope.chequeno = true;
                    $scope.referenceno = false;

                    $scope.readonlybankname = true;
                    $scope.readonlychequeno = true;
                    $scope.readonlychequedate = true;
                    $scope.credit_card = false;
                }

                else if (str == 'Ch') {
                    $scope.readonlybankname = false;
                    $scope.readonlychequeno = false;
                    $scope.readonlychequedate = false;
                    $scope.chequeno = true;
                    $scope.referenceno = false;
                    $scope.credit_card = false;

                }
                else if (str == 'Cc') {
                    $scope.chequeno = false;
                    $scope.referenceno = false;

                    $scope.readonlybankname = false;
                    $scope.readonlychequeno = true;
                    $scope.readonlychequedate = true;

                    $scope.credit_card = true;


                }

                else if (str == 'BT') {
                    $scope.chequeno = false;
                    $scope.referenceno = true;
                    $scope.readonlybankname = false;
                    $scope.readonlychequeno = true;
                    $scope.readonlychequedate = true;
                    $scope.credit_card = false;
                }

            }

            $scope.getgrid = function (cur_code, acad_yr, grade_code, adm_no) {
                debugger;
                if ($scope.temp.sims_grade_code==null)
                {
                    grade_code = 'undefined';
                }
                
                $('#loader').modal({ backdrop: 'static', keyboard: false });
                $scope.ac_year = acad_yr;
                $scope.grade = grade_code;
                $scope.admino = adm_no;
                $http.get(ENV.apiUrl + "api/AdmissionFees/getAdmissionFeesGeis?cur_code=" + cur_code + "&academic_year=" + acad_yr + "&grade_code=" + grade_code + "&admission_no_name=" + adm_no).then(function (res1) {

                    $scope.AdmissionFeesData = res1.data;
                    if ($scope.AdmissionFeesData.length > 0) {
                        $scope.table = true;
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.AdmissionFeesData.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.AdmissionFeesData.length, data: 'All' })
                        }
                        $scope.totalItems = $scope.AdmissionFeesData.length;
                        $scope.todos = $scope.AdmissionFeesData;
                        $scope.makeTodos();
                    }
                    else {
                        $scope.table = false;
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });

                    }

                    $('#loader').modal('hide');
                    $(".modal-backdrop").removeClass("modal-backdrop");
                });

            }

            $scope.getgrid1 = function () {


                $http.get(ENV.apiUrl + "api/AdmissionFees/getAdmissionGradeFees?cur_code=" + temp.sims_cur_code + "&academic_year=" + temp.sims_academic_year + "&grade_code=" + temp.sims_grade_code + "&admission_no_name=" + temp.sims_admission_no).then(function (res1) {

                    $scope.AdmissionGradeFeesData = res1.data;
                    //$scope.totalItems = $scope.AdmissionGradeFeesData.length;
                    //$scope.todos = $scope.AdmissionGradeFeesData;
                    //$scope.makeTodos();


                });

            }

            $scope.checkAmount = function (str) {
                debugger;

                if (str.paying == '-')
                {
                    swal({ title: "Alert", text: "Amount Should be Greater than" + str.sims_grade_fee_amount, width: 300, height: 200 });
                    str.paying = 0
                }
                if (parseFloat(0) < parseFloat(str.paying) > parseFloat(str.sims_grade_fee_amount)) {
                    swal({ title: "Alert", text: "Amount Should be less than" + str.sims_grade_fee_amount, width: 300, height: 200 });
                    str.paying = 0
                }
                
            }

        }])

})();
