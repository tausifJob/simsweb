﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Student');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('UpdateStudentImageCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.search_btn = true;
            $scope.clr_btn = true;
            $scope.Update_btn = false;
            $scope.newdisplay = false;
            $scope.table = true;
            $scope.grid = false;
            $scope.pager = true;
            $scope.ImgLoaded = false;
            $scope.imgBoxWihoutImg = false;

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.imageUrl = ENV.apiUrl + '/Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/';
           

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.studImg = $scope.searched($scope.studImg, $scope.searchText);

                if ($scope.searchText != '') {
                    $scope.studImg = $scope.studImg;
                }
                else {
                    $scope.studImg = $scope.fetchedRecords;
                }

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_enrollment_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_student_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_student_ea_number == toSearch) ? true : false;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.stud_Stud_Img_Details_fun = function (curcode, acYear, gCode, sCode) {

                //$scope.chk = {};
                //$scope.chk['showWithoutImage'] = false;

                $scope.grid = true;
                $scope.imgBox = true;
                $scope.imgBoxWihoutImg = false;
                $scope.upldButtonforWithoutimg = false;
                $scope.upldButton = true;
                $scope.ImageView = false;

                var k = document.getElementById('kbh');

                $http.get(ENV.apiUrl + "api/StudentImg/getStudentImg?cur_code=" + curcode + "&academic_year=" + acYear + "&grade_code=" + gCode + "&section_code=" + sCode + "&flag=" + k.checked).then(function (StudimgDetails) {
                    $scope.studImg = StudimgDetails.data;
                    $scope.fetchedRecords = StudimgDetails.data;

                    setTimeout(function () {
                        angular.forEach($scope.studImg, function (value, key) {
                            var num = Math.random();
                            var imgSrc = ENV.apiUrl + '/Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/' +value.sims_student_img + "?v=" + num;
                            //document.getElementById(value.sims_student_enroll_number).setAttribute('src', imgSrc);
                            $("#" + value.sims_enrollment_number).attr('src', imgSrc);
                        });
                    }, 1000);

                    if (StudimgDetails.data.length > 0) { }

                    else {
                        $scope.ImageView = true;
                    }

                    if (k.checked == true) {

                        $scope.imgBox = false;
                        $scope.imgBoxWihoutImg = false;
                        $scope.upldButtonforWithoutimg = true;
                        $scope.upldButton = false;
                    }
                    else { $scope.grid = true; }

                    if ($scope.studImg.length > 0) {
                        $scope.table = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.studImg.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.studImg.length, data: 'All' })
                        }
                    }
                    else {
                        swal({ text: "Records not found", width: 300, showCloseButton: true });
                    }

                });
            }

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.temp = {
                    'sims_cur_code': $scope.curriculum[0].sims_cur_code
                }
                $scope.getAccYear($scope.temp.sims_cur_code);
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.temp.sims_cur_code).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                        'sims_academic_year': $scope.Acc_year[0].sims_academic_year
                    }
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                });
            }

            $scope.getGrade = function (curCode, accYear) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $(function () {
                $('#cmb_grade_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.getSection = function (curCode, gradeCode, accYear) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.temp.sims_cur_code + "&grade_code=" + $scope.temp.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $(function () {
                $('#cmb_section_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.sims_student_image = $scope.imageUrl + $scope.sims_student_img;

            var formdata = new FormData();

            $scope.getTheFiles = function ($files) {
                $scope.filesize = true;

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);

                    var i = 0;
                    if ($files[i].size > 800000) {
                        $scope.filesize = false;
                        $scope.edt.photoStatus = false;
                        swal({ text: "File Should Not Exceed 800Kb.", imageUrl: "assets/img/notification-alert.png", widht: 380, showCloseButton: true });
                    }
                    else { }
                });
            };

            $scope.file_changed = function (element, info) {

                var photofile = element.files[0];
                $scope.photo_filename = (photofile.name);
                $scope.sims_student_img = $scope.photo_filename;
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.data['prev_img'] = e.target.result;

                    });
                };
                reader.readAsDataURL(photofile);
                $scope.main_loader_show = true;
                var request = {
                    method: 'POST',
                    url: ENV.apiUrl + 'api/file/upload?filename=' + $scope.data.sims_enrollment_number + '&location=StudentImages',
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };
                $http(request).success(function (d) {
                    var dataupdate = [];
                    var data = {};
                    data ={
                        sims_enrollment_number: $scope.data.sims_enrollment_number,
                        sims_student_img: d,
                        opr: 'U',
                    }
                    dataupdate.push(data);
                    $scope.data['sims_student_img'] = d;
                    if (dataupdate.length > 0) {
                        $http.post(ENV.apiUrl + "api/StudentImg/CUDStudentImg", dataupdate).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                $scope.main_loader_show = false;
                                swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, showCloseButton: true });
                            }
                            else if ($scope.msg1 == false) {
                                $scope.main_loader_show = false;
                                swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, showCloseButton: true });
                            }
                            else {
                                $scope.main_loader_show = false;
                                swal("Error-" + $scope.msg1)
                            }
                            //});
                            $scope.stud_Stud_Img_Details_fun($scope.temp.sims_cur_code, $scope.temp.sims_academic_year, $scope.temp.sims_grade_code, $scope.temp.sims_section_code);
                        })
                        $scope.table = true;
                        $scope.newdisplay = false;
                        dataupdate = [];
                        angular.forEach(
                          angular.element("input[type='file']"),
                          function (inputElem) {
                              angular.element(inputElem).val(null);
                          });
                    }
                    else {
                        swal({ text: "Change Atleast one Record to Update", imageUrl: "assets/img/notification-alert.png", width: 380, showCloseButton: true });
                        angular.forEach(
                          angular.element("input[type='file']"),
                          function (inputElem) {
                              angular.element(inputElem).val(null);
                          });
                    }

                });             
            };

            $scope.image_click = function (info) {
                $scope.imgBoxWihoutImg = false;
                info.ischange = true;
                $scope.data = '';
                $scope.data = info;
            }

            $scope.image_click1 = function (info) {
                $('#upload_img').modal('show');
                $scope.imgBoxWihoutImg = true;
                info.ischange = true;
                $scope.data = '';
                $scope.data = info;
            }

            $scope.update = function () {
                var dataupdate = [];
                var data = {};
                for (var k = 0; k < $scope.studImg.length; k++) {
                    if ($scope.studImg[k].ischange == true) {
                        data ={
                           sims_enrollment_number: $scope.studImg[k].sims_enrollment_number,
                           sims_student_img: $scope.studImg[k].sims_student_img,
                           opr: 'U',
                       }
                        dataupdate.push(data);
                    }
                }
                if (dataupdate.length > 0) {
                    $http.post(ENV.apiUrl + "api/StudentImg/CUDStudentImg", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        // $http.get(ENV.apiUrl + "api/StudentImg/getAllStudentImg").then(function (res1) {
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, showCloseButton: true });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, showCloseButton: true });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        //});
                        $scope.stud_Stud_Img_Details_fun($scope.temp.sims_cur_code, $scope.temp.sims_academic_year, $scope.temp.sims_grade_code, $scope.temp.sims_section_code);
                    })
                    $scope.table = true;
                    $scope.newdisplay = false;
                    dataupdate = [];
                }
                else {
                    swal({ text: "Change Atleast one Record to Update", imageUrl: "assets/img/notification-alert.png", width: 380, showCloseButton: true });
                }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.newdisplay = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.dest_code = "";
                $scope.temp.country_code = "";
                $scope.temp.dest_name = "";
            }

            $scope.clear = function () {
                debugger
                try {
                    $("#cmb_grade_code").multipleSelect("uncheckAll");                    
                }
                catch (e) {
                }
                $("#cmb_section_code").multipleSelect("uncheckAll");
                
                $http.get(ENV.apiUrl + "api/StudentEA/getAllStudentEA").then(function (res1) {
                    $scope.studEA = res1.data;
                    $scope.totalItems = $scope.studEA.length;
                    $scope.todos = $scope.studEA;
                    $scope.makeTodos();
                });
                $scope.table = true;
                $scope.pager = true;

            }
            
        }])

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
     }])

})();
