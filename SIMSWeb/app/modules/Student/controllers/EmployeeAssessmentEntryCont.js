﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeAssessmentEntryCont',
        ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.display = true;
            $scope.grid = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.flag = true;
            var str, cnt;
            var admdetails = [];
            $scope.filesize = true;
            //  $scope.div_head = false;
            var date = new Date();
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');
            $scope.reportHeadig = [];
            $scope.assessmentscheduleData = [];

            $scope.obj1 = [], $scope.obj3 = [];

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            //$scope.edt =
            //    {
            //        sims_assesment_date: $scope.ddMMyyyy
            //    }

            // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {

                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;

                $scope.propertyName = propertyName;
            };

            $scope.countData = [

                  { val: 5, data: 5 },
                  { val: 10, data: 10 },
                  { val: 15, data: 15 },
            ]

    

           

          

            $scope.getAssesmentEmployeeDate = function () {
                $http.get(ENV.apiUrl + "api/TeacherAssessmentEntry/GetAssesmentEmployeeDate").then(function (res) {
                    $scope.assesmentDate_data = res.data;
                });
            }
            $scope.getAssesmentEmployeeDate();


            $scope.edt = {};

            //$scope.getAssessmentTeacherEntryData = function () {
            //    debugger;
            //    var obj = {};
            //    $scope.reportHeadig = [];
            //    $scope.assessmentscheduleData = [];
            //    $scope.filteredTodos = [];

            //    if ($scope.edt['sims_assesment_date'] == null) {
            //        swal({ title: "Alert", text: "Please select Assesment Date.", showCloseButton: true, width: 380, });
            //    }
            //    else {
            //        //?cur_code=" + $scope.edt.curr_code + "&acad_yr=" + $scope.edt.academic_year + "&grade_code=" + $scope.edt.grade_code + "&assessment_date=" + $scope.edt.sims_assesment_date
            //        var a = { opr: 'S',sims_assesment_date: $scope.edt.sims_assesment_date };

            //        $http.post(ENV.apiUrl + "api/TeacherAssessmentEntry/assesmentemployeeEntry", a).then(function (response) {
            //            $scope.assessmentscheduleData = response.data.table;
            //            $scope.pager = true;

            //            //  console.log($scope.assessmentscheduleData);
            //            $scope.headers = response.data.table[0];

            //            angular.forEach($scope.headers, function (value, key) {
            //                var obj = {
            //                    heading: key
            //                }
            //                $scope.reportHeadig.push(obj);
            //            });


            //            $scope.new_arr = [];
            //            setTimeout(function () {
            //                angular.forEach($scope.assessmentscheduleData, function (val, k) {
            //                    console.log(val, k);
            //                    angular.forEach(val, function (value, key) {
            //                        console.log(value, key);
            //                        var ob = {
            //                            sims_attendance_code: key,
            //                            sims_attendance_cout: value
            //                        }
            //                        if ('admission_Number' == key) {
            //                            $("#" + key + '-' + k).attr('disabled', true);
            //                            // console.log('key', key +k);
            //                        }
            //                        //if ('Student_Name' == key)
            //                        //{
            //                        //    $("#" + key + '-' + k).attr('disabled', true);
            //                        //}
            //                    });
            //                });
            //            }, 500);



            //            if ($scope.assessmentscheduleData.length == 0) {
            //                swal({ title: 'Alert', text: "No record(s) found.", showCloseButton: true, width: 450, height: 200 });
            //            }
            //            else {
            //                $scope.grid = true;
            //                $scope.disabledGenerate = false;
            //                $scope.totalItems = $scope.assessmentscheduleData.length;
            //                $scope.todos = $scope.assessmentscheduleData;
            //                $scope.makeTodos();
            //            }

            //        });
            //    }
            //}

            $scope.getAssessmentTeacherEntryData = function () {
                $scope.filteredTodos = [];
                debugger;
                $http.get(ENV.apiUrl + "api/TeacherAssessmentEntry/GetEmployeeAssesmentSchedule?date=" + $scope.edt.sims_assesment_date).then(function (res) {
                    $scope.assessmentscheduleData = res.data;
                    if ($scope.assessmentscheduleData.length > 0) {
                        $scope.table = true;
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.assessmentscheduleData.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.assessmentscheduleData.length, data: 'All' })
                        }
                        $scope.totalItems = $scope.assessmentscheduleData.length;
                        $scope.todos = $scope.assessmentscheduleData;
                        $scope.grid = true;
                        $scope.makeTodos();
                    }
                    else {
                        $scope.table = false;
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                        $scope.filteredTodos = [];
                    }

                });

            }
            $scope.makeTodos = function () {
                var rem = parseFloat($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseFloat(begin) + parseFloat($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.cancel = function () {
                $scope.filteredTodos = [];
                $scope.assessmentscheduleData = [];
                $scope.pager = false;
            }

            $scope.datasend = [];



            $scope.save = function () {
                debugger;
                $scope.postData = [];
                $scope.disabledGenerate = true;

                if ($scope.edt['sims_assesment_date'] == null) {
                    swal({ title: "Alert", text: "Please select Assesment Date.", showCloseButton: true, width: 380, });
                }
                else {
                    //var data = ({
                        
                    //    opr: 'I'
                    //});
                    //data2.push(data);
                    //   console.log($scope.postData);


                    $http.post(ENV.apiUrl + "api/TeacherAssessmentEntry/CURDassesmentEmployeeEntry", $scope.filteredTodos).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.postData = [];
                        if ($scope.msg1 == true) {
                            swal({ text: 'Update successful', width: 320, showCloseButton: true });
                            //  $scope.display = true;
                            //  $scope.grid = false;
                            //  $scope.disabledGenerate = false;
                            $scope.getAssessmentTeacherEntryData();
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: 'Update Not successful. ', width: 320, showCloseButton: true });
                            $scope.display = true;
                            $scope.grid = false;
                            $scope.disabledGenerate = false;
                            $scope.getAssessmentTeacherEntryData();
                        }
                        else {
                            swal("Error-" + $scope.msg1);
                            $scope.getAssessmentTeacherEntryData();
                        }

                    });
                }
            }

            //$scope.Checkval = function (val, index) {
            //    val.opr = "I";
            //    $scope.values = val;

            //    $scope.chekindex = index;
            //    var main = document.getElementById(index + val['admission_Number']);

            //    if (main.checked == true) {
            //        var data = {
            //            admission_no: val['admission_Number'],
            //            student_name: val['student Name'],
            //            admission_date: val['admission Date'],
            //            opr: "I",
            //        }
            //        $scope.datasend.push(data);

            //    } else {
            //        main.checked = false;
            //        var rm = del.indexOf(index);
            //        $scope.datasend.splice(rm, 1);
            //    }
            //    $scope.filecount = "You Selected " + $scope.datasend.length + " Records";

            //    console.log($scope.datasend);
            //}



            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
        }])

})();