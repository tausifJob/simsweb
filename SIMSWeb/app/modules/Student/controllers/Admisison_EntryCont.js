﻿(function () {
    'use strict';
    var opr = '';
    var itemcode = [];
    var main;
    var data1 = [];
    var data = [];
    var itemset;
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('Admisison_EntryCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
           
            $scope.edt = {};

            $scope.save_btn_disable = false;

            $http.get(ENV.apiUrl + "api/common/ProspectDashboard/GetAcademicYear").then(function (res) {
                $scope.academicYears = res.data
                $scope.edt['academic_year'] = res.data[0].academic_year;

               // $scope.getAcademicYear($scope.edt['academic_year']);

              
            });

        //    var school_code = $http.defaults.headers.common['schoolId'];

            $http.get("https://oa.mograsys.com/" + $http.defaults.headers.common['schoolId'] +  "/api/common/Admission/Get_Course").then(function (res) {
                $scope.course_lst = res.data;
            });

            //$scope.getAcademicYear = function (currYear) {
            //    $http.get(ENV.apiUrl + "api/common/ProspectDashboard/GetAllGradeSec?currYear=" + currYear).then(function (res) {
            //        $scope.allgradesSection = res.data
            //    });
            //}

           

            $scope.RegisterUser = function (edt, v) {
                if (v) {
                    $scope.save_btn_disable = true;
                    $http.post(ENV.apiUrl + "api/common/ProspectDashboard/AddEnquiryNew", edt).success(function (data) {
                        $scope.p_data = data;

                        if ($scope.p_data) {
                          
                            $scope.edt = {}
                            swal({ title: "Registered Successfully", text: "Email sent to registered email address. ", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            $scope.save_btn_disable = false;
                            $scope.myForm.$setPristine();
                            $scope.myForm.$setUntouched();
                        } else {
                           
                            swal({ title: "Alert", text: " Not Registered", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                            $scope.save_btn_disable = false;
                        }

                    });

                }
            }

            $scope.mobile_change = function () {
                $scope.p_data = {};
                debugger
                $scope.p_data['father_mobile'] = $scope.edt.mobile;
                $http.post("https://oa.mograsys.com/" + $http.defaults.headers.common['schoolId'] + "/api/Comn/CheckMobileID", $scope.p_data).then(function (res) {

                    if (res.data == "true") {
                        $scope.edt.mobile = "";
                        swal({ title: "Alert", text: "Mobile No already registered", width: 380, imageUrl: "assets/img/notification-alert.png", });
                    }
                    else {
                        if (res.data == "false") {

                        }
                    }
                });
            }
            

            $scope.getEmailId = function () {

                $scope.p_data = {};
                $scope.p_data['sims_admission_parent_email'] = $scope.edt.email;

                if ($scope.p_data.sims_admission_parent_email != undefined || $scope.p_data.sims_admission_parent_email != "") {

                    $http.post("https://oa.mograsys.com/" + $http.defaults.headers.common['schoolId'] + "/api/Comn/CheckEmailID", $scope.p_data).then(function (res) {

                        if (res.data == "true") {
                            $scope.edt.email = "";
                            swal({ title: "Alert", text: "Email Address Already Exist", width: 380, imageUrl: "assets/img/notification-alert.png", });
                        }
                        else {
                            if (res.data == "false") {

                            }
                        }
                        $rootScope.BUSY.IsBusy = false;
                    }, function (err) {
                       
                    });
                }
            }

         }])
})();





