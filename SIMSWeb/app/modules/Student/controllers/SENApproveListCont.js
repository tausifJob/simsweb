﻿(function () {

    var main;
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SENApproveListCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = 'All';
            $scope.pageindex = "0";
            $scope.table = true;
            $scope.search_btn = true;
            $scope.clr_btn = true;
            $scope.pager = true;
            $scope.save_btn = true;
            var loginuser = $rootScope.globals.currentUser.username;
            $scope.users = loginuser;
            $scope.sims_admission_number_save = "";
            $scope.sims_admission_grade_code_save = "";
            $scope.sims_admission_academic_year_save = "";
            $scope.sims_admission_cur_code_save = "";
            $scope.sims_sen_transaction_number = "";
            $scope.student_name = "";
            $scope.student_dob = "";
            $scope.student_add_date = "";
            $scope.student_class = "";
            $scope.student_parent_no = "";
            $scope.block_1 = [];
            $scope.block_2 = [];
            $scope.block_3 = [];

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    $scope.filteredTodos[i].icon = 'fa fa-plus-circle';
                }

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Stud_Details, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Stud_Details;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }


            //check all 
            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk11');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            //checked one by one
            var ind;
            var data = [];
            $scope.checkonebyonedelete = function (str, index) {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                        //str.status = true;
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                        //str.status = false;
                    }
                });
                str.status = str.datastatus;
                ind = index;
                //data = str;
            }



            $scope.getcur = function () {
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                    $scope.curriculum = AllCurr.data;
                    $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                    $scope.getAccYear($scope.edt.sims_cur_code);
                });
            }

            $scope.getcur();

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                    $scope.getGrade($scope.edt.sims_cur_code, $scope.temp.sims_academic_year);
                    $scope.health();
                });
            }

            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    console.log($scope.Grade_code);
                });
            }

            $scope.stud_Details_fun = function (curcode, acYear, gCode, admission_status) {
                debugger;
                cur = $scope.edt.sims_cur_code;
                year = $scope.temp.sims_academic_year;
                grade = $scope.temp.sims_grade_code;
                section = $scope.temp.sims_section_code;
                $http.get(ENV.apiUrl + "api/SENAdmClass/getApproveList?sims_cur_code=" + curcode + "&sims_academic_year=" + acYear + "&sims_grade_code=" + gCode + "&value=" + admission_status).then(function (StudDetails) {
                    $scope.Stud_Details = StudDetails.data;
                    $scope.totalItems = $scope.Stud_Details.length;
                    $scope.todos = $scope.Stud_Details;
                    $scope.makeTodos();
                });
            }

            $scope.update = function () {
                debugger;
                var send = [];
                code = [];
                $scope.flag = false;

                $http.post(ENV.apiUrl + "api/SENAdmClass/CUDStudentSenApprove", $scope.filteredTodos).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }

                    $scope.stud_Details_fun($scope.edt.sims_cur_code, $scope.temp.sims_academic_year, $scope.temp.sims_grade_code);
                });

            }


            $scope.call_modal = function (str) {
                var right = document.getElementById('rightdiv').style.height;
                var left = document.getElementById('leftdiv').style.height;
                if (left > right) {
                    document.getElementById('rightdiv').style.height = left;
                }
                else {
                    document.getElementById('leftdiv').style.height = right;
                }
                debugger
                $scope.sims_admission_number_save = str.sims_admission_number;
                $scope.sims_admission_grade_code_save = str.sims_admission_grade_code;
                $scope.sims_admission_academic_year_save = str.sims_admission_academic_year;
                $scope.sims_admission_cur_code_save = str.sims_admission_cur_code;
                $scope.student_name = str.name;
                $scope.student_dob = str.sims_admission_dob;
                $scope.student_add_date = str.sims_admission_date;
                $scope.student_class = str.admission_class;
                $scope.student_parent_no = str.sims_admission_mobile;
                $scope.sims_sen_transaction_number = str.sims_admission_sr_no;
                console.log(str);
              

                $http.get(ENV.apiUrl + "api/SENAdmClass/getStudAttribute").then(function (getStudAttributeData) {
                    $scope.getStudAttribute_data = getStudAttributeData.data;
                    console.log($scope.getStudAttribute_data);
                    debugger
                    for (var i = 0; i < $scope.getStudAttribute_data.length; i++) {
                        if ($scope.getStudAttribute_data[i].sims_appl_form_field_value2 == '1') {
                            $scope.block_1.push($scope.getStudAttribute_data[i]);
                        }
                        if ($scope.getStudAttribute_data[i].sims_appl_form_field_value2 == '2') {
                            $scope.block_2.push($scope.getStudAttribute_data[i]);
                        }
                        if ($scope.getStudAttribute_data[i].sims_appl_form_field_value2 == '3') {
                            $scope.block_3.push($scope.getStudAttribute_data[i]);
                        }
                    }

                    $http.get(ENV.apiUrl + "api/SENAdmClass/getStudAttributeAssign?sims_admission_number=" + str.sims_admission_number + "&sims_sen_transaction_number=" + str.sims_admission_sr_no).then(function (getStudAttributeAssignData) {
                        $scope.getStudAttributeAssign_data = getStudAttributeAssignData.data;
                      
                        debugger
                        if ($scope.getStudAttributeAssign_data.length > 0) {
                            if ($scope.getStudAttributeAssign_data[0].sims_diagnosis != 'undefined' || $scope.getStudAttributeAssign_data[0].sims_diagnosis != '') {
                                $scope.sims_diagnosis = $scope.getStudAttributeAssign_data[0].sims_diagnosis;
                            }
                        }
                        for (var i = 0; i < $scope.block_1.length; i++) {
                            for (var j = 0; j < $scope.getStudAttributeAssign_data.length; j++) {
                                if ($scope.block_1[i].sims_appl_form_field_value2 == $scope.getStudAttributeAssign_data[j].sims_admission_subject_group
                                    && $scope.block_1[i].sims_appl_parameter == $scope.getStudAttributeAssign_data[j].sims_admission_subject_group_value) {

                                    $scope.block_1[i]['block_1_check'] = true;
                                }
                            }
                        }
                        for (var i = 0; i < $scope.block_2.length; i++) {
                            for (var j = 0; j < $scope.getStudAttributeAssign_data.length; j++) {
                                if ($scope.block_2[i].sims_appl_form_field_value2 == $scope.getStudAttributeAssign_data[j].sims_admission_subject_group
                                    && $scope.block_2[i].sims_appl_parameter == $scope.getStudAttributeAssign_data[j].sims_admission_subject_group_value) {

                                    $scope.block_2[i]['block_2_check'] = true;
                                }
                            }
                        }
                        for (var i = 0; i < $scope.block_3.length; i++) {
                            for (var j = 0; j < $scope.getStudAttributeAssign_data.length; j++) {
                                if ($scope.block_3[i].sims_appl_form_field_value2 == $scope.getStudAttributeAssign_data[j].sims_admission_subject_group
                                    && $scope.block_3[i].sims_appl_parameter == $scope.getStudAttributeAssign_data[j].sims_admission_subject_group_value) {

                                    $scope.block_3[i]['block_3_check'] = true;
                                }
                            }
                        }

                        $(document).ready(function () {
                            $('#MyModal5').fadeIn('fast');
                        });

                    });
                });
               
          
            }


            $scope.Close_modal = function (str) {
                $(document).ready(function () {
                    $('#MyModal5').fadeOut('fast');
                });
                $scope.savedata = [];
                $scope.save_final = [];
                $scope.block_1 = [];
                $scope.block_2 = [];
                $scope.block_3 = [];
                $scope.sims_admission_number_save = "";
                $scope.sims_admission_grade_code_save = "";
                $scope.sims_admission_academic_year_save = "";
                $scope.sims_admission_cur_code_save = "";
                $scope.student_name = "";
                $scope.student_dob = "";
                $scope.student_add_date = "";
                $scope.student_class = "";
                $scope.student_parent_no = "";
                $scope.sims_diagnosis = "";
                $scope.sims_sen_transaction_number = "";

            }


            $scope.save = function (str) {
                $scope.savedata = [];
                $scope.save_final = [];
                debugger
                for (var i = 0; i < $scope.block_1.length; i++) {
                    //var t = $scope.StudentList[i].index;
                    //var v = document.getElementById("block_1_check");
                    if ($scope.block_1[i].block_1_check == true)
                    {
                        $scope.savedata.push($scope.block_1[i]);
                    }
                }


                for (var i = 0; i < $scope.block_2.length; i++) {
                    //var t = $scope.StudentList[i].index;
                    //var v = document.getElementById("block_1_check");
                    if ($scope.block_2[i].block_2_check == true) {
                        $scope.savedata.push($scope.block_2[i]);
                    }
                }

                for (var i = 0; i < $scope.block_3.length; i++) {
                    //var t = $scope.StudentList[i].index;
                    //var v = document.getElementById("block_1_check");
                    if ($scope.block_3[i].block_3_check == true) {
                        $scope.savedata.push($scope.block_3[i]);
                    }
                }

                console.log("data to be save" + $scope.savedata);

                for (var i = 0; i < $scope.savedata.length; i++)
                {
                    data = {
                        sims_admission_number: $scope.sims_admission_number_save,
                        sims_diagnosis:$scope.sims_diagnosis,
                        sims_admission_grade_code: $scope.sims_admission_grade_code_save,
                        sims_admission_academic_year: $scope.sims_admission_academic_year_save,
                        sims_admission_cur_code: $scope.sims_admission_cur_code,
                        sims_admission_subject_group: $scope.savedata[i].sims_appl_form_field_value2,
                        sims_admission_subject_group_value: $scope.savedata[i].sims_appl_parameter,
                        sims_sen_transaction_number:$scope.sims_sen_transaction_number,
                        sims_created_by: loginuser
                    }
                    $scope.save_final.push(data);
                    console.log(data);
                }

                console.log("Final data to be insert :" + $scope.save_final);


                ///INSERT BLOG

                $http.post(ENV.apiUrl + "api/SENAdmClass/CUDStudentSenDeleteApproveList", $scope.save_final).then(function (msg) {


                    $http.post(ENV.apiUrl + "api/SENAdmClass/CUDStudentSenApproveList", $scope.save_final).then(function (msg) {


                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            $scope.showReport($scope.save_final[0], $scope.admission_status);
                            $scope.Close_modal();
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Already Exists.", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });

                });
                ///END INSERT BLOCK
            }


            $scope.showReport = function (str,admission_status) {
                debugger
                var rname = 'Gradebook.SIMR130';
                    var data = {

                        location: rname,
                        parameter: {
                            cur_code: str.sims_admission_cur_code,
                            acad_year: str.sims_admission_academic_year,
                            grade_code: str.sims_admission_grade_code,
                            serch_student: str.sims_admission_number,
                            value: admission_status
                        },
                        state: 'main.SENLST',
                        ready: function () {
                            this.refreshReport();
                        },
                        show_pdf: 'y'
                    }

                    console.log(data);
                    window.localStorage["ReportDetails"] = JSON.stringify(data);
                    $state.go('main.ReportCardParameter')
            }

            //$scope.reject = function (str) {
            //    debugger
            //    $scope.dataupdatesend = [];
            //    data = {
            //        sims_admission_sr_no: str.sims_admission_sr_no,
            //        sims_admission_number: str.sims_admission_number
            //    }

            //    $scope.dataupdatesend.push(data);
                
            //    $http.post(ENV.apiUrl + "api/SENAdmClass/CUDStudentSenAdmclass", $scope.dataupdatesend).then(function (msg) {
                    
            //        $scope.msg1 = msg.data;

            //        if ($scope.msg1 == true) {
            //            swal({ text: "Rejected Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
            //            $scope.dataupdatesend = [];
            //        }
            //        else if ($scope.msg1 == false) {
            //            swal({ text: "Unable To Rejected", imageUrl: "assets/img/close.png", width: 300, height: 200 });
            //        }
            //        else {
            //            swal("Error-" + $scope.msg1)
            //        }

            //    });
                
            //}

            $scope.call_modal_new = function (str) {
                debugger
                $(document).ready(function () {
                    $('#MyModal6').fadeIn('fast');
                });

                $scope.dataupdatesend = [];
                data = {
                    sims_admission_sr_no: str.sims_admission_sr_no,
                    sims_admission_number: str.sims_admission_number,
                    sims_sen_rejected_by: $scope.users,
                }
                $scope.dataupdatesend.push(data);

                $scope.reject = function () {
                    debugger
                    data_remark = {
                        sims_admission_sr_no: $scope.dataupdatesend[0].sims_admission_sr_no,
                        sims_admission_number: $scope.dataupdatesend[0].sims_admission_number,
                        sims_sen_rejected_by: $scope.dataupdatesend[0].sims_sen_rejected_by,
                        sims_sen_rejected_remark: $scope.sims_sen_rejected_remark

                    }

                    $scope.dataupdatesend = [];

                    $scope.dataupdatesend.push(data_remark);

                    console.log($scope.dataupdatesend);
                    swal({
                        title: "Alert",
                        text: "Do you want to reject This Student?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/SENAdmClass/CUDStudentSenAdmclass", $scope.dataupdatesend).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Rejected Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                    $scope.dataupdatesend = [];
                                    $scope.Close_modal_new();
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Unable To Reject. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                                $scope.stud_Details_fun($scope.edt.sims_cur_code, $scope.temp.sims_academic_year, $scope.temp.sims_grade_code, $scope.temp.value);
                            });
                        }
                    });
                }

            }


            $scope.Close_modal_new = function () {
                $scope.sims_sen_rejected_remark = "";
                $(document).ready(function () {
                    $('#MyModal6').fadeOut('fast');
                });
            }


        }])
})();
