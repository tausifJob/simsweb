﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AttendanceAnalysisDashboardReportCont',
       ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV',
           function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

               $scope.modalGraphId = '';
               $scope.edt = [];
               $http.get(ENV.apiUrl + "api/analysisreport/getCuriculum").then(function (res) {
                   $scope.curiculums = res.data;
                   debugger;
                   $scope.edt['cur_code_comp'] = $scope.curiculums[0].cur_code;
                   $scope.edt['cur_code_statmonth'] = $scope.curiculums[0].cur_code;
                   $scope.edt['cur_code_statclass'] = $scope.curiculums[0].cur_code;
                  

                   $scope.getfirstAcademicYear($scope.edt['cur_code_comp']);
                   $scope.getSecondAcademicYearmonth($scope.edt['cur_code_statmonth']);
                   $scope.getSecondAcademicYearclass($scope.edt['cur_code_statclass']);
                   
                   
               })


               $http.get(ENV.apiUrl + "api/attendanceDashAnalysis/getAttendanceAnalysis").then(function (res1) {
                   debugger;
                   $scope.daily_status = res1.data;
               });

               $http.get(ENV.apiUrl + "api/attendanceDashAnalysis/getAttendanceAnalysis_total").then(function (res1) {
                   debugger;
                   $scope.daily_status_total = res1.data;
               });

               $http.get(ENV.apiUrl + "api/attendanceDashAnalysis/getAbsentStudentList").then(function (res1) {
                   debugger;
                   $scope.absent_list = res1.data;
               });

               //Months in graph//

               $http.get(ENV.apiUrl + "api/attendanceDashAnalysis/getmonth").then(function (res1) {
                   debugger;
                   $scope.months = res1.data;   
                   if (res1.data.length > 0) {
                       for (var i = 0; i < $scope.months.length; i++) {
                           if ($scope.months[i].month_status == 'C') {
                               $scope.edt.sims_appl_parameter = $scope.months[i].sims_appl_parameter;

                           }
                       }
                   }
              });



               // 1. COMPARATIVE ANALYSIS OF STUDENT INTAKE//////////
               $scope.hideGraph = true;

               $scope.getfirstAcademicYear = function (curCode) {
                   $http.get(ENV.apiUrl + "api/analysisreport/getAcademicYear?cur_code=" + curCode).then(function (res) {
                       $scope.firstacademicYears = res.data;
                       $scope.edt['academic_year_comp'] = $scope.firstacademicYears[0].academic_year;
                       $scope.getDatafirstGraph($scope.edt['cur_code_comp'], $scope.edt['academic_year_comp']);
                       setTimeout(function () {
                           var e = document.getElementById('comp_acad');
                           $scope.selected = e.options[e.selectedIndex].text;
                       }, 300)
                   })
               }

               $scope.showcompBarChartModal = function () {
                   if (window.bar3 != undefined) {
                       window.bar3.destroy();
                   }
                   if (window.bar4 != undefined) {
                       window.bar4.destroy();
                   }
                   $scope.dash1_recieved_lst = [];
                   $scope.dash1_total_lst = [];
                   $('#firstgraphModal').modal({ backdrop: 'static', keyboard: false });

                   $http.get(ENV.apiUrl + "api/analysisreport/getStudentIntakeData?curCode=" + $scope.edt['cur_code_comp'] + "&acaYear=" + $scope.edt['academic_year_comp']).then(function (res) {
                       $scope.studentIntakeData = res.data;

                       console.log($scope.studentIntakeData);
                       for (var i = 0; $scope.studentIntakeData.length > i; i++) {
                           if ($scope.studentIntakeData[i].enquiry_cnt == 0 && $scope.studentIntakeData[i].reg_stud == 0 && $scope.studentIntakeData[i].paid_stud == 0) {
                               $scope.errormsg = "Data Not Available";
                           } else {
                               $scope.dash1_total_lst.push($scope.studentIntakeData[i].enquiry_cnt);
                               $scope.dash1_total_lst.push($scope.studentIntakeData[i].reg_stud);
                               $scope.dash1_total_lst.push($scope.studentIntakeData[i].paid_stud);
                           }
                           if ($scope.studentIntakeData[i].per_stud_reg_to_enq == 0 && $scope.studentIntakeData[i].per_stud_paid_to_enq == 0 && $scope.studentIntakeData[i].per_stud_paid_to_reg) {
                               $scope.errormsg = "Data Not Available";
                           } else {
                               $scope.dash1_recieved_lst.push($scope.studentIntakeData[i].per_stud_paid_to_enq);
                               $scope.dash1_recieved_lst.push($scope.studentIntakeData[i].per_stud_reg_to_enq);
                               $scope.dash1_recieved_lst.push($scope.studentIntakeData[i].per_stud_paid_to_reg);
                           }
                       }
                       setTimeout(function () {
                           var ctx = document.getElementById("compBarChart2").getContext("2d");

                           var data = {
                               labels: ["Enq.Recieved", "Stud Reg", "Paid Fee"],
                               datasets: [{
                                   label: "Total No.",
                                   backgroundColor: ['#FF5A5E', '#5AD3D1', '#9467bd'],
                                   borderColor: ['#FF5A5E', '#5AD3D1', '#9467bd'],
                                   data: $scope.dash1_total_lst
                               }]
                           };

                           window.bar3 = new Chart(ctx, {
                               type: 'bar',
                               data: data,
                               options: {
                                   legend: {
                                       display: false,
                                   },
                                   title: {
                                       display: true,
                                       text: 'Total No. Of Student',
                                       position: 'bottom'
                                   }
                               }
                           });

                       }, 300)

                       var ctx = document.getElementById("compBarChart4").getContext("2d");
                       var data = {
                           labels: ['Reg.Student enq. recieved(%)', 'Student paid to enq. recieved(%)', 'Student paid to reg.students(%)'],
                           datasets: [{
                               data: $scope.dash1_recieved_lst,
                               backgroundColor: ['#FF5A5E', '#5AD3D1', '#9467bd']
                           }]
                       }


                       window.bar4 = new Chart(ctx, {
                           type: 'pie',
                           data: data,
                           options: {
                               legend: {
                                   display: true,
                                   labels: {
                                       boxWidth: 10,
                                       fontSize: 10,
                                   }
                               },
                               title: {
                                   display: true,
                                   text: 'Percentage Of Student',
                                   position: 'bottom'
                               }
                           }
                       });
                   })
               }


               $scope.getDatafirstGraph = function (cur, year) {
                   $scope.dash1_recieved_lst = [];
                   $scope.dash1_total_lst = [];
                   if (window.bar != undefined) {
                       window.bar.destroy();
                   }
                   if (window.bar1 != undefined) {
                       window.bar1.destroy();
                   }
                   $http.get(ENV.apiUrl + "api/analysisreport/getStudentIntakeData?curCode=" + cur + "&acaYear=" + year).then(function (res) {
                       $scope.studentIntakeData = res.data;
                       if ($scope.studentIntakeData.length > 0) {
                           for (var i = 0; $scope.studentIntakeData.length > i; i++) {
                               if ($scope.studentIntakeData[i].enquiry_cnt == 0 && $scope.studentIntakeData[i].reg_stud == 0 && $scope.studentIntakeData[i].paid_stud == 0) {
                                   $scope.errormsg = "Data Not Available";
                               } else {
                                   $scope.dash1_total_lst.push($scope.studentIntakeData[i].enquiry_cnt);
                                   $scope.dash1_total_lst.push($scope.studentIntakeData[i].reg_stud);
                                   $scope.dash1_total_lst.push($scope.studentIntakeData[i].paid_stud);
                               }

                               if ($scope.studentIntakeData[i].per_stud_reg_to_enq == 0 && $scope.studentIntakeData[i].per_stud_paid_to_enq == 0 && $scope.studentIntakeData[i].per_stud_paid_to_reg) {
                                   $scope.errormsg = "Data Not Available";
                               } else {
                                   $scope.dash1_recieved_lst.push($scope.studentIntakeData[i].per_stud_paid_to_enq);
                                   $scope.dash1_recieved_lst.push($scope.studentIntakeData[i].per_stud_reg_to_enq);
                                   $scope.dash1_recieved_lst.push($scope.studentIntakeData[i].per_stud_paid_to_reg);
                               }
                           }

                           var ctx = document.getElementById("compBarChart").getContext("2d");

                           ctx.canvas.parentNode.style.height = '120px';

                           var data = {
                               labels: ["Enq Rec", "Stud Reg", "Paid Fee"],
                               datasets: [{
                                   label: "Total No.",
                                   backgroundColor: ['#FF5A5E', '#5AD3D1', '#9467bd'],
                                   borderColor: ['#FF5A5E', '#5AD3D1', '#9467bd'],
                                   data: $scope.dash1_total_lst
                               }]
                           };



                           window.bar = new Chart(ctx, {
                               type: 'bar',
                               data: data,
                               options: {
                                   legend: {
                                       display: false,
                                   },
                                   title: {
                                       display: true,
                                       text: 'Total No. Of Student',
                                       position: 'bottom'
                                   }
                               }
                           });

                           var ctx3 = document.getElementById("compBarChart3").getContext("2d");

                           ctx3.canvas.parentNode.style.height = '128px';

                           var data = {
                               labels: ['Reg.Student enq. recieved(%)', 'Student paid to enq. recieved(%)', 'Student paid to reg.students(%)'],
                               datasets: [{
                                   data: $scope.dash1_recieved_lst,
                                   backgroundColor: ['#FF5A5E', '#5AD3D1', '#9467bd']
                               }]
                           }

                           window.bar1 = new Chart(ctx3, {
                               type: 'pie',
                               data: data,
                               options: {
                                   legend: {
                                       display: false,
                                       labels: {
                                           boxWidth: 10,
                                           fontSize: 10,
                                           position: 'bottom'
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: 'Percentage Of Student',
                                       position: 'bottom'
                                   }
                               }
                           });

                       } else {
                           $scope.errormsg = "Data Not Available";
                       }
                   })
               }


               $scope.showTableData = function () {
                   $scope.hideGraph = false;
               }
               $scope.showGraph = function () {
                   $scope.hideGraph = true;
               }



               ////////// end  //////////

               // 2. Month wise //////////////////


               $scope.showsecondgraphTablemonth = function () {
                   $scope.hideGraph = false;
               }

               $scope.showsecondgraphmonth = function () {
                   $scope.hideGraph = true;
               }

               $scope.getSecondAcademicYearmonth = function (curCode) {
                   $http.get(ENV.apiUrl + "api/analysisreport/getAcademicYear?cur_code=" + curCode).then(function (res) {
                       $scope.secondacademicYears = res.data;
                       $scope.edt['academic_year_statmonth'] = $scope.secondacademicYears[0].academic_year;
                       $scope.graphStudentStatusmonth($scope.edt['cur_code_statmonth'], $scope.edt['academic_year_statmonth']);
                   })
               }


               $scope.graphStudentStatusmonth = function (cur, year) {
                   $scope.month_grades_lst = [];
                   $scope.month_att_month_lst = [];
                   $scope.month_sims_attendance_date_lst = [];
                   $scope.month_present_lst = [];
                   $scope.month_absent_lst = [];
                   $scope.month_others_students_lst = [];
                   $scope.month_unmark_lst = [];


                   if (window.bar5 != undefined) {
                       window.bar5.destroy();
                   }

                   $http.get(ENV.apiUrl + "api/attendanceDashAnalysis/getStudentAnalysisGraph1?curCode=" + cur + "&acaYear=" + year + "&month=" + $scope.edt.sims_appl_parameter).then(function (res) {
                       debugger;
                       console.log(res.data);
                       $scope.studStsChartDatamonth = res.data;
                       for (var i = 0; i < res.data.length; i++) {
                           if (res.data[i].grades != "Grand Total") {
                               $scope.month_grades_lst.push(res.data[i].sims_grade_name_en);
                               $scope.month_att_month_lst.push(res.data[i].att_month);
                               $scope.month_sims_attendance_date_lst.push(res.data[i].sims_attendance_date);
                               $scope.month_present_lst.push(res.data[i].present);
                               $scope.month_absent_lst.push(res.data[i].absent);
                               $scope.month_others_students_lst.push(res.data[i].others);
                               $scope.month_unmark_lst.push(res.data[i].unmark);

                           }

                       }

                       var ctxs = document.getElementById("studStsChartmonth").getContext("2d");

                       //  ctxs.canvas.parentNode.style.width = '528px';
                       ctxs.canvas.parentNode.style.height = '400px';

                       var StatusData = {
                           labels: $scope.month_grades_lst,
                           datasets: [

                             {
                                 label: "Present",

                                 backgroundColor: "#2ca02c",
                                 borderColor: "#2ca02c",
                                 data: $scope.month_present_lst
                             },

                             {
                                 label: "Absent",
                                 backgroundColor: "#9467bd",
                                 borderColor: "#9467bd",
                                 data: $scope.month_absent_lst
                             },

                             {
                                 label: "Unmark",
                                 backgroundColor: "#7f7f7f",
                                 borderColor: "#7f7f7f",
                                 data: $scope.month_unmark_lst

                             }
                           ]
                       };



                       setTimeout(function () {
                           window.bar5 = new Chart(ctxs, {
                               type: 'bar',
                               data: StatusData,
                               options: {
                                   responsive: true,
                                   maintainAspectRatio: false,
                                   scales: {
                                       xAxes: [{
                                           stacked: true
                                       }],
                                       yAxes: [{
                                           stacked: true
                                       }]
                                   },
                                   legend: {
                                       display: true,
                                       labels: {
                                           boxWidth: 10,
                                           fontSize: 10,
                                           position: 'bottom'
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: $scope.month_att_month_lst[0],
                                       position: 'bottom'
                                   }
                               }
                           });

                       }, 300);

                   });
               }


               $scope.showGraphModalmonth = function () {
                   debugger;
                   $('#secondgraphModalmonth').modal({ backdrop: 'static', keyboard: false });
                   $scope.month_grades_lst = [];
                   $scope.month_att_month_lst = [];
                   $scope.month_sims_attendance_date_lst = [];
                   $scope.month_present_lst = [];
                   $scope.month_absent_lst = [];
                   $scope.month_others_students_lst = [];
                   $scope.month_unmark_lst = [];

                   if (window.bar6 != undefined) {
                       window.bar6.destroy();
                   }
                  
                   $http.get(ENV.apiUrl + "api/attendanceDashAnalysis/getStudentAnalysisGraph1?curCode=" + $scope.edt['cur_code_statmonth'] + "&acaYear=" + $scope.edt['academic_year_statmonth'] + "&month=" + $scope.edt.sims_appl_parameter).then(function (res) {
                       for (var i = 0; i < res.data.length; i++) {
                           if (res.data[i].grades != "Grand Total") {
                               $scope.month_grades_lst.push(res.data[i].sims_grade_name_en);
                               $scope.month_att_month_lst.push(res.data[i].att_month);
                               $scope.month_sims_attendance_date_lst.push(res.data[i].sims_attendance_date);
                               $scope.month_present_lst.push(res.data[i].present);
                               $scope.month_absent_lst.push(res.data[i].absent);
                               $scope.month_others_students_lst.push(res.data[i].others);
                               $scope.month_unmark_lst.push(res.data[i].unmark);

                           }
                       }

                       var ctx = document.getElementById("studStsChart2month").getContext("2d");
                       ctx.canvas.parentNode.style.height = '400px';
                       var modaldata = {
                           labels: $scope.month_grades_lst,
                           datasets: [

                               {
                                   label: "Present",

                                   backgroundColor: "#2ca02c",
                                   borderColor: "#2ca02c",
                                   data: $scope.month_present_lst
                               },

                             {
                                 label: "Absent",
                                 backgroundColor: "#9467bd",
                                 borderColor: "#9467bd",
                                 data: $scope.month_absent_lst
                             },

                             {
                                 label: "Unmark",
                                 backgroundColor: "#7f7f7f",
                                 borderColor: "#7f7f7f",
                                 data: $scope.month_unmark_lst

                             }
                           ]
                       };



                       setTimeout(function () {
                           window.bar6 = new Chart(ctx, {
                               type: 'bar',
                               data: modaldata,
                               options: {
                                   responsive: true,
                                   maintainAspectRatio: false,
                                   scales: {
                                       xAxes: [{
                                           stacked: true
                                       }],
                                       yAxes: [{
                                           stacked: true
                                       }]
                                   },
                                   legend: {
                                       display: true,
                                       labels: {
                                           boxWidth: 10,
                                           fontSize: 10
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: $scope.month_att_month_lst[0],
                                       position: 'bottom'
                                   }
                               }
                           });

                       }, 300);

                   });
               }

               //////////// end /////////////////      




               // 2. class wise //////////////////


               $scope.showsecondgraphTableclass = function () {
                   $scope.hideGraph = false;
               }
               $scope.showsecondgrapheclass = function () {
                   $scope.hideGraph = true;
               }

               $scope.getSecondAcademicYearclass = function (curCode) {
                   debugger;
                   $http.get(ENV.apiUrl + "api/analysisreport/getAcademicYear?cur_code=" + curCode).then(function (res) {
                       $scope.secondacademicYearsclass = res.data;
                       $scope.edt['academic_year_statclass'] = $scope.secondacademicYearsclass[0].academic_year;
                       $scope.graphStudentStatusclass($scope.edt['cur_code_statclass'], $scope.edt['academic_year_statclass']);
                       $scope.getclass($scope.edt['cur_code_statclass'], $scope.edt['academic_year_statclass']);
                   })
               }


               $scope.getclass = function (curCode, year) {
                   debugger;
                   $http.get(ENV.apiUrl + "api/attendanceDashAnalysis/getClass?curCode=" + curCode + "&acaYear=" + year).then(function (res) {
                       $scope.classData = res.data;
                       $scope.edt.sims_grade_code = $scope.classData[0].sims_grade_code;
                       $scope.graphStudentStatusclass($scope.edt['cur_code_statclass'], $scope.edt['academic_year_statclass'],$scope.edt.sims_grade_code);
                   })
               }


               $scope.graphStudentStatusclass = function (cur, year) {
                   $scope.class_grades_lstclass = [];
                   $scope.class_att_month_lstclass = [];
                   $scope.class_sims_attendance_date_lstclass = [];
                   $scope.class_present_lstclass = [];
                   $scope.class_absent_lstclass = [];
                   $scope.class_others_students_lstclass = [];
                   $scope.class_unmark_lstclass = [];


                   if (window.bar5 != undefined) {
                       window.bar5.destroy();
                   }

                   $http.get(ENV.apiUrl + "api/attendanceDashAnalysis/getStudentAnalysisGraph1class?curCode=" + cur + "&acaYear=" + year + "&grade=" + $scope.edt.sims_grade_code).then(function (res) {
                       debugger;
                       console.log(res.data);
                       $scope.studStsChartDataclass = res.data;
                       for (var i = 0; i < res.data.length; i++) {
                           if (res.data[i].att_month != "Grand Total") {
                               $scope.class_att_month_lstclass.push(res.data[i].att_month);
                               $scope.class_grades_lstclass.push(res.data[i].sims_grade_name_en);
                               $scope.class_sims_attendance_date_lstclass.push(res.data[i].sims_attendance_date);
                               $scope.class_present_lstclass.push(res.data[i].present);
                               $scope.class_absent_lstclass.push(res.data[i].absent);
                               $scope.class_others_students_lstclass.push(res.data[i].others);
                               $scope.class_unmark_lstclass.push(res.data[i].unmark);

                           }

                       }

                       var ctxs = document.getElementById("studStsChartclass").getContext("2d");

                       //  ctxs.canvas.parentNode.style.width = '528px';
                       ctxs.canvas.parentNode.style.height = '400px';

                       var StatusData = {
                           labels: $scope.class_att_month_lstclass,
                           datasets: [

                             {
                                 label: "Present",

                                 backgroundColor: "#2ca02c",
                                 borderColor: "#2ca02c",
                                 data: $scope.class_present_lstclass
                             },

                             {
                                 label: "Absent",
                                 backgroundColor: "#9467bd",
                                 borderColor: "#9467bd",
                                 data: $scope.class_absent_lstclass
                             },

                             {
                                 label: "Unmark",
                                 backgroundColor: "#7f7f7f",
                                 borderColor: "#7f7f7f",
                                 data: $scope.class_unmark_lstclass

                             }
                           ]
                       };



                       setTimeout(function () {
                           window.bar5 = new Chart(ctxs, {
                               type: 'bar',
                               data: StatusData,
                               options: {
                                   responsive: true,
                                   maintainAspectRatio: false,
                                   scales: {
                                       xAxes: [{
                                           stacked: true
                                       }],
                                       yAxes: [{
                                           stacked: true
                                       }]
                                   },
                                   legend: {
                                       display: true,
                                       labels: {
                                           boxWidth: 10,
                                           fontSize: 10,
                                           position: 'bottom'
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: $scope.class_grades_lstclass[0],
                                       position: 'bottom'
                                   }
                               }
                           });

                       }, 300);

                   });
               }


               $scope.showGraphModalclass = function () {
                   debugger;
                   $('#secondgraphModalclass').modal({ backdrop: 'static', keyboard: false });
                   $scope.class_grades_lstclass = [];
                   $scope.class_att_month_lstclass = [];
                   $scope.class_sims_attendance_date_lstclass = [];
                   $scope.class_present_lstclass = [];
                   $scope.class_absent_lstclass = [];
                   $scope.class_others_students_lstclass = [];
                   $scope.class_unmark_lstclass = [];

                   if (window.bar6 != undefined) {
                       window.bar6.destroy();
                   }

                   $http.get(ENV.apiUrl + "api/attendanceDashAnalysis/getStudentAnalysisGraph1class?curCode=" + $scope.edt['cur_code_statclass'] + "&acaYear=" + $scope.edt['academic_year_statclass'] + "&grade=" + $scope.edt.sims_grade_code).then(function (res) {
                       for (var i = 0; i < res.data.length; i++) {
                           if (res.data[i].att_month != "Grand Total") {
                               $scope.class_att_month_lstclass.push(res.data[i].att_month);
                               $scope.class_grades_lstclass.push(res.data[i].sims_grade_name_en);
                               $scope.class_sims_attendance_date_lstclass.push(res.data[i].sims_attendance_date);
                               $scope.class_present_lstclass.push(res.data[i].present);
                               $scope.class_absent_lstclass.push(res.data[i].absent);
                               $scope.class_others_students_lstclass.push(res.data[i].others);
                               $scope.class_unmark_lstclass.push(res.data[i].unmark);

                           }
                       }

                       var ctx = document.getElementById("studStsChart2class").getContext("2d");
                       ctx.canvas.parentNode.style.height = '400px';
                       var modaldata = {
                           labels: $scope.class_att_month_lstclass,
                           datasets: [

                               {
                                   label: "Present",

                                   backgroundColor: "#2ca02c",
                                   borderColor: "#2ca02c",
                                   data: $scope.class_present_lstclass
                               },

                             {
                                 label: "Absent",
                                 backgroundColor: "#9467bd",
                                 borderColor: "#9467bd",
                                 data: $scope.class_absent_lstclass
                             },

                             {
                                 label: "Unmark",
                                 backgroundColor: "#7f7f7f",
                                 borderColor: "#7f7f7f",
                                 data: $scope.class_unmark_lstclass

                             }
                           ]
                       };



                       setTimeout(function () {
                           window.bar6 = new Chart(ctx, {
                               type: 'bar',
                               data: modaldata,
                               options: {
                                   responsive: true,
                                   maintainAspectRatio: false,
                                   scales: {
                                       xAxes: [{
                                           stacked: true
                                       }],
                                       yAxes: [{
                                           stacked: true
                                       }]
                                   },
                                   legend: {
                                       display: true,
                                       labels: {
                                           boxWidth: 10,
                                           fontSize: 10
                                       }
                                   },
                                   title: {
                                       display: true,
                                       text: $scope.class_grades_lstclass[0],
                                       position: 'bottom'
                                   }
                               }
                           });

                       }, 300);

                   });
               }

               //////////// end /////////////////   



           

               /////  Export Excel ///////

               $scope.exportTable = function (tablename, id) {
                   var blob = new Blob([document.getElementById(id).innerHTML], {
                       type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                   });
                   saveAs(blob, tablename + ".xls");
               }
               /// end //

               $scope.printGraph = function (curgraph) {
                   var options = {
                   };
                   var pdf = new jsPDF('p', 'pt', 'a4');
                   pdf.setTextColor(100)
                   //                    pdf.setFontSize(30)
                   //                    pdf.addImage(img, 'JPEG', 15, 40, 50, 50)
                   pdf.text(20, 20, $scope.user_details['lic_school_name']);
                   pdf.addHTML($("#" + curgraph), 30, 30, options, function () {
                       pdf.save(curgraph + '.pdf');
                   });
                   //                    addPage()
                   //   pdf.autoPrint();
               }

               //////Attendance Snapshot Report

               $scope.Report_call = function () {
                   debugger
                   $http.get(ENV.apiUrl + "api/attendanceDashAnalysis/getAttendanceSnapshotReport").then(function (res1) {
                       $scope.result1 = res1.data;
                       debugger
                       var data = {
                           location: $scope.result1,//"Gradebook.GBR039QR", 
                           //parameter: {
                           //    cur_code: str.sims_cur_code,
                           //    acad_year: str.sims_academic_year,
                           //    grade_code: str.sims_grade_code,
                           //    section_code: str.sims_section_code,
                           //    term_code: str.sims_term_code,
                           //    sub_code: str.sims_subject_code,
                           //    serch_student: null,
                           //},
                           state: 'main.atdr01',
                           ready: function () {
                               this.refreshReport();
                           },
                       }
                       window.localStorage["ReportDetails"] = JSON.stringify(data)
                       $state.go('main.ReportCardParameter')


                   });

                   console.log($scope.result1[0].attendanca_snapshot_report);

               }

           }]);

    simsController.filter('titleCase', function () {
        return function (input) {
            input = input || '';
            return input.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
        };
    });



})();