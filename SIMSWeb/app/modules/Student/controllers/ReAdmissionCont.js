﻿(function () {
    'use strict';
    var obj1, temp, opr, comp, edt;
    var main, deletefin = [];
    var deletecode = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ReAdmissionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.search = true;
            var dataforSave = [];
            $scope.reset = true;
            $scope.display = true;
            $scope.save_btn = true;
            $scope.txt_read = true;
            $scope.Is_Save = false;
            $scope.temp = [];
            $scope.edt = {};

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100)

            var user = $rootScope.globals.currentUser.username;

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.reset = function () {
                debugger;
                $scope.temp = {};
                //$scope.edt = {};
                $scope.savedata = [];
                $scope.edt.search_std_grade_name = '';
                $scope.edt.search_std_section_name = '';
                $scope.edt.sims_attendance_date = '';
                $scope.edt.sims_fee_from_date = '';
                $scope.edt.sims_enroll_remark = '';
                $scope.Section_code = '';
                //$state.go($state.current, {}, { reload: true });
                //$scope.temp.sims_enroll_no = '';
                //$scope.temp.sims_cur_name = '';
                //$scope.temp.sims_academic_year = '';
                //$scope.temp.sims_grade_name = '';
                //$scope.temp.sims_section_name = '';
                //$scope.temp.stud_full_name = '';
                //$scope.edt.s_cur_code = '';
                //$scope.edt.sims_academic_year = '';
                //$scope.edt.search_std_grade_name = '';
                //$scope.edt.search_std_section_name = '';
                //$scope.edt.sims_attendance_date = '';
                //$scope.edt.sims_fee_from_date = '';
            }

            function getCur(flag, comp_code) {
                debugger

                if (flag) {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        $scope.curriculum = res.data;
                        $scope.edt["s_cur_code"] = $scope.curriculum[0].sims_cur_code;
                        $scope.getAccYear();

                    });
                }
                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        $scope.curriculum = res.data;
                        $scope.edt["s_cur_code"] = $scope.curriculum[0].sims_cur_code;
                        $scope.getAccYear();

                    });


                }

            }

            $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.global_count_comp = res.data;

                if ($scope.global_count_comp) {
                    getCur(true, $scope.user_details.comp);
                }
                else {
                    getCur(false, $scope.user_details.comp)
                }
            });

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                debugger;
                $scope.ComboBoxValues = AllComboBoxValues.data;
                $scope.edt = {
                    sims_academic_year: $scope.ComboBoxValues[0].sims_acadmic_year,
                    s_cur_code: $scope.ComboBoxValues[0].sims_cur_code,
                };
            });

            $scope.getAccYear = function () {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.edt.s_cur_code).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    if (Acyear.data.length > 0) {
                        $scope.edt["sims_academic_year"] = $scope.Acc_year[0].sims_academic_year;
                        $scope.getGrade();
                    }
                });
            }

            //Grade
            $scope.getGrade = function () {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.s_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
                $scope.getSection($scope.edt.s_cur_code, $scope.edt.search_std_grade_name, $scope.edt.sims_academic_year)
            }

            //Section
            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.edt.s_cur_code + "&grade_code=" + $scope.edt.search_std_grade_name + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.CreDiv;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                debugger;
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_enroll_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.stud_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            //Stud details
            $scope.access_nos = function (event) {
                debugger
                //savedata = [];
                if (event.key == "Tab" || event.key == "Enter") {
                    if ($scope.temp.sims_enroll_no == null || $scope.temp.sims_enroll_no == "") {
                        swal('', 'Please Enter Enroll No.')
                        return;
                    }
                    $http.get(ENV.apiUrl + "api/ReAdmission/Get_studentlist?enroll_no=" + $scope.temp.sims_enroll_no).then(function (Allstudent) {
                        debugger
                        if (Allstudent.data.length == 0) {
                            swal('', 'No Record Found');
                            return;
                        }
                        $scope.savedata = Allstudent.data;
                        $scope.temp = $scope.savedata[0];
                        console.log($scope.savedata)
                    });
                }
            }

            $scope.stud_details = function () {
                debugger;
                if ($scope.temp.sims_enroll_no == null || $scope.temp.sims_enroll_no == "" || $scope.temp.sims_enroll_no== undefined) {
                    $('#myModal1').modal('show');
                    $http.get(ENV.apiUrl + "api/ReAdmission/Get_studentlist?enroll_no=" + $scope.temp.sims_enroll_no).then(function (Allstudent) {
                       
                        $scope.CreDiv = Allstudent.data;
                        $scope.totalItems = $scope.CreDiv.length;
                        // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                        $scope.todos = $scope.CreDiv;
                        $scope.makeTodos();
                    });
                    $scope.list = true;
                }
                else {
                    $http.get(ENV.apiUrl + "api/ReAdmission/Get_studentlist?enroll_no=" + $scope.temp.sims_enroll_no).then(function (Allstudent) {
                        debugger
                        if (Allstudent.data.length == 0) {
                            swal('', 'No Record Found');
                            return;
                        }
                        $scope.savedata = Allstudent.data;
                        $scope.temp = $scope.savedata[0];
                        console.log($scope.savedata)
                    });
                }
            }

            $scope.checkonebyonedelete = function (info) {

                for (var i = 0; i < $scope.student.length; i++) {
                    var d = document.getElementById(i);
                    if (d.checked == true) {
                        $scope.student[i].ischange = true;
                    }
                    else {
                        $scope.student[i].ischange = false;
                    }
                }

                $("input[type='radio']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('$index');
                if (main.checked == true) {

                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.submit = function () {
                for (var j = 0; j < $scope.filteredTodos.length; j++) {
                    var v = document.getElementById(j + $scope.filteredTodos[j].sims_enroll_no);
                    if (v.checked == true) {
                        $scope.savedata = $scope.filteredTodos[j];
                        $scope.temp = $scope.savedata;
                    }
                }
                $('#myModal1').modal('hide');
            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.SaveData = function () {
                debugger
                if ($scope.temp.sims_enroll_no == null || $scope.temp.sims_enroll_no == "") {
                    swal('', 'Please Enter Enroll No.')
                    return;
                }
                if ($scope.edt == undefined || $scope.edt.s_cur_code == undefined || $scope.edt.s_cur_code == null || $scope.edt.sims_academic_year == undefined || $scope.edt.sims_academic_year == null) {
                    swal('', 'Please Select Curriculum And Academic Year');
                    return;
                }
                if ($scope.edt == undefined || $scope.edt.search_std_grade_name == undefined || $scope.edt.search_std_grade_name == null || $scope.edt.search_std_section_name == undefined || $scope.edt.search_std_section_name == null) {
                    swal('', 'Please Select Grade And Section');
                    return;
                }
                if ($scope.edt.sims_attendance_date == undefined || $scope.edt.sims_attendance_date == null) {
                    swal('', 'Please Select Attendance From date');
                    return;
                }
                if ($scope.edt.sims_fee_from_date == undefined || $scope.edt.sims_fee_from_date == null) {
                    swal('', 'Please Select Fee Applicable From date');
                    return;
                }
                $scope.Is_Save = true;
                var data = {
                    'opr': 'V',
                    'username': user,
                    'sims_enroll_no': $scope.temp.sims_enroll_no,
                    'sims_cur_code': $scope.temp.sims_cur_code,
                    'sims_academic_year': $scope.temp.sims_academic_year,
                    'sims_grade_code': $scope.temp.sims_grade_code,
                    'sims_section_code': $scope.temp.sims_section_code,
                    'sims_cur_code_new': $scope.edt.s_cur_code,
                    'sims_academic_year_new': $scope.edt.sims_academic_year,
                    'sims_grade_code_new': $scope.edt.search_std_grade_name,
                    'sims_section_code_new': $scope.edt.search_std_section_name,
                    'sims_attendance_date': $scope.edt.sims_attendance_date,
                    'sims_fee_from_date': $scope.edt.sims_fee_from_date,
                    'sims_readmission_remark': $scope.edt.sims_enroll_remark
                }
                datasend.push(data);
                $http.post(ENV.apiUrl + "api/ReAdmission/CUDRe_admission", datasend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 });
                    $scope.Is_Save = false;
                    $scope.reset();
                    //if ($scope.msg1 == true) {
                    //    swal({ title: "Alert", text: "Re-Admission is successfully done", width: 300, height: 200 });
                    //    $scope.reset();
                    //}
                    //else {
                    //    swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                    //    $scope.currentPage = true;
                    //}
                });
                //$scope.edt.search_std_grade_name = '';
                //$scope.edt.search_std_section_name = '';
                //$scope.edt.sims_attendance_date = '';
                //$scope.edt.sims_fee_from_date = '';

                //datasend = [];
                $scope.table = true;
                $scope.display = true;
            }
         }])
})();