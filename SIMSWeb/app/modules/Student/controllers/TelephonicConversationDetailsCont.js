﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    
    simsController.controller('TelephonicConversationDetailsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = true; 
            $scope.save_btn = true;
            $scope.table = true;
            $scope.Update_btn = false;
            $scope.cancel_btn = false;

            var user = $rootScope.globals.currentUser.username;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            
            $scope.users = $rootScope.globals.currentUser.username;
             
            //  $scope.edt.date = new Date();

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "dd-mm-yyyy"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.edt = {
                from_date: dd + '-' + mm + '-' + yyyy,
                to_date: dd + '-' + mm + '-' + yyyy,

            }
            $(function () {
                debugger;
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.edt["from_date"] = '';
                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.edt = {
                            //from_date: yyyy + '-' + mm + '-' + dd,
                            //to_date: yyyy + '-' + mm + '-' + dd,
                            from_date: dd + '-' + mm + '-' + yyyy,
                            to_date: dd + '-' + mm + '-' + yyyy,
                        }

                    }
                });
            });
           

            //CUD
            $scope.save = function (Myform) {
                debugger;
                var datasend = [];
                if (Myform) {
                    //$scope.data = $scope.int;
                    debugger;
                    $scope.int['opr'] = 'I';
                    $scope.int['sims_comm_user_by'] = $scope.users;
                    datasend.push($scope.int);
                    $http.post(ENV.apiUrl + "api/TelephonicConversationDetails/CUDTelephonicConversationDetails", datasend).then(function (res) {

                        $scope.msg1 = res.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });

                            $scope.int = {};
                             
                            //show data
                            $scope.getgrid();
                          
                        }
                        else {
                            //swal({ title: "Alert", text: "Record NOt Inserted ", showCloseButton: true, width: 300, height: 200 });
                            swal('', 'Record not inserted');

                        }
                    });
                }
            }
           
            //getgrid main page
            $scope.getgrid = function () {
                $scope.editor = true;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.table = true;
                $scope.Update_btn = false;
                $scope.cancel_btn = false;
                $scope.save_btn = true;
                $scope.int = '';

                $http.get(ENV.apiUrl + "api/TelephonicConversationDetails/get_TelephonicConversationDetails").then(function (res1) {
                    debugger;
                    $scope.CreDiv = res1.data;
                    $scope.totalItems = $scope.CreDiv.length;
                    // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                    $scope.todos = $scope.CreDiv;
                    $scope.makeTodos();

                });

            }


            //DATA EDIT  
            $scope.edit = function (str) {
                debugger
                $scope.editor = false;
                $scope.Update_btn = true;
                $scope.cancel_btn = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Evidences = true;
                $scope.divcode_readonly = true;


                debugger
                $scope.int = {
                    sims_comm_user_code: str.sims_comm_user_code,
                    sims_comm_message_remark: str.sims_comm_message_remark,
                    sims_comm_message_feedback: str.sims_comm_message_feedback,
                    sims_comm_date: str.sims_comm_date,

                 
                }

            }

            //cancle
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = true;
                $scope.int = " ";
                $scope.editor = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
            }

            //DATA UPDATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                if (Myform) {
                    debugger;
                    var data = $scope.int;
                   // data.sims_checklist_section_parameter_number = $scope.int.sims_checklist_section_parameter_number;
                    data.opr = "U";
                    dataforUpdate.push(data);
                    //dataupdate.push(data);
                    $http.post(ENV.apiUrl + "api/TelephonicConversationDetails/CUDTelephonicConversationDetails", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.getgrid();
                        }
                        else {
                            swal({ text: "Record Not Update / Order No Is Exists", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }
                        //search update data 
                        $scope.searchbutton();



                    });
                    dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            $(document).ready(function () {

                $scope.display = true;
                $scope.getgrid();
               
            });
        }])
})();