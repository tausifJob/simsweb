﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Student');
    simsController.controller('SurveySelectionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var user = $rootScope.globals.currentUser.username;
            $scope.btn_sub = true;
            $scope.itemsPerPage = 1;
            $scope.currentPage = 0;
            $scope.size = function (pagesize) {
                $scope.itemsPerPage = pagesize;

            }


            $http.get(ENV.apiUrl + "api/surveyselection/Get_survey_for_user?user_code=" + user).then(function (survey_detail) {
                $scope.Survey_Details = survey_detail.data;
                if ($scope.Survey_Details.length > 0) {
                    $scope.survey_detail_table = true;
                }
                else {
                    $scope.survey_detail_table = false;
                    swal({ text: "Data Not Found", width: 250, height: 250, showCloseButton: true });
                }

            });

            $scope.Start_survey_btn_click = function (str) {
                debugger;
                $scope.survey_code = str.sims_survey_code;
                //window.localStorage.removeItem('survey' + $scope.survey_code);

                $http.get(ENV.apiUrl + "api/surveyselection/get_survey_details_check?sims_survey_code=" + str.sims_survey_code + "&sims_user_code=" + user).then(function (survey_details_check) {
                    $scope.survey_details_check = survey_details_check.data;
                    if ($scope.survey_details_check == true) {
                        swal({ title: "Alert", text: 'Sorry, you have already completed this survey', width: 300, height: 250, showCloseButton: true });


                    }
                    else {
                        $http.get(ENV.apiUrl + "api/surveyselection/get_survey_details_quetions?sims_survey_code=" + str.sims_survey_code).then(function (survey_detail) {
                            $scope.start_Survey = survey_detail.data;
                            //shuffle($scope.start_Survey);
                            if (window.localStorage['survey' + $scope.survey_code] != undefined)
                                $scope.start_Survey = JSON.parse(window.localStorage['survey' + $scope.survey_code]);

                            $scope.ques_count = $scope.start_Survey.length;
                            if ($scope.start_Survey.length > 0) {
                                $scope.sims_survey_type = $scope.start_Survey[0].sims_survey_type;
                                $scope.sims_survey_no_of_question_available = $scope.start_Survey[0].sims_survey_no_of_question_available;
                                $scope.sims_survey_no_of_question_to_attempt = $scope.start_Survey[0].sims_survey_no_of_question_to_attempt;
                                $scope.sims_survey_subject = str.sims_survey_subject;
                                $scope.sims_survey_desc_en = str.sims_survey_desc_en;
                                // $("#MyModal").modal({ backdrop: 'static' })

                                $scope.popup_start_btn_click();
                            }
                            else {
                                $scope.start_survey_table = false;
                                swal({ title: "Alert", text: 'Sorry,questions are not available for this survey', width: 350, height: 250, showCloseButton: true });
                            }
                        });
                    }
                });
            }

            $scope.Prevoius_btn_click = function () {
                if ($scope.question_counter != 0) {
                    $scope.question_counter = $scope.question_counter - 1;
                    $scope.binding_object = $scope.start_Survey[$scope.question_counter];
                    $scope.btn_next = false;
                    $scope.btn_sub = true;
                    $scope.question_counter1 = $scope.question_counter1 - 1;
                }
                if ($scope.question_counter == 0) {
                    $scope.btn_pre = true;
                }

                $scope.SubmitObject = [];

                $scope.progress_counter = 0;
                for (var j = 0; j < $scope.start_Survey.length; j++) {
                    for (var i = 0; i < $scope.start_Survey[j].que_ans.length; i++) {
                        if ($scope.start_Survey[j].que_ans[i].question_ans_status == true) {
                            $scope.progress_counter = $scope.progress_counter + 1;
                            $scope.maxmark1 = Math.round(($scope.progress_counter * 100) / $scope.ques_count) + '%';
                            break;
                        }
                        if ($scope.start_Survey[j].que_ans[i].sims_survey_question_type == '2' && $scope.start_Survey[j].que_ans[i].sims_survey_question_answer_desc_en1 != '' && $scope.start_Survey[j].que_ans[i].sims_survey_question_answer_desc_en1 != undefined) {
                            $scope.progress_counter = $scope.progress_counter + 1;
                            $scope.maxmark1 = Math.round(($scope.progress_counter * 100) / $scope.ques_count) + '%';
                            break;

                        }
                    }
                }

            }

            $scope.progress_counter = 0;

            $scope.next_btn_click = function () {

                if ($scope.question_counter <= $scope.start_Survey.length - 1) {
                    $scope.question_counter = $scope.question_counter + 1;
                    $scope.binding_object = $scope.start_Survey[$scope.question_counter];
                    $scope.question_counter1 = $scope.question_counter1 + 1;
                    $scope.btn_pre = false;
                }
                if ($scope.question_counter == $scope.start_Survey.length - 1) {
                    $scope.btn_next = true;
                    $scope.btn_sub = false;
                }

                $scope.SubmitObject = [];

                $scope.progress_counter = 0;
                for (var j = 0; j < $scope.start_Survey.length; j++) {
                    for (var i = 0; i < $scope.start_Survey[j].que_ans.length; i++) {

                        if ($scope.start_Survey[j].que_ans[i].question_ans_status == true) {
                            $scope.progress_counter = $scope.progress_counter + 1;
                            $scope.maxmark1 = Math.round(($scope.progress_counter * 100) / $scope.ques_count) + '%';
                            break;
                        }
                        if ($scope.start_Survey[j].que_ans[i].sims_survey_question_type == '2' && $scope.start_Survey[j].que_ans[i].sims_survey_question_answer_desc_en1 != '' && $scope.start_Survey[j].que_ans[i].sims_survey_question_answer_desc_en1 != undefined) {
                            $scope.progress_counter = $scope.progress_counter + 1;
                            $scope.maxmark1 = Math.round(($scope.progress_counter * 100) / $scope.ques_count) + '%';

                            break;

                        }
                    }
                }
            }


            $scope.save_later_btn_click = function () {
                debugger;
                //  console.log($scope.binding_object.que_ans);

                window.localStorage['survey' + $scope.binding_object.sims_survey_code] = JSON.stringify($scope.start_Survey);

                swal({ title: "Alert", text: 'Answer to the survey questions are saved for later use ', width: 300, height: 250, showCloseButton: true });


            }

            function shuffle(array) {
                var currentIndex = array.length, temporaryValue, randomIndex;

                // While there remain elements to shuffle...
                while (0 !== currentIndex) {
                    // Pick a remaining element...
                    randomIndex = Math.floor(Math.random() * currentIndex);
                    currentIndex -= 1;

                    // And swap it with the current element.
                    temporaryValue = array[currentIndex];
                    array[currentIndex] = array[randomIndex];
                    array[randomIndex] = temporaryValue;
                }
                return array;
            }

            $scope.popup_start_btn_click = function () {

                $scope.start_survey_table = true;
                $scope.survey_detail_table = false;
                $scope.btn_pre = true;
                $scope.btn_next = false;
                $scope.binding_object = $scope.start_Survey[0];



                $scope.question_counter1 = 1;
                $scope.question_counter = 0;

                $scope.totalSeconds = 3600;
                var timerVar = setInterval(countTimer, 1000);

                function countTimer() {

                    if ($scope.totalSeconds < 0) {
                        clearInterval(timerVar);
                        $scope.Submit_btn_click();


                    } else {
                        var hour = Math.floor($scope.totalSeconds / 3600);
                        var minute = Math.floor(($scope.totalSeconds - hour * 3600) / 60);
                        var seconds = $scope.totalSeconds - (hour * 3600 + minute * 60);
                        $scope.$apply(function () {
                            $scope.time = hour + ":" + minute + ":" + seconds;
                        });
                        --$scope.totalSeconds;
                    }

                }

            }

            $scope.Submit_btn_click = function () {

                swal({
                    title: "Do you want to submit survey?", text: "Cilck OK to Submit",
                    showCancelButton: true, closeOnConfirm: true,
                    animation: "slide-from-top",
                    closeOnCancel: true,
                }).then(function (isConfirm) {
                    if (isConfirm) {
                                      $scope.SenddataObject = [];
                                      $scope.ques_ans_key = '';
                                      for (var j = 0; j < $scope.start_Survey.length; j++) {
                                          for (var i = 0; i < $scope.start_Survey[j].que_ans.length; i++) {
                                              if ($scope.start_Survey[j].que_ans[i].question_ans_status == true && $scope.start_Survey[j].que_ans[i].sims_survey_question_type != '5') {
                                                  $scope.ques_ans_key = $scope.ques_ans_key + $scope.start_Survey[j].sims_survey_question_code + $scope.start_Survey[j].que_ans[i].sims_survey_question_answer_code + ',';

                                                  var data = {};
                                                  data.sims_survey_user_response_code = user;
                                                  data.sims_survey_code = $scope.start_Survey[j].sims_survey_code;
                                                  data.sims_survey_question_code = $scope.start_Survey[j].sims_survey_question_code;
                                                  data.sims_survey_question_answer_code = $scope.start_Survey[j].que_ans[i].sims_survey_question_answer_code
                                                  data.sims_survey_question_answer_desc_en = null

                                                  $scope.SenddataObject.push(data);

                                              }
                                              else if ($scope.start_Survey[j].que_ans[i].sims_survey_question_type == '5') {
                                                  if ($scope.start_Survey[j].que_ans.question_ans_status == undefined) {
                                                      break;
                                                  }
                                                  else {
                                                      var data = {};
                                                      data.sims_survey_user_response_code = user;
                                                      data.sims_survey_code = $scope.start_Survey[j].sims_survey_code;
                                                      data.sims_survey_question_code = $scope.start_Survey[j].sims_survey_question_code;
                                                      data.sims_survey_question_answer_code = $scope.start_Survey[j].que_ans.question_ans_status;
                                                      data.sims_survey_question_answer_desc_en = null;
                                                      //data.attempt_count = $scope.questionbank_code_Selected.attempt_count;
                                                      //data.attempt_time = $scope.totalSeconds;
                                                      $scope.SenddataObject.push(data);
                                                      break;
                                                  }
                                              }

                                              else if ($scope.start_Survey[j].que_ans[i].sims_survey_question_type == '2' && $scope.start_Survey[j].que_ans[i].sims_survey_question_answer_desc_en1 != '' && $scope.start_Survey[j].que_ans[i].sims_survey_question_answer_desc_en1 != undefined) {

                                                  var data = {};
                                                  data.sims_survey_user_response_code = user;
                                                  data.sims_survey_code = $scope.start_Survey[j].sims_survey_code;
                                                  data.sims_survey_question_code = $scope.start_Survey[j].sims_survey_question_code;
                                                  data.sims_survey_question_answer_code = $scope.start_Survey[j].que_ans[i].sims_survey_question_answer_code
                                                  data.sims_survey_question_answer_desc_en = $scope.start_Survey[j].que_ans[i].sims_survey_question_answer_desc_en1
                                                  $scope.SenddataObject.push(data);

                                              }


                                          }
                                      }

                                      if ($scope.SenddataObject.length > 0) {
                                          $http.post(ENV.apiUrl + "api/surveyselection/CUD_PP_Survey", $scope.SenddataObject).then(function (survey_res) {
                                              $scope.Surveyresult = survey_res.data;
                                              if ($scope.Surveyresult == true) {
                                                  $scope.survey_detail_table = true;
                                                  $scope.start_survey_table = false;
                                                  $scope.maxmark1 = '0%';
                                                  $scope.progress_counter = '0';
                                                  swal({ title: "Alert", text: 'Survey Submitted Successfully', width: 300, height: 250, showCloseButton: true });
                                                  window.localStorage.removeItem('survey' + $scope.survey_code);
                                                  $scope.get_link();
                                              } else {
                                                  $scope.btn_pre = false;
                                                  swal({ title: "Alert", text: 'Survey Not Submitted', width: 300, height: 250, showCloseButton: true });
                                              }
                                          });
                                      } else {
                                          swal({ title: "Alert", text: 'Select Atleast One Answer Or Back', width: 300, height: 250, showCloseButton: true });
                                      }
                                  }
                                  
                              });



            }

            $scope.Back_btn_click = function () {
                $scope.survey_detail_table = true;
                $scope.start_survey_table = false;
                $scope.maxmark1 = '0%';
                $scope.progress_counter = '0';
            }
            $scope.btn_sub1 = true;
            $scope.answer_change = function (str) {

                for (var i = 0; i < str.length; i++) {
                    if (str.question_ans_status == undefined) {
                        //str[i].question_ans_status = true
                        break;
                    }
                    else {
                        str[i].question_ans_status = true
                        break;
                    }
                }

                $scope.progress_counter = 0;
                var counter = 0;
                for (var j = 0; j < $scope.start_Survey.length; j++) {
                    for (var i = 0; i < $scope.start_Survey[j].que_ans.length; i++) {

                        if ($scope.start_Survey[j].que_ans[i].question_ans_status == true) {
                            counter = counter + 1;
                            $scope.progress_counter = $scope.progress_counter + 1;
                            $scope.maxmark1 = Math.round(($scope.progress_counter * 100) / $scope.ques_count) + '%';

                            break;
                        }
                        if ($scope.start_Survey[j].que_ans[i].sims_survey_question_type == '2' && $scope.start_Survey[j].que_ans[i].sims_survey_question_answer_desc_en1 != '' && $scope.start_Survey[j].que_ans[i].sims_survey_question_answer_desc_en1 != undefined) {
                            counter = counter + 1;
                            $scope.progress_counter = $scope.progress_counter + 1;
                            $scope.maxmark1 = Math.round(($scope.progress_counter * 100) / $scope.ques_count) + '%';

                            break;
                        }
                    }
                }
                if (counter == $scope.start_Survey.length)
                    $scope.btn_sub1 = false;
                else
                    $scope.btn_sub1 = true;



            }



            $scope.range = function () {
                var rangeSize = 20;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;


            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.start_Survey.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;

                $scope.binding_object = $scope.start_Survey[n];
                $scope.question_counter1 = n + 1;

                if (n == 1) {
                    $scope.btn_pre = false;
                    $scope.btn_next = true;
                }
                else {

                }

                $scope.progress_counter = 0;
                for (var j = 0; j < $scope.start_Survey.length; j++) {
                    for (var i = 0; i < $scope.start_Survey[j].que_ans.length; i++) {

                        if ($scope.start_Survey[j].que_ans[i].question_ans_status == true) {
                            $scope.progress_counter = $scope.progress_counter + 1;
                            $scope.maxmark1 = Math.round(($scope.progress_counter * 100) / $scope.ques_count) + '%';
                        }
                        if ($scope.start_Survey[j].que_ans[i].sims_survey_question_type == '2' && $scope.start_Survey[j].que_ans[i].sims_survey_question_answer_desc_en1 != '' && $scope.start_Survey[j].que_ans[i].sims_survey_question_answer_desc_en1 != undefined) {
                            $scope.progress_counter = $scope.progress_counter + 1;
                            $scope.maxmark1 = Math.round(($scope.progress_counter * 100) / $scope.ques_count) + '%';
                        }
                    }
                }
            };

         }])
})();