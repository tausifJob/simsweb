﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, main1, deletefin = [];
    var data = [], data1 = [];
    var simsController = angular.module('sims.module.Student');
    simsController.controller('StudentBehaviourTargetDetailsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.pagesize = '';
            $scope.pageindex = 1;
            $scope.table = true;
            $scope.Main_table = false;
            $scope.searchitems = [];
            $scope.GetItems = [];
            var flagri = false;
            $scope.hide = true;

            //console.clear();

          

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.gettargetcode = function () {
                $http.get(ENV.apiUrl + "api/StudentReport/gettargetcode").then(function (target) {
                    $scope.targetdata = target.data;
                    $scope.temp = {
                            sims_target_code: $scope.targetdata[0].sims_target_code,
                    }
                });
            }

            $scope.gettargetcode();

            $scope.Add = function () {

                if ($scope.flagrs = true) {
                    if ($scope.searchitems.length == 0) {
                        $scope.searchitems = [];
                    }
                }

                var GetItemsadd = ({
                    'sims_target_line_no': $scope.edt.sims_target_code,
                    'sims_target_desc': $scope.edt.sims_target_desc,
                    'sims_student_behaviour_target_status': $scope.edt.sims_student_behaviour_target_status,

                });
                $scope.searchitems.push(GetItemsadd);
                $scope.Main_table = true;
                $scope.edt.sims_target_desc = '';

                $scope.display = true;
                $scope.save_btn = true;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            $scope.edit = function () {
                $scope.hide1 = true;
                $scope.hide = false;
                $http.get(ENV.apiUrl + "api/StudentReport/gettargetcodes").then(function (targets) {
                    $scope.targetdatas = targets.data;
                });
            }

            $scope.gettargetdata = function () {
                $http.get(ENV.apiUrl + "api/StudentReport/gettargetcodesdetails?targetcode=" + $scope.temp1.sims_target_code).then(function (targetdata) {
                    $scope.searchitems = targetdata.data;
                });
            }

            $scope.RemoveItemdetails = function ($event, index, str) {
                str.splice(index, 1);
                if ($scope.searchitems.length == 0) {
                    $scope.Main_table = false;
                }
            }

           

            var datasend = [];
            var datasend1 = [];

                
                $scope.Save = function (Myform) {
                debugger;
                if (Myform) {
                    if ($scope.searchitems.length > 0) {
                        var j = 0;
                        for (var i = 0; i < $scope.searchitems.length; i++) {
                            j = i;
                            data = {
                                'opr': 'I',
                                'sims_target_code': $scope.temp.sims_target_code,
                                'sims_target_code_line_no': j + 1,
                                'sims_target_desc': $scope.searchitems[i].sims_target_desc,
                                'sims_student_behaviour_target_status': $scope.searchitems[i].sims_student_behaviour_target_status
                            }
                            datasend.push(data);
                        }

                        //$http.post(ENV.apiUrl + "api/StudentReport/CUDTtargetdetails", datasend).then(function (msg) {
                        $http.post(ENV.apiUrl + "api/StudentReport/CUDTtargetdetails?targetcode=" + '', datasend).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 != null || $scope.msg1 != undefined) {
                                var m = $scope.msg1.strMessage;
                                swal({ title: "Alert", text: m, width: 450, height: 200 });
                                $scope.gettargetcode();
                            }
                        });
                        datasend = [];
                        datasend1 = [];
                        $scope.Clear();
                    }
                }
            }

            $scope.Update = function () {
                debugger;
                if ($scope.searchitems.length > 0) {
                    var j = 0;
                    for (var i = 0; i < $scope.searchitems.length; i++) {
                        j = i;
                        data = {
                            'opr': 'U',
                            'sims_target_code': $scope.temp1.sims_target_code,
                            'sims_target_code_line_no': j + 1,
                            'sims_target_desc': $scope.searchitems[i].sims_target_desc,
                            'sims_student_behaviour_target_status': $scope.searchitems[i].sims_student_behaviour_target_status
                        }
                        datasend.push(data);
                    }
                    // $http.post(ENV.apiUrl + "api/RequestDetail/CUDRequestDetails?data1=" + JSON.stringify($scope.data1), datasend).then(function (msg) {
                    $http.post(ENV.apiUrl + "api/StudentReport/CUDTtargetdetails?targetcode=" + $scope.temp1.sims_target_code, datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 != null || $scope.msg1 != undefined) {
                            var m = $scope.msg1.strMessage;
                            swal({ title: "Alert", text: m, width: 450, height: 200 });
                            $scope.gettargetcode();
                        }
                    });
                    datasend = [];
                    datasend1 = [];
                    $scope.Clear();
                }
            }


            $scope.Clear = function () {

                $scope.searchitems = [];
                $scope.GetItems = [];
                $scope.Main_table = false;
                $scope.hide1 = false;
                $scope.hide = true;
                
                $scope.edt = {
                    sims_target_desc: '',
                }

                $scope.temp1 = '';
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });
        }
         ])
})();