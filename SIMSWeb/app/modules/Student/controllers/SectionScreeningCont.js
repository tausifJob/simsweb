﻿(function () {
    'use strict';
    var del = [];
    var main, grade;
    var simsController = angular.module('sims.module.Student');
    
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SectionScreeningCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.pager = true;
            $scope.sectionScreening = [];
            $scope.grid1 = true;
            $scope.grid = true;
            $scope.display = false;
            $scope.temp = {};

            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });


            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

              $scope.countData = [

                  { val: 10, data: 10 },
                  { val: 20, data: 20 },
                  //{ val: 15, data: 15 },

            ]

            $http.get(ENV.apiUrl + "api/common/SectionScreening/Get_All_Section_Screening").then(function (res1) {
                $scope.obj1 = res1.data;
                if ($scope.obj1.length > 0)
                {
                    $scope.table = true;
                    $scope.pager = true;
                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.obj1.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.obj1.length, data: 'All' })
                    }
                    $scope.totalItems = $scope.obj1.length;
                    $scope.todos = $scope.obj1;
                    $scope.makeTodos();
                }
            });

          

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };



            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj1, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj1;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.screening_grade_code_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.screening_criteria_code_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.screening_rating_code_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.screening_academic_year.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.desg_company_code == toSearch) ? true : false;
            }

            $scope.edt={};

            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.obj2 = res.data;

                $scope.edt['screening_cur_code'] = $scope.obj2[0].sims_attendance_cur_code;
                $scope.getAcdyr($scope.obj2[0].sims_attendance_cur_code);
            });



            $http.get(ENV.apiUrl + "api/common/SectionScreening/getAllCriteriaNames").then(function (res) {
                $scope.critype = res.data;
            });



            $scope.getAcdyr = function (crcode) {
               
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + crcode).then(function (res) {
                    $scope.acad = res.data;
                    $scope.edt['screening_academic_year'] = $scope.acad[0].sims_academic_year;

                    $scope.GetGrade(crcode, $scope.acad[0].sims_academic_year);
                    
                });
            }

            $scope.GetRating = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/common/SectionScreening/getRating").then(function (res1) {
                    
                    $scope.rate_data = res1.data;
                    console.log("GetRating",$scope.rate_data);
                    $scope.temp.sims_survey_rating_group_code = $scope.rate_data[0].sims_survey_rating_group_code;



                });

            }

            $scope.GetGrade = function (cur, acad_yr) {

                if (cur != null && acad_yr != null) {
                    $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + cur + "&academic_year=" + acad_yr).then(function (res) {
                        $scope.grade = res.data;
                      
                        setTimeout(function () {
                            $('#cmb_grade').change(function () {
                                console.log($(this).val());
                            }).multipleSelect({
                                width: '100%'
                            });
                        }, 1000);
                    });


                }
            }

            $scope.GetSection = function (cur, acad_yr, grade) {
                console.log(grade);
                if (cur != null && acad_yr != null && grade != null) {
                    $http.get(ENV.apiUrl + "api/common/SectionScreening/getAllSection?cur_code=" + cur + "&ac_year=" + acad_yr + "&grade=" + grade).then(function (res) {

                        $scope.section = res.data;
                        console.log($scope.section);
                        setTimeout(function () {
                            $('#cmb_section').change(function () {
                                console.log($(this).val());
                            }).multipleSelect({
                                width: '100%'
                            });
                        }, 1000);
                    });
                }
            }

           

            $scope.getscreening = function () {
                $http.get(ENV.apiUrl + "api/common/SectionScreening/Get_All_Section_Screening").then(function (res1) {
                    $scope.obj1 = res1.data;
                    if ($scope.obj1.length > 0)
                    {
                        $scope.table = true;
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.obj1.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.obj1.length, data: 'All' })
                        }
                        $scope.totalItems = $scope.obj1.length;
                        $scope.todos = $scope.obj1;
                        $scope.makeTodos();
                    }
                });
            }

            $scope.edit = function (str) {
                debugger;
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.flag = false;
                    $scope.display = true;
                    $scope.grid1 = false;
                    $scope.grid = false;
                    $scope.btn_save1 = false;
                    $scope.btn_update1 = true;
                    $scope.delete1 = false;
                    $scope.ermsgshow = false;
                    $scope.select_grade = false;
                    $scope.txt_grade = true;
                    $scope.sec_screen = true;

                    console.log(str);
                    $scope.edt = str;
                    // $scope.getAcdyr(str.screening_cur_code);
                    $scope.sec_screen = true;

                    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str.screening_cur_code).then(function (res) {
                        $scope.acad = res.data;

                    });

                    grade = $scope.edt.screening_grade_code;

                    var gradeValues = [];
                    $scope.grade_codenew = str.screening_grade_code.split(',');


                    for (var i = 0; i < $scope.grade_codenew.length; i++) {
                        gradeValues.push($scope.grade_codenew[i]);
                    }

                    $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str.screening_cur_code + "&academic_year=" + str.screening_academic_year).then(function (res) {
                        $scope.grade = res.data;

                        setTimeout(function () {
                            $('#cmb_grade').change(function () {
                                console.log($(this).val());
                            }).multipleSelect({
                                width: '100%'
                            });
                            $("#cmb_grade").multipleSelect("setSelects", gradeValues);
                        }, 1000);
                    });

                    $("#cmb_grade").multipleSelect("disable");
                    console.log(grade);


                }
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.grid1 = false;
                    $scope.btn_save1 = true;
                    $scope.btn_update1 = false;
                    $scope.delete1 = false;
                    $scope.flag = true;
                    $scope.GetRating();
                    var grade_code = "";
                    $scope.edt['screening_grade_code'] = "";
                    $scope.edt['screening_criteria_code'] = "";
                    $scope.edt['screening_rating_code'] = "";
                    $scope.edt['screening_rating'] = "";
                    $scope.edt['screening_marks'] = "";
                    $scope.select_grade = true;
                    $scope.txt_grade = false;
                    $scope.sec_screen = false;
                    $("#cmb_grade").multipleSelect("enable");
                    try {
                        $('#cmb_grade').multipleSelect('uncheckAll');
                    } catch (e) { }
                    $scope.myForm.$setPristine();
                    $scope.myForm.$setUntouched();

                    //$http.get(ENV.apiUrl + "api/common/SectionScreening/getRating").then(function (res1) {
                    //    debugger;
                    //    //$scope.ratinggrp = res.data;
                    //    $scope.rate_data = res1.data;
                    //    $scope.temp.rating_data = $scope.rate_data[0].sims_survey_rating_group_code;

                    //});
                }
            }

            var datasend = [];
            $scope.Save = function (isvalidate) {
                
              
                if (isvalidate)
                {
                    if ($scope.btn_update1 == false)
                    {
                        var data = $scope.edt;
                        // var str_section;
                        var grade_code;

                        var grade = $scope.edt.screening_grade_code;
                        grade_code = grade_code + ',' + grade;
                        var str2 = grade_code.substr(grade_code.indexOf(',') + 1);

                        data.screening_grade_code = str2;
                        //data.screening_grade_sec = str1;
                        data.opr = "Y";
                        datasend.push(data);

                        $http.post(ENV.apiUrl + "api/common/SectionScreening/CUDSection_Screening", datasend).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                            }
                            else {
                                swal({ text: "Selected Grade, Section And Criteria is Already Defined. " + $scope.msg1, imageUrl: "assets/img/notification-alert.png", width: 380, showCloseButton: true });
                            }

                            $scope.getscreening();

                            $scope.grid1 = true;
                            $scope.grid = true;
                            $scope.display = false;
                        });

                        datasend = [];
                    }
                    else {
                        $scope.Update(isvalidate);
                    }

                }

            }

            var dataupdate = [];
            $scope.Update = function (isvalidate) {
                
                if (isvalidate) {
                    var data = $scope.edt;
                    var grade_code;
                    var grade = $scope.edt.screening_grade_code;
                    grade_code = grade_code + ',' + grade;
                    var str2 = grade_code.substr(grade_code.indexOf(',') + 1);

                    data.screening_grade_code = str2;

                    data.opr = "U";
                    dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/common/SectionScreening/CUDSection_Screening", dataupdate).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $scope.getscreening();

                        $scope.grid1 = true;
                        $scope.grid = true;
                        $scope.display = false;
                    });
                    dataupdate = [];
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.grid1 = true;
                $scope.sec_screen = false;
                $scope.ermsgshow = false;
                $scope.getscreening();
            }


            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-"+i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    var deletefin = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodulecode = ({
                                'screening_cur_code': $scope.filteredTodos[i].screening_cur_code,
                                'screening_academic_year': $scope.filteredTodos[i].screening_academic_year,
                                'screening_grade_code': $scope.filteredTodos[i].screening_grade_code,
                                'screening_criteria_code': $scope.filteredTodos[i].screening_criteria_code,
                                'screening_rating_code': $scope.filteredTodos[i].screening_rating_code,
                                opr: 'D'
                            });
                            deletefin.push(deletemodulecode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $http.post(ENV.apiUrl + "api/common/SectionScreening/CUDSection_Screening?data=", deletefin).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getscreening();
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                                $scope.currentPage = true;
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {

                                                $scope.getscreening();
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                                $scope.currentPage = true;
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                    $scope.getscreening();

                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById("test-" + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                    }
                    $scope.currentPage = str;
                }
            }

            $scope.size = function (str) {
                if (str == 10 || str == 20 || str == 'All') {
                    if (str == "All") {
                        $scope.currentPage = '1';
                        $scope.filteredTodos = $scope.obj1;
                        $scope.pager = false;
                    }
                    else {
                        $scope.pager = true;
                        $scope.pagesize = str;
                        $scope.currentPage = 1;
                        $scope.numPerPage = str;
                        $scope.makeTodos();
                    }
                }
                else {
                    $scope.pager = false;
                }
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;

                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.ermsgshow = false;
            $scope.mesgMark = function (curr) {
                if ($scope.flag) {
                    $scope.ermsgshow = false;
                    $scope.mesg = '';
                } else {
                    $scope.ermsgshow = true;
                    var curgrade = $("#cmb_grade option:selected").text();
                    $scope.mesg = 'Assessement marks for student in ' + curgrade + ' will be update';
                    $scope.flag = false;
                }
            }



         }])
})();