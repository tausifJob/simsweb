﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AdmissionDashboard_SISOCont',
        ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.display = true;
            $scope.grid = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.pagesize1 = "5";
            $scope.pageindex1 = "1";
            $scope.pagesize2 = "5";
            $scope.pageindex2 = "1";
            $scope.pagesize3 = "5";
            $scope.pageindex3 = "1";
            $scope.pagesize4 = "5";
            $scope.pageindex4 = "1";

            $scope.btn_visible = '';
            $scope.gender = '';
            $scope.btn_nationality = '';
            $scope.btn_emp = '';
            $scope.btn_siblingenroll = '';

            $scope.flag = true;
            var str, cnt;
            $scope.dash = [];
            $scope.view = [];
            $scope.screen_datails = [];
            $scope.Upload_doc_datails = [];
            var admdetails = [];
            $scope.btn_prospect = true;
            $scope.btn_edit = true;
            $scope.btn_reject = true;
            $scope.btn_selected = false;
            var del = [];
            var t = false;
            var main, section = "", fee_category = "";
            $scope.filesize = true;
            var date = new Date();
            $scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.email = { 'sims_msg_subject': undefined }
            //$scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss');
            $scope.once = true;
            var param = $stateParams.Class;

            $scope.filteredTodos1 = [], $scope.currentPage1 = 1, $scope.numPerPage1 = 5, $scope.maxSize1 = 5;
            $scope.filteredTodos2 = [], $scope.currentPage2 = 1, $scope.numPerPage2 = 5, $scope.maxSize2 = 5;
            $scope.filteredTodos3 = [], $scope.currentPage3 = 1, $scope.numPerPage3 = 5, $scope.maxSize3 = 5;
            $scope.filteredTodos4 = [], $scope.currentPage4 = 1, $scope.numPerPage4 = 5, $scope.maxSize4 = 5;


            $scope.docurl = ENV.apiUrl + '/content/' + $http.defaults.headers.common['schoolId'];

            $scope.exportData = function () {
                debugger;

                var check = true;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {//exportable
                            var blob = new Blob([document.getElementById('Div21').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, "Report.xls");


                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Report Not Save", showCloseButton: true, width: 380, })
                }
            };

            $timeout(function () {
                $("#fixedtable,#fixedtable4").tableHeadFixer({ 'top': 1 });
            }, 100);


            // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {
                debugger;
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;

                $scope.propertyName = propertyName;
            };
            //Sorting

            if (param != "") {
                $scope.edt =
                    {
                        curr_code: param.curr_code,
                        academic_year: param.academic_year,
                        grade_code: param.grade_code,
                        sims_appl_parameter_reg: param.sims_appl_parameter_reg,
                        //sims_flag: 
                    };
            }

            //$scope.printDetails = function (str) {
            //    console.log(str);
            //    var data = {
            //        location: 'Sims.SIMR41',
            //        parameter: {
            //            admission_no: str,
            //        },
            //        state: 'main.Sim000',
            //        ready: function (
            //            this.refreshReport();
            //        },
            //    }
            //    console.log(data);

            //    window.localStorage["ReportDetails"] = JSON.stringify(data);
            //    $state.go('main.ReportCardParameter');
            //}

            $scope.printDetails = function (str) {
                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetAdm_dashRpt").then(function (res) {
                    $scope.reportparameter = res.data;
                    var data = {
                        location: res.data,
                        parameter: {
                            admission_no: str,
                        },
                        state: 'main.DsSISO',
                        ready: function () {
                            this.refreshReport();
                        },
                    }
                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                    $state.go('main.ReportCardParameter')
                });

            }


            /*start_paging_gridview*/

            $scope.size3 = function (pagesize) {
                $scope.itemsPerPage3 = pagesize;
            }

            $scope.range3 = function () {
                var rangeSize3 = 5;
                var ret3 = [];
                var start3;

                start3 = $scope.currentPage3;
                if (start3 > $scope.pageCount3() - rangeSize3) {
                    start3 = $scope.pageCount3() - rangeSize3 + 1;
                }

                for (var i = start3; i < start3 + rangeSize3; i++) {
                    if (i >= 0)
                        ret3.push(i);
                }
                return ret3;
            }

            $scope.prevPage3 = function () {
                if ($scope.currentPage3 > 0) {
                    $scope.currentPage3--;
                }
            }

            $scope.prevPageDisabled3 = function () {
                return $scope.currentPage3 === 0 ? "disabled" : "";
            };

            $scope.pageCount3 = function () {
                return Math.ceil($scope.Upload_doc_datails.length / $scope.itemsPerPage3) - 1;
            };

            $scope.nextPage3 = function () {
                if ($scope.currentPage3 < $scope.pageCount3()) {
                    $scope.currentPage3++;
                }
            };

            $scope.nextPageDisabled3 = function () {
                return $scope.currentPage3 === $scope.pageCount3() ? "disabled" : "";
            };

            $scope.setPage3 = function (n) {
                $scope.currentPage3 = n;
            };

            /*End Region*/

            $scope.Cancel = function () {
                $scope.view = "";
                $scope.maingrid1 = true;
                $scope.Screening = false;
                $scope.Upload_doc = false;
                $scope.div_Communication = false;
                $scope.div_CommunicationHist = false;
                $scope.commcancel_btn();
            }

            $scope.link = function (str) {

                debugger;
                $('#viewdashDetailsModal').modal('hide');
                setTimeout(function () {
                    $state.go("main.AdSISO", { admission_num: str, Class: $scope.edt });
                }, 500);
            }


            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.obj2 = res.data;
                $scope.edt['curr_code'] = $scope.obj2[0].sims_attendance_cur_code;
                $scope.curr_code = $scope.obj2[0].sims_attendance_cur_code;
                $scope.getCur($scope.edt.curr_code);
            });

            $scope.getCur = function (cur_code) {
                $http.get(ENV.apiUrl + "api/common/getAllAcademicYearDash?cur_code=" + cur_code).then(function (res) {
                    $scope.obj1 = res.data;
                    $scope.edt['academic_year'] = $scope.obj1[0].sims_academic_year;
                    $scope.acad_yr = $scope.obj1[0].sims_academic_year;
                    $scope.GetGrade(cur_code, $scope.acad_yr, $scope.edt.sims_grade_code, $scope.edt.sims_appl_parameter_reg);
                });


                $scope.GetInfo(cur_code, $scope.acad_yr, $scope.edt.grade_code, $scope.edt.sims_appl_parameter_reg);

            }

            $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/getRegistration").then(function (res) {
                $scope.reg = res.data;
            });

            //Get Dashboard Details
            var da = false
            if (window.localStorage['Admflag'] != "true") {
                window.localStorage.removeItem("Admflag");
            }

            function labelFormatter(label, series) {
                return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>"
            //+ label + "<br/>" 
                    + Math.round(series.percent) + "</div>";
            }

            $scope.GetInfo = function (cur_code, AcadmicYear, gradeCode, reg) {
                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetDashTilesDetails?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode).then(function (res) {
                    $scope.dashTiles = res.data;
                    console.log(res.data);
                });

                if (cur_code === '' && AcadmicYear === '' && gradeCode === '' && reg === '') {
                }
                else {
                    //+ "&admission_status=" + reg
                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetEmployeeCodecntDetails?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode).then(function (res) {
                        $scope.empCnt_details = res.data;
                        if ($scope.empCnt_details.count == 0) {
                            swal({ title: "Alert", text: "No Record Found", showCloseButton: true, width: 380, });
                        }
                        $scope.grid = true;
                    });

                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetsiblingcntDetails?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode).then(function (res) {
                        $scope.siblingCnt_details = res.data;
                        if ($scope.siblingCnt_details.count == 0) {
                            swal({ title: "Alert", text: "No Record Found", showCloseButton: true, width: 380, });
                        }
                        $scope.grid = true;
                    });

                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetGendercntDetails?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode).then(function (res) {
                        $scope.genderCnt_deatails = res.data;
                        $scope.grid = true;

                        $scope.labels = [];
                        $scope.data = [];
                        $scope.value = [];

                        var doughnutData = [];

                        for (var i = 0; i < $scope.genderCnt_deatails.length; i++) {
                            $scope.labels.push($scope.genderCnt_deatails[i].gender);
                            $scope.data.push(parseInt($scope.genderCnt_deatails[i].gender_cnt));
                            $scope.value.push($scope.genderCnt_deatails[i].gender_code)
                        }

                        $scope.grid = true;

                    });

                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetNationalityCntDetails?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode).then(function (res) {
                        $scope.nationalityCnt_deatails = res.data;
                        $scope.grid = true;

                        $scope.chartLabels = [];
                        $scope.chartData = [];
                        $scope.chartColor = [];

                        var pieChartData = [];

                        for (var i = 0; i < $scope.nationalityCnt_deatails.length; i++) {
                            var color = Math.floor(Math.random() * 16777216).toString(16);
                            color: '#000000'.slice(0, -color.length) + color;
                            $scope.nationalityCnt_deatails[i].color = color;

                            $scope.chartLabels.push($scope.nationalityCnt_deatails[i].nationality_name);
                            $scope.chartData.push(parseInt($scope.nationalityCnt_deatails[i].nationality_code));
                            $scope.chartColor.push($scope.nationalityCnt_deatails[i].color);


                        }


                    });
                }

                if (window.localStorage['Admflag'] == "true") {
                    $scope.viewdashDetailsModal(param.curr_code, param.academic_year, param.grade_code, param.sims_appl_parameter_reg, null, null, null, null);
                    window.localStorage.removeItem("Admflag");
                }

            }

            $scope.GetGrade = function (cur, acad_yr, grade, reg) {
                // console.log($scope.obj1[0].sims_academic_year);
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + cur + "&academic_year=" + acad_yr).then(function (res) {
                    $scope.obj3 = res.data;
                    $scope.edt['grade_code'] = $scope.obj3[0].sims_grade_code;
                    $scope.grade = $scope.obj3[0].sims_grade_code;
                    $scope.GetGradechage($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.edt.sims_appl_parameter_reg);
                });


                if (reg === "") {
                }
                else {
                    $scope.GetInfo(cur, acad_yr, grade, reg);
                }
            }

            $scope.GetGradechage = function (cur, acad_yr, grade, reg) {
                if (reg === "") {
                    $scope.GetInfo(cur, acad_yr, grade, reg);
                }
                else {
                    $scope.GetInfo(cur, acad_yr, grade, reg);
                }
            }

            //Show_Tile_Details

            $scope.viewdashDetailsModal = function (cur_code, AcadmicYear, gradeCode, adm_status, gender, nationality, employee_code, sibling_enroll) {
                $scope.btn_visible = adm_status;
                $scope.gender = gender;
                $scope.btn_nationality = nationality;
                $scope.btn_emp = employee_code;
                $scope.btn_siblingenroll = sibling_enroll;

                if (adm_status == 'W') {
                    $scope.btn_prospect = false;
                    $scope.btn_reject = false;
                }
                if (adm_status == 'R') {
                    $scope.btn_prospect = false;
                    $scope.btn_reject = true;
                }
                if (adm_status == 'C') {
                    $scope.btn_prospect = true;
                    $scope.btn_reject = false;
                }
                if (adm_status == '') {
                    $scope.btn_prospect = true;
                    $scope.btn_reject = true;
                }
                if (adm_status == '1') {
                    adm_status = 'W';
                }
                if (adm_status == '2') {
                    adm_status = 'R';
                }


                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetStudents?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode + "&admission_status=" + adm_status + "&gender_code=" + gender + "&nationality_code=" + nationality + "&employee_code=" + employee_code + "&sibling_enroll=" + sibling_enroll).then(function (res) {
                    $scope.dash_data = res.data;
                    $scope.grid = true;
                    $scope.maingrid1 = true;
                    if ($scope.dash_data.length > 0) {
                        $('#viewdashDetailsModal').modal({ backdrop: 'static', keyboard: true });
                    }
                    else {
                        swal({ title: "Alert", text: "No Record Found", showCloseButton: true, width: 380, });
                    }
                });

            }

            $scope.getEmpDetails = function () {
                var empDetails = [];
                for (var i = 0; i < $scope.empCnt_details.length; i++) {
                    empDetails = empDetails + $scope.empCnt_details[i].empl_code + ',';
                }

                if (empDetails.length == 0) {
                    //$scope.viewdashDetailsModal(null, null, null, null, null, null, empDetails, null);
                }
                else {
                    $scope.btn_emp = empDetails;
                    $scope.viewdashDetailsModal($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.btn_visible, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll);
                }
            }

            $scope.getSiblingDetails = function () {
                var siblingDetails = [];
                for (var j = 0; j < $scope.siblingCnt_details.length; j++) {
                    siblingDetails = siblingDetails + $scope.siblingCnt_details[j].sibling_enroll + ',';
                }

                if (siblingDetails.length == 0) {
                    //$scope.viewdashDetailsModal(null, null, null, null, null, null, null, siblingDetails);
                }
                else {
                    $scope.btn_siblingenroll = siblingDetails;
                    $scope.viewdashDetailsModal($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.btn_visible, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll);
                }
            }

            $scope.chartLabels = ['Series A', 'Series B', 'Series C'];
            $scope.chartData = [65, 59, 20];
            $scope.chartColor = ['#CCCCCC', '#CCCCCC', '#CCCCCC'];



            $(document).keyup(function (e) {
                if (e.keyCode == 27) { // escape key maps to keycode `27`
                    // <DO YOUR WORK HERE>

                    $('#viewdashDetailsModal').modal('hide');
                    $scope.Cancel();
                }
            });

            $scope.onClickSlice = function (points, evt) {
                console.log(points, evt);
                $scope.btn_nationality = points[0]._model.label;
                $scope.viewdashDetailsModal($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.btn_visible, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll);
            };

            $scope.labels = ["ABC", "XYZ"];
            $scope.data = [300, 500];

            $scope.onClick = function (points, evt) {
                console.log(points, evt);
                $scope.gender = points[0]._model.label;
                //console.log(points[0]._model.label);
                $scope.viewdashDetailsModal($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.btn_visible, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll);
            };

            $scope.getCur(param.curr_code);

            $scope.GetGrade(param.curr_code, param.academic_year, param.grade_code, param.sims_appl_parameter_reg);

            $scope.GetInfo(param.curr_code, param.academic_year, param.grade_code, param.sims_appl_parameter_reg);

            $scope.check1 = function (dash) {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

                admdetails = dash.admission_number;
                var v = document.getElementById(dash.admission_number);
                if (v.checked == true)
                    // if (dash.admission_number1==true)
                {
                    $scope.btn_edit = false;
                    $scope.btn_reject = false;
                    //if (dash.curr_code == '' || dash.academic_year == '' || dash.grade_code == '' || dash.section_code == '' || dash.fee_category == '')
                    //{
                    //    swal({ title: "Alert", text: "Please Check Whether Section and Fee Category are Selected for this Students", showCloseButton: true, width: 380, });
                    //    v.checked = false;
                    //    var index = del.indexOf(dash.admission_number);

                    //    if (index > -1) {
                    //        del.splice(index, 3);
                    //    }
                    //}
                    //else {
                    del.push(dash.admission_number, dash.section_code, dash.fee_category);
                    $scope.row1 = '';
                    //}
                }
                else {
                    //$scope.btn_edit = true;

                    v.checked = false;
                    var index = del.indexOf(dash.admission_number);

                    if (index > -1) {
                        del.splice(index, 3);
                    }
                }
            }

            $scope.check = function () {
                main = document.getElementById('mainchk');
                del = [];
                if (main.checked == true) {
                    debugger;
                    for (var i = 0; i < $scope.dash_data.length; i++) {
                        var v = document.getElementById($scope.dash_data[i].admission_number);
                        // console.log($scope.dash_data[i].curr_code);
                        //if ($scope.dash_data[i].curr_code == '' || $scope.dash_data[i].academic_year == '' || $scope.dash_data[i].grade_code == '' || $scope.dash_data[i].section_code == '' || $scope.dash_data[i].fee_category == '')
                        //{
                        //    v.checked = false;
                        //    var index = del.indexOf($scope.dash_data[i].admission_number);

                        //    if (index > -1) {
                        //        del.splice(index, 3);
                        //    }
                        //}
                        //else
                        //{

                        // if ($scope.dash_data[i].admission_number1 == true)
                        //{

                        v.checked = true;
                        del.push($scope.dash_data[i].admission_number, $scope.dash_data[i].section_code, $scope.dash_data[i].fee_category);
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                        //}
                        //}
                    }
                }
                else {
                    for (var i = 0; i < $scope.dash_data.length; i++) {
                        var t = $scope.dash_data[i].admission_number;
                        var v = document.getElementById(t, $scope.dash_data[i].section_code, $scope.dash_data[i].fee_category);
                        v.checked = false;
                        del.pop(t);
                        $scope.row1 = '';
                    }
                }
            }

            //Promote the Admissions
            $scope.getPromote = function () {
                admdetails = [];
                for (var i = 0; i < $scope.dash_data.length; i++) {
                    var t = $scope.dash_data[i].admission_number;
                    var v = document.getElementById(t);

                    if (v.checked == true) {
                        if ($scope.dash_data[i].curr_code == '' || $scope.dash_data[i].academic_year == '' || $scope.dash_data[i].grade_code == '' || $scope.dash_data[i].section_code == '' || $scope.dash_data[i].fee_category == '') {
                            swal({ title: "Alert", text: "Please Check Whether Section and Fee Category are Selected for this Students", showCloseButton: true, width: 380, });
                            v.checked = false;
                            $scope.row1 = '';
                            $('tr').removeClass("row_selected");
                        }
                        else {
                            admdetails = admdetails + $scope.dash_data[i].admission_number + ',';
                        }
                    }
                }
                //if (admdetails.length == 0) {
                //    swal({ title: "Alert", text: "Nothing Selected", showCloseButton: true, width: 380, });
                //}
                if (admdetails.length > 0) {
                    var data = $scope.edt;
                    data.user_code = $rootScope.globals.currentUser.username;
                    data.admission_number = admdetails;
                    console.log(data);
                    if (admdetails.length > 0) {
                        $http.post(ENV.apiUrl + "api/common/Admission/ApproveMultiple", data).then(function (res) {
                            $scope.promote = res.data;
                            console.log($scope.promote);
                            $scope.GetInfo($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.btn_visible);
                            $scope.viewdashDetailsModal($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.btn_visible, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll);
                        });




                    }
                }

            }

            //Reject the Admissions
            $scope.getReject = function () {
                admdetails = [];
                for (var i = 0; i < $scope.dash_data.length; i++) {
                    var t = $scope.dash_data[i].admission_number;
                    var v = document.getElementById(t);

                    if (v.checked == true) {
                        admdetails = admdetails + $scope.dash_data[i].admission_number + ',';
                    }
                }
                if (admdetails.length == 0) {
                    swal({ title: "Alert", text: "Please select atleast 1 applicant", showCloseButton: true, width: 380, });
                    //$('#message').modal({ backdrop: 'static', keyboard: true });
                }
                else {
                    swal({
                        title: '',
                        text: "The selected applications will be rejected",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.OkRejectadm();
                        }
                    });
                }
            }

            $scope.OkRejectadm = function () {
                var data = $scope.edt;
                data.user_code = $rootScope.globals.currentUser.username;
                data.admission_number = admdetails;
                data.status = 'R';
                data.opr = 'J';

                $http.post(ENV.apiUrl + "api/common/Admission/AdmissionStatusOpr", data).then(function (res) {
                    $scope.display = true;
                    $scope.msg1 = res.data;
                    console.log($scope.msg1);

                    if ($scope.msg1 > 0) {
                        $scope.viewdashDetailsModal($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.btn_visible, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll);
                        $scope.GetInfo($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.btn_visible);
                    }
                });
            }

            //getSelected
            $scope.getSelected = function () {
                $scope.btn_selected = true;
                admdetails = [];
                for (var i = 0; i < $scope.dash_data.length; i++) {
                    var t = $scope.dash_data[i].admission_number;
                    var v = document.getElementById(t);

                    if (v.checked == true) {
                        admdetails = admdetails + $scope.dash_data[i].admission_number + ',';
                    }
                }
                if (admdetails.length == 0) {
                    swal({ title: "Alert", text: "Please select atleast 1 applicant", showCloseButton: true, width: 380, });
                }
                else {
                    swal({
                        title: '',
                        text: "The applicants will be Selected",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.OkselectedAdm();
                        }
                    });
                }
            }

            $scope.OkselectedAdm = function () {
                var data = $scope.edt;
                data.user_code = $rootScope.globals.currentUser.username;
                data.admission_number = admdetails;
                data.status = 'S';

                console.log(data);

                $http.post(ENV.apiUrl + "api/common/Admission/AdmissionStatusOpr", data).then(function (res) {
                    $scope.display = true;
                    $scope.msg1 = res.data;
                    console.log($scope.msg1);

                    if ($scope.msg1 > 0) {
                        $scope.viewdashDetailsModal($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.btn_visible, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll);
                        $scope.GetInfo($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.btn_visible);
                    }
                });
            }

            $scope.makeTodos1 = function () {
                var rem = parseInt($scope.totalItems1 % $scope.numPerPage1);
                if (rem == '0') {
                    $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1);
                }
                else {
                    $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1) + 1;
                }
                var begin1 = (($scope.currentPage1 - 1) * $scope.numPerPage1);
                var end1 = parseInt(begin1) + parseInt($scope.numPerPage1);

                $scope.filteredTodos1 = $scope.todos1.slice(begin1, end1);
            };

            $scope.size1 = function (str) {
                console.log(str);
                $scope.pagesize1 = str;
                $scope.currentPage1 = 1;
                $scope.numPerPage1 = str;
                console.log("numPerPage=" + $scope.numPerPage1);
                $scope.makeTodos1();
            }

            $scope.index1 = function (str) {
                $scope.pageindex1 = str;
                $scope.currentPage1 = str;
                console.log("currentPage1=" + $scope.currentPage1);
                $scope.makeTodos1();
            }

            $scope.searched1 = function (valLists, toSearch) {

                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil1(i, toSearch);
                });
            };

            $scope.search1 = function () {
                $scope.todos1 = $scope.searched1($scope.view, $scope.searchText1);
                $scope.totalItems1 = $scope.todos1.length;
                $scope.currentPage1 = '1';
                if ($scope.searchText1 == '') {
                    $scope.todos1 = $scope.view;
                }
                $scope.makeTodos1();
            }

            function searchUtil1(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.admission_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.stud_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            //Upload_document
            $scope.UploadDocument = function (adm_no) {
                //  $scope.admission_no = adm_no;
                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetCriteriaName?admission_num=" + adm_no).then(function (res) {
                    $scope.Upload_doc_datails = res.data;
                    $scope.totalItems3 = $scope.Upload_doc_datails.length;
                    $scope.todos3 = $scope.Upload_doc_datails;
                    for (var i = 0; i < $scope.totalItems3; i++) {
                        $scope.Upload_doc_datails[i] = res.data[i];
                        //  $scope.todos = res.data[i];
                        $scope.visible = [];
                        $scope.visible = res.data[i].sims_admission_doc_path.split(",");
                        $scope.Upload_doc_datails[i].path = $scope.visible;
                    }
                    $scope.makeTodos3();
                    console.log($scope.Upload_doc_datails);
                    $scope.Upload_doc = true;
                    $scope.maingrid1 = false;
                    $scope.edt.sims_admission_doc_path1 = "NO";
                    // $('#UploadDocModal').modal({ backdrop: 'static', keyboard: true });
                });

            }

            $scope.makeTodos3 = function () {
                var rem = parseInt($scope.totalItems3 % $scope.numPerPage3);
                if (rem == '0') {
                    $scope.pagersize3 = parseInt($scope.totalItems3 / $scope.numPerPage3);
                }
                else {
                    $scope.pagersize3 = parseInt($scope.totalItems3 / $scope.numPerPage3) + 1;
                }
                var begin3 = (($scope.currentPage3 - 1) * $scope.numPerPage3);
                var end3 = parseInt(begin3) + parseInt($scope.numPerPage3);
                $scope.filteredTodos3 = $scope.todos3.slice(begin3, end3);
            };

            $scope.size3 = function (str) {
                console.log(str);
                $scope.pagesize3 = str;
                $scope.currentPage3 = 1;
                $scope.numPerPage3 = str;
                console.log("numPerPage3=" + $scope.numPerPage3);
                $scope.makeTodos3();
            }

            $scope.index3 = function (str) {
                $scope.pageindex3 = str;
                $scope.currentPage3 = str;
                console.log("currentPage3=" + $scope.currentPage3);
                $scope.makeTodos3();
            }

            $scope.searched3 = function (valLists, toSearch) {

                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil3(i, toSearch);
                });
            };

            $scope.search3 = function () {
                $scope.todos3 = $scope.searched3($scope.Upload_doc_datails, $scope.searchText3);
                $scope.totalItems3 = $scope.todos3.length;
                $scope.currentPage3 = '1';
                if ($scope.searchText3 == '') {
                    $scope.todos3 = $scope.Upload_doc_datails;
                }
                $scope.makeTodos3();
            }

            function searchUtil3(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_criteria_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_criteria_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            //screeningMarks
            $scope.screeningMarks = function (adm_no) {
                //$scope.admission_no = adm_no;

                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetcriteriaMarks?admission_num=" + adm_no).then(function (res) {
                    $scope.screen_datails = res.data;
                    $scope.totalItems2 = $scope.screen_datails.length;
                    $scope.todos2 = $scope.screen_datails;
                    $scope.makeTodos2();
                    $scope.Screening = true;
                    $scope.maingrid1 = false;
                    $scope.t = true;
                    //$('#screeningModal').modal({ backdrop: 'static', keyboard: true });
                });
            }

            $scope.makeTodos2 = function () {
                var rem = parseInt($scope.totalItems2 % $scope.numPerPage2);
                if (rem == '0') {
                    $scope.pagersize2 = parseInt($scope.totalItems2 / $scope.numPerPage2);
                }
                else {
                    $scope.pagersize2 = parseInt($scope.totalItems2 / $scope.numPerPage2) + 1;
                }
                var begin2 = (($scope.currentPage2 - 1) * $scope.numPerPage2);
                var end2 = parseInt(begin2) + parseInt($scope.numPerPage2);

                $scope.filteredTodos2 = $scope.todos2.slice(begin2, end2);
            };

            $scope.size2 = function (str) {
                console.log(str);
                $scope.pagesize2 = str;
                $scope.currentPage2 = 1;
                $scope.numPerPage2 = str; console.log("numPerPage2=" + $scope.numPerPage2); $scope.makeTodos2();
            }

            $scope.index2 = function (str) {
                $scope.pageindex2 = str;
                $scope.currentPage2 = str; console.log("currentPage2=" + $scope.currentPage2); $scope.makeTodos2();
            }

            $scope.searched2 = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil2(i, toSearch);
                });
            };

            $scope.search2 = function () {
                $scope.todos2 = $scope.searched2($scope.screen_datails, $scope.searchText2);
                $scope.totalItems2 = $scope.todos2.length;
                $scope.currentPage2 = '1';
                if ($scope.searchText2 == '') {
                    $scope.todos2 = $scope.screen_datails;
                }
                $scope.makeTodos2();
            }

            function searchUtil2(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_admission_criteria_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_criteria_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            //Update_screening_details
            $scope.screeningModal_Cancel = function () {
                var data = $scope.screen_datails;
                $http.post(ENV.apiUrl + "api/common/AdmissionDashboard/CUD_UpdateAdmissionMarks", data).then(function (res) {
                    $scope.display = true;
                    $scope.msg1 = res.data;
                    console.log($scope.msg1);
                    if ($scope.msg1.messageType > 0) {
                        swal({ title: "Admission Marks", text: "'" + $scope.msg1.strMessage + "'", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.Screening = false;
                                $scope.maingrid1 = true;
                                // $('#screeningModal').modal('hide');
                                $scope.viewdashDetailsModal($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.btn_visible, $scope.gender, $scope.btn_nationality, $scope.btn_emp, $scope.btn_siblingenroll);
                                // $scope.viewdashDetailsModal($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.edt.sims_appl_parameter_reg, null, null, null, null);
                                // $scope.GetInfo($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.edt.sims_appl_parameter_reg);
                            }
                        });

                    }
                });
            }

            $scope.screeningModal_Reset = function () {
                $scope.Screening = false;
                $scope.maingrid1 = true;
            }

            //HTML5 editor

            $scope.getCommunicate = function () {
                console.log('ppp');
                admdetails = [];
                var adm_data = [];

                debugger;
                for (var i = 0; i < $scope.dash_data.length; i++) {
                    // var v = document.getElementById($scope.dash_data[i].admission_number);
                    // if ($scope.dash_data[i].admission_number1 == true)

                    var t = $scope.dash_data[i].admission_number;
                    var v = document.getElementById(t);

                    if (v.checked == true) {
                        adm_data.push($scope.dash_data[i].admission_number);
                        admdetails = admdetails + $scope.dash_data[i].admission_number + ',';
                    }
                }


                //if (adm_data.length <= 1)
                // {
                console.log(admdetails);
                $scope.div_Communication = true;
                $scope.maingrid1 = false;
                // $('#commnModal').modal({ backdrop: 'static', keyboard: true });

                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetAdmissionTemplates").then(function (res) {
                    $scope.template_data = res.data;
                });

                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetAdmission_EmailIds?admission_nos=" + admdetails).then(function (res) {
                    $scope.emailId_data = res.data;
                });
                // }
                //else
                //{
                //    swal({ title: "Alert", text: "Should Not be communicate with Multiple Admission No.", showCloseButton: true, width: 380, });
                //    admdetails = [];
                //    for (var i = 0; i < $scope.dash_data.length; i++) {
                //        $scope.dash_data[i].admission_number1 = false;
                //    }
                //    $('tr').removeClass("row_selected");
                //}
            }

            $('#text-editor').wysihtml5();

            $scope.getbody = function (msg_type) {
                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetAdmissionTemplatesBody?template_subject=" + msg_type).then(function (res) {
                    $scope.tempBody_data = res.data;
                    var body = $scope.tempBody_data.sims_msg_body + ' ' + $scope.tempBody_data.sims_msg_signature;
                    var v = document.getElementById('text-editor');
                    v.value = body;
                    $scope.email.msgbody = v.value;
                    $scope.flag = false;
                    console.log($scope.tempBody_data.sims_msg_sr_no);
                    $('#text-editor').data("wysihtml5").editor.setValue($scope.email.msgbody);

                    $http.get(ENV.apiUrl + "api/common/Email/GetcheckEmailProfile?sr_no=" + $scope.tempBody_data.sims_msg_sr_no).then(function (res) {
                        $scope.emailProfile_data = res.data;
                        console.log($scope.emailProfile_data);
                    });
                });
            }

            $scope.sendMail = function () {

                $scope.email_exists = false;
                var data1 = [];
                var lst_cc = [];
                var define_admission = '';
                if ($scope.email.sims_msg_subject == undefined)
                {
                    swal({ title: "Alert", text: "Please Select Template to Sent Mail.", showCloseButton: true, width: 380, });
                    return;
                }
                else
                {
                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetAdmissionTemplatesBody?template_subject=" + $scope.email.sims_msg_subject).then(function (res) {
                        $scope.TemplatesBody_data = res.data;
                        debugger
                        $scope.sims_msg_body = $scope.TemplatesBody_data.sims_msg_body;
                        $scope.sims_msg_signature = $scope.TemplatesBody_data.sims_msg_signature;

                        var msgbody = $('#text-editor').val();
                        // console.log(msgbody);
                        
                        define_admission = admdetails;
                        var adm_number = admdetails;

                            for (var i = 0; i < $scope.emailId_data.length; i++)
                            {
                                var adm_no = adm_number.indexOf(',')
                                var admission_num = adm_number.substr(0, adm_no);
                                adm_number = adm_number.substr(adm_no + 1);

                                if ($scope.emailId_data[i].chk_email == true)
                                {
                                    //var adm_number = admdetails;
                                    //var adm_no = adm_number.indexOf(',')
                                    //var admission_num = adm_number.substr(0, adm_no);

                                    var data =
                                       ({
                                           emailsendto: $scope.emailId_data[i].emailid,
                                           body: msgbody,
                                           subject: $scope.email.sims_msg_subject,
                                           comm_desc: msgbody,
                                           admis_num: admission_num,
                                           comm_method: 'E',
                                           comm_date: $scope.ddMMyyyy,
                                           sender_emailid: $scope.emailProfile_data
                                       });

                                    data1.push(data);

                                    $scope.email_exists = true;
                                    //console.log(data1);
                                }
                            }

                            if (($scope.email.ccTo == "" || $scope.email.ccTo == undefined) && ($scope.email_exists == false))
                            {
                                // $scope.email_exists = false;
                                swal({ title: "Alert", text: "Unable to Sent Mail", showCloseButton: true, width: 380, });
                            }
                            

                            if ($scope.email.ccTo != "" || $scope.email.ccTo != undefined) {
                                var v = [];
                                var s = $scope.email.ccTo;
                                if (s != null) {
                                    v = s.split(',');
                                }

                                var adm_number = admdetails;
                                var adm_no = adm_number.indexOf(',')
                                var admission_num = adm_number.substr(0, adm_no);

                                for (var i = 0; i < v.length; i++) {
                                    var data =
                                      ({
                                          emailsendto: v[i],
                                          body: msgbody,
                                          subject: $scope.email.sims_msg_subject,
                                          comm_desc: msgbody,
                                          admis_num: admission_num,
                                          comm_method: 'E',
                                          comm_date: $scope.ddMMyyyy,
                                          sender_emailid: $scope.emailProfile_data
                                      });

                                    data1.push(data);
                                }

                                var data2 =
                                    {
                                        attFilename: ''
                                    }
                                lst_cc.push(data2);

                            }

                            // console.log(data1);

                            $http.post(ENV.apiUrl + "api/common/AdmissionDashboard/CUDCommunication", data1).then(function (res) {
                                $scope.Admcomm_data = res.data;

                                if ($scope.Admcomm_data == true) {

                                    $http.post(ENV.apiUrl + "api/common/Email/ScheduleMails?filenames=" + JSON.stringify(lst_cc), data1).then(function (res) {
                                        $scope.ScheduleMails_data = res.data;

                                        if ($scope.ScheduleMails_data == true) {
                                            swal({ text: "The email has been sent successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                                        }
                                        else {
                                            $scope.commnModaldisplay();
                                        }

                                        $scope.commnModaldisplay();
                                    });
                                }
                            });
                        
                    });
                    
                }
            }

            //$scope.sendMail = function () {
            //    $scope.email_exists = false;
            //    var data1 = [];
            //    var lst_cc = [];

            //    if ($scope.email.sims_msg_subject != "" || $scope.email.sims_msg_subject != "Template") {
            //        $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetAdmissionTemplatesBody?template_subject=" + $scope.email.sims_msg_subject).then(function (res) {
            //            $scope.TemplatesBody_data = res.data;

            //            $scope.sims_msg_body = $scope.TemplatesBody_data.sims_msg_body;
            //            $scope.sims_msg_signature = $scope.TemplatesBody_data.sims_msg_signature;

            //            var msgbody = $('#text-editor').val();
            //            // console.log(msgbody);

            //            for (var i = 0; i < $scope.emailId_data.length; i++) {
            //                if ($scope.emailId_data[i].chk_email == true) {
            //                    var adm_number = admdetails;
            //                    var adm_no = adm_number.indexOf(',')
            //                    var admission_num = adm_number.substr(0, adm_no);

            //                    var data =
            //                       ({
            //                           emailsendto: $scope.emailId_data[i].emailid,
            //                           body: msgbody,
            //                           subject: $scope.email.sims_msg_subject,
            //                           comm_desc: msgbody,
            //                           admis_num: admission_num,
            //                           comm_method: 'E',
            //                           comm_date: $scope.ddMMyyyy,
            //                           sender_emailid: $scope.emailProfile_data
            //                       });

            //                    data1.push(data);

            //                    $scope.email_exists = true;
            //                    //console.log(data1);
            //                }
            //            }

            //            if ($scope.email_exists == false) {
            //                swal({ title: "Alert", text: "Unable to Send Mail", showCloseButton: true, width: 380, });
            //            }

            //            if ($scope.email.ccTo != "" || $scope.email.ccTo != undefined) {
            //                var v = [];
            //                var s = $scope.email.ccTo;
            //                if (s != null) {
            //                    v = s.split(',');
            //                }

            //                var adm_number = admdetails;
            //                var adm_no = adm_number.indexOf(',')
            //                var admission_num = adm_number.substr(0, adm_no);

            //                for (var i = 0; i < v.length; i++) {
            //                    var data =
            //                      ({
            //                          emailsendto: v[i],
            //                          body: msgbody,
            //                          subject: $scope.email.sims_msg_subject,
            //                          comm_desc: msgbody,
            //                          admis_num: admission_num,
            //                          comm_method: 'E',
            //                          comm_date: $scope.ddMMyyyy,
            //                          sender_emailid: $scope.emailProfile_data
            //                      });

            //                    data1.push(data);
            //                }

            //                var data2 =
            //                    {
            //                        attFilename: ''
            //                    }
            //                lst_cc.push(data2);

            //            }

            //            // console.log(data1);

            //            $http.post(ENV.apiUrl + "api/common/AdmissionDashboard/CUDCommunication", data1).then(function (res) {
            //                $scope.Admcomm_data = res.data;
            //                if ($scope.Admcomm_data == true) {

            //                    $http.post(ENV.apiUrl + "api/common/Email/ScheduleMails?filenames=" + JSON.stringify(lst_cc), data1).then(function (res) {
            //                        $scope.ScheduleMails_data = res.data;
            //                        if ($scope.ScheduleMails_data == true) {
            //                            swal({ title: "Alert", text: "Mail Send Successfully", showCloseButton: true, width: 380, });
            //                        }
            //                        else {
            //                            $scope.commnModaldisplay();
            //                        }

            //                        $scope.commnModaldisplay();
            //                    });
            //                }
            //            });
            //        });
            //    }
            //    else {
            //        swal({ title: "Alert", text: "Please Select Template to Send Mail.", showCloseButton: true, width: 380, });
            //    }
            //}

            $scope.commnModaldisplay = function () {
                $scope.email = [];
                $scope.emailId_data = [];
                $scope.maingrid1 = true;
                // $('#commnModal').modal('hide');
                $('#text-editor').data("wysihtml5").editor.clear();
                $scope.div_Communication = false;
            }

            $scope.CancelEmailId = function (indx) {
                console.log(indx);
                $scope.emailId_data.splice(indx, 1);
                console.log($scope.emailId_data.splice(indx, 1));
            }



            /*start Upload*/

            var formdata = new FormData();

            $scope.getTheFiles = function ($files) {
                $scope.filesize = true;
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                    var i = 0;
                    if ($files[i].size > 200000) {
                        $scope.filesize = false;
                        $scope.edt.photoStatus = false;
                        swal({  text: "File Should Not Exceed 200Kb.", imageUrl: "assets/img/notification-alert.png" });
                    }
                });

            };

            $scope.uploadClick = function (str) {
                $scope.filesize = true;
                $scope.edt1 = str;
                console.log($scope.edt1);
                $scope.edt.photoStatus = true;
                $scope.ins = false;
                formdata = new FormData();
            }

            $scope.file_changed = function (element, str) {
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;
                    });
                };

                reader.readAsDataURL(photofile);

                if (element.files[0].size < 200000) {
                    console.log($scope.edt1.count);
                    if ($scope.edt1.count < 2) {
                        console.log($scope.edt1.sims_admission_doc_path);
                        if ($scope.edt1.sims_admission_doc_path == null || $scope.edt1.sims_admission_doc_path == "") {
                            $scope.edt1.count = $scope.edt1.count;
                        }
                        else {
                            $scope.edt1.count = ($scope.edt1.count) + 1;
                        }

                        // if ($scope.edt1.count < 2)
                        {
                            var request = {
                                method: 'POST',
                                url: ENV.apiUrl + 'api/fileNew/uploadDocument?filename=' + $scope.edt1.admis_num + '_' + $scope.edt1.sims_criteria_code + '_' + $scope.edt1.count + '(' + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss') + ')' + "&location=" + "Docs/Student",
                                data: formdata,
                                headers: {
                                    'Content-Type': undefined
                                }
                            };
                            $http(request).success(function (d) {
                                var data = {
                                    admis_num: $scope.edt1.admis_num,
                                    sims_criteria_code: $scope.edt1.sims_criteria_code,
                                    sims_admission_doc_path: d,
                                    sims_admission_doc_path_old: $scope.edt1.sims_admission_doc_path
                                }

                                if (data.sims_admission_doc_path != null || data.sims_admission_doc_path != "") {
                                    if ($scope.edt1.sims_admission_doc_path == null || $scope.edt1.sims_admission_doc_path == "") {
                                        console.log($scope.edt1.count);
                                        data.sims_admission_doc_status = 'false';
                                        data.sims_admission_doc_verify = 'false';
                                        data.opr = 'N';

                                        console.log(data);

                                        $http.post(ENV.apiUrl + "api/common/AdmissionDashboard/CUD_Update_Admission_Doc", data).then(function (res) {
                                            $scope.ins = res.data;
                                            if (res.data) {
                                                swal({ text: "Document Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                                                $scope.edt1.count = ($scope.edt1.count) + 1;
                                            }

                                            $scope.UploadDocument($scope.edt1.admis_num);
                                        });
                                    }
                                    else {
                                        console.log($scope.edt1.count);
                                        $http.post(ENV.apiUrl + "api/common/AdmissionDashboard/CUD_Insert_Admission_Doc", data).then(function (res) {
                                            $scope.ins = res.data;
                                            if (res.data) {
                                                swal({ text: "Document Uploaded Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                                                $scope.edt1.count = ($scope.edt1.count) + 1;
                                            }

                                            $scope.UploadDocument($scope.edt1.admis_num);
                                        });
                                    }
                                }

                            });
                        }
                    }
                    else {
                        swal({ title: "Alert", text: "You Can Upload Max 2 File Documents For Each Criteria", showCloseButton: true, width: 380, });

                    }

                }
            };

            $scope.add_upload_doc = function () {
                var data = $scope.Upload_doc_datails;
                $http.post(ENV.apiUrl + "api/common/AdmissionDashboard/CUD_Update_Admission_DocList", data).then(function (res) {
                    $scope.ins = res.data;
                    if (res.data) {
                    }
                    $scope.maingrid1 = true;
                    $scope.Upload_doc = false;
                    //$scope.GetInfo($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.edt.sims_appl_parameter_reg);
                    $scope.viewdashDetailsModal($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.edt.sims_appl_parameter_reg, null, null, null, null);
                });
            }

            $scope.UploadDocModal_Reset = function () {
                $scope.maingrid1 = true;
                $scope.Upload_doc = false;
            }

            $scope.doc_delete = function (doc_path, crit_code, adm_no) {
                swal({
                    title: '',
                    text: "Are you sure you want to Delete?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        $http.post(ENV.apiUrl + "api/common/AdmissionDashboard/CUD_Delete_Admission_Doc?adm_no=" + adm_no + "&criteria_code=" + crit_code + "&doc_path=" + doc_path).then(function (res) {
                            $scope.del = res.data;
                            if (res.data) {
                                swal({ text: "Document Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                            }
                            $scope.UploadDocument(adm_no);
                        });
                    }
                });


            }

            /*End Upload*/

            /*start History Communication*/

            $scope.getcommunicationhistory = function (adm_No) {
                $scope.AdmissionNo = adm_No;
                $scope.div_CommunicationHist = true;
                $scope.maingrid1 = false;
                // $('#commhistModal').modal({ backdrop: 'static', keyboard: true });

                $http.get(ENV.apiUrl + "api/common/ProspectDashboard/GetCommMethods").then(function (res) {
                    $scope.method_data = res.data;
                });

                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/getCommunication?adm_no=" + $scope.AdmissionNo).then(function (res) {
                    $scope.comm_data = res.data;
                    $scope.totalItems4 = $scope.comm_data.length;
                    $scope.todos4 = $scope.comm_data;
                    $scope.makeTodos4();
                    $scope.grid1 = true;
                    $scope.display1 = false;
                });

            }

            $scope.makeTodos4 = function () {
                var rem = parseInt($scope.totalItems4 % $scope.numPerPage4);
                if (rem == '0') {
                    $scope.pagersize4 = parseInt($scope.totalItems4 / $scope.numPerPage4);
                }
                else {
                    $scope.pagersize4 = parseInt($scope.totalItems4 / $scope.numPerPage4) + 1;
                }
                var begin4 = (($scope.currentPage4 - 1) * $scope.numPerPage4);
                var end4 = parseInt(begin4) + parseInt($scope.numPerPage4);

                $scope.filteredTodos4 = $scope.todos4.slice(begin4, end4);
            };

            $scope.size4 = function (str) {
                console.log(str);
                $scope.pagesize1 = str;
                $scope.currentPage4 = 1;
                $scope.numPerPage4 = str;
                console.log("numPerPage=" + $scope.numPerPage4);
                $scope.makeTodos4();
            }

            $scope.index4 = function (str) {
                $scope.pageindex4 = str;
                $scope.currentPage4 = str;
                console.log("currentPage4=" + $scope.currentPage4);
                $scope.makeTodos4();
            }

            $scope.New = function () {
                $scope.display1 = true;
                $scope.grid1 = false;
                $scope.temp = "";
            }

            $scope.showPaid = function () {
                var ob = {
                    curr_code: $scope.edt.curr_code,
                    AcadmicYear: $scope.edt.academic_year,
                    gradeCode: $scope.edt.grade_code,
                    admission_status: 'W',
                    gender_code: '',
                    nationality_code: '',
                    employee_code: '',
                    sibling_enroll: ''
                    // prospect_fee_status:'Paid'

                }
                if (ob.admission_status == 'W') {
                    $scope.btn_prospect = false;
                    $scope.btn_reject = false;
                }
                if (ob.admission_status == 'R') {
                    $scope.btn_prospect = false;
                    $scope.btn_reject = true;
                }
                if (ob.admission_status == 'C') {
                    $scope.btn_prospect = true;
                    $scope.btn_reject = false;
                }
                if (ob.admission_status == '') {
                    $scope.btn_prospect = true;
                    $scope.btn_reject = true;
                }
                if (ob.admission_status == '1') {
                    ob.admission_status = 'W';
                }
                if (ob.admission_status == '2') {
                    ob.admission_status = 'R';
                }


                $http.post(ENV.apiUrl + "api/common/AdmissionDashboard/waitingPaidStudents", ob).then(function (res) {
                    $scope.dash_data = res.data;
                    $scope.grid = true;
                    $scope.maingrid1 = true;
                    if ($scope.dash_data.length > 0) {
                        $('#viewdashDetailsModal').modal({ backdrop: 'static', keyboard: true });
                    }
                    else {
                        swal({ title: "Alert", text: "No Record Found", showCloseButton: true, width: 380, });
                    }
                });
            }

            $scope.showUnPaid = function ()
            {
                debugger;
                var ob = {
                    curr_code: $scope.edt.curr_code,
                    AcadmicYear: $scope.edt.academic_year,
                    gradeCode: $scope.edt.grade_code,
                    admission_status: 'W',
                    gender_code: '',
                    nationality_code: '',
                    employee_code: '',
                    sibling_enroll: ''
                    //prospect_fee_status: 'Unpaid'
                }
                if (ob.admission_status == 'W') {
                    $scope.btn_prospect = false;
                    $scope.btn_reject = false;
                }
                if (ob.admission_status == 'R') {
                    $scope.btn_prospect = false;
                    $scope.btn_reject = true;
                }
                if (ob.admission_status == 'C') {
                    $scope.btn_prospect = true;
                    $scope.btn_reject = false;
                }
                if (ob.admission_status == '') {
                    $scope.btn_prospect = true;
                    $scope.btn_reject = true;
                }
                if (ob.admission_status == '1') {
                    ob.admission_status = 'W';
                }
                if (ob.admission_status == '2') {
                    ob.admission_status = 'R';
                }


                $http.post(ENV.apiUrl + "api/common/AdmissionDashboard/waitingUnPaidStudents", ob).then(function (res) {
                    $scope.dash_data = res.data;
                    $scope.grid = true;
                    $scope.maingrid1 = true;
                    if ($scope.dash_data.length > 0) {
                        $('#viewdashDetailsModal').modal({ backdrop: 'static', keyboard: true });
                    }
                    else {
                        swal({ title: "Alert", text: "No Record Found", showCloseButton: true, width: 380, });
                    }
                });
            }

            $scope.CommHist_Cancel = function () {
                $scope.div_CommunicationHist = false;
                $scope.maingrid1 = true;
            }

            $scope.commcancel_btn = function () {
                $scope.grid1 = true;
                $scope.display1 = false;
                $scope.myForm1.$setPristine();
                $scope.myForm1.$setUntouched();
            }

            $scope.Save = function (isvalidate) {

                var commdata1 = [];
                var commdata = [];

                if (isvalidate) {
                    var commdata = ({
                        admis_num: $scope.AdmissionNo,
                        comm_method: $scope.temp.comm_method,
                        comm_date: $scope.temp.comm_date,
                        comm_desc: $scope.temp.comm_desc,
                        enq_rem: $scope.temp.enq_rem,
                        status: $scope.temp.status,
                        opr: 'I'
                    });

                    commdata1.push(commdata);

                    $http.post(ENV.apiUrl + "api/common/AdmissionDashboard/CUDCommunication", commdata1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Admission Communication Added Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getcommunicationhistory($scope.AdmissionNo);
                                }
                            });
                        }
                        else {
                            swal({ text: "Admission Communication Not Added Successfully", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getcommunicationhistory($scope.AdmissionNo);
                                }
                            });
                        }
                    });
                    $scope.myForm1.$setPristine();
                    $scope.myForm1.$setUntouched();
                }
            }

            $scope.searched4 = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil4(i, toSearch);
                });
            };

            $scope.search4 = function () {
                $scope.todos4 = $scope.searched4($scope.comm_data, $scope.searchText4);
                $scope.totalItems4 = $scope.todos4.length;
                $scope.currentPage4 = '1';
                if ($scope.searchText4 == '') {
                    $scope.todos4 = $scope.comm_data;
                }
                $scope.makeTodos4();
            }

            function searchUtil4(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.comm_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.comm_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            /* End History Communication*/

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
        }])

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])
})();