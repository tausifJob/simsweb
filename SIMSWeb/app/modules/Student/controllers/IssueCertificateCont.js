﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var user_type = 'S'
    var issueno;
    
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('IssueCertificateCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            debugger;
            var username = $rootScope.globals.currentUser.username;
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //Select Data SHOW
            $scope.BindGrid = function (str) {
                debugger;
                user_type=str;
                $scope.getgrid(str,username);
            }
            
           
            $http.get(ENV.apiUrl + "api/IssueCertificate/getIssueCertificate?opr1=" + user_type + "&username=" + username).then(function (res1) {
                    $scope.IssueCertificateData = res1.data;
                    $scope.totalItems = $scope.IssueCertificateData.length;
                    $scope.todos = $scope.IssueCertificateData;
                    $scope.makeTodos();

                });
            
            $scope.getgrid = function (opr1, user) {
                debugger;
                
                $http.get(ENV.apiUrl + "api/IssueCertificate/getIssueCertificate?opr1=" + opr1 + "&username=" + user).then(function (res1) {

                    $scope.IssueCertificateData = res1.data;
                    $scope.totalItems = $scope.IssueCertificateData.length;
                    $scope.todos = $scope.IssueCertificateData;
                    $scope.makeTodos();

                });

            }
            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.IssueCertificateData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                //$scope.CheckAllChecked();
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Get  data AllRequestNos
            $scope.getAllRequestNosData = function (str) {
                $http.get(ENV.apiUrl + "api/IssueCertificate/getAllRequestCertificateNo").then(function (res1) {

                    $scope.AllRequestCertificateNo = res1.data;
                });
            }
            //Bind Combo

            $http.get(ENV.apiUrl + "api/IssueCertificate/getUserType").then(function (res1) {

                $scope.UserTypeData = res1.data;
            });



          
            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.IssueCertificateData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.IssueCertificateData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_certificate_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_certificate_request_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_certificate_issue_person_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_certificate_issue_number == toSearch) ? true : false;
            }


            $scope.myFunct = function (keyEvent) {
                debugger;
                if (keyEvent.which == 13)
                    $scope.temp.date = dd + '-' + mm + '-' + yyyy;
            }

            //$scope.myFunction(event) {
            //    if(event.keyCode == 13) {   // '13' is the key code for enter
            //        // do what you want to do when 'enter' is pressed :)
            //    }
            //}

            //NEW BUTTON
            $scope.New = function () {

                $scope.disabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = [];
                $scope.temp.sup_code = "";
                $scope.temp.sup_name = "";
                $scope.sdt_type = "";
                $scope.sdt_desc = "";
                $scope.temp.sd_desc = "";
                $scope.sd_desc = "";
                $scope.temp.date = dd + '-' + mm + '-' + yyyy;
                $scope.temp.status = true;
                $scope.user_typeReadonly = false;
                $scope.sims_certificate_request_numberReadonly = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = [];
                $scope.table = true;
                $scope.display = false;
                $scope.sdt_type = "";
                $scope.sdt_desc = "";
                $scope.temp.sdt_type = "";
                $scope.temp.sdt_desc = "";
                $scope.temp.sup_code = "";
                $scope.temp.sup_name = "";
                $scope.sdt_type = "";
                $scope.sdt_desc = "";
                $scope.temp.sd_desc = "";
                $scope.sd_desc = "";
                $scope.user_typeReadonly = false;
                $scope.sims_certificate_request_numberReadonly = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.user_typeReadonly = true;
                $scope.sims_certificate_request_numberReadonly = true;
                $scope.temp = {                    
                    sims_certificate_request_number: str.sims_certificate_request_number,
                    sims_certificate_name_display:str.sims_certificate_name_display,
                    sims_certificate_name: str.sims_certificate_name,
                    sims_certificate_number: str.sims_certificate_number,
                    sims_certificate_issue_person_number: str.sims_certificate_issue_person_number,
                    date: str.sims_certificate_issue_date,
                    status: str.sims_certificate_print_flag,
                    sims_certificate_print_count: str.sims_certificate_print_count,
                    sims_certificate_issue_number: str.sims_certificate_issue_number,
                };
                $scope.temp.user_type_code = user_type;
                issueno = str.sims_certificate_request_number;

            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger;
                if (Myform) {
                    datasend = [];
                    data = [];
                    // var data = $scope.temp;
                    user_type = $scope.temp.user_type_code;
                    if ($scope.temp.status == true) {
                        $scope.temp.sims_certificate_print_flag = true;
                    }
                    else {
                        $scope.temp.sims_certificate_print_flag = false;
                    }
                    var data = {
                        
                        opr1: $scope.temp.user_type_code,
                        sims_certificate_issue_number: $scope.temp.sims_certificate_issue_number,
                        sims_certificate_issue_date: $scope.temp.date,
                        sims_certificate_number: $scope.temp.sims_certificate_number,
                        sims_certificate_issue_person_number: $scope.temp.sims_certificate_issue_person_number,
                        sims_certificate_request_number: $scope.temp.sims_certificate_request_number,
                        sims_certificate_print_count: $scope.temp.sims_certificate_print_count,
                        sims_certificate_doc_path: $scope.temp.sims_certificate_doc_path,
                        sims_certificate_print_flag: $scope.temp.sims_certificate_print_flag,
                    }
                    data.opr = 'I';
                    datasend.push(data);

                    $http.post(ENV.apiUrl + "api/IssueCertificate/CUDIssueCertificate", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 380 });

                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 380 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $scope.BindGrid(user_type);
                    });

                    $scope.table = true;
                    $scope.display = false;
                }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                debugger;
                if (Myform) {
                    dataforUpdate = [];
                    if ($scope.temp.status == true) {
                        $scope.temp.sims_certificate_print_flag = true;
                    }
                    else {
                        $scope.temp.sims_certificate_print_flag = false;
                    }
                    user_type = $scope.temp.user_type_code;
                    var data = {
                        opr1: $scope.temp.user_type_code,
                        sims_certificate_issue_number: $scope.temp.sims_certificate_issue_number,
                        sims_certificate_issue_date: $scope.temp.date,
                        sims_certificate_number: $scope.temp.sims_certificate_number,
                        sims_certificate_issue_person_number: $scope.temp.sims_certificate_issue_person_number,
                        sims_certificate_request_number: $scope.temp.sims_certificate_request_number,
                        sims_certificate_print_count: $scope.temp.sims_certificate_print_count,
                        sims_certificate_doc_path: $scope.temp.sims_certificate_doc_path,
                        sims_certificate_print_flag: $scope.temp.sims_certificate_print_flag,
                    }
                    data.opr = "U";
                    dataforUpdate.push(data);
                    //dataupdate.push(data);
                    $http.post(ENV.apiUrl + "api/IssueCertificate/CUDIssueCertificate", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 380 });
                            $scope.BindGrid(user_type);
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 380 });
                            $scope.BindGrid(user_type);
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        
                    });
                   dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_certificate_issue_number);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_certificate_issue_number);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.bindtext = function (str) {
                debugger;
                for (var i = 0; i < $scope.IssueNoData.length; i++) {
                    var issueno = $scope.IssueNoData[i].sims_certificate_request_number;
                    if (issueno == str) {
                       
                        $scope.temp.sims_certificate_issue_person_number= $scope.IssueNoData[i].sims_certificate_issue_person_number,
                        $scope.temp.sims_certificate_name=$scope.IssueNoData[i].sims_certificate_name,
                        $scope.temp.sims_certificate_number=$scope.IssueNoData[i].sims_certificate_number
                        
                    }
                }
            }

            $scope.getIssueNo = function (str) {
                debugger;
                $http.get(ENV.apiUrl + "api/IssueCertificate/getIssueNo?user_name="+ str).then(function (res1) {

                    $scope.IssueNoData = res1.data;
                    

                });
            }

           
            $scope.Delete = function (str) {
                debugger;
                var deletecode = [];
                $scope.flag = false;
                if (str == undefined) {
                    user_type = 'S';
                }
                else{
                    user_type = str;
                }
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    //var t = $scope.filteredTodos[i].uom_code;
                    var v = document.getElementById(i + $scope.filteredTodos[i].sims_certificate_issue_number);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sims_certificate_issue_number': $scope.filteredTodos[i].sims_certificate_issue_number,
                            'opr1': user_type,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {

                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/IssueCertificate/CUDIssueCertificate", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.BindGrid(user_type);
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;

                                        }
                                    });
                                }
                                else {
                                    swal({ text: "Record Already Mapped.Can't be Deleted. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.BindGrid(user_type);
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i + $scope.filteredTodos[i].sims_certificate_issue_number);
                                //var t = $scope.filteredTodos[i].sims_fee_category;
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");

                                }


                            }

                        }

                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
              
                $scope.currentPage = str;
            }



            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.print = function (str) {
                debugger
                if (str.sims_certificate_number = '001') {
                    $scope.report_name = 'Certificate.CERR22SISConductReport';
                }
                console.log(str);
                var data = {
                    location: $scope.report_name,
                    parameter: {
                        roll_no: str.sims_certificate_issue_person_number,
                        //  acad:'2017'
                    },
                    state: 'main.Sim105',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')//Certificate.CERR27ADIS
            }
         }])

})();
