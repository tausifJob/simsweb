﻿

(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('SchoolLeaversReportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.temp = {};
            $scope.hideprintpdf = false;
            $scope.display_on_print_excel = false;

            $scope.pagesize = '50';
            $scope.pageindex = "0";
            $scope.pager = true;
            //$scope.colsvis = false;
            $scope.temp = {};
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.SchoolLogo = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/SchoolLogo/';

            $(function () {
                $('#bell_box').multipleSelect({ width: '100%' });
                $('#teacher_box').multipleSelect({ width: '100%', filter: true });

            });

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
            $scope.temp.from_date = dd + '-' + mm + '-' + yyyy;
            $scope.temp.to_date = dd + '-' + mm + '-' + yyyy;

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_start_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });


            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_end_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {

                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['chk_all'] = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 30, $scope.maxSize = 100;

            $scope.makeTodos = function () {
                debugger;
                if ($scope.pagesize == 'All') {
                    $scope.currentPage = 1;
                    $scope.showPager = false;
                    $scope.filteredTodos = $scope.report_data;
                }
                else {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                }
            };


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res1) {
                $scope.cur_data = res1.data;
                $scope.temp.sims_cur_code = $scope.cur_data[0].sims_cur_code;
                $scope.getAcademic_year($scope.cur_data[0].sims_cur_code);
            });
            //supriya
            $scope.getAcademic_year = function (cur_code1) {
                debugger;
                $scope.temp.sims_academic_year = [];
                $http.get(ENV.apiUrl + "api/SchoolLeaver/getAcademicYear?curCode=" + cur_code1).then(function (res1) {
                    $scope.acad_data = res1.data;
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                    setTimeout(function () {
                        $('#cmb_acade_year').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%',
                            filter:true
                        });
                    }, 1000);

                    for (var i = 0; i < $scope.acad_data.length; i++) {
                        if ($scope.acad_data[i].sims_academic_year_status == 'C') {
                            $scope.temp.sims_academic_year.push($scope.acad_data[i].sims_academic_year);
                        }
                    }

                    setTimeout(function () {
                        $("#cmb_acade_year").multipleSelect("setSelects", $scope.temp.sims_academic_year);
                    }, 3000);

                        console.log("sims_academic_year",$scope.temp.sims_academic_year)

                    });
                
                }
               

            $scope.getGrade = function (cur_code, acad_year) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%',
                            filter: true
                        });
                    }, 1000);

                });
            }


            $scope.getSection = function (cur_code, academic_year, grade_code) {
                debugger;
                $http.get(ENV.apiUrl + "api/SchoolLeaver/getSectionFromGrade?cur_code=" + $scope.temp.sims_cur_code + "&grade_code=" + $scope.temp.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%',
                            filter: true
                        });
                        $("#cmb_section_code").multipleSelect("checkAll");
                    }, 1000);
                });
            }

         



            $scope.getdetail = function () {
                $scope.section = $('#cmb_section_code').multipleSelect('getSelects', 'text');
                $scope.grade = $('#cmb_grade_code').multipleSelect('getSelects', 'text');
                $scope.acad = $('#cmb_acade_year').multipleSelect('getSelects', 'text');
                $scope.cur = $scope.cur_data[0].sims_cur_short_name_en;
              
                               debugger;
                $scope.colsvis = false;
                $http.get(ENV.apiUrl + "api/SchoolLeaver/getSchoolLeaversdetails?cur_code=" + $scope.temp.sims_cur_code + "&acad_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&from_date=" + $scope.mom_start_date + "&to_date=" + $scope.mom_end_date).then(function (res1) {
                       if (res1.data.length > 0) {
                           $scope.total_length = angular.copy(res1.data.length);
                            $scope.report_data = res1.data;
                            $scope.totalItems = $scope.report_data.length;
                            $scope.todos = $scope.report_data;
                            $scope.makeTodos();
                            $scope.pager = true;

                        }
                        else {
                            swal({ title: "Alert", text: "Data Not Found", showCloseButton: true, width: 300, height: 200 });
                            $scope.report_data = [];
                            $scope.filteredTodos = [];
                            $scope.section = '';
                            $scope.grade = '';
                            $scope.acad = '';
                            $scope.cur = '';
                            $scope.pager = false;

                        }

                        $http.get(ENV.apiUrl + "api/SchoolLeaver/getdashboarddetails?cur_code=" + $scope.temp.sims_cur_code + "&acad_year=" + $scope.temp.sims_academic_year).then(function (res2) {
                            if (res1.data.length > 0) {
                                $scope.dash_data = res2.data;

                            }
                        });

                    });
            }


            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };
            $scope.search = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $scope.colsvis = false;


            $scope.exportData = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('rpt_data1').innerHTML],
                         {
                             type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                         });
                        $scope.detailsField = false;
                        saveAs(blob, "SchoolLeversReport.xls");
                        $scope.colsvis = false;
                        $scope.getdetail();

                    }
                    $scope.getdetail();
                });
                
            };

            $scope.print = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var docHead = document.head.outerHTML;
                        var printContents = document.getElementById('rpt_data1').outerHTML;
                        var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                        var newWin = window.open("", "_blank", winAttr);
                        var writeDoc = newWin.document;
                        writeDoc.open();
                        writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                        writeDoc.close();
                        newWin.focus();
                        $scope.getdetail();

                    }
                    $scope.getdetail();
                });

            };



            $scope.reset_data = function () {
                debugger;
                $scope.temp.sims_grade_code = '';
                $scope.temp.sims_academic_year = '';


                try {
                    $('#cmb_acade_year').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_grade_code').multipleSelect('uncheckAll');
                } catch (e) {

                }

                

                try {
                    $('#cmb_section_code').multipleSelect('uncheckAll');
                } catch (e) {

                }
                $scope.temp.sims_section_code = '';
                $scope.filteredTodos = [];
                $scope.dash_data = [];
                $scope.section = '';
                $scope.grade = '';
                $scope.acad = '';
                $scope.cur = '';
                $scope.total_length = '';
                $scope.pager = false;
            }

            $scope.ischeck = function () {
                debugger
                $scope.checklick = function () {
                    var v = document.getElementById('expire');
                    if (v.checked == true) {
                        v.checked == false;
                    }
                }
            }


        }])

})();

