﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, modulecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AdmissionReportSVCCCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.showdisabled = true;

            $timeout(function () {
                $("#example").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.exportData = function () {
                var check = true;
                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            alasql('SELECT * INTO XLSX("Report.xlsx",{headers:true}) \ FROM HTML("#example",{headers:true,skipdisplaynone:true})');
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Report Not Save", showCloseButton: true, width: 380, })
                }
            };
            $scope.status = function () {
                debugger;
                $http.post(ENV.apiUrl + "api/StudentReport/AdmissionStatus").then(function (Statuscode) {
                    $scope.Admissionstatus = Statuscode.data;
                    setTimeout(function () {
                        $('#cmb_admission_status').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }


            $scope.status();

            $(function () {
                $('#cmb_admission_status').multipleSelect({
                    width: '100%'
                });
            });
            $scope.busyindicator = false;
            $scope.ShowCheckBoxes = true;

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear($scope.edt.sims_cur_code);
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                    $scope.getGrade(curCode, $scope.temp.sims_academic_year);
                });
            }

            $scope.GetGrade = function () {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }



            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $(function () {
                $('#cmb_grade_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.Show_Data = function (str, str1, str2) {
                debugger;
                var res;
                if (str != undefined && str1 != undefined && str2.length != 0 & $scope.edt.sims_grade_code.length != 0) {
                    if ($scope.chk_stud_name == true || $scope.chk_fami_name == true || $scope.chk_nick_name == true || $scope.chk_stud_gend == true || $scope.chk_stud_adno == true || $scope.chk_stud_addt == true || $scope.chk_stud_aday == true || $scope.chk_stud_tjdt == true || $scope.chk_stud_codt == true || $scope.chk_stud_agrc == true || $scope.chk_stud_grna == true || $scope.chk_stud_scco == true || $scope.chk_stud_csht == true || $scope.chk_stud_cusc == true || $scope.chk_stud_cusg == true || $scope.chk_stud_csen == true || $scope.chk_stud_cusl == true || $scope.chk_stud_cuad == true || $scope.chk_stud_cuci == true || $scope.chk_stud_cuct == true || $scope.chk_stud_cuph == true || $scope.chk_stud_cufa == true || $scope.chk_stud_cufd == true || $scope.chk_stud_cutd == true || $scope.chk_stud_cude == true || $scope.chk_stud_feca == true || $scope.chk_stud_feps == true || $scope.chk_stud_usco == true || $scope.chk_stud_adst == true || $scope.chk_stud_orsk == true || $scope.chk_stud_sosk == true || $scope.chk_stud_sped == true || $scope.chk_stud_cout == true || $scope.chk_stud_reli == true || $scope.chk_stud_nati == true || $scope.chk_stud_naid == true || $scope.chk_stud_nadt == true || $scope.chk_stud_naed == true || $scope.chk_stud_vino == true || $scope.chk_stud_vity == true || $scope.chk_stud_viip == true || $scope.chk_stud_viau == true || $scope.chk_stud_viid == true || $scope.chk_stud_viex == true || $scope.chk_stud_pano == true || $scope.chk_stud_paia == true || $scope.chk_stud_paip == true || $scope.chk_stud_stid == true || $scope.chk_stud_sted == true || $scope.chk_stud_stcp == true || $scope.chk_stud_paid == true || $scope.chk_stud_mkco == true || $scope.chk_stud_mkde == true || $scope.chk_stud_leco == true || $scope.chk_stud_dobr == true || $scope.chk_stud_sina == true || $scope.chk_stud_sisu == true || $scope.chk_stud_sien == true || $scope.chk_stud_sido == true || $scope.chk_fath_fana == true || $scope.chk_fath_fano == true || $scope.chk_fath_faad == true || $scope.chk_fath_fani == true || $scope.chk_fath_fanf == true || $scope.chk_fath_fans == true || $scope.chk_fath_fand == true || $scope.chk_fath_fapn == true || $scope.chk_fath_famn == true || $scope.chk_fath_faem == true || $scope.chk_fath_fafx == true || $scope.chk_fath_faps == true || $scope.chk_fath_faed == true || $scope.chk_fath_faqu == true || $scope.chk_fath_faca == true || $scope.chk_fath_facn == true || $scope.chk_fath_faon == true || $scope.chk_moth_mona == true || $scope.chk_moth_mono == true || $scope.chk_moth_moad == true || $scope.chk_moth_moni == true || $scope.chk_moth_monf == true || $scope.chk_moth_mons == true || $scope.chk_moth_mond == true || $scope.chk_moth_mopn == true || $scope.chk_moth_momn == true || $scope.chk_moth_moem == true || $scope.chk_moth_mofx == true || $scope.chk_moth_mops == true || $scope.chk_moth_moed == true || $scope.chk_moth_moqu == true || $scope.chk_moth_moca == true || $scope.chk_moth_mocn == true || $scope.chk_moth_moon == true || $scope.chk_guar_guna == true || $scope.chk_guar_guno == true || $scope.chk_guar_guad == true || $scope.chk_guar_guni == true || $scope.chk_guar_gunf == true || $scope.chk_guar_guns == true || $scope.chk_guar_gund == true || $scope.chk_guar_gupn == true || $scope.chk_guar_gumn == true || $scope.chk_guar_guem == true || $scope.chk_guar_gufx == true || $scope.chk_guar_gups == true || $scope.chk_guar_gued == true || $scope.chk_guar_guqu == true || $scope.chk_guar_guca == true || $scope.chk_guar_gucn == true || $scope.chk_guar_guon == true) {
                        $http.get(ENV.apiUrl + "api/StudentReport/getAllAdmissionStudentReport?cur_name=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grd_code=" + $scope.edt.sims_grade_code + "&from=" + $scope.edt.rd_from_required + "&to=" + $scope.edt.rd_up_required + "&admission_status=" + str2).then(function (res) {
                            $scope.studlist = res.data;
                            //$scope.table = true;
                            console.log("Stud list is ", $scope.studlist);
                            if ($scope.studlist.length == 0) {
                                swal({ title: 'Alert', text: "Record Not Found.", showCloseButton: true, width: 450, height: 200 });
                                $scope.ShowCheckBoxes = true;
                            }

                            else {
                                $scope.table = true;
                                $scope.busyindicator = false;
                                $scope.ShowCheckBoxes = false;
                            }
                        })
                    }
                    else {
                        swal({ title: 'Alert', text: "Please Select Atleast One Checkbox Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                    }
                }
                else {
                    swal({ title: 'Alert', text: "Please Select mandatory fields.", showCloseButton: true, width: 450, height: 200 });
                }
            }

            $scope.back = function () {
                debugger;
                $scope.table = false;
                $scope.busyindicator = false;
                $scope.ShowCheckBoxes = true;
            }

            $scope.checkallClick = function (str) {
                if (str) {
                    $scope.showdisabled = false;

                    $scope.chk_stud_name = true;
                    $scope.chk_fami_name = true;
                    $scope.chk_nick_name = true;
                    $scope.chk_stud_gend = true;
                    $scope.chk_stud_adno = true;
                    $scope.chk_stud_addt = true;
                    $scope.chk_stud_aday = true;
                    $scope.chk_stud_tjdt = true;
                    $scope.chk_stud_codt = true;
                    $scope.chk_stud_agrc = true;
                    $scope.chk_stud_grna = true;
                    $scope.chk_stud_scco = true;
                    $scope.chk_stud_csht = true;
                    $scope.chk_stud_cusc = true;
                    $scope.chk_stud_cusg = true;
                    $scope.chk_stud_csen = true;
                    $scope.chk_stud_cusl = true;
                    $scope.chk_stud_cuad = true;
                    $scope.chk_stud_cuci = true;
                    $scope.chk_stud_cuct = true;
                    $scope.chk_stud_cuph = true;
                    $scope.chk_stud_cufa = true;
                    $scope.chk_stud_cufd = true;
                    $scope.chk_stud_cutd = true;
                    $scope.chk_stud_cude = true;
                    $scope.chk_stud_feca = true;
                    $scope.chk_stud_feps = true;
                    $scope.chk_stud_usco = true;
                    $scope.chk_stud_adst = true;
                    $scope.chk_stud_orsk = true;
                    $scope.chk_stud_sosk = true;
                    $scope.chk_stud_sped = true;
                    $scope.chk_stud_cout = true;
                    $scope.chk_stud_reli = true;
                    $scope.chk_stud_nati = true;
                    $scope.chk_stud_naid = true;
                    $scope.chk_stud_nadt = true;
                    $scope.chk_stud_naed = true;
                    $scope.chk_stud_vino = true;
                    $scope.chk_stud_vity = true;
                    $scope.chk_stud_viip = true;
                    $scope.chk_stud_viau = true;
                    $scope.chk_stud_viid = true;
                    $scope.chk_stud_viex = true;
                    $scope.chk_stud_pano = true;
                    $scope.chk_stud_paia = true;
                    $scope.chk_stud_paip = true;
                    $scope.chk_stud_stid = true;
                    $scope.chk_stud_sted = true;
                    $scope.chk_stud_stcp = true;
                    $scope.chk_stud_paid = true;
                    $scope.chk_stud_mkco = true;
                    $scope.chk_stud_mkde = true;
                    $scope.chk_stud_leco = true;
                    $scope.chk_stud_dobr = true;
                    $scope.chk_stud_sina = true;
                    $scope.chk_stud_sisu = true;
                    $scope.chk_stud_sien = true;
                    $scope.chk_stud_sido = true;

                    $scope.chk_fath_fana = true;
                    $scope.chk_fath_fano = true;
                    $scope.chk_fath_faad = true;
                    $scope.chk_fath_fani = true;
                    $scope.chk_fath_fanf = true;
                    $scope.chk_fath_fans = true;
                    $scope.chk_fath_fand = true;
                    $scope.chk_fath_fapn = true;
                    $scope.chk_fath_famn = true;
                    $scope.chk_fath_faem = true;
                    $scope.chk_fath_fafx = true;
                    $scope.chk_fath_faps = true;
                    $scope.chk_fath_faed = true;
                    $scope.chk_fath_faqu = true;
                    $scope.chk_fath_faca = true;
                    $scope.chk_fath_facn = true;
                    $scope.chk_fath_faon = true;
                    $scope.chk_moth_mona = true;
                    $scope.chk_moth_mono = true;
                    $scope.chk_moth_moad = true;
                    $scope.chk_moth_moni = true;
                    $scope.chk_moth_monf = true;
                    $scope.chk_moth_mons = true;
                    $scope.chk_moth_mond = true;
                    $scope.chk_moth_mopn = true;
                    $scope.chk_moth_momn = true;
                    $scope.chk_moth_moem = true;
                    $scope.chk_moth_mofx = true;
                    $scope.chk_moth_mops = true;
                    $scope.chk_moth_moed = true;
                    $scope.chk_moth_moqu = true;
                    $scope.chk_moth_moca = true;
                    $scope.chk_moth_mocn = true;
                    $scope.chk_moth_moon = true;
                    $scope.chk_guar_guna = true;
                    $scope.chk_guar_guno = true;
                    $scope.chk_guar_guad = true;
                    $scope.chk_guar_guni = true;
                    $scope.chk_guar_gunf = true;
                    $scope.chk_guar_guns = true;
                    $scope.chk_guar_gund = true;
                    $scope.chk_guar_gupn = true;
                    $scope.chk_guar_gumn = true;
                    $scope.chk_guar_guem = true;
                    $scope.chk_guar_gufx = true;
                    $scope.chk_guar_gups = true;
                    $scope.chk_guar_gued = true;
                    $scope.chk_guar_guqu = true;
                    $scope.chk_guar_guca = true;
                    $scope.chk_guar_gucn = true;
                    $scope.chk_guar_guon = true;
                    $scope.chk_hsc_institution_name = true;
                    $scope.chk_hsc_institution_place = true;
                    $scope.chk_board_name = true;
                    $scope.chk_year_pasing = true;
                    $scope.chk_seat_no = true;
                    $scope.chk_no_of_attempts = true;
                    $scope.chk_hsc_marks = true;
                    $scope.chk_marks_out = true;
                    $scope.chk_hsc_percentage = true;
                    $scope.chk_fy_institution_name = true;
                    $scope.chk_fy_institution_place = true;
                    $scope.chk_fy_board_name = true;
                    $scope.chk_fy_year_pasing = true;
                    $scope.chk_fy_seat_no = true;
                    $scope.chk_fy_no_of_attempts_I = true;
                    $scope.chk_fy_no_of_obtained_I = true;
                    $scope.chk_fy_marks_out_I = true;
                    $scope.chk_fy_percentage_I = true;
                    $scope.chk_fy_no_of_attempts_II = true;
                    $scope.chk_fy_no_of_obtained_II = true;
                    $scope.chk_fy_marks_out_II = true;
                    $scope.chk_fy_percentage_II = true;
                    $scope.chk_sy_institution_name = true;
                    $scope.chk_sy_institution_place = true;
                    $scope.chk_sy_board_name = true;
                    $scope.chk_sy_year_pasing = true;
                    $scope.chk_sy_seat_no = true;
                    $scope.chk_sy_no_of_attempts_III = true;
                    $scope.chk_sy_no_of_obtained_III = true;
                    $scope.chk_sy_marks_out_III = true;
                    $scope.chk_sy_percentage_III = true;
                    $scope.chk_sy_no_of_attempts_IV = true;
                    $scope.chk_sy_no_of_obtained_IV = true;
                    $scope.chk_sy_marks_out_IV = true;
                    $scope.chk_sy_percentage_IV = true;
                    $scope.chk_Email = true;
                    $scope.chk_Mobile = true;
                    $scope.chk_physical = true;
                    $scope.chk_marital_status = true;
                    $scope.chk_Caste = true;
                    $scope.chk_blood_group = true;
                    $scope.chk_fighter = true;
                    $scope.chk_place_of_birth = true;
                    try {
                        $scope.chk_fy_percentage_math = true;
                        $scope.chk_fy_percentage_science = true;
                        $scope.chk_ssc_percentage_math = true;
                        $scope.chk_ssc_percentage_science = true;
                    } catch (e) {

                    }
                }
                else {
                    $scope.Uncheck();
                }
            }

            $scope.Uncheck = function () {
                $scope.showdisabled = true;

                $scope.chk_stud_name = false;
                $scope.chk_fami_name = false;
                $scope.chk_nick_name = false;
                $scope.chk_stud_gend = false;
                $scope.chk_stud_adno = false;
                $scope.chk_stud_addt = false;
                $scope.chk_stud_aday = false;
                $scope.chk_stud_tjdt = false;
                $scope.chk_stud_codt = false;
                $scope.chk_stud_agrc = false;
                $scope.chk_stud_grna = false;
                $scope.chk_stud_scco = false;
                $scope.chk_stud_csht = false;
                $scope.chk_stud_cusc = false;
                $scope.chk_stud_cusg = false;
                $scope.chk_stud_csen = false;
                $scope.chk_stud_cusl = false;
                $scope.chk_stud_cuad = false;
                $scope.chk_stud_cuci = false;
                $scope.chk_stud_cuct = false;
                $scope.chk_stud_cuph = false;
                $scope.chk_stud_cufa = false;
                $scope.chk_stud_cufd = false;
                $scope.chk_stud_cutd = false;
                $scope.chk_stud_cude = false;
                $scope.chk_stud_feca = false;
                $scope.chk_stud_feps = false;
                $scope.chk_stud_usco = false;
                $scope.chk_stud_adst = false;
                $scope.chk_stud_orsk = false;
                $scope.chk_stud_sosk = false;
                $scope.chk_stud_sped = false;
                $scope.chk_stud_cout = false;
                $scope.chk_stud_reli = false;
                $scope.chk_stud_nati = false;
                $scope.chk_stud_naid = false;
                $scope.chk_stud_nadt = false;
                $scope.chk_stud_naed = false;
                $scope.chk_stud_vino = false;
                $scope.chk_stud_vity = false;
                $scope.chk_stud_viip = false;
                $scope.chk_stud_viau = false;
                $scope.chk_stud_viid = false;
                $scope.chk_stud_viex = false;
                $scope.chk_stud_pano = false;
                $scope.chk_stud_paia = false;
                $scope.chk_stud_paip = false;
                $scope.chk_stud_stid = false;
                $scope.chk_stud_sted = false;
                $scope.chk_stud_stcp = false;
                $scope.chk_stud_paid = false;
                $scope.chk_stud_mkco = false;
                $scope.chk_stud_mkde = false;
                $scope.chk_stud_leco = false;
                $scope.chk_stud_dobr = false;
                $scope.chk_stud_sina = false;
                $scope.chk_stud_sisu = false;
                $scope.chk_stud_sien = false;
                $scope.chk_stud_sido = false;

                $scope.chk_fath_fana = false;
                $scope.chk_fath_fano = false;
                $scope.chk_fath_faad = false;
                $scope.chk_fath_fani = false;
                $scope.chk_fath_fanf = false;
                $scope.chk_fath_fans = false;
                $scope.chk_fath_fand = false;
                $scope.chk_fath_fapn = false;
                $scope.chk_fath_famn = false;
                $scope.chk_fath_faem = false;
                $scope.chk_fath_fafx = false;
                $scope.chk_fath_faps = false;
                $scope.chk_fath_faed = false;
                $scope.chk_fath_faqu = false;
                $scope.chk_fath_faca = false;
                $scope.chk_fath_facn = false;
                $scope.chk_fath_faon = false;
                $scope.chk_moth_mona = false;
                $scope.chk_moth_mono = false;
                $scope.chk_moth_moad = false;
                $scope.chk_moth_moni = false;
                $scope.chk_moth_monf = false;
                $scope.chk_moth_mons = false;
                $scope.chk_moth_mond = false;
                $scope.chk_moth_mopn = false;
                $scope.chk_moth_momn = false;
                $scope.chk_moth_moem = false;
                $scope.chk_moth_mofx = false;
                $scope.chk_moth_mops = false;
                $scope.chk_moth_moed = false;
                $scope.chk_moth_moqu = false;
                $scope.chk_moth_moca = false;
                $scope.chk_moth_mocn = false;
                $scope.chk_moth_moon = false;
                $scope.chk_guar_guna = false;
                $scope.chk_guar_guno = false;
                $scope.chk_guar_guad = false;
                $scope.chk_guar_guni = false;
                $scope.chk_guar_gunf = false;
                $scope.chk_guar_guns = false;
                $scope.chk_guar_gund = false;
                $scope.chk_guar_gupn = false;
                $scope.chk_guar_gumn = false;
                $scope.chk_guar_guem = false;
                $scope.chk_guar_gufx = false;
                $scope.chk_guar_gups = false;
                $scope.chk_guar_gued = false;
                $scope.chk_guar_guqu = false;
                $scope.chk_guar_guca = false;
                $scope.chk_guar_gucn = false;
                $scope.chk_guar_guon = false;
                $scope.chk_hsc_institution_name = false;
                $scope.chk_hsc_institution_place = false;
                $scope.chk_board_name = false;
                $scope.chk_year_pasing = false;
                $scope.chk_seat_no = false;
                $scope.chk_no_of_attempts = false;
                $scope.chk_hsc_marks = false;
                $scope.chk_marks_out = false;
                $scope.chk_hsc_percentage = false;
                $scope.chk_fy_institution_name = false;
                $scope.chk_fy_institution_place = false;
                $scope.chk_fy_board_name = false;
                $scope.chk_fy_year_pasing = false;
                $scope.chk_fy_seat_no = false;
                $scope.chk_fy_no_of_attempts_I = false;
                $scope.chk_fy_no_of_obtained_I = false;
                $scope.chk_fy_marks_out_I = false;
                $scope.chk_fy_percentage_I = false;
                $scope.chk_fy_no_of_attempts_II = false;
                $scope.chk_fy_no_of_obtained_II = false;
                $scope.chk_fy_marks_out_II = false;
                $scope.chk_fy_percentage_II = false;
                $scope.chk_sy_institution_name = false;
                $scope.chk_sy_institution_place = false;
                $scope.chk_sy_board_name = false;
                $scope.chk_sy_year_pasing = false;
                $scope.chk_sy_seat_no = false;
                $scope.chk_sy_no_of_attempts_III = false;
                $scope.chk_sy_no_of_obtained_III = false;
                $scope.chk_sy_marks_out_III = false;
                $scope.chk_sy_percentage_III = false;
                $scope.chk_sy_no_of_attempts_IV = false;
                $scope.chk_sy_no_of_obtained_IV = false;
                $scope.chk_sy_marks_out_IV = false;
                $scope.chk_sy_percentage_IV = false;
                $scope.chk_Email = false;
                $scope.chk_Mobile = false;
                $scope.chk_physical = false;
                $scope.chk_marital_status = false;
                $scope.chk_Caste = false;
                $scope.chk_blood_group = false;
                $scope.chk_fighter = false;
                $scope.chk_place_of_birth = false;
                try {
                    $scope.chk_fy_percentage_math = false;
                    $scope.chk_fy_percentage_science = false;
                    $scope.chk_ssc_percentage_math = false;
                    $scope.chk_ssc_percentage_science = false;
                } catch (e) {

                }
            }

            $scope.Reset = function () {
                $scope.Uncheck();
                $scope.select_all = false;
                $scope.active_stud = false;
                $scope.inactive_stud = false;
                $scope.edt = '';
                $scope.temp = {
                    sims_academic_year: '',
                }
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                    $scope.curriculum = AllCurr.data;
                    $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                    $scope.getAccYear($scope.edt.sims_cur_code);
                });
            }

            $scope.checkevent = function () {
                if ($scope.chk_stud_name == true || $scope.chk_fami_name == true || $scope.chk_nick_name == true || $scope.chk_stud_gend == true || $scope.chk_stud_adno == true || $scope.chk_stud_addt == true || $scope.chk_stud_aday == true || $scope.chk_stud_tjdt == true || $scope.chk_stud_codt == true || $scope.chk_stud_agrc == true || $scope.chk_stud_grna == true || $scope.chk_stud_scco == true || $scope.chk_stud_csht == true || $scope.chk_stud_cusc == true || $scope.chk_stud_cusg == true || $scope.chk_stud_csen == true || $scope.chk_stud_cusl == true || $scope.chk_stud_cuad == true || $scope.chk_stud_cuci == true || $scope.chk_stud_cuct == true || $scope.chk_stud_cuph == true || $scope.chk_stud_cufa == true || $scope.chk_stud_cufd == true || $scope.chk_stud_cutd == true || $scope.chk_stud_cude == true || $scope.chk_stud_feca == true || $scope.chk_stud_feps == true || $scope.chk_stud_usco == true || $scope.chk_stud_adst == true || $scope.chk_stud_orsk == true || $scope.chk_stud_sosk == true || $scope.chk_stud_sped == true || $scope.chk_stud_cout == true || $scope.chk_stud_reli == true || $scope.chk_stud_nati == true || $scope.chk_stud_naid == true || $scope.chk_stud_nadt == true || $scope.chk_stud_naed == true || $scope.chk_stud_vino == true || $scope.chk_stud_vity == true || $scope.chk_stud_viip == true || $scope.chk_stud_viau == true || $scope.chk_stud_viid == true || $scope.chk_stud_viex == true || $scope.chk_stud_pano == true || $scope.chk_stud_paia == true || $scope.chk_stud_paip == true || $scope.chk_stud_stid == true || $scope.chk_stud_sted == true || $scope.chk_stud_stcp == true || $scope.chk_stud_paid == true || $scope.chk_stud_mkco == true || $scope.chk_stud_mkde == true || $scope.chk_stud_leco == true || $scope.chk_stud_dobr == true || $scope.chk_stud_sina == true || $scope.chk_stud_sisu == true || $scope.chk_stud_sien == true || $scope.chk_stud_sido == true || $scope.chk_fath_fana == true || $scope.chk_fath_fano == true || $scope.chk_fath_faad == true || $scope.chk_fath_fani == true || $scope.chk_fath_fanf == true || $scope.chk_fath_fans == true || $scope.chk_fath_fand == true || $scope.chk_fath_fapn == true || $scope.chk_fath_famn == true || $scope.chk_fath_faem == true || $scope.chk_fath_fafx == true || $scope.chk_fath_faps == true || $scope.chk_fath_faed == true || $scope.chk_fath_faqu == true || $scope.chk_fath_faca == true || $scope.chk_fath_facn == true || $scope.chk_fath_faon == true || $scope.chk_moth_mona == true || $scope.chk_moth_mono == true || $scope.chk_moth_moad == true || $scope.chk_moth_moni == true || $scope.chk_moth_monf == true || $scope.chk_moth_mons == true || $scope.chk_moth_mond == true || $scope.chk_moth_mopn == true || $scope.chk_moth_momn == true || $scope.chk_moth_moem == true || $scope.chk_moth_mofx == true || $scope.chk_moth_mops == true || $scope.chk_moth_moed == true || $scope.chk_moth_moqu == true || $scope.chk_moth_moca == true || $scope.chk_moth_mocn == true || $scope.chk_moth_moon == true || $scope.chk_guar_guna == true || $scope.chk_guar_guno == true || $scope.chk_guar_guad == true || $scope.chk_guar_guni == true || $scope.chk_guar_gunf == true || $scope.chk_guar_guns == true || $scope.chk_guar_gund == true || $scope.chk_guar_gupn == true || $scope.chk_guar_gumn == true || $scope.chk_guar_guem == true || $scope.chk_guar_gufx == true || $scope.chk_guar_gups == true || $scope.chk_guar_gued == true || $scope.chk_guar_guqu == true || $scope.chk_guar_guca == true || $scope.chk_guar_gucn == true || $scope.chk_guar_guon == true) {
                    $scope.showdisabled = false;
                }
                else {
                    $scope.showdisabled = true;
                }
            }

        }])
})();