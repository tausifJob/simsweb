﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ProspectDashboardReportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.grid = true;
            var data1 = [];
            var edt = [];
            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.parameter_data = [];
            $scope.edit_code = false;

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            $scope.search_data= function () {
                debugger;
                $http.get(ENV.apiUrl + "api/ProspectDashboardReportController/getProspecDashboardReport?sims_pros_number="+''+"&from_date="+''+"&to_date=").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.parameter_data = res.data;
                    $scope.totalItems = $scope.parameter_data.length;
                    $scope.todos = $scope.parameter_data;
                    $scope.makeTodos();
                    for (var i = 0; i < $scope.totalItems; i++) {
                        $scope.parameter_data[i].icon = "fa fa-plus-circle";
                    }
                });
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

         


          
         




         

            


            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }


            
          
      

            var dom;
            $scope.flag = true;
            $scope.icon = "fa fa-plus-circle";

            

            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.parameter_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.parameter_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.parameter_data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_mod_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_appl_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  || item.sims_appl_form_field.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

        }])
})();