﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var sims_level_code;
    var section_code;
    var simsController = angular.module('sims.module.ReportCard');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ReportCardLevelCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.sectiontxtReadonly = true;
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            //Select Data SHOW

           

            //Bind Combo
          
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCurriculum) {
                $scope.Curriculum = getCurriculum.data;
                
                $scope.temp['sims_cur_code'] = $scope.Curriculum[0].sims_cur_code;
                $scope.getacademicYear($scope.temp['sims_cur_code']);
                console.log($scope.Curriculum);
            });
            $http.get(ENV.apiUrl + "api/ReportCardLevel/getReportCardLevel").then(function (res1) {

                $scope.ReportcardLevelData = res1.data;
                $scope.totalItems = $scope.ReportcardLevelData.length;
                $scope.todos = $scope.ReportcardLevelData;
                $scope.makeTodos();

            });

            $scope.getacademicYear = function (str) {
                $scope.curriculum_code = str;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (getAcademicYear) {
                    $scope.AcademicYear = getAcademicYear.data;
                    $scope.temp['sims_academic_year'] = $scope.AcademicYear[0].sims_academic_year;
                    $scope.getGrade($scope.curriculum_code, $scope.temp['sims_academic_year'])
                    console.log($scope.AcademicYear);
                })
            }


            $scope.getGrade = function (str1, str2) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str1 + "&academic_year=" + str2).then(function (getAllGrades) {
                    $scope.AllGrades = getAllGrades.data;
                    // $scope.temp['sims_grade_code'] = $scope.AllGrades[0].sims_grade_code;
                    console.log($scope.AllGrades);
                })
            }

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.ReportcardLevelData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                //$scope.CheckAllChecked();
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.ReportcardLevelData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.ReportcardLevelData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_cur_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_section_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_report_card_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_fee_amount == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
                debugger;
                $scope.disabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = [];
                $scope.CurriculumReadonly = false;
                $scope.AcademicYearReadonly = false;
               
                $scope.GradeReadonly = false;
                $scope.sectiontxt = false;
                $scope.sectionrbt = true;
                $scope.temp = {
                    status: true,

                }

                if ($scope.Curriculum.length > 0) {
                    $scope.temp.sims_cur_code = $scope.Curriculum[0].sims_cur_code;
                    $scope.getacademicYear($scope.temp.sims_cur_code);
                }

                $scope.AcademicYear = getAcademicYear.data;
                $scope.temp['sims_academic_year'] = $scope.AcademicYear[0].sims_academic_year;
                $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                
                $scope.temp.sims_section_code = "";
              
               
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = [];
                $scope.table = true;
                $scope.display = false;
                $scope.edt = [];
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.sectionrbt = false;
                $scope.sectiontxt = true;
                $scope.Update_btn = true;
                $scope.readonlyAdjustableFeeType = true;
                $scope.CurriculumReadonly = true;
                $scope.AcademicYearReadonly = true;
                $scope.GradeReadonly = true;
               // $scope.sectionReadonly = true;
                //if (str.sims_level_status == "A") {
                //    $scope.sims_level_status = true;
                //}
                //else {
                //    $scope.sims_level_status = false;
                //}
                $scope.temp = {
                    status: str.sims_level_status,
                   
                    sims_cur_code: str.sims_cur_code,
                    sims_academic_year: str.sims_academic_year,
                    sims_grade_code: str.sims_grade_code,
                    sims_section_code1: str.sims_section_name,
                    name: str.sims_report_card_name,

                    };
                section_code=str.sims_section_code
                sims_level_code = str.sims_level_code;
              
              
                $scope.getacademicYear(str.sims_cur_code);
                $scope.getGrade(str.sims_cur_code, str.sims_academic_year)
                $scope.getsection(str.sims_cur_code,str.sims_grade_code, str.sims_academic_year)
            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger;
               
                if (Myform) {
                    if ($scope.temp.sims_section_code.length == 0 || $scope.temp == undefined) {
                        swal({ title: "Alert", text: "Please Select Section", width: 380 });
                    }
                    else {
                        var sectioncode1;
                        var sectioncode = [];
                        if ($scope.temp.sims_section_code.length == 0 || $scope.temp == undefined) {
                            sectioncode = '';
                        }
                        else {
                            sectioncode1 = $scope.temp.sims_section_code;
                            sectioncode = sectioncode + sectioncode1 + ',';
                        }
                        datasend = [];
                        data = [];

                        var data = {
                            sims_section_code: sectioncode,
                            sims_cur_code: $scope.temp.sims_cur_code,
                            sims_academic_year: $scope.temp.sims_academic_year,
                            sims_grade_code: $scope.temp.sims_grade_code,
                            sims_report_card_name: $scope.temp.name,
                            sims_level_status: $scope.temp.status,
                        }
                        data.opr = 'I';
                        datasend.push(data);

                        $http.post(ENV.apiUrl + "api/ReportCardLevel/InsertUpdateDeleteReportCardLevel", datasend).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 380 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 380 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                            $scope.getgrid();
                        });
                        $scope.table = true;
                        $scope.display = false;
                    }
                }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                debugger;
                $scope.flag1 = false;
                if (Myform) {
                    var sectioncode1;
                    var sectioncode = [];
                   
                   
                    dataforUpdate = [];
                   
                    var data = {
                        sims_section_code:section_code,
                        sims_cur_code: $scope.temp.sims_cur_code,
                        sims_academic_year: $scope.temp.sims_academic_year,
                        sims_grade_code: $scope.temp.sims_grade_code,
                        sims_report_card_name: $scope.temp.name,
                        sims_level_code: sims_level_code,
                        sims_level_status: $scope.temp.status,
                    }
                        
                        data.opr = "U";
                       
                        dataforUpdate.push(data);
                      
                        $http.post(ENV.apiUrl + "api/ReportCardLevel/InsertUpdateDeleteReportCardLevel", dataforUpdate).then(function (msg) {
                            debugger;
                            $scope.updateResult = msg.data;
                            if ($scope.updateResult == true) {
                                swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 380 });
                            }
                            else if ($scope.updateResult == false) {
                                swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 380 });
                            }
                            else {
                                swal("Error-" + $scope.updateResults)
                            }
                            $scope.getgrid();
                        });
                      
                        $scope.table = true;
                        $scope.display = false;
                    }
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_report_card_name + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_report_card_name + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/ReportCardLevel/getReportCardLevel").then(function (res1) {

                    $scope.ReportcardLevelData = res1.data;
                    $scope.totalItems = $scope.ReportcardLevelData.length;
                    $scope.todos = $scope.ReportcardLevelData;
                    $scope.makeTodos();

                });
            }

            $scope.Delete = function () {
                debugger;
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    //var t = $scope.filteredTodos[i].uom_code;
                    var v = document.getElementById($scope.filteredTodos[i].sims_report_card_name + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                            'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            'sims_grade_code':$scope.filteredTodos[i].sims_grade_code,                           
                            'sims_level_code': $scope.filteredTodos[i].sims_level_code,                            
                            'opr': 'D',
                          
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {

                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/ReportCardLevel/InsertUpdateDeleteReportCardLevel", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;



                                        }
                                    });
                                }
                                else {
                                    swal({ text: "Record Already Mapped.Can't be Deleted. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }

                            });
                        }
                        else {

                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_report_card_name + i);
                                //var t = $scope.filteredTodos[i].sims_fee_category;
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");


                                }


                            }

                        }

                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }

               
            }

             $(function () {
                $('#app_on1').multipleSelect({
                    width: '100%'
                });
             });

           

            $scope.getsection = function (str, str1, str2) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + str + "&grade_code=" + str1 + "&academic_year=" + str2).then(function (Allsection) {
                    $scope.getSectionFromGrade = Allsection.data;
                    setTimeout(function () {
                        $('#app_on1').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                })
            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });



        }])

})();
