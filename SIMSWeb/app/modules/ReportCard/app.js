﻿(function () {
    'use strict';
    angular.module('sims.module.ReportCard', [
        'sims',
        'gettext'
    ]);
})();