﻿
(function () {
    'use strict';

    var simsController = angular.module('sims.module.main');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('MainController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', 'AuthenticationService', '$interval', '$location',  function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, AuthenticationService, $interval, $location) {
            var user = $rootScope.globals.currentUser.username;
            $rootScope.isCondensed = false;
            $scope.lang = "en";
            $scope.new_std = "";
            $rootScope.strMessage = "";
            $scope.SchoolLogo = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/SchoolLogo/';
            $rootScope.module = '';
            $rootScope.location = '';
            $scope.SelectedUserLst = [];
            $scope.global_Search = {}
            $scope.global_search_result = [];
            $scope.showDDFlagForNISS = false;
            $scope.hideMyAccountLink = false;
            $scope.paging = true;
            $scope.pagesize = '25';
            $scope.pageindex = "0";
            $scope.showRFIDIIS = false;
            $scope.acyear_disable = false;
            $scope.pager = true;
            $scope.Mobilecode = false;

            $scope.acyear_disable_fun = function () {
               
                $scope.acyear_disable=true;
            }

            $scope.acyear_enable_fun = function () {
                
                $scope.acyear_disable = false;
            }
           

            $scope.size = function (str) {
               // //console.log(str);
               // $scope.pagesize = str;
               // $scope.currentPage = 1;
               // $scope.numPerPage = str;
               //// debugger;
               // if (str == "All") {
               //     $scope.currentPage = '1';
               //     $scope.filteredTodos = $scope.global_search_result;
               //     $scope.pager = false;
               // }
               // else {
               //     $scope.pager = true;
               //     $scope.pagesize = str;
               //     $scope.currentPage = 1;
               //     $scope.numPerPage = str;
               //     $scope.makeTodos();
               // }
                // console.log("numPerPage=" + $scope.numPerPage); 

                if (str == '0') {
                    
                    var result = confirm('Do you want to continue?');
                    if (result) {
                        $scope.itemsPerPage = $scope.global_search_result.length;
                        $scope.pagesize = '0';
                    }
                    else {
                        $scope.itemsPerPage = '25';
                        $scope.pagesize = '25';
                       }
                    $scope.pager = false;
                    
                }
                else {
                  
                    $scope.itemsPerPage = str;
                    $scope.pager = true;
                }
                $scope.currentPage = 0;

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 25, $scope.maxSize = 25;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };
            
            $scope.brd = { brd_application_nm: '', brd_module_code: '' }
            // $scope.brd_application_nm = ''
            $scope.uname = $rootScope.globals.currentUser.username;

            var hide_language = ['qfis', 'pearl', 'dpsmis', 'oes', 'siscollege'];
            var timetale_pdf = ['eiama', 'eiam', 'eiand', 'eiagps'];


           // if ($http.defaults.headers.common['schoolId'] == 'qfis' || $http.defaults.headers.common['schoolId'] == 'pearl' || $http.defaults.headers.common['schoolId'] == 'dpsmis' || $http.defaults.headers.common['schoolId'] == 'siscollege' || $http.defaults.headers.common['schoolId'] == 'staging' || $http.defaults.headers.common['schoolId'] == 'staging2') {
           //     $scope.languageicon = true;
            //}

            $http.get(ENV.apiUrl + "api/common/getLang_hide").then(function (res) {
                $scope.languageicon = res.data;
            });

            if ($http.defaults.headers.common['schoolId'] == 'sbis') {
                $scope.showDDFlagForNISS = true;
            }


            //$http.defaults.headers.common['schoolId'] == 'iis'

            if ($http.defaults.headers.common['schoolId'] == 'ges' || $http.defaults.headers.common['schoolId'] == 'staging' || $http.defaults.headers.common['schoolId'] == 'staging2') {
                $scope.showRFIDIIS = true;
                $scope.Mobilecode = true;
            }



            if ($rootScope.isfistTimeLog) {
                $('#ResetPasswordModal').modal({ backdrop: 'static', keyboard: false });
                $scope.hide_reset_pass = true;
            }
            else {
                $scope.hide_reset_pass = false;

            }

            //$http.get(ENV.apiUrl + "api/common/Get_Arebicenglishdrop").then(function (res) {
            //    debugger;
            //    $scope.appl_items = res.data;
            //    console.log('Arebic deropdown' + $scope.appl_items);

            //});
            //$.ajax({
            //    url: '//freegeoip.net/json/',
            //    datatype: "json",
            //    success: function (e) {
            //        // Success callback
            //        //  alert(e.ip);
            //        //console.log(e.ip);
            //        $rootScope.ip_add = e.ip;
            //        $rootScope.dns = e.country_code;

            //    }
            //})

            console.log('Befor Ip :');

            $.getJSON("https://jsonip.com/?callback=?", function (data) {
                var Ip = data.ip;
              //  console.log('https://jsonip.com')
              // alert(data.ip);
                $rootScope.ip_add = data.ip;
                $rootScope.dns = '';
            });


            $scope.reload_page = function () {
                $state.reload();
            }


            $http.get(ENV.apiUrl + "api/common/Empdashboard/getUname?name=" + $rootScope.globals.currentUser.username).then(function (res) {


                if (res.data != '') {
                    $scope.uname = res.data;
                    $rootScope.globals.currentUser['username'] = res.data;
                    $http.get(ENV.apiUrl + "api/common/getAllModules?username=" + $scope.uname).then(function (res2) {
                        $scope.display = true;
                        $scope.obj2 = res2.data;
                        $scope.traslateEn = angular.copy(res2.data);


                        var data = $scope.obj2;
                        setTimeout(function () {
                            jQuery('.page-sidebar li > a').on('click', function (e) {
                                if ($(this).next().hasClass('sub-menu') === false) {
                                    return;
                                }
                                var parent = $(this).parent().parent();


                                parent.children('li.open').children('a').children('.arrow').removeClass('open');
                                parent.children('li.open').children('a').children('.arrow').removeClass('active');
                                parent.children('li.open').children('.sub-menu').slideUp(200);
                                parent.children('li').removeClass('open');
                                //parent.children('li').removeClass('active');

                                var sub = jQuery(this).next();
                                if (sub.is(":visible")) {
                                    jQuery('.arrow', jQuery(this)).removeClass("open");
                                    jQuery(this).parent().removeClass("active");
                                    sub.slideUp(200, function () {
                                        //handleSidenarAndContentHeight();
                                    });
                                } else {
                                    jQuery('.arrow', jQuery(this)).addClass("open");
                                    jQuery(this).parent().addClass("open");
                                    sub.slideDown(200, function () {
                                        // handleSidenarAndContentHeight();
                                    });
                                }

                                e.preventDefault();
                            });
                        }, 800);

                        // $scope.routepage($scope.obj2);
                    });

                    $http.get(ENV.apiUrl + "api/common/Get_Application?s=" + $rootScope.globals.currentUser.username).then(function (res) {
                        $scope.appl_items = res.data;
                        $scope.chkExist_state = false;


                        $scope.appcode = $state.current.name.split('.');

                        for (var i = 0; i < $scope.appl_items.length; i++) {


                            if ($scope.appl_items[i].url == $scope.appcode[1]) {
                                $scope.chkExist_state = true;
                                if ($scope.brd['brd_application_code'] == undefined) {
                                    //if ($scope.lang == 'en') {
                                        $scope.brd['brd_module_nm'] = $scope.appl_items[i].comn_mod_name_en;
                                        $scope.brd['brd_application_nm'] = $scope.appl_items[i].name_only;
                                //    }
                                 //  else {
                                        $scope.brd['brd_module_nm_ar'] = $scope.appl_items[i].comn_mod_name_ar;
                                        $scope.brd['brd_application_nm_ar'] = $scope.appl_items[i].name_only_ar;
                                   // }
                                    $scope.brd['brd_module_code'] = $scope.appl_items[i].comn_mod_code;
                                    $scope.brd['brd_application_code'] = $scope.appcode[1];

                                    $scope.check_favorite($scope.appcode[1]);
                                    $scope.ar_en_fun($scope.appcode[1]);
                                }
                            }

                        }

                        if (!$scope.chkExist_state) {
                            $state.go('main.TimeLine')
                        }





                    });
                    $http.get(ENV.apiUrl + "api/common/getUserAllDetails?uname=" + $rootScope.globals.currentUser.username).then(function (res) {

                        $scope.school_img_url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/SchoolLogo/'
                        if (res.data.group_Code == '06') {

                            $scope.flg_ck_group = true;
                        }
                        else {
                            $scope.flg_ck_group = false;
                            if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                                if ('EMP004005' == $rootScope.globals.currentUser.username) {
                                    $scope.flg_ck_group = true;
                                }
                            }

                        }
                        $scope.user_details = res.data;
                        $scope.circularobj = res.data;
                        $scope.newsobj = res.data;
                        $scope.alert_count = res.data.userAlertCount;


                        if ($scope.user_details['lic_school_logo'] == "" || $scope.user_details['lic_school_logo'] == undefined) {
                            $http.get(ENV.apiUrl + "api/common/getSchoolDetails").then(function (res) {
                                $scope.user_details['lic_school_logo'] = $scope.obj1.lic_school_logo;
                                $scope.user_details['lic_school_name'] = $scope.obj1.lic_school_name;
                                $scope.user_details['lic_school_other_name'] = $scope.obj1.lic_school_other_name;
                            });

                        }


                        if ($scope.user_details.group_Code == "06") {
                            $scope.hideMyAccountLink = true;
                        }

                        $scope.uimage = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/' + $scope.user_details.user_image;

                        if ($scope.user_details.user_image == "" || $scope.user_details.user_image == undefined || $scope.user_details.user_image == null) {
                            $('#userImage').attr('src', 'assets/img/profile.png');
                        }
                        else {
                            $('#userImage').attr('src', $scope.uimage);
                        }

                    });
                    $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                        $scope.global_count_comp = res.data;

                        if ($scope.global_count_comp) {
                            getCur(true, $scope.user_details.comp);


                        }
                        else {
                            getCur(false, $scope.user_details.comp)
                        }
                    });
                }


                else {

                    $http.get(ENV.apiUrl + "api/common/getAllModules?username=" + $scope.uname).then(function (res2) {
                        $scope.display = true;
                        $scope.obj2 = res2.data;
                        $scope.traslateEn = angular.copy(res2.data);


                        var data = $scope.obj2;
                        setTimeout(function () {
                            jQuery('.page-sidebar li > a').on('click', function (e) {
                                if ($(this).next().hasClass('sub-menu') === false) {
                                    return;
                                }
                                var parent = $(this).parent().parent();


                                parent.children('li.open').children('a').children('.arrow').removeClass('open');
                                parent.children('li.open').children('a').children('.arrow').removeClass('active');
                                parent.children('li.open').children('.sub-menu').slideUp(200);
                                parent.children('li').removeClass('open');
                                //parent.children('li').removeClass('active');

                                var sub = jQuery(this).next();
                                if (sub.is(":visible")) {
                                    jQuery('.arrow', jQuery(this)).removeClass("open");
                                    jQuery(this).parent().removeClass("active");
                                    sub.slideUp(200, function () {
                                        //handleSidenarAndContentHeight();
                                    });
                                } else {
                                    jQuery('.arrow', jQuery(this)).addClass("open");
                                    jQuery(this).parent().addClass("open");
                                    sub.slideDown(200, function () {
                                        // handleSidenarAndContentHeight();
                                    });
                                }

                                e.preventDefault();
                            });
                        }, 800);

                        // $scope.routepage($scope.obj2);
                    });

                    $http.get(ENV.apiUrl + "api/common/Get_Application?s=" + $rootScope.globals.currentUser.username).then(function (res) {
                        $scope.appl_items = res.data;
                        $scope.chkExist_state = false;


                        $scope.appcode = $state.current.name.split('.');

                        for (var i = 0; i < $scope.appl_items.length; i++) {


                            if ($scope.appl_items[i].url == $scope.appcode[1]) {
                                $scope.chkExist_state = true;
                                if ($scope.brd['brd_application_code'] == undefined) {

                                   // if ($scope.lang == 'en') {
                                        $scope.brd['brd_module_nm'] = $scope.appl_items[i].comn_mod_name_en;
                                        $scope.brd['brd_application_nm'] = $scope.appl_items[i].name_only;
                                    //}
                                   // else {
                                        $scope.brd['brd_module_nm_ar'] = $scope.appl_items[i].comn_mod_name_ar;
                                        $scope.brd['brd_application_nm_ar'] = $scope.appl_items[i].name_only_ar;
                                  //  }
                                    $scope.brd['brd_module_code'] = $scope.appl_items[i].comn_mod_code;
                                    $scope.brd['brd_application_code'] = $scope.appcode[1];

                                    $scope.check_favorite($scope.appcode[1]);
                                    $scope.ar_en_fun($scope.appcode[1]);
                                }
                            }

                        }

                        if (!$scope.chkExist_state) {
                            $state.go('main.TimeLine')
                        }





                    });
                    $http.get(ENV.apiUrl + "api/common/getUserAllDetails?uname=" + $rootScope.globals.currentUser.username).then(function (res) {

                        $scope.school_img_url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/SchoolLogo/'
                        if (res.data.group_Code == '06') {

                            $scope.flg_ck_group = true;
                        }
                        else {
                            $scope.flg_ck_group = false;
                            if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                                if ('EMP004005' == $rootScope.globals.currentUser.username) {
                                    $scope.flg_ck_group = true;
                                }
                            }

                        }
                        $scope.user_details = res.data;
                        $scope.circularobj = res.data;
                        $scope.newsobj = res.data;
                        $scope.alert_count = res.data.userAlertCount;

                     
                        if ($scope.user_details['lic_school_logo'] == "" || $scope.user_details['lic_school_logo'] == undefined) {
                            $http.get(ENV.apiUrl + "api/common/getSchoolDetails").then(function (res) {
                                $scope.user_details['lic_school_logo'] = $scope.obj1.lic_school_logo;
                                $scope.user_details['lic_school_name'] = $scope.obj1.lic_school_name;
                                $scope.user_details['lic_school_other_name'] = $scope.obj1.lic_school_other_name;
                            });

                        }


                        if ($scope.user_details.group_Code == "06") {
                            $scope.hideMyAccountLink = true;
                        }

                        $scope.uimage = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/' + $scope.user_details.user_image;

                        if ($scope.user_details.user_image == "" || $scope.user_details.user_image == undefined || $scope.user_details.user_image == null) {
                            $('#userImage').attr('src', 'assets/img/profile.png');
                        }
                        else {
                            $('#userImage').attr('src', $scope.uimage);
                        }

                    });
                    $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                        $scope.global_count_comp = res.data;

                        if ($scope.global_count_comp) {
                            getCur(true, $scope.user_details.comp);


                        }
                        else {
                            getCur(false, $scope.user_details.comp)
                        }
                    });



                }
            });

            $scope.hide_folat_btn = true;

            $http.get(ENV.apiUrl + "api/common/getUrls").then(function (res) {
                console.log('URL: ', res.data.table);
                if (res.data.table.length > 0) {
                    
                    var f = 0;
                    for (var i = 0; i < res.data.table.length; i++) {
                        if (res.data.table[i].sims_appl_parameter == 'V') {
                            $scope.video_url = res.data.table[i].sims_appl_form_field_value1;
                            $scope.video_url_hide = res.data.table[i].sims_appl_form_field_value3;
                            if (res.data.table[i].sims_appl_form_field_value3 == 'I') { f++; }
                        }

                        if (res.data.table[i].sims_appl_parameter == 'S') {
                            $scope.support_url = res.data.table[i].sims_appl_form_field_value1;
                            $scope.support_url_hide = res.data.table[i].sims_appl_form_field_value3;
                            if (res.data.table[i].sims_appl_form_field_value3 == 'I') { f++; }

                        }

                        if (res.data.table[i].sims_appl_parameter == 'W') {
                            $scope.webinor_url = res.data.table[i].sims_appl_form_field_value1;
                            $scope.webinor_url_hide = res.data.table[i].sims_appl_form_field_value3;
                            if (res.data.table[i].sims_appl_form_field_value3 == 'I') { f++; }


                        }

                        if (res.data.table[i].sims_appl_parameter == 'C') {
                            $scope.chat_url = res.data.table[i].sims_appl_form_field_value1;
                            $scope.chat_url_hide = res.data.table[i].sims_appl_form_field_value3;
                            if (res.data.table[i].sims_appl_form_field_value3 == 'I') { f++; }


                        }

                    }
                    if (f == 3) {
                        $scope.hide_folat_btn = true;
                    }
                }
            });


            $scope.float_button_clk = function (str) {

                switch (str) {
                    case 1:

                        window.open($scope.webinor_url);
                        break;
                    case 2:
                        window.open($scope.support_url);
                        break;

                    case 3:
                        window.open($scope.video_url);
                        break;

                    case 4:
                        var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
                        (function () {
                            var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
                            s1.async = true;
                            s1.src = $scope.chat_url;
                            s1.charset = 'UTF-8';
                            s1.setAttribute('crossorigin', '*');
                            s0.parentNode.insertBefore(s1, s0);
                        })();

                        $scope.on_click_support();
                        break;



                }

            }

            $scope.resetDashborad = function () {
                $http.get(ENV.apiUrl + "api/common/Empdashboard/getApplication_Dash?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                    for (var i = 0; i < res.data.length; i++) {
                        var id = '#' + res.data[i].comn_user_appl_code;
                        $(id).show();
                    }
                });
            }
            $scope.defaultGrid = function (res) {
                
                $('.table-responsive').addClass(res);
                $('.table-responsive').removeClass('comfort');
                $('.table-responsive').removeClass('compact');

                $http.post(ENV.apiUrl + "api/common/updateMenu?menu=" + '1' + "&uname=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.Main_menu_pref = '1';

                });


            }
            $scope.comfortGrid = function (res) {

                $('.table-responsive').addClass(res);
                $('.table-responsive').removeClass('default');
                $('.table-responsive').removeClass('compact');
                $http.post(ENV.apiUrl + "api/common/updateMenu?menu=" + '2' + "&uname=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.Main_menu_pref = '2';
                });
            }
            $scope.compactGrid = function (res) {

                $('.table-responsive').addClass(res);
                $('.table-responsive').removeClass('default');
                $('.table-responsive').removeClass('comfort');

                $http.post(ENV.apiUrl + "api/common/updateMenu?menu=" + '3' + "&uname=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.Main_menu_pref = '3';

                });
            }


            $http.get(ENV.apiUrl + "api/common/getMenuPref?uname=" + $rootScope.globals.currentUser.username).then(function (res) {

                $scope.Main_menu_pref = res.data;
            });


            $scope.is_support = false;
            $scope.on_click_support = function () {
                if ($scope.is_support) {
                    $('.help_widget_bounce_in').addClass('help_widget_bounce_out').removeClass('help_widget_bounce_in');


                    $('.fa-times').addClass('fa-question').removeClass('fa-times');


                    $('support_btn_open').css({ "display": "block" })

                    $('support_btn_closed').css({ "display": "none" })



                }
                else {
                    $('.help_widget_bounce_out').addClass('help_widget_bounce_in').removeClass('help_widget_bounce_out');
                    $('.fa-question').addClass('fa-times').removeClass('fa-question');
                    $('support_btn_open').css({ "display": "block" })

                    $('support_btn_closed').css({ "display": "none" })




                }
                $scope.is_support = !$scope.is_support;
            }


            $scope.check_favorite = function (appl_code) {
                $http.get(ENV.apiUrl + "api/common/getAllFaviourate?user=" + $rootScope.globals.currentUser.username + "&appl_code=" + appl_code).then(function (res) {

                    if (res.data == '1') {
                        $scope.flg_favorite = true;
                    }
                    else {
                        $scope.flg_favorite = false;

                    }

                });

                $http.get(ENV.apiUrl + "api/common/GetAllIcon?app_code=" + appl_code).then(function (res) {

                    $scope.flg_help = res.data.help;
                    $scope.flg_video_help = res.data.video_help;
                    $scope.flg_dependancy = res.data.dependancy;

                });
                $scope.hide_folat_btn = true;
            }

            $scope.report = function () {
                
                try {
                    $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                    $scope.location = $scope.rpt.location;
                    $scope.parameter = $scope.rpt.parameter;
                    $scope.state = $scope.rpt.state;
                }
                catch (ex) {
                }
                var s;

                //if ($http.defaults.headers.common['schoolId'] == 'dpsmis')
                //    s = "SimsReports." + $scope.location + ",SimsReports";


                //else
                //    s = "SimsReports." + $scope.location + ",SimsReports" + $http.defaults.headers.common['schoolId'];


                //  window.localStorage["Finn_comp"] = JSON.stringify(data)

                s = "SimsReports." + $scope.location + ",SimsReports";

                var url = window.location.href;
                var domain = url.substring(0, url.indexOf(':'))
                if ($http.defaults.headers.common['schoolId'] == 'sms12') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.nis.edu.kw/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.nis.edu.kw/report/api/reports/';

                }
                else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                }

                else {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                }
                console.log(service_url);


                $scope.parameters = {}

                $("#reportViewer4")
                               .telerik_ReportViewer({
                                   //serviceUrl: ENV.apiUrl + "api/reports/",
                                   serviceUrl: service_url,

                                   viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                   scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                   // Zoom in and out the report using the scale
                                   // 1.0 is equal to 100%, i.e. the original size of the report
                                   scale: 1.0,
                                   ready: function () {
                                       //this.refreshReport();
                                   },
                                   PersistSession: true

                               });





                var reportViewer = $("#reportViewer4").data("telerik_ReportViewer");
                reportViewer.reportSource({
                    report: s,
                    parameters: $scope.parameter,
                });

                //var rv = $("#reportViewer1").data("telerik_ReportViewer");
                //rv.commands.print.exec();

                setInterval(function () {

                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                    //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

                }, 1000);


                $timeout(function () {
                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                    //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


                }, 100)

                $('#report_Modal').modal({ backdrop: 'static', keyboard: false });
            }


            $scope.report_cancel = function () {

                $('#report_Modal').modal('hide');

            }


            $scope.brd_module_click = function () {

                $rootScope.module = {};
                $rootScope.module['module_code'] = $scope.brd['brd_module_code'];
                $rootScope.module['module_name_en'] = $scope.brd['brd_module_nm'];
                $rootScope.module['module_name_ar'] = '';
                $scope.brd['brd_application_nm'] = '';
                $state.go('main.UserMenu');
                $rootScope.$broadcast('module-chage', { data: $rootScope.module });
                $("#Applsource").select2("val", "null");
                $scope.appl_Code = '';
            }

            $scope.redirect = function (i) {
                console.log(i);
                $rootScope.module = i;
                //if ($scope.lang == 'en')
                    $scope.brd['brd_module_nm'] = i.module_name_en;
               // else
                    $scope.brd['brd_module_nm_ar'] = i.module_name_ar;


                $scope.brd['brd_module_code'] = i.module_code;

                $scope.brd['brd_application_nm'] = '';
                $state.go('main.UserMenu');
                $rootScope.$broadcast('module-chage', { data: $rootScope.module });
                $("#Applsource").select2("val", "null");
                $scope.appl_Code = '';

            }

            if (window.localStorage["Finn_comp"] != undefined) {
                $scope.fin = {};

                $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
                $scope.fin['comp_code'] = $scope.finnDetail.company;
                $scope.fin['financial_year'] = $scope.finnDetail.year;
                $scope.companyName = $scope.finnDetail.companyame;

            }



            $scope.chk_url = function () {
                $scope.chkExist_state = false;


                $scope.appcode = $state.current.name.split('.');

                for (var i = 0; i < $scope.appl_items.length; i++) {


                    if ($scope.appl_items[i].url == $scope.appcode[1]) {
                        $scope.chkExist_state = true;

                    }

                }
                if (!$scope.chkExist_state) {
                    $state.go('main.TimeLine')
                }

            }




            $scope.alert_count_change = function (str) {
                $scope.alert_count = str;

            }






            $scope.help_window = function () {

                $http.get(ENV.apiUrl + "api/common/getapplicationPdfUrl?appl_code=" + $scope.brd['brd_application_code']).then(function (res) {


                    window.open(res.data);

                });

            }

            $scope.video_help_window = function () {

                $http.get(ENV.apiUrl + "api/common/getapplicationVideoUrl?appl_code=" + $scope.brd['brd_application_code']).then(function (res) {


                    window.open(res.data);

                });

            }

            $scope.favorite_click = function (str) {

                if ($scope.brd.brd_application_nm == undefined || $scope.brd.brd_application_nm == '') {
                    swal('please select Application first')
                }
                else {
                    if (str) {
                        $scope.flg_favorite = true;
                        $http.post(ENV.apiUrl + "api/ResetPassword/CUDfavorite?app_code=" + $scope.brd.brd_application_code + "&user_name=" + $rootScope.globals.currentUser.username + "&status=" + 1).then(function (AlertDetails) {

                        });
                    }
                    else {
                        $scope.flg_favorite = false;
                        $http.post(ENV.apiUrl + "api/ResetPassword/CUDfavorite?app_code=" + $scope.brd.brd_application_code + "&user_name=" + $rootScope.globals.currentUser.username + "&status=" + 0).then(function (AlertDetails) {

                        });
                    }
                }
            }

            $scope.dependancy_window = function () {

                $("#dependancy_model").modal("show");
                $scope.no_record_msg = false;
                var appcode = $state.current.name.split(".");

                if (appcode.length > 0) {
                    if (appcode[1] == "ReportCard") { appcode[1] = $rootScope.main_appl_code }
                    $http.get(ENV.apiUrl + "api/common/getdepandacy_detail?appl_code=" + appcode[1]).then(function (res) {
                        $scope.depandancy_lst = res.data;
                        if ($scope.depandancy_lst.length > 0) {
                            $scope.no_record_msg = false;

                        }
                        else {
                            $scope.no_record_msg = true;
                        }
                    });


                }
            }

            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };


            $scope.get_time_line_appl_code = function (appl_code) {

                for (var i = 0; i < $scope.appl_items.length; i++) {

                    if ($scope.appl_items[i].url == appl_code) {
                     //   if ($scope.lang == 'en') {
                            $scope.brd['brd_application_nm'] = $scope.appl_items[i].name_only;
                            $scope.brd['brd_module_nm'] = $scope.appl_items[i].comn_mod_name_en;
                      //  }
                       // else {
                            $scope.brd['brd_application_nm_ar'] = $scope.appl_items[i].name_only_ar;
                            $scope.brd['brd_module_nm_ar'] = $scope.appl_items[i].comn_mod_name_ar;
                       // }
                        $scope.brd['brd_module_code'] = $scope.appl_items[i].comn_mod_code;

                        $scope.brd['brd_application_code'] = appl_code;
                        $scope.check_favorite($scope.appcode[1]);
                        $scope.ar_en_fun($scope.appcode[1]);

                        if ($scope.fin == undefined) {
                            if ($scope.appl_items[i].comn_mod_code == '005') {
                                $scope.main_loader_show = true;
                                $http.get(ENV.apiUrl + "api/PDCSubmission/GetComp_Name?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                                    $scope.cmbCompany = res.data;
                                    if (res.data.length == 1) {
                                        $scope.companyName = res.data[0].comp_name;

                                        $scope.finnComp = res.data[0].comp_code;
                                        $scope.comp_short_name = res.data[0].comp_short_name;
                                        $scope.input_vat_acct = res.data[0].comp_input_vat_posting_account_code;
                                        $scope.comp_vat_status = res.data[0].include_vat_enable_status;
                                        $scope.comp_vat_per = res.data[0].comp_vat_per;


                                        $http.get(ENV.apiUrl + "api/PDCSubmission/?comp_code=" + $scope.finnComp).then(function (res) {

                                            if (res.data.length > 0) {

                                                console.log('Finn');

                                                var data = {
                                                    year: res.data[0].financial_year,
                                                    company: $scope.finnComp,
                                                    companyame: $scope.companyName,
                                                    comp_short_name: $scope.comp_short_name,
                                                    input_vat_acct: $scope.input_vat_acct,
                                                    comp_vat_status: $scope.comp_vat_status,
                                                    comp_vat_per: $scope.comp_vat_per
                                                }
                                                window.localStorage["Finn_comp"] = JSON.stringify(data)

                                                $state.reload();
                                                setTimeout(function () {
                                                    $scope.main_loader_show = false;
                                                }, 500);

                                            }
                                        });
                                    }
                                    else if (res.data.length > 1) {
                                        $scope.financeWindow();
                                    }


                                });
                            }
                        }
                    }

                }
            }


            $scope.RedirectCommunicationPage = function () {
                $state.go('main.communication');
                $scope.show_module_brdcrumb('Communication');

                $scope.hide_folat_btn = false;

            }



            $scope.contentClick = function () {
                $("#notificationContainer").fadeOut('slow');
            }
            $scope.goAlert = function () {
                $http.get(ENV.apiUrl + "api/common/Alert/getTop10Alert?username=" + $rootScope.globals.currentUser.username).then(function (AlertDetailsd) {
                    $scope.AlertDetails = AlertDetailsd.data;
                    if ($scope.AlertDetails.length > 0) {
                        $("#notificationContainer").fadeToggle(300);
                    }
                    else {
                        //                        swal({ text: "Record not found", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                    }
                });
            }

            $scope.goAlertPage = function () {
                $("#notificationContainer").fadeOut('slow');
                $state.go('main.Sim166', { flag: 'ALL' })
                $scope.show_module_brdcrumb('Alert');
                $scope.hide_folat_btn = false;
            }

            $scope.notClick = function (curr) {
                
                $("#notificationContainer").fadeOut('slow');
                $state.go('main.Sim166', { flag: 'one', curnum: curr });
            }
            $scope.total_counts = 0;

            $scope.getTotal_Communication_count = function () {
               
                $http.get(ENV.apiUrl + "api/Communication/getPortalCommunicationCounts?user_code_from=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.total_counts = res.data;
                });
            }
            $scope.getTotal_Communication_count();

            $scope.lms_redirct = function (str) {

                


                if ($http.defaults.headers.common['schoolId'] == 'adis') {
                    if (str == '2')   //Online Access
                    {
                        window.open('http://oa.mograsys.com/adis/OnlineAdmission.html#?flag=true', "_new");
                    }


                }


                if ($http.defaults.headers.common['schoolId'] == 'imert') {
                    if (str == 'Lms001' || str == '2') {
                        window.open('http://imert.learntron.net/learntron/index.html', "_new");

                    }
                }

                else if ($http.defaults.headers.common['schoolId'] == 'gsis') {
                    if (str == 'Lms001')   //Online Access
                    {
                        window.open('http://lms.mograsys.com/mdlgsis/', "_new");
                    }
                }

                else if ($http.defaults.headers.common['schoolId'] == 'asd' || $http.defaults.headers.common['schoolId'] == 'asdportal') {
                    if (str == 'Lms001')   //Online Access
                    {
                        $state.go('main.lmsAsd')
                    }
                }
                else {
                    if (str == 'Lms001') {

                        $http.get(ENV.apiUrl + "api/common/getUserDetails_pass?uname=" + $rootScope.globals.currentUser.username).then(function (res) {
                            // $scope.obj = res.data;
                            console.log($scope.schoolDetails_data.lic_lms_url);
                            //  window.open($scope.schoolDetails_data.lic_lms_url + "?u=" + $rootScope.globals.currentUser.username + "&p=" + res.data.table[0].comn_user_password, "_new");
                            window.open($scope.schoolDetails_data.lic_lms_url + "?u=" + $rootScope.globals.currentUser.username + "&p=" + res.data.table[0].comn_user_password + '&t=t&sc=' + $http.defaults.headers.common['schoolId'], "_new");



                        });


                    }
                }

                if (str == 'Lms003')   // local Access
                {
                    window.open('http://10.10.200.3:4000/learntron/index.html?u=' + $rootScope.globals.currentUser.username + '&k=kHs6EhhdGGU=', "_new");
                }
                if (str == 'Lms002') //Online
                {
                    window.open('http://dpsmisdohacsms.com:4000/learntronhome/index.html?u=' + $rootScope.globals.currentUser.username + '&k=kHs6EhhdGGU=', "_new");

                }
            }


            $scope.dashboard_load = function () {
                $state.go('main.EmpDshBrd')
                $scope.show_module_brdcrumb('Dashboard');
                $scope.hide_folat_btn = false;

            }

            $scope.menu_load = function () {
                $scope.show_module_brdcrumb('Menu');
                $state.go('main.menuModule');
                $scope.hide_folat_btn = false;
            }

            $scope.show_module_brdcrumb = function (str) {
                $scope.brd['brd_module_code'] = str;
                $scope.brd['brd_module_nm'] = str;

                $scope.brd['brd_application_nm'] = '';
            }

            $scope.timeline_load = function () {
                $scope.show_module_brdcrumb('Timeline');
                $state.go('main.TimeLine')

                $scope.hide_folat_btn = false;
            }


            //$http.get(ENV.apiUrl + "api/common/GetSystemAlerts").then(function (res) {

            //});



            $scope.changePassword = function () {
                var data =
                    {
                        OldPasscode: $scope.temp.OldPasscode,
                        NewPasscode: $scope.temp.NewPasscode,
                        UserName: $rootScope.globals.currentUser.username
                    }
                var NewPasscode = $scope.temp.NewPasscode;
                var confirmpass = $scope.temp.cnfrmPass;

                if (NewPasscode == confirmpass) {
                    $scope.temp.UserName = $rootScope.globals.currentUser.username;
                    $http.post(ENV.apiUrl + "api/common/ResetPassword", data).then(function (res) {
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Password Changes Sussessfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                            $('#top').modal('hide');
                            $(".modal-backdrop").removeClass("modal-backdrop");

                            $scope.display = true;
                            $scope.obj = res.data;
                            console.log($rootScope.globals.currentUser.username);
                            AuthenticationService.ClearCredentials();
                            setTimeout(function () { $state.go("login"); }, 1500)
                            $scope.temp["NewPasscode"] = "";
                            $scope.temp["cnfrmPass"] = "";
                            $scope.temp["OldPasscode"] = "";
                        }
                        else {
                            swal({ text: "Plese Enter Correct Old Password", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 300, height: 200 });
                            $('#top').modal('show');
                            $scope.temp["OldPasscode"] = "";

                        }

                    });
                }

                else {
                    swal({ text: "New Password And Confirm Password Doesn`t Match ", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 300, height: 200 });
                    $('#top').modal('show');
                    $scope.temp["cnfrmPass"] = "";
                } sbi


                //console.log("change");
            }


            $(window).bind('keydown', function (event) {

                if (event.keyCode == '13' && $scope.global_Search_show) {

                    if ($scope.gbl_tab == 'tab1') {
                        $scope.Global_Search_by_student();
                    }
                    else if ($scope.gbl_tab == 'tab2') {
                        $scope.Global_Search_by_parent();
                    }
                    else if ($scope.gbl_tab == 'tab3') {
                        $scope.Global_Search_SearchParentSectionWise();
                    }
                    else if ($scope.gbl_tab == 'tab4') {
                        $scope.Global_Search_by_Teacher();
                    }
                    else if ($scope.gbl_tab == 'tab5') {
                        $scope.Global_Search_by_User();
                    }
                    else if ($scope.gbl_tab == 'tab6') {
                        $scope.Global_Search_by_employee();
                    }
                    else if ($scope.gbl_tab == 'tab7') {
                        $scope.Global_Search_by_Uassignedstudent();
                    }
                    else if ($scope.gbl_tab == 'tab8') {
                        $scope.Global_Search_by_parent_unassigned();
                    }
                }
            });



            $scope.navigate = function (str) {
                console.log(str);
                switch (str) {
                    case 1:
                        //if ($http.defaults.headers.common['schoolId'] == 'oes')
                        //    $state.go("main.UpdOES");

                        //else if ($http.defaults.headers.common['schoolId'] == 'pearl')
                        //    $state.go("main.EmProf");
                        //else if ($http.defaults.headers.common['schoolId'] == 'adisw')
                        //    $state.go("main.UAdisw");
                        //else if ($http.defaults.headers.common['schoolId'] == 'dpsmis')
                        //    $state.go("main.edtemp");
                        //else if ($http.defaults.headers.common['schoolId'] == 'asd')
                        //    $state.go("main.UEmASD");
                        //else
                        //    $state.go("main.UpdateEmployee");
                        //break;
                        $state.go("main.TsCaPr"); //TsCaPr //VwAppE
                        break;
                    case 2:
                        $('#ResetPasswordModal').modal('show');
                        break;

                    case 3:
                        window.open('https://help.mograsys.com/course/view.php?id=52');
                        break;

                    case 4:
                        window.open('https://www.youtube.com/channel/UCwukbTbjMaaWTo7XeECKDyw?view_as=subscriber');
                        break;
                }
            }


            $scope.url = ENV.siteUrl;

            console.log("In Main Page::" + $rootScope.globals.currentUser.username);

            $scope.started = false;



            $scope.financeWindow = function () {
                $scope.fin = [];
                $http.get(ENV.apiUrl + "api/PDCSubmission/GetComp_Name?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.cmbCompany = res.data;
                    if (res.data.length >= 1) {
                        $scope.fin.comp_code = res.data[0].comp_code;
                        $scope.fin.comp_short_name = res.data[0].comp_short_name;
                        $scope.fin.input_vat_acct = res.data[0].comp_input_vat_posting_account_code;
                        $scope.fin.comp_vat_status = res.data[0].include_vat_enable_status;
                        $scope.fin.comp_vat_per = res.data[0].comp_vat_per;

                        $http.get(ENV.apiUrl + "api/PDCSubmission/GetFinancial_year?comp_code=" + $scope.fin.comp_code + "&username=" + $rootScope.globals.currentUser.username).then(function (res) {
                            $scope.cmbYear = res.data;
                            if (res.data.length > 0) {
                                $scope.fin.financial_year = res.data[0].financial_year;
                            }
                        });
                    }


                });

                $('#myModal3').modal('show');

            }

            $scope.company_change = function (str) {
                $http.get(ENV.apiUrl + "api/PDCSubmission/GetFinancial_year?comp_code=" + str + "&username=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.cmbYear = res.data;
                    if (res.data.length > 0)
                        $scope.fin['financial_year'] = res.data[0].financial_year;
                });
            }

            $scope.submit = function () {
                $('#myModal3').modal('hide');
                if ($scope.fin.comp_code != undefined) {
                    if ($scope.fin.financial_year != undefined) {

                        $scope.main_loader_show = true;
                        $http.post(ENV.apiUrl + "api/PDCSubmission/UpdateFinancial_year?comp_cd=" + $scope.fin.comp_code + "&year=" + $scope.fin.financial_year + "&username=" + $rootScope.globals.currentUser.username).then(function (res) {

                            if (res.data) {
                                $scope.companyName = $("#finCompany").find("option:selected").text();

                                var data = {
                                    year: $scope.fin.financial_year,
                                    company: $scope.fin.comp_code,
                                    companyame: $scope.companyName,
                                    comp_short_name: $scope.fin.comp_short_name,
                                    input_vat_acct: $scope.fin.input_vat_acct,
                                    comp_vat_status: $scope.fin.comp_vat_status,
                                    comp_vat_per: $scope.fin.comp_vat_per
                                }
                                window.localStorage["Finn_comp"] = JSON.stringify(data)





                                $state.reload();
                                setTimeout(function () {
                                    $scope.main_loader_show = false;
                                }, 500);
                            }
                            else {
                            }
                        });
                    }
                }
            }



            $scope.changeColor = function (str, i) {
                $scope.test = '#mod' + i;
                $scope.sd = $scope.obj2[i].module_color;
                $($scope.test).css({ 'background-color': $scope.sd })
            }

            $scope.resetColor = function (i) {
                $scope.test = '#mod' + i
                $scope.sd = $scope.obj2[i].module_color;
                $($scope.test).css({ 'background-color': '' })
            }

            $scope.idSelectedMenu = null;
            $scope.setSelected = function (idSelectedMenu) {
                $scope.idSelectedMenu = idSelectedMenu;
                console.log(idSelectedMenu);
            }



            setTimeout(function () {
                $("#Applsource").select2();

            }, 100);

            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

                //if (isMobile.any()) {
                $rootScope.mobStyle = { 'display': 'block' };
                //   $rootScope.moblie_logout = { 'display': 'block' };
                $('.user-info-wrapper').css({ 'display': 'block' });
                $('.mobstud').css({ 'display': 'block' });
                $scope.mobleView = true;
            }
            else {
                $rootScope.mobStyle = { 'display': 'none' };
                // $rootScope.moblie_logout = { 'display': 'none' };
                $('.user-info-wrapper').css({ 'display': 'none' });
                $('.mobstud').css({ 'display': 'none' });
                $scope.mobleView = false;

            }

            $scope.menutoggle = function () {


                if ($scope.mobile == true) {

                    $('body').addClass('breakpoint-768 pace-done open-menu-left');
                    $('#main-menu').addClass('page-sidebar visible');
                    $scope.mobile = false;
                }
                else {
                    $('body').removeClass('breakpoint-768 pace-done open-menu-left');
                    $('body').addClass('breakpoint-768 pace-done ');
                    $('#main-menu').removeClass('page-sidebar visible');
                    $('#main-menu').addClass('page-sidebar');
                    $scope.mobile = true;

                }
            }

            $scope.submnuClick = function () {
                $('body').removeClass('breakpoint-768 pace-done open-menu-left');
                $('body').addClass('breakpoint-768 pace-done ');
                $('#main-menu').removeClass('page-sidebar visible');
                $('#main-menu').addClass('page-sidebar');
            }


            $scope.getDateTime = function () {
                var now = new Date();
                var year = now.getFullYear();
                var month = now.getMonth() + 1;
                var day = now.getDate();
                var hour = now.getHours();
                var minute = now.getMinutes();
                var second = now.getSeconds();
                if (month.toString().length == 1) {
                    var month = '0' + month;
                }
                if (day.toString().length == 1) {
                    var day = '0' + day;
                }
                if (hour.toString().length == 1) {
                    var hour = '0' + hour;
                }
                if (minute.toString().length == 1) {
                    var minute = '0' + minute;
                }
                if (second.toString().length == 1) {
                    var second = '0' + second;
                }
                var dateTime = year + '/' + month + '/' + day + ' ' + hour + ':' + minute + ':' + second;
                return dateTime;
            }




            $scope.application_change = function (str1, index) {
                // $scope.hide_folat_btn = true;
                
                $scope.check_favorite(str1);
                $scope.ar_en_fun(str1);
                try {
                    var index = $("select[name='Applsource'] option:selected").index();
                    console.log(index)

                    var state = 'main.' + str1;
                    console.log($scope.appl_items[index]);
                    $scope.location = $scope.appl_items[index].location;
                    $rootScope.location = $scope.location;

                    $rootScope.main_appl_code = str1;


                    var mo = '#m' + $scope.appl_items[index].comn_mod_code;
                    var mod = '#mod' + $scope.appl_items[index].comn_mod_code;
                    if ($scope.mod_old != undefined) {
                        $(('#mod' + $scope.mod_old)).css({ 'display': 'none' });

                    }
                    //$(mod).focus(function () { console.log( 'focus')})
                    console.log('applode');

                    console.log(str1);
                    var v = document.getElementById(str1);

                    console.log(v);

                    $(mod).css({ 'display': 'block' });

                    $scope.mod_old = $scope.appl_items[index].comn_mod_code;

                    $scope.idSelectedMenu = str1;
                }
                catch (ex) {
                }

                if ($scope.appl_items[index].comn_appl_type == 'R') {
                    setTimeout(function () { $state.go('main.ReportCard'); }, 100);
                    $scope.brd['brd_module_code'] = $scope.appl_items[index].comn_mod_code;
                  //  if ($scope.lang == 'en') {
                        $scope.brd['brd_module_nm'] = $scope.appl_items[index].comn_mod_name_en;
                        $scope.brd['brd_application_nm'] = $scope.appl_items[index].name_only;
                   // }
                   // else {
                        $scope.brd['brd_module_nm_ar'] = $scope.appl_items[index].comn_mod_name_ar;
                        $scope.brd['brd_application_nm_ar'] = $scope.appl_items[index].name_only_ar;
                   /// }
                    $scope.brd['brd_application_code'] = str1;

                    $state.go('main');

                }
                else {


                    if ($scope.fin == undefined) {
                        if ($scope.appl_items[index].comn_mod_code == '005') {
                            $scope.main_loader_show = true;
                            $http.get(ENV.apiUrl + "api/PDCSubmission/GetComp_Name?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                                $scope.cmbCompany = res.data;
                                if (res.data.length == 1) {
                                    $scope.companyName = res.data[0].comp_name;

                                    $scope.finnComp = res.data[0].comp_code;
                                    $scope.comp_short_name = res.data[0].comp_short_name;
                                    $scope.input_vat_acct = res.data[0].comp_input_vat_posting_account_code;
                                    $scope.comp_vat_status = res.data[0].include_vat_enable_status;
                                    $scope.comp_vat_per = res.data[0].comp_vat_per;


                                    $http.get(ENV.apiUrl + "api/PDCSubmission/GetFinancial_year?comp_code=" + $scope.finnComp + "&username=" + $rootScope.globals.currentUser.username).then(function (res) {

                                        if (res.data.length > 0) {

                                            console.log('Finn');

                                            var data = {
                                                year: res.data[0].financial_year,
                                                company: $scope.finnComp,
                                                companyame: $scope.companyName,
                                                comp_short_name: $scope.comp_short_name,
                                                input_vat_acct: $scope.input_vat_acct,
                                                comp_vat_status: $scope.comp_vat_status,
                                                comp_vat_per: $scope.comp_vat_per

                                            }
                                            window.localStorage["Finn_comp"] = JSON.stringify(data)

                                            $state.reload();
                                            setTimeout(function () {
                                                $scope.main_loader_show = false;
                                            }, 500);

                                        }
                                    });
                                }
                                else if (res.data.length > 1) {
                                    $scope.financeWindow();
                                }


                            });
                        }
                    }

                    try {
                        $state.go(state);
                       // if ($scope.lang == 'en') {
                            $scope.brd['brd_module_nm'] = $scope.appl_items[index].comn_mod_name_en;
                            $scope.brd['brd_application_nm'] = $scope.appl_items[index].name_only;
                      //  }
                       // else {
                            $scope.brd['brd_module_nm_ar'] = $scope.appl_items[index].comn_mod_name_ar;
                            $scope.brd['brd_application_nm_ar'] = $scope.appl_items[index].name_only_ar;
                       // }

                        $scope.brd['brd_module_code'] = $scope.appl_items[index].comn_mod_code;
                        $scope.brd['brd_application_code'] = str1;
                    }
                    catch (ex) { }
                }

                try {
                    if (!$rootScope.isCondensed) {
                        v.scrollIntoView();
                    }
                }
                catch (e) { }
                if ($scope.oldobj == undefined) {


                    $scope.oldobj = {
                        opr: 'I',
                        comn_audit_user_code: $rootScope.globals.currentUser.username,
                        comn_appl_name_en: str1,
                        comn_audit_start_time: $scope.getDateTime(),
                        comn_audit_ip: $rootScope.ip_add,
                        comn_audit_dns: $rootScope.dns,

                        //comn_audit_end_time: '',
                    }
                    $http.post(ENV.apiUrl + "api/common/InsertUserAudit", $scope.oldobj).then(function (res) {

                    });
                }
                else {

                    var edate = $scope.getDateTime()
                    var data = {
                        opr: 'I',
                        comn_audit_user_code: $rootScope.globals.currentUser.username,
                        comn_appl_name_en: str1,
                        comn_audit_start_time: edate,
                        //comn_audit_end_time: '',
                        comn_audit_ip: $rootScope.ip_add,
                        comn_audit_dns: $rootScope.dns,
                    }

                    $http.post(ENV.apiUrl + "api/common/InsertUserAudit", data).then(function (res) {


                        $scope.oldobj['comn_audit_end_time'] = edate
                        $scope.oldobj['opr'] = 'J'

                        $http.post(ENV.apiUrl + "api/common/InsertUserAudit", $scope.oldobj).then(function (res) {

                            $scope.oldobj['comn_audit_start_time'] = edate
                            $scope.oldobj['comn_appl_name_en'] = str1

                        });

                    });

                }
                $scope.lms_redirct(str1);
                window.localStorage.removeItem("ReportProvNumber");

            }

            $scope.$on('circular-created')
            {
                $http.get(ENV.apiUrl + "api/common/Circular/getUserDetails?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.circularobj = res.data;
                    $scope.newsobj = res.data;
                });
            }
            $rootScope.$on('circular-created')
            {
                $http.get(ENV.apiUrl + "api/common/Circular/getUserDetails?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.circularobj = res.data;
                    $scope.newsobj = res.data;
                });
            }

            $scope.timeInMs = 5000;

            var countUp = function () {
                $http.get(ENV.apiUrl + "api/common/Circular/getUserDetails?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.circularobj = res.data;
                    $scope.newsobj = res.data;
                });
            }

            $scope.$on('module-chage', function () {
                console.log('New Info');
                console.log($rootScope.module);
                $http.get(ENV.apiUrl + "api/common/GetMenu?user_code=" + user + "&module_code=" + $rootScope.module.module_code).then(function (menuData) {
                    $scope.menuData = menuData.data;
                    $scope.tags = $scope.menuData[0];
                    $scope.applications = $scope.menuData[1];
                });
            })

            function callAtInterval() {
                $http.get(ENV.apiUrl + "api/common/Circular/getUserDetails?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.circularobj = res.data;
                    $scope.newsobj = res.data;
                });

            }



            $scope.condenseMenu = function () {
                if ($rootScope.isCondensed) {
                    $('body').removeClass('grey condense-menu');
                    $('#main-menu').removeClass('mini');
                    $('.page-content').removeClass('condensed');
                    $rootScope.isCondensed = false;
                } else {
                    $('body').addClass('grey condense-menu');
                    $('#main-menu').addClass('mini');
                    $('.page-content').addClass('condensed');
                    $rootScope.isCondensed = true;
                }
            };

            if (!$rootScope.globals)
                $rootScope.globals = {};
            $rootScope.globals.studentsLoaded = false;


            //For Circular Count
            $http.get(ENV.apiUrl + "api/common/Circular/getUserDetails?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.circularobj = res.data;
                console.log($scope.circularobj.userCircularCount);
            });

            //For News Count
            $http.get(ENV.apiUrl + "api/common/News/getUserDetails?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.newsobj = res.data;
                console.log($scope.newsobj.userNewsCount);
            });

            $scope.RedirectPage = function () {
                //console.log("main.cir");GetUserNews
                $state.go("main.cir");
                $scope.show_module_brdcrumb('Circular');
                $scope.hide_folat_btn = false;
            }

            $scope.RedirectNewsPage = function () {
                //console.log("main.news");
                $state.go("main.news");
                $scope.show_module_brdcrumb('News');
                $scope.hide_folat_btn = false;

            }

            $http.get(ENV.apiUrl + "api/common/getSchoolDetailsNew").then(function (res) {
                $scope.schoolDetails_data = angular.copy(res.data);
                $scope.obj1 = res.data;
                $scope.SchoolLogo = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/SchoolLogo/';

                $rootScope.globals.currentSchool = $scope.obj1;
                $rootScope.globals.currentSchool.lic_website_url = $scope.obj1.lic_website_url;
            });

            $scope.Click = function (str) {
                console.log(str);

                $scope.new_std = str;
            }
            $scope.microsoftLogout = function () {
            $http.get(ENV.apiUrl + "api/common/getSchoolDetailsNew").then(function (res) {
                $scope.schoolDetails_data = angular.copy(res.data);
                $scope.obj1 = res.data;
                $scope.SchoolLogo = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/SchoolLogo/';

                $rootScope.globals.currentSchool = $scope.obj1;
                $rootScope.globals.currentSchool.lic_website_url = $scope.obj1.lic_website_url;

                window.config = {
                    tenant: $scope.schoolDetails_data.lic_school_microsoft_tenant_id,//'3fa94ae1-2da6-47b4-b2db-852e0d4afa73',
                    clientId: $scope.schoolDetails_data.lic_school_microsoft_client_id //'2204eefd-41dd-4473-819d-bdb132509c57'
                };
                var authContext = new AuthenticationContext(config);


                    authContext.logOut();


                console.log(authContext);
                // Check For & Handle Redirect From AAD After Login
                var isCallback = authContext.isCallback(window.location.hash);
                authContext.handleWindowCallback();

                if (isCallback && !authContext.getLoginError()) {
                    window.location = authContext._getItem(authContext.CONSTANTS.STORAGE.LOGIN_REQUEST);
                    console.log(window.location);
                }
                

                // microsoft



                var msalconfig = {

                    //clientID: "487343a6-31f8-413d-b82c-a97a2aed0348",
                    //redirectUri: "https://test.mograsys.com/login"

                    clientID: $scope.schoolDetails_data.lic_school_microsoft_client_id,  //"e623a34d-4e13-4f1f-a112-dde80b6abf84",
                    redirectUri: $scope.schoolDetails_data.lic_school_microsoft_redirect_url//"http://" + $http.defaults.headers.common['schoolId'] + ".mograsys.com/login"

                    //  redirectUri: "http://localhost:8081/SIMSWeb/login"
                };


                // Graph API endpoint to show user profile
                var graphApiEndpoint = "https://graph.microsoft.com/v1.0/me";

                // Graph API scope used to obtain the access token to read user profile
                var graphAPIScopes = ["https://graph.microsoft.com/user.read"];

                // Initialize application
                var userAgentApplication = new Msal.UserAgentApplication(msalconfig.clientID, null, loginCallback, {
                    redirectUri: msalconfig.redirectUri
                });

                //Previous version of msal uses redirect url via a property
                if (userAgentApplication.redirectUri) {
                    userAgentApplication.redirectUri = msalconfig.redirectUri;
                }

                window.onload = function () {
                    // If page is refreshed, continue to display user info
                    if (!userAgentApplication.isCallback(window.location.hash) && window.parent === window && !window.opener) {
                        var user = userAgentApplication.getUser();
                        if (user) {
                            callGraphApi();
                        }
                    }
                }

                $scope.te_graph = function () {


                    var user = userAgentApplication.getUser();
                    if (!user) {
                        // If user is not signed in, then prompt user to sign in via loginRedirect.
                        // This will redirect user to the Azure Active Directory v2 Endpoint
                        userAgentApplication.loginRedirect(graphAPIScopes);
                        // The call to loginRedirect above frontloads the consent to query Graph API during the sign-in.
                        // If you want to use dynamic consent, just remove the graphAPIScopes from loginRedirect call.
                        // As such, user will be prompted to give consent when requested access to a resource that 
                        // he/she hasn't consented before. In the case of this application - 
                        // the first time the Graph API call to obtain user's profile is executed.
                    } else {
                        // If user is already signed in, display the user info



                        var userInfoElement = document.getElementById("userInfo");
                        userInfoElement.parentElement.classList.remove("hidden");
                        userInfoElement.innerHTML = JSON.stringify(user, null, 4);

                        // Show Sign-Out button
                        document.getElementById("signOutButton").classList.remove("hidden");

                        // Now Call Graph API to show the user profile information:
                        var graphCallResponseElement = document.getElementById("graphResponse");
                        graphCallResponseElement.parentElement.classList.remove("hidden");
                        graphCallResponseElement.innerText = "Calling Graph ...";

                        // In order to call the Graph API, an access token needs to be acquired.
                        // Try to acquire the token used to query Graph API silently first:
                        userAgentApplication.acquireTokenSilent(graphAPIScopes)
                            .then(function (token) {
                                //After the access token is acquired, call the Web API, sending the acquired token
                                callWebApiWithToken(graphApiEndpoint, token, graphCallResponseElement, document.getElementById("accessToken"));

                            }, function (error) {
                                // If the acquireTokenSilent() method fails, then acquire the token interactively via acquireTokenRedirect().
                                // In this case, the browser will redirect user back to the Azure Active Directory v2 Endpoint so the user 
                                // can reenter the current username/ password and/ or give consent to new permissions your application is requesting.
                                // After authentication/ authorization completes, this page will be reloaded again and callGraphApi() will be executed on page load.
                                // Then, acquireTokenSilent will then get the token silently, the Graph API call results will be made and results will be displayed in the page.
                                if (error) {
                                    userAgentApplication.acquireTokenRedirect(graphAPIScopes);
                                }
                            });
                    }

                }

                /**
                 * Call the Microsoft Graph API and display the results on the page. Sign the user in if necessary
                 */
                function callGraphApi() {
                    var user = userAgentApplication.getUser();
                    $scope.user_lst = user;
                    if (!user) {
                        // If user is not signed in, then prompt user to sign in via loginRedirect.
                        // This will redirect user to the Azure Active Directory v2 Endpoint
                        userAgentApplication.loginRedirect(graphAPIScopes);
                        // The call to loginRedirect above frontloads the consent to query Graph API during the sign-in.
                        // If you want to use dynamic consent, just remove the graphAPIScopes from loginRedirect call.
                        // As such, user will be prompted to give consent when requested access to a resource that 
                        // he/she hasn't consented before. In the case of this application - 
                        // the first time the Graph API call to obtain user's profile is executed.
                    } else {
                        // If user is already signed in, display the user info
                        var userInfoElement = document.getElementById("userInfo");
                        userInfoElement.parentElement.classList.remove("hidden");

                        userInfoElement.innerHTML = JSON.stringify(user, null, 4);

                        // Show Sign-Out button
                        document.getElementById("signOutButton").classList.remove("hidden");

                        // Now Call Graph API to show the user profile information:
                        var graphCallResponseElement = document.getElementById("graphResponse");
                        graphCallResponseElement.parentElement.classList.remove("hidden");
                        graphCallResponseElement.innerText = "Calling Graph ...";

                        // In order to call the Graph API, an access token needs to be acquired.
                        // Try to acquire the token used to query Graph API silently first:
                        userAgentApplication.acquireTokenSilent(graphAPIScopes)
                            .then(function (token) {
                                //After the access token is acquired, call the Web API, sending the acquired token
                                callWebApiWithToken(graphApiEndpoint, token, graphCallResponseElement, document.getElementById("accessToken"));

                            }, function (error) {
                                // If the acquireTokenSilent() method fails, then acquire the token interactively via acquireTokenRedirect().
                                // In this case, the browser will redirect user back to the Azure Active Directory v2 Endpoint so the user 
                                // can reenter the current username/ password and/ or give consent to new permissions your application is requesting.
                                // After authentication/ authorization completes, this page will be reloaded again and callGraphApi() will be executed on page load.
                                // Then, acquireTokenSilent will then get the token silently, the Graph API call results will be made and results will be displayed in the page.
                                if (error) {
                                    userAgentApplication.acquireTokenRedirect(graphAPIScopes);
                                }
                            });
                    }
                }

                /**
                 * Callback method from sign-in: if no errors, call callGraphApi() to show results.
                 * @param {string} errorDesc - If error occur, the error message
                 * @param {object} token - The token received from login
                 * @param {object} error - The error string
                 * @param {string} tokenType - The token type: For loginRedirect, tokenType = "id_token". For acquireTokenRedirect, tokenType:"access_token".
                 */
                function loginCallback(errorDesc, token, error, tokenType) {
                    if (errorDesc) {
                        showError(msal.authority, error, errorDesc);
                    } else {
                        callGraphApi();
                    }
                }

                /**
                 * Show an error message in the page
                 * @param {string} endpoint - the endpoint used for the error message
                 * @param {string} error - Error string
                 * @param {string} errorDesc - Error description
                 */
                function showError(endpoint, error, errorDesc) {
                    var formattedError = JSON.stringify(error, null, 4);
                    if (formattedError.length < 3) {
                        formattedError = error;
                    }
                    document.getElementById("errorMessage").innerHTML = "An error has occurred:<br/>Endpoint: " + endpoint + "<br/>Error: " + formattedError + "<br/>" + errorDesc;
                    console.error(error);
                }

                /**
                 * Call a Web API using an access token.
                 * @param {any} endpoint - Web API endpoint
                 * @param {any} token - Access token
                 * @param {object} responseElement - HTML element used to display the results
                 * @param {object} showTokenElement = HTML element used to display the RAW access token
                 */
                function callWebApiWithToken(endpoint, token, responseElement, showTokenElement) {
                    var headers = new Headers();
                    var bearer = "Bearer " + token;
                    headers.append("Authorization", bearer);
                    var options = {
                        method: "GET",
                        headers: headers
                    };

                    fetch(endpoint, options)
                        .then(function (response) {
                            var contentType = response.headers.get("content-type");
                            if (response.status === 200 && contentType && contentType.indexOf("application/json") !== -1) {
                                response.json()
                                    .then(function (data) {
                                        // Display response in the page
                                        console.log(data);
                                        console.log($scope.user_lst);

                                        $http.get(ENV.apiUrl + "api/common/getuserpass?email_id=" + data.userPrincipalName).then(function (res) {
                                            console.log(res.data);
                                            if (res.data == '') {
                                                swal({ title: "Alert", text: "Your email Id is not found please contact to administrator", imageUrl: "assets/img/notification-alert.png", });
                                            }
                                            else {
                                                if (res.data.length == 1) {
                                                    // $scope.username =res.data[0].comn_user_name
                                                    //$scope.password = res.data[0].comn_user_password
                                                    //$scope.Login_microsoft(res.data[0].comn_user_name, res.data[0].comn_user_password);
                                                }
                                            }

                                        });


                                        responseElement.innerHTML = JSON.stringify(data, null, 4);
                                        if (showTokenElement) {
                                            showTokenElement.parentElement.classList.remove("hidden");
                                            showTokenElement.innerHTML = token;
                                        }
                                    })
                                    .catch(function (error) {
                                        showError(endpoint, error);
                                    });
                            } else {
                                response.json()
                                    .then(function (data) {
                                        // Display response as error in the page
                                        showError(endpoint, data);
                                    })
                                    .catch(function (error) {
                                        showError(endpoint, error);
                                    });
                            }
                        })
                        .catch(function (error) {
                            showError(endpoint, error);
                        });
                }


                /**
                 * Sign-out the user
                 */
                function signOut() {
                    userAgentApplication.logout();
                }

                $scope.signOutnew = function () {
                    userAgentApplication.logout();

                }
            });

            }




            console.log($scope.uname = $rootScope.globals.currentUser.username);



            $scope.arr = function () {

                $('.horizontal-menu .bar-inner > ul > li').on('click', function () {
                    $(this).toggleClass('open').siblings().removeClass('open');

                });
                $('.content').on('click', function () {

                    $('.horizontal-menu .bar-inner > ul > li').removeClass('open');
                });
            };

            $scope.menu_list_click = function (mod_nm, mod_code, appl_name, appl_code,appl_Ar) {
                if (mod_nm != '')
                    $scope.brd['brd_module_nm'] = mod_nm;
                $scope.brd['brd_module_code'] = mod_code;
                $scope.brd['brd_application_code'] = appl_code;
                $scope.brd['brd_application_nm'] = appl_name;
                $scope.brd['brd_application_nm_ar'] = appl_Ar;

            }

            $scope.load_page = function (path, obj, mod_code, modobj) {

                $scope.ar_en_fun(path);
                $scope.check_favorite(path);

                if (obj.comn_appl_type == 'R') {

                    $scope.location = obj.comn_appl_location;
                    $rootScope.location = $scope.location;
                    $rootScope.main_appl_code = path;
                    setTimeout(function () { $state.go('main.ReportCard'); }, 100);
                  //  if ($scope.lang == 'en') {
                        $scope.brd['brd_module_nm'] = modobj.module_name_en;
                        $scope.brd['brd_application_nm'] = obj.comn_appl_name_en;
                  //  }
                   // else {
                        $scope.brd['brd_module_nm_ar'] = modobj.module_name_ar;
                        $scope.brd['brd_application_nm_ar'] = obj.comn_appl_name_ar;
                  //  }


                    $scope.brd['brd_module_code'] = mod_code;
                    $scope.brd['brd_application_code'] = path;


                    $state.go('main');
                }

                else {


                    if ($scope.fin == undefined) {
                        if (mod_code == '005') {
                            $scope.main_loader_show = true;
                            $http.get(ENV.apiUrl + "api/PDCSubmission/GetComp_Name?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                                $scope.cmbCompany = res.data;
                                if (res.data.length == 1) {
                                    $scope.companyName = res.data[0].comp_name;

                                    $scope.finnComp = res.data[0].comp_code;
                                    $scope.comp_short_name = res.data[0].comp_short_name;
                                    $scope.input_vat_acct = res.data[0].comp_input_vat_posting_account_code;
                                    $scope.comp_vat_status = res.data[0].include_vat_enable_status;
                                    $scope.comp_vat_per = res.data[0].comp_vat_per;


                                    $http.get(ENV.apiUrl + "api/PDCSubmission/GetFinancial_year?comp_code=" + $scope.finnComp + "&username=" + $rootScope.globals.currentUser.username).then(function (res) {

                                        if (res.data.length > 0) {

                                            console.log('Finn');

                                            var data = {
                                                year: res.data[0].financial_year,
                                                company: $scope.finnComp,
                                                companyame: $scope.companyName,
                                                comp_short_name: $scope.comp_short_name,
                                                input_vat_acct: $scope.input_vat_acct,
                                                comp_vat_status: $scope.comp_vat_status,
                                                comp_vat_per: $scope.comp_vat_per
                                            }
                                            window.localStorage["Finn_comp"] = JSON.stringify(data)

                                            $state.reload();

                                            setTimeout(function () {
                                                $scope.main_loader_show = false;
                                            }, 500);
                                        }
                                    });


                                }
                                else if (res.data.length > 1) {
                                    $scope.financeWindow();
                                }


                            });
                        }
                    }

                    $scope.lms_redirct(path);


                    var state = 'main.' + path;

                    $state.go(state);

                  //  if ($scope.lang == 'en') {
                        $scope.brd['brd_module_nm'] = modobj.module_name_en;
                        $scope.brd['brd_application_nm'] = obj.comn_appl_name_en;
                    //}
                    //else {
                        $scope.brd['brd_module_nm_ar'] = modobj.module_name_ar;
                        $scope.brd['brd_application_nm_ar'] = obj.comn_appl_name_ar;
                    //}
                    $scope.brd['brd_module_code'] = mod_code;
                    $scope.brd['brd_application_code'] = path;


                }




                if ($scope.oldobj == undefined) {


                    $scope.oldobj = {
                        opr: 'I',
                        comn_audit_user_code: $rootScope.globals.currentUser.username,
                        comn_appl_name_en: path,
                        comn_audit_start_time: $scope.getDateTime(),
                        comn_audit_ip: $rootScope.ip_add,
                        comn_audit_dns: $rootScope.dns,
                        //comn_audit_end_time: '',
                    }
                    $http.post(ENV.apiUrl + "api/common/InsertUserAudit", $scope.oldobj).then(function (res) {

                    });
                }
                else {

                    var edate = $scope.getDateTime()
                    var data = {
                        opr: 'I',
                        comn_audit_user_code: $rootScope.globals.currentUser.username,
                        comn_appl_name_en: path,
                        comn_audit_start_time: edate,
                        comn_audit_ip: $rootScope.ip_add,
                        comn_audit_dns: $rootScope.dns,
                    }

                    $http.post(ENV.apiUrl + "api/common/InsertUserAudit", data).then(function (res) {


                        $scope.oldobj['comn_audit_end_time'] = edate
                        $scope.oldobj['opr'] = 'J'

                        $http.post(ENV.apiUrl + "api/common/InsertUserAudit", $scope.oldobj).then(function (res) {

                            $scope.oldobj['comn_audit_start_time'] = edate
                            $scope.oldobj['comn_appl_name_en'] = path

                        });

                    });

                }

                window.localStorage.removeItem("ReportProvNumber");

                $("#Applsource").select2("val", "null");
                $scope.appl_Code = '';


            }


            $scope.parentDetailsClick = function (enrollno, parentid) {

                $http.get(ENV.apiUrl + "api/StudetProfile/getParentDetails?enroll_no=" + enrollno).then(function (ParentDetails) {
                    $scope.Parent_Details = ParentDetails.data;
                    $scope.parent_info = $scope.Parent_Details[0];

                    $scope.student_father_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.parent_info.sims_admisison_father_image;
                    $scope.student_mother_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.parent_info.sims_admisison_mother_image;
                    $scope.student_guardian_image1 = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.parent_info.sims_admisison_guardian_image;
                    $scope.p = {
                        parentImage: $scope.student_father_image,
                    }
                    $scope.getstatefather($scope.parent_info.sims_admission_father_country_code);
                    $scope.getcityname($scope.parent_info.sims_admission_father_state);

                    $scope.getmothercityname($scope.parent_info.sims_admission_mother_state);
                    $scope.getstatemother($scope.parent_info.sims_admission_mother_country_code);

                    $scope.getguardiancityname($scope.parent_info.sims_admission_guardian_state);
                    $scope.getstateguardian($scope.parent_info.sims_admission_guardian_country_code);
                    $scope.isparentschoolemployee($scope.parent_info.sims_parent_is_employment_status);




                });

                $http.get(ENV.apiUrl + "api/StudetProfile/getStudentList?parent_no=" + parentid).then(function (Childresult) {
                    $scope.ChildDetails = Childresult.data;
                    if ($scope.ChildDetails.length > 0) {
                        $scope.ChildDetailsshow = true;
                        $scope.ChildDetailshide = false;
                    }
                    else {
                        $scope.ChildDetailsshow = false;
                        $scope.ChildDetailshide = true;
                    }
                });
                $scope.CurrentParentImage = function (parentimg) {
                    $scope.p.parentImage = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + parentimg;

                }
                $('#ParentProfileModal').modal('show');

            }

            $scope.language = function (str) {
            console.log(str);
            
                var lang;
                if (str == 'en') {

                    console.log("English");
                    lang = "en";
                    $scope.lang = "en";
                    $rootScope.langDirection = lang == "ar" ? "rtl" : "ltr";
                    gettextCatalog.setCurrentLanguage("en");
                    $rootScope.$broadcast('lagnuage_chaged', "en");
                    //$http.get(ENV.apiUrl + "api/common/Get_Application?s=" + $rootScope.globals.currentUser.username).then(function (res) {
                    //    $scope.appl_items = res.data;

                    //});


                    $scope.display = true;



                    //$http.post(ENV.apiUrl + "api/common/updateLanguage?lang=" + str + "&uname=" + $rootScope.globals.currentUser.username).then(function (res) {

                    //});

                }
                    //else if (str == 'et') {
                    //    lang = "ar";
                    //    $rootScope.locale = $rootScope.locales["ar"];

                    //    gettextCatalog.setCurrentLanguage("ar");
                    //    $rootScope.$broadcast('lagnuage_chaged', "ar");

                    //}

                else if (str == 'ar') {
                    lang = "ar";
                    $scope.lang = "ar";
                    $rootScope.locale = $rootScope.locales["ar"];
                    $rootScope.langDirection = lang == "ar" ? "rtl" : "ltr";
                    gettextCatalog.setCurrentLanguage("ar");
                    $rootScope.$broadcast('lagnuage_chaged', "ar");
                    $scope.arabic_chat = { "display": "none" }






                    //$http.post(ENV.apiUrl + "api/common/updateLanguage?lang=" + str + "&uname=" + $rootScope.globals.currentUser.username).then(function (res) {

                    //});


                }

            }

            //temprarory Commented

            //$http.get(ENV.apiUrl + "api/common/getLanguge?uname=" + $rootScope.globals.currentUser.username).then(function (res) {

            //    if (res.data != '') {
            //        $scope.lang = res.data;
            //        $scope.language(res.data);
            //    }
            //    else {
            //        $scope.lang = "en";
            //        $scope.language("en");
            //    }
            //});



            //if ($http.defaults.headers.common['schoolId'] == 'siscollege')
            //{
            //    $scope.language('et');
            //}

            $scope.itemsPerPage = 25;
            $scope.currentPage = 0;
            $scope.global_search_result = [];
            $scope.perPageRecords = [];


            $scope.startPage = function () {
                $scope.currentPage = 0;
            }

            $scope.lastPage = function () {
                $scope.currentPage = $scope.pageCount();
            }

            $scope.range = function () {
                var rangeSize = 15;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;


            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
                $scope.selectedAll = false;
                angular.forEach($scope.perPageRecords, function (info, key) {
                    info.user_chk = false;
                });
                $scope.singlePage = $scope.currentPage + 1;
                var begin = (($scope.singlePage - 1) * $scope.itemsPerPage);
                var end = parseInt(begin) + parseInt($scope.itemsPerPage);
                $scope.perPageRecords = $scope.global_search_result.slice(begin, end);
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.global_search_result.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;

                // to select single page records
                $scope.selectedAll = false;
                //angular.forEach($scope.perPageRecords, function (info, key) {
                //    info.user_chk = false;
                //});
                $scope.singlePage = n + 1;
                var begin = (($scope.singlePage - 1) * $scope.itemsPerPage);
                var end = parseInt(begin) + parseInt($scope.itemsPerPage);
                $scope.perPageRecords = $scope.global_search_result.slice(begin, end);

            };

            $scope.getPerPageRecords = function () {
                $scope.singlePage = 1;
                var begin = (($scope.singlePage - 1) * $scope.itemsPerPage);
                var end = parseInt(begin) + parseInt($scope.itemsPerPage);
                $scope.perPageRecords = $scope.global_search_result.slice(begin, end);
            }




            function getCur(flag, comp_code) {
                if (flag) {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        $scope.cmbCur = res.data;
                        if ($scope.cmbCur.length > 0) {
                            $scope.global_Search['global_curriculum_code'] = $scope.cmbCur[0].sims_cur_code;
                            $scope.getAcadm_yr($scope.cmbCur[0].sims_cur_code)

                        }

                    });
                }
                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        $scope.cmbCur = res.data;
                        if ($scope.cmbCur.length > 0) {
                            $scope.global_Search['global_curriculum_code'] = $scope.cmbCur[0].sims_cur_code;
                            $scope.getAcadm_yr($scope.cmbCur[0].sims_cur_code)

                        }

                    });
                }
            }






            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;

                console.log($scope.ComboBoxValues);
            });

            $http.get(ENV.apiUrl + "api/EmployeeReport/GetDepartmentName").then(function (res) {
                $scope.cmbDepartment = res.data;
            })


            $http.get(ENV.apiUrl + "api/EmployeeReport/GetCompanyNameForShift").then(function (res) {
                //   $scope.cmbCompany = res.data[0];

                if (res.data.length > 0) {
                    $http.get(ENV.apiUrl + "api/EmployeeReport/GetDesignation?com_code=" + res.data[0].sh_company_code).then(function (res) {
                        $scope.cmbDesignation = res.data;
                    })
                }
            })


            $http.get(ENV.apiUrl + "api/common/getVehicleCode").then(function (get_VehicleCode) {
                $scope.VehicleCode = get_VehicleCode.data;
            })


            $scope.academic_change = function (acd) {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.global_Search.global_curriculum_code + "&academic_year=" + acd).then(function (res) {
                    $scope.All_grade_names = res.data;
                })
            }

            $scope.getallsectioncode = function (gr_code) {

                $http.get(ENV.apiUrl + "api/common/SectionSubject/getSectionNames?curcode=" + $scope.global_Search.global_curriculum_code + "&academicyear=" + $scope.global_Search.global_acdm_yr + "&gradecode=" + gr_code).then(function (res) {
                    $scope.All_Section_names = res.data;
                    $('#example-enableFiltering-includeSelectAllOption').multiselect({
                        includeSelectAllOption: true,
                        enableFiltering: true
                    });
                });
            }

            $scope.getAcadm_yr = function (cur_code) {
                $scope.curriculum_code = cur_code;
                $http.get(ENV.apiUrl + "api/calendarexception/getAttendanceyear?cur_code=" + cur_code).then(function (res) {
                    $scope.Acdm_year = res.data; console.log($scope.Acdm_year);

                    if ($scope.Acdm_year.length > 0) {
                        $scope.global_Search['global_acdm_yr'] = res.data[0].sims_academic_year;
                        $scope.academic_change(res.data[0].sims_academic_year);


                    }
                });
            }

            $scope.modal_cancel = function () {
                //  $scope.All_grade_names = '';
                //$scope.All_Nationality_names = '';
                //  $scope.Cur_Names = '';
                $scope.glbl_obj = {};
                $scope.global_Search = {};
                $scope.global_search_result = [];
                $scope.gbl_tab = '';
                $scope.global_Search_show = false;
                $scope.$broadcast('global_cancel', $scope.SelectedUserLst);
            }


            $scope.Global_Search_modal = function () {

                $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getAllCurName").then(function (res) {
                    $scope.Cur_Names = res.data;
                });

                $('#Global_Search_Modal').modal({ backdrop: "static" });

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getAllGradeName").then(function (res) {
                    $scope.All_grade_names = res.data;

                });
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getAllNationality").then(function (res) {
                    $scope.All_Nationality_names = res.data;
                    console.log(res.data)

                });

            }


            $scope.Global_Search_by_Uassignedstudent = function () {

                $scope.global_Uassigned_student_table = false;
                $scope.busy = true;
                if ($scope.global_Search == "") {
                    $scope.glbl_obj = $scope.global_Search;
                }
                else {
                    $scope.glbl_obj = {
                        s_cur_code: $scope.global_Search.global_curriculum_code,
                        search_std_enroll_no: $scope.global_Search.glbl_enroll_num,
                        search_std_name: $scope.global_Search.glbl_name,
                        search_std_grade_name: $scope.global_Search.gradecode,
                        search_std_section_name: $scope.global_Search.sectioncode,
                        search_std_passport_no: $scope.global_Search.glbl_pass_num,
                        search_std_family_name: $scope.global_Search.glbl_family_name,
                        sims_academic_year: $scope.global_Search.global_acdm_yr,
                        sims_nationality_name_en: $scope.global_Search.nationalitycode,
                        std_national_id: $scope.global_Search.nationalityID
                    }
                }
                //myarray = []; $scope.global_search_result = "";

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudentUnassigned?data=" + JSON.stringify($scope.glbl_obj)).then(function (res) {
                    $scope.global_search_result = res.data;
                    console.log($scope.global_search_result);
                    $scope.busy = false;
                    if ($scope.global_search_result.length > 0) {
                        $scope.global_Uassigned_student_table = true;
                        $scope.showUnassignedStudentFilterBtn = true;
                        $scope.showUnassignedStudentFilter = true;
                    }

                    $timeout(function () {
                        $("#Table6").tableHeadFixer({ 'top': 1 });
                    }, 100);

                    if ($scope.global_search_result.length == 0) {
                        swal({ text: "No record(s) found.", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 450, height: 200, timer: 1000000 });
                    }
                });


            }

            $scope.vieprofile = function (emp) {
                
                $('#EmployeeProfileModal').modal('show');
                $http.get(ENV.apiUrl + "api/common/getEmployeeProfileView?em_login_code=" + emp.em_login_code).then(function (getEmployeeProfileView_Data) {
                    $scope.EmployeeProfileView = getEmployeeProfileView_Data.data;
                    console.log($scope.EmployeeProfileView);
                    for (var i = 0; i < $scope.EmployeeProfileView.length; i++) {
                        $scope.emp_image_profile = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/' + $scope.EmployeeProfileView[i].em_img;
                        //$scope.imageUrl = ENV.apiUrl + 'Content/sjs/Images/EmployeeImages/' + $scope.EmployeeProfileView[i].em_img;
                        // $scope.imageUrl = ENV.siteUrl + 'Images/EmployeeImages/' + $scope.EmployeeProfileView[i].em_img;
                        $scope.em_full_name = $scope.EmployeeProfileView[i].em_full_name;
                        $scope.em_name_ot = $scope.EmployeeProfileView[i].em_name_ot;
                        $scope.em_family_name = $scope.EmployeeProfileView[i].em_family_name;
                        $scope.em_sex = $scope.EmployeeProfileView[i].em_sex;
                        $scope.em_marital_status = $scope.EmployeeProfileView[i].em_marital_status;
                        $scope.em_nation_name = $scope.EmployeeProfileView[i].em_nation_name;
                        $scope.em_last_login = $scope.EmployeeProfileView[i].em_last_login;
                        $scope.em_date_of_birth = $scope.EmployeeProfileView[i].em_date_of_birth;
                        $scope.em_handicap_status = $scope.EmployeeProfileView[i].em_handicap_status;
                        $scope.em_religion_name = $scope.EmployeeProfileView[i].em_religion_name;
                        $scope.em_salutation = $scope.EmployeeProfileView[i].em_salutation;
                        $scope.em_Salutation_Code = $scope.EmployeeProfileView[i].em_Salutation_Code;


                        //Passport & Contact_Details
                        $scope.em_passport_no = $scope.EmployeeProfileView[i].em_passport_no;
                        $scope.em_passport_issue_date = $scope.EmployeeProfileView[i].em_passport_issue_date;
                        $scope.em_passport_exp_date = $scope.EmployeeProfileView[i].em_passport_exp_date;
                        $scope.em_passport_issuing_authority = $scope.EmployeeProfileView[i].em_passport_issuing_authority;

                        $scope.em_emergency_contact_name1 = $scope.EmployeeProfileView[i].em_emergency_contact_name1;
                        $scope.em_emergency_contact_number1 = $scope.EmployeeProfileView[i].em_emergency_contact_number1;
                        $scope.em_emergency_contact_name2 = $scope.EmployeeProfileView[i].em_emergency_contact_name2;
                        $scope.em_emergency_contact_number2 = $scope.EmployeeProfileView[i].em_emergency_contact_number2;

                        //Visa & NationalID_Details
                        $scope.em_visa_no = $scope.EmployeeProfileView[i].em_visa_no;
                        $scope.em_visa_type = $scope.EmployeeProfileView[i].em_visa_type;
                        $scope.em_visa_issuing_authority = $scope.EmployeeProfileView[i].em_visa_issuing_authority;
                        $scope.em_visa_issuing_place = $scope.EmployeeProfileView[i].em_visa_issuing_place;
                        $scope.em_visa_issue_date = $scope.EmployeeProfileView[i].em_visa_issue_date;
                        $scope.em_visa_exp_date = $scope.EmployeeProfileView[i].em_visa_exp_date;

                        $scope.em_national_id = $scope.EmployeeProfileView[i].em_national_id;
                        $scope.em_national_id_issue_date = $scope.EmployeeProfileView[i].em_national_id_issue_date;
                        $scope.em_national_id_expiry_date = $scope.EmployeeProfileView[i].em_national_id_expiry_date;

                        //Company_Details
                        $scope.em_company_name = $scope.EmployeeProfileView[i].em_company_name;
                        $scope.em_dept_name = $scope.EmployeeProfileView[i].em_dept_name;
                        $scope.em_desg_name = $scope.EmployeeProfileView[i].em_desg_name;
                        $scope.em_joining_ref = $scope.EmployeeProfileView[i].em_joining_ref;
                        $scope.em_grade_name = $scope.EmployeeProfileView[i].em_grade_name;
                        $scope.em_grade_effect_from = $scope.EmployeeProfileView[i].em_grade_effect_from;
                        $scope.em_date_of_join = $scope.EmployeeProfileView[i].em_date_of_join;
                        $scope.em_agreement_start_date = $scope.EmployeeProfileView[i].em_agreement_start_date;
                        $scope.em_agreement_exp_date = $scope.EmployeeProfileView[i].em_agreement_exp_date;
                        $scope.em_punching_id = $scope.EmployeeProfileView[i].em_punching_id;
                        $scope.em_staff_type = $scope.EmployeeProfileView[i].em_staff_type;
                        $scope.em_service_status = $scope.EmployeeProfileView[i].em_service_status;

                        //Bank_Details
                        $scope.em_bank_name = $scope.EmployeeProfileView[i].em_bank_name;
                        $scope.em_gpf_ac_no = $scope.EmployeeProfileView[i].em_gpf_ac_no;
                        $scope.em_gosi_ac_no = $scope.EmployeeProfileView[i].em_gosi_ac_no;
                        $scope.em_labour_card_no = $scope.EmployeeProfileView[i].em_labour_card_no;
                        $scope.em_bank_ac_no = $scope.EmployeeProfileView[i].em_bank_ac_no;
                        $scope.em_bank_swift_code = $scope.EmployeeProfileView[i].em_bank_swift_code;
                        $scope.em_gosi_start_date = $scope.EmployeeProfileView[i].em_gosi_start_date;
                        $scope.em_iban_no = $scope.EmployeeProfileView[i].em_iban_no;


                        $scope.em_summary_address = $scope.EmployeeProfileView[i].em_summary_address;
                        $scope.em_country = $scope.EmployeeProfileView[i].em_country;
                        $scope.em_state = $scope.EmployeeProfileView[i].em_state;
                        $scope.em_city = $scope.EmployeeProfileView[i].em_city;
                        $scope.em_po_box = $scope.EmployeeProfileView[i].em_po_box;
                        $scope.em_phone = $scope.EmployeeProfileView[i].em_phone;
                        $scope.em_mobile = $scope.EmployeeProfileView[i].em_mobile;
                        $scope.em_iban_no = $scope.EmployeeProfileView[i].em_iban_no;
                        $scope.em_email = $scope.EmployeeProfileView[i].em_email;

                    }
                });

                $http.get(ENV.apiUrl + "api/common/getEmployeeAttendanceDetails?em_login_code=" + emp.em_login_code).then(function (getEmployeeAttendanceDetails_Data) {
                    $scope.EmployeeAttendanceDetails = getEmployeeAttendanceDetails_Data.data;
                    console.log($scope.EmployeeAttendanceDetails);

                    if ($scope.EmployeeAttendanceDetails < 1) {
                        $scope.NodataAttendance = true;

                    }
                    else {

                        $scope.AttendanceData = true;
                    }

                });


                $http.get(ENV.apiUrl + "api/common/getEmployeeLeavesDetails?el_number=" + emp.em_login_code).then(function (getEmployeeLeavesDetails_Data) {
                    $scope.EmployeeLeavesDetails = getEmployeeLeavesDetails_Data.data;
                    console.log($scope.EmployeeLeavesDetails);

                    if ($scope.EmployeeLeavesDetails < 1) {
                        $scope.NodataLeave = true;

                    }
                    else {

                        $scope.LeaveData = true;
                    }
                });

                $http.get(ENV.apiUrl + "api/common/getEmployeeClassDetails?em_login_code=" + emp.em_login_code).then(function (getEmployeeClassDetails_Data) {
                    $scope.EmployeeClassDetails = getEmployeeClassDetails_Data.data;
                    console.log($scope.EmployeeClassDetails);

                    if ($scope.EmployeeClassDetails < 1) {
                        $scope.NodataClass = true;

                    }
                    else {

                        $scope.ClassData = true;
                    }
                });
            }


            $(function () {
                $('#cmb_gradeparent').multipleSelect({
                    width: '100%',
                    //placeholder: "Select Grade"
                });

                $('#cmb_sectionparent').multipleSelect({
                    width: '100%',
                    //placeholder: "Select Section"
                });


            });

            $http.get(ENV.apiUrl + "api/common/GlobalSearch/getAllGradeName").then(function (res) {
                $scope.All_grade_nameParent = res.data;

                setTimeout(function () {
                    $('#cmb_gradeparent').change(function () {
                        console.log($(this).val());
                        //section = $(this).val();
                    }).multipleSelect({
                        multiple: true,
                        width: '100%'
                    });
                    //$("#cmb_section").multipleSelect("checkAll");
                }, 1000);



            });


            $scope.getallsectioncodeparent = function (str) {

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/Get_Section_CodebyAll?grade_code=" + str).then(function (res) {
                    $scope.All_Section_namesParent = res.data;

                    setTimeout(function () {
                        $('#cmb_sectionparent').change(function () {
                            console.log($(this).val());
                            //section = $(this).val();
                        }).multipleSelect({
                            multiple: true,
                            width: '100%'
                        });
                        //$("#cmb_section").multipleSelect("checkAll");
                    }, 1000);

                });
            }





            $scope.Global_Search_by_parent_unassigned = function () {
               
                $scope.global_unassigned_parent_table = false;
                $scope.busy = true;

                $scope.searchbyparent = {
                    search_std_grade_name: $scope.global_Search.gradecode2,
                    search_std_section_name: $scope.global_Search.sectioncode2,
                    search_parent_id: $scope.global_Search.search_parent_id,
                    search_parent_name: $scope.global_Search.search_parent_name,
                    search_parent_email_id: $scope.global_Search.search_parent_email_id,
                    search_parent_mobile_no: $scope.global_Search.search_parent_mobile_no,
                    search_parent_mother_name: $scope.global_Search.search_parent_mother_name
                }

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchParentUassigned?data=" + JSON.stringify($scope.searchbyparent)).then(function (res) {
                    $scope.global_search_result = res.data;
                    $scope.busy = false;
                    if ($scope.global_search_result.length > 0) {
                        $scope.global_unassigned_parent_table = true;
                        $scope.showUnassignedParentFilterBtn = true;
                        $scope.showUnassignedParentFilter = true;
                    }
                    if ($scope.global_search_result.length == 0) {
                        swal({ text: "No record(s) found.", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 450, height: 200, timer: 1000000 });
                    }

                    $timeout(function () {
                        $("#Table7").tableHeadFixer({ 'top': 1 });
                    }, 100);

                    console.log($scope.global_search_result);
                });
            }

            $scope.Global_Search_by_student_filter = function () {
                $scope.showStudentFilterBtn = false;
                $scope.showStudentFilter = false;
                $scope.global_student_table = false;
                $scope.global_search_result = [];
                $scope.searchTextStudent = '';

            }

            $scope.Global_Search_by_parent_filter = function () {
                $scope.showParentFilterBtn = false;
                $scope.showParentFilter = false;
                $scope.global_parent_table = false;
                $scope.global_search_result = [];
            }

            $scope.Global_Search_by_teacher_filter = function () {
                $scope.showTeacherFilterBtn = false;
                $scope.showTeacherFilter = false;
                $scope.global_teacher_table = false;
                $scope.global_search_result = [];
            }

            $scope.Global_Search_by_employee_filter = function () {
                $scope.showEmployeeFilterBtn = false;
                $scope.showEmployeeFilter = false;
                $scope.EmployeeTable = false;
                $scope.global_search_result = [];
            }
            $scope.Global_Search_by_Uassignedstudent_Filter = function () {
                $scope.showUnassignedStudentFilterBtn = false;
                $scope.showUnassignedStudentFilter = false;
                $scope.global_Uassigned_student_table = false;
                $scope.global_search_result = [];
            }

            $scope.Global_Search_by_UassignedParent_Filter = function () {
                $scope.showUnassignedParentFilterBtn = false;
                $scope.showUnassignedParentFilter = false;
                $scope.global_unassigned_parent_table = false;
                $scope.global_search_result = [];
            }

            $scope.Global_Search_by_User_filter = function () {
                $scope.showUserFilterBtn = false;
                $scope.showUserFilter = false;
                $scope.table = false;
                $scope.global_search_result = [];
            }




            //$scope.downloaddoc = function (str) {
            //    debugger;
            //    $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/';
            //    $scope.url = $scope.imageUrl + str.sims_student_image;
            //    //$scope.url = 'http://api.mograsys.com/APIERP/Content/adis/Images/StudentImages/' + str.sims_student_image;
            //    window.open($scope.url, '_new');
            //}

            $scope.Global_Search_by_student = function () {
               
                $scope.global_student_table = false;
                $scope.busy = true;
                $scope.paging = true;
                if ($scope.global_Search == "") {
                    $scope.glbl_obj = $scope.global_Search;
                }
                else {
                    $scope.glbl_obj = {
                        s_cur_code: $scope.global_Search.global_curriculum_code,
                        search_std_enroll_no: $scope.global_Search.glbl_enroll_num,
                        search_std_name: $scope.global_Search.glbl_name,
                        search_std_middle_name: $scope.global_Search.glbl_middle_name,
                        search_std_last_name: $scope.global_Search.glbl_last_name,
                        search_std_grade_name: $scope.global_Search.gradecode,
                        search_std_section_name: $scope.global_Search.sectioncode,
                        search_std_passport_no: $scope.global_Search.glbl_pass_num,
                        search_std_family_name: $scope.global_Search.glbl_family_name,
                        sims_academic_year: $scope.global_Search.global_acdm_yr,
                        sims_nationality_name_en: $scope.global_Search.nationalitycode,
                        std_national_id: $scope.global_Search.nationalityID,
                        search_parent_emp_id: $scope.global_Search.search_parent_emp_id,
                        sims_student_ea_number: $scope.global_Search.sims_student_ea_number,
                        search_std_transport_bus: $scope.global_Search.search_std_transport_bus,

                        search_parent_no: $scope.global_Search.glbl_parent_num
                    }
                }
                //myarray = []; $scope.global_search_result = "";

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.glbl_obj)).then(function (res) {
                    
                    $scope.global_search_result = res.data; console.log($scope.global_search_result);
                    if ($scope.global_search_result.length > 0) {
                        $scope.currentPage = 0;
                        $scope.showStudentFilterBtn = true;
                        $scope.showStudentFilter = true;
                        $scope.global_student_table = true;
                        $timeout(function () {
                            $("#Table_Fix1").tableHeadFixer({ 'top': 1 });
                        }, 100);
                    }
                    else if ($scope.global_search_result.length == 0) {
                        swal({ text: "No record(s) found.", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 450, height: 200, timer: 1000000 });

                    }


                    $scope.busy = false;


                    $scope.getPerPageRecords();
                });


            }


            $scope.Global_Search_by_parent = function () {

                $scope.global_parent_table = false;
                $scope.busy = true;
                $scope.paging = true;
                $scope.searchbyparent = {
                    search_std_grade_name: $scope.global_Search.gradecode2,
                    search_std_section_name: $scope.global_Search.sectioncode2,
                    search_parent_id: $scope.global_Search.search_parent_id,
                    search_parent_name: $scope.global_Search.search_parent_name,
                    search_parent_email_id: $scope.global_Search.search_parent_email_id,
                    search_parent_mobile_no: $scope.global_Search.search_parent_mobile_no,
                    search_parent_mother_name: $scope.global_Search.search_parent_mother_name,
                    search_parent_mother_email_id: $scope.global_Search.search_parent_mother_email_id,
                    search_parent_mother_mobile_no: $scope.global_Search.search_parent_mother_mobile_no
                }

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchParent?data=" + JSON.stringify($scope.searchbyparent)).then(function (res) {
                    if (res.data.length > 0) {
                        $scope.currentPage = 0;
                        $scope.global_parent_table = true;
                        $scope.showParentFilterBtn = true;
                        $scope.showParentFilter = true;
                        $scope.global_search_result = res.data;
                      
                    }
                        
                    else {
                        $scope.global_search_result = [];

                        swal({ text: "No record(s) found.", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 450, height: 200, timer: 1000000 });
                    }

                    $timeout(function () {
                        $("#Table_Fix2").tableHeadFixer({ 'top': 1 });
                    }, 100);

                    $scope.busy = false;
                    console.log($scope.global_search_result);
                });
            }

            $scope.Global_Search_SearchParentSectionWise = function () {
                $scope.global_parent_table = false;
                $scope.busy = true;

                $scope.searchbyparent = {
                    search_std_grade_name: $scope.global_Search.gradecode2,
                    search_std_section_name: $scope.global_Search.sectioncode2,
                }

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchParentSectionWise?grade=" + $scope.global_Search.gradecode2 + "&section=" + $scope.global_Search.sectioncode2).then(function (res) {
                    $scope.global_search_result = res.data;
                    $scope.global_parent_table = true;
                    $scope.busy = false;
                    if ($scope.global_search_result.length == 0) {
                        swal({ text: "No record(s) found.", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 450, height: 200, timer: 1000000 });
                    }
                });
            }

            $scope.Global_Search_by_Teacher = function () {
                $scope.global_teacher_table = false;
                $scope.busy = true;

                $scope.searchbyparent = {
                    search_teacher_id: $scope.global_Search.search_teacher_id,
                    search_teacher_name: $scope.global_Search.search_teacher_name
                }

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchTeacher?data=" + JSON.stringify($scope.searchbyparent)).then(function (res) {
                    $scope.currentPage = 0;
                    if (res.data.length > 0) {
                        $scope.global_search_result = res.data;
                        $scope.showTeacherFilterBtn = true;
                        $scope.showTeacherFilter = true;
                        $scope.global_teacher_table = true;
                    }
                    else {
                        $scope.global_search_result = [];

                        swal({ text: "No record(s) found.", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 450, height: 200, timer: 1000000 });
                    }

                    $timeout(function () {
                        $("#Table4").tableHeadFixer({ 'top': 1 });
                    }, 100);

                    $scope.busy = false;
                    console.log($scope.global_search_result);
                    $scope.getPerPageRecords();
                });
            }

            $scope.Global_Search_by_User = function () {
                $scope.global_search_result = [];
                $scope.currentPage = 0;

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchUser?data=" + JSON.stringify($scope.temp1)).then(function (users) {
                    $scope.busy = false;
                    if (users.data.length > 0) {
                        $scope.global_search_result = users.data;
                        $scope.table = true;
                        $scope.showUserFilterBtn = true;
                        $scope.showUserFilter = true;
                        $scope.chkMulti = true;
                    }
                    else {
                        $scope.global_search_result = [];

                        swal({ text: "No record(s) found.", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 450, height: 200, timer: 1000000 });
                    }

                    $timeout(function () {
                        $("#Table5").tableHeadFixer({ 'top': 1 });
                    }, 100);

                    $scope.getPerPageRecords();
                })
            }


            $http.get(ENV.apiUrl + "api/common/GlobalSearch/GetCompanyNameForShift").then(function (res) {
                $scope.cmbCompany = res.data;
            });

            $scope.getDeptFromCompany = function (company_code) {
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/GetDepartmentName?company_code=" + company_code).then(function (res) {
                    $scope.compDepartment = res.data;
                });
            }

            $scope.getDesignationFromCompany = function (company_code) {
                $http.get(ENV.apiUrl + "api/EmployeeReport/GetDesignation?com_code=" + company_code).then(function (res) {
                    $scope.compDesignation = res.data;
                })
            }

            $scope.Global_Search_by_employee = function () {
                $scope.NodataEmployee = false;
                var obj = {};
                if ($scope.showDDFlagForNISS) {
                    obj.em_login_code = $scope.temp.em_login_code;
                    obj.EmpName = $scope.temp.EmpName;
                    obj.em_department = $scope.temp.em_department;
                    obj.em_desg_code = $scope.temp.em_desg_code;
                    obj.company_code = $scope.temp.company_code;

                    $http.post(ENV.apiUrl + "api/common/GlobalSearch/SearchEmployeeForNIS?data=" + JSON.stringify(obj)).then(function (SearchEmployee_Data) {
                        $scope.global_search_result = SearchEmployee_Data.data;
                        console.log($scope.global_search_result);
                        $scope.showEmployeeFilterBtn = true;
                        $scope.showEmployeeFilter = true;
                        $scope.EmployeeTable = true;
                        $scope.currentPage = 0;

                        if (SearchEmployee_Data.data.length > 0) {
                            //   $scope.EmployeeTable = true;
                        }
                        else {
                            $scope.global_search_result = [];
                            swal({ text: "No record(s) found.", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 450, height: 200, timer: 1000000 });
                            $scope.NodataEmployee = true;
                            //  $scope.EmployeeTable = false;
                        }
                        $timeout(function () {
                            $("#fixTable1").tableHeadFixer({ 'top': 1 });
                        }, 100);
                        $scope.getPerPageRecords();
                    });

                }
                else {
                    obj.em_login_code = $scope.temp.em_login_code;
                    obj.EmpName = $scope.temp.EmpName;
                    obj.em_department = $scope.temp.em_department;
                    obj.em_desg_code = $scope.temp.em_desg_code;

                    $http.post(ENV.apiUrl + "api/common/GlobalSearch/SearchEmployee?data=" + JSON.stringify(obj)).then(function (SearchEmployee_Data) {
                        $scope.global_search_result = SearchEmployee_Data.data;
                        console.log($scope.global_search_result);
                        $scope.EmployeeTable = true;
                        $scope.showEmployeeFilterBtn = true;
                        $scope.showEmployeeFilter = true;

                        $scope.currentPage = 0;

                        if (SearchEmployee_Data.data.length > 0) {
                            //   $scope.EmployeeTable = true;
                        }
                        else {
                            $scope.global_search_result = [];
                            swal({ text: "No record(s) found.", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 450, height: 200, timer: 1000000 });
                            $scope.NodataEmployee = true;
                            //  $scope.EmployeeTable = false;
                        }
                        $timeout(function () {
                            $("#fixTable1").tableHeadFixer({ 'top': 1 });
                        }, 100);
                        $scope.getPerPageRecords();
                    });

                }
            }



            $scope.addUser = function (obj, flg, tab) {

                obj['tab'] = tab;
                if (flg) {
                    if ($scope.chkMulti) {
                        $scope.SelectedUserLst.push(obj);
                    }
                    else {
                        if ($scope.old_obj != undefined) {
                            $scope.SelectedUserLst.pop($scope.old_obj);
                            // $scope.SelectedUserLst.pop($scope.old_obj);
                            if ($scope.old_obj != obj)
                                $scope.old_obj.user_chk = false;
                            $scope.SelectedUserLst.push(obj);
                        }
                        else {
                            $scope.SelectedUserLst.push(obj);
                        }

                    }
                    $scope.old_obj = obj;
                    if ($scope.SelectedUserLst.length == $scope.perPageRecords.length) {
                        $scope.selectedAll = true;
                    }
                }
                else {
                    $scope.SelectedUserLst.pop(obj);
                    $scope.selectedAll = false;
                }

            }

            //$scope.isSelectAll = function () {
            //    $scope.SelectedUserLst = [];
            //    if ($scope.selectedAll) {
            //        $scope.selectedAll = true;
            //        for (var i = 0; i < $scope.perPageRecords.length; i++) {
            //            $scope.perPageRecords[i]['tab'] = 'tab6';
            //            $scope.SelectedUserLst.push($scope.perPageRecords[i]);
            //        }
            //        // $scope.row1 = 'row_selected';
            //    }
            //    else {
            //        $scope.selectedAll = false;
            //        $scope.SelectedUserLst = [];
            //    }

            //    angular.forEach($scope.perPageRecords, function (item) {
            //        item.user_chk = $scope.selectedAll;
            //    });

            //}

            /*  select all records of per page */
            //$scope.isSelectAll = function (tab) {
            //    $scope.SelectedUserLst = [];
            //    if ($scope.selectedAll) {
            //        $scope.selectedAll = true;
            //        for (var i = 0; i < $scope.perPageRecords.length; i++) {
            //            $scope.perPageRecords[i]['tab'] = tab;
            //            $scope.SelectedUserLst.push($scope.perPageRecords[i]);
            //        }
            //        //$scope.global_search_result
            //        // $scope.row1 = 'row_selected';
            //    }
            //    else {
            //        $scope.selectedAll = false;
            //        $scope.SelectedUserLst = [];
            //    }

            //    angular.forEach($scope.perPageRecords, function (item) {
            //        item.user_chk = $scope.selectedAll;
            //    });

            //}

            /*  select all records */ 
            $scope.isSelectAll = function (tab) {
                debugger
                $scope.SelectedUserLst = [];
                if ($scope.selectedAll) {
                    $scope.selectedAll = true;
                    for (var i = 0; i < $scope.global_search_result.length; i++) {
                        $scope.global_search_result[i]['tab'] = tab;
                        $scope.SelectedUserLst.push($scope.global_search_result[i]);
                    }
                    //$scope.global_search_result
                    // $scope.row1 = 'row_selected';
                }
                else {
                    $scope.selectedAll = false;
                    $scope.SelectedUserLst = [];
                }

                angular.forEach($scope.global_search_result, function (item) {
                    item.user_chk = $scope.selectedAll;
                });
            }


            $scope.markall = function (flg, tab) {
                for (var i = 0; i < $scope.global_search_result.length; i++) {
                    if (flg) {
                        $scope.global_search_result[i].user_chk = true;
                        $scope.global_search_result[i]['tab'] = tab;
                        $scope.SelectedUserLst.push($scope.global_search_result[i])
                    }
                    else {
                        $scope.global_search_result[i].user_chk = false;
                        $scope.SelectedUserLst = [];
                    }
                }
            }




            $scope.global_Search_click = function () {
                $scope.temp1 = {};
                $scope.SelectedUserLst = [];
                if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                    if ($rootScope.globals.currentUser.username == 'admin') {
                        $scope.visible_stud = $rootScope.visible_stud;
                        $scope.visible_parent = $rootScope.visible_parent;
                        $scope.visible_search_parent = $rootScope.visible_search_parent
                        $scope.visible_teacher = $rootScope.visible_teacher;
                        $scope.visible_User = $rootScope.visible_User;
                        $scope.visible_Employee = $rootScope.visible_Employee;
                        $scope.chkMulti = $rootScope.chkMulti;
                        $scope.visible_UnassignedStudent = $rootScope.visible_UnassignedStudent;
                        $scope.visible_UnassignedParent = $rootScope.visible_UnassignedParent;
                    }
                    else {
                        $scope.visible_stud = $rootScope.visible_stud;
                        $scope.visible_parent = $rootScope.visible_parent;
                        $scope.visible_search_parent = false;
                        $scope.visible_teacher = $rootScope.visible_teacher;
                        $scope.visible_User = $rootScope.visible_User;
                        $scope.visible_Employee = $rootScope.visible_Employee;
                        $scope.chkMulti = $rootScope.chkMulti;
                        $scope.visible_UnassignedStudent = false;
                        $scope.visible_UnassignedParent = false;
                    }
                }
                else {
                    $scope.visible_stud = $rootScope.visible_stud;
                    $scope.visible_parent = $rootScope.visible_parent;
                    $scope.visible_search_parent = $rootScope.visible_search_parent
                    $scope.visible_teacher = $rootScope.visible_teacher;
                    $scope.visible_User = $rootScope.visible_User;
                    $scope.visible_Employee = $rootScope.visible_Employee;
                    $scope.chkMulti = $rootScope.chkMulti;
                    $scope.visible_UnassignedStudent = $rootScope.visible_UnassignedStudent;
                    $scope.visible_UnassignedParent = $rootScope.visible_UnassignedParent;
                }
                //   $("#tab1").css({ display: "none" });
                //  $("#tab2").css({ display: "none" });
                // $("#tab3").css({ display: "none" });
                // $("#tab4").css({ display: "none" });
                // $("#tab5").css({ display: "none" });
                // $("#tab6").css({ display: "none" });
                $scope.global_Search_show = true;
                $scope.selectedAll = false;
                $scope.markall1 = false;
                $scope.showStudentFilterBtn = false;
                $scope.showStudentFilter = false;
                $scope.showParentFilterBtn = false;
                $scope.showParentFilter = false;
                $scope.showTeacherFilterBtn = false;
                $scope.showTeacherFilter = false;
                $scope.showEmployeeFilterBtn = false;
                $scope.showEmployeeFilter = false;
                $scope.showUnassignedStudentFilterBtn = false;
                $scope.showUnassignedStudentFilter = false;
                $scope.showUnassignedParentFilterBtn = false;
                $scope.showUnassignedParentFilter = false;
                $scope.showUserFilterBtn = false;
                $scope.showUserFilter = false;
                $scope.global_student_table = false;
                $scope.global_teacher_table = false;
                $scope.EmployeeTable = false;
                $scope.table = false;
                //$("#Global_Search_Modal").draggable({
                //    handle: ".modal-header"
                //});
                $('#tab1').removeClass('active');
                $('#tab2').removeClass('active');
                $('#tab3').removeClass('active');
                $('#tab4').removeClass('active');
                $('#tab5').removeClass('active');
                $('#tab6').removeClass('active');

                $('#tab7').removeClass('active');
                $('#tab8').removeClass('active');

                $('#mtab1').addClass('active');
                $('#mtab2').removeClass('active');
                $('#mtab3').removeClass('active');
                $('#mtab4').removeClass('active');
                $('#mtab5').removeClass('active');
                $('#mtab6').removeClass('active');
                $('#mtab7').removeClass('active');
                $('#mtab8').removeClass('active');

                if ($rootScope.visible_stud) {
                    //$("#tab1").css({ display: "block" });
                    $('#tab1').addClass('active');
                    $scope.gbl_tab = 'tab1'
                }
                else if ($scope.visible_parent) {
                    //$("#tab2").css({ display: "block" });
                    $('#tab2').addClass('active');
                    $scope.gbl_tab = 'tab2'

                }

                else if ($scope.visible_search_parent) {
                    //$("#tab3").css({ display: "block" });
                    $('#tab3').addClass('active');
                    $scope.gbl_tab = 'tab3'

                }

                else if ($scope.visible_teacher) {
                    //$("#tab4").css({ display: "block" });
                    $('#tab4').addClass('active');
                    $scope.gbl_tab = 'tab4'

                }

                else if ($scope.visible_User) {
                    //  $("#tab5").css({ display: "block" });
                    $('#tab5').addClass('active');
                    $scope.gbl_tab = 'tab5'

                }

                else if ($scope.visible_Employee) {
                    // $("#tab6").css({ display: "block" });
                    $('#tab6').addClass('active');
                    $scope.gbl_tab = 'tab6'

                }

                else if ($scope.visible_UnassignedStudent) {
                    //  $("#tab5").css({ display: "block" });
                    $('#tab7').addClass('active');
                    $scope.gbl_tab = 'tab7'

                }

                else if ($scope.visible_UnassignedParent) {
                    // $("#tab6").css({ display: "block" });
                    $('#tab8').addClass('active');
                    $scope.gbl_tab = 'tab8'

                }

                $scope.glbl_obj = '';
                $scope.global_Search = {};
                $scope.global_search_result = [];
                $scope.temp = '';
                $scope.EmployeeDetails = '';
                try {
                    $('#cmb_gradeparent').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_sectionparent').multipleSelect('uncheckAll');
                }
                catch (e) {

                }
                $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                    $scope.cmbCur = res.data;
                    if ($scope.cmbCur.length > 0) {
                        $scope.global_Search['global_curriculum_code'] = $scope.cmbCur[0].sims_cur_code;
                        $scope.getAcadm_yr($scope.cmbCur[0].sims_cur_code)

                    }

                });

                //$http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (res) {
                //    $scope.cmbCur = res.data;
                //    if ($scope.cmbCur.length > 0) {
                //        $scope.global_Search['global_curriculum_code'] = $scope.cmbCur[0].sims_cur_code;

                //        $scope.getAcadm_yr($scope.cmbCur[0].sims_cur_code)
                //    }
                //})

            }
            $scope.firstTime = true;


            $scope.searchGlobalClick = function () {
               
                // if ($scope.firstTime) {
                //    $scope.firstTime = false;
                $scope.NodataEmployee = false;
                $scope.showStudentFilterBtn = false;
                $scope.showStudentFilter = false;
                $scope.showParentFilterBtn = false;
                $scope.showParentFilter = false;
                $scope.showTeacherFilterBtn = false;
                $scope.showTeacherFilter = false;
                $scope.showEmployeeFilterBtn = false;
                $scope.showEmployeeFilter = false;
                $scope.showUnassignedStudentFilterBtn = false;
                $scope.showUnassignedStudentFilter = false;
                $scope.showUnassignedParentFilterBtn = false;
                $scope.showUnassignedParentFilter = false;
                $scope.showUserFilterBtn = false;
                $scope.showUserFilter = false;
                $('#tab1').removeClass('active');
                $('#tab2').removeClass('active');
                $('#tab3').removeClass('active');
                $('#tab4').removeClass('active');
                $('#tab5').removeClass('active');
                $('#tab6').removeClass('active');
                $('#tab7').removeClass('active');
                $('#tab8').removeClass('active');

                $('#mtab1').removeClass('active');
                $('#mtab2').removeClass('active');
                $('#mtab3').removeClass('active');
                $('#mtab4').removeClass('active');
                $('#mtab5').removeClass('active');
                $('#mtab6').removeClass('active');
                $('#mtab7').removeClass('active');
                $('#mtab8').removeClass('active');
                $scope.gbl_tab = 'tab1';
                //  }

                if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                    $('#tab1').addClass('active');
                    $('#mtab1').addClass('active');


                    if ($rootScope.globals.currentUser.username == 'admin') {
                        $scope.visible_stud = true;
                        $scope.visible_parent = true;
                        $scope.visible_search_parent = false;
                        $scope.visible_teacher = true;
                        $scope.visible_User = true;
                        $scope.visible_Employee = true;
                        $scope.visible_UnassignedStudent = true;
                        $scope.visible_UnassignedParent = true;
                        $scope.visible_parentview = true;
                        $scope.visible_studentview = true;
                        $scope.visible_employeeview = true;

                    }
                    else {
                        $scope.visible_stud = true;
                        $scope.visible_parent = true;
                        $scope.visible_search_parent = false;
                        $scope.visible_teacher = true;
                        $scope.visible_User = true;
                        $scope.visible_Employee = false;
                        $scope.visible_UnassignedStudent = false;
                        $scope.visible_UnassignedParent = false;
                        $scope.visible_parentview = false;
                        $scope.visible_studentview = false;
                        $scope.visible_employeeview = false;
                    }
                }
                else {


                    $http.get(ENV.apiUrl + "api/GlobalSearchMapping/getApplTab?empcode=" + $rootScope.globals.currentUser.username).then(function (result) {
                        $scope.tabs = result.data;
                        $scope.visible_stud = false;
                        $scope.visible_parent = false;
                        $scope.visible_teacher = false;
                        $scope.visible_User = false;
                        $scope.visible_Employee = false;
                        $scope.visible_UnassignedStudent = false;
                        $scope.visible_UnassignedParent = false;
                        $scope.visible_parentview = false;
                        $scope.visible_studentview = false;
                        $scope.visible_employeeview = false;

                        if (!jQuery.isEmptyObject($scope.tabs)) {
                            for (var i = 0; $scope.tabs.length > i; i++) {
                                var onetab = $scope.tabs[i];
                                tab(onetab.comn_user_tab);
                            }


                            $('#mtab1').removeClass('active');
                            $('#mtab2').removeClass('active');
                            $('#mtab3').removeClass('active');
                            $('#mtab4').removeClass('active');
                            $('#mtab5').removeClass('active');
                            $('#mtab6').removeClass('active');
                            $('#mtab7').removeClass('active');
                            $('#mtab8').removeClass('active');

                            if ($scope.visible_stud) {
                                //$("#tab1").css({ display: "block" });
                                $('#tab1').addClass('active');
                                $('#mtab1').addClass('active');

                                $scope.gbl_tab = 'tab1'
                            }
                            else if ($scope.visible_parent) {
                                //$("#tab2").css({ display: "block" });
                                $('#tab2').addClass('active');
                                $('#mtab2').addClass('active');
                                $scope.gbl_tab = 'tab2'

                            }

                            else if ($scope.visible_search_parent) {
                                //$("#tab3").css({ display: "block" });
                                $('#tab3').addClass('active');
                                $('#mtab3').addClass('active');

                                $scope.gbl_tab = 'tab3'

                            }

                            else if ($scope.visible_teacher) {
                                //$("#tab4").css({ display: "block" });
                                $('#tab4').addClass('active');
                                $('#mtab4').addClass('active');

                                $scope.gbl_tab = 'tab4'

                            }

                            else if ($scope.visible_User) {
                                //  $("#tab5").css({ display: "block" });
                                $('#tab5').addClass('active');
                                $('#mtab5').addClass('active');

                                $scope.gbl_tab = 'tab5'

                            }

                            else if ($scope.visible_Employee) {
                                // $("#tab6").css({ display: "block" });
                                $('#tab6').addClass('active');
                                $('#mtab6').addClass('active');

                                $scope.gbl_tab = 'tab6'

                            }

                            else if ($scope.visible_UnassignedStudent) {
                                //  $("#tab5").css({ display: "block" });
                                $('#tab7').addClass('active');
                                $('#mtab7').addClass('active');

                                $scope.gbl_tab = 'tab7'

                            }

                            else if ($scope.visible_UnassignedParent) {
                                // $("#tab6").css({ display: "block" });
                                $('#tab8').addClass('active');
                                $('#mtab8').addClass('active');

                                $scope.gbl_tab = 'tab8'

                            }
                        } else {
                            $('#tab1').addClass('active');
                            $('#mtab1').addClass('active');

                            // tab('0')
                            $scope.visible_stud = true;
                            $scope.visible_parent = true;
                            $scope.visible_teacher = true;
                            $scope.visible_User = true;
                            $scope.visible_Employee = true;
                            $scope.visible_UnassignedStudent = true;
                            $scope.visible_UnassignedParent = true;
                            $scope.visible_parentview = true;
                            $scope.visible_studentview = true;
                            $scope.visible_employeeview = true;
                        }
                    });

                }
                function tab(t) {
                    switch (t) {
                        case "1":
                            $scope.visible_stud = true;
                            break;
                        case "2":
                            $scope.visible_parent = true;
                            break;
                        case "3":
                            $scope.visible_teacher = true;
                            break;
                        case "4":
                            $scope.visible_User = true;
                            break;
                        case "5":
                            $scope.visible_Employee = true;
                            break;
                        case "6":
                            $scope.visible_UnassignedStudent = true;
                            break;
                        case "7":
                            $scope.visible_UnassignedParent = true;
                            break;
                        case "8":
                            $scope.visible_parentview = true;
                            break;
                        case "9":
                            $scope.visible_studentview = true;
                            break;
                        case "10":
                            $scope.visible_employeeview = true;
                            break;

                    }

                }
                $scope.global_Search_show = true;
                //if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                //    if ($rootScope.globals.currentUser.username == 'admin') {
                //        $scope.visible_stud = true;
                //        $scope.visible_parent = true;
                //        $scope.visible_search_parent = false;
                //        $scope.visible_teacher = true;
                //        $scope.visible_User = true;
                //        $scope.visible_Employee = true;
                //        $scope.visible_UnassignedStudent = true;
                //        $scope.visible_UnassignedParent = true;
                //    }
                //    else {
                //        $scope.visible_stud = true;
                //        $scope.visible_parent = true;
                //        $scope.visible_search_parent = false;
                //        $scope.visible_teacher = true;
                //        $scope.visible_User = true;
                //        $scope.visible_Employee = false;
                //        $scope.visible_UnassignedStudent = false;
                //        $scope.visible_UnassignedParent = false;
                //    }
                //}
                //else {
                //    $scope.visible_stud = true;
                //    $scope.visible_parent = true;
                //    $scope.visible_search_parent = false;
                //    $scope.visible_teacher = true;
                //    $scope.visible_User = true;
                //    $scope.visible_Employee = true;
                //    $scope.visible_UnassignedStudent = true;
                //    $scope.visible_UnassignedParent = true;
                //}

                $scope.chkMulti = false;

                $('#Global_Search_Modal').modal({
                    backdrop: "static"
                });

                //$("#Global_Search_Modal").draggable({
                //    handle: ".modal-header"
                //});

                if ($http.defaults.headers.common['schoolId'] == 'sisqatar') {
                    $('#Global_Search_Modal').draggable({
                        cursor: 'move',
                        handle: '.modal-header'
                    });
                    $('.modal.draggable>.modal-dialog>.modal-content>.modal-header').css('cursor', 'move');
                }

                //  $http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (res) {
                //      $scope.cmbCur = res.data;
                //      if ($scope.cmbCur.length > 0) {
                //          $scope.global_Search['global_curriculum_code'] = $scope.cmbCur[0].sims_cur_code;
                //
                //          $scope.getAcadm_yr($scope.cmbCur[0].sims_cur_code)
                //      }
                //  })

                if ($http.defaults.headers.common['schoolId'] == 'lpna') {
                    $('#Global_Search_Modal').draggable({
                        cursor: 'move',
                        handle: '.modal-header'
                    });
                    $('.modal.draggable>.modal-dialog>.modal-content>.modal-header').css('cursor', 'move');
                }

                $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                    $scope.cmbCur = res.data;
                    if ($scope.cmbCur.length > 0) {
                        $scope.global_Search['global_curriculum_code'] = $scope.cmbCur[0].sims_cur_code;
                        $scope.getAcadm_yr($scope.cmbCur[0].sims_cur_code)

                    }
                });



                if ($scope.global_count_comp) {
                    getCur(true, $scope.user_details.comp);


                }
                else {
                    getCur(false, $scope.user_details.comp)
                }







            }

            $scope.getFocus = function () {
                document.getElementById("textPF").focus();
                document.getElementById("textID").focus();
                document.getElementById("textTN").focus();
                document.getElementById("txt_user_id").focus();
                document.getElementById("textEI").focus();
                document.getElementById("textSI").focus();
                document.getElementById("textUPN").focus();
            }

            $scope.menu_click = function (str, tab) {
                $scope.gbl_tab = tab;
                $scope.SelectedUserLst = [];
                $scope.glbl_obj = '';
                $scope.global_Search = {};
                $scope.global_search_result = '';
                $scope.temp = {};
                $scope.temp1 = {};

                $scope.getFocus()
                try {
                    $('#cmb_gradeparent').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_sectionparent').multipleSelect('uncheckAll');
                }
                catch (e) {

                }

                $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                    $scope.cmbCur = res.data;
                    if ($scope.cmbCur.length > 0) {
                        $scope.global_Search['global_curriculum_code'] = $scope.cmbCur[0].sims_cur_code;
                        $scope.getAcadm_yr($scope.cmbCur[0].sims_cur_code)

                    }
                });

            }

            ///Student Profile

            $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/';



            if ($http.defaults.headers.common['schoolId'] == 'eiama' || $http.defaults.headers.common['schoolId'] == 'eiam' || $http.defaults.headers.common['schoolId'] == 'eiand' || $http.defaults.headers.common['schoolId'] == 'eiagps') {
               $scope.pdfUrl = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/Docs/timetable/';

            }
            else {
                $scope.pdfUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Docs/timetable/';

            }
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
                $("#fixTable2").tableHeadFixer({ 'top': 1 });
                $("#fixTable5").tableHeadFixer({ 'top': 1 });
                $("#fixTable6").tableHeadFixer({ 'top': 1 });
                $("#fixTable3").tableHeadFixer({ 'top': 1 });
                $("#fixTable4").tableHeadFixer({ 'top': 1 });
                $("#fixTable7").tableHeadFixer({ 'top': 1 });
                $("#example_wrapper3").scrollbar();
                $("#example_wrapper4").scrollbar();
                $("#example_wrapper5").scrollbar();
                $("#example_wrapper6").scrollbar();
                $("#example_wrapper7").scrollbar();
            }, 100);


            $scope.studentProfile = function (enrollNo, parentId, Gradecode, SectionCode, curcode, teachername) {
               
                $scope.temp6 = {
                    enrollNo: enrollNo,
                    teachername: teachername,
                }
                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentGradeSection?enroll_no=" + enrollNo).then(function (res1) {
                    $scope.studentGrade = res1.data;
                    if ($scope.studentGrade.length > 0) {

                        $scope.edt1 = {
                            sims_grade_name: $scope.studentGrade[0].sims_grade_name,
                            sims_section_name: $scope.studentGrade[0].sims_section_name,
                            sims_teacher_name: $scope.studentGrade[0].sims_teacher_name,
                        }
                    }
                });

                $http.get(ENV.apiUrl + "api/StudetProfile/getParentDetails?enroll_no=" + enrollNo).then(function (ParentDetails) {

                    $scope.Parent_Details = ParentDetails.data;
                    $scope.parent_info = $scope.Parent_Details[0];

                    $scope.student_father_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.parent_info.sims_admisison_father_image;
                    $scope.student_mother_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.parent_info.sims_admisison_mother_image;
                    $scope.student_guardian_image1 = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.parent_info.sims_admisison_guardian_image;

                    $scope.getstatefather($scope.parent_info.sims_admission_father_country_code);
                    $scope.getcityname($scope.parent_info.sims_admission_father_state);

                    $scope.getmothercityname($scope.parent_info.sims_admission_mother_state);
                    $scope.getstatemother($scope.parent_info.sims_admission_mother_country_code);

                    $scope.getguardiancityname($scope.parent_info.sims_admission_guardian_state);
                    $scope.getstateguardian($scope.parent_info.sims_admission_guardian_country_code);
                    $scope.isparentschoolemployee($scope.parent_info.sims_parent_is_employment_status);





                });

                $http.get(ENV.apiUrl + "api/StudetProfile/getStaffchilddetail?enroll_no=" + enrollNo).then(function (Staffchil) {
                    $scope.Staffchilddata = Staffchil.data;
                    if ($scope.Staffchilddata.length > 0) {
                        $scope.staffchildshow = true;
                        $scope.temp4 = {
                            comp_name: $scope.Staffchilddata[0].comp_name,
                            em_login_code: $scope.Staffchilddata[0].em_login_code,
                            employeeName: $scope.Staffchilddata[0].employeeName,
                            codp_dept_name: $scope.Staffchilddata[0].codp_dept_name,
                            dg_desc: $scope.Staffchilddata[0].dg_desc,
                        }
                    }
                    else {
                        $scope.staffchildshow = false;
                    }
                });

                $http.get(ENV.apiUrl + "api/StudetProfile/getLeaveApplicationDetails?enroll_no=" + enrollNo).then(function (leaveAppl) {
                    if ($http.defaults.headers.common['schoolId'] == 'adisw') {
                        $scope.feeDetailsshow1 = true;
                        $("#fee_details").css({ display: "none" });
                        // $('#fee_details').addClass
                    }

                    $scope.Leaveapplication = leaveAppl.data;
                    if ($scope.Leaveapplication.length > 0) {
                        $scope.LeaveApplicationshow = true;
                        $scope.LeaveApplicationhide = false;
                    }
                    else {
                        $scope.LeaveApplicationhide = true;
                        $scope.LeaveApplicationshow = false;
                    }
                });

                $http.get(ENV.apiUrl + "api/StudetProfile/getSubjectDetails?enroll_no=" + enrollNo + "&grade_cd=" + Gradecode + "&section_cd=" + SectionCode).then(function (subject) {

                    $scope.subjectDetails = subject.data;
                    if ($scope.subjectDetails.length > 0) {
                        $scope.subjectDatashow = true;
                        $scope.subjectdetailhide = false;
                    }
                    else {
                        $scope.subjectdetailhide = true;
                        $scope.subjectDatashow = false;
                    }
                });

                $http.get(ENV.apiUrl + "api/StudetProfile/Get_SimsActiveForm").then(function (res1) {

                    $scope.activeForms = res1.data;
                    if ($scope.activeForms != undefined || $scope.activeForms != null) {

                        for (var i = 0; i < $scope.activeForms.length; i++) {
                            
                            if ($scope.activeForms[i].sims_active_form == "student" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentDetails?student_enroll=" + enrollNo + "&parent_no=" + parentId).then(function (res) {
                                    $scope.studentProfileshow = true;
                                    $scope.academicYearshow = true;
                                    $scope.StudetProfile = res.data;
                                    if ($scope.StudetProfile.length > 0) {
                                        $scope.temp = {
                                            student_full_name: $scope.StudetProfile[0].student_full_name,
                                            sims_student_family_name_en: $scope.StudetProfile[0].sims_student_family_name_en,
                                            sims_student_image: $scope.StudetProfile[0].sims_student_image,

                                            sims_student_family_name_ot: $scope.StudetProfile[0].sims_student_family_name_ot,
                                            sims_student_nickname: $scope.StudetProfile[0].sims_student_nickname,
                                            student_dob: $scope.StudetProfile[0].sims_student_dob,
                                            sims_student_nationality_code: $scope.StudetProfile[0].sims_student_nationality_code,
                                            sims_student_gender: $scope.StudetProfile[0].sims_student_gender,
                                            sims_student_birth_country_code: $scope.StudetProfile[0].sims_student_birth_country_code,
                                            sims_student_religion_code: $scope.StudetProfile[0].sims_student_religion_code,
                                            sims_parent_father_summary_address: $scope.StudetProfile[0].sims_parent_father_summary_address,
                                            sims_parent_country_code: $scope.StudetProfile[0].sims_parent_country_code,
                                            sims_parent_father_city: $scope.StudetProfile[0].sims_parent_father_city,
                                            sims_parent_father_phone: $scope.StudetProfile[0].sims_parent_father_phone,
                                            sims_parent_father_email: $scope.StudetProfile[0].sims_parent_father_email,
                                            sims_parent_father_state: $scope.StudetProfile[0].sims_parent_father_state,
                                            sims_parent_father_po_box: $scope.StudetProfile[0].sims_parent_father_po_box,
                                            sims_parent_father_mobile: $scope.StudetProfile[0].sims_parent_father_mobile,
                                            sims_parent_father_fax: $scope.StudetProfile[0].sims_parent_father_fax,
                                            sims_student_emergency_contact_name1: $scope.StudetProfile[0].sims_student_emergency_contact_name1,
                                            sims_student_emergency_contact_name2: $scope.StudetProfile[0].sims_student_emergency_contact_name2,
                                            sims_student_emergency_contact_number1: $scope.StudetProfile[0].sims_student_emergency_contact_number1,
                                            sims_student_emergency_contact_number2: $scope.StudetProfile[0].sims_student_emergency_contact_number2,

                                            sims_student_passport_number: $scope.StudetProfile[0].sims_student_passport_number,
                                            sims_student_passport_issue_date: $scope.StudetProfile[0].sims_student_passport_issue_date,
                                            sims_student_passport_expiry_date: $scope.StudetProfile[0].sims_student_passport_expiry_date,
                                            sims_student_passport_issuing_authority: $scope.StudetProfile[0].sims_student_passport_issuing_authority,
                                            sims_student_passport_issue_place: $scope.StudetProfile[0].sims_student_passport_issue_place,

                                            sims_student_visa_number: $scope.StudetProfile[0].sims_student_visa_number,
                                            sims_student_visa_type: $scope.StudetProfile[0].sims_student_visa_type,
                                            sims_student_visa_issuing_authority: $scope.StudetProfile[0].sims_student_visa_issuing_authority,

                                            sims_student_visa_issuing_place: $scope.StudetProfile[0].sims_student_visa_issuing_place,
                                            sims_student_visa_expiry_date: $scope.StudetProfile[0].sims_student_visa_expiry_date,
                                            sims_student_visa_issue_date: $scope.StudetProfile[0].sims_student_visa_issue_date,
                                            sims_student_national_id: $scope.StudetProfile[0].sims_student_national_id,
                                            sims_student_national_id_issue_date: $scope.StudetProfile[0].sims_student_national_id_issue_date,
                                            sims_student_national_id_expiry_date: $scope.StudetProfile[0].sims_student_national_id_expiry_date,

                                            sims_student_current_school_code: $scope.StudetProfile[0].sims_student_current_school_code,
                                            sims_student_ea_number: $scope.StudetProfile[0].sims_student_ea_number,
                                            sims_student_commence_date: $scope.StudetProfile[0].sims_student_commence_date,
                                            sims_student_date: $scope.StudetProfile[0].sims_student_date,
                                            sims_student_academic_status: $scope.StudetProfile[0].sims_student_academic_status,
                                            sims_student_house: $scope.StudetProfile[0].sims_student_house,
                                            sims_student_class_rank: $scope.StudetProfile[0].sims_student_class_rank,
                                            sims_student_transport_status: $scope.StudetProfile[0].sims_student_transport_status,
                                            sims_student_main_language: $scope.StudetProfile[0].sims_student_main_language,
                                            sims_student_other_language: $scope.StudetProfile[0].sims_student_other_language,
                                            sims_student_last_login: $scope.StudetProfile[0].sims_student_last_login,
                                            sims_student_remark: $scope.StudetProfile[0].sims_student_remark,
                                            sims_student_main_language_m: $scope.StudetProfile[0].sims_student_main_language_m,
                                        }
                                    }

                                });
                            }
                            else if ($scope.activeForms[i].sims_active_form == "medical" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentMedicalDetails?enroll_no=" + enrollNo).then(function (res) {
                                    $scope.MedicalData = res.data;

                                    $scope.medicalDetailsshow = true;
                                    if ($scope.MedicalData.length > 0) {
                                        $scope.medicalDetailsshowData = true;
                                        $scope.medicalDetailshideData = false;

                                        $scope.edt = {
                                            health_card_number: $scope.MedicalData[0].health_card_number,
                                            health_card_issue_date: $scope.MedicalData[0].health_card_issue_date,
                                            health_card_expiry_date: $scope.MedicalData[0].health_card_expiry_date,
                                            health_card_issuing_authority: $scope.MedicalData[0].health_card_issuing_authority,
                                            student_blood_group_code: $scope.MedicalData[0].student_blood_group_code,
                                            student_health_restriction_status: $scope.MedicalData[0].student_health_restriction_status,
                                            student_disability_status: $scope.MedicalData[0].student_disability_status,
                                            student_medication_status: $scope.MedicalData[0].student_medication_status,
                                            health_other_status: $scope.MedicalData[0].health_other_status,
                                            health_hearing_status: $scope.MedicalData[0].health_hearing_status,
                                            health_vision_status: $scope.MedicalData[0].health_vision_status,

                                            regular_doctor_name: $scope.MedicalData[0].regular_doctor_name,
                                            regular_hospital_name: $scope.MedicalData[0].regular_hospital_name,
                                            regular_doctor_phone: $scope.MedicalData[0].regular_doctor_phone,
                                            regular_hospital_phone: $scope.MedicalData[0].regular_hospital_phone,

                                            student_height: $scope.MedicalData[0].student_height,
                                            student_wieght: $scope.MedicalData[0].student_wieght,
                                            student_teeth: $scope.MedicalData[0].student_teeth,


                                        }
                                    }
                                    else {
                                        $scope.medicalDetailshideData = true;
                                        $scope.medicalDetailsshowData = false;
                                    }

                                });

                            }

                            else if ($scope.activeForms[i].sims_active_form == "fees" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_FeeDetails?enroll_no=" + enrollNo + '&academic=' + $scope.global_Search.global_acdm_yr).then(function (res) {
                                    $scope.feeDetailsshow = true;

                                    $scope.FeeDetails = res.data;
                                    if ($scope.FeeDetails.length > 0) {
                                        $scope.feeDetailsData = true;
                                        $scope.feeDetailshide = false;
                                    }

                                    else {
                                        $scope.feeDetailshide = true;
                                        $scope.feeDetailsData = false;
                                    }


                                });

                            }

                            else if ($scope.activeForms[i].sims_active_form == "incidence" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_SimsIncidence?enroll_no=" + enrollNo).then(function (res) {

                                    $scope.IncidanceDetails = res.data;

                                    $scope.incidenceDetailsshow = true;
                                    if ($scope.IncidanceDetails.length > 0) {
                                        $scope.incidenceDetailsData = true;
                                        $scope.incidenceDetailshide = false;
                                    }
                                    else {
                                        $scope.incidenceDetailshide = true;
                                        $scope.incidenceDetailsData = false;
                                    }

                                });

                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_SimsDetention?enroll_no=" + enrollNo).then(function (res) {

                                    $scope.DetentionDetails = res.data;

                                    $scope.DetentionDetailsshow = true;
                                    if ($scope.DetentionDetails.length > 0) {
                                        $scope.DetentionDetailsData = true;
                                        $scope.DetentionDetailshide = false;
                                    }
                                    else {
                                        $scope.DetentionDetailshide = true;
                                        $scope.DetentionDetailsData = false;
                                    }

                                });
                            }

                            else if ($scope.activeForms[i].sims_active_form == "library" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_LibraryTransaction?enroll_no=" + enrollNo).then(function (res) {
                                  
                                    $scope.libraryDetailsshow = true;

                                    $scope.LibraryDetails = res.data;
                                    if ($scope.LibraryDetails.length > 0) {
                                        $scope.libraryDetailsData = true;
                                        $scope.libraryDetailshide = false;
                                    }
                                    else {
                                        $scope.libraryDetailshide = true;
                                        $scope.libraryDetailsData = false;
                                    }



                                });

                            }

                            else if ($scope.activeForms[i].sims_active_form == "attendance" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentAttendancechartview?enroll_no=" + enrollNo).then(function (res) {
                                    $scope.attendanceDetailsshow = true;
                                    $scope.AttendanceDetails = res.data;
                                    if ($scope.AttendanceDetails.length > 0) {
                                        $scope.attendanceDetailsData = true;
                                        $scope.attendanceDetailshide = false;
                                    }
                                    else {
                                        $scope.attendanceDetailshide = true;
                                        $scope.attendanceDetailsshow = false;
                                    }


                                    $scope.totalpresent = 0;
                                    $scope.totalApsent = 0;
                                    $scope.totalDays = 0;
                                    for (var k = 0; k < $scope.AttendanceDetails.length; k++) {
                                        $scope.totalpresent = $scope.totalpresent + $scope.AttendanceDetails[k].sims_attendance_cd_p;
                                        $scope.totalApsent = $scope.totalApsent + $scope.AttendanceDetails[k].sims_attendance_cd_a;
                                        $scope.totalDays = $scope.totalDays + $scope.AttendanceDetails[k].sims_attendance_totaldays;
                                    }
                                    $scope.edt2 = {
                                        sims_attendance_cd_p: $scope.totalpresent,
                                        sims_attendance_cd_a: $scope.totalApsent,
                                    }

                                });

                            }
                            else if ($scope.activeForms[i].sims_active_form == "result" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentResult?enroll_no=" + enrollNo + "&grade_cd=" + Gradecode + "&section_cd=" + SectionCode).then(function (res) {
                                    $scope.ResultDetails = res.data;

                                    $scope.resultDetailsshow = true;
                                    $scope.resultDetailshide = false;
                                    if ($scope.ResultDetails.length > 0) {
                                        $scope.resultDetailsData = true;
                                    }
                                    else {
                                        $scope.resultDetailshide = true;
                                        $scope.resultDetailsshow = false;
                                    }


                                });

                            }

                            else if ($scope.activeForms[i].sims_active_form == "timetable" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_TimetableFileName?grade_cd=" + Gradecode + "&section_cd=" + SectionCode).then(function (res) {
                                   
                                    $scope.TimeTableDetails = res.data;
                                    $scope.timeTableDetailsshow = true;
                                    if ($scope.TimeTableDetails.length > 0) {
                                        $scope.timeTableDetailsData = true;
                                        $scope.timeTableDetailshide = false;
                                        //$scope.edt3 = {
                                        //    pdffile: $scope.TimeTableDetails,
                                        //}
                                        $scope.edt3 = {
                                            pdffile: $scope.pdfUrl + $scope.TimeTableDetails
                                        }
                                        $("#pdfFileE").attr('src', $scope.edt3.pdffile);
                                    }
                                    else {
                                        $scope.timeTableDetailshide = true;
                                        $scope.timeTableDetailsData = false;
                                        $("#pdfFileE").attr('src', '');
                                    }


                                });

                            }

                            else if ($scope.activeForms[i].sims_active_form == "transport" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_TransportRouteStudent?enroll_no=" + enrollNo).then(function (res) {
                                    $scope.TransportDetails = res.data;
                                    $scope.tansportDetailsshow = true;
                                    if ($scope.TransportDetails.length > 0) {
                                        $scope.tansportDetailsshowData = true;
                                        $scope.tansportDetailshideData = false;

                                        $scope.temp1 = {
                                            sims_transport_route_code_name: $scope.MedicalData[0].sims_transport_route_code_name,
                                            sims_transport_route_direction: $scope.MedicalData[0].sims_transport_route_direction,
                                            sims_transport_effective_from: $scope.MedicalData[0].sims_transport_effective_from,
                                            sims_transport_effective_upto: $scope.MedicalData[0].sims_transport_effective_upto,
                                            sims_transport_pickup_stop_name: $scope.MedicalData[0].sims_transport_pickup_stop_name,
                                            sims_transport_drop_stop_name: $scope.MedicalData[0].sims_transport_drop_stop_name,

                                        }
                                    }

                                    else {
                                        $scope.tansportDetailsshowData = false;
                                        $scope.tansportDetailshideData = true;
                                    }


                                });

                            }

                            else if ($scope.activeForms[i].sims_active_form == "sibling" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_SiblingDetails?enroll_no=" + enrollNo).then(function (res) {

                                    $scope.SiblingDetails = res.data;
                                    if ($scope.SiblingDetails.length > 0) {
                                        $scope.temp2 = {
                                            siblingCount: $scope.SiblingDetails.length,

                                        }
                                    }
                                    else {
                                        $scope.temp2 = {
                                            siblingCount: 0,
                                        }



                                    }

                                });

                            }
                        }
                    }

                });

                $('#StudentProfileModal').modal('show');



            }

            $scope.check_clickdataP = function (atte) {
                var enrollno = null;
                var f = atte.sims_attendance_cd_p;
                for (var i = 0; i < $scope.global_search_result.length; i++) {
                    enrollno = $scope.global_search_result[i].s_enroll_no;
                }

                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentAttendancedayview?enroll_no=" + enrollno + "&status=" + 'P' + "&month=" + atte.sims_month_no).then(function (res) {
                    $scope.all_data_stud_perticular = res.data;
                    $scope.all_data_emp_perticular1 = res.data;
                    if ($scope.all_data_emp_perticular1.length > 0) {
                        $('#MyModal42').modal('show');
                        $scope.tabledata = true;
                        $scope.tansportDetailsshow = true;
                        //$scope.tansportDetailsshow

                    }
                    else {
                        swal({ text: "Data not Availiable", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                });
            }

            $scope.check_clickdataa = function (atte) {
                var enrollno = null;
                var f = atte.sims_attendance_cd_p;
                for (var i = 0; i < $scope.global_search_result.length; i++) {
                    enrollno = $scope.global_search_result[i].s_enroll_no;
                }

                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentAttendancedayview?enroll_no=" + enrollno + "&status=" + 'A' + "&month=" + atte.sims_month_no).then(function (res) {
                    $scope.all_data_stud_perticular = res.data;
                    $scope.all_data_emp_perticular1 = res.data;
                    if ($scope.all_data_emp_perticular1.length > 0) {
                        $('#MyModal42').modal('show');
                        $scope.tabledata = true;
                        $scope.tansportDetailsshow = true;
                        //$scope.tansportDetailsshow

                    }
                    else {
                        swal({ text: "Data not Availiable", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                });
            }

            $scope.check_clickdataae = function (atte) {
                var enrollno = null;
                var f = atte.sims_attendance_cd_p;
                for (var i = 0; i < $scope.global_search_result.length; i++) {
                    enrollno = $scope.global_search_result[i].s_enroll_no;
                }

                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentAttendancedayview?enroll_no=" + enrollno + "&status=" + 'AE' + "&month=" + atte.sims_month_no).then(function (res) {
                    $scope.all_data_stud_perticular = res.data;
                    $scope.all_data_emp_perticular1 = res.data;
                    if ($scope.all_data_emp_perticular1.length > 0) {
                        $('#MyModal42').modal('show');
                        $scope.tabledata = true;
                        $scope.tansportDetailsshow = true;
                        //$scope.tansportDetailsshow

                    }
                    else {
                        swal({ text: "Data not Availiable", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                });
            }

            $scope.check_clickdatat = function (atte) {
                var enrollno = null;
                var f = atte.sims_attendance_cd_p;
                for (var i = 0; i < $scope.global_search_result.length; i++) {
                    enrollno = $scope.global_search_result[i].s_enroll_no;
                }

                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentAttendancedayview?enroll_no=" + enrollno + "&status=" + 'T' + "&month=" + atte.sims_month_no).then(function (res) {
                    $scope.all_data_stud_perticular = res.data;
                    $scope.all_data_emp_perticular1 = res.data;
                    if ($scope.all_data_emp_perticular1.length > 0) {
                        $('#MyModal42').modal('show');
                        $scope.tabledata = true;
                        $scope.tansportDetailsshow = true;
                        //$scope.tansportDetailsshow

                    }
                    else {
                        swal({ text: "Data not Availiable", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                });
            }

            $scope.check_clickdatate = function (atte) {
                var enrollno = null;
                var f = atte.sims_attendance_cd_p;
                for (var i = 0; i < $scope.global_search_result.length; i++) {
                    enrollno = $scope.global_search_result[i].s_enroll_no;
                }

                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentAttendancedayview?enroll_no=" + enrollno + "&status=" + 'TE' + "&month=" + atte.sims_month_no).then(function (res) {
                    $scope.all_data_stud_perticular = res.data;
                    $scope.all_data_emp_perticular1 = res.data;
                    if ($scope.all_data_emp_perticular1.length > 0) {
                        $('#MyModal42').modal('show');
                        $scope.tabledata = true;
                        $scope.tansportDetailsshow = true;
                        //$scope.tansportDetailsshow

                    }
                    else {
                        swal({ text: "Data not Availiable", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                });
            }

            $scope.check_clickdataum = function (atte) {
                var enrollno = null;
                var f = atte.sims_attendance_cd_p;
                for (var i = 0; i < $scope.global_search_result.length; i++) {
                    enrollno = $scope.global_search_result[i].s_enroll_no;
                }

                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentAttendancedayview?enroll_no=" + enrollno + "&status=" + 'UM' + "&month=" + atte.sims_month_no).then(function (res) {
                    $scope.all_data_stud_perticular = res.data;
                    $scope.all_data_emp_perticular1 = res.data;
                    if ($scope.all_data_emp_perticular1.length > 0) {
                        $('#MyModal42').modal('show');
                        $scope.tabledata = true;
                        $scope.tansportDetailsshow = true;
                        //$scope.tansportDetailsshow

                    }
                    else {
                        swal({ text: "Data not Availiable", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                });
            }

            $scope.check_clickdataw = function (atte) {
                var enrollno = null;
                var f = atte.sims_attendance_cd_p;
                for (var i = 0; i < $scope.global_search_result.length; i++) {
                    enrollno = $scope.global_search_result[i].s_enroll_no;
                }

                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentAttendancedayview?enroll_no=" + enrollno + "&status=" + 'W' + "&month=" + atte.sims_month_no).then(function (res) {
                    $scope.all_data_stud_perticular = res.data;
                    $scope.all_data_emp_perticular1 = res.data;
                    if ($scope.all_data_emp_perticular1.length > 0) {
                        $('#MyModal42').modal('show');
                        $scope.tabledata = true;
                        $scope.tansportDetailsshow = true;
                        //$scope.tansportDetailsshow

                    }
                    else {
                        swal({ text: "Data not Availiable", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                });
            }

            $scope.check_clickdatah = function (atte) {
                var enrollno = null;
                var f = atte.sims_attendance_cd_p;
                for (var i = 0; i < $scope.global_search_result.length; i++) {
                    enrollno = $scope.global_search_result[i].s_enroll_no;
                }

                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentAttendancedayview?enroll_no=" + enrollno + "&status=" + 'H' + "&month=" + atte.sims_month_no).then(function (res) {
                    $scope.all_data_stud_perticular = res.data;
                    $scope.all_data_emp_perticular1 = res.data;
                    if ($scope.all_data_emp_perticular1.length > 0) {
                        $('#MyModal42').modal('show');
                        $scope.tabledata = true;
                        $scope.tansportDetailsshow = true;
                        //$scope.tansportDetailsshow

                    }
                    else {
                        swal({ text: "Data not Availiable", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                });
            }

            $scope.closemymodule2 = function () {
                $('#MyModal42').modal('hide');
                $scope.attendanceDetailsshow = true;
            }


            //$scope.check_clickdata = function () {
            //    $('#MyModal2').modal('show');
            //}

            $timeout(function () {
                $("#printdata").tableHeadFixer({ 'top': 1 });
            }, 100);






            // for employee_login details
            $scope.LoginDetails = function () {
                $('#employeeProfile').modal('show');
                $scope.profile_details = true;
                $scope.NodataAttendance = false;
                $scope.NodataLeave = false;
                $scope.NodataClass = false;
                $http.get(ENV.apiUrl + "api/common/getEmployeeProfileView?em_login_code=" + user + "&em_number=" + user).then(function (getEmployeeProfileView_Data) {
                    $scope.EmployeeProfileView = getEmployeeProfileView_Data.data;
                    console.log($scope.EmployeeProfileView);
                    for (var i = 0; i < $scope.EmployeeProfileView.length; i++) {


                        //   $scope.imageUrl = ENV.apiUrl + 'Content/sjs/Images/EmployeeImages/' + $scope.EmployeeProfileView[i].em_img;
                        $scope.imageUrl = ENV.siteUrl + 'Images/EmployeeImages/' + $scope.EmployeeProfileView[i].em_img;
                        $scope.em_full_name = $scope.EmployeeProfileView[i].em_full_name;
                        $scope.em_name_ot = $scope.EmployeeProfileView[i].em_name_ot;
                        $scope.em_family_name = $scope.EmployeeProfileView[i].em_family_name;
                        $scope.em_sex = $scope.EmployeeProfileView[i].em_sex;
                        $scope.em_marital_status = $scope.EmployeeProfileView[i].em_marital_status;
                        $scope.em_nation_name = $scope.EmployeeProfileView[i].em_nation_name;
                        $scope.em_last_login = $scope.EmployeeProfileView[i].em_last_login;
                        $scope.em_date_of_birth = $scope.EmployeeProfileView[i].em_date_of_birth;
                        $scope.em_handicap_status = $scope.EmployeeProfileView[i].em_handicap_status;
                        $scope.em_religion_name = $scope.EmployeeProfileView[i].em_religion_name;
                        $scope.em_salutation = $scope.EmployeeProfileView[i].em_salutation;
                        $scope.em_Salutation_Code = $scope.EmployeeProfileView[i].em_Salutation_Code;


                        //Passport & Contact_Details
                        $scope.em_passport_no = $scope.EmployeeProfileView[i].em_passport_no;
                        $scope.em_passport_issue_date = $scope.EmployeeProfileView[i].em_passport_issue_date;
                        $scope.em_passport_exp_date = $scope.EmployeeProfileView[i].em_passport_exp_date;
                        $scope.em_passport_issuing_authority = $scope.EmployeeProfileView[i].em_passport_issuing_authority;

                        $scope.em_emergency_contact_name1 = $scope.EmployeeProfileView[i].em_emergency_contact_name1;
                        $scope.em_emergency_contact_number1 = $scope.EmployeeProfileView[i].em_emergency_contact_number1;
                        $scope.em_emergency_contact_name2 = $scope.EmployeeProfileView[i].em_emergency_contact_name2;
                        $scope.em_emergency_contact_number2 = $scope.EmployeeProfileView[i].em_emergency_contact_number2;

                        //Visa & NationalID_Details
                        $scope.em_visa_no = $scope.EmployeeProfileView[i].em_visa_no;
                        $scope.em_visa_type = $scope.EmployeeProfileView[i].em_visa_type;
                        $scope.em_visa_issuing_authority = $scope.EmployeeProfileView[i].em_visa_issuing_authority;
                        $scope.em_visa_issuing_place = $scope.EmployeeProfileView[i].em_visa_issuing_place;
                        $scope.em_visa_issue_date = $scope.EmployeeProfileView[i].em_visa_issue_date;
                        $scope.em_visa_exp_date = $scope.EmployeeProfileView[i].em_visa_exp_date;

                        $scope.em_national_id = $scope.EmployeeProfileView[i].em_national_id;
                        $scope.em_national_id_issue_date = $scope.EmployeeProfileView[i].em_national_id_issue_date;
                        $scope.em_national_id_expiry_date = $scope.EmployeeProfileView[i].em_national_id_expiry_date;

                        //Company_Details
                        $scope.em_company_name = $scope.EmployeeProfileView[i].em_company_name;
                        $scope.em_dept_name = $scope.EmployeeProfileView[i].em_dept_name;
                        $scope.em_desg_name = $scope.EmployeeProfileView[i].em_desg_name;
                        $scope.em_joining_ref = $scope.EmployeeProfileView[i].em_joining_ref;
                        $scope.em_grade_name = $scope.EmployeeProfileView[i].em_grade_name;
                        $scope.em_grade_effect_from = $scope.EmployeeProfileView[i].em_grade_effect_from;
                        $scope.em_date_of_join = $scope.EmployeeProfileView[i].em_date_of_join;
                        $scope.em_agreement_start_date = $scope.EmployeeProfileView[i].em_agreement_start_date;
                        $scope.em_agreement_exp_date = $scope.EmployeeProfileView[i].em_agreement_exp_date;
                        $scope.em_punching_id = $scope.EmployeeProfileView[i].em_punching_id;
                        $scope.em_staff_type = $scope.EmployeeProfileView[i].em_staff_type;
                        $scope.em_service_status = $scope.EmployeeProfileView[i].em_service_status;

                        //Bank_Details
                        $scope.em_bank_name = $scope.EmployeeProfileView[i].em_bank_name;
                        $scope.em_gpf_ac_no = $scope.EmployeeProfileView[i].em_gpf_ac_no;
                        $scope.em_gosi_ac_no = $scope.EmployeeProfileView[i].em_gosi_ac_no;
                        $scope.em_labour_card_no = $scope.EmployeeProfileView[i].em_labour_card_no;
                        $scope.em_bank_ac_no = $scope.EmployeeProfileView[i].em_bank_ac_no;
                        $scope.em_bank_swift_code = $scope.EmployeeProfileView[i].em_bank_swift_code;
                        $scope.em_gosi_start_date = $scope.EmployeeProfileView[i].em_gosi_start_date;
                        $scope.em_iban_no = $scope.EmployeeProfileView[i].em_iban_no;


                        $scope.em_summary_address = $scope.EmployeeProfileView[i].em_summary_address;
                        $scope.em_country = $scope.EmployeeProfileView[i].em_country;
                        $scope.em_state = $scope.EmployeeProfileView[i].em_state;
                        $scope.em_city = $scope.EmployeeProfileView[i].em_city;
                        $scope.em_po_box = $scope.EmployeeProfileView[i].em_po_box;
                        $scope.em_phone = $scope.EmployeeProfileView[i].em_phone;
                        $scope.em_mobile = $scope.EmployeeProfileView[i].em_mobile;
                        $scope.em_iban_no = $scope.EmployeeProfileView[i].em_iban_no;
                        $scope.em_email = $scope.EmployeeProfileView[i].em_email;

                    }
                });

                $http.get(ENV.apiUrl + "api/common/getEmployeeAttendanceDetails?em_login_code=" + user).then(function (getEmployeeAttendanceDetails_Data) {
                    $scope.EmployeeAttendanceDetails = getEmployeeAttendanceDetails_Data.data;
                    console.log($scope.EmployeeAttendanceDetails);

                    if ($scope.EmployeeAttendanceDetails < 1) {
                        $scope.NodataAttendance = true;

                    }
                    else {

                        $scope.AttendanceData = true;
                    }

                });


                $http.get(ENV.apiUrl + "api/common/getEmployeeLeavesDetails?el_number=" + user).then(function (getEmployeeLeavesDetails_Data) {
                    $scope.EmployeeLeavesDetails = getEmployeeLeavesDetails_Data.data;
                    console.log($scope.EmployeeLeavesDetails);

                    if ($scope.EmployeeLeavesDetails < 1) {
                        $scope.NodataLeave = true;

                    }
                    else {

                        $scope.LeaveData = true;
                    }
                });

                $http.get(ENV.apiUrl + "api/common/getEmployeeClassDetails?em_login_code=" + user).then(function (getEmployeeClassDetails_Data) {
                    $scope.EmployeeClassDetails = getEmployeeClassDetails_Data.data;
                    console.log($scope.EmployeeClassDetails);

                    if ($scope.EmployeeClassDetails < 1) {
                        $scope.NodataClass = true;

                    }
                    else {

                        $scope.ClassData = true;
                    }
                });
            }

            $scope.timeoutlogout = function () {
                AuthenticationService.ClearCredentials();
                $('#idlepop').modal('hide');
                setTimeout(function () {
                    $state.go("login");
                }, 500)

                //window.localStorage["Finn_comp"]
                window.localStorage.removeItem("ReportProvNumber");
            }

            $scope.logout = function () {
               

                if (window.localStorage["microsoft_flg"]) {
                    $scope.microsoftLogout()
                   // $scope.signOutnew();
                    
                    $state.go("login");
                    

                }
                else if ($http.defaults.headers.common['schoolId'] == 'tiadxb' || $http.defaults.headers.common['schoolId'] == 'tosdxb' || $http.defaults.headers.common['schoolId'] == 'tiashj' || $http.defaults.headers.common['schoolId'] == 'staging2' || $http.defaults.headers.common['schoolId'] == 'staging')
                {
                    localStorage.clear();                    
                    AuthenticationService.ClearCredentials();
                    $scope.microsoftLogout()
                    window.open("https://eportal.leamseducation.com", "_self");

                }
                else {
                    AuthenticationService.ClearCredentials();
                    $state.go("login");

                    //window.localStorage["Finn_comp"]
                   
                }
                window.localStorage.removeItem("ReportProvNumber");
                window.localStorage.removeItem("Finn_comp");
                window.localStorage.removeItem("microsoft_flg");
            }




            $('body').addClass('grey condense-menu');
            $('#main-menu').addClass('mini');
            $('.page-content').addClass('condensed');
            $rootScope.isCondensed = true;

            $scope.temp_notifications = [];
            $scope.notifications = [];
            $timeout(function () {
                $("#main-menu-wrapper").scrollbar();
            }, 100);


            //emirates id code start////

            $scope.personaltab = true;
            $scope.contacttab = false;
            $scope.othertab = false;

            $scope.connectCard = function () {
                var Ret = Initialize();
                if (Ret == false)
                    return;
                Ret = ReadPublicData(true, true, true, true, true, true);
                if (Ret == false)
                    return;


                $scope.idncn = GetEF_IDN_CN();
                $scope.nonmodidata = GetEF_NonModifiableData();
                $scope.modidata = GetEF_ModifiableData();
                $scope.photo = GetEF_Photography();
                $scope.signimg = GetEF_HolderSignatureImage();
                $scope.homeadd = GetEF_HomeAddressData();
                $scope.workadd = GetEF_WorkAddressData();
                $scope.rootcert = GetEF_RootCertificate();



                var data = {
                    idn_cn: $scope.idncn,
                    non_mod_data: $scope.nonmodidata,
                    mod_data: $scope.modidata,
                    sign_image: $scope.signimg,
                    photo: $scope.photo,
                    root_cert: $scope.rootcert,
                    home_address: $scope.homeadd,
                    work_address: $scope.workadd,

                }

                $http.post(ENV.apiUrl + "api/nationalid/EIDRecord", data).then(function (res) {
                    $scope.obj1 = res.data[0];
                    
                    console.log($scope.obj1);
                    $scope.photoimg = $scope.obj1.srcphoto;
                    $scope.signphoto = $scope.obj1.srcsign;
                    $("#emiratesmodal").modal('show');
                });
            }

            $scope.personalClick = function () {
                if ($scope.personaltab) {
                    $scope.personaltab = false;
                    $scope.contacttab = true;
                    $scope.othertab = false;
                } else {
                    $scope.personaltab = true;
                    $scope.contacttab = false;
                    $scope.othertab = false;
                }
            }
            $scope.contactClick = function () {
                if ($scope.contacttab) {
                    $scope.personaltab = false;
                    $scope.contacttab = false;
                    $scope.othertab = true;
                } else {
                    $scope.personaltab = false;
                    $scope.contacttab = true;
                    $scope.othertab = true;
                }
            }

            $scope.otherClick = function () {
                if ($scope.othertab) {
                    $scope.personaltab = true;
                    $scope.contacttab = false;
                    $scope.othertab = false;
                } else {
                    $scope.personaltab = true;
                    $scope.contacttab = false;
                    $scope.othertab = true;
                }
            }


            $scope.saveData = function () {

                var sendData = {
                    CardNumber: $scope.obj1.cardNumber,
                    IDN: $scope.obj1.idn,
                    IdType: $scope.obj1.idType,
                    FullName: $scope.obj1.fullName,
                    FulllNameAr: $scope.obj1.fullName_ar,
                    FamilyId: $scope.obj1.familyId,
                    motherfullname: $scope.obj1.motherName,
                    motherfullnamear: $scope.obj1.motherName_ar,
                    DoB: $scope.obj1.dob,
                    Sex: $scope.obj1.sex,
                    photo: '',
                    sign_image: '',
                    Nationality: $scope.obj1.nationality,
                    NatinalityAr: $scope.obj1.nationality_ar,
                    MaritalStatus: $scope.obj1.maritalStatus,
                    HusbandIDN: $scope.obj1.husbandIDN,
                    CardIssueDate: $scope.obj1.issueDate,
                    CardExpiryDate: $scope.obj1.expiryDate,
                    Occupation: $scope.obj1.occupation,
                    OccupationField: $scope.obj1.occupationField,
                    OccupationType: $scope.obj1.occupationType,
                    OccupationTypeAr: $scope.obj1.occupationTypeAr,
                    ResidencyNumber: $scope.obj1.residencyNumber,
                    ResidencyType: $scope.obj1.residencyType,
                    ResidencyExpiryDate: $scope.obj1.residencyExpiryDate,
                    SponsorName: $scope.obj1.sponsorName,
                    SponsorNo: $scope.obj1.sponsorUnifiedNumber,
                    SponsorType: $scope.obj1.sponsorType,
                    Title: $scope.obj1.title,
                    TitleAr: $scope.obj1.title_ar,
                    FlatNo: $scope.obj1.flatNo,
                    Street: $scope.obj1.street,
                    Area: $scope.obj1.area,
                    BldgName: $scope.obj1.bldgName,
                    City: $scope.obj1.city,
                    Email: $scope.obj1.email,
                    ResPhone: $scope.obj1.residentPhone,
                    Mobile: $scope.obj1.mobile,
                    POBox: $scope.obj1.poBox,
                    EmirateDesc: $scope.obj1.emirateDesc,
                    companyname: $scope.obj1.companyname,
                    companyname_ar: $scope.obj1.companyname_ar,
                    DateofGraduation: $scope.obj1.dateOfGraduation,
                    Degree: $scope.obj1.degree,
                    DegreeAr: $scope.obj1.degreeAr,
                    FieldofStudy: $scope.obj1.fieldofStudy,
                    FieldofStudyAr: $scope.obj1.fieldofStudyAr,
                    PassportNumber: $scope.obj1.passportNumber,
                    PassportCountry: $scope.obj1.passportCountry,
                    PassportCountryDesc: $scope.obj1.passportCountryDesc,
                    PassportCountryDescAr: $scope.obj1.passportCountryAr,
                    PassportType: $scope.obj1.passportType,
                    PassportIssueDate: $scope.obj1.passportIssueDate,
                    PassportExpiryDate: $scope.obj1.passportExpiryDate,
                    placeofbirth: $scope.obj1.placeofbirth,
                    placeofbirthar: $scope.obj1.placeofbirthAr,
                    Placeofstudy: $scope.obj1.placeofstudy,
                    Placeofstudyar: $scope.obj1.placeofstudy_ar,
                    QualificationLevel: $scope.obj1.qualificationLevel,
                    QualificationLevelAr: $scope.obj1.qualificationLevelAr,
                    SponsorUnifiedNo: $scope.obj1.sponsorUnifiedNo,
                    workaddress: $scope.obj1.workAddress,
                    CreationUser: $rootScope.globals.currentUser.username,
                    CreationDate: '',
                    UserName: 'student'
                }

                $http.post(ENV.apiUrl + "api/nationalid/InsertEIDCardDetails", sendData).then(function (res) {
                    $scope.result = res.data;
                    if ($scope.result) {
                        swal('Success', 'Records Saved Successfully');
                        $("#emiratesmodal").modal('hide');
                    } else {
                        swal('Error', 'Records Not Saved');
                    }
                });
            }


            $scope.ar_en_fun=function(appl_code)
            {
                $http.get(ENV.apiUrl + "api/common/getEngAr?appl_code="+appl_code).then(function (res) {
                    
                    var daa = res.data;
                    var lang = {
                    };
                    for (var i = 0; i < res.data.length; i++) {
                        lang[res.data[i].sims_english] = res.data[i].sims_arabic;

                    }
                
                    gettextCatalog.setStrings('ar', lang);
                });
            }

            $scope.ar_en_fun('');

            $scope.language_application = function () {
                var t=$state.current.name.split('.');
                if (t.length > 0) {

                    $scope.current_appcode = t[1];
                    $rootScope.current_appcode = t[1];

                    $state.go('main.UserLa');
                }

            }
            var dataforUpdate = [];
            $scope.CancelAlert = function (str) {
                
                 var data = {};
                data.opr ="K";
                data.alertno = str;
                dataforUpdate.push(data);
                //dataupdate.push(data);
                $http.post(ENV.apiUrl + "api/common/Alert/UAlertStatus", dataforUpdate).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {

                        $scope.goAlert();
                        $http.get(ENV.apiUrl + "api/common/getUserAllDetails?uname=" + $rootScope.globals.currentUser.username).then(function (res) {

                            $scope.school_img_url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/SchoolLogo/'
                            if (res.data.group_Code == '06') {

                                $scope.flg_ck_group = true;
                            }
                            else {
                                $scope.flg_ck_group = false;
                                if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                                    if ('EMP004005' == $rootScope.globals.currentUser.username) {
                                        $scope.flg_ck_group = true;
                                    }
                                }

                            }
                            $scope.user_details = res.data;
                            $scope.circularobj = res.data;
                            $scope.newsobj = res.data;
                            $scope.alert_count = res.data.userAlertCount;


                            if ($scope.user_details['lic_school_logo'] == "" || $scope.user_details['lic_school_logo'] == undefined) {
                                $http.get(ENV.apiUrl + "api/common/getSchoolDetails").then(function (res) {
                                    $scope.user_details['lic_school_logo'] = $scope.obj1.lic_school_logo;
                                    $scope.user_details['lic_school_name'] = $scope.obj1.lic_school_name;
                                    $scope.user_details['lic_school_other_name'] = $scope.obj1.lic_school_other_name;
                                });

                            }


                            if ($scope.user_details.group_Code == "06") {
                                $scope.hideMyAccountLink = true;
                            }

                            $scope.uimage = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/' + $scope.user_details.user_image;

                            if ($scope.user_details.user_image == "" || $scope.user_details.user_image == undefined || $scope.user_details.user_image == null) {
                                $('#userImage').attr('src', 'assets/img/profile.png');
                            }
                            else {
                                $('#userImage').attr('src', $scope.uimage);
                            }

                        });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                    }
                 



                });
            }
             
            $scope.ClearAll = function () {
              
                var data = {};
                data.opr = "X";
                data.user = $rootScope.globals.currentUser.username;
                dataforUpdate.push(data);
                //dataupdate.push(data);
                $http.post(ENV.apiUrl + "api/common/Alert/UAlertAllStatus", dataforUpdate).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {

                        $scope.goAlert();
                        $http.get(ENV.apiUrl + "api/common/getUserAllDetails?uname=" + $rootScope.globals.currentUser.username).then(function (res) {

                            $scope.school_img_url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/SchoolLogo/'
                            if (res.data.group_Code == '06') {

                                $scope.flg_ck_group = true;
                            }
                            else {
                                $scope.flg_ck_group = false;
                                if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                                    if ('EMP004005' == $rootScope.globals.currentUser.username) {
                                        $scope.flg_ck_group = true;
                                    }
                                }

                            }
                            $scope.user_details = res.data;
                            $scope.circularobj = res.data;
                            $scope.newsobj = res.data;
                            $scope.alert_count = res.data.userAlertCount;


                            if ($scope.user_details['lic_school_logo'] == "" || $scope.user_details['lic_school_logo'] == undefined) {
                                $http.get(ENV.apiUrl + "api/common/getSchoolDetails").then(function (res) {
                                    $scope.user_details['lic_school_logo'] = $scope.obj1.lic_school_logo;
                                    $scope.user_details['lic_school_name'] = $scope.obj1.lic_school_name;
                                    $scope.user_details['lic_school_other_name'] = $scope.obj1.lic_school_other_name;
                                });

                            }


                            if ($scope.user_details.group_Code == "06") {
                                $scope.hideMyAccountLink = true;
                            }

                            $scope.uimage = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/' + $scope.user_details.user_image;

                            if ($scope.user_details.user_image == "" || $scope.user_details.user_image == undefined || $scope.user_details.user_image == null) {
                                $('#userImage').attr('src', 'assets/img/profile.png');
                            }
                            else {
                                $('#userImage').attr('src', $scope.uimage);
                            }

                        });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                    }
                 



                });
            }
         }])


})();