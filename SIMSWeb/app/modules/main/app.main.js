﻿(function () {
    'use strict';
    angular.module('sims.module.main', [
        'sims',
        'gettext'
    ]);
})();