﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CFRCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            // Start
      
            $('*[data-datepicker="true"] input[type="text"]').datepicker(
                {
                    todayBtn: true,
                    orientation: "top left",
                    autoclose: true,
                    todayHighlight: true,
                    format: 'dd-mm-yyyy'
                });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            function pad(num) {
                num = num + '';
                return num.length < 2 ? '0' + num : num;
            }
            function customDate_format(date) {
                //return date.getFullYear() + '-' +
                //    pad(date.getMonth() + 1) + '-' +
                //    pad(date.getDate());
                return  pad(date.getDate()) + '-' +
                    pad(date.getMonth() + 1) + '-' +
                   date.getFullYear();
            }
            $scope.backTofees = function () {
                $state.go('main.Sim043');
            }
            $scope.display = false;
            $scope.table = false;
            $scope.username = $rootScope.globals.currentUser.username;
            $scope.isResultEmpty = false;
            $scope.isResult = false;
            $scope.enroll_number = '';
            $scope.receipt_start_date = '';
            $scope.receipt_end_date = '';
            var date = new Date();
            $scope.receipt_cancel_date = customDate_format(date);
            $scope.disabled_opt = true;
            $scope.receiptData = [];
            // End

            //Events Start
            $http.get(ENV.apiUrl + "api/Fee/CancelFeeReceipt/getCFRYear").then(function (res) {
                $scope.academic_year = res.data;
                for (var x = 0; x < $scope.academic_year.length; x++) {
                    if ($scope.academic_year[x].sims_academic_year_status == 'C') {
                        $scope.edt = { sims_academic_year: $scope.academic_year[x].sims_academic_year }
                        break;
                    }
                }
                })

            function _onFetchData() {
                
                var main = document.getElementById('mainchk');
                main.checked = false;
                if ($scope.enroll_number == undefined)
                    $scope.enroll_number = '';

                $http.get(ENV.apiUrl + "api/Fee/CancelFeeReceipt/getCFRReceipts?academic_year=" + $scope.edt.sims_academic_year + "&user_name=" + $scope.username + "&enroll_list=" + $scope.enroll_number + "&start_date=" + $scope.receipt_start_date + "&end_date=" + $scope.receipt_end_date).then(function (res) {
                    
                    $scope.receiptData = res.data;
                    $scope.isResultEmpty = res.data.length <= 0 ? false : true;
                    $scope.disabled_opt = res.data.length > 0 ? false : true;
                    $scope.cancelReuest = res.data.length > 0 ? false : true;
                    $scope.isResult = res.data.length <= 0 ? true : false;
                    if (res.data.length <= 0) {
                        swal({
                            text: 'Receipt(s) Data Not Found.',
                            imageUrl: "assets/img/close.png",
                            width: 400,
                            height: 300
                        });
                    }
                   })
            }
            $scope.fetchDatakeyPress = function ($event) {
                if ($scope.enroll_number == undefined) $scope.enroll_number = '';
                if ($event.keyCode == 13) {
                    if ($scope.enroll_number.length != 0) {
                        _onFetchData();
                    }
                    else {
                        swal({
                            text: 'Please Enter Student ID.',
                            imageUrl: "assets/img/notification-alert.png",
                            width: 400,
                            height: 300
                        });
                    }
                }
            }
            $scope.btnPreview_click = function () {
                _onFetchData();
            }

            $scope.btnReset_click = function () {
                $scope.cancelReuest = false;
                $scope.receipt_start_date = '';
                $scope.receipt_end_date = '';
                $scope.enroll_number = '';
                $scope.edt = { sims_academic_year: $scope.academic_year[0].sims_academic_year }
                $scope.receiptData = [];
                var main = document.getElementById('mainchk');
                main.checked = false;
            }
         
            //$scope.cancelRequest = function () {               
                
            //    $scope.cancelReuest = true;

            //    var selected_receiptData = [];
            //    var flag = false;
            //    for (var i = 0; i < $scope.receiptData.length; i++) {
            //        var t = $scope.receiptData[i].sims_receipt_no;
            //        var v = document.getElementById(t);
            //        if (v.checked == true) {
            //            selected_receiptData.push($scope.receiptData[i]);
            //            flag = true;
            //        }
            //    }
            //    if (flag == false) {
            //        swal({
            //            text: 'Please Select At Least One Receipt for Cancellation.',
            //            imageUrl: "assets/img/notification-alert.png",
            //            width: 400,
            //            height: 300
            //        });
                    
            //    }
            //    else {
            //        $http.post(ENV.apiUrl + "api/Fee/CancelFeeReceipt/CFRCancelReceipts?cancel_doc_date=" + $scope.receipt_cancel_date + "&user_name=" + $scope.username, selected_receiptData).then(function (res) {
            //            if (res.data == true) {
            //                swal({
            //                    text: 'Selected Receipt(s) are Cancelled Sucessfully.',
            //                    imageUrl: "assets/img/check.png",
            //                    width: 400,
            //                    height: 300
            //                });
            //                var selected_receiptData = [];
            //                $scope.receiptData = [];
            //                _onFetchData();
            //            }
            //            else {
            //                swal({
            //                    text: 'Error in while cancelling receipt(s).',
            //                    imageUrl: "assets/img/notification-alert.png",
            //                    width: 400,
            //                    height: 300
            //                });
            //                _onFetchData();
            //            }
            //        });
                    
            //        /**/
            //    }
            //    //$scope.dubbleClicked = false;
            //    //$scope.singleClicked = true;

            //}

            $scope.btnCancel_click = function () {
                debugger;
                $scope.cancelReuest = true;
                var selected_receiptData = [];
                var flag = false;
                for (var i = 0; i < $scope.receiptData.length; i++) {
                    var t = $scope.receiptData[i].sims_receipt_no;
                    var v = document.getElementById(t);
                    if (v.checked == true) {
                        selected_receiptData.push($scope.receiptData[i]);
                        flag = true;
                    }
                }
                if (flag == false) {
                    swal({
                        text: 'Please Select At Least One Receipt for Cancellation.',
                        imageUrl: "assets/img/notification-alert.png",
                        width: 400,
                        height: 300
                    });
                }
                else {
                    $http.post(ENV.apiUrl + "api/Fee/CancelFeeReceipt/CFRCancelReceipts?cancel_doc_date=" + $scope.receipt_cancel_date + "&user_name=" + $scope.username, selected_receiptData).then(function (res) {
                        if (res.data == true) {
                            swal({
                                text: 'Selected Receipt(s) are Cancelled Sucessfully.',
                                imageUrl: "assets/img/check.png",
                                width: 400,
                                height: 300
                            });
                            var selected_receiptData = [];
                            $scope.receiptData = [];
                            _onFetchData();
                        }
                        else {
                            swal({
                                text: 'Error in while cancelling receipt(s).',
                                imageUrl: "assets/img/notification-alert.png",
                                width: 400,
                                height: 300
                            });
                            _onFetchData();
                        }
                    });

                    /**/
                }
            }
            $scope.CheckMultiple = function () {
                var main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.receiptData.length; i++) {
                    var t = $scope.receiptData[i].sims_receipt_no;
                    var v = document.getElementById(t);
                    if (main.checked == true) {
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                    else {
                        v.checked = false;
                        $scope.row1 = '';

                    }
                }
            }
            $scope.check1 = function (sims_receipt_no) {
                var main = document.getElementById('mainchk');
                var check = document.getElementById(sims_receipt_no);
                var t = sims_receipt_no;
                var v = document.getElementById(t);
                if (v.checked == false) {
                    main.checked = false;
                }
            }
            //Events End


            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.receipt_start_date= dd + '-' + mm + '-' + yyyy;
            $scope.receipt_end_date= dd + '-' + mm + '-' + yyyy;


        }])
})();

