﻿(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [];
    var doc_no;
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('FeeCollectionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            var user = $rootScope.globals.currentUser.username;
            $scope.table1 = true;
            $scope.busy = false;
            $scope.rgvtbl = false;
            $scope.edt = {};


            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm1 = today.getMonth() + 1; //January is 0!
            var mm = today.getMonth() + 1; //January is 0!

            if (mm1 > 4) {
                mm1 = (mm - 3);
            }
            if (mm1 == 1) {
                mm1 = 10;
            }
            if (mm1 == 2) {
                mm1 = 11;
            }
            if (mm1 == 3) {
                mm1 = 12;
            }
            if (mm1 == 4) {
                mm1 = 1;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            if (mm1 < 10) {
                mm1 = '0' + mm1;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm1 + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.edt = {
                from_date: dd + '-' + mm1 + '-' + yyyy,
                to_date: dd + '-' + mm + '-' + yyyy,
            }

            $http.post(ENV.apiUrl + "api/FeeReceipt/Cashier").then(function (Cashier) {
                $scope.Cashier = Cashier.data;
            });

            $http.post(ENV.apiUrl + "api/FeeReceipt/PaymentMode").then(function (PaymentMode) {
                $scope.PaymentMode = PaymentMode.data;
            });

            $http.post(ENV.apiUrl + "api/FeeReceipt/FeeType").then(function (FeeType) {
                $scope.FT = FeeType.data;
            });


            //$('*[data-datepicker="true"] input[type="text"]').datepicker({
            //    todayBtn: true,
            //    orientation: "top left",
            //    autoclose: true,
            //    todayHighlight: true,
            //    format: "yyyy-mm-dd"
            //});

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


           

            $scope.Show_data = function () {

                if ($scope.edt.from_date == undefined || $scope.edt.from_date == '' || $scope.edt.to_date == undefined || $scope.edt.to_date == '') {
                    swal('', 'From date and to date are mandatory.');
                }
                else {


                    var data = {
                        from_date: $scope.edt.from_date,
                        to_date: $scope.edt.to_date,
                        cashier_code: $scope.edt.cashier_code,
                        payment_mode_code: $scope.edt.payment_mode_code,
                        enroll_number: $scope.edt.search
                    }
                    $scope.busy = true;
                    $scope.rgvtbl = false;
                    $http.post(ENV.apiUrl + "api/FeeReceipt/FeeCollection", data).then(function (FeeCollection) {
                        $scope.FeeCollection = FeeCollection.data;
                        $scope.header_data = [];

                        angular.forEach($scope.FeeCollection[0], function (value, key) {
                            $scope.header_data.push({ name: key });
                        });
                        if ($scope.FeeCollection.length > 0) {
                            $scope.busy = false;
                            $scope.rgvtbl = true;
                        }
                        else {
                            $scope.busy = false;
                            $scope.rgvtbl = false;
                            swal('', 'Record not found.');
                        }
                        $timeout(function () {
                            $("#fixTable").tableHeadFixer({ 'top': 1 });
                        }, 100);
                    });
                }
            }
                     
             $scope.exportData = function () {
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('rpt_data').innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        saveAs(blob, "FeeCollection.xls");
                        $scope.colsvis = false;
                    }

                });
             };


            $scope.Reset_data = function () {
                $scope.busy = false;
                $scope.rgvtbl = false;
                $scope.edt = {};

            }



            $scope.Report = function (str) {

                var data = {
                    location: 'Sims.SIMR51FeeRec',
                    parameter: {
                        fee_rec_no: str['doc name'],
                    },
                    state: 'main.Sim700',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
            }

        }]
        )
})();