﻿/// <reference path="StudentFeeConcessionCont.js" />
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('FeeCategoryChangeController',
        ['$scope', '$state', '$rootScope', '$timeout', '$stateParams', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, $stateParams, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            // Start

            $scope.propertyName = null;
            $scope.reverse = false;
            var user = $rootScope.globals.currentUser.username.toLowerCase();
            $scope.sortBy = function (propertyName) {

                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker(
                {
                    todayBtn: true,
                    orientation: "top left",
                    autoclose: true,
                    todayHighlight: true,
                    //format: 'yyyy-mm-dd'
                    format: 'dd-mm-yyyy'
                });
            var op = $stateParams.sel;
            if (op != undefined) {
                try {
                    $scope.edt = {};
                    $scope.edt.sims_cur_code = op.cur;
                    $scope.edt.sims_academic_year = op.ac;
                    $scope.edt.sims_grade_code = op.gr;
                    $scope.edt.sims_section_code = op.sc;
                    $scope.edt.search_txt = op.st;
                    //_onFetchData();
                } catch (e) {

                }
            }

           

            $scope.gotoCancelReceipt = function () {
                $state.go('main.Sm043C');
            }
            var sarr = ['adis', 'sbis', 'pearl'];
            var smfb = ['smfb', 'smfc'];

          
            //params: { 'StudCurr': '', 'StudAcademic': '', 'StudEnroll': '' }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '', sims_search_code: '' };
            /*FILTER*/
            $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllCurName").then(function (CurData) {
                $scope.CurData = CurData.data;
                $scope.edt['sims_cur_code'] = CurData.data[0].sims_cur_code;
                $scope.cur_code_change(CurData.data[0].sims_cur_code);

            });
            $http.get(ENV.apiUrl + "api/Fee/SFS/getSeachOptions").then(function (searchoptions) {
                $scope.searchoptions = searchoptions.data;

                for (var i = 0; i < $scope.searchoptions.length; i++) {
                    if ($scope.searchoptions[i].sims_search_code_status == 'Y') {
                        $scope.edt['sims_search_code'] = $scope.searchoptions[i].sims_search_code;
                    }
                }
            });
            $scope.currentYear_status = 'C';
            $scope.cur_code_change = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + $scope.edt.sims_cur_code).then(function (AcademicYears) {
                    $scope.AcademicYears = AcademicYears.data;
                    //for (var i = 0; i < $scope.AcademicYears.length; i++) {
                    //    if ($scope.AcademicYears[i].sims_academic_year_status == 'C') {
                    //        //   $scope.edt['sims_cur_code'] = cur_values;
                    //        $scope.edt['sims_academic_year'] = $scope.AcademicYears[0].sims_academic_year;
                    //    }
                    //}
                    $scope.edt['sims_academic_year'] = $scope.AcademicYears[0].sims_academic_year;
                    $scope.academic_year_change();
                });
            }
            $scope.academic_year_change = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year).then(function (Grades) {
                    $scope.Grades = Grades.data;
                });

            }
            $scope.grade_change = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllSections?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year + "&g_code=" + $scope.edt.sims_grade_code).then(function (sections) {
                    $scope.sections = sections.data;
                    console.log($scope.sections);

                });

            }

            $scope.reset = function () {
               
                //$scope.edt = { 'sims_grade_code': '', 'sims_section_code': '', 'search_txt': '' };
                $scope.edt['sims_grade_code'] = '';
                $scope.edt['sims_section_code'] = '';
                $scope.edt['search_txt'] = '';
                $scope.feesData = '';
                $scope.todos = $scope.feesData;
                $scope.makeTodos();
                $scope.current_obje = '';
            }

            /* PAGER FUNCTIONS*/

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 9, $scope.maxSize = 7, $scope.currentPage_ind = 0;
            $scope.makeTodos = function () {
                $scope.currentPage_ind = 0;

                if ((Math.round($scope.totalItems / $scope.numPerPage) * $scope.numPerPage) > $scope.totalItems)
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage);
                else
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage) + 1;

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.currentPage_ind = str;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage_ind = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                console.log("PageNumber=" + $scope.numPerPage);
                $scope.makeTodos();
            }

            /* PAGER FUNCTIONS*/

            function _onFetchData() {
                if ($scope.edt.search_txt == undefined)
                    $scope.edt.search_txt = '';
                $http.get(ENV.apiUrl + "api/Fee/SFS/getFCCStudents?cc=" + $scope.edt.sims_cur_code + "&ay=" + $scope.edt.sims_academic_year + "&gc=" + $scope.edt.sims_grade_code + "&sc=" + $scope.edt.sims_section_code + "&search_stud=" + $scope.edt.search_txt ).then(function (feesData) {
                    $scope.feesData = feesData.data;
                    if ($scope.feesData.length <= 0) {
                        swal(
                            {
                                showCloseButton: true,
                                text: 'No Student Data found.',
                                imageUrl: "assets/img/close.png",
                                width: 350,
                                showCloseButon: true
                            });
                    }
                    else
                    {
                        $scope.totalItems = $scope.feesData.length;
                        $scope.todos = $scope.feesData;
                        $scope.makeTodos();
                    }
                });
            }

            $scope.fetchDatakeyPress = function ($event) {
                if ($event.keyCode == 13) {
                    _onFetchData();
                }
            }

            $scope.btnPreview_click = function () {
                _onFetchData();
            }
        
            $scope.current_obje = '';
            $scope.ChangeCategory = function (info) {
                $scope.current_obje = '';
                $scope.current_obje = info;

                $scope.student_name = info.std_fee_student_name;
                $scope.enroll_number = info.std_fee_enroll_number;
                debugger
                $http.get(ENV.apiUrl + "api/Fee/SFS/getStudentFeeCategory?cc=" + info.std_fee_cur_code + "&ay=" + info.std_fee_academic_year + "&gc=" + info.std_fee_grade_code + "&sc=" + info.std_fee_section_code + "&search_stud=" + info.std_fee_enroll_number).then(function (StudentCategory) {
                    debugger
                    $scope.StudentCategory = StudentCategory.data;

                    if ($scope.StudentCategory.length == 1) {
                        $scope.edtch =
                            {
                                sims_old_cat_code: $scope.StudentCategory[0].std_fee_category_code
                            }
                    }

                });

                $http.get(ENV.apiUrl + "api/Fee/SFS/getAllFeeCategory").then(function (AllCategory) {
                    $scope.AllCategory = AllCategory.data;
                });

                $('#MyModal').modal({ backdrop: 'static', keyboard: false });
            }
            $scope.edtch = { sims_old_cat_code: '', sims_new_cat_code: ''};

            $scope.fnchangecat = function () {
                //changeCategory
                debugger;
                $http.post(ENV.apiUrl + "api/Fee/SFS/changeCategory?oldCate=" + $scope.edtch.sims_old_cat_code + "&newCat=" + $scope.edtch.sims_new_cat_code + "&user_name=" + user, $scope.current_obje).then(function (result) {
                    $('#MyModal').modal('hide');
                    $scope.result = result.data;
                    if ($scope.result = "true") {
                        swal(
                           {
                               showCloseButton: true,
                               text: 'Student Fee Category Changed Successfully.',
                               imageUrl: "assets/img/check.png",
                               width: 350,
                               showCloseButon: true
                           });
                    }
                    /*else if (result.data == "NC") {
                        swal(
                           {
                               showCloseButton: true,
                               text: 'No Change in Fee Category.',
                               imageUrl: "assets/img/close.png",
                               width: 350,
                               showCloseButon: true
                           });
                    }*/
                    else {
                        swal(
                           {
                               showCloseButton: true,
                               text: 'Error while Updating Student Fee Category.',
                               imageUrl: "assets/img/notification-alert.png",
                               width: 350,
                               showCloseButon: true
                           });
                    }
                });
            }

            $scope.closeAdv = function () {

                $('#MyModal').modal('hide');
                $scope.current_obje = '';
            }
            //Events End
        }])
})();

