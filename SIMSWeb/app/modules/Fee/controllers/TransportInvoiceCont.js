﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TransportInvoiceCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            // Start
            $('*[data-datepicker="true"] input[type="text"]').datepicker(
                {
                    todayBtn: true,
                    orientation: "top left",
                    autoclose: true,
                    todayHighlight: true,
                    format: 'yyyy-mm-dd'
                });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '' };;

            $scope.info = [];

            $scope.edt1 = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '' };

            /*FILTER*/

            $http.get(ENV.apiUrl + "api/StudentReport/GetAllCurName").then(function (CurData) {
                $scope.CurData = CurData.data;
                $scope.edt['sims_cur_code'] = CurData.data[0].sims_cur_code;
                $scope.edt1['sims_cur_code'] = CurData.data[0].sims_cur_code;
                $scope.cur_code_change(CurData.data[0].sims_cur_code);

            });

            $scope.currentYear_status = 'C';

            $scope.cur_code_change = function (cur_values) {
                $http.get(ENV.apiUrl + "api/StudentReport/GetAllAcademicYears?cur_code=" + cur_values).then(function (AcademicYears) {
                    $scope.AcademicYears = AcademicYears.data;
                    for (var i = 0; i < $scope.AcademicYears.length; i++) {
                        if ($scope.AcademicYears[i].sims_academic_year_status == 'C' || $scope.AcademicYears[i].sims_academic_year_status == 'Current') {
                            $scope.edt['sims_academic_year'] = $scope.AcademicYears[i].sims_academic_year;
                            $scope.edt1['sims_academic_year'] = $scope.AcademicYears[i].sims_academic_year;
                        }
                    }

                    $scope.academic_year_change(cur_values, $scope.edt.sims_academic_year);

                    $http.get(ENV.apiUrl + "api/StudentReport/Getsims541_InvoiceMode").then(function (InvoiceMode) {
                        $scope.InvoiceMode = InvoiceMode.data;
                        $scope.edt['sims_invoce_mode_code'] = $scope.InvoiceMode[0].sims_invoce_mode_code;
                        $scope.InvoiceMode_change();
                    });

                });
            }

            $scope.academic_year_change = function (cur_values, academic_year) {

                $http.get(ENV.apiUrl + "api/StudentReport/GetAllGrades?cur_code=" + cur_values + "&ac_year=" + academic_year).then(function (Grades) {
                    $scope.Grades = Grades.data;
                });
                //$http.get(ENV.apiUrl + "api/StudentReport/GetAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year).then(function (Grades) {
                //    $scope.Grades = Grades.data;
                //});

            }

            $scope.grade_change = function (cur_values, academic_year, grade) {
                $http.get(ENV.apiUrl + "api/StudentReport/GetAllSections?cur_code=" + cur_values + "&ac_year=" + academic_year + "&g_code=" + grade).then(function (sections) {
                    $scope.sections = sections.data;
                });
                //$http.get(ENV.apiUrl + "api/StudentReport/GetAllSections?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year + "&g_code=" + $scope.edt.sims_grade_code).then(function (sections) {
                //    $scope.sections = sections.data;


                //});

            }

            $scope.section_change = function () {
                //if ($scope.feeonInvoice == true)
                {
                    $http.get(ENV.apiUrl + "api/StudentReport/Getsims541_FeeTypes?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&grade_code=" + $scope.edt.sims_grade_code + "&section_code=" + $scope.edt.sims_section_code).then(function (fee_type) {
                        $scope.fee_type = fee_type.data;
                        $scope.edt = {
                            sims_fee_code: $scope.fee_type[0].sims_fee_code,
                            sims_cur_code: $scope.edt.sims_cur_code,
                            sims_academic_year: $scope.edt.sims_academic_year,
                            sims_grade_code: $scope.edt.sims_grade_code,
                            sims_section_code: $scope.edt.sims_section_code,
                            sims_invoce_mode_code: $scope.edt.sims_invoce_mode_code,
                            sims_invoce_frq_name_value: $scope.edt.sims_invoce_frq_name_value
                        }
                        $scope.edt1 = {
                            //sims_fee_code: $scope.fee_type[0].sims_fee_code,
                            sims_cur_code: $scope.edt.$scope.edt1.sims_cur_code,
                            sims_academic_year: $scope.edt1.sims_academic_year,
                            sims_grade_code: $scope.edt1.sims_grade_code,
                            sims_section_code: $scope.edt1.sims_section_code,
                            //sims_invoce_mode_code: $scope.edt1.sims_invoce_mode_code,
                            //sims_invoce_frq_name_value: $scope.edt1.sims_invoce_frq_name_value
                        }
                    });
                }
            }

            $scope.InvoiceValue_change = function (str) {
                $scope.edt.sims_invoce_frq_name_value = str;

            }

            $http.get(ENV.apiUrl + "api/StudentReport/Getsims541_InvoiceReportUrl").then(function (InvoiceReport) {
                $scope.InvoiceReport = InvoiceReport.data;
            });

            $scope.InvoiceMode_change = function () {

                $http.get(ENV.apiUrl + "api/StudentReport/Getsims541_FrequencyValue?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&freq_flag=" + $scope.edt.sims_invoce_mode_code).then(function (FeeFreqValue) {
                    $scope.FeeFreqValue = FeeFreqValue.data;
                    $scope.edt['sims_invoce_frq_name_value'] = $scope.FeeFreqValue[0].sims_invoce_frq_name_value;
                });

                $http.get(ENV.apiUrl + "api/StudentReport/Getsims541_FeeTypes?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&grade_code=" + '' + "&section_code=" + '').then(function (fee_type) {
                    $scope.fee_type = fee_type.data;
                    $scope.edt['sims_fee_code'] = $scope.fee_type[0].sims_fee_code;
                });
            }

            //$scope.feeonInvoice = false;

            //$http.get(ENV.apiUrl + "api/StudentReport/Getsims541_CreationOnFee").then(function (InvoiceOnFee) {
            //    debugger;
            //    $scope.InvoiceOnFee = InvoiceOnFee.data;
            //    if ($scope.InvoiceOnFee == 'N' || $scope.InvoiceOnFee == '')
            //        $scope.feeonInvoice = false;
            //    else
            //        $scope.feeonInvoice = true;
            //});

            /* PAGER FUNCTIONS*/

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 7, $scope.currentPage_ind = 0;

            $scope.makeTodos = function () {
                $scope.currentPage_ind = 0;

                if ((Math.round($scope.totalItems / $scope.numPerPage) * $scope.numPerPage) > $scope.totalItems)
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage);
                else
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage) + 1;

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                var main = document.getElementById('mainchk');
                main.checked = false;
                $scope.CheckMultiple();

            };

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.currentPage_ind = str;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage_ind = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }


            /* PAGER FUNCTIONS*/

            function _onFetchData() {
                if ($scope.edt.sims_grade_code == undefined) $scope.edt = { sims_grade_code: '' };
                if ($scope.edt.sims_section_code == undefined) $scope.edt = { sims_section_code: '' };
                if ($scope.edt.sims_enroll_number == undefined) $scope.edt.sims_enroll_number = '';
                if ($scope.edt.sims_fee_code == undefined) $scope.edt.sims_fee_code = '';
                var main = document.getElementById('mainchk');
                main.checked = false;

                $http.get(ENV.apiUrl + "api/StudentReport/Getsims541_InvoiceStudents?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&grade_code=" + $scope.edt.sims_grade_code + "&section_code=" + $scope.edt.sims_section_code + "&term_code=" + $scope.edt.sims_invoce_frq_name_value + "&search=" + $scope.edt.sims_enroll_number + "&fee_code_search=" + $scope.edt.sims_fee_code).then(function (InvoiceData) {
                    $scope.InvoiceData = InvoiceData.data;
                    $scope.totalItems = $scope.InvoiceData.length;
                    $scope.todos = $scope.InvoiceData;
                    $scope.filteredTodos = InvoiceData.data;
                    //$scope.currentPage = 0;
                    //  $scope.makeTodos();
                });
            }

            $scope.search = function ($event) {
                if ($event.keyCode == 13) {
                    _onFetchData();
                }
            }

            $scope.btnReset_click = function () {
                $scope.edt.sims_cur_code = '';
                $scope.edt.sims_academic_year = '';
                $scope.edt.sims_cur_code = '';
                $scope.edt.sims_invoce_mode_code = '';
                $scope.edt.sims_invoce_frq_name_value = '';
                $scope.edt.sims_grade_code = '';
                $scope.edt.sims_section_code = '';
                $scope.edt.sims_enroll_number = '';
                $scope.edt.sims_fee_code = '';
                $scope.todos = [];
                $scope.makeTodos();
            }

            $scope.btnPreview_click = function () {
                _onFetchData();
            }

            $scope.btnGenerate_invoice = function () {

                var isSelected = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].sims_invoice_status == true) {
                        isSelected = true; break;
                    }
                }
                if (isSelected == true) {
                    $http.post(ENV.apiUrl + "api/StudentReport/GenerateInvoiceForStudents?&term_code=" + $scope.edt.sims_invoce_frq_name_value + "&invoice_mode=" + $scope.edt.sims_invoce_mode_code + "&payingAgentFlag=N&fee_code_search=" + $scope.edt.sims_fee_code, $scope.filteredTodos).then(function (result) {
                        var result = result.data;
                        if (result == true) {
                            swal(
                            {
                                showCloseButton: true,
                                text: 'Invoice Generated Sucessfully',
                                width: 350,
                                imageUrl: "assets/img/check.png",
                                showCloseButon: true
                            });
                            _onFetchData();
                        }
                        else {
                            swal(
                           {
                               showCloseButton: true,
                               text: 'Error in Generating Invoice',
                               width: 350,
                               imageUrl: "assets/img/close.png",
                               showCloseButon: true
                           });
                        }
                    });
                }
                else {
                    swal({
                        
                        text: 'Please Select Student to Generate Invoice', width: 350, imageUrl: "assets/img/notification-alert.png",showCloseButon: true
                    });
                }
                var main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    //$scope.color = '#edefef';
                }
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);


            /*SEARCH INVOICES*/
            /*PAGER*/

            $scope.filteredTodos_invoice = [], $scope.currentPage_invoice = 1, $scope.numPerPage_invoice = 10, $scope.maxSize_invoice = 10;

            $scope.makeTodos_invoice = function () {
                if ((Math.round($scope.totalItems_invoice / $scope.numPerPage_invoice) * $scope.numPerPage_invoice) > $scope.totalItems_invoice)
                    $scope.pagersize_invoice = Math.round($scope.totalItems_invoice / $scope.numPerPage_invoice);
                else
                    $scope.pagersize_invoice = Math.round($scope.totalItems_invoice / $scope.numPerPage_invoice) + 1;
                var begin_invoice = (($scope.currentPage_invoice - 1) * $scope.numPerPage_invoice);
                var end_invoice = parseInt(begin_invoice) + parseInt($scope.numPerPage_invoice);
                $scope.filteredTodos_invoice = $scope.InvoiceDetailsData.slice(begin_invoice, end_invoice);
                var main = document.getElementById('chkSearch');
                main.checked = false;
                $scope.CheckMultipleSearch();

            };

            $scope.size_invoice = function (str) {
                $scope.pagesize_invoice = str;
                $scope.currentPage_invoice = 1;
                $scope.numPerPage_invoice = str;
                $scope.makeTodos_invoice();
            }

            $scope.index_invoice = function (str) {
                $scope.pageindex_invoice = str;
                $scope.currentPage_invoice = str;
                $scope.makeTodos_invoice();
            }
            /*PAGER*/

            $scope.seachInvoiceKeyPress = function ($event) {
                if ($event.keyCode == 13) {
                    $scope.seachInvoice(2);
                }
            }

            $scope.seachInvoice = function (flag) {

                $scope.hideIfEmpty_invoice = false;
                var search = $scope.info.sims_enroll_number;

                var main = document.getElementById('chkSearch');
                main.checked = false;
                $scope.info = [];
                if (flag != 1) {

                    $scope.info.sims_cur_code = $scope.edt.sims_cur_code;
                    $scope.info.sims_academic_year = $scope.edt.sims_academic_year;
                    $scope.info.sims_grade_code = $scope.edt1.sims_grade_code;
                    $scope.info.sims_enroll_number = $scope.edt1.sims_enroll_number;
                    // if ($scope.info.sims_enroll_number == undefined) $scope.info.sims_enroll_number = '';
                    $scope.info.sims_section_code = $scope.edt1.sims_section_code;

                }
                if (flag == 1) {

                    $scope.info.sims_cur_code = $scope.edt.sims_cur_code;
                    $scope.info.sims_academic_year = $scope.edt.sims_academic_year;
                    $scope.info.sims_grade_code = $scope.edt1.sims_grade_code;
                    $scope.info.sims_enroll_number = $scope.edt1.sims_enroll_number;
                    $scope.info.sims_section_code = $scope.edt1.sims_section_code;

                    //                    if ($scope.info.sims_enroll_number == undefined) $scope.info.sims_enroll_number = '';

                }


                if ($scope.info.sims_cur_code == undefined) $scope.info.sims_cur_code = '';
                if ($scope.info.sims_academic_year == undefined) $scope.info.sims_academic_year = '';
                if ($scope.info.sims_grade_code == undefined) $scope.info.sims_grade_code = '';
                if ($scope.info.sims_section_code == undefined) $scope.info.sims_section_code = '';

                if ($scope.info.sims_enroll_number == undefined) $scope.info.sims_enroll_number = '';

                if ($scope.info.sims_cur_code != undefined || $scope.info.sims_enroll_number != '') {

                    $http.post(ENV.apiUrl + "api/StudentReport/SearchInvoice?search_code=" + search + "&cur_code=" + $scope.info.sims_cur_code + "&academic_year=" + $scope.info.sims_academic_year + "&grade_code=" + $scope.info.sims_grade_code + "&section_code=" + $scope.info.sims_section_code + "&fee_code_search=" + $scope.edt.sims_fee_code).then(function (InvoiceDetailsData) {


                        if (InvoiceDetailsData.data.length <= 0) {
                            if (flag != 1) {
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'No Result Found',
                                    width: 350,
                                    imageUrl: "assets/img/close.png",
                                    showCloseButon: true
                                });
                            }
                            $scope.hideIfEmpty_invoice = false;
                            $scope.InvoiceDetailsData = [];
                        }
                        else {
                            $scope.hideIfEmpty_invoice = true;
                            $scope.InvoiceDetailsData = InvoiceDetailsData.data;
                            $scope.totalItems_invoice = $scope.InvoiceDetailsData.length;
                            $scope.filteredTodos_invoice = $scope.InvoiceDetailsData;
                            //$scope.currentPage_invoice = 0;
                            $scope.makeTodos_invoice();
                        }

                    });
                }
            }

            $scope.viewInvoice = function (info, flag) {
                //Default Cur Selection
                $http.get(ENV.apiUrl + "api/StudentReport/GetAllCurName").then(function (CurData) {
                    $scope.CurData = CurData.data;
                    $scope.edt1['sims_cur_code'] = CurData.data[0].sims_cur_code;
                    $http.get(ENV.apiUrl + "api/StudentReport/GetAllAcademicYears?cur_code=" + $scope.edt1['sims_cur_code']).then(function (AcademicYears) {
                        $scope.AcademicYears = AcademicYears.data;
                        for (var i = 0; i < $scope.AcademicYears.length; i++) {
                            if ($scope.AcademicYears[i].sims_academic_year_status == 'C' || $scope.AcademicYears[i].sims_academic_year_status == 'Current') {
                                $scope.edt1['sims_academic_year'] = $scope.AcademicYears[i].sims_academic_year;
                            }
                        }
                    });
                });


                $scope.hideIfEmpty_invoice = false;

                if (flag == 1) {
                    $http.post(ENV.apiUrl + "api/StudentReport/SearchInvoice?search_code=" + info.sims_enroll_number + "&cur_code=" + info.sims_cur_code + "&academic_year=" + info.sims_academic_year + "&grade_code=" + info.sims_grade_code + "&section_code=" + info.sims_section_code + "&fee_code_search=" + $scope.edt.sims_fee_code).then(function (InvoiceDetailsData) {
                        $scope.hideIfEmpty_invoice = true;
                        $scope.InvoiceDetailsData = InvoiceDetailsData.data;
                        $scope.totalItems_invoice = $scope.InvoiceDetailsData.length;
                        $scope.filteredTodos_invoice = $scope.InvoiceDetailsData;
                        $scope.currentPage_invoice = 1;
                        $scope.makeTodos_invoice();
                    });
                }
                else if (flag == 2) {
                    $scope.info.sims_enroll_number = '';
                    $scope.resetSeachInvoice();
                }
                $('#MyModal').modal('show');
            }

            $scope.resetSeachInvoice = function () {
                $scope.InvoiceDetailsData = [];
                $scope.filteredTodos_invoice = [];
                $scope.info.sims_enroll_number = '';
                $scope.edt1 = [];
            }

            function viewInvoice(info) {
                $http.post(ENV.apiUrl + "api/StudentReport/SearchInvoice?search_code=" + $scope.info.sims_enroll_number + "&cur_code=" + $scope.info.sims_cur_code + "&academic_year=" + $scope.info.sims_academic_year + "&grade_code=" + $scope.info.sims_grade_code + "&section_code=" + $scope.info.sims_section_code).then(function (InvoiceDetailsData) {
                    $scope.InvoiceDetailsData = InvoiceDetailsData.data;
                });
            }

            $scope.deleteInvoice = function () {
                $scope.deleteInvoiceNos = [];
                var flag = false;
                for (var i = 0; i < $scope.filteredTodos_invoice.length; i++) {

                    if ($scope.filteredTodos_invoice[i].sims_invoice_status == true) {
                        flag = true;
                        break;
                    }
                }
                if (flag == false) {
                    swal(
                                {
                                    showCloseButton: true,
                                    text: 'Please select Invoice(s) to Delete.',
                                    width: 350,
                                    imageUrl: "assets/img/check.png",
                                    showCloseButon: true,
                                    allowOutsideClick: true,
                                });
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos_invoice.length; i++) {

                        if ($scope.filteredTodos_invoice[i].sims_invoice_status == true) {
                            $scope.deleteInvoiceNos.push($scope.filteredTodos_invoice[i].in_no);
                        }
                    }
                    $http.post(ENV.apiUrl + "api/StudentReport/updateInvoices?invoice_nos=", $scope.deleteInvoiceNos).then(function (result) {
                        if (result.data == true) {
                            swal(
                               {
                                   showCloseButton: true,
                                   text: 'Selected Invoice(s) are deleted.',
                                   width: 350,
                                   imageUrl: "assets/img/check.png",
                                   showCloseButon: true,
                                   allowOutsideClick: true,
                               });

                            $scope.seachInvoice(1);
                            var main = document.getElementById('chkSearch');
                            main.checked = false;
                            $scope.refresh();//Main Grid
                        }
                        else {
                            swal(
                                {
                                    showCloseButton: true,
                                    text: 'Error while Deleting Invoice(s).',
                                    width: 350,
                                    imageUrl: "assets/img/close.png",
                                    showCloseButon: true
                                });
                        }

                    });

                }
            }

            $scope.isGenerateEnable = true;

            $scope.CheckMultipleSearch = function () {
                var main = document.getElementById('chkSearch');
                for (var i = 0; i < $scope.filteredTodos_invoice.length; i++) {
                    if (main.checked == true) {
                        $scope.filteredTodos_invoice[i].sims_invoice_status = true;
                    }
                    else if (main.checked == false) {
                        $scope.row1 = '';
                        $scope.color = '#edefef';
                        main.checked = false;
                        $scope.filteredTodos_invoice[i].sims_invoice_status = false;
                    }
                }
            }

            $scope.CheckOneByOneSearch = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                    }
                });

                var main = document.getElementById('chkSearch');
                if (main.checked == true) {
                    main.checked = false;
                    //$scope.color = '#edefef';
                }

            }

            $scope.CheckMultiple = function () {
                paging();
            }

            function paging() {
                var main = document.getElementById('mainchk');

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_enroll_number;
                    var v = document.getElementById(t);
                    if (main.checked == true && !$scope.filteredTodos[i].sims_invoice_student_status) {
                        $scope.filteredTodos[i].sims_invoice_status = true;
                        v.checked = true;
                        $('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                        $scope.isGenerateEnable = false;
                    }
                    else if (main.checked == false) {

                        $('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                        main.checked = false;
                        $scope.isGenerateEnable = true;
                        $scope.filteredTodos[i].sims_invoice_status = false;

                    }
                }
            }

            $scope.refresh = function () { _onFetchData(); }

            $scope.CheckOneByOne = function () {

                //$("input[type='checkbox']").click(function (e) {
                //    if ($(this).is(":checked")) {
                //        $(this).closest('tr').addClass("row_selected");
                //        $scope.isGenerateEnable = true;
                //    }
                //    else {
                //        $scope.isGenerateEnable = false;
                //        $(this).closest('tr').removeClass("row_selected");
                //    }
                //});

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                    }
                });

                var main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    //$scope.color = '#edefef';
                }

            }

            $scope.Report = function (str) {
                var data = {
                    location: $scope.InvoiceReport,
                    parameter: {
                        cur_code: str.sims_cur_code,
                        acad_year: str.sims_academic_year,
                        grade_code: str.sims_grade_code,
                        section_code: str.sims_section_code,
                        enroll: str.sims_enroll_number,
                        invmode: str.sims_invoce_mode,
                        invperiod: str.sims_period_code,
                        in_no: str.in_no,
                    },
                    state: 'main.SimTIG',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
            }

            //Events End
        }])

})();
