﻿(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [];
    var doc_no;
    var simsController = angular.module('sims.module.Fee');
    simsController.controller('ClassWiseFeeReceiptCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.pager = true;
            $scope.pagggg = false;
            var user = $rootScope.globals.currentUser.username;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $http.get(ENV.apiUrl + "api/ClassWiseFeeReceipt/getAcademicYearClass").then(function (academic_year) {
                $scope.academic_year = academic_year.data;
                for (var i = 0; i < $scope.academic_year.length; i++) {
                    if ($scope.academic_year[i].academic_year_status == "C") {
                        $scope.edt['academic_year'] = $scope.academic_year[i].academic_year;
                    }
                }

                $scope.getacademic_year($scope.edt['academic_year'])
            });
            $scope.size = function (str) {
                1
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.FeeReceiptData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "dd-mm-yyyy"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

   

            $scope.paginationview1 = document.getElementById("paginationview");

            $scope.showdate = function (date, name) {
                
                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //$scope.edt[name] = year + "/" + month + "/" + day;

                //var day = date.split("-")[0];
                //var month = date.split("-")[1];
                //var year = date.split("-")[2];
                //$scope.edt[name] = day + "-" + month + "-" + year;
                $scope.edt[name] = date;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };



            $scope.getacademic_year = function (academic_year) {
               
                $http.get(ENV.apiUrl + "api/ClassWiseFeeReceipt/get_grades?acayr=" + academic_year).then(function (get_grades) {
                    $scope.get_grades = get_grades.data;
                });
            }

            $scope.getgrade = function (grade_code) {
                $http.get(ENV.apiUrl + "api/ClassWiseFeeReceipt/get_section?acayr=" + $scope.edt.academic_year + "&grade=" + grade_code).then(function (get_section) {
                    $scope.get_section = get_section.data;
                });
            }


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.FeeReceiptData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.FeeReceiptData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked()

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.doc_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }


            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.edt = {
                from_date: dd + '-' + mm + '-' + yyyy,
                to_date: dd + '-' + mm + '-' + yyyy,

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.onKeyPress = function ($event) {
               
                if ($event.keyCode == 13) {

                    $scope.Show($scope.edt.from_date, $scope.edt.to_date, $scope.edt.search);
                   
                }
            };


            $scope.Show = function () {

                //if ($scope.edt.from_date == undefined || $scope.edt.from_date == "") {
                //    $scope.flag1 = true;
                //    swal({ title: "Alert", text: "Please select From Date", showCloseButton: true, width: 380, });

                //}
                //else if ($scope.edt.to_date == undefined || $scope.edt.to_date == "") {
                //    $scope.flag1 = true;
                //    swal({ title: "Alert", text: "Please select To Date", showCloseButton: true, width: 380, });
                //}
                //else {

                if ($scope.edt.search == undefined || $scope.edt.search == "") {
                    $scope.edt.search = '';
                }
                $scope.table1 = true;
                $scope.pagggg = true;
                $scope.ImageView = false;
                
                $http.get(ENV.apiUrl + "api/ClassWiseFeeReceipt/getAllRecordsFeeReceipt?acayr=" + $scope.edt.academic_year + "&from_date=" + $scope.edt.from_date + "&to_date="
                    + $scope.edt.to_date + "&grade=" + $scope.edt.grade_code+ "&section=" + $scope.edt.section_code+ "&search=" + $scope.edt.search).then(function (FeeReceipt_Data) {
                    $scope.FeeReceiptData = FeeReceipt_Data.data;
                    $scope.totalItems = $scope.FeeReceiptData.length;
                    $scope.todos = $scope.FeeReceiptData;
                    $scope.makeTodos();
                    if (FeeReceipt_Data.data.length > 0) { }
                    else {
                        $scope.ImageView = true;
                        $scope.pager = false;
                    }
                });
                //}
            }

            $scope.Reset = function () {
                $scope.edt = '';
                $scope.table1 = false;
            }

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.edt["from_date"] = '';
                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.edt = {
                            //from_date: yyyy + '-' + mm + '-' + dd,
                            //to_date: yyyy + '-' + mm + '-' + dd,
                            from_date: dd + '-' + mm + '-' + yyyy,
                            to_date: dd + '-' + mm + '-' + yyyy,
                        }

                    }
                });
            });

           
            $scope.Report = function (str) {
                $http.get(ENV.apiUrl + "api/StudentFee/GetReceipt").then(function (res) {
                    var rname = res.data;

                var data = {
                    //location: 'Sims.SIMR51FeeRec',
                    location: rname,
              parameter: {
                        fee_rec_no: str.doc_no,
                    },
                    state: 'main.Sim710',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
            });
            }

        }]
        )
})();