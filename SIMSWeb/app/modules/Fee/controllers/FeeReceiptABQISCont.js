﻿(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [];

    var doc_no;
    var simsController = angular.module('sims.module.Fee');
    simsController.controller('FeeReceiptABQISCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = 'All';
            $scope.pager = true;
            $scope.pagggg = false;
            $scope.table2 = false;
            $scope.currency_code = '';
            var user = $rootScope.globals.currentUser.username;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.export_btn = false;
            $scope.print_vouchers = false;
            $scope.total_amt = 0;
            $scope.total_cash_amount = 0;
            $scope.total_cheque_amount = 0;
            $scope.total_credit_card_amount = 0;
            $scope.total_other_amount = 0;
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            //$http.post(ENV.apiUrl + "http://api.mograsys.com/APIERP/api/StudentFee/GetReceipt").then(function (res) {
            //    $scope.reportparameter = res.data;
            //});

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.etemp = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear();
            });

            $http.get(ENV.apiUrl + "api/StudentFee/getDecimalPlaces").then(function (getDecimal) {
                debugger;
                $scope.decimal_degit = getDecimal.data;
                $scope.decimal_dsc = $scope.decimal_degit[0].comp_curcy_dec;
            });


            $scope.getAccYear = function () {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.etemp.sims_cur_code).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                });
            }


    
            $scope.size = function (str) {

                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.FeeReceiptData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy/mm/dd"
                // format: "dd/mm/yyyy"

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            //$scope.createdate = function (end_date, start_date, name) {
            //    var month1 = end_date.split("/")[0];
            //    var day1 = end_date.split("/")[1];
            //    var year1 = end_date.split("/")[2];
            //    var new_end_date = year1 + "/" + month1 + "/" + day1;
            //    //var new_end_date = day1 + "/" + month1 + "/" + year1;

            //    var year = start_date.split("/")[0];
            //    var month = start_date.split("/")[1];
            //    var day = start_date.split("/")[2];
            //    var new_start_date = year + "/" + month + "/" + day;
            //    // var new_start_date = day + "/" + month + "/" + year;

            //    if (new_end_date < new_start_date) {
            //        swal({ title: 'Please Select Next Date From ', width: 380, height: 100 });
            //        $scope.edt[name] = '';
            //    }
            //    else {
            //        $scope.edt[name] = new_end_date;
            //    }
            //}

            $scope.paginationview1 = document.getElementById("paginationview");

            $scope.showdate = function (date, name) {

                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //$scope.edt[name] = year + "/" + month + "/" + day;
                $scope.edt[name] = date;
                //var day = date.split("-")[0];
                //var month = date.split("-")[1];
                //var year = date.split("-")[2];
                //$scope.edt[name] = year + "-" + month + "-" + day;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.FeeReceiptData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.FeeReceiptData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked()

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.doc_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();

            //$scope.sdate = yyyy + '-' + mm + '-' + dd;
            //$scope.edate = yyyy + '-' + mm + '-' + dd;
            //$scope.edt = {
            //    from_date: yyyy + '-' + mm + '-' + dd,
            //    to_date: yyyy + '-' + mm + '-' + dd,
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.edt = {
                from_date: dd + '-' + mm + '-' + yyyy,
                to_date: dd + '-' + mm + '-' + yyyy,
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.onKeyPress = function ($event) {

                if ($event.keyCode == 13) {

                    $scope.Show($scope.edt.from_date, $scope.edt.to_date, $scope.edt.search);

                }
            };

            $scope.Show = function () {
                $scope.FeeReceiptData = [];
                $scope.total_amt = 0;
                $scope.total_cash_amount = 0;
                $scope.total_cheque_amount = 0;
                $scope.total_credit_card_amount = 0;
                $scope.total_other_amount = 0;

                if ($scope.edt.search == undefined || $scope.edt.search == "") {
                    $scope.edt.search = '';
                }
                $scope.table1 = true;
                $scope.pagggg = true;
                $scope.ImageView = false;
                $scope.export_btn = true;
                $scope.print_vouchers = true;
                $scope.total_amt = 0;
                $http.get(ENV.apiUrl + "api/FeeReceipt/getAllRecordsFeeReceiptNew1_ABQIS?from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date + "&search=" + $scope.edt.search + "&ac_year=" + $scope.temp.sims_academic_year).then(function (FeeReceipt_Data) {
                    console.log(FeeReceipt_Data.data);
                    $scope.FeeReceiptData = FeeReceipt_Data.data;
                    $scope.totalItems = $scope.FeeReceiptData.length;
                    $scope.todos = $scope.FeeReceiptData;
                    $scope.makeTodos();
                    debugger
                    $scope.currency_code = $scope.FeeReceiptData[0].comp_curcy_dec;
                    if (FeeReceipt_Data.data.length > 0) { $scope.size('All') }
                    else {
                        $scope.ImageView = true;
                        $scope.pager = false;
                    }

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        if ($scope.edt.search != undefined) {
                            if ($scope.edt.search != '') {
                                if ($scope.edt.search.length == $scope.filteredTodos[i].doc_no.length) {
                                    $scope.total_amt = 0
                                    $scope.total_cash_amount = 0;
                                    $scope.total_cheque_amount = 0;
                                    $scope.total_credit_card_amount = 0;
                                    $scope.total_other_amount = 0;
                                    $scope.total_amt = (parseFloat($scope.total_amt) + parseFloat($scope.filteredTodos[i].grand_total)).toFixed($scope.currency_code);
                                    $scope.total_cash_amount = (parseFloat($scope.total_cash_amount) + parseFloat($scope.filteredTodos[i].cash_amount)).toFixed($scope.currency_code);
                                    $scope.total_cheque_amount = (parseFloat($scope.total_cheque_amount) + parseFloat($scope.filteredTodos[i].cheque_amount)).toFixed($scope.currency_code);
                                    $scope.total_credit_card_amount = (parseFloat($scope.total_credit_card_amount) + parseFloat($scope.filteredTodos[i].credit_card_amount)).toFixed($scope.currency_code);
                                    $scope.total_other_amount = (parseFloat($scope.total_other_amount) + parseFloat($scope.filteredTodos[i].other_amount)).toFixed($scope.currency_code);
                                }
                                else {
                                    $scope.total_amt = (parseFloat($scope.total_amt) + parseFloat($scope.filteredTodos[i].grand_total)).toFixed($scope.currency_code);
                                    $scope.total_cash_amount = (parseFloat($scope.total_cash_amount) + parseFloat($scope.filteredTodos[i].cash_amount)).toFixed($scope.currency_code);
                                    $scope.total_cheque_amount = (parseFloat($scope.total_cheque_amount) + parseFloat($scope.filteredTodos[i].cheque_amount)).toFixed($scope.currency_code);
                                    $scope.total_credit_card_amount = (parseFloat($scope.total_credit_card_amount) + parseFloat($scope.filteredTodos[i].credit_card_amount)).toFixed($scope.currency_code);
                                    $scope.total_other_amount = (parseFloat($scope.total_other_amount) + parseFloat($scope.filteredTodos[i].other_amount)).toFixed($scope.currency_code);
                                }
                            }
                            else {
                                $scope.total_amt = (parseFloat($scope.total_amt) + parseFloat($scope.filteredTodos[i].grand_total)).toFixed($scope.currency_code);
                                $scope.total_cash_amount = (parseFloat($scope.total_cash_amount) + parseFloat($scope.filteredTodos[i].cash_amount)).toFixed($scope.currency_code);
                                $scope.total_cheque_amount = (parseFloat($scope.total_cheque_amount) + parseFloat($scope.filteredTodos[i].cheque_amount)).toFixed($scope.currency_code);
                                $scope.total_credit_card_amount = (parseFloat($scope.total_credit_card_amount) + parseFloat($scope.filteredTodos[i].credit_card_amount)).toFixed($scope.currency_code);
                                $scope.total_other_amount = (parseFloat($scope.total_other_amount) + parseFloat($scope.filteredTodos[i].other_amount)).toFixed($scope.currency_code);
                            }
                        }
                        else {
                            $scope.total_amt = (parseFloat($scope.total_amt) + parseFloat($scope.filteredTodos[i].grand_total)).toFixed($scope.currency_code);
                            $scope.total_cash_amount = (parseFloat($scope.total_cash_amount) + parseFloat($scope.filteredTodos[i].cash_amount)).toFixed($scope.currency_code);
                            $scope.total_cheque_amount = (parseFloat($scope.total_cheque_amount) + parseFloat($scope.filteredTodos[i].cheque_amount)).toFixed($scope.currency_code);
                            $scope.total_credit_card_amount = (parseFloat($scope.total_credit_card_amount) + parseFloat($scope.filteredTodos[i].credit_card_amount)).toFixed($scope.currency_code);
                            $scope.total_other_amount = (parseFloat($scope.total_other_amount) + parseFloat($scope.filteredTodos[i].other_amount)).toFixed($scope.currency_code);
                        }

                    }

                });
                //}
            }


            
            $scope.checkonebyoneselect = function () {

                $("input[type='checkbox']").change(function (e) {

                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                //alert(main);
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            //check all 
            $scope.CheckAllChecked = function () {

                debugger;
                var main = document.getElementById('mainchk');
                //alert(main);
                if (main.checked == true) {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].doc_no + i);
                        v.checked = true;
                        $scope.filteredTodos[i].chk_multi_print = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].doc_no + i);
                        v.checked = false;
                        $scope.filteredTodos[i].chk_multi_print = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.Export = function () {
                var check = true;


                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " Fee Receipt.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            var blob = new Blob([document.getElementById('exporting_data').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " Fee Receipt" + ".xls");
                            //alasql('SELECT * INTO XLSX("Report.xlsx",{headers:true}) \ FROM HTML("#printdata",{headers:true,skipdisplaynone:true})');

                        }
                    });
                }
                else {
                    swal({ text: "Report Not Save", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, })
                }

            }

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.edt["from_date"] = '';
                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.edt = {
                            //from_date: yyyy + '-' + mm + '-' + dd,
                            //to_date: yyyy + '-' + mm + '-' + dd,
                            from_date: dd + '-' + mm + '-' + yyyy,
                            to_date: dd + '-' + mm + '-' + yyyy,
                        }

                    }
                });
            });


            //$scope.Report = function (str) {
            //    $('#MyModal5').fadeIn('slow');
            //    if ($http.defaults.headers.common['schoolId'] == 'ajb' || $http.defaults.headers.common['schoolId'] == 'aji' || $http.defaults.headers.common['schoolId'] == 'zps') {

            //        $http.get(ENV.apiUrl + "api/StudentFee/GetReceiptCon").then(function (res) {
            //            var rname = res.data;
            //            var data = {
            //                //location: 'Sims.SIMR51FeeRec',
            //                location: rname,
            //                parameter: {
            //                    fee_rec_no: str.doc_no,
            //                    duplicate_flag: '1',
            //                },
            //                state: 'main.Sim615',
            //                ready: function () {
            //                    this.refreshReport();
            //                },
            //            }
            //            window.localStorage["ReportDetails"] = JSON.stringify(data)
            //            $state.go('main.ReportCardParameter')
            //        });

            //    }
            //    else {

            //        if (str.dt_code == '1') {
            //            $http.get(ENV.apiUrl + "api/StudentFee/GetReceipt").then(function (res) {
            //                var rname = res.data;



            //                var data = {
            //                    //location: 'Sims.SIMR51FeeRec',
            //                    location: rname,
            //                    parameter: {
            //                        fee_rec_no: str.doc_no,

            //                    },
            //                    state: 'main.Sim615',
            //                    ready: function () {
            //                        this.refreshReport();
            //                    },
            //                    show_pdf:'y'
            //                }
            //                window.localStorage["ReportDetails"] = JSON.stringify(data)
            //                $state.go('main.ReportCardParameter')
            //            });
            //        }

            //        else {

            //            $http.get(ENV.apiUrl + "api/StudentFee/GetReceiptCon").then(function (res) {
            //                var rname = res.data;
            //                var data = {
            //                    //location: 'Sims.SIMR51FeeRec',
            //                    location: rname,
            //                    parameter: {
            //                        fee_rec_no: str.doc_no,

            //                    },
            //                    state: 'main.Sim615',
            //                    ready: function () {
            //                        this.refreshReport();
            //                    },
            //                    show_pdf: 'y'
            //                }
            //                window.localStorage["ReportDetails"] = JSON.stringify(data)
            //                $state.go('main.ReportCardParameter')
            //            });

            //        }
            //    }
            //}

            //supriya
            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=PrintBulk_FR").then(function (res) {
                $scope.final_doc_url = res.data;
            });

            $scope.report_show = true;
            $scope.print_multiple_voucher = function () {
                debugger;
                $('#feemodal').modal('show');
                $scope.report_show = false;
                
                $scope.doc_cd_final_doc_no = '';
                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    if ($scope.filteredTodos[i].chk_multi_print == true) {
                        $scope.doc_cd_final_doc_no = $scope.doc_cd_final_doc_no + $scope.filteredTodos[i].doc_no + ',';
                    }


                }
                var data = {
                    location: $scope.final_doc_url,
                    //parameter: { fee_rec_no: doc_no, doc: $scope.doc_cd_final_doc_no },
                    parameter: { fee_rec_no: $scope.doc_cd_final_doc_no },
                    state: 'main.Sim61A'
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)
                // $state.go('main.ReportCardParameter')

                try {
                    $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                    $scope.location = $scope.rpt.location;
                    $scope.parameter = $scope.rpt.parameter;
                    $scope.state = $scope.rpt.state;
                }
                catch (ex) {
                }
                var s;

                //if ($http.defaults.headers.common['schoolId'] == 'dpsmis')
                //    s = "SimsReports." + $scope.location + ",SimsReports";


                //else
                //    s = "SimsReports." + $scope.location + ",SimsReports" + $http.defaults.headers.common['schoolId'];


                //  window.localStorage["Finn_comp"] = JSON.stringify(data)

                s = "SimsReports." + $scope.location + ",SimsReports";

                var url = window.location.href;
                var domain = url.substring(0, url.indexOf(':'))
                if ($http.defaults.headers.common['schoolId'] == 'sms') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                }
                else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                }
                
                else if ($http.defaults.headers.common['schoolId'] == 'ibserp') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.ibskuwait.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.ibskuwait.com/report/api/reports/';
                }


                else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'staging2') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'staging') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'tiadxb') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'tiashj') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                }

                else {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                }
                console.log(service_url);


                $scope.parameters = {}
                debugger;
                $("#reportViewer1")
                               .telerik_ReportViewer({
                                   //serviceUrl: ENV.apiUrl + "api/reports/",
                                   serviceUrl: service_url,

                                   viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                   scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                   // Zoom in and out the report using the scale
                                   // 1.0 is equal to 100%, i.e. the original size of the report
                                   scale: 1.0,
                                   ready: function () {
                                       //this.refreshReport();
                                   }

                               });





                var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                reportViewer.reportSource({
                    report: s,
                    parameters: $scope.parameter,
                });

                //var rv = $("#reportViewer1").data("telerik_ReportViewer");
                //rv.commands.print.exec();

                setInterval(function () {

                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                    //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

                }, 1000);


                $timeout(function () {
                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                    //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


                }, 100)
            }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                
                $('input[type="text"]', $(this).parent()).focus();
            });
    



            $scope.report_show = true;
            $scope.Report = function (str) {
                debugger;
                //$('#feemodal').modal({ backdrop: 'static', keyboard: false });
                $('#feemodal').modal('show');

                $scope.report_show = false;
                $http.get(ENV.apiUrl + "api/StudentFee/GetReceiptCon").then(function (res) {
                    var rname = res.data;
                    var data = {
                        //location: 'Sims.SIMR51FeeRec',
                        location: rname,
                        parameter: {
                            fee_rec_no: str.doc_no,
                            duplicate_flag: '5',
                        },
                        state: 'main.Sim615',
                        ready: function () {
                            this.refreshReport();
                        },
                    }

                    window.localStorage["ReportDetails"] = JSON.stringify(data)

                    // });

                    //For Report Tool////////////////////////////////////////////////////////////////////////////////


                    try {
                        $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                        $scope.location = $scope.rpt.location;
                        $scope.parameter = $scope.rpt.parameter;
                        $scope.state = $scope.rpt.state;
                    }
                    catch (ex) {
                    }
                    var s;

                    //if ($http.defaults.headers.common['schoolId'] == 'dpsmis')
                    //    s = "SimsReports." + $scope.location + ",SimsReports";


                    //else
                    //    s = "SimsReports." + $scope.location + ",SimsReports" + $http.defaults.headers.common['schoolId'];


                    //  window.localStorage["Finn_comp"] = JSON.stringify(data)

                    s = "SimsReports." + $scope.location + ",SimsReports";

                    var url = window.location.href;
                    var domain = url.substring(0, url.indexOf(':'))
                    if ($http.defaults.headers.common['schoolId'] == 'sms') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'ibserp') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.ibskuwait.com/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.ibskuwait.com/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'tiadxb') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'tiashj') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    }

                    else {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    }
                    console.log(service_url);


                    $scope.parameters = {}

                    $("#reportViewer1")
                                   .telerik_ReportViewer({
                                       //serviceUrl: ENV.apiUrl + "api/reports/",
                                       serviceUrl: service_url,

                                       viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                       scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                       // Zoom in and out the report using the scale
                                       // 1.0 is equal to 100%, i.e. the original size of the report
                                       scale: 1.0,
                                       ready: function () {
                                           //this.refreshReport();
                                       }

                                   });





                    var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                    reportViewer.reportSource({
                        report: s,
                        parameters: $scope.parameter,
                    });

                    //var rv = $("#reportViewer1").data("telerik_ReportViewer");
                    //rv.commands.print.exec();

                    setInterval(function () {

                        $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                        $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                        //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

                    }, 1000);


                    $timeout(function () {
                        $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                        $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                        //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


                    }, 100)


                });
            }

            document.onkeydown = function (event) {
                debugger
                event = event || window.event;
                if (event.keyCode == 27) {
                    $('#feemodal').modal('hide');
                }
            }

        }]
        )
})();