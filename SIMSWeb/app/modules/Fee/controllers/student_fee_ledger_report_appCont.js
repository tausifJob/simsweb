﻿(function () {
    'use strict';
    var teacher_code = [];
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('student_fee_ledger_report_appCont',
    ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
        $scope.display = true;
        $scope.busyindicator = false;
        $scope.show_ins_table_loading = false;
        //$scope.balance;

        $http.get(ENV.apiUrl + "api/LibraryAttribute/getCuriculum").then(function (res1) {
            $scope.cur_data = res1.data;
            $scope.sims_Cur = $scope.cur_data[0].sims_cur_code;
            $scope.getAcademic_year($scope.cur_data[0].sims_Cur);
        });

        $scope.getAcademic_year = function (cur_code1) {
            debugger
            $http.get(ENV.apiUrl + "api/LibraryAttribute/getAcademicYear?curCode=" + cur_code1).then(function (res1) {
                $scope.acad_data = res1.data;
                $scope.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                $scope.getAllGrades();
            });
        }

        $scope.getAllGrades = function () {
            debugger
            $http.get(ENV.apiUrl + "api/LibraryAttribute/getAllGrades?cur_code=" + $scope.sims_Cur + "&academic_year=" + $scope.sims_academic_year).then(function (res1) {
                $scope.grade_data = res1.data;
             //   $scope.sims_grade_code = $scope.grade_data[0].sims_grade_code;
             //  $scope.getSectionFromGrade($scope.sims_Cur, $scope.sims_grade_code,$scope.sims_academic_year);
                setTimeout(function () {
                    $('#grade_box').change(function () {

                    }).multipleSelect({
                        width: '100%'
                    });
                    //$("#show_box").multipleSelect("checkAll");
                }, 1000);
               
            });
        }

        $(function () {
            $('#grade_box').multipleSelect({ width: '100%' });
        });

        $scope.getSectionFromGrade = function (cur_code1,grade_code1,academic_year1) {
            debugger
            $http.get(ENV.apiUrl + "api/LibraryAttribute/getSectionFromGrade?cur_code=" + $scope.sims_Cur + "&grade_code=" + $scope.sims_grade_code + "&academic_year=" + $scope.sims_academic_year).then(function (res1) {
                $scope.section_data = res1.data;
              //  $scope.sims_section_code = $scope.section_data[0].sims_section_code;
                setTimeout(function () {
                    $('#section_box').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                    //$("#show_box").multipleSelect("checkAll");
                }, 1000);
            });
        }

        $(function () {
            $('#section_box').multipleSelect({ width: '100%' });
        });

        $timeout(function () {
            $("#customers").tableHeadFixer({ 'top': 1 });
        }, 100);

        $scope.exportData = function () {
            swal({
                title: "Alert",
                text: "Do you want to Export in MS-Excel?",
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonText: 'Yes',
                width: 380,
                cancelButtonText: 'No',
            }).then(function (isConfirm) {
                if (isConfirm) {
                    //var blob = new Blob([document.getElementById('Div1').innerHTML], {
                    //            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                    //        });
                    //        saveAs(blob, "Report.xlsx");
                    //alasql('Select * INTO XLSX("Report.xlsx",{headers:true}) \ FROM HTML ("#DIV1",{headers:true,skipdisplaynone:true})');
                    //alasql('SELECT * INTO XLSX("Report.xlsx",{headers:true}) \ FROM HTML("#DIV1",{headers:true,skipdisplaynone:true})');

                    //    var data1 = alasql('SELECT * FROM HTML("#DIV1",{headers:true})');
                    //var data = $scope.stud_data1;
                       alasql('SELECT * INTO XLSX("Fee_ledger.xlsx",{headers:true}) FROM ?', [$scope.famstat_data]);
                   // alasql('SELECT * INTO XLSX("Fee_ledger.xlsx",{headers:true})\FROM HTML("#printdata3",{headers:true,skipdisplaynone:true})', [$scope.famstat_data]);
                }
            });
        };

        $scope.getstud_ledge_data = function (cur_code1, academic_year1, grade_code1, section_code1, search1) {
            debugger
            $http.get(ENV.apiUrl + "api/LibraryAttribute/getstud_ledge_data?cur_code=" + $scope.sims_Cur + "&acad_year=" + $scope.sims_academic_year + "&grade_code=" + $scope.sims_grade_code + "&section_code=" + $scope.sims_section_code + "&search=" + $scope.search).then(function (res1) {
                $scope.ledger_data = res1.data;
            });
        }

        $scope.openNewWindow = function (obj) {
            console.log(obj);
            debugger
            $http.get(ENV.apiUrl + "api/LibraryAttribute/getfamily_stat?cur_code=" + $scope.sims_Cur + "&acad_year=" + $scope.sims_academic_year + "&acc_no=" + obj).then(function (res1) {
                $scope.famstat_data = res1.data;
                console.log($scope.famstat_data);
              
                //for (var i = 0; i < $scope.famstat_data.length; i++) {
                //    $scope.balance = parseFloat($scope.famstat_data[i].amount_payable) - parseFloat($scope.famstat_data[i].amount_paid);
                //}
            });
            $('#MyModal4').modal('show');
        }

        $scope.openDocumentReport = function () {
            var data = {
                //location: 'Sims.SIMR51FeeRec',
                location: 'Payroll.PERR19',
                parameter: {
                    user_no: $scope.sims_academic_year,
                    acad_year: $scope.parent_no
                },
                state: 'main.FEELEG',
                ready: function () {
                    this.refreshReport();
                },
            }
            window.localStorage["ReportDetails"] = JSON.stringify(data)
            $state.go('main.ReportCardParameter');
        }

    }]);
})();