﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CautionDepositCancelReceiptCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            var user = $rootScope.globals.currentUser.username;
            $scope.table1 = true;
            $scope.busy = false;
            $scope.rgvtbl = false;
            $scope.edt = {};



            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }

            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.edt = {
                from_date: dd + '-' + mm + '-' + yyyy,
                to_date: dd + '-' + mm + '-' + yyyy,
                cancel_date: dd + '-' + mm + '-' + yyyy,
            }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.Show_data = function () {
                $scope.rgvtbl = false;
                $scope.busy = true;

                if ($scope.edt.search == undefined || $scope.edt.search == "") {
                    $scope.edt.search = null;
                }
                $http.get(ENV.apiUrl + "api/CancelCautionFeeReceipt/getAllRecordsCancelCautionFeeReceipt?from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date + "&search=" + $scope.edt.search).then(function (PaymentMode) {
                    $scope.mainObject = PaymentMode.data;
                    $scope.busy = false;

                    if ($scope.mainObject.length > 0) {
                        $scope.rgvtbl = true;
                    }
                    else {
                        swal({ text: 'Data Not Found',imageUrl: "assets/img/close.png", width: 300, height: 250, showCloseButton: true });
                    }
                });
            }

            $scope.SelectAll = function () {
                for (var i = 0; i < $scope.mainObject.length; i++) {
                    if ($scope.main_checkbox == true) {

                        $scope.mainObject[i].sims_status = true;
                    } else {
                        $scope.mainObject[i].sims_status = false;

                    }
                }
            }

            $scope.textconditions = function (str) {
                if (str != '') {
                    $scope.textdis = true;
                }
                else {
                    $scope.textdis = false;
                }
            }

            $scope.cancel = function () {
                $scope.main_checkbox = false;
                for (var i = 0; i < $scope.mainObject.length; i++) {
                    if ($scope.main_checkbox == true) {

                        $scope.mainObject[i].sims_status = true;
                    } else {
                        $scope.mainObject[i].sims_status = false;

                    }
                }

            }

            $scope.Save_Btn_Cliack = function () {
                $scope.DataObject = [];
                for (var i = 0; i < $scope.mainObject.length; i++) {
                    if ($scope.mainObject[i].sims_status == true) {
                        $scope.mainObject[i].cashier_name = user;
                        $scope.mainObject[i].to_date =$scope.edt.cancel_date;
                        $scope.DataObject.push($scope.mainObject[i]);
                    }
                }

                if ($scope.DataObject.length > 0) {
                    $http.post(ENV.apiUrl + "api/CancelCautionFeeReceipt/CUDCancelCautionFeeReceipt", $scope.DataObject).then(function (message) {
                        $scope.message = message.data;
                        if ($scope.message == true) {
                            swal({ text: 'Receipt Cancelled Successfully', imageUrl: "assets/img/check.png",width: 300, height: 250, showCloseButton: true });
                            $scope.Show_data();
                        }
                        else {
                            swal({ text: 'Receipt Not Cancelled',imageUrl: "assets/img/close.png", width: 300, height: 250, showCloseButton: true });
                        }
                    });
                } else {
                    swal({ text: 'Select AtLeast One Record',imageUrl: "assets/img/notification-alert.png", width: 300, height: 250, showCloseButton: true });
                }
            }

            

        }]);
})();