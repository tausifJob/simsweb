﻿
(function () {
    'use strict';
    var feenumber = [];
    var copy_to_sectionnumber = [];
    var main;
    var grade_code;
    var cur_code;
    var section_code1, academic_years1, fee_Category1;
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CopySectionFeeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.table = false;
            $scope.table1 = false;
            $scope.edt = {};
            $scope.temp = {};
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
                if ($scope.curriculum.length > 0)
                {
                    $scope.edt.sims_cur_code = $scope.curriculum[0].sims_cur_code;
                    $scope.temp.sims_cur_code = $scope.curriculum[0].sims_cur_code;
                    $scope.getacyr($scope.edt.sims_cur_code);
                    $scope.getacyr1($scope.temp.sims_cur_code);
                }
                $scope.opration = false;
                $scope.display = true;

            })

            $scope.getacyr = function (str) {
                $http.get(ENV.apiUrl + "api/common/SectionFees/getCurrentAdvanceAY?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    if ($scope.Academic_year.length > 0)
                        {
                        $scope.edt.section_fee_academic_year = $scope.Academic_year[0].section_fee_academic_year;
                        $scope.getgradecodes();
                        }
                    })
            }
           
            $scope.getacyr1 = function (str) {
                $http.get(ENV.apiUrl + "api/common/SectionFees/getCurrentAdvanceAY?curcode=" + str).then(function (Academicyear1) {
                    $scope.Academic_year1 = Academicyear1.data;
                    if ($scope.Academic_year1.length > 0) {
                        $scope.temp.section_fee_academic_year = $scope.Academic_year1[0].section_fee_academic_year;
                        $scope.getgradecodes1();
                    }

                })
            }

            $scope.getgradecodes = function () {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.section_fee_academic_year).then(function (res1) {
                    $scope.grade1 = res1.data;
                })

            }

            $scope.getgradecodes1 = function () {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.section_fee_academic_year).then(function (res1) {

                    $scope.grade = res1.data;
                });

            }

            $http.get(ENV.apiUrl + "api/common/SectionFees/getAllFeeCategoryNames").then(function (AllFeeCategoryNames) {
                $scope.FeeCategoryNames = AllFeeCategoryNames.data;
                })

            $scope.getsection = function () {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.edt.section_fee_academic_year).then(function (res) {
                    $scope.section = res.data;

                })
            };
            
            $scope.alldata = function () {
                //$scope.edt = {};
                
                $scope.busy = true;
                if ($scope.edt.section_fee_category != "" && $scope.edt.section_fee_category != undefined && $scope.edt.sims_grade_code != "" && $scope.edt.sims_grade_code != undefined && $scope.edt.sims_section_code != "" && $scope.edt.sims_section_code != undefined) {
                    $scope.msg = false;
                    $http.get(ENV.apiUrl + "api/common/SectionFees/getSectionFeeByIndex?cur_name=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.section_fee_academic_year + "&grade_name=" + $scope.edt.sims_grade_code + "&section_name=" + $scope.edt.sims_section_code + "&fee_category=" + $scope.edt.section_fee_category + "&PageIndex=1&PageSize=5").then(function (fee_list) {
                        $scope.allfee_list = fee_list.data;
                        $scope.busy = false;
                        $scope.table = true;
                    });
                }
                else {
                    $scope.msg = 'Please Select All Fields';
                }
            }

            $scope.GradeSectionName = function () {
                $scope.busy1 = true;
               
                if ($scope.temp.sims_grade_code != "" && $scope.temp.sims_grade_code) {
                    $scope.msg1 = false;
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.temp.sims_cur_code + "&grade_code=" + $scope.temp.sims_grade_code + "&academic_year=" + $scope.temp.section_fee_academic_year).then(function (AllGradeSectionName) {

                    var data = [];
                    var data1 = [];
                    $scope.sectiondataobj = AllGradeSectionName.data;
                    for (var i = 0; i < $scope.sectiondataobj.length; i++) {
                        if ($scope.sectiondataobj[i].sims_section_code == $scope.edt.sims_section_code) {
                            $scope.sectiondataobj.splice(i, -1);
                            break;
                        }
                    }
                    //$scope.filteredTodos = data1;
                    $scope.filteredTodos = $scope.sectiondataobj;
                    $scope.busy1 = false;
                    $scope.table1 = true;
                });
                }
                else {
                    $scope.msg1 = 'Please Select All Fields';
                }

            }

            var allfee = [];

            $scope.Save = function () {

                copy_to_sectionnumber = [];
                feenumber = [];
                var box1 = document.getElementById('Checkbox1');
                var overwriteobject1 = [];

                for (var i = 0; i < $scope.allfee_list.length; i++) {
                    var t = $scope.allfee_list[i].section_fee_code;
                    var v = document.getElementById(t);

                    if (v.checked == true)
                        feenumber = feenumber + $scope.allfee_list[i].section_fee_code + ',';
                }

                for (var j = 0; j < $scope.filteredTodos.length ; j++) {
                    var t = $scope.filteredTodos[j].sims_section_code + $scope.filteredTodos[j].sims_section_name;
                    var v = document.getElementById(t);
                    if (v.checked == true) {
                        if ($scope.filteredTodos[j].overwrite == true) {
                            for (var k = 0; k < $scope.allfee_list.length; k++) {
                                var t1 = $scope.allfee_list[k].section_fee_code;
                                var v1 = document.getElementById(t1);
                                if (v1.checked == true) {
                                    var overwriteobject = {};
                                    overwriteobject.sims_cur_code = $scope.edt.sims_cur_code,
                                    overwriteobject.sims_academic_year = $scope.edt.section_fee_academic_year,
                                    overwriteobject.sims_fee_cat = $scope.edt.section_fee_category,
                                    overwriteobject.sims_grade_code = $scope.edt.sims_grade_code,
                                    overwriteobject.sims_fee_code = $scope.allfee_list[k].section_fee_code,
                                    overwriteobject.sims_section_code = $scope.edt.sims_section_code

                                    overwriteobject.sims_fee_cur_code_to = $scope.temp.sims_cur_code,
                                    overwriteobject.sims_fee_academic_year_to = $scope.temp.section_fee_academic_year,
                                    overwriteobject.sims_fee_grade_code_to = $scope.temp.sims_grade_code,
                                    overwriteobject.sims_fee_section_code_to = $scope.filteredTodos[j].sims_section_code
                                    overwriteobject1.push(overwriteobject);
                                }
                            }
                        }
                        else {
                            copy_to_sectionnumber = copy_to_sectionnumber + $scope.filteredTodos[j].sims_section_code + ',';
                        }
                    }

                }

                var copy_section_fee_from = ({

                    'sims_cur_code': $scope.edt.sims_cur_code,
                    'sims_academic_year': $scope.edt.section_fee_academic_year,
                    'sims_fee_cat': $scope.edt.section_fee_category,
                    'sims_grade_code': $scope.edt.sims_grade_code,
                    'sims_fee_code': feenumber,
                    'sims_section_code': $scope.edt.sims_section_code
                });

                var copy_section_fee_to = ({
                    'sims_cur_code': $scope.temp.sims_cur_code,
                    'sims_academic_year': $scope.temp.section_fee_academic_year,
                    'sims_grade_code': $scope.temp.sims_grade_code,
                    'sims_section_code': copy_to_sectionnumber

                });

                if ((copy_to_sectionnumber.length <= 0 || feenumber.length <= 0) && overwriteobject1.length <= 0) {
                    swal({ text: 'Please Select Feetype Or Section Code', width: 380, showCloseButton: true });

                } else {

                    if (copy_to_sectionnumber.length > 0 && feenumber.length > 0) {
                        $http.post(ENV.apiUrl + "api/common/SectionFees/CUDSectionFeeDetails?fromstr=" + JSON.stringify(copy_section_fee_from) + "&tostr=" + JSON.stringify(copy_section_fee_to)).then(function (msg) {
                            $scope.msg1 = msg.data

                            if (overwriteobject1.length <= 0 && copy_to_sectionnumber.length > 0) {
                                swal({ text: $scope.msg1.strMessage, width: 300, showCloseButton: true });
                                copy_to_sectionnumber = [];
                                feenumber = [];
                                $scope.row1 = '';
                                $scope.row2 = '';
                                main = document.getElementById('mainchk');
                                box1.checked = false;
                                main.checked = false;
                                allfee = [];
                                $scope.table1 = false;
                                $scope.table = false;

                            }
                        });
                    }
                    if (overwriteobject1.length > 0) {
                        $http.post(ENV.apiUrl + "api/common/SectionFees/CUDSectionFeeDetailsoverwrite", overwriteobject1).then(function (msg2) {
                            $scope.message = msg2.data;

                            if (overwriteobject1.length > 0) {
                                swal({ text: $scope.message, width: 380, showCloseButton: true });
                                copy_to_sectionnumber = [];
                                feenumber = [];
                                $scope.row1 = '';
                                $scope.row2 = '';
                                box1.checked = false;
                                main = document.getElementById('mainchk');
                                main.checked = false;
                                allfee = [];
                                $scope.table1 = false;
                                $scope.table = false;
                                overwriteobject1 = [];
                            }


                        })
                    }

                }
            }
            $scope.CheckAllChecked = function () {


                main = document.getElementById('mainchk');
                feenumber = [];
                if (main.checked == true) {
                    for (var i = 0; i < $scope.allfee_list.length; i++) {
                        var t = $scope.allfee_list[i].section_fee_code;
                        var v = document.getElementById(t);
                        v.checked = true;
                        feenumber = feenumber + $scope.allfee_list[i].section_fee_code + ','
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.allfee_list.length; i++) {
                        var t = $scope.allfee_list[i].section_fee_code;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;

                        $scope.row1 = '';
                    }
                }
                }

            var main1 = '';

            $scope.select_all_feenumber_to_copy = function () {

                main1 = document.getElementById('Checkbox1');
                feenumber = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_section_code + $scope.filteredTodos[i].sims_section_name;
                    var v = document.getElementById(t);
                    if (main1.checked == true) {
                        v.checked = true;
                        copy_to_sectionnumber = copy_to_sectionnumber + $scope.filteredTodos[i].sims_section_code + ','
                        $scope.row2 = 'row_selected';
                    }
                    else {
                        v.checked = false;
                        main1.checked = false;
                        $scope.row2 = '';
                    }
                }

            }

            $scope.copy_to_section_onebyone_number = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('Checkbox1');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }
            }

            $scope.cancel = function () {
                $scope.table1 = false;
                $scope.table = false;
                $scope.edt = '';
                $scope.temp = '';
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);

        }])
})();

