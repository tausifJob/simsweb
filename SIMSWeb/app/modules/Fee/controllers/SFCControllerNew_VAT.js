﻿/// <reference path="StudentFeeConcessionCont.js" />
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SFCControllerNew_VAT',
        ['$scope', '$state', '$rootScope', '$timeout', '$stateParams', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, $stateParams, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            // Start
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.pagesize = '10';
            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.small_table = false;
            $scope.amount_total = 0;
            $scope.total_paid = 0;
            $scope.balance = 0;

            $scope.sortBy = function (propertyName) {

                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker(
                {
                    todayBtn: true,
                    orientation: "top left",
                    autoclose: true,
                    todayHighlight: true,
                    //format: 'yyyy-mm-dd'
                    format: 'dd-mm-yyyy'
                });
            var op = $stateParams.sel;
            if (op != undefined) {
                try {
                    $scope.edt = {};
                    $scope.edt.sims_cur_code = op.cur;
                    $scope.edt.sims_academic_year = op.ac;
                    $scope.edt.sims_grade_code = op.gr;
                    $scope.edt.sims_section_code = op.sc;
                    $scope.edt.search_txt = op.st;
                    //_onFetchData();
                } catch (e) {

                }
            }

           


            $scope.gotoCancelReceipt = function () {
                $state.go('main.Sm043C');
            }
            var sarr = ['adis', 'sbis'];//, 'pearl'
            var smfb = ['smfb', 'smfc'];

            var AJB = ['aji', 'ajb', 'ajn', 'egn', 'zps'];
            $scope.callDet = function (info) {

                debugger;
                if ($scope.sims_academic_year_status_code == 'O') {

                    swal({ text: "Academic Year is Closed", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                }
                else {
                    if (sarr.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043SBIS', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.std_fee_academic_year, 'StudEnroll': info.std_fee_enroll_number } });
                    }
                    else if (AJB.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043AJB', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.std_fee_academic_year, 'StudEnroll': info.std_fee_enroll_number } });
                    }
                    else if (smfb.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043SBISMFB', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.std_fee_academic_year, 'StudEnroll': info.std_fee_enroll_number } });
                    }
                    else {
                        $state.go('main.Sim043SearchNew_VAT', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.std_fee_academic_year, 'StudEnroll': info.std_fee_enroll_number } });
                    }
                }
            }

            //params: { 'StudCurr': '', 'StudAcademic': '', 'StudEnroll': '' }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '', sims_search_code: '' };
            /*FILTER*/
            $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllCurName").then(function (CurData) {
                debugger
                $scope.CurData = CurData.data;
                $scope.edt['sims_cur_code'] = CurData.data[0].sims_cur_code;
                $scope.cur_code_change(CurData.data[0].sims_cur_code);

            });
            $http.get(ENV.apiUrl + "api/Fee/SFS/getSeachOptions").then(function (searchoptions) {
                $scope.searchoptions = searchoptions.data;

                for (var i = 0; i < $scope.searchoptions.length; i++) {
                    if ($scope.searchoptions[i].sims_search_code_status == 'Y') {
                        $scope.edt['sims_search_code'] = $scope.searchoptions[i].sims_search_code;
                    }
                }
            });



            $scope.currentYear_status = 'C';
            $scope.cur_code_change = function () {

                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + $scope.edt.sims_cur_code).then(function (AcademicYears) {
                    $scope.AcademicYears = AcademicYears.data;
                    for (var i = 0; i < $scope.AcademicYears.length; i++) {
                        if ($scope.AcademicYears[i].sims_academic_year_status == 'C' || $scope.AcademicYears[i].sims_academic_year_status == "Current") {
                            //   $scope.edt['sims_cur_code'] = cur_values;
                            $scope.edt['sims_academic_year'] = $scope.AcademicYears[i].sims_academic_year;
                        }
                    }
                    $scope.academic_year_change();
                });

            }
            $scope.academic_year_change = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year).then(function (Grades) {
                    $scope.Grades = Grades.data;
                });

            }
            $scope.year_change = function () {
                debugger;

                for (var i = 0; i < $scope.AcademicYears.length; i++) {
                    if ($scope.edt.sims_academic_year == $scope.AcademicYears[i].sims_academic_year)
                        $scope.sims_academic_year_status_code = $scope.AcademicYears[i].sims_academic_year_status;

                }


            }


            $scope.grade_change = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllSections?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year + "&g_code=" + $scope.edt.sims_grade_code).then(function (sections) {
                    $scope.sections = sections.data;

                });

            }

            $scope.reset = function () {

                //$scope.edt = { 'sims_grade_code': '', 'sims_section_code': '', 'search_txt': '' };
                $scope.edt['sims_grade_code'] = '';
                $scope.edt['sims_section_code'] = '';
                $scope.edt['search_txt'] = '';
                $scope.feesData = '';
                $scope.todos = $scope.feesData;
                $scope.makeTodos();


            }

            /* PAGER FUNCTIONS*/

            //$scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 9, $scope.maxSize = 7, $scope.currentPage_ind = 0;
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 50, $scope.maxSize = 30; $scope.currentPage_ind = 0;
            $scope.allSize = false;

            $scope.makeTodos = function () {
                $scope.currentPage_ind = 0;

                if ((Math.round($scope.totalItems / $scope.numPerPage) * $scope.numPerPage) > $scope.totalItems)
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage);
                else
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage) + 1;

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
                $scope.amount_total = 0;
                $scope.total_paid = 0;
                $scope.balance = 0;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.amount_total = parseFloat($scope.filteredTodos[i].std_fee_amount) + parseFloat($scope.amount_total);
                    $scope.total_paid = parseFloat($scope.filteredTodos[i].std_TotalPaid) + parseFloat($scope.total_paid);
                    $scope.balance = parseFloat($scope.filteredTodos[i].std_BalanceFee) + parseFloat($scope.balance);
                }
            };

            //$scope.size = function (str) {
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.currentPage_ind = str;
            //    $scope.numPerPage = str;  $scope.makeTodos();
            //}

            $scope.size = function (str) {

                if (str == 'All') {

                    $scope.totalItems = $scope.feesData.length;
                    $scope.todos = $scope.feesData;
                    $scope.filteredTodos = $scope.GetAllStudentFee;
                    $scope.numPerPage = $scope.feesData.length;
                    $scope.maxSize = $scope.feesData.length
                    $scope.makeTodos();
                    $scope.allSize = true;
                }
                else {
                    $scope.allSize = false;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage_ind = str;
                $scope.currentPage = str;


                $scope.makeTodos();
            }

            /* PAGER FUNCTIONS*/

            function _onFetchData() {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllTotalStudent?acyear=" + $scope.edt.sims_academic_year).then(function (res) {
                    $scope.total_count = res.data;

                    $scope.count_total = $scope.total_count[0].student_count;

                });
                if ($scope.edt.search_txt == undefined)
                    $scope.edt.search_txt = '';
                $scope.sel = { 'cur': $scope.edt.sims_cur_code, 'ac': $scope.edt.sims_academic_year, 'gr': $scope.edt.sims_grade_code, 'sc': $scope.edt.sims_section_code, 'st': $scope.edt.search_txt, 'search_flag': '3' }
                $http.get(ENV.apiUrl + "api/Fee/SFS/getStudent_Fees?minval=0&maxval=0&cc=" + $scope.edt.sims_cur_code + "&ay=" + $scope.edt.sims_academic_year + "&gc=" + $scope.edt.sims_grade_code + "&sc=" + $scope.edt.sims_section_code + "&search_stud=" + $scope.edt.search_txt + "&search_flag=" + $scope.edt.sims_search_code).then(function (feesData) {
                    $scope.feesData = feesData.data;



                    if ($scope.feesData.length <= 0) {
                        swal(
                            {
                                showCloseButton: true,
                                text: 'No Data found.',
                                width: 350,
                                imageUrl: "assets/img/close.png",
                                showCloseButon: true
                            });
                    }
                    else {
                        if ($scope.feesData.length == 1) {
                            //$scope.callDet($scope.feesData[0]);
                        }
                        $scope.totalItems = $scope.feesData.length;
                        $scope.todos = $scope.feesData;
                        $scope.makeTodos();
                    }
                    $scope.length_array = $scope.feesData.length;

                });
            }
            $scope.fetchDatakeyPress = function ($event) {
                if ($event.keyCode == 13) {
                    _onFetchData();

                }
                else if ($event.keyCode == 5 && $event.ctrlKey && $event.shiftKey) {
                    $http.get(ENV.apiUrl + "api/StudentFee/GetReceipt").then(function (res) {
                        var rname = res.data;
                        var data = {
                            location: rname,
                            parameter: { fee_rec_no: '1120' },
                            state: 'main.Sim043'
                        }
                        window.localStorage["ReportDetails"] = JSON.stringify(data)
                        $state.go('main.ReportCardParameter')
                    });
                }
            }
            $scope.btnPreview_click = function () {
                _onFetchData();
            }
            //Events End

            /* Previous Details */

            $scope.student_name = '';

            $scope.onFetchPFRData = function (info) {
                debugger
                $scope.student_name = info.std_fee_enroll_number + '-' + info.std_fee_student_name;
                $scope.summery = { dd_fee_amount_final: 0, dd_fee_amount_discounted: 0, doc_status_code: 3 };

                $http.get(ENV.apiUrl + "api/Fee/SFS/GetPreviousReceipts_NEW?ay=" + info.std_fee_academic_year + "&enroll=" + info.std_fee_enroll_number).then(function (feesReceiptData) {
                    $scope.feesReceiptData = feesReceiptData.data[0];

                    $scope.small_table = true;

                    $scope.paid_amt = 0;
                    $scope.vat_amt = 0;
                    $scope.discounted_amt = 0;
                    $scope.total_amt = 0;

                    for (var i = 0; i < $scope.feesReceiptData.length; i++) {

                        $scope.paid_amt = parseFloat($scope.paid_amt) + parseFloat($scope.feesReceiptData[i].dd_fee_amount);
                        $scope.vat_amt = parseFloat($scope.vat_amt) + parseFloat($scope.feesReceiptData[i].dd_other_charge_amount);

                        $scope.discounted_amt = parseFloat($scope.discounted_amt) + parseFloat($scope.feesReceiptData[i].dd_fee_amount_discounted);
                        $scope.total_amt = parseFloat($scope.total_amt) + parseFloat($scope.feesReceiptData[i].dd_fee_amount_final);

                       /*$scope.expected_amt = parseFloat($scope.expected_amt) + parseFloat($scope.feesReceiptData[i].dd_fee_amount_final);
                        $scope.Fee_amt = parseFloat($scope.Fee_amt) + parseFloat($scope.feesReceiptData[i].dd_fee_amount_final);*/

                    }



                    //  $scope.feesDisReceiptData = feesReceiptData.data[1];

                    //angular.forEach($scope.feesDisReceiptData, function (value, key) {
                    //    value['sims_icon'] = 'fa fa-minus-circle';
                    //});




                    //$scope.doc_nos = [];

                    //angular.forEach($scope.feesReceiptData, function (value, key) {
                    //    var flag = false;

                    //    ////angular.forEach($scope.doc_nos, function (value1, key1) {
                    //    ////    if (value.doc_no == value1.doc_no)
                    //    ////        flag = true;
                    //    ////});


                    //    $scope.summery.dd_fee_amount_final = parseFloat($scope.summery.dd_fee_amount_final) + parseFloat(value.dd_fee_amount_final);
                    //    $scope.summery.dd_fee_amount_discounted = parseFloat($scope.summery.dd_fee_amount_discounted) + parseFloat(value.dd_fee_amount_discounted);
                    //});

                    // $scope.feesReceiptData.push($scope.summery);
                    $('#MyModal').modal({ backdrop: 'static', keyboard: false });

                });

                $scope.edt.sims_cur_code
                $scope.edt.sims_academic_year
                $scope.edt.sims_grade_code
                $scope.edt.sims_section_code

                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllTotalStudent?ay=" + info.std_fee_academic_year + "&enroll=" + info.std_fee_enroll_number).then(function (feesReceiptData) {
                });


            }


            $scope.expand = function (j, $event) {
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none";
                }
            }

            $scope.edtadv = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '', sims_search_code: '' };

            $scope.cur_code_change_adv = function () {

                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + $scope.edtadv.sims_cur_code).then(function (AcademicYears) {
                    $scope.AcademicYears = AcademicYears.data;
                });

            }
            $scope.academic_year_change_adv = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllGrades?cur_code=" + $scope.edtadv.sims_cur_code + "&ac_year=" + $scope.edtadv.sims_academic_year).then(function (Grades) {
                    $scope.AdvGrades = Grades.data;
                });

            }
            $scope.grade_change_adv = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllSections?cur_code=" + $scope.edtadv.sims_cur_code + "&ac_year=" + $scope.edtadv.sims_academic_year + "&g_code=" + $scope.edtadv.sims_grade_code).then(function (sections) {
                    $scope.Advsections = sections.data;

                });

            }


            $scope.divAdvanceYearFee = function (info) {
                var adv_academic_year_status = ''
                debugger;
                for (var i = 0; i < $scope.AcademicYears.length; i++) {
                    if (info.adv_fee_year == $scope.AcademicYears[i].sims_academic_year)
                        adv_academic_year_status = $scope.AcademicYears[i].sims_academic_year_status;

                }
                if (adv_academic_year_status != 'O') {

                $scope.student_name = info.std_fee_enroll_number + '-' + info.std_fee_student_name;
                if (info.is_adv_section_defined) {
                    if (sarr.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043SBIS', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.adv_fee_year, 'StudEnroll': info.std_fee_enroll_number } });
                    }
                    else if (AJB.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043AJB', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.adv_fee_year, 'StudEnroll': info.std_fee_enroll_number } });
                    }
                    else if (smfb.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043SBISMFB', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.std_fee_academic_year, 'StudEnroll': info.std_fee_enroll_number } });
                    }
                    else {
                        $state.go('main.Sim043SearchNew_VAT', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.adv_fee_year, 'StudEnroll': info.std_fee_enroll_number } });
                    }
                }
                else {
                    $scope.edtadv['sims_cur_code'] = info.std_fee_cur_code;
                    $scope.cur_code_change_adv();
                    $scope.edtadv['sims_academic_year'] = info.adv_fee_year;
                    $scope.academic_year_change_adv();
                    $scope.edtadv['sims_grade_code'] = info.is_adv_grade;
                    $scope.grade_change_adv();
                    $scope.edtadv['sims_section_code'] = info.is_adv_section;
                    $scope.edtadv['std_fee_enroll_number'] = info.std_fee_enroll_number;
                    $('#divAdvanceYearFee').modal({ backdrop: 'static', keyboard: true });
                }
                }
                else {
                    swal({ text: "Academic Year is Closed", imageUrl: "assets/img/check.png", width: 300, height: 200 });

                }

            }
            $scope.closeAdv = function () {
                $('#divAdvanceYearFee').modal('hide');
            }
            $scope.submitAdvFees = function () {
                //string cur_code,string ay,string grade,string section,string enroll
                $('#divAdvanceYearFee').modal('hide');

                $http.post(ENV.apiUrl + "api/Fee/SFS/DefineAdvFee?cur_code=" + $scope.edtadv.sims_cur_code + "&ay=" + $scope.edtadv.sims_academic_year + "&grade=" + $scope.edtadv.sims_grade_code + "&section=" + $scope.edtadv.sims_section_code + "&enroll=" + $scope.edtadv.std_fee_enroll_number).then(function (result) {
                    if (sarr.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043SBIS', { 'sel': $scope.sel, 'IP': { 'StudCurr': $scope.edtadv.sims_cur_code, 'StudAcademic': $scope.edtadv.sims_academic_year, 'StudEnroll': $scope.edtadv.std_fee_enroll_number } });
                    }
                    else if (AJB.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043AJB', { 'sel': $scope.sel, 'IP': { 'StudCurr': $scope.edtadv.sims_cur_code, 'StudAcademic': $scope.edtadv.sims_academic_year, 'StudEnroll': $scope.edtadv.std_fee_enroll_number } });
                    }
                    else if (smfb.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043SBISMFB', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.std_fee_academic_year, 'StudEnroll': info.std_fee_enroll_number } });
                    }
                    else {
                        $state.go('main.Sim043SearchNew_VAT', { 'sel': $scope.sel, 'IP': { 'StudCurr': $scope.edtadv.sims_cur_code, 'StudAcademic': $scope.edtadv.sims_academic_year, 'StudEnroll': $scope.edtadv.std_fee_enroll_number } });
                    }
                });
            }

        }])
})();

