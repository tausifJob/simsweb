﻿/// <reference path="StudentFeeConcessionCont.js" />
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SiblingConcessionController',
        ['$scope', '$state', '$rootScope', '$timeout', '$stateParams', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, $stateParams, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            // Start
            
            $('*[data-datepicker="true"] input[type="text"]').datepicker(
                {
                    todayBtn: true,
                    orientation: "top left",
                    autoclose: true,
                    todayHighlight: true,
                    format: 'yyyy-mm-dd'
                });
            var op = $stateParams.sel;
            if (op != undefined) {
                try {
                    $scope.edt = {};
                    $scope.edt.sims_cur_code = op.cur;
                    $scope.edt.sims_academic_year = op.ac;
                    $scope.edt.sims_grade_code = op.gr;
                    $scope.edt.sims_section_code = op.sc;
                    $scope.edt.search_txt = op.st;
                    //_onFetchData();
                } catch (e) {

                }
            }
            
            
            //params: { 'StudCurr': '', 'StudAcademic': '', 'StudEnroll': '' }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '', sims_search_code: '',receipt_start_date:'',receipt_end_date:'' };
            /*FILTER*/
            $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllCurName").then(function (CurData) {
                $scope.CurData = CurData.data;
                $scope.edt['sims_cur_code'] = CurData.data[0].sims_cur_code;
                $scope.cur_code_change(CurData.data[0].sims_cur_code);

            });
            $http.get(ENV.apiUrl + "api/Fee/SFS/getSeachOptions").then(function (searchoptions) {
                $scope.searchoptions = searchoptions.data;

                for (var i = 0; i < $scope.searchoptions.length; i++) {
                    if ($scope.searchoptions[i].sims_search_code_status == 'Y') {
                        $scope.edt['sims_search_code'] = $scope.searchoptions[i].sims_search_code;
                    }
                }
            });



            $scope.currentYear_status = 'C';
            $scope.cur_code_change = function () {
                
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + $scope.edt.sims_cur_code).then(function (AcademicYears) {
                    $scope.AcademicYears = AcademicYears.data;
                    for (var i = 0; i < $scope.AcademicYears.length; i++) {
                        if ($scope.AcademicYears[i].sims_academic_year_status == 'C') {
                            //   $scope.edt['sims_cur_code'] = cur_values;
                            $scope.edt['sims_academic_year'] = $scope.AcademicYears[i].sims_academic_year;
                        }
                    }
                    $scope.academic_year_change();
                });

            }
            $scope.academic_year_change = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year).then(function (Grades) {
                    $scope.Grades = Grades.data;
                });
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllConcession?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year).then(function (Concessions) {
                    $scope.Concessions = Concessions.data;
                });

            }
            $scope.grade_change = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllSections?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year + "&g_code=" + $scope.edt.sims_grade_code).then(function (sections) {
                    $scope.sections = sections.data;
                    
                });

            }

            $scope.reset = function () {
               
                //$scope.edt = { 'sims_grade_code': '', 'sims_section_code': '', 'search_txt': '' };
                $scope.edt['sims_grade_code'] = '';
                $scope.edt['sims_section_code'] = '';
                $scope.edt['search_txt'] = '';
                $scope.feesData = '';
                $scope.todos = $scope.feesData;
                $scope.makeTodos();
              

            }

            /* PAGER FUNCTIONS*/

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 15, $scope.maxSize = 7, $scope.currentPage_ind = 0;

            $scope.makeTodos = function () {
                var main = document.getElementById('mainchk');
                main.checked = false;
                $scope.currentPage_ind = 0;

                if ((Math.round($scope.totalItems / $scope.numPerPage) * $scope.numPerPage) > $scope.totalItems)
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage);
                else
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage) + 1;

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.currentPage_ind = str;
                $scope.numPerPage = str;  $scope.makeTodos();
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage_ind = str;
                $scope.currentPage = str;
                

                $scope.makeTodos();
            }

            /* PAGER FUNCTIONS*/
            function _onFetchData() {
                if ($scope.edt.search_txt == undefined)
                    $scope.edt.search_txt = '';
                $scope.sel = { 'cur': $scope.edt.sims_cur_code, 'ac': $scope.edt.sims_academic_year, 'gr': $scope.edt.sims_grade_code, 'sc': $scope.edt.sims_section_code, 'st': $scope.edt.search_txt, 'search_flag': '3' }
                $http.get(ENV.apiUrl + "api/Fee/SFS/getStudentSiblingConcession?cc=" + $scope.edt.sims_cur_code + "&ay=" + $scope.edt.sims_academic_year + "&gc=" + $scope.edt.sims_grade_code + "&sc=" + $scope.edt.sims_section_code + "&concession_no=" + $scope.edt.sims_concession_number).then(function (feesData) {
                    $scope.feesData = feesData.data;
                        if ($scope.feesData.length <= 0) {
                        swal(
                            {
                                showCloseButton: true,
                                text: 'No Data found.',
                                width: 350,
                                imageUrl: "assets/img/close.png",
                                showCloseButon: true
                            });
                    }
                    else {
                        //if ($scope.feesData.length == 1) {
                        //   // $scope.callDet($scope.feesData[0]);
                        //}
                        $scope.totalItems = $scope.feesData.length;
                        $scope.todos = $scope.feesData;
                        $scope.makeTodos();
                    }
                });
            }
            $scope.btnPreview_click = function () {
                if ($scope.edt.sims_grade_code != "" && $scope.edt.sims_grade_code && $scope.edt.sims_section_code != "" && $scope.edt.sims_section_code && $scope.edt.sims_concession_number != "" && $scope.edt.sims_concession_number) {
                    $scope.msg1 = false;
                    _onFetchData();
                }
                else {
                    $scope.msg1 = 'Please Select All Fields';
                }
            }
            //Events End

            $scope.save_concession = function () {
                var flag = false;
                for (var i = 0; i < $scope.feesData.length; i++)
                {
                    if ($scope.feesData[i].std_fee_selected_status == true) {
                        flag = true;
                    }
                }
                if ($scope.edt.receipt_start_date == '' || $scope.edt.receipt_end_date == '') {
                    swal({
                        text: 'Please select From And To date.',
                        width: 400,
                        height: 300,
                        imageUrl: "assets/img/notification-alert.png"
                    });
                    return;
                }
                if (flag == false) {
                    swal({
                        text: 'Please select at Least one student.',
                        width: 400,
                        height: 300,
                        imageUrl: "assets/img/notification-alert.png"
                    });
                    return;
                }
                $http.post(ENV.apiUrl + "api/Fee/SFS/StudentSiblingConcession?concession_no=" + $scope.edt.sims_concession_number + "&from_date=" + $scope.edt.receipt_start_date + "&to_date=" + $scope.edt.receipt_end_date + "&created_user=admin", $scope.feesData).then(function (res)
                {
                    if (res.data == true) {
                        swal({
                            text: 'Concession Mapped Sucessfully.',
                            width: 400,
                            height: 300,
                            imageUrl: "assets/img/check.png"
                        });
                    }
                    else {
                        swal({
                            text: 'Error while mapping concession.' ,
                            width: 400,
                            height: 300
                        });
                    }
                    _onFetchData();

                });
            }

            $scope.CheckMultiple = function () {
                var main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].std_fee_enroll_number;
                    var v = document.getElementById(t);
                    if (($scope.filteredTodos[i].std_fee_status == false && $scope.filteredTodos[i].std_fee_Concession_status == 'P')
                        ||
                        ($scope.filteredTodos[i].std_fee_status && $scope.filteredTodos[i].std_fee_Concession_status == 'A')
                        )
                    {
                        continue;
                    }
                    else {
                        if (main.checked == true) {
                            v.checked = true;
                            $scope.row1 = 'row_selected';
                            $scope.filteredTodos[i].std_fee_selected_status = true;
                        }
                        else {
                            v.checked = false;
                            $scope.row1 = '';
                            $scope.filteredTodos[i].std_fee_selected_status = false;
                        }
                    }
                }
            }
            $scope.check1 = function (obj) {
                var main = document.getElementById('mainchk');
                var check = document.getElementById(obj.std_fee_enroll_number);
                var t = obj.std_fee_enroll_number;
                var v = document.getElementById(t);
                if (v.checked == false) {
                    obj.std_fee_selected_status = false;
                    main.checked = false;
                }
                else {
                    obj.std_fee_selected_status = true;
                }
            }

            $scope.delete_concession = function (obj) {
                $http.post(ENV.apiUrl + "api/Fee/SFS/StudentSiblingConcessionremv?concession_no=" + $scope.edt.sims_concession_number, obj).then(function (res)
                {
                    if (res.data == true) {
                        swal({
                            text: 'Concession removed Sucessfully.',
                            width: 400,
                            height: 300,
                            imageUrl: "assets/img/check.png"
                        });
                    }
                    else {
                        swal({
                            text: 'Error while removeing concession.',
                            width: 400,
                            height: 300,
                            imageUrl: "assets/img/close.png"
                        });
                    }
                    _onFetchData();

                });
            }

            $scope.edt = {};
            var dt = new Date();
            $scope.edt.receipt_start_date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
            $scope.edt.receipt_end_date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();


        }])
})();

