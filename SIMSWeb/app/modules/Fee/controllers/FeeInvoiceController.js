﻿﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('FeeInvoiceCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            // Start
            $('*[data-datepicker="true"] input[type="text"]').datepicker(
                {
                    todayBtn: true,
                    orientation: "top left",
                    autoclose: true,
                    todayHighlight: true,
                    format: 'yyyy-mm-dd'
                });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '' };;
            $scope.info = [];
            $scope.edt1 = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '' };
            /*FILTER*/

            $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllCurName").then(function (CurData) {
                $scope.CurData = CurData.data;
                $scope.edt['sims_cur_code'] = CurData.data[0].sims_cur_code;
                $scope.edt1['sims_cur_code'] = CurData.data[0].sims_cur_code;
                $scope.cur_code_change(CurData.data[0].sims_cur_code);

            });


            $scope.currentYear_status = 'C';

            $scope.cur_code_change = function (cur_values) {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + cur_values).then(function (AcademicYears) {
                    $scope.AcademicYears = AcademicYears.data;
                    for (var i = 0; i < $scope.AcademicYears.length; i++) {
                        if ($scope.AcademicYears[i].sims_academic_year_status == 'C' || $scope.AcademicYears[i].sims_academic_year_status == 'Current') {
                            $scope.edt['sims_academic_year'] = $scope.AcademicYears[i].sims_academic_year;
                            $scope.edt1['sims_academic_year'] = $scope.AcademicYears[i].sims_academic_year;
                        }
                    }

                    $scope.academic_year_change(cur_values, $scope.edt.sims_academic_year);
                    $http.get(ENV.apiUrl + "api/Fee/SFS/Getsims541_InvoiceMode").then(function (InvoiceMode) {
                        $scope.InvoiceMode = InvoiceMode.data;
                        $scope.edt['sims_invoce_mode_code'] = $scope.InvoiceMode[0].sims_invoce_mode_code;
                        $scope.InvoiceMode_change();
                    });
                });
            }



            $scope.academic_year_change = function (cur_values, academic_year) {

                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllGrades?cur_code=" + cur_values + "&ac_year=" + academic_year).then(function (Grades) {
                    $scope.Grades = Grades.data;
                });
                $scope.InvoiceMode_change();

                //$http.get(ENV.apiUrl + "api/Fee/SFS/GetAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year).then(function (Grades) {
                //    $scope.Grades = Grades.data;
                //});

            }

            $scope.grade_change = function (cur_values, academic_year, grade) {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllSections?cur_code=" + cur_values + "&ac_year=" + academic_year + "&g_code=" + grade).then(function (sections) {
                    $scope.sections = sections.data;
                });
                //$http.get(ENV.apiUrl + "api/Fee/SFS/GetAllSections?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year + "&g_code=" + $scope.edt.sims_grade_code).then(function (sections) {
                //    $scope.sections = sections.data;
               

                //});

            }


            $scope.section_change = function () {
                if ($scope.feeonInvoice == true) {
                    $http.get(ENV.apiUrl + "api/Fee/SFS/Getsims541_FeeTypes?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&grade_code=" + $scope.edt.sims_grade_code + "&section_code=" + $scope.edt.sims_section_code).then(function (fee_type) {
                        $scope.fee_type = fee_type.data;
                    });
                }
            }

            $scope.InvoiceValue_change = function (str) {
                $scope.edt.sims_invoce_frq_name_value = str;
            }


            $http.get(ENV.apiUrl + "api/Fee/SFS/Getsims541_InvoiceReportUrl").then(function (InvoiceReport) {
                $scope.InvoiceReport = InvoiceReport.data;
            });

            
            $scope.InvoiceMode_change = function () {
                

                $http.get(ENV.apiUrl + "api/Fee/SFS/Getsims541_FrequencyValue?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&freq_flag=" + $scope.edt.sims_invoce_mode_code).then(function (FeeFreqValue) {
                    $scope.FeeFreqValue = FeeFreqValue.data;
                    $scope.edt['sims_invoce_frq_name_value'] = $scope.FeeFreqValue[0].sims_invoce_frq_name_value;
                });

            }


            $scope.feeonInvoice = false;
            $http.get(ENV.apiUrl + "api/Fee/SFS/Getsims541_CreationOnFee").then(function (InvoiceOnFee) {
                $scope.InvoiceOnFee = InvoiceOnFee.data;
                if ($scope.InvoiceOnFee == 'N' || $scope.InvoiceOnFee == '')
                    $scope.feeonInvoice = false;
                else
                    $scope.feeonInvoice = true;
            });


            /* PAGER FUNCTIONS*/

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 7, $scope.currentPage_ind = 0;
            $scope.makeTodos = function () {
                $scope.currentPage_ind = 0;

                if ((Math.round($scope.totalItems / $scope.numPerPage) * $scope.numPerPage) > $scope.totalItems)
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage);
                else
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage) + 1;

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                var main = document.getElementById('mainchk');
                main.checked = false;
                $scope.CheckMultiple();

            };
            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.currentPage_ind = str;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage_ind = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }


            /* PAGER FUNCTIONS*/
            function _onFetchData() {
                debugger
                if ($scope.edt.sims_grade_code == undefined) $scope.edt = { sims_grade_code: '' };
                if ($scope.edt.sims_section_code == undefined) $scope.edt = { sims_section_code: '' };
                if ($scope.edt.sims_enroll_number == undefined) $scope.edt.sims_enroll_number = '';
                if ($scope.edt.sims_fee_code == undefined) $scope.edt.sims_fee_code = '';
                var main = document.getElementById('mainchk');
                main.checked = false;

                $http.get(ENV.apiUrl + "api/Fee/SFS/Getsims541_InvoiceStudents?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&grade_code=" + $scope.edt.sims_grade_code + "&section_code=" + $scope.edt.sims_section_code + "&term_code=" + $scope.edt.sims_invoce_frq_name_value + "&search=" + $scope.edt.sims_enroll_number + "&fee_code_search=" + $scope.edt.sims_fee_code).then(function (InvoiceData) {
                    $scope.InvoiceData = InvoiceData.data;
                    $scope.totalItems = $scope.InvoiceData.length;
                    $scope.todos = $scope.InvoiceData;
                    $scope.filteredTodos = InvoiceData.data;
                    //$scope.currentPage = 0;
                    //  $scope.makeTodos();
                });
            }
            $scope.search = function ($event) {
                if ($event.keyCode == 13) {
                    _onFetchData();
                }
            }
            $scope.btnReset_click = function () {
                $scope.edt.sims_cur_code = '';
                $scope.edt.sims_academic_year = '';
                $scope.edt.sims_cur_code = '';
                $scope.edt.sims_invoce_mode_code = '';
                $scope.edt.sims_invoce_frq_name_value = '';
                $scope.edt.sims_grade_code = '';
                $scope.edt.sims_section_code = '';
                $scope.edt.sims_enroll_number = '';
                $scope.todos = [];
                $scope.makeTodos();
            }
            $scope.btnPreview_click = function () {
                _onFetchData();
            }
           /* $scope.checkInvoice = function (str) {
                debugger;
                $http.get(ENV.apiUrl + "api/Fee/SFS/getGeneratedInvoice?&enroll_number=" + str.sims_enroll_number + "&academic_year=" + $scope.edt.sims_academic_year).then(function (res) {
                                      $scope.generateddata = res.data;
                                     
                                      if($scope.generateddata.length > 0) {
                                          alert("Invoice Already generated,you can't generate");
                                          var numb = str.sims_enroll_number;
                                          var cbox = document.getElementById(numb);
                                          if (cbox.checked == true)
                                              cbox.checked = false;
                                          str.sims_invoice_status = false;
                                         
                                      }
                                      else {
                                          btnGenerate_invoice();
                                      }
                                  });
            }*/

            $scope.btnGenerate_invoice = function () {
                debugger;
                var isSelected = false;
               
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].sims_invoice_status == true) {
                      
                        var isSelected = true; break;
                    }
                }
                if (isSelected == true) {
                   

                    $http.post(ENV.apiUrl + "api/Fee/SFS/GenerateInvoiceForStudents?&term_code=" + $scope.edt.sims_invoce_frq_name_value + "&invoice_mode=" + $scope.edt.sims_invoce_mode_code + "&payingAgentFlag=N&fee_code_search=" + $scope.edt.sims_fee_code, $scope.filteredTodos).then(function (result) {
                        var result = result.data;
                        if (result == true) {
                            swal(
                            {
                                showCloseButton: true,
                                text: 'Invoice Generated Sucessfully',
                                imageUrl: "assets/img/check.png",
                                width: 350,
                                showCloseButon: true
                            });
                            _onFetchData();
                        }
                        else {
                            swal(
                           {
                               showCloseButton: true,
                               text: 'Error in Generating Invoice',
                               imageUrl: "assets/img/close.png",
                               width: 350,
                               showCloseButon: true
                           });
                        }
                    });
                }
                else {
                    swal({
                        
                        text: 'Please Select Student to Generate Invoice',imageUrl: "assets/img/notification-alert.png", width: 350, showCloseButon: true
                    });
                }
                var main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    //$scope.color = '#edefef';
                }
            }
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);


            /*SEARCH INVOICES*/
            /*PAGER*/
            $scope.filteredTodos_invoice = [], $scope.currentPage_invoice = 1, $scope.numPerPage_invoice = 10, $scope.maxSize_invoice = 10;
            $scope.makeTodos_invoice = function () {
                if ((Math.round($scope.totalItems_invoice / $scope.numPerPage_invoice) * $scope.numPerPage_invoice) > $scope.totalItems_invoice)
                    $scope.pagersize_invoice = Math.round($scope.totalItems_invoice / $scope.numPerPage_invoice);
                else
                    $scope.pagersize_invoice = Math.round($scope.totalItems_invoice / $scope.numPerPage_invoice) + 1;
                var begin_invoice = (($scope.currentPage_invoice - 1) * $scope.numPerPage_invoice);
                var end_invoice = parseInt(begin_invoice) + parseInt($scope.numPerPage_invoice);
                $scope.filteredTodos_invoice = $scope.InvoiceDetailsData.slice(begin_invoice, end_invoice);
                var main = document.getElementById('chkSearch');
                main.checked = false;
                $scope.CheckMultipleSearch();

            };
            $scope.size_invoice = function (str) {
                $scope.pagesize_invoice = str;
                $scope.currentPage_invoice = 1;
                $scope.numPerPage_invoice = str;
                $scope.makeTodos_invoice();
            }
            $scope.index_invoice = function (str) {
                $scope.pageindex_invoice = str;
                $scope.currentPage_invoice = str;
                $scope.makeTodos_invoice();
            }
            /*PAGER*/

            $scope.seachInvoiceKeyPress = function ($event) {
                if ($event.keyCode == 13) {
                    $scope.seachInvoice(2);
                }
            }
            $scope.seachInvoice = function (flag) {
                debugger;
                $scope.hideIfEmpty_invoice = false;
                var search = $scope.info.sims_enroll_number;

                var main = document.getElementById('chkSearch');
                main.checked = false;
                $scope.info = [];
                if (flag != 1) {

                    $scope.info.sims_cur_code = $scope.edt.sims_cur_code;
                    $scope.info.sims_academic_year = $scope.edt1.sims_academic_year;
                    $scope.info.sims_grade_code = $scope.edt1.sims_grade_code;
                    $scope.info.sims_enroll_number = $scope.edt1.sims_enroll_number;
                    // if ($scope.info.sims_enroll_number == undefined) $scope.info.sims_enroll_number = '';
                    $scope.info.sims_section_code = $scope.edt1.sims_section_code;


                }
                if (flag == 1) {

                    $scope.info.sims_cur_code = $scope.edt.sims_cur_code;
                    $scope.info.sims_academic_year = $scope.edt.sims_academic_year;
                    $scope.info.sims_grade_code = $scope.edt1.sims_grade_code;
                    $scope.info.sims_enroll_number = $scope.edt1.sims_enroll_number;
                    $scope.info.sims_section_code = $scope.edt1.sims_section_code;

                    //                    if ($scope.info.sims_enroll_number == undefined) $scope.info.sims_enroll_number = '';

                }


                if ($scope.info.sims_cur_code == undefined) $scope.info.sims_cur_code = '';
                if ($scope.info.sims_academic_year == undefined) $scope.info.sims_academic_year = '';
                if ($scope.info.sims_grade_code == undefined) $scope.info.sims_grade_code = '';
                if ($scope.info.sims_section_code == undefined) $scope.info.sims_section_code = '';

                if ($scope.info.sims_enroll_number == undefined) $scope.info.sims_enroll_number = '';

                if ($scope.info.sims_cur_code != undefined || $scope.info.sims_enroll_number != '') {

                    $http.post(ENV.apiUrl + "api/Fee/SFS/SearchInvoice?search_code=" + search + "&cur_code=" + $scope.info.sims_cur_code + "&academic_year=" + $scope.info.sims_academic_year + "&grade_code=" + $scope.info.sims_grade_code + "&section_code=" + $scope.info.sims_section_code).then(function (InvoiceDetailsData) {
                        

                        if (InvoiceDetailsData.data.length <= 0) {
                            if (flag != 1) {
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'No Result Found',
                                    width: 350,
                                    showCloseButon: true
                                });
                            }
                            $scope.hideIfEmpty_invoice = false;
                            $scope.InvoiceDetailsData = [];
                        }
                        else {
                            $scope.hideIfEmpty_invoice = true;
                            $scope.InvoiceDetailsData = InvoiceDetailsData.data;
                            $scope.totalItems_invoice = $scope.InvoiceDetailsData.length;
                            $scope.filteredTodos_invoice = $scope.InvoiceDetailsData;
                            //$scope.currentPage_invoice = 0;
                            $scope.makeTodos_invoice();
                        }

                    });
                }
            }
            $scope.viewInvoice = function (info, flag) {
                //Default Cur Selection
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllCurName").then(function (CurData) {
                    $scope.CurData = CurData.data;
                    $scope.edt1['sims_cur_code'] = CurData.data[0].sims_cur_code;
                    $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + $scope.edt1['sims_cur_code']).then(function (AcademicYears) {
                        $scope.AcademicYears = AcademicYears.data;
                        for (var i = 0; i < $scope.AcademicYears.length; i++) {
                            if ($scope.AcademicYears[i].sims_academic_year_status == 'C' || $scope.AcademicYears[i].sims_academic_year_status == 'Current') {
                                $scope.edt1['sims_academic_year'] = $scope.AcademicYears[i].sims_academic_year;
                            }
                        }
                    });
                });


                $scope.hideIfEmpty_invoice = false;

                if (flag == 1) {
                    $http.post(ENV.apiUrl + "api/Fee/SFS/SearchInvoice?search_code=" + info.sims_enroll_number + "&cur_code=" + info.sims_cur_code + "&academic_year=" + info.sims_academic_year + "&grade_code=" + info.sims_grade_code + "&section_code=" + info.sims_section_code).then(function (InvoiceDetailsData) {
                        $scope.hideIfEmpty_invoice = true;
                        $scope.InvoiceDetailsData = InvoiceDetailsData.data;
                     
                        $scope.totalItems_invoice = $scope.InvoiceDetailsData.length;
                        $scope.filteredTodos_invoice = $scope.InvoiceDetailsData;
                        $scope.currentPage_invoice = 1;
                        $scope.makeTodos_invoice();
                    });
                }
                else if (flag == 2) {
                    $scope.info.sims_enroll_number = '';
                    $scope.resetSeachInvoice();
                }
                $('#MyModal').modal('show');
            }
            $scope.resetSeachInvoice = function () {
                $scope.InvoiceDetailsData = [];
                $scope.filteredTodos_invoice = [];
                $scope.info.sims_enroll_number = '';
                $scope.edt1 = [];
            }
            function viewInvoice(info) {
                $http.post(ENV.apiUrl + "api/Fee/SFS/SearchInvoice?search_code=" + $scope.info.sims_enroll_number + "&cur_code=" + $scope.info.sims_cur_code + "&academic_year=" + $scope.info.sims_academic_year + "&grade_code=" + $scope.info.sims_grade_code + "&section_code=" + $scope.info.sims_section_code).then(function (InvoiceDetailsData) {
                    $scope.InvoiceDetailsData = InvoiceDetailsData.data;
                });
            }
            $scope.deleteInvoice = function () {
                $scope.deleteInvoiceNos = [];
                var flag = false;
                for (var i = 0; i < $scope.filteredTodos_invoice.length; i++) {

                    if ($scope.filteredTodos_invoice[i].sims_invoice_status == true) {
                        flag = true;
                        break;
                    }
                }
                if (flag == false) {
                    swal(
                                {
                                    showCloseButton: true,
                                    text: 'Please select Invoice(s) to Delete.',
                                    imageUrl: "assets/img/notification-alert.png",
                                    width: 350,
                                    showCloseButon: true,
                                    allowOutsideClick: true,
                                });
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos_invoice.length; i++) {

                        if ($scope.filteredTodos_invoice[i].sims_invoice_status == true) {
                            $scope.deleteInvoiceNos.push($scope.filteredTodos_invoice[i].in_no);
                        }
                    }
                    $http.post(ENV.apiUrl + "api/Fee/SFS/updateInvoices?invoice_nos=", $scope.deleteInvoiceNos).then(function (result) {
                        if (result.data == true) {
                            swal(
                               {
                                   showCloseButton: true,
                                   text: 'Selected Invoice(s) are deleted.',
                                   imageUrl: "assets/img/notification-alert.png",
                                   width: 350,
                                   showCloseButon: true,
                                   allowOutsideClick: true,
                               });

                            $scope.seachInvoice(1);
                            var main = document.getElementById('chkSearch');
                            main.checked = false;
                            $scope.refresh();//Main Grid
                        }
                        else {
                            swal(
                                {
                                    showCloseButton: true,
                                    text: 'Error while Deleting Invoice(s).',
                                    imageUrl: "assets/img/notification-alert.png",
                                    width: 350,
                                    showCloseButon: true
                                });
                        }

                    });

                }
            }
            $scope.isGenerateEnable = true;
            $scope.CheckMultipleSearch = function () {
                var main = document.getElementById('chkSearch');
                for (var i = 0; i < $scope.filteredTodos_invoice.length; i++) {
                    if (main.checked == true) {
                        $scope.filteredTodos_invoice[i].sims_invoice_status = true;
                    }
                    else if (main.checked == false) {
                        $scope.row1 = '';
                        $scope.color = '#edefef';
                        main.checked = false;
                        $scope.filteredTodos_invoice[i].sims_invoice_status = false;
                    }
                }
            }
            $scope.CheckOneByOneSearch = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                    }
                });

                var main = document.getElementById('chkSearch');
                if (main.checked == true) {
                    main.checked = false;
                    //$scope.color = '#edefef';
                }

            }
            $scope.CheckMultiple = function () {
                paging();
            }
            function paging() {
                var main = document.getElementById('mainchk');

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_enroll_number;
                    var v = document.getElementById(t);
                    if (main.checked == true && !$scope.filteredTodos[i].sims_invoice_student_status) {
                        $scope.filteredTodos[i].sims_invoice_status = true;
                        v.checked = true;
                        $('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                        $scope.isGenerateEnable = false;
                    }
                    else if (main.checked == false) {

                        $('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                        main.checked = false;
                        $scope.isGenerateEnable = true;
                        $scope.filteredTodos[i].sims_invoice_status = false;

                    }
                }
            }
            $scope.refresh = function () { _onFetchData(); }
            $scope.CheckOneByOne = function () {

                //$("input[type='checkbox']").click(function (e) {
                //    if ($(this).is(":checked")) {
                //        $(this).closest('tr').addClass("row_selected");
                //        $scope.isGenerateEnable = true;
                //    }
                //    else {
                //        $scope.isGenerateEnable = false;
                //        $(this).closest('tr').removeClass("row_selected");
                //    }
                //});

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                    }
                });

                var main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    //$scope.color = '#edefef';
                }

            }

            $scope.Report = function (str) {
                var data = {
                    location: $scope.InvoiceReport,
                    parameter: {
                        cur_code: str.sims_cur_code,
                        acad_year: str.sims_academic_year,
                        grade_code: str.sims_grade_code,
                        section_code: str.sims_section_code,
                        enroll: str.sims_enroll_number,
                        invmode: str.sims_invoce_mode,
                        term_code: str.sims_period_code,
                        in_no: str.in_no,
                    },
                    state: 'main.Sim541',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
            }
            //Events End

            ////// Signature Capture code start ////

            $scope.Capture = function () {

                $('#signature_pad_modal').modal({ backdrop: 'static', keyboard: false });
                try {
                    // alert("Capturing signature...");
                    var sigCtl = document.getElementById("sigCtl1");
                    var dc = new ActiveXObject("Florentis.DynamicCapture");
                    var rc = dc.Capture(sigCtl, "who", "why");
                    if (rc != 0)
                        print("Capture returned: " + rc);
                    switch (rc) {
                        case 0: // CaptureOK
                            alert("Signature captured successfully");
                            //                            var txtSignature = document.getElementById("txtSignature");
                            var flags = 0x2000 + 0x80000 + 0x400000; //SigObj.outputBase64 | SigObj.color32BPP | SigObj.encodeData
                            $scope.b64 = sigCtl.Signature.RenderBitmap("", 300, 150, "image/png", 0.5, 0xff0000, 0xffffff, 0.0, 0.0, flags);
                            //                          txtSignature.value = b64;
                            $scope.imgSrcData = "data:image/png;base64," + $scope.b64;
                            //  document.getElementById("b64image").src = imgSrcData;
                            console.log($scope.imgSrcData);
                        case 1: // CaptureCancel
                            alert("Signature capture cancelled");
                            break;
                        case 100: // CapturePadError
                            alert("No capture service available");
                            break;
                        case 101: // CaptureError
                            alert("Tablet Error");
                            break;
                        case 102: // CaptureIntegrityKeyInvalid
                            alert("The integrity key parameter is invalid (obsolete)");
                            break;
                        case 103: // CaptureNotLicensed
                            alert("No valid Signature Capture licence found");
                            break;
                        case 200: // CaptureAbort
                            alert("Error - unable to parse document contents");
                            break;
                        default:
                            alert("Capture Error " + rc);
                            break;
                    }
                }
                catch (ex) {
                    console.log("Capture() error: " + ex.message);
                }
            }

            $scope.clearSign = function () {
                try {
                    var sigCtl = document.getElementById("sigCtl1");
                    sigCtl.Signature.Clear();
                }
                catch (ex) {
                    console.log("ClearSignature() error: " + ex.message);
                }
            }
            $scope.saveSignatureData = function (x, y, z) {
                debugger;
                if ($scope.imgSrcData != undefined) {
                    var signData = {
                        academic_year: '',
                        parent_id: '',
                        student_id: '',
                        signature_details: $scope.imgSrcData,
                        signature_img_path: '',
                        takenby_user_name: $rootScope.globals.currentUser.username
                    }

                    $http.post(ENV.apiUrl + "api/signature/InsertSignatureDetails", signData).then(function (res) {
                        $scope.result = res.data;
                        if ($scope.result) {
                            swal('Success', 'Records Saved Successfully');
                            $("#signature_pad_modal").modal('hide');
                        } else {
                            swal('Error', 'Records Not Saved');
                        }
                    });
                } else {
                    swal('Signature is Empty');
                }
            }



            $scope.modal_cancel = function () {
                $("#signature_pad_modal").modal('hide');
            }

            /////////////// end //////////////

        }])

})();

