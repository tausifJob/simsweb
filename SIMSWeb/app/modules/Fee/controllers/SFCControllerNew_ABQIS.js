﻿/// <reference path="StudentFeeConcessionCont.js" />
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SFCControllerNew_ABQIS',
        ['$scope', '$state', '$rootScope', '$timeout', '$stateParams', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, $stateParams, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            // Start
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.pagesize = '10';
            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.small_table = false;


            $scope.sortBy = function (propertyName) {

                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker(
                {
                    todayBtn: true,
                    orientation: "top left",
                    autoclose: true,
                    todayHighlight: true,
                    //format: 'yyyy-mm-dd'
                    format: 'dd-mm-yyyy'
                });
            var op = $stateParams.sel;
            if (op != undefined) {
                try {
                    $scope.edt = {};
                    $scope.edt.sims_cur_code = op.cur;
                    $scope.edt.sims_academic_year = op.ac;
                    $scope.edt.sims_grade_code = op.gr;
                    $scope.edt.sims_section_code = op.sc;
                    $scope.edt.search_txt = op.st;
                    //_onFetchData();
                } catch (e) {

                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
            };


            $http.get(ENV.apiUrl + "api/StudentFee/getDecimalPlaces").then(function (getDecimal) {
                debugger;
                $scope.decimal_degit = getDecimal.data;
                $scope.decimal_dsc = $scope.decimal_degit[0].comp_curcy_dec;
            });


            $scope.search = function () {
                debugger;
                $scope.todos = $scope.searched($scope.feesData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.feesData;
                }
                $scope.makeTodos();

                var totalAmt = 0;
                var totaPaid = 0;
                var totbal = 0;
                debugger;
                for (var i = 0; i < $scope.todos.length; i++) {
                    totalAmt = totalAmt + parseFloat($scope.todos[i].std_fee_amount);
                    totaPaid = totaPaid + parseFloat($scope.todos[i].std_TotalPaid);
                    totbal = totbal + parseFloat($scope.todos[i].std_BalanceFee);
                }
                $scope.toamt = totalAmt;
                $scope.topaid = totaPaid;
                $scope.tobal = totbal;

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.std_fee_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.std_fee_student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    || item.std_fee_Class.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.std_fee_parent_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    || item.parent_mobile_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.std_fee_mother_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    ) ? true : false;
            }

            $scope.gotoCancelReceipt = function () {
                $state.go('main.Sm043C');
            }
            var sarr = ['adis', 'sbis'];//, 'pearl'
            var smfb = ['smfb', 'smfc'];

            var AJB = ['aji', 'ajb', 'ajn', 'egn', 'zps'];
            $scope.callDet = function (info) {
                
                if ($scope.sims_academic_year_status_code == 'O') {

                    swal({ text: "Academic Year is Closed", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                }
                else {

                    if (sarr.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043SBIS', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.std_fee_academic_year, 'StudEnroll': info.std_fee_enroll_number } });
                    }
                    else if (AJB.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043AJB', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.std_fee_academic_year, 'StudEnroll': info.std_fee_enroll_number } });
                    }
                    else if (smfb.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043SBISMFB', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.std_fee_academic_year, 'StudEnroll': info.std_fee_enroll_number } });
                    }
                    else {
                        $state.go('main.Sim043SearchNew_ABQIS', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.std_fee_academic_year, 'StudEnroll': info.std_fee_enroll_number } });
                    }
                }
            }

            //params: { 'StudCurr': '', 'StudAcademic': '', 'StudEnroll': '' }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '', sims_search_code: '' };
            /*FILTER*/
            $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllCurName").then(function (CurData) {
            debugger
                $scope.CurData = CurData.data;
                $scope.edt['sims_cur_code'] = CurData.data[0].sims_cur_code;
                $scope.cur_code_change(CurData.data[0].sims_cur_code);

            });
            $http.get(ENV.apiUrl + "api/Fee/SFS/getSeachOptions").then(function (searchoptions) {
                $scope.searchoptions = searchoptions.data;

                for (var i = 0; i < $scope.searchoptions.length; i++) {
                    if ($scope.searchoptions[i].sims_search_code_status == 'Y') {
                        $scope.edt['sims_search_code'] = $scope.searchoptions[i].sims_search_code;
                    }
                }
            });



            $scope.currentYear_status = 'C';
            $scope.cur_code_change = function () {
                
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + $scope.edt.sims_cur_code).then(function (AcademicYears) {
                    $scope.AcademicYears = AcademicYears.data;
                    for (var i = 0; i < $scope.AcademicYears.length; i++) {
                        if ($scope.AcademicYears[i].sims_academic_year_status == 'C' || $scope.AcademicYears[i].sims_academic_year_status=="Current") {
                            //   $scope.edt['sims_cur_code'] = cur_values;
                            $scope.edt['sims_academic_year'] = $scope.AcademicYears[i].sims_academic_year;
                        }
                    }
                    $scope.academic_year_change();
                });

            }
            $scope.academic_year_change = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year).then(function (Grades) {
                    $scope.Grades = Grades.data;
                });

            }
            $scope.year_change = function () {
                debugger;

                for (var i = 0; i < $scope.AcademicYears.length; i++) {
                    if ($scope.edt.sims_academic_year == $scope.AcademicYears[i].sims_academic_year)
                        $scope.sims_academic_year_status_code = $scope.AcademicYears[i].sims_academic_year_status;

                }


            }


            $scope.grade_change = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllSections?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year + "&g_code=" + $scope.edt.sims_grade_code).then(function (sections) {
                    $scope.sections = sections.data;
                    
                });

            }

            $scope.reset = function () {
               
                //$scope.edt = { 'sims_grade_code': '', 'sims_section_code': '', 'search_txt': '' };
                $scope.edt['sims_grade_code'] = '';
                $scope.edt['sims_section_code'] = '';
                $scope.edt['search_txt'] = '';
                $scope.feesData = '';
                $scope.todos = $scope.feesData;
                $scope.makeTodos();
              

            }

            /* PAGER FUNCTIONS*/

            //$scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 9, $scope.maxSize = 7, $scope.currentPage_ind = 0;
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 50, $scope.maxSize = 30; $scope.currentPage_ind = 0;
            $scope.allSize = false;

            $scope.makeTodos = function () {
                $scope.currentPage_ind = 0;

                if ((Math.round($scope.totalItems / $scope.numPerPage) * $scope.numPerPage) > $scope.totalItems)
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage);
                else
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage) + 1;

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            //$scope.size = function (str) {
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.currentPage_ind = str;
            //    $scope.numPerPage = str;  $scope.makeTodos();
            //}

            $scope.size = function (str) {

                if (str == 'All') {

                    $scope.totalItems = $scope.feesData.length;
                    $scope.todos = $scope.feesData;
                    $scope.filteredTodos = $scope.GetAllStudentFee;
                    $scope.numPerPage = $scope.feesData.length;
                    $scope.maxSize = $scope.feesData.length
                    $scope.makeTodos();
                    $scope.allSize = true;
                }
                else {
                    $scope.allSize = false;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }
            
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage_ind = str;
                $scope.currentPage = str;
                

                $scope.makeTodos();
            }

            /* PAGER FUNCTIONS*/

            function _onFetchData() {
                debugger;
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllTotalStudent?acyear=" + $scope.edt.sims_academic_year).then(function (res) {
                    $scope.total_count = res.data;

                    $scope.count_total = $scope.total_count[0].student_count;

                });

                if ($scope.edt.search_txt == undefined)
                    $scope.edt.search_txt = '';
                $scope.sel = { 'cur': $scope.edt.sims_cur_code, 'ac': $scope.edt.sims_academic_year, 'gr': $scope.edt.sims_grade_code, 'sc': $scope.edt.sims_section_code, 'st': $scope.edt.search_txt, 'search_flag': '3' }
                $http.get(ENV.apiUrl + "api/Fee/SFS/getStudent_Fees?minval=0&maxval=0&cc=" + $scope.edt.sims_cur_code + "&ay=" + $scope.edt.sims_academic_year + "&gc=" + $scope.edt.sims_grade_code + "&sc=" + $scope.edt.sims_section_code + "&search_stud=" + $scope.edt.search_txt + "&search_flag=" + $scope.edt.sims_search_code).then(function (feesData) {
                    debugger;
                    $scope.feesData = feesData.data;
                    
                    var totalAmt = 0;
                    var totaPaid = 0;
                    var totbal = 0;
                    debugger;
                    for (var i = 0; i < $scope.feesData.length; i++) {
                        totalAmt = totalAmt + parseFloat($scope.feesData[i].std_fee_amount);
                        totaPaid = totaPaid + parseFloat($scope.feesData[i].std_TotalPaid);
                        totbal = totbal + parseFloat($scope.feesData[i].std_BalanceFee);
                    }
                    $scope.toamt = totalAmt;
                    $scope.topaid = totaPaid;
                    $scope.tobal = totbal;
                     
                    if ($scope.feesData.length <= 0) {
                        swal(
                            {
                                showCloseButton: true,
                                text: 'No Data found.',
                                width: 350,
                                imageUrl: "assets/img/close.png",
                                showCloseButon: true
                            });
                    }
                    else {
                        if ($scope.feesData.length == 1) {
                            //$scope.callDet($scope.feesData[0]);
                        }
                        $scope.totalItems = $scope.feesData.length;
                        $scope.todos = $scope.feesData;
                        $scope.makeTodos();
                    }
                    $scope.length_array = $scope.feesData.length;
                });
            }
            $scope.fetchDatakeyPress = function ($event) {
                if ($event.keyCode == 13) {
                    _onFetchData();

                }
                else if ($event.keyCode == 5 && $event.ctrlKey && $event.shiftKey) {
                    $http.get(ENV.apiUrl + "api/StudentFee/GetReceipt").then(function (res) {
                        var rname = res.data;
                        var data = {
                            location: rname,
                            parameter: { fee_rec_no: '1120' },
                            state: 'main.Sim043'
                        }
                        window.localStorage["ReportDetails"] = JSON.stringify(data)
                        $state.go('main.ReportCardParameter')
                    });
                }
            }
            $scope.btnPreview_click = function () {
                _onFetchData();
            }
            //Events End

            /* Previous Details */

            $scope.student_name = '';

            $scope.onFetchPFRData = function (info) {
                debugger
                $scope.student_name = info.std_fee_enroll_number + '-' + info.std_fee_student_name;
                $scope.summery = { dd_fee_amount_final: 0, dd_fee_amount_discounted: 0, doc_status_code: 3 };

                $http.get(ENV.apiUrl + "api/Fee/SFS/GetPreviousReceipts_NEW?ay=" + info.std_fee_academic_year + "&enroll=" + info.std_fee_enroll_number).then(function (feesReceiptData) {
                    $scope.feesReceiptData = feesReceiptData.data[0];

                    $scope.small_table = true;

                    $scope.expected_amt = 0;
                    $scope.Fee_amt = 0;

                    for (var i = 0; i < $scope.feesReceiptData.length; i++) {

                        $scope.expected_amt = parseFloat($scope.expected_amt) + parseFloat($scope.feesReceiptData[i].dd_fee_amount_final);
                        $scope.Fee_amt = parseFloat($scope.Fee_amt) + parseFloat($scope.feesReceiptData[i].dd_fee_amount_final);
                    }



                    //  $scope.feesDisReceiptData = feesReceiptData.data[1];

                    //angular.forEach($scope.feesDisReceiptData, function (value, key) {
                    //    value['sims_icon'] = 'fa fa-minus-circle';
                    //});




                    //$scope.doc_nos = [];

                    //angular.forEach($scope.feesReceiptData, function (value, key) {
                    //    var flag = false;

                    //    ////angular.forEach($scope.doc_nos, function (value1, key1) {
                    //    ////    if (value.doc_no == value1.doc_no)
                    //    ////        flag = true;
                    //    ////});


                    //    $scope.summery.dd_fee_amount_final = parseFloat($scope.summery.dd_fee_amount_final) + parseFloat(value.dd_fee_amount_final);
                    //    $scope.summery.dd_fee_amount_discounted = parseFloat($scope.summery.dd_fee_amount_discounted) + parseFloat(value.dd_fee_amount_discounted);
                    //});

                    // $scope.feesReceiptData.push($scope.summery);
                    $('#MyModal').modal({ backdrop: 'static', keyboard: false });

                });

                $scope.edt.sims_cur_code
                $scope.edt.sims_academic_year
                $scope.edt.sims_grade_code
                $scope.edt.sims_section_code

                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllTotalStudent?ay=" + info.std_fee_academic_year + "&enroll=" + info.std_fee_enroll_number).then(function (feesReceiptData) {
                });


            }


            $scope.expand = function (j, $event) {
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";
                    }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none";
                }
            }

            $scope.edtadv = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '', sims_search_code: '' };

            $scope.cur_code_change_adv = function () {
                
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + $scope.edtadv.sims_cur_code).then(function (AcademicYears) {
                    $scope.AcademicYears = AcademicYears.data;
                });

            }
            $scope.academic_year_change_adv = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllGrades?cur_code=" + $scope.edtadv.sims_cur_code + "&ac_year=" + $scope.edtadv.sims_academic_year).then(function (Grades) {
                    $scope.AdvGrades = Grades.data;
                });

            }
            $scope.grade_change_adv = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllSections?cur_code=" + $scope.edtadv.sims_cur_code + "&ac_year=" + $scope.edtadv.sims_academic_year + "&g_code=" + $scope.edtadv.sims_grade_code).then(function (sections) {
                    $scope.Advsections = sections.data;
                    
                });

            }


            $scope.grade_change_adv_sibling=function(cur,year,grade,section) {
                debugger;
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllSections?cur_code=" + cur + "&ac_year=" + year + "&g_code=" + grade).then(function (sections) {
                    debugger;
                    $scope.siblingsAdvsections = sections.data;
                    for (var i = 0; i < $scope.siblingsAdvsections.length; i++) {
                        debugger;
                        if (section == $scope.siblingsAdvsections[i].is_adv_section) {
                            $scope.sibling.sims_section_code = $scope.siblingsAdvsections[i].is_adv_section;
                        }
                    }
                });
            }


            $scope.grade_change_adv_sibling_new = function (cur, year, grade, obj) {
                debugger;
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllSections?cur_code=" + cur + "&ac_year=" + year + "&g_code=" + grade).then(function (sections) {
                    debugger;
                    $scope.siblingsAdvsections = sections.data;
                    obj['section_lst'] = sections.data;
                });
            }

            $scope.divAdvanceYearFee = function (info) {
                var adv_academic_year_status = ''
               
                for (var i = 0; i < $scope.AcademicYears.length; i++) {
                    if (info.adv_fee_year == $scope.AcademicYears[i].sims_academic_year)
                        adv_academic_year_status = $scope.AcademicYears[i].sims_academic_year_status;

                }
                if (adv_academic_year_status != 'O') {

                $scope.student_name = info.std_fee_enroll_number + '-' + info.std_fee_student_name;
                if (info.is_adv_section_defined) {
                    if (sarr.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043SBIS', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.adv_fee_year, 'StudEnroll': info.std_fee_enroll_number } });
                    }
                    else if (AJB.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043AJB', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.adv_fee_year, 'StudEnroll': info.std_fee_enroll_number } });
                    }
                    else if (smfb.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043SBISMFB', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.std_fee_academic_year, 'StudEnroll': info.std_fee_enroll_number } });
                    }
                    else {
                        $state.go('main.Sim043SearchNew_ABQIS', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.adv_fee_year, 'StudEnroll': info.std_fee_enroll_number } });
                    }
                }
                else {
                    $scope.edtadv['sims_cur_code'] = info.std_fee_cur_code;
                    $scope.cur_code_change_adv();
                    $scope.edtadv['sims_academic_year'] = info.adv_fee_year;
                    $scope.academic_year_change_adv();
                    $scope.edtadv['sims_grade_code'] = info.is_adv_grade;
                   // $scope.grade_change_adv();
                    $scope.edtadv['sims_section_code'] = info.is_adv_section;
                    $scope.edtadv['std_fee_enroll_number'] = info.std_fee_enroll_number;
                    //$('#divAdvanceYearFee').modal({ backdrop: 'static', keyboard: true });
                    $('#AdvanceYearCollection').modal({ backdrop: 'static', keyboard: true });
                    debugger;
                    //$scope.grade_change_adv_sibling();
                    $scope.parent_name = info.std_fee_parent_id + '-' + info.std_fee_parent_name;
                          
                    $http.get(ENV.apiUrl + "api/Fee/SFS/GetSiblingDetails?parent_id=" + info.std_fee_parent_id + "&academic_year=" + info.std_fee_academic_year).then(function (siblings) {
                        $scope.siblingsDetails = siblings.data;
                        for (var j = 0; j < $scope.siblingsDetails.length; j++) {
                          //  $scope.grade_change_adv_sibling($scope.siblingsDetails[j].sims_cur_code, $scope.siblingsDetails[j].adv_fee_year, $scope.siblingsDetails[j].is_adv_grade, $scope.siblingsDetails[j].is_adv_section);
                            // $scope.sibling_sims_section_code = $scope.siblingsDetails[j].sims_section_code;

                            $scope.grade_change_adv_sibling_new($scope.siblingsDetails[j].sims_cur_code, $scope.siblingsDetails[j].adv_fee_year, $scope.siblingsDetails[j].is_adv_grade, $scope.siblingsDetails[j]);

                        }
                    });
                    
                }
                }
                else {
                    swal({ text: "Academic Year is Closed", imageUrl: "assets/img/check.png", width: 300, height: 200 });

                }

            }
            $scope.siblingsData = [];
            $scope.CheckAllChecked = function () {
                debugger;
                var main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.siblingsDetails.length; i++) {
                    if (main.checked == true) {
                        $scope.siblingsDetails[i].stud_check = true;
                       
                        $scope.siblingsDetails[i].row_color = '#ffffcc'

                       
                    }
                    else {
                        $scope.siblingsDetails[i].stud_check = false;
                       
                        $scope.siblingsDetails[i].row_color = ''
                        
                    }
                }

            }


            $scope.selectonebyone = function (sibling,index) {
                debugger;
                
                if (sibling.is_adv_grade == undefined || sibling.is_adv_grade == "") {
                    swal({ text: "Please Select Grade", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    sibling.stud_check = false;
                    return;
                }
                if (sibling.is_adv_section == undefined || sibling.is_adv_section == "") {
                    swal({ text: "Please Select Section", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    sibling.stud_check = false;
                    return;
                }
               
                $scope.submitflag = false;

                for (var k = 0; k < $scope.siblingsDetails.length; k++) {
                    
                    if ($scope.siblingsDetails[k].stud_check == true) {
                        $scope.submitflag = true;
                    }
                }
              
            }

            
            $scope.submitAdvSiblingsFees = function () {
                $scope.siblingsData = [];
                for (var k = 0; k < $scope.siblingsDetails.length; k++) {
                    if ($scope.siblingsDetails[k].stud_check == true) {
                        $scope.siblingsData.push($scope.siblingsDetails[k]);
                    }
                }

                $('#AdvanceYearCollection').modal('hide');

                $http.post(ENV.apiUrl + "api/Fee/SFS/submitAdvSiblingsFees", $scope.siblingsData).then(function (result) {
                    if (sarr.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043SBIS', { 'sel': $scope.sel, 'IP': { 'StudCurr': $scope.edtadv.sims_cur_code, 'StudAcademic': $scope.edtadv.sims_academic_year, 'StudEnroll': $scope.edtadv.std_fee_enroll_number } });
                    }
                    else if (AJB.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043AJB', { 'sel': $scope.sel, 'IP': { 'StudCurr': $scope.edtadv.sims_cur_code, 'StudAcademic': $scope.edtadv.sims_academic_year, 'StudEnroll': $scope.edtadv.std_fee_enroll_number } });
                    }
                    else if (smfb.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043SBISMFB', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.std_fee_academic_year, 'StudEnroll': info.std_fee_enroll_number } });
                    }
                    else {
                        $state.go('main.Sim043SearchNew_ABQIS', { 'sel': $scope.sel, 'IP': { 'StudCurr': $scope.edtadv.sims_cur_code, 'StudAcademic': $scope.edtadv.sims_academic_year, 'StudEnroll': $scope.edtadv.std_fee_enroll_number } });
                    }
                });
            }

            $scope.closeAdvCollect = function () {
                $('#AdvanceYearCollection').modal('hide');
            }

            $scope.closeAdv = function () {
                $('#divAdvanceYearFee').modal('hide');
            }
            $scope.submitAdvFees = function () {
                //string cur_code,string ay,string grade,string section,string enroll
                $('#divAdvanceYearFee').modal('hide');

                $http.post(ENV.apiUrl + "api/Fee/SFS/DefineAdvFee?cur_code=" + $scope.edtadv.sims_cur_code + "&ay=" + $scope.edtadv.sims_academic_year + "&grade=" + $scope.edtadv.sims_grade_code + "&section=" + $scope.edtadv.sims_section_code + "&enroll=" + $scope.edtadv.std_fee_enroll_number).then(function (result) {
                    if (sarr.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043SBIS', { 'sel': $scope.sel, 'IP': { 'StudCurr': $scope.edtadv.sims_cur_code, 'StudAcademic': $scope.edtadv.sims_academic_year, 'StudEnroll': $scope.edtadv.std_fee_enroll_number } });
                    }
                    else if (AJB.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043AJB', { 'sel': $scope.sel, 'IP': { 'StudCurr': $scope.edtadv.sims_cur_code, 'StudAcademic': $scope.edtadv.sims_academic_year, 'StudEnroll': $scope.edtadv.std_fee_enroll_number } });
                    }
                    else if (smfb.includes($http.defaults.headers.common['schoolId'])) {
                        $state.go('main.Sim043SBISMFB', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.std_fee_academic_year, 'StudEnroll': info.std_fee_enroll_number } });
                    }
                    else {
                        $state.go('main.Sim043SearchNew_ABQIS', { 'sel': $scope.sel, 'IP': { 'StudCurr': $scope.edtadv.sims_cur_code, 'StudAcademic': $scope.edtadv.sims_academic_year, 'StudEnroll': $scope.edtadv.std_fee_enroll_number } });
                    }
                });
            }

            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=family").then(function (res) {
                $scope.receipt_url = res.data;
            });


            $scope.report_show = true;
            $scope.Report = function (str) {
                debugger;
                $scope.student_name = str.std_fee_parent_id + '-' + str.std_fee_parent_name;
                $('#feemodal').modal('show');
                $scope.report_show = false;

                // $http.get(ENV.apiUrl + "api/StudentFee/GetReceipt").then(function (res) {
                // $scope.reportparameter = res.data;
                var data = {
                    location: $scope.receipt_url,
                    parameter: {
                        cur_code: str.std_fee_cur_code,
                        acad_year: str.std_fee_academic_year,
                        parent_flag:2,
                        user_no: str.std_fee_parent_id,
                        from_date: moment(new Date()).format('YYYY-MM-DD'),
                        other_fee_flag: true,
                        cumfeeflag: false,

                    },
                    state: 'main.Sim43S',
                    ready: function () {
                        this.refreshReport();
                    },
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)
                //$state.go('main.ReportCardParameter')
                try {
                    $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                    $scope.location = $scope.rpt.location;
                    $scope.parameter = $scope.rpt.parameter;
                    $scope.state = $scope.rpt.state;
                }
                catch (ex) {
                }
                var s;
                s = "SimsReports." + $scope.location + ",SimsReports";
                var url = window.location.href;
                var domain = url.substring(0, url.indexOf(':'))
                if ($http.defaults.headers.common['schoolId'] == 'sms') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                }
                else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                }

                else {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                }
                console.log(service_url);
                $scope.parameters = {}

                $("#reportViewer1")
                               .telerik_ReportViewer({
                                   serviceUrl: service_url,
                                   viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                   scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                   // Zoom in and out the report using the scale
                                   // 1.0 is equal to 100%, i.e. the original size of the report
                                   scale: 1.0,
                                   ready: function () {
                                       //this.refreshReport();
                                   }

                               });
                var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                reportViewer.reportSource({
                    report: s,
                    parameters: $scope.parameter,
                });

                setInterval(function () {

                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                }, 1000);

                $timeout(function () {
                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                }, 100)
                // });
            }

            $scope.changeparent = function (c) {
                debugger
                if (c == '4') {
                    $scope.edt['search_txt'] = 'P';
                }
                else {
                    $scope.edt['search_txt'] = '';

                }
            }

        }])
})();

