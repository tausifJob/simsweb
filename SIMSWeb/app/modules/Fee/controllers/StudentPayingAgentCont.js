﻿(function () {
    'use strict';
    var obj1, temp, opr, comp, Pay_agent;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudentPayingAgentCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
           // $scope.temp = {};

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            // ---this Api for get session value
          


            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;



            $http.get(ENV.apiUrl + "api/Payagent/getAgentNameType?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (agentnametype) {
                $scope.agent_name_type = agentnametype.data;
                });




            $http.get(ENV.apiUrl + "api/Payagent/getAllPayAgent").then(function (res1) {
                $scope.Pay_agent = res1.data;
                $scope.totalItems = $scope.Pay_agent.length;
                $scope.todos = $scope.Pay_agent;
                $scope.makeTodos();
            });

            $http.get(ENV.apiUrl + "api/Payagent/getAcademicYear" ).then(function (AccYear) {
                $scope.Acc_Year = AccYear.data;
                $scope.temp['sims_sip_academic_year'] = $scope.Acc_Year[0].sims_sip_academic_year;
            });
            $http.get(ENV.apiUrl + "api/Payagent/getConcessionType").then(function (ConcessionType) {
                $scope.Concession_Type = ConcessionType.data;
                });

            $http.get(ENV.apiUrl + "api/Payagent/getDiscountType").then(function (distype) {
                $scope.dis_type = distype.data;
                });

            $http.get(ENV.apiUrl + "api/Payagent/getConcessApplicableOn").then(function (applion) {
                $scope.appli_on = applion.data;
                setTimeout(function () {
                    $('#Cmd_appli_On').change(function () {
                        }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);

                $scope.applicableon = true;
                });
            $(function () {
                $('#Cmd_appli_On').multipleSelect({
                    width: '100%'
                });
            });


            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.Pay_agent;
                    $scope.pager = false;
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                     }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                 $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Pay_agent, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Pay_agent;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_paying_agent_description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.fee_description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_paying_agent_discount_value.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                       // item.sims_attendance_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                       // item.sims_transport_amount ||
                        item.sims_paying_agent_number == toSearch) ? true : false;

            }
            
            //$scope.showdate = function (date, name1) {

            //    //var day = date.split("-")[0];
            //    //var month = date.split("-")[1];
            //    //var year = date.split("-")[2];

            //    //$scope.temp[name1] = day + "-" + month + "-" + year;
            //    $scope.temp[name1] = date;
            //}

            $scope.New = function () {

                $scope.ComboApplicableOn = true;
                $scope.txtApplicableOn = false;
                // $scope.select_type = true;
                $scope.select_types = 'Discount';
                $scope.payagereadonly1 = true;
                $scope.ledger_txt_box = false;
                $scope.ledger_combo_box = true;

                var dt = new Date();
                $scope.temp.sims_paying_created_date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                $http.get(ENV.apiUrl + "api/Payagent/getPayAgentNumber").then(function (payagent_no) {
                    $scope.pay_agent_no = payagent_no.data;
                    
                    $scope.temp = {
                        sims_paying_agent_number: $scope.pay_agent_no[0].sims_paying_agent_number,
                        sims_paying_agent_corporate_billing: true,
                        sims_paying_created_date: dd + '-' + mm + '-' + yyyy
                    };
                    $scope.temp['sims_sip_academic_year'] = $scope.Acc_Year[0].sims_sip_academic_year;
                });
                $scope.applicableon = false;
                $scope.disabled = false;
                $scope.payagereadonly = true;
                $scope.accyreadonly = false;
                $scope.gtcreadonly = true;
                $scope.gtdreadonly = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.ConcessDis = false;

                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

           

            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger
                if (Myform) {
                    // var data = $scope.temp;
                    //data.opr = "I";

                    var terminal = document.getElementById("selectPay");
                    var selectedText = terminal.options[terminal.selectedIndex].text;

                    for (var i = 0; i < $scope.temp.sims_fee_code.length; i++) {
                        var sims_fee_code_s = $scope.temp.sims_fee_code[i];
                        debugger

                        var data = {
                            sims_paying_agent_number: $scope.temp.sims_paying_agent_number
                                      , sims_paying_agent_description: selectedText
                                      , sims_sip_academic_year: $scope.temp.sims_sip_academic_year
                                      , sims_concession_type: $scope.temp.sims_concession_type
                                      , sims_discount_type: $scope.temp.sims_discount_type
                                      , sims_paying_agent_discount_value: $scope.temp.sims_paying_agent_discount_value
                                      , sims_fee_code: sims_fee_code_s
                                      , sims_ledger_ac_no: $scope.temp.sims_ledger_ac_no
                                      , sims_ledger_code: $scope.temp.sims_ledger_code
                                      , sims_paying_created_date: $scope.temp.sims_paying_created_date
                                      , sims_paying_agent_corporate_billing: $scope.temp.sims_paying_agent_corporate_billing
                                       ,sims_paying_agent_receivable_acno_flag:$scope.temp.sims_paying_agent_receivable_acno_flag
                                      , opr: 'I'
                        };
                        datasend.push(data);

                    }
                    $http.post(ENV.apiUrl + "api/Payagent/CUDStudentPayingAgent", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        $http.get(ENV.apiUrl + "api/Payagent/getAllPayAgent").then(function (res1) {
                            $scope.Pay_agent = res1.data;
                            $scope.totalItems = $scope.Pay_agent.length;
                            $scope.todos = $scope.Pay_agent;
                            $scope.makeTodos();

                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({  text: "Record Inserted Successfully", width: 300, height: 200, imageUrl: "assets/img/check.png" });
                                $("#Cmd_appli_On").select2("val", "");
                            }
                            else {
                                swal({  text: "Record Not Inserted. " + $scope.msg1, width: 300, height: 200, imageUrl: "assets/img/close.png" });
                                $("#Cmd_appli_On").select2("val", "");
                            }
                        });
                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;


                }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                //$scope.temp.sims_paying_agent_number = "";
                $scope.temp.sims_sip_academic_year = "";
                $scope.temp.sims_concession_type = "";
                $scope.temp.sims_discount_type = "";
                $scope.temp.sims_paying_agent_discount_value = "";
                $scope.temp.sims_fee_code = "";
                $scope.temp.sims_ledger_ac_no = "";
                $scope.temp.sims_ledger_code = "";
                $scope.temp.sims_paying_created_date = "";
                $scope.temp.sims_paying_agent_corporate_billing = "";
            }

            $scope.edit = function (str) {
                
                $scope.ledger_txt_box = true;
                $scope.ledger_combo_box = false;
                $scope.gtcreadonly = true;
                $scope.gtdreadonly = true;
                $scope.gdisabled = true;
                $scope.aydisabled = true;
                $scope.select_type = false;
                $scope.table = false;
                $scope.accyreadonly = true;
                $scope.payagereadonly = true;
                $scope.payagereadonly1 = true;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.ConcessDis = true;

                $scope.ComboApplicableOn = false;
                $scope.txtApplicableOn = true;
                // $scope.temp = str;

                
                $scope.temp = {
                    sims_paying_agent_number: str.sims_paying_agent_number
                 , sims_paying_agent_description: str.sims_paying_agent_description
                 , sims_sip_academic_year: str.sims_paying_agent_academic_year
                 , sims_discount_type: str.sims_paying_agent_discount_type
                 , sims_paying_agent_discount_value: str.sims_paying_agent_discount_value
                 , sims_fee_code: str.sims_paying_agent_fee_code
                 , sims_ledger_ac_no: str.sims_paying_agent_slma_ldgr
                 , sims_ledger_code: str.sims_paying_ldgrctl_code
                 , sims_concession_type: str.discount_description
                 , sims_paying_created_date: str.sims_paying_created_date
                 , sims_paying_agent_corporate_billing: str.sims_paying_agent_corporate_billing
                 , fee_description: str.fee_description
                 ,sims_paying_agent_receivable_acno_flag:str.sims_paying_agent_receivable_acno_flag
                 ,ledgercodeampunt:str.discount_description + "/" + str.sims_paying_ldgrctl_code
                };
              //  $scope.ledgercodeampunt = 

                $scope.select_types = 'Discount';
                var add = [];
                add.push(str.sims_paying_agent_fee_code)

              
                

                try {
                    $("#Cmd_appli_On").multipleSelect("setSelects", add);
                }
                catch (e) {

                }
                $scope.AppOnvalue = $scope.temp.fee_description;
            }

            var dataupdate = [];
            $scope.update = function () {


                //for (var i = 0; i < $scope.temp.sims_fee_code.length; i++) {
                //    var sims_fee_code_s = $scope.temp.sims_fee_code[0];
                debugger

                var data = {
                    sims_paying_agent_number: $scope.temp.sims_paying_agent_number
                              , sims_paying_agent_description: $scope.temp.sims_paying_agent_description
                              , sims_sip_academic_year: $scope.temp.sims_sip_academic_year
                              , sims_concession_type: $scope.temp.sims_concession_type
                              , sims_discount_type: $scope.temp.sims_discount_type
                              , sims_paying_agent_discount_value: $scope.temp.sims_paying_agent_discount_value
                              , sims_fee_code: $scope.temp.sims_fee_code
                              , sims_ledger_ac_no: $scope.temp.sims_ledger_ac_no
                              , sims_ledger_code: $scope.temp.sims_ledger_code
                              , sims_paying_created_date: $scope.temp.sims_paying_created_date
                              , sims_paying_agent_receivable_acno_flag: $scope.temp.sims_paying_agent_receivable_acno_flag
                              , sims_paying_agent_corporate_billing: $scope.temp.sims_paying_agent_corporate_billing
                              , opr: 'U'
                };
                dataupdate.push(data);
                //  }

                //var data = $scope.temp;
                // data.opr = 'U';
                //dataupdate.push(data);

                $http.post(ENV.apiUrl + "api/Payagent/CUDStudentPayingAgent", dataupdate).then(function (msg) {
                    $scope.msg1 = msg.data;

                    $http.get(ENV.apiUrl + "api/Payagent/getAllPayAgent").then(function (res1) {
                        $scope.Pay_agent = res1.data;
                        $scope.totalItems = $scope.Pay_agent.length;
                        $scope.todos = $scope.Pay_agent;
                        $scope.makeTodos();
                        if ($scope.msg1 == true) {
                            swal({  text: "Record Updated Successfully", width: 300, height: 200, imageUrl: "assets/img/check.png" });
                        }
                        else if ($scope.msg1 == false) {
                            swal({  text: "Record Not Updated. " , width: 300, height: 200,imageUrl: "assets/img/close.png" });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                });
                dataupdate = [];
                $scope.table = true;
                $scope.display = false;


            }

            $scope.CheckAllChecked = function () {
                
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("pay-"+i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("pay-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                debugger
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById("pay-" + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({

                            sims_paying_agent_number: $scope.filteredTodos[i].sims_paying_agent_number
                        , sims_paying_agent_description: $scope.filteredTodos[i].sims_paying_agent_description
                        , sims_sip_academic_year: $scope.filteredTodos[i].sims_paying_agent_academic_year
                        , sims_discount_type: $scope.filteredTodos[i].sims_paying_agent_discount_type
                        , sims_paying_agent_discount_value: $scope.filteredTodos[i].sims_paying_agent_discount_value
                        , sims_fee_code: $scope.filteredTodos[i].sims_paying_agent_fee_code
                        , sims_ledger_ac_no: $scope.filteredTodos[i].sims_paying_agent_slma_ldgr
                        , sims_ledger_code: $scope.filteredTodos[i].sims_paying_ldgrctl_code
                        , sims_paying_created_date: $scope.filteredTodos[i].sims_paying_created_date
                        , sims_paying_agent_corporate_billing: $scope.filteredTodos[i].sims_paying_agent_corporate_billing
                        , sims_paying_agent_receivable_acno_flag: $scope.temp.sims_paying_agent_receivable_acno_flag
                        , opr: 'D'

                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/Payagent/CUDStudentPayingAgent", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({  text: "Record Deleted Successfully", imageUrl: "assets/img/check.png",showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Payagent/getAllPayAgent").then(function (res1) {
                                                $scope.Pay_agent = res1.data;
                                                $scope.totalItems = $scope.Pay_agent.length;
                                                $scope.todos = $scope.Pay_agent;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({  text: "Record is Mapped Can`t Deleted. ",imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Payagent/getAllPayAgent").then(function (res1) {
                                                $scope.Pay_agent = res1.data;
                                                $scope.totalItems = $scope.Pay_agent.length;
                                                $scope.todos = $scope.Pay_agent;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById("pay-" + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({  text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png",showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }


            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.temp = {
                sims_paying_created_date: dd + '-' + mm + '-' + yyyy
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });


        }])

})();
