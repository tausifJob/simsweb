﻿(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [];
    var doc_no;
    var simsController = angular.module('sims.module.Fee');
    simsController.controller('FeeReceiptNewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            //$scope.pagesize = '10';
            //$scope.pager = true;
            //$scope.pagggg = false;
            //var user = $rootScope.globals.currentUser.username;
            //$scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
           // var doc_no;

            $(document).ready(function () {
                $scope.data = [];
                var duetotal = 0;
                var paidtotal = 0;
                var bal = 0;
                $http.post(ENV.apiUrl + "api/FeeReceipt/FetchAllReceipt?doc_no=30").then(function (res) {

                    $scope.data = res.data;
                    var d = new Date(res.data[0].doc_date);
                    $scope.date = d.getDate() + "-" + d.getMonth() + "-" + d.getFullYear();

                    $($scope.data).each(function (k, v) {
                        var finalbal = v.dd_fee_amount - v.dd_fee_amount_final;
                        duetotal = duetotal + v.dd_fee_amount;
                        paidtotal = paidtotal + v.dd_fee_amount_final;
                        bal = bal + finalbal;
                        v['serial_no'] = k + 1;
                    });

                    $scope.duetotal = duetotal;
                    $scope.paidtotal = paidtotal;
                    $scope.remainingbal = bal;

                    console.log("Due total = " + duetotal + " paid total = " + paidtotal + " remaining total = " + bal);
                    console.log("Res Data", $scope.data);

                    setTimeout(function () {

                        var printContents = document.getElementById("page1").innerHTML;

                        var originalContents = document.body.innerHTML;

                        document.body.innerHTML = printContents;

                        window.print();

                        document.body.innerHTML = originalContents;

                    }, 300)
                    
                       // $scope.printDiv();
                   
                });


                $scope.data = [];
                $http.post(ENV.apiUrl + "api/FeeReceipt/FetchMograsysLicenceDetails?opr=S").then(function (res) {

                    $scope.school_data = res.data[0];
                    // var d = new Date(res.data[0].lic_school_code);

                    console.log("Res School Data", $scope.data);
                });
            })


        }]
        )
})();