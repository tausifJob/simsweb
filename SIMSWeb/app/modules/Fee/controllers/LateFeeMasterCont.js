﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Fee');
    simsController.controller('LateFeeMasterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.academic_year = false;
            $scope.cur_culum = false;



            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //Select Data SHOW

            //CUR
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
               
                $scope.temp = {}
                $scope.temp['sims_cur_code'] = AllCurr.data[0].sims_cur_code

                $scope.getAccYear(AllCurr.data[0].sims_cur_code)
            });


            //Academic Year
            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                   
                    $scope.temp['sims_academic_year'] = $scope.Acc_year[0].sims_academic_year
                });

            }

            $http.get(ENV.apiUrl + "api/PayagentMappingController/getLateFeeFrequency").then(function (Allfrequency) {
                $scope.Frequency = Allfrequency.data;
            });

            $http.get(ENV.apiUrl + "api/PayagentMappingController/getLateFeeDetails").then(function (AllData) {
                $scope.LateFeeDataFrequency = AllData.data;
                $scope.totalItems = $scope.LateFeeDataFrequency.length;
                $scope.todos = $scope.LateFeeDataFrequency;
                $scope.makeTodos();
            });



            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.LateFeeDataFrequency, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.LateFeeDataFrequency;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_fee_frequency_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_late_fee_applicable_after_days.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
                $scope.disabled = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.divcode_readonly = false;
                $scope.temp = "";
                $scope.temp = {};
                $scope.temp['sims_library_catalogue_status'] = true;
                $scope.temp["sims_library_catalogue_name"] = "";
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                    $scope.curriculum = AllCurr.data;

                    $scope.temp = {}
                    $scope.temp['sims_cur_code'] = AllCurr.data[0].sims_cur_code

                    $scope.getAccYear(AllCurr.data[0].sims_cur_code)
                });

            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {
               
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.divcode_readonly = true;
                $scope.academic_year = true;
                $scope.cur_culum = true;
                //   $scope.temp = str;

                $scope.temp = {
                    sims_academic_year: str.sims_academic_year,
                    sims_cur_code: str.sims_cur_code,
                    sims_appl_parameter: str.sims_fee_frequency,
                    sims_late_fee_applicable_after_days: str.sims_late_fee_applicable_after_days,
                    sims_late_fee_end_date_flag: str.sims_late_fee_end_date_flag,
                    sims_late_fee_status: str.sims_late_fee_status,
                    sims_late_fee_master_sr_no: str.sims_late_fee_master_sr_no
                };
            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
               
                if (Myform) {

                    var data = {
                        'sims_cur_code': $scope.temp.sims_cur_code,
                        'sims_academic_year': $scope.temp.sims_academic_year,
                        'sims_appl_parameter': $scope.temp.sims_appl_parameter,
                        'sims_late_fee_applicable_after_days': $scope.temp.sims_late_fee_applicable_after_days,
                        'sims_late_fee_end_date_flag': $scope.temp.sims_late_fee_end_date_flag,
                        'sims_late_fee_status': $scope.temp.sims_late_fee_status,
                        'opr': 'I'
                    }
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/PayagentMappingController/CUDLateFeeSchedule", datasend).then(function (msg) {
                        datasend = [];
                        $scope.msg1 = msg.data;
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({  text: "Record Inserted Successfully",imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        }
                        else {
                            swal({  text: "Record already present." + $scope.msg1, imageUrl: "assets/img/close.png",showCloseButton: true, width: 300, height: 200 });
                        }

                        $http.get(ENV.apiUrl + "api/PayagentMappingController/getLateFeeDetails").then(function (AllData) {
                            $scope.LateFeeDataFrequency = AllData.data;
                            $scope.totalItems = $scope.LateFeeDataFrequency.length;
                            $scope.todos = $scope.LateFeeDataFrequency;
                            $scope.makeTodos();
                        });

                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
               
                if (Myform) {

                    var data = {
                        'sims_cur_code': $scope.temp.sims_cur_code,
                        'sims_academic_year': $scope.temp.sims_academic_year,
                        'sims_appl_parameter': $scope.temp.sims_appl_parameter,
                        'sims_late_fee_applicable_after_days': $scope.temp.sims_late_fee_applicable_after_days,
                        'sims_late_fee_end_date_flag': $scope.temp.sims_late_fee_end_date_flag,
                        'sims_late_fee_master_sr_no': $scope.temp.sims_late_fee_master_sr_no,
                        'sims_late_fee_status': $scope.temp.sims_late_fee_status,
                        'opr': 'U'
                    }
                    dataforUpdate.push(data);
                    //dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/PayagentMappingController/CUDLateFeeSchedule", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({  text: "Record Updated Successfully",imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        }
                        else {
                            swal({  text: "Record Not Updated. " + $scope.msg1,imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }
                        $http.get(ENV.apiUrl + "api/PayagentMappingController/getLateFeeDetails").then(function (AllData) {
                            $scope.LateFeeDataFrequency = AllData.data;
                            $scope.totalItems = $scope.LateFeeDataFrequency.length;
                            $scope.todos = $scope.LateFeeDataFrequency;
                            $scope.makeTodos();
                        });

                    });
                    dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            // Data DELETE RECORD

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }





            $scope.OkDelete = function () {
               
                deletefin = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_late_fee_master_sr_no': $scope.filteredTodos[i].sims_late_fee_master_sr_no,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/PayagentMappingController/CUDLateFeeSchedule", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({  text: "Record Deleted Successfully", imageUrl: "assets/img/check.png",showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/PayagentMappingController/getLateFeeDetails").then(function (AllData) {
                                                $scope.LateFeeDataFrequency = AllData.data;
                                                $scope.totalItems = $scope.LateFeeDataFrequency.length;
                                                $scope.todos = $scope.LateFeeDataFrequency;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal({ text: "Record Not Deleted Successfully. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/PayagentMappingController/getLateFeeDetails").then(function (AllData) {
                                                $scope.LateFeeDataFrequency = AllData.data;
                                                $scope.totalItems = $scope.LateFeeDataFrequency.length;
                                                $scope.todos = $scope.LateFeeDataFrequency;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    //  $scope.row1 = '';
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({  text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png",showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });


            $scope.propertyName = null;
            $scope.reverse = false;
            // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {
                
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.checkdays = function (info) {
                if (info.sims_appl_parameter == 'M') {
                    if (info.sims_late_fee_applicable_after_days > 31) {
                        info.sims_late_fee_applicable_after_days = '';
                    }
                } else {
                    if (info.sims_late_fee_applicable_after_days > 365) {
                        info.sims_late_fee_applicable_after_days = '';
                    }
                }
            }

        }])

})();
