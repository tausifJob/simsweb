﻿(function () {
    'use strict';
    var feecode = [], code = '';
    var main;
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    var data1 = [];
    simsController.controller('FeeTermCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.pagesize = '10';
            //$scope.operation = true;
            $scope.table1 = true;
            $scope.editmode = false;
            $scope.edt = {};
            $scope.temp = {};
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });


            //$scope.size = function (str) {
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str;  $scope.makeTodos();
            //}

            $scope.edt['sims_term_status'] = true;

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.FeeData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                $scope.pagesize = str;
                $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    //$scope.makeTodos();
                    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                }
                    $scope.numPerPage = str;
                    $scope.makeTodos();
            }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
            }

            $http.get(ENV.apiUrl + "api/FeeTerm/getFeeTerm").then(function (Fee_Data) {
                $scope.FeeData = Fee_Data.data;
                $scope.totalItems = $scope.FeeData.length;
                $scope.todos = $scope.FeeData;
                $scope.makeTodos();
                
            })

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
                $scope.temp['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr($scope.curriculum[0].sims_cur_code);
            })

            $scope.getacyr = function (str) {
                debugger;
                $scope.cur_code = str;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    debugger;
                    $scope.Academic_year = Academicyear.data;
                    $scope.temp['sims_academic_year'] = $scope.Academic_year[0].sims_academic_year;
                    $scope.Alltermcode($scope.curriculum[0].sims_cur_code, $scope.Academic_year[0].sims_academic_year);

                    if ($scope.Academic_year.length > 0) {
                        $scope.sDate = $scope.Academic_year[0].sims_academic_year_start_date;
                        $scope.eDate = $scope.Academic_year[0].sims_academic_year_end_date;

                        $scope.edt = {
                            sims_term_start_date: $scope.Academic_year[0].sims_academic_year_start_date,
                            sims_term_end_date: $scope.Academic_year[0].sims_academic_year_end_date
                        }

                        $(document).ready(function () {
                            $("#from_date_new").kendoDatePicker({

                                format: "dd-MM-yyyy",
                                min: new Date(moment($scope.sDate, 'DD-MM-YYYY').format('YYYY-MM-DD')),
                                max: new Date(moment($scope.eDate, 'DD-MM-YYYY').format('YYYY-MM-DD'))


                            });
                        });


                        $(document).ready(function () {
                            $("#to_date_new").kendoDatePicker({

                                format: "dd-MM-yyyy",
                                min: new Date(moment($scope.sDate, 'DD-MM-YYYY').format('YYYY-MM-DD')),
                                max: new Date(moment($scope.eDate, 'DD-MM-YYYY').format('YYYY-MM-DD'))

                                //format: "dd/MM/yyyy"
                                //format: "yyyy/MM/dd"
                            });
                        });
                    }

                    else {
                        $(document).ready(function () {
                            $("#from_date_new").kendoDatePicker({

                                format: "dd-MM-yyyy",


                            });
                        });


                        $(document).ready(function () {
                            $("#to_date_new").kendoDatePicker({

                                format: "dd-MM-yyyy",

                                //format: "dd/MM/yyyy"
                                //format: "yyyy/MM/dd"
                            });
                        });

                    }
                    //console.log($scope.getDates)
                });
            }

            $scope.Yeardate = function (str) {
                debugger;
                //$http.get(ENV.apiUrl + "api/FeeTerm/getAlldate?curCode=" + JSON.stringify($scope.temp)).then(function (Alldate) {
                //    $scope.Academic_year = Alldate.data;
                //})

                for (var i = 0; i < $scope.Academic_year.length; i++) {

                    if ($scope.Academic_year[i].sims_academic_year == str) {

                          $scope.sDate = $scope.Academic_year[i].sims_academic_year_start_date;
                            $scope.eDate = $scope.Academic_year[i].sims_academic_year_end_date;

                            $scope.edt = {
                                sims_term_start_date: $scope.Academic_year[i].sims_academic_year_start_date,
                                sims_term_end_date: $scope.Academic_year[i].sims_academic_year_end_date
                            }

                            $(document).ready(function () {
                                $("#from_date_new").kendoDatePicker({

                                    format: "dd-MM-yyyy",
                                    min: new Date(moment($scope.sDate, 'DD-MM-YYYY').format('YYYY-MM-DD')),
                                    max: new Date(moment($scope.eDate, 'DD-MM-YYYY').format('YYYY-MM-DD'))


                                });
                            });


                            $(document).ready(function () {
                                $("#to_date_new").kendoDatePicker({

                                    format: "dd-MM-yyyy",
                                    min: new Date(moment($scope.sDate, 'DD-MM-YYYY').format('YYYY-MM-DD')),
                                    max: new Date(moment($scope.eDate, 'DD-MM-YYYY').format('YYYY-MM-DD'))

                                    //format: "dd/MM/yyyy"
                                    //format: "yyyy/MM/dd"
                                });
                            });
                        }

                        //else {
                        //    $(document).ready(function () {
                        //        $("#from_date_new").kendoDatePicker({

                        //            format: "dd-MM-yyyy",


                        //        });
                        //    });


                        //    $(document).ready(function () {
                        //        $("#to_date_new").kendoDatePicker({

                        //            format: "dd-MM-yyyy",

                        //            //format: "dd/MM/yyyy"
                        //            //format: "yyyy/MM/dd"
                        //        });
                        //    });

                        //}


                   

                }
            }

            $scope.Alltermcode = function () {
                $http.get(ENV.apiUrl + "api/FeeTerm/getAllTerms?curCode=" + JSON.stringify($scope.temp)).then(function (AllTerms) {
                    $scope.All_Terms = AllTerms.data;
                })

            }

            $scope.showdata = function (str) {

                $scope.code = str;
                for (var i = 0; i < $scope.All_Terms.length; i++) {
                    if ($scope.All_Terms[i].sims_term_code == str) {
                        $scope.edt['sims_fee_term_code'] = $scope.All_Terms[i].sims_fee_term_code;
                        $scope.edt['sims_term_status'] = $scope.All_Terms[i].sims_term_status;
                        $scope.edt['sims_term_desc_en'] = $scope.All_Terms[i].sims_term_desc_en;
                        $scope.edt['sims_term_desc_ar'] = $scope.All_Terms[i].sims_term_desc_ar;
                        $scope.edt['sims_term_desc_ot'] = $scope.All_Terms[i].sims_term_desc_ot;
                        $scope.edt['sims_term_desc_fr'] = $scope.All_Terms[i].sims_term_desc_fr;
                        $scope.edt['sims_term_start_date'] = $scope.All_Terms[i].sims_term_start_date;
                        $scope.edt['sims_term_end_date'] = $scope.All_Terms[i].sims_term_end_date;

                        }
                    }
                }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    setTimeout(function () {
                        $(document).ready(function () {
                            $("#from_date_new, #to_date_new").kendoDatePicker({
                                format: "dd-MM-yyyy",
                                value: ''
                            });
                        });
                    }, 300);

                    $scope.newmode = true;
                    $scope.check = true;
                    $scope.edt = {};
                    $scope.editmode = false;
                    $scope.opr = 'S';
                    $scope.readonly = false;
                    $scope.table1 = false;
                    $scope.operation = true;
                    $scope.savebtn = true;
                    $scope.updatebtn = false;
                    $scope.temp = {};
                    $scope.edt = {
                        sims_term_start_date: dd + '-' + mm + '-' + yyyy,
                        sims_term_end_date: dd + '-' + mm + '-' + yyyy,
                        sims_term_status: true
                    }
                    $scope.temp['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                    $scope.temp['sims_academic_year'] = $scope.Academic_year[0].sims_academic_year;

                    $scope.myForm.$setPristine();
                    $scope.myForm.$setUntouched();
                    //var dt = new Date();
                    //$scope.edt.sims_term_start_date=('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                    //$scope.edt.sims_term_end_date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                    $scope.edt.sims_term_status = true;
                }
            }

            $scope.up = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    setTimeout(function () {
                        $(document).ready(function () {
                            $("#from_date_new, #to_date_new").kendoDatePicker({
                                format: "dd-MM-yyyy",
                                value: ''
                            });
                        });
                    }, 300);
                    $scope.opr = 'U';
                    $scope.editmode = true;
                    $scope.newmode = false;
                    $scope.savebtn = false;
                    $scope.updatebtn = true;
                    $scope.readonly = true;
                    $scope.table1 = false;
                    $scope.operation = true;

                    $scope.edt = str;
                }
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

            }

            $scope.Save = function (myForm) {

                setTimeout(function () {
                    $(document).ready(function () {
                        $("#from_date_new, #to_date_new").kendoDatePicker({
                            format: "dd-MM-yyyy",
                            value: ''
                        });
                    });
                }, 300);
                if (myForm) {
                    var data = {
                        sims_cur_code: $scope.temp.sims_cur_code,
                        sims_academic_year: $scope.temp.sims_academic_year,
                        sims_term_code: $scope.temp.sims_term_code,
                        sims_term_desc_en: $scope.edt.sims_term_desc_en,
                        sims_term_desc_ar: $scope.edt.sims_term_desc_ar,
                        sims_term_desc_fr: $scope.edt.sims_term_desc_ot,
                        sims_term_desc_ot: $scope.edt.sims_term_desc_fr,
                        sims_term_start_date: $scope.edt.sims_term_start_date,
                        sims_term_end_date: $scope.edt.sims_term_end_date,
                        sims_term_status: $scope.edt.sims_term_status,
                        opr: 'I'
                    }

                    $scope.exist = false;
                    for (var i = 0; i < $scope.FeeData.length; i++) {
                        if ($scope.FeeData[i].sims_cur_code == data.sims_cur_code && $scope.FeeData[i].sims_academic_year == data.sims_academic_year && $scope.FeeData[i].sims_term_code == data.sims_term_code && data.sims_term_start_date == $scope.FeeData[i].sims_term_start_date && data.sims_term_end_date == $scope.FeeData[i].sims_term_end_date) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({  text: "Record Already exists",imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/FeeTerm/FeeTermCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                setTimeout(function () {
                                    $(document).ready(function () {
                                        $("#from_date_new, #to_date_new").kendoDatePicker({
                                            format: "dd-MM-yyyy",
                                            value: ''
                                        });
                                    });
                                }, 300);
                            }
                            else if ($scope.msg1 == false) {
                                swal({  text: "Record Not Inserted. ",imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                            $scope.operation = false;
                            $http.get(ENV.apiUrl + "api/FeeTerm/getFeeTerm").then(function (Fee_Data) {
                                $scope.FeeData = Fee_Data.data;
                                $scope.totalItems = $scope.FeeData.length;
                                $scope.todos = $scope.FeeData;
                                $scope.makeTodos();

                            });

                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                    data1 = [];
                }
            }

            $scope.Update = function () {
               

                    setTimeout(function () {
                        $(document).ready(function () {
                            $("#from_date_new, #to_date_new").kendoDatePicker({
                                format: "dd-MM-yyyy",
                                value: ''
                            });
                        });
                    }, 300);
                    var data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/FeeTerm/FeeTermCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            setTimeout(function () {
                                $(document).ready(function () {
                                    $("#from_date_new, #to_date_new").kendoDatePicker({
                                        format: "dd-MM-yyyy",
                                        value: ''
                                    });
                                });
                            }, 300);
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $scope.operation = false;
                        $http.get(ENV.apiUrl + "api/FeeTerm/getFeeTerm").then(function (Fee_Data) {
                            $scope.FeeData = Fee_Data.data;
                            $scope.totalItems = $scope.FeeData.length;
                            $scope.todos = $scope.FeeData;
                            $scope.makeTodos();
                        });
                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                    data1 = [];
                

            }

            $scope.deleterecord = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    var data1 = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_term_code + i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletelocacode = ({
                                'sims_term_code': $scope.filteredTodos[i].sims_term_code,
                                'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                                'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
                                opr: 'D'
                            });
                            feecode.push(deletelocacode);
                        }
                    }

                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $http.post(ENV.apiUrl + "api/FeeTerm/FeeTermCUD", feecode).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/FeeTerm/getFeeTerm").then(function (Fee_Data) {
                                                    $scope.FeeData = Fee_Data.data;
                                                    $scope.totalItems = $scope.FeeData.length;
                                                    $scope.todos = $scope.FeeData;
                                                    $scope.makeTodos();
                                                });
                                            }
                                        });
                                    }
                                    else {
                                        swal({ text: "Record Not Deleted. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/FeeTerm/getFeeTerm").then(function (Fee_Data) {
                                                    $scope.FeeData = Fee_Data.data;
                                                    $scope.totalItems = $scope.FeeData.length;
                                                    $scope.todos = $scope.FeeData;
                                                    $scope.makeTodos();
                                                });
                                            }
                                        });
                                    }

                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    //var v = document.getElementById(i);
                                    var v = document.getElementById($scope.filteredTodos[i].sims_term_code + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        $scope.row1 = '';
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    data1 = [];
                    $scope.currentPage = true;
                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_term_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_term_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd-mm-yyyy'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.createdate = function (end_date, start_date, name) {


                //var month1 = end_date.split("/")[0];
                //var day1 = end_date.split("/")[1];
                //var year1 = end_date.split("/")[2];
                var day1 = end_date.split("-")[0];
                var month1 = end_date.split("-")[1];
                var year1 = end_date.split("-")[2];
                //var new_end_date = year1 + "/" + month1 + "/" + day1;
                var new_end_date = day1 + "-" + month1 + "-" + year1;

                //var year = start_date.split("/")[0];
                //var month = start_date.split("/")[1];
                //var day = start_date.split("/")[2];
                //var new_start_date = year + "/" + month + "/" + day;
                var day = start_date.split("-")[0];
                var month = start_date.split("-")[1];
                var year = start_date.split("-")[2];
                var new_start_date = day + "-" + month + "-" + year;

                if (new_end_date < new_start_date) {

                    $rootScope.strMessage = "Please Select Future Date";
                    $('#message').modal('show');

                    $scope.edt[name] = '';
                }
                else {

                    $scope.edt[name] = new_end_date;
                }
            }

            $scope.showdate = function (date, name) {

                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //$scope.edt[name] = year + "/" + month + "/" + day;
                var day = date.split("-")[0];
                var month = date.split("-")[1];
                var year = date.split("-")[2];
                $scope.edt[name] = day + "-" + month + "-" + year;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.FeeData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.FeeData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                return (
                     item.sims_term_desc_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_term_desc_fr.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_term_desc_ar.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_term_desc_ot.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_term_code == toSearch) ? true : false;
            }


            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();

            $scope.edt = {
                sims_term_start_date: dd + '-' + mm + '-' + yyyy,
                sims_term_end_date: dd + '-' + mm + '-' + yyyy
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            $scope.showdatecheck = function (exp_date_pass, issue_date_pass, name1) {

                var month1 = exp_date_pass.split("-")[1];
                var day1 = exp_date_pass.split("-")[0];
                var year1 = exp_date_pass.split("-")[2];
                var new_exp_date_pass = year1 + "-" + month1 + "-" + day1;

                var year = issue_date_pass.split("-")[2];
                var month = issue_date_pass.split("-")[1];
                var day = issue_date_pass.split("-")[0];
                var new_issue_date_pass = year + "-" + month + "-" + day;

                if (new_exp_date_pass < new_issue_date_pass) {

                    swal({
                        text: 'Please Select Future Date',
                        width: 300,
                        showCloseButton: true
                    });

                    $scope.edt[name1] = '';
                }
                else {

                    $scope.edt[name1] = day1 + "-" + month1 + "-" + year1;
                }
            }














        }]);
})();