﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('RefundTransactionCont',
        ['$scope', '$state', '$rootScope', '$timeout', '$stateParams', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, $stateParams, gettextCatalog, $http, $filter, ENV) {
            $scope.IP = $stateParams.IP;//{ 'StudCurr': '01', 'StudAcademic': '2017', 'StudEnroll': 'P10010' };
            $scope.$$hist = $stateParams.sel;
            $scope.currentClass = 0;
            $scope.maxLength = 0;
            $scope.CHd = {};
            $scope.CHd.srNo = 'none';
            $scope.SelectedFees = [];
            $scope.SelectedFeesBack = [];
            $scope.CHd.noOfCheques = 1;
            $scope.click = false
            $scope.DefaultPM = undefined;
            var rname = '';
            $scope.DPM = '';
            var comp_code = '1';
            $scope.SchoolLogo = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/SchoolLogo/';
            //$scope.receiptDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
            $scope.receiptDate = new Date().getDate() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getFullYear();
            $scope.$$cre = {};
            $scope.studName = '';
            $scope.studGradeName = '';
            $scope.studSectionName = '';
            $scope.studPAName = '';
            $scope.studCurr = '';
            $scope.appliedDD = [];
            $scope.dis = {};
            $scope.dis.showDis = false;
            $scope.dis.dd = [];
            $scope.total_paying = 0;
            $http.get(ENV.apiUrl + "api/Fee/StudentFee_New/GetReceipt").then(function (res) {
                rname = res.data;
            });

            $http.get(ENV.apiUrl + "api/Fee/StudentFee_New/GetDefaultPM").then(function (res) {
                $scope.DPM = res.data;
                if ($scope.DefaultPM == undefined) {
                    PM();
                }
            });
            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Final").then(function (res) {
                $scope.multi_print_final_doc_url = res.data;
            });
            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };


            $scope.search = function (callback) {
                $scope.clicked = false;
                $scope.pd = [];
                $scope.$$cre.totalFeeamut = 0;
                $scope.$$cre.Damut = 0;
                $scope.$$cre.GrandTotal = 0;
                $scope.dis.showDis = false;
                $scope.dis.dd = [];
                $scope.dis.sDcode = [];
                $scope.appliedDD = [];
                $scope.$$cre.totalamutPM = 0;
                $scope.CHd.srNo = 'none';
                $scope.click = false;
                //$scope.receiptDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
                $scope.receiptDate = new Date().getDate() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getFullYear();
                $scope.StudFeeDetails = [];
                $scope.SelectedFees = [];
                $scope.maxLength = 0;
                $scope.currentClass = 0;
                $http.get(ENV.apiUrl + "api/Fee/StudentFee_New/GetStudentFeeRefund?data=" + JSON.stringify($scope.IP)).then(function (res) {
                    $scope.StudFeeDetails = res.data;
                    console.log('Student Fee Details');
                    console.log($scope.StudFeeDetails);
                    if ($scope.StudFeeDetails.length > 0) {
                        for (var i = 0; i < $scope.StudFeeDetails.length; i++) {
                            if ($scope.StudFeeDetails[i].studDEnroll == $scope.IP.StudEnroll) {
                                debugger;
                                $scope.studName = $scope.StudFeeDetails[i].studDName;
                                $scope.studGradeName = $scope.StudFeeDetails[i].studGradeName;
                                $scope.studSectionName = $scope.StudFeeDetails[i].studSectionName;
                                $scope.studPAName = $scope.StudFeeDetails[i].payingAgentName;
                                $scope.studGrade = $scope.StudFeeDetails[i].studGrade;
                                $scope.studAcademicYear = $scope.StudFeeDetails[i].studAcademic;
                                $scope.studCurr = $scope.StudFeeDetails[i].studCurr;
                                $scope.currentClass = i;
                                //$http.get(ENV.apiUrl + "api/Fee/StudentFee_New/GetBanks").then(function (res) {
                                //    $scope.BankDet = res.data;
                                //});
                               // $scope.ParentName = $scope.StudFeeDetails[i].parentName;
                                $http.get(ENV.apiUrl + "api/Fee/StudentFee_New/GetBanksWithCompany?cur=" + $scope.studCurr + "&ayear=" + $scope.studAcademicYear + "&grade=" + $scope.studGrade).then(function (res) {
                                    $scope.BankDet = res.data;
                                });
                                break;

                            }
                        }
                        for (var i = 0; i < $scope.StudFeeDetails.length; i++) {
                            for (var j = 0; j < $scope.dis.dd.length; j++) {
                                copyDiscount($scope.dis.dd[j], $scope.StudFeeDetails[i].studDEnroll);
                            }
                        }

                        for (var i = 0; i < $scope.StudFeeDetails.length; i++) {
                            debugger
                            for (var j = 0; j < $scope.StudFeeDetails[i].feeDetails.length; j++) {
                                $scope.StudFeeDetails[i].feeDetails[j]['total_paying'] = parseFloat($scope.StudFeeDetails[i].feeDetails[j]['studBalancePayingAMT']) + parseFloat($scope.StudFeeDetails[i].feeDetails[j]['stud_vat_calc_amt']);
                            }

                        }
                    }
                    else {
                        swal({ text: 'Selected ' + $scope.IP.StudEnroll + ' Student Fee Not Defined/Data Fetching Issue...!', imageUrl: "assets/img/close.png", showCloseButton: true, width: 450, });
                    }
                });

                callback.call();
                $http.get(ENV.apiUrl + "api/Fee/StudentFee_New/GetStudDiscount?ay=" + $scope.IP.StudAcademic).then(function (res) {
                    $scope.dis.dd = res.data.table;
                    if ($scope.dis.dd.length > 0) {
                        for (var i = 0; i < $scope.StudFeeDetails.length; i++) {
                            for (var j = 0; j < $scope.dis.dd.length; j++) {
                                copyDiscount($scope.dis.dd[j], $scope.StudFeeDetails[i].studDEnroll);
                            }
                        }
                    }
                });
              
                $http.get(ENV.apiUrl + "api/studentfeerefund/GetStudentParentDetails?enroll=" + $scope.IP.StudEnroll + "&cur_code=" + $scope.IP.StudCurr + "&aca_year=" + $scope.IP.StudAcademic).then(function (GetStudentParentDetails) {
                    $scope.Parent_Name = GetStudentParentDetails.data;
                    if ($scope.Parent_Name.length > 0) {
                        $scope.ParentName = $scope.Parent_Name[0].std_fee_parent_name;
                       
                    }
                });
                
            }

            function copyDiscount(e, enroll) {
                var r = {};
                if (e != undefined) {
                    r.sims_concession_academic_year = e.sims_concession_academic_year;
                    r.sims_concession_applicable_on = e.sims_concession_applicable_on;
                    r.sims_concession_corporate_billing = e.sims_concession_corporate_billing;
                    r.sims_concession_description = e.sims_concession_description;
                    r.sims_concession_discount_type = e.sims_concession_discount_type;
                    r.sims_concession_discount_value = e.sims_concession_discount_value;
                    r.sims_concession_fee_code = e.sims_concession_fee_code;
                    r.sims_concession_number = e.sims_concession_number;
                    r.sims_concession_onward_child = e.sims_concession_onward_child;
                    r.sims_concession_status = e.sims_concession_status;
                    r.sims_concession_type = e.sims_concession_type;
                    r.feeDesc = e.feeDesc;
                    r.didDesc = e.didDesc;
                    r.feeAmt = 0;
                    r.enrollNumber = enroll;
                    r.visible = false;
                    //r.Used = false;
                    r.status = false;
                    $scope.appliedDD.push(r);
                }

                if ($scope.dis.sDcode[enroll] == undefined) {
                    $scope.dis.sDcode[enroll] = [];
                }
                if (e.sims_concession_fee_code != '000') {
                    $scope.dis.sDcode[enroll].push(e.sims_concession_fee_code);
                }

                return r;
            }

            function PM() {
                $http.get(ENV.apiUrl + "api/Fee/StudentFee_New/GetPaymentMode").then(function (res) {
                    $scope.PaymentModes = res.data;
                    $scope.pd = [];
                    for (var i = 0; i < $scope.PaymentModes.length; i++) {
                        if ($scope.PaymentModes[i].pmType == "1") {
                            $scope.caPM = $scope.PaymentModes[i];
                            break;
                        }
                    }

                    for (var i = 0; i < $scope.PaymentModes.length; i++) {
                        if ($scope.PaymentModes[i].pmDescShort == $scope.DPM) {
                            $scope.DefaultPM = {};
                            $scope.DefaultPM.modeO = $scope.PaymentModes[i];
                            $scope.DefaultPM.chequeNo = '';
                            $scope.DefaultPM.chqBank = { 'bankCode': '' };
                            $scope.DefaultPM.feeNumber = '';
                            $scope.DefaultPM.chqDate = '';
                            $scope.DefaultPM.trxID = '';
                            $scope.DefaultPM.flag = false;
                            $scope.DefaultPM.FPamt = $scope.$$cre.totalFeeamt() - $scope.$$cre.Damut;
                            $scope.pd.push($scope.DefaultPM);
                            break;
                        }
                    }


                    for (var i = 0; i < $scope.PaymentModes.length; i++) {
                        if ($scope.PaymentModes[i].pmType == "2") {
                            $scope.chPM = $scope.PaymentModes[i];
                            break;
                        }
                    }
                });

            }

            $scope.dis.toggleDisPane = function () {
                //$scope.dis.showDis = false;
                this.cheight = this.cheight == "1px" ? "inherit" : "1px";
                this.nBottomPaadding = this.cheight;
            }

            $scope.OnSelAll = function (std) {
                if (std.selAll) {
                    for (var i = 0; i < std.feeDetails.length; i++) {
                        var item = std.feeDetails[i];
                        if (!item.master) {
                            item.master = true;
                            var ob = {};
                            var ID = $scope.maxLength++;
                            item.ID = ID;
                            ob.ref = item.$$hashKey
                            ob.pno = item.studFeePeriodNo;
                            ob.feeNumber = item.studFeeNumber;
                            ob.ID = ID;
                            ob.rowType = "M";
                            ob.flag = false;
                            ob.splitFeesAmt = parseFloat(item.studBalancePayingAMT);
                            ob.FD = item.studFeeDesc;
                            ob.enroll = item.studEnroll;
                            ob.studCurr = item.studCurr;
                            ob.studAcademic = item.studAcademic;
                            ob.studGrade = item.studGrade;
                            ob.studSection = item.studSection;
                            ob.mode = {};
                            //ob.mode = $scope.caPM;
                            ob.chqNo = "NA";
                            ob.isSplitted = false;
                            ob.chqBank = "NA";
                            ob.chqDate = null;
                            ob.trxID = "NA";
                            ob.sr = $scope.SelectedFees.length + 1;
                            ob.ExpAmt = item.studBalanceAMT;

                            ob.FPamtORG = parseFloat(parseFloat(item.studBalancePayingAMT) + parseFloat((parseFloat(item.studBalancePayingAMT) * parseFloat(item.stud_vat_per)) / 100)).toFixed(2)
                            ob.FPamt = parseFloat(parseFloat(item.studBalancePayingAMT) + parseFloat((parseFloat(item.studBalancePayingAMT) * parseFloat(item.stud_vat_per)) / 100)).toFixed(2);
                            ob.FPamtINT = parseFloat(parseFloat(item.studBalancePayingAMT) + parseFloat((parseFloat(item.studBalancePayingAMT) * parseFloat(item.stud_vat_per)) / 100)).toFixed(2);
                            ob.stud_vat_calc_amt = parseFloat((item.studBalancePayingAMT * item.stud_vat_per) / 100).toFixed(2);

                            //ob.FPamtORG = item.studBalancePayingAMT;
                            //ob.FPamt = item.studBalancePayingAMT;
                            //ob.FPamtINT = parseFloat(item.studBalancePayingAMT);
                            //ob.stud_vat_calc_amt = (item.studBalancePayingAMT * item.stud_vat_per) / 100;

                            ob.FCAmt = item.studConcessionAMT;
                            ob.FeeCat = item.studFeeCategoryCode;
                            ob.FeeCode = item.studFeeCode;
                            $scope.SelectedFees.push(ob);
                            $scope.SelectedFeesBack.push(ob);
                        }
                    }
                }
                else {
                    var le = $scope.SelectedFees.length;
                    var lf = std.feeDetails.length;
                    for (var j = 0; j < lf; j++) {
                        var item = std.feeDetails[j];
                        item.master = false;
                        for (var i = 0; i < le; i++) {
                            if ($scope.SelectedFees[i].ref == item.$$hashKey) {
                                $scope.SelectedFees.splice(i, 1);
                                $scope.SelectedFeesBack.splice(i, 1);
                                break;
                            }
                        }
                    }
                }


                var ta = $scope.$$cre.totalFeeamt();
                for (var i = 0; i < $scope.pd.length; i++) {
                    if ($scope.pd[i].modeO == $scope.caPM) {
                        $scope.pd[i].FPamt = ta;
                        break;
                    }
                }
                if ($scope.pd.length == 1) {
                    $scope.DefaultPM.FPamt = ta;
                }
                $scope.$$cre.totalamtPM();
                if (std.feeDetails.length > 0)
                    showHideDiscounts(std.feeDetails[0], std);
            }

            if ($scope.IP != undefined) {
                $scope.search(PM);
            }

            $scope.CHd.colse = function (v, a) {
                $scope.CHd.srNo = v;
                $scope.ne = {}
                $scope.ne.FPamt = (($scope.$$cre.totalFeeamt() - $scope.$$cre.Damut) - $scope.$$cre.totalamtPM());
                $scope.ne.valAMT = $scope.ne.FPamt;
                if (a == undefined) {
                    if ($scope.ne.FPamt == 0) {
                        $scope.CHd.srNo = 'none';
                        swal({ text: 'Please remove the below entries first.', imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 450, });
                    }
                }
                $scope.ne.parent_name=$scope.ParentName;
            }

            $scope.adPMode = function (item) {
                debugger;
                var l = $scope.pd.length;
                if (item.FPamt > item.valAMT) {
                    $scope.valcc = 1;
                    return;
                }
                $scope.valcc = 0;
                if (item == undefined) {
                    document.getElementById('pm').focus();
                    return;
                }
                if (item.modeO.pmType != "1") {
                    
                    if (item.chequeNo == undefined || item.chequeNo == '') {
                        swal({ text: 'Please Enter Cheque No.', imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, });
                        document.getElementById('chno').focus();
                        return;
                    }
                    if (item.chqDate == undefined || item.chqDate == '') {
                        swal({ text: 'Please select date', imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, });
                        return;
                    }
                    if (item.chqBank == undefined || item.chqBank == '') {
                        swal({ text: 'Please select Bank Name', imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, });
                        return;
                    }
                }

                if (item.FPamt == undefined || item.FPamt == '') {
                    document.getElementById('pm').focus();
                    return;
                }
                if ((item.chequeNo == undefined || item.chequeNo == '') && item.modeO.pmType != "1") {
                    document.getElementById('chno').focus();
                    return;
                }
                if (item.modeO == $scope.caPM) {
                    for (var i = 0; i < l; i++) {
                        if ($scope.pd[i].modeO == item.modeO) {
                            $scope.pd[i].FPamt = parseFloat($scope.pd[i].FPamt) + parseFloat(item.FPamt)
                            return;
                        }
                    }
                }
               
               

                for (var i = 0; i < l; i++) {
                    if ($scope.pd[i].modeO == item.modeO && $scope.pd[i].chequeNo == item.chequeNo && $scope.pd[i].chqBank == item.chqBank && item.modeO == $scope.chPM) {
                        $scope.pd[i].FPamt = parseFloat($scope.pd[i].FPamt) + parseFloat(item.FPamt)
                        return;
                    }
                }
                var ob = {};
                ob.modeO = item.modeO;
                ob.chequeNo = item.chequeNo;
                ob.chqBank = item.chqBank == undefined ? { 'bankCode': '' } : item.chqBank;
                ob.feeNumber = '';
                //if (!(item.chqDate == '' || item.chqDate == undefined)) {
                //    var v = new Date(item.chqDate);
                //    item.chqDate = v.getFullYear() + "-" + (v.getMonth() + 1) + "-" + v.getDate();
                //}
                ob.chqDate = item.chqDate;
                ob.trxID = item.trxID;
                ob.flag = false;
                ob.FPamt = parseFloat(item.FPamt);
                ob.parent_name = $scope.ParentName;
                $scope.pd.push(ob);
                $scope.$$cre.totalamtPM();
                item.modeO = {};
                item.chequeNo = '';
                item.chqBank = {};
                item.chqDate = '';
                item.trxID = '';
                item.FPamt = ($scope.$$cre.totalFeeamt() - $scope.$$cre.Damut) - $scope.$$cre.totalamutPM;
                item.valAMT = item.FPamt;
                if (item.FPamt == 0)
                    $scope.CHd.colse('none', 'x');
            }

            $scope.rmPMode = function (item) {
                var l = $scope.pd.length;
                for (var i = 0; i < l; i++) {
                    if ($scope.pd[i].$$hashKey == item.$$hashKey) {
                        $scope.pd.splice(i, 1);
                        break;
                    }
                }
                $scope.$$cre.totalamutPM = $scope.$$cre.totalamtPM();
            }

            $scope.OnFeeAdd = function (item, p) {
                if (item.master) {
                    var ob = {};
                    var ID = $scope.maxLength++;
                    item.ID = ID;

                    ob.ref = item.$$hashKey
                    ob.pno = item.studFeePeriodNo;
                    ob.feeNumber = item.studFeeNumber;
                    ob.ID = ID;
                    ob.rowType = "M";
                    ob.flag = false;
                    ob.splitFeesAmt = parseFloat(item.studBalancePayingAMT);
                    ob.FD = item.studFeeDesc;
                    ob.enroll = item.studEnroll;
                    ob.studCurr = item.studCurr;
                    ob.studAcademic = item.studAcademic;
                    ob.studGrade = item.studGrade;
                    ob.studSection = item.studSection;
                    ob.mode = {};
                    //ob.mode = $scope.caPM;
                    ob.chqNo = "NA";
                    ob.isSplitted = false;
                    ob.chqBank = "NA";
                    ob.chqDate = null;
                    ob.trxID = "NA";
                    ob.sr = $scope.SelectedFees.length + 1;
                    ob.ExpAmt = item.studBalanceAMT;
                    ob.FPamtORG = parseFloat(parseFloat(item.studBalancePayingAMT) + parseFloat((parseFloat(item.studBalancePayingAMT) * parseFloat(item.stud_vat_per)) / 100)).toFixed(2)
                    ob.FPamt = parseFloat(parseFloat(item.studBalancePayingAMT) + parseFloat((parseFloat(item.studBalancePayingAMT) * parseFloat(item.stud_vat_per)) / 100)).toFixed(2);
                    ob.FPamtINT = parseFloat(parseFloat(item.studBalancePayingAMT) + parseFloat((parseFloat(item.studBalancePayingAMT) * parseFloat(item.stud_vat_per)) / 100)).toFixed(2);
                    ob.stud_vat_calc_amt = parseFloat((item.studBalancePayingAMT * item.stud_vat_per) / 100).toFixed(2);

                    ob.FCAmt = item.studConcessionAMT;
                    ob.FeeCat = item.studFeeCategoryCode;
                    ob.FeeCode = item.studFeeCode;
                    ob.disAmt = 0;
                    ob.disNumber = '';
                    ob.disApplied = false;

                    $scope.SelectedFees.push(ob);
                    $scope.SelectedFeesBack.push(ob);

                    var added = false;
                    var l = p.feeDetails.length;
                    debugger
                    for (var i = 0; i < l; i++) {
                        if (p.feeDetails[i].studFeeNumber == item.studFeeNumber && p.feeDetails[i].studEnroll == item.studEnroll && p.feeDetails[i].srNo == item.srNo) {
                            debugger;
                            p.feeDetails[i].stud_vat_calc_amt = (item.studBalancePayingAMT * item.stud_vat_per) / 100;
                            p.feeDetails[i].total_paying = parseFloat(item.studBalancePayingAMT) + parseFloat(p.feeDetails[i].stud_vat_calc_amt);
                            added = true;
                            break;

                        }
                    }

                    var b = true;
                    for (var i = 0; i < p.feeDetails.length; i++) {
                        b = b && p.feeDetails[i].master;
                    }
                    p.selAll = b;
                }
                else {
                    var le = $scope.SelectedFees.length;
                    for (var i = 0; i < le; i++) {
                        if ($scope.SelectedFees[i].ref == item.$$hashKey) {
                            $scope.SelectedFees.splice(i, 1);
                            $scope.SelectedFeesBack.splice(i, 1);
                            break;
                        }
                    }
                    p.selAll = false;
                }

                showHideDiscounts(item, p);
                $scope.check();
                var ta = $scope.$$cre.totalFeeamt();
                for (var i = 0; i < $scope.pd.length; i++) {
                    if ($scope.pd[i].modeO == $scope.caPM) {
                        $scope.pd[i].FPamt = (ta - $scope.$$cre.Damut) - $scope.$$cre.totalamtPM() < 0 ? 0 : (ta - $scope.$$cre.Damut) - $scope.$$cre.totalamtPM();
                        break;
                    }
                }
                if ($scope.pd.length == 1) {
                    $scope.DefaultPM.FPamt = ta - $scope.$$cre.Damut;
                }
                $scope.$$cre.totalamtPM();
            }

            $scope.$$cre.totalFeeamt = function () {
                if (typeof ($scope.SelectedFees) === 'undefined') {
                    return 0;
                }
                var sum = 0;
                var Dissum = 0;
                for (var i = $scope.SelectedFees.length - 1; i >= 0; i--) {
                    sum += parseFloat($scope.SelectedFees[i].FPamt); //+ parseFloat($scope.SelectedFees[i].stud_vat_calc_amt);
                    if ($scope.SelectedFees[i].disApplied) {
                        Dissum = Dissum + parseFloat($scope.SelectedFees[i].disAmt);
                    }
                }

                $scope.$$cre.totalFeeamut = parseFloat(sum).toFixed($scope.decimal_dsc);
                $scope.$$cre.Damut = Math.ceil(Dissum).toFixed($scope.decimal_dsc);
                return sum.toFixed($scope.decimal_dsc);
            }

            $scope.$$cre.totalamtPM = function () {
                if (typeof ($scope.pd) === 'undefined') {
                    return 0;
                }
                var l = $scope.pd.length;
                var sum = 0;
                for (var i = 0; i < l; i++) {
                    sum += parseFloat($scope.pd[i].FPamt);
                }

                $scope.$$cre.totalamutPM = sum.toFixed($scope.decimal_dsc);
                return sum.toFixed($scope.decimal_dsc);
            }

            $scope.check = function () {
                var ta = $scope.$$cre.totalFeeamt();
                //$scope.$$cre.totalamtPM();
                if ($scope.pd.length == 1) {
                    $scope.DefaultPM.FPamt = ta - $scope.$$cre.Damut;
                }
                $scope.$$cre.totalamtPM();
            }

            function showHideDiscounts(item, p) {
                var sl = $scope.appliedDD.length;
                for (var i = 0; i < sl; i++) {
                    if ($scope.appliedDD[i].enrollNumber == item.studEnroll) {
                        var ob = getDFDetails(p, $scope.appliedDD[i].sims_concession_fee_code, $scope.appliedDD[i].sims_concession_fee_code == '000')
                        $scope.appliedDD[i].feeAmt = ob.amt;
                        $scope.appliedDD[i].visible = ob.flag;
                        //$scope.dis.showDis = $scope.dis.showDis || ob.flag;
                    }
                }
                qat2v6();
            }

            function qat2v6() {
                var sl = $scope.appliedDD.length;
                $scope.dis.showDis = false;
                for (var i = 0; i < sl; i++) {
                    $scope.dis.showDis = $scope.appliedDD[i].visible;
                    if ($scope.dis.showDis)
                        break;
                }
            }

            function getDFDetails(p, fc, c) {
                var l = p.feeDetails.length;
                var s = 0; var v = false;
                if (c) {
                    for (var i = 0; i < l; i++) {
                        var amt = parseFloat(p.feeDetails[i].studBalancePayingAMT);
                        if (p.feeDetails[i].master && $scope.dis.sDcode[p.feeDetails[i].studEnroll].includes(p.feeDetails[i].studFeeCode) == false) { //&& amt>0
                            s = s + amt;
                            v = v || true;
                        }
                    }
                }
                else {
                    for (var i = 0; i < l; i++) {
                        var amt = parseFloat(p.feeDetails[i].studBalancePayingAMT);
                        if (p.feeDetails[i].studFeeCode == fc && p.feeDetails[i].master) { //&& amt>0
                            s = s + amt;
                            v = v || true;
                        }
                    }
                }
                return { 'amt': s, 'flag': (v && s > 0) };
            }

            $scope.hitEnter = function (event) {
                if (event.key == "Enter") {
                    $scope.search(PM);
                }
            }

            function CheckblockSpecialChar(a) {
                var k = a.charCodeAt(0);
                return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
            }

            $scope.numOnly = function (e) {
                var k = e.which;
                var r = (k == 8 || k == 110 || k == 37 || k == 39 || k == 9 || k == 32 || (k >= 48 && k <= 57) || (k >= 96 && k <= 105) || k == 190);
                if (r == false) {
                    e.preventDefault();
                    return;
                }
                $scope.$$cre.totalamtPM();
            }

            $scope.submit = function () {
                debugger;
                console.clear();
                console.log($scope.SelectedFees);
                sort($scope.pd, 'FPamt');
                sort($scope.SelectedFees, 'FPamt');

                if ($scope.click == false) {;
                    $scope.click = true;
                    $scope.clicked = true;
                    var feedata = {};
                    feedata.ParentID = $scope.StudFeeDetails[0].parentID;
                    feedata.Students = [];
                    feedata.ReceiptDate = $scope.receiptDate;
                    feedata.Remark = $scope.IP.Remark;
                    feedata.UserName = $rootScope.globals.currentUser.username;
                    $scope.tmpFn = '';
                    $scope.tmpEn = '';
                    var fnFound = false;
                    var enFound = false;
                    var l = $scope.SelectedFees.length;
                    if (l == 0) {
                        $rootScope.strMessage = 'Please select fee';
                        $('#message').modal('show');
                        return;
                    }
                    var v = ($scope.$$cre.totalFeeamt() - $scope.$$cre.Damut) - $scope.$$cre.totalamtPM();
                    if (v > 0) {
                        $rootScope.strMessage = 'Amount Mismatch; Need ' + v + ' More.';
                        $('#message').modal('show');
                        return;
                    }
                    step3();
                    debugger;
                    l = $scope.SelectedFees.length;
                    for (var i = 0; i < l; i++) {
                        if ($scope.SelectedFees[i].ID != undefined) {
                            $scope.tmpFn = $scope.SelectedFees[i].feeNumber;
                            $scope.tmpEn = $scope.SelectedFees[i].enroll;
                            $scope.tmpFc = $scope.SelectedFees[i].FeeCode;
                            fnFound = false;
                            enFound = false;
                            for (var j = 0; j < feedata.Students.length; j++) {
                                fnFound = false;
                                enFound = false;
                                if ($scope.tmpEn == feedata.Students[j].Enroll) {
                                    enFound = true;
                                    fnFound = false;
                                    for (var k = 0; k < feedata.Students[j].Fees.length; k++) {
                                        if ($scope.tmpFn == feedata.Students[j].Fees[k].Fee_number && $scope.tmpFc == feedata.Students[j].Fees[k].FeeCode) {
                                            feedata.Students[j].Fees[k].Periods.push({ 'Pno': $scope.SelectedFees[i].pno, 'FPamt': $scope.SelectedFees[i].FPamt, 'PayMode': $scope.SelectedFees[i].mode.pmDescShort, 'ChequeNo': $scope.SelectedFees[i].chqNo, 'BankCode': $scope.SelectedFees[i].chqBank['bankCode'], 'ChequeDt': $scope.SelectedFees[i].chqDate, 'TXNNo': $scope.SelectedFees[i].TXNNo, 'FCAmt': $scope.SelectedFees[i].disAmt, 'DisNumber': $scope.SelectedFees[i].disNumber, 'Vat': $scope.SelectedFees[i].stud_vat_calc_amt, 'Vat_rnd': $scope.SelectedFees[i].stud_vat_per });
                                            fnFound = true;
                                            break;
                                        }
                                    }

                                    if (fnFound == false) {
                                        var len = feedata.Students[j].Fees.length;
                                        feedata.Students[j].Fees[len] = { 'Fee_number': $scope.tmpFn, 'Fee_Category': $scope.SelectedFees[i].FeeCat, 'Fee_Code': $scope.SelectedFees[i].FeeCode, 'Periods': [] };
                                        feedata.Students[j].Fees[len].Periods.push({ 'Pno': $scope.SelectedFees[i].pno, 'FPamt': $scope.SelectedFees[i].FPamt, 'PayMode': $scope.SelectedFees[i].mode.pmDescShort, 'ChequeNo': $scope.SelectedFees[i].chqNo, 'BankCode': $scope.SelectedFees[i].chqBank['bankCode'], 'ChequeDt': $scope.SelectedFees[i].chqDate, 'TXNNo': $scope.SelectedFees[i].TXNNo, 'FCAmt': $scope.SelectedFees[i].disAmt, 'DisNumber': $scope.SelectedFees[i].disNumber, 'Vat': $scope.SelectedFees[i].stud_vat_calc_amt, 'Vat_rnd': $scope.SelectedFees[i].stud_vat_per });
                                        break;
                                    }
                                }
                            }
                            if (enFound == false) {
                                var len = feedata.Students.length;
                                feedata.Students[len] = { 'Enroll': $scope.tmpEn, 'StudCurr': $scope.SelectedFees[i].studCurr, 'StudAcademic': $scope.SelectedFees[i].studAcademic, 'StudGrade': $scope.SelectedFees[i].studGrade, 'StudSection': $scope.SelectedFees[i].studSection, 'Fees': [], 'ReceiptDate': $scope.receiptDate };
                                feedata.Students[len].Fees.push({ 'Fee_number': $scope.tmpFn, 'Fee_Category': $scope.SelectedFees[i].FeeCat, 'Fee_Code': $scope.SelectedFees[i].FeeCode, 'Periods': [] });
                                feedata.Students[len].Fees[0].Periods.push({ 'Pno': $scope.SelectedFees[i].pno, 'FPamt': $scope.SelectedFees[i].FPamt, 'PayMode': $scope.SelectedFees[i].mode.pmDescShort, 'ChequeNo': $scope.SelectedFees[i].chqNo, 'BankCode': $scope.SelectedFees[i].chqBank['bankCode'], 'ChequeDt': $scope.SelectedFees[i].chqDate, 'TXNNo': $scope.SelectedFees[i].TXNNo, 'FCAmt': $scope.SelectedFees[i].disAmt, 'DisNumber': $scope.SelectedFees[i].disNumber, 'Vat': $scope.SelectedFees[i].stud_vat_calc_amt, 'Vat_rnd': $scope.SelectedFees[i].stud_vat_per });
                            }
                        };
                    }
                    $http.post(ENV.apiUrl + "api/Fee/StudentFee_New/SubmitStudRefundFees", feedata).then(function (resl) {
                        $scope.Result = resl.data;
                        $scope.SelectedFees = [];
                        $scope.currentPM = undefined;
                        $rootScope.showReport = true;
                        $scope.search(PM);
                        if (resl.data == undefined || resl.data == '') {
                            $rootScope.strMessage = 'Error';
                        }
                        else {

                            if ($http.defaults.headers.common['schoolId'] == 'abqis' || $http.defaults.headers.common['schoolId'] == 'asis' || $http.defaults.headers.common['schoolId'] == 'siso') {

                                $http.post(ENV.apiUrl + "api/studentfeerefund/VoucherGeneration?receipt_no=" + $scope.Result).then(function (res) {
                                    var voucher = res.data;
                                    swal({ text: 'Voucher No:- ' + voucher, imageUrl: "assets/img/check.png", width: 350, showCloseButton: true });
                                    var data = {
                                        location: $scope.multi_print_final_doc_url,
                                        parameter: {
                                            comp_detail: comp_code,
                                            doc: voucher,
                                        },

                                        state: 'main.SFRef',
                                        ready: function () {
                                            this.refreshReport();
                                        },
                                    }
                                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                                    $state.go('main.ReportCardParameter')

                                });
                            }
                            else {

                                $rootScope.strMessage = 'Fee Collected Successfully( ' + $scope.Result + ')';
                                var data = {
                                    location: rname,
                                    parameter: { fee_rec_no: $scope.Result, duplicate_flag: '0' },
                                    state: 'main.SFRef'
                                }
                                window.localStorage["ReportDetails"] = JSON.stringify(data)
                                $state.go('main.ReportCardParameter')
                            }
                        }
                        $('#message').modal('show');
                        //  $state.go('main.Sim043');
                        // showReport($scope.reportData);
                    });
                }
            }

            function step1() {
                var leF = $scope.SelectedFees.length;
                var lep = $scope.pd.length;
                for (var i = 0; i < leF; i++) {
                    if ($scope.SelectedFees[i].flag == false) {
                        for (var j = 0; j < lep; j++) {
                            var a = $scope.SelectedFees[i].disApplied ? parseFloat($scope.SelectedFees[i].disAmt) : 0;
                            if ($scope.pd[j].flag == false && $scope.SelectedFees[i].flag == false && parseFloat($scope.pd[j].FPamt) == (parseFloat($scope.SelectedFees[i].FPamt) - parseFloat(a))) {
                                $scope.SelectedFees[i].chqNo = $scope.pd[j].chequeNo;
                                $scope.SelectedFees[i].chqDate = $scope.pd[j].chqDate;
                                $scope.SelectedFees[i].chqBank = $scope.pd[j].chqBank;
                                $scope.SelectedFees[i].mode = $scope.pd[j].modeO;
                                $scope.SelectedFees[i].TXNNo = $scope.pd[j].trxID;
                                $scope.pd[j].feeNumber = $scope.SelectedFees[i].feeNumber;
                                $scope.pd[j].flag = true;
                                $scope.SelectedFees[i].flag = true;
                            }
                        }
                    }
                }
                sort($scope.SelectedFees, 'FPamt');
            }

            function step2() {
                var isbreak = false;
                step1();
                sort($scope.pd, 'FPamt');
                var leF = $scope.SelectedFees.length;
                var lep = $scope.pd.length;
                for (var i = 0; i < leF; i++) {
                    if ($scope.SelectedFees[i].flag == false) {
                        for (var j = 0; j < lep; j++) {
                            if ($scope.pd[j].flag == false) {
                                var a = $scope.SelectedFees[i].disApplied ? parseFloat($scope.SelectedFees[i].disAmt) : 0;
                                if ((parseFloat($scope.SelectedFees[i].FPamt) - parseFloat(a)) < parseFloat($scope.pd[j].FPamt)) {
                                    /*Split here*/
                                    var amt = parseFloat($scope.pd[j].FPamt) - (parseFloat($scope.SelectedFees[i].FPamt) - parseFloat(a));
                                    $scope.SelectedFees[i].chqNo = $scope.pd[j].chequeNo;
                                    $scope.SelectedFees[i].chqDate = $scope.pd[j].chqDate;
                                    $scope.SelectedFees[i].chqBank = $scope.pd[j].chqBank;
                                    $scope.SelectedFees[i].mode = $scope.pd[j].modeO;
                                    $scope.SelectedFees[i].TXNNo = $scope.pd[j].trxID;
                                    $scope.pd[j].feeNumber = $scope.SelectedFees[i].feeNumber;
                                    $scope.pd[j].FPamt = parseFloat($scope.SelectedFees[i].FPamt);
                                    $scope.pd[j].flag = true;
                                    $scope.SelectedFees[i].flag = true;

                                    var ob = {};
                                    ob.modeO = $scope.pd[j].modeO;
                                    ob.chequeNo = $scope.pd[j].chequeNo;
                                    ob.chqBank = $scope.pd[j].chqBank;
                                    ob.feeNumber = '';
                                    ob.chqDate = $scope.pd[j].chqDate;
                                    ob.trxID = $scope.pd[j].trxID;
                                    ob.flag = false;
                                    ob.FPamt = parseFloat(amt);
                                    $scope.pd.push(ob);
                                    i = leF;
                                    j = lep;
                                    isbreak = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (isbreak) {
                        step2();
                        break;
                    }
                }
            }

            function step3() {
                var isbreak = false;
                step2();
                sort($scope.pd, 'FPamt');
                sort($scope.SelectedFees, 'FPamt');
                var leF = $scope.SelectedFees.length;
                var lep = $scope.pd.length;
                for (var i = 0; i < lep; i++) {
                    if ($scope.pd[i].flag == false) {
                        for (var j = 0; j < leF; j++) {
                            if ($scope.SelectedFees[j].flag == false) {
                                var a = $scope.SelectedFees[i].disApplied ? parseFloat($scope.SelectedFees[i].disAmt) : 0;
                                if ((parseFloat($scope.SelectedFees[j].FPamt) - parseFloat(a)) > parseFloat($scope.pd[i].FPamt)) {
                                    /*Split here*/
                                    var amt = (parseFloat($scope.SelectedFees[j].FPamt) - parseFloat(a)) - parseFloat($scope.pd[i].FPamt);
                                    $scope.SelectedFees[j].chqNo = $scope.pd[i].chequeNo;
                                    $scope.SelectedFees[j].chqDate = $scope.pd[i].chqDate;
                                    $scope.SelectedFees[j].chqBank = $scope.pd[i].chqBank;
                                    $scope.SelectedFees[j].mode = $scope.pd[i].modeO;
                                    $scope.SelectedFees[j].FPamt = parseFloat($scope.pd[i].FPamt);
                                    $scope.SelectedFees[j].TXNNo = $scope.pd[i].trxID;
                                    $scope.pd[i].feeNumber = $scope.SelectedFees[j].feeNumber;
                                    $scope.pd[i].flag = true;
                                    $scope.SelectedFees[j].flag = true;

                                    var ob = {};
                                    ob.ref = $scope.SelectedFees[j].$$hashKey
                                    ob.pno = $scope.SelectedFees[j].pno;
                                    ob.feeNumber = $scope.SelectedFees[j].feeNumber;
                                    ob.ID = $scope.SelectedFees[j].ID;
                                    ob.flag = false;
                                    ob.FD = $scope.SelectedFees[j].FD;
                                    ob.enroll = $scope.SelectedFees[j].enroll;
                                    ob.studCurr = $scope.SelectedFees[j].studCurr;
                                    ob.studAcademic = $scope.SelectedFees[j].studAcademic;
                                    ob.studGrade = $scope.SelectedFees[j].studGrade;
                                    ob.studSection = $scope.SelectedFees[j].studSection;
                                    //ob.disApplied = $scope.SelectedFees[j].disApplied;
                                    //ob.disAmt = $scope.SelectedFees[j].disAmt;
                                    //ob.disNumber = $scope.SelectedFees[j].disNumber;
                                    ob.mode = {};
                                    ob.chqNo = "NA";
                                    ob.isSplitted = false;
                                    ob.chqBank = "NA";
                                    ob.chqDate = null;
                                    ob.trxID = "NA";
                                    ob.sr = $scope.SelectedFees.length + 1;
                                    ob.ExpAmt = parseFloat($scope.SelectedFees[j].studBalanceAMT);
                                    ob.FPamtORG = parseFloat($scope.SelectedFees[j].studBalancePayingAMT);
                                    ob.FPamt = parseFloat(amt);
                                    ob.FPamtINT = parseFloat(amt);
                                    //***ob.FCAmt = $scope.SelectedFees[j].studConcessionAMT;
                                    ob.FeeCat = $scope.SelectedFees[j].FeeCat;
                                    ob.FeeCode = $scope.SelectedFees[j].FeeCode;

                                    $scope.SelectedFees.push(ob);
                                    i = leF;
                                    j = lep;
                                    isbreak = true;
                                    break;
                                }
                            }
                        }
                        if (isbreak) {
                            step3();
                            break;
                        }
                    }
                }
            }

            function sort(lst, prop) {
                var len = lst.length;
                for (var i = 0; i < len; i++) {
                    for (var j = i + 1; j < len; ++j) {
                        if (parseFloat(lst[i][prop]) < parseFloat(lst[j][prop])) {
                            var a = lst[i];
                            lst[i] = lst[j];
                            lst[j] = a;
                        }
                    }
                }
            }

            $scope.OnDisAdd = function (item) {
                if (!item.status) {
                    rd(item);
                }
            }

            $scope.addOtherFee = function (item) {
                debugger;
                if (item.OFD.oF_AMT == undefined || parseInt(item.OFD.oF_AMT) <= 0) {
                    $rootScope.strMessage = 'Amount Must be greater than Zero(0)';
                    $('#message').modal('show');
                    return;
                }
                var o = {};
                o.studFeePeriodNo = new Date().getMonth() + 1;
                o.studFeeNumber = null;
                o.studBalancePayingAMT = parseFloat(item.OFD.oF_AMT);
                o.studFeeDesc = item.OFD.oF_Desc;
                o.studEnroll = item.studDEnroll;
                o.studCurr = item.studCurr;
                o.studAcademic = item.studAcademic;
                o.studGrade = item.studGrade;
                o.studSection = item.studSection;
                o.studBalanceAMT = parseFloat(item.OFD.oF_AMT) + (item.OFD.oF_AMT * item.OFD.vat_per) / 100;
                o.studExpectedFeeAMT = 0;
                o.studTotalPaidAMT = 0;
                o.studBalancePayingAMT = parseFloat(item.OFD.oF_AMT); //+ (item.OFD.oF_AMT * item.OFD.vat_per) / 100;
                o.stud_vat_calc_amt = parseFloat((item.OFD.oF_AMT * item.OFD.vat_per) / 100).toFixed(2);
                o.studConcessionAMT = item.studConcessionAMT;
                o.studFeeCategoryCode = item.OFD.oF_Fee_cat;
                o.studFeeCode = item.OFD.oF_Fee_code;
                o.bgcolor = '#f0ad4e'
                o.master = true;
                o.disAmt = 0;
                o.disNumber = '';
                o.disApplied = false;
                o.total_paying = parseFloat(o.stud_vat_calc_amt) + parseFloat(item.OFD.oF_AMT);
                o.studFeetype = 'O';

                //item.feeDetails.push(o);

                var ob = {};
                ob.ref = item.$$hashKey;
                ob.pno = new Date().getMonth() + 1;
                ob.enroll = item.studDEnroll;
                ob.studCurr = item.studCurr;
                ob.studAcademic = item.studAcademic;
                ob.studGrade = item.studGrade;
                ob.studSection = item.studSection;
                ob.FPamtORG = parseFloat(item.OFD.oF_AMT) + (item.OFD.oF_AMT * item.OFD.vat_per) / 100;
                ob.feeNumber = null;
                ob.ID = $scope.maxLength++;
                ob.FD = item.OFD.oF_Desc;
                ob.flag = false;
                ob.mode = {};
                ob.mode = $scope.caPM;
                ob.chqNo = "NA";
                ob.chqBank = "NA";
                ob.chqDate = null;
                ob.trxID = "NA";
                ob.isSplitted = false;
                ob.sr = $scope.SelectedFees.length + 1;
                ob.srNo = $scope.SelectedFees.length + 1;
                ob.ExpAmt = "0";
                ob.FPamt = parseFloat(item.OFD.oF_AMT) + (item.OFD.oF_AMT * item.OFD.vat_per) / 100;
                ob.FPamtINT = parseFloat(item.OFD.oF_AMT) + (item.OFD.oF_AMT * item.OFD.vat_per) / 100;
                ob.stud_vat_calc_amt = parseFloat((item.OFD.oF_AMT * item.OFD.vat_per) / 100).toFixed(2);
                ob.FCAmt = "0";
                ob.FeeCat = item.OFD.oF_Fee_cat;
                ob.FeeCode = item.OFD.oF_Fee_code;
                ob.disAmt = 0;
                ob.disNumber = null;


                var added = false;
                var l = item.feeDetails.length;
                for (var i = 0; i < l; i++) {
                    if (item.feeDetails[i].studFeeNumber == null && item.feeDetails[i].studEnroll == item.studDEnroll) {
                        if (item.feeDetails[i].studFeeCode == item.OFD.oF_Fee_code && item.feeDetails[i].studFeeCategoryCode == item.OFD.oF_Fee_cat) {
                            item.feeDetails[i].studBalancePayingAMT = parseFloat(item.feeDetails[i].studBalancePayingAMT) + parseFloat(item.OFD.oF_AMT);
                            item.feeDetails[i].studBalanceAMT = parseFloat(item.feeDetails[i].studBalanceAMT) + parseFloat(item.OFD.oF_AMT);
                            item.feeDetails[i].FPamt = parseFloat(item.feeDetails[i].FPamt) + parseFloat(item.OFD.oF_AMT);
                            item.feeDetails[i].stud_vat_calc_amt = (item.feeDetails[i].studBalancePayingAMT * item.OFD.vat_per) / 100;
                            item.feeDetails[i].total_paying = parseFloat(item.feeDetails[i].studBalancePayingAMT) + parseFloat(item.feeDetails[i].stud_vat_calc_amt);

                            added = true;
                            break;
                        }
                    }
                }

                for (var i = 0; i < $scope.SelectedFees.length; i++) {
                    if ($scope.SelectedFees[i].feeNumber == null && $scope.SelectedFees[i].enroll == item.studDEnroll) {
                        if ($scope.SelectedFees[i].FeeCode == item.OFD.oF_Fee_code && $scope.SelectedFees[i].FeeCat == item.OFD.oF_Fee_cat) {
                            $scope.SelectedFees[i].FPamt = parseFloat($scope.SelectedFees[i].FPamt) + parseFloat(item.OFD.oF_AMT);
                            $scope.SelectedFees[i].stud_vat_calc_amt = ($scope.SelectedFees[i].FPamt * item.OFD.vat_per) / 100;

                            break;
                        }
                    }
                }

                if (!added) {
                    item.feeDetails.push(o);
                    $scope.SelectedFees.push(ob);
                    $scope.SelectedFeesBack.push(ob);
                }
                item.OFD.oF_AMT = 0;
                showHideDiscounts(o, item);
                $scope.check();
                if ($scope.pd.length == 1) {
                    $scope.DefaultPM.FPamt = $scope.$$cre.totalFeeamut - $scope.$$cre.Damut;
                }
            }

            $scope.removeOtherFee = function (stdf, item) {
                var lst = stdf.feeDetails;
                if (item.studFeeNumber == null || item.studFeeNumber == undefined) {
                    lst.splice(lst.indexOf(item), 1);
                }

                var v = $scope.SelectedFees.length;

                for (var i = 0; i < v; i++) {
                    if ($scope.SelectedFees[i].feeNumber == null && $scope.SelectedFees[i].enroll == item.studEnroll) {
                        if ($scope.SelectedFees[i].FeeCode == item.studFeeCode && $scope.SelectedFees[i].FeeCat == item.studFeeCategoryCode) {
                            $scope.SelectedFees.splice(i, 1);
                            break;
                        }
                    }
                }

                $scope.check();
                showHideDiscounts(item, stdf);
                if ($scope.pd.length == 1) {
                    $scope.DefaultPM.FPamt = $scope.$$cre.totalFeeamut - $scope.$$cre.Damut;;
                }

            }

            function rd(item) {
                var el = getStdList(item);
                var v = function (item, el) {
                    var dl = el.data.length;
                    for (var j = 0; j < dl; j++) {
                        el.data[j].disApplied = false;
                        el.data[j].disAmt = 0;
                        el.data[j].disNumber = '';
                    }
                }
                v(item, el);
                $scope.check();
            }

            $scope.UpdateDiscount = function () {
                var sl = $scope.appliedDD.length;
                for (var i = 0; i < sl; i++) {
                    var item = $scope.appliedDD[i];
                    var s = item.visible && item.status;//&& item.sims_concession_fee_code == '000';
                    if (s) {
                        var el = getStdList(item);
                        var v = function (item, el) {
                            var dl = el.data.length;
                            if (item.sims_concession_discount_type == 'F') {
                                var amt = parseInt(parseFloat(item.sims_concession_discount_value) / dl);
                                var rem = parseFloat(item.sims_concession_discount_value) % dl;
                                for (var j = 0; j < dl; j++) {
                                    if (el.data[j].disApplied == false) {
                                        el.data[j].disApplied = true;
                                        el.data[j].disAmt = amt + rem;
                                        rem = 0;
                                        el.data[j].disNumber = item.sims_concession_number + ',';
                                    }
                                    else {
                                        el.data[j].disAmt = el.data[j].disAmt + amt;
                                        el.data[j].disNumber = el.data[j].disNumber + item.sims_concession_number + ',';
                                    }
                                }
                            }
                            else if (item.sims_concession_discount_type == 'P') {
                                //var totval = el.sum();
                                //var samt = (parseFloat(totval) / 100) * parseFloat(item.sims_concession_discount_value);
                                //var amt = parseInt(samt) / dl;
                                //var rem = parseFloat(samt) % dl;
                                var samt = 0;
                                for (var j = 0; j < dl; j++) {
                                    samt = (parseFloat(el.data[j].FPamt) / 100) * parseFloat(item.sims_concession_discount_value);
                                    if (el.data[j].disApplied == false) {
                                        el.data[j].disApplied = true;
                                        samt = (parseFloat(el.data[j].FPamt) / 100) * parseFloat(item.sims_concession_discount_value);
                                        el.data[j].disAmt = samt;
                                        el.data[j].disNumber = item.sims_concession_number + ',';
                                    }
                                    else {
                                        el.data[j].disAmt = samt;
                                        el.data[j].disNumber = el.data[j].disNumber + item.sims_concession_number + ',';
                                    }
                                }
                            }
                        }
                        v(item, el);
                    }
                }
                $scope.check();
            }

            function getStdList(rn) {
                var v = {
                    data: [],
                    sum: function () {
                        var s = 0;
                        for (var i = 0; i < this.data.length; i++) {
                            s = s + parseFloat(this.data[i].FPamt);
                        }
                        return s;
                    },

                };
                if (rn.sims_concession_fee_code == '000') {
                    for (var i = 0; i < $scope.SelectedFees.length; i++) {
                        if ($scope.SelectedFees[i].enroll == rn.enrollNumber && $scope.dis.sDcode[$scope.SelectedFees[i].enroll].includes($scope.SelectedFees[i].FeeCode) == false) { // && $scope.SelectedFees[i].FeeCode == rn.sims_concession_fee_code) {
                            $scope.SelectedFees[i].disAmt = 0;
                            $scope.SelectedFees[i].disNumber = '';
                            $scope.SelectedFees[i].disApplied = false;
                            v.data.push($scope.SelectedFees[i]);
                        }
                    }
                    return v;
                }
                else {
                    for (var i = 0; i < $scope.SelectedFees.length; i++) {
                        if ($scope.SelectedFees[i].enroll == rn.enrollNumber && $scope.SelectedFees[i].FeeCode == rn.sims_concession_fee_code) {
                            $scope.SelectedFees[i].disAmt = 0;
                            $scope.SelectedFees[i].disNumber = '';
                            $scope.SelectedFees[i].disApplied = false;
                            v.data.push($scope.SelectedFees[i]);
                        }
                    }
                    return v;
                }
            }

            $scope.GOBack = function () {
                $state.go('main.SFRef', { 'sel': $scope.$$hist });
            }

            $scope.setActive = function (index, item) {
                debugger;
                $scope.currentClass = index;
                $scope.studName = item.studDName;
                $scope.studGradeName = item.studGradeName;
                $scope.studSectionName = item.studSectionName;
                $scope.studPAName = item.payingAgentName;
                $scope.studGrade = item.studGrade;
                $scope.studAcademicYear = item.studAcademic;
                $scope.studCurr = item.studCurr;

                //Incase of error need to uncomment
                //$http.get(ENV.apiUrl + "api/Fee/StudentFee_New/GetBanksWithCompany?cur=" + $scope.studCurr + "&ayear=" + $scope.studAcademicYear + "&grade=" + $scope.studGrade).then(function (res) {
                //    $scope.BankDet = res.data;
                //});
            }


            function checkClicked() {
                $scope.clicked = !$scope.SelectedFees.length > 0;
            }

            //$http.get(ENV.apiUrl + "api/Fee/StudentFee_New/GetBanks").then(function (res) {
            //    $scope.BankDet = res.data;
            //});


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                //format: 'yyyy-mm-dd'
                format: 'dd-mm-yyyy'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            function drag_start(event) {

                // Only allow dragging when it's from the 'title bar' area. Only need to test Y position of cursor.
                var bound = document.getElementById("pop-over");
                var box = bound.getBoundingClientRect();
                if (event.clientY > box.top + 22) {
                    return false;
                }

                // Grab all computed styles of the dragged object
                var style = window.getComputedStyle(event.target, null);
                // dataTransfer sets data that is being dragged. In this case, the current X and Y values (ex. "1257,104")
                event.dataTransfer.setData("text",
                (parseInt(style.getPropertyValue("left"), 10) - event.clientX) + ',' + (parseInt(style.getPropertyValue("top"), 10) - event.clientY));
            }

            function drag_over(event) {
                event.preventDefault();
                return false;
            }

            function drop(event) {
                // Set array of x and y values from the transfer data
                var offset = event.dataTransfer.getData("text").split(',');
                var dm = document.getElementById('pop-over');
                dm.style.left = ((event.clientX + parseInt(offset[0], 10)) * 100) / window.innerWidth + "%";
                dm.style.top = (event.clientY + parseInt(offset[1], 10)) + 'px';
                event.preventDefault();
                return false;
            }

            var dm = document.getElementById('pop-over');
            dm.addEventListener('dragstart', drag_start, false);
            document.body.addEventListener('dragover', drag_over, false);
            document.body.addEventListener('drop', drop, false);

            // Enable selection of text within content area of draggable div
            var editable = document.getElementById("pop-content");
            //var editable = document.getElementById("pop-over");
            editable.addEventListener('mousedown', doit, false);
            editable.addEventListener('mouseup', undoit, false);

            function doit() {
                document.getElementById("pop-over").setAttribute("draggable", "false");
            }

            function undoit() {
                document.getElementById("pop-over").setAttribute("draggable", "true");
            }


            $scope.student_name = '';

            //$scope.onFetchPFRData = function () {

            //    $scope.student_name = $scope.IP.StudEnroll + '-' + $scope.studName;
            //    $scope.summery = { dd_fee_amount_final: 0, dd_fee_amount_discounted: 0, doc_status_code: 3 };

            //    $http.get(ENV.apiUrl + "api/Fee/SFS/GetPreviousReceipts?ay=" + $scope.IP.StudAcademic + "&enroll=" + $scope.IP.StudEnroll).then(function (feesReceiptData) {
            //        $scope.feesReceiptData = feesReceiptData.data[0];
            //        $scope.feesDisReceiptData = feesReceiptData.data[1];

            //        angular.forEach($scope.feesDisReceiptData, function (value, key) {
            //            value['sims_icon'] = 'fa fa-minus-circle';
            //        });
            //        $scope.doc_nos = [];

            //        angular.forEach($scope.feesReceiptData, function (value, key) {
            //            var flag = false;

            //            ////angular.forEach($scope.doc_nos, function (value1, key1) {
            //            ////    if (value.doc_no == value1.doc_no)
            //            ////        flag = true;
            //            ////});


            //            $scope.summery.dd_fee_amount_final = parseFloat($scope.summery.dd_fee_amount_final) + parseFloat(value.dd_fee_amount_final);
            //            $scope.summery.dd_fee_amount_discounted = parseFloat($scope.summery.dd_fee_amount_discounted) + parseFloat(value.dd_fee_amount_discounted);
            //        });
            //        $scope.feesReceiptData.push($scope.summery);
            //        if ($scope.feesReceiptData.length > 1) {
            //            $('#MyModal').modal({ backdrop: 'static', keyboard: false });
            //        }
            //        else {
            //            swal({ text: 'No Previous Receipts Found.', imageUrl: "assets/img/close.png", showCloseButton: true, width: 450, });

            //        }
            //    });
            //}
            $scope.onFetchPFRData = function () {
                debugger
                $scope.student_name = $scope.IP.StudEnroll + '-' + $scope.studName;
                $scope.summery = { dd_fee_amount_final: 0, dd_fee_amount_discounted: 0, doc_status_code: 3 };

                //   $http.get(ENV.apiUrl + "api/Fee/SFS/GetPreviousReceipts_NEW?ay=" + info.std_fee_academic_year + "&enroll=" + info.std_fee_enroll_number).then(function (feesReceiptData) {

                $http.get(ENV.apiUrl + "api/Fee/SFS/GetPreviousReceipts_NEW?ay=" + $scope.IP.StudAcademic + "&enroll=" + $scope.IP.StudEnroll).then(function (feesReceiptData) {
                    $scope.feesReceiptData = feesReceiptData.data[0];

                    $scope.small_table = true;
                    $scope.paid_amt = 0;
                    $scope.vat_amt = 0;
                    $scope.discounted_amt = 0;
                    $scope.total_amt = 0;
                    //$scope.expected_amt = 0;
                    //$scope.Fee_amt = 0;

                    for (var i = 0; i < $scope.feesReceiptData.length; i++) {

                        $scope.paid_amt = parseFloat($scope.paid_amt) + parseFloat($scope.feesReceiptData[i].dd_fee_amount);
                        $scope.vat_amt = parseFloat($scope.vat_amt) + parseFloat($scope.feesReceiptData[i].dd_other_charge_amount);

                        $scope.discounted_amt = parseFloat($scope.discounted_amt) + parseFloat($scope.feesReceiptData[i].dd_fee_amount_discounted);
                        $scope.total_amt = parseFloat($scope.total_amt) + parseFloat($scope.feesReceiptData[i].dd_fee_amount_final);



                        /* $scope.expected_amt = parseFloat($scope.expected_amt) + parseFloat($scope.feesReceiptData[i].dd_fee_amount_final);
                         $scope.Fee_amt = parseFloat($scope.Fee_amt) + parseFloat($scope.feesReceiptData[i].dd_fee_amount_final);*/
                    }
                    $('#MyModal').modal({ backdrop: 'static', keyboard: false });

                });
            }

            $scope.expand = function (j, $event) {
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none"
                }
            }


            /* Total Fee Due Code */

            $http.get(ENV.apiUrl + "api/Fee/StudentFee_New/getFeeTerm?acdemicYear=" + $scope.IP.StudAcademic).then(function (getFeeTerm) {
                $scope.termData = getFeeTerm.data;
            });

            var Duebtn = ['aji', 'ajb', 'zps'];
            $scope.showDueBtn = false;
            $scope.ajiMonth = false;

            if (Duebtn.includes($http.defaults.headers.common['schoolId'])) {
                $scope.showDueBtn = true;
            }
            if ($http.defaults.headers.common['schoolId'] == 'aji') {
                $scope.ajiMonth = true;
            }

            var option = '';
            $scope.showDueDropdonn = function (option) {
                option = option;
                if (option == 'M') {
                    $scope.showDDM = true;
                    $scope.showDDT = false;
                }
                else if (option == 'T') {
                    $scope.showDDT = true;
                    $scope.showDDM = false;
                }
                else {
                    $scope.showDDM = false;
                    $scope.showDDT = false;
                    $scope.showDueData('Y');
                }
            }

            $scope.onFetchDueData = function () {
                $scope.showDDM = false;
                $scope.showDDT = false;
                $scope.temp = {};
                $scope.temp.yearly = "3";
                $scope.temp.yearlycheked = true;
                $scope.showDueData('Y');
                $('#DueModal').modal({ backdrop: 'static', keyboard: false });
            }

            $scope.showDueData = function (option) {
                $scope.showDueTable = false;

                $http.get(ENV.apiUrl + "api/Fee/StudentFee_New/getStudent_DueFees?cc=" + $scope.IP.StudCurr + "&ay=" + $scope.IP.StudAcademic + "&search_stud=" + $scope.IP.StudEnroll + '&option=' + option + '&month_no=' + $scope.temp.monthNo + '&term_code=' + $scope.temp.termCode).then(function (feesData) {
                    $scope.feesData = feesData.data;
                    if ($scope.feesData.length > 0) {
                        $scope.showDueTable = true;
                        $scope.ExpectedAmount = 0;
                        $scope.paidAmount = 0;
                        $scope.balanceAmount = 0;
                        $scope.totalArray = [];
                        $scope.feeTypeArray = [];

                        $scope.array_key = [];
                        $scope.array_key[0] = [];
                        $scope.array_key[1] = [];
                        $scope.array_key[2] = [];
                        $scope.array_key[3] = [];

                        angular.forEach($scope.feesData, function (value, key) {
                            $scope.ExpectedAmount = parseFloat($scope.ExpectedAmount) + parseFloat(value.std_fee_amount);
                            $scope.paidAmount = parseFloat($scope.paidAmount) + parseFloat(value.std_TotalPaid);
                            $scope.balanceAmount = parseFloat($scope.balanceAmount) + parseFloat(value.std_BalanceFee);

                            if (!$scope.feeTypeArray.includes(value.sims_fee_code_description)) {
                                $scope.feeTypeArray.push(value.sims_fee_code_description);
                            }
                        });

                        angular.forEach($scope.feesData[0], function (value, key) {
                            $scope.array_key[0].push(key);
                        });

                        angular.forEach($scope.feeTypeArray, function (value, key) {
                            $scope.array_key[1].push(0);
                            $scope.array_key[2].push(0);
                            $scope.array_key[3].push(0);
                        });

                        angular.forEach($scope.feesData, function (value, key) {
                            angular.forEach($scope.feeTypeArray, function (value2, key2) {
                                if (value2 == value.sims_fee_code_description) {
                                    $scope.array_key[1][key2] += parseFloat(value.std_fee_amount);
                                    $scope.array_key[2][key2] += parseFloat(value.std_TotalPaid);
                                    $scope.array_key[3][key2] += parseFloat(value.std_BalanceFee);
                                }
                            });
                        });

                        angular.forEach($scope.feeTypeArray, function (val, key) {
                            var obj = {
                                feeName: val,
                                expectedAmount: $scope.array_key[1][key],
                                paidamount: $scope.array_key[2][key],
                                balanceAmount: $scope.array_key[3][key]
                            }
                            $scope.totalArray.push(obj);
                        });

                        setTimeout(function () {
                            $("#fixTable").tableHeadFixer({ 'top': 1 });
                        }, 1000);
                    }
                    else {
                        swal({ text: 'Fee Due Not Found', imageUrl: "assets/img/close.png", showCloseButton: true, width: 450, });
                    }

                });
            }

            $http.get(ENV.apiUrl + "api/StudentFee/getDecimalPlaces").then(function (getDecimal) {
                debugger;
                $scope.decimal_degit = getDecimal.data;
                $scope.decimal_dsc = $scope.decimal_degit[0].comp_curcy_dec;
            });

            $http.get(ENV.apiUrl + "api/Fee/SFS/GetReceiptDateParam").then(function (res) {

                $scope.ReceiptDate = res.data;
                $scope.ReceiptDateData = $scope.ReceiptDate[0].sims_appl_parameter;

                if ($scope.ReceiptDateData == 'E') {
                    $('#from_date').data('kendoDatePicker').enable(true);

                } else {

                    $('#from_date').data('kendoDatePicker').enable(false);
                }

            });
            /*-- Total Fee Due Code ---*/

        }]);

})();