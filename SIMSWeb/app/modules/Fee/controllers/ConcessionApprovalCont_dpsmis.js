﻿
(function () {
    'use strict';

    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('ConcessionApprovalCont_dpsmis',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.table = false;
            $scope.pagesize = '10';
            $scope.searchtable1 = true;
            $scope.pager = true;
            $scope.temp1 = {};
            $(function () {
                $('#cmb_sections').multipleSelect({ width: '100%' });
            });
            var user = $rootScope.globals.currentUser.username;
            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;
            });


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCuriculum) {
                $scope.getCuriculum = getCuriculum.data;
                $scope.temp1['sims_cur_code'] = $scope.getCuriculum[0].sims_cur_code;
                $scope.getAcademicyear($scope.getCuriculum[0].sims_cur_code);
            });

            $scope.getAcademicyear = function (str) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (getAcademicYear) {
                    $scope.getAcademicYear = getAcademicYear.data;
                    $scope.temp1['sims_fee_academic_year'] = $scope.getAcademicYear[0].sims_academic_year;

                    $scope.getConcessionTypes();

                });
            }

            $scope.Getterm1 = function () {

                $scope.temp1['termStartDate'] = '';
                $scope.temp1['termEndDate'] = '';
                $scope.temp1['sims_term_code'] = '';

                $http.get(ENV.apiUrl + "api/Gradebook/getTermforGradebook?cur_code=" + $scope.temp1.sims_cur_code + "&academic_year=" + $scope.temp1.sims_fee_academic_year).then(function (Getsims550_TermsAcademicYear) {
                    $scope.TermsAcademicYear = Getsims550_TermsAcademicYear.data;
                });
            }

            $scope.termdatesforconcession = function () {

                if ($scope.temp1.sims_term_code != '' && $scope.temp1.sims_term_code != undefined) {
                    $('#from_date').data('kendoDatePicker').enable(false);
                    $('#to_date').data('kendoDatePicker').enable(false);
                } else {
                    $('#from_date').data('kendoDatePicker').enable(true);
                    $('#to_date').data('kendoDatePicker').enable(true);
                    $scope.temp1['termStartDate'] = '';
                    $scope.temp1['termEndDate'] = '';
                    $scope.temp1['sims_term_code'] = '';
                }

                var data = ({
                    sims_cur_code: $scope.temp1.sims_cur_code,
                    sims_academic_year: $scope.temp1.sims_fee_academic_year,
                    sims_gb_term_name: $scope.temp1.sims_term_code,

                });

                $http.post(ENV.apiUrl + "api/Gradebook/All_GetTerms", data).then(function (termdates) {
                    $scope.term_dates = termdates.data;

                    var day = $scope.term_dates[0].termStartDate.split('-')[2];
                    var month = $scope.term_dates[0].termStartDate.split('-')[1];
                    var year = $scope.term_dates[0].termStartDate.split('-')[0];

                    var day1 = $scope.term_dates[0].termEndDate.split('-')[2];
                    var month1 = $scope.term_dates[0].termEndDate.split('-')[1];
                    var year1 = $scope.term_dates[0].termEndDate.split('-')[0];

                    $scope.temp1.termStartDate = day + '-' + month + '-' + year;
                    $scope.temp1.termEndDate = day1 + '-' + month1 + '-' + year1;

                });

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#Table3").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.btn_StudentView_Click = function () {
                $scope.busy = true;
                $scope.searchtable1 = true;
                $scope.concession_number = '';
                for (var i = 0; i < $scope.temp1.sims_concession_number.length; i++) {
                    $scope.concession_number = $scope.concession_number + $scope.temp1.sims_concession_number[i] + ",";
                }

                $http.post(ENV.apiUrl + "api/concession/allsims013_Students?concession_number=" + $scope.concession_number, $scope.temp1).then(function (Get_sims013_Students) {
                    $scope.Get_sims013_Students = Get_sims013_Students.data;
                    $scope.filteredTodos = [];
                    if ($scope.Get_sims013_Students.length <= 0) {
                        $scope.searchtable = false;
                        $scope.busy = false;
                        $scope.searchtable1 = false;
                        //swal({
                        //    text: 'No concessions awaiting for approval',
                        //    width: 300,
                        //    height: 300
                        //});
                    }
                    else {
                        $scope.busy = false;
                        $scope.totalItems = $scope.Get_sims013_Students.length;
                        $scope.todos = $scope.Get_sims013_Students;
                        $scope.makeTodos();
                        $scope.searchtable = true;
                    }
                })
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.getConcessionTypes = function () {

                $http.get(ENV.apiUrl + "api/concession/getConcession_type?sims_fee_academic_year=" + $scope.temp1.sims_fee_academic_year).then(function (Get_Concession_type) {
                    $scope.Get_Concession_type = Get_Concession_type.data;

                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);


                    $scope.Getterm1();
                });
            }

            $scope.btn_StudentSearch_Click = function () {

                $scope.searchtable = false;
                $scope.temp = '';

                $('#myModal').modal('show');
            }

            $scope.edit = function (str) {
                $scope.display = false;
                $scope.table = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.opration = true;
                $scope.edt = str;


                if (main.checked == true) {

                    for (var i = 0; i < $scope.obj.length; i++) {
                        var t = $scope.obj[i].sims_student_enroll_number; console.log(t);
                        var v = document.getElementById(t);
                        v.checked = false;

                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.New = function () {
                $scope.display = false;
                $scope.table = false;
                $scope.opration = true;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
            }

            var check = '';

            $scope.btn_StudentSave_Click = function () {
                var allfee = [];
                var flag = false;
                for (var j = 0; j < $scope.filteredTodos.length ; j++) {

                    if ($scope.filteredTodos[j].sims_status == true) {

                        if ($scope.temp1.termStartDate != '' && $scope.temp1.termStartDate != undefined && $scope.temp1.termEndDate != '' && $scope.temp1.termEndDate != undefined) {
                            $scope.filteredTodos[j].sims_concession_from_date = $scope.temp1.termStartDate;
                            $scope.filteredTodos[j].sims_concession_to_date = $scope.temp1.termEndDate;
                            $scope.sims_concession_term = $scope.temp1.sims_term_code;
                        }

                        $scope.filteredTodos[j]['sims_created_by'] = user;
                        allfee.push($scope.filteredTodos[j]);

                        flag = true;
                    }
                }

                if (flag == true) {
                    swal({
                        text: 'Are you sure to approve the concession?',
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 400,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/concession/CUD_sims013_Students", allfee).then(function (msg) {
                                $scope.msg1 = msg.data;

                                if ($scope.msg1 == true) {
                                    allfee = [];
                                    swal({
                                        text: 'Concession Approved Successfully',
                                        imageUrl: "assets/img/check.png",
                                        width: 300,
                                        height: 300,
                                        imageUrl: "assets/img/check.png"
                                    });
                                    $scope.btn_StudentView_Click();
                                }
                                else {
                                    swal({
                                        text: 'Concession Not Approved',
                                        imageUrl: "assets/img/close.png",
                                        width: 300,
                                        height: 300,
                                        imageUrl: "assets/img/close.png"
                                    });
                                }
                            })
                        }
                        else {

                            check = document.getElementById("mainchk");
                            check.checked = false;
                            $scope.MultipleSelectrecords();
                        }
                    })
                }
                else {
                    swal({
                        text: 'Select Record To Apply Concession',
                        imageUrl: "assets/img/check.png",
                        width: 320,
                        showCloseButton: true
                    });
                }
            }

            $scope.MultipleSelectrecords = function () {

                check = document.getElementById("mainchk");
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if (check.checked == true) {
                        $scope.filteredTodos[i].sims_status = true;
                        $('tr').addClass("row_selected");
                    }
                    else {
                        $scope.filteredTodos[i].sims_status = false;
                        $('tr').removeClass("row_selected");
                    }
                }
            }

            $scope.checkAny = function (info) {

                //$("input[type='checkbox']").change(function (e) {
                //    if ($(this).is(":checked")) { //If the checkbox is checked
                //        $(this).closest('tr').addClass("row_selected");
                //        //Add class on checkbox checked
                //        $scope.color = '#edefef';
                //    } else {
                //        $(this).closest('tr').removeClass("row_selected");
                //        //Remove class on checkbox uncheck
                //        $scope.color = '#edefef';
                //    }
                //});

                var main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if (info.sims_status == true)
                    {
                        if (info.sims_enroll_number == $scope.filteredTodos[i].sims_enroll_number && info.sims_concession_number == $scope.filteredTodos[i].sims_concession_number)
                        {
                            $scope.filteredTodos[i].sims_status = true;
                            $scope.color = '#edefef';
                        }
                    }
                     else if (info.sims_enroll_number == $scope.filteredTodos[i].sims_enroll_number && info.sims_concession_number == $scope.filteredTodos[i].sims_concession_number) {
                         $scope.filteredTodos[i].sims_status = false;
                         $scope.color = '#fff';
                    }
                }

            }

            $scope.SearchSudent = function () {

                $scope.searchtable = false;
                $scope.busy = true;
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.temp)).then(function (Allstudent) {
                    $scope.student = Allstudent.data;
                    $scope.searchtable = true;
                    $scope.busy = false;

                });
            }

            var enroll = '';

            $scope.DataEnroll = function () {
                var sims_enroll_number = 'sims_enroll_number';
                var str = '';
                for (var i = 0; i < $scope.student.length; i++) {
                    var t = $scope.student[i].s_enroll_no;
                    var v = document.getElementById(t + i);
                    if (v.checked == true)
                        str = str + $scope.student[i].s_enroll_no + ',';
                }

                $scope.temp1[sims_enroll_number] = str;
            }

            $scope.size = function (str) {

                //if (str == "All") {

                //    $scope.filteredTodos = $scope.Get_sims013_Students;
                //    $scope.todos = $scope.Get_sims013_Students;
                //    $scope.totalItems = $scope.Get_sims013_Students.length;
                //    $scope.numPerPage = $scope.Get_sims013_Students.length;
                //    $scope.makeTodos();
                //}
                //else {
                //    $scope.pagesize = str;
                //    $scope.currentPage = 1;
                //    $scope.numPerPage = str;
                //    $scope.makeTodos();
                //}
                if (str == "All" || str == "all") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };
            $scope.searchText = '';
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Get_sims013_Students, $scope.searchText);
                $scope.totalItems = $scope.Get_sims013_Students.length;;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Get_sims013_Students;
                }

                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd-mm-yyyy'
            });

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };


        }])
})();

