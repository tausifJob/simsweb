﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SimsInvoiceEmailCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.table = false;
            $scope.pagesize = "10";

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/Fee/SFS/Getsims541_InvoiceMode").then(function (InvoiceMode) {
                $scope.InvoiceMode = InvoiceMode.data;
                $scope.edt['sims_invoce_mode_code'] = $scope.InvoiceMode[0].sims_invoce_mode_code;
                $scope.InvoiceMode_change();
            });

            $http.get(ENV.apiUrl + "api/Fee/SFS/Getsims541_InvoiceReportUrl").then(function (InvoiceReport) {
                $scope.InvoiceReport = InvoiceReport.data;
            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };  

            $scope.size = function (str) {
                
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }

                if (str == "All") {
                    $scope.currentPage = 1;
                    $scope.numPerPage = $scope.studlist.length;
                    $scope.makeTodos();

                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
            }

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear($scope.edt.sims_cur_code);
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                    $scope.getGrade(curCode, $scope.temp.sims_academic_year);
                    $scope.InvoiceMode_change();
                });
            }

            $scope.InvoiceMode_change = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/Getsims541_FrequencyValue?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&freq_flag=" + $scope.edt.sims_invoce_mode_code).then(function (FeeFreqValue) {
                    $scope.FeeFreqValue = FeeFreqValue.data;
                    $scope.edt['sims_invoce_frq_name_value'] = $scope.FeeFreqValue[0].sims_invoce_frq_name_value;
                });

            }


            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
            }

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }

            $scope.Reset = function () {
                $scope.edt = {};
                $scope.temp.sims_academic_year = "";
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                    $scope.curriculum = AllCurr.data;
                    $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                    $scope.getAccYear($scope.edt.sims_cur_code);
                });
                $scope.table1 = false;
            }

            $scope.Show_Data = function (str, str1, start_date, end_date) {
                if (str != undefined && str1 != undefined) {
                    $http.get(ENV.apiUrl + "api/StudentReport/getStudentEmailInvoice?cur_name=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grd_code=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&user_code=" + $scope.edt.sims_student_enroll_number + "&start_date=" + $scope.edt.sims_from_date + "&end_date=" + $scope.edt.sims_to_date + "&term_code=" + $scope.edt.sims_invoce_frq_name_value).then(function (res) {
                        $scope.studlist = res.data;
                        $scope.totalItems = $scope.studlist.length;
                        $scope.todos = $scope.studlist;
                        $scope.makeTodos();
                        if ($scope.studlist.length == 0) {
                            swal({  text: "Please Try Again Record Not Found...", imageUrl: "assets/img/close.png",showCloseButton: true, width: 450, height: 200 });
                            $scope.table1 = false;
                        }
                        else {
                            $scope.table1 = true;
                        }
                    })

                }
                else {
                    swal({  text: "Please Select Curriculum & Academic Year Then Try Again...",imageUrl: "assets/img/close.png", showCloseButton: true, width: 450, height: 200 });
                }
            }

           
            $scope.View = function (str) {
                debugger
                
                var data = {
                    location: $scope.InvoiceReport,
                    parameter: {
                        sims_invoce_mode: str.sims_invoce_mode,
                        sims_period_code: str.sims_period_code,
                        enroll:str.sims_enroll_number,
                    },
                    state: 'main.SimIEM',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
            }

            $scope.checkSchool = function (str) {
                if ($http.defaults.headers.common['schoolId'] == 'csdportal' || $http.defaults.headers.common['schoolId'] == 'sis') {
                    $scope.SendEmailForCSD(str);
                } else {
                    $scope.SendEmail(str);
                }
            }

            $scope.SendEmail = function (gtr) {
                //$http.get(ENV.apiUrl + "api/StudentReport/sendemailinvoicenew?cur_name1=" + $scope.edt.sims_cur_code + "&academic_year1=" + $scope.temp.sims_academic_year + "&grd_code1=" + gtr.sims_grade_code + "&sec_code1=" + gtr.sims_section_code + "&enroll1=" + gtr.sims_enroll_number + "&invoice_no1=" + gtr.in_no + "&invoice_mode1=" + gtr.sims_invoce_mode + "&invoice_period1=" + gtr.sims_period_code + "&emailids1=" + gtr.father_email).then(function (res) {
                //    swal({ title: "Alert", text: "Email Sent Successfully...", showCloseButton: true, width: 380, });
                //})
                var datasend = [];
                var data =
                    {
                        sims_cur_code:$scope.edt.sims_cur_code,
                        sims_academic_year:$scope.temp.sims_academic_year,
                        sims_grade_code:gtr.sims_grade_code,
                        sims_section_code:gtr.sims_section_code,
                        sims_enroll_number:gtr.sims_enroll_number,
                        in_no:gtr.in_no,
                        sims_invoce_mode:gtr.sims_invoce_mode,
                        sims_period_code:gtr.sims_period_code,
                        father_email: gtr.father_email,
                        report_location: $scope.InvoiceReport,
                }
                datasend.push(data);
                $http.post(ENV.apiUrl + "api/StudentReport/CUDsendemailinvoicenew", datasend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 !=undefined) {
                        swal({ text: 'Record  Inserted Successfully', width: 380, imageUrl: "assets/img/check.png",showCloseButton: true });
                    }
                    else if ($scope.msg1 == undefined || $scope.msg1 == '' || $scope.msg1 == null) {
                        swal({ text: 'Record Not Inserted', width: 380,imageUrl: "assets/img/close.png", showCloseButton: true });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                })

            }

            $scope.SendEmailForCSD = function (gtr) {
                //$http.get(ENV.apiUrl + "api/StudentReport/sendemailinvoicenew?cur_name1=" + $scope.edt.sims_cur_code + "&academic_year1=" + $scope.temp.sims_academic_year + "&grd_code1=" + gtr.sims_grade_code + "&sec_code1=" + gtr.sims_section_code + "&enroll1=" + gtr.sims_enroll_number + "&invoice_no1=" + gtr.in_no + "&invoice_mode1=" + gtr.sims_invoce_mode + "&invoice_period1=" + gtr.sims_period_code + "&emailids1=" + gtr.father_email).then(function (res) {
                //    swal({ title: "Alert", text: "Email Sent Successfully...", showCloseButton: true, width: 380, });
                //})
                debugger
                var datasend = [];
                var data =
                    {
                        sims_cur_code: $scope.edt.sims_cur_code,
                        sims_academic_year: $scope.temp.sims_academic_year,
                        sims_grade_code: gtr.sims_grade_code,
                        sims_section_code: gtr.sims_section_code,
                        sims_enroll_number: gtr.sims_enroll_number,
                        in_no: gtr.in_no,
                        term_code: gtr.sims_period_code,
                        sims_invoce_mode: gtr.sims_invoce_mode,
                        sims_period_code: gtr.sims_period_code,
                        father_email: gtr.father_email,
                        report_location: $scope.InvoiceReport,
                    }
                datasend.push(data);
                $http.post(ENV.apiUrl + "api/StudentReport/CUDsendemailinvoicenew", datasend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 != undefined) {
                        swal({ text: 'Record  Inserted Successfully', width: 380, imageUrl: "assets/img/check.png", showCloseButton: true });
                    }
                    else if ($scope.msg1 == undefined || $scope.msg1 == '' || $scope.msg1 == null) {
                        swal({ text: 'Record Not Inserted', width: 380, imageUrl: "assets/img/close.png", showCloseButton: true });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                })

            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.studlist, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.studlist;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.in_no == toSearch) ? true : false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $scope.Check_Date = function (fromdate, todate) {
                if (fromdate > todate) {
                    swal({  text: "Please Select Future Date...", imageUrl: "assets/img/notification-alert.png",showCloseButton: true, width: 450, height: 200 });
                    $scope.edt.sims_to_date = '';
                }
            }

        }])
})();