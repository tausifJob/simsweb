﻿(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [];

    var doc_no;
    var simsController = angular.module('sims.module.Fee');
    simsController.controller('FeeReceiptView_pearl',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = 'All';
            $scope.pager = true;
            $scope.pagggg = false;
            var user = $rootScope.globals.currentUser.username;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.export_btn = false;
            $scope.total_amt = 0;
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            $scope.busy = false;

            
            //$http.post(ENV.apiUrl + "http://api.mograsys.com/APIERP/api/StudentFee/GetReceipt").then(function (res) {
            //    debugger
            //    $scope.reportparameter = res.data;
            //    console.log($scope.reportparameter);


            //});


            $scope.size = function (str) {

                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.FeeReceiptData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy/mm/dd"
                // format: "dd/mm/yyyy"

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            //$scope.createdate = function (end_date, start_date, name) {
            //    var month1 = end_date.split("/")[0];
            //    var day1 = end_date.split("/")[1];
            //    var year1 = end_date.split("/")[2];
            //    var new_end_date = year1 + "/" + month1 + "/" + day1;
            //    //var new_end_date = day1 + "/" + month1 + "/" + year1;

            //    var year = start_date.split("/")[0];
            //    var month = start_date.split("/")[1];
            //    var day = start_date.split("/")[2];
            //    var new_start_date = year + "/" + month + "/" + day;
            //    // var new_start_date = day + "/" + month + "/" + year;

            //    if (new_end_date < new_start_date) {
            //        swal({ title: 'Please Select Next Date From ', width: 380, height: 100 });
            //        $scope.edt[name] = '';
            //    }
            //    else {
            //        $scope.edt[name] = new_end_date;
            //    }
            //}

            $scope.paginationview1 = document.getElementById("paginationview");
            debugger
            $scope.showdate = function (date, name) {
                debugger
                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //$scope.edt[name] = year + "/" + month + "/" + day;
                $scope.edt[name] = date;
                //var day = date.split("-")[0];
                //var month = date.split("-")[1];
                //var year = date.split("-")[2];
                //$scope.edt[name] = year + "-" + month + "-" + day;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.FeeReceiptData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.FeeReceiptData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked()

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.doc_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }


            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();




            //$scope.sdate = yyyy + '-' + mm + '-' + dd;
            //$scope.edate = yyyy + '-' + mm + '-' + dd;
            //$scope.edt = {
            //    from_date: yyyy + '-' + mm + '-' + dd,
            //    to_date: yyyy + '-' + mm + '-' + dd,
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.edt = {
                from_date: dd + '-' + mm + '-' + yyyy,
                to_date: dd + '-' + mm + '-' + yyyy,
            }




            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.onKeyPress = function ($event) {
                debugger;
                if ($event.keyCode == 13) {

                    $scope.Show($scope.edt.from_date, $scope.edt.to_date, $scope.edt.search);
                    // console.log("enetr");
                }
            };


            $scope.Show = function () {
                $scope.busy = true;
                $scope.FeeReceiptData = [];
                debugger
                if ($scope.edt.search == undefined || $scope.edt.search == "") {
                    $scope.edt.search = '';
                }
                $scope.table1 = true;
                $scope.pagggg = true;
                $scope.ImageView = false;
                $scope.export_btn = true;
                $scope.total_amt = 0;
                $http.get(ENV.apiUrl + "api/FeeReceipt/getAllRecordsFeeReceipt?from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date + "&search=" + $scope.edt.search).then(function (FeeReceipt_Data) {
                    $scope.FeeReceiptData = FeeReceipt_Data.data;
                    $scope.totalItems = $scope.FeeReceiptData.length;
                    $scope.todos = $scope.FeeReceiptData;
                    $scope.makeTodos();
                    console.log($scope.FeeReceiptData);
                    $scope.busy = false;
                    if (FeeReceipt_Data.data.length > 0) { $scope.size('All') }
                    else {
                        $scope.ImageView = true;
                        $scope.pager = false;
                    }

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        if ($scope.edt.search != undefined) {
                            if ($scope.edt.search != '') {
                                if ($scope.edt.search.length == $scope.filteredTodos[i].doc_no.length) {
                                    $scope.total_amt = 0
                                    $scope.total_amt = parseFloat($scope.total_amt) + parseFloat($scope.filteredTodos[i].grand_total);
                                }
                                else {
                                    $scope.total_amt = parseFloat($scope.total_amt) + parseFloat($scope.filteredTodos[i].grand_total);
                                }
                            }
                            else {
                                $scope.total_amt = parseFloat($scope.total_amt) + parseFloat($scope.filteredTodos[i].grand_total);
                            }
                        }
                        else {
                            $scope.total_amt = parseFloat($scope.total_amt) + parseFloat($scope.filteredTodos[i].grand_total);
                        }

                    }

                    console.log($scope.total_amt);
                });
                //}
            }

            $scope.Export = function () {
                var check = true;
                debugger;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " Fee Receipt.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            var blob = new Blob([document.getElementById('exporting_data').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " Fee Receipt" + ".xls");
                            //alasql('SELECT * INTO XLSX("Report.xlsx",{headers:true}) \ FROM HTML("#printdata",{headers:true,skipdisplaynone:true})');

                        }
                    });
                }
                else {
                    swal({ text: "Report Not Save",imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, })
                }

            }



            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.edt["from_date"] = '';
                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.edt = {
                            //from_date: yyyy + '-' + mm + '-' + dd,
                            //to_date: yyyy + '-' + mm + '-' + dd,
                            from_date: dd + '-' + mm + '-' + yyyy,
                             to_date: dd + '-' + mm + '-' + yyyy,
                        }

                    }
                });
            });

            $scope.Reset=function()
            {
                debugger
                $scope.edt.search = '';
                $scope.table1 = false;
            }


            $scope.Report = function (str) {
                $http.get(ENV.apiUrl + "api/StudentFee/GetReceipt").then(function (res) {
                    var rname = res.data;

                    var data = {
                        //location: 'Sims.SIMR51FeeRec',
                        location: rname,
                        parameter: {
                            fee_rec_no: str.doc_no,
                        },
                        state: 'main.Sim615',
                        ready: function () {
                            this.refreshReport();
                        },
                    }
                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                    $state.go('main.ReportCardParameter')
                });
            }

        }]
        )
})();