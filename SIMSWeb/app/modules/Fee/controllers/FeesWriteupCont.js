﻿/// <reference path="StudentFeeConcessionCont.js" />
(function () {
    'use strict';
    var main;
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('FeesWriteupCont',
        ['$scope', '$state', '$rootScope', '$timeout', '$stateParams', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, $stateParams, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            // Start
           
            
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.small_table = false;
            var user = $rootScope.globals.currentUser.username;
            var now = new Date();
            var month = (now.getMonth() + 1);
            var day = now.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            $scope.savebtn = false;
            $scope.sortBy = function (propertyName) {

                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker(
                {
                    todayBtn: true,
                    orientation: "top left",
                    autoclose: true,
                    todayHighlight: true,
                    //format: 'yyyy-mm-dd'
                    format: 'dd-mm-yyyy'
                });
            var op = $stateParams.sel;
            if (op != undefined) {
                try {
                    $scope.edt = {};
                    $scope.edt.sims_cur_code = op.cur;
                    $scope.edt.sims_academic_year = op.ac;
                    $scope.edt.sims_grade_code = op.gr;
                    $scope.edt.sims_section_code = op.sc;
                    $scope.edt.search_txt = op.st;
                    //_onFetchData();
                } catch (e) {

                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
            };


            $http.get(ENV.apiUrl + "api/StudentFee/getDecimalPlaces").then(function (getDecimal) {
                debugger;
                $scope.decimal_degit = getDecimal.data;
                $scope.decimal_dsc = $scope.decimal_degit[0].comp_curcy_dec;
            });


            $scope.search = function () {
                debugger;
                $scope.todos = $scope.searched($scope.feesData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.feesData;
                }
                $scope.makeTodos();

                var totalAmt = 0;
                var totaPaid = 0;
                var totbal = 0;
                debugger;
                for (var i = 0; i < $scope.todos.length; i++) {
                    totalAmt = totalAmt + parseFloat($scope.todos[i].std_fee_amount);
                    totaPaid = totaPaid + parseFloat($scope.todos[i].std_TotalPaid);
                    totbal = totbal + parseFloat($scope.todos[i].std_BalanceFee);
                }
                $scope.toamt = totalAmt;
                $scope.topaid = totaPaid;
                $scope.tobal = totbal;

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.std_fee_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.std_fee_student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    || item.std_fee_Class.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.std_fee_parent_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    || item.parent_mobile_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.std_fee_mother_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    ) ? true : false;
            }

           

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '', sims_search_code: '' };
           
            $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllCurName").then(function (CurData) {
                debugger
                $scope.CurData = CurData.data;
                $scope.edt['sims_cur_code'] = CurData.data[0].sims_cur_code;
                $scope.cur_code_change(CurData.data[0].sims_cur_code);

            });
            $http.get(ENV.apiUrl + "api/Fee/SFS/getSeachOptions").then(function (searchoptions) {
                $scope.searchoptions = searchoptions.data;

                for (var i = 0; i < $scope.searchoptions.length; i++) {
                    if ($scope.searchoptions[i].sims_search_code_status == 'Y') {
                        $scope.edt['sims_search_code'] = $scope.searchoptions[i].sims_search_code;
                    }
                }
            });



            $scope.currentYear_status = 'C';
            $scope.cur_code_change = function () {

                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + $scope.edt.sims_cur_code).then(function (AcademicYears) {
                    $scope.AcademicYears = AcademicYears.data;
                    for (var i = 0; i < $scope.AcademicYears.length; i++) {
                        if ($scope.AcademicYears[i].sims_academic_year_status == 'C' || $scope.AcademicYears[i].sims_academic_year_status == "Current") {
                            //   $scope.edt['sims_cur_code'] = cur_values;
                            $scope.edt['sims_academic_year'] = $scope.AcademicYears[i].sims_academic_year;
                        }
                    }
                    $scope.academic_year_change();
                });

            }
            $scope.academic_year_change = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year).then(function (Grades) {
                    $scope.Grades = Grades.data;
                });

            }



            $scope.grade_change = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllSections?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year + "&g_code=" + $scope.edt.sims_grade_code).then(function (sections) {
                    $scope.sections = sections.data;

                });

            }

            $scope.reset = function () {

                //$scope.edt = { 'sims_grade_code': '', 'sims_section_code': '', 'search_txt': '' };
                $scope.edt['sims_grade_code'] = '';
                $scope.edt['sims_section_code'] = '';
                $scope.edt['search_txt'] = '';
                $scope.feesData = '';
                $scope.todos = $scope.feesData;
                $scope.makeTodos();


            }

            /* PAGER FUNCTIONS*/

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10; $scope.currentPage_ind = 0;
            $scope.allSize = false;

           
             $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

             $scope.size = function (str) {
                 
                 debugger;
                 if (str == "All") {
                     $scope.totalItems = $scope.feesData.length;
                     $scope.currentPage = '1';
                     $scope.numPerPage = $scope.feesData.length;
                     $scope.filteredTodos = $scope.feesData;
                     $scope.pager = false;
                     $scope.makeTodos();
                 }
                 else {
                     $scope.totalItems = str;
                     $scope.pager = true;
                     $scope.pagesize = str;
                     $scope.currentPage = 1;
                     $scope.numPerPage = str;
                     $scope.makeTodos();
                 }
             }

             $scope.index = function (str) {
                 $scope.pageindex = str;
                 $scope.currentPage = str; $scope.makeTodos();
                 main.checked = false;
                 $scope.row1 = '';
             }

           /* $scope.size = function (str) {
                
                if (str == 'All') {
                    
                    $scope.totalItems = $scope.feesData.length;
                    $scope.todos = $scope.feesData;
                    $scope.filteredTodos = $scope.feesData;
                    $scope.numPerPage = $scope.feesData.length;
                    $scope.maxSize = $scope.feesData.length;
                    $scope.makeTodos();
                    $scope.allSize = true;
                }
                else {
                    
                    $scope.allSize = false;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage_ind = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }*/

            /* PAGER FUNCTIONS*/
            $scope.CheckAllChecked = function () {
               
             var  main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.feesData.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById(i + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.feesData.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById(i + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyone = function () {
               
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

               var main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }


            $scope.SaveFeesData = function () {
               
                $scope.datasend = [];
                $scope.receipt_date = day + '-' + month + '-' + now.getFullYear();
                for (var i = 0; i < $scope.feesData.length; i++) {
                    //var v = document.getElementById($scope.feesData[i].std_fee_enroll_number + i);
                    
                    if ($scope.feesData[i].sims_student_check == true) {
                        $scope.feesData[i].remark = $scope.feesData[i].remark;
                        $scope.feesData[i].receipt_date = $scope.receipt_date;
                        $scope.feesData[i].username = user;
                        $scope.datasend.push($scope.feesData[i]);
                    }

                }

                if ($scope.datasend.length > 0) {

                    $http.post(ENV.apiUrl + "api/FeesWriteup/SaveFeesData", $scope.datasend).then(function (res) {
                        debugger;
                        $scope.result = res.data;
                        if ($scope.result!='') {
                            swal({ text: 'Saved succesfully', imageUrl: "assets/img/check.png", width: 300, height: 250, showCloseButton: true });
                            $('#feetypemodal').modal('show');

                            var data = {
                                location: final_doc_url,
                                parameter: { fee_rec_no: $scope.result },
                                state: 'main.FeeWU'
                            }
                            window.localStorage["ReportDetails"] = JSON.stringify(data)
                            $state.go('main.ReportCardParameter')


                            try {
                                $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                                $scope.location = $scope.rpt.location;
                                $scope.parameter = $scope.rpt.parameter;
                                $scope.state = $scope.rpt.state;
                            }
                            catch (ex) {
                            }
                            var s;


                            s = "SimsReports." + $scope.location + ",SimsReports";

                            var url = window.location.href;
                            var domain = url.substring(0, url.indexOf(':'))
                            if ($http.defaults.headers.common['schoolId'] == 'sms') {
                                if (domain == 'https')
                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                                else
                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                            }
                            else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc') {
                                if (domain == 'https')
                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                                else
                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                            }

                            else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                                if (domain == 'https')
                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                                else
                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                            }
                            else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                                if (domain == 'https')
                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                                else
                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                            }

                            else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                                if (domain == 'https')
                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                                else
                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                            }

                            else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                                if (domain == 'https')
                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                                else
                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                            }
                            else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                                if (domain == 'https')
                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                                else
                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                            }

                            else {
                                if (domain == 'https')
                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                                else
                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                            }
                            console.log(service_url);


                            $scope.parameters = {}
                            debugger;
                            $("#reportViewer1")
                                           .telerik_ReportViewer({
                                               //serviceUrl: ENV.apiUrl + "api/reports/",
                                               serviceUrl: service_url,

                                               viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                               scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                               // Zoom in and out the report using the scale
                                               // 1.0 is equal to 100%, i.e. the original size of the report
                                               scale: 1.0,
                                               ready: function () {
                                                   //this.refreshReport();
                                               }

                                           });





                            var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                            reportViewer.reportSource({
                                report: s,
                                parameters: $scope.parameter,
                            });

                            //var rv = $("#reportViewer1").data("telerik_ReportViewer");
                            //rv.commands.print.exec();

                            setInterval(function () {

                                $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                                $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                                //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                                //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                                //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                                //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                                //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                                //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

                            }, 1000);


                            $timeout(function () {
                                $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                                $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                                //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                                //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                                //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                                //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                                //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                                //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


                            }, 100)

                        }
                        else {
                            swal({ text: 'Record Not Saved', imageUrl: "assets/img/close.png", width: 300, height: 250, showCloseButton: true });

                        }

                    });

                }
                else {
                    swal({ text: 'Select Record', imageUrl: "assets/img/close.png", width: 300, height: 250, showCloseButton: true });
                }
                $scope.filteredTodos = [];
            }


            function _onFetchData() {
                debugger;
                $scope.savebtn = true;
                if ($scope.edt.search_txt == undefined)
                    $scope.edt.search_txt = '';
                $scope.sel = { 'cur': $scope.edt.sims_cur_code, 'ac': $scope.edt.sims_academic_year, 'gr': $scope.edt.sims_grade_code, 'sc': $scope.edt.sims_section_code, 'st': $scope.edt.search_txt, 'search_flag': '3' }
                $http.get(ENV.apiUrl + "api/FeesWriteup/getAllStudent_FeesData?cc=" + $scope.edt.sims_cur_code + "&ay=" + $scope.edt.sims_academic_year + "&gc=" + $scope.edt.sims_grade_code + "&sc=" + $scope.edt.sims_section_code + "&search_stud=" + $scope.edt.search_txt + "&search_flag=" + $scope.edt.sims_search_code).then(function (feesData) {
                    debugger;
                    $scope.feesData = feesData.data;

                    var totalAmt = 0;
                    var totaPaid = 0;
                    var totbal = 0;
                    debugger;
                    for (var i = 0; i < $scope.feesData.length; i++) {
                        totalAmt = totalAmt + parseFloat($scope.feesData[i].std_fee_amount);
                        totaPaid = totaPaid + parseFloat($scope.feesData[i].std_TotalPaid);
                        totbal = totbal + parseFloat($scope.feesData[i].std_BalanceFee);
                    }
                    $scope.toamt = totalAmt;
                    $scope.topaid = totaPaid;
                    $scope.tobal = totbal;

                    if ($scope.feesData.length <= 0) {
                        swal(
                            {
                                showCloseButton: true,
                                text: 'No Data found.',
                                width: 350,
                                imageUrl: "assets/img/close.png",
                                showCloseButon: true
                            });
                    }
                    else {

                        $scope.totalItems = $scope.feesData.length;
                        $scope.todos = $scope.feesData;
                        $scope.makeTodos();
                    }
                });
               
            }
            $scope.fetchDatakeyPress = function ($event) {
                if ($event.keyCode == 13) {
                    _onFetchData();

                }
                else if ($event.keyCode == 5 && $event.ctrlKey && $event.shiftKey) {
                    $http.get(ENV.apiUrl + "api/StudentFee/GetReceipt").then(function (res) {
                        var rname = res.data;
                        var data = {
                            location: rname,
                            parameter: { fee_rec_no: '1120' },
                            state: 'main.Sim043'
                        }
                        window.localStorage["ReportDetails"] = JSON.stringify(data)
                        $state.go('main.ReportCardParameter')
                    });
                }
            }
            $scope.btnPreview_click = function () {
                _onFetchData();
            }
            //Events End

            /* Previous Details */

            $scope.student_name = '';

           
          
            $scope.edtadv = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '', sims_search_code: '' };

            $scope.cur_code_change_adv = function () {

                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + $scope.edtadv.sims_cur_code).then(function (AcademicYears) {
                    $scope.AcademicYears = AcademicYears.data;
                });

            }
            $scope.academic_year_change_adv = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllGrades?cur_code=" + $scope.edtadv.sims_cur_code + "&ac_year=" + $scope.edtadv.sims_academic_year).then(function (Grades) {
                    $scope.AdvGrades = Grades.data;
                });

            }
            $scope.grade_change_adv = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllSections?cur_code=" + $scope.edtadv.sims_cur_code + "&ac_year=" + $scope.edtadv.sims_academic_year + "&g_code=" + $scope.edtadv.sims_grade_code).then(function (sections) {
                    $scope.Advsections = sections.data;

                });

            }


            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=PrintBulk_FR").then(function (res) {
                $scope.final_doc_url = res.data;
            });

            $scope.report_show = true;
            $scope.Report = function (str) {
                debugger;
                $scope.student_name = str.std_fee_parent_id + '-' + str.std_fee_parent_name;
                $('#feemodal').modal('show');
                $scope.report_show = false;

                // $http.get(ENV.apiUrl + "api/StudentFee/GetReceipt").then(function (res) {
                // $scope.reportparameter = res.data;
                var data = {
                    location: $scope.receipt_url,
                    parameter: {
                        cur_code: str.std_fee_cur_code,
                        acad_year: str.std_fee_academic_year,
                        parent_flag: 2,
                        user_no: str.std_fee_parent_id,
                        from_date: moment(new Date()).format('YYYY-MM-DD'),
                        other_fee_flag: true,
                        cumfeeflag: false,

                    },
                    state: 'main.Sim43S',
                    ready: function () {
                        this.refreshReport();
                    },
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)
                //$state.go('main.ReportCardParameter')
                try {
                    $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                    $scope.location = $scope.rpt.location;
                    $scope.parameter = $scope.rpt.parameter;
                    $scope.state = $scope.rpt.state;
                }
                catch (ex) {
                }
                var s;
                s = "SimsReports." + $scope.location + ",SimsReports";
                var url = window.location.href;
                var domain = url.substring(0, url.indexOf(':'))
                if ($http.defaults.headers.common['schoolId'] == 'sms') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                }
                else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                }

                else {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                }
                console.log(service_url);
                $scope.parameters = {}

                $("#reportViewer1")
                               .telerik_ReportViewer({
                                   serviceUrl: service_url,
                                   viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                   scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                   // Zoom in and out the report using the scale
                                   // 1.0 is equal to 100%, i.e. the original size of the report
                                   scale: 1.0,
                                   ready: function () {
                                       //this.refreshReport();
                                   }

                               });
                var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                reportViewer.reportSource({
                    report: s,
                    parameters: $scope.parameter,
                });

                setInterval(function () {

                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                }, 1000);

                $timeout(function () {
                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                }, 100)
                // });
            }

            $scope.changeparent = function (c) {
                debugger
                if (c == '4') {
                    $scope.edt['search_txt'] = 'P';
                }
                else {
                    $scope.edt['search_txt'] = '';

                }
            }

        }])
})();

