﻿(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [];

    var doc_no;
    var simsController = angular.module('sims.module.Fee');
    simsController.controller('FeeCollectionRptCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {
            $scope.pagesize = 'All';
            $scope.pager = true;
            $scope.pagggg = false;
            var user = $rootScope.globals.currentUser.username;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.export_btn = false;
            $scope.total_amt = 0;
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;


           

            $scope.size = function (str) {

                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.FeeReceiptData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                $timeout(function () {
                    $("#fixTable").tableHeadFixer({ 'top': 1 });
                }, 100);
            };

        //    $scope.edt.to_date = new Date(); //$filter('date')(new Date(), "yyyy-MM-dd");

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy/mm/dd"
                // format: "dd/mm/yyyy"

            });

            $scope.showdate = function (date, name) {
                
                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //$scope.temp[name] = year + "/" + month + "/" + day;
                $scope.temp[name] = date;
                //var day = date.split("-")[0];
                //var month = date.split("-")[1];
                //var year = date.split("-")[2];
                //$scope.edt[name] = year + "-" + month + "-" + day;
            }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

           
            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.FeeReceiptData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.FeeReceiptData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked()

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.doc_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCurriculum) {
                $scope.Curriculum = getCurriculum.data;
                $scope.sims_cur_code = $scope.Curriculum[0].sims_cur_code
                $scope.getacademicYear($scope.sims_cur_code);
            });

            $scope.getacademicYear = function (str) {
                debugger
                $scope.AcademicYear = [];
                $http.get(ENV.apiUrl + "api/BankMaster/getAcademicYearsnew?curCode=" + str).then(function (getAcademicYear) {
                    $scope.AcademicYear = getAcademicYear.data;
                    $scope.sims_academic_year = $scope.AcademicYear[4].sims_academic_year;
                   
                });
            }

          
          //  $filter('date')(date, format, timezone)
          

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();

            //$scope.sdate = yyyy + '/' + mm + '/' + dd;
            //$scope.edate = yyyy + '/' + mm + '/' + dd;
            //$scope.temp = {
            //    from_date: yyyy + '/' + mm + '/' + dd,
            //    to_date: yyyy + '/' + mm + '/' + dd,
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.temp = {
                from_date: dd + '-' + mm + '-' + yyyy,
                to_date: dd + '-' + mm + '-' + yyyy,
            }

            $scope.Show = function () {
                $scope.FeeReceiptData = [];
                $scope.export_btn = true;
                $scope.table1 = true;
                $scope.pagggg = true;
                $scope.ImageView = false;
                $scope.export_btn = true;
                $scope.total_amt = 0;
                $http.get(ENV.apiUrl + "api/BankMaster/getfeecollectionDataNew?curCode=" + $scope.sims_cur_code + "&acad_year=" + $scope.sims_academic_year + "&from_date=" + $scope.temp.from_date + "&to_date=" + $scope.temp.to_date).then(function (FeeReceipt_Data) {
                    $scope.FeeReceiptData = FeeReceipt_Data.data;
                    $scope.totalItems = $scope.FeeReceiptData.length;
                    $scope.todos = $scope.FeeReceiptData;
                   
                    $scope.array_key = [];
                    $scope.array_key[0] = []
                    $scope.array_key[1] = []

                    $scope.array_key[2] = []

                    $scope.makeTodos();
                   

                    console.log($scope.FeeReceiptData);
                    if (FeeReceipt_Data.data.length > 0) {
                        $scope.size('All')

                        angular.forEach($scope.FeeReceiptData[0], function (value, key) {
                            
                            $scope.array_key[0].push(key);
                           
                            //if (value.Password == "thomasTheKing")
                            //  console.log("username is thomas");
                        });

                        console.log('keys', $scope.array_key);

                        angular.forEach($scope.FeeReceiptData[0], function (value, key) {
                            $scope.array_key[1].push(0);
                            $scope.array_key[2].push(0);

                            //if (value.Password == "thomasTheKing")
                            //  console.log("username is thomas");
                        });

                    }

                    
                    else {
                        $scope.ImageView = true;
                        $scope.pager = false;
                    }


                    angular.forEach($scope.FeeReceiptData, function (value, key) {
                        
                            if (value.doc_status == '6') {
                            }
                        else {
                            angular.forEach(value, function (value1, key1) {
                                


                                angular.forEach($scope.array_key[0], function (value2, key2) {
                                    
                                    if (value2 == key1) {
                                        //  if (value2 == 'doc_status') {

                                        $scope.array_key[1][key2] += parseInt(value1)
                                        //}
                                    }
                                });

                            });
                        }
                        
                    });

                    angular.forEach($scope.FeeReceiptData, function (value, key) {

                        if (value.doc_status != '6') {
                        }
                        else {
                            angular.forEach(value, function (value1, key1) {



                                angular.forEach($scope.array_key[0], function (value2, key2) {

                                    if (value2 == key1) {
                                        //  if (value2 == 'doc_status') {

                                        $scope.array_key[2][key2] += parseInt(value1)
                                        //}
                                    }
                                });

                            });
                        }

                    });
                   
                    debugger
                    var final_amt = [];
                    console.log('result', $scope.array_key[1])
                    $scope.final_amt = angular.copy($scope.array_key[1]);
                    
                    $scope.totalra = []
                    $scope.totalra1 = []

                    $scope.totalra2 = []

                    for (var i = 0; i < $scope.array_key[1].length; i++) {
                        if (!isNaN($scope.array_key[1][i])) {
                            var obj = { amt: $scope.array_key[1][i] }
                        }
                        else
                            var obj = { amt:0 }
                        $scope.totalra.push(obj);
                    }

                    for (var i = 0; i < $scope.array_key[2].length; i++) {
                        if (!isNaN($scope.array_key[2][i])) {
                            var obj = { amt: $scope.array_key[2][i] }
                        }
                        else
                            var obj = { amt: 0 }
                        $scope.totalra1.push(obj);
                    }

                    console.log('total', $scope.totalra)


                    for (var i = 0; i < $scope.array_key[2].length; i++) {
                        if (!isNaN($scope.array_key[2][i])) {
                            var obj = { amt:parseFloat( $scope.array_key[1][i])- parseFloat( $scope.array_key[2][i])}
                        }
                        else
                            var obj = { amt: 0 }
                        $scope.totalra2.push(obj);
                    }
                    console.log($scope.total_amt);
                });
                //}
            }

            $scope.Export = function () {
                var check = true;
                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " Fee Collection.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                         //   var blob = new Blob([document.getElementById('exporting_data').innerHTML], {
                         //       type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                         //   });
                         //   saveAs(blob, $scope.obj1.lic_school_name + " Fee Collection" + ".xls");
                         //   //alasql('SELECT * INTO XLSX("Report.xlsx",{headers:true}) \ FROM HTML("#printdata",{headers:true,skipdisplaynone:true})');

                            alasql('SELECT * INTO XLSX("Report.xlsx",{headers:true}) FROM ?', [$scope.FeeReceiptData]);

                        }
                    });
                }
                else {
                    swal({  text: "Report Not Save",imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, })
                }

            }

            $(function () {
                debugger
                $("#chkfrmdate").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.temp["from_date"] = '';
                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.temp = {
                            //from_date: yyyy + '-' + mm + '-' + dd,
                            //to_date: yyyy + '-' + mm + '-' + dd,
                            from_date: dd + '-' + mm + '-' + yyyy,
                             to_date: dd + '-' + mm + '-' + yyyy,
                        }

                    }
                });
            });

            $(function () {
                $("#Chktodate").click(function () {
                    if ($(this).is(":checked")) {
                        $("#tdate").attr("disabled", "disabled");
                        $scope.temp["to_date"] = '';
                    } else {

                        $("#tdate").removeAttr("disabled");
                        $("#tdate").focus();
                        $scope.temp = {
                            //from_date: yyyy + '-' + mm + '-' + dd,
                            //to_date: yyyy + '-' + mm + '-' + dd,
                            from_date: dd + '-' + mm + '-' + yyyy,
                             to_date: dd + '-' + mm + '-' + yyyy,
                        }

                    }
                });
            });

            $scope.Report = function (str) {
                debugger;
                console.log(str);
                var doc_no = str['doc No'];
                $http.get(ENV.apiUrl + "api/StudentFee/GetReceipt").then(function (res) {
                    var rname = res.data;

                    var data = {
                        //location: 'Sims.SIMR51FeeRec',
                        location: rname,
                        parameter: {
                            fee_rec_no: doc_no,
                        },
                        state: 'main.FeeCol',
                        ready: function () {
                            this.refreshReport();
                        },
                    }
                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                    $state.go('main.ReportCardParameter')
                });
            }

        }]
        )
})();