﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.TimeTable');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('FamilyAnalysisReport_NewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.temp = {};
            $scope.detailsField = true;
            $scope.hideprintpdf = false;
            $scope.family_analysis = true;
            $scope.temp.report_status = 'F';
            $scope.temp.with_pdc = false;
            $scope.temp.other_fee_flag = true;
            $scope.temp.cumfeeflag = false;
            $scope.sims_edit_remark = true;
            $scope.pagesize = '30';
            $scope.pageindex = "0";
            $scope.busy = false;
            $scope.pager = true;
            $scope.com_total = 0;
            $scope.temp.family_status = 'AF';
            $scope.temp.amount_filter = 'FA';
            $scope.SchoolLogo = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/SchoolLogo/';
            var user = $rootScope.globals.currentUser.username;
            $scope.show_rerturn_value_column = true;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            var today = new Date();
            var dd = today.getDate();
            var user = $rootScope.globals.currentUser.username;
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.from_date = dd + '-' + mm + '-' + yyyy;
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
            $http.get(ENV.apiUrl + "api/StudentFee/getDecimalPlaces").then(function (getDecimal) {
                debugger;
                $scope.decimal_degit = getDecimal.data;
                $scope.decimal_dsc = $scope.decimal_degit[0].comp_curcy_dec;
            });

            $http.get(ENV.apiUrl + "api/StudentReport/getRegistrationStatus").then(function (res) {
                $scope.Resstatus = res.data;

                if ($http.defaults.headers.common['schoolId'] == 'rakmps') {
                    console.log('scholl', $http.defaults.headers.common['schoolId']);
                    $scope.show_rerturn_value_column = true;
                }
            });

            if ($http.defaults.headers.common['schoolId'] == 'rakmps') {
                console.log('scholl', $http.defaults.headers.common['schoolId']);
                $scope.show_rerturn_value_column = true;
            }

            $http.get(ENV.apiUrl + "api/StudentReport/getFeedescStatus").then(function (res1) {
                $scope.Feestatus = res1.data;
            });

           
           
            $scope.report_all_parent_data = [];

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.from_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.from_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });


            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_end_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['chk_all'] = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 30, $scope.maxSize = 100;

            $scope.makeTodos = function () {
                debugger;
                if ($scope.pagesize == 'All') {
                    $scope.currentPage = 1;
                    $scope.showPager = false;
                    $scope.filteredTodos = $scope.report_data_new;
                }
                else {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                }
            };


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res1) {
                $scope.cur_data = res1.data;
                $scope.temp.sims_cur_code = $scope.cur_data[0].sims_cur_code;
                $scope.getAcademic_year($scope.cur_data[0].sims_cur_code);
            });

            $scope.getAcademic_year = function (cur_code1) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.temp.sims_cur_code).then(function (res1) {
                    $scope.acad_data = res1.data;
                    $scope.temp.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                });
            }

            $scope.getDetails = function () {
                $scope.selected_date = $scope.from_date;
                $scope.selected_academic_year = $("#academic_box option:selected").text();


                debugger;
                $scope.report_data_new = [];
                $scope.detailsField = true;
                $scope.busy = true;
                // $scope.family_analysis = true;
                //string cur_code, string acad_year, string circular_status, string user_group,string from,string to,string search,string report_view)
                $http.get(ENV.apiUrl + "api/FinanceReportRoute/GetFamilyAnalysisDetails_New?acad_year=" + $scope.temp.sims_academic_year + "&from=" + $scope.from_date + "&rpt_status=" + $scope.temp.report_status
                        + "&with_pdc=" + $scope.temp.with_pdc + "&family_status=" + $scope.temp.family_status + "&amount_filter=" + $scope.temp.amount_filter
                        + "&filter_amount=" + $scope.temp.amount_filter_textbox).then(function (res1) {
                            if (res1.data.length > 0) {
                                $scope.report_data_new = res1.data;
                                $scope.totalItems = $scope.report_data_new.length;
                                $scope.todos = $scope.report_data_new;
                                $scope.makeTodos();
                                $scope.pager = true;

                                $scope.temp.sltr_tran_amt_dr_total = 0;
                                $scope.temp.sltr_tran_amt_cr_total = 0;
                                $scope.temp.return_check_amt_total = 0;
                                $scope.temp.pdc_amount_total = 0;
                                $scope.temp.net_amt_total = 0;
                                $scope.temp.perc_amt_total = 0;


                                for (var j = 0; j < $scope.report_data_new.length; j++) {
                                    $scope.temp.sltr_tran_amt_dr_total = parseFloat($scope.temp.sltr_tran_amt_dr_total) + parseFloat($scope.report_data_new[j].sltr_tran_amt_dr);
                                    $scope.temp.sltr_tran_amt_cr_total = parseFloat($scope.temp.sltr_tran_amt_cr_total) + parseFloat($scope.report_data_new[j].sltr_tran_amt_cr);
                                    $scope.temp.return_check_amt_total = parseFloat($scope.temp.return_check_amt_total) + parseFloat($scope.report_data_new[j].return_check_amt);
                                    $scope.temp.pdc_amount_total = parseFloat($scope.temp.pdc_amount_total) + parseFloat($scope.report_data_new[j].pdc_amount);
                                    $scope.temp.net_amt_total = parseFloat($scope.temp.net_amt_total) + parseFloat($scope.report_data_new[j].net_amt);
                                    $scope.temp.perc_amt_total = parseFloat($scope.temp.perc_amt_total) + parseFloat($scope.report_data_new[j].perc_amt);

                                    //
                                }
                                $scope.busy = false;
                            }
                            else {
                                $scope.busy = false;
                                $scope.filteredTodos = [];
                                swal({ title: "Alert", text: " Sorry !!!!Data is not Available", showCloseButton: true, width: 300, height: 200 });
                                $scope.report_data_new = [];
                                $scope.temp.sltr_tran_amt_dr_total = 0;
                                $scope.temp.sltr_tran_amt_cr_total = 0;
                                $scope.temp.return_check_amt_total = 0;
                                $scope.temp.pdc_amount_total = 0;
                                $scope.temp.net_amt_total = 0;
                                $scope.temp.perc_amt_total = 0;

                            }
                            debugger;
                            if ($scope.temp.report_status == 'F') {
                                $scope.busy = false;
                                $scope.family_analysis = true;
                                $scope.student_analysis = false;
                                $scope.name = 'Family Analysis Report'
                            }
                            else {
                                $scope.busy = false;
                                $scope.family_analysis = false;
                                $scope.student_analysis = true;
                                $scope.name = 'Student Analysis Report'
                            }

                            console.log("$scope.return_check_amt_total", $scope.return_check_amt_total);
                        });
            }


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };
            $scope.search = function () {
                debugger;
                $scope.todos = $scope.searched($scope.report_data_new, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.report_data_new;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].report_data_new;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }

                }


            }


            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                        item.father_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_parent_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.class1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_grade_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_section_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_cur_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_academic_year.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.parent_contact_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }
            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $scope.colsvis = false;


            $scope.exportData = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        if ($scope.temp.report_status == 'F') {
                            var blob = new Blob([document.getElementById('pdf_print').innerHTML],
                             {
                                 type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                             });
                            $scope.detailsField = false;

                        }
                        else {

                            var blob = new Blob([document.getElementById('pdf_print1').innerHTML],
                            {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            $scope.showsumaryField = false;

                        }
                        $scope.detailsField = false;
                        saveAs(blob, "AnalysysReport.xls");
                        $scope.colsvis = false;
                        $scope.getDetails();

                    }

                });
                $scope.colsvis = true;

            };


            $scope.print = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        if ($scope.temp.report_status == 'F') {
                            var docHead = document.head.outerHTML;
                            var printContents = document.getElementById('pdf_print').outerHTML;
                            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                            var newWin = window.open("", "_blank", winAttr);
                            var writeDoc = newWin.document;
                            writeDoc.open();
                            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                            writeDoc.close();
                            newWin.focus();
                            $scope.detailsField = false;
                        }
                        else {
                            var docHead = document.head.outerHTML;
                            var printContents = document.getElementById('pdf_print1').outerHTML;
                            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                            var newWin = window.open("", "_blank", winAttr);
                            var writeDoc = newWin.document;
                            writeDoc.open();
                            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                            writeDoc.close();
                            newWin.focus();

                        }
                        $scope.detailsField = false;
                        $scope.colsvis = false;
                        $scope.getDetails();

                    }
                });
            };

            $scope.report_show = true;
            $scope.Report_call = function (r) {
                debugger;
                //$('#feemodal').modal({ backdrop: 'static', keyboard: false });
                $('#feemodal').modal('show');
                $scope.report_show = false;
                debugger
                $http.get(ENV.apiUrl + "api/family_statement/getReport").then(function (res1) {
                    $scope.result1 = res1.data;
                    if ($scope.temp.report_status == "R") {
                        var data = {
                            location: $scope.result1[0].sims_appl_form_field_value1,//"Gradebook.GBR039QR", 
                            parameter: {
                                cur_code: $scope.temp.sims_cur_code,
                                acad_year: $scope.temp.sims_academic_year,
                                parent_flag: 1,
                                user_no: r.sims_enroll_number,
                                from_date: moment($scope.from_date, 'DD-MM-YYYY').format('YYYY-MM-DD'),
                                other_fee_flag: $scope.temp.other_fee_flag,
                                cumfeeflag: $scope.temp.cumfeeflag,
                                printedby: user,
                                as_date: $scope.from_date,
                            },
                            state: 'main.FMLANA',
                            ready: function () {
                                this.refreshReport();
                            },
                        }
                    }

                    else {
                        var data = {
                            location: $scope.result1[0].sims_appl_form_field_value1,//"Gradebook.GBR039QR", 
                            parameter: {
                                cur_code: $scope.temp.sims_cur_code,
                                acad_year: $scope.temp.sims_academic_year,
                                parent_flag: 2,
                                user_no: r.sims_parent_number,
                                from_date: moment($scope.from_date, 'DD-MM-YYYY').format('YYYY-MM-DD'),
                                other_fee_flag: $scope.temp.other_fee_flag,
                                cumfeeflag: $scope.temp.cumfeeflag,
                                printedby: user,
                                as_date: $scope.from_date,
                            },
                            state: 'main.FMLANA',
                            ready: function () {
                                this.refreshReport();
                            },
                        }

                    }

                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                    try {
                        $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                        $scope.location = $scope.rpt.location;
                        $scope.parameter = $scope.rpt.parameter;
                        $scope.state = $scope.rpt.state;
                    }
                    catch (ex) {
                    }
                    var s;
                    s = "SimsReports." + $scope.location + ",SimsReports";

                    var url = window.location.href;
                    var domain = url.substring(0, url.indexOf(':'))
                    if ($http.defaults.headers.common['schoolId'] == 'sms') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'imert') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'staging2') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'staging') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'imert') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/reportsss/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/reportsss/api/reports/';
                    }


                    else {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    }
                    console.log(service_url);


                    $scope.parameters = {}

                    $("#reportViewer1")
                                   .telerik_ReportViewer({
                                       //serviceUrl: ENV.apiUrl + "api/reports/",
                                       serviceUrl: service_url,

                                       viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                       scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                       // Zoom in and out the report using the scale
                                       // 1.0 is equal to 100%, i.e. the original size of the report
                                       scale: 1.0,
                                       ready: function () {
                                           //this.refreshReport();
                                       }

                                   });





                    var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                    reportViewer.reportSource({
                        report: s,
                        parameters: $scope.parameter,
                    });


                    setInterval(function () {

                        $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                        $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });


                    }, 1000);


                    $timeout(function () {
                        $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                        $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });



                    }, 100)


                });
            }


            $scope.reset = function () {
                debugger;
                // $scope.temp.sims_grade_code = '';


                $scope.filteredTodos = [];
                $scope.pager = false;
                $scope.temp.sltr_tran_amt_dr_total = 0;
                $scope.temp.sltr_tran_amt_cr_total = 0;
                $scope.temp.pdc_amount_total = 0;
                $scope.temp.net_amt_total = 0;
                $scope.temp.perc_amt_total = 0;


            }

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.ischeck = function () {
                debugger
                $scope.checklick = function () {
                    var v = document.getElementById('expire');
                    if (v.checked == true) {
                        v.checked == false;
                    }
                }
            }

            $scope.communication = function (str) {
                debugger;
                $('#feemodal').modal('hide');
                $scope.busy = true;
                $scope.parentstudent = [];
                $scope.paymentPlanDetails = [];
                $scope.registration_status = '';
                $scope.fees_payment_plan = '';
                $scope.sims_promote_remark = '';
                $scope.check_remark_flag = false;
                $scope.sims_edit_remark = true;
                $scope.parent_id = str.sims_parent_number;
                $scope.parent_name = str.father_name;
                $scope.parent_contact = str.parent_contact_number;
                $scope.getcommunicationdetailsl($scope.parent_id);
                $scope.IP = {};
                $scope.IP.cur_shrt_name = str.sims_cur_code;
                $scope.IP.sims_academic_year = str.sims_academic_year;
                $scope.IP.sims_student_enroll_number = str.sims_parent_number;
                debugger
                $http.get(ENV.apiUrl + "api/StudentReport/getAdmRemarkStudList_ABQIS?data=" + JSON.stringify($scope.IP)).then(function (res) {
                    $scope.paymentPlanDetails = res.data.table;

                    if ($scope.paymentPlanDetails.length > 0) {
                        $scope.registration_status = $scope.paymentPlanDetails[0].promote_desc;
                        $scope.fees_payment_plan = $scope.paymentPlanDetails[0].fee_status_desc;
                        $scope.sims_promote_remark = $scope.paymentPlanDetails[0].sims_promote_remark;
                    }

                    $http.get(ENV.apiUrl + "api/FinanceReportRoute/GetFamilyAnalysisDetails_New_studentwise?acad_year=" + $scope.temp.sims_academic_year + "&from=" + $scope.from_date + "&rpt_status=" + "R"
                         + "&with_pdc=" + $scope.temp.with_pdc + "&sims_parent_number=" + str.sims_parent_number).then(function (res1) {
                             $scope.parentstudent = res1.data;

                             //for (var i = 0; i < $scope.report_all_parent_data.length; i++) {
                             //    if ($scope.parent_id == $scope.report_all_parent_data[i].sims_parent_number) {
                             //        $scope.parentstudent.push($scope.report_all_parent_data[i]);
                             //    }
                             //}

                             $scope.com_total = 0;
                             for (var i = 0; i < $scope.parentstudent.length; i++) {
                                 $scope.com_total = parseFloat($scope.com_total) + parseFloat($scope.parentstudent[i].net_amt);
                             }
                             $scope.busy = false;

                             setTimeout(function () {
                                 $('#CommunicationModal').modal('show');
                                 $('#feemodal').modal('hide');
                             }, 700);

                         });
                });
                console.log($scope.parentstudent);
                //$('#CommunicationModal').modal({ backdrop: 'static', keyboard: false });
            }

            $scope.getcommunicationdetailsl = function (sims_parent_number) {
                debugger

                $http.get(ENV.apiUrl + "api/FeeReceipt/getcommunicationdetails?sims_parent_number=" + sims_parent_number).then(function (getcommunicationdetails) {
                    $scope.getcommunicationdetails = getcommunicationdetails.data;
                });
            }

            $scope.save_communication = function () {
                debugger;
                $scope.com_datasend = [];
                var sims_parent_number_com = $scope.parentstudent[0].sims_parent_number;
               // for (var i = 0; i < $scope.parentstudent.length; i++) {
                    $scope.com_data = {
                        sims_fee_cur_code: $scope.parentstudent[0].sims_cur_code,
                        sims_fee_academic_year: $scope.parentstudent[0].sims_academic_year,
                        sims_parent_number: $scope.parentstudent[0].sims_parent_number,
                        sims_enroll_number: $scope.parentstudent[0].sims_enroll_number,
                        sims_fee_grade_code: $scope.parentstudent[0].sims_grade_code,
                        sims_fee_section_code: $scope.parentstudent[0].sims_section_code,
                        final_total: $scope.com_total,

                        sims_transaction_by: user,
                        sims_school_remark: $scope.school_remark,
                        sims_parent_remark: $scope.parent_remark,
                        expected_payment_date: $scope.expected_date,
                        sims_status: $scope.parentstudent[0].sims_status,
                    }
                    $scope.com_datasend.push($scope.com_data);
                //}
                $http.post(ENV.apiUrl + "api/FeeReceipt/InsertCommunication", $scope.com_datasend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    console.log("msg", msg);

                    if ($scope.msg1 == true) {
                        $(document).ready(function () {
                            $('#MyModal4').fadeOut('fast');

                            $scope.data = [];
                        });
                        swal({ title: "Alert", text: "Records Inserted successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        $scope.busyindicator = true;
                        $scope.school_remark = '';
                        $scope.parent_remark = '';
                        $scope.expected_date = '';

                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        $('#MyModal4').fadeOut('fast');
                        $scope.busyindicator = true;
                        $scope.school_remark = '';
                        $scope.parent_remark = '';
                        $scope.expected_date = '';

                    }
                });


            }


            $scope.saveFamilyPlan = function (str) {
                debugger;
                $scope.busy = true;
                var data1 = [];
                var data = $scope.parentstudent.length;
                for (var i = 0; i < $scope.parentstudent.length; i++) {
                    var data2 = {
                        cur_shrt_name: $scope.parentstudent[i].sims_cur_code,
                        sims_academic_year: $scope.parentstudent[i].sims_academic_year,
                        sims_grade_code: $scope.parentstudent[i].sims_grade_code,
                        sims_section_code: $scope.parentstudent[i].sims_section_code,
                        sims_student_enroll_number: $scope.parentstudent[i].sims_parent_number,
                        sims_student_attribute1: $scope.sims_promote_status,
                        sims_student_attribute3: $scope.sims_fee_paid,
                        sims_student_attribute5: $scope.sims_promote_remark
                    };
                    data1.push(data2);
                }

                $http.post(ENV.apiUrl + "api/StudentReport/CUDAdmissionstudentpro_ABQIS", data1).then(function (res2) {
                    $scope.msg1 = res2.data;
                    debugger;
                    if ($scope.msg1 == true) {

                        swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        $scope.busy = false;
                        $scope.sims_promote_status = '';
                        $scope.sims_fee_paid = '';
                        $scope.sims_promote_remark = '';
                        $('#CommunicationModal').modal('hide');
                    }
                    else {
                        swal({ text: "Record already present. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        $scope.busy = false;
                        $scope.sims_promote_status = '';
                        $scope.sims_fee_paid = '';
                        $scope.sims_promote_remark = '';
                        $('#CommunicationModal').modal('hide');
                    }

                });
            }

            $scope.check_edit_remark = function (str) {
                debugger
                if (str == true) {
                    $scope.sims_edit_remark = false;
                } else {
                    $scope.sims_edit_remark = true;
                }
            }


        }])

})();

