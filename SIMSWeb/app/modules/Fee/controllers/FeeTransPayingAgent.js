﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('FeeTransPayingAgent',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.maxLength = 0;
            $scope.table = false;

            $scope.SelectedFees = [];
            $scope.FeeDet = [];
            $scope.show_det = false;
            $scope.sel = {};
            $scope.IP = {};
            $scope.SUM = 0;
            $scope.show_det = true;
            $scope.IP.StudFeeTermCode = '';
            $scope.IP.PACODE = '';
            $scope.IP.StudEnroll = '';
            $scope.s = {};
            $scope.receiptDate = new Date().getDate() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getFullYear();
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
                $scope.s['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr();
            })

            $scope.getacyr = function () {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.s.sims_cur_code).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    $scope.s['sims_academic_year'] = $scope.Academic_year[0].sims_academic_year;
                    $scope.getterm();
                })
            }

            $http.post(ENV.apiUrl + "api/StudentFee/d5b1d259f086f320d2f5479209eb338a6ce47bc0").then(function (res) {
                $scope.sel.pas = res.data.table;
            });

            $scope.getterm = function () {
                $http.post(ENV.apiUrl + "api/StudentFee/da7b7454507b2a8fa08803a7cb06f938758d109d?academic_year=" + $scope.s.sims_academic_year + "&cur_code=" + $scope.s.sims_cur_code).then(function (res) {
                    $scope.sel.terms = res.data.table;
                });
            }
            $scope.search = function () {
                $scope.show_det = true;
                $scope.SUM = 0;
                //$scope.receiptDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();

                $scope.IP.StudFeeTermCode = $scope.s.StudFeeTermCode.termCode;
                $scope.IP.PACODE = $scope.s.PACODE.pcode;
                $scope.IP.StudEnroll = $scope.s.StudEnroll;
                $scope.IP.StudAcademic = $scope.s.sims_academic_year;
                $scope.IP.StudCurr = $scope.s.sims_cur_code;

                $scope.StudFeeDetails = [];
                $scope.SelectedFees = [];
                $http.get(ENV.apiUrl + "api/StudentFee/GetStudentPAFee?data=" + JSON.stringify($scope.IP)).then(function (res) {
                    $scope.StudFeeDetails = res.data.table;
                    if ($scope.StudFeeDetails.length > 0) {
                        $scope.table = true;
                    } else {
                        $scope.table = false;
                        swal({ text: 'Data Not Found', width: 300, height: 250,imageUrl: "assets/img/close.png", showCloseButton: true });
                    }
                });

            }

            //$scope.search();

            $scope.OnFeeAdd = function (item) {
                if (item.master) {
                    $scope.show_det = false;
                    var ob = {};
                    var ID = $scope.maxLength++;
                    item.ID = ID;
                    ob.pno = item.periodNo;
                    ob.feeNumber = item.fn;
                    ob.ID = ID;
                    ob.FD = item.studFeeDesc;
                    ob.enroll = item.enroll;
                    ob.bal_Fee = item.bal_Fee;
                    ob.studCurr = item.cur_code;
                    ob.studAcademic = item.ayear;
                    ob.term_code = item.term_code
                    ob.studGrade = item.grade_code;
                    ob.studSection = item.section_code;
                    ob.p_number = item.p_number;
                    ob.ppacode = item.pacode;
                    ob.sr = $scope.SelectedFees.length + 1;
                    ob.FeeCat = item.sims_fee_category;
                    ob.FeeCode = item.fc;
                    ob.flag = false;
                    $scope.SelectedFees.push(ob);
                    var v = true;
                    for (var i = 0; i < $scope.StudFeeDetails.length; i++) {
                        var item = $scope.StudFeeDetails[i];
                        v = v && item.master;
                    }
                    $scope.selAll = v;
                }
                else {
                    $scope.selAll = false;
                    for (var i = 0; i < $scope.SelectedFees.length; i++) {
                        if ($scope.SelectedFees[i].ID == item.ID) {
                            $scope.SelectedFees.splice(i, 1);
                            break;
                            $scope.show_det = true;
                        }
                    }
                }
                $scope.totalFeeamt();
                if ($scope.SelectedFees.length <= 0) {
                    $scope.show_det = true;
                }
            }

            $scope.OnAllFeeAdd = function () {
                if ($scope.selAll) {
                    for (var i = 0; i < $scope.StudFeeDetails.length; i++) {
                        var item = $scope.StudFeeDetails[i];
                        if (!item.master) {
                            item.master = true;
                            var ob = {};
                            var ID = $scope.maxLength++;
                            item.ID = ID;
                            $scope.show_det = false;
                            ob.pno = item.periodNo;
                            ob.feeNumber = item.fn;
                            ob.ID = ID;
                            ob.bal_Fee = item.bal_Fee;
                            ob.FD = item.studFeeDesc;
                            ob.enroll = item.enroll;
                            ob.studCurr = item.cur_code;
                            ob.studAcademic = item.ayear;
                            ob.term_code = item.term_code
                            ob.studGrade = item.grade_code;
                            ob.studSection = item.section_code;
                            ob.p_number = item.p_number;
                            ob.ppacode = item.pacode;
                            ob.sr = $scope.SelectedFees.length + 1;
                            ob.FeeCat = item.sims_fee_category;
                            ob.FeeCode = item.fc;
                            ob.flag = false;
                            $scope.SelectedFees.push(ob);
                        }
                    }
                }
                else {
                    for (var i = 0; i < $scope.StudFeeDetails.length; i++) {
                        var item = $scope.StudFeeDetails[i];
                        item.master = false;
                        $scope.show_det = true;

                    }
                    $scope.SelectedFees = [];
                }
                $scope.totalFeeamt();
                if ($scope.SelectedFees.length <= 0) {
                    $scope.show_det = true;
                }
            }

            $scope.submit = function () {
                $scope.clicked = true;
                var ar = [];
                for (var i = 0; i < $scope.SelectedFees.length; i++) {
                    if ($scope.SelectedFees[i].flag == false) {
                        var ob = {};
                        ob.P_NUMBER = $scope.SelectedFees[i].p_number;
                        ob.P_code = $scope.SelectedFees[i].ppacode;
                        ob.ReceiptDate = $scope.receiptDate;
                        ob.Remark = $scope.SelectedFees[i].Remark;
                        ob.ENROLL = $scope.SelectedFees[i].enroll;
                        ob.CURR = $scope.SelectedFees[i].studCurr;
                        ob.ACADEMIC = $scope.SelectedFees[i].studAcademic;
                        ob.GRADE = $scope.SelectedFees[i].studGrade;
                        ob.SECTION = $scope.SelectedFees[i].studSection;
                        ob.q = getFeeCode(ob.ENROLL);
                        ob.FEE_CATEGORY = $scope.SelectedFees[i].FeeCat;
                        ob.FEE_Per = $scope.SelectedFees[i].pno;
                        ob.FEE_AMT = '0';
                        ob.UserNm = $rootScope.globals.currentUser.username;
                        ob.T_code = $scope.SelectedFees[i].term_code;
                        ar.push(ob);
                    }
                }
                if (ar.length == 0) {
                    alert("Nothing Selected");
                    return;
                }
                $http.post(ENV.apiUrl + "api/StudentFee/a0f1490a20d0211c997b44bc357e1972deab8ae3", ar).then(function (resl) {
                    $scope.Result = resl.data;
                    $scope.SelectedFees = [];
                    $scope.show_det = true;
                    swal({ text: 'Fee Collected Successfully', imageUrl: "assets/img/check.png" });

                    $scope.search();
                });
            }

            function getFeeCode(enroll) {
                var ret = '';
                for (var i = 0; i < $scope.SelectedFees.length; i++) {
                    if ($scope.SelectedFees[i].flag == false && $scope.SelectedFees[i].enroll == enroll) {
                        ret = ret + $scope.SelectedFees[i].FeeCode + $scope.SelectedFees[i].feeNumber + ',';
                        $scope.SelectedFees[i].flag = true;
                    }
                }
                return ret;
            }

            function checkClicked() {
                $scope.clicked = !$scope.SelectedFees.length > 0;
            }

            $scope.totalFeeamt = function () {
                if (typeof ($scope.SelectedFees) === 'undefined') {
                    return 0;
                }
                var sum = 0;
                for (var i = $scope.SelectedFees.length - 1; i >= 0; i--) {
                    sum += parseFloat($scope.SelectedFees[i].bal_Fee);
                }
                $scope.SUM = sum.toFixed(2);
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                // format: 'yyyy-mm-dd'
                format: 'dd-mm-yyyy'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }]);
})();