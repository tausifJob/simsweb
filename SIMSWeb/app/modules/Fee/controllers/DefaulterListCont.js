﻿(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [];
    var doc_no;
    var simsController = angular.module('sims.module.Fee');
    simsController.controller('DefaulterListCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = 'All';
            $scope.pager = true;
            $scope.pagggg = false;
            var user = $rootScope.globals.currentUser.username;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            $scope.table2 = false;
            $scope.FeeReceiptData_all = [];
            

            //CUR
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.temp = {
                    'sims_cur_code': $scope.curriculum[0].sims_cur_code
                }
                $scope.getAccYear($scope.temp.sims_cur_code)
            });

            debugger
            $http.get(ENV.apiUrl + "api/FeeReceipt/getViewFor").then(function (view_for) {
                $scope.for_view = view_for.data;
            });
            
          


            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    debugger;
                    $scope.temp = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                        'sims_academic_year': $scope.Acc_year[0].sims_academic_year
                    }
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                });

            }

            $scope.getGrade = function (curCode, accYear) {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            


            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/StudentReport/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }
            $(function () {
                $('#cmb_section_code').multipleSelect({
                    width: '100%'
                });
            });

            $http.get(ENV.apiUrl + "api/FeeReceipt/getAcademicstatus").then(function (academic_year_status) {
                $scope.asacdmic_status = academic_year_status.data;
                setTimeout(function () {
                    $('#cmb_academic_code').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });
            $(function () {
                $('#cmb_academic_code').multipleSelect({
                    width: '100%'
                });
            });


            $scope.getFeeTyep = function (curCode, gradeCode, accYear, section)
            {
                debugger
                $http.get(ENV.apiUrl + "api/FeeReceipt/getFeeType?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear + "&section=" + section).then(function (feetype) {
                    $scope.fee_type = feetype.data;
                    setTimeout(function () {
                        $('#cmb_feetype_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }
            $(function () {
                $('#cmb_feetype_code').multipleSelect({
                    width: '100%'
                });
            });

            //$scope.size = function (str) {

            //    if (str == "All") {
            //        $scope.currentPage = '1';
            //        $scope.filteredTodos = $scope.FeeReceiptData;
            //        $scope.pager = false;
            //    }
            //    else {
            //        $scope.pager = true;
            //        $scope.pagesize = str;
            //        $scope.currentPage = 1;
            //        $scope.numPerPage = str;
            //        $scope.makeTodos();
            //    }
            //}

            //$scope.index = function (str) {
            //    $scope.pageindex = str;
            //    $scope.currentPage = str;
            //    console.log("currentPage=" + $scope.currentPage);
            //    $scope.makeTodos();
            //    main.checked = false;
            //    $scope.row1 = '';
            //}

            //$scope.makeTodos = function () {
            //    var rem = parseInt($scope.totalItems % $scope.numPerPage);
            //    if (rem == '0') {
            //        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
            //    }
            //    else {
            //        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
            //    }
            //    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
            //    var end = parseInt(begin) + parseInt($scope.numPerPage);

            //    console.log("begin=" + begin); console.log("end=" + end);

            //    $scope.filteredTodos = $scope.todos.slice(begin, end);
            //};

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

           

            $scope.paginationview1 = document.getElementById("paginationview");

            $scope.showdate = function (date, name) {
                debugger
                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //$scope.edt[name] = year + "/" + month + "/" + day;
                $scope.edt[name] = date;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.FeeReceiptData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.FeeReceiptData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked()

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.doc_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }


            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            //$scope.sdate = yyyy + '-' + mm + '-' + dd;
            //$scope.edate = yyyy + '-' + mm + '-' + dd;
            //$scope.edt = {
            //    from_date: yyyy + '-' + mm + '-' + dd,
            //    to_date: yyyy + '-' + mm + '-' + dd,

            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.edt = {
                from_date: dd + '-' + mm + '-' + yyyy,
                to_date: dd + '-' + mm + '-' + yyyy,

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.onKeyPress = function ($event) {
                debugger;
                if ($event.keyCode == 13) {

                    $scope.Show($scope.edt.from_date, $scope.edt.to_date, $scope.edt.search);
                    // console.log("enetr");
                }
            };


            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + 5);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + 5);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.Submit = function () {
                debugger
                $scope.busy = true;
                if ($scope.temp.sims_enroll_number == undefined || $scope.temp.sims_enroll_number == "") {
                    $scope.temp.sims_enroll_number = '';
                }
                $scope.table1 = true;
              
                $scope.pagggg = true;
                $scope.ImageView = false;
              
                $http.get(ENV.apiUrl + "api/FeeReceipt/getAllDefaulterListdataNewALLGrade?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&feetype=" + $scope.temp.sims_fee_code + "&academic_status=" + $scope.temp.sims_appl_parameter + "&enrollno=" + $scope.temp.sims_enroll_number + "&doc_date=" + $scope.temp.doc_date).then(function (FeeReceipt_Data_all) {
                    $scope.FeeReceiptData_all = FeeReceipt_Data_all.data;
                    console.log("All Student List :" + $scope.FeeReceiptData_all);
                });
                


                $http.get(ENV.apiUrl + "api/FeeReceipt/getAllDefaulterListdataNew?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section=" + $scope.temp.sims_section_code + "&feetype=" + $scope.temp.sims_fee_code + "&academic_status=" + $scope.temp.sims_appl_parameter + "&enrollno=" + $scope.temp.sims_enroll_number + "&doc_date=" + $scope.temp.doc_date).then(function (FeeReceipt_Data) {
                    $scope.FeeReceiptData = FeeReceipt_Data.data[0];

                    $scope.final_total = 0;
                    $scope.pending_memo = 0;
                  
                    $scope.busy = false;
                    $scope.table2 = true;
                    console.log($scope.FeeReceiptData);
                    for (var i = 0; i < $scope.FeeReceiptData.length; i++) {
                        $scope.final_total = parseFloat($scope.FeeReceiptData[i].final_total) + parseFloat($scope.final_total);
                        $scope.pending_memo = parseFloat($scope.FeeReceiptData[i].pending_memo) + parseFloat($scope.pending_memo);
                        
                    }
                    console.log($scope.final_total);


                    $scope.OutstandingData = FeeReceipt_Data.data[1];

                    $scope.cnt_90_total = 0
                    $scope.cnt_75_total = 0
                    $scope.cnt_50_total = 0
                    $scope.cnt_30_total = 0
                    $scope.cnt_bel_30_total = 0


                    $scope.bal_90_total = 0
                    $scope.bal_75_total = 0
                    $scope.bal_50_total = 0
                    $scope.bal_30_total = 0
                    $scope.bal_bel_30_total = 0
                    $scope.fam_amt_total = 0;
                    $scope.bal_amt_total = 0;


                    for (var i = 0; i < $scope.OutstandingData.length; i++) {
                        $scope.cnt_90_total = parseFloat($scope.OutstandingData[i].cnt_90) + parseFloat($scope.cnt_90_total);
                        $scope.cnt_75_total = parseFloat($scope.OutstandingData[i].cnt_75) + parseFloat($scope.cnt_75_total);
                        $scope.cnt_50_total = parseFloat($scope.OutstandingData[i].cnt_50) + parseFloat($scope.cnt_50_total);
                        $scope.cnt_30_total = parseFloat($scope.OutstandingData[i].cnt_30) + parseFloat($scope.cnt_30_total);
                        $scope.cnt_bel_30_total = parseFloat($scope.OutstandingData[i].cnt_bel_30) + parseFloat($scope.cnt_bel_30_total);

                        $scope.bal_90_total = parseFloat($scope.OutstandingData[i].bal_90) + parseFloat($scope.bal_90_total);
                        $scope.bal_75_total = parseFloat($scope.OutstandingData[i].bal_75) + parseFloat($scope.bal_75_total);
                        $scope.bal_50_total = parseFloat($scope.OutstandingData[i].bal_50) + parseFloat($scope.bal_50_total);
                        $scope.bal_30_total = parseFloat($scope.OutstandingData[i].bal_30) + parseFloat($scope.bal_30_total);
                        $scope.bal_bel_30_total = parseFloat($scope.OutstandingData[i].bal_bel_30) + parseFloat($scope.bal_bel_30_total);

                    }

                    $scope.fam_amt_total = $scope.cnt_90_total + $scope.cnt_75_total + $scope.cnt_50_total + $scope.cnt_30_total + $scope.cnt_bel_30_total;
                    $scope.bal_amt_total = $scope.bal_90_total + $scope.bal_75_total + $scope.bal_50_total + $scope.bal_30_total + $scope.bal_bel_30_total;



                });


                debugger
             
                //Outstanding list JS Calling                                                                                                                                                                                                                                                                                                                                       
                //$http.get(ENV.apiUrl + "api/FeeReceipt/getAllOutStandingList?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section=" + $scope.temp.sims_section_code + "&feetype=" + $scope.temp.sims_fee_code + "&param_type=" + 'F' + "&param1=" + "undefined" + "&param2=" + "undefined" + "&search=" + "undefined").then(function (out_standing) {
                //    $scope.OutstandingData = out_standing.data;

                //    $scope.cnt_90_total=0
                //    $scope.cnt_75_total=0
                //    $scope.cnt_50_total=0
                //    $scope.cnt_30_total=0
                //    $scope.cnt_bel_30_total = 0


                //    $scope.bal_90_total = 0
                //    $scope.bal_75_total = 0
                //    $scope.bal_50_total = 0
                //    $scope.bal_30_total = 0
                //    $scope.bal_bel_30_total = 0
                //    $scope.fam_amt_total = 0;
                //    $scope.bal_amt_total = 0;


                //    for (var i = 0; i < $scope.OutstandingData.length; i++) {
                //        $scope.cnt_90_total = parseFloat($scope.OutstandingData[i].cnt_90) + parseFloat($scope.cnt_90_total);
                //        $scope.cnt_75_total = parseFloat($scope.OutstandingData[i].cnt_75) + parseFloat($scope.cnt_75_total);
                //        $scope.cnt_50_total = parseFloat($scope.OutstandingData[i].cnt_50) + parseFloat($scope.cnt_50_total);
                //        $scope.cnt_30_total = parseFloat($scope.OutstandingData[i].cnt_30) + parseFloat($scope.cnt_30_total);
                //        $scope.cnt_bel_30_total = parseFloat($scope.OutstandingData[i].cnt_bel_30) + parseFloat($scope.cnt_bel_30_total);

                //        $scope.bal_90_total = parseFloat($scope.OutstandingData[i].bal_90) + parseFloat($scope.bal_90_total);
                //        $scope.bal_75_total = parseFloat($scope.OutstandingData[i].bal_75) + parseFloat($scope.bal_75_total);
                //        $scope.bal_50_total = parseFloat($scope.OutstandingData[i].bal_50) + parseFloat($scope.bal_50_total);
                //        $scope.bal_30_total = parseFloat($scope.OutstandingData[i].bal_30) + parseFloat($scope.bal_30_total);
                //        $scope.bal_bel_30_total = parseFloat($scope.OutstandingData[i].bal_bel_30) + parseFloat($scope.bal_bel_30_total);

                //    }
                     
                //    $scope.fam_amt_total = $scope.cnt_90_total + $scope.cnt_75_total + $scope.cnt_50_total + $scope.cnt_30_total + $scope.cnt_bel_30_total;
                //    $scope.bal_amt_total = $scope.bal_90_total + $scope.bal_75_total + $scope.bal_50_total + $scope.bal_30_total + $scope.bal_bel_30_total;

                //    console.log($scope.fam_amt_total);
                //    console.log($scope.bal_amt_total);




                //});

                //}
            }

          //  $('#urlModal').modal({ backdrop: 'static', keyboard: false });


            $scope.getpercentagewiseRecord = function (min_value, max_value,name) {
                debugger
                $scope.name_selected=name;

                debugger
                $http.get(ENV.apiUrl + "api/Fee/SFS/getDefaulterListNEw1?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section=" + $scope.temp.sims_section_code + "&feetype=" + $scope.temp.sims_fee_code + "&academic_status=" + $scope.temp.sims_appl_parameter  + "&doc_date=" + $scope.temp.doc_date + "&min_value=" + min_value + "&max_value=" + max_value).then(function (res) {
                    $scope.defaulterdata = res.data;

                    if ($scope.defaulterdata.length > 0) {
                        setTimeout(function(){
                            $('#urlModal').modal({ backdrop: 'static', keyboard: false });},500)
                    }

                    $scope.final_total = 0
                    for (var i = 0; i < $scope.defaulterdata.length; i++) {
                        $scope.final_total = parseFloat($scope.defaulterdata[i].final_total) + parseFloat($scope.final_total);
                    }
                    console.log($scope.final_total);


                  //  $('#loader').modal('hide')

               });


            }





            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.edt["from_date"] = '';
                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.edt = {//dd + '-' + mm + '-' + yyyy
                            //from_date: yyyy + '-' + mm + '-' + dd,
                            //to_date: yyyy + '-' + mm + '-' + dd,
                            from_date: dd + '-' + mm + '-' + yyyy,
                            to_date: dd + '-' + mm + '-' + yyyy,
                        }

                    }
                });
            });


            $scope.Report = function () {
                var check = true;
                debugger;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " DefaulterListOnDate.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger;
                            var blob = new Blob([document.getElementById('printdata3').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " DefaulterListOnDate" + ".xls");
                        }
                    });
                }
                else {
                    swal({ text: "Report Not Save", imageUrl: "assets/img/close.png",showCloseButton: true, width: 380, })
                }
            }


            $scope.Report1 = function () {
                var check = true;
                debugger;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " DefaulterListOnDate.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger;
                            var blob = new Blob([document.getElementById('percentage').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " DefaulterListOnDate" + ".xls");
                        }
                    });
                }
                else {
                    swal({ text: "Report Not Save",imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, })
                }
            }


            $scope.communication = function (str) {
                setTimeout(function () {
                $('#urlModal').modal('hide');
                }, 100);
                $scope.parentstudent = [];
                $scope.parent_id = str.sims_parent_number;
                $scope.parent_name = str.sims_parent_name;
                $scope.parent_contact = str.sims_parent_contact;
                $scope.getcommunicationdetailsl($scope.parent_id);
                debugger
                for (var i = 0; i < $scope.FeeReceiptData_all.length; i++) {
                    if ($scope.parent_id == $scope.FeeReceiptData_all[i].sims_parent_number) {
                        $scope.parentstudent.push($scope.FeeReceiptData_all[i]);
                    }
                }
                debugger
                $scope.com_total = 0;
                for (i = 0; i < $scope.parentstudent.length; i++)
                {
                    $scope.com_total = parseFloat($scope.com_total )+ parseFloat($scope.parentstudent[i].final_total);
                }

                console.log($scope.parentstudent);
               
                //$('#CommunicationModal').modal({ backdrop: 'static', keyboard: false });
                setTimeout(function () {
                    $('#CommunicationModal').modal('show');
                }, 700);
               
            }

            $scope.save_communication = function () {
                $scope.com_datasend = [];
                var sims_parent_number_com = $scope.parentstudent[0].sims_parent_number;
                for (var i = 0; i < $scope.parentstudent.length; i++) {
                    $scope.com_data = {
                    sims_fee_cur_code: $scope.parentstudent[i].sims_fee_cur_code,
                    sims_fee_academic_year: $scope.parentstudent[i].sims_fee_academic_year,
                    sims_parent_number: $scope.parentstudent[i].sims_parent_number,
                    sims_enroll_number: $scope.parentstudent[i].sims_enroll_number,
                    sims_fee_grade_code: $scope.parentstudent[i].sims_fee_grade_code,
                    sims_fee_section_code: $scope.parentstudent[i].sims_fee_section_code,
                    final_total: $scope.parentstudent[i].final_total,

                    sims_transaction_by: user,
                    sims_school_remark: $scope.school_remark,
                    sims_parent_remark: $scope.parent_remark,
                    expected_payment_date: $scope.expected_date
                    }
                    $scope.com_datasend.push($scope.com_data);
                }
                $http.post(ENV.apiUrl + "api/FeeReceipt/InsertCommunication", $scope.com_datasend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    console.log("msg", msg);

                    if ($scope.msg1 == true) {
                        $(document).ready(function () {
                            $('#MyModal4').fadeOut('fast');
                            
                            $scope.data= [];                        
                        });
                        swal({ title: "Alert", text: "Records Inserted successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        $scope.busyindicator = true;
                        $scope.getcommunicationdetailsl(sims_parent_number_com);
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        $('#MyModal4').fadeOut('fast');
                        $scope.busyindicator = true;
                    }
                });
                debugger
                
            }

            $scope.getcommunicationdetailsl = function (sims_parent_number) {
                debugger
               
                $http.get(ENV.apiUrl + "api/FeeReceipt/getcommunicationdetails?sims_parent_number=" + sims_parent_number).then(function (getcommunicationdetails) {
                    $scope.getcommunicationdetails = getcommunicationdetails.data;
                   
                });
            }

            $scope.comunnication_report = function () {
                var check = true;
                debugger;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " CommunicationDetails.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger;
                            var blob = new Blob([document.getElementById('commuication_table').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " CommunicationDetails" + ".xls");
                        }
                    });
                }
                else {
                    swal({ text: "Report Not Save", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, })
                }
            }
        }]
        )
})();