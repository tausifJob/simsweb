﻿(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [];
    var doc_no;
    var simsController = angular.module('sims.module.Fee');
    simsController.controller('DefaulterListCont_pearl',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = 'All';
            $scope.pager = true;
            $scope.pagggg = false;
            var user = $rootScope.globals.currentUser.username;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;




            //CUR
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.temp = {
                    'sims_cur_code': $scope.curriculum[0].sims_cur_code
                }
                $scope.getAccYear($scope.temp.sims_cur_code)
            });

            debugger
            $http.get(ENV.apiUrl + "api/FeeReceipt/getViewFor").then(function (view_for) {
                $scope.for_view = view_for.data;
            });




            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    debugger;
                    $scope.temp = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                        'sims_academic_year': $scope.Acc_year[0].sims_academic_year
                    }
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                });

            }

            $scope.getGrade = function (curCode, accYear) {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }




            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/StudentReport/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }
            $(function () {
                $('#cmb_section_code').multipleSelect({
                    width: '100%'
                });
            });

            $http.get(ENV.apiUrl + "api/FeeReceipt/getAcademicstatus").then(function (academic_year_status) {
                $scope.asacdmic_status = academic_year_status.data;
                setTimeout(function () {
                    $('#cmb_academic_code').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });
            $(function () {
                $('#cmb_academic_code').multipleSelect({
                    width: '100%'
                });
            });



            $http.get(ENV.apiUrl + "api/FeeReceipt/getViewmode").then(function (viewmode) {
                $scope.view_mode = viewmode.data;
            });




            $scope.getperiod = function (view_mode) {
                debugger
                $http.get(ENV.apiUrl + "api/FeeReceipt/getperiodsdetails?view_mode=" + view_mode + "&cur_code=" + $scope.temp.sims_cur_code + "&acad_year=" + $scope.temp.sims_academic_year).then(function (get_period) {
                    $scope.get_period_det = get_period.data;
                    setTimeout(function () {
                        $('#getPeriods').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });

            }

            $(function () {
                $('#getPeriods').multipleSelect({
                    width: '100%'
                });
            });


            $scope.getFeeTyep = function (curCode, gradeCode, accYear, section) {
                debugger
                $http.get(ENV.apiUrl + "api/FeeReceipt/getFeeType?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear + "&section=" + section).then(function (feetype) {
                    $scope.fee_type = feetype.data;
                    setTimeout(function () {
                        $('#cmb_feetype_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }
            $(function () {
                $('#cmb_feetype_code').multipleSelect({
                    width: '100%'
                });
            });



            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });



            $scope.paginationview1 = document.getElementById("paginationview");

            $scope.showdate = function (date, name) {
                debugger
                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //$scope.edt[name] = year + "/" + month + "/" + day;
                $scope.edt[name] = date;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.FeeReceiptData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.FeeReceiptData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked()

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.doc_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }


            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            //$scope.sdate = yyyy + '-' + mm + '-' + dd;
            //$scope.edate = yyyy + '-' + mm + '-' + dd;
            //$scope.edt = {
            //    from_date: yyyy + '-' + mm + '-' + dd,
            //    to_date: yyyy + '-' + mm + '-' + dd,

            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.edt = {
                from_date: dd + '-' + mm + '-' + yyyy,
                to_date: dd + '-' + mm + '-' + yyyy,

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.onKeyPress = function ($event) {
                debugger;
                if ($event.keyCode == 13) {

                    $scope.Show($scope.edt.from_date, $scope.edt.to_date, $scope.edt.search);
                    // console.log("enetr");
                }
            };


            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + 5);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + 5);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.Submit = function () {
                debugger
                $scope.busy = true;
                if ($scope.temp.sims_enroll_number == undefined || $scope.temp.sims_enroll_number == "") {
                    $scope.temp.sims_enroll_number = '';
                }
                $scope.table1 = true;
                $scope.pagggg = true;
                $scope.ImageView = false;

                $http.get(ENV.apiUrl + "api/FeeReceipt/getAllDefaulterListdataNew_pearl?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section=" + $scope.temp.sims_section_code + "&feetype=" + $scope.temp.sims_fee_code + "&academic_status=" + $scope.temp.sims_appl_parameter + "&enrollno=" + $scope.temp.sims_enroll_number + "&doc_date=" + $scope.temp.doc_date + "&view_mode=" + $scope.temp.comn_appl_parameter2 + "&period_details=" + $scope.temp.comn_appl_parameter1).then(function (FeeReceipt_Data) {
                    $scope.FeeReceiptData = FeeReceipt_Data.data;

                    $scope.final_total = 0;
                    $scope.pending_memo = 0;
                    //$scope.totalItems = $scope.FeeReceiptData.length;
                    //$scope.todos = $scope.FeeReceiptData;
                    //$scope.makeTodos();
                    $scope.busy = false;
                    console.log($scope.FeeReceiptData);
                    for (var i = 0; i < $scope.FeeReceiptData.length; i++) {
                        $scope.final_total = parseFloat($scope.FeeReceiptData[i].final_total) + parseFloat($scope.final_total);
                        $scope.pending_memo = parseFloat($scope.FeeReceiptData[i].pending_memo) + parseFloat($scope.pending_memo);

                    }
                    console.log($scope.final_total);

                });
                //}
            }

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.edt["from_date"] = '';
                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.edt = {//dd + '-' + mm + '-' + yyyy
                            //from_date: yyyy + '-' + mm + '-' + dd,
                            //to_date: yyyy + '-' + mm + '-' + dd,
                            from_date: dd + '-' + mm + '-' + yyyy,
                            to_date: dd + '-' + mm + '-' + yyyy,
                        }

                    }
                });
            });


            $scope.Report = function () {
                var check = true;
                debugger;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " DefaulterListOnDate.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger;
                            var blob = new Blob([document.getElementById('printdata3').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " DefaulterListOnDate" + ".xls");
                        }
                    });
                }
                else {
                    swal({ text: "Report Not Save", imageUrl: "assets/img/close.png",showCloseButton: true, width: 380, })
                }
            }


        }]
        )
})();