﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('RejectInvoiceCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.grid = true;
            $scope.maintable1 = false;
            $scope.busyindicator1 = false;

            var user = $rootScope.globals.currentUser.username;
            $scope.pagesize = '10';

            var now = new Date();
            var month = (now.getMonth() + 1);
            var day = now.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;

            $http.get(ENV.apiUrl + "api/DebitCreditNote/getischeck?user=" + user).then(function (res) {
                $scope.ischeck = res.data;
            });

            $scope.payment_date = day + '-' + month + '-' + now.getFullYear();

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
                $scope.sims_cur_code = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr();
            })

            $http.get(ENV.apiUrl + "api/studentfeerefund/GetSimsSearchOptions").then(function (res) {
                $scope.search_by_data = res.data;
                $scope.search_by_code = $scope.search_by_data[0].search_by_code;
            })

            $scope.getacyr = function () {

                $http.get(ENV.apiUrl + "api/DebitCreditNote/GetAcademic_year?curCode=" + $scope.sims_cur_code).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    $scope.sims_academic_year = $scope.Academic_year[0].sims_academic_year;
                    $scope.getsections();
                })
            }
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.getsections = function () {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.sims_cur_code + "&academic_year=" + $scope.sims_academic_year).then(function (res) {
                    $scope.grade = res.data;
                    $scope.getsection();
                })
            };

            $scope.getsection = function () {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.sims_cur_code + "&grade_code=" + $scope.sims_grade_code + "&academic_year=" + $scope.sims_academic_year).then(function (Allsection) {
                    $scope.section1 = Allsection.data;
                });
            };

            $scope.Select_Multiple = function (str) {
                for (var i = 0; i < $scope.Fee_Approavl_Details.length; i++) {
                    if (str == true) {
                        $scope.Fee_Approavl_Details[i].status = true;
                    }
                    else {
                        $scope.Fee_Approavl_Details[i].status = false;
                    }
                }
            }

            $scope.Approval_show_click = function () {
                $scope.maintable1 = false;
                $scope.busyindicator1 = true;

                debugger;
                if ($scope.sims_cur_code == undefined) {
                    $scope.sims_cur_code = '';
                }

                if ($scope.sims_academic_year == undefined) {
                    $scope.sims_academic_year = '';
                }

                if ($scope.sims_grade_code == undefined) {
                    $scope.sims_grade_code = '';
                }

                if ($scope.sims_section_name == undefined) {
                    $scope.sims_section_name = '';
                }

                if ($scope.searchText1 == undefined) {
                    $scope.searchText1 = '';
                }
                
                $http.get(ENV.apiUrl + "api/DebitCreditNote/getAllStudentFeeDetailsforInvoice?cur_code=" + $scope.sims_cur_code + "&aca_year=" + $scope.sims_academic_year + "&grade=" + $scope.sims_grade_code + "&section=" + $scope.sims_section_name + "&search=" + $scope.searchText1).then(function (Approval_fee) {
                    $scope.Fee_Approavl_Details = Approval_fee.data;
                   
                    if ($scope.Fee_Approavl_Details.length > 0) {
                        $scope.maintable1 = true;
                    }
                    else {
                        swal({ text: 'Data Not Found', imageUrl: "assets/img/close.png", width: 300, height: 250, showCloseButton: true });
                    }

                    $scope.busyindicator1 = false;

                });
            }
            $scope.selectOnebyOne = function (info) {

                for (var i = 0; i < $scope.Fee_Approavl_Details.length; i++) {

                    if (info.doc_no == $scope.Fee_Approavl_Details[i].doc_no) {

                        var v = document.getElementById($scope.Fee_Approavl_Details[i].std_fee_enroll_number + $scope.Fee_Approavl_Details[i].doc_no + i);

                        if (info.status == true)
                            v.checked = true;
                        else
                            v.checked = false;

                    }
                }
            }
            $scope.rejectInvoice = function () {
                debugger;
                $scope.datasend = [];

                for (var i = 0; i < $scope.Fee_Approavl_Details.length; i++) {
                    if ($scope.Fee_Approavl_Details[i].status == true) {
                        $scope.Fee_Approavl_Details[i].username = user;
                        $scope.datasend.push($scope.Fee_Approavl_Details[i]);
                    }
                   
               }

                if ($scope.datasend.length > 0) {

                    $http.post(ENV.apiUrl + "api/DebitCreditNote/RejectInvoice", $scope.datasend).then(function (res) {
                        $scope.rejected = res.data;
                        if ($scope.rejected == true) {
                            swal({ text: 'Invoice Rejected', imageUrl: "assets/img/check.png", width: 300, height: 250, showCloseButton: true });
                            $scope.Approval_show_click();
                        }
                        else {
                            swal({ text: 'Invoice Not Rejected', imageUrl: "assets/img/close.png", width: 300, height: 250, showCloseButton: true });
                            $scope.Approval_show_click();
                        }
                       
                    });
                   
                }
                else {
                    swal({ text: 'Select Record', imageUrl: "assets/img/close.png", width: 300, height: 250, showCloseButton: true });
                }

            }


            $scope.Approval_Select_Multiple = function (str) {

               
                for (var i = 0; i < $scope.Fee_Approavl_Details.length; i++) {
                    if (str == true) {
                        $scope.Fee_Approavl_Details[i].status = true;
                    }
                    else {
                        $scope.Fee_Approavl_Details[i].status = false;
                    }
                }

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
        }]);
})();
