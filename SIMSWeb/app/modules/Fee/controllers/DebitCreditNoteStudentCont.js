﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('DebitCreditNoteStudentCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            
            $scope.defined = true;
            $scope.grid = true;
            $scope.newnotedata = true;
            $scope.maintable1 = false;
            $scope.busyindicator1 = false;
            $scope.FeeDetailFeetype = [];
            var user = $rootScope.globals.currentUser.username;
            $scope.pagesize = '10';
            $scope.Fee_Details = [];
            var now = new Date();
            var month = (now.getMonth() + 1);
            var day = now.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;

            $http.get(ENV.apiUrl + "api/DebitCreditNote/getischeck?user=" + user).then(function (res) {
                $scope.ischeck = res.data;
            });

            $scope.payment_date = day + '-' + month + '-' + now.getFullYear();
            $scope.doc_date = day + '-' + month + '-' + now.getFullYear();

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
                $scope.sims_cur_code = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr();
            })

            $http.get(ENV.apiUrl + "api/studentfeerefund/GetSimsSearchOptions").then(function (res) {
                $scope.search_by_data = res.data;
                $scope.search_by_code = $scope.search_by_data[0].search_by_code;
            })

            $http.get(ENV.apiUrl + "api/DebitCreditNote/getparameter").then(function (fee_types) {
                $scope.parameter = fee_types.data;
                $scope.sims_parameter = $scope.parameter[0].gldc_doc_code;
                getParameterDetails();
            });

            $scope.getacyr = function () {

                $http.get(ENV.apiUrl + "api/DebitCreditNote/GetAcademic_year?curCode=" + $scope.sims_cur_code).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    $scope.sims_academic_year = $scope.Academic_year[0].sims_academic_year;
                    $scope.getsections();
                })
            }

            $scope.getsections = function () {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.sims_cur_code + "&academic_year=" + $scope.sims_academic_year).then(function (res) {
                    $scope.grade = res.data;
                    $scope.getsection();
                })
            };

            $scope.getsection = function () {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.sims_cur_code + "&grade_code=" + $scope.sims_grade_code + "&academic_year=" + $scope.sims_academic_year).then(function (Allsection) {
                    $scope.section1 = Allsection.data;
                });
            };

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseFloat($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseFloat(begin) + parseFloat($scope.numPerPage);



                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.Show_Data = function () {

                $scope.maintable = false;
                if ($scope.sims_cur_code == undefined) {

                    $scope.sims_cur_code = null;
                }
                if ($scope.sims_grade_code == undefined) {

                    $scope.sims_grade_code = null;
                }
                if ($scope.sims_academic_year == undefined) {

                    $scope.sims_academic_year = null;
                }
                if ($scope.sims_section_name == undefined) {

                    $scope.sims_section_name = null;
                }
                if ($scope.search_flag == undefined) {

                    $scope.search_flag = null;
                }
                $scope.busyindicator = true;

                if ($scope.search_text == undefined || $scope.search_text == '') {
                    $scope.search_text = null;
                }

                $http.get(ENV.apiUrl + "api/DebitCreditNote/GetdebitcreditAllStudentFee?cc=" + $scope.sims_cur_code + "&ay=" + $scope.sims_academic_year + "&gc=" + $scope.sims_grade_code + "&sc=" + $scope.sims_section_name + "&search_stud=" + $scope.search_text + "&search_flag=" + $scope.search_by_code).then(function (GetAllStudentFee) {
                    $scope.GetAllStudentFee = GetAllStudentFee.data;
                    if ($scope.GetAllStudentFee.length != 0) {
                        $scope.currentPage = '1';
                        $scope.grid = true;
                        $scope.maintable = true;
                        $scope.busyindicator = false;
                        $scope.totalItems = $scope.GetAllStudentFee.length;
                        $scope.todos = $scope.GetAllStudentFee;
                        $scope.makeTodos();
                    }
                    else {
                        $scope.maintable = false;
                        $scope.busyindicator = false;
                        swal({ text: 'Data Not Found',imageUrl: "assets/img/close.png", width: 300, showCloseButton: true });
                    }
                    $scope.busyindicator = false;
                });
            }

            $scope.size = function (str) {

                if (str == 'All') {

                    $scope.totalItems = $scope.GetAllStudentFee.length;
                    $scope.todos = $scope.GetAllStudentFee;
                    $scope.filteredTodos = $scope.GetAllStudentFee;
                    $scope.numPerPage = $scope.GetAllStudentFee.length;
                    $scope.maxSize = $scope.GetAllStudentFee.length
                    $scope.makeTodos();
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.fee_types = [];

            
            $('#cmb_fee_Code').multipleSelect({
                onClick: function (arg1,arg2) {
                    debugger;
                   
                    if (!arg1.checked) {
                        for (var j = 0; j < $scope.Fee_Details.length; j++) {
                            if (arg1.value == $scope.Fee_Details[j].std_fee_code)
                                $scope.Fee_Details.splice(j, 1);
                        }
                    }
                 
                },
                onUncheckAll: function (arg1, arg2) {
                    debugger;
                   
                    for (var i = 0; i < $scope.FeeDetailFeetype.length; i++) {
                        for (var j = 0; j < $scope.Fee_Details.length; j++) {

                            if ($scope.Fee_Details[j].std_fee_code == $scope.FeeDetailFeetype[i].sims_fee_code) {
                              
                                $scope.Fee_Details.splice(j, 1);
                            }

                        }
                    }
                }
                
            });
            
           

            $scope.getAllFeedata = function (info) {
                debugger
                $scope.Fee_Details = [];
                if (info.std_fee_enroll_number != '') {
                    $scope.edt = info;

                    if ($scope.sims_fee_code == "" || $scope.sims_fee_code == undefined) {
                        $scope.sims_fee_code = null;
                    }

                    $scope.Student_name = info.std_fee_student_name + ' (' + info.std_fee_enroll_number + ') / '+info.std_fee_Class;
                    $scope.student_parent_name = info.std_fee_parent_name;
                    $http.get(ENV.apiUrl + "api/DebitCreditNote/getAllFeeDetailsStudent?enroll=" + info.std_fee_enroll_number + "&cur_code=" + info.std_fee_cur_code + "&aca_year=" + info.std_fee_academic_year + "&Fee_code=" + $scope.sims_fee_code).then(function (AdjustFeeDetails) {
                        $scope.Fee_Details = angular.copy(AdjustFeeDetails.data);
                        $scope.Fee_Details1 = angular.copy(AdjustFeeDetails.data);
                        if ($scope.Fee_Details.length > 0) {
                            $scope.grid = false;
                       //     if ($scope.fee_types.length <= 0) {
                                $http.get(ENV.apiUrl + "api/DebitCreditNote/GetSimsFeeType?cc=" + info.std_fee_cur_code + "&ay=" + info.std_fee_academic_year + "&gd=" + info.std_fee_grade_code + "&sc=" + info.std_fee_section_code + "&sims_enroll_number=" + info.std_fee_enroll_number).then(function (fee_types) {
                                    $scope.fee_types = fee_types.data;
                                    $scope.FeeDetailFeetype = fee_types.data;

                                    setTimeout(function () {
                                        $('#cmb_fee_Code').change(function () {
                                          //  console.log($(this).val());
                                        }).multipleSelect({
                                            width: '100%'
                                        });
                                    }, 1000);
                                });
                            }
                      //  }
                        else {
                            swal({ text: 'Data Not Found', imageUrl: "assets/img/close.png",width: 300, height: 250, showCloseButton: true });
                        }
                    });
                }
            }

            $scope.getNewNoteData = function (info) {
                debugger
            
            $scope.sims_fee_code_len=$scope.sims_fee_code.length;
            if ($scope.sims_fee_code_len > 0) {
           
                $scope.Fee_Details_t = [];
                $scope.Fee_Details_temp = [];
                $scope.grid = false;
                for (var i = 0; i<$scope.FeeDetailFeetype.length; i++)
                {
                    for (var j = 0;j<$scope.sims_fee_code.length; j++) { 

                        if ($scope.sims_fee_code[j] == $scope.FeeDetailFeetype[i].sims_fee_code) {
                            $scope.Fee_Details_t.push($scope.FeeDetailFeetype[i]);

                        }
                           
                    }
                }

                for (var i = 0; i < $scope.Fee_Details_t.length; i++) {
                    var abc = {
                        std_fee_type: $scope.Fee_Details_t[i].sims_fee_code_desc,
                        std_fee_child_period: $scope.Fee_Details_t[i].period_desc,
                        std_fee_child_exp_amount: $scope.Fee_Details_t[i].sims_fee_amount,
                        std_fee_change_amount: 0,
                        std_fee_child_paid_amount: 0,
                        sims_fee_code: $scope.Fee_Details_t[i].sims_fee_code,
                        std_fee_grade_code: info.std_fee_grade_code,
                        std_fee_section_code: info.std_fee_section_code,
                        std_fee_cur_code: info.std_fee_cur_code,
                        std_fee_academic_year: info.std_fee_academic_year,
                        std_fee_code: $scope.Fee_Details_t[i].sims_fee_code,
                        std_fee_enroll_number: info.std_fee_enroll_number,
                        std_fee_number: $scope.Fee_Details_t[i].sims_fee_code,
                        std_fee_child_remaining_amount: 0,
                        std_fee_child_period_No: $scope.Fee_Details_t[i].period_code,
                        std_fee_remaining: 0,
                        main_amount:0
                    }

                    $scope.Fee_Details_temp.push(abc);
                }
                console.log($scope.Fee_Details_temp);
              
                for (var i = 0; i < $scope.Fee_Details_temp.length; i++)
                {
                    var flag = 0;
                   
                    for (var j = 0; j < $scope.Fee_Details.length; j++) {
                        if ($scope.Fee_Details_temp[i].std_fee_code == $scope.Fee_Details[j].std_fee_code) {
                            flag = 1;
                            
                        }
                       
                    }
                    
                    debugger;
                    if (flag == 0) {
                        $scope.Fee_Details.push($scope.Fee_Details_temp[i]);
                        
                    }
                    
                }
                
                }
                    
            }

            $scope.CheckAmount = function (str,flag) {
                debugger
                $scope.sum_flag = 1;
              //  if ($scope.sims_parameter != 'DR') {
                    $scope.main_amount = 0;
                    if ($scope.sims_parameter == 'DR' || $scope.sims_parameter == 'IN') {
                        $scope.sum_flag = 1;
                    } else {
                        $scope.sum_flag = 0;
                    }
                    if (str.std_fee_remaining != '' && str.std_fee_remaining != undefined && str.std_fee_remaining != '0') {
                        if ($scope.sum_flag==0) {
                            str.std_fee_change_amount = parseFloat(str.std_fee_remaining) - parseFloat(str.std_fee_child_exp_amount);
                        } else {
                            str.std_fee_change_amount = parseFloat(str.std_fee_remaining) + parseFloat(str.std_fee_child_exp_amount);
                        }

                        if (str.std_fee_change_amount < 0) {
                            str.std_fee_change_amount = -1 * str.std_fee_change_amount;
                        }
                    }
                    else {
                        str.std_fee_change_amount = 0;
                    }

                    for (var i = 0; i < $scope.Fee_Details.length; i++) {
                        if ($scope.Fee_Details[i].std_fee_remaining > 0) {
                            if ($scope.sum_flag == 0) {
                                //$scope.main_amount = parseFloat($scope.main_amount) + parseFloat($scope.Fee_Details[i].std_fee_change_amount);
                                $scope.main_amount = parseFloat($scope.main_amount) - parseFloat($scope.Fee_Details[i].std_fee_remaining);
                            } else {
                                $scope.main_amount = parseFloat($scope.main_amount) + parseFloat($scope.Fee_Details[i].std_fee_remaining);
                            }
                        }
                    }
                    if (str.std_fee_remaining == '0') {
                        //str.std_fee_change_amount = '-' + str.std_fee_child_exp_amount;
                        str.std_fee_change_amount = str.std_fee_child_exp_amount;
                    }

                    console.log("$scope.main_amount", $scope.main_amount);
            }

            $scope.Back = function () {
                $scope.grid = true;
                $scope.edt = { std_fee_enroll_number: '' };
                $scope.fee_types = [];

            }

            $scope.getParameterDetails = function () {
                //for (var i = 0; i < $scope.parameter.length; i++) {
                //    if ($scope.parameter[i].gldc_doc_code == $scope.sims_parameter) {
                //        $scope.doc_code = $scope.parameter[i].gldc_doc_code;
                //        $scope.doc_type = $scope.parameter[i].doc_type;
                //        $scope.adjust_fee_code = $scope.parameter[i].fee_code;
                //    }
                //}
            }


            $scope.Submit_Data = function () {
                $scope.checkstatus = false;
                for (var i = 0; i < $scope.Fee_Details.length; i++) {
                    if ($scope.Fee_Details[i].status == true) {
                        $scope.checkstatus = true;
                        break;
                    }
                }

                $scope.main_Data_Object = [];

                $scope.SendData = [];

                for (var i = 0; i < $scope.parameter.length; i++) {
                    if ($scope.parameter[i].gldc_doc_code == $scope.sims_parameter) {
                        $scope.doc_code = $scope.parameter[i].gldc_doc_code;
                        $scope.doc_type = $scope.parameter[i].doc_type;
                        $scope.adjust_fee_code = $scope.parameter[i].fee_code;
                    }
                }
                var data = {};
                debugger
                data.doc_date = $scope.payment_date;
                data.doc_total_amount = $scope.main_amount;
                data.doc_enroll_no = $scope.Fee_Details[0].std_fee_enroll_number;
                data.doc_narration = $scope.doc_narration;
                data.doc_date = $scope.payment_date;
                data.std_fee_student_name = user;
                data.std_fee_academic_year = $scope.Fee_Details[0].std_fee_academic_year
                data.std_fee_cur_code = $scope.Fee_Details[0].std_fee_cur_code;
                data.std_fee_grade_code = $scope.Fee_Details[0].std_fee_grade_code;
                data.std_fee_section_code = $scope.Fee_Details[0].std_fee_section_code;
                data.bill_date = $scope.doc_date,
                data.doc_code = $scope.doc_code,
                data.doc_type = $scope.doc_type,
                data.adjust_fee_code = $scope.adjust_fee_code 
                var data2 = data;
       
                data2.std_fee_enroll_number = $scope.Fee_Details[0].std_fee_enroll_number;
                if ($scope.checkstatus == true) {

                    $http.post(ENV.apiUrl + "api/DebitCreditNote/CUDDebitCreditNoteFee_Document", data).then(function (fee_types) {
                        $scope.fee_types = fee_types.data;
                        if ($scope.fee_types != '') {

                            for (var i = 0; i < $scope.Fee_Details.length; i++) {
                                if ($scope.Fee_Details[i].status == true) {
                                    var data = {};

                                    data.doc_no = $scope.fee_types;
                                    data.dd_fee_number = $scope.Fee_Details[i].std_fee_number;
                                    data.PayDisPeriodNo = $scope.Fee_Details[i].std_fee_child_period_No;
                                    data.PayAmount = $scope.Fee_Details[i].std_fee_remaining;//std_fee_change_amount;
                                    data.FeeExpected = $scope.Fee_Details[i].std_fee_child_exp_amount
                                    data.doc_narration = $scope.doc_narration;
                                    data.std_fee_academic_year = $scope.Fee_Details[i].std_fee_academic_year

                                    $scope.SendData.push(data);
                                }
                            }

                            $http.post(ENV.apiUrl + "api/DebitCreditNote/CUDDebitCreditNoteFee_Document_Details", $scope.SendData).then(function (mess) {
                                $scope.MSG = mess.data;
                                if ($scope.MSG == true) {
                                    swal({ text: 'Note Created Successfully', imageUrl: "assets/img/check.png", width: 300, height: 250, showCloseButton: true });
                                    $scope.sims_fee_code = '';
                                    $scope.getAllFeedata(data2);
                                } else {
                                    swal({ text: 'Note Not Created', imageUrl: "assets/img/close.png",width: 300, height: 250, showCloseButton: true });

                                }
                            });
                        }
                    });
                } else {
                    swal({ text: 'Select Atleast One Record To Update Fee', imageUrl: "assets/img/notification-alert.png",width: 350, height: 250, showCloseButton: true });
                }
            }



            $scope.print = function () {
                debugger;
                $scope.printclick = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var docHead = document.head.outerHTML;
                        var printContents = document.getElementById('fixTable').outerHTML;
                        var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                        var newWin = window.open("", "_blank", winAttr);
                        var writeDoc = newWin.document;
                        writeDoc.open();
                        writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                        writeDoc.close();
                        newWin.focus();
                        $scope.Show_Data();

                    }
                    $scope.Show_Data();
                });

            };
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);
        }]);
})();