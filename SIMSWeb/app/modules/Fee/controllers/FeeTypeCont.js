﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('FeeTypeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.feetype_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            $scope.t = false;

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $timeout(function () {
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $http.get(ENV.apiUrl + "api/common/FeeType/getFeeTypeByIndex").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.feetype_data = res.data;
                $scope.totalItems = $scope.feetype_data.length;
                $scope.todos = $scope.feetype_data;
                $scope.makeTodos();
                $scope.grid = true;
            });


            $scope.percentage = function (str) {
                debugger
                if (str > 100) {

                    swal({ title: "Alert", text: "VAT% less than 100", showCloseButton: true, width: 380, })
                    $scope.edt.sims_fee_code_vat_percentage = '';
                }
                else {
                    $scope.edt.sims_fee_code_vat_percentage = str;
                }
            }


            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edit = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = false;
                    $scope.update1 = true;
                    $scope.delete1 = false;
                    //$scope.edt = str;

                    $scope.edt =
                        {
                            sims_fee_code: str.sims_fee_code,
                            sims_fee_code_desc: str.sims_fee_code_desc,
                            sims_fee_code_status: str.sims_fee_code_status,
                            fee_recievable_staus: str.fee_recievable_staus,
                            fee_applicable_status: str.fee_applicable_status,
                            sims_fee_code_overpaid: str.sims_fee_code_overpaid,
                            sims_section_ob_fee_code: str.sims_section_ob_fee_code,
                            sims_fee_code_vat_percentage: str.sims_fee_code_vat_percentage,
                            sims_fee_Exampted: str.sims_fee_Exampted,
                        }

                    $scope.t = true;
                }
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.edt = {};
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = true;
                    $scope.update1 = false;
                    $scope.delete1 = false;
                    $scope.t = false;
                    $scope.edt.sims_fee_code_status = true;
                }
            }

            $scope.cancel = function () {
                $scope.edt = {};
                $scope.grid = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }


            $scope.Save = function (isvalidate) {

                var data1 = [];

                if (isvalidate) {
                    if ($scope.update1 == false) {
                        $http.post(ENV.apiUrl + "api/common/FeeType/CheckTypeCode?feecode=" + $scope.edt.sims_fee_code).then(function (res) {
                            $scope.msg1 = res.data;
                            if ($scope.msg1.status == true) {
                                swal({  text: "Fee Type Already Exists",imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });



                                $scope.Myform.$setPristine();
                                $scope.Myform.$setUntouched();
                            }
                            else {
                                var data = ({
                                    sims_fee_code: $scope.edt.sims_fee_code,
                                    sims_fee_code_desc: $scope.edt.sims_fee_code_desc,
                                    sims_fee_code_status: $scope.edt.sims_fee_code_status,
                                    fee_recievable_staus: $scope.edt.fee_recievable_staus,
                                    fee_applicable_status: $scope.edt.fee_applicable_status,
                                    sims_fee_code_overpaid: $scope.edt.sims_fee_code_overpaid,
                                    sims_section_ob_fee_code: $scope.edt.sims_section_ob_fee_code,
                                    sims_fee_code_vat_percentage: $scope.edt.sims_fee_code_vat_percentage,
                                    sims_fee_Exampted: $scope.edt.sims_fee_Exampted,
                                    opr: 'I'
                                });

                                data1.push(data);

                                $http.post(ENV.apiUrl + "api/common/FeeType/CUDFeeType", data1).then(function (res) {
                                    $scope.display = true;
                                    $scope.msg1 = res.data;
                                    if ($scope.msg1 == true) {
                                        swal({  text: "Fee Type Added Successfully",imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({  text: "Fee Type Not Added. ",imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }

            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/common/FeeType/getFeeTypeByIndex").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.feetype_data = res.data;
                    $scope.totalItems = $scope.feetype_data.length;
                    $scope.todos = $scope.feetype_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }


            $scope.Update = function (isvalidate) {
                debugger
                var data1 = [];

                if (isvalidate) {
                    var data = ({
                        sims_fee_code: $scope.edt.sims_fee_code,
                        sims_fee_code_desc: $scope.edt.sims_fee_code_desc,
                        sims_fee_code_status: $scope.edt.sims_fee_code_status,
                        fee_recievable_staus: $scope.edt.fee_recievable_staus,
                        fee_applicable_status: $scope.edt.fee_applicable_status,
                        sims_fee_code_overpaid: $scope.edt.sims_fee_code_overpaid,
                        sims_section_ob_fee_code: $scope.edt.sims_section_ob_fee_code,
                        sims_fee_code_vat_percentage: $scope.edt.sims_fee_code_vat_percentage,
                        sims_fee_Exampted: $scope.edt.sims_fee_Exampted,
                        opr: 'U'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/FeeType/CUDFeeType", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({  text: "Fee Type Updated Successfully",imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else if ($scope.msg1 == false) {
                            swal({  text: "Fee Type Not Updated Successfully. ",imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.Delete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    var deletecode = [];
                    $scope.flag = false;

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sims_fee_code;
                        var v = document.getElementById(t);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodercode = ({
                                'sims_fee_code': $scope.filteredTodos[i].sims_fee_code,
                                'opr': 'D'
                            });
                            deletecode.push(deletemodercode);
                        }
                    }

                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                $http.post(ENV.apiUrl + "api/common/FeeType/CUDFeeType", deletecode).then(function (res) {
                                    $scope.msg1 = res.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Fee Type  Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Record Already Mapped.Can't be Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }

                                });
                            }
                            else {
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }

                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    //var v = document.getElementById(i);
                                    var t = $scope.filteredTodos[i].sims_fee_code;
                                    var v = document.getElementById(t);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }


                                }

                            }

                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sims_fee_code;
                        var v = document.getElementById(t);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var t = $scope.filteredTodos[i].sims_fee_code;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.size = function (str) {
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;  $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.feetype_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.feetype_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.feetype_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sims_fee_code;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_fee_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_fee_code_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

        }])
})();