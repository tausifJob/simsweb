﻿(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [];
    var doc_no;
    var simsController = angular.module('sims.module.Fee');
    simsController.controller('ClasswiseFeeReceiptMultipleCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.pager = true;
            $scope.pagggg = false;
            var user = $rootScope.globals.currentUser.username;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;



            



            //CUR
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.temp = {
                    'sims_cur_code': $scope.curriculum[0].sims_cur_code
                }
                $scope.getAccYear($scope.temp.sims_cur_code)
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    debugger;
                    $scope.temp = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                        'sims_academic_year': $scope.Acc_year[0].sims_academic_year
                    }
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                });

            }

            //Grade
            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
                $scope.getSection($scope.temp.sims_cur_code, $scope.temp.sims_grade_code, $scope.temp.sims_academic_year)
            }

            //Section
            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }


            debugger
            //$http.post(ENV.apiUrl + "http://api.mograsys.com/APIERP/api/StudentFee/GetReceipt").then(function (res) {
            //    debugger
            //    $scope.reportparameter = res.data;
            //    console.log($scope.reportparameter);

            //});


            $scope.size = function (str) {

                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.FeeReceiptData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            //$scope.createdate = function (end_date, start_date, name) {
            //    var month1 = end_date.split("/")[0];
            //    var day1 = end_date.split("/")[1];
            //    var year1 = end_date.split("/")[2];
            //    var new_end_date = year1 + "/" + month1 + "/" + day1;
            //    //var new_end_date = day1 + "/" + month1 + "/" + year1;

            //    var year = start_date.split("/")[0];
            //    var month = start_date.split("/")[1];
            //    var day = start_date.split("/")[2];
            //    var new_start_date = year + "/" + month + "/" + day;
            //    // var new_start_date = day + "/" + month + "/" + year;

            //    if (new_end_date < new_start_date) {
            //        swal({ title: 'Please Select Next Date From ', width: 380, height: 100 });
            //        $scope.edt[name] = '';
            //    }
            //    else {
            //        $scope.edt[name] = new_end_date;
            //    }
            //}

            $scope.paginationview1 = document.getElementById("paginationview");

            $scope.showdate = function (date, name) {
                debugger
                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //$scope.edt[name] = year + "/" + month + "/" + day;
                $scope.edt[name] = date;
                

            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.FeeReceiptData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.FeeReceiptData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked()

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.doc_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }


            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            //$scope.sdate = yyyy + '-' + mm + '-' + dd;
            //$scope.edate = yyyy + '-' + mm + '-' + dd;
            //$scope.edt = {
            //    from_date: yyyy + '-' + mm + '-' + dd,
            //    to_date: yyyy + '-' + mm + '-' + dd,

            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.edt = {
                from_date: dd + '-' + mm + '-' + yyyy,
                to_date: dd + '-' + mm + '-' + yyyy,

                
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.onKeyPress = function ($event) {
                debugger;
                if ($event.keyCode == 13) {

                    $scope.Show($scope.edt.from_date, $scope.edt.to_date, $scope.edt.search);
                    // console.log("enetr");
                }
            };


            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + 5);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + 5);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.Show = function () {
                debugger
                $http.get(ENV.apiUrl + "api/FeeReceipt/getReoprtParamenterClasswise").then(function (parameter) {
                    $scope.report_parameter = parameter.data;
                    console.log($scope.report_parameter)
                });



                if ($scope.edt.search == undefined || $scope.edt.search == "") {
                    $scope.edt.search = '';
                }
                $scope.table1 = true;
                $scope.pagggg = true;
                $scope.ImageView = false; 
                $http.get(ENV.apiUrl + "api/FeeReceipt/getAllRecordsClasswiseFeeReceipt?sims_cur_code=" + $scope.temp.sims_cur_code + "&sims_academic_year=" + $scope.temp.sims_academic_year + "&sims_grade_code=" + $scope.temp.sims_grade_code + "&sims_section_code=" + $scope.temp.sims_section_code + "&from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date).then(function (FeeReceipt_Data) {
                    $scope.FeeReceiptData = FeeReceipt_Data.data;
                    $scope.totalItems = $scope.FeeReceiptData.length;
                    $scope.todos = $scope.FeeReceiptData;
                    $scope.makeTodos();
                    console.log($scope.FeeReceiptData);
                    if (FeeReceipt_Data.data.length > 0) { }
                    else {
                        $scope.ImageView = true;
                        $scope.pager = false;
                    }
                });
                //}
            }

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.edt["from_date"] = '';
                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.edt = {
                            //from_date: yyyy + '-' + mm + '-' + dd,
                            //to_date: yyyy + '-' + mm + '-' + dd,
                            from_date: dd + '-' + mm + '-' + yyyy,
                            to_date: dd + '-' + mm + '-' + yyyy,
                        }

                    }
                });
            });



          

            $scope.Report = function () {
                debugger;
                var numbers=[];
                var maindocs=[];
                var concats;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i+5);
                    if (v.checked == true) 
                    {
                        $scope.numbers = $scope.filteredTodos[i].doc_no;
                        maindocs.push($scope.numbers);
                        concats += $scope.numbers + ',';
                    }

                }

                if ($scope.temp.sims_grade_code == undefined) {
                    $scope.temp.sims_grade_code = null;
                    $scope.edt.from_date=null;
                }

                if ($scope.temp.sims_section_code == undefined) {
                    $scope.temp.sims_section_code = null;
                }


                        var data = {
                           
                            location: $scope.report_parameter[0].sims_appl_parameter,//'Sims.SIMR51Classwise1',
                            parameter: {
                                doc_no: maindocs+',',
                                cur_code:$scope.temp.sims_cur_code,
                                acad_year:$scope.temp.sims_academic_year,
                                grade_code:$scope.temp.sims_grade_code,
                                Section:$scope.temp.sims_section_code,
                                from_date:$scope.edt.from_date,
                                to_date:$scope.edt.to_date
                            },
                            state: 'main.SisCla',
                }
                   
                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                    $state.go('main.ReportCardParameter')
               
            }

        }]
        )
})();