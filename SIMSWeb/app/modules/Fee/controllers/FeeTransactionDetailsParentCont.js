﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, modulecode = [];
    var data = [], data1 = [];
    var simsController = angular.module('sims.module.Fee');

    simsController.controller('FeeTransactionDetailsParentCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.busyindicator = false;
            $scope.hide = true;
            $scope.hide2 = false;
           
            var objnew = {};

            $http.post(ENV.apiUrl + "api/common/UserApplicationsController/FeeTransactionDetails", objnew).then(function (res) {

                $scope.rows = res.data.table;
                $scope.Allrows = res.data.table1;
                $scope.Allrowslin = res.data.table2;
                if ($scope.Allrowslin.length <= 0) {
                    swal({  text: "Records not found", imageUrl: "assets/img/close.png",showCloseButton: true, width: 380, });
                    $scope.hide2 = false;
                }
                else {
                    for (var r in $scope.rows) {
                        $scope.rows[r]['sims_icon'] = "fa fa-minus-circle";
                        $scope.rows[r]['isexpanded'] = "grid";
                        $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].temp_dd_enroll_number + $scope.rows[r].temp_dd_fee_transaction_id);
                    }
                    $scope.hide2 = true;
                }
               
            });

            //$scope.Show_Data();


            $scope.approve_Data = function (str) {
                
                var data_lst = [];

                for (var i = 0; i < str.length; i++) {
                    if (  str[i]['approve_status']) {
                        var ins_obj = {
                            temp_dd_doc_no: str[i].temp_dd_doc_no,
                            temp_dd_realize_date: $scope.temp.sims_realizedate
                        }

                        data_lst.push(ins_obj);
                    }
                }
               
                $http.post(ENV.apiUrl + "api/common/UserApplicationsController/CUD_fee_doc", data_lst).then(function (msg) {
                    //  $scope.msg1 = msg.data;
                    //$scope.refNumbertrans = parseInt(msg.data);
                    if (msg.data ) {
                        swal({  text: "Posted..", width: 380, height: 200 });
                    }
                    else {
                        swal({  text: "Posted..", width: 380, height: 200 });


                    }

                });

            }
           
            $scope.Show_Data = function () {

                var objnew = {
                    sims_cur_code: $scope.edt.sims_cur_code,
                    sims_academic_year: $scope.temp.sims_academic_year,
                    sims_grade_code: $scope.edt.sims_grade_code + '',
                    sims_section_code: $scope.edt.sims_section_code + '',
                    temp_dd_enroll_number: $scope.edt.temp_dd_enroll_number,
                    sims_target_code: $scope.edt.sims_target_code + '',
                    sims_target_code_line_no: $scope.target.sims_target_code_line_no + '',
                    sims_from_date: fd,
                    sims_upto_date: ud,
                }

                $http.post(ENV.apiUrl + "api/common/UserApplicationsController/FeeTransactionDetails", objnew).then(function (res) {

                    $scope.rows = res.data.table;
                    $scope.Allrows = res.data.table1;
                    $scope.Allrowslin = res.data.table2;
                    if ($scope.Allrowslin.length <= 0) {
                        swal({  text: "Records not found",imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                        $scope.hide2 = false;
                    }
                    else {
                        for (var r in $scope.rows) {
                            $scope.rows[r]['sims_icon'] = "fa fa-minus-circle";
                            $scope.rows[r]['isexpanded'] = "grid";
                            $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].temp_dd_enroll_number);
                        }
                        $scope.hide2 = true;
                    }
                    
                });
            }

            function getSubitems(dno) {
                var arr = [];
                for (var i = 0; i < $scope.Allrows.length; i++) {
                    if ($scope.Allrows[i].temp_dd_enroll_number + $scope.Allrows[i].temp_dd_fee_transaction_id == dno) {
                        $scope.Allrows[i]['sims_icon'] = "fa fa-minus-circle";
                        $scope.Allrows[i]['isexpanded'] = "grid";
                        $scope.Allrows[i]['subItems1'] = getSubitems1(dno, $scope.Allrows[i].temp_dd_fee_code);
                        arr.push($scope.Allrows[i]);
                    }
                }
                return arr;
            }

            function getSubitems1(dno, tno) {
              
                var arr1 = [];
                for (var i = 0; i < $scope.Allrowslin.length; i++) {
                    if ($scope.Allrowslin[i].temp_dd_enroll_number + $scope.Allrowslin[i].temp_dd_fee_transaction_id == dno && $scope.Allrowslin[i].temp_dd_fee_code == tno) {
                        arr1.push($scope.Allrowslin[i]);
                    }
                }
                return arr1;
            }

            $scope.ordclick = function (gh) {
                var l = gh.subItems.length;
                for (var i = 0; i < l; i++) {
                    gh.subItems[i]['isChecked'] = gh.isChecked;
                }
            }

            $scope.ordlineclick = function (r, m) {
                var l = r.subItems.length;
                var f = false;
                if (m) {
                    f = true;
                    for (var i = 0; i < l; i++) {
                        f = f && r.subItems[i]['isChecked'];
                    }
                }
                r.isChecked = f;
            }

            $scope.expand = function (j, $event) {
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none";
                    if ($scope.Allrowslin.length <= 0) {
                        swal({  text: "Records not found", showCloseButton: true, width: 380, });
                        $scope.hide2 = false;
                    }
                    else {
                        for (var r in $scope.rows) {
                            $scope.rows[r]['sims_icon'] = "fa fa-plus-circle";
                            $scope.rows[r]['isexpanded'] = "none";
                            $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].temp_dd_enroll_number + $scope.rows[r].temp_dd_fee_transaction_id);
                        }
                        $scope.hide2 = true;
                    }
                   
                }
            }

            $scope.expand1 = function (j, $event) {
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none";
                }
            }

            $scope.click_one_by = function (str) {
                }

            var datasend = [];
            var datasend1 = [];
              $('*[data-datepicker="true"] input[type="text"]').datepicker(
                {
                    todayBtn: true,
                    orientation: "top left",
                    autoclose: true,
                    todayHighlight: true,
                    format: 'yyyy-mm-dd',
                    //endDate:(new Date())
                });

              $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                  $('input[type="text"]', $(this).parent()).focus();
              });
          
        }
        ])
})();