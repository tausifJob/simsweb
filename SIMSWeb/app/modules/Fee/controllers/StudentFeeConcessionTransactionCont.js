﻿(function () {
    'use strict';
    var del = [], listofstudents;
    var myarray = [];
    var main, temp, global_Search, opr;
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
        simsController.controller('StudentFeeConcessionTransactionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            var datasend = [];
           
            $scope.sibling_block = false;
            $scope.display = false;
            $scope.table = true;
            $scope.operation = false;
            $scope.pagesize = "10";
            $scope.studenttable = false;
            $scope.sibtable = false;

            data();
            $scope.studentdata = false;
            $scope.global_Search = {};
            $scope.chck_obj = [];
            $scope.report_data = [];


            function data()
            {
                                
                $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getAllSimsConcessionTransaction").then(function (res) {
                    $scope.display = true;
                    $scope.obj = res.data;
                    $scope.table = true;
                    datasend = [];
                    $scope.operation = false;
                    if ($scope.obj.length != 0) {
                        $scope.totalItems = $scope.obj.length;
                        $scope.todos = $scope.obj;
                        $scope.makeTodos();

                    }
                    else {
                        $scope.table = true;
                        $scope.display = false;
                        $scope.operation = false;
                        swal({ text: 'Sorry Data Not Found', width: 380, imageUrl: "assets/img/close.png",showCloseButton: true });
                    }

                });
            }

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.checkAll();
                }
            };

            $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getAllCurName").then(function (res) {
                $scope.Cur_Names = res.data;
                $scope.global_Search['global_curriculum_code'] = $scope.Cur_Names[0].sims_cur_code;
            });

            $scope.Cancel = function () {
                $scope.sibling_block = false;
               
                
                $scope.table = true; $scope.temp = "";
                $scope.operation = false;

            }

            $scope.edit = function (str) {
                
                $scope.opr = '';
                $scope.studenttable = false;
                $scope.sibtable = false;

                $scope.table = false;
                $scope.operation = true;
                $scope.opr = 'U';
                $scope.opr = 'Y';

                $scope.edtobj = angular.copy(str);
                var data = angular.copy(str);
                $scope.temp = data;

                $scope.date = data.sims_created_date;

                $scope.Save_btn = false; $scope.Update_btn = true; $scope.readonly = true; $scope.multi_stud_select = false;
                del = []; myarray = []; $scope.opr = 'U';

                $scope.Save_sibling = false; $scope.Update_btn = true; $scope.readonly = true; $scope.multi_stud_select = false;
                del = []; myarray = []; $scope.opr = 'Y';
                

                $scope.getAcadm_yr1(data.sims_cur_short_name);

               //  $scope.getconcession_description1(str.sims_academic_year)

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.checkAll();
                }
            }

            $scope.getAcadm_yr = function (cur_code) {
                $scope.curriculum_code = cur_code;
                $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getAllACAYearCurCode?curcode=" + cur_code).then(function (res) {
                    $scope.Acdm_year = res.data;
                    $scope.global_Search['global_acdm_yr'] = $scope.Acdm_year[0].sims_academic_year;
                    $scope.temp['sims_fee_academic_year'] = $scope.Acdm_year[0].sims_academic_year;
                    $scope.getGrade($scope.temp.sims_cur_short_name, $scope.temp.sims_fee_academic_year);
                   $scope.getconcession_description($scope.Acdm_year[0].sims_academic_year);
                    
                });
            }


            $scope.getSection = function (cur_code, academic_year, grade_code) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.temp.sims_cur_short_name + "&grade_code=" + $scope.temp.sims_grade_code + "&academic_year=" + $scope.temp.sims_fee_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#section').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }

            $scope.getGrade = function (cur_code, academic_year) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.sims_cur_short_name + "&academic_year=" + $scope.temp.sims_fee_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);



                });
                $scope.getSection($scope.temp.sims_cur_code, $scope.temp.sims_academic_year, $scope.temp.grade_code);
                setTimeout(function () {
                    $('#grade').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);

            }

            $scope.getdetails = function () {
                $scope.sibling_data = true;
                 debugger;
                $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getSiblingConcession?cur_code=" + $scope.temp.sims_cur_short_name + "&acad_year=" + $scope.temp.sims_fee_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&search=" + $scope.temp.sims_search_id + "&sims_concession_number=" + $scope.temp.sims_concession_number).then(function (res1) {
                    $scope.report_data = res1.data;
                    $scope.totalItems = $scope.report_data.length;
                    $scope.todos = $scope.report_data;
                    $scope.makeTodos();

                });
            }





            $scope.getconcession_description = function (str) {

                $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getAllConcessionCode?academic_year=" + str).then(function (res) {
                    $scope.Concession_nums = res.data;


                    $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getAllConcessionDates?academic_year=" + str).then(function (res) {
                        $scope.dates = res.data;

                        $scope.temp['sims_concession_from_date'] = $scope.dates[0].sims_concession_from_date;
                        $scope.temp['sims_concession_to_date'] = $scope.dates[0].sims_concession_to_date;
                     //   $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                        
                    });
                });
            }
            $scope.getconcession_description_sibling = function (str) {

                $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getAllConcessionCodeSibling?academic_year=" + str).then(function (res) {
                    $scope.Concession_nums_sib = res.data;


                    $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getAllConcessionDates?academic_year=" + str).then(function (res) {
                        $scope.dates = res.data;

                        $scope.temp['sims_concession_from_date'] = $scope.dates[0].sims_concession_from_date;
                        $scope.temp['sims_concession_to_date'] = $scope.dates[0].sims_concession_to_date;
                        //   $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);

                    });
                });
            }

            $scope.getconcession_description1 = function (str) {

                $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getAllConcessionCode?academic_year=" + str).then(function (res) {
                    $scope.Concession_nums = res.data;

                })
            };

            $scope.getAcadm_yr1 = function (cur_code) {
                $scope.curriculum_code = cur_code;
                $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getAllACAYearCurCode?curcode=" + cur_code).then(function (res) {
                    $scope.Acdm_year = res.data;
                    $scope.temp['sims_fee_academic_year'] = $scope.edtobj.sims_fee_academic_year;
                    $scope.getconcession_description1($scope.edtobj.sims_fee_academic_year)
                });
            }

            $scope.New = function () {
                debugger
                $scope.sibling_block = false;
                setTimeout(function () {
                    $(document).ready(function () {
                        $("#from_date, #to_date,#create_date").kendoDatePicker({
                            format: "dd-MM-yyyy",
                            value: ''
                        });
                    });
                }, 100);
              
                $scope.tenantForm.$setPristine();
                $scope.opr = 'I';
                $scope.dis_button = true;
                $scope.studenttable = true;
                var d = new Date();
                var year = d.getFullYear();
                var month = d.getMonth() + 1;
                if (month < 10) {
                    month = "0" + month;
                };
                var day = d.getDate();
                if (day < 10) {
                    day = "0" + day;
                };
               //$scope.date = year + "/" + month + "/" + day;
                $scope.date = day + "-" + month + "-" + year;

                $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getComnSequenceCode").then(function (res) {
                    $scope.Transaction_Number = res.data; 
                    $scope.temp = {
                        sims_transaction_number: $scope.Transaction_Number,
                        sims_created_date: $scope.date,
                        sims_created_by: $rootScope.globals.currentUser.username,
                        sims_status: true,
                        sims_cur_short_name: $scope.Cur_Names[0].sims_cur_code
                    };
                   

                    $scope.table = false; $scope.Save_btn = true; $scope.Update_btn = false; $scope.readonly = true; $scope.multi_stud_select = true;
                    $scope.operation = true; del = []; $scope.chck_obj = "";
                    $scope.getAcadm_yr($scope.Cur_Names[0].sims_cur_code);
                });

            }

            $scope.Sibling = function () {
                debugger
                $scope.operation = false;
                $scope.sibling_block = true;
                setTimeout(function () {
                    $(document).ready(function () {
                        $("#from_date, #to_date,#create_date").kendoDatePicker({
                            format: "dd-MM-yyyy",
                            value: ''
                        });
                    });
                }, 100);

                $scope.tenantForm.$setPristine();
                $scope.opr = 'I';
                $scope.dis_button = true;
                $scope.sibtable = true;
                var d = new Date();
                var year = d.getFullYear();
                var month = d.getMonth() + 1;
                if (month < 10) {
                    month = "0" + month;
                };
                var day = d.getDate();
                if (day < 10) {
                    day = "0" + day;
                };
                //$scope.date = year + "/" + month + "/" + day;
                $scope.date = day + "-" + month + "-" + year;

                $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getComnSequenceCode").then(function (res) {
                    $scope.Transaction_Number = res.data;
                    $scope.temp = {
                        sims_transaction_number: $scope.Transaction_Number,
                        sims_created_date: $scope.date,
                        sims_created_by: $rootScope.globals.currentUser.username,
                        sims_status: true,
                        sims_cur_short_name: $scope.Cur_Names[0].sims_cur_code
                    };


                    $scope.table = false; $scope.Save_sibling = true; $scope.Update_btn = false; $scope.readonly = true; $scope.multi_stud_select = true;
                    del = []; $scope.report_data = "";
                    $scope.getAcadm_yr($scope.Cur_Names[0].sims_cur_code);
                });

            }


           
            $rootScope.visible_stud = true;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = false;

            $rootScope.chkMulti = true;
            $scope.Global_Search_modal = function () {

               
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }

            $scope.$on('global_cancel', function (str) {
                debugger
              
                var temp = [];
                var finalTemp = [];
                var falg = false;
                if ($scope.SelectedUserLst.length > 0) {
                         //  datasend.push(info)
                    if ($scope.chck_obj!='')
                    finalTemp = $scope.chck_obj;
                    
                    try {
                        for (var i = 0; i < $scope.SelectedUserLst.length; i++) {
                            falg = false;
                             for (var j = 0; j < $scope.chck_obj.length; j++) {
                                if ($scope.chck_obj[j].s_enroll_no == $scope.SelectedUserLst[i].s_enroll_no) {
                                    falg = true;
                                }
                             }
                             if (falg == false) {
                                 temp.push($scope.SelectedUserLst[i]);
                             }
                        }
                        if (temp.length > 0) {
                            $scope.chck_obj = [];
                            for (var i = 0; i < temp.length; i++) {
                                finalTemp.push(temp[i]);
                            }
                            $scope.chck_obj=finalTemp;
                            

                        }
                        else {
                            if (flag!=false) {
                                $scope.chck_obj = $scope.SelectedUserLst;
                            }
                        }
                    } catch (e) {

                    }
                }
            });

            $scope.chck_obj = [];

            //$scope.Gbl_checkbox_click = function (info) {
            //    if (info.ischecked == true) {
            //        datasend.push(info)
            //        $scope.chck_obj = datasend;
            //    }
            //    else {
            //        for (var i = 0; i < $scope.chck_obj.length; i++) {
            //            if (info.s_enroll_no == $scope.chck_obj[i].s_enroll_no) {

            //                $scope.chck_obj.splice(i, 1);
            //            }
            //        }
            //    }

            //}

            $scope.glbl_remove = function (index, str) {
                str.splice(index, 1);
            }

            $scope.SaveData = function (isvalid) {
                $scope.dis_button = true;
                if (isvalid) {
                    if ($scope.opr == 'U') {

                         
                        $http.post(ENV.apiUrl + "api/common/ConcessionTransaction/UpdateConcessionTransaction?data=" + JSON.stringify($scope.temp) + "&opr=U").then(function (res) {
                            $scope.Message = res.data;
                            swal({ text: $scope.Message.strMessage, width: 320, showCloseButton: true });
                            data();
                        });
                    }
                    else {

                        if ($scope.chck_obj.length > 0) {
                            var datasendObject = [];
                            for (var i = 0; i < $scope.chck_obj.length; i++) {
                                var data1 = {

                                  sims_transaction_number: $scope.temp.sims_transaction_number
                                , sims_cur_code: $scope.temp.sims_cur_short_name
                                , sims_fee_academic_year: $scope.temp.sims_fee_academic_year
                                , sims_enroll_number: $scope.chck_obj[i].s_enroll_no
                                , sims_concession_number: $scope.temp.sims_concession_number
                                , sims_concession_from_date: $scope.temp.sims_concession_from_date
                                , sims_concession_to_date: $scope.temp.sims_concession_to_date
                                , sims_created_by: $scope.temp.sims_created_by
                                , sims_created_date: $scope.temp.sims_created_date
                                , sims_status: $scope.temp.sims_status
                                , opr: 'I'
                                }
                                datasendObject.push(data1);
                            }

                            $http.post(ENV.apiUrl + "api/common/ConcessionTransaction/CUConcessionTransaction", datasendObject).then(function (res) {
                                
                                $scope.Message = res.data;

                                if ($scope.Message != "") {
                                    //swal({ alert: 'Concession Mapped Successfully', width: 320, showCloseButton: true });
                                    swal({ text: $scope.Message, width: 300, height: 200, showCloseButton: true });
                                    
                                    setTimeout(function () {
                                        $(document).ready(function () {
                                            $("#from_date, #to_date,#create_date").kendoDatePicker({
                                                format: "dd-MM-yyyy",
                                                value: ''
                                            });
                                        });
                                    }, 700);
                                    data();
                                }

                                else
                                {
                                    swal({ text: "Concession not applied", width: 300, height: 200, imageUrl: "assets/img/close.png", showCloseButton: true });
                                    setTimeout(function () {
                                        $(document).ready(function () {
                                            $("#from_date, #to_date,#create_date").kendoDatePicker({
                                                format: "dd-MM-yyyy",
                                                value: ''
                                            });
                                        });
                                    }, 700);
                                }

                            });
                        }
                        else {
                            swal({ text: 'Select At Least One Student To Insert Record', width: 380, imageUrl: "assets/img/notification-alert.png", showCloseButton: true });
                            setTimeout(function () {
                                $(document).ready(function () {
                                    $("#from_date, #to_date,#create_date").kendoDatePicker({
                                        format: "dd-MM-yyyy",
                                        value: ''
                                    });
                                });
                            }, 700);
                        }
                    }
                }
            }



            $scope.SaveSiblingData = function (isvalid) {
                debugger;
                //$scope.dis_button = true;
                $scope.sibling_block = false;
                //$scope.opr = 'U';
                if (isvalid) {
                    
                        if ($scope.report_data.length > 0) {
                            var datasendObject = [];
                            for (var i = 0; i < $scope.report_data.length; i++) {
                                var data1 = {

                                    sims_transaction_number: $scope.temp.sims_transaction_number
                                , sims_cur_code: $scope.temp.sims_cur_short_name
                                , sims_fee_academic_year: $scope.temp.sims_fee_academic_year
                                , sims_enroll_number: $scope.report_data[i].sims_enroll_number
                                , sims_concession_number: $scope.temp.sims_concession_number
                                , sims_concession_from_date: $scope.temp.sims_concession_from_date
                                , sims_concession_to_date: $scope.temp.sims_concession_to_date
                                , sims_created_by: $scope.temp.sims_created_by
                                , sims_created_date: $scope.temp.sims_created_date
                                , sims_status: $scope.temp.sims_status
                                , opr: 'I'
                                }
                                datasendObject.push(data1);
                            }

                            $http.post(ENV.apiUrl + "api/common/ConcessionTransaction/CUConcessionTransaction", datasendObject).then(function (res) {

                                $scope.Message = res.data;

                                if ($scope.Message != "") {
                                    //swal({ alert: 'Concession Mapped Successfully', width: 320, showCloseButton: true });
                                    swal({ text: $scope.Message, width: 300, height: 200, showCloseButton: true });

                                    setTimeout(function () {
                                        $(document).ready(function () {
                                            $("#from_date, #to_date,#create_date").kendoDatePicker({
                                                format: "dd-MM-yyyy",
                                                value: ''
                                            });
                                        });
                                    }, 700);
                                    data();
                                }

                                else {
                                    swal({ text: "Concession not applied", width: 300, height: 200, imageUrl: "assets/img/close.png", showCloseButton: true });
                                    setTimeout(function () {
                                        $(document).ready(function () {
                                            $("#from_date, #to_date,#create_date").kendoDatePicker({
                                                format: "dd-MM-yyyy",
                                                value: ''
                                            });
                                        });
                                    }, 700);
                                }

                            });
                        }
                        else {
                            swal({ text: 'Select At Least One Student To Insert Record', width: 380, imageUrl: "assets/img/notification-alert.png", showCloseButton: true });
                            setTimeout(function () {
                                $(document).ready(function () {
                                    $("#from_date, #to_date,#create_date").kendoDatePicker({
                                        format: "dd-MM-yyyy",
                                        value: ''
                                    });
                                });
                            }, 700);
                        }
                    
                }
            }

            

            $scope.getallsectioncode = function (gr_code) {
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getAllSectionName?gradecode=" + gr_code).then(function (res) {
                    $scope.All_Section_names = res.data;
                });
            }

        
      

            var dom;

            $scope.flag = true;
            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='12'>" +
                    "<table class='inner-table' width='100%' cellpadding='5' cellspacing='0' style='border-color:red;border:solid;border-width:02px'>" +
                    "<tbody>" +

                     "<tr> <td class='semi-bold'>" + "Concession Type" + "</td> <td class='semi-bold'>" + "Concession From Date" + "</td> <td class='semi-bold'>" + "Concession To Date" + " </td><td class='semi-bold'>" + "Created By" + "</td><td class='semi-bold'>"
                      + "Created Date" + "</td></tr>" +

                       "<tr><td>" + (info.sims_concession_description) + "</td> <td>" + (info.sims_concession_from_date) + "</td> <td>" + (info.sims_concession_to_date) + " </td><td>" + (info.sims_created_by) + "</td><td>"
                       + (info.sims_created_date)
                       + "</td></tr>" +
                       " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }

            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
                //format: 'dd-mm-yyyy'
            });

            $scope.size = function (str) {

                if (str == "All") {

                    $scope.filteredTodos = $scope.obj;
                    $scope.todos = $scope.obj;
                    $scope.totalItems = $scope.obj.length;
                    $scope.numPerPage = $scope.obj.length;
                    $scope.makeTodos();

                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();

                
            }

            $scope.checkAll = function () {

                main = document.getElementById('mainchk');
                del = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_transaction_number;
                    var v = document.getElementById(t);
                    if (main.checked == true) {
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                    else {
                        v.checked = false;
                        $scope.row1 = ''
                        $('tr').removeClass("row_selected");
                    }

                }
            }

            $scope.checkAny = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }

            }

            $scope.checkDate = function (from, to) {
                if (from > to) {
                    swal({ text: 'Select Future date', width: 300, showCloseButton: true });
                    $scope.temp.sims_concession_to_date = '';
                }

            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {

                
                return (item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.student_full_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.Delete = function () {

                var datasend = [];

                var sims_enroll_number = '';
                var sims_transaction_number = '';
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_transaction_number);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var data1 = {
                            opr: 'D',
                            sims_enroll_number: sims_enroll_number + $scope.filteredTodos[i].sims_enroll_number,
                            sims_transaction_number: sims_transaction_number + $scope.filteredTodos[i].sims_transaction_number
                        }
                        datasend.push(data1);
                    }
                }

                if ($scope.flag) {
                    swal({
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/common/ConcessionTransaction/SimsConcessionTransactionDelete", datasend).then(function (res) {
                                $scope.CUDobj = res.data;
                                swal({ text: $scope.CUDobj.strMessage, width: 380, showCloseButton: true });
                                data();
                            })
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                                $scope.row1 = '';
                            }
                            $scope.checkAll();
                        }
                    })
                }
                else {
                    swal({ text: 'Select At Least One Record To Delete', width: 380, imageUrl: "assets/img/notification-alert.png",showCloseButton: true });
                }
            }

            $scope.check = function () {

                var data2 = [];
                var data3 = [];
                var Checkbox = document.getElementById('Checkbox1');
                for (var i = 0; i < $scope.global_search_result.length; i++) {

                    var t = document.getElementById($scope.global_search_result[i].s_enroll_no);
                    if (Checkbox.checked == true) {
                        t.checked = true;
                        data2.push({ id: $scope.global_search_result[i].s_enroll_no, name: $scope.global_search_result[i].s_sname_in_english, s_enroll_no: $scope.global_search_result[i].s_enroll_no });
                        data3.push($scope.global_search_result[i].s_enroll_no);
                    }
                    else {
                        t.checked = false;
                        Checkbox.checked = false;
                        del = [];
                        myarray = [];
                    }
                }

                del = data2;
                myarray = data3;
                $scope.chck_obj = del;

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#Table3").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });

            }, 100);
            
            $(window).bind('keydown', function (event) {

                
                if ($scope.searchmodel == true && event.which=='13')
                {
                    
                    $scope.Global_Search_by_student();
                }
            });

            $scope.FocuslostFunction = function () {

                $scope.searchmodel = false;
            }
            $scope.dis_button = true;
            $scope.selectConcession=function(info)
            {
                if(info !='' && info !=undefined)
                {
                    $scope.dis_button = false;
                }
                else
                {
                    $scope.dis_button = true;
                }
            }
        }])
})();