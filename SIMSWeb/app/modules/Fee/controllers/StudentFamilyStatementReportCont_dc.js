﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('StudentFamilyStatementReportCont_dc',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {

            $scope.temp = {};
            $scope.detailsField = true;
            $scope.hideprintpdf = false;
            $scope.student_analysis = true;            
            $scope.temp.other_fee_flag = true;
            $scope.temp.cumfeeflag = false;
            $scope.pagesize = '50';
            $scope.pageindex = "0";
            $scope.busy = false;
            $scope.pager = true;
            $scope.SchoolLogo = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/SchoolLogo/';
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            var today = new Date();
            var dd = today.getDate();
            var user = $rootScope.globals.currentUser.username;
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.from_date = dd + '-' + mm + '-' + yyyy;
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.from_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.from_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });


            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_end_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['chk_all'] = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 50, $scope.maxSize = 100;

            $scope.makeTodos = function () {
                debugger;
                if ($scope.pagesize == 'All') {
                    $scope.currentPage = 1;
                    $scope.showPager = false;
                    $scope.filteredTodos = $scope.report_data_new;
                }
                else {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                }
            };


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res1) {
                $scope.cur_data = res1.data;
                $scope.temp.sims_cur_code = $scope.cur_data[0].sims_cur_code;
                $scope.getAcademic_year($scope.cur_data[0].sims_cur_code);
            });           

            $scope.getAcademic_year = function (cur_code1) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.temp.sims_cur_code).then(function (res1) {
                    $scope.acad_data = res1.data;
                    $scope.temp.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                });
            }

            $http.get(ENV.apiUrl + "api/family_statement/getParent_student_status").then(function (res1) {
                $scope.satus = res1.data;
                $scope.temp.parent_flag = $scope.satus[0].code;
                });            

            $scope.getdetail1 = function () {
                //debugger;
                $scope.colsvis = false;
                $scope.date = $scope.from_date;
                $scope.acad = $("#acad_box option:selected").text();
                $scope.cur = $("#cur_box option:selected").text();
                $scope.user = $("#user_box option:selected").text();
                debugger;
                 $scope.detailsField = true;
                $scope.busy = true;
               $http.get(ENV.apiUrl + "api/family_statement/getStudentParentDetails?acad_year=" + $scope.temp.sims_academic_year + "&cur_code=" + $scope.temp.sims_cur_code + "&parent_flag=" + $scope.temp.parent_flag).then(function (res1) {
                            if (res1.data.length > 0) {
                                $scope.report_data_new = res1.data;
                                $scope.total_length = angular.copy(res1.data.length);
                                $scope.totalItems = $scope.report_data_new.length;
                                $scope.todos = $scope.report_data_new;
                                $scope.makeTodos();                                                              
                                $scope.busy = false;
                                console.log("$scope.pagesize", $scope.pagesize)
                                var page_size = $("#Select1").val();
                                $scope.size(page_size);
                            }
                            else {
                                $scope.busy = false;
                                $scope.filteredTodos = [];
                                swal({ title: "Alert", text: "Data is not Found", showCloseButton: true, width: 300, height: 200 });
                                $scope.report_data_new = [];
                               
                            }
                            debugger;
                            if ($scope.temp.parent_flag == '2') {
                                $scope.family_analysis = true;
                                $scope.student_analysis = false;
                                 }
                            else {
                                $scope.family_analysis = false;
                                $scope.student_analysis = true;
                                
                            }
                        });
            }


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.report_data_new, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.report_data_new;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].report_data_new;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }

                }


            }


            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
            return (item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.class_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.family_id.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.father_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_parent_father_mobile.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.mother_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_parent_mother_mobile.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $scope.colsvis = false;


            $scope.exportData = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        if ($scope.temp.report_status == '2') {
                            var blob = new Blob([document.getElementById('pdf_print').innerHTML],
                             {
                                 type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                             });
                            $scope.detailsField = false;

                        }
                        else {

                            var blob = new Blob([document.getElementById('pdf_print1').innerHTML],
                            {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            $scope.showsumaryField = false;

                        }
                        $scope.detailsField = false;
                        saveAs(blob, "AnalysysReport.xls");
                        $scope.colsvis = false;
                        $scope.getDetail1();

                    }

                });
                $scope.colsvis = true;

            };


            $scope.print = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        if ($scope.temp.parent_flag == '2') {
                            var docHead = document.head.outerHTML;
                            var printContents = document.getElementById('rpt_data1').outerHTML;
                            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                            var newWin = window.open("", "_blank", winAttr);
                            var writeDoc = newWin.document;
                            writeDoc.open();
                            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                            writeDoc.close();
                            newWin.focus();
                            $scope.detailsField = false;
                            $scope.getDetail1();
                        }
                        else {
                            var docHead = document.head.outerHTML;
                            var printContents = document.getElementById('pdf_print1').outerHTML;
                            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                            var newWin = window.open("", "_blank", winAttr);
                            var writeDoc = newWin.document;
                            writeDoc.open();
                            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                            writeDoc.close();
                            newWin.focus();
                            $scope.getDetail1();

                        }
                        $scope.detailsField = false;
                        $scope.colsvis = false;
                        $scope.getDetail1();

                    }
                });
            };
            $scope.report_show = true;
            $scope.Report_call = function (r) {
                debugger;
                //$('#feemodal').modal({ backdrop: 'static', keyboard: false });
                $('#feemodal').modal('show');
                $scope.report_show = false;
                debugger
                $http.get(ENV.apiUrl + "api/family_statement/getReport_dc").then(function (res1) {
                    $scope.result1 = res1.data;
                    if ($scope.temp.parent_flag == "1") {
                        var data = {
                            location: $scope.result1[0].sims_appl_form_field_value1,//"Gradebook.GBR039QR", 
                            parameter: {
                                cur_code: r.sims_cur_code,
                                acad_year: r.sims_academic_year,
                                parent_flag: $scope.temp.parent_flag,
                                user_no: r.sims_enroll_number,
                                from_date : moment($scope.from_date, 'DD-MM-YYYY').format('YYYY-MM-DD') ,
                                other_fee_flag: $scope.temp.other_fee_flag,
                                cumfeeflag: $scope.temp.cumfeeflag,
                                printedby: user,
                                as_date: moment($scope.from_date, 'DD-MM-YYYY').format('DD MMMM YYYY'),
                            },
                            state: 'main.SFSQ02',
                            ready: function () {
                                this.refreshReport();
                            },
                        }
                    }

                    else {
                        var data = {
                            location: $scope.result1[0].sims_appl_form_field_value1,//"Gradebook.GBR039QR", 
                            parameter: {
                                cur_code: $scope.temp.sims_cur_code,
                                acad_year: $scope.temp.sims_academic_year,
                                parent_flag: $scope.temp.parent_flag,
                                user_no: r.family_id,
                                from_date: moment($scope.from_date, 'DD-MM-YYYY').format('YYYY-MM-DD'),
                                other_fee_flag: $scope.temp.other_fee_flag,
                                cumfeeflag: $scope.temp.cumfeeflag,
                                printedby: user,
                                as_date: moment($scope.from_date, 'DD-MM-YYYY').format('DD MMMM YYYY'),
                            },
                            state: 'main.SFSQ02',
                            ready: function () {
                                this.refreshReport();
                            },
                        }

                    }

                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                    try {
                        $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                        $scope.location = $scope.rpt.location;
                        $scope.parameter = $scope.rpt.parameter;
                        $scope.state = $scope.rpt.state;
                    }
                    catch (ex) {
                    }
                    var s;
                    s = "SimsReports." + $scope.location + ",SimsReports";

                    var url = window.location.href;
                    var domain = url.substring(0, url.indexOf(':'))
                    if ($http.defaults.headers.common['schoolId'] == 'sms') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'staging2') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'staging') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'imert') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/reportsss/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/reportsss/api/reports/';
                    }

                    else {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    }
                    console.log(service_url);


                    $scope.parameters = {}

                    $("#reportViewer1")
                                   .telerik_ReportViewer({
                                       //serviceUrl: ENV.apiUrl + "api/reports/",
                                       serviceUrl: service_url,

                                       viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                       scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                       // Zoom in and out the report using the scale
                                       // 1.0 is equal to 100%, i.e. the original size of the report
                                       scale: 1.0,
                                       ready: function () {
                                           //this.refreshReport();
                                       }

                                   });





                    var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                    reportViewer.reportSource({
                        report: s,
                        parameters: $scope.parameter,
                    });

                    
                    setInterval(function () {

                        $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                        $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                      
                    }, 1000);


                    $timeout(function () {
                        $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                        $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                        


                    }, 100)


                });
            }


            $scope.reset = function () {
                debugger;
                // $scope.temp.sims_grade_code = '';


                $scope.filteredTodos = [];
                $scope.pager = false;
                $scope.temp.sltr_tran_amt_dr_total = 0;
                $scope.temp.sltr_tran_amt_cr_total = 0;
                $scope.temp.pdc_amount_total = 0;
                $scope.temp.net_amt_total = 0;
                $scope.temp.perc_amt_total = 0;


            }

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.ischeck = function () {
                debugger
                $scope.checklick = function () {
                    var v = document.getElementById('expire');
                    if (v.checked == true) {
                        v.checked == false;
                    }
                }
            }

        }])

})();

