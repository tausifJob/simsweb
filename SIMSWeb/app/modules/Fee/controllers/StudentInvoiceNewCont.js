﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Fee');
    simsController.controller('StudentInvoiceNewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.academic_year = false;
            $scope.cur_culum = false;
            $scope.temp = {};

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            $scope.fins_comp_code = $scope.finnDetail.company;
            $scope.finance_year = $scope.finnDetail.year;
            $scope.user = $rootScope.globals.currentUser.username;

            $scope.edt = {
                to_date: dd + '-' + mm + '-' + yyyy,
                doc_from_date: dd + '-' + mm + '-' + yyyy,
                doc_to_date: dd + '-' + mm + '-' + yyyy,
            }

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res1) {
                debugger
                $scope.cur_data = res1.data;
                $scope.temp.sims_cur_code = $scope.cur_data[0].sims_cur_code;
                $scope.getAcademic_year($scope.temp.sims_cur_code);
            });

            $http.get(ENV.apiUrl + "api/DebitCreditNote/getparameter").then(function (fee_types) {
                debugger;
                $scope.parameter = fee_types.data;
                $scope.sims_parameter = $scope.parameter[0].gldc_doc_code;
               
            });

            $scope.getAcademic_year = function (cur_code1) {
                debugger
                $http.get(ENV.apiUrl + "api/FeeTerm/getAcademicYearNew?curCode=" + cur_code1).then(function (res1) {
                    $scope.acad_data = res1.data;
                    $scope.temp.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                });
            }

            $scope.CheckAllChecked_s = function () {
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById('tt-' + i);
                    var main = document.getElementById('mainchk');
                    main.checked = true;
                    v.checked = true;
                    $scope.row1 = 'row_selected';
                    $('tr').addClass("row_selected");
                }
            }
            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('tt-' + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('tt-' + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.getinvoiceData = function () {
                debugger
                $http.get(ENV.apiUrl + "api/FeeTerm/getAllStudentInvoiceNew?sims_cur_code=" + $scope.temp.sims_cur_code + "&sims_academic_year=" + $scope.temp.sims_academic_year + "&fins_comp_code=" + $scope.fins_comp_code + "&finance_year=" + $scope.finance_year + "&fee_type=" + $scope.sims_parameter +
                    "&doc_from_date=" + $scope.edt.doc_from_date + "&doc_to_date=" + $scope.edt.doc_to_date + "&user=" + $scope.user).then(function (res) {
                debugger
                $scope.all_stud_inv = res.data;
                $scope.totalItems = $scope.all_stud_inv.length;
                $scope.todos = $scope.all_stud_inv;
                $scope.makeTodos();
               

                $scope.expected_amount_total = 0;
                $scope.posted_amount_total = 0;
                $scope.difference_amount_total = 0;


                for(var i=0;i<$scope.all_stud_inv.length;i++)
                {
                    $scope.expected_amount_total = parseFloat($scope.all_stud_inv[i].expected_amount) + parseFloat($scope.expected_amount_total);
                    $scope.posted_amount_total = parseFloat($scope.all_stud_inv[i].posted_amount) + parseFloat($scope.posted_amount_total);
                    $scope.difference_amount_total = parseFloat($scope.all_stud_inv[i].difference_amount) + parseFloat($scope.difference_amount_total);
                }

                
            });
            }

            var senddata = [];
            $scope.generate_inv = function (to_date) {
                debugger
                $scope.enroll_no_Feecode = '';
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById('tt-'+i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        $scope.enroll_no_Feecode = $scope.filteredTodos[i].sims_enroll_number + $scope.filteredTodos[i].sims_fee_code + ',' + $scope.enroll_no_Feecode;
                    }
                   
                }
                var chkdata = {
                    'user': $rootScope.globals.currentUser.username,
                    'sims_fee_academic_year': $scope.temp.sims_academic_year,
                    'sims_fee_cur_code': $scope.temp.sims_cur_code,
                    'inv_date': to_date,
                    'enroll_no_Feecode': $scope.enroll_no_Feecode,
                    'doc_code': $scope.sims_parameter,
                   // 'doc_date': $scope.filteredTodos[0].doc_date,
                    'finance_year': $scope.finance_year,
                    'fins_comp_code': $scope.fins_comp_code,
                    'doc_from_date':$scope.edt.doc_from_date,
                    'doc_to_date':$scope.edt.doc_to_date,
                    opr: 'P'

                }
                senddata.push(chkdata);

                $http.post(ENV.apiUrl + "api/FeeTerm/Student_invoice_generate_ASIS", senddata).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({  text: "Invoice posted Successfully",imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, })
                    }
                    else {
                        swal({  text: "Failed",imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, })
                    }
                });
            //    console.log(senddata);

            }



            $scope.size = function (str) {
              
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                if ($scope.pagesize == 'All') {
                    $scope.currentPage = 1;
                    $scope.showPager = false;
                    $scope.filteredTodos = $scope.all_stud_inv;
                }
                else {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                }

            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.all_stud_inv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.all_stud_inv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
                $scope.disabled = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.divcode_readonly = false;
                $scope.temp = "";
                $scope.temp = {};
                $scope.temp['sims_library_catalogue_status'] = true;
                $scope.temp["sims_library_catalogue_name"] = "";
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                    $scope.curriculum = AllCurr.data;

                    $scope.temp = {}
                    $scope.temp['sims_cur_code'] = AllCurr.data[0].sims_cur_code

                    $scope.getAccYear(AllCurr.data[0].sims_cur_code)
                });

            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {

                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.divcode_readonly = true;
                $scope.academic_year = true;
                $scope.cur_culum = true;
                //   $scope.temp = str;

                $scope.temp = {
                    sims_academic_year: str.sims_academic_year,
                    sims_cur_code: str.sims_cur_code,
                    sims_appl_parameter: str.sims_fee_frequency,
                    sims_late_fee_applicable_after_days: str.sims_late_fee_applicable_after_days,
                    sims_late_fee_end_date_flag: str.sims_late_fee_end_date_flag,
                    sims_late_fee_status: str.sims_late_fee_status,
                    sims_late_fee_master_sr_no: str.sims_late_fee_master_sr_no
                };
            }

          
            // Data DELETE RECORD

                 $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

        }])

})();
