﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('PayingAgentMappingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            //************************************
            //$http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
            //    $scope.curriculum = AllCurr.data;
            //});

            //$scope.getAccYear = function (curCode) {
            //    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
            //        $scope.Acc_year = Acyear.data;

            //    });
            //}

            $http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (curiculum) {
                $scope.Curriculum_obj = curiculum.data;
                if (curiculum.data.length > 0) {
                    $scope.cur_code = curiculum.data[0].sims_cur_code;
                    $scope.Cur_Change();
                }
            });


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
               
                $scope.sims_cur_code= $scope.curriculum[0].sims_cur_code

                $scope.getAccYear($scope.sims_cur_code)
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    
                    //$scope.temp = {
                    //    'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                    //    'sims_academic_year': $scope.Acc_year[0].sims_academic_year
                    //}
                    $scope.sims_academic_year= $scope.Acc_year[0].sims_academic_year

                    $scope.getGrade($scope.sims_cur_code, $scope.sims_academic_year);
                });

            }


            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;

                });


                $http.get(ENV.apiUrl + "api/PayagentMappingController/getPayingAgentDetails?academic_year=" + accYear).then(function (getPayingAgentDetails_Data) {
                    $scope.PayingAgentDetails = getPayingAgentDetails_Data.data;
                });

            }

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }


            $scope.Show_Data = function (cur_code, a_year, grade_code, s_code, search, agent_tran_no) {
                debugger
                var searchteaxt = '';
                if (search == undefined || search == "") {
                    searchteaxt = 'null';
                }
                else {
                    searchteaxt = search;
                }

                $http.get(ENV.apiUrl + "api/PayagentMappingController/GetSPAM_Students?cur_code=" + cur_code + "&a_year=" + a_year + "&grade_code=" + grade_code + "&s_code=" + s_code + "&search=" + searchteaxt + "&agent_tran_no=" + agent_tran_no).then(function (GetSPAM_Students) {
                    $scope.GetSPAM_Students = GetSPAM_Students.data;
                    $scope.totalItems = $scope.GetSPAM_Students.length;
                    $scope.todos = $scope.GetSPAM_Students;
                    $scope.makeTodos();

                });
            }


            $scope.SaveData = function () {
                debugger
                var datasend = [];
                for (var i = 0; i < $scope.GetSPAM_Students.length; i++) {
                    for (var j = 0; j < $scope.GetSPAM_Students[i].mapping.length; j++) {
                        if ($scope.GetSPAM_Students[i].mapping[j].ischecked == true) {
                            datasend.push($scope.GetSPAM_Students[i].mapping[j]);
                        }
                    }
                }

                


                $http.post(ENV.apiUrl + "api/PayagentMappingController/SPAM_Save", datasend).then(function (SPAM_Save) {
                    $scope.SPAM_Save = SPAM_Save.data;
                    swal({  text: "Paying Agent Successfully Mapped", width: 300, height: 200, imageUrl: "assets/img/check.png" });
                });

            }

            $scope.Reset = function () {
                $scope.sims_cur_code = "";
                $scope.temp.sims_grade_code = "";
                $scope.temp.sims_section_code = "";
                $scope.sims_academic_year = "";
                $scope.temp.sims_paying_agent_number = "";

            }


            $scope.AgentInfo = function () {
                // $('#MyModal').modal('show');
                $('#myModal3').modal('show')
                $http.get(ENV.apiUrl + "api/PayagentMappingController/getPayingAgentDetails").then(function (Paying_agent) {
                    $scope.Payingagent = Paying_agent.data;
                });
            }




            //************************************
            //SearchData
            var main = '';
            var count = 0;
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                for (var i = 0; i < $scope.GetSPAM_Students.length; i++) {
                    var t = document.getElementById($scope.GetSPAM_Students[i].sims_enroll_number)
                    for (var j = 0; j < $scope.GetSPAM_Students[i].mapping.length; j++) {
                        var v = document.getElementById($scope.GetSPAM_Students[i].mapping[j].sims_enroll_number + j);
                        if (main.checked == true) {
                            t.checked = true;
                            $scope.GetSPAM_Students[i].count = parseInt($scope.GetSPAM_Students[i].count) + 1;
                            v.checked = true;

                            $scope.GetSPAM_Students[i].mapping[j].ischecked = true;

                            count = parseInt(count) + 1;

                        }

                        else {
                            count = parseInt(count) - 1;
                            $scope.GetSPAM_Students[i].count = parseInt($scope.GetSPAM_Students[i].count) - 1;
                            v.checked = false;
                            t.checked = false;
                            $scope.GetSPAM_Students[i].mapping[j].ischecked = true;

                        }
                    }
                }

                var d = parseInt($scope.GetSPAM_Students.length);

                if (d == count) {
                    main.checked = true;
                }
                else {
                    main.checked = false;
                }

            }

            $scope.SelectOnyByOneSection = function (info1, info, str) {

                var v = document.getElementById(str);


                for (var i = 0; i < info.length; i++) {
                    if (info1.status == true) {
                        info[i].ischecked = true;
                        info[i].sims_status = true;
                    }
                    else {
                        count = 0;
                        info1.count = parseInt(info1.count) - 1;
                        info[i].sims_status = false;
                        info[i].ischecked = true;
                    }
                }



            }

            $scope.SelectOnyByOneFee = function (info, str, info1) {

                info1['ischecked'] = true;
                var d = parseInt(info.mapping.length);
                var v = document.getElementById(str);
                var t = document.getElementById(info.sims_enroll_number);
                if (v.checked == true) {


                    info.count = parseInt(info.count) + 1;
                    if (d == info.count) {
                        t.checked = true;
                    }
                    count = 0;
                }
                else {
                    t.checked = false;
                    info.count = parseInt(info.count) - 1;
                    if (main.checked == true) {
                        main.checked = false;
                    }
                }

            }


            //Select Data SHOW


            debugger;
            $scope.size = function (str) {
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                debugger;
                if (str == "All") {
                    $scope.currentPage = 1;
                    $scope.filteredTodos = $scope.GetSPAM_Students;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                }

            $scope.index = function (str) {

                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
                //$scope.CheckAllChecked();

                //$scope.pageindex = str;
                //$scope.currentPage = str;
                
                //main.checked = false;
                //$scope.row1 = '';
                //// $scope.CheckAllChecked();

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);

                var count = 0;


                for (var i = 0; i < $scope.GetSPAM_Students.length; i++) {
                    var t = parseInt($scope.GetSPAM_Students[i].mapping.length);
                    for (var j = 0; j < $scope.GetSPAM_Students[i].mapping.length; j++) {

                        if ($scope.GetSPAM_Students[i].mapping[j].sims_status == true) {
                            count = parseInt(count) + 1;
                            if (count == t) {
                                $scope.GetSPAM_Students[i].status = true;
                            }

                        }

                    }
                    count = 0;
                }
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {

                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();

            }



        }])

})();
