﻿/// <reference path="FeeInvoiceController.js" />

(function () {
    'use strict';
    var simsController = angular.module('sims.module.Fee');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('StudentLateFeeController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$location', '$anchorScroll', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $location, $anchorScroll) {
          // Pre-Required Functions and Variables
          // Start

            $scope.url = $state.current.url;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1, 'z-index': 1 });
               
            }, 100);

            
            
            $('*[data-datepicker="true"] input[type="text"]').datepicker(
                {
                    todayBtn: true,
                    orientation: "top left",
                    autoclose: true,
                    todayHighlight: true,
                    format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '' };
            $scope.info = [];
            $scope.edt1 = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '' };
            /*FILTER*/
            $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllCurName").then(function (CurData) {
                $scope.CurData = CurData.data;
                $scope.edt['sims_cur_code'] = CurData.data[0].sims_cur_code;
                $scope.cur_code_change(CurData.data[0].sims_cur_code);

            });

            $scope.paging = true;
            $scope.currentYear_status = 'C';
            $scope.cur_code_change = function (cur_values) {


                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + cur_values).then(function (AcademicYears) {
                    $scope.AcademicYears = AcademicYears.data;

                    for(var i=0;i<$scope.AcademicYears.length;i++)
                    {
                        if ($scope.AcademicYears[i].sims_academic_year_status == 'C') {
                            //   $scope.edt['sims_cur_code'] = cur_values;
                            $scope.edt['sims_academic_year'] = $scope.AcademicYears[i].sims_academic_year;
                        }
                    }
                    $scope.academic_year_change(cur_values, $scope.edt.sims_academic_year);
                });
                
            }
            $scope.academic_year_change = function (cur_values,academic_year) {

                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllGrades?cur_code=" + cur_values + "&ac_year=" + academic_year).then(function (Grades) {
                    $scope.Grades = Grades.data;
                });
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetLateRulesTerm?cur=" + $scope.edt.sims_cur_code + "&aca=" + $scope.edt.sims_academic_year).then(function (FeeFreqValue) {
                    $scope.FeeFreqValue = FeeFreqValue.data;
                });
                //$http.get(ENV.apiUrl + "api/Fee/SFS/GetAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year).then(function (Grades) {
                //    $scope.Grades = Grades.data;
                //});

            }
            $scope.grade_change = function (cur_values, academic_year,grade) {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllSections?cur_code=" + cur_values + "&ac_year=" + academic_year + "&g_code=" + grade).then(function (sections) {
                    $scope.sections = sections.data;
                });
                //$http.get(ENV.apiUrl + "api/Fee/SFS/GetAllSections?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year + "&g_code=" + $scope.edt.sims_grade_code).then(function (sections) {
                //    $scope.sections = sections.data;
                //    
                //});

            }
       
            
            /* PAGER FUNCTIONS*/
           
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 7, $scope.currentPage_ind = 0;
            $scope.makeTodos = function () {
                $scope.currentPage_ind = 0;
                
                if ((Math.round($scope.totalItems / $scope.numPerPage) * $scope.numPerPage) > $scope.totalItems)
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage);
                else
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage) + 1;

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                var main = document.getElementById('mainchk');
                main.checked = false;
                $scope.CheckMultiple();

            };
            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.currentPage_ind = str;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage_ind = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            /* PAGER FUNCTIONS*/
            function _onFetchData() {
                if ($scope.edt.sims_grade_code == undefined) $scope.edt = { sims_grade_code: '' };
                if ($scope.edt.sims_section_code == undefined) $scope.edt = { sims_section_code: '' };
                if ($scope.edt.sims_enroll_number == undefined) $scope.edt.sims_enroll_number = '';
               
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetLateFeeeStudents?cc=" + $scope.edt.sims_cur_code + "&ay=" + $scope.edt.sims_academic_year + "&gc=" + $scope.edt.sims_grade_code + "&sc=" + $scope.edt.sims_section_code + "&term=" + $scope.edt.sims_term_code ).then(function (InvoiceData) {
                    $scope.InvoiceData = InvoiceData.data;
                    $scope.totalItems = $scope.InvoiceData.length;
                    $scope.todos = $scope.InvoiceData;
                    $scope.makeTodos();
                    $scope.paging = true;
                    if ($scope.totalItems > 0)
                        $scope.isApprov = false;
                    else {
                        $scope.isApprov = true;
                        swal(
                                {
                                    showCloseButton: true,
                                    text: 'No Record(s) found.',
                                    width: 350,
                                    imageUrl: "assets/img/close.png",
                                    showCloseButon: true
                                });
                    }

                });
            }
            $scope.approve = function () {
                $scope.datasend = [];
                angular.forEach($scope.filteredTodos, function (value, key) {
                    if (value.sims_late_rule_status)
                        $scope.datasend.push(value);
                });
                if ($scope.datasend.length <= 0) {
                    swal(
                            {
                                showCloseButton: true,
                                text: 'Please select at least one student.',
                                width: 350,
                                imageUrl: "assets/img/notification-alert.png",
                                showCloseButon: true
                            });
                }
                else {
                    $http.post(ENV.apiUrl + "api/Fee/SFS/CRUDLateFeeStudents", $scope.datasend).then(function (res) {
                        if (res.data == true) {
                            swal(
                               {
                                   showCloseButton: true,
                                   text: 'Late Fee Mapped Sucessfully',
                                   width: 350,
                                   imageUrl: "assets/img/check.png",
                                   showCloseButon: true
                               });

                            _onFetchData();
                        }
                    });
                }
            }
            $scope.btnReset_click = function () {

                $scope.edt.sims_cur_code = '';
                $scope.edt.sims_academic_year = '';
                $scope.edt.sims_cur_code = '';
                $scope.edt.sims_term_code = '';
                $scope.edt.sims_grade_code = '';
                $scope.edt.sims_section_code = '';
                $scope.edt.sims_enroll_number = '';
                $scope.todos = [];
                $scope.filteredTodos = [];
                $scope.paging = false;
                $scope.isApprov = true;

                $scope.makeTodos();
            }
            $scope.btnPreview_click = function () {
                _onFetchData();
            }
            $scope.isApprov = true;

            /*SEARCH INVOICES*/
            /*PAGER*/
            $scope.filteredTodos_invoice = [], $scope.currentPage_invoice = 1, $scope.numPerPage_invoice = 10, $scope.maxSize_invoice = 10;
            $scope.makeTodos_invoice = function () {
                if ((Math.round($scope.totalItems_invoice / $scope.numPerPage_invoice) * $scope.numPerPage_invoice) > $scope.totalItems_invoice)
                    $scope.pagersize_invoice = Math.round($scope.totalItems_invoice / $scope.numPerPage_invoice);
                else
                    $scope.pagersize_invoice = Math.round($scope.totalItems_invoice / $scope.numPerPage_invoice) + 1;
                var begin_invoice = (($scope.currentPage_invoice - 1) * $scope.numPerPage_invoice);
                var end_invoice = parseInt(begin_invoice) + parseInt($scope.numPerPage_invoice);
                $scope.filteredTodos_invoice = $scope.InvoiceDetailsData.slice(begin_invoice, end_invoice);
                var main = document.getElementById('chkSearch');
                main.checked = false;
                $scope.CheckMultipleSearch();

            };
            $scope.size_invoice = function (str) {
                $scope.pagesize_invoice = str;
                $scope.currentPage_invoice = 1;
                $scope.numPerPage_invoice = str;
                $scope.makeTodos_invoice();
            }
            $scope.index_invoice = function (str) {
                $scope.pageindex_invoice = str;
                $scope.currentPage_invoice = str;
                $scope.makeTodos_invoice();
            }
            /*PAGER*/

            $scope.CheckMultipleSearch = function () {
                var main = document.getElementById('chkSearch');
                for (var i = 0; i < $scope.filteredTodos_invoice.length; i++) {
                    if (main.checked == true) {
                        $scope.filteredTodos_invoice[i].sims_invoice_status = true;
                    }
                    else if (main.checked == false) {
                        $scope.row1 = '';
                        $scope.color = '#edefef';
                        main.checked = false;
                        $scope.filteredTodos_invoice[i].sims_invoice_status = false;
                    }
                }
            }
            $scope.CheckOneByOneSearch = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                    }
                });

                var main = document.getElementById('chkSearch');
                if (main.checked == true) {
                    main.checked = false;
                    //$scope.color = '#edefef';
                }

            }
            $scope.CheckMultiple = function () {
                paging();
            }
            function paging() {
                var main = document.getElementById('mainchk');
                
                for (var i = 0; i < $scope.filteredTodos.length; i++)
                {
                    var t = $scope.filteredTodos[i].sims_enroll_number;
                    var v = document.getElementById(t);
                    if (main.checked == true && !$scope.filteredTodos[i].sims_late_rule_status) {
                        $scope.filteredTodos[i].sims_late_rule_status = true;
                        v.checked = true;
                        $('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                        $scope.isGenerateEnable = false;
                    }
                    else if (main.checked == false) {
                       
                        $('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                        main.checked = false;
                        $scope.isGenerateEnable = true;
                        $scope.filteredTodos[i].sims_late_rule_status = false;

                    }
                }
            }
            $scope.refresh = function () { _onFetchData(); }
            $scope.CheckOneByOne = function () {
              
                //$("input[type='checkbox']").click(function (e) {
                //    if ($(this).is(":checked")) {
                //        $(this).closest('tr').addClass("row_selected");
                //        $scope.isGenerateEnable = true;
                //    }
                //    else {
                //        $scope.isGenerateEnable = false;
                //        $(this).closest('tr').removeClass("row_selected");
                //    }
                //});

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                    }
                });

              var  main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    //$scope.color = '#edefef';
                }

            }
           
        //Events End
        }])

})();
