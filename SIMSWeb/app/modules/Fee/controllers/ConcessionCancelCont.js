﻿(function () {
    'use strict';
    var main = '';
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ConcessionCancelCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.busyindicator = false;
            $scope.grid = true;
            $scope.color = '#edefef';
            $scope.rgvtbl = false;
            $scope.rgvtbl1 = true;
            $scope.btnhide = true;

            $scope.dsible = false;
            $scope.edt = [];

            var date3 = new Date();
            var month3 = (date3.getMonth() + 1);
            var day3 = date3.getDate();
            if (month3 < 10)
                month3 = "0" + month3;
            if (day3 < 10)
                day3 = "0" + day3;

            $scope.edt['cancel_date'] = (day3) + '-' + (month3) + '-' + date3.getFullYear();

            $http.get(ENV.apiUrl + "api/concessioncel/GetCancelAcademic_year").then(function (GetCancelAcademic_year) {
                $scope.GetCancelAcademic_year = GetCancelAcademic_year.data;
                $scope.edt['academic_year'] = $scope.GetCancelAcademic_year[0].academic_year;
            });

            $rootScope.visible_stud = true;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = false;

            $rootScope.chkMulti = true;

            $scope.Search_student = function () {
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                $rootScope.visible_stud = true;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = false;
                $rootScope.chkMulti = true;
            }

            $scope.$on('global_cancel', function (str) {
                var stud_no = '';
                for (var i = 0; i < $scope.SelectedUserLst.length; i++) {
                    if ($scope.SelectedUserLst[i].user_chk == true)
                        stud_no = stud_no + $scope.SelectedUserLst[i].s_enroll_no + ',';
                }
                $scope.emp_code = stud_no;

            });

            var uname = $rootScope.globals.currentUser.username;

            $scope.genrate = function () {
            debugger
                if ($scope.edt.academic_year == undefined || $scope.edt.academic_year == '') {
                    swal('', 'Please select academic year.');
                }
                else {
                    var srt = '', end = '';
                    if ($scope.emp_code == undefined)
                        $scope.emp_code = '';

                    if ($scope.edt.start_date != '' || $scope.edt.start_date != undefined || $scope.edt.end_date != '' || $scope.edt.end_date != undefined) {
                        var date = new Date($scope.edt.start_date);
                        var month = (date.getMonth() + 1);
                        var day = date.getDate();
                        if (month < 10)
                            month = "0" + month;
                        if (day < 10)
                            day = "0" + day;

                        var date1 = new Date($scope.edt.end_date);
                        var month1 = (date1.getMonth() + 1);
                        var day1 = date1.getDate();
                        if (month1 < 10)
                            month1 = "0" + month1;
                        if (day1 < 10)
                            day1 = "0" + day1;

                        //  srt = date.getFullYear() + '-' + (month) + '-' + (day);
                        //  end = date1.getFullYear() + '-' + (month1) + '-' + (day1);

                        srt = (day) + '-' + (month) + '-' + date.getFullYear();
                        end = (day1) + '-' + (month1) + '-' + date1.getFullYear();
                        if (srt == 'NaN-NaN-NaN')
                            srt = '';
                        if (end == 'NaN-NaN-NaN')
                            end = '';
                    }

                    var data = {
                        enrollment_no: $scope.emp_code,
                        doc_user: uname,
                        start_date: $scope.edt.start_date,
                        end_date: $scope.edt.end_date,
                        sims_academic_year_description: $scope.edt.academic_year
                    }

                    $scope.buzy = true;

                    $http.post(ENV.apiUrl + "api/concessioncel/Concession_ReceiptList", data).then(function (Concession_ReceiptList) {
                        $scope.Concession_ReceiptList = Concession_ReceiptList.data;
                        if ($scope.Concession_ReceiptList.length > 0) {
                            $scope.rgvtbl = true;
                            $scope.rgvtbl1 = false;
                            $scope.btnhide = false;
                        }

                        $scope.buzy = false;
                    });
                }
            }

            $scope.SelectAll = function (info) {

                for (var i = 0; i < $scope.Concession_ReceiptList.length; i++) {
                    if (info == true) {
                        $scope.Concession_ReceiptList[i].isEnabled = true;
                    }
                    else {
                        $scope.Concession_ReceiptList[i].isEnabled = false;
                    }
                }
            }

            $scope.Cancel_recipt = function () {

                var datalist = [];
                if ($scope.edt.cancel_date == undefined || $scope.edt.cancel_date == '') {
                    swal('', 'Please select cancellation date.');
                }
                else {


                    for (var i = 0; i < $scope.Concession_ReceiptList.length; i++) {
                        if ($scope.Concession_ReceiptList[i].isEnabled) {
                            var data = {
                                doc_no: $scope.Concession_ReceiptList[i].doc_no,
                                enrollment_no: $scope.Concession_ReceiptList[i].enrollment_no,
                                cur_code: $scope.Concession_ReceiptList[i].cur_code,
                                academic_year: $scope.Concession_ReceiptList[i].academic_year,
                                grade_code: $scope.Concession_ReceiptList[i].grade_code,
                                section_code: $scope.Concession_ReceiptList[i].section_code,
                                doc_date: $scope.edt.cancel_date
                            }
                            datalist.push(data);
                        }
                    }

                    if (datalist.length > 0) {
                        $scope.buzy = true;

                        $http.post(ENV.apiUrl + "api/concessioncel/Cancel_Concession_FeeReceipt", datalist).then(function (Cancel_Concession_FeeReceipt) {
                            $scope.Cancel_Concession_FeeReceipt = Cancel_Concession_FeeReceipt.data;
                            if ($scope.Cancel_Concession_FeeReceipt)
                                swal('', 'Concession cancelled successfully.');
                            $scope.buzy = false;
                            $scope.Reset_student();

                        });

                    }
                }

            }

            $scope.Reset_student = function () {
                $scope.edt['start_date'] = '';
                $scope.edt['end_date'] = '';
                $("#start_date").val('');
                $("#End_Date").val('');
                setTimeout(function () {
                    $(document).ready(function () {
                        $("#start_date, #End_Date").kendoDatePicker({
                            format: "dd-MM-yyyy",
                            value: ''
                        });
                    });
                }, 500);
                $scope.emp_code = '';
                $scope.Concession_ReceiptList = [];
                $scope.SelectedUserLst = [];
                $scope.rgvtbl = false;
                $scope.rgvtbl1 = true;
                $scope.cancel_date = true;
                $scope.btnhide = true;
            }


            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd-mm-yyyy'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }])
})();