﻿(function () {
    'use strict';
    var opr = '';
    var belldaycode = [];
    var main;
    var data1 = [];
    var data = [];
    var simsController = angular.module('sims.module.Scheduling');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('BellDayCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';


            $scope.table1 = true;
            $scope.operation = false;
            delete $scope.edt;
            $http.get(ENV.apiUrl + "api/BellDay/getBellDay").then(function (getBellDay_Data) {
                $scope.BellDay_Data = getBellDay_Data.data;
                $scope.totalItems = $scope.BellDay_Data.length;
                $scope.todos = $scope.BellDay_Data;
                $scope.makeTodos();
            });

            //$http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2015&routeCode=01").then(function (AllComboBoxValues) {
            //    $scope.ComboBoxValues = AllComboBoxValues.data;


            //    console.log($scope.ComboBoxValues);
            //});
            $http.get(ENV.apiUrl + "api/common/getAllAcademicYear").then(function (get_AllAcademicYear) {
                $scope.ActiveYear = get_AllAcademicYear.data;
            });

            $scope.getBellDesc = function () {

                $http.get(ENV.apiUrl + "api/BellDay/getBellDescByAcy?Acayear=" + $scope.edt.sims_bell_academic_year).then(function (getBellDescByAcy_Data) {
                    $scope.BellDescByAcy = getBellDescByAcy_Data.data;
                });

            }

            $scope.editmode = false;

            $scope.size = function (str) {
               
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;  $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.newmode = true;
                $scope.check = true;
                $scope.editmode = false;
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {
                    sims_bell_day_desc: "",
                    sims_bell_day_order: "",
                    sims_bell_day_short_desc: ""
                }
                //$scope.edt = {};
                //$scope.edt['sims_bell_status'] = true;
            }

            $scope.up = function (str) {

                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;

                $scope.edt = {
                    sims_bell_academic_year: str.sims_bell_academic_year,
                    sims_academic_year_description: str.sims_academic_year_description,
                    sims_bell_code: str.sims_bell_code,
                    sims_bell_desc: str.sims_bell_desc,
                    sims_bell_day_code: str.sims_bell_day_code,
                    sims_bell_day_desc: str.sims_bell_day_desc,
                    sims_bell_day_short_desc: str.sims_bell_day_short_desc,
                    sims_bell_day_order: str.sims_bell_day_order
                }

            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                delete $scope.edt;
                $scope.edt = {
                    sims_bell_day_desc: "",
                    sims_bell_day_order: "",
                    sims_bell_day_short_desc: ""
                }

            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    data1 = [];
                    data = [];
                    var data = $scope.edt;
                    data.opr = 'I';
                    $scope.exist = false;
                    for (var i = 0; i < $scope.BellDay_Data.length; i++) {
                        if ($scope.BellDay_Data[i].sims_bell_code == data.sims_bell_code && $scope.BellDay_Data[i].sims_bell_day_code == data.sims_bell_day_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ text: "Bell Day Already Exists", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }
                    else {
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/BellDay/BellDayCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;

                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                            $http.get(ENV.apiUrl + "api/BellDay/getBellDay").then(function (getBellDay_Data) {
                                $scope.BellDay_Data = getBellDay_Data.data;
                                $scope.totalItems = $scope.BellDay_Data.length;
                                $scope.todos = $scope.BellDay_Data;
                                $scope.makeTodos();
                            });
                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                }
            }

            $scope.Update = function (myForm) {
                if (myForm) {
                    data = [];
                    data1 = [];
                    data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/BellDay/BellDayCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $http.get(ENV.apiUrl + "api/BellDay/getBellDay").then(function (getBellDay_Data) {
                            $scope.BellDay_Data = getBellDay_Data.data;
                            $scope.totalItems = $scope.BellDay_Data.length;
                            $scope.todos = $scope.BellDay_Data;
                            $scope.makeTodos();
                        });
                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
              
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

              
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_bell_day_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_bell_day_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.deleterecord = function () {
                belldaycode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_bell_day_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletebelldaycode = ({
                            'sims_bell_academic_year': $scope.filteredTodos[i].sims_bell_academic_year,
                            'sims_bell_day_code': $scope.filteredTodos[i].sims_bell_day_code,
                            'sims_bell_code': $scope.filteredTodos[i].sims_bell_code,
                            opr: 'D'
                        });
                        belldaycode.push(deletebelldaycode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                           
                            $http.post(ENV.apiUrl + "api/BellDay/BellDayCUD", belldaycode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BellDay/getBellDay").then(function (getBellDay_Data) {
                                                $scope.BellDay_Data = getBellDay_Data.data;
                                                $scope.totalItems = $scope.BellDay_Data.length;
                                                $scope.todos = $scope.BellDay_Data;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BellDay/getBellDay").then(function (getBellDay_Data) {
                                                $scope.BellDay_Data = getBellDay_Data.data;
                                                $scope.totalItems = $scope.BellDay_Data.length;
                                                $scope.todos = $scope.BellDay_Data;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_bell_day_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';

                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.BellDay_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.BellDay_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                         item.sims_bell_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.sims_bell_day_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.sims_bell_day_short_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.sims_bell_day_code == toSearch ||
                         item.sims_bell_day_order == toSearch ||
                         item.sims_bell_code == toSearch) ? true : false;
            }

        }])
})();

