﻿(function () {
    'use strict';
    var subject1 = [],
         subject, Employee_code = [],
        subject_code = [],
        bell_code = [],
        lesson_type = [],
        group_order = [];
    var le = 0;
    var main, strMessage;

    var simsController = angular.module('sims.module.Scheduling');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('BellSectionTeacherSubjectCont', ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
        $scope.display = false;

        $timeout(function () {
            $("#fixTable").tableHeadFixer({
                "top": 1
            });
        }, 100);
        $scope.Grid = true;
        $scope.add_subject = false;
        $scope.table = false;
        $scope.insert = true;
        $scope.busyindicator = true;
        $scope.edt = {};

        $scope.propertyName = null;
        $scope.reverse = false;

        $scope.sortBy = function (propertyName) {
            $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        };

        $http.get(ENV.apiUrl + "api/bellsectionteacher/GetAllTeacherName").then(function (GetAllTeacherName) {
            $scope.GetAllTeacherName = GetAllTeacherName.data;
        });

        $scope.getSlotRoom = function () {
            $http.get(ENV.apiUrl + "api/bellsectionteacher/GetAllslotroom").then(function (GetAllslotroom) {
                $scope.slotrom = GetAllslotroom.data;
            });
        }

        $http.get(ENV.apiUrl + "api/bellsectionteacher/GetAllslotgroup").then(function (GetAllslotgroup) {
            debugger
            $scope.slotgrup = GetAllslotgroup.data;
        });

        $scope.getSubjects = function () {
            var a = {
                opr: 'B',
                cur_code: $scope.edt.sims_cur_code,
                academic_year: $scope.edt.academic_year
            };
            $http.post(ENV.apiUrl + "api/bellsectionteacher/TeacherSubjectCommon", a).then(function (res) {
                $scope.subtype = res.data.table;
            });
        }

        $scope.getacyr = function (str) {
            debugger;
            $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                $scope.getAcademicYear = Academicyear.data;
                $scope.edt['academic_year'] = $scope.getAcademicYear[0].sims_academic_year;
            });
        }

        $scope.AddtechSubject = function () {
            debugger
            $scope.rm_edt = true;
            var a = {
                opr: 'I',
                cur_code: $scope.edt.sims_cur_code,
                academic_year: $scope.edt.academic_year,
                grade_code: $scope.edt2.grade_code,
                section_code: $scope.edt2.section_code,
                subject_code: $scope.edt2.sims_subject_code,
                teacher_code: $scope.edt.sims_employee_code,
                lect_count: $scope.edt2.sims_bell_lecture_count,
                slot_group: $scope.edt2.sims_bell_slot_group,
                slot_room: $scope.edt2.sims_bell_slot_room_code
            };
            $http.post(ENV.apiUrl + "api/bellsectionteacher/TeacherSubjectCommon", a).then(function (res) {
                if (Object.keys(res.data).length === 0) {
                    swal({
                        text: 'Information Inserted Successfully',
                        imageUrl: "assets/img/check.png",
                        width: 300,
                        showCloseButton: true
                          
                    });
                    $scope.edt2.sims_subject_code = "";
                     $scope.edt2.sims_bell_lecture_count = "";
                    $scope.edt2.sims_bell_slot_group = "";
                    $scope.edt2.sims_bell_slot_room_code = "";
                    $scope.edt2.grade_code = "";
                    $scope.edt2.section_code = "";
                    $scope.table = true;
                    $scope.Grid = true;
                    $scope.temp = "";
                } else {

                    swal({
                        text: 'Information Not Inserted. Maybe subject already assigned to that grade section',
                        imageUrl: "assets/img/close.png",
                        width: 300,
                        showCloseButton: true
                    });
                } 

                $scope.Cancel();
                $scope.Show_Data();
            });
        }


        function getCur(flag, comp_code) {
            if (flag) {

                $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                    $scope.curriculum = res.data;
                    $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code
                    $scope.getacyr($scope.curriculum[0].sims_cur_code)

                });
            } else {

                $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                    $scope.curriculum = res.data;
                    $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code
                    $scope.getacyr($scope.curriculum[0].sims_cur_code)

                });


            }

        }

        $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
            $scope.global_count_comp = res.data;

            if ($scope.global_count_comp) {
                getCur(true, $scope.user_details.comp);


            } else {
                getCur(false, $scope.user_details.comp)
            }
        });

        //$http.get(ENV.apiUrl + "api/bellsectionteacher/getAllSubjectCode").then(function (getmedtype) {
        //    $scope.allsubject = getmedtype.data;
        //    $scope.temp2 = { sims_subject_code: $scope.medtype[0].sims_subject_code };
        //});

        $scope.getGrade = function () {
            var a = {
                opr: 'E',
                cur_code: $scope.edt.sims_cur_code,
                academic_year: $scope.edt.academic_year,
                subject_code: $scope.edt2.sims_subject_code
            };
            $http.post(ENV.apiUrl + "api/bellsectionteacher/TeacherSubjectCommon", a).then(function (res) {
                $scope.AllGrades = res.data.table;
            });
        }


        $scope.getSection = function () {
            var a = {
                opr: 'F',
                cur_code: $scope.edt.sims_cur_code,
                academic_year: $scope.edt.academic_year,
                subject_code: $scope.edt2.sims_subject_code,
                grade_code: $scope.edt2.grade_code
            };
            $http.post(ENV.apiUrl + "api/bellsectionteacher/TeacherSubjectCommon", a).then(function (res) {
                $scope.AllSectionByGrade = res.data.table;
            });
        }

        $scope.Show_Data = function () {
            $scope.getSlotRoom();
            $scope.expanded = true;
            $scope.busyindicator = false;
            $scope.add_subject = true;
            $scope.insert = true;
            $scope.Table2 = true;
            if (($scope.edt.academic_year == "" || $scope.edt.academic_year == undefined) && ($scope.edt.sims_employee_code == "" || $scope.edt.sims_employee_code == undefined)) {
                swal({
                    text: 'Please select Academic and Teacher ',
                    width: 300,
                    showCloseButton: true
                });
            } else {
                var a = {
                    opr: 'S',
                    cur_code: $scope.edt.sims_cur_code,
                    academic_year: $scope.edt.academic_year,
                    teacher_code: $scope.edt.sims_employee_code
                };
                $http.post(ENV.apiUrl + "api/bellsectionteacher/TeacherSubjectCommon", a).then(function (res) {
                    $scope.All_Teacher_Name = res.data.table;
                    console.log("$scope.All_Teacher_Name value is ", $scope.All_Teacher_Name);
                    $scope.busyindicator = true;
                    $scope.table = true;
                });
            }
        }

        $scope.UpdateTeacherData = function () {
            var send = [];
            var sims_Bell_Section_Teacher_Subject = [];
            $scope.flag = false;
            debugger;
          
            
                for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {
                    var v = document.getElementById('mainchk11' + $scope.All_Teacher_Name[i].sims_subject_code + i);

                    if (v.checked == true) {
                        //if ($scope.file_doc.length == 0) {
                        sims_Bell_Section_Teacher_Subject = {
                            'opr': 'U',
                            
                            'sims_bell_slot_group': $scope.All_Teacher_Name[i].sims_bell_slot_group,
                            'sims_bell_slot_room_code': $scope.All_Teacher_Name[i].sims_bell_slot_room_code,
                            'sims_bell_lecture_per_week': $scope.All_Teacher_Name[i].sims_bell_lecture_per_week,
                             
                            'sims_subject_code': $scope.All_Teacher_Name[i].sims_subject_code,
                            'teacher_code': $scope.edt.sims_employee_code,
                            'grade_code': $scope.All_Teacher_Name[i].sims_grade_code,
                            'section_code': $scope.All_Teacher_Name[i].sims_section_code,
                            'academic_year': $scope.edt.academic_year,

                            //}
                        }

                        $scope.flag = true;
                        send.push(sims_Bell_Section_Teacher_Subject);
                    }
                }
                if ($scope.flag == false) {
                    swal('', 'Please select the records to update');
                    return;
                }
             
            $http.post(ENV.apiUrl + "api/bellsectionteacher/CUDbellsectionteacher", send).then(function (res) {
                $scope.msg1 = res.data;
                if ($scope.msg1 == true) {
                    swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                    $scope.Show_Data();
                }
                else if ($scope.msg1 == false) {
                    swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                }
                else {
                    swal("Error-" + $scope.msg1)
                }
                $scope.Show_Data();

            });
            $scope.table = true;
            $scope.Grid = true;
            $scope.Show_Data();
        }

        //$scope.UpdateData = function () {

        //    var Sdata = [{}];
        //    for (var j = 0; j < $scope.GetAllTeacherName.length; j++) {
        //        for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {
        //            $scope.new_teacher_code = $scope.All_Teacher_Name[i].sims_gb_teacher_code_name.split('/')[0];
        //            if ($scope.GetAllTeacherName[j].sims_teacher_code == $scope.new_teacher_code) {
        //                $scope.teacher_code = $scope.GetAllTeacherName[j].sims_employee_code;
        //                break;
        //            }
        //        }
        //    }

        //        for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {

        //            if ($scope.All_Teacher_Name[i].ischange == true) {
        //                if ($scope.All_Teacher_Name[i].sims_bell_group_order == "" || $scope.All_Teacher_Name[i].sims_bell_group_order == undefined || $scope.All_Teacher_Name[i].sims_bell_group_order == null) {
        //                    $scope.All_Teacher_Name[i].sims_bell_group_order = '1';
        //                }
        //                $scope.new_teacher_code = $scope.All_Teacher_Name[i].sims_gb_teacher_code_name.split('/')[0];
        //                if ($scope.new_teacher_code != '' && $scope.new_teacher_code != undefined) {
        //                    for (var j = 0; j < $scope.GetAllTeacherName.length; j++) {
        //                        if ($scope.GetAllTeacherName[j].sims_teacher_code == $scope.new_teacher_code) {
        //                            $scope.All_Teacher_Name[i].sims_teacher_code = $scope.GetAllTeacherName[j].sims_employee_code;
        //                            break;
        //                        }
        //                    }
        //                }

        //                var le = Sdata.length;
        //                Sdata[le] = {
        //                    'academic_year': $scope.edt.academic_year,
        //                    'sims_bell_desc': $scope.All_Teacher_Name[i].sims_bell_code,
        //                    'sims_grade_code': $scope.edt.grade_code,
        //                    'sims_section_code': $scope.edt.section_code,
        //                    'sims_subject_code': $scope.All_Teacher_Name[i].sims_subject_code,
        //                    'sims_gb_teacher_code': $scope.All_Teacher_Name[i].sims_teacher_code,
        //                    'sims_bell_lecutre_count': (parseInt($scope.All_Teacher_Name[i].sims_bell_lecture_per_week) * parseInt($scope.All_Teacher_Name[i].sims_bell_slot_per_lecture)) + (parseInt($scope.All_Teacher_Name[i].sims_bell_practical_per_week) * parseInt($scope.All_Teacher_Name[i].sims_bell_slot_per_practical)),
        //                    'sims_bell_lecture_practical_count': $scope.All_Teacher_Name[i].sims_bell_lecture_practical_count,
        //                    'sims_bell_slot_group_code': $scope.All_Teacher_Name[i].sims_bell_slot_group,
        //                    'sims_bell_slot_room': $scope.All_Teacher_Name[i].sims_bell_slot_room,
        //                    'sims_bell_group_order': $scope.All_Teacher_Name[i].sims_bell_group_order,
        //                    'sims_bell_lecture_per_week': $scope.All_Teacher_Name[i].sims_bell_lecture_per_week,
        //                    'sims_bell_slot_per_lecture': $scope.All_Teacher_Name[i].sims_bell_slot_per_lecture,
        //                    'sims_bell_practical_per_week': $scope.All_Teacher_Name[i].sims_bell_practical_per_week,
        //                    'sims_bell_slot_per_practical': $scope.All_Teacher_Name[i].sims_bell_slot_per_practical,
        //                    'sims_lesson_type': $scope.All_Teacher_Name[i].sims_lession_type1,
        //                    'sims_old_teacher_code': $scope.All_Teacher_Name[i].sims_old_teacher_code,
        //                    'opr': 'U'
        //                }
        //            }
        //        }



        //    Sdata.splice(0, 1);
        //    var data = Sdata;

        //    if (data.length > 0) {
        //        $http.post(ENV.apiUrl + "api/bellsectionsubject/UpdateSectionSubjectTeacher?simsobj=", data).then(function (msg) {

        //            $scope.busyindicator = false;
        //            $scope.table = false;

        //            $scope.msg1 = msg.data;
        //            if ($scope.msg1 == true) {
        //                swal({ text: 'Information Updated Successfully', imageUrl: "assets/img/check.png", width: 300, showCloseButton: true });
        //                $scope.teacher_code = '';
        //                $scope.Show_Data();
        //                $scope.busyindicator = true;
        //                $scope.table = true;
        //                //swal({text: 'Information Updated Successfully', timer: 2000, showConfirmButton: false });
        //            }

        //            else if ($scope.msg1 == false) {

        //                swal({ text: 'Information Not Updated', imageUrl: "assets/img/close.png", width: 300, showCloseButton: true });
        //                $scope.Show_Data();
        //                $scope.table = true;
        //                $scope.busyindicator = true;
        //            }
        //            else {
        //                swal({ text: 'Section Bell Not Defined For This Grade And Section', imageUrl: "assets/img/close.png", width: 300, height: 250, showCloseButton: true });
        //                $scope.Show_Data();
        //            }
        //        });
        //    }
        //    else {
        //        swal({ text: 'Please Change At Least One Field To Update', imageUrl: "assets/img/notification-alert.png", width: 380, showCloseButton: true });
        //        $scope.table = true;
        //        $scope.busyindicator = true;
        //    }
        //}

        //checked all
        $scope.CheckAllChecked = function () {

            main = document.getElementById('mainchk11');

            if (main.checked == true) {
                for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {
                    var v = document.getElementById('mainchk11' + $scope.All_Teacher_Name[i].sims_subject_code + i);
                    v.checked = true;
                    $('tr').addClass("row_selected");
                }
                //$('.sub_checkbox').find('input').prop('checked', true);
                //$(this).closest('tr').addClass("row_selected");
                //$scope.color = '#edefef';
                //for (var i = 0; i < $scope.ratingArray.length; i++) {
                //    $scope.ratingArray[i].datastatus = true;
                //}
            }
            else {
                for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {
                    var v = document.getElementById('mainchk11' + $scope.All_Teacher_Name[i].sims_subject_code + i);
                    v.checked = false;
                    main.checked = false;
                    $scope.row1 = '';
                    $('tr').removeClass("row_selected");
                }
                //$('.sub_checkbox').find('input').prop('checked', false);
                //$(this).closest('tr').addClass("row_selected");
                //$scope.color = '#edefef';

            }
        }

        //checked one by one
        var ind;
        var data = [];
        $scope.checkonebyonedelete = function (str, index) {
            debugger;
            $("input[type='checkbox']").change(function (e) {
                if ($(this).is(":checked")) { //If the checkbox is checked
                    $(this).closest('tr').addClass("row_selected");
                    //Add class on checkbox checked
                    $scope.color = '#edefef';
                    //str.status = true;
                }
                else {
                    $(this).closest('tr').removeClass("row_selected");
                    //Remove class on checkbox uncheck
                    $scope.color = '#edefef';
                    //str.status = false;
                }
            });
            str.status = str.datastatus;
            ind = index;
            //data = str;
        }
        //delete
        $scope.DeleteTeacherData = function () {
            debugger
           var deletefin = [];
            $scope.flag = false;
            for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {
                var v = document.getElementById('mainchk11' + $scope.All_Teacher_Name[i].sims_subject_code + i);
                if (v.checked == true) {
                    $scope.flag = true;
                    var deletemodulecode = {
                        'sims_subject_code': $scope.All_Teacher_Name[i].sims_subject_code,
                        'teacher_code': $scope.edt.sims_employee_code,
                        'grade_code': $scope.All_Teacher_Name[i].sims_grade_code,
                        'section_code': $scope.All_Teacher_Name[i].sims_section_code,
                        'academic_year': $scope.edt.academic_year,

                        opr: 'D'
                    }
                    deletefin.push(deletemodulecode);
                }
            }
            if ($scope.flag) {
                swal({
                    title: '',
                    text: "Are you sure you want to Delete? Delete this field Related all record",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {

                    if (isConfirm) {
                        debugger;
                        $http.post(ENV.apiUrl + "api/bellsectionteacher/CUDbellsectionteacher", deletefin).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    $scope.getgrid();
                                    if (isConfirm) {

                                        main = document.getElementById('mainchk');
                                        if (main.checked == true) {
                                            main.checked = false;
                                        }

                                        $scope.CheckAllChecked();
                                    }
                                    $scope.currentPage = true;
                                });
                            }
                            else {
                                swal({ text: "Record Not Deleted", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                });
                            }
                            $scope.Show_Data();
                        });
                        deletefin = [];
                    }
                    else {
                        for (var i = 0; i < $scope.filteredTodos.length; i++) {
                            var v = document.getElementById($scope.filteredTodos[i].sims_checklist_section_number + i);
                            if (v.checked == true) {
                                v.checked = false;
                                main.checked = false;
                                $('tr').removeClass("row_selected");
                            }
                        }
                    }
                });
            }
            else {
                swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
            }
            $scope.currentPage = true;
        }
        //$scope.DeleteTeacherData = function () {
        //    debugger
        //    for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {
        //        var t = document.getElementById($scope.All_Teacher_Name[i].sims_teacher_code + $scope.All_Teacher_Name[i].sims_subject_code)
        //        if (t.checked == true) {
        //            Employee_code = Employee_code + $scope.All_Teacher_Name[i].sims_teacher_code + ',';
        //            subject_code = subject_code + $scope.All_Teacher_Name[i].sims_subject_code + ',';
        //            bell_code = bell_code + $scope.All_Teacher_Name[i].sims_bell_code + ',';
        //            lesson_type = lesson_type + $scope.All_Teacher_Name[i].sims_lession_type1 + ',';
        //            group_order = group_order + $scope.All_Teacher_Name[i].sims_bell_group_order + ',';

        //        }
        //    }
        //    var Employee_code_list = ({
        //        'sims_cur_code': $scope.edt.sims_cur_code,
        //        'sims_grade_code': $scope.edt.grade_code,
        //        'sims_section_code': $scope.edt.section_code,
        //        'academic_year': $scope.edt.academic_year,
        //        'sims_gb_teacher_code': Employee_code,
        //        'sims_subject_code': subject_code,
        //        'sims_bell_desc': bell_code,
        //        'sims_lesson_type': lesson_type,
        //        'sims_bell_group_order': group_order,
        //        'opr': 'D'
        //    });



        //    $http.post(ENV.apiUrl + "api/bellsectionsubject/SectionSubjectTeacherDelete?simsobj=", Employee_code_list).then(function (msg) {

        //        Employee_code = [];
        //        subject_code = [], bell_code = [], lesson_type = [];
        //        group_order = [];
        //        $scope.msg1 = msg.data;
        //        Employee_code_list = {};
        //        if ($scope.msg1 == true) {
        //            swal({
        //                text: 'Information Deleted Successfully',
        //                imageUrl: "assets/img/check.png",
        //                width: 300,
        //                showCloseButton: true
        //            });
        //            $scope.Show_Data();
        //        } else if ($scope.msg1 == false) {
        //            swal({
        //                text: 'Information Not Deleted. ' + $scope.msg1,
        //                imageUrl: "assets/img/close.png",
        //                width: 300,
        //                showCloseButton: true
        //            });
        //        } else {
        //            swal("Error-" + $scope.msg1)
        //        }
        //    });
        //}

        $scope.Check = function (teacher) {
            teacher.ischange = true;
        }

        $scope.showcmb = function (str) {
            for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {
                if (str == $scope.All_Teacher_Name[i].sims_subject_name)
                    $scope[str] = true;
            }
        }

        var teacher_code1 = '';

        $scope.CheckTeacher = function (str) {

            teacher_code1 = str.sims_teacher_code;
            $scope.Check(str);
        }

        $scope.CheckteacherAssigned = function (teacher_name1, subject_name) {

            for (var i = 0; i < $scope.GetAllTeacherName.length; i++)
                if ($scope.GetAllTeacherName[i].sims_teacher_code == teacher_name1.split('/')[0]) {
                    $scope.teacher_code = $scope.GetAllTeacherName[i].sims_employee_code;
                }


            for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {
                if ($scope.All_Teacher_Name[i].sims_teacher_code == $scope.teacher_code && $scope.All_Teacher_Name[i].sims_subject_code == subject_name) {
                    swal('', 'Please Select Another Teacher, This Teacher  already Assiged Same Subject ' + '   ' + $scope.All_Teacher_Name[i].sims_subject_name);
                    $scope.temp.sims_gb_teacher_code_name = '';
                    teacher_name1 = '';
                    $scope.teacher_code = '';
                }
            }
            $scope.new = true;
        }

        $scope.AddNewData = function (info) {
            //$scope.NewData = true;
            $scope.Grid = true;
            $scope.table = false;
            $scope.add_btn = true;
            $scope.Table2 = false;
            $scope.rm_edt = true;
            $scope.status1 = false;
            //$scope.copyobject = angular.copy(info);

            //$scope.temp = [];

            //$scope.copyobject.sims_bell_code = '';
            //$scope.copyobject.sims_bell_desc = '';

            //$scope.copyobject.sims_bell_slot_room = '';
            //$scope.copyobject.sims_bell_slot_room_desc = '';
            //$scope.copyobject.sims_gb_teacher_code_name = '';
            //$scope.copyobject.sims_grade_section = '';
            //$scope.copyobject.sims_lession_type1 = '';
            //$scope.copyobject.sims_lesson_type = '';
            //$scope.copyobject.sims_pre_status = '';
            //$scope.copyobject.sims_section_code
            //$scope.copyobject.sims_sub_status
            //$scope.copyobject.sims_subject_code
            //$scope.copyobject.sims_subject_name
            //$scope.copyobject.sims_teacher_code = '';

            //$scope.temp.push($scope.copyobject);
            //$scope.temp['sims_subject_code'] = $scope.copyobject.sims_subject_code
            //$scope.temp['sims_bell_group_order'] = $scope.copyobject.sims_bell_group_order;
            //$scope.temp['sims_bell_lecture_per_week'] = $scope.copyobject.sims_bell_lecture_per_week;
            //$scope.temp['sims_bell_lecture_practical_count'] = $scope.copyobject.sims_bell_lecture_practical_count;
            //$scope.temp['sims_bell_lecutre_count'] = $scope.copyobject.sims_bell_lecutre_count;
            //$scope.temp['sims_bell_practical_per_week'] = $scope.copyobject.sims_bell_practical_per_week;
            //$scope.temp['sims_bell_slot_group'] = $scope.copyobject.sims_bell_slot_group;
            //$scope.temp['sims_bell_slot_group_desc'] = $scope.copyobject.sims_bell_slot_group;
            //$scope.temp['sims_bell_slot_per_lecture'] =$scope.copyobject.sims_bell_slot_per_lecture;
            //$scope.temp['sims_bell_slot_per_practical'] = $scope.copyobject.sims_bell_slot_per_practical;
            $scope.insert = false;
            $scope.Table2 = false;
            $scope.getSubjects();
        }

        $scope.Cancel = function () {
            $scope.Grid = true;
            $scope.insert = true;
            $scope.temp = [];
            $scope.table = true;
        }

    }])
})();