﻿(function () {
    'use strict';
    var subject1 = [],
        subject, Employee_code = [],
        subject_code = [],
        bell_code = [],
        lesson_type = [],
        group_order = [];
    var le = 0;
    var main, strMessage;

    var simsController = angular.module('sims.module.Scheduling');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('BellSectionSubjectTeacher_DMCCont', ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
        $scope.display = false;

        $timeout(function () {
            $("#fixTable").tableHeadFixer({
                "top": 1
            });
        }, 100);
        $scope.Grid = true;

        $scope.table = false;
        $scope.insert = true;
        $scope.busyindicator = true;
        $scope.edt = {};

        $scope.propertyName = null;
        $scope.reverse = false;
        $scope.showRFIDDMC = false;

        $scope.sortBy = function (propertyName) {
            $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        };


        if ($http.defaults.headers.common['schoolId'] == 'dmc' || $http.defaults.headers.common['schoolId'] == 'sis') {
            $scope.showRFIDDMC = true;
        }

        $scope.getacyr = function (str) {
            console.log(str);
            $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                $scope.getAcademicYear = Academicyear.data;
                $scope.edt['academic_year'] = $scope.getAcademicYear[0].sims_academic_year;
            });
        }

        //$http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
        //    $scope.curriculum = res.data;
        //    $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code
        //    $scope.getacyr($scope.curriculum[0].sims_cur_code)

        //});

        $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum?username=" + $rootScope.globals.currentUser.username).then(function (res) {
            debugger;
            $scope.curriculum = res.data;
            $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code
            $scope.getacyr($scope.curriculum[0].sims_cur_code)
        });

        //function getCur(flag, comp_code) {
        //    if (flag) {

        //        $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
        //            $scope.curriculum = res.data;
        //            $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code
        //            $scope.getacyr($scope.curriculum[0].sims_cur_code)

        //        });
        //    }
        //    else {

        //        $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
        //            $scope.curriculum = res.data;
        //            $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code
        //            $scope.getacyr($scope.curriculum[0].sims_cur_code)

        //        });


        //    }

        //}

        //$http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
        //    $scope.global_count_comp = res.data;

        //    if ($scope.global_count_comp) {
        //        getCur(true, $scope.user_details.comp);


        //    }
        //    else {
        //        getCur(false, $scope.user_details.comp)
        //    }
        //});

        $http.get(ENV.apiUrl + "api/bellsectionsubject/GetAllTeacherName").then(function (GetAllTeacherName) {
            $scope.GetAllTeacherName = GetAllTeacherName.data;
        });

        $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
            $scope.ComboBoxValues = AllComboBoxValues.data;
        });

        $scope.term_changeNew = function (section_name) {
            $http.get(ENV.apiUrl + "api/StudentSectionSubject/getTermNew?curcode=" + $scope.edt.sims_cur_code + "&academicyear=" + $scope.edt.academic_year + "&sims_grade_code=" + $scope.edt.grade_code + "&sims_section_code=" + section_name).then(function (terms) {
                $scope.terms_obj = terms.data;
                $scope.edt.sims_section_subject_term_code = '';
            });
        };

        $scope.Show_Data = function () {
            $scope.expanded = true;
            $scope.busyindicator = false;

            if (($scope.edt.grade_code == "" || $scope.edt.grade_code == undefined) && ($scope.edt.section_code == "" || $scope.edt.section_code == undefined)) {
                swal({
                    text: 'Please select Grade and Section ',
                    width: 300,
                    showCloseButton: true
                });
            } else {
                $http.get(ENV.apiUrl + "api/bellsectionsubject/GetSectionSubjectTeacher?data=" + JSON.stringify($scope.edt)).then(function (AllTeacher_Name) {

                    $scope.All_Teacher_Name1 = AllTeacher_Name.data;

                    for (var i = 0; i < $scope.All_Teacher_Name1.length; i++) {
                        if ($scope.All_Teacher_Name1[i].sims_bell_slot_room_desc == "" || $scope.All_Teacher_Name1[i].sims_gb_teacher_code_name == "" || $scope.All_Teacher_Name1[i].sims_teacher_code == "") {
                            $scope.All_Teacher_Name1[i].color = '#e8f7c2';
                        }
                    }

                    $scope.All_Teacher_Name = $scope.All_Teacher_Name1;
                    var subject_names1 = [];

                    $http.get(ENV.apiUrl + "api/common/SectionSubject/getSubjectByIndex?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.grade_code + "&academicyear=" + $scope.edt.academic_year + "&sectioncode=" + $scope.edt.section_code).then(function (allSection_Subject) {

                        $scope.SectionSubject5 = allSection_Subject.data;
                        $scope.busyindicator = true;
                        $scope.table = true;
                        for (var i = 0; i < $scope.SectionSubject5.length; i++) {

                            if ($scope.SectionSubject5[i].sims_subject_status == true) {

                                var data = ({
                                    'sims_subject_code': $scope.SectionSubject5[i].sims_subject_code,
                                    'sims_subject_name': $scope.SectionSubject5[i].sims_subject_name
                                })
                                subject_names1.push(data);
                            }
                        }
                        $scope.subject_names = subject_names1;
                    });
                });
            }
        }

        $scope.Show_Data_DMC = function () {

            if (($scope.edt.grade_code == "" || $scope.edt.grade_code == undefined) && ($scope.edt.section_code == "" || $scope.edt.section_code == undefined)) {
                swal({
                    text: 'Please select Grade and Section ',
                    width: 300,
                    showCloseButton: true
                });
            }
            if (($scope.edt.sims_section_subject_term_code == "" || $scope.edt.sims_section_subject_term_code == undefined)) {
                swal({
                    text: 'Please select term ',
                    width: 300,
                    showCloseButton: true
                });
            }
            else {

                $scope.expanded = true;
                $scope.busyindicator = false;

                $http.get(ENV.apiUrl + "api/bellsectionsubject/GetSectionSubjectTeacher_DMC?data=" + JSON.stringify($scope.edt)).then(function (AllTeacher_Name) {

                    $scope.All_Teacher_Name1 = AllTeacher_Name.data;

                    for (var i = 0; i < $scope.All_Teacher_Name1.length; i++) {
                        if ($scope.All_Teacher_Name1[i].sims_bell_slot_room_desc == "" || $scope.All_Teacher_Name1[i].sims_gb_teacher_code_name == "" || $scope.All_Teacher_Name1[i].sims_teacher_code == "") {
                            $scope.All_Teacher_Name1[i].color = '#e8f7c2';
                        }
                    }

                    $scope.All_Teacher_Name = $scope.All_Teacher_Name1;
                    var subject_names1 = [];

                    $http.get(ENV.apiUrl + "api/common/SectionSubject/getSubjectByIndex?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.grade_code + "&academicyear=" + $scope.edt.academic_year + "&sectioncode=" + $scope.edt.section_code).then(function (allSection_Subject) {

                        $scope.SectionSubject5 = allSection_Subject.data;
                        $scope.busyindicator = true;
                        $scope.table = true;
                        for (var i = 0; i < $scope.SectionSubject5.length; i++) {

                            if ($scope.SectionSubject5[i].sims_subject_status == true) {

                                var data = ({
                                    'sims_subject_code': $scope.SectionSubject5[i].sims_subject_code,
                                    'sims_subject_name': $scope.SectionSubject5[i].sims_subject_name
                                })
                                subject_names1.push(data);
                            }
                        }
                        $scope.subject_names = subject_names1;
                    });
                });
            }
        }

        $scope.SaveBellData = function (isvalid, str) {

            if (isvalid) {
                var data;
                if ($scope.temp !== undefined) {
                    data = {
                        'academic_year': $scope.edt.academic_year,
                        'sims_bell_desc': str.sims_bell_desc,
                        'sims_grade_code': $scope.edt.grade_code,
                        'sims_section_code': $scope.edt.section_code,
                        'sims_subject_code': str.sims_subject_code,
                        'sims_gb_teacher_code': $scope.teacher_code,
                        'sims_bell_lecutre_count': (parseInt(str.sims_bell_lecture_per_week) * parseInt(str.sims_bell_slot_per_lecture)) + (parseInt(str.sims_bell_lecture_practical_count) * parseInt(str.sims_bell_slot_per_practical)),
                        'sims_bell_lecture_practical_count': str.sims_bell_lecture_practical_count,
                        'sims_bell_slot_group_code': str.sims_bell_slot_group_code,
                        'sims_bell_slot_room': str.sims_bell_slot_room,
                        'sims_bell_group_order': str.sims_bell_group_order,
                        'sims_bell_lecture_per_week': str.sims_bell_lecture_per_week,
                        'sims_bell_slot_per_lecture': str.sims_bell_slot_per_lecture,
                        'sims_bell_practical_per_week': str.sims_bell_practical_per_week,
                        'sims_bell_slot_per_practical': str.sims_bell_slot_per_practical,
                        'sims_lesson_type': str.sims_lesson_type,
                        'sims_bell_section_term': $scope.edt.sims_section_subject_term_code,
                        'opr': 'I'
                    }
                    $http.post(ENV.apiUrl + "api/bellsectionsubject/InsertSectionSubjectTeacher_DMC", data).then(function (msg) {

                        $scope.data = '';
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({
                                text: 'Information Inserted Successfully',
                                imageUrl: "assets/img/check.png",
                                width: 300,
                                showCloseButton: true
                            });
                            $scope.table = true;
                            $scope.Grid = true;
                            $scope.insert = true;
                            $scope.temp = "";
                            $scope.Show_Data_DMC();
                        } else if ($scope.msg1 == false) {

                            swal({
                                text: 'Information Not Inserted. ',
                                imageUrl: "assets/img/close.png",
                                width: 300,
                                showCloseButton: true
                            });
                        } else {
                            swal({
                                text: "Section Bell Not Defined For This Grade And Section",
                                imageUrl: "assets/img/close.png",
                                width: 300,
                                showCloseButton: true,
                                height: 250
                            });
                            $scope.Show_Data_DMC();
                        }

                    });

                } else {
                    swal({
                        text: 'Please Fill Field(S) To Insert Record',
                        imageUrl: "assets/img/notification-alert.png",
                        width: 300,
                        showCloseButton: true
                    });
                    $scope.table = true;
                }
            }
            $scope.data = '';
        }

        $scope.UpdateBellData = function () {
            $scope.flag = true;
            if ($scope.flag) {
                swal({
                    title: '',
                    text: "Are you sure you want to Update the data and delete the related fields for all record?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {

                    if (isConfirm) {
                        var Sdata = [{}];
                        for (var j = 0; j < $scope.GetAllTeacherName.length; j++) {
                            for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {
                                $scope.new_teacher_code = $scope.All_Teacher_Name[i].sims_gb_teacher_code_name.split('/')[0];
                                $scope.old_teacher_code = $scope.All_Teacher_Name[i].sims_gb_teacher_code_name.split('/')[0];
                                if ($scope.GetAllTeacherName[j].sims_teacher_code == $scope.new_teacher_code) {
                                    $scope.teacher_code = $scope.GetAllTeacherName[j].sims_employee_code;
                                    break;
                                }
                            }
                        }

                        for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {

                            if ($scope.All_Teacher_Name[i].ischange == true) {
                                if ($scope.All_Teacher_Name[i].sims_bell_group_order == "" || $scope.All_Teacher_Name[i].sims_bell_group_order == undefined || $scope.All_Teacher_Name[i].sims_bell_group_order == null) {
                                    $scope.All_Teacher_Name[i].sims_bell_group_order = '1';
                                }
                                $scope.new_teacher_code = $scope.All_Teacher_Name[i].sims_gb_teacher_code_name.split('/')[0];
                                if ($scope.new_teacher_code != '' && $scope.new_teacher_code != undefined) {
                                    for (var j = 0; j < $scope.GetAllTeacherName.length; j++) {
                                        if ($scope.GetAllTeacherName[j].sims_teacher_code == $scope.new_teacher_code) {
                                            $scope.All_Teacher_Name[i].sims_teacher_code = $scope.GetAllTeacherName[j].sims_employee_code;
                                            break;
                                        }
                                    }
                                }

                                var le = Sdata.length;
                                Sdata[le] = {
                                    'academic_year': $scope.edt.academic_year,
                                    'sims_bell_desc': $scope.All_Teacher_Name[i].sims_bell_code,
                                    'sims_grade_code': $scope.edt.grade_code,
                                    'sims_section_code': $scope.edt.section_code,
                                    'sims_subject_code': $scope.All_Teacher_Name[i].sims_subject_code,
                                    'sims_gb_teacher_code': $scope.All_Teacher_Name[i].sims_teacher_code,
                                    'sims_bell_lecutre_count': (parseInt($scope.All_Teacher_Name[i].sims_bell_lecture_per_week) * parseInt($scope.All_Teacher_Name[i].sims_bell_slot_per_lecture)) + (parseInt($scope.All_Teacher_Name[i].sims_bell_practical_per_week) * parseInt($scope.All_Teacher_Name[i].sims_bell_slot_per_practical)),
                                    'sims_bell_lecture_practical_count': $scope.All_Teacher_Name[i].sims_bell_lecture_practical_count,
                                    'sims_bell_slot_group_code': ($scope.All_Teacher_Name[i].sims_bell_slot_group == undefined || $scope.All_Teacher_Name[i].sims_bell_slot_group == '') ? '01' : $scope.All_Teacher_Name[i].sims_bell_slot_group,
                                    'sims_bell_slot_room': ($scope.All_Teacher_Name[i].sims_bell_slot_room == undefined || $scope.All_Teacher_Name[i].sims_bell_slot_room == '') ? '01' : $scope.All_Teacher_Name[i].sims_bell_slot_room,
                                    'sims_bell_group_order': $scope.All_Teacher_Name[i].sims_bell_group_order,
                                    'sims_bell_lecture_per_week': $scope.All_Teacher_Name[i].sims_bell_lecture_per_week,
                                    'sims_bell_slot_per_lecture': $scope.All_Teacher_Name[i].sims_bell_slot_per_lecture,
                                    'sims_bell_practical_per_week': $scope.All_Teacher_Name[i].sims_bell_practical_per_week,
                                    'sims_bell_slot_per_practical': $scope.All_Teacher_Name[i].sims_bell_slot_per_practical,
                                    'sims_lesson_type': $scope.All_Teacher_Name[i].sims_lession_type1,
                                    'sims_old_teacher_code': $scope.All_Teacher_Name[i].sims_old_teacher_code,

                                    'sims_bell_section_block': $scope.All_Teacher_Name[i].sims_bell_section_block,
                                    'sims_bell_section_block_size': $scope.All_Teacher_Name[i].sims_bell_section_block_size,
                                    'sims_bell_section_term': $scope.edt.sims_section_subject_term_code,
                                    'opr': 'U'
                                }
                            }
                        }



                        Sdata.splice(0, 1);
                        var data = Sdata;

                        if (data.length > 0) {
                            $http.post(ENV.apiUrl + "api/bellsectionsubject/UpdateSectionSubjectTeacher_DMC?simsobj=", data).then(function (msg) {

                                $scope.busyindicator = false;
                                $scope.table = false;

                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({
                                        text: 'Information Updated Successfully',
                                        imageUrl: "assets/img/check.png",
                                        width: 300,
                                        showCloseButton: true
                                    });
                                    $scope.teacher_code = '';
                                    $scope.Show_Data_DMC();
                                    $scope.busyindicator = true;
                                    $scope.table = true;
                                    //swal({text: 'Information Updated Successfully', timer: 2000, showConfirmButton: false });
                                } else if ($scope.msg1 == false) {

                                    swal({
                                        text: 'Information Not Updated',
                                        imageUrl: "assets/img/close.png",
                                        width: 300,
                                        showCloseButton: true
                                    });
                                    $scope.Show_Data_DMC();
                                    $scope.table = true;
                                    $scope.busyindicator = true;
                                } else {
                                    swal({
                                        text: 'Error. Either Section Bell Not Defined or Database not created',
                                        imageUrl: "assets/img/close.png",
                                        width: 300,
                                        height: 250,
                                        showCloseButton: true
                                    });
                                    $scope.Show_Data_DMC();
                                }
                            });
                        }
                    }

                });
            } else {
                swal({
                    text: 'Please Change At Least One Field To Update',
                    imageUrl: "assets/img/notification-alert.png",
                    width: 380,
                    showCloseButton: true
                });
                $scope.table = true;
                $scope.busyindicator = true;
            }
        }

        $scope.CheckOnebyOneDelete = function () {

            $("input[type='checkbox']").change(function (e) {
                if ($(this).is(":checked")) { //If the checkbox is checked
                    $(this).closest('tr').addClass("row_selected");
                    //Add class on checkbox checked
                    $scope.color = '#edefef';
                } else {
                    $(this).closest('tr').removeClass("row_selected");
                    //Remove class on checkbox uncheck
                    $scope.color = '#edefef';
                }
            });
        }

        $scope.DeleteTeacherData = function () {
            for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {
                var t = document.getElementById($scope.All_Teacher_Name[i].sims_teacher_code + $scope.All_Teacher_Name[i].sims_subject_code)
                if (t.checked == true) {
                    Employee_code = Employee_code + $scope.All_Teacher_Name[i].sims_teacher_code + ',';
                    subject_code = subject_code + $scope.All_Teacher_Name[i].sims_subject_code + ',';
                    bell_code = bell_code + $scope.All_Teacher_Name[i].sims_bell_code + ',';
                    lesson_type = lesson_type + $scope.All_Teacher_Name[i].sims_lession_type1 + ',';
                    group_order = group_order + $scope.All_Teacher_Name[i].sims_bell_group_order + ',';

                }
            }
            var Employee_code_list = ({
                'sims_cur_code': $scope.edt.sims_cur_code,
                'sims_grade_code': $scope.edt.grade_code,
                'sims_section_code': $scope.edt.section_code,
                'academic_year': $scope.edt.academic_year,
                'sims_gb_teacher_code': Employee_code,
                'sims_subject_code': subject_code,
                'sims_bell_desc': bell_code,
                'sims_lesson_type': lesson_type,
                'sims_bell_group_order': group_order,
                'opr': 'D'
            });



            $http.post(ENV.apiUrl + "api/bellsectionsubject/SectionSubjectTeacherDelete?simsobj=", Employee_code_list).then(function (msg) {

                Employee_code = [];
                subject_code = [], bell_code = [], lesson_type = [];
                group_order = [];
                $scope.msg1 = msg.data;
                Employee_code_list = {};
                if ($scope.msg1 == true) {
                    swal({
                        text: 'Information Deleted Successfully',
                        imageUrl: "assets/img/check.png",
                        width: 300,
                        showCloseButton: true
                    });
                    $scope.Show_Data_DMC();
                } else if ($scope.msg1 == false) {
                    swal({
                        text: 'Information Not Deleted. ' + $scope.msg1,
                        imageUrl: "assets/img/close.png",
                        width: 300,
                        showCloseButton: true
                    });
                } else {
                    swal("Error-" + $scope.msg1)
                }
            });
        }

        $scope.Check = function (teacher) {
            teacher.ischange = true;

        }

        $scope.chk_b = function (flg, val, info) {
            if (flg) {
                info.sims_bell_section_block_size = val;
            } else {
                info.sims_bell_section_block_size = '';
            }

        }
        $scope.showcmb = function (str) {
            for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {
                if (str == $scope.All_Teacher_Name[i].sims_subject_name)
                    $scope[str] = true;
            }
        }

        var teacher_code1 = '';

        $scope.CheckTeacher = function (str) {

            teacher_code1 = str.sims_teacher_code;
            teacher_code_old = str.sims_teacher_code;
            $scope.Check(str);
        }

        $scope.CheckteacherAssigned = function (teacher_name1, subject_name) {

            for (var i = 0; i < $scope.GetAllTeacherName.length; i++)
                if ($scope.GetAllTeacherName[i].sims_teacher_code == teacher_name1.split('/')[0]) {
                    $scope.teacher_code = $scope.GetAllTeacherName[i].sims_employee_code;
                }


            for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {
                if ($scope.All_Teacher_Name[i].sims_teacher_code == $scope.teacher_code && $scope.All_Teacher_Name[i].sims_subject_code == subject_name) {
                    swal('', 'Please Select Another Teacher, This Teacher  already Assiged Same Subject ' + '   ' + $scope.All_Teacher_Name[i].sims_subject_name);
                    $scope.temp.sims_gb_teacher_code_name = '';
                    teacher_name1 = '';
                    $scope.teacher_code = '';
                }
            }
            $scope.new = true;
        }

        $scope.AddNewData = function (info) {
            //$scope.NewData = true;
            $scope.Grid = true;
            $scope.table = false;
            $scope.copyobject = angular.copy(info);

            $scope.temp = [];

            $scope.copyobject.sims_bell_code = '';
            $scope.copyobject.sims_bell_desc = '';

            $scope.copyobject.sims_bell_slot_room = '';
            $scope.copyobject.sims_bell_slot_room_desc = '';
            $scope.copyobject.sims_gb_teacher_code_name = '';
            $scope.copyobject.sims_grade_section = '';
            $scope.copyobject.sims_lession_type1 = '';
            $scope.copyobject.sims_lesson_type = '';
            $scope.copyobject.sims_pre_status = '';
            $scope.copyobject.sims_section_code
            $scope.copyobject.sims_sub_status
            $scope.copyobject.sims_subject_code
            $scope.copyobject.sims_subject_name
            $scope.copyobject.sims_teacher_code = '';

            $scope.temp.push($scope.copyobject);
            $scope.temp['sims_subject_code'] = $scope.copyobject.sims_subject_code
            $scope.temp['sims_bell_group_order'] = $scope.copyobject.sims_bell_group_order;
            $scope.temp['sims_bell_lecture_per_week'] = $scope.copyobject.sims_bell_lecture_per_week;
            $scope.temp['sims_bell_lecture_practical_count'] = $scope.copyobject.sims_bell_lecture_practical_count;
            $scope.temp['sims_bell_lecutre_count'] = $scope.copyobject.sims_bell_lecutre_count;
            $scope.temp['sims_bell_practical_per_week'] = $scope.copyobject.sims_bell_practical_per_week;
            $scope.temp['sims_bell_slot_group'] = $scope.copyobject.sims_bell_slot_group;
            $scope.temp['sims_bell_slot_group_desc'] = $scope.copyobject.sims_bell_slot_group;
            $scope.temp['sims_bell_slot_per_lecture'] = $scope.copyobject.sims_bell_slot_per_lecture;
            $scope.temp['sims_bell_slot_per_practical'] = $scope.copyobject.sims_bell_slot_per_practical;
            $scope.insert = false;
        }

        $scope.Cancel = function () {
            $scope.Grid = true;
            $scope.insert = true;
            $scope.temp = [];
            $scope.table = true;
        }

    }])
})();