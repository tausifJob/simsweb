﻿(function () {
    'use strict';
    var main, temp, del = [];
    var simsController = angular.module('sims.module.Scheduling');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('timeTableUploadCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.display = false;
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.grid = true;
            $scope.currentPage = 0;
            $scope.items = [];
            $scope.url = ENV.apiUrl + 'Content' + '/' + $http.defaults.headers.common['schoolId'];
            var photofile;

            $scope.countData = [
              { val: 5, data: 5 },
              { val: 10, data: 10 },
              { val: 15, data: 15 },

            ]

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.items;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

               // $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                //$scope.CheckAllChecked();
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.items, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.items;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_timetable_cur_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_section_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_timetable_number == toSearch) ? true : false;
            }
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_timetable_number + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_timetable_number + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.getgrid = function () {

                
                $http.get(ENV.apiUrl + "api/common/TimeTable/GetAll_Sims_Timetable_file").then(function (res) {
                    debugger;
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.items = res.data;
                    if ($scope.items.length > 0) {
                        $scope.table = true;
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.items.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.items.length, data: 'All' })
                        }
                        $scope.totalItems = $scope.items.length;
                        $scope.todos = $scope.items;
                        $scope.currentPage = 1;
                        $scope.makeTodos();
                    }
                    else {
                        $scope.table = false;
                        swal({ text: "Record Not Found", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                        $scope.filteredTodos = [];
                    }

                    


                });
            }

            $http.get(ENV.apiUrl + "api/common/TimeTable/GetAll_Sims_Timetable_file").then(function (res) {
                debugger;
                $scope.display = false;
                $scope.grid = true;
                $scope.items = res.data;
                if ($scope.items.length > 0) {
                    $scope.table = true;
                    $scope.pager = true;
                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.items.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.items.length, data: 'All' })
                    }
                    $scope.totalItems = $scope.items.length;
                    $scope.todos = $scope.items;
                    $scope.makeTodos();
                }
                else {
                    $scope.table = false;
                    swal({ text: "Record Not Found", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    $scope.filteredTodos = [];
                }

                console.log($scope.filteredTodos);


            });

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.cmbcur = res.data;
                if (res.data.length > 0) {
                    $scope.edt.sims_timetable_cur = res.data[0].sims_cur_code;
                    $scope.curChange($scope.edt.sims_timetable_cur);
                }
                console.log($scope.cmbcur);


            });

            $scope.curChange = function (cur) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur).then(function (res) {
                    $scope.cmbAcademic = res.data;
                    if (res.data.length > 0) {
                        $scope.edt.sims_academic_year = res.data[0].sims_academic_year;
                        $scope.academicChange($scope.edt.sims_academic_year);
                    }

                });

            }

            $scope.academicChange = function (academic) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_timetable_cur + "&academic_year=" + academic).then(function (res) {

                    $scope.cmbGrade = res.data;

                });

            }

            $scope.gradeChange = function (grade) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.edt.sims_timetable_cur + "&grade_code=" + grade + "&academic_year=" + $scope.edt.sims_academic_year).then(function (res) {

                    $scope.cmbsection = res.data;

                });

            }

            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;

            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.items.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;
            };

            $scope.edit = function (str) {
                $scope.edt = str;
                $scope.display = true;
                $scope.grid = false;
                $scope.update1 = true;
                $scope.editflg = true;
                $scope.save1 = false;
                $scope.curChange(str.sims_timetable_cur);
                $scope.academicChange(str.sims_academic_year);
                $scope.gradeChange(str.sims_grade);
                //  $scope.edt = str;


            }

            $scope.downloaddoc = function (str) {
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Docs/timetable/' + str;
                window.open($scope.url);
            }
            $scope.Update = function (newEventForm) {
                debugger;
                if (newEventForm.$valid) {

                    var curriculumname = document.getElementById("txt_Section_name");
                    var curriculumname1 = curriculumname.options[curriculumname.selectedIndex].text;

                    var ayear = document.getElementById("txt_name_en");
                    var ayear1 = ayear.options[ayear.selectedIndex].text;

                    var gradename = document.getElementById("txt_name_fr");
                    var gradename1 = gradename.options[gradename.selectedIndex].text;
                    var sectionname = document.getElementById("txt_name_ot");
                    var sectionname1 = sectionname.options[sectionname.selectedIndex].text;
                    //var filename1 = curriculumname1 + ayear1 + gradename1 + sectionname1 + '.' + fortype;
                    var filename1 = curriculumname1 + ayear1 + gradename1 + sectionname1;
                    var request = {
                        method: 'POST',
                       // url: ENV.apiUrl + 'api/common/TimeTable/upload?filename=' + filename1 + "&location=" + "/Docs/timetable",
                        url: ENV.apiUrl + 'api/file/uploadDocument_new?filename=' + filename1 + "&location=" + "Docs/timetable/",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    
                    $http(request).success(function (d) {
                        //alert(d);
                        var updatedata = [];
                        var data = $scope.edt;
                        data.opr = 'U';
                        if (d !== 'false')
                            data.sims_timetable_filename = d;
                        updatedata.push(data);
                        $http.post(ENV.apiUrl + "api/common/TimeTable/Insert_Sims_Timetable_File", updatedata).then(function (res) {
                            $scope.ins = res.data;
                            if (res.data==true) {
                                swal({  text: "Record Updated Successfully.", imageUrl: "assets/img/check.png" });
                                $scope.getgrid();

                            }
                            else if (res.data == false)
                                swal({ text: "Record Not Updated .", imageUrl: "assets/img/close.png", });
                            else
                                swal("Error-" + $scope.ins)

                        });



                    });
                }
            }

            $scope.Save = function (newEventForm) {
                debugger;
                if (newEventForm.$valid) {
                    var curriculumname = document.getElementById("txt_Section_name");
                    var curriculumname1 = curriculumname.options[curriculumname.selectedIndex].text;

                    var ayear = document.getElementById("txt_name_en");
                    var ayear1 = ayear.options[ayear.selectedIndex].text;

                    var gradename = document.getElementById("txt_name_fr");
                    var gradename1 = gradename.options[gradename.selectedIndex].text;
                    var sectionname = document.getElementById("txt_name_ot");
                    var sectionname1 = sectionname.options[sectionname.selectedIndex].text;
                    //var filename1 = curriculumname1 + ayear1 + gradename1 + sectionname1 + '.' + fortype;
                    var filename1 = curriculumname1 + ayear1 + gradename1 + sectionname1 ;
                    var request = {

                        method: 'POST',
                        //url: ENV.apiUrl + 'api/common/TimeTable/upload?filename=' + filename1 + "&location=" + "Docs/timetable",
                        url: ENV.apiUrl + 'api/file/uploadDocument_new?filename=' + filename1 + "&location=" + "Docs/timetable/",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };

                    $http(request).success(function (d) {
                        //alert(d);
                        var senddata = [];
                        var data = $scope.edt;
                        data.opr = 'I';
                        data.sims_timetable_filename = d;
                        senddata.push(data);
                        $http.post(ENV.apiUrl + "api/common/TimeTable/Insert_Sims_Timetable_File", senddata).then(function (res) {
                            $scope.ins = res.data;
                            if (res.data==true) {
                                swal({ text: "Record Inserted Successfully.", imageUrl: "assets/img/check.png" });
                                $scope.getgrid();

                            }
                            else if (res.data == false)
                                swal({ text: "Record Not Inserted.", imageUrl: "assets/img/close.png", });
                            else
                                swal("Error-" + $scope.ins)

                        });



                    });
                }
            }


            $scope.New = function () {
                $scope.edt = "";
                $scope.grid = false;
                $scope.display = true;
                $scope.update1 = false;
                $scope.save1 = true;
                $scope.editflg = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

                $scope.edt = {};
                //$scope.edt.sims_reference_status = true;
                $scope.edt.sims_timetable_status = true;
                
                if ($scope.cmbcur.length > 0) {
                    $scope.edt.sims_timetable_cur = $scope.cmbcur[0].sims_cur_code;
                    $scope.curChange($scope.edt.sims_timetable_cur);
                }

                
                if ($scope.cmbAcademic.length > 0) {
                    $scope.edt.sims_academic_year = $scope.cmbAcademic[0].sims_academic_year;
                    $scope.academicChange($scope.edt.sims_academic_year);
                }

            }


            var main, del = '', delvar = [];
            $scope.chk = function () {

                main = document.getElementById('chk_min');
                del = [];
                if (main.checked == true) {
                    for (var i = 0; i < $scope.items.length; i++) {
                        $scope.items[i].sims_timetable_number1 = true;


                    }
                }
                else {
                    for (var i = 0; i < $scope.items.length; i++) {
                        $scope.items[i].sims_timetable_number1 = false;

                        main.checked = false;

                        $scope.row1 = '';
                    }
                }


            }


            //$scope.Delete = function () {
            //    debugger;
            //    del = '';
            //    for (var i = 0; i < $scope.items.length; i++) {
            //        // var t = $scope.items[i].sims_timetable_number;
            //        // var v = document.getElementById(t);
            //        if ($scope.items[i].sims_timetable_number1 == true) {
            //            del = del + $scope.items[i].sims_timetable_number + ','
            //        }

            //    }
            //    console.log(del)
            //    //if ($scope.items.length > 0)
            //    //    del = del.substring(0, del.length - 1);
            //    //console.log(del)
            //    //delvar = [];
            //    //delvar.push({ sims_timetable_number: del });

            //    //$http.post(ENV.apiUrl + "api/common/TimeTable/Insert_Sims_Timetable_File?obj=" + JSON.stringify(delvar[0]) + "&opr=X").then(function (res) {

            //    var data = $scope.items[0];

            //    data.opr = 'X';
            //    data.sims_timetable_number = del;

            //    $http.post(ENV.apiUrl + "api/common/TimeTable/Insert_Sims_Timetable_File", data).then(function (res) {

            //        if (res.data) {
            //            swal({ title: "Alert", text: "Record Deleted Successfully.", imageUrl: "assets/img/check.png", });


            //            $scope.getgrid();
            //        }
            //        else {
            //            swal({ title: "Alert", text: "Record Not Deleted.", imageUrl: "assets/img/notification-alert.png", });

            //        }
            //    });

            //}
            var fortype = '';
            $scope.Delete = function () {
                debugger;
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    //var t = $scope.filteredTodos[i].uom_code;
                    var v = document.getElementById($scope.filteredTodos[i].sims_timetable_number + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sims_timetable_number': $scope.filteredTodos[i].sims_timetable_number,
                            'sims_timetable_cur': $scope.filteredTodos[i].sims_timetable_cur,
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            'sims_grade': $scope.filteredTodos[i].sims_grade,
                            'sims_section': $scope.filteredTodos[i].sims_section,
                            'sims_timetable_filename': $scope.filteredTodos[i].sims_timetable_filename,
                            //'sims_report_card_desc': $scope.filteredTodos[i].sims_report_card_desc,
                            //'sims_report_card_declaration_date': $scope.filteredTodos[i].sims_report_card_declaration_date,
                            //'sims_report_card_publish_date': $scope.filteredTodos[i].sims_report_card_publish_date,
                            //'sims_report_card_created_user': $scope.filteredTodos[i].sims_report_card_created_user,
                            //'sims_report_card_freeze_status': $scope.filteredTodos[i].sims_report_card_freeze_status,
                            //'param_desc': $scope.filteredTodos[i].param_desc,
                            //'term_name': $scope.filteredTodos[i].sims_report_card_term,
                            'opr': 'D',

                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {

                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/common/TimeTable/Insert_Sims_Timetable_File", deletecode).then(function (res) {

                                if (res.data==true) {
                                    swal({  text: "Record Deleted Successfully.", imageUrl: "assets/img/check.png" });


                                    $scope.getgrid();
                                }
                                else if (res.data == false) {
                                    swal({ text: "Record Not Deleted.", imageUrl: "assets/img/close.png" });

                                }
                                else
                                {
                                    swal("Error-" + $scope.ins)
                                }
                              
                            });
                        }
                        else {

                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_timetable_number + i);
                                //var t = $scope.filteredTodos[i].sims_fee_category;
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");


                                }


                            }

                        }

                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }

                $scope.currentPage = 1;
            }

            $scope.cancel = function () {
                debugger;
                $scope.grid = true;
                $scope.display = false;
                $scope.update1 = false;
                $scope.save1 = false;
                $scope.editflg = false;
                $scope.getgrid();
            }

            var formdata = new FormData();
            debugger;
            $scope.getTheFiles = function ($files) {
                $scope.filesize = true;
                angular.forEach($files, function (value, key) {

                    formdata.append(key, value);

                    var i = 0;
                    //if ($files[i].size > 200000) {
                    //    $scope.filesize = false;
                    //    $scope.edt.photoStatus = false;

                    //    swal({ title: "Alert", text: "File Should Not Exceed 200Kb.", imageUrl: "assets/img/notification-alert.png", });

                    //}
                    //else {




                    //}

                });
            };

            $scope.file_changed = function (element, str) {
                debugger;
                photofile = element.files[0];


                $scope.photo_filename = (photofile.name);

                var len = 0;
                len = $scope.photo_filename.split('.');
                fortype = $scope.photo_filename.split('.')[len.length - 1];
                $scope.edt1 = { sims_timetable_filename: $scope.photo_filename }
                $.extend($scope.edt, $scope.edt1)
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;

                    });
                };
                reader.readAsDataURL(photofile);


            };



            $scope.uploadClick = function (str) {
                debugger;
                $scope.filesize = true;
                //$scope.edt = str;
                //$scope.edt.photoStatus = true;
                //$scope.ins = false;
                formdata = new FormData();

            }


        }]);

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])


})();


