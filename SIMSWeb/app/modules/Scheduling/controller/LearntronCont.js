﻿(function () {
    'use strict';
    var main;
    
    var simsController = angular.module('sims.module.Scheduling');
    simsController.controller('LearntronCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var user = $rootScope.globals.currentUser.username;

            $scope.data1 = function (str) {

                $scope.disable();
                
                $http.post(ENV.apiUrl + "api/bell/insertupdate_ClassSubject").then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1!='') {
                        swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 });

                       
                        var data = {
                            comn_audit_user_name: user,
                            comn_audit_user_appl_code: 'Lrnton',
                            comn_audit_ip: str,
                            comn_audit_dns: null,
                            comn_audit_remark: $scope.msg1,
                            
                        }

                        $http.post(ENV.apiUrl + "api/bell/Insert_Learntron_user_audit_data", data).then(function (res1) {
                            $scope.msg1 = res1.data;
                            
                        });

                    }
                    else {
                        swal({ text: "No New Records To Update", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                    $scope.enable();

                });

            }

            $scope.data2 = function (str) {

                $scope.disable();

                $http.post(ENV.apiUrl + "api/bell/insertupdate_student_section_subject").then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 != '') {
                        swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 });

                        
                        var data = {
                            comn_audit_user_name: user,
                            comn_audit_user_appl_code: 'Lrnton',
                            comn_audit_ip: str,
                            comn_audit_dns: null,
                            comn_audit_remark: $scope.msg1,

                        }

                        $http.post(ENV.apiUrl + "api/bell/Insert_Learntron_user_audit_data", data).then(function (res1) {
                            $scope.msg1 = res1.data;

                        });
                    }
                    else {
                        swal({ text: "No New Records To Update", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                    $scope.enable();

                });

            }

            $scope.data3 = function (str) {

                $scope.disable();

                $http.post(ENV.apiUrl + "api/bell/insertupdate_homeroom_batch_student").then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 != '') {
                        swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 });

                        
                        var data = {
                            comn_audit_user_name: user,
                            comn_audit_user_appl_code: 'Lrnton',
                            comn_audit_ip: str,
                            comn_audit_dns: null,
                            comn_audit_remark: $scope.msg1,

                        }

                        $http.post(ENV.apiUrl + "api/bell/Insert_Learntron_user_audit_data", data).then(function (res1) {
                            $scope.msg1 = res1.data;

                        });
                    }
                    else {
                        swal({ text: "No New Records To Update", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                    $scope.enable();

                });

            }

            $scope.data4 = function (str) {

                $scope.disable();

                $http.post(ENV.apiUrl + "api/bell/insertupdate_TeacherMaster").then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 != '') {
                        swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 });

                        
                        var data = {
                            comn_audit_user_name: user,
                            comn_audit_user_appl_code: 'Lrnton',
                            comn_audit_ip: str,
                            comn_audit_dns: null,
                            comn_audit_remark: $scope.msg1,

                        }

                        $http.post(ENV.apiUrl + "api/bell/Insert_Learntron_user_audit_data", data).then(function (res1) {
                            $scope.msg1 = res1.data;

                        });
                    }
                    else {
                        swal({ text: "No New Records To Update", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                    $scope.enable();

                });

            }

            $scope.data5 = function (str) {

                $scope.disable();

                $http.post(ENV.apiUrl + "api/bell/insertupdate_homeroom_teacher").then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 != '') {
                        swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 });

                        
                        var data = {
                            comn_audit_user_name: user,
                            comn_audit_user_appl_code: 'Lrnton',
                            comn_audit_ip: str,
                            comn_audit_dns: null,
                            comn_audit_remark: $scope.msg1,

                        }

                        $http.post(ENV.apiUrl + "api/bell/Insert_Learntron_user_audit_data", data).then(function (res1) {
                            $scope.msg1 = res1.data;

                        });
                    }
                    else {
                        swal({ text: "No New Records To Update", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                    $scope.enable();

                });

            }

            $scope.data6 = function (str) {

                $scope.disable();

                $http.post(ENV.apiUrl + "api/bell/insertupdate_section_subject_teacher").then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 != '') {
                        swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 });

                        
                        var data = {
                            comn_audit_user_name: user,
                            comn_audit_user_appl_code: 'Lrnton',
                            comn_audit_ip: str,
                            comn_audit_dns: null,
                            comn_audit_remark: $scope.msg1,

                        }

                        $http.post(ENV.apiUrl + "api/bell/Insert_Learntron_user_audit_data", data).then(function (res1) {
                            $scope.msg1 = res1.data;

                        });
                    }
                    else {
                        swal({ text: "No New Records To Update", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                    $scope.enable();

                });

            }

            $scope.disable = function () {

                //$scope.busy = true;
                $('#loader').modal({ backdrop: 'static', keyboard: false });

                $scope.dis_cls_subj = true;
                $scope.dis_std_sec_subj = true;
                $scope.dis_batch_std = true;
                $scope.dis_teacher = true;
                $scope.dis_hr_teacher = true;
                $scope.dis_sec_subj_teacher = true;
            }

            $scope.enable = function () {


                $scope.dis_cls_subj = false;
                $scope.dis_std_sec_subj = false;
                $scope.dis_batch_std = false;
                $scope.dis_teacher = false;
                $scope.dis_hr_teacher = false;
                $scope.dis_sec_subj_teacher = false;

                $('#loader').modal('hide');
                $(".modal-backdrop").removeClass("modal-backdrop");

                //$scope.busy = false;
            }


          
        }])
})();

