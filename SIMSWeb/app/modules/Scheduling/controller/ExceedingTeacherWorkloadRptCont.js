﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Scheduling');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('ExceedingTeacherWorkloadRptCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.temp = {};
            $scope.detailsField = true;
            $scope.hideprintpdf = false;
            $scope.temp.sims_version = 'T';
            $scope.pagesize = '30';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['chk_all'] = false;
                $scope.row1 = '';
            }
        $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 30, $scope.maxSize = 100;

            $scope.makeTodos = function () {
                debugger;
                if ($scope.pagesize == 'All') {
                    $scope.currentPage = 1;
                    $scope.showPager = false;
                    $scope.filteredTodos = $scope.report_data_new;
                }
                else {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                }
            };
         $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res1) {
                $scope.cur_data = res1.data;
                $scope.temp.sims_cur_code = $scope.cur_data[0].sims_cur_code;
                $scope.getAcademic_year($scope.cur_data[0].sims_cur_code);
            });


    $scope.getAcademic_year = function (cur_code1) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code1).then(function (res1) {
                    $scope.acad_data = res1.data;
                    $scope.temp.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                    $scope.getbell($scope.temp.sims_academic_year);
                    $scope.getversion($scope.temp.sims_academic_year, $scope.temp.sims_bell);
                   
                });
            }

            $scope.getbell = function (acad_year) {
                debugger;
                $http.get(ENV.apiUrl + "api/SchedulingExceed/getBell?acad_year=" + $scope.temp.sims_academic_year).then(function (res1) {
                     $scope.Bell_as = res1.data;
                    $scope.temp.sims_bell = $scope.Bell_as[0].sims_bell_code;

                    setTimeout(function () {
                        $('#bell_id').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                    $scope.getversion($scope.temp.sims_academic_year, $scope.temp.sims_bell);

                });
            }

            $scope.getversion = function (acad_year,bell) {
                debugger;
                $http.get(ENV.apiUrl + "api/SchedulingExceed/getVersion?acad_year=" + $scope.temp.sims_academic_year + "&bell=" + $scope.temp.sims_bell).then(function (res1) {
                   
                    $scope.version_as = res1.data;
                    $scope.temp.sims_version = $scope.version_as[0].sims_bell_version_name;
                });
            }

            

            $http.get(ENV.apiUrl + "api/SchedulingExceed/getType").then(function (res1) {
                debugger;
                $scope.type_as = res1.data;
                $scope.temp.sims_type = $scope.type_as[0].value;

            });

          


            $scope.getdetails = function () {
                
                //$scope.hide_column = '';
                //var v = document.getElementById('chk_report');
                //if (v.checked == true) {
                //    $scope.hide_column=
                //}

                debugger;
                $http.get(ENV.apiUrl + "api/SchedulingExceed/getExceedingWorkLoad?cur_code=" + $scope.temp.sims_cur_code + "&acad_year=" + $scope.temp.sims_academic_year + "&bell=" + $scope.temp.sims_bell + "&version=" + $scope.temp.sims_version + "&param=" + $scope.temp.sims_type + "&value=" + $scope.temp.enter_value).then(function (res1) {
                    if (res1.data.length > 0) {
                        $scope.report_data = res1.data;
                        $scope.report_data = res1.data;
                        $scope.totalItems = $scope.report_data.length;
                        $scope.todos = $scope.report_data;
                        $scope.makeTodos();
                    }
                    else {
                        swal({ title: "Alert", text: "Data is not Available", showCloseButton: true, width: 300, height: 200 });
                        $scope.report_data = [];

                    }

                });
            }



           




       


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };
            $scope.search = function () {

                $scope.todos = $scope.searched($scope.studlist, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.report_data_new;
                }
                $scope.makeTodos();
            }



            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $scope.colsvis = false;


            $scope.exportData = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('pdf_print').innerHTML],
                         {
                             type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                         });
                        $scope.detailsField = false;
                        saveAs(blob, "SubstituteLectureDetailsReport.xls");
                        $scope.colsvis = false;
                        $scope.getdetails();


                    }



                });
                $scope.colsvis = true;


            };


            $scope.print = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var docHead = document.head.outerHTML;
                        var printContents = document.getElementById('pdf_print').outerHTML;
                        var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                        var newWin = window.open("", "_blank", winAttr);
                        var writeDoc = newWin.document;
                        writeDoc.open();
                        writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                        writeDoc.close();
                        newWin.focus();
                        $scope.detailsField = false;
                        $scope.colsvis = false;
                        $scope.getdetails();


                    }

                });

            };

            $scope.reset_data = function () {
                debugger;
                // $scope.temp.sims_grade_code = '';
                $scope.filteredTodos = [];

                try {
                    $('#cmb_grade_code').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_section_code').multipleSelect('uncheckAll');
                } catch (e) {

                }
                $scope.temp.sims_bell_code = '';
                $scope.report_data_new = [];


            }

            $scope.ischeck = function () {
                debugger
                $scope.checklick = function () {
                    var v = document.getElementById('show_unassign');
                    if (v.checked == true) {
                        v.checked == false;
                    }

                }
            }


        }])

})();

