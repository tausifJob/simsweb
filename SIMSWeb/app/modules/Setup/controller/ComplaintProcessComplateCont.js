﻿(function () {
    'use strict';
    var del = [];
    var main, temp, opr, date1, date3;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ComplaintProcessComplateCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.acadyr_data = [];
            $scope.edit_code = false;
            $scope.dis_no = true;
            $scope.grid = true;
            $scope.display = false;
            var data1 = [];
            var deletecode = [];
            $scope.btn_update = false;
            if ($http.defaults.headers.common['schoolId'] == 'asd') {
                $scope.showASDFlag = true;
            }

            var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.dt = {
                sims_complaint_registration_date: dateyear,
            }

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;


            $http.get(ENV.apiUrl + "api/common/AcademicYear/getAcademicStatus").then(function (res) {
                $scope.obj3 = res.data;
                console.log($scope.obj3);
            });

            $http.get(ENV.apiUrl + "api/StudentReport/getcomplaintdetailsassign?username=" + '').then(function (res) {
                $scope.grid = true;
                $scope.display = false;
                $scope.acadyr_data = res.data;
                $scope.totalItems = $scope.acadyr_data.length;
                $scope.todos = $scope.acadyr_data;
                $scope.makeTodos();
            });


            $http.get(ENV.apiUrl + "api/StudentReport/getstatus").then(function (status) {
                $scope.status_code = status.data;
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.SearchUser = function () {
                $scope.chku = true;
                $rootScope.chkMulti = false;
                $rootScope.visible_User = true;
                $scope.visible_Employee = false;
                $rootScope.visible_stud = false;
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }

            $scope.$on('global_cancel', function () {
                debugger;
                $scope.edte =
                   {
                       sims_user_code_created_code: $scope.SelectedUserLst[0].user_name,
                       sims_user_code_created_name: $scope.SelectedUserLst[0].name,
                   }

            });

            $scope.edit = function (str) {
                debugger;
                $scope.display = true;
                $scope.grid = false;

                $scope.update1 = true;
                $scope.delete1 = false;

                $scope.temp = {
                    sims_complaint_srl_no: str.sims_complaint_srl_no
                }

                if (str.sims_complaint_assigned_to_user_code != '') {
                    $scope.btn_update = true;
                    $scope.save1 = false;
                    $scope.assign_to = true;
                    $scope.assign_date = true;
                    $scope.btn_search = true;
                    $scope.neweditdate = true;
                    $scope.edt = {
                        sims_action_code: str.sims_complaint_status,
                        sims_complaint_subject: str.sims_complaint_subject
                    }
                    $scope.edte = {
                        sims_user_code_created_name: str.sims_complaint_assigned_to_user_code

                    }
                    $scope.dt = {
                        sims_complaint_registration_date: str.sims_complaint_registration_date
                    }
                }
                else {
                    $scope.neweditdate = false;
                    $scope.save1 = true;
                    $scope.btn_update = false;
                    $scope.assign_to = false;
                    $scope.assign_date = false;
                    $scope.btn_search = false;

                }
            }

            $scope.Update = function () {

                var data = ({
                    sims_complaint_subject: $scope.edt.sims_complaint_subject,
                    sims_action_code: $scope.edt.sims_action_code,
                    sims_complaint_srl_no: $scope.temp.sims_complaint_srl_no,
                    sims_complaint_registration_date: $scope.dt.sims_complaint_update_date,
                    sims_complaint_register_user_code: $rootScope.globals.currentUser.username,
                    sims_complaint_assigned_to_user_code: $scope.edte.sims_user_code_created_code,
                    opr: 'U'
                });

                data1.push(data);

                $http.post(ENV.apiUrl + "api/StudentReport/CUDcomplainttassign?complaintcode=" + '', data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 != null || $scope.msg1 != undefined) {
                        var m = $scope.msg1.strMessage;
                        swal({ title: "Alert", text: m, width: 450, height: 200 });
                        $scope.cancel();
                    }
                });

            }

            $scope.Save = function () {
                var data1 = [];

                var data = ({
                    sims_complaint_subject: $scope.edt.sims_complaint_subject,
                    sims_action_code: $scope.edt.sims_action_code,
                    sims_complaint_srl_no: $scope.temp.sims_complaint_srl_no,
                    sims_complaint_registration_date: $scope.dt.sims_complaint_registration_date,
                    sims_complaint_register_user_code: $rootScope.globals.currentUser.username,
                    sims_complaint_assigned_to_user_code: $scope.edte.sims_user_code_created_code,
                    opr: 'E'
                });

                data1.push(data);

                $http.post(ENV.apiUrl + "api/StudentReport/CUDcomplainttassign?complaintcode=" + '', data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 != null || $scope.msg1 != undefined) {
                        var m = $scope.msg1.strMessage;
                        swal({ title: "Alert", text: m, width: 450, height: 200 });
                        $scope.cancel();
                    }
                });

            }

            $scope.getgrid = function () {
                $scope.currentPage = 1;
                $http.get(ENV.apiUrl + "api/common/AcademicYear/getAcademicYear").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.acadyr_data = res.data;
                    $scope.totalItems = $scope.acadyr_data.length;
                    $scope.todos = $scope.acadyr_data;
                    $scope.makeTodos();
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }



            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;

                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.btn_update = false;
                $scope.edt = "";
                $scope.edit_code = false;
                $http.get(ENV.apiUrl + "api/StudentReport/getcomplaintcode").then(function (res) {
                    $scope.com_no = res.data;
                    $scope.temp = { sims_complaint_srl_no: $scope.com_no[0].sims_complaint_srl_no };
                });
            }

            $scope.cancel = function () {

                $scope.grid = true;
                $scope.display = false;
                $scope.edt = [];
                $scope.edte = [];
                $scope.dt = [];
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

            }




            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.acadyr_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.acadyr_data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                return (item.sims_complaint_srl_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sagar == toSearch) ? true : false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
        }])
})();