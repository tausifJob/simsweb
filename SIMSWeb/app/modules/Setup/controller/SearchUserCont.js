﻿(function () {
    'use strict';
    var opr = '';
    var searchCode = [];
    var main;
    var data1 = [];
    var mydata = [];
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SearchUserCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.pagesize2 = '5';
            $scope.table1 = true;
            $scope.operation = false;
            $scope.editmode = false;


            $http.get(ENV.apiUrl + "api/SearchUserDetails/getSearchUser").then(function (getSearchUser_data) {
                $scope.SearchUser_data = getSearchUser_data.data;
                $scope.totalItems = $scope.SearchUser_data.length;
                $scope.todos = $scope.SearchUser_data;
                $scope.makeTodos();
                console.log($scope.SearchUser_data);
            });

            $scope.New = function ()
            {
                $state.go('main.Sim507');
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                //  main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.up = function (str) {
                opr = 'U';
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.readonly = true;
                $scope.edt = "";
                $scope.edt = {
                    sims_building_code: str.sims_building_code
                           , sims_building_desc: str.sims_building_desc
                           , sims_buiding_student_capacity: str.sims_buiding_student_capacity
                };
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.edt.sims_buiding_student_capacity = "";
                $scope.edt.sims_building_code = "";
                $scope.edt.sims_building_desc = "";
            }

            $scope.CheckAllChecked = function () {
                debugger
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_search_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_search_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.deleterecord = function () {
                var data1 = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_search_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletesearchcode = ({
                            'sims_search_code': $scope.filteredTodos[i].sims_search_code,
                            opr: 'D'
                        });
                        searchCode.push(deletesearchcode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/SearchUserDetails/CUDSearch_User", searchCode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/SearchUserDetails/getSearchUser").then(function (getSearchUser_data) {
                                                $scope.SearchUser_data = getSearchUser_data.data;
                                                $scope.totalItems = $scope.SearchUser_data.length;
                                                $scope.todos = $scope.SearchUser_data;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/SearchUserDetails/getSearchUser").then(function (getSearchUser_data) {
                                                $scope.SearchUser_data = getSearchUser_data.data;
                                                $scope.totalItems = $scope.SearchUser_data.length;
                                                $scope.todos = $scope.SearchUser_data;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_search_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                data1 = [];
                $scope.currentPage = true;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.SearchUser_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.SearchUser_data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                return (
                     item.users.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_search_code == toSearch) ? true : false;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixModaltable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size2 = function (str) {
                console.log(str);
                $scope.pagesize2 = str;
                $scope.currentPage2 = 1;
                $scope.numPerPage2 = str; console.log("numPerPage2=" + $scope.numPerPage2);
                $scope.makeTodos2();
            }

            $scope.index2 = function (str) {
                $scope.pageindex2 = str;
                $scope.currentPage2 = str; console.log("currentPage2=" + $scope.currentPage2);
                $scope.makeTodos2();
                // main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos2 = [], $scope.currentPage2 = 1, $scope.numPerPage2 = 5, $scope.maxSize1 = 5;

            $scope.makeTodos2 = function () {
                var rem2 = parseInt($scope.totalItems2 % $scope.numPerPage2);
                if (rem2 == '0') {
                    $scope.pagersize2 = parseInt($scope.totalItems2 / $scope.numPerPage2);
                }
                else {
                    $scope.pagersize2 = parseInt($scope.totalItems2 / $scope.numPerPage2) + 1;
                }
                var begin2 = (($scope.currentPage2 - 1) * $scope.numPerPage2);
                var end2 = parseInt(begin2) + parseInt($scope.numPerPage2);

                console.log("begin2=" + begin2); console.log("end2=" + end2);

                $scope.filteredTodos2 = $scope.todos2.slice(begin2, end2);
                // mydata = $scope.filteredTodos2;
            };

            $scope.View = function (obj) {

                $('#MyModal').modal('show');
                $scope.Modaltable = true;

                $scope.sims_search_code = obj.sims_search_code;

                $http.get(ENV.apiUrl + "api/SearchUserDetails/getSearchUserOut?search_code=" + $scope.sims_search_code).then(function (getSearchUserOutout_data) {
                    $scope.SearchUserOutout_data = getSearchUserOutout_data.data;
                    $scope.totalItems2 = $scope.SearchUserOutout_data.length;
                    $scope.todos2 = $scope.SearchUserOutout_data;
                    $scope.makeTodos2();

                    console.log($scope.SearchUserOutout_data);
                    //  mydata = $scope.SearchUserOutout_data;
                    //var key = [];
                    //for (key in $scope.SearchUserOutout_data) {
                    //    if (key === "$$hashKey") break; //angular adds new keys to the object
                    //    if ($scope.SearchUserOutout_data.hasOwnProperty(key));
                    //}


                    //var key;
                    //var keys = [];
                    //for (key in obj) {
                    //        if (key === "$$hashKey") break; //angular adds new keys to the object
                    //        if (obj.hasOwnProperty(key)) keys.push(key);
                    //    }
                    //    return keys;

                    //$scope.totalItems2 = $scope.SearchUserOutout_data.length;
                    //$scope.todos2 = $scope.SearchUserOutout_data;
                    //$scope.makeTodos2();
                });



            }

        }])
})();





