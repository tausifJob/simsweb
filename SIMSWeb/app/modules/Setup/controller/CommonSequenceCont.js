﻿(function () {
    'use strict';
    var del = [];
    var oldvalue = [], appl_form_field, appl_parameter, appl_form_field_value1;
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CommonSequenceCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.grid = true;
            $scope.edit_data = false;
            $scope.sequencedata = [];
            var data1 = [];
            var deletecode = [];
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {
                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $http.get(ENV.apiUrl + "api/common/common_seq/getcomn004_show").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.sequencedata = res.data;
                $scope.totalItems = $scope.sequencedata.length;
                $scope.todos = $scope.sequencedata;
                $scope.makeTodos();

                for (var i = 0; i < $scope.totalItems; i++) {
                    $scope.sequencedata[i].icon = "fa fa-plus-circle";
                }
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $http.get(ENV.apiUrl + "api/common/ApplicationMaster/getModules").then(function (res) {
                $scope.module_data = res.data;
                console.log($scope.module_data);
            });
            //$scope.edt = {};
            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.cur_data = res.data;
                if (res.data.length > 0) {
                    $scope.edt.comm_cur_code = res.data[0].sims_attendance_cur_code;
                    $scope.GetAacd_yr($scope.edt.comm_cur_code);
                }
                console.log($scope.cur_data);
            });

            //$scope.GetAacd_yr = function (cur) {
            //    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur).then(function (res) {
            //        $scope.acad = res.data;
            //        console.log($scope.acad);
            //    });
            //}
            $http.get(ENV.apiUrl + "api/common/ParameterMaster/getApplication").then(function (res) {
                $scope.appl_name = res.data;
                console.log("appl_name");
                console.log($scope.appl_name);
            });
            $scope.GetAacd_yr = function (cur)
            {
                $http.get(ENV.apiUrl + "api/common/common_seq/getAllAcademicYear?cur=" + cur).then(function (res) {
                    $scope.acad = res.data;
                    if (res.data.length > 0) {
                        $scope.edt.comn_academic_year = res.data[0].sims_academic_year;
                    }
                    console.log($scope.acad);
                });
            }
            $scope.GetSelectedApplication = function (str) {
                debugger;
                if (str == undefined)
                { 
                    $scope.getgrid();
                }
                else { 
                    $http.get(ENV.apiUrl + "api/common/common_seq/getcomn004_show?comn_appl_code=" + str).then(function (res) {
                        $scope.display = false;
                        $scope.sequencedata = res.data;
                        $scope.totalItems = $scope.sequencedata.length;
                        $scope.todos = $scope.sequencedata;
                        $scope.makeTodos();
                        $scope.grid = true;


                        for (var i = 0; i < $scope.totalItems; i++) {
                            $scope.sequencedata[i].icon = "fa fa-plus-circle";
                        }
                    });
                }
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.GetApplication = function (modulecode) {
                $scope.appl_data = undefined;

                if (modulecode != undefined) {
                    $http.get(ENV.apiUrl + "api/common/ParameterMaster/getApplication?modCode=" + modulecode).then(function (res) {
                        $scope.appl_data = res.data;
                        console.log($scope.appl_data);
                    });
                }
            }


            $scope.edit = function (str)
            {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = false;
                    $scope.update1 = true;
                    $scope.delete1 = false;
                    $scope.edit_data = true;
                    $scope.appl_data = undefined;

                    $scope.GetAacd_yr(str.comm_cur_code);

                    $http.get(ENV.apiUrl + "api/common/ParameterMaster/getApplication?modCode=" + str.comn_mod_code).then(function (res) {
                        $scope.appl_data = res.data;
                        //$scope.appl_name = res.data;                    
                        //console.log($scope.appl_name);

                        //  $scope.GetApplication(str.comn_mod_code);

                        // setTimeout(function () {
                        $scope.edt =
                           {
                               comn_mod_code: str.comn_mod_code,
                               comn_application_code: str.comn_application_code,
                               comn_squence_code: str.comn_squence_code,
                               comn_description: str.comn_description,
                               comm_cur_code: str.comm_cur_code,
                               comn_academic_year: str.comn_academic_year,
                               comn_character_count: str.comn_character_count,
                               comn_character_squence: str.comn_character_squence,
                               comm_number_count: str.comm_number_count,
                               comn_number_squence: str.comn_number_squence,
                               comn_parameter_ref: str.comn_parameter_ref,
                           }
                    });
                    //  }, 2000);
                }
            }


            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                $scope.edit_data = false;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = {};
                $scope.edt.comn_parameter_ref = true;
                if ($scope.cur_data.length > 0) {
                    $scope.edt.comm_cur_code = $scope.cur_data[0].sims_attendance_cur_code;
                    $scope.GetAacd_yr($scope.edt.comm_cur_code);
                }
                if ($scope.acad.length > 0) {
                    $scope.edt.comn_academic_year = $scope.acad[0].sims_academic_year;
                }
            }
        }

            $scope.Save = function (isvalidate) {
                var data1 = [];

                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({
                            comn_application_code: $scope.edt.comn_application_code,
                            comn_mod_code: $scope.edt.comn_mod_code,
                            comn_squence_code: $scope.edt.comn_squence_code,
                            comn_description: $scope.edt.comn_description,
                            comm_cur_code: $scope.edt.comm_cur_code,
                            comn_academic_year: $scope.edt.comn_academic_year,
                            comn_character_count: $scope.edt.comn_character_count,
                            comn_character_squence: $scope.edt.comn_character_squence,
                            comm_number_count: $scope.edt.comm_number_count,
                            comn_number_squence: $scope.edt.comn_number_squence,
                            comn_parameter_ref: $scope.edt.comn_parameter_ref,
                            opr: 'I'
                        });

                        data1.push(data);

                        $http.post(ENV.apiUrl + "api/common/common_seq/comn_comn004insert", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Common Sequence Added Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.edt = '';
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Common Sequence Already Exists.", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.Update = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    var data = ({
                        comn_application_code: $scope.edt.comn_application_code,
                        comn_mod_code: $scope.edt.comn_mod_code,
                        comn_squence_code: $scope.edt.comn_squence_code,
                        comn_description: $scope.edt.comn_description,
                        comm_cur_code: $scope.edt.comm_cur_code,
                        comn_academic_year: $scope.edt.comn_academic_year,
                        comn_character_count: $scope.edt.comn_character_count,
                        comn_character_squence: $scope.edt.comn_character_squence,
                        comm_number_count: $scope.edt.comm_number_count,
                        comn_number_squence: $scope.edt.comn_number_squence,
                        comn_parameter_ref: $scope.edt.comn_parameter_ref,
                        opr: 'U'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/common_seq/comn_comn004Update", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Common Sequence Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Common Sequence Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal1("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/common/common_seq/getcomn004_show").then(function (res) {
                    $scope.display = false;
                    $scope.sequencedata = res.data;
                    $scope.totalItems = $scope.sequencedata.length;
                    $scope.todos = $scope.sequencedata;
                    $scope.makeTodos();
                    $scope.grid = true;


                    for (var i = 0; i < $scope.totalItems; i++) {
                        $scope.sequencedata[i].icon = "fa fa-plus-circle";
                    }
                });
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
               // $scope.appl_data = null;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.sequencedata;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            //$scope.searched = function (valLists, toSearch) {

            //    return _.filter(valLists,

            //    function (i) {
            //        /* Search Text in all  fields */
            //        return searchUtil(i, toSearch);
            //    });
            //};

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
            };
            

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.sequencedata, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.sequencedata;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sequencedata;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }

                }


            }

          

          

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.comn_squence_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.comn_description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  || item.comm_cur_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.comn_academic_year_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.comn_application_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            var dom;
            $scope.flag = true;
            $scope.icon = "fa fa-plus-circle";

            $scope.expand = function (info, $event) {
                console.log(info);
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                        "<tr> <td class='semi-bold'>" + gettextCatalog.getString('Character Count') + "</td> <td class='semi-bold'>" + gettextCatalog.getString('Character Sequence') + " </td><td class='semi-bold'>" + gettextCatalog.getString('Number Count') + "</td><td class='semi-bold'>" + gettextCatalog.getString('Number Sequence') + "</td>" +
                            "</tr>" +
                              "<tr><td>" + (info.comn_character_count) + "</td> <td>" + (info.comn_character_squence) + " </td><td>" + (info.comm_number_count) + "</td><td>" + (info.comn_number_squence) + "</td>" +
                            "</tr>" +

                        "</tbody>" +
                        " </table></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    info.icon = "fa fa-plus-circle";
                    $scope.flag = true;
                }
            };



        }])
})();