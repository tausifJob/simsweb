﻿(function () {
    'use strict';
    var opr = '';
    var loccode = [];
    var main;

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ApplicationModuleCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

                            $http.get(ENV.apiUrl + "api/common/getModulesOrder").then(function (res) {
                                console.log(res.data);
                                $scope.containers = res.data;

                                ///api/common/getAllModules?username=admin
                            });
                            $http.get(ENV.apiUrl + "api/common/getAppType").then(function (res) {
                                console.log(res.data);
                                $scope.apptype = res.data;
                                ///api/common/getAllModules?username=admin
                            });

                            ///////////    drageble ///////////////

                            $scope.dragoverCallback = function (event, index, external, type) {
                                //debugger;
                                $scope.dragIndex = index;
                                $scope.logListEvent('dragged over' + index, event, index, external, type);
                                // Disallow dropping in the third row. Could also be done with dnd-disable-if.
                                return index < 20;
                                console.log($scope.dragIndex);

                            };

                            $scope.dropCallback = function (event, index, item, external, type, allowedType) {
                                //debugger;
                                $scope.dropIndex = index;
                                $scope.logListEvent('dropped at' + index, event, index, external, type);
                                if (external) {
                                    if (allowedType === 'itemType' && !item.comn_appl_name_en) return false;
                                    if (allowedType === 'containerType' && !angular.isArray(item)) return false;
                                }
                                console.log(item);
                                if (allowedType === 'containerType') {
                                    $http.post(ENV.apiUrl + "api/common/updateModule?modCode=" + item.module_code + "&modDropCode=" + index).then(function (res) {

                                    });
                                }
                                if (allowedType === 'itemType') {
                                    $http.post(ENV.apiUrl + "api/common/updateApplication?modCode=" + item.comn_mod_code + "&appDropCode=" + index + "&appCode=" + item.comn_appl_code).then(function (res) {
                                        console.log(res.data);
                                    });
                                }
                                return item;
                            };


                            $scope.url = ENV.siteUrl;
                            console.log($scope.url);
                            $scope.daycodeClick = function (count, appcode, index, mod) {
                                console.log(mod);
                                console.log(appcode);
                                for (var j = 0; j < $scope.containers.length; j++) {
                                    var d = $scope.containers[j].comn_appl[j].comn_appl_code;
                                    if (mod.comn_appl_code == $scope.containers[j].comn_appl[j].comn_appl_code) {
                                        $scope.mnuCard = '#' + appcode + index;
                                        $($scope.mnuCard).addClass('dayySelected');
                                    }
                                }
                                for (var i = 0; i < count.length; i++) {
                                    if (count[i].sims_appl_code != appcode) {
                                        $scope.mnuCard1 = '#' + count[i].sims_appl_code + i;

                                        $($scope.mnuCard1).removeClass('dayySelected');
                                    }

                                }
                            }


                            $scope.radioClick = function (appcode, apptype) {
                                console.log(appcode);
                                console.log(apptype);
                                $http.post(ENV.apiUrl + "api/common/updateApplType?applCode=" + apptype.comn_appl_code + "&applType=" + appcode).then(function (res) {
                                    console.log(res.data);
                                });
                            }

                            $scope.logEvent = function (message, event) {
                                // console.log(message, '(triggered by the following', event.type, 'event)');
                                // console.log(event);
                            };

                            $scope.logListEvent = function (action, event, index, external, type) {
                                console.log(action);
                                var message = external ? 'External ' : '';
                                message += type + ' element is ' + action + ' position ' + index;
                                $scope.logEvent(message, event);
                            };
                            $scope.label = 'Show';
                            $scope.moduleClick = function (item, module) {
                                console.log(module)
                                //if ($scope.flag == true) {

                                if (module.module_code_show == true) {
                                    module['label'] = 'Show';

                                    module['module_code_show'] = false;
                                    $scope.label = 'Show';
                                    $scope.application = '#' + module.module_code;
                                    $($scope.application).css({ 'display': 'none' });
                                }
                                else {
                                    // for (var i = 0; i < $scope.containers.length; i++) {
                                    //  if (item == $scope.containers[i].module_code) {
                                    module['module_code_show'] = true;
                                    // $scope.label = 'Hide';
                                    module['label'] = 'Hide';

                                    $scope.application = '#' + module.module_code;
                                    $($scope.application).css({ 'display': 'block' });
                                    //}

                                    // }
                                }
                            }


        }])
})();
