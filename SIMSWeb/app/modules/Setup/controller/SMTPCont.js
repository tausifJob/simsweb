﻿(function () {
    'use strict';
    var datasend = [];
    var data1 = [];
    var opr = '';
    var deletefin = [];
    var main, temp;
   
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SMTPCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.display = false;
            $scope.grid = true;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //Select Data
            $http.get(ENV.apiUrl + "api/SMTProtocol/getSMTP").then(function (smtp_data) {
            $scope.smtpdata = smtp_data.data;
            $scope.totalItems = $scope.smtpdata.length;
            $scope.todos = $scope.smtpdata;
            $scope.makeTodos();
            console.log($scope.smtpdata);
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str;
            //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            $scope.size = function (str) {
                1
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.smtpdata;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.check_all();
            }


            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                console.log("begin=" + begin); console.log("end=" + end);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };



            //School
            $http.get(ENV.apiUrl + "api/SMTProtocol/getSchools").then(function (res) {
                debugger
                $scope.schools = res.data;
                console.log($scope.schools);
            });

            //Modules
            $http.get(ENV.apiUrl + "api/SMTProtocol/getModule").then(function (res) {
                debugger
                $scope.modulecode = res.data;
                console.log($scope.schools);
            });



            $scope.New = function () {

                $scope.display = true;
                $scope.grid = false;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.temp = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.temp = {};
                $scope.temp.sims_smtp_status = true;
            }
            $scope.cancel = function () {

                $scope.display = false;
                $scope.grid = true;
            }

            //DATA SAVE INSERT
            
            $scope.Save = function (myForm) {
                debugger
                if (myForm) {
                    data1 = [];
                    var data = $scope.temp;
                    data.opr = 'I';
                    $scope.temp = "";
                    $scope.exist = false;
                    data1.push(data);
                    for (var i = 0; i < $scope.smtpdata.length; i++) {
                        if ($scope.smtpdata[i].sims_smtp_code == data.sims_smtp_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ text: "Record Already exists", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                    else {
                        $http.post(ENV.apiUrl + "api/SMTProtocol/CUDSMTPDemo?simsobj=", data1).then(function (msg) {
                            $scope.BoardData1 = false;
                            $http.get(ENV.apiUrl + "api/SMTProtocol/getSMTP").then(function (smtp_data) {
                                $scope.smtpdata = smtp_data.data;
                                $scope.totalItems = $scope.smtpdata.length;
                                $scope.todos = $scope.smtpdata;
                                $scope.makeTodos();
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        });
                        $scope.grid = true;
                        $scope.display = false;
                    }
                    data1 = [];
                }
            }

            $scope.up = function (str) {
                debugger
               opr = 'U';
               
                $scope.savebtn = false;
                $scope.updatebtn = true;
              
                $scope.grid = false;
                $scope.display = true;
                $scope.temp = {
                    
                    sims_school_code: str.sims_school_code
                ,sims_school_name:str.sims_school_name
                , sims_smtp_code: str.sims_smtp_code
                , sims_mod_code: str.sims_mod_code
                , sims_smtp_serverip: str.sims_smtp_serverip
                , sims_smtp_port: str.sims_smtp_port
                ,sims_smtp_username:str.sims_smtp_username
                ,sims_smtp_password:str.sims_smtp_password
                ,sims_smtp_from_email:str.sims_smtp_from_email
                ,sims_smtp_status:str.sims_smtp_status
                ,sims_smtp_auth_required:str.sims_smtp_auth_required
                ,sims_smtp_ssl_required:str.sims_smtp_ssl_required
                ,sims_smtp_max_mail_hour:str.sims_smtp_max_mail_hour
                ,sims_smtp_max_mail_day:str.sims_smtp_max_mail_day
                ,sims_smtp_encrypt_mode:str.sims_smtp_encrypt_mode
                ,sims_smtp_from_email_name:str.sims_smtp_from_email_name
                ,sims_smtp_config_id:str.sims_smtp_config_id
                ,sims_smtp_signature:str.sims_smtp_signature
                ,sims_smtp_remark:str.sims_smtp_remark
                };

            }

            $scope.Update = function (myForm) {
                debugger;
                if (myForm) {
                    data1 = [];
                    var data = $scope.temp;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/SMTProtocol/CUDSMTPDemo", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $scope.BoardData1 = false;
                        $http.get(ENV.apiUrl + "api/SMTProtocol/getSMTP").then(function (smtp_data) {
                            $scope.smtpdata = smtp_data.data;
                            $scope.totalItems = $scope.smtpdata.length;
                            $scope.todos = $scope.smtpdata;
                            $scope.makeTodos();
                        });

                    })
                    $scope.display = false;
                    $scope.grid = true;
                }
            }
            
            $scope.check_all = function () {
                debugger
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.check_once = function () {
                debugger
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }
                      
            $scope.deleterecord = function () {
                debugger;
                deletefin = [];
           
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_smtp_code': $scope.filteredTodos[i].sims_smtp_code,
                            'sims_school_code': $scope.filteredTodos[i].sims_school_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/SMTProtocol/CUDSMTPDemo", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {

                                            $http.get(ENV.apiUrl + "api/SMTProtocol/getSMTP").then(function (res1) {
                                                $scope.CreDiv = res1.data;
                                                $scope.totalItems = $scope.CreDiv.length;
                                                $scope.todos = $scope.CreDiv;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }

                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/SMTProtocol/getSMTP").then(function (res1) {
                                                $scope.CreDiv = res1.data;
                                                $scope.totalItems = $scope.CreDiv.length;
                                                $scope.todos = $scope.CreDiv;
                                                $scope.makeTodos();


                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            //main.checked = false;
                                            // $('tr').removeClass("row_selected");
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.row1 = '';
                $scope.currentPage = str;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.smtpdata, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.smtpdata;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_smtp_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_smtp_username.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  || item.sims_mod_code == toSearch) ? true : false;
            }





            }]);
})();