﻿(function () {
    'use strict';
    var obj1, obj2, temp, main;
    var opr = '';
    var portalcode = [];

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('PortalReferenceCont',
         ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
             $scope.pagesize = '10';
             $scope.pageindex = "0";
             $scope.pager = true;
             $scope.table1 = true;
             $scope.lbl_filename = false;
             $scope.busy = false;
             $scope.editmode = false;
             var formdata = new FormData();
             var dataSave = [];
             var dataUpdate = [];
             $scope.ischanged = false;
             $scope.eiamchkbox = false;
             $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

             $scope.url = ENV.apiUrl + 'Content' + '/' + $http.defaults.headers.common['schoolId'];
             debugger
             if ($http.defaults.headers.common['schoolId'] == 'eiama' || $http.defaults.headers.common['schoolId'] == 'EIAMA') {

                 $scope.eiamchkbox = true;
             }
             $scope.forName ='';
             $scope.fortype = '';
             $scope.file_name_name = '';


             $scope.size = function (str) {
                 //console.log(str);
                 //$scope.pagesize = str;
                 //$scope.currentPage = 1;
                 //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                 debugger;
                 if (str == "All") {
                     $scope.currentPage = '1';
                     $scope.filteredTodos = $scope.PortalData;
                     $scope.pager = false;
                 }
                 else {
                     $scope.pager = true;
                     $scope.pagesize = str;
                     $scope.currentPage = 1;
                     $scope.numPerPage = str;
                     $scope.makeTodos();
                 }
             }

             $scope.index = function (str) {
                 $scope.pageindex = str;
                 $scope.currentPage = str;
                 console.log("currentPage=" + $scope.currentPage);
                 $scope.makeTodos();
                 main.checked = false;
                 $scope.row1 = '';
             }




           
             

             $scope.New = function () {
                 $scope.lbl_filename = false;
                 $scope.myForm.$setPristine();
                 $scope.myForm.$setUntouched();
                 $scope.newmode = true;
                 $scope.edt = '';
                 $scope.editmode = false;
                 opr = 'S';
                 $scope.readonly = false;
                 $scope.table1 = false;
                 $scope.operation = true;
                 $scope.savebtn = true;
                 $scope.updatebtn = false;
                 $scope.ischanged = false;
                 $scope.filedemo = true;
                 $scope.temp = {};
                 $scope.photo_filename = '';
                 $scope.forName = '';
                 $scope.fortype = '';
                 $scope.file_name_name = '';
                 
                 $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                     $scope.curriculum = res.data;
                     $scope.temp = {
                         sims_cur_code: $scope.curriculum[0].sims_cur_code,
                         sims_reference_status: true
                     }
                 })

                 debugger
                 var today = new Date();
                 var dd = today.getDate();
                 if (dd < 10) {
                     dd = '0' + dd;
                 }
                 var mm = today.getMonth() + 1; //January is 0!
                 if (mm < 10) {
                     mm = '0' + mm;
                 }
                 var yyyy = today.getFullYear();

                 // edt.sims_reference_publish_date, edt.sims_reference_expiry_date
                 $scope.edt = {
                     sims_reference_publish_date: dd + '-' + mm + '-' + yyyy,
                     sims_reference_expiry_date: dd + '-' + mm + '-' + yyyy
                 }

                 //$scope.edt['sims_reference_status'] = true;
             }

             $scope.up = function (str) {
                 debugger
                 opr = 'U';
                 $scope.editmode = true;
                 $scope.newmode = false;
                 $scope.savebtn = false;
                 $scope.updatebtn = true;
                 $scope.readonly = true;
                 $scope.table1 = false;
                 $scope.operation = true;
                 $scope.lbl_filename = true;
                 $scope.ischanged = false;
                 //$scope.edt = str;
                 $scope.edt = {
                     sims_reference_number: str.sims_reference_number,
                     sims_cur_code: str.sims_cur_code,
                     sims_reference_name: str.sims_reference_name,
                     sims_reference_filename: str.sims_reference_filename,
                     sims_reference_desc: str.sims_reference_desc,
                     sims_reference_publish_date: str.sims_reference_publish_date,
                     sims_reference_expiry_date: str.sims_reference_expiry_date,
                     sims_reference_status: str.sims_reference_status,
                     sims_learn_arabic_status: str.sims_learn_arabic_status,
                     sims_cur_name: str.sims_cur_name,
                     sims_reference_video_resource: str.sims_reference_video_resource
                 }

             }

             $http.get(ENV.apiUrl + "api/portalrefer/getPortalDetail").then(function (Portal_Data) {
                 $scope.PortalData = Portal_Data.data;
                 $scope.totalItems = $scope.PortalData.length;
                 $scope.todos = $scope.PortalData;
                 $scope.makeTodos();
             });

             $scope.cancel = function () {
                 $scope.table1 = true;
                 $scope.operation = false;
                 $scope.edt = "";
                 $scope.photo_filename = '';
                 $scope.forName = '';
                 $scope.fortype = '';
                 $scope.file_name_name = '';
                 $scope.myForm.$setPristine();
             }

             $scope.Save = function (myForm) {
                 debugger
                 if ($http.defaults.headers.common['schoolId'] == 'eiama' || $http.defaults.headers.common['schoolId'] == 'EIAMA')
                 {
                     $('#loader0712').modal({ backdrop: 'static', keyboard: false });
                     if (myForm) {
                         
                             debugger
                             var flag = false;
                             var data = $scope.edt;
                             data.opr = 'I';

                             data.sims_cur_code = $scope.temp.sims_cur_code;

                             if ($scope.photo_filename === undefined || $scope.photo_filename === "" || $scope.photo_filename === null) {
                                 data.sims_reference_filename = '';
                                 data.sims_timetable_filename = '';
                             }
                             else {
                                 data.sims_reference_filename = $scope.forName + '.' + $scope.fortype;
                                 data.sims_timetable_filename = $scope.forName + '.' + $scope.fortype;
                             }
                             //data.sims_reference_filename = $scope.edt.sims_reference_name;
                             //data.sims_reference_filename = d;
                             //data.sims_timetable_filename = d;
                             //if (data.sims_timetable_filename == "false") {
                             //    swal({ title: "Alert", text: "Please Select File....", width: 300, height: 200 });
                             //    $('#loader0712').modal('hide');
                             //}
                             //else 
                             //{
                             for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                 if ($scope.filteredTodos[i].sims_reference_number == $scope.edt.sims_reference_number) {
                                     flag = true;
                                 }
                             }
                             if (flag) {
                                 swal({ text: "Reference number Already exists", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                                 $state.go($state.current, {}, { reload: true });
                                 $('#loader0712').modal('hide');
                             }
                             else {
                                 dataSave.push(data);
                                 $http.post(ENV.apiUrl + "api/portalrefer/PortalCUD?simsobj=", dataSave).then(function (msg) {
                                     $scope.msg1 = msg.data;
                                     if ($scope.msg1 == true) {
                                         swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                         $('#loader0712').modal('hide');

                                         if ($scope.photo_filename === undefined || $scope.photo_filename === "" || $scope.photo_filename === null) {                                             
                                         }
                                         else {
                                             var request = {
                                                 method: 'POST',
                                                 url: ENV.apiUrl + 'api/file/uploadDocument_new?filename=' + $scope.forName + "&location=" + "Docs/portalReference/",
                                                 data: formdata,
                                                 headers: {
                                                     'Content-Type': undefined
                                                 }
                                             };
                                             $http(request).success(function (d) {
                                                 $scope.cancel();
                                             });
                                         }

                                     }
                                     else if ($scope.msg1 == false) {
                                         swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                         $('#loader0712').modal('hide');
                                     }
                                     else {
                                         swal("Error-" + $scope.msg1)
                                     }
                                     $http.get(ENV.apiUrl + "api/portalrefer/getPortalDetail").then(function (Portal_Data) {
                                         $scope.PortalData = Portal_Data.data;
                                         $scope.totalItems = $scope.PortalData.length;
                                         $scope.todos = $scope.PortalData;
                                         $scope.makeTodos();
                                         $('#loader0712').modal('hide');
                                     });
                                 });
                             }
                             //}
                         
                         dataSave = [];                         
                         $scope.table1 = true;
                         $scope.operation = false;

                     }
                 }
                 else {

                     

                     if (myForm) {

                         $('#loader0712').modal({ backdrop: 'static', keyboard: false });

                      
                              var flag = false;

                             //if (data.sims_timetable_filename == "false") {
                             //    swal({ title: "Alert", text: "Please Select File....", width: 300, height: 200 });
                             //    $('#loader0712').modal('hide');
                             //}
                             //else 
                             //{
                             for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                 if ($scope.filteredTodos[i].sims_reference_number == $scope.edt.sims_reference_number) {
                                     flag = true;
                                 }
                             }
                             if (flag) {
                                 swal({ text: "Reference number Already exists", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                                 $state.go($state.current, {}, { reload: true });
                                 $('#loader0712').modal('hide');
                             }
                             else {

                                 var data = $scope.edt;
                                 data.sims_cur_code = $scope.temp.sims_cur_code;
                                 data.opr = 'I';

                                 if ($scope.photo_filename === undefined || $scope.photo_filename === "" || $scope.photo_filename === null) {
                                     data.sims_reference_filename = '';
                                     data.sims_timetable_filename = '';
                                 }
                                 else {
                                     data.sims_reference_filename = $scope.forName +'.'+ $scope.fortype;
                                     data.sims_timetable_filename = $scope.forName +'.'+ $scope.fortype;
                                 }

                                 //data.sims_reference_filename = $scope.edt.sims_reference_name;
                                 //data.sims_reference_filename = $scope.d   //d;
                                 //data.sims_timetable_filename = $scope.d   //d;

                                 dataSave.push(data);
                                 $http.post(ENV.apiUrl + "api/portalrefer/PortalCUD?simsobj=", dataSave).then(function (msg) {
                                     $scope.msg1 = msg.data;
                                     if ($scope.msg1 == true) {
                                         swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                         $('#loader0712').modal('hide');

                                         if ($scope.photo_filename === undefined || $scope.photo_filename === "" || $scope.photo_filename === null) {                                             
                                         
                                         }
                                         else{
                                             var request = {
                                                 method: 'POST',
                                                 url: ENV.apiUrl + 'api/file/uploadDocument_new?filename=' + $scope.forName + "&location=" + "Docs/portalReference/",
                                                 data: formdata,
                                                 headers: {
                                                     'Content-Type': undefined
                                                 }
                                             };
                                             $http(request).success(function (d) {
                                                 debugger
                                                 $scope.d = d;
                                                 $scope.cancel();
                                             });
                                         }

                                     }
                                     else if ($scope.msg1 == false) {
                                         swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                         $('#loader0712').modal('hide');
                                     }
                                     else {
                                         swal("Error-" + $scope.msg1)
                                     }

                                     $http.get(ENV.apiUrl + "api/portalrefer/getPortalDetail").then(function (Portal_Data) {
                                         $scope.PortalData = Portal_Data.data;
                                         $scope.totalItems = $scope.PortalData.length;
                                         $scope.todos = $scope.PortalData;
                                         $scope.makeTodos();
                                         $('#loader0712').modal('hide');
                                     });
                                 });
                             }
                         //}
                 
                         dataSave = [];                         
                         $scope.table1 = true;
                         $scope.operation = false;

                     }

                 }
                

             }

             $scope.Update = function () {
                 debugger
                 $scope.busy = true;
                 if (!$scope.ischanged) {
                     var data = $scope.edt;
                     data.opr = 'U';
                     dataUpdate.push(data);
                     $http.post(ENV.apiUrl + "api/portalrefer/PortalCUD?simsobj=", dataUpdate).then(function (msg) {
                         $scope.msg1 = msg.data;
                         $scope.operation = false;
                         $http.get(ENV.apiUrl + "api/portalrefer/getPortalDetail").then(function (Portal_Data) {
                             $scope.PortalData = Portal_Data.data;
                             $scope.totalItems = $scope.PortalData.length;
                             $scope.todos = $scope.PortalData;
                             formdata = new FormData();
                             $scope.makeTodos();

                             if ($scope.msg1 == true) {
                                 swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                 $('#loader0712').modal('hide');
                             }
                             else if ($scope.msg1 == false) {
                                 swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                 $('#loader0712').modal('hide');
                             }
                             else {
                                 swal("Error-" + $scope.msg1)
                             }
                         });

                     })
                 }
                 else {

                         var data = $scope.edt;
                         data.opr = 'U';
                         //data.sims_reference_filename = $scope.edt.sims_reference_name;
                         if ($scope.photo_filename === undefined || $scope.photo_filename === "" || $scope.photo_filename === null) {
                             data.sims_reference_filename = '';
                             data.sims_timetable_filename = '';
                         }
                         else {
                             data.sims_reference_filename = $scope.forName + '.' + $scope.fortype;
                             data.sims_timetable_filename = $scope.forName + '.' + $scope.fortype;
                         }

                         //data.sims_reference_filename = d;
                         //data.sims_timetable_filename = d;
                         //if (data.sims_timetable_filename == "false") {
                         //    swal({ title: "Alert", text: "Please Select File....", width: 300, height: 200 });
                         //    $('#loader0712').modal('hide');
                         //}
                         //else {
                             dataUpdate.push(data);
                             $http.post(ENV.apiUrl + "api/portalrefer/PortalCUD?simsobj=", dataUpdate).then(function (msg) {
                                 $scope.msg1 = msg.data;
                                 $scope.operation = false;
                                 $http.get(ENV.apiUrl + "api/portalrefer/getPortalDetail").then(function (Portal_Data) {
                                     $scope.PortalData = Portal_Data.data;
                                     $scope.totalItems = $scope.PortalData.length;
                                     $scope.todos = $scope.PortalData;
                                     formdata = new FormData();
                                     $scope.makeTodos();

                                     if ($scope.msg1 == true) {
                                         swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                         $('#loader0712').modal('hide');

                                         if($scope.photo_filename === undefined || $scope.photo_filename === "" || $scope.photo_filename === null) {                                             
                                         
                                         }
                                         else {
                                             var request = {
                                                 method: 'POST',
                                                 url: ENV.apiUrl + 'api/file/uploadDocument_new?filename=' + $scope.forName + "&location=" + "Docs/portalReference/",
                                                 data: formdata,
                                                 headers: {
                                                     'Content-Type': undefined
                                                 }
                                             };
                                             $http(request).success(function (d) {
                                                 $scope.cancel();
                                             });
                                         }
                                     }
                                     else if ($scope.msg1 == false) {
                                         swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                         $('#loader0712').modal('hide');
                                     }
                                     else {
                                         swal("Error-" + $scope.msg1)
                                     }
                                 });

                             });
                         //}
                    
                 }
                
                 $scope.operation = false;
                 $scope.table1 = true;
             }

             $scope.makeTodos = function () {
                 var rem = parseInt($scope.totalItems % $scope.numPerPage);
                 if (rem == '0') {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                 }
                 else {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                 }
                 var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                 var end = parseInt(begin) + parseInt($scope.numPerPage);

                 console.log("begin=" + begin); console.log("end=" + end);

                 $scope.filteredTodos = $scope.todos.slice(begin, end);
             };

             $scope.getTheFiles = function ($files) {
                 $scope.filesize = true;
                 $scope.lbl_filename = false;

                 angular.forEach($files, function (value, key) {
                     formdata.append(key, value);

                     var i = 0;
                     if ($files[i].size > 6097152) {
                         $scope.filesize = false;
                         $scope.edt.photoStatus = false;

                         swal({ title: "Alert", text: "File Should Not Exceed 6MB.",});
                         $scope.edt.sims_timetable_filename = '';
                         $scope.edt.sims_reference_filename = '';

                     }
                     else {

                     }

                 });
             };

             $scope.file_changed = function (element, str) {
                 $scope.lbl_filename = false;
                 var photofile = element.files[0];
                 $scope.ischanged = true;
                 $scope.photo_filename = (photofile.name);

                 var v = new Date();
                 $scope.file_name_name = photofile.name;
                 var len = 0;
                 len = $scope.file_name_name.split('.');
                 $scope.forName = $scope.file_name_name.split('.')[0] + '_' + v.getHours() + v.getSeconds();
                 $scope.fortype = $scope.file_name_name.split('.')[len.length - 1];


                 $scope.edt1 = { sims_timetable_filename: $scope.photo_filename }
                 $.extend($scope.edt, $scope.edt1)
                 $scope.photo_filename = (photofile.type);
                 var reader = new FileReader();
                 reader.onload = function (e) {
                     $scope.$apply(function () {
                         $scope.prev_img = e.target.result;

                     });
                 };
                 reader.readAsDataURL(photofile);

                 if (element.files[0].size < 2097152) {

                 }
             };

             $(function () {

                 $('table tbody tr:even').addClass('even_row');
                 $("#all_chk").change(function () {
                     $('input:checkbox').prop('checked', this.checked);
                     $('tr').toggleClass("selected_row", this.checked)
                 });

                 $('table tbody :checkbox').change(function (event) {
                     $(this).closest('tr').toggleClass("selected_row", this.checked);
                 });
                 $('table tbody tr').click(function (event) {
                     if (event.target.type !== 'checkbox') {
                         $(':checkbox', this).trigger('click');
                     }
                     $("input[type='checkbox']").not('#all_chk').change(function (e) {
                         if ($(this).is(":checked")) {
                             $(this).closest('tr').addClass("selected_row");
                         } else {
                             $(this).closest('tr').removeClass("selected_row");
                         }
                     });
                 });

             });

             $timeout(function () {
                 $("#fixTable").tableHeadFixer({ 'top': 1 });
             }, 100);

             $timeout(function () {
                 $("#fixTable1").tableHeadFixer({ 'top': 1 });

             }, 100);

             $scope.check_all = function () {
                 main = document.getElementById('all_chk');
                 if (main.checked == true) {
                     for (var i = 0; i < $scope.filteredTodos.length; i++) {
                         var v = document.getElementById(i + $scope.filteredTodos[i].sims_reference_number);
                         v.checked = true;
                         $scope.row1 = 'row_selected';
                         $('tr').addClass("row_selected");
                     }
                 }
                 else {
                     for (var i = 0; i < $scope.filteredTodos.length; i++) {
                         var v = document.getElementById(i + $scope.filteredTodos[i].sims_reference_number);
                         v.checked = false;
                         main.checked = false;
                         $scope.row1 = '';

                     }
                 }

             }

             $scope.check_once = function (info) {
                 $("input[type='checkbox']").change(function (e) {
                     if ($(this).is(":checked")) { //If the checkbox is checked
                         $(this).closest('tr').addClass("row_selected");
                         //Add class on checkbox checked
                         $scope.color = '#edefef';
                     }
                     else {
                         $(this).closest('tr').removeClass("row_selected");
                         //Remove class on checkbox uncheck
                         $scope.color = '#edefef';
                     }
                 });

                 main = document.getElementById('all_chk');
                 if (main.checked == true) {
                     main.checked = false;
                     $("input[type='checkbox']").change(function (e) {
                         if ($(this).is(":checked")) {
                             $(this).closest('tr').addClass("row_selected");
                         }
                         else {
                             $(this).closest('tr').removeClass("row_selected");
                         }
                     });
                 }

             }



             $scope.Chk_onces = function () {
                 debugger
                 main = document.getElementById('ChkNull');
                 if (main.checked == true) {
                     $scope.filedemo = false;
                 }
                 else {
                     $scope.filedemo = true;
                 }


             }



             $scope.deleterecord = function () {
                 debugger;
                 $scope.flag = false;
                 var deleteleave = [];
                 for (var i = 0; i < $scope.filteredTodos.length; i++) {
                     var v = document.getElementById(i + $scope.filteredTodos[i].sims_reference_number);
                     if (v.checked == true) {
                         $scope.flag = true;
                         var deletemodulecode = ({
                             'sims_reference_number': $scope.filteredTodos[i].sims_reference_number,
                             opr: 'D'
                         });
                         deleteleave.push(deletemodulecode);
                     }
                 }
                 if ($scope.flag) {
                     swal({
                         title: '',
                         text: "Are you sure you want to Delete?",
                         showCloseButton: true,
                         confirmButtonText: 'Yes',
                         showCancelButton: true,
                         width: 380,
                         cancelButtonText: 'No',

                     }).then(function (isConfirm) {
                         if (isConfirm) {
                             $http.post(ENV.apiUrl + "api/portalrefer/PortalCUD", deleteleave).then(function (msg) {
                                 $scope.msg1 = msg.data;
                                 if ($scope.msg1 == true) {
                                     swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                         if (isConfirm) {
                                             $http.get(ENV.apiUrl + "api/portalrefer/getPortalDetail").then(function (portal_Data) {
                                                 $scope.AttendanceCode_Data = portal_Data.data;
                                                 $scope.totalItems = $scope.AttendanceCode_Data.length;
                                                 $scope.todos = $scope.AttendanceCode_Data;
                                                 $scope.makeTodos();
                                             });
                                         }

                                         $scope.currentPage = true;
                                     });
                                 }
                                 else if ($scope.msg1 == false) {
                                     swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                         if (isConfirm) {
                                             $http.get(ENV.apiUrl + "api/portalrefer/getPortalDetail").then(function (portal_Data) {
                                                 $scope.AttendanceCode_Data = portal_Data.data;
                                                 $scope.totalItems = $scope.AttendanceCode_Data.length;
                                                 $scope.todos = $scope.AttendanceCode_Data;
                                                 $scope.makeTodos();
                                                 main.checked = false;
                                                 $('tr').removeClass("row_selected");
                                             });
                                         }
                                     });
                                 }
                                 else {
                                     swal("Error-" + $scope.msg1)
                                 }
                             });
                         }
                         else {
                             main = document.getElementById('all_chk');
                             if (main.checked == true) {
                                 main.checked = false;
                             }
                             for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                 var v = document.getElementById(i + $scope.filteredTodos[i].sims_reference_number);
                                 if (v.checked == true) {
                                     v.checked = false;
                                     main.checked = false;
                                     $('tr').removeClass("row_selected");
                                 }
                             }
                         }
                     });
                 }
                 else {
                     swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                 }

                 $scope.currentPage = str;
             }

             $scope.searched = function (valLists, toSearch) {
                 return _.filter(valLists, function (i) {
                     /* Search Text in all  fields */
                     return searchUtil(i, toSearch);
                 });
             };

             $scope.search = function () {
                 $scope.todos = $scope.searched($scope.PortalData, $scope.searchText);
                 $scope.totalItems = $scope.todos.length;
                 $scope.currentPage = '1';
                 if ($scope.searchText == '') {
                     $scope.todos = $scope.PortalData;
                 }
                 $scope.makeTodos();
             }

             function searchUtil(item, toSearch) {
                 /* Search Text in all 3 fields */
                 return (item.sims_reference_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_reference_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_reference_filename.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_reference_publish_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_reference_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_reference_number == toSearch) ? true : false;
             }

             $('*[data-datepicker="true"] input[type="text"]').datepicker({
                 todayBtn: true,
                 orientation: "top left",
                 autoclose: true,
                 todayHighlight: true
             });

             $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                 $('input[type="text"]', $(this).parent()).focus();
             });
             $scope.createdate = function (end_date, start_date, name) {


                 //var month1 = end_date.split("/")[0];
                 //var day1 = end_date.split("/")[1];
                 //var year1 = end_date.split("/")[2];
                 //var new_end_date = year1 + "/" + month1 + "/" + day1;
                 var new_end_date = end_date;//day1 + "/" + month1 + "/" + year1;

                 //var year = start_date.split("/")[0];
                 //var month = start_date.split("/")[1];
                 //var day = start_date.split("/")[2];
                 //var new_start_date = year + "/" + month + "/" + day;
                 var new_start_date = start_date;//day + "/" + month + "/" + year;

                 if (new_end_date < new_start_date) {

                     $rootScope.strMessage = "Please Select Future Date";
                     $('#message').modal('show');

                     $scope.edt[name] = '';
                 }
                 else {

                     $scope.edt[name] = new_end_date;
                 }
             }

             $scope.showdate = function (date, name) {

                 //var month = date.split("/")[0];
                 //var day = date.split("/")[1];
                 //var year = date.split("/")[2];
                 $scope.edt[name] = date;//year + "/" + month + "/" + day;


             }

             simsController.directive('ngFiles', ['$parse', function ($parse) {

                 function fn_link(scope, element, attrs) {
                     var onChange = $parse(attrs.ngFiles);
                     element.on('change', function (event) {
                         onChange(scope, { $files: event.target.files });
                     });
                 };

                 return {
                     link: fn_link
                 }
             }])




         }])
})();
