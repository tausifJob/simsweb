﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PPFieldStatusCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = "1";

            $http.get(ENV.apiUrl + "api/common/PPFieldSatus/getPPFieldSatus").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.obj = res.data;
                console.log($scope.obj);

            });


            $scope.Getinfo = function (field_code, editable, visible)
            {
                console.log(field_code, editable, visible);
                $http.post(ENV.apiUrl + "api/common/PPFieldSatus/updatePPFieldSatus?field_code=" + field_code + "&iseditable=" + editable + "&isvisible=" + visible).then(function (res) {
                    $scope.msg1 = res.data;
                    console.log($scope.msg1);
                    $scope.getGrid();
                });
            }

            $scope.getGrid = function ()
            {
                $http.get(ENV.apiUrl + "api/common/PPFieldSatus/getPPFieldSatus").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.obj = res.data;
                    console.log($scope.obj);

                });
            }

        }]);
})();