﻿(function () {
    'use strict';
    var citycode = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.directive('myEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.myEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    });

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    var data1 = [];
    simsController.controller('OtpMasterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            //$scope.operation = true;
            $scope.table1 = true;
            $scope.editmode = false;

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
            }
            $scope.edt = "";

            $http.get(ENV.apiUrl + "api/OtpMaster/getOtp").then(function (get_City) {
                $scope.City_Data = get_City.data;
                $scope.totalItems = $scope.City_Data.length;
                $scope.todos = $scope.City_Data;
                $scope.makeTodos();
            })

        

            $scope.New = function () {

                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                $scope.opr = 'S';
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {};
               // $scope.edt['sims_council_status'] = true;
                $scope.temp = '';
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.up = function (str) {
                $scope.opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;

                $scope.edt = {
                    sims_country_code: str.sims_country_code,
                    sims_country_name_en: str.sims_country_name_en,
                    sims_state_code: str.sims_state_code,
                    sims_state_name_en: str.sims_state_name_en,
                    sims_city_code: str.sims_city_code,
                    sims_status: str.sims_status,
                    sims_city_name_en: str.sims_city_name_en,
                    sims_city_name_ar: str.sims_city_name_ar,
                    sims_city_name_fr: str.sims_city_name_fr,
                    sims_city_name_ot: str.sims_city_name_ot
                };
                $scope.getacyr(str.sims_country_code);
                $scope.nullcheck();
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.doSomething = function ()
            {
                setTimeout(function () {
                    $scope.generate();
                }, 600);
            }

            $scope.generate = function () {
               
                swal({
                    title: '',
                    text: "Are you sure you want to Generate?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                    allowEnterKey: false
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var data = $scope.edt;
                        data.opr = 'I';
                        for (var i = 0; i < parseInt($scope.edt['no_of_link']) ; i++) {
                            data1.push(data);
                        }

                        $http.post(ENV.apiUrl + "api/OtpMaster/CUDgetCity", data1).then(function (msg) {
                            $scope.msg1 = msg.data;

                            if ($scope.msg1 == true) {



                                $http.get(ENV.apiUrl + "api/OtpMaster/getOtp").then(function (get_City) {
                                    $scope.City_Data = get_City.data;
                                    $scope.totalItems = $scope.City_Data.length;
                                    $scope.todos = $scope.City_Data;
                                    for (var i = 0; i < parseInt($scope.edt['no_of_link']) ; i++) {
                                        $scope.todos[i]['class'] = 'navajowhite' 
                                   }
                                  
                                    $scope.makeTodos();
                                    $scope.operation = false;
                                  
                                    

                                    data1 = [];
                                    $scope.edt = {};
                                });

                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                           

                        });
                    }
                });
            }

      

            

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 5;
            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
               
            };

            
            $scope.chk_click = function (i,chk) {
                var id = '#tr-' + i;
                if (chk)
                    $(id).css('background-color', 'navajowhite');
                else
                    $(id).css('background-color', '');

            }
            

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_city_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_city_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.deleterecord = function () {
                debugger
                $scope.flag = false;
                citycode = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById('test-' + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletelocacode = ({
                            'sims_otp': $scope.filteredTodos[i].sims_otp,
                            opr: 'D'
                        });
                        citycode.push(deletelocacode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger
                            $http.post(ENV.apiUrl + "api/OtpMaster/CUDgetCity", citycode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/OtpMaster/getOtp").then(function (get_City) {
                                                $scope.City_Data = get_City.data;
                                                $scope.totalItems = $scope.City_Data.length;
                                                $scope.todos = $scope.City_Data;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/OtpMaster/getOtp").then(function (get_City) {
                                                $scope.City_Data = get_City.data;
                                                $scope.totalItems = $scope.City_Data.length;
                                                $scope.todos = $scope.City_Data;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_city_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                data1 = [];
                $scope.currentPage = true;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.City_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.City_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                    item.sims_city_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_state_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_city_code == toSearch) ? true : false;
            }

            $scope.onlyNumbers = function (event) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
        }]
        )
})();