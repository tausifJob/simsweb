﻿(function () {
    'use strict';
    var main, temp, del = [];
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    
    simsController.controller('ReligionController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.table1 = true;


            var religioncode = [];
            var data1 = [];
            var data = [];
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });



            // $scope.imageUrl = ENV.apiUrl + '/Content/sjs/Images/ReligionImage1/';

            $scope.propertyName = null;
            $scope.reverse = false;
            // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $http.get(ENV.apiUrl + "api/ReligionDetails/getReligion").then(function (getReligion_Data) {
                $scope.Religion_Data = getReligion_Data.data;
                $scope.totalItems = $scope.Religion_Data.length;
                $scope.todos = $scope.Religion_Data;
                $scope.makeTodos();

            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };
            //size
            $scope.size = function (str) {
                if (str == 10 || str == 20) {
                    $scope.pager = true;
                }

                else {

                    $scope.pager = false;
                }
                if (str == "*") {
                    $scope.numPerPage = $scope.Religion_Data.length;
                    $scope.filteredTodos = $scope.Religion_Data;
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                }
                $scope.makeTodos();

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['check_all'] = false;
                $scope.row1 = '';
            }

            $scope.up = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
               
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.editmode = true;
                $scope.newmode = false;


                $scope.imageUrl = ENV.apiUrl + '/Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ReligionImage/' + str.sims_religion_img;

                $scope.sims_religion_img = str.sims_religion_img;

                $scope.edt = {
                    sims_religion_code: str.sims_religion_code,
                    sims_religion_name_en: str.sims_religion_name_en,
                    sims_religion_name_ar: str.sims_religion_name_ar,
                    sims_religion_name_fr: str.sims_religion_name_fr,
                    sims_religion_name_ot: str.sims_religion_name_ot,
                    sims_religion_img: $scope.sims_religion_img,
                    sims_religion_status: str.sims_religion_status
                };
            }

            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {


                var img = document.getElementById("file1");
                img = "";
                $scope.prev_img = "";

                $scope.editmode = false;
                $scope.newmode = true;
                $scope.readonly = false;

                $scope.table1 = false;
                $scope.operation = true;

                $scope.savebtn = true;
                $scope.updatebtn = false;

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.edt = {
                    sims_religion_code: "",
                    sims_religion_name_en: "",
                    sims_religion_name_ar: "",
                    sims_religion_name_fr: "",
                    sims_religion_name_ot: "",
                    sims_religion_status: ""
                }

            }
        }

            $scope.Save = function (myForm) {

                if (myForm) {
                    data1 = [];
                    data = [];
                    data = $scope.edt;
                    data.opr = 'I';
                    $scope.exist = false;
                    for (var i = 0; i < $scope.Religion_Data.length; i++) {
                        if ($scope.Religion_Data[i].sims_religion_code == data.sims_religion_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ text: "Religion Code Already Exists", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 300, height: 200 })
                    }

                    else {
                        data1.push(data);
                       
                        $http.post(ENV.apiUrl + "api/ReligionDetails/ReligionCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                            $http.get(ENV.apiUrl + "api/ReligionDetails/getReligion").then(function (getReligion_Data) {
                                $scope.Religion_Data = getReligion_Data.data;
                                $scope.totalItems = $scope.Religion_Data.length;
                                $scope.todos = $scope.Religion_Data;
                                $scope.makeTodos();
                            });
                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                }
            }

            $scope.Update = function (myForm) {
            
                    if (myForm) {
                        data1 = [];
                        data = $scope.edt;
                        data.opr = 'U';
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/ReligionDetails/ReligionCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                            $scope.operation = false;
                            $http.get(ENV.apiUrl + "api/ReligionDetails/getReligion").then(function (getReligion_Data) {
                                $scope.Religion_Data = getReligion_Data.data;
                                $scope.totalItems = $scope.Religion_Data.length;
                                $scope.todos = $scope.Religion_Data;
                                $scope.makeTodos();
                            });
                        })

                        $scope.operation = false;
                        $scope.table1 = true;
                    }
                
            }

            $scope.Cancel = function () {

                var img = document.getElementById("file1");
                img = "";
                $scope.prev_img = "";

                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();


                $scope.edt = {
                    sims_religion_code: "",
                    sims_religion_name_en: "",
                    sims_religion_name_ar: "",
                    sims_religion_name_fr: "",
                    sims_religion_name_ot: "",
                    sims_religion_status: ""
                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_religion_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_religion_code + i);
                        v.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.deleterecord = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_religion_code + i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletelocacode = ({
                                'sims_religion_code': $scope.filteredTodos[i].sims_religion_code,
                                opr: 'D'
                            });
                            religioncode.push(deletelocacode);
                        }
                    }

                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $http.post(ENV.apiUrl + "api/ReligionDetails/ReligionCUD", religioncode).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/ReligionDetails/getReligion").then(function (getReligion_Data) {
                                                    $scope.Religion_Data = getReligion_Data.data;
                                                    $scope.totalItems = $scope.Religion_Data.length;
                                                    $scope.todos = $scope.Religion_Data;
                                                    $scope.makeTodos();
                                                });
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/ReligionDetails/getReligion").then(function (getReligion_Data) {
                                                    $scope.Religion_Data = getReligion_Data.data;
                                                    $scope.totalItems = $scope.Religion_Data.length;
                                                    $scope.todos = $scope.Religion_Data;
                                                    $scope.makeTodos();
                                                });
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }

                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById($scope.filteredTodos[i].sims_religion_code + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        $scope.row1 = '';
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    $scope.currentPage = true;
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Religion_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Religion_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                return (
                     item.sims_religion_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_religion_name_ar.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_religion_name_fr.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_religion_name_ot.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_religion_code == toSearch) ? true : false;


            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            var formdata = new FormData();

            $scope.getTheFiles = function ($files) {
                $scope.filesize = true;

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);

                    var i = 0;
                    if ($files[i].size > 800000) {
                        $scope.filesize = false;
                        $scope.edt.photoStatus = false;
                        swal({  text: "File Should Not Exceed 800Kb.", imageUrl: "assets/img/notification-alert.png" });
                    }
                    else { }
                });
            };

            $scope.file_changed = function (element) {


                var photofile = element.files[0];

                $scope.photo_filename = (photofile.name);

                $scope.edt['sims_religion_img'] = $scope.photo_filename;

                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;

                    });
                };
                reader.readAsDataURL(photofile);

                var request = {
                    method: 'POST',
                    url: ENV.apiUrl + 'api/file/upload?filename=' + $scope.edt.sims_religion_name_en + '&location=ReligionImage',
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };


                $http(request).success(function (d) {

                    $scope.edt['sims_religion_img'] = $scope.edt.sims_religion_img;
                    $scope.edt['sims_religion_img'] = d;
                });
            };
        }]);

    
   simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
   }])


})();


