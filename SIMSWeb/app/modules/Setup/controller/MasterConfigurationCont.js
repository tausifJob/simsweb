﻿(function () {
    'use strict';
    var regioncode = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    var data1 = [];
    simsController.controller('MasterConfigurationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            //$scope.operation = true;
            $scope.table1 = true;
            $scope.editmode = false;
        //    var paramenter;
            $scope.size = function (str) {
                if (str == 10 || str == 20) {
                    $scope.pager = true;
                }

                else {

                    $scope.pager = false;
                }
                if (str == "*") {
                    $scope.numPerPage = $scope.RegionData.length;
                    $scope.filteredTodos = $scope.RegionData;
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                }
                $scope.makeTodos();

            }
            var parameter;
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
            }
            $scope.edt = "";


            //###########################This resion for Setup Tab#######################################################
            $scope.all_data_trans = '';
            $http.get(ENV.apiUrl + "api/TimeLineAlert/getModuleName").then(function (modname) {
                $scope.ModuleName = modname.data;
              
            });

            $scope.saveAlbumCreate = function (appl_mod_code) {
                $http.get(ENV.apiUrl + "api/TimeLineAlert/getApplicatioName?mod_code=" + appl_mod_code + "&appl_type=" + 'S').then(function (modname1) {
                    $scope.all_data = modname1.data;
                    $scope.totalItems = $scope.all_data.length;
                    $scope.todos = $scope.all_data;
                    $scope.makeTodos();
                        $http.get(ENV.apiUrl + "api/common/common_seq/getAllAcademicYear1?cur=" + $scope.all_data[0].comn_cur_code).then(function (res) {
                            $scope.acad = res.data;
                        });
                });
            }
            
            $scope.setup_click = function () {
                $scope.filteredTodos = '';
                parameter = '';
                parameter='1'
            }

            var updateSetup = [];
            $scope.Update1 = function () {
                debugger;
               updateSetup = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.all_data.length; i++) {
                    var v = document.getElementById(i + 5);
                    if (v.checked == true) {
                        var updatecode = {
                            'comn_character_count': $scope.all_data[i].comn_character_count,
                            'comn_character_sequence': $scope.all_data[i].comn_character_sequence,
                            'comn_number_count': $scope.all_data[i].comn_number_count,
                            'comn_number_sequence': $scope.all_data[i].comn_number_sequence,
                            'comn_academic_year': $scope.all_data[i].comn_academic_year,
                            'comn_mod_code': $scope.all_data[i].comn_mod_code,
                            'comn_appl_code': $scope.all_data[i].comn_appl_code,
                            'comn_sequence_code': $scope.all_data[i].comn_sequence_code,
                            'comn_description':$scope.all_data[i].comn_description,
                            opr: 'U'
                        }

                        updateSetup.push(updatecode)


                    }
                }
                $http.post(ENV.apiUrl + "api/TimeLineAlert/UpdateMatrConfig", updateSetup).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                });


            }




            //############################################################################################################


            //############################################################################################################
            //This resion for Transaction Tab
            $scope.saveAlbumCreate1 = function (appl_mod_code) {
                debugger
                $http.get(ENV.apiUrl + "api/TimeLineAlert/getApplicatioName?mod_code=" + appl_mod_code + "&appl_type=" + 'T').then(function (modname1) {
                    $scope.all_data_trans = modname1.data;
                    $scope.totalItems = $scope.all_data_trans.length;
                    $scope.todos = $scope.all_data_trans;
                    $scope.makeTodos();

                    $http.get(ENV.apiUrl + "api/common/common_seq/getAllAcademicYear1?cur=" + $scope.all_data_trans[0].comn_cur_code).then(function (res) {
                        $scope.acad = res.data;
                    });

                });
            }


            $scope.transaction_click = function () {
                parameter = '2';
                $scope.temp.comn_mod_code = '';
                $scope.all_data = '';
                $scope.all_data_report = '';
                $scope.totalItems = '';
                $scope.todos = '';
                $scope.filteredTodos = '';

            }
           var updateTrans = [];
            $scope.Update2 = function () {
                debugger;
                updateTrans = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.all_data_trans.length; i++) {
                    var v = document.getElementById(i + 6);
                    if (v.checked == true) {
                        var updatecode = {
                            'comn_character_count': $scope.all_data_trans[i].comn_character_count,
                            'comn_character_sequence': $scope.all_data_trans[i].comn_character_sequence,
                            'comn_number_count': $scope.all_data_trans[i].comn_number_count,
                            'comn_number_sequence': $scope.all_data_trans[i].comn_number_sequence,
                            'comn_academic_year': $scope.all_data_trans[i].comn_academic_year,
                            'comn_mod_code': $scope.all_data_trans[i].comn_mod_code,
                            'comn_appl_code': $scope.all_data_trans[i].comn_appl_code,
                            'comn_sequence_code': $scope.all_data_trans[i].comn_sequence_code,
                            'comn_description': $scope.all_data_trans[i].comn_description,
                            opr: 'U'
                        }

                        updateTrans.push(updatecode)
                    }
                }

                $http.post(ENV.apiUrl + "api/TimeLineAlert/UpdateMatrConfig", updateTrans).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                });
            }

            //############################################################################################################



            //############################################################################################################
            //This resion for Report Tab
            $scope.saveAlbumCreate2 = function (appl_mod_code) {
                debugger
                $http.get(ENV.apiUrl + "api/TimeLineAlert/getApplicatioName?mod_code=" + appl_mod_code + "&appl_type=" + 'R').then(function (modname1) {
                    $scope.all_data_report = modname1.data;
                    $scope.totalItems = $scope.all_data_report.length;
                    $scope.todos = $scope.all_data_report;
                    $scope.makeTodos();

                    $http.get(ENV.apiUrl + "api/common/common_seq/getAllAcademicYear1?cur=" + $scope.all_data_report[0].comn_cur_code).then(function (res) {
                        $scope.acad = res.data;
                    });
                });
            }
            $scope.reportClick = function () {
                parameter = '3';
                $scope.temp.comn_mod_code = '';
                $scope.all_data = '';
                $scope.all_data_trans = '';
                $scope.filteredTodos = '';

            }

            var updateReport = [];
            $scope.Update3 = function () {
                debugger;
                updateReport = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.all_data_report.length; i++) {
                    var v = document.getElementById(i + 7);
                    if (v.checked == true) {
                        var updatecode = {
                            'comn_character_count': $scope.all_data_report[i].comn_character_count,
                            'comn_character_sequence': $scope.all_data_report[i].comn_character_sequence,
                            'comn_number_count': $scope.all_data_report[i].comn_number_count,
                            'comn_number_sequence': $scope.all_data_report[i].comn_number_sequence,
                            'comn_academic_year': $scope.all_data_report[i].comn_academic_year,
                            'comn_mod_code': $scope.all_data_report[i].comn_mod_code,
                            'comn_appl_code': $scope.all_data_report[i].comn_appl_code,
                            'comn_sequence_code': $scope.all_data_report[i].comn_sequence_code,
                            'comn_description': $scope.all_data_report[i].comn_description,
                            opr: 'U'
                        }

                        updateReport.push(updatecode)
                    }
                }

                $http.post(ENV.apiUrl + "api/TimeLineAlert/UpdateMatrConfig", updateReport).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                });
            }

            //############################################################################################################

           

            //############################################################################################################
            //THis resion for Image URL

            $http.get(ENV.apiUrl + "api/TimeLineAlert/getReportURL").then(function (reportname) {
                $scope.all_report_data = reportname.data;
            });
            

            //############################################################################################################




           

          

       
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 5;
            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };


            //FOr SEtup
            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.all_data.length; i++) {
                        var v = document.getElementById(i + 5);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.all_data.length; i++) {
                        var v = document.getElementById(i + 5);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }
            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }


            //FOr TRAnsaction
            $scope.CheckAllCheckedTrans = function () {
                debugger;
                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.all_data_trans.length; i++) {
                        var v = document.getElementById(i + 6);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.all_data_trans.length; i++) {
                        var v = document.getElementById(i + 6);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }
            $scope.checkonebyonedeleteTrans = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            //FOr REport
            $scope.CheckAllCheckedReport = function () {
                debugger;
                main = document.getElementById('mainchk2');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.all_data_report.length; i++) {
                        var v = document.getElementById(i + 7);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.all_data_report.length; i++) {
                        var v = document.getElementById(i + 7);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }
            $scope.checkonebyonedeleteReport = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk2');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }



       

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function (str) {
                debugger;
                if (parameter == '1') {
                    $scope.searching_data = $scope.all_data;//$scope.all_data_report;
                }
                else if (parameter == '2') {
                    $scope.searching_data= $scope.all_data_trans;
                }
                else{

                    $scope.searching_data = $scope.all_data_report;
                }

                $scope.todos = $scope.searched($scope.searching_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.searching_data;
                }
                $scope.makeTodos();
            }


            function searchUtil(item, toSearch) {
                debugger
                return (
                     item.comn_appl_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_region_code == toSearch) ? true : false;
            }


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


        }]
        )

})();