﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TcCertificateRequiestCont',
          ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

             // console.clear();

              $scope.pagesize = "All";
              $scope.pageindex = "0";
              var main;
              $scope.hide_approve = false;
              // $scope.global_Search_click1 = function () {
              $rootScope.visible_stud = true;
              //$rootScope.visible_parent = false;
              //$rootScope.visible_search_parent = false;
              //$rootScope.visible_teacher = false;
              //$rootScope.visible_User = true;
              //$rootScope.visible_Employee = false;
              $rootScope.chkMulti = false;

              $timeout(function () {
                  $("#table").tableHeadFixer({ 'top': 1 });
              }, 100);

              var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');
              $scope.dt = {
                  sims_tc_certificate_request_date: dateyear,
              }

              $http.get(ENV.apiUrl + "api/StudentReport/getTcDirectApproveStatus").then(function (approvestatus) {
                  $scope.approve_status = approvestatus.data;
              });

              //$scope.size = function (str) {
              //    $scope.pagesize = str;
              //    $scope.currentPage = 1;
              //    $scope.numPerPage = str;
              //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
              //}

              //$scope.index = function (str) {
              //    $scope.pageindex = str;
              //    $scope.currentPage = str;
              //    console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
              //    main.checked = false;
              //    $scope.CheckAllChecked();
              //}

              //$scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

              //$scope.makeTodos = function () {
              //    var rem = parseInt($scope.totalItems % $scope.numPerPage);
              //    if (rem == '0') {
              //        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
              //    }
              //    else {
              //        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
              //    }

              //    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
              //    var end = parseInt(begin) + parseInt($scope.numPerPage);

              //    console.log("begin=" + begin); console.log("end=" + end);

              //    $scope.filteredTodos = $scope.todos.slice(begin, end);
              //};

              ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
              $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

              $scope.makeTodos = function () {
                  var rem = parseInt($scope.totalItems % $scope.numPerPage);
                  if (rem == '0') {
                      $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                  }


                  else {
                      $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                  }

                  var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                  var end = parseInt(begin) + parseInt($scope.numPerPage);


                  $scope.filteredTodos = $scope.todos.slice(begin, end);
              };

              $scope.size = function (str) {
                  //console.log(str);
                  //$scope.pagesize = str;
                  //$scope.currentPage = 1;
                  //$scope.numPerPage = str;
                  //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();

                  main = document.getElementById('mainchk');
                  if (main.checked == true) {
                      main.checked = false;
                      $scope.row1 = '';
                      $scope.color = '#edefef';
                  }

                  if (str == "All") {
                      $scope.currentPage = 1;
                      $scope.numPerPage = $scope.obj.length;
                      $scope.makeTodos();

                  }
                  else {
                      $scope.pagesize = str;
                      $scope.currentPage = 1;
                      $scope.numPerPage = str;
                      $scope.makeTodos();
                  }
              }

              $scope.index = function (str) {
                  //$scope.pageindex = str;
                  //$scope.currentPage = str;
                  //console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                  //// main.checked = false;
                  //$scope.CheckAllChecked();
                  $scope.pageindex = str;
                  $scope.currentPage = str;
                  $scope.makeTodos();

                  main = document.getElementById('mainchk');
                  if (main.checked == true) {
                      main.checked = false;
                      $scope.row1 = '';
                      $scope.color = '#edefef';
                  }
              }

              ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

              $('*[data-datepicker="true"] input[type="text"]').datepicker({
                  todayBtn: true,
                  orientation: "top left",
                  autoclose: true,
                  todayHighlight: true,
                  format: 'yyyy-mm-dd'
              });

              $scope.CheckAllChecked = function () {
                  debugger;
                  main = document.getElementById('mainchk');
                  if (main.checked == true) {
                      for (var i = 0; i < $scope.filteredTodos.length; i++) {
                          var v = document.getElementById($scope.filteredTodos[i].sims_tc_certificate_request_number + i);
                          v.checked = true;
                          $('tr').addClass("row_selected");
                      }
                  }
                  else {

                      for (var i = 0; i < $scope.filteredTodos.length; i++) {
                          var v = document.getElementById($scope.filteredTodos[i].sims_tc_certificate_request_number + i);
                          v.checked = false;
                          main.checked = false;
                          $scope.row1 = '';
                          $('tr').removeClass("row_selected");
                      }
                  }

              }


              $scope.checkonebyonedelete = function () {

                  $("input[type='checkbox']").change(function (e) {
                      if ($(this).is(":checked")) {
                          $(this).closest('tr').addClass("row_selected");
                          $scope.color = '#edefef';
                      } else {
                          $(this).closest('tr').removeClass("row_selected");
                          $scope.color = '#edefef';
                      }
                  });

                  main = document.getElementById('mainchk');
                  if (main.checked == true) {
                      main.checked = false;
                      $scope.color = '#edefef';
                      $scope.row1 = '';
                  }
              }

              $scope.shownew=function(str)
              {
                  
                  $http.get(ENV.apiUrl + "api/StudentReport/getTcCertificateRequieststatus?status=" + str).then(function (res1) {
                      $scope.obj = res1.data;
                      $scope.totalItems = $scope.obj.length;
                      $scope.todos = $scope.obj;
                      $scope.makeTodos();
                      if ($scope.obj.length == 0) {
                          $scope.tablehide = false;
                      }
                      else {
                          $scope.tablehide = true;
                      }
                      $scope.currentPage = true;

                  });
              }


              $scope.shownew('N');

            //  $scope.showdata();

              $scope.Show = function () {
                  $http.get(ENV.apiUrl + "api/StudentReport/getTcCertificateRequiestDate?date_wisesearch=" + $scope.dt.sims_tc_certificate_request_date).then(function (res1) {
                      $scope.obj = res1.data;
                     
                      $scope.totalItems = $scope.obj.length;
                      $scope.todos = $scope.obj;
                      $scope.makeTodos();
                      if ($scope.obj.length == 0) {
                          swal({ title: 'Alert', text: "No Record Found...", showCloseButton: true, width: 450, height: 200 });
                          $scope.tablehide = false;

                      }
                      else {
                          $scope.tablehide = true;
                      }
                  })
              }

              $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                  $('input[type="text"]', $(this).parent()).focus();
              });

              $scope.SearchEnroll = function () {
                  $scope.global_Search_click();
                  $('#Global_Search_Modal').modal({ backdrop: "static" });
              }

              $scope.$on('global_cancel', function () {
                  if ($scope.SelectedUserLst.length > 0) {
                      $scope.edt =
                          {
                              sims_enroll_number: $scope.SelectedUserLst[0].s_enroll_no,
                              sims_student_name: $scope.SelectedUserLst[0].name,
                              sims_cur_code: $scope.SelectedUserLst[0].s_cur_code,
                              sims_academic_year: $scope.SelectedUserLst[0].sims_academic_year,
                          }
                  }
              });

              $scope.Save = function (MyForm) {
                  
                  if (MyForm) {
                      $http.post(ENV.apiUrl + "api/StudentReport/GetTCappliedStatus?enroll=" + $scope.edt.sims_enroll_number + "&curr=" + $scope.edt.sims_tc_certificate_cur_code).then(function (check) {

                          $scope.msg = check.data;
                          //$scope.msg1 = $scope.msg;
                          if ($scope.msg == true) {
                              swal('', 'Certificate Already Issued for this Student');
                              return;
                          }
                      });
                      var user = $rootScope.globals.currentUser.username;
                      var Savedata = [];
                      var obj = ({
                          'sims_enroll_number': $scope.edt.sims_enroll_number,
                          'sims_tc_certificate_reason': $scope.edt.sims_tc_certificate_reason,
                          'sims_tc_certificate_requested_by': user,
                          'sims_tc_certificate_request_date': $scope.dt.sims_tc_certificate_request_date,
                          'sims_cur_code': $scope.edt.sims_cur_code,
                          'sims_academic_year': $scope.edt.sims_academic_year,
                          opr: 'I',
                      });

                      Savedata.push(obj);

                      $http.post(ENV.apiUrl + "api/StudentReport/CUDTccertificaterequiest", Savedata).then(function (msg) {

                          $scope.msg1 = msg.data;
                          if ($scope.msg1.strMessage != undefined) {
                              if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                  swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                                  //$scope.showdata();
                                  $scope.Reset();
                                  $scope.currentPage = true;
                              }
                          }
                          else {
                              swal({  text: "Record Not Inserted...", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                          }
                      });
                  }
              }

              $scope.searched = function (valLists, toSearch) {
                  return _.filter(valLists,
                  function (i) {
                      return searchUtil(i, toSearch);
                  });
              };

              $scope.search = function () {
                  $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                  $scope.totalItems = $scope.todos.length;
                  $scope.currentPage = '1';
                  if ($scope.searchText == '') {
                      $scope.todos = $scope.obj;
                  }
                  $scope.makeTodos();
              }

              function searchUtil(item, toSearch) {
                  return (item.sims_tc_certificate_request_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_tc_certificate_request_status.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sager == toSearch) ? true : false;
              }

              $scope.clear = function () {
                  $scope.edt = {
                      sims_enroll_number: '',
                      sims_tc_certificate_reason: '',
                  }
                  $scope.edt = '';
                  $scope.searchText = '';
              }

              $scope.Reset = function () {
                  
                  $scope.edt = {};                  
                  $scope.clear();
                  $scope.tablehide = true;
              }

              //Pending, Cancelled and Approved

              $scope.cancel_adm = function (info) {
                  
                  $('#myModal1').modal('hide');
                  setTimeout(function () {
                      $state.go('main.Sim622', { data: info });
                  }, 500);
              }

              $scope.cancel_adm_only = function (info) {
                  debugger;
                  var data = {}
                  data.enroll = info.sims_enroll_number;
                  data.reason = info.sims_tc_certificate_reason;
                  data.cancel_date = moment(new Date()).format('DD-MM-YYYY')
                  data.cur_code ="01";
                  data.academic_year = info.sims_tc_certificate_academic_year;
                  //data.fee_category_code = feedetails;
                  data.fees_paid_status = '1';
                  data.opr = "P";
                  data.username = $rootScope.globals.currentUser.username;
                  console.log(data);

                  $http.post(ENV.apiUrl + "api/common/CancelAdmission/CancelAdmissionProceed", data).then(function (res) {
                      $scope.obj1 = res.data;
                      swal({ title: "Alert", text: "Admission Cancelled", showCloseButton: true, width: 380, });
                      $scope.shownew('A');
                      $('#myModal1').modal('hide');
                      
                  });

                  }

              //$scope.viewDetails = function (clear) {
              //    
              //    console.log($scope.clear)
              //    //$state.go('main.CancelAdmissionClearFees', { data: $scope.info });
              //    $('#myModal1').modal('show');
              //    $http.get(ENV.apiUrl + "api/StudentReport/GetStatusforStudent?enroll_no=" + clear.sims_enroll_number).then(function (res) {
              //        debugger
              //        $scope.clear = res.data;
              //       // $scope.totalItems = $scope.clear.length;
              //       // $scope.todos = $scope.clear;
              //      //  $scope.makeTodos();
              //        $scope.clearance = true;
              //    });
              //}



              $scope.viewDetails = function (clear) {
                 
                  $scope.hide_approve = false;

                
                  //$state.go('main.CancelAdmissionClearFees', { data: $scope.info });

                  $('#myModal1').modal('show');
                  $http.get(ENV.apiUrl + "api/StudentReport/GetStatusforStudentNew?enroll_no=" + clear.sims_enroll_number).then(function (res) {
                    
                      $scope.clear = res.data;
                      // $scope.totalItems = $scope.clear.length;
                      // $scope.todos = $scope.clear;
                      //  $scope.makeTodos();
                      $scope.clearance = true;
                  });
              }

              $scope.viewDetailsApprove = function (clear,str) {
                  
                  $scope.main_obj_approve = clear;
                  $scope.hide_approve = true;

                
                  //$state.go('main.CancelAdmissionClearFees', { data: $scope.info });

                  $('#myModal1').modal('show');
                  $http.get(ENV.apiUrl + "api/StudentReport/GetStatusforStudentNew?enroll_no=" + clear.sims_enroll_number).then(function (res) {
                    
                      $scope.clear = res.data;
                      // $scope.totalItems = $scope.clear.length;
                      // $scope.todos = $scope.clear;
                      //  $scope.makeTodos();
                      $scope.clearance = true;
                  });
              }

              $scope.reject = function (str) {
                  swal({
                      title: '',
                      text: "Are you sure you want to Reject?",
                      showCloseButton: true,
                      confirmButtonText: 'Yes',
                      showCancelButton: true,
                      width: 380,
                      cancelButtonText: 'No',

                  }).then(function (isConfirm) {
                      if (isConfirm) {
                          //sims_tc_certificate_request_number
                          var user = $rootScope.globals.currentUser.username;
                          var Savedata = [];
                          var obj = ({
                              'sims_enroll_number': str.sims_enroll_number,
                              
                              'sims_tc_certificate_requested_by': user,
                              'sims_tc_certificate_request_number': str.sims_tc_certificate_request_number,
                              
                              opr: 'U',
                          });

                          Savedata.push(obj);

                          $http.post(ENV.apiUrl + "api/StudentReport/CUDTccertificaterequiestUpdate", Savedata).then(function (msg) {

                              $scope.msg1 = msg.data;
                              if (msg.data==true) {
                                  swal({ text: "Record  Rejected Successfully...", imageUrl: "assets/img/check.png", width: 380, height: 200 });
                                      $scope.showdata();
                                      $scope.Reset();
                                      $scope.currentPage = true;
                                  
                              }
                              else if (msg.data == false) {
                                  swal({ text: "Record Not Rejected...", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                              }
                              else {
                                  swal("Error-" + $scope.msg1)
                              }
                          });
                      }
                  });
                  
              }

              $scope.modal_cancel = function () {
                  //$scope.j = '';
                  $scope.clear = '';
                  $scope.showdata();
              }

              $scope.direct_approve = function () {
                  debugger;
                  var deletefin = [];
                  $scope.flag = false;
                  for (var i = 0; i < $scope.filteredTodos.length; i++) {
                      var v = document.getElementById($scope.filteredTodos[i].sims_tc_certificate_request_number + i);
                      if (v.checked == true) {
                          $scope.flag = true;
                          var deletemodulecode = ({
                              'sims_tc_certificate_request_number': $scope.filteredTodos[i].sims_tc_certificate_request_number,
                              'sims_tc_certificate_request_status': $scope.temp.sims_tc_direct_approve_status_code,
                              //'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                              'sims_enroll_number': $scope.filteredTodos[i].sims_enroll_number,
                              'opr': 'Q'
                          });
                          deletefin.push(deletemodulecode);
                      }
                  }
                  if ($scope.flag) {
                      swal({
                          title: '',
                          text: "Are you sure you want to Approve/Pending?",
                          showCloseButton: true,
                          showCancelButton: true,
                          confirmButtonText: 'Yes',
                          width: 380,
                          cancelButtonText: 'No',

                      }).then(function (isConfirm) {
                          if (isConfirm) {
                              $http.post(ENV.apiUrl + "api/StudentReport/approvePendingStatus", deletefin).then(function (msg) {
                                  $scope.msg1 = msg.data;
                                  if ($scope.msg1 == true) {
                                      swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                          if (isConfirm) {
                                              $scope.shownew('N');
                                              $scope.temp.sims_tc_direct_approve_status_code = '';
                                              main = document.getElementById('mainchk');
                                              if (main.checked == true) {
                                                  main.checked = false;
                                                  {
                                                      $scope.row1 = '';
                                                  }
                                              }
                                          }
                                      });
                                  }
                                  else if ($scope.msg1 == false) {
                                      swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                          if (isConfirm) {
                                              $scope.shownew('N');
                                              $scope.temp.sims_tc_direct_approve_status_code = '';
                                              main = document.getElementById('mainchk');
                                              if (main.checked == true) {
                                                  main.checked = false;
                                                  {
                                                      $scope.row1 = '';
                                                  }
                                              }
                                          }
                                      });
                                  }
                                  else {
                                      swal("Error-" + $scope.msg1)
                                  }
                              });
                          }
                          else {
                              for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                  var v = document.getElementById($scope.filteredTodos[i].sims_tc_certificate_request_number+ i);
                                  if (v.checked == true) {
                                      v.checked = false;

                                      main.checked = false;
                                      $('tr').removeClass("row_selected");
                                  }
                              }
                          }
                      });
                  }
                  else {
                      swal({  text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                  }
                  $scope.currentPage = true;
              }

          }]);
})();

