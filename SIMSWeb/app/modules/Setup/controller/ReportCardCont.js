﻿(function () {
    'use strict';
    var citycode = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    var data1 = [];
    simsController.controller('ReportCardCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
          
           
            var s
        
            //if ($http.defaults.headers.common['schoolId'] == 'dpsmis')
            //    s = "SimsReports." + $rootScope.location + ",SimsReports";
          
      
            //else
            //    s = "SimsReports." + $rootScope.location + ",SimsReports" + $http.defaults.headers.common['schoolId'];


            s = "SimsReports." + $rootScope.location + ",SimsReports" ;
            var url = window.location.href;
            var domain = url.substring(0, url.indexOf(':'))

            if ($http.defaults.headers.common['schoolId'] == 'sms') {
                if (domain == 'https')
                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                else
                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

            }
            else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc' || $http.defaults.headers.common['schoolId'] == 'christ' || $http.defaults.headers.common['schoolId'] == 'cesc' || $http.defaults.headers.common['schoolId'] == 'gvmcce' || $http.defaults.headers.common['schoolId'] == 'veitpoc' || $http.defaults.headers.common['schoolId'] == 'balgyani' || $http.defaults.headers.common['schoolId'] == 'ssvm' || $http.defaults.headers.common['schoolId'] == 'facp' || $http.defaults.headers.common['schoolId'] == 'gurukulponda'  || $http.defaults.headers.common['schoolId'] == 'clarahealtonation' || $http.defaults.headers.common['schoolId'] == 'vdpes' || $http.defaults.headers.common['schoolId'] == 'modern') {
                if (domain == 'https')
                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                else
                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
            }

            else if ( $http.defaults.headers.common['schoolId'] == 'asdportal') {
                if (domain == 'https')
                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                else
                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
            }

            else if ($http.defaults.headers.common['schoolId'] == 'imert') {
                if (domain == 'https')
                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                else
                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
            }
            else if ($http.defaults.headers.common['schoolId'] == 'portal' ) {
                if (domain == 'https')
                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                else
                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
            }

            else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                if (domain == 'https')
                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                else
                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
            }

            else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                if (domain == 'https')
                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                else
                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
            }
            else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                if (domain == 'https')
                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                else
                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
            }
            else if ($http.defaults.headers.common['schoolId'] == 'ibserp') {
                if (domain == 'https')
                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.ibskuwait.com/report/api/reports/';
                else
                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.ibskuwait.com/report/api/reports/';
            }

            else if ($http.defaults.headers.common['schoolId'] == 'sims') {
                if (domain == 'https')
                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.alhamdanyaschool.ae/report/api/reports/';
                else
                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.alhamdanyaschool.ae/report/api/reports/';
            }

            else if ($http.defaults.headers.common['schoolId'] == 'staging' || $http.defaults.headers.common['schoolId'] == 'staging2' || $http.defaults.headers.common['schoolId'] == 'aisdxb' || $http.defaults.headers.common['schoolId'] == 'tiadxb' || $http.defaults.headers.common['schoolId'] == 'tiashj' || $http.defaults.headers.common['schoolId'] == 'tosdxb') {
                if (domain == 'https')
                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                else
                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
            }

            else {
                if (domain == 'https')
                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                else
                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
            }
            console.log(service_url);
            $("#reportViewer1")
                           .telerik_ReportViewer({
                               //serviceUrl: ENV.apiUrl + "api/reports/",
                               serviceUrl: service_url,

                               viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                               scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                               // Zoom in and out the report using the scale
                               // 1.0 is equal to 100%, i.e. the original size of the report
                               scale: 1.0,
                               ready: function () {
                                   //this.refreshReport();
                               },
                               reportSource: {
                                   report:s ,
                                   //parameters: {
                                   //    //Date: new Date(),
                                   //    acad_year: '2016',
                                   //    cur_code:'01',
                                   //    grade_code: '07',
                                   //    section_code: '24',

                                   //    serch_student: '59401'
                                   //},
                               } 
                           });


       


            var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
            reportViewer.reportSource({
                report:s ,
                //parameters: {
                //    //Date: new Date(),
                //    acad_year: '2016',
                //    cur_code:'01',
                //    grade_code: '07',
                //    section_code: '24',

                //    serch_student: '59401'
                //},
            });

            setInterval(function () {

                $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                //$('.list k-widget k-listview').css({ 'style': 'height: 100px' })
                $('.list').attr('style', 'max-height: 150px;overflow-y: auto;overflow-x: hidden;');

                //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

            }, 100);
           

            $timeout(function () {
                
                $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                $('.list').attr('style', 'max-height: 150px;overflow-y: auto;overflow-x: hidden;');
                
                //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


            }, 100)
          


        }])
})();