﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('UserApplicationsForTeacherAppCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {



            //$scope.display1 = true;
            //$scope.grid = true;
            //$scope.pagesize = "5";
            //$scope.pageindex = "1";
            //var data = "";
            //var select, totalItems;
            //var Allusers;
            $scope.Allusers = [];
            $scope.busy = false;

            $scope.table = false;
            $scope.newmode = false;
            $scope.editonly = false;
            $scope.edt = { comn_user_date_created: new Date() }


           

            //$http.get(ENV.apiUrl + "api/common/User/getGroupCode").then(function (group) {
            //    $scope.usergroup = group.data;
            //    console.log($scope.usergroup);

            //})

            //child





            var theme = '';
            $scope.copy = 'Update';
            $scope.FlagUpdate = true;
            $scope.chkcopy = true;

            // var data = ['admin'];
            var data = [];

            $scope.UserApplicationList = undefined;
            var data1 = [];
            $("#Selected_User").jqxListBox({ allowDrop: true, allowDrag: true, source: data, width: 150, height: 150, theme: 'energyblue' });
            $("#Selected_Role").jqxListBox({ allowDrop: true, allowDrag: true, source: data1, width: 150, height: 150, theme: 'energyblue' });


            $http.get(ENV.apiUrl + "api/common/UserApplicationsForTeacherAppController/GetRoles").then(function (res) {

                $scope.role = res.data;


            });


            $scope.$on('global_cancel', function (str) {
                delvar = [];
                var selectedUserNames = [];
               console.log($scope.SelectedUserLst);
                if ($scope.SelectedUserLst.length > 0) {
                   

                    var data = [];
                    if ($scope.copy == 'Update') {
                        $("#Selected_Role").jqxListBox('clear');
                        $("#Selected_User").jqxListBox('clear');
                        //  if (delvar.length > 0) {
                        data.push($scope.SelectedUserLst[0].user_name);
                        $scope.selectedUser = $scope.SelectedUserLst[0].user_name;
                        $("#Selected_User").jqxListBox({ allowDrop: true, allowDrag: true, source: data, width: 150, height: 150, theme: 'energyblue' });
                        $http.get(ENV.apiUrl + "api/common/UserApplicationsForTeacherAppController/GetUserAssignRoles?userName=" + $scope.SelectedUserLst[0].user_code).then(function (res) {

                            $scope.da = res.data;

                            $("#Selected_Role").jqxListBox({ allowDrop: true, allowDrag: true, source: res.data, width: 150, height: 150, theme: 'energyblue' });


                        });
                        // }
                    }
                    else if ($scope.copy == 'Copy') {
                        for (var i = 0; i < $scope.SelectedUserLst.length; i++) {
                            selectedUserNames.push($scope.SelectedUserLst[i].user_name)
                        }
                        if ($scope.copysearch) {

                            $scope.copy_from_user = angular.copy($scope.SelectedUserLst[0].user_name);//delvar[0].user_name;
                            $http.get(ENV.apiUrl + "api/common/UserApplicationsForTeacherAppController/GetAssignRoles?userName=" + $scope.copy_from_user).then(function (res) {
                                console.log('tests');

                                console.log(res.data);

                                $scope.UserApplicationList = res.data;

                            });
                        }
                        else {

                            $("#Selected_User").jqxListBox({ allowDrop: true, allowDrag: true, source: selectedUserNames, width: 150, height: 150, theme: 'energyblue' });
                        }

                        // data.push($scope.SelectedUserLst[0].user_name);


                    }
                }

               
                // $scope.getstudentList();
            });




            $scope.copy_from_user_click = function () {
                 $scope.temp1 = undefined;
                $scope.Allusers = [];
                //   $('#searchUser').modal('show');
                $rootScope.visible_stud = false;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = true;
                $rootScope.visible_Employee = false;
                //if ($scope.copy == 'Update') {
                $rootScope.chkMulti = false;
                // }


                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });


                $scope.copysearch = true;
            }


            $scope.query = function (role) {

                if ($scope.test_comn_role_code == undefined || $scope.test_comn_role_code == '') {
                    alert('Please Select User or Role');
                }
                else {
                    //console.log($scope.comn_role_code);
                    var item = $('#Selected_User').jqxListBox('getItem', 0);
                    //var items = $("#Selected_User").jqxListBox('getItems');

                    if (item != null)
                        //console.log(item.value);
                        $http.get(ENV.apiUrl + "api/common/UserApplicationsForTeacherAppController/EditRoleApplication?userName=" + item.value + "&roleCode=" + $scope.test_comn_role_code).then(function (res) {

                            $scope.UserApplicationList = res.data;


                        });
                }

            }


            $scope.radioclick = function (str) {
                $scope.clear();
                // console.log(str)
                if (str == 'Update') {
                    $scope.FlagUpdate = true;
                    $scope.FlagCopy = false;
                    $scope.chkcopy = true;
                    $scope.uCopy = false;



                }
                else {
                    $scope.FlagUpdate = false;
                    $scope.FlagCopy = true;
                    $scope.chkcopy = false;


                }

            }


            $('#Selected_Role').bind('select', function (event) {

                var args = event.args;
                var item = $('#Selected_Role').jqxListBox('getItem', args.index);

                if (item != null) {
                    console.log(item.value);
                    $scope.test_comn_role_code = item.value;
                    $http.get(ENV.apiUrl + "api/common/UserApplicationsForTeacherAppController/EditRoleApplication?userName=" + $scope.selectedUser + "&roleCode=" + item.value).then(function (res) {

                        $scope.UserApplicationList = res.data;


                    });
                }
            });

            $scope.itemsPerPage = "5";
            $scope.currentPage = 0;
            $scope.items = [];

            $scope.size = function (pagesize) {
                $scope.itemsPerPage = pagesize;

            }

            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;

            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.Allusers.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;
            };

            $scope.serachUser = function () {
                $scope.temp1 = undefined;
                $scope.Allusers = [];
                $scope.copysearch = false;


                $rootScope.visible_stud = false;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = true;
                $rootScope.visible_Employee = false;

                $rootScope.chkMulti = false;

                $scope.global_Search_click();
               $('#Global_Search_Modal').modal({ backdrop: "static" });
         




            }



            $scope.cancel = function () {
                debugger
                delvar = [];
                var selectedUserNames = [];


                for (var i = 0; i < $scope.Allusers.length; i++) {

                    if ($scope.Allusers[i].user_code1 == true) {
                        delvar.push($scope.Allusers[i])
                        selectedUserNames.push($scope.Allusers[i].user_name)

                    }

                }
                console.log(delvar)
                var data = [];
                if ($scope.copy == 'Update') {
                    $("#Selected_Role").jqxListBox('clear');
                    $("#Selected_User").jqxListBox('clear');
                    if (delvar.length > 0) {
                        data.push(delvar[0].user_name);
                        $scope.selectedUser = delvar[0].user_name;
                        $("#Selected_User").jqxListBox({ allowDrop: true, allowDrag: true, source: data, width: 150, height: 150, theme: 'energyblue' });

                        $http.get(ENV.apiUrl + "api/common/UserApplicationsForTeacherAppController/GetUserAssignRoles?userName=" + delvar[0].user_code).then(function (res) {

                            $scope.da = res.data;

                            $("#Selected_Role").jqxListBox({ allowDrop: true, allowDrag: true, source: res.data, width: 150, height: 150, theme: 'energyblue' });


                        });
                    }
                }

                else {

                    if ($scope.copysearch) {

                        $scope.copy_from_user = delvar[0].user_name;
                        $http.get(ENV.apiUrl + "api/common/UserApplicationsForTeacherAppController/GetAssignRoles?userName=" + $scope.copy_from_user).then(function (res) {

                            $scope.UserApplicationList = res.data;
                            //console.log($scope.items);

                        });
                    }
                    else {

                        $("#Selected_User").jqxListBox({ allowDrop: true, allowDrag: true, source: selectedUserNames, width: 150, height: 150, theme: 'energyblue' });
                    }
                }

                $('#searchUser').modal('hide');

            }
            var main, del = '', delvar = [];
            $scope.chk = function () {

                main = document.getElementById('chk_min');
                del = [];
                if (main.checked == true) {
                    for (var i = 0; i < $scope.Allusers.length; i++) {

                        $scope.Allusers[i].user_code1 = true;

                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.Allusers.length; i++) {
                        $scope.Allusers[i].user_code1 = false;
                        main.checked = false;


                        $scope.row1 = '';
                    }
                }


            }


            $scope.Delete1 = function () {
                $scope.Allusers = [];
                $scope.currentPage = 0;

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchUser?data=" + JSON.stringify($scope.temp1)).then(function (users) {
                    $scope.Allusers = users.data;

                    $scope.table = true;


                    $scope.busy = false;
                })


                //del = '';

                //for (var i = 0; i < $scope.Allusers.length; i++) {

                //    if ($scope.Allusers[i].user_code1 == true) {
                //        delvar.push($scope.Allusers[i])
                //    }

                //}
                //console.log(delvar)
                //var data = { user_code: del };


            }


            //$http.get(ENV.apiUrl + "api/common/UserApplicationsForTeacherAppController/GetAssignRoles?userName=1mp49").then(function (res) {

            //    $scope.UserApplicationList = res.data;
            //    console.log($scope.items);

            //});




            $scope.statusCheck = function (str) {
                if (str) {
                    // $scope.obj1 = res.data;
                    for (var i = 0; i < $scope.UserApplicationList.length; i++) {
                        $scope.UserApplicationList[i].comn_user_read = true;
                        $scope.UserApplicationList[i].comn_user_insert = true;
                        $scope.UserApplicationList[i].comn_user_update = true;
                        $scope.UserApplicationList[i].comn_user_delete = true;
                        $scope.UserApplicationList[i].comn_user_massupdate = true;
                        $scope.UserApplicationList[i].comn_user_appl_status = true;

                    }
                }
                else {

                    for (var i = 0; i < $scope.UserApplicationList.length; i++) {
                        $scope.UserApplicationList[i].comn_user_read = false;
                        $scope.UserApplicationList[i].comn_user_insert = false;
                        $scope.UserApplicationList[i].comn_user_update = false;
                        $scope.UserApplicationList[i].comn_user_delete = false;
                        $scope.UserApplicationList[i].comn_user_massupdate = false;
                        $scope.UserApplicationList[i].comn_user_appl_status = false;


                    }
                }
            }

            $scope.ReadCheck = function (str, name) {
                if (str) {
                    // $scope.obj1 = res.data;
                    for (var i = 0; i < $scope.UserApplicationList.length; i++) {
                        if (name == 'Read')
                            $scope.UserApplicationList[i].comn_user_read = true;
                        if (name == 'Insert')
                            $scope.UserApplicationList[i].comn_user_insert = true;
                        if (name == 'Update')
                            $scope.UserApplicationList[i].comn_user_update = true;
                        if (name == 'Delete')
                            $scope.UserApplicationList[i].comn_user_delete = true;
                        if (name == 'Mass_Update')
                            $scope.UserApplicationList[i].comn_user_massupdate = true;


                    }
                }
                else {

                    for (var i = 0; i < $scope.UserApplicationList.length; i++) {
                        if (name == 'Read')
                            $scope.UserApplicationList[i].comn_user_read = false;
                        if (name == 'Insert')
                            $scope.UserApplicationList[i].comn_user_insert = false;
                        if (name == 'Update')
                            $scope.UserApplicationList[i].comn_user_update = false;
                        if (name == 'Delete')
                            $scope.UserApplicationList[i].comn_user_delete = false;
                        if (name == 'Mass_Update')
                            $scope.UserApplicationList[i].comn_user_massupdate = false;


                    }
                }
            }


            $scope.rowchk = function (index, status) {
                console.log(index);

                if (status) {
                    $scope.UserApplicationList[index].comn_user_read = true;
                    $scope.UserApplicationList[index].comn_user_insert = true;
                    $scope.UserApplicationList[index].comn_user_update = true;
                    $scope.UserApplicationList[index].comn_user_delete = true;
                    $scope.UserApplicationList[index].comn_user_massupdate = true;

                }
                else {

                    $scope.UserApplicationList[index].comn_user_read = false;
                    $scope.UserApplicationList[index].comn_user_insert = false;
                    $scope.UserApplicationList[index].comn_user_update = false;
                    $scope.UserApplicationList[index].comn_user_delete = false;
                    $scope.UserApplicationList[index].comn_user_massupdate = false;


                }


            }

            $scope.save = function () {
                debugger;
                if ($scope.copy == 'Update') {

                    var data = $scope.UserApplicationList;
                    var item = $('#Selected_User').jqxListBox('getItem', 0);
                    if (item != null) {
                        $scope.user = item.value;
                    }
                    for (var i = 0; i < $scope.UserApplicationList.length; i++) {



                        data[i].users = $scope.user;
                    }

                    $http.post(ENV.apiUrl + "api/common/UserApplicationsForTeacherAppController/InsertApplications", data).then(function (res) {

                        if (res.data==true) {
                            swal({ text: "Record Updated Successfully",  imageUrl: "assets/img/check.png" });
                            $scope.clear();
                        }
                        else if (res.data == false) {
                            swal({ text: "Record Not Updated Successfully ", imageUrl: "assets/img/close.png" });
                        }
                        else {
                            swal("Error-"+res.data)
                        }

                    });
                    //   }

                }
                else {

                    var items = $("#Selected_User").jqxListBox('getItems');

                    var users = '';
                    for (var i = 0; i < items.length; i++) {
                        console.log(items[i].label);
                        debugger;
                        users = users + items[i].label + ',';
                        

                    }
                        var data = $scope.UserApplicationList;

                        data[0].users = users;

                        $http.post(ENV.apiUrl + "api/common/UserApplicationsForTeacherAppController/CopyApplications", data).then(function (res) {

                            $scope.ins = res.data;

                            if (res.data==true) {
                                //$scope.clear();
                                swal({  text: "Record Updated Successfully", imageUrl: "assets/img/check.png" });
                            }
                            else if (res.data == false) {
                                swal({  text: "Record Not Updated Successfully ", imageUrl: "assets/img/notification-alert.png" });
                            }
                            else {
                                swal("Error-"+res.data)
                            }
                        });
                    
                }


            }
            $scope.clear = function () {
                $scope.copy_from_user = undefined;
                $scope.UserApplicationList = undefined;
                $scope.test_comn_role_code = undefined;
                $("#Selected_Role").jqxListBox('clear');
                $("#Selected_User").jqxListBox('clear');
                $scope.Allusers = [];

            }
            $scope.copyCheck = function (str) {
                if (str) {

                    $scope.FlagUpdate = false;

                }


                else {
                    $scope.FlagUpdate = true;
                }

            }



        }])
})();
