﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Setup');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SubjectCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            //$scope.pagesize = "5";
            //$scope.pageindex = "1";

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.sub_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.edit_code = false;
            $scope.temp = [];
            $scope.att = [];
            $scope.user_access = [];
            
            

            $scope.edt = [];
            $scope.multi_array = [{}];
            //$scope.subject_code_att = '';

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $http.get(ENV.apiUrl + "api/common/Subject/getSubject").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.sub_data = res.data;
                $scope.totalItems = $scope.sub_data.length;
                $scope.todos = $scope.sub_data;
                $scope.makeTodos();

                for (var i = 0; i < $scope.totalItems; i++) {
                    $scope.sub_data[i].icon = "fa fa-plus-circle";
                }

            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };


            $scope.Getinfo = function (cur) {

                if (cur != '') {
                    $http.get(ENV.apiUrl + "api/common/Subject/getSubjectGroup?curCode=" + cur).then(function (res) {
                        $scope.subgrp = res.data;
                        console.log($scope.obj1);
                    });

                    $http.get(ENV.apiUrl + "api/common/Subject/getSubjectType?curCode=" + cur).then(function (res) {
                        $scope.obj3 = res.data;
                        console.log($scope.obj3);
                    });

                }

            }



            $http.get(ENV.apiUrl + "api/common/Subject/getCountry").then(function (res) {
                $scope.obj4 = res.data;
                console.log($scope.obj4);
            });

            $scope.Save = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        $http.post(ENV.apiUrl + "api/common/Subject/CheckSubject?SubjectCode=" + $scope.edt.subject_code + "&cur_code=" + $scope.edt.sims_attendance_cur_code).then(function (res) {
                            $scope.msg1 = res.data;
                            console.log($scope.msg1);

                            if ($scope.msg1.status == true) {
                                //  $('#myModal').modal('show');
                                swal({ text: "Subject Already Exists", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                                $scope.edt = "";

                            }
                            else {
                                var data = ({
                                    sims_attendance_cur_code: $scope.edt.sims_attendance_cur_code,
                                    subject_code: $scope.edt.subject_code,
                                    subject_group_code: $scope.edt.subject_group_code,
                                    subject_name_en: $scope.edt.subject_name_en,
                                    subject_name_ar: $scope.edt.subject_name_ar,
                                    subject_name_fr: $scope.edt.subject_name_fr,
                                    subject_name_ot: $scope.edt.subject_name_ot,
                                    subject_name_abbr: $scope.edt.subject_name_abbr,
                                    subject_moe_code: $scope.edt.subject_moe_code,
                                    subject_status: $scope.edt.subject_status,
                                    subject_type: $scope.edt.subject_type,
                                    subject_country_code: $scope.edt.subject_country_code,
                                    sims_subject_color_code: $scope.edt.sims_subject_color_code,
                                    sims_religion_language: $scope.edt.sims_religion_language,
                                    sims_isArabic_subject :$scope.edt.sims_isArabic_subject,
                                    opr: 'I'
                                });
                                debugger;
                                data1.push(data);
                                $http.post(ENV.apiUrl + "api/common/Subject/updateInsertDeleteSubject", data1).then(function (res) {
                                    $scope.display = true;
                                    $scope.msg1 = res.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Subject Added Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Subject Not Added Successfully. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/common/Subject/getSubject").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.sub_data = res.data;
                    $scope.totalItems = $scope.sub_data.length;
                    $scope.todos = $scope.sub_data;
                    $scope.makeTodos();
                    for (var i = 0; i < $scope.totalItems; i++) {
                        $scope.sub_data[i].icon = "fa fa-plus-circle";
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    var data = ({
                        sims_attendance_cur_code: $scope.edt.sims_attendance_cur_code,
                        subject_code: $scope.edt.subject_code,
                        subject_group_code: $scope.edt.subject_group_code,
                        subject_name_en: $scope.edt.subject_name_en,
                        subject_name_ar: $scope.edt.subject_name_ar,
                        subject_name_fr: $scope.edt.subject_name_fr,
                        subject_name_ot: $scope.edt.subject_name_ot,
                        subject_name_abbr: $scope.edt.subject_name_abbr,
                        subject_moe_code: $scope.edt.subject_moe_code,
                        subject_status: $scope.edt.subject_status,
                        subject_type: $scope.edt.subject_type,
                        subject_country_code: $scope.edt.subject_country_code,
                        sims_subject_color_code: $scope.edt.sims_subject_color_code,
                        subject_language: $scope.edt.subject_language,
                        sims_religion_language: $scope.edt.sims_religion_language,
                        sims_isArabic_subject: $scope.edt.sims_isArabic_subject,
                        opr: 'U'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/Subject/updateInsertDeleteSubject", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Subject Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });

                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Subject Not Updated Successfully. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.Delete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    var deletecode = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {

                        var v = document.getElementById($scope.filteredTodos[i].subject_code + i);

                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodercode = ({
                                'subject_code': $scope.filteredTodos[i].subject_code,
                                'sims_attendance_cur_code': $scope.filteredTodos[i].sims_attendance_cur_code,
                                'opr': 'D'
                            });
                            deletecode.push(deletemodercode);
                        }
                    }

                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {


                                $http.post(ENV.apiUrl + "api/common/Subject/updateInsertDeleteSubject", deletecode).then(function (res) {
                                    $scope.msg1 = res.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Subject Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });

                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Subject Not Deleted Successfully. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                            else {
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }

                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById($scope.filteredTodos[i].subject_code + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                                $scope.row1 = '';
                            }

                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                }
            }
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].subject_code + i);
                        v.checked = true;
                        if (v.checked == true) {
                            $('tr').addClass("row_selected");
                        }
                        //$scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].subject_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    var autoid;
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = true;
                    $scope.update1 = false;
                    $scope.saveattr = false;
                    $scope.addattribuet1 = false;
                    $scope.delete1 = false;
                    $scope.edt = [];

                    $scope.edit_code = false;
                    $scope.edt['subject_status'] = true;
                    $scope.edt['sims_isArabic_subject'] = false;
                    $http.get(ENV.apiUrl + "api/common/Subject/getAutoGeneratesubjectCode").then(function (res) {
                        autoid = res.data;
                        console.log(autoid);
                        $scope.edt['subject_code'] = autoid;
                    });

                    $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                        $scope.obj2 = res.data;
                        $scope.edt['sims_attendance_cur_code'] = $scope.obj2[0].sims_attendance_cur_code;
                        $scope.Getinfo($scope.edt.sims_attendance_cur_code);
                    });
                }
            }

            $scope.edit = function (str) {
                debugger;
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = false;
                    $scope.update1 = true;
                    $scope.saveattr = true;
                    $scope.addattribuet1 = true;
                    $scope.delete1 = false;
                    $scope.attributedisplay = false;

                    $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                        $scope.obj2 = res.data;
                        $scope.edt =
                            {
                                sims_attendance_cur_code: str.sims_attendance_cur_code,
                                subject_code: str.subject_code,
                                subject_group_code: str.subject_group_code,
                                subject_name_en: str.subject_name_en,
                                subject_name_ar: str.subject_name_ar,
                                subject_name_fr: str.subject_name_fr,
                                subject_name_ot: str.subject_name_ot,
                                subject_name_abbr: str.subject_name_abbr,
                                subject_moe_code: str.subject_moe_code,
                                subject_status: str.subject_status,
                                subject_type: str.subject_type,
                                subject_country_code: str.subject_country_code,
                                sims_subject_color_code: str.sims_subject_color_code,
                                sims_religion_language: str.sims_religion_language,
                                sims_isArabic_subject: str.sims_isArabic_subject
                            }

                        $scope.edit_code = true;


                        //$scope.edt['sims_attendance_cur_code'] = $scope.obj2[0].sims_attendance_cur_code;
                        $scope.Getinfo($scope.edt.sims_attendance_cur_code);
                    });

                    // $scope.Getinfo($scope.edt.sims_attendance_cur_code);
                }
            }
            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.attributedisplay = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.myFormN.$setPristine();
                $scope.myFormN.$setUntouched();
            }

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.sub_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                debugger;
                $scope.todos = $scope.searched($scope.sub_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.sub_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].subject_code+i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.subject_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.subject_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.subject_name_abbr.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.subject_group_code_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;

            }

            var dom;
            $scope.flag = true;
            $scope.icon = "fa fa-plus-circle";

            $scope.expand = function (info, $event) {
                console.log(info);

                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table table table-hover table-bordered table-condensed' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                        //"<tr><td class='semi-bold'>" + "In Arabic" + "</td>" + "<td class='semi-bold'colsapn='2'>" + "In French" + "" + "<td class='semi-bold'colsapn='2'>" + "In Regional" + "<td class='semi-bold'>" + "Short Name" + "</td></tr>" +
                        //  "<tr><td>" + (info.subject_name_ar) + "</td> " + "<td colsapn='2'>" + (info.subject_name_fr) + "" + "<td colsapn='2'>" + (info.subject_name_ot) + "<td>" + (info.subject_name_abbr) + "</td></tr>" +
                        //  "<tr><td class='semi-bold'colsapn='2'>" + "MOE Code" + "<td class='semi-bold'>" + "Subject Type" + "</td> " + "<td class='semi-bold'colsapn='2'>" + "Country Name" + "</td></tr>" +
                        //   "<tr><td colsapn='2'>" + (info.subject_moe_code) + "<td>" + (info.subject_type_name) + "</td>" + "<td colsapn='2'>" + (info.subject_country_code_name) + "</td></tr>" +

                         "<tr style='background-color: #e9e4e4'><td class='semi-bold'>" + "In Arabic" + "</td>" + "<td class='semi-bold'colsapn='2'>" + "In French" + "" + "<td class='semi-bold'colsapn='2'>" + "In Regional" + "<td class='semi-bold'>" + "Short Name" + "</td><td class='semi-bold'colsapn='2'>" + "MOE Code" + "<td class='semi-bold'>" + "Subject Type" + "</td> " + "<td class='semi-bold'colsapn='2'>" + "Country Name" + "</td></tr>" +
                          "<tr><td>" + (info.subject_name_ar) + "</td> " + "<td colsapn='2'>" + (info.subject_name_fr) + "" + "<td colsapn='2'>" + (info.subject_name_ot) + "<td>" + (info.subject_name_abbr) + "</td><td colsapn='2'>" + (info.subject_moe_code) + "<td>" + (info.subject_type_name) + "</td>" + "<td colsapn='2'>" + (info.subject_country_code_name) + "</td></tr>" +
                        "</tbody>" +
                        " </table></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    info.icon = "fa fa-plus-circle";
                    $scope.flag = true;
                }
            };

            $scope.ex = function (str) {
                console.log("hello");
            }
            //-------------------Add Attributes----------------

            $scope.Add = function () {
                $scope.multi_array.push({});
                for (var i = 0; i < $scope.multi_array.length; i++) {
                    $scope.multi_array[i].sims_attribute_status = true;
                }
            }

            for (var i = 0; i < $scope.multi_array.length; i++) {
                $scope.multi_array[i].sims_attribute_status = true;
            }


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res1) {
                debugger
                $scope.cur_data = res1.data;
                $scope.att.sims_cur_code = $scope.cur_data[0].sims_cur_code;
                $scope.getAcademic_year($scope.cur_data[0].sims_cur_code);


            });

            $scope.getAcademic_year = function (cur_code1) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code1).then(function (res1) {
                    $scope.acad_data = res1.data;
                    $scope.att.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                    $scope.getGrade($scope.att.sims_cur_code, $scope.att.sims_academic_year);
                    //  $scope.getSection($scope.temp.sims_academic_year, $scope.temp.grade_code, $scope.temp.sims_cur_code);
                });
            }

            $scope.getGrade = function (cur_code, academic_year) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.att.sims_cur_code + "&academic_year=" + $scope.att.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%',
                            filter: true
                        });
                        $("#cmb_grade_code").multipleSelect("checkAll");
                    }, 1000);



                });
                $scope.getSection($scope.att.sims_cur_code, $scope.att.sims_academic_year, $scope.att.grade_code);
            }

            $scope.getSection = function (cur_code, academic_year, grade_code) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/Subject/getSectionFromGradeForAttr?cur_code=" + $scope.att.sims_cur_code + "&grade_code=" + $scope.att.sims_grade_code + "&academic_year=" + $scope.att.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%',
                            filter: true
                        });
                        $("#cmb_section_code").multipleSelect("checkAll");
                    }, 1000);
                });
            }


            $scope.addattribuet = function (edt) {

                $scope.grid = false;
                $scope.display = false;
                $scope.attributedisplay = true;
                $scope.sims_attribute_group_code = $scope.edt.subject_code;
                $scope.sims_attribute_group_name_en = $scope.edt.subject_name_en;
                $scope.sims_attribute_group_name_ot = $scope.edt.subject_name_ot;
                $scope.sims_attribute_group_type = $scope.edt.subject_type;
                $scope.sims_attribute_group_status = $scope.edt.subject_status;
                $scope.showtable();

            };

            
            $scope.SaveAttribute = function (MyformN) {
                debugger
               var datasend = [];
               if (MyformN) {

                  //var sec=''

                  //for (var a = 0; a < $scope.att.sims_section_code.length; a++) {
                  //    sec = sec + $scope.att.sims_section_code[a] + ',';
                  //}
                              var agcoll = [];
                             for (var i = 0; i < $scope.multi_array.length; i++) {
                                  var d = {
                                     sims_academic_year: $scope.att.sims_academic_year,
                                     sims_cur_code: $scope.att.sims_cur_code,
                                     sims_grade_code:' ',//$scope.att.sims_grade_code,
                                     sims_section_code: ' ',//$scope.att.sims_section_code,
                                     sims_attribute_group_code: $scope.sims_attribute_group_code,
                                     sims_attribute_grade: $scope.multi_array[i].sims_attribute_grade,
                                     sims_attribute_name: $scope.multi_array[i].sims_attribute_name,
                                     sims_attribute_name_ot: $scope.multi_array[i].sims_attribute_name_ot,
                                     sims_attribute_max_score: $scope.multi_array[i].sims_attribute_max_score,
                                     sims_attribute_display_order: $scope.multi_array[i].sims_attribute_display_order,
                                     sims_attribute_status: $scope.multi_array[i].sims_attribute_status,
                                     sims_attribute_group_name_en: $scope.sims_attribute_group_name_en,
                                     sims_attribute_group_name_ot: $scope.sims_attribute_group_name_ot,
                                     sims_attribute_group_type: $scope.sims_attribute_group_type,
                                     sims_attribute_group_status: $scope.sims_attribute_group_status,
                                     sims_attribute_type: 'S',
                                     //sims_attribute_group_status: $scope.sims_attribute_group_status,
                           }
                           agcoll.push(d);
                       
                   }
                    
                   $http.post(ENV.apiUrl + "api/common/Subject/CUDAttribute", agcoll).then(function (msg) {
                       $scope.msg1 = msg.data;                       
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.reset_multi();
                            $scope.showtable();
                            
                            
                        }
                        else {
                            swal({ text: "Record Not Inserted " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.showtable();
                        }

                       
                    });
                   
                    $scope.table = true;
                    $scope.display = false;
                    
                }

            }

            $scope.reset_multi = function () {
                debugger;

                for (var i = 0; i < $scope.multi_array.length; i++) {
                    $scope.multi_array[i].sims_attribute_grade = '';
                    $scope.multi_array[i].sims_attribute_name = '';
                    $scope.multi_array[i].sims_attribute_name_ot = '';
                    $scope.multi_array[i].sims_attribute_display_order = '';
                    $scope.multi_array[i].sims_attribute_status = true;
                }

                $scope.multi_array = [];

                $scope.att.sims_attribute_name = '';
                $scope.att.sims_attribute_name_ot = '';
                $scope.att.sims_attribute_grade = '';
                $scope.att.sims_attribute_display_order = '';
                $scope.Add();

            }

            $scope.showtable = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/common/Subject/getAttribute?cur_code=" + $scope.att.sims_cur_code + "&academic_year=" + $scope.att.sims_academic_year + "&subject=" + $scope.edt.subject_code).then(function (res) {
                    $scope.attribute_data = res.data;
                    $scope.totalItems = $scope.attribute_data.length;
                    //$scope.todos = $scope.attribute_data;
                    //$scope.makeTodos();
                    $scope.grid = false;
                    $scope.display = false;
                    $scope.attributedisplay = true;

                });
                main = document.getElementById('mainchkatt');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myFormN.$setPristine();
                $scope.myFormN.$setUntouched();

            }

            $scope.cancelatt = function () {
             
                for (var i = 0; i < $scope.multi_array.length; i++) {
                    $scope.multi_array[i].sims_attribute_grade = '';
                    $scope.multi_array[i].sims_attribute_name = '';
                    $scope.multi_array[i].sims_attribute_name_ot = '';
                    $scope.multi_array[i].sims_attribute_display_order = '';
                    $scope.multi_array[i].sims_attribute_status = true;
                    $scope.grid = false;
                    $scope.display = true;
                    $scope.attributedisplay = false;
                }
            }



















            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search1 = function () {
                debugger
                $scope.todos = $scope.searched($scope.attribute_data, $scope.searchText);
                $scope.totalItems = $scope.attribute_data.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.attribute_data;
                }
                //$scope.makeTodos();
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.attribute_data.length; i++) {
                        var t = $scope.filteredTodos[i].attribute_data;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }

                }


            }


            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.class1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_attribute_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_attribute_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_attribute_group_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_academic_year.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.searchedsubject = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtilsubject(i, toSearch);
                });
            };

            $scope.searchsubject = function () {
                debugger;
                $scope.todos = $scope.searchedsubject($scope.sub_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.sub_data;
                }
                $scope.makeTodos();
            }

            function searchUtilsubject(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                     item.subject_cur_code_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.subject_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.subject_group_code_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.subject_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ) ? true : false;
            }

        }])
})();


