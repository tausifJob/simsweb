﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('MograDeploymentFinalisationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            

            debugger
            $http.get(ENV.apiUrl + "api/TimeLineAlert/getprojectName").then(function (projname) {
                $scope.projectName = projname.data;
            });

            $http.get(ENV.apiUrl + "api/TimeLineAlert/getModuleName").then(function (modname) {
                $scope.ModuleName = modname.data;
            });

            $scope.getApplications = function (module_code) {

                $http.get(ENV.apiUrl + "api/TimeLineAlert/getProjectApplications?mod_code=" + module_code).then(function (applname) {
                    $scope.appication_Name = applname.data;
                });
            }

            $scope.temp = {};

            $scope.searchdata = function () {
                debugger
                $http.get(ENV.apiUrl + "api/TimeLineAlert/getinnerDeployedChangesetData?change_setNo=" + $scope.temp.changeset_code + "&pro_name=" + $scope.temp.comn_appl_project_code + "&dev_name=" + $scope.temp.comn_developer_name + "&appl_code=" + $scope.temp.comn_appl_code).then(function (res) {
                    if (res.data.length > 0) {
                        $scope.Search_data = res.data;
                    }
                    else {
                        swal({ text: "Record Not present", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, })
                        $('#myModal').modal('hide');
                        $scope.temp = "";
                        $scope.Search_data = "";
                    }
                });
            }

            $scope.cross = function () {
                $scope.temp = "";
                $scope.Search_data = "";
            }
            $scope.infoget = [];

            $scope.AddDetail = function () {
                debugger
                $scope.newdata = [];
                for (var i = 0; i < $scope.Search_data.length; i++) {
                    if ($scope.Search_data[i].ischeck == true) {
                        $scope.newdata.push($scope.Search_data[i]);
                    }

                }
                if ($scope.infoget.length > 0) {
                    for (var j = 0; j < $scope.infoget.length; j++) {
                        $scope.newdata.push($scope.infoget[j]);
                    }
                }
                $scope.infoget = $scope.newdata;
                $('#myModal').modal('hide');
                $scope.changeset_number = $scope.infoget[0].lic_changeset_no;

            }

            $scope.checMultiple = function () {
                $scope.newdata = [];
                var v = document.getElementById('chk_term2');
                for (var i = 0; i < $scope.Search_data.length; i++) {
                    if (v.checked == true) {
                        $scope.Search_data[i].ischeck = true;
                    } else {
                        $scope.Search_data[i].ischeck = false;
                    }
                }
            }

            $scope.multipleselect = function () {
                var v = document.getElementById('mainchk');
                for (var i = 0; i < $scope.infoget.length; i++) {
                    if (v.checked == true) {
                        $scope.infoget[i].ischeck1 = true;
                    }
                    else {
                        $scope.infoget[i].ischeck1 = false;
                    }
                }
            }

            $scope.SaveData = function () {
                debugger
                var senddata = [];

                for (var i = 0; i < $scope.infoget.length; i++) {

                    if ($scope.infoget[i].ischeck1 == true) {
                        $scope.infoget[i].lic_version_number = $scope.edt.lic_version_number;
                        $scope.infoget[i].lic_release_number = $scope.edt.lic_release_number;
                        $scope.infoget[i].lic_patch_number = $scope.edt.lic_patch_number;

                        senddata.push($scope.infoget[i]);
                        //   concole($scope.senddata);

                    }
                }
                $http.post(ENV.apiUrl + "api/TimeLineAlert/CUDDeployedVersionUpdate", senddata).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Approved Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, })
                        $scope.infoget = [];
                        $scope.edt = "";
                        $scope.changeset_number = "";

                    }
                });
            }


        }]);

})();


