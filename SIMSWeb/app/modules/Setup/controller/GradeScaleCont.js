﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('GradeScaleCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;

            $scope.propertyName = null;
            $scope.reverse = false;
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {
                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //$http.get(ENV.apiUrl + "api/grd/getGrd").then(function (getGrade_Data) {

            //    $scope.Location_Data = getGrade_Data.data;
            //    $scope.totalItems = $scope.Location_Data.length;
            //    $scope.todos = $scope.Location_Data;
            //    $scope.makeTodos();
            //});

            $scope.getGrid = function () {
                $http.get(ENV.apiUrl + "api/grd/getGrd").then(function (getLocation_Data) {
                    $scope.Location_Data = getLocation_Data.data;
                    $scope.totalItems = $scope.Location_Data.length;
                    $scope.todos = $scope.Location_Data;
                    $scope.makeTodos();
                    $scope.getcur();
                });
                
            }
            $scope.getGrid();
            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str;
            //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            // Curiculm
            $scope.getcur = function () {
                $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                    debugger;
                    $scope.curriculum = res.data;
                    $scope.edt = {
                        sims_cur_code: $scope.curriculum[0].sims_cur_code
                    };
                  
                    $scope.getacyr($scope.curriculum[0].sims_cur_code);
                });
            }
            // acdemic year
            
            $scope.getacyr = function (str) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    $scope.edt = {
                        sims_academic_year: $scope.Academic_year[0].sims_academic_year,
                        sims_cur_code: str
                    };
                  
                    $scope.getsections($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);

                });
            }

            // grade

            $scope.getsections = function (str, str1) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str + "&academic_year=" + str1).then(function (res) {
                    $scope.grade = res.data;

                    console.log($scope.grade);

                })

            };

            $scope.size = function (str) {
                if (str == 10 || str == 20) {
                    $scope.pager = true;
                }

                else {

                    $scope.pager = false;
                }
                if (str == "*") {
                    $scope.numPerPage = $scope.Location_Data.length;
                    $scope.filteredTodos = $scope.Location_Data;
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }


            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Location_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Location_Data;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_mark_assesment_grade_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_mark_assesment_grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_mark_assesment_grade_code == toSearch) ? true : false;
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    $scope.getGrid();
                    $scope.getcur();
                    $scope.disabled = false;
                    $scope.readonly = true;
                    $scope.table = false;
                    $scope.display = true;
                    $scope.save_btn = true;
                    $scope.Update_btn = false;
                    $scope.edt = "";
                    $scope.Myform.$setPristine();
                    $scope.Myform.$setUntouched();
                    $scope.temp = {};
                    //$scope.temp.sims_location_status = true;
                    $scope.curname = false;
                    $scope.acdyear = false;
                }
            }


            var datasend = [];
            $scope.savedata = function (Myform) {
                if (Myform) {
                    debugger;
                    //var flag = false;
                    var data = $scope.edt;
                    //for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    //    if ($scope.filteredTodos[i].sims_scale_desc == $scope.edt.sims_scale_desc) {
                    //        swal({ text: "Record Already Exist", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    //        flag = false;
                    //        break;
                    //    }
                    //    else {
                    //        flag = true;
                    //    }
                    //}
                    //if ($scope.filteredTodos.length == 0) {
                    //    flag = true;
                    //}
                    // if (flag == true) {

                        data.opr = "I";
                        datasend.push(data);

                        $http.post(ENV.apiUrl + "api/grd/GradeScaleeCUD", datasend).then(function (msg) {
                            $scope.msg1 = msg.data;

                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                                $scope.getGrid();
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });
                        datasend = [];
                        $scope.table = true;
                        $scope.display = false;
                    //}
                }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
               

            }

            $scope.edit = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                debugger;
                //$scope.getcur();
                $scope.readonly = true;
                $scope.disabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.curname = true;
                $scope.acdyear = true;
                $scope.edt.sims_mark_assesment_grade_name = str.sims_mark_assesment_grade_name;
                $scope.edt.sims_academic_year = str.sims_academic_year;
                $scope.edt.sims_cur_code = str.sims_cur_code;
                $scope.edt.sims_mark_assesment_grade_code = str.sims_mark_assesment_grade_code;
                //, sims_location_desc: str.sims_location_desc
                //, sims_location_status: str.sims_location_status
            }
                };
                
             

            var dataupdate = [];
            $scope.update = function (Myform) {
                debugger;
                if (Myform) {
                    
                    var Udata = $scope.edt;
                    Udata.opr = "U";
                    dataupdate.push(Udata);

                    $http.post(ENV.apiUrl + "api/grd/GradeScaleeCUD", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                            $scope.getGrid();
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $scope.getGrid();
                    });
                    dataupdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_mark_assesment_grade_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_mark_assesment_grade_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    var deletefin = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_mark_assesment_grade_code + i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodulecode = ({
                                'sims_mark_assesment_grade_code': $scope.filteredTodos[i].sims_mark_assesment_grade_code,
                                opr: 'D'
                            });
                            deletefin.push(deletemodulecode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $http.post(ENV.apiUrl + "api/grd/GradeScaleeCUD", deletefin).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getGrid();
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                                $scope.currentPage = true;
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {

                                                $scope.getGrid();
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                                $scope.currentPage = true;
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById($scope.filteredTodos[i].sims_mark_assesment_grade_code + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    $scope.currentPage = str;
                }

            }
        }])

})();
