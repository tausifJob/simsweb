﻿(function () {
    'use strict';
    var del = [];
    var main, temp, opr, date1, date3;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ComplaintMaintenanceCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.acadyr_data = [];
            $scope.edit_code = false;
            $scope.dis_no = true;
            $scope.grid = true;
            $scope.display = false;
            var data1 = [];
            var deletecode = [];

            if ($http.defaults.headers.common['schoolId'] == 'asd') {
                $scope.showASDFlag = true;
            }

            var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.dt = {
                sims_complaint_registration_date: dateyear,
            }

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;


            $http.get(ENV.apiUrl + "api/common/AcademicYear/getAcademicStatus").then(function (res) {
                $scope.obj3 = res.data;
                console.log($scope.obj3);
            });

            $http.get(ENV.apiUrl + "api/StudentReport/getcomplaintdetails?username="+'').then(function (res) {
                $scope.grid = true;
                $scope.display = false;
                $scope.acadyr_data = res.data;
                $scope.totalItems = $scope.acadyr_data.length;
                $scope.todos = $scope.acadyr_data;
                $scope.makeTodos();
            });


            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edt =
                    {
                        sims_cur_code: str.sims_cur_code,
                        sims_academic_year: str.sims_academic_year,
                        sims_academic_year_start_date: str.sims_academic_year_start_date,
                        sims_academic_year_end_date: str.sims_academic_year_end_date,
                        sims_academic_year_status: str.sims_academic_year_status,
                        sims_academic_year_desc: str.sims_academic_year_desc,
                    }

                $scope.edit_code = true;


            }

            $scope.Save = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({
                            sims_complaint_srl_no: $scope.temp.sims_complaint_srl_no,
                            sims_complaint_subject: $scope.edt.sims_complaint_subject,
                            sims_complaint_desc: $scope.edt.sims_complaint_desc,
                            sims_complaint_status:null,
                            sims_complaint_registration_date: $scope.dt.sims_complaint_registration_date,
                            sims_complaint_register_user_code: $rootScope.globals.currentUser.username,
                            sims_complaint_assigned_to_user_code: null,
                            opr: 'I'
                        });

                        data1.push(data);

                        $http.post(ENV.apiUrl + "api/StudentReport/CUDcomplainttails?complaintcode=" + '', data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 != null || $scope.msg1 != undefined) {
                                var m = $scope.msg1.strMessage;
                                swal({ title: "Alert", text: m, width: 450, height: 200 });
                                $scope.cancel();
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $scope.currentPage = 1;
                $http.get(ENV.apiUrl + "api/common/AcademicYear/getAcademicYear").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.acadyr_data = res.data;
                    $scope.totalItems = $scope.acadyr_data.length;
                    $scope.todos = $scope.acadyr_data;
                    $scope.makeTodos();
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    var data = ({
                        sims_cur_code: $scope.edt.sims_cur_code,
                        sims_academic_year: $scope.edt.sims_academic_year,
                        sims_academic_year_start_date: $scope.edt.sims_academic_year_start_date,
                        sims_academic_year_end_date: $scope.edt.sims_academic_year_end_date,
                        sims_academic_year_status: $scope.edt.sims_academic_year_status,
                        sims_academic_year_desc: $scope.edt.sims_academic_year_desc,
                        opr: 'U'
                    });

                    data1.push(data);
                    //CUDAcademicYear
                    $http.post(ENV.apiUrl + "api/common/AcademicYear/CUDInsertAcademicYear", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Academic Year Updated Successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal({ title: "Alert", text: "Only one Academic Year can be set as current at a time.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                //swal({ title: "Alert", text: "Academic Year Not Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                    });
                }
            }

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
                $scope.edit_code = false;
                $http.get(ENV.apiUrl + "api/StudentReport/getcomplaintcode").then(function (res) {
                    $scope.com_no = res.data;
                    $scope.temp = { sims_complaint_srl_no: $scope.com_no[0].sims_complaint_srl_no };
                });
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_academic_year + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_academic_year + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_academic_year + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
                            'sims_academic_year_start_date': $scope.filteredTodos[i].sims_academic_year_start_date,
                            'sims_academic_year_end_date': $scope.filteredTodos[i].sims_academic_year_end_date,
                            'sims_academic_year_status': $scope.filteredTodos[i].sims_academic_year_status,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {


                            $http.post(ENV.apiUrl + "api/common/AcademicYear/CUDAcademicYear", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Academic Year Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                                else {
                                    swal({ text: "Record Already Mapped.Can't be Deleted", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                            });


                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                //var v = document.getElementById(i);
                                var v = document.getElementById($scope.filteredTodos[i].sims_academic_year + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }

                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }

            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.acadyr_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.acadyr_data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                return (item.sims_complaint_srl_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sagar == toSearch) ? true : false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
        }])
})();