﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    var main, del = [];
    simsController.controller('GradeLevelCont',
       ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
           $scope.operation = false;
           $scope.pagesize = '10';
           $scope.pageindex = "0";
           $scope.pager = true;
           $scope.grid = true;
           $scope.user_access = [];

           $scope.appcode = $state.current.name.split('.');
           var user = $rootScope.globals.currentUser.username;

           $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
               debugger;
               $scope.user_rights = usr_rights.data;

               for (var i = 0; i < $scope.user_rights.length; i++) {
                   if (i == 0) {
                       $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                       $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                       $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                       $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                       $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                       $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                   }
               }

               //console.log($scope.user_access);
           });

           $http.get(ENV.apiUrl + "api/CurLevelGrade/getCurLevelGrade").then(function (res) {
               $scope.obj = res.data;
               $scope.totalItems = $scope.obj.length;
               $scope.todos = $scope.obj;
             
               $scope.makeTodos();
               $scope.operation = false;
               $scope.grid = true;
           });

           $scope.currGradeLevelDisplay = function () {
               $http.get(ENV.apiUrl + "api/CurLevelGrade/getCurLevelGrade").then(function (res) {
                   $scope.obj = res.data;
                   $scope.totalItems = $scope.obj.length;
                   $scope.todos = $scope.obj;
                 
                   $scope.makeTodos();
                   $scope.operation = false;
                   $scope.grid = true;
               });
           }

           $timeout(function () {
               $("#fixTable").tableHeadFixer({ 'top': 1 });
           }, 100);

           $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

           $scope.makeTodos = function () {
               var rem = parseInt($scope.totalItems % $scope.numPerPage);
               if (rem == '0') {
                   $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
               }
               else {
                   $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
               }

               var begin = (($scope.currentPage - 1) * $scope.numPerPage);
               var end = parseInt(begin) + parseInt($scope.numPerPage);

            
               $scope.filteredTodos = $scope.todos.slice(begin, end);
           };

           $scope.searched = function (valLists, toSearch) {
               return _.filter(valLists,

               function (i) {
                   /* Search Text in all  fields */
                   return searchUtil(i, toSearch);
               });
           };

           $scope.search = function () {
               $scope.todos = $scope.searched($scope.obj, $scope.searchText);
               $scope.totalItems = $scope.todos.length;
               $scope.currentPage = '1';
               if ($scope.searchText == '') {
                   $scope.todos = $scope.obj;
               }
               $scope.makeTodos();
               main.checked = false;
               $scope.CheckAllChecked();
           }

           function searchUtil(item, toSearch) {
               /* Search Text in all 3 fields */
               return (item.level_grade_cur_short_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.level_grade_grade_code_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.level_grade_level_code_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.level_grade_grade_code_name == toSearch) ? true : false;
           }
           $scope.temp = {};
           $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
               $scope.cur_obj = res.data;
               if (res.data.length > 0) {
                   $scope.temp.sims_cur_code = res.data[0].sims_cur_code;
                   $scope.getLevelNames($scope.temp.sims_cur_code);
                   $scope.getAcdmYr($scope.temp.sims_cur_code);
               }
           });

           $scope.getLevelNames = function (cur_code) {
               $http.get(ENV.apiUrl + "api/CurLevelGrade/getCuriculumLevel?cur_code=" + cur_code).then(function (res) {
                   $scope.level_names_obj = res.data;
               });

           }

           $scope.getAcdmYr = function (cur_code) {
               $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code).then(function (res) {
                   $scope.Acdm_yr_obj = res.data;
                   if (res.data.length > 0) {
                       $scope.temp.sims_academic_year = res.data[0].sims_academic_year;
                       $scope.getGradeNames($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                   }
                   
               });
           }

           $scope.getGradeNames = function (cur_code, acdm_yr) {
               $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + cur_code + "&academic_year=" + acdm_yr).then(function (res) {
                   $scope.gradeNames_obj = res.data;
               });
           }

           $scope.size = function (str) {
               if (str == 10 || str == 20) {
                   $scope.pager = true;
               }

               else {

                   $scope.pager = false;
               }
               if (str == "*") {
                   $scope.numPerPage = $scope.obj.length;
                   $scope.filteredTodos = $scope.obj;
                   $scope.pager = false;
               }
               else {
                   $scope.pagesize = str;
                   $scope.currentPage = 1;
                   $scope.numPerPage = str;
               }
               $scope.makeTodos();

           }


           $scope.index = function (str) {
               $scope.pageindex = str;
               $scope.currentPage = str;
               $scope.makeTodos();
               main.checked = false;
               $scope.CheckAllChecked();
           }

           $scope.CheckAllChecked = function () {
               main = document.getElementById('mainchk');
               if (main.checked == true) {
                   for (var i = 0; i < $scope.filteredTodos.length; i++) {
                       var v = document.getElementById($scope.filteredTodos[i].level_grade_grade_code_name+i);
                       v.checked = true;
                       $scope.row1 = 'row_selected';
                       $('tr').addClass("row_selected");
                   }
               }
               else {

                   for (var i = 0; i < $scope.filteredTodos.length; i++) {
                       var v = document.getElementById($scope.filteredTodos[i].level_grade_grade_code_name + i);
                       v.checked = false;
                       main.checked = false;
                       $scope.row1 = '';
                   }
               }

           }

           $scope.checkonebyonedelete = function () {

               $("input[type='checkbox']").change(function (e) {
                   if ($(this).is(":checked")) {
                       $(this).closest('tr').addClass("row_selected");
                       $scope.color = '#edefef';
                   } else {
                       $(this).closest('tr').removeClass("row_selected");
                       $scope.color = '#edefef';
                   }
               });

               main = document.getElementById('mainchk');
               if (main.checked == true) {
                   main.checked = false;
                   $("input[type='checkbox']").change(function (e) {
                       if ($(this).is(":checked")) {
                           $(this).closest('tr').addClass("row_selected");
                       }
                       else {
                           $(this).closest('tr').removeClass("row_selected");
                       }
                   });
               }
           }

           $scope.OkDelete = function () {
               if ($scope.user_access.data_delete == false) {
                   swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
               }
               else {
            
               var deletefin = [];
               $scope.flag = false;
               for (var i = 0; i < $scope.filteredTodos.length; i++) {
                   var v = document.getElementById($scope.filteredTodos[i].level_grade_grade_code_name + i);
                   if (v.checked == true) {
                       $scope.flag = true;
                       var deletemodulecode = ({
                           'sims_cur_code': $scope.filteredTodos[i].level_grade_cur_code,
                           'sims_cur_level_code': $scope.filteredTodos[i].level_grade_level_code,
                           'sims_academic_year': $scope.filteredTodos[i].level_grade_academic_year,
                           'sims_grade_code': $scope.filteredTodos[i].level_grade_grade_code,
                           'opr': "D"
                       });
                       deletefin.push(deletemodulecode);
                   }
               }
               if ($scope.flag) {
                   swal({
                       title: '',
                       text: "Are you sure you want to Delete?",
                       showCloseButton: true,
                       showCancelButton: true,
                       confirmButtonText: 'Yes',
                       width: 380,
                       cancelButtonText: 'No',

                   }).then(function (isConfirm) {
                       if (isConfirm) {
                           $http.post(ENV.apiUrl + "api/CurLevelGrade/CUDCurGradeLevel", deletefin).then(function (res) {
                               $scope.msg1 = res.data;
                               if ($scope.msg1 == true) {
                                   swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                       if (isConfirm) {
                                           $scope.currGradeLevelDisplay();
                                           main = document.getElementById('mainchk');
                                           if (main.checked == true) {
                                               main.checked = false;
                                               $scope.row1 = '';
                                           }
                                           $scope.currentPage = true;
                                       }
                                   });
                               }
                               else if ($scope.msg1 == false) {
                                   swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                       if (isConfirm) {
                                           $scope.currGradeLevelDisplay();
                                           main = document.getElementById('mainchk');
                                           if (main.checked == true) {
                                               main.checked = false;
                                               $scope.row1 = '';
                                           }
                                           $scope.currentPage = true;
                                       }
                                   });
                               }
                               else {
                                   swal("Error-" + $scope.msg1)
                               }
                           });
                       }
                       else {
                           main = document.getElementById('mainchk');
                           if (main.checked == true) {
                               main.checked = false;
                           }
                           for (var i = 0; i < $scope.filteredTodos.length; i++) {
                               var v = document.getElementById($scope.filteredTodos[i].level_grade_grade_code_name + i);
                               if (v.checked == true) {
                                   v.checked = false;
                                   $('tr').removeClass("row_selected");
                               }
                           }
                       }
                   });
               }
               else {
                   swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
               }
               $scope.currentPage = str;
           }
           }

           var datasend = [];
           $scope.save = function (isvalid) {
               if (isvalid) {
                
                   var f = false;
                   for (var k = 0; k < $scope.obj.length; k++) {
                       if ($scope.obj[k].level_grade_cur_code == $scope.temp.sims_cur_code && $scope.obj[k].level_grade_academic_year == $scope.temp.sims_academic_year &&
                            $scope.obj[k].level_grade_level_code == $scope.temp.sims_cur_level_code &&
                            $scope.obj[k].level_grade_grade_code == $scope.temp.sims_grade_code) {
                           swal({ text: "Record Already Present", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                           f = true;
                           break;
                       }
                   }
                   if (!f) {
                       var data = $scope.temp;
                       data.opr = 'I';
                       datasend.push(data);

                       $http.post(ENV.apiUrl + "api/CurLevelGrade/CUDCurGradeLevel", datasend).then(function (res) {
                           $scope.msg1 = res.data;

                           if ($scope.msg1 == true) {
                               swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                           }
                           else if ($scope.msg1 == false) {
                               swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                           }
                           else {
                               swal("Error-" + $scope.msg1)
                           }
                           $scope.currGradeLevelDisplay();
                           main = document.getElementById('mainchk');
                           if (main.checked == true) {
                               main.checked = false;
                               $scope.row1 = '';
                           }
                           $scope.currentPage = true;
                       });
                       datasend = [];
                       $scope.operation = false;
                       $scope.grid = true;
                   }
                   else {
                       swal({ text: "Record Already Present", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                   }
               }
           }
           debugger;
           $scope.New = function () {
               if ($scope.user_access.data_insert == false) {
                   swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
               }
               else {

               $scope.grid = false;
               $scope.operation = true;
               $scope.temp = "";
               $scope.Myform.$setPristine();
               $scope.Myform.$setUntouched();

               $scope.temp = {};
               if ($scope.cur_obj.length > 0) {
                   $scope.temp.sims_cur_code = $scope.cur_obj[0].sims_cur_code;
                   $scope.getLevelNames($scope.temp.sims_cur_code);
                   $scope.getAcdmYr($scope.temp.sims_cur_code);
               }

               
               if ($scope.Acdm_yr_obj.length > 0) {
                   $scope.temp.sims_academic_year = $scope.Acdm_yr_obj[0].sims_academic_year;
                   $scope.getGradeNames($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
               }
           }

           
           }

           $scope.cancel = function () {
               $scope.grid = true;
               $scope.operation = false;
               $scope.temp = "";
               $scope.Myform.$setPristine();
               $scope.Myform.$setUntouched();
           }

       }]);
})();