﻿(function () {
    'use strict';
    var opr = '';
    var buildcode = [];
    var main;
    var data1 = [];

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('BuildingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';

            $scope.edt = "";
            $scope.table1 = true;

            $http.get(ENV.apiUrl + "api/building/getBuilding").then(function (getBuilding_Data) {
                $scope.Building_Data = getBuilding_Data.data;
                $scope.totalItems = $scope.Building_Data.length;
                $scope.todos = $scope.Building_Data;
                $scope.makeTodos();

            });

            $scope.operation = false;
            $scope.editmode = false;
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {
                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });


            $scope.size = function (str) {
                if (str == 10 || str == 20) {
                    $scope.pager = true;
                }

                else {

                    $scope.pager = false;
                }
                if (str == "*") {
                    $scope.numPerPage = $scope.Building_Data.length;
                    $scope.filteredTodos = $scope.Building_Data;
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                }
                $scope.makeTodos();

            }


            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }


            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    $scope.edt = "";
                    $scope.check = true;
                    opr = 'S';
                    $scope.editmode = false;
                    $scope.newmode = true;
                    $scope.readonly = false;
                    $scope.table1 = false;
                    $scope.operation = true;
                    $scope.savebtn = true;
                    $scope.updatebtn = false;
                    $scope.myForm.$setPristine();
                    $scope.myForm.$setUntouched();
                }
            }

            $scope.up = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                opr = 'U';
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.readonly = true;
                $scope.edt = "";
                $scope.edt = {
                    sims_building_code: str.sims_building_code
                           , sims_building_desc: str.sims_building_desc
                           , sims_buiding_student_capacity: str.sims_buiding_student_capacity
                };
            }
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.edt.sims_buiding_student_capacity = "";
                $scope.edt.sims_building_code = "";
                $scope.edt.sims_building_desc = "";
            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'I';
                    $scope.edt = "";
                    //$scope.exist = false;
                    //for (var i = 0; i < $scope.Building_Data.length; i++) {
                    //    if ($scope.Building_Data[i].sims_building_code == data.sims_building_code) {
                    //        $scope.exist = true;
                    //    }
                    //}
                    //if ($scope.exist) {
                    //    swal({ title: "Alert", text: "Record Already exists", width: 300, height: 200 });
                    //}
                    //else
                    {
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/building/BuildingCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            $scope.operation = false;
                            $http.get(ENV.apiUrl + "api/building/getBuilding").then(function (getBuilding_Data) {
                                $scope.Building_Data = getBuilding_Data.data;
                                $scope.totalItems = $scope.Building_Data.length;
                                $scope.todos = $scope.Building_Data;
                                $scope.makeTodos();

                                $scope.msg1 = msg.data;

                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                            });

                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                    data1 = [];
                }

            }

            $scope.Update = function (myForm) {
  
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'U';
                    $scope.edt = "";
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/building/BuildingCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;

                        $scope.operation = false;
                        $http.get(ENV.apiUrl + "api/building/getBuilding").then(function (getBuilding_Data) {
                            $scope.Building_Data = getBuilding_Data.data;
                            $scope.totalItems = $scope.Building_Data.length;
                            $scope.todos = $scope.Building_Data;
                            $scope.makeTodos();

                            if ($scope.msg1 == true) {
                                swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });
                        data1 = [];
                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                }
            
        }
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

             
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            //$scope.deleterecord = function () {
            //    var data1 = [];
            //    $scope.flag = false;
            //    for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //        var v = document.getElementById(i);
            //        if (v.checked == true) {
            //            $scope.flag = true;
            //            var deletelocacode = ({
            //                'sims_building_code': $scope.filteredTodos[i].sims_building_code,
            //                opr: 'D'
            //            });
            //            buildcode.push(deletelocacode);
            //        }
            //    }
            //    
            //    if ($scope.flag) {
            //        swal({
            //            title: '',
            //            text: "Are you sure you want to Delete?",
            //            showCloseButton: true,
            //            showCancelButton: true,
            //            confirmButtonText: 'Yes',
            //            width: 380,
            //            cancelButtonText: 'No',

            //        }).then(function (isConfirm) {
            //            if (isConfirm) {
            //                $http.post(ENV.apiUrl + "api/building/BuildingCUD", buildcode).then(function (msg) {
            //                    $scope.msg1 = msg.data;
            //                    if ($scope.msg1 == true) {
            //                        swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
            //                            if (isConfirm) {
            //                                $http.get(ENV.apiUrl + "api/building/getBuilding").then(function (getBuilding_Data) {
            //                                    $scope.Building_Data = getBuilding_Data.data;
            //                                    $scope.totalItems = $scope.Building_Data.length;
            //                                    $scope.todos = $scope.Building_Data;
            //                                    $scope.makeTodos();

            //                                });
            //                                main = document.getElementById('mainchk');
            //                                if (main.checked == true) {
            //                                    main.checked = false;
            //                                    {
            //                                        $scope.row1 = '';
            //                                    }
            //                                }
            //                            }
            //                        });
            //                    }
            //                    else {
            //                        swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
            //                            if (isConfirm) {
            //                                $http.get(ENV.apiUrl + "api/building/getBuilding").then(function (getBuilding_Data) {
            //                                    $scope.Building_Data = getBuilding_Data.data;
            //                                    $scope.totalItems = $scope.Building_Data.length;
            //                                    $scope.todos = $scope.Building_Data;
            //                                    $scope.makeTodos();
            //                                });
            //                                main = document.getElementById('mainchk');
            //                                if (main.checked == true) {
            //                                    main.checked = false;
            //                                    {
            //                                        $scope.row1 = '';
            //                                    }
            //                                }
            //                            }
            //                        });
            //                    }

            //                });
            //            }
            //            else {
            //                for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //                    var v = document.getElementById(i);
            //                    if (v.checked == true) {
            //                        v.checked = false;
            //                        main.checked = false;
            //                        $('tr').removeClass("row_selected");
            //                    }
            //                }
            //            }
            //        });
            //    }
            //    else {
            //        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
            //    }
            //    data1 = [];
            //    $scope.currentPage = true;
            //}

            $scope.deleterecord = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    $scope.flag = false;
                    var deleteleave = [];
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodulecode = ({
                                'sims_building_code': $scope.filteredTodos[i].sims_building_code,
                                opr: 'D'
                            });
                            deleteleave.push(deletemodulecode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            confirmButtonText: 'Yes',
                            showCancelButton: true,
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                $http.post(ENV.apiUrl + "api/building/BuildingCUD", deleteleave).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/building/getBuilding").then(function (getBuilding_Data) {
                                                    $scope.Building_Data = getBuilding_Data.data;
                                                    $scope.totalItems = $scope.Building_Data.length;
                                                    $scope.todos = $scope.Building_Data;
                                                    $scope.makeTodos();
                                                });
                                            }

                                            $scope.currentPage = true;
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/building/getBuilding").then(function (getBuilding_Data) {
                                                    $scope.Building_Data = getBuilding_Data.data;
                                                    $scope.totalItems = $scope.Building_Data.length;
                                                    $scope.todos = $scope.Building_Data;
                                                    $scope.makeTodos();
                                                });
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById(i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    $scope.row1 = '';
                    main.checked = false;
                    $scope.currentPage = str;
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Building_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Building_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                return (
                     item.sims_building_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_buiding_student_capacity.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_building_code == toSearch) ? true : false;


            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

        }])
})();
