﻿(function () {
    'use strict';
    var del = [];
    var oldvalue = [], appl_form_field, appl_parameter, appl_form_field_value1;
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SchoolParameterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.grid = true;
            var data1 = [];
            $scope.pagesize = "10";
            $scope.save1 = true;
            $scope.pageindex = "1";
            $scope.parameter_data = [];
            $scope.edit_code = false;
            $scope.edt = {};
            var deletefin = [];
            var id = [];
            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $http.get(ENV.apiUrl + "api/SchoolParameter/getSchoolParameter").then(function (res) {
                debugger;
                $scope.display = false;
                $scope.grid = true;
                $scope.parameter_data = res.data;
                $scope.totalItems = $scope.parameter_data.length;
                $scope.todos = $scope.parameter_data;
                $scope.makeTodos();
                for (var i = 0; i < $scope.totalItems; i++) {
                    $scope.parameter_data[i].icon = "fa fa-plus-circle";
                }
            });

            //$http.get(ENV.apiUrl + "api/common/ParameterMaster/getSchoolApplication").then(function (res) {
            //    debugger;
            //    $scope.display = false;
            //    $scope.appl_data = res.data;
            //    $scope.edt['comn_application_code'] = $scope.appl_data[0].comn_application_code;
            //    //$scope.selectApplication($scope.appl_data[0].comn_application_code);
            //    console.log("appl_name");
            //    console.log($scope.appl_name);
            //});


            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            //$http.get(ENV.apiUrl + "api/common/ApplicationMaster/getSchoolModules").then(function (res) {
            //    debugger;
            //    $scope.display = false;
            //    $scope.obj = res.data;
            //    $scope.edt['comn_appl_mod_name'] = $scope.obj[0].comn_appl_mod_code;
            //   // $scope.selectModule($scope.obj[0].comn_appl_mod_name);
            //});

           

            $scope.Getinfo1 = function (modulecode) {

                if (modulecode != undefined) {
                    $http.get(ENV.apiUrl + "api/common/ParameterMaster/getApplication?modCode=" + modulecode).then(function (res) {
                        $scope.display = true;
                        $scope.appl_data = res.data;
                    });
                }
            }

            $scope.edit = function (str) {
                debugger;
                $scope.edit_code = true;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.divcode_readonly = true;
               // $scope.id = str.id;
                // $scope.edt = str;

                $scope.edt =
                    {
                        sims_appl_parameter:str. sims_appl_parameter,
                        sims_appl_form_field_value1: str.sims_appl_form_field_value1,
                    }
                $scope.Getinfo1(str.comn_appl_mod_code);

                //$scope.oldvalue.push({appl_form_field:$scope.edt.sims_appl_form_field, appl_parameter: $scope.edt.sims_appl_parameter, appl_form_field_value1: $scope.edt.sims_appl_form_field_value1 });

            }




            $scope.New = function () {
                debugger;
                $scope.edit_code = true;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.divcode_readonly = false;
                $scope.delete1 = false;
                $scope.edt = {};
               
                $http.get(ENV.apiUrl + "api/SchoolParameter/getParameterBySchool").then(function (res) {
                    debugger
                    $scope.obj1 = res.data;
                    $scope.edt['sims_appl_parameter'] = $scope.obj1[0].sims_appl_parameter;;
                    console.log($scope.edt);
                    //$scope.selectAppField($scope.obj1[0].sims_appl_form_field);
                });

                //$scope.edt['comn_application_code'] = $scope.appl_data[0].comn_application_code;
                //$scope.edt['comn_appl_mod_name'] = $scope.obj[0].comn_appl_mod_code;
                //$scope.edt['sims_appl_form_field'] = $scope.obj1[0].sims_appl_form_field_id;
            }


            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }


            $scope.Save = function (isvalidate) {
                debugger;
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = $scope.edt;
                        data.opr= 'I';
                        data1.push(data);

                        $http.post(ENV.apiUrl + "api/SchoolParameter/CUDSchoolByParameter", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "School Added Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                        $scope.table = true;
                                        $scope.display = false;
                                    }
                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "School Already Exists. ", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                        
                                    }
                                });
                            }
                            else {
                                swal("Error-" + $scope.msg1)

                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
                data1 = [];
                
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/SchoolParameter/getSchoolParameter").then(function (res) {
                    debugger;
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.parameter_data = res.data;
                    $scope.totalItems = $scope.parameter_data.length;
                    $scope.todos = $scope.parameter_data;
                    $scope.makeTodos();
                    for (var i = 0; i < $scope.totalItems; i++) {
                        $scope.parameter_data[i].icon = "fa fa-plus-circle";
                    }
                });

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                debugger;
                if (isvalidate) {

                    // var data = $scope.edt;
                    //data.opr = "U";

                    var data = ({
                        //comn_appl_mod_code: $scope.edt.comn_appl_mod_code,
                        //comn_application_code: $scope.edt.comn_application_code,
                        //sims_appl_form_field: $scope.edt.sims_appl_form_field,
                        sims_appl_parameter: $scope.edt.sims_appl_parameter,
                        sims_appl_form_field_value1: $scope.edt.sims_appl_form_field_value1,
                        //sims_appl_form_field_value2: $scope.edt.sims_appl_form_field_value2,
                        //sims_appl_form_field_value3: $scope.edt.sims_appl_form_field_value3,
                        //sims_appl_form_field_value4: $scope.edt.sims_appl_form_field_value4,
                        //appl_form_field: $scope.edt.appl_form_field,
                        //appl_form_field_value1: $scope.edt.appl_form_field_value1,
                        //appl_parameter: $scope.edt.appl_parameter,
                        //id:$scope.id,
                        opr: 'U'
                    });
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/SchoolParameter/CUDSchoolByParameter", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "School Name Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "School Name Not Updated Successfully. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.Delete = function () {
                debugger;
                var data = $scope.del1;
                data.opr = "D";

                $http.post(ENV.apiUrl + "api/SchoolParameter/CUDSchoolByParameter", data).then(function (res) {
                    $scope.msg1 = res.data;
                    $rootScope.strMessage = $scope.msg1.strMessage;

                    $('#message').modal('show');

                    $http.get(ENV.apiUrl + "api/SchoolParameter/getSchoolParameter").then(function (res) {
                        debugger;
                        $scope.display = false;
                        $scope.grid = true;
                        $scope.parameter_data = res.data;
                        $scope.totalItems = $scope.parameter_data.length;
                        $scope.todos = $scope.parameter_data;
                        $scope.makeTodos();
                        for (var i = 0; i < $scope.totalItems; i++) {
                            $scope.parameter_data[i].icon = "fa fa-plus-circle";
                        }
                    });
                })
                // }
            }

            var dom;
            $scope.flag = true;
            $scope.icon = "fa fa-plus-circle";

            $scope.expand = function (info, $event) {

                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                        "<tr> <td class='semi-bold'>" + gettextCatalog.getString('School Name') + "</td>"+
                            "</tr>" +
                              "<tr><td>" + (info.sims_appl_form_field_value1) + "</td>" +
                            "</tr>" +

                        "</tbody>" +
                        " </table></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    info.icon = "fa fa-plus-circle";
                    $scope.flag = true;
                }
            };


            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.parameter_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.parameter_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.parameter_data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_mod_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    || item.sims_appl_parameter.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }
            //Delete

            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_appl_parameter + i );
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_appl_parameter + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }
            }


            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.Delete = function () {
                debugger;
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_appl_parameter+ i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                              'sims_appl_parameter':$scope.filteredTodos[i].sims_appl_parameter,
                            //'id': $scope.filteredTodos[i].id,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {

                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/SchoolParameter/CUDSchoolByParameter", deletefin).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_appl_parameter + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    //$scope.row1 = '';
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }
        }])
})();