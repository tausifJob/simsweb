﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('FieldDetailsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;


            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            setTimeout(function () {
                $("#account_codes").select2();
            }, 100);

            var user = $rootScope.globals.currentUser.username;
           

            setTimeout(function () {
                $("#application_code").select2();
                $("#code_application").select2();
                $("#parent_appli").select2();
                $("#parent_appli1").select2();
            }, 100);


            $http.get(ENV.apiUrl + "api/TimeLine/getApplicationCode").then(function (res1) {
                $scope.appl_code = res1.data;
            });

           

            $http.get(ENV.apiUrl + "api/TimeLine/getModuleCode").then(function (res2) {
                $scope.module_code = res2.data;
            });

           

            debugger
            $http.get(ENV.apiUrl + "api/TimeLine/getFieldDetails").then(function (res1) {
                debugger;
                $scope.fielddetail = res1.data;
                console.log("F Data: ",$scope.fielddetail);
                $scope.totalItems = $scope.fielddetail.length;
                $scope.todos = $scope.fielddetail;
                $scope.makeTodos();
            });

            $scope.getdriddata = function () {
                $http.get(ENV.apiUrl + "api/TimeLine/getFieldDetails").then(function (res1) {
                    debugger;
                    $scope.fielddetail = res1.data;
                    console.log("F Data1: ", $scope.fielddetail);
                    $scope.totalItems = $scope.fielddetail.length;
                    $scope.todos = $scope.fielddetail;
                    $scope.makeTodos();
                });
            }



            $scope.main_edit = function (str) {
                debugger
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.ldgno = false;
                $scope.table = false;

                $http.get(ENV.apiUrl + "api/TimeLine/getFieldDetails?application_code=" + str.comn_appl_code + "&mod_code=" + str.comn_appl_mod_code).then(function (res1) {
                    debugger;
                    $scope.fielddetail = res1.data;
                    $scope.totalItems = $scope.fielddetail.length;
                    $scope.todos = $scope.fielddetail;
                    $scope.makeTodos();
                });

            }


            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str;
            //    main.checked = false;
            //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}
            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.getcount;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.getcount, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.getcount;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.comn_appl_code_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.comn_appl_mod_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.cnt == toSearch) ? true : false;
            }

            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "/" + month + "/" + day;
            }


            //NEW BUTTON
            $scope.New = function () {

                $scope.disabled = false;
                $scope.gtcreadonly = true;
                $scope.readonlybankname = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.ldgno = false;
                $scope.slno = false;
                $scope.cmbstatus = true;
                $scope.txtyear = false;
                $scope.glaccno = false;
                $scope.temp = "";
                $scope.temp = {};
                $scope.gtdreadonlyBnkcode = false;
                $("#account_codes").select2("val", "null");
                $scope.temp.comn_appl_field_status = true;

            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger;
                dataforSave = [];
                if (Myform) {
                        var data = $scope.temp;
                        data.opr = 'I';
                        data.comn_creation_user = $rootScope.globals.currentUser.username;
                        dataforSave.push(data);
                        $http.post(ENV.apiUrl + "api/TimeLine/CUDFieldDetails", dataforSave).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                                //$scope.getdriddata();
                                $scope.temp = '';
                                $scope.table = true;
                                $scope.display = false;
                                $("#code_application").select2("val", "");
                                $("#parent_appli1").select2("val", "");
                            }
                            else {
                                swal({ text: "Record Not inserted", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                                $scope.getdriddata();
                            }

                          //  $scope.table = true;
                          //  $scope.display = false;

                        });
                        datasend = [];
                }
            }

            
            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $("#application_code").select2("val", "");
                $scope.edt = "";
                $scope.getcount = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }
            //DATA EDIT

            $scope.up = function (str1) {
                debugger
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.temp = {
                    sr_no:str1.sr_no,
                    comn_mod_code:str1.comn_appl_mod_code, 
                    comn_appl_code:str1.comn_appl_code, 
                    comn_appl_field_name_en:str1.comn_appl_field_name_en, 
                    comn_appl_code_parent:str1.comn_field_appl_code, 
                    comn_appl_field_desc:str1.comn_appl_field_desc, 
                    comn_show_icon_status:str1.comn_show_icon_status, 
                    comn_appl_field_status: str1.comn_appl_field_status,
                    comn_field_display_order: str1.comn_field_display_order
                }

                setTimeout(function () {
                    $("#code_application").select2("val", str1.comn_appl_code);
                }, 100);

                setTimeout(function () {
                    $("#parent_appli1").select2("val", str1.comn_field_appl_code);
                }, 100);
               
            }


            $scope.View = function (appl_code, mod_code) {
                debugger
                
                $http.get(ENV.apiUrl + "api/TimeLine/getFirstDetailsCount?application_code=" + appl_code + "&mod_code=" + mod_code).then(function (result1) {
                    $scope.getcount = result1.data


                });
            }

            $scope.Reset1 = function () {
                $("#application_code").select2("val", "");
                $scope.edt = "";
                $scope.temp = '';
                $("#code_application").select2("val", "");
                $("#parent_appli1").select2("val", "");
            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function () {
                debugger
                dataforUpdate = [];
                    var data = $scope.temp;
                    data.opr = "U";
                    data.comn_creation_user = $rootScope.globals.currentUser.username;
                    dataforUpdate.push(data);
                    //dataupdate.push(data); 

                    $http.post(ENV.apiUrl + "api/TimeLine/CUDFieldDetails", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {    
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.temp = '';
                            $scope.table = true;
                            $scope.display = false;
                            $("#code_application").select2("val", "");
                            $("#parent_appli1").select2("val", "");
                            $scope.save_btn = true;
                            $scope.Update_btn = false;
                        }
                        else {
                            swal({ text: "Record Not Updated", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }
                    });
                    data2 = [];

               
            }

            $scope.Reset = function () {
                $scope.temp = '';
                $("#code_application").select2("val", "");
                $("#parent_appli1").select2("val", "");
            }

            //DELETE RECORD
            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("bank-" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("bank-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                debugger;
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById("bank-" + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'pb_bank_code': $scope.filteredTodos[i].pb_bank_code,
                            'comp_code': $scope.filteredTodos[i].pb_comp_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/BankMaster/CUDBankMaster", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BankMaster/getAllBankMaster?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res1) {
                                                debugger;
                                                $scope.Bankmaster = res1.data;
                                                $scope.totalItems = $scope.Bankmaster.length;
                                                $scope.todos = $scope.Bankmaster;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal({ text: "Record Not Deleted", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BankMaster/getAllBankMaster?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res1) {
                                                debugger;
                                                $scope.Bankmaster = res1.data;
                                                $scope.totalItems = $scope.Bankmaster.length;
                                                $scope.todos = $scope.Bankmaster;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById("bank-" + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");

                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }


        }])

})();
