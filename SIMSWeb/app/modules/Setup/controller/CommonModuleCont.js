﻿(function () {
    'use strict';
    var Modulecode = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CommonModuleCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.display = false;
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.grid = true;
            $scope.obj4 = [];
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {
                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            
            $scope.getdata = function () {
                $http.get(ENV.apiUrl + "api/ModuleMaster/getModulesByIndex").then(function (res) {
                    $scope.obj4 = res.data;
                    $scope.totalItems = $scope.obj4.length;
                    $scope.todos = $scope.obj4;
                    $scope.searchText = '';
                    $scope.makeTodos();
                });
            }

            $scope.getdata();

            $scope.getdriddata = function () {
                $http.get(ENV.apiUrl + "api/ModuleMaster/getModulesByIndex").then(function (res) {
                    $scope.obj4 = res.data;
                    $scope.totalItems = $scope.obj4.length;
                    $scope.todos = $scope.obj4;
                    $scope.makeTodos();
                });
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }

            };

            $scope.edit = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = false;
                    $scope.update1 = true;
                    $scope.delete1 = false;
                    var data = angular.copy(str);
                    $scope.edt = data;
                    main = document.getElementById('mainchk');
                    if (main.checked == true) {
                        main.checked = false;
                    }
                    $scope.CheckAllChecked();
                }
            }

            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.obj4;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            var data1 = '';
            var DeleteData = '';
            $scope.Delete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    DeleteData = '';
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = document.getElementById($scope.filteredTodos[i].module_code);
                        if (t.checked == true) {
                            $scope.flag = true;
                            DeleteData = DeleteData + $scope.filteredTodos[i].module_code + ',';
                        }
                    }

                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                var data = { module_code: DeleteData };
                                data.opr = 'D';
                                $http.post(ENV.apiUrl + "api/ModuleMaster/CUDupdateModule", data).then(function (msg) {
                                    debugger
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                        $scope.display = false;
                                        $scope.grid = true;
                                        $scope.getdata();
                                    }
                                    else {
                                        swal("Error-" + "Record Not Deleted Successfully")
                                    }
                                });
                            }
                            else {

                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }
                                $scope.CheckAllChecked();
                            }
                        });
                    }
                    else {
                        DeleteData = '';
                        swal({ text: 'Select At Least One Record To Delete', imageUrl: "assets/img/notification-alert.png", width: 380, showCloseButton: true });
                    }
                }
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = true;
                    $scope.update1 = false;
                    $scope.delete1 = false;
                    $scope.edt = "";
                    $scope.edt = {};


                    $scope.edt = {
                        module_create_date: dd + '-' + mm + '-' + yyyy,
                    }
                    $scope.edt.module_status = true;
                    main = document.getElementById('mainchk');
                    if (main.checked == true) {
                        main.checked = false;
                    }

                    $scope.CheckAllChecked();
                }

            }
            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;

            }

            $scope.SaveData = function (isvalid) {

                if (isvalid) {
                    debugger;
                    var data = $scope.edt;
                    data.opr = 'I';
                    $http.post(ENV.apiUrl + "api/ModuleMaster/CUDupdateModule", data).then(function (msg) {
                        debugger
                        $scope.msg1 = msg.data;
                        if ($scope.msg1) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            $scope.display = false;
                            $scope.grid = true;
                            $scope.getdata();
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.Update = function () {
                
                debugger;
                var data = $scope.edt;
                data.opr = 'U';
                $http.post(ENV.apiUrl + "api/ModuleMaster/CUDupdateModule", data).then(function (msg) {
                    $scope.msg1 = msg.data;
                    // swal({ text: $scope.msg1.strMessage, width: 350, showCloseButton: true });
                    if ($scope.msg1 == true) {

                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }

                    $scope.display = false;
                    $scope.grid = true;
                    $scope.getdata();
                })
            }
            

            $scope.CheckAllChecked = function () {


                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].module_code);// + $scope.filteredTodos[i].comn_app_mode
                    if (main.checked == true) {
                        v.checked = true;

                        $('tr').addClass("row_selected");
                    }
                    else {
                        v.checked = false;
                        main.checked = false;
                        $('tr').removeClass("row_selected");

                    }
                }

            }

            $scope.checkonebyonedelete = function () {


                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
            }

            var dom;
            $scope.flag = true;

            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                    "<table class='inner-table'  width='100%' cellpadding='5' cellspacing='0' style='border-color:red;border:solid;border-width:02px'>" +
                    "<tbody>" +

                    "<tr><td class='semi-bold'>" + "NAME(Arabic)" + "</td><td class='semi-bold'>" + "NAME(French)" + "</td><td class='semi-bold'>" + "NAME(Other)" + "</td><td class='semi-bold'>" + "KEYWORD" + "</td><td class='semi-bold'>" + "VERSION" + "</td> </tr>" +
                        "<tr><td>" + (info.comn_appl_name_ar) + "</td><td>" + (info.comn_appl_name_fr) + "</td><td>" + (info.comn_appl_name_ot) + "</td><td>" + (info.comn_appl_keywords) + "</td><td>" + (info.comn_appl_version) + "</td></tr>" +

                      "<tr><td class='semi-bold'>" + "HELP URL" + "</td><td class='semi-bold'>" + "LOCATION" + "</td><td class='semi-bold'>" + "DISPALY ORDER" + "</td><td class='semi-bold'>" + "APPLICATOIN TAG" + "</td> <td class='semi-bold'colsapn='2'>" + "LOGIC" + "</td><td></td></tr>" +

                       "<tr><td>" + (info.comn_appl_help_url) + "</td><td>" + (info.comn_appl_location) + "</td><td>" + (info.comn_appl_display_order) + "</td><td>" + (info.comn_appl_tag) + "</td><td>" + (info.comn_appl_logic) + "</td><td></td></tr>" +
                    "</tbody>" +
                    " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                } else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj4, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj4;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.module_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.module_short_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.module_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            //$scope.sdate = dd + '-' + mm + '-' + yyyy;
            //$scope.edate = dd + '-' + mm + '-' + yyyy;

            debugger
            $scope.edt = {
                module_create_date: dd + '-' + mm + '-' + yyyy,
            }

        }]);
})();



