﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AlbumController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.display = false;
            $scope.create = false;
            $scope.imghide = false;
            $scope.photoShow = false;
            var arr_check = [];
            var arr_files = [];
            var arr_files1 = [];
            $scope.images = [];
            $scope.images_delete = [];
            var checkboxes = new Array();
            var comn_files = "";
            var ufilecount = 1;
            $scope.obj = [];

            $('#readyClass').removeClass('mfp-bg mfp-ready');

            $('#cmb_Group_create').multipleSelect({
                width: '100%'
            });

            $('#cmb_Classes_create').multipleSelect({
                width: '100%'
            });

            $('#cmb_Group').multipleSelect({
                width: '100%'
            });

            $('#cmb_Classes').multipleSelect({
                width: '100%'
            });

            
            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {
                var i = 0;

                var exists = "no";

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                    if ($files[i].size > 200000) {
                        $rootScope.strMessage = $files[i].name + " exceeds file size limit.";
                        $('#message').modal('show');
                    }
                    else {
                        arr_files.push($files[i].name);
                        if ($scope.images.length > 0) {
                            for (var j = 0 ; j < $scope.images.length; j++) {
                                if ($scope.images[j].name == $files[i].name) {
                                    exists = "yes";
                                    return;
                                }

                            }

                            if (exists == "no") {
                                $scope.images.push({
                                    name: $files[i].name,
                                    file: $files[i].size
                                });

                            }
                            exists = "no";
                        }
                        else {
                            $scope.images.push({
                                name: $files[i].name,
                                file: $files[i].size
                            });
                        }
                        $scope.$apply();

                        // console.log($scope.images);

                    }
                    i = i + 1;
                });
            };

            $scope.file_changed = function (element) {


            };

            $scope.CancelFileUpload = function (idx) {
                //alert(idx);
                //alert($scope.images[idx].name);
                $scope.images.splice(idx, 1);
                //console.log($scope.images.length);
                $scope.images_delete.push($scope.images[idx].name);
            };




            $scope.uploadImgClickF = function () {
                formdata = new FormData();
                $scope.uploadImgClickFShow = true;
            }

            $scope.uploadImgCancel = function () {
                $scope.uploadImgClickFShow = false;
            }

            $scope.uploadImg = function () {

                for (var j = 0 ; j < $scope.images.length; j++) {
                    comn_files = comn_files + "," + $scope.images[j].name;
                }


                var request = {
                    method: 'POST',
                    url: ENV.apiUrl + '/api/file/uploadmuti?filename=' + 'gallery' + "&location=" + "Images/PhotoGallery/" + "&value=" + comn_files + "&delimages" + $scope.images_delete,
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request).success(function (d) {

                    $scope.uploadImgClickFShow = false;

                });







            }

            $scope.uploadImg = function () {

                for (var j = 0 ; j < $scope.images.length; j++) {
                    comn_files = comn_files + "," + $scope.images[j].name;
                }

                var request = {
                    method: 'POST',
                    url: ENV.apiUrl + '/api/file/uploadmuti?filename=' + 'gallery' + "&location=" + "Images/PhotoGallery/" + "&value=" + comn_files + "&delimages" + $scope.images_delete,
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };


                $http(request).success(function (d) {
                    //alert(d);
                    //  for (var k = 0 ; k < d.length; k++) {

                    var data = {

                        opr: 'I',
                        sims_album_code: $scope.sims_album_code,
                        sims_album_photo_serial_number: '012',
                        sims_album_photo_id: '022',
                        // sims_album_photo_description: str.sims_album_photo_description,
                        sims_album_photo_sell_flag: 'N',
                        sims_album_photo_price: '0.00',
                        sims_album_photo_path_lst: d,
                        sims_album_photo_status: 'A'
                    }


                    $http.post(ENV.apiUrl + "api/common/Album/InsertPhotoInAlbumDataList", data).then(function (res) {

                        if (res.data==true) {
                            swal({ text: "Updated Successfully", imageUrl: "assets/img/check.png" });

                            $http.get(ENV.apiUrl + "api/common/Album/getPhotoInAlbum?album_name=" + $scope.sims_album_code).then(function (res) {

                                $scope.photo = res.data;
                                console.log($scope.photo);

                            });
                        }
                        else if (res.data == false) {
                            swal({ text: " Not Updated. ", imageUrl: "assets/img/close.png" });
                        }
                        else {
                            swal("Error-" + res.data)
                        }

                    });
                    // }

                    $scope.uploadImgClickFShow = false;


                });



            }





            $scope.back = function () {
                $scope.photoShow = false;

            }

            $http.get(ENV.apiUrl + "api/common/Album/getAllGroupNames").then(function (res) {

                $scope.CmbGroupNm = res.data;

                setTimeout(function () {
                    $('#cmb_Group').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });

                    $('#cmb_Group_create').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });


            debugger;
           

            $http.get(ENV.apiUrl + "api/common/Album/getAlbumInfo?album_code=").then(function (res) {

                $scope.CmbAllAlbum_details = res.data;

                console.log(res.data);

            });


            //OnAlbumClick

            $scope.OnAlbumClick = function (albumCode, albumName) {
                $scope.photoShow = true;

                $scope.albumTitle = albumName;


                $http.get(ENV.apiUrl + "api/common/Album/getPhotoInAlbum?album_name=" + albumCode).then(function (res) {

                    $scope.photoList = res.data;
                    console.log(res.data.length);

                    $scope.PhotoCount = $scope.photoList.length;


                });
            }
            $scope.CurChange = function (str) {

                $http.get(ENV.apiUrl + "api/common/Album/getClassListHtml?sims_cur_code=" + str + "&academic_year=2016").then(function (res) {

                    $scope.CmbClass = res.data;
                    console.log($scope.CmbClass);
                });

            }




            $scope.Create = function () {
                $http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (res) {

                    $scope.CmbCur = res.data;

                    if (res.data.length == 1) {
                        $scope.obj = { sims_cur_code: res.data[0].sims_cur_code };


                        $http.get(ENV.apiUrl + "api/common/Album/getClassListHtml?sims_cur_code=" + res.data[0].sims_cur_code + "&academic_year=2016").then(function (res) {

                            $scope.CmbClass = res.data;
                            console.log($scope.CmbClass);

                            setTimeout(function () {
                                $('#cmb_Classes_create').change(function () {
                                    console.log($(this).val());
                                }).multipleSelect({
                                    width: '100%'
                                });
                            }, 1000);

                        });

                    }
                });

                console.log("sd");
                //$scope.create = true;
                $('#CreateAlbumMode').modal('show');
            }


            $scope.saveAlbumCreate = function () {


                var data = $scope.obj;
                data.userGroups = $scope.obj.userGroups.toString();
                data.classes = $scope.obj.classes.toString();
                data.opr = 'I';

                $http.post(ENV.apiUrl + "api/common/Album/InsertSimAlbumDataHTML", data).then(function (res) {

                    if (res.data) {
                        swal({  text: "Album Created Successfully", imageUrl: "assets/img/check.png" });

                        try {
                            $("#cmb_Group_create").multipleSelect("uncheckAll");
                        }
                        catch (ex) {
                        }
                        $http.get(ENV.apiUrl + "api/common/Album/getAlbumInfo?album_code=").then(function (res) {

                            $scope.CmbAllAlbum_details = res.data;
                            data = [];
                            console.log(res.data);

                        });

                    }
                    else {
                        swal({ text: "Album Not Created. ", imageUrl: "assets/img/close.png" });
                    }
                    $scope.resd = res.data;
                    console.log($scope.resd);
                });

            }


            $scope.Update = function () {
                $scope.obj = [];
                $http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (res) {
                    $scope.CmbCur = res.data;
                    $http.get(ENV.apiUrl + "api/common/Album/getClassListHtml?sims_cur_code=01&academic_year=2016").then(function (res) {

                        $scope.CmbClass = res.data;
                        console.log($scope.CmbClass);

                        setTimeout(function () {
                            $('#cmb_Classes').change(function () {
                                console.log($(this).val());
                            }).multipleSelect({
                                width: '100%'
                            });
                        }, 1000);

                    });

                });

                $http.get(ENV.apiUrl + "api/common/Album/getAlbumListHtml").then(function (res) {

                    $scope.CmbAllAlbum = res.data;



                });
                //$("#cmb_Group").multipleSelect("uncheckAll");

                //$("#cmb_Classes").multipleSelect("uncheckAll");

                $('#UpdateAlbumModel').modal('show');
            }
            $scope.UpadteAlbum = function () {

                var data = $scope.obj;
                data.userGroups = $scope.obj.userGroups.toString();

                data.classes = $scope.obj.classes.toString();

                data.opr = 'U';

                $http.post(ENV.apiUrl + "api/common/Album/InsertSimAlbumDataHTML", data).then(function (res) {

                    if (res.data==true) {
                        swal({  text: "Album Updated Successfully", imageUrl: "assets/img/check.png" });
                        $http.get(ENV.apiUrl + "api/common/Album/getAlbumInfo?album_code=").then(function (res) {

                            $scope.CmbAllAlbum_details = res.data;

                            console.log(res.data);

                        });

                    }
                    else if (res.data == false) {
                        swal({ text: "Album Not Updated. ", imageUrl: "assets/img/close.png" });
                    }
                    else {
                        swal("Error-" + res.data)
                    }
                    $scope.resd = res.data;
                    console.log($scope.resd);
                });
            }

            $scope.DeleteAlbum = function () {
                var data = $scope.obj;
                data.opr = 'D';

                $http.post(ENV.apiUrl + "api/common/Album/InsertSimAlbumDataHTML", data).then(function (res) {

                    if (res.data==true) {
                        swal({  text: "Album Deleted Successfully", imageUrl: "assets/img/check.png" });
                        $http.get(ENV.apiUrl + "api/common/Album/getAlbumInfo?album_code=").then(function (res) {

                            $scope.CmbAllAlbum_details = res.data;

                            console.log(res.data);

                        });
                    }
                    else if (res.data == false) {
                        swal({ text: "Album Not Deleted. ", imageUrl: "assets/img/close.png" });
                    }
                    else {
                        swal("Error-" + res.data)
                    }
                    $scope.resd = res.data;

                    console.log($scope.resd);
                });
            }

            $scope.AlbumChange = function (sims_album_code) {
                $http.get(ENV.apiUrl + "api/common/Album/getAlbumInfo?album_code=" + sims_album_code).then(function (res) {

                    $scope.obj = res.data[0];
                    console.log($scope.obj);



                    var add = [];
                    $scope.st = $scope.obj.userGroups.substring(0, $scope.obj.userGroups.length - 1);
                    $scope.test = $scope.st.split(",");

                    for (var i = 0; i < $scope.test.length; i++) {

                        add.push($scope.test[i]);
                    }

                    try {
                        $("#cmb_Group").multipleSelect("setSelects", add);
                    }
                    catch (e) {

                    }


                    // $scope.obj.classes = $scope.obj.classes.split("/");

                    var classlst = [];
                    $scope.cls = $scope.obj.classes.substring(0, $scope.obj.classes.length - 1);
                    $scope.test = $scope.cls.split(",");

                    for (var i = 0; i < $scope.test.length; i++) {

                        classlst.push($scope.test[i]);
                    }

                    try {
                        $("#cmb_Classes").multipleSelect("setSelects", classlst);
                    }
                    catch (e) {

                    }
                });

            }




            $scope.Upload = function () {
                $http.get(ENV.apiUrl + "api/common/Album/getAlbumListHtml").then(function (res) {

                    $scope.CmbAllAlbum = res.data;



                });
                $('#UploadImageModel').modal('show');
            }

            $scope.AlbumUploadChange = function (albumCode) {

                $http.get(ENV.apiUrl + "api/common/Album/getPhotoInAlbum?album_name=" + albumCode).then(function (res) {

                    $scope.photo = res.data;
                    console.log($scope.photo);

                });
            }


            $scope.updatePhotoDetails = function (str) {

                var data = {

                    opr: 'U',
                    sims_album_code: str.sims_album_code,
                    sims_album_photo_serial_number: str.sims_album_photo_serial_number,
                    sims_album_photo_id: str.sims_album_photo_id,
                    sims_album_photo_description: str.sims_album_photo_description,
                    sims_album_photo_sell_flag: str.sims_album_photo_sell_flag,
                    sims_album_photo_price: str.sims_album_photo_price,
                    sims_album_photo_path: str.sims_album_photo_path,
                    sims_album_photo_status: str.sims_album_photo_status
                }


                $http.post(ENV.apiUrl + "api/common/Album/InsertPhotoInAlbumData", data).then(function (res) {

                    if (res.data==true) {
                        swal({  text: "Updated Successfully", imageUrl: "assets/img/check.png" });
                    }
                    else if (res.data == false) {
                        swal({ text: " Not Updated. ", imageUrl: "assets/img/close.png" });
                    }
                    else {
                        swal("Error-" + res.data)
                    }
                });
            }


            $scope.deletePhotoDetails = function () {

                var photo_id = '';
                for (var i = 0; i < $scope.photo.length; i++) {
                    if ($scope.photo[i].chkstatus) {

                        photo_id += $scope.photo[i].sims_album_photo_id + ','
                    }
                }

                var data = {

                    opr: 'D',
                    sims_album_code: $scope.photo[0].sims_album_code,
                    // sims_album_photo_serial_number: str.sims_album_photo_serial_number,
                    sims_album_photo_id: photo_id,

                }


                $http.post(ENV.apiUrl + "api/common/Album/InsertPhotoInAlbumData", data).then(function (res) {

                    if (res.data==true) {
                        swal({  text: "Deleted Successfully", imageUrl: "assets/img/check.png" });
                    }
                    else if (res.data == false) {
                        swal({ text: " Not Deleted. ", imageUrl: "assets/img/close.png" });
                    }
                    else {
                        swal("Error-" + res.data)
                    }
                });
            }



            $scope.imageClose = function () {
                $('#readyClass').removeClass('mfp-bg mfp-ready');
                $scope.imghide = false;
            }
            $scope.viewimg = function (str, index) {
                $scope.showIndex = index + 1;

                $scope.ImgIndex = index;


                $scope.imgsrc = str[index].sims_album_photo_path;
                $('#readyClass').addClass('mfp-bg mfp-ready');
                $scope.imghide = true;



                // $timeout(function () { $('.superbox').SuperBox(); }, 100)
            }

            $scope.nextimg = function () {

                if ($scope.photoList.length - 1 == $scope.ImgIndex) {
                    $scope.ImgIndex = 0;
                    $scope.showIndex = 1;
                }
                else {
                    $scope.ImgIndex = $scope.ImgIndex + 1;
                    $scope.showIndex = $scope.ImgIndex + 1
                }
                $scope.imgsrc = $scope.photoList[$scope.ImgIndex].sims_album_photo_path;


            }

            $scope.Previmg = function () {

                if ($scope.ImgIndex == 0) {
                    $scope.ImgIndex = $scope.photoList.length - 1;
                    $scope.showIndex = $scope.photoList.length;
                }
                else {
                    $scope.ImgIndex = $scope.ImgIndex - 1;
                    $scope.showIndex = $scope.ImgIndex + 1;
                }
                $scope.imgsrc = $scope.photoList[$scope.ImgIndex].sims_album_photo_path;


            }









            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


        }]);

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])


})();


