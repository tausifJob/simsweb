﻿(function () {
    'use strict';
    var obj1;
    var temp, opr = "";
    var obj2;
    var temp;
    var main, del = [];
    var data1 = [];
    var simsController = angular.module('sims.module.Setup');

    simsController.controller('NationalityController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'filterFilter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog,
            $http, filterFilter, ENV, $filter) {
            $scope.dis1 = true;
            $scope.cmsl = true;
            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.msg = false; $scope.operation = false; $scope.table = true;
            $scope.Sbtn = false; $scope.Nbtn = true; $scope.update = false; $scope.Update_btn = false;

            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });
            //$scope.roles.comn_role_name = 'myRole';
            //$http.get(ENV.apiUrl + "api/common/Nationality/getAllSimsNationality").then(function (res1) {
            //    $scope.display = true;
            //    $scope.dis1 = true;
            //    $scope.obj1 = res1.data;
            //    $scope.totalItems = $scope.obj1.length;
            //    console.log($scope.totalItems);
            //    $scope.todos = $scope.obj1;
            //    console.log($scope.totalItems);
            //    $scope.makeTodos();
            //});

            $http.get(ENV.apiUrl + "api/common/Nationality/getAllCountries").then(function (res) {
                $scope.country = res.data;
            })
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);
            $http.get(ENV.apiUrl + "api/common/Nationality/getAllCountries").then(function (res1) {
                $scope.obj2 = res1.data;
             
            });

            $http.get(ENV.apiUrl + "api/common/Nationality/getAllNationalityDetails").then(function (SGD_Data) {
                $scope.SGDExamData = SGD_Data.data;
                $scope.totalItems = $scope.SGDExamData.length;
                $scope.todos = $scope.SGDExamData;
                $scope.makeTodos();
               
            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

              
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.check_once = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }

            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.SGDExamData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.SGDExamData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
              
                /* Search Text in all 3 fields */
                return (item.sims_country_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.nationality_name_eng.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.nationality_name_ar.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.nationality_name_fr.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.nationality_name_ot.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.nationality_code == toSearch) ? true : false;
            }

            $scope.update_click = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                
                $scope.operation = true;
                $scope.table = false;
                $scope.Update_btn = true;
                $scope.Save_btn = false;
                $scope.readonly = true;
                $scope.temp = {
                    nationality_code: str.nationality_code
                        , sims_country_code: str.sims_country_code
                        , sims_country_name_en: str.sims_country_name_en
                        , nationality_name_eng: str.nationality_name_eng
                        , nationality_name_ar: str.nationality_name_ar
                        , nationality_name_fr: str.nationality_name_fr
                        , nationality_name_ot: str.nationality_name_ot
                        , nationality_status: str.nationality_status



                };
                  }
            }

            $scope.size = function (str) {
                if (str == 10 || str == 20) {
                    $scope.pager = true;
                }

                else {

                    $scope.pager = false;
                }
                if (str == "*") {
                    $scope.numPerPage = $scope.SGDExamData.length;
                    $scope.filteredTodos = $scope.SGDExamData;
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                }
                $scope.makeTodos();

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.check_all();
            }
            $scope.New = function (str) {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.temp = "";
                    $scope.opr = 'I';
                    $scope.operation = true;
                    $scope.table = false;
                    $scope.Update_btn = false;
                    $scope.Save_btn = true;
                    $scope.readonly = false;
                    $scope.Myform.$setPristine();
                    $scope.Myform.$setUntouched();
                    $scope.temp = {};
                    $scope.temp.nationality_status = true;
                }
            }

            $scope.can = function () {
                $scope.operation = false;
                $scope.table = true;
                $scope.temp = "";
                $scope.opr = "";
            }

            $scope.Save = function (Myform) {
                if (Myform) {
                    var data = $scope.temp;
                    data.opr = 'I';
                    $scope.exist = false;
                    data1.push(data);
                    for (var i = 0; i < $scope.SGDExamData.length; i++) {
                        if ($scope.SGDExamData[i].nationality_code == data.nationality_code && $scope.SGDExamData[i].sims_country_code == data.sims_country_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ text: "Record Already exists", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                    else {

                        $http.post(ENV.apiUrl + "api/common/Nationality/NationalityCUD?simsobj=", data1).then(function (msg) {

                            $scope.ScheduleGroupDetailOperation = false;
                            $http.get(ENV.apiUrl + "api/common/Nationality/getAllNationalityDetails").then(function (SGD_Data) {
                                $scope.SGDData = SGD_Data.data;
                                $scope.totalItems = $scope.SGDData.length;
                                $scope.todos = $scope.SGDData;
                                $scope.makeTodos();
                                $scope.msg1 = msg.data;

                                if ($scope.msg1 == true) {

                                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });

                                    $http.get(ENV.apiUrl + "api/common/Nationality/getAllNationalityDetails").then(function (SGD_Data) {
                                        $scope.SGDExamData = SGD_Data.data;
                                        $scope.totalItems = $scope.SGDExamData.length;
                                        $scope.todos = $scope.SGDExamData;
                                        $scope.makeTodos();
                                      
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                            });

                        });

                        $scope.table = true;
                        $scope.operation = false;
                    }
                    data1 = [];
                }
            }

            $scope.Update = function () {
                var data = $scope.temp;
                data.opr = 'U';
                $scope.temp = "";
                data1.push(data);
                $http.post(ENV.apiUrl + "api/common/Nationality/NationalityCUD", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    $scope.ScheduleGroupDetailOperation = false;
                    //$http.get(ENV.apiUrl + "api/common/Nationality/getAllNationalityDetails").then(function (SGD_Data) {
                    //    $scope.SGDData = SGD_Data.data;
                    //    $scope.totalItems = $scope.SGDData.length;
                    //    $scope.todos = $scope.SGDData;

                    //    $scope.makeTodos();

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });

                        $http.get(ENV.apiUrl + "api/common/Nationality/getAllNationalityDetails").then(function (SGD_Data) {
                            $scope.SGDExamData = SGD_Data.data;
                            $scope.totalItems = $scope.SGDExamData.length;
                            $scope.todos = $scope.SGDExamData;
                            $scope.makeTodos();
                            
                        });

                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                    //  });

                })
                $scope.table = true;
                $scope.operation = false;
                data1 = [];
            
        }


            $scope.Delete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    var data1 = [];
                    $scope.flag = false;
                    var deleteleave = [];
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodulecode = ({
                                'nationality_code': $scope.filteredTodos[i].nationality_code,
                                opr: 'D'
                            });
                            deleteleave.push(deletemodulecode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                $http.post(ENV.apiUrl + "api/common/Nationality/NationalityCUD", deleteleave).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/common/Nationality/getAllNationalityDetails").then(function (getLeaveDetail_data) {
                                                    $scope.LeaveData = getLeaveDetail_data.data;
                                                    $scope.totalItems = $scope.LeaveData.length;
                                                    $scope.todos = $scope.LeaveData;
                                                    $scope.makeTodos();
                                                });
                                            }
                                            $scope.currentPage = true;
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/common/Nationality/getAllNationalityDetails").then(function (getLeaveDetail_data) {
                                                    $scope.LeaveData = getLeaveDetail_data.data;
                                                    $scope.totalItems = $scope.LeaveData.length;
                                                    $scope.todos = $scope.LeaveData;
                                                    $scope.makeTodos();
                                                });
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-"+$scope.msg1)
                                    }

                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById("test-" + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        $scope.row1 = '';
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    $scope.row1 = '';
                    main.checked = false;
                    $scope.currentPage = str;
                }
            }

            //sort
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };
        }])
})();
