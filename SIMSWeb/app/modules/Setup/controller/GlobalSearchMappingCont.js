﻿(function () {
    'use strict';
    var appcode = [];
    var getdata = [];
    var senddata = [], demo = [], demo1 = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('GlobalSearchMappingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.busyindicator = true;

          

      //      $scope.Show_Data = function () {

                $scope.busyindicator = false;
                $scope.info1 = [];
                var demo = [];
                var demo1 = [];
                $scope.table = false;
                $scope.subject = false;

                $http.get(ENV.apiUrl + "api/GlobalSearchMapping/getRoleName").then(function (allSectionStudent) {

                    $scope.SectionStudent = allSectionStudent.data;
                    $scope.busyindicator = true;
                    if (allSectionStudent.data.length > 0) {
                        for (var j = 0; j < $scope.SectionStudent[0].tablist.length; j++) {
                            demo = {
                                'sims_subject_name_en': $scope.SectionStudent[0].tablist[j].comn_user_tab_name,
                                'sims_subject_code': $scope.SectionStudent[0].tablist[j].comn_user_tab_no
                            }
                            demo1.push(demo);
                        }
                        $timeout(function () {
                            $("#fixTable").tableHeadFixer({ "left": 2 });
                        }, 100);
                        //$timeout(function () {
                        //       $("#fixTable").tableHeadFixer({ "left": 3 });
                        //       //  $("#fixTable").tableHeadFixer({ 'top': 1 });

                        //   }, 100);

                        $scope.info1 = demo1;
                        $scope.table = true;
                        console.log(demo1);
                    }
                    else {

                        $scope.subject = true;

                    }
                });
          //  }

            var subject1;
            $scope.Check_Single_Subject = function (enroll) {
                enroll.ischange = true;
            }

            $scope.Select_ALL_Subject = function (str) {
                var id = '';
                var check = document.getElementById(str);
                for (var i = 0; i < $scope.SectionStudent.length; i++) {
                    $scope.SectionStudent[i].tablist[check.tabIndex].comn_status = check.checked;
                    $scope.SectionStudent[i].ischange = true;
                }
            }

            $scope.Select_Student_ALL_Subject = function (str) {
                var id = '';
                var check = document.getElementById(str);
                for (var i = 0; i < $scope.SectionStudent.length; i++) {
                    if ($scope.SectionStudent[i].comn_role_code == str) {
                        for (var j = 0; j < $scope.SectionStudent[i].tablist.length; j++) {
                            $scope.SectionStudent[i].tablist[j].comn_status = check.checked;
                            $scope.SectionStudent[i].ischange = true;
                        }
                    }
                }
            }

            $scope.Submit = function () {

                $scope.busyindicator = false;
                $scope.subject = false;
                getdata = [];
                var Sdata = [{}];
                for (var i = 0; i < $scope.SectionStudent.length; i++) {

                    if ($scope.SectionStudent[i].ischange == true) {
                        var le = Sdata.length;
                        Sdata[le] = {
                            'comn_role_code': $scope.SectionStudent[i].comn_role_code,
                            //'sims_cur_code': $scope.SectionStudent[i].sims_cur_code,
                            //'sims_academic_year': $scope.SectionStudent[i].sims_academic_year,
                            //'sims_grade_code': $scope.SectionStudent[i].sims_grade_code,
                            //'sims_section_code': $scope.SectionStudent[i].sims_section_code,
                            //'sims_subject_name_en': ''
                        };
                        var sbcode = '';
                        for (var j = 0; j < $scope.SectionStudent[i].tablist.length; j++) {

                            if ($scope.SectionStudent[i].tablist[j].comn_status) {
                                sbcode = sbcode + $scope.SectionStudent[i].tablist[j].comn_user_tab_no + ',';
                            }
                        }

                        Sdata[le].comn_user_tab_name = sbcode;
                    }
                }

                Sdata.splice(0, 1);
                var data = Sdata;
                if (Sdata.length != 0) {
                    $http.post(ENV.apiUrl + "api/GlobalSearchMapping/insert_student_section_subject", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        swal({ text: 'Application Mapped Successfully', imageUrl: "assets/img/check.png", width: 380 });
                        $http.get(ENV.apiUrl + "api/GlobalSearchMapping/getRoleName").then(function (allSectionStudent) {
                            $scope.SectionStudent = allSectionStudent.data;
                            $scope.busyindicator = true;
                            if (allSectionStudent.data.length > 0) {
                                //for (var j = 0; j < $scope.SectionStudent[0].tablist.length; j++) {
                                //    demo = {
                                //        'sims_subject_name_en': $scope.SectionStudent[0].tablist[j].comn_user_tab_name,
                                //        'sims_subject_code': $scope.SectionStudent[0].tablist[j].comn_user_tab_no
                                //    }
                                //    demo1.push(demo);
                                //}
                                $scope.info1 = demo1;
                                $scope.table = true;

                            }
                            else {

                                $scope.subject = true;

                            }
                        });
                    });
                    senddata = [];
                    $scope.table = true;
                    $scope.busyindicator = true;

                    subject1.checked = false;
                }
                else {

                    swal({ text: 'Application Not Mapped', imageUrl: "assets/img/close.png", width: 380 });
                }
            }


        }]);
})();



