﻿(function () {
    'use strict';
    var regioncode = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ComnMasterStoredProcedureCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.critobj = [];
            var data1 = [];
            var deletecode = [];

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            debugger;
            $http.get(ENV.apiUrl + "api/common/MasterStoredProcedureList/getStoredProcedureList").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.critobj = res.data;
                console.log("SP DATA:",$scope.critobj);
                $scope.totalItems = $scope.critobj.length;
                $scope.todos = $scope.critobj;
                $scope.makeTodos();

            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $http.get(ENV.apiUrl + "api/common/MasterStoredProcedureList/getSPType").then(function (res) {
                $scope.display = false;
                $scope.SP_type = res.data;
                console.log($scope.SP_type);
            });

            $scope.New = function () {
                var autoid;
                $scope.display = true;
                $scope.grid = false;
                $scope.btn_save1 = true;
                $scope.btn_update1 = false;
                $scope.delete1 = false;
                $scope.edt = [];

                $http.get(ENV.apiUrl + "api/common/MasterStoredProcedureList/getStoredProcedureList").then(function (res) {
                    autoid = res.data;
                    // var v = document.getElementById('txt_criteria_code');
                    v.value = $scope.autoid[0].sp_code;
                    $scope.edt['sp_code'] = autoid;
                });
            }

            debugger;
            $scope.edit = function (str) {
                debugger;
                $scope.display = true;
                $scope.grid = false;
                $scope.btn_save1 = false;
                $scope.btn_update1 = true;
                $scope.delete1 = false;
                console.log(str);
                //$scope.edt = str;
                $scope.edt =
                    {
                        sp_code: str.sp_code,
                        sp_name: str.sp_name,
                        sp_appl_code: str.sp_appl_code,
                        sp_type: str.sp_type,
                        sp_read: str.sp_read,
                        sp_insert: str.sp_insert,
                        sp_update: str.sp_update,
                        sp_delete: str.sp_delete,
                        sp_remark: str.sp_remark,
                    }
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];

                if (isvalidate) {
                    if ($scope.btn_save1 == true) {
                        var data = ({
                            sp_code: $scope.edt.sp_code,
                            sp_name: $scope.edt.sp_name,
                            sp_type: $scope.edt.sp_type,
                            sp_appl_code: $scope.edt.sp_appl_code,
                            sp_read: $scope.edt.sp_read,
                            sp_insert: $scope.edt.sp_insert,
                            sp_update: $scope.edt.sp_update,
                            sp_delete: $scope.edt.sp_delete,
                            sp_remark: $scope.edt.sp_remark,
                            opr: 'I'
                        });
                       
                        data1.push(data);

                        $http.post(ENV.apiUrl + "api/common/MasterStoredProcedureList/CUDInsertStoredProcedure", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });

                            }
                            else {
                                swal({ text: "Record Not Inserted", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $scope.currentPage = 1;
                $http.get(ENV.apiUrl + "api/common/MasterStoredProcedureList/getStoredProcedureList").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.critobj = res.data;
                    $scope.totalItems = $scope.critobj.length;
                    $scope.todos = $scope.critobj;
                    $scope.makeTodos();
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

            }

            $scope.Update = function (isvalidate) {
                debugger;

                var data1 = [];

                if (isvalidate) {
                    var data = ({
                        sp_code: $scope.edt.sp_code,
                        sp_name: $scope.edt.sp_name,
                        sp_type: $scope.edt.sp_type,
                        sp_appl_code: $scope.edt.sp_appl_code,
                        sp_read: $scope.edt.sp_read,
                        sp_insert: $scope.edt.sp_insert,
                        sp_update: $scope.edt.sp_update,
                        sp_delete: $scope.edt.sp_delete,
                        sp_remark: $scope.edt.sp_remark,
                        opr: 'U'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/MasterStoredProcedureList/CUDInsertStoredProcedure", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });

                        }
                        else {
                            swal({ text: "Record Not Updated", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                    });
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        // var v = document.getElementById(i);
                        var t = $scope.filteredTodos[i].sp_code;
                        var v = document.getElementById(t+i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        // var v = document.getElementById(i);
                        var t = $scope.filteredTodos[i].sp_code;
                        var v = document.getElementById(t+i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++)
                {
                    var t = $scope.filteredTodos[i].sp_code;
                    var v = document.getElementById(t+i);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sp_code': $scope.filteredTodos[i].sp_code,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/common/MasterStoredProcedureList/CUDInsertStoredProcedure", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });

                                }
                                else {
                                    swal({ text: "Record Not Deleted.", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var t = $scope.filteredTodos[i].sp_code;
                                var v = document.getElementById(t+i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.critobj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.critobj;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sp_code+i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }
           
            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sp_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sp_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sp_appl_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sp_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            var dom;
            var count = 0;

            $scope.expand = function (info, $event) {
                console.log(info);
                $(dom).remove();

                dom = $("<tr><td class='details' colspan='11'>" +
                    "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                    "<tbody>" +
                     "<tr><td class='semi-bold'>&nbsp;</td><td class='semi-bold'></td><td class='semi-bold'></td> <td class='semi-bold'>" + gettextCatalog.getString('French Short Name') + "</td> <td class='semi-bold'>" + gettextCatalog.getString('Regional Short Name') + " </td><td class='semi-bold'>" + gettextCatalog.getString('French Full Name') + "</td><td class='semi-bold'>" + gettextCatalog.getString('Regional Full Name') + "</td>" +
                    "</tr>" +

                      "<tr><td class='semi-bold'>&nbsp;</td><td class='semi-bold'>&nbsp;</td><td class='semi-bold'>&nbsp;</td><td>" + (info.curriculum_short_name_fr) + "</td> <td>" + (info.curriculum_short_name_ot) + " </td><td>" + (info.curriculum_full_name_fr) + "</td><td>" + (info.curriculum_full_name_ot) + "</td>" +
                    "</tr>" +

                    " </table></td></tr>")

                $($event.currentTarget).parents("tr").after(dom);


            };

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

        }])
})();