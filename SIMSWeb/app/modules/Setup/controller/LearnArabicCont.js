﻿(function () {
    'use strict';
    var obj1, obj2, temp, main;
    var opr = '';
    var portalcode = [];

    var simsController = angular.module('sims.module.Setup');

    simsController.controller('LearnArabicCont',
         ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
             $scope.pagesize = '5';
             var formdata = new FormData();
             var data1 = [];
             $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

             $scope.size = function (str) {
                 console.log(str);
                 $scope.pagesize = str;
                 $scope.currentPage = 1;
                 $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
             }

             $scope.index = function (str) {
                 $scope.pageindex = str;
                 $scope.currentPage = str;
                 console.log("currentPage=" + $scope.currentPage);
                 $scope.makeTodos();
                 main.checked = false;
                 $scope.row1 = '';
             }

             $http.get(ENV.apiUrl + "api/portalrefer/getLearnArabic").then(function (res) {
                 $scope.learnarabic = res.data;
                 $scope.totalItems = $scope.learnarabic.length;
                 $scope.todos = $scope.learnarabic;
                 $scope.makeTodos();
             })
             
             $scope.downloaddoc = function (str) {
                 debugger;
                 $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Docs/PortalReference/' + str;
                // $scope.url = ENV.apiUrl + 'Content/' +'sis'+ '/UploadedFiles/' + str;
               //  C:\Users\Ganesh\Desktop\SimsErpProc\SIMSAPI\Content\sis\UploadedFiles\
                 window.open($scope.url);
             }

            

             $scope.makeTodos = function () {
                 var rem = parseInt($scope.totalItems % $scope.numPerPage);
                 if (rem == '0') {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                 }
                 else {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                 }
                 var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                 var end = parseInt(begin) + parseInt($scope.numPerPage);

                 console.log("begin=" + begin); console.log("end=" + end);

                 $scope.filteredTodos = $scope.todos.slice(begin, end);
             };

             $scope.getTheFiles = function ($files) {
                 $scope.filesize = true;

                 angular.forEach($files, function (value, key) {
                     formdata.append(key, value);

                     var i = 0;
                     if ($files[i].size > 800000) {
                         $scope.filesize = false;
                         $scope.edt.photoStatus = false;

                         swal({ text: "File Should Not Exceed 800Kb.", imageUrl: "assets/img/notification-alert.png" });

                     }
                     else {




                     }

                 });
             };

             $scope.file_changed = function (element, str) {

                 var photofile = element.files[0];

                 $scope.photo_filename = (photofile.name);

                 $scope.edt1 = { sims_timetable_filename: $scope.photo_filename }
                 $.extend($scope.edt, $scope.edt1)
                 $scope.photo_filename = (photofile.type);
                 var reader = new FileReader();
                 reader.onload = function (e) {
                     $scope.$apply(function () {
                         $scope.prev_img = e.target.result;

                     });
                 };
                 reader.readAsDataURL(photofile);

                 if (element.files[0].size < 200000) {

                 }
             };

             $(function () {

                 $('table tbody tr:even').addClass('even_row');
                 $("#all_chk").change(function () {
                     $('input:checkbox').prop('checked', this.checked);
                     $('tr').toggleClass("selected_row", this.checked)
                 });

                 $('table tbody :checkbox').change(function (event) {
                     $(this).closest('tr').toggleClass("selected_row", this.checked);
                 });
                 $('table tbody tr').click(function (event) {
                     if (event.target.type !== 'checkbox') {
                         $(':checkbox', this).trigger('click');
                     }
                     $("input[type='checkbox']").not('#all_chk').change(function (e) {
                         if ($(this).is(":checked")) {
                             $(this).closest('tr').addClass("selected_row");
                         } else {
                             $(this).closest('tr').removeClass("selected_row");
                         }
                     });
                 });

             });

             $timeout(function () {
                 $("#fixTable").tableHeadFixer({ 'top': 1 });
             }, 100);

             $timeout(function () {
                 $("#fixTable1").tableHeadFixer({ 'top': 1 });

             }, 100);
             
             $scope.searched = function (valLists, toSearch) {
                 return _.filter(valLists, function (i) {
                     /* Search Text in all  fields */
                     return searchUtil(i, toSearch);
                 });
             };

             $scope.search = function () {
                 $scope.todos = $scope.searched($scope.learnarabic, $scope.searchText);
                 $scope.totalItems = $scope.todos.length;
                 $scope.currentPage = '1';
                 if ($scope.searchText == '') {
                     $scope.todos = $scope.learnarabic;
                 }
                 $scope.makeTodos();
             }

             function searchUtil(item, toSearch) {
                 /* Search Text in all 3 fields */
                 return (item.sims_reference_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_reference_filename.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_reference_publish_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_reference_number == toSearch) ? true : false;
             }


         }])
})();
