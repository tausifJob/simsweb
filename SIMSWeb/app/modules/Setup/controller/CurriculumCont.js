﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CurriculumCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.curriculum_data = [];
            $scope.edit_code = false;
            var data1 = [];
            var deletecode = [];
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });


            $timeout(function () {
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $http.get(ENV.apiUrl + "api/common/Curriculum/getCurriculum").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.curriculum_data = res.data;
                $scope.totalItems = $scope.curriculum_data.length;
                $scope.todos = $scope.curriculum_data;
                $scope.makeTodos();

                for (var i = 0; i < $scope.totalItems; i++) {
                    $scope.curriculum_data[i].icon = "fa fa-plus-circle";
                }
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edit = function (str) {

                if ($scope.user_access.data_update==false)
                {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else
                {
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = false;
                    $scope.update1 = true;
                    $scope.delete1 = false;
                    $scope.edt =
                        {
                            curriculum_code: str.curriculum_code,
                            curriculum_short_name: str.curriculum_short_name,
                            curriculum_short_name_ar: str.curriculum_short_name_ar,
                            curriculum_short_name_fr: str.curriculum_short_name_fr,
                            curriculum_short_name_ot: str.curriculum_short_name_ot,
                            curriculum_full_name: str.curriculum_full_name,
                            curriculum_full_name_ar: str.curriculum_full_name_ar,
                            curriculum_full_name_fr: str.curriculum_full_name_fr,
                            curriculum_full_name_ot: str.curriculum_full_name_ot,
                            curriculum_status: str.curriculum_status,
                        }
                    $scope.edit_code = true;
                }
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({
                            curriculum_code: $scope.edt.curriculum_code,
                            curriculum_short_name: $scope.edt.curriculum_short_name,
                            curriculum_short_name_ar: $scope.edt.curriculum_short_name_ar,
                            curriculum_short_name_fr: $scope.edt.curriculum_short_name_fr,
                            curriculum_short_name_ot: $scope.edt.curriculum_short_name_ot,
                            curriculum_full_name: $scope.edt.curriculum_full_name,
                            curriculum_full_name_ar: $scope.edt.curriculum_full_name_ar,
                            curriculum_full_name_fr: $scope.edt.curriculum_full_name_fr,
                            curriculum_full_name_ot: $scope.edt.curriculum_full_name_ot,
                            curriculum_status: $scope.edt.curriculum_status,
                            opr: 'I'
                        });

                        data1.push(data);

                        $http.post(ENV.apiUrl + "api/common/Curriculum/CUDInsertCurriculum", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Curriculum  Added Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Curriculum Record Already Exists. ", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/common/Curriculum/getCurriculum").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.curriculum_data = res.data;
                    $scope.totalItems = $scope.curriculum_data.length;
                    $scope.todos = $scope.curriculum_data;
                    $scope.makeTodos();

                    for (var i = 0; i < $scope.totalItems; i++) {
                        $scope.curriculum_data[i].icon = "fa fa-plus-circle";
                    }

                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    var data = ({
                        curriculum_code: $scope.edt.curriculum_code,
                        curriculum_short_name: $scope.edt.curriculum_short_name,
                        curriculum_short_name_ar: $scope.edt.curriculum_short_name_ar,
                        curriculum_short_name_fr: $scope.edt.curriculum_short_name_fr,
                        curriculum_short_name_ot: $scope.edt.curriculum_short_name_ot,
                        curriculum_full_name: $scope.edt.curriculum_full_name,
                        curriculum_full_name_ar: $scope.edt.curriculum_full_name_ar,
                        curriculum_full_name_fr: $scope.edt.curriculum_full_name_fr,
                        curriculum_full_name_ot: $scope.edt.curriculum_full_name_ot,
                        curriculum_status: $scope.edt.curriculum_status,
                        opr: 'U'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/Curriculum/CUDCurriculum", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Curriculum Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    //, imageUrl: "assets/img/check.png", 
                                    $scope.getgrid();

                                }
                            });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Curriculum  Not Updated Successfully. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                // imageUrl: "assets/img/notification-alert.png",
                                if (isConfirm) {
                                    //, imageUrl: "assets/img/check.png", 
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.New = function () {

                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = true;
                    $scope.update1 = false;
                    $scope.delete1 = false;
                    $scope.edt = "";
                    $scope.edit_code = false;
                    $scope.edt =
                        {
                            curriculum_status: true
                        }
                }
            }

            $scope.cancel = function () {
                $scope.edt = "";
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Delete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    var deletecode = [];
                    $scope.flag = false;

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].curriculum_code;
                        var v = document.getElementById(t);

                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodercode = ({
                                'curriculum_code': $scope.filteredTodos[i].curriculum_code,
                                'opr': 'D'
                            });
                            deletecode.push(deletemodercode);
                        }
                    }

                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {


                                $http.post(ENV.apiUrl + "api/common/Curriculum/CUDCurriculum", deletecode).then(function (res) {
                                    $scope.msg1 = res.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Curriculum  Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            //, imageUrl: "assets/img/check.png",
                                            if (isConfirm) {
                                                //, imageUrl: "assets/img/check.png", 
                                                $scope.getgrid();
                                            }

                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Curriculum  Not Deleted Successfully. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            //, imageUrl: "assets/img/notification-alert.png",
                                            if (isConfirm) {
                                                //, imageUrl: "assets/img/check.png", 
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                            else {
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }

                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var t = $scope.filteredTodos[i].curriculum_code;
                                    var v = document.getElementById(t);

                                    if (v.checked == true) {
                                        v.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                }

            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        // var v = document.getElementById(i);
                        var t = $scope.filteredTodos[i].curriculum_code;
                        var v = document.getElementById(t);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].curriculum_code;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        //$scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        //$scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    //main.checked = false;
                    //$scope.color = '#edefef';
                    //$scope.row1 = '';

                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
                //else
                //{
                //    $scope.color = '#fff';
                //    $scope.row1 = '';
                //}
            }


            $scope.size = function (str) {
                //if (str == 10 || str == 20) {
                //    $scope.pager = true;
                //}

                //else {

                //    $scope.pager = false;
                //}
                //if (str == "*") {
                //    $scope.numPerPage = $scope.curriculum_data.length;
                //    $scope.filteredTodos = $scope.curriculum_data;
                //}
                //else {
                //    $scope.pagesize = str;
                //    $scope.currentPage = 1;
                //    $scope.numPerPage = str;
                //}
                //$scope.makeTodos();

                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.curriculum_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();

            }


            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            var dom;
            var count = 0;

            $scope.flag = true;
            $scope.icon = "fa fa-plus-circle";

            $scope.expand = function (info, $event) {
              
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                    "<table class='inner-table table table-hover table-bordered table-condensed' cellpadding='5' cellspacing='0'>" +
                    "<tbody>" +
                     "<tr style='background-color: #e9e4e4'> <td class='semi-bold'><i class='fa fa-align-justify'></i>&nbsp;" + gettextCatalog.getString('French Short Name') + "</td> <td class='semi-bold'><i class='fa fa-align-justify'></i>&nbsp;" + gettextCatalog.getString('Regional Short Name') + " </td><td class='semi-bold'><i class='fa fa-align-justify'></i>&nbsp;" + gettextCatalog.getString('French Full Name') + "</td><td class='semi-bold'><i class='fa fa-align-justify'></i>&nbsp;" + gettextCatalog.getString('Regional Full Name') + "</td>" +
                    "</tr>" +

                      "<tr><td>" + (info.curriculum_short_name_fr) + "</td> <td>" + (info.curriculum_short_name_ot) + " </td><td>" + (info.curriculum_full_name_fr) + "</td><td>" + (info.curriculum_full_name_ot) + "</td>" +
                    "</tr>" +

                    " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);

                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    info.icon = "fa fa-plus-circle";
                    $scope.flag = true;
                }
            };

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.curriculum_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.curriculum_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].curriculum_code;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }

                }


            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.curriculum_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.curriculum_short_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  || item.curriculum_full_name == toSearch) ? true : false;
            }

        }])


})();