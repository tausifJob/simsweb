﻿

(function () {
    'use strict';
    var Student_bacth_code = [], selected_enroll_number = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('HomeroomStudentCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.grid = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.enrollno = true;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $scope.enroll_number = [];

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear($scope.edt.sims_cur_code);
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                    $scope.GetHomeroom();
                });
            }

            $scope.GetHomeroom = function (x) {
                $http.get(ENV.apiUrl + "api/homeroom/gethomeroomall?cur_code=" + $scope.edt.sims_cur_code + "&a_year=" + $scope.temp.sims_academic_year + "&user_code=" + $rootScope.globals.currentUser.username).then(function (homeroomcode) {
                    $scope.homeroom_code = homeroomcode.data;
                    setTimeout(function () {
                        $('#cmb_homeroom_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $(function () {
                $('#cmb_homeroom_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.GetHomeroombatch = function (x, y) {
                $http.get(ENV.apiUrl + "api/homeroom/getAllhomeroombatch?cur_code=" + $scope.edt.sims_cur_code + "&a_year=" + $scope.temp.sims_academic_year + "&homeroom_code=" + $scope.edt.sims_homeroom_code).then(function (homeroombatch) {
                    debugger;
                    $scope.homeroom_batch = homeroombatch.data;
                    setTimeout(function () {
                        $('#cmb_homeroombatch_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $(function () {
                $('#cmb_homeroombatch_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.GetHomeroomdetails = function (x, y) {
                debugger;
                $http.get(ENV.apiUrl + "api/homeroom/getHomeroomBatchStudent?cur_code=" + $scope.edt.sims_cur_code + "&a_year=" + $scope.temp.sims_academic_year + "&homeroom_code=" + $scope.edt.sims_homeroom_code + "&homeroom_batch=" + $scope.edt.sims_batch_code).then(function (res) {
                    if (res.data !== null) {
                        $scope.obj = res.data;
                        $scope.totalItems = $scope.obj.length;
                        $scope.todos = $scope.obj;
                        $scope.makeTodos();
                    }
                    else {
                        swal({ text: 'Sorry Data Not Found', width: 380 });
                    }
                });
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);



                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $http.get(ENV.apiUrl + "api/homeroom/GetAllHomeroomName").then(function (AllHomeroomName) {
                $scope.Homeroomnames = AllHomeroomName.data;

            });

            $http.get(ENV.apiUrl + "api/homeroom/getAllHomeroomBatchName").then(function (AllHomeroomBatchName) {
                $scope.AllHomeroom_BatchName = AllHomeroomBatchName.data;

            });

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;

            });

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;

            });

            $scope.getacyr = function (str) {

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                })
            }

            $scope.edit = function (str) {
                $scope.studentsearch = false;

                $scope.enrollno = false;
                $scope.enrollno_name = true;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edt = str;
                $scope.getacyr(str.sims_cur_name);
                $scope.enrollno = false;
                $scope.ClearCheckBox();
                $scope.enroll_number = [];
            }

            $scope.New = function () {
                $scope.studentsearch = true;

                $scope.enrollno = false;
                $scope.enrollno_name = false;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
                $scope.ClearCheckBox();
                $scope.enroll_number = [];

            }

            $scope.Delete = function () {

                Student_bacth_code = '';
                $scope.enroll_number = [];
                var flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_batch_enroll_number;
                    var v = document.getElementById(t);

                    if (v.checked == true)
                        flag = true;
                    Student_bacth_code = Student_bacth_code + $scope.filteredTodos[i].sims_batch_enroll_number + ',';
                }

                var deletebacthcode = ({
                    'sims_batch_enroll_number': Student_bacth_code,
                    'opr': 'D'
                });
                if (flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/homeroom/UHomeroomBatchStudent", deletebacthcode).then(function (MSG1) {

                                $scope.msg = MSG1.data;

                                $scope.grid = true;
                                $scope.display = false;
                                if ($scope.msg == true) {

                                    swal({ text: 'Student Homeroom Deleted Successfully', imageUrl: "assets/img/check.png", width: 380 });

                                    $http.get(ENV.apiUrl + "api/homeroom/getHomeroomBatchStudent").then(function (res) {
                                        if (res.data !== null) {
                                            $scope.obj = res.data;
                                            $scope.row1 = '';
                                            $scope.totalItems = $scope.obj.length;
                                            $scope.todos = $scope.obj;
                                            $scope.currentPage = 1;
                                            $scope.makeTodos();
                                        }
                                        else {
                                            swal({ text: 'Sorry Data Not Found', width: 380 });
                                        }
                                    });
                                }
                                else if ($scope.msg == false) {
                                    swal({ text: 'Student Homeroom Not Deleted. ', imageUrl: "assets/img/close.png", width: 380 });
                                    main = document.getElementById('mainchk');
                                    if (main.checked == true) {
                                        main.checked = false;
                                    }
                                    $scope.CheckAllChecked();
                                }
                                else {
                                    swal("Error-" + $scope.msg)
                                }
                            });


                        }
                        else {

                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            $scope.CheckAllChecked();
                        }
                    })
                }
                else {

                    swal({ text: 'Select At Least One Recoed To Delete', imageUrl: "assets/img/notification-alert.png" });

                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_batch_enroll_number;
                    var v = document.getElementById(t);
                    Student_bacth_code = [];
                    if (main.checked == true) {

                        v.checked = true;
                        Student_bacth_code = Student_bacth_code + $scope.filteredTodos[i].sims_batch_enroll_number + ',';
                        $('tr').addClass("row_selected");
                    }
                    else {
                        v.checked = false;
                        main.checked == false;
                        $('tr').removeClass("row_selected");
                        Student_bacth_code = [];

                    }
                }
            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }
            }

            $scope.cancel = function () {

                $scope.display = false;
                $scope.grid = true;


            }

            var sims_student_enrolls = [];

            $scope.Save = function (isvalid) {

                if (isvalid) {

                    if ($scope.enroll_number.length > 0) {

                        for (var i = 0; i < $scope.enroll_number.length; i++) {

                            sims_student_enrolls = sims_student_enrolls + $scope.enroll_number[i].s_enroll_no + ','
                        }

                        $scope.edt.sims_student_passport_first_name_en = sims_student_enrolls;
                        var data = {

                            sims_cur_name: $scope.edt.sims_cur_name,
                            sims_academic_year: $scope.edt.sims_academic_year,
                            sims_batch_enroll_number: $scope.edt.sims_student_passport_first_name_en,
                            sims_batch_name: $scope.edt.sims_batch_code,
                            sims_batch_status: $scope.edt.sims_batch_status,
                            sims_homeroom_code: $scope.edt.sims_homeroom_code,
                            opr: 'I'
                        }
                        $http.post(ENV.apiUrl + "api/homeroom/CHomeroomBatchStudent", data).then(function (MSG1) {


                            $scope.msg = MSG1.data;

                            $scope.grid = true;
                            $scope.display = false;
                            if ($scope.msg == true) {

                                swal({ text: 'Student Homeroom Created Successfully', imageUrl: "assets/img/check.png", width: 380 });
                                $scope.checkonebyonedelete();

                            }
                            else {

                                swal({ text: 'Student Homeroom Not Created. ', imageUrl: "assets/img/close.png", width: 380 });

                            }

                            $http.get(ENV.apiUrl + "api/homeroom/getHomeroomBatchStudent").then(function (res) {
                                if (res.data !== null) {
                                    $scope.obj = res.data;
                                    $scope.totalItems = $scope.obj.length;
                                    $scope.todos = $scope.obj;
                                    $scope.makeTodos();
                                }
                                else {

                                    swal('', 'Sorry Data Not Found');

                                }

                            })
                        });
                        $scope.enroll_number = [];
                        sims_student_enrolls = [];
                        data = '';
                    }
                    else {

                        swal({ text: 'Select Atleast One Student To Insert Record', imageUrl: "assets/img/notification-alert.png", width: 380 });
                    }
                }
            }

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.ClearCheckBox();
                $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.ClearCheckBox();
                $scope.makeTodos();
            }

            $scope.Update = function () {

                $scope.enroll_number = [];

                var enroll_no = '';

                for (var i = 0; i < $scope.obj.length; i++) {

                    if ($scope.obj[i].sims_student_passport_first_name_en == $scope.edt.sims_student_passport_first_name_en) {

                        enroll_no = $scope.obj[i].sims_batch_enroll_number;
                    }
                }

                var data = {
                    sims_cur_name: $scope.edt.sims_cur_name,
                    sims_academic_year: $scope.edt.sims_academic_year,
                    sims_batch_enroll_number: enroll_no,
                    sims_batch_name: $scope.edt.sims_batch_code,
                    sims_batch_status: $scope.edt.sims_batch_status,
                    sims_homeroom_code: $scope.edt.sims_homeroom_code,
                    opr: 'U'
                }

                $http.post(ENV.apiUrl + "api/homeroom/UHomeroomBatchStudent", data).then(function (MSG1) {

                    $scope.msg = MSG1.data;

                    $scope.grid = true;
                    $scope.display = false;
                    if ($scope.msg == true) {

                        swal({ text: 'Information Updated Successfully', imageUrl: "assets/img/check.png", width: 380 });
                        $scope.checkonebyonedelete();

                    }
                    else if ($scope.msg == false) {

                        swal({ text: 'Information Not Updated. ', imageUrl: "assets/img/close.png", width: 380 });

                    }
                    else {
                        swal("Error-" + $scope.msg)
                    }

                    $http.get(ENV.apiUrl + "api/homeroom/getHomeroomBatchStudent").then(function (res) {
                        if (res.data !== null) {
                            $scope.obj = res.data;
                            $scope.totalItems = $scope.obj.length;
                            $scope.todos = $scope.obj;
                            $scope.makeTodos();
                        }
                        else {

                            swal({ text: 'Sorry Data Not Found', width: 380 });

                        }

                    })


                });
            }

            $scope.SearchStudentWindow = function () {
                $scope.searchtable = false;
                $scope.student = '';
                $('#MyModal').modal('show');
                $scope.temp = '';
                $("#MyModal").modal({}).draggable();

            }

            $scope.SearchSudent = function () {

                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    main.checked = false;
                }

                $scope.enrollno = true;
                $scope.searchtable = false;
                $scope.busy = true;
                $scope.sibling_result



                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.temp)).then(function (Allstudent) {

                    $scope.student = Allstudent.data;
                    $scope.searchtable = true;
                    $scope.busy = false;


                });
            }

            $scope.searched = function (valLists, toSearch) {



                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_student_passport_first_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.Reset = function () {
                $scope.temp = '';
            }

            $scope.ClearCheckBox = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;

                }
                $scope.CheckAllChecked();
            }

            $scope.RemoveEnrollMentNo = function ($event, index, str) {

                str.splice(index, 1);

            }

            $scope.MultipleStudentSelect = function () {

                $scope.enroll_number = [];
                main = document.getElementById('mainchk1');
                for (var i = 0; i < $scope.student.length; i++) {
                    var t = $scope.student[i].s_enroll_no;
                    var v = document.getElementById(t + i);
                    if (main.checked == true) {

                        v.checked = true;

                        $scope.enroll_number.push($scope.student[i]);
                    }
                    else {
                        v.checked = false;
                        $scope.enroll_number = [];
                    }
                }
            }


            $scope.DataEnroll = function () {
                for (var i = 0; i < $scope.student.length; i++) {
                    var t = $scope.student[i].s_enroll_no;
                    var v = document.getElementById(t + i);
                    if (v.checked == true)

                        $scope.enroll_number.push($scope.student[i]);
                }
                $scope.student = [];

            }

            $(document).keydown(function (e) {
                // ESCAPE key pressed
                if (e.keyCode == 27) {
                    $('#MyModal').modal('hide');
                }
            });

            $(document).keydown(function (e) {
                // ESCAPE key pressed

            });



        }])
})();