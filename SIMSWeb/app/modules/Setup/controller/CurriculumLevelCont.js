﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    var main, del = [];
    simsController.controller('CurriculumLevelController',
       ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', 'filterFilter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout,
           gettextCatalog, filterFilter, $http, ENV, $filter) {
           $scope.display = false;
           $scope.pagesize = '10';
           $scope.pageindex = "0";
           $scope.pager = true;

           $scope.levelcodeRonly = false;
           $http.get(ENV.apiUrl + "api/common/CurLevel/getCurlevel").then(function (res) {
               $scope.display = false;
               $scope.grid = true;
               $scope.user_access = [];

               $scope.appcode = $state.current.name.split('.');
               var user = $rootScope.globals.currentUser.username;

               $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                   debugger;
                   $scope.user_rights = usr_rights.data;

                   for (var i = 0; i < $scope.user_rights.length; i++) {
                       if (i == 0) {
                           $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                           $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                           $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                           $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                           $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                           $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                       }
                   }

                   //console.log($scope.user_access);
               });
               $scope.obj = res.data;
              
               $scope.totalItems = $scope.obj.length;
               $scope.todos = $scope.obj;
              
               $scope.makeTodos();
           });
           $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

           $scope.makeTodos = function () {

               var rem = parseInt($scope.totalItems % $scope.numPerPage);
               if (rem == '0') {
                   $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
               }
               else {
                   $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
               }


               var begin = (($scope.currentPage - 1) * $scope.numPerPage);
               var end = parseInt(begin) + parseInt($scope.numPerPage);

               $scope.filteredTodos = $scope.todos.slice(begin, end);
           };
           debugger;
           $scope.temp = {};
           $http.get(ENV.apiUrl + "api/common/CurLevel/getCuriculum").then(function (res) {
               $scope.cur = res.data;
               if (res.data.length > 0) {
                   $scope.temp.level_cur_code = res.data[0].level_cur_code;
               }
           });

           $scope.CheckAll = function () {
               debugger;
               main = document.getElementById('chk_min');
               del = [];
               if (main.checked == true) {
                   // $scope.obj1 = res.data;
                   for (var i = 0; i < $scope.filteredTodos.length; i++) {
                       var t = $scope.filteredTodos[i].level_code + $scope.filteredTodos[i].level_cur_code_name;
                       var v = document.getElementById(t);
                       v.checked = true;
                       //  
                       // $scope.deleteobj[i].nationality_code = t;
                       del.push(t);
                   }
               }
               else {
                   // $scope.obj1 = res.data;
                   for (var i = 0; i < $scope.filteredTodos.length; i++) {
                       var t = $scope.filteredTodos[i].level_code + $scope.filteredTodos[i].level_cur_code_name;
                       var v = document.getElementById(t);
                       v.checked = false;
                   }
               }
              
           }

           $scope.CheckAny = function (level_code) {
               debugger;
               var v = document.getElementById(level_code);
               if (v.checked == true) {
                 
                   del.push(level_code);
                  
               }
               else {
                
                   v.checked = false;

                   var index = del.indexOf(level_code);
                 
                   if (index > -1) {
                       del.splice(index, 1);
                   }
                   //del.pop({id:sims_concession_number});
                   
               }

           }

           $scope.size = function (str) {
               if (str == 10 || str == 20) {
                   $scope.pager = true;
               }

               else {

                   $scope.pager = false;
               }
               if (str == "*") {
                   $scope.numPerPage = $scope.obj.length;
                   $scope.filteredTodos = $scope.obj;
                   $scope.pager = false;
               }
               else {
                   $scope.pagesize = str;
                   $scope.currentPage = 1;
                   $scope.numPerPage = str;
               }
               $scope.makeTodos();

           }


           $scope.index = function (str) {
               // $scope.pageindex = str;
               $scope.pageindex = str;
               //   main.checked = false;
               $scope.currentPage = str;  $scope.makeTodos();
           }

           $scope.searched = function (valLists, toSearch) {
               // 
               return _.filter(valLists,

               function (i) {
                   /* Search Text in all  fields */
                   return searchUtil(i, toSearch);
               });
           };

           $scope.search = function () {
               $scope.todos = $scope.searched($scope.obj, $scope.searchText);
               $scope.totalItems = $scope.todos.length;
               $scope.currentPage = '1';
               if ($scope.searchText == '') {
                   $scope.todos = $scope.obj;
               }
               $scope.makeTodos();
           }

           function searchUtil(item, toSearch) {
               /* Search Text in all 3 fields */
               return (item.level_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.level_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                  item.level_cur_code_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.level_cur_code_name == toSearch) ? true : false;
           }

           $scope.edit = function (str) {
               if ($scope.user_access.data_update == false) {
                   swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
               }
               else {
               $scope.levelcodeRonly = true;
               $scope.display = true; $scope.update_flag = true;
               $scope.grid = false; $scope.btn_save = false; $scope.btn_update = true;
               // $scope.temp = str;
               $scope.temp = {
                   level_cur_code: str.level_cur_code
                              , sims_academic_year: str.sims_academic_year
                              , level_code: str.level_code
                              , level_name_en: str.level_name_en
                              , level_name_fr: str.level_name_fr
                              , level_name_ar: str.level_name_ar
                              , level_name_ot: str.level_name_ot
                              , level_status: str.level_status

               };

           }
          }

           $scope.Save = function (isvalid) {
               if (isvalid) {
                   if ($scope.update_flag == true) {
                       $scope.Update();
                   }
                   else {
                       var data = $scope.temp;
                       data.opr = 'I';
                       $http.post(ENV.apiUrl + "api/common/CurLevel/CUDCurLevel", data).then(function (res) {
                           $scope.msg = res.data;
                         
                           if ($scope.msg.strMessage == '0' || $scope.msg.strMessage == '3') {
                               $('#CurLevelCodeError').modal({ backdrop: "static" });
                           }
                           else {
                               $('#SucessMessage').modal({ backdrop: "static" });
                               $http.get(ENV.apiUrl + "api/common/CurLevel/getCurlevel").then(function (res) {
                                   $scope.display = false;
                                   $scope.grid = true;
                                   $scope.obj = res.data;
                                   
                                   $scope.totalItems = $scope.obj.length;
                                   $scope.todos = $scope.obj;
                                  
                                   $scope.makeTodos();
                               }, function () {
                                   $scope.loading = true;
                                   alert("Failed to get Daata");
                               });
                               $scope.msg = true; $scope.dis2 = false; $scope.dis1 = true;
                           }
                       }, function () {
                           $scope.show_ins_table_loading = false;
                           $('#ErrorMessage').modal({ backdrop: "static" });
                       });
                   }
               }
           }

           $scope.Update = function () {
               var data = $scope.temp;
               data.opr = 'U';
               $http.post(ENV.apiUrl + "api/common/CurLevel/CUDCurLevel", data).then(function (res) {
                   $scope.up = res.data;
                   $('#Success_Update').modal({ backdrop: "static" });
                   $http.get(ENV.apiUrl + "api/common/CurLevel/getCurlevel").then(function (res) {
                       $scope.display = false;
                       $scope.grid = true;
                       $scope.obj = res.data;
                     
                       $scope.totalItems = $scope.obj.length;
                       $scope.todos = $scope.obj;
                       
                       $scope.makeTodos();
                   }, function () {
                       $scope.loading = true;
                       alert("Failed to get Daata");
                   });
                   $scope.msg = true; $scope.dis2 = false; $scope.dis1 = true;

               }, function () {
                   $scope.show_ins_table_loading = false;
                   $('#ErrorMessage').modal({ backdrop: "static" });
               });
           }

           $scope.Delete = function () {
               if ($scope.user_access.data_delete == false) {
                   swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
               }
               else {

               if (del.length != 0) {
                   $http.post(ENV.apiUrl + "api/common/CurLevel/DCurLevel?objlist=" + JSON.stringify(del)).then(function (res) {
                       $scope.ins = res.data;
                       del = [];
                       if ($scope.ins.strMessage == "1") {
                           $('#Success_Delete').modal({ backdrop: "static" });

                           $http.get(ENV.apiUrl + "api/common/CurLevel/getCurlevel").then(function (res) {
                               $scope.display = false;
                               $scope.grid = true;
                               $scope.obj = res.data;
                              
                               $scope.totalItems = $scope.obj.length;
                               $scope.todos = $scope.obj;
                             
                               $scope.makeTodos();
                           }, function () {
                               $scope.loading = true;
                               alert("Failed to get Daata");
                           });
                       }
                       else {
                           $('#Error_Delete').modal({ backdrop: "static" });
                       }
                       $scope.msg = true; $scope.dis2 = false; $scope.dis1 = true;
                       // console.log($scope.sectionsobj);
                   }, function () {
                       $scope.show_ins_table_loading = false;
                       $('#ErrorMessage').modal({ backdrop: "static" });
                   });
               }
               else {
                   alert("Select Record");
               }
           }
           }

           $scope.New = function () {
               if ($scope.user_access.data_insert == false) {
                   swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
               }
               else {
               $scope.temp = "";
               $scope.levelcodeRonly = false;
               $scope.grid = false; $scope.update_flag = false;
               $scope.display = true; $scope.btn_save = true; $scope.btn_update = false;

               $scope.temp = {};
               $scope.temp.level_status = true;
               if ($scope.cur.length > 0) {
                   $scope.temp.level_cur_code = $scope.cur[0].level_cur_code;
               }
           }
           }

           $scope.cancel = function () {
               $scope.grid = true;
               $scope.display = false;
               $scope.update1 = false;
               $scope.save1 = false;
               $scope.Myform.$setPristine();
               $scope.Myform.$setUntouched();
           }


           $('*[data-datepicker="true"] input[type="text"]').datepicker({
               todayBtn: true,
               orientation: "top left",
               autoclose: true,
               todayHighlight: true
           });

           $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
               $('input[type="text"]', $(this).parent()).focus();
           });



       }]);
})();