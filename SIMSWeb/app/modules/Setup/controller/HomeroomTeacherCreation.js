﻿(function () {
    'use strict';
    var appcode = [];
    var getdata = [];
    var senddata = [], demo = [], demo1 = [];
    var main;
    var simsController = angular.module('sims.module.Setup');
    simsController.controller('HomeRoomTeacherCreationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.edt = {};
            $scope.temp = {};
            $scope.selectedStudentList = [];
            var user = $rootScope.globals.currentUser.username;
            $scope.busyindicator = true;
            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.grade = [];
            $scope.section1 = [];
            $scope.gradeSectionArray = [];
            $scope.SectionArray = [];
            $scope.cmb_subject_name = [];
            $scope.postData = [];
            $scope.homeroomStudents = [];
            $scope.disabledSubmit = true;
            $scope.showSectionMsg = false;
            $scope.showGradeMsg = false;
            $scope.showEditField = false;
            $scope.edt.homeroomStatus = true;
            $scope.showSubjectMsg = false;
            $scope.gradeSection = [];
            $scope.tempArray = [];
            $scope.edt.sims_grade_section = [];
            $scope.SectionStudent = [];
            $scope.showDeleteBtn = false;
            $scope.showTeacherDropdown = false;
            var HomeroomEmployeeCode = '';
           
            // Change the selector if needed
            var $table = $('table.scroll'),
                $bodyCells = $table.find('tbody tr:first').children(),
                colWidth;

            // Adjust the width of thead cells when window resizes
            $(window).resize(function () {
                // Get the tbody columns width array
                colWidth = $bodyCells.map(function () {
                    return $(this).width();
                }).get();

                // Set the width of thead columns
                $table.find('thead tr').children().each(function (i, v) {
                    $(v).width(colWidth[i]);
                });
            }).resize(); // Trigger resize handler

            $(function () {
                $('#cmb_grade').multipleSelect({
                    width: '100%'
                });
            });

            $(function () {
                $('#cmb_section_code').multipleSelect({
                    width: '100%'
                });
            });

            $(function () {
                $('#gradesection').multipleSelect({
                    width: '100%'
                });              
            });
            $(function () {
                $('#gradesec').multipleSelect({
                    width: '100%'
                });
            });
            
            $scope.getUserRole = function () {
                $http.get(ENV.apiUrl + "api/HomeroomCreation/GetUserRole?username=" + user).then(function (userRole) {
                    if (userRole.data.length > 0) {
                        $scope.userStatus = userRole.data[0].user_status;
                    }
                    if ($scope.userStatus == "Y") {
                        $scope.showTeacherDropdown = true;
                    }
                    console.log("userStatus",$scope.userStatus);
                });
            }
            $scope.getUserRole();

            $scope.getTeacher = function () {

                var sims_grade_section = "";
                for (var i = 0; i < $scope.edt.sims_grade_section.length; i++) {
                    sims_grade_section = sims_grade_section + $scope.edt.sims_grade_section[i] + ',';
                }

                $http.get(ENV.apiUrl + "api/HomeroomCreation/GetTeacher?academicyear=" + $scope.edt.sims_academic_year + "&subject_code=" + $scope.edt.sims_subject_code + "&grade_section=" + sims_grade_section).then(function (res) {
                    $scope.getTeacherData = res.data;                    
                });
            }
           /// $scope.getTeacher();

            $scope.setTeacherCode = function () {
                HomeroomEmployeeCode = $scope.edt.sims_teacher_code;

                $http.get(ENV.apiUrl + "api/HomeroomCreation/getSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&academicyear=" + $scope.edt.sims_academic_year + "&login_user=" + $scope.edt.sims_teacher_code).then(function (sub) {                   
                    if (sub.data.length > 0) {
                        $scope.showSubjectMsg = false;
                        $scope.disabledSubmit = false;
                    }
                    else {
                        //$scope.showSubjectMsg = true;
                        $scope.disabledSubmit = true;
                        swal({ text: 'Selected subject not assigned for this teacher\n Please assign subject to create homeroom', width: 400, showCloseButton: true });
                    }
                });
            }




            $scope.getCurriculum = function(){
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                    $scope.curriculum = res.data;
                    $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                    $scope.temp['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                    $scope.getacyr($scope.curriculum[0].sims_cur_code);
                });
            }
            $scope.getCurriculum();

            $scope.getacyr = function (str) {
                $scope.cur_code = str;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    $scope.edt['sims_academic_year'] = $scope.Academic_year[0].sims_academic_year;
                    $scope.temp['sims_academic_year'] = $scope.Academic_year[0].sims_academic_year;
                    //$scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                    // $scope.getsections(str, $scope.Academic_year[0].sims_academic_year);
                    //$scope.getGradeSection(str, $scope.Academic_year[0].sims_academic_year);
                    $scope.getSubject(str, $scope.Academic_year[0].sims_academic_year);
                    $scope.getSubjectAbqis(str, $scope.Academic_year[0].sims_academic_year);
                })
            }

            $scope.getSubject = function (sims_cur_code, sims_academic_year) {
                //$scope.cmb_subject_name = [];
                //var sims_grade_section = "";
                //for (var i = 0; i < $scope.edt.sims_grade_section.length; i++) {
                //    sims_grade_section = sims_grade_section + $scope.edt.sims_grade_section[i] + ',';
                //}

                $http.get(ENV.apiUrl + "api/HomeroomCreation/getSectionsubject_name?curcode=" + sims_cur_code + "&academicyear=" + sims_academic_year + "&login_user=" + $rootScope.globals.currentUser.username).then(function (getSectionsubject_name) {
                    $scope.cmb_subject_name = getSectionsubject_name.data;

                    if ($scope.cmb_subject_name.length > 0) {
                        //$scope.getGradeSection(sims_cur_code, sims_academic_year, $scope.cmb_subject_name[0].sims_subject_code);
                        $scope.showSubjectMsg = false;
                    }
                    else {
                        $scope.showSubjectMsg = true;
                    }

                });

                //$http.get(ENV.apiUrl + "api/HomeroomCreation/getSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + sims_grade_section + "&login_user=" + user).then(function (getSectionsubject_name) {
                //    $scope.cmb_subject_name = getSectionsubject_name.data;                                       
                //});
            }


            $scope.getSubjectAbqis = function (sims_cur_code,sims_academic_year) {
               

                $http.get(ENV.apiUrl + "api/HomeroomCreation/getSectionsubject_name_abqis?sims_cur_code=" + sims_cur_code + "&sims_academic_year=" + sims_academic_year).then(function (res) {
                    $scope.sub_name = res.data;
                   
                    if ($scope.sub_name.length > 0) {
                        //$scope.getGradeSection(sims_cur_code, sims_academic_year, $scope.cmb_subject_name[0].sims_subject_code);
                        $scope.showSubjectMsg = false;
                    }
                    else {
                        $scope.showSubjectMsg = true;
                    }
                  
                });

                //$http.get(ENV.apiUrl + "api/HomeroomCreation/getSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + sims_grade_section + "&login_user=" + user).then(function (getSectionsubject_name) {
                //    $scope.cmb_subject_name = getSectionsubject_name.data;                                       
                //});
            }



            $scope.getGradeSectionAbqis = function (str1, str2, str3) {
                debugger
                $http.get(ENV.apiUrl + "api/HomeroomCreation/getgradesection_abqis?sims_cur_code=" + str1 + "&sims_academic_year=" + str2 + "&HomeroomSubject=" + str3).then(function (res) {
                    $scope.gradedata = res.data;
                    setTimeout(function () {
                        $('#gradesection').change(function () {
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                    setTimeout(function () {
                        $('#gradesec').change(function () {
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }
          

            // For Grade and Section

           
          $scope.getGradeSection = function (str, str1, str2) {
                debugger
                $http.get(ENV.apiUrl + "api/HomeroomCreation/getGradeSection?sims_cur_code=" + str + "&academic_year=" + str1 + "&subject_code=" + str2 + "&teachercode=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.gradeSection = res.data;
                    setTimeout(function () {
                        $('#gradesection').change(function () {
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                    setTimeout(function () {
                        $('#gradesec').change(function () {
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }

            //$scope.getsections = function (str, str1) {
            //    $http.get(ENV.apiUrl + "api/HomeroomCreation/GetRCGrade?curcode=" + str + "&ayear=" + str1 + "&teachercode=" + user).then(function (res) {
            //        $scope.grade = res.data;
            //        setTimeout(function () {
            //            $('#cmb_grade').change(function () {
            //            }).multipleSelect({
            //                width: '100%'
            //            });
            //        }, 1000);
            //    })
            //};

            //$scope.getsection = function (str, str1, str2) {

            //    var sims_grade_code = "";
            //    for (var i = 0; i < $scope.edt.sims_grade_code.length; i++) {
            //        sims_grade_code = sims_grade_code + $scope.edt.sims_grade_code[i] + ',';
            //    }

            //    $http.get(ENV.apiUrl + "api/HomeroomCreation/GetRCSection?curcode=" + str + "&ayear=" + str1 + "&gradecode=" + sims_grade_code + "&teachercode=" + user).then(function (Allsection) {
            //        $scope.section1 = Allsection.data;
            //        setTimeout(function () {
            //            $('#cmb_section_code').change(function () {
            //            }).multipleSelect({
            //                width: '100%'
            //            });
            //        }, 1000);
            //    })
            //};

            // Get Homeroom
          $scope.getHoomRoomCreation = function () {

              debugger;
              //var sims_grade_section = "";
              //for (var i = 0; i < $scope.temp.sims_grade_section.length; i++) {
              //    sims_grade_section = sims_grade_section + $scope.temp.sims_grade_section[i] + ',';
              //}
              $http.get(ENV.apiUrl + "api/HomeroomCreation/GetHoomroomCreation?teachercode=" + $rootScope.globals.currentUser.username + "&sims_cur_code=" + $scope.temp.sims_cur_code + "&sims_academic_year=" + $scope.temp.sims_academic_year + "&HomeroomSubject=" + $scope.temp.sims_subject_code + "&sims_grade_code=" + $scope.temp.sims_grade_section).then(function (res) {
                    $scope.homerooms = res.data;
                    if ($scope.homerooms.length > 0) {
                        $scope.showEditField = true;
                        $scope.showDeleteBtn = true;
                    }
                    else {
                        $scope.showEditField = false;
                    }
                });
            }
           // $scope.getHoomRoomCreation();

            $scope.getHoomRoomStudents = function (hoomroom_code) {                
                $scope.homeroomStudents = [];
                $scope.tempArray = [];
                $http.get(ENV.apiUrl + "api/HomeroomCreation/GetHoomroomStudents?hoomroom_code=" + hoomroom_code).then(function (res) {
                    $scope.homeroomStudents = angular.copy(res.data);
                    debugger
                    if ($scope.homeroomStudents.length > 0) {
                        $scope.disabledCur = true;
                        $scope.disabledAcaYear = true;
                        $scope.disabledSubj = true;
                        
                        $scope.edt = {
                            sims_cur_code: $scope.homeroomStudents[0].sims_cur_code,
                            sims_academic_year: $scope.homeroomStudents[0].sims_academic_year,
                            sims_subject_code: $scope.homeroomStudents[0].homeroomSubject,
                            homroomName: $scope.homeroomStudents[0].homeroomName,
                            homeroomStatus: $scope.homeroomStudents[0].homeroomStatus,
                            sims_teacher_code: $scope.homeroomStudents[0].homeroomEmployeeCode
                        }

                        $scope.edt.sims_subject_code = $scope.homeroomStudents[0].homeroomSubject;                        
                        $scope.getSubject($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                        $scope.getGradeSection($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.homeroomStudents[0].homeroomSubject);

                        for (var i = 0; i < $scope.homeroomStudents.length; i++) {
                            if (!$scope.tempArray.includes($scope.homeroomStudents[i].sims_grade_section_code)) {
                                $scope.tempArray.push($scope.homeroomStudents[i].sims_grade_section_code);
                            }
                        }
                        
                        setTimeout(function() {
                            $scope.edt.sims_grade_section = $scope.tempArray;                            
                            $("#gradesection").multipleSelect("setSelects", $scope.edt.sims_grade_section);                            
                        }, 2000);
                                                
                        setTimeout(function() {
                            $scope.Show_Data(true);
                        },3000);
                        
                    }

                });
            }



            $scope.Show_Data = function (Myform) {
                $scope.gradeSectionArray = [];

                if ($scope.edt.sims_grade_section == "" || $scope.edt.sims_grade_section == undefined || $scope.edt.sims_grade_section == null) {
                    $scope.showGradeMsg = true;
                    return false;
                } else {
                    $scope.showGradeMsg = false;
                }

                if (Myform) {

                    $scope.info1 = [];
                    var demo = [];
                    var demo1 = [];
                    $scope.secData = [];
                    $scope.table = false;
                    //$scope.subject = false;
                    $scope.disabledSubmit = false;
                    $scope.showSelectedSubject = false;
                    $scope.selectedAll = false;
                    $scope.busyindicator = false;

                    //var sims_grade_code = "";
                    //var sims_section_name = "";
                    //for (var i = 0; i < $scope.edt.sims_grade_code.length; i++) {
                    //    sims_grade_code = sims_grade_code + $scope.edt.sims_grade_code[i] + ',';
                    //}
                    //for (var i = 0; i < $scope.edt.sims_section_name.length; i++) {
                    //    sims_section_name = sims_section_name + $scope.edt.sims_section_name[i] + ',';
                    //}

                    //for (var i = 0; i < $scope.grade.length; i++) {
                    //    for (var j = 0; j < $scope.edt.sims_grade_code.length; j++) {
                    //        if ($scope.edt.sims_grade_code[j] == $scope.grade[i].sims_grade_code) {
                    //            $scope.gradeSectionArray.push({ sims_grade_code: $scope.grade[i].sims_grade_code, sims_grade_name: $scope.grade[i].sims_grade_name_en });
                    //        }
                    //    }
                    //}
                    var sims_grade_section = "";
                    for (var i = 0; i < $scope.edt.sims_grade_section.length; i++) {
                        sims_grade_section = sims_grade_section + $scope.edt.sims_grade_section[i] + ',';
                    }

                    //for (var i = 0; i < $scope.gradeSection.length; i++) {
                    //    for (var j = 0; j < $scope.edt.sims_grade_section.length; j++) {
                    //        if ($scope.edt.sims_grade_section[j] == $scope.gradeSection[i].classCode) {
                    //            $scope.gradeSectionArray.push({ sims_grade_section_code: $scope.gradeSection[i].classCode, sims_grade_section_name: $scope.gradeSection[i].className });
                    //        }
                    //    }
                    //}

                    $http.get(ENV.apiUrl + "api/HomeroomCreation/getStudentName?curcode=" + $scope.edt.sims_cur_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + sims_grade_section + "&subcode=" + $scope.edt.sims_subject_code).then(function (allSectionStudent) {
                        $scope.busyindicator = true;
                        if (allSectionStudent.data.length > 0) {
                            $scope.SectionStudent = allSectionStudent.data;
                            $scope.table = true;
                            $scope.flg = false;
                            $scope.arr = [];
                            for (var i = 0; i < $scope.SectionStudent.length; i++) {
                                for (var k = 0; k < $scope.gradeSection.length; k++) {
                                    for (var j = 0; j < $scope.edt.sims_grade_section.length; j++) {
                                        if ($scope.edt.sims_grade_section[j] == $scope.SectionStudent[i].sims_grade_section_code && $scope.SectionStudent[i].sims_grade_section_code == $scope.gradeSection[k].classCode) {
                                            if (!$scope.arr.includes($scope.SectionStudent[i].sims_grade_section_code)) {
                                                $scope.arr.push($scope.SectionStudent[i].sims_grade_section_code);
                                                $scope.gradeSectionArray.push({ sims_grade_section_code: $scope.SectionStudent[i].sims_grade_section_code, sims_grade_section_name: $scope.gradeSection[k].className });
                                            }
                                        }
                                    }
                                }
                            }
                            
                            if ($scope.homeroomStudents.length > 0) {
                                debugger
                                for (var i = 0; i < $scope.SectionStudent.length; i++) {
                                    for (var j = 0; j < $scope.homeroomStudents.length; j++) {
                                        for (var k = 0; k < $scope.homeroomStudents[j].studlist.length; k++) {
                                            if ($scope.homeroomStudents[j].studlist[k].homeroomStudentEnrollNumber == $scope.SectionStudent[i].sims_student_enroll_number 
                                                && $scope.homeroomStudents[j].sims_grade_code == $scope.SectionStudent[i].sims_grade_code && $scope.homeroomStudents[j].sims_section_code == $scope.SectionStudent[i].sims_section_code
                                                && $scope.homeroomStudents[j].homeroomSubject == $scope.SectionStudent[i].sims_subject_code) {
                                                $scope.SectionStudent[i].chkStudent = true;
                                                //&& $scope.homeroomStudents[j].homeroomEmployeeCode.toUpperCase() == $rootScope.globals.currentUser.username.toUpperCase()
                                            }
                                        }
                                    }
                                }
                            }

                            setTimeout(function () {
                                for (var m = 0; m < $scope.edt.sims_grade_section.length; m++) {
                                    $("#fixTable-" + m).tableHeadFixer({ 'top': 1 });
                                }
                            }, 1000);


                        }
                        else {
                            //$scope.subject = true;
                            swal({ text: 'No Records Found', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true, timer: 4000 });
                        }
                    });
                }

            }

           
            $scope.submitHomroom = function () {
                $scope.postData = [];
                $scope.classData = [];
                $scope.busyindicator = false;
                $scope.table = false;

                for (var i = 0; i < $scope.SectionStudent.length; i++) {
                    var t = $scope.SectionStudent[i].sims_student_enroll_number;
                    var v = document.getElementById(t + i);
                    if (v.checked == true) {
                        var obj = {
                            Status: true,
                            HomeroomStudentEnrollNumber: $scope.SectionStudent[i].sims_student_enroll_number,
                            sims_cur_code: $scope.edt.sims_cur_code,
                            sims_academic_year: $scope.edt.sims_academic_year
                        }
                        $scope.postData.push(obj);
                        var ob = {
                            sims_cur_code: $scope.edt.sims_cur_code,
                            sims_academic_year: $scope.edt.sims_academic_year,
                            sims_grade_code: $scope.SectionStudent[i].sims_grade_code,
                            sims_section_code: $scope.SectionStudent[i].sims_section_code,
                            Status: true
                        }
                        $scope.classData.push(ob);
                    }
                }

                //for (var j = 0; j < $scope.gradeSectionArray.length; j++) {
                //    var ob = {
                //        sims_cur_code: $scope.edt.sims_cur_code,
                //        sims_academic_year: $scope.edt.sims_academic_year,
                //        sims_grade_code: $scope.gradeSectionArray[j].sims_grade_code,
                //        sims_section_code: $scope.gradeSectionArray[j].sims_section_code,
                //        Status: true
                //    }
                //    $scope.classData.push(ob);
                //}
                if ($scope.edt.sims_teacher_code == undefined || $scope.edt.sims_teacher_code == "" || $scope.edt.sims_teacher_code == null) {
                     HomeroomEmployeeCode = user;
                }
                else {
                     HomeroomEmployeeCode = $scope.edt.sims_teacher_code;
                }

                $scope.opr = {
                    HomeroomName: $scope.edt.homroomName,
                    HomeroomSubject: $scope.edt.sims_subject_code,
                    HomeroomEmployeeCode: HomeroomEmployeeCode,
                    HomeroomStatus: $scope.edt.homeroomStatus,
                    CreatedBy: $rootScope.globals.currentUser.username,
                    UpdatedBy: $rootScope.globals.currentUser.username,
                    sims_cur_code: $scope.edt.sims_cur_code,
                    sims_academic_year: $scope.edt.sims_academic_year,
                    homeroomId: $scope.temp.hoomroom_code
                }

                if ($scope.postData.length == 0) {
                    swal({ text: 'At least One Record Required for Homeroom', imageUrl: "assets/img/notification-alert.png", width: 360, showCloseButton: true, timer: 3000 });
                    $scope.busyindicator = true;
                    $scope.table = true;
                    return false;
                }

                $http.post(ENV.apiUrl + "api/HomeroomCreation/AddHoomroomStudents?data1=" + JSON.stringify($scope.opr), $scope.postData).then(function (msg) {
                    $scope.homeroomCode = msg.data;
                    $http.post(ENV.apiUrl + "api/HomeroomCreation/AddHoomroomclass?homeroom_code=" + $scope.homeroomCode, $scope.classData).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: 'Homeroom Added Successfully', imageUrl: "assets/img/check.png", width: 340, showCloseButton: true });
                            $scope.busyindicator = true;
                            $scope.getHoomRoomCreation();
                            $scope.table = false;
                            $scope.disabledSubmit = true;
                            $scope.disabledCur = false;
                            $scope.disabledAcaYear = false;
                            $scope.disabledSubj = false;
                            $scope.Myform.$setPristine();
                            $scope.Myform.$setUntouched();
                            $scope.edt.sims_cur_code = '';
                            $scope.edt.sims_academic_year = '';
                            $scope.edt.sims_subject_code = '';
                            $scope.temp.hoomroom_code = '';
                            $scope.edt.homroomName = '';
                            $scope.edt.sims_teacher_code = '';
                            $scope.temp.sims_subject_code = '';
                            $scope.temp.sims_grade_section = '';
                            $scope.temp.sims_cur_code = '';
                            $scope.temp.sims_academic_year = '';

                                try {
                                    $("#gradesection").multipleSelect("uncheckAll");
                                }
                                catch (e) {

                                }
                                try {
                                    $("#gradesec").multipleSelect("uncheckAll");
                                }
                                catch (e) {

                                }

                            $("#gradesection").multipleSelect("uncheckAll");

                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: 'Homeroom Not Added. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                            $scope.table = false;
                            $scope.disabledSubmit = true;
                            $scope.busyindicator = true;
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                });
            }

            $scope.Delete = function (homeroom_code) {
               
                var data = [];
                if (homeroom_code == undefined || homeroom_code == "" || homeroom_code == null) {
                    swal({ text: 'Please select Homeroom', width: 360, showCloseButton: true });
                }
                else {
                    var obj = {
                        opr: 'D',
                        homeroom_code: homeroom_code
                    }

                    data.push(obj);

                    swal({
                        title: '',
                        text: "Are you sure you want to delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/HomeroomCreation/HomeroomDelete", data).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: 'Homeroom Deleted Successfully', imageUrl: "assets/img/check.png", width: 360, showCloseButton: true });
                                    $scope.getHoomRoomCreation();
                                    $scope.reset();
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: 'Homeroom Not Deleted. ', imageUrl: "assets/img/close.png", width: 360, showCloseButton: true });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            })

                        }
                    })
                }                                                  
            }

            $scope.isSelectAll = function (obj) {
                if (obj.selectedAll) {
                    obj.selectedAll = true;
                }
                else {
                    obj.selectedAll = false;
                }
                angular.forEach($scope.SectionStudent, function (item) {
                    if (item.sims_grade_section_code == obj.sims_grade_section_code) {
                        item.chkStudent = obj.selectedAll;
                    }
                });
            }

            $scope.reset = function () {
                $scope.table = false;
                $scope.disabledSubmit = true;
                $scope.disabledCur = false;
                $scope.disabledAcaYear = false;
                $scope.disabledSubj = false;
                $scope.showGradeMsg = false;
                $scope.homeroomStudents = [];
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                //$scope.edt.sims_cur_code = '';
                //$scope.edt.sims_academic_year = '';
                $scope.temp.hoomroom_code = '';
                $scope.edt.sims_subject_code = '';
                $scope.edt.homroomName = '';                             
                $scope.edt.sims_teacher_code = '';
                $scope.edt.sims_grade_section = '';
                $scope.temp.sims_subject_code = '';
                $scope.temp.sims_grade_section = '';
                //$scope.temp.sims_cur_code = '';
                //$scope.temp.sims_academic_year = '';

                try {
                    $("#gradesection").multipleSelect("uncheckAll");
                }
                catch (e) {

                }
                try {
                    $("#gradesec").multipleSelect("uncheckAll");
                }
                catch (e) {

                }

                try{
                    $("#gradesection").multipleSelect("uncheckAll");
                }
                catch(e){

                }
               
            }

        }]);

})();



