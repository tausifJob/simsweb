﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, modulecode = [];
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('BadgeAllocationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.showdisabled = true;
            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.pager = true;
            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.table1 = true;
            $scope.table2 = false;
            // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.countData = [
             { val: 10, data: 10 },
             { val: 20, data: 20 },
             { val: 'All', data: 'All' }, ]

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                if (str == 10 || str == 20 || str == 'All') {
                    if (str == "All") {
                        $scope.currentPage = '1';
                        $scope.filteredTodos = $scope.Route_Data;
                        $scope.pager = false;
                    }
                    else {
                        $scope.pager = true;
                        $scope.pagesize = str;
                        $scope.currentPage = 1;
                        $scope.numPerPage = str;
                        $scope.makeTodos();
                    }
                }
                $scope.numPerPage = str;
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $timeout(function () {
                $("#example").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.busyindicator = false;

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.temp = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear();
            });

            $scope.getAccYear = function () {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.temp.sims_cur_code).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = {
                        sims_academic_year: $scope.Acc_year[0].sims_academic_year,
                        sims_cur_code: $scope.temp.sims_cur_code
                    };
                    $scope.GetGrade();
                });
            }

            $scope.GetGrade = function () {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }

            $(function () {
                $('#cmb_grade_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.getSection = function () {
                $http.get(ENV.apiUrl + "api/StudentReport/getSectionFromGrade?cur_code=" + $scope.temp.sims_cur_code + "&grade_code=" + $scope.temp.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $(function () {
                $('#cmb_section_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.getBadge = function () {
                $http.get(ENV.apiUrl + "api/DeliveryMode/getBadgeOptions").then(function (Badgecode) {
                    $scope.Badge_code = Badgecode.data;
                });
            }

            $scope.getBadge();

            $http.get(ENV.apiUrl + "api/DeliveryMode/getSeachOptions").then(function (searchoptions) {
                $scope.searchoptions = searchoptions.data;
                for (var i = 0; i < $scope.searchoptions.length; i++) {
                    if ($scope.searchoptions[i].fee_concession_type == 'Y') {
                        $scope.temp = {
                            'fee_concession_number': $scope.searchoptions[i].sims_search_code
                        }
                    }
                }
            });

            $scope.getgrid = function () {
                $scope.currentPage = 1;
                $scope.PI = {}
                $http.get(ENV.apiUrl + "api/DeliveryMode/getBadgeAllocationStudent?data1=" + JSON.stringify($scope.PI)).then(function (get_Route_Data) {
                    $scope.operation = false;
                    $scope.table1 = true;
                    $scope.Route_Data = get_Route_Data.data.table;
                    if ($scope.Route_Data.length > 0) {
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                        }
                        $scope.totalItems = $scope.Route_Data.length;
                        $scope.todos = $scope.Route_Data;
                        $scope.makeTodos();
                        $scope.table1 = true;
                    }
                    else {
                        swal({
                            text: "Record Not Found",
                            imageUrl: "assets/img/close.png", width: 300, height: 200
                        });
                        $scope.filteredTodos = [];
                    }

                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
                //$scope.row1 = '';
                //$scope.myForm.$setPristine();
                //$scope.myForm.$setUntouched();
            }

            $scope.getgrid();

            $scope.New = function () {
                debugger;
                $scope.check = true;
                $scope.temp.dm_name = true;
                $scope.temp.dm_lead_time = true;
                $scope.temp.dm_type = true;
                $scope.editmode = false;
                $scope.newmode = true;
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.temp = {
                    sims_academic_year: $scope.Acc_year[0].sims_academic_year,
                    sims_cur_code: $scope.curriculum[0].sims_cur_code
                };
                $scope.GetGrade();
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Show_Data = function () {

                $scope.IP = {};
                $scope.IP.req_no = $scope.temp.sims_cur_code,
                $scope.IP.dept_code = $scope.temp.sims_academic_year,
                $scope.IP.rd_item_desc = $scope.temp.sims_grade_code + ',',
                $scope.IP.request_mode_code = $scope.temp.sims_section_code + ',',
                $scope.IP.req_range_no = $scope.temp.sims_search_code

                $http.get(ENV.apiUrl + "api/DeliveryMode/getBadgeAllocationStudent?data1=" + JSON.stringify($scope.IP)).then(function (res) {
                    $scope.rows = res.data.table;
                    $scope.table2 = true;
                });
            }

            $scope.Reset = function () {
                $scope.temp = '';
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                    $scope.curriculum = AllCurr.data;
                    $scope.temp = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                    $scope.getAccYear();
                });
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.table2 = false;
                $scope.operation = false;
                $scope.Reset();
            }

            $scope.Save = function (myForm, str) {

                if (myForm) {
                    var data1 = [];
                    var data = $scope.temp;
                    for (var i = 0; i < $scope.rows.length; i++) {
                        var v = document.getElementById($scope.rows[i].sims_enroll_number + i);
                        if (v.checked == true) {
                            var ob = {};
                            ob.dm_code = $scope.temp.sims_cur_code,
                            ob.dm_name = $scope.temp.sims_academic_year,
                            ob.dm_lead_time = $scope.temp.dm_type_name
                            ob.dm_type = $scope.rows[i].sims_enroll_number;
                            ob.dm_type_desc = $rootScope.globals.currentUser.username;
                            ob.opr = 'H';
                            ob.dm_type_name = $scope.temp.sims_remark;
                            data1.push(ob);
                        }
                    }
                    $http.post(ENV.apiUrl + "api/DeliveryMode/CUDBadgeAllocationMaster", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $scope.getgrid();
                    });
                    $scope.table1 = true;
                    $scope.table2 = false;
                }
            }

            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.rows.length; i++) {
                    var v = document.getElementById($scope.rows[i].sims_enroll_number + i);
                    if (main.checked == true) {
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }

                    else {
                        v.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.CheckAllChecked1 = function () {

                main = document.getElementById('mainchk1');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].student_name + i);
                    if (main.checked == true) {
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }

                    else {
                        v.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete1 = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.deleterecord = function () {
                var routecode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].student_name + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deleteroutecode = ({
                            'dm_code': $scope.filteredTodos[i].sims_cur_code,
                            'dm_name': $scope.filteredTodos[i].sims_academic_year,
                            'dm_lead_time': $scope.filteredTodos[i].sims_badge_id,
                            'dm_type': $scope.filteredTodos[i].sims_enroll_number,
                            'opr': 'K'
                        });
                        routecode.push(deleteroutecode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/DeliveryMode/CUDBadgeAllocationMaster", routecode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)

                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].student_name + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                        $scope.getgrid();
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Route_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Route_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                return (item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sagar == toSearch) ? true : false;
            }

        }])
})();