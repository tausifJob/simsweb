﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SujectTypeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.subjectType_data = [];
            $scope.edit_code = false;
            var data1 = [];
            var deletecode = [];

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            $scope.edt = {};
            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.obj2 = res.data;
                if (res.data.length > 0) {
                    $scope.edt.sims_attendance_cur_code = res.data[0].sims_attendance_cur_code;
                }
                console.log($scope.obj2);
            });

            $scope.countData = [
                  { val: 5, data: 5 },
                  { val: 10, data: 10 },
                  { val: 15, data: 15 },

            ]


            $http.get(ENV.apiUrl + "api/common/SubjectType/getSubjectType").then(function (res) {
                $scope.subjectType_data = res.data;
                $scope.display = false;
                $scope.grid = true;
                if ($scope.subjectType_data.length > 0) {


                    $scope.pager = true;
                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.subjectType_data.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.subjectType_data.length, data: 'All' })
                    }
                    $scope.totalItems = $scope.subjectType_data.length;
                    $scope.todos = $scope.subjectType_data;
                    $scope.makeTodos();
                }
                else {

                    swal({  text: "Record Not Found",imageUrl: "assets/img/close.png", width: 300, height: 200 });
                }

            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edt =
                    {
                        sims_attendance_cur_code: str.sims_attendance_cur_code,
                        subject_type_code: str.subject_type_code,
                        subject_type_name_eng: str.subject_type_name_eng,
                        subject_type_name_ar: str.subject_type_name_ar,
                        subject_type_name_fr: str.subject_type_name_fr,
                        subject_type_name_ot: str.subject_type_name_ot,
                    }

                $scope.edit_code = true;
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        $http.post(ENV.apiUrl + "api/common/SubjectType/CheckSubjectType?SubjectType=" + $scope.edt.subject_type_code + "&cur_code=" + $scope.edt.sims_attendance_cur_code).then(function (res) {
                            $scope.msg1 = res.data;
                            console.log($scope.msg1);

                            if ($scope.msg1.status == true) {
                                // $rootScope.strMessage = $scope.msg1.strMessage;
                                //$('#message').modal('show');
                                swal({ text: "Subject Type Already Exists", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                                $scope.edt = "";
                                $scope.myForm.$setPristine();
                                $scope.myForm.$setUntouched();
                            }
                            else {
                                // var data = $scope.edt;
                                //data.opr = "I";

                                var data = ({
                                    sims_attendance_cur_code: $scope.edt.sims_attendance_cur_code,
                                    subject_type_code: $scope.edt.subject_type_code,
                                    subject_type_name_eng: $scope.edt.subject_type_name_eng,
                                    subject_type_name_ar: $scope.edt.subject_type_name_ar,
                                    subject_type_name_fr: $scope.edt.subject_type_name_fr,
                                    subject_type_name_ot: $scope.edt.subject_type_name_ot,
                                    opr: 'I'
                                });

                                data1.push(data);

                                $http.post(ENV.apiUrl + "api/common/SubjectType/CUDSubjectType", data1).then(function (res) {
                                    $scope.display = true;
                                    $scope.msg1 = res.data;
                                    if ($scope.msg1 == true) {
                                        swal({  text: "Subject Type Added Successfully",imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({  text: "Subject Type Data Not Added Successfully. ",imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.Update = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    var data = ({
                        sims_attendance_cur_code: $scope.edt.sims_attendance_cur_code,
                        subject_type_code: $scope.edt.subject_type_code,
                        subject_type_name_eng: $scope.edt.subject_type_name_eng,
                        subject_type_name_ar: $scope.edt.subject_type_name_ar,
                        subject_type_name_fr: $scope.edt.subject_type_name_fr,
                        subject_type_name_ot: $scope.edt.subject_type_name_ot,
                        opr: 'U'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/SubjectType/CUDSubjectType", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;

                        if ($scope.msg1 == true) {
                            swal({  text: "Subject Type Updated Successfully",imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else if ($scope.msg1 == false) {
                            swal({  text: "Subject Type Not Updated Successfully. " ,imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/common/SubjectType/getSubjectType").then(function (res) {
                    $scope.subjectType_data = res.data;
                    $scope.display = false;
                    $scope.grid = true;
                    if ($scope.subjectType_data.length > 0) {


                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.subjectType_data.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.subjectType_data.length, data: 'All' })
                        }
                        $scope.totalItems = $scope.subjectType_data.length;
                        $scope.todos = $scope.subjectType_data;
                        $scope.makeTodos();
                    }
                    else {

                        swal({  text: "Record Not Found",imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
                $scope.edit_code = false;

                $scope.edt = {};
                if ($scope.obj2.length > 0) {
                    $scope.edt.sims_attendance_cur_code = $scope.obj2[0].sims_attendance_cur_code;
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].subject_type_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].subject_type_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].subject_type_code + i);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sims_attendance_cur_code': $scope.filteredTodos[i].sims_attendance_cur_code,
                            'subject_type_code': $scope.filteredTodos[i].subject_type_code,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/common/SubjectType/CUDSubjectType", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({  text: "Subject Type Deleted Successfully",imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });

                                }
                                else if ($scope.msg1 == false) {
                                    swal({  text: "Subject Type Not Deleted Successfully. " ,imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                            });
                        }
                        else {

                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].subject_type_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
            }

            $scope.size = function (str) {

                //if (str == 5 || str == 10 || str == 15) {
                //    $scope.pager = true;
                //}
                //else {
                //    $scope.pager = false;
                //}

                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();

                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.subjectType_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

               // $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }


            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.subjectType_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.subjectType_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].subject_type_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.subject_type_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.subject_type_name_eng.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

        }])
})();