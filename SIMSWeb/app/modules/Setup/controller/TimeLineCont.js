﻿(function () {
    'use strict';
    var obj1, obj2, temp, main;
    var opr = '';
    var portalcode = [];

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TimeLineCont',
         ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
             $scope.pagesize = '5';
             $scope.color_code = '';
             $scope.table1 = true;
             $scope.editmode = false;
             var formdata = new FormData();
             var data1 = [];
             $scope.url = ENV.siteUrl;

             //$scope.UserAuditfun = function (modcode, appcode, sdt, edt) {
             //    debugger;
             //    sdt = $scope.temp.comn_audit_start_time;
             //    edt = $scope.temp.comn_audit_end_time;
                    
             //}

             $scope.lagnn = gettextCatalog.currentLanguage == "en" ? true : false;

             $scope.$on('lagnuage_chaged', function (str) {

                 $scope.lagnn = gettextCatalog.currentLanguage == "en" ? true : false;
             });

             $scope.application_click_new = function (app) {
                 debugger

                 $scope.check_favorite(app.comn_appl_location_html);
                 if (app.comn_appl_type == 'R') {
                     $rootScope.location = app.comn_appl_location;
                    // setTimeout(function () { $state.go('main.ReportCard'); }, 100);
                     //  $state.go('main');

                     $state.go('main.ReportCard');
                 }
                 else
                     $state.go("main." + app.comn_appl_location_html);

                 if ($scope.fin == undefined) {
                     if (app.comn_appl_mod_code == '005') {
                         $scope.main_loader_show = true;
                         $http.get(ENV.apiUrl + "api/PDCSubmission/GetComp_Name?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                             $scope.cmbCompany = res.data;
                             if (res.data.length == 1) {
                                 $scope.companyName = res.data[0].comp_name;

                                 $scope.finnComp = res.data[0].comp_code;

                                 $http.get(ENV.apiUrl + "api/PDCSubmission/GetFinancial_year?comp_code=" + $scope.finnComp).then(function (res) {

                                     if (res.data.length > 0) {

                                         console.log('Finn');

                                         var data = {
                                             year: res.data[0].financial_year,
                                             company: $scope.finnComp,
                                             companyame: $scope.companyName
                                         }
                                         window.localStorage["Finn_comp"] = JSON.stringify(data)

                                         $state.reload();
                                         setTimeout(function () {
                                             $scope.main_loader_show = false;
                                         }, 500);

                                     }
                                 });
                             }
                             else if (res.data.length > 1) {
                                 $scope.financeWindow();
                             }


                         });
                     }
                 }

                 if ($scope.oldobj == undefined) {


                     $scope.oldobj = {
                         opr: 'I',
                         comn_audit_user_code: $rootScope.globals.currentUser.username,
                         comn_appl_name_en: app.comn_appl_location_html,
                         comn_audit_start_time: $scope.getDateTime(),
                         comn_audit_ip: $rootScope.ip_add,
                         comn_audit_dns: $rootScope.dns,


                         //comn_audit_end_time: '',
                     }
                     $http.post(ENV.apiUrl + "api/common/InsertUserAudit", $scope.oldobj).then(function (res) {

                     });
                 }
                 else {

                     var edate = $scope.getDateTime()
                     var data = {
                         opr: 'I',
                         comn_audit_user_code: $rootScope.globals.currentUser.username,
                         comn_appl_name_en: app.comn_appl_location_html,
                         comn_audit_start_time: edate,
                         comn_audit_ip: $rootScope.ip_add,
                         comn_audit_dns: $rootScope.dns,


                         //comn_audit_end_time: '',
                     }

                     $http.post(ENV.apiUrl + "api/common/InsertUserAudit", data).then(function (res) {


                         $scope.oldobj['comn_audit_end_time'] = edate
                         $scope.oldobj['opr'] = 'J'

                         $http.post(ENV.apiUrl + "api/common/InsertUserAudit", $scope.oldobj).then(function (res) {

                             $scope.oldobj['comn_audit_start_time'] = edate
                             $scope.oldobj['comn_appl_name_en'] = app.comn_appl_location_html

                         });

                     });

                 }
                 $scope.menu_list_click(app.comn_mod_name_en, app.comn_appl_mod_code, app.comn_appl_name_en, app.comn_appl_location_html)


             }


             $scope.appl_load=function(str,obj)
             {
                 
                 $scope.get_time_line_appl_code(str);

                 for (var i = 0; i < $scope.appl_items.length; i++) {


                     if ($scope.appl_items[i].url == str) {
                         $scope.chkExist_state = true;
                         
                     }

                 }

                 if (!$scope.chkExist_state) {
                     $state.go('main.TimeLine')
                 }
                 else {

                     if (obj.comn_appl_type == 'R') {
                         $rootScope.location = obj.comn_appl_location;
                         $rootScope.main_appl_code = str;
                         //setTimeout(function () { $state.go('main.ReportCard'); }, 100);
                        // $state.go('main');
                         $state.go('main.ReportCard');
                     }
                     else {
                         $state.go("main." + str);
                     }
                 }
                 if ($scope.oldobj == undefined) {


                     $scope.oldobj = {
                         opr: 'I',
                         comn_audit_user_code: $rootScope.globals.currentUser.username,
                         comn_appl_name_en: str,
                         comn_audit_start_time: $scope.getDateTime(),
                         comn_audit_ip: $rootScope.ip_add,
                         comn_audit_dns: $rootScope.dns,


                         //comn_audit_end_time: '',
                     }
                     $http.post(ENV.apiUrl + "api/common/InsertUserAudit", $scope.oldobj).then(function (res) {

                     });
                 }
                 else {

                     var edate = $scope.getDateTime()
                     var data = {
                         opr: 'I',
                         comn_audit_user_code: $rootScope.globals.currentUser.username,
                         comn_appl_name_en: str,
                         comn_audit_start_time: edate,
                         comn_audit_ip: $rootScope.ip_add,
                         comn_audit_dns: $rootScope.dns,


                         //comn_audit_end_time: '',
                     }

                     $http.post(ENV.apiUrl + "api/common/InsertUserAudit", data).then(function (res) {


                         $scope.oldobj['comn_audit_end_time'] = edate
                         $scope.oldobj['opr'] = 'J'

                         $http.post(ENV.apiUrl + "api/common/InsertUserAudit", $scope.oldobj).then(function (res) {

                             $scope.oldobj['comn_audit_start_time'] = edate
                             $scope.oldobj['comn_appl_name_en'] = str

                         });

                     });

                 }
             }

             $http.get(ENV.apiUrl + "api/TimeLine/getUserAuditNew?uname=" + $rootScope.globals.currentUser.username).then(function (auditDetails) {
                 $scope.audit_Details_new = auditDetails.data;
                 $http.get(ENV.apiUrl + "api/TimeLine/getUserAudit?uname=" + $rootScope.globals.currentUser.username).then(function (auditDetails1) {
                     $scope.audit_Details = auditDetails1.data;


                 });
             });
          

             $http.get(ENV.apiUrl + "api/common/Empdashboard/get_favirate_Application_Dash?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                 $scope.applications_time = res.data;
             });
             
             

             
           
    }])
})();
