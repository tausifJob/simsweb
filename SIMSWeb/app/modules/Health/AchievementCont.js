﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.HRMS');
    simsController.controller('AchievementCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;


            $http.get(ENV.apiUrl + "api/Achievement/getAchievementType").then(function (achieveType) {
                $scope.achieve_Type = achieveType.data;
                console.log($scope.achieve_Type);
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/Achievement/getAllAchievement").then(function (res1) {
                $scope.achieveData = res1.data;
                $scope.totalItems = $scope.achieveData.length;
                $scope.todos = $scope.achieveData;
                $scope.makeTodos();
            });

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj1, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj1;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_doc_mod_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_doc_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_doc_srno == toSearch) ? true : false;
            }

            $scope.New = function () {

                $scope.disabled = false;
                $scope.readonly = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = "";
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            var datasend = [];
            $scope.savedata = function (Myform) {

                if (Myform) {
                    var data = $scope.temp;
                    data.opr = "I";
                    datasend.push(data);

                    $http.post(ENV.apiUrl + "api/Achievement/CUDAchievement", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Inserted Successfully", showCloseButton: true, width: 380, });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Inserted", showCloseButton: true, width: 380, });
                        }
                        $http.get(ENV.apiUrl + "api/Achievement/getAllAchievement").then(function (res1) {
                            $scope.achieveData = res1.data;
                            $scope.totalItems = $scope.achieveData.length;
                            $scope.todos = $scope.achieveData;
                            $scope.makeTodos();
                        });
                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.sims_sip_achievement_type = "";
                $scope.temp.sims_sip_achievement_desc = "";
                $scope.temp.sims_sip_achievement_point = "";
                $scope.temp.sims_sip_achievement_status = "";
            }

            $scope.edit = function (str) {

                $scope.readonly = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;

                $scope.temp = {
                    sims_sip_achievement_type: str.sims_sip_achievement_type
                   , sims_sip_achievement_code: str.sims_sip_achievement_code
                   , sims_sip_achievement_desc: str.sims_sip_achievement_desc
                   , sims_sip_achievement_status: str.sims_sip_achievement_status
                   , sims_sip_achievement_point: str.sims_sip_achievement_point
                   , sims_appl_form_field_value1: str.sims_appl_form_field_value1

                };
            }

            var dataupdate = [];
            $scope.update = function (Myform) {
                if (Myform) {
                    var data = {
                        sims_sip_achievement_type: $scope.temp.sims_sip_achievement_type
                       , sims_sip_achievement_code: $scope.temp.sims_sip_achievement_code
                       , sims_sip_achievement_desc: $scope.temp.sims_sip_achievement_desc
                       , sims_sip_achievement_status: $scope.temp.sims_sip_achievement_status
                       , sims_sip_achievement_point: $scope.temp.sims_sip_achievement_point
                       , sims_appl_form_field_value1: $scope.temp.sims_appl_form_field_value1
                       , opr: 'U'
                    };
                    dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/Achievement/CUDAchievement", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", showCloseButton: true, width: 380, });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", showCloseButton: true, width: 380, });
                        }

                        $http.get(ENV.apiUrl + "api/Achievement/getAllAchievement").then(function (res1) {
                            $scope.achieveData = res1.data;
                            $scope.totalItems = $scope.achieveData.length;
                            $scope.todos = $scope.achieveData;
                            $scope.makeTodos();
                        });
                    });
                    dataupdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }


            $scope.OkDelete = function () {
                debugger;
                var data1 = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_sip_achievement_code': $scope.filteredTodos[i].sims_sip_achievement_code,
                            'opr': "D"
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/Achievement/CUDAchievement", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Achievement/getAllAchievement").then(function (res1) {
                                                $scope.achieveData = res1.data;
                                                $scope.totalItems = $scope.achieveData.length;
                                                $scope.todos = $scope.achieveData;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                    main.checked = false;
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted ", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Achievement/getAllAchievement").then(function (res1) {
                                                $scope.achieveData = res1.data;
                                                $scope.totalItems = $scope.achieveData.length;
                                                $scope.todos = $scope.achieveData;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                data1 = [];
            }

        }])

})();
