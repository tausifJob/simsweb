﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Health');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('medicalVisitMedicineCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.display = false;
            $scope.grid = true;
            var deletefin = [];
            var datasend = [];
            $scope.itemsPerPage = "10";
            $scope.currentPage = 0;
            $scope.items = [];
            $scope.url = ENV.apiUrl + 'Content' + '/' + $http.defaults.headers.common['schoolId'];
            var photofile;

            $http.get(ENV.apiUrl + "api/common/Medical_Visit_Medicine/GetSims_MedicalVisitMedicine").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.CreDiv = res.data;
                console.log(res.data);
                $scope.totalItems = $scope.CreDiv.length;
                // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                $scope.todos = $scope.CreDiv;
                $scope.makeTodos();
            });

            $http.get(ENV.apiUrl + "api/common/Medical_Visit_Medicine/GetAllVisitNumber").then(function (res) {
                $scope.cmbVisit = res.data;
            });


            $http.get(ENV.apiUrl + "api/common/Medical_Visit_Medicine/GetAllMedicalMedicineCode").then(function (res) {

                $scope.cmbMedicine = res.data;

            });

            //$scope.range = function () {
            //    var rangeSize = 5;
            //    var ret = [];
            //    var start;

            //    start = $scope.currentPage;
            //    if (start > $scope.pageCount() - rangeSize) {
            //        start = $scope.pageCount() - rangeSize + 1;
            //    }

            //    for (var i = start; i < start + rangeSize; i++) {
            //        if (i >= 0)
            //            ret.push(i);
            //    }

            //    return ret;

            //};

            //$scope.prevPage = function () {
            //    if ($scope.currentPage > 0) {
            //        $scope.currentPage--;
            //    }
            //};

            //$scope.prevPageDisabled = function () {
            //    return $scope.currentPage === 0 ? "disabled" : "";
            //};

            //$scope.pageCount = function () {

            //    return Math.ceil($scope.items.length / $scope.itemsPerPage) - 1;
            //};

            //$scope.nextPage = function () {
            //    if ($scope.currentPage < $scope.pageCount()) {
            //        $scope.currentPage++;
            //    }
            //};

            //$scope.nextPageDisabled = function () {
            //    return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            //};

            //$scope.setPage = function (n) {
            //    $scope.currentPage = n;
            //};

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                debugger;
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                debugger;
                /* Search Text in all 3 fields */
                return (item.sims_medicine_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_medical_visit_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_medicine_dosage.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_medicine_duration.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_serial_number == toSearch) ? true : false; 
            }

            $scope.edit = function (str) {
                debugger;
                //  $scope.edt = str;
                $scope.v_number = true;
                $scope.display = true;
                $scope.grid = false;
                $scope.update1 = true;
                $scope.editflg = true;
                $scope.save1 = false;
                $scope.edt = {
                    'sims_medical_visit_number': str.sims_medical_visit_number,
                    'sims_medicine_code': str.sims_medicine_code,
                    'sims_medicine_dosage': str.sims_medicine_dosage,
                    'sims_medicine_duration': str.sims_medicine_duration,
                    'sims_status': str.sims_status,
                    'sims_serial_number': str.sims_serial_number

                }
                //edt.sims_medical_visit_number
                //edt.sims_medicine_code
                //edt.sims_medicine_dosage
                //edt.sims_medicine_duration
                //edt.sims_status
                // 'sims_serial_number'

            }


            $scope.exportData = function () {
               debugger;
              //  $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('pdf_print').innerHTML],
                         {
                             type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                         });
                        $scope.detailsField = false;
                        saveAs(blob, "SubstituteLectureDetailsReport.xls");
                        $scope.colsvis = false;
                        

                    }

                });
                $scope.colsvis = true;

            };





            var dataforUpdate = [];
            $scope.Update = function (newEventForm) {

                debugger;
                if (newEventForm.$valid) {

                    var data = $scope.edt;
                    data.opr = "U";
                    dataforUpdate.push(data);
                    //dataupdate.push(data);
                    $http.post(ENV.apiUrl + "api/common/Medical_Visit_Medicine/InsertSims_MedicalVisitMedicine", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.getgrid();
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $http.get(ENV.apiUrl + "api/common/Medical_Visit_Medicine/GetSims_MedicalVisitMedicine").then(function (res) {

                            $scope.filteredTodos = res.data;
                            $scope.currentPage = 0;
                        });

                    });
                    dataforUpdate = [];
                    $scope.display = false;
                    $scope.grid = true;
                }
            }

            //$scope.Save = function (newEventForm) {
            //    debugger
            //    if (newEventForm.$valid) {
            //        var data = $scope.edt;
            //        data.opr = 'I';
            //        $http.post(ENV.apiUrl + "api/common/Medical_Visit_Medicine/InsertSims_MedicalVisitMedicine", data).then(function (res) {
            //            $scope.ins = res.data;
            //            if (res.data) {
            //                swal({ title: "Alert", text: "Record Inserted Successfully.", imageUrl: "assets/img/check.png", });
            //                $http.get(ENV.apiUrl + "api/common/Medical_Visit_Medicine/GetSims_MedicalVisitMedicine").then(function (res) {
            //                    $scope.display = false;
            //                    $scope.grid = true;
            //                    $scope.items = res.data;
            //                    $scope.currentPage = 0;
            //                });
            //            }
            //            else
            //                swal({ title: "Alert", text: "Record Not Inserted.", imageUrl: "assets/img/notification-alert.png", });
            //        });
            //    }
            //}
            $scope.Save = function (newEventForm) {
                debugger
                if (newEventForm.$valid) {
                    var data = $scope.edt;
                    data.opr = 'I';
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/common/Medical_Visit_Medicine/InsertSims_MedicalVisitMedicine", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.getgrid();
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Inserted.", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        //$http.get(ENV.apiUrl + "api/common/Medical_Visit_Medicine/GetSims_MedicalVisitMedicine").then(function (res) {
                        //    $scope.filteredTodos = res.data;
                        //    $scope.currentPage = 0;
                        //});

                    });
                    datasend = [];
                    $scope.display = false;
                    $scope.grid = true;
                }

            }

            $scope.New = function () {
                $scope.edt = "";
                $scope.grid = false;
                $scope.display = true;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.cancel = true;
                $scope.editflg = false;
                $scope.v_number = false;
                $scope.edt = {};
                $scope.edt.sims_status = true;
            }


            var main, del = '', delvar = [];
            $scope.chk = function () {

                main = document.getElementById('chk_min');
                del = [];
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].sims_serial_number1 = true;


                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].sims_serial_number1 = false;

                        main.checked = false;

                        $scope.row1 = '';
                    }
                }


            }


            //$scope.Delete = function () {
            //    debugger
            //    del = '';

            //    for (var i = 0; i < $scope.items.length; i++) {

            //        if ($scope.items[i].sims_serial_number1 == true) {
            //            del = del + $scope.items[i].sims_serial_number + ','
            //        }

            //    }
            //    // var data = $scope.items[0];
            //    var data = { sims_serial_number: del, opr: 'X' }
            //    //data.sims_serial_number = del;
            //    //data.opr = 'X';
            //    //delvar = [];
            //    //delvar.push({ sims_timetable_number: del });

            //    $http.post(ENV.apiUrl + "api/common/Medical_Visit_Medicine/InsertSims_MedicalVisitMedicine", data).then(function (res) {

            //        if (res.data) {
            //            swal({ title: "Alert", text: "Record Deleted Successfully.", imageUrl: "assets/img/check.png", });


            //            $http.get(ENV.apiUrl + "api/common/Medical_Visit_Medicine/GetSims_MedicalVisitMedicine").then(function (res) {
            //                $scope.display = false;
            //                $scope.grid = true;
            //                $scope.items = res.data;
            //                $scope.currentPage = 0;
            //            });
            //        }
            //        else {
            //            swal({ title: "Alert", text: "Record Not Deleted.", imageUrl: "assets/img/notification-alert.png", });

            //        }
            //    });

            //}
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_serial_number);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_serial_number);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }


            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.Delete = function () {
                debugger
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i + $scope.filteredTodos[i].sims_serial_number);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_serial_number': $scope.filteredTodos[i].sims_serial_number,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            debugger;
                            $http.post(ENV.apiUrl + "api/common/Medical_Visit_Medicine/InsertSims_MedicalVisitMedicine", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i + $scope.filteredTodos[i].sims_serial_number);
                                if (v.checked == true) {
                                    v.checked = false;
                                    //$scope.row1 = '';
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }

            $scope.cancels = function () {
                debugger;
                $scope.grid = true;
                $scope.display = false;
                $scope.update1 = false;
                $scope.save1 = false;
                $scope.editflg = false;
            }
            $scope.getgrid = function () {
            $http.get(ENV.apiUrl + "api/common/Medical_Visit_Medicine/GetSims_MedicalVisitMedicine").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.CreDiv = res.data;
                console.log(res.data);
                $scope.totalItems = $scope.CreDiv.length;
                // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                $scope.todos = $scope.CreDiv;
                $scope.makeTodos();
            });
        }

        }]);



})();


