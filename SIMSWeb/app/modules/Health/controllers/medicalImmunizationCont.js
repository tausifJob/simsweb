﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Health');
    simsController.controller('medicalImmunizationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.display = false;
            $scope.itemsPerPage = "10";
            $scope.currentPage = 0;
            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.items = [];
            $scope.url = ENV.apiUrl + 'Content' + '/' + $http.defaults.headers.common['schoolId'];
            var photofile;
            $scope.display = false;
            $scope.grid = true;

            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //$scope.size = function (str) {
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str;
            //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    //$scope.makeTodos();
                    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $http.get(ENV.apiUrl + "api/common/Medical_Immunization/GetSims_MedicalImmunization").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.items = res.data;
                $scope.totalItems = $scope.items.length;
                $scope.todos = $scope.items;
                $scope.makeTodos();
                if ($scope.items.length == 0) {
                    swal({ title: 'Alert', text: "No Record Found...", showCloseButton: true, width: 450, height: 200 });
                }
            });

            $http.get(ENV.apiUrl + "api/common/Medical_Immunization/GetAllMedicalImmunizationAgeGroupCode").then(function (res) {

                $scope.cmbgroupCode = res.data;

                //console.log($scope.cmbVisit);


            });

            $http.get(ENV.apiUrl + "api/common/MedicalExamination/GetAllMedicalExaminationName").then(function (res) {

                $scope.cmbExaminationName = res.data;

            });

            $scope.edit = function (str) {

                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                $scope.edt = str;
                $scope.display = true;
                $scope.grid = false;
                $scope.update1 = true;
                $scope.editflg = true;
                $scope.save1 = false;
                //$scope.curChange(str.sims_timetable_cur);
                //$scope.academicChange(str.sims_academic_year);
                //$scope.gradeChange(str.sims_grade);
                //  $scope.edt = str;
                }
            }

            $scope.Update = function (newEventForm) {
                debugger;
                if (newEventForm.$valid) {

                    var data = $scope.edt;
                    data.opr = 'U';

                    $http.post(ENV.apiUrl + "api/common/Medical_Immunization/InsertSims_MedicalImmunization", data).then(function (res) {
                        $scope.ins = res.data;
                        if (res.data==true) {
                            swal({  text: "Record Updated Successfully.", imageUrl: "assets/img/check.png" });
                            $http.get(ENV.apiUrl + "api/common/Medical_Immunization/GetSims_MedicalImmunization").then(function (res) {
                                $scope.display = false;
                                $scope.grid = true;
                                $scope.items = res.data;
                                $scope.totalItems = $scope.items.length;
                                $scope.todos = $scope.items;
                                $scope.makeTodos();
                            });

                        }
                        else if (res.data==false) {
                            swal({  text: "Record Not Updated ." , imageUrl: "assets/img/close.png" });
                        }
                        else {
                            swal("Error-" + $scope.ins)
                        }
                    });
                }
            }

            $scope.Save = function (newEventForm) {

                if (newEventForm.$valid) {

                    var data = $scope.edt;
                    data.opr = 'I';

                    $http.post(ENV.apiUrl + "api/common/Medical_Immunization/InsertSims_MedicalImmunization", data).then(function (res) {
                        $scope.ins = res.data;
                        if (res.data==true) {
                            swal({  text: "Record Inserted Successfully.", imageUrl: "assets/img/check.png" });
                            $http.get(ENV.apiUrl + "api/common/Medical_Immunization/GetSims_MedicalImmunization").then(function (res) {
                                $scope.display = false;
                                $scope.grid = true;
                                $scope.items = res.data;
                                $scope.totalItems = $scope.items.length;
                                $scope.todos = $scope.items;
                                $scope.makeTodos();
                            });

                        }
                        else if (res.data==false) {
                            swal({  text: "Record Not Inserted.", imageUrl: "assets/img/notification-alert.png" });
                        }
                        else {
                            swal("Error-" + $scope.ins)
                        }

                    });

                }
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.edt = "";
                    $scope.grid = false;
                    $scope.display = true;
                    $scope.save1 = true;
                    $scope.editflg = false;
                    $scope.edt = {};
                    $scope.edt.sims_immunization_status = true;

                }
            }

            var main, del = '', delvar = [];

            $scope.CheckAllChecked = function () {
                main = document.getElementById('chk_min');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_immunization_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_immunization_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyoneselect = function () {

                $("input[type='checkbox']").change(function (e) {

                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('chk_min');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                var data1 = [];
                $scope.flag = false;
                del = '';
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_immunization_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        //var deletemodulecode = ({
                        //    'sims_immunization_code': $scope.filteredTodos[i].sims_immunization_code,
                        //    opr: 'X'
                        //});
                        //deleteleave.push(deletemodulecode);
                        del = del + $scope.items[i].sims_immunization_code + ','
                    }
                }

                var data = { sims_immunization_code: del, opr: 'X' }

                if ($scope.flag) {
                    swal({
                        title: '', text: "Are you sure you want to Delete?", showCloseButton: true, showCancelButton: true, confirmButtonText: 'Yes', width: 380, cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {


                            $http.post(ENV.apiUrl + "api/common/Medical_Immunization/InsertSims_MedicalImmunization", data).then(function (res) {

                                if (res.data==true) {
                                    swal({  text: "Record Deleted Successfully.", imageUrl: "assets/img/check.png" });

                                    $http.get(ENV.apiUrl + "api/common/Medical_Immunization/GetSims_MedicalImmunization").then(function (res) {
                                        $scope.display = false;
                                        $scope.grid = true;
                                        $scope.items = res.data;
                                        $scope.totalItems = $scope.items.length;
                                        $scope.todos = $scope.items;
                                        $scope.makeTodos();
                                    });
                                }
                                else if (res.data==false) {
                                    swal({  text: "Record Not Deleted." + $scope.ins, imageUrl: "assets/img/notification-alert.png" });

                                }
                                else {
                                    swal("Error-" + $scope.ins)
                                }
                            });

                        }
                        else {
                            main = document.getElementById('chk_min');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_library_fee_number + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }

                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }

                $scope.currentPage = str;
                $scope.row1 = '';
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.update1 = false;
                $scope.save1 = false;
                $scope.editflg = false;
            }


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {

                $scope.todos = $scope.searched($scope.items, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.items;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_immunization_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_immunization_dosage1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_immunization_dosage2.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_immunization_dosage3.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_immunization_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;

            }

        }]);

})();