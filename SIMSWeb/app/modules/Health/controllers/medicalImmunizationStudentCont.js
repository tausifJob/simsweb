﻿(function () {
    'use strict';
    var main
    var simsController = angular.module('sims.module.Health');
    simsController.controller('medicalImmunizationStudentCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.display = false;
            $scope.not_editable = true;
            $scope.grid = true;
            //$scope.itemsPerPage = "5";
            //$scope.currentPage = 0;

            $scope.pagesize = "10";

            $scope.items = [];
            $scope.url = ENV.apiUrl + 'Content' + '/' + $http.defaults.headers.common['schoolId'];
            var photofile;
            var edt1 = '';
            var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');

           

            $scope.read = true;
            
            $rootScope.visible_stud = true;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = false;
            $rootScope.chkMulti = false;


            $scope.Search = function () {
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }

            /*$http.get(ENV.apiUrl + "api/common/Medical_Immunization/GetSims_MedicalImmunization").then(function (res) {
                $scope.cmbImmunization = res.data;
            });*/

            $scope.CmbimmunizationChange = function (str) {

                var immunization_desc = $.grep($scope.cmbImmunization, function (fruit) {
                    return fruit.sims_immunization_code == str;
                })[0].sims_immunization_desc;

                var sims_immunization_age_group = $.grep($scope.cmbImmunization, function (fruit) {
                    return fruit.sims_immunization_code == str;
                })[0].sims_immunization_age_group_code;
                $scope.edt['sims_immunization_desc'] = immunization_desc;
                $scope.edt['sims_immunization_age_group_code'] = sims_immunization_age_group;
            }

            $http.get(ENV.apiUrl + "api/common/Medical_Immunization/GetSims_MedicalImmunization").then(function (res) {
                $scope.cmbImmunization = res.data;
            });

            $scope.CmbimmunizationChange = function (str) {
               

                var immunization_desc = $.grep($scope.cmbImmunization, function (fruit) {
                    return fruit.sims_immunization_code == str;
                })[0].sims_immunization_desc;

                var sims_immunization_age_group = $.grep($scope.cmbImmunization, function (fruit) {
                    return fruit.sims_immunization_code == str;
                })[0].sims_immunization_age_group_code;

                $scope.edt['sims_immunization_desc'] = immunization_desc;
                $scope.edt['sims_immunization_age_group_code'] = sims_immunization_age_group;
            }

            $http.get(ENV.apiUrl + "api/common/Medical_Immunization/GetAllMedicalImmunizationAgeGroupCode").then(function (res) {
                $scope.cmbgroupCode = res.data;
            });

            //$http.get(ENV.apiUrl + "api/common/MedicalExamination/GetAllMedicalExaminationName").then(function (res) {

            //    $scope.cmbExaminationName = res.data;

            //});
            /*
            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;

            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.items.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;
            };
            */
            $scope.changDose = function (str) {
                debugger;
                $scope.edt.sims_immunization_dosage1 = ' ';
                $scope.edt.sims_immunization_dosage2 = ' ';
                $scope.edt.sims_immunization_dosage3 = ' ';
                $scope.edt.sims_immunization_dosage4 = ' ';
                $scope.edt.sims_immunization_dosage5 = ' ';
                $http.get(ENV.apiUrl + "api/Medical_Immunization_Student/getDosage?str=" + str).then(function (res) {
                    $scope.Dosage = res.data;

                    $scope.edt.sims_immunization_dosage1 = $scope.Dosage[0].sims_immunization_dosage1;
                    $scope.edt.sims_immunization_dosage2 = $scope.Dosage[0].sims_immunization_dosage2;
                    $scope.edt.sims_immunization_dosage3 = $scope.Dosage[0].sims_immunization_dosage3;
                    $scope.edt.sims_immunization_dosage4 = $scope.Dosage[0].sims_immunization_dosage4;
                    $scope.edt.sims_immunization_dosage5 = $scope.Dosage[0].sims_immunization_dosage5;
                });

            }
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.CheckMultiple();
                }
            };
 

            $http.get(ENV.apiUrl + "api/Medical_Immunization_Student/GetStudent_Medical_Immunization").then(function (res) {
             
                $scope.display = false;
                $scope.grid = true;
                $scope.items = res.data;
                $scope.totalItems = $scope.items.length;
                $scope.todos = $scope.items;
                $scope.makeTodos();
            });
            
            $scope.size = function (str) {
                /*console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }*/
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }

                if (str == "All") {
                    $scope.currentPage = 1;
                    $scope.numPerPage = $scope.items.length;
                    $scope.makeTodos();

                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
            }

            $scope.edit = function (str) {
               
                $scope.read = true;
                $scope.btn_se = true;
                $scope.edt = {
                    'sims_immunization_age_group_code': str.sims_immunization_age_group_code,
                    'sims_immunization_code': str.sims_immunization_code,
                    'sims_immunization_desc': str.sims_immunization_desc,
                    'sims_immunization_dosage1': str.sims_immunization_dosage1,
                    'sims_immunization_dosage2': str.sims_immunization_dosage2,
                    'sims_immunization_dosage3': str.sims_immunization_dosage3,
                    'sims_immunization_dosage4': str.sims_immunization_dosage4,
                    'sims_immunization_dosage5': str.sims_immunization_dosage5,
                    'sims_vaccination_date': str.sims_vaccination_date,
                    'sims_vaccination_renewal_date': str.sims_vaccination_renewal_date

                }
                $scope.edt1 = {
                    'sims_enroll_number': str.sims_enroll_number
                }

                $scope.display = true;
                $scope.grid = false;
                $scope.update1 = true;
                $scope.save1 = false;

                $scope.editflg = true;
                //$scope.curChange(str.sims_timetable_cur);
                //$scope.academicChange(str.sims_academic_year);
                //$scope.gradeChange(str.sims_grade);
                //  $scope.edt = str;


            }

            $scope.Update = function (newEventForm) {
                if (newEventForm.$valid) {
                  
                    var updateData = [];
                    $scope.edt = {
                        'sims_enroll_number': $scope.edt1.sims_enroll_number.split('(')[0],
                        'sims_immunization_age_group_code': $scope.edt.sims_immunization_age_group_code,
                        'sims_immunization_code': $scope.edt.sims_immunization_code,
                        'sims_vaccination_date': $scope.edt.sims_vaccination_date,
                        'sims_vaccination_renewal_date': $scope.edt.sims_vaccination_renewal_date,
                        'sims_immunization_desc': $scope.edt.sims_immunization_desc,
                        'sims_immunization_dosage1': $scope.edt.sims_immunization_dosage1,
                        'sims_immunization_dosage2': $scope.edt.sims_immunization_dosage2,
                        'sims_immunization_dosage3': $scope.edt.sims_immunization_dosage3,
                        'sims_immunization_dosage4': $scope.edt.sims_immunization_dosage4,
                        'sims_immunization_dosage5': $scope.edt.sims_immunization_dosage5,
                        'opr': 'U'
                    }

                    var data = $scope.edt;
                    updateData.push(data);
                    $http.post(ENV.apiUrl + "api/Medical_Immunization_Student/InsertStudent_Medical_Immunization", updateData).then(function (res) {
                        $scope.ins = res.data;
                        if (res.data==true) {
                            swal({  text: "Record Updated Successfully.", imageUrl: "assets/img/check.png" });
                            $http.get(ENV.apiUrl + "api/Medical_Immunization_Student/GetStudent_Medical_Immunization").then(function (res) {
                                $scope.display = false;
                                $scope.grid = true;
                                $scope.items = res.data;
                                $scope.currentPage = 0;
                            });

                        }
                        else if (res.data == false) {
                            swal({ text: "Record Not Updated .", imageUrl: "assets/img/close.png" });
                        }
                        else {
                            swal("Error-" + $scope.ins)
                        }
                    });
                    $scope.cancel();
                }
            }

            $scope.Save = function (newEventForm) {
              
                if (newEventForm.$valid) {
                    var savedata = [];
                    $scope.edt = {
                        'sims_enroll_number': $scope.edt1.stud_number,
                        'sims_immunization_age_group_code': $scope.edt.sims_immunization_age_group_code,
                        'sims_immunization_code': $scope.edt.sims_immunization_code,
                        'sims_vaccination_date': $scope.edt.sims_vaccination_date,
                        'sims_vaccination_renewal_date': $scope.edt.sims_vaccination_renewal_date,
                        'sims_immunization_desc': $scope.edt.sims_immunization_desc,
                        'sims_immunization_dosage1': $scope.edt.sims_immunization_dosage1,
                        'sims_immunization_dosage2': $scope.edt.sims_immunization_dosage2,
                        'sims_immunization_dosage3': $scope.edt.sims_immunization_dosage3,
                        'sims_immunization_dosage4': $scope.edt.sims_immunization_dosage4,
                        'sims_immunization_dosage5': $scope.edt.sims_immunization_dosage5,
                        'opr': 'I'
                    }

                    var data = $scope.edt;
                    //data.opr = 'I';
                    savedata.push(data);
                    $http.post(ENV.apiUrl + "api/Medical_Immunization_Student/InsertStudent_Medical_Immunization", savedata).then(function (res) {
                        $scope.ins = res.data;
                        if (res.data==true) {
                            swal({  text: "Record Inserted Successfully.", imageUrl: "assets/img/check.png" });
                            $http.get(ENV.apiUrl + "api/Medical_Immunization_Student/GetStudent_Medical_Immunization").then(function (res) {
                                $scope.display = false;
                                $scope.grid = true;
                                $scope.items = res.data;
                                $scope.totalItems = $scope.items.length;
                                $scope.todos = $scope.items;
                                $scope.makeTodos();
                            });

                        }
                        else if (res.data == false) {
                            swal({ text: "Record Not Inserted.", imageUrl: "assets/img/close.png" });

                        }
                        else {
                            swal("Error-" + $scope.ins)
                        }
                    });
                    $scope.cancel();
                }
            }

            $scope.New = function () {
                $scope.edt = "";
                $scope.edt1 = "";
                $scope.grid = false;
                $scope.display = true;
                $scope.save1 = true;
                $scope.editflg = false;
                $scope.update1 = false;
                $scope.btn_se = false;
                $scope.edt = {
                    sims_vaccination_date: dateyear,
                    sims_vaccination_renewal_date: dateyear
                }
                //$scope.edt.sims_reference_status = true;

            }

            var main, del = '', delvar = [];
            $scope.chk = function () {

                main = document.getElementById('chk_min');
                del = [];
                if (main.checked == true) {
                    for (var i = 0; i < $scope.items.length; i++) {
                        $scope.items[i].sims_immunization_code1 = true;


                    }
                }
                else {
                    for (var i = 0; i < $scope.items.length; i++) {
                        $scope.items[i].sims_immunization_code1 = false;

                        main.checked = false;

                        $scope.row1 = '';
                    }
                }


            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.items.length; i++) {
                        var v = document.getElementById(i + $scope.items[i].sims_immunization_code);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.items.length; i++) {
                        var v = document.getElementById(i + $scope.items[i].sims_immunization_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.Delete = function () {
                debugger;
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.items.length; i++) {
                    var v = document.getElementById(i + $scope.items[i].sims_immunization_code);
                    $scope.sims_enroll_number = $scope.items[i].sims_enroll_number.split('(')[0];
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_immunization_code': $scope.items[i].sims_immunization_code,
                            'sims_enroll_number': $scope.sims_enroll_number,
                            'sims_immunization_age_group_code': $scope.items[i].sims_immunization_age_group_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/Medical_Immunization_Student/InsertStudent_Medical_Immunization", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Medical_Immunization_Student/GetStudent_Medical_Immunization").then(function (res) {
                                                $scope.display = false;
                                                $scope.grid = true;
                                                $scope.items = res.data;
                                                $scope.totalItems = $scope.items.length;
                                                $scope.todos = $scope.items;
                                                $scope.makeTodos();
                                               
                                            });
                                            //main = document.getElementById('mainchk');
                                            //if (main.checked == true) {
                                            //    main.checked = false;
                                            //    {
                                            //        $scope.row1 = '';
                                            //    }
                                            //}
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Medical_Immunization_Student/GetStudent_Medical_Immunization").then(function (res) {
                                                $scope.display = false;
                                                $scope.grid = true;
                                                $scope.items = res.data;
                                                $scope.currentPage = 0;
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.ins)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.items.length; i++) {
                                var v = document.getElementById(i + $scope.items[i].sims_immunization_code);
                                if (v.checked == true) {
                                    v.checked = false;
                                    //  $scope.row1 = '';
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.update1 = false;
                $scope.save1 = false;
                $scope.editflg = false;
                $scope.edt = '';
                $scope.edt1 = '';
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd-mm-yyyy',

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.$on('global_cancel', function () {
               
                if ($scope.SelectedUserLst.length > 0) {
                    
                    $scope.edt1 =
                        {
                            studName: $scope.SelectedUserLst[0].name,
                            stud_number: $scope.SelectedUserLst[0].s_enroll_no
                        }
                }
                $scope.edt1.sims_enroll_number = $scope.edt1.studName
            });

            //=====================================================
            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {

                $scope.todos = $scope.searched($scope.items, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.items;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
            /* Search Text in all 3 fields */
            return (item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_immunization_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_immunization_age_group_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_immunization_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_immunization_dosage4.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_immunization_dosage2.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_immunization_dosage3.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_immunization_dosage1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }



        }]);

})();


