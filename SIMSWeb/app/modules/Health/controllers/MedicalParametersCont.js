﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Health');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('MedicalParametersCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.display = false;
            $scope.save1 = false;
            $scope.update1 = false;
            $scope.cancel1 = false;
            $scope.table1 = true;
            $scope.fee_no = true;
            $scope.table = true;
            $scope.visit_no = true;
            var check_no = null;
            var drug = null;
            $scope.chku = false;
            $scope.stud_info = false;
            $scope.Emp_info = false;
            $scope.emp_pre = false;
            $scope.stud_pre = false;
            $scope.Emp_number = false;
            $scope.Stud_number = false;
            $scope.ref_user = false;
            $scope.edt = {};
            $scope.user = $rootScope.globals.currentUser.username;
            $scope.edt12 = {};
            $scope.edte = {};
            $scope.temp = {};


            var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.dt = {
                sims_from_date: dateyear,
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.showdata = function () {
                $scope.display = false;
                $scope.table = true;
                $scope.table1 = true;
                
                $http.get(ENV.apiUrl + "api/MedicalParametersController/getMedicalParameters").then(function (res1) {
                    debugger;
                    $scope.obj = res1.data;
                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.makeTodos();
                    if ($scope.obj.length == 0) {
                        swal({ title: 'Alert', text: "No Record Found...", showCloseButton: true, width: 450, height: 200 });
                        $scope.display = false;
                        $scope.table = false;
                        $scope.table1 = true;
                    }
                    else {
                        $scope.display = false;
                        $scope.table = true;
                        $scope.table1 = true;
                    }
                })
            }

            $scope.showdata();

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {

                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_health_parameter_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_health_parameter_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;

            }



          

            $scope.New = function () {
                debugger;
                
                $scope.no_selection = false;
                $scope.status = true;
                var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');
                $scope.dt = {
                    sims_from_date: dateyear,
                }

                $scope.edt['medical_attended_by'] = $rootScope.globals.currentUser.username;

                $http.get(ENV.apiUrl + "api/MedicalParametersController/getAutoGenerateMedicalparameterNo").then(function (res) {
                    $scope.temp1 = {
                        sims_health_parameter_code: res.data,
                    }
                });

             

                
                $scope.clearvalue();
                $scope.display = true;
                $scope.table = false;
                $scope.table1 = false;
                $scope.instaminimuma = true;
                $scope.update1 = false;
                $scope.save1 = true;
                $scope.add_btn = true;
                $scope.Table2 = true;
                $scope.MedicalVisiteArray = [];
                $scope.rm_edt = true;


                 
            }
            $scope.MedicalVisiteArray = [];
            $scope.AddMedicine = function () {
                debugger;
                var obj = [];
                
                if ($scope.temp1.sims_health_parameter_code == undefined) {
                        obj = {
                            sims_health_parameter_check_name_en: $scope.edt6.sims_health_parameter_check_name_en,
                            sims_health_parameter_check_min_value: $scope.edt6.sims_health_parameter_check_min_value,
                            sims_health_parameter_check_max_value: $scope.edt6.sims_health_parameter_check_max_value,
                            sims_health_parameter_check_comment: $scope.edt6.sims_health_parameter_check_comment,

                            sims_health_parameter_code: $scope.temp1.sims_health_parameter_code,
                        }
                    }
                    else {
                        obj = {
                            sims_health_parameter_check_name_en: $scope.edt6.sims_health_parameter_check_name_en,
                            sims_health_parameter_check_min_value: $scope.edt6.sims_health_parameter_check_min_value,
                            sims_health_parameter_check_max_value: $scope.edt6.sims_health_parameter_check_max_value,
                            sims_health_parameter_check_comment: $scope.edt6.sims_health_parameter_check_comment,

                            sims_health_parameter_code: $scope.temp1.sims_health_parameter_code,
                        }
                    }

                    $scope.MedicalVisiteArray.push(obj);
                    $scope.edt6.sims_health_parameter_check_name_en = "";
                    $scope.edt6.sims_health_parameter_check_min_value = "";
                    $scope.edt6.sims_health_parameter_check_max_value = "";
                    $scope.edt6.sims_health_parameter_check_comment = "";

            }

            // remove
            //$scope.Remove = function ($event, index, str) {
            //    str.splice(index, 1);
            //}
            //checked one by one
            var ind;
            var data = [];


            $scope.CheckInstallment = function () {
                debugger
                if ($scope.edt.sims_library_fee_installment_mode == true) {
                    $scope.instaminimuma = false;
                }
                else {
                    $scope.instaminimuma = true;
                }
            }

            $scope.obj1 = [];
            $scope.obj2 = [];
            debugger

            //check all 
            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk11');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            //checked one by one
            var ind;
            var data = [];
            $scope.checkonebyonedelete = function (str, index) {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                        //str.status = true;
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                        //str.status = false;
                    }
                });
                str.status = str.datastatus;
                ind = index;
                //data = str;
            }

             

            //Save data

            $scope.submit = function (isvalid) {
                debugger;
                $scope.res = {};
               
                if (isvalid) {
                    $scope.stud_info = false;
                    $scope.Emp_info = false;
                    debugger;
                    $scope.res['med_det'] = [];
                    var Savedata = [];
                    var rt_code = [];

                    console.log($scope.res);
                    $scope.res.opr = 'I';
                    $scope.res['sims_health_parameter_code'] = $scope.temp1.sims_health_parameter_code,
                     $scope.res['sims_health_parameter_name_en'] = $scope.temp1.sims_health_parameter_name_en,
                     $scope.res['sims_health_parameter_min_value'] = $scope.temp1.sims_health_parameter_min_value,
                     $scope.res['sims_health_parameter_max_value'] = $scope.temp1.sims_health_parameter_max_value,
                   
                     $scope.res['sims_health_parameter_default_value'] = $scope.temp1.sims_health_parameter_default_value,
                     $scope.res['sims_health_parameter_unit'] = $scope.temp1.sims_health_parameter_unit,
                     $scope.res['sims_health_parameter_display_order'] = $scope.temp1.sims_health_parameter_display_order,
                     $scope.res['sims_health_parameter_isNumeric'] = $scope.temp1.sims_health_parameter_isNumeric,

                     $scope.res['user'] = $scope.user
                

                    //send.push($scope.Rating);
                    // if (Myform) {


                    for (var i = 0; i < $scope.MedicalVisiteArray.length; i++) {
                        //var v = document.getElementById($scope.MedicalVisiteArray[i].sims_medical_visit_number + i);
                        //if (v.checked == true) {

                        rt_code = {
                            'sims_health_parameter_check_name_en': $scope.MedicalVisiteArray[i].sims_health_parameter_check_name_en,
                            'sims_health_parameter_check_min_value': $scope.MedicalVisiteArray[i].sims_health_parameter_check_min_value,
                            'sims_health_parameter_check_max_value': $scope.MedicalVisiteArray[i].sims_health_parameter_check_max_value,
                            'sims_health_parameter_check_comment': $scope.MedicalVisiteArray[i].sims_health_parameter_check_comment,

                            'sims_health_parameter_code': $scope.temp1.sims_health_parameter_code,
                            'user' : $scope.user,
                        }
                        $scope.res['med_det'].push(rt_code);
                        $scope.flag = true;

                        //}

                        //}


                        // $scope.flag = true;


                        if ($scope.flag == false) {
                            swal('', 'Please select the records to Insert');
                            return;
                        }

                    }

                    $http.post(ENV.apiUrl + "api/MedicalParametersController/CUDSaveMedicalParameter", $scope.res).then(function (res) {
                        console.log(res);
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.showdata();
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.showdata();
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                    $scope.display = false;
                    $scope.table = true;
                    $scope.showdata();

                }
            }

            //cancel
            $scope.cancel = function () {
                $scope.showdata();
                $scope.display = false;
                $scope.table = true;
                $scope.table1 = true;
                $scope.clearvalue();
                $scope.stud_info = false;
                $scope.Emp_info = false;
                $scope.enroll_no = false;
                $scope.stud_pre = false;
                $scope.employee_no = false;
                $scope.emp_pre = false;
                $scope.MedicalVisiteArray = [];

            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_medical_visit_number + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_medical_visit_number + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyoneselect = function () {

                $("input[type='checkbox']").change(function (e) {

                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.edit = function (j) {
                debugger;
                $scope.no_selection = true;
                $scope.visit_no = true;
                $scope.Emp_number = true;
                $scope.Stud_number = true;
                $scope.update1 = true;
                $scope.ref_user = true;
                $scope.save1 = false;
                $scope.Table2 = true;
                $scope.add_btn = true;
                $scope.status = false;
                $scope.status1 = true;
                $scope.rm_edt = true;
                 
                // $scope.edt = j;
                $scope.MedicalVisiteArray = [];

                
                $http.post(ENV.apiUrl + "api/MedicalParametersController/parameterDetails?vino=" + j.sims_health_parameter_code).then(function (rate_data) {
                    var temp_medtype = rate_data.data;
                    $scope.MedicalVisiteArray = temp_medtype;


                    $scope.MedicalVisiteArray['sims_health_parameter_code'] = j.sims_health_parameter_code

                    $scope.temp1 = {
                        'sims_health_parameter_code': j.sims_health_parameter_code,
                        'sims_health_parameter_name_en': j.sims_health_parameter_name_en,
                        'sims_health_parameter_min_value': j.sims_health_parameter_min_value,
                        'sims_health_parameter_max_value': j.sims_health_parameter_max_value,
                        'sims_health_parameter_default_value': j.sims_health_parameter_default_value,
                        'sims_health_parameter_unit': j.sims_health_parameter_unit,
                        'sims_health_parameter_display_order': j.sims_health_parameter_display_order,
                        'sims_health_parameter_isNumeric': j.sims_health_parameter_isNumeric,

                    }
                     

                    


                    //var a = [];
                    //var b = j.medical_visit_date;
                    //a = b.split(" ");
                    //$scope.dt['sims_from_date'] = a[0];
 
                    $scope.display = true;
                    $scope.table = false;
                    $scope.table1 = false;
                })
            }

            $scope.Update = function () {
                debugger
                //$scope.send = [];
                //$scope.res['med_det'] = [];
                var send = [];
                var rt_code = [];

                console.log($scope.res);
                if ($scope.MedicalVisiteArray.length == '0') {

                    var rt_code = ({
                        'sims_health_parameter_code': $scope.temp1.sims_health_parameter_code,
                         'sims_health_parameter_name_en': $scope.temp1.sims_health_parameter_name_en,
                        'sims_health_parameter_min_value': $scope.temp1.sims_health_parameter_min_value,
                        'sims_health_parameter_max_value': $scope.temp1.sims_health_parameter_max_value,
                        'sims_health_parameter_default_value': $scope.temp1.sims_health_parameter_default_value,
                        'sims_health_parameter_unit': $scope.temp1.sims_health_parameter_unit,
                        'sims_health_parameter_display_order': $scope.temp1.sims_health_parameter_display_order,
                        'sims_health_parameter_isNumeric': $scope.temp1.sims_health_parameter_isNumeric,
                        'user' : $scope.user,

                        opr: 'U',
                    });
                    send.push(rt_code);



                }
                     
                else {

                    for (var i = 0; i < $scope.MedicalVisiteArray.length; i++) {
                         

                        rt_code = {
                            'sims_health_parameter_code': $scope.temp1.sims_health_parameter_code,
                            'sims_health_parameter_name_en': $scope.temp1.sims_health_parameter_name_en,
                            'sims_health_parameter_min_value': $scope.temp1.sims_health_parameter_min_value,
                            'sims_health_parameter_max_value': $scope.temp1.sims_health_parameter_max_value,
                            'sims_health_parameter_default_value': $scope.temp1.sims_health_parameter_default_value,
                            'sims_health_parameter_unit': $scope.temp1.sims_health_parameter_unit,
                            'sims_health_parameter_display_order': $scope.temp1.sims_health_parameter_display_order,
                            'sims_health_parameter_isNumeric': $scope.temp1.sims_health_parameter_isNumeric,
                            opr: 'U',

                            'sims_health_parameter_check_name_en': $scope.MedicalVisiteArray[i].sims_health_parameter_check_name_en,
                            'sims_health_parameter_check_min_value': $scope.MedicalVisiteArray[i].sims_health_parameter_check_min_value,
                            'sims_health_parameter_check_max_value': $scope.MedicalVisiteArray[i].sims_health_parameter_check_max_value,
                            'sims_health_parameter_check_comment': $scope.MedicalVisiteArray[i].sims_health_parameter_check_comment,
                            'sims_health_parameter_check_code': $scope.MedicalVisiteArray[i].sims_health_parameter_check_code,
                            'user': $scope.user,
                        }
                        send.push(rt_code);
                        $scope.flag = true;
                    }
                 


                    if ($scope.flag == false) {
                        swal('', 'Please select the records to Insert');
                        return;
                    }

                }
                 

                $http.post(ENV.apiUrl + "api/MedicalParametersController/CUDMedicalParameter", send).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        $scope.showdata();
                    }
                    else {
                        swal({ text: "Record Not Updated", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                    }
                    $scope.clearvalue();
                });

            }

            $scope.OkDelete = function () {
                debugger;

                var data1 = [];
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_health_parameter_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_health_parameter_code': $scope.filteredTodos[i].sims_health_parameter_code,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '', text: "Are you sure you want to Delete?", showCloseButton: true, showCancelButton: true, confirmButtonText: 'Yes', width: 380, cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/MedicalParametersController/CUDMedicalParameter", deleteleave).then(function (msg) {


                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        $scope.showdata();
                                        if (isConfirm) {

                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                            }

                                            $scope.CheckAllChecked();
                                        }
                                        $scope.currentPage = true;
                                    });
                                }

                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_library_fee_number + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }

                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }

                $scope.currentPage = str;
                $scope.row1 = '';
            }

            $scope.clearvalue = function () {

                $scope.temp1 = { sims_medical_visit_number: '', }
                $scope.dt = { sims_from_date: '', }
                $scope.edt = { sims_enroll_number: '' }
                $scope.edte = { sims_employee_number: '', }
                $scope.edt1 = { medical_complaint_desc: '', }
                $scope.edt2 = { medical_attendant_observation: '', }
                $scope.edt3 = { medical_health_education: '', }
                $scope.edt3 = { sims_medical_examination_name: '', }
                $scope.edt4 = { medical_followup_comment: '', }
                $scope.temp2 = { sims_medicine_code: '', }
                $scope.edt5 = {
                    sims_medicine_dosage: '',
                    //sims_medicine_dosage: '',
                    sims_blood_pressure: '',
                    sims_temperature: '',
                    sims_pulse_rate: '',
                    sims_respiratory_rate: '',
                    sims_spo2: '',

                }
                $scope.edt6 = { sims_medicine_duration: '', }
                $scope.edt = { sims_user_code_created_by: '', }
                $scope.edt5 = { sims_status: true, }
                $scope.edtu = { sims_user_code_created_name: '', }
                $scope.searchText = '';
                $scope.MyForm.$setPristine();
                $scope.MyForm.$setUntouched();
            }

           

            var std_code = [];
            $scope.Remove = function ($event, index, str) {
                debugger;
                $scope.std = {};
                var i = 0;
                i = index;
                $scope.std.sims_health_parameter_code = str[i].sims_health_parameter_code;

                $scope.std.sims_health_parameter_check_code = str[i].sims_health_parameter_check_code;


                $scope.std.opr = 'M';

                std_code.push($scope.std);
                $http.post(ENV.apiUrl + "api/MedicalParametersController/CUDMedicalParameter", std_code).then(function (savedata) {
                    $scope.msg1 = savedata.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 300, height: 200 });
                        $scope.grid2 = '';
                        $scope.save_btn = false;
                      
                       
                        $http.post(ENV.apiUrl + "api/MedicalParametersController/parameterDetails?vino=" + $scope.std.sims_health_parameter_code).then(function (rate_data) {
                            var temp_medtype = rate_data.data;
                            $scope.MedicalVisiteArray = temp_medtype;
                        });
                    }
                    else {
                        //swal({ title: "Alert", text: "Record not saved.", showCloseButton: true, width: 300, height: 200 });
                        str.splice(index, 1);
                    }
                });
                std_code = [];
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

        }])
})();
