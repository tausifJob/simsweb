﻿/// <reference path="../../HRMS/controller/AttendanceMachine.js" />

(function () {

    'use strict';
    var temp = [];
    var simsController = angular.module('sims.module.Health');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('campFileUploadCont',
      ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
          $scope.temp = {};
          $scope.edt = {};
          var insertDetailData = [];
          var uname = $rootScope.globals.currentUser.username;
          $scope.expand_object = false;

          var today = new Date();
          today = ('0' + today.getDate()).slice(-2) + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + today.getFullYear();
          $scope.temp["to_date"] = today
          $scope.temp["from_date"] = today
          $scope.temp["issue_creation_date"] = today
          $scope.hide_submit = false;
          $scope.showDetails = false;
          $scope.showStudDetails = false;
          $scope.submitBtnDisabled = false;
          $scope.hide_grid = false;
          $scope.busyindicator = true;
          $scope.hide_save = true;
          $scope.loadingscr = true;
          // $scope.uploadImgClickFShow = true;
          var arr_files = [];
          $scope.images = [];
          $scope.images_delete = [];
          var comn_files = "";

          $scope.getcur = function () {
              $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                  $scope.curriculum = AllCurr.data;
                  $scope.temp.sims_cur_code = $scope.curriculum[0].sims_cur_code;
                  $scope.getAccYear($scope.temp.sims_cur_code);
              });
          }

          $scope.getcur();

          $scope.getAccYear = function (curCode) {
              $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                  $scope.Acc_year = Acyear.data;
                  $scope.temp.sims_academic_year = $scope.Acc_year[0].sims_academic_year;
                  $scope.getCampDetails();
              });
          }

          $scope.getCampDetails = function () {
              $http.get(ENV.apiUrl + "api/CampTransaction/getCampNameForFileUpload?username=" + $rootScope.globals.currentUser.username + "&cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (res) {
                  $scope.campDetails = res.data;
                  console.log("campDetails", $scope.campDetails);
              });
          }

          $scope.getTestNames = function (obj) {
              debugger;
              $scope.showDetails = true;
              console.log("obj", obj);
              console.log("json obj", JSON.parse(obj));
              var ob = JSON.parse(obj);
              $scope.edt.sims_health_package_code = ob.sims_health_package_code;
              $scope.edt.sims_health_camp_code = ob.sims_health_camp_code;

              $http.get(ENV.apiUrl + "api/CampTransaction/getTestName?package_code=" + ob.sims_health_package_code + "&camp_code=" + ob.sims_health_camp_code).then(function (res) {
                  $scope.testNames = res.data;
                  console.log("testNames", $scope.testNames);
              });

          }
          var formdata = new FormData();
          //$scope.uploadImgClickF = function () {
          //    formdata = new FormData();
          //    $scope.uploadImgClickFShow = true;
          //}

          $scope.uploadImgCancel = function () {
              $scope.uploadImgClickFShow = false;
              arr_files = [];
              $scope.images = [];
              // $scope.showalbum_details($scope.edt.sims_academic_year);;
          }

          $scope.getTheFiles = function ($files) {
              debugger
              var i = 0;

              var exists = "no";

              angular.forEach($files, function (value, key) {
                  formdata.append(key, value);
                  if ($files[i].size > 2097152) {
                      $rootScope.strMessage = $files[i].name + " exceeds file size limit.";
                      $('#message').modal('show');
                  }
                  else {
                      arr_files.push($files[i].name);
                      if ($scope.images.length > 0) {
                          for (var j = 0 ; j < $scope.images.length; j++) {
                              if ($scope.images[j].name == $files[i].name) {
                                  exists = "yes";
                                  return;
                              }

                          }

                          if (exists == "no") {
                              $scope.images.push({
                                  name: $files[i].name,
                                  file: $files[i].size
                              });

                          }
                          exists = "no";
                      }
                      else {
                          $scope.images.push({
                              name: $files[i].name,
                              file: $files[i].size
                          });
                      }
                      $scope.$apply();

                      // console.log($scope.images);

                  }
                  i = i + 1;
              });
          };

          var filesDataArray = [];
          $scope.file_changed = function (element) {
              debugger;
              $scope.uploadImgClickFShow = true;
              var photofile = element.files[0];
              var v = photofile.name.indexOf('.');
              $scope.file_name = photofile.name.substr(0, v);
              //$scope.fileext = photofile.name.substr(v + 1);
              //$scope.photo_filename = (photofile.type);

              var files = new FormData();
              //var fileArray = document.getElementsByName('file');
              //console.log("fileArray ", fileArray);
              filesDataArray = $('#file1')[0].files;
              console.log("filesData ", filesDataArray);
          };


          $scope.uploadImg1 = function (myform) {
              debugger;
              //$('#loader712').modal({ backdrop: 'static', keyboard: false });
              if (myform) {

                  $scope.loadingscr = false;
                  var sendData = [];
                  for (var j = 0 ; j < $scope.images.length; j++) {
                      comn_files = comn_files + $scope.images[j].name + ",";

                      var obj = {
                          file_name: comn_files,
                          sims_health_camp_code: $scope.edt.sims_health_camp_code,
                          sims_health_enroll_number: $scope.images[j].name.split('_')[2],
                          sims_health_test_code: $scope.temp.sims_health_test_code,
                          sims_health_test_file: $scope.images[j].name,
                          sims_health_test_file_uploaded_by: $rootScope.globals.currentUser.username,
                          sims_health_file_status: 'A'
                      }
                      sendData.push(obj);
                  }
                  console.log("sendData", sendData);

                  var request = {
                      method: 'POST',
                      //url: ENV.apiUrl + '/api/file/uploadmuti?filename=' + 'gallery' + "&location=" + "Images/PhotoGallery/" + "&value=" + comn_files + "&delimages" + $scope.images_delete,
                      url: ENV.apiUrl + '/api/file/uploadmultiple?filename=' + "&location=" + "/Images/CampDocuments/" + "&value=" + comn_files + "&delimages" + $scope.images_delete,
                      data: formdata,
                      headers: {
                          'Content-Type': undefined
                      }
                  };

                  $http(request).success(function (d) {
                      $scope.loadingscr = true;
                      $scope.albumPath = d[0];
                      console.log("path", $scope.albumPath);

                      $http.post(ENV.apiUrl + "api/CampTransaction/InsertCampDocuments", sendData).then(function (res) {

                          if (res.data == true) {

                              swal({ text: "file Uploaded Successfully", imageUrl: "assets/img/check.png" });
                              arr_files = [];
                              $scope.images = [];

                              $http.get(ENV.apiUrl + "api/CampTransaction/getCampDocuments?sims_health_camp_code=" + $scope.edt.sims_health_camp_code + "&sims_health_test_code=" + $scope.temp.sims_health_test_code).then(function (res) {
                                  $scope.photo = res.data;
                                  console.log($scope.photo);
                              });
                          }
                          else if (res.data == false) {
                              swal({ text: " Not Updated ", imageUrl: "assets/img/close.png" });
                          }
                          else {
                              swal("Error-" + res.data)
                          }
                      });
                      // }

                      $scope.uploadImgClickFShow = false;




                  });
              }
              else {
                  swal({ text: "Please select all fields",imageUrl: "assets/img/close.png" });
              }
          }

          $scope.uploadImg = function (myform) {
              debugger;
              //$('#loader712').modal({ backdrop: 'static', keyboard: false });
              if (myform) {

                  $scope.loadingscr = false;
               
                 
                  var fileUploadCount = 0;

                  for(var i = 0; i < filesDataArray.length; i++) {
                      var sendData = [];
                      var files = new FormData();

                          var obj = {
                              //file_name: comn_files,
                              sims_health_camp_code: $scope.edt.sims_health_camp_code,
                              sims_health_enroll_number: filesDataArray[i].name.split('_')[2],
                              sims_health_test_code: $scope.temp.sims_health_test_code,
                              sims_health_test_file: filesDataArray[i].name,
                              sims_health_test_file_uploaded_by: $rootScope.globals.currentUser.username,
                              sims_health_file_status: 'A'
                          }
                          sendData.push(obj);
                          console.log("sendData", sendData);

                          files.append(filesDataArray[i].name, filesDataArray[i], filesDataArray[i].name.replace(/\s/g, ""));

                          $scope.callFileUpoadAPI(filesDataArray[i].name.split('.')[0], sendData, fileUploadCount, files);
                     
                  }

              }
              else {
                  swal({ text: "Please select all fields", imageUrl: "assets/img/close.png" });
              }
          }

          $scope.callFileUpoadAPI = function (filename, sendData, fileUploadCount, files) {
              debugger;
              var request = {
                  method: 'POST',
                  url: ENV.apiUrl + 'api/file/uploadCampDocument?filename=' + filename + "&location=" + "CampDocuments",
                  data: files,
                  headers: {
                      'Content-Type': undefined
                  }
              };

              $http(request).success(function (res) {
                  console.log("file upload res", res);

                  if (res != 'false') {

                      $http.post(ENV.apiUrl + "api/CampTransaction/InsertCampDocuments", sendData).then(function (res) {

                          fileUploadCount = fileUploadCount + 1;
                          console.log("fileUploadCount", fileUploadCount);
                          debugger;
                          if (fileUploadCount == filesDataArray.length) {
                              if (res.data == true) {
                                  swal({ text: "file Uploaded Successfully", imageUrl: "assets/img/check.png" });
                                  arr_files = [];
                                  $scope.images = [];
                                  $scope.loadingscr = true;
                              }                              
                              else if (res.data == false) {
                                      swal({ text: " Not Updated ", imageUrl: "assets/img/close.png" });
                              }
                              else {
                                  swal("Error-" + res.data);
                              }
                          }
                          
                          $http.get(ENV.apiUrl + "api/CampTransaction/getCampDocuments?sims_health_camp_code=" + $scope.edt.sims_health_camp_code + "&sims_health_test_code=" + $scope.temp.sims_health_test_code).then(function (res) {
                              $scope.photo = res.data;
                              console.log($scope.photo);
                          });
                          $scope.uploadImgClickFShow = false;
                          $scope.loadingscr = true;
                          sendData = [];

                      });
                  }
                  else{
                      swal("Error-" + filename + 'This File Not Uploaded.' );
                  }
              });
          }

          $scope.download = function (str) {
              window.open(ENV.apiUrl + '/content/' + $http.defaults.headers.common['schoolId'] + "/Images/CampDocuments/" + str);
          };

          $scope.getCampDocuments = function () {
              $http.get(ENV.apiUrl + "api/CampTransaction/getCampDocuments?sims_health_camp_code=" + $scope.edt.sims_health_camp_code + "&sims_health_test_code=" + $scope.temp.sims_health_test_code).then(function (res) {
                  $scope.photo = res.data;
                  console.log($scope.photo);
              });
          }

          $scope.DeleteCampDocuments = function (info) {
              var sendDeleteData = [];
              var obj = {
                  sims_health_camp_code: info.sims_health_camp_code,
                  sims_health_enroll_number: info.sims_health_enroll_number,
                  sims_health_csf_sr_no: info.sims_health_csf_sr_no,
                  sims_health_test_code: info.sims_health_test_code,
                  sims_health_csfd_sr_no: info.sims_health_csfd_sr_no
              }
              sendDeleteData.push(obj);

              $http.post(ENV.apiUrl + "api/CampTransaction/CampDocumentsDelete", sendDeleteData).then(function (res) {

                  if (res.data == true) {

                      swal({ text: "File deleted successfully", imageUrl: "assets/img/check.png" });
                      arr_files = [];
                      $scope.images = [];

                      $http.get(ENV.apiUrl + "api/CampTransaction/getCampDocuments?sims_health_camp_code=" + $scope.edt.sims_health_camp_code + "&sims_health_test_code=" + $scope.temp.sims_health_test_code).then(function (res) {
                          $scope.photo = res.data;
                          console.log($scope.photo);
                      });
                  }
                  else if (res.data == false) {
                      swal({ text: " Not Deleted ", imageUrl: "assets/img/close.png" });
                  }
                  else {
                      swal("Error-" + res.data)
                  }
              });
          }


          //$scope.savephoto = function () {
          //    debugger

          //    //if ($scope.sims_album_photo_url == undefined || $scope.sims_album_photo_url == "" || $scope.sims_album_photo_url == null) {
          //    //    $scope.sims_album_photo_url = $scope.albumPath;
          //    //}
          //    var sendData = [];
          //    if ($scope.edt.sims_health_camp_code != undefined) {
          //        $scope.loadingscr = true;
          //        var data = {
          //            opr: 'Q',
          //            sims_health_camp_code: $scope.edt.sims_health_camp_code,
          //            sims_health_enroll_number: enroll,
          //        }

          //        var obj = {
          //            sims_health_test_code: $scope.getDeails[i].sims_health_test_code,
          //            sims_health_test_file: sims_health_test_file,
          //            sims_health_test_file_uploaded_by: $rootScope.globals.currentUser.username,
          //            sims_health_file_status:'A'                    
          //        }
          //        sendData.push(obj);

          //        $http.post(ENV.apiUrl + "api/CampTransaction/InsertCampDocuments?data1=" + JSON.stringify(object), sendData).then(function (res) {

          //            if (res.data == true) {

          //                swal({ text: "Updated Successfully", imageUrl: "assets/img/check.png" });
          //                $scope.getalbums();
          //                $http.get(ENV.apiUrl + "api/common/Album/getPhotoInAlbum?album_name=" + $scope.sims_album_code).then(function (res) {
          //                    $scope.photo = res.data;
          //                    console.log($scope.photo);
          //                });
          //            }
          //            else if (res.data == false) {
          //                swal({ text: " Not Updated ", imageUrl: "assets/img/close.png" });
          //            }
          //            else {
          //                swal("Error-" + res.data)
          //            }
          //        });
          //        // }

          //        $scope.uploadImgClickFShow = false;

          //        //$scope.getalbums();
          //        //$scope.showalbum_details($scope.edt.sims_academic_year);;
          //        //$scope.sims_album_photo_url = '';
          //        //$scope.sims_album_code = '';
          //        $scope.hide_save = true;
          //    }

          //    else
          //        swal({ text: "Please Select Album ", imageUrl: "assets/img/close.png" });
          //}

          $scope.resetData = function () {
              debugger;
              $scope.temp.stud_obj = '';
              $scope.temp.enroll_number = '';
              $scope.hide_submit = false;
              $scope.hide_grid = false;
              $scope.showStudDetails = false;
              $scope.submitBtnDisabled = false;
              //$scope.showDetails = false;
              //$scope.temp.sims_cur_code = $scope.curriculum[0].sims_cur_code;
              //$scope.temp.sims_academic_year = $scope.Acc_year[0].sims_academic_year;

              $("#Selectstud").select2("val", "");

              try {
                  $("#grades").multipleSelect("uncheckAll");
              }
              catch (e) { }
              try {
                  $("#sections").multipleSelect("uncheckAll");
              }
              catch (e) { }
          }

      }])

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])


})();