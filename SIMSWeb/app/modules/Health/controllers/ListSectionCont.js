﻿(function () {

    var simsController = angular.module('sims.module.Health');

    simsController.controller('ListSectionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            
            $scope.tmp = {};
            $scope.tmp.name = " ";
            $scope.edt = {};
            var datasend = [];
            debugger;
            $scope.display = false;
            $scope.table = true;
            $scope.Responsedisplay = false;
            $scope.Table2 = false;

            $scope.pagesize = '10';
            $scope.pageindex ="0";
            $scope.pager = true;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
            
 
            


            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.CreDiv;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

           
           

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };



            //$scope.makeTodos = function () {
            //    var rem = parseInt($scope.totalItems % $scope.numPerPage);
            //    if (rem == '0') {
            //        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
            //    }
            //    else {
            //        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
            //    }

            //    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
            //    var end = parseInt(begin) + parseInt($scope.numPerPage);

            //    console.log("begin=" + begin); console.log("end=" + end);

            //    $scope.filteredTodos = $scope.todos.slice(begin, end);
            //};

            //cancle
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.int = " ";
                $scope.Responsedisplay = false;
                $scope.Table2 = false;
                $scope.getgrid();
            }

            $scope.getParent = function () {

                //getParentbyList dropdownlist
                $http.get(ENV.apiUrl + "api/ListSection/get_parentbyList").then(function (parentlist_data) {
                    $scope.parentbylist = parentlist_data.data;
                    console.log("parentbylist = ", $scope.parentbylist);
                });
            }


            $scope.getAllSection = function () {

                //getallsectionbyList dropdownlist
                $http.get(ENV.apiUrl + "api/ListSection/get_sectionbyList").then(function (parentlist_data) {
                    $scope.sectionBylist = parentlist_data.data;
                    console.log("sectionBylist = ", $scope.sectionBylist);
                });
            }

            //typelist
            $scope.typelist = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/ListSection/get_typebyList").then(function (typelist_data) {
                    debugger;
                    $scope.typebylist = typelist_data.data;
                    console.log($scope.typebylist);

                });
            }

           

            //insert data 
            $scope.save_section = function (Myform) {
                var datasend = [];
                if (Myform) {
                    $scope.data = $scope.int;
                   
                    $scope.data['opr'] = 'I';
                    datasend.push($scope.data);
                    $http.post(ENV.apiUrl + "api/ListSection/CUDListSection", datasend).then(function (res) {

                        $scope.msg1 = res.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });

                            $scope.int = {};
                            $scope.Cancel();
                            //show data
                            $scope.getgrid();
                            $scope.getParent();
                        }
                        else {
                            //swal({ title: "Alert", text: "Record NOt Inserted ", showCloseButton: true, width: 300, height: 200 });
                            swal('', 'Record not inserted');

                        }
                    });
                }
            }

            //  new
            $scope.New = function () {
                var datasend = [];
                $scope.int = "";
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.move = true;
                $scope.Table2 = false;
                $scope.Responsedisplay = false;
                $scope.responseArray = [];
            }

            
            $scope.getgrid = function () {
                     $http.get(ENV.apiUrl + "api/ListSection/get_listsection").then(function (res1) {
                    debugger
                    $scope.CreDiv = res1.data;
                    $scope.totalItems = $scope.CreDiv.length;
                    // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                    $scope.todos = $scope.CreDiv;
                    $scope.makeTodos();
                    $scope.Table2 = false;
                    $scope.Responsedisplay = false;
                });
            }

        
          
            
             //check all 
            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            // onebyone  check
            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }


            //delete
            $scope.OkDelete=function()
 {
                debugger
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = {
                            'sims_checklist_section_number': $scope.filteredTodos[i].sims_checklist_section_number,
                            opr: 'D'
                        }
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete? Delete this field Related all record",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            debugger;
                            $http.post(ENV.apiUrl + "api/ListSection/CUDListSection", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        $scope.getgrid();
                                        if (isConfirm) {
                                           
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                            }

                                            $scope.CheckAllChecked();
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else {
                                    swal({ text: "Record Not Deleted", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main.checked = false;
                                            $('tr').removeClass("row_selected");
                                        }
                                    });
                                }

                            });
                            deletefin = [];
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_checklist_section_number + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }


            //DATA EDIT  
            $scope.edit = function (str) {
                debugger
                $scope.Update_btn = true;
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;

                $scope.divcode_readonly = true;


                debugger
                $scope.int = {
                    sims_checklist_section_number: str.sims_checklist_section_number,
                    sims_checklist_section_name: str.sims_checklist_section_name,
                    sims_checklist_section_parent_number: str.sims_checklist_section_parent_number,
                    sims_checklist_section_name_ot: str.sims_checklist_section_name_ot,
                    sims_checklist_section_desc: str.sims_checklist_section_desc,
                    sims_checklist_section_status: str.sims_checklist_section_status,
                    sims_checklist_section_response_type: str.sims_checklist_section_response_type,
                    sims_response_code: str.sims_response_code,
                    id:str.id,
                }
            }
            


            //DATA UPDATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                if (Myform) {
                    debugger;
                    var data = $scope.int;
                    data.sims_checklist_section_number = $scope.int.sims_checklist_section_number;
                    data.opr = "U";
                    dataforUpdate.push(data);
                    //dataupdate.push(data);
                    $http.post(ENV.apiUrl + "api/ListSection/CUDListSection", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.getgrid();
                        }
                        else {
                            swal({ text: "Record Not Updated", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        }
                        //search data show                        
                        $scope.searchbutton();
                        //$http.get(ENV.apiUrl + "api/demo/get_demo").then(function (res1) {
                        //    $scope.CreDiv = res1.data;
                        //    $scope.totalItems = $scope.CreDiv.length;
                        //    $scope.todos = $scope.CreDiv;
                        //    $scope.makeTodos();

                        //});

                    });
                    dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            //Search
            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };
             $scope.search = function () {
 
                 $scope.todos = $scope.searched($scope.CreDiv, $scope.edt.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_checklist_section_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_checklist_section_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||


                        item.sims_checklist_section_number == toSearch) ? true : false;
            }

            //search dropdown
            $scope.searchdrop = function () {
                if ($scope.edt.sims_checklist_section_parent_number == '' || $scope.edt.sims_checklist_section_parent_number == undefined) {
                    //reset page
                    $scope.getgrid();
                }
                else {

                    $http.post(ENV.apiUrl + "api/ListSection/searchdropdown?str=" + $scope.edt.sims_checklist_section_parent_number).then(function (res2) {
                        debugger
                        $scope.CreDiv = res2.data;
                        $scope.totalItems = $scope.CreDiv.length;
                        // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                        $scope.todos = $scope.CreDiv;
                        $scope.makeTodos();
                    });
                }
            }
            //pagesize

          
            //

            $scope.move = function () {
                var elem = document.getElementById("myBar");
                var width = 10;
                var id = setInterval(frame, 10);
                function frame() {
                    if (width >= 100) {
                        clearInterval(id);
                    } else {
                        width++;
                        elem.style.width = width + '%';
                        elem.innerHTML = width * 1 + '%';
                    }
                }
            }

            

            // search button
            $scope.searchbutton = function () {
                debugger;
                if ($scope.edt.searchText == undefined && $scope.edt.sims_checklist_section_parent_number == undefined) {
                    swal('', 'Please select Section name And Parent Section Name ');

                }
                else {

                    console.log($scope.edt.searchText);
                    console.log($scope.edt.sims_checklist_section_parent_number);

                    $http.get(ENV.apiUrl + "api/ListSection/getsearchlist?str=" + $scope.edt.searchText + "&str1=" + $scope.edt.sims_checklist_section_parent_number).then(function (res5) {
                        debugger
                        $scope.CreDiv = res5.data;
                        $scope.totalItems = $scope.CreDiv.length;
                        // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                        $scope.todos = $scope.CreDiv;
                        $scope.makeTodos();
                    
                    });
                    }
            }

            $scope.getAllrseponse = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/ListSection/get_responsebyList").then(function (response_data) {
                    $scope.responseBylist = response_data.data;
                    console.log("responseBylist = ", $scope.responseBylist);
                });
            }

            //show Response data TABLE get respone data
            $scope.changResponsData = function () {
                debugger
                $scope.Table2 = true;
                $scope.Responsedisplay = true;
                if ($scope.int.sims_response_code == '' || $scope.int.sims_response_code == undefined) {
                    //reset page
                    $scope.getgrid();
                   
                }
                else {
                    console.log($scope.int.sims_response_code);
                    $http.get(ENV.apiUrl + "api/ListSection/get_responsedata?str=" + $scope.int.sims_response_code).then(function (res9) {
                        debugger
                        $scope.CreDiv = res9.data;
                        console.log($scope.CreDiv.sims_response_status);
                        $scope.CreDiv.sims_response_title = $scope.CreDiv[0].sims_response_title;
                        $scope.CreDiv.sims_response_status = $scope.CreDiv[0].sims_response_status;
                        $scope.totalItems = $scope.CreDiv.length;
                        // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                        $scope.todos = $scope.CreDiv;
                        $scope.makeTodos();
                    });
                }
            }
            $(document).ready(function () {
                $scope.getgrid();
                $scope.getParent();
                $scope.getAllSection();
                $scope.getAllrseponse();
                $scope.edt = { };
                $scope.typelist();
                $scope.getAllrseponse();
            });

        }])



})();