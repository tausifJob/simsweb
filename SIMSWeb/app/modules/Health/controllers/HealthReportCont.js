﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, modulecode = [];
    var simsController = angular.module('sims.module.Health');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('HealthReportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var active = '';
            var inactive = '';

            $scope.showdisabled = true;

            $timeout(function () {
                $("#example").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.exportData = function () {
     
                var check = true;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {//exportable
                            //var blob = new Blob([document.getElementById('exportable').innerHTML], {
                            //    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            //});
                            //saveAs(blob, "Report.xls");

                            alasql('SELECT * INTO XLSX("Report.xlsx",{headers:true}) \ FROM HTML("#example",{headers:true,skipdisplaynone:true})');

                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Report Not Save", showCloseButton: true, width: 380, })
                }
            };

            $scope.items = [{
                "Name": "ANC101",
                "Date": "10/02/2014",
                "Terms": ["samsung", "nokia", "apple"],
                "temp": "ANC101"

            }, {
                "Name": "ABC102",
                "Date": "10/02/2014",
                "Terms": ["motrolla", "nokia", "iPhone"],
                "temp": "ANC102"
            }]

            $scope.busyindicator = false;
            $scope.ShowCheckBoxes = true;

            //$http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
            //    $scope.curriculum = AllCurr.data;
            //    $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
            //    $scope.getAccYear($scope.edt.sims_cur_code);
            //});


            function getCur(flag, comp_code) {
                if (flag) {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        $scope.curriculum = res.data;
                        $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                        $scope.getAccYear($scope.edt.sims_cur_code);

                    });
                }

                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        $scope.curriculum = res.data;
                        $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                        $scope.getAccYear($scope.edt.sims_cur_code);

                    });
                }

            }

            $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.global_count_comp = res.data;

                if ($scope.global_count_comp) {
                    getCur(true, $scope.user_details.comp);


                }
                else {
                    getCur(false, $scope.user_details.comp)
                }
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                    $scope.getGrade(curCode, $scope.temp.sims_academic_year);
                });
            }

            $scope.GetGrade = function () {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $(function () {
                $('#cmb_grade_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/StudentReport/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $(function () {
                $('#cmb_section_code').multipleSelect({
                    width: '100%'
                });
            });
            
            $scope.Show_Data = function (str, str1) {
                if (str != undefined && str1 != undefined) {
                    if ($scope.chk_enoll_no == true || $scope.chk_stu_name == true || $scope.chk_stu_first_name == true || $scope.chk_stu_middel_name == true || $scope.chk_stu_last_name == true || $scope.chk_stu_nickname == true || $scope.chk_stu_family_name == true || $scope.chk_stu_name_ol == true || $scope.chk_stu_first_name_ol == true || $scope.chk_stu_middel_name_ol == true || $scope.chk_stu_last_name_ol == true || $scope.chk_stu_family_name_ol == true || $scope.chk_stu_gander == true || $scope.chk_stu_parent_id == true || $scope.chk_stu_grade_name == true || $scope.chk_stu_section_name == true || $scope.chk_health_card_issue_date == true || $scope.chk_health_card_number == true || $scope.chk_health_card_expiry_date == true || $scope.chk_health_card_authority == true || $scope.chk_health_blood_group == true || $scope.chk_health_restriction_status == true || $scope.chk_health_restriction_desc == true || $scope.chk_health_disability_status == true || $scope.chk_health_disability_desc == true || $scope.chk_health_mediciation_status == true || $scope.chk_health_mediciation_desc == true || $scope.chk_health_other_status == true || $scope.chk_health_other_dec == true || $scope.chk_health_hearing_status == true || $scope.chk_health_hearing_desc == true || $scope.chk_health_vision_status == true || $scope.chk_health_vision_desc == true || $scope.chk_health_other_disability_status == true || $scope.chk_health_other_disability_desc == true || $scope.chk_health_height == true || $scope.chk_health_weight == true || $scope.chk_health_teeth == true || $scope.chk_health_dr_name == true || $scope.chk_health_dr_phone == true || $scope.chk_hospital_name == true || $scope.chk_hospital_phone == true
                || $scope.chk_sims_medical_consent_health_records == true
                || $scope.chk_sims_medical_consent_immunization == true
                || $scope.chk_sims_medical_consent_emergency_treatment == true
                || $scope.chk_sims_medical_consent_medical_treatment == true
                || $scope.chk_sims_medical_consent_over_the_counter_medicine == true
                || $scope.chk_sims_medical_consent_medicine_allowed == true
                || $scope.chk_sims_medical_consent_remark == true
                || $scope.chk_sims_medical_consent_updated_on == true
                || $scope.chk_sims_medical_consent_approved_by == true
                        ) {
                        debugger
                        $http.get(ENV.apiUrl + "api/LibraryAttribute/gethealthreport?cur_name=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grd_code=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&student_en=" + $scope.edt.sims_student_enroll_number).then(function (res) {
                            $scope.studlist = res.data;
                            console.log($scope.studlist);
                            if ($scope.studlist.length == 0) {
                                swal({ title: 'Alert', text: "Please Try Again Record Not Found...", showCloseButton: true, width: 450, height: 200 });
                                $scope.ShowCheckBoxes = true;
                            }
                            else {
                                $scope.table = true;
                                $scope.busyindicator = true;
                                $scope.ShowCheckBoxes = false;

                            }
                        })
                    }
                    else {
                        swal({ title: 'Alert', text: "Please Select Atleast One Checkbox Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                    }
                }
                else {
                    swal({ title: 'Alert', text: "Please Select Curriculum & Academic Year Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                }
            }

            $scope.back = function () {
                debugger;
                $scope.table = false;
                $scope.busyindicator = false;
                $scope.ShowCheckBoxes = true;

            }

            $scope.checkallClick = function (str) {
                if (str) {
                    $scope.showdisabled = false;
                    $scope.chk_language_details = true;
                    $scope.chk_health_details = true;
                    $scope.chk_enoll_no = true;
                    $scope.chk_stu_name = true;
                    $scope.chk_stu_first_name = true;
                    $scope.chk_stu_middel_name = true;
                    $scope.chk_stu_last_name = true;
                    $scope.chk_stu_nickname = true;
                    $scope.chk_stu_family_name = true;
                    $scope.chk_stu_name_ol = true;
                    $scope.chk_stu_first_name_ol = true;
                    $scope.chk_stu_middel_name_ol = true;
                    $scope.chk_stu_last_name_ol = true;
                    $scope.chk_stu_family_name_ol = true;
                    $scope.chk_stu_gander = true;
                    $scope.chk_stu_parent_id = true;
                    $scope.chk_stu_grade_name = true;
                    $scope.chk_stu_section_name = true;
                    $scope.chk_health_card_issue_date = true;
                    $scope.chk_health_card_number = true;
                    $scope.chk_health_card_expiry_date = true;
                    $scope.chk_health_card_authority = true;
                    $scope.chk_health_blood_group = true;
                    $scope.chk_health_restriction_status = true;
                    $scope.chk_health_restriction_desc = true;
                    $scope.chk_health_disability_status = true;
                    $scope.chk_health_disability_desc = true;
                    $scope.chk_health_mediciation_status = true;
                    $scope.chk_health_mediciation_desc = true;
                    $scope.chk_health_other_status = true;
                    $scope.chk_health_other_dec = true;
                    $scope.chk_health_hearing_status = true;
                    $scope.chk_health_hearing_desc = true;
                    $scope.chk_health_vision_status = true;
                    $scope.chk_health_vision_desc = true;
                    $scope.chk_health_other_disability_status = true;
                    $scope.chk_health_other_disability_desc = true;
                    $scope.chk_health_height = true;
                    $scope.chk_health_weight = true;
                    $scope.chk_health_teeth = true;
                    $scope.chk_health_dr_name = true;
                    $scope.chk_health_dr_phone = true;
                    $scope.chk_hospital_name = true;
                    $scope.chk_hospital_phone = true;

                    $scope.chk_sims_medical_consent_health_records = true;
                    $scope.chk_sims_medical_consent_immunization = true;
                    $scope.chk_sims_medical_consent_emergency_treatment = true;
                    $scope.chk_sims_medical_consent_medical_treatment = true;
                    $scope.chk_sims_medical_consent_over_the_counter_medicine = true;
                    $scope.chk_sims_medical_consent_medicine_allowed = true;
                    $scope.chk_sims_medical_consent_remark = true;
                    $scope.chk_sims_medical_consent_updated_on = true;
                    $scope.chk_sims_medical_consent_approved_by = true;

                }
                else {
                    $scope.Uncheck();
                }
            }

            $scope.Uncheck = function () {
                $scope.showdisabled = true;
                $scope.chk_language_details = false;
                $scope.chk_health_details = false;
                $scope.chk_enoll_no = false;
                $scope.chk_stu_name = false;
                $scope.chk_stu_first_name = false;
                $scope.chk_stu_middel_name = false;
                $scope.chk_stu_last_name = false;
                $scope.chk_stu_nickname = false;
                $scope.chk_stu_family_name = false;
                $scope.chk_stu_name_ol = false;
                $scope.chk_stu_first_name_ol = false;
                $scope.chk_stu_middel_name_ol = false;
                $scope.chk_stu_last_name_ol = false;
                $scope.chk_stu_family_name_ol = false;
                $scope.chk_stu_gander = false;
                $scope.chk_stu_parent_id = false;
                $scope.chk_stu_grade_name = false;
                $scope.chk_stu_section_name = false;
                $scope.chk_health_card_issue_date = false;
                $scope.chk_health_card_number = false;
                $scope.chk_health_card_expiry_date = false;
                $scope.chk_health_card_authority = false;
                $scope.chk_health_blood_group = false;
                $scope.chk_health_restriction_status = false;
                $scope.chk_health_restriction_desc = false;
                $scope.chk_health_disability_status = false;
                $scope.chk_health_disability_desc = false;
                $scope.chk_health_mediciation_status = false;
                $scope.chk_health_mediciation_desc = false;
                $scope.chk_health_other_status = false;
                $scope.chk_health_other_dec = false;
                $scope.chk_health_hearing_status = false;
                $scope.chk_health_hearing_desc = false;
                $scope.chk_health_vision_status = false;
                $scope.chk_health_vision_desc = false;
                $scope.chk_health_other_disability_status = false;
                $scope.chk_health_other_disability_desc = false;
                $scope.chk_health_height = false;
                $scope.chk_health_weight = false;
                $scope.chk_health_teeth = false;
                $scope.chk_health_dr_name = false;
                $scope.chk_health_dr_phone = false;
                $scope.chk_hospital_name = false;
                $scope.chk_hospital_phone = false;
                $scope.chk_sims_medical_consent_health_records = false;
                $scope.chk_sims_medical_consent_immunization = false;
                $scope.chk_sims_medical_consent_emergency_treatment = false;
                $scope.chk_sims_medical_consent_medical_treatment = false;
                $scope.chk_sims_medical_consent_over_the_counter_medicine = false;
                $scope.chk_sims_medical_consent_medicine_allowed = false;
                $scope.chk_sims_medical_consent_remark = false;
                $scope.chk_sims_medical_consent_updated_on = false;
                $scope.chk_sims_medical_consent_approved_by = false;
            }

            $scope.Reset = function () {
                $scope.Uncheck();
                $scope.select_all = false;
                $scope.active_stud = false;
                $scope.inactive_stud = false;
                $scope.edt = {
                    sims_cur_code: '',
                    sims_grade_code: '',
                    sims_section_code: '',
                }
                $scope.temp = {
                    sims_academic_year: '',
                }

            }

            $scope.checkevent = function () {
                if ($scope.chk_enoll_no == true || $scope.chk_stu_name == true || $scope.chk_stu_first_name == true || $scope.chk_stu_middel_name == true || $scope.chk_stu_last_name == true || $scope.chk_stu_nickname == true || $scope.chk_stu_family_name == true || $scope.chk_stu_name_ol == true || $scope.chk_stu_first_name_ol == true || $scope.chk_stu_middel_name_ol == true || $scope.chk_stu_last_name_ol == true || $scope.chk_stu_family_name_ol == true || $scope.chk_stu_gander == true || $scope.chk_stu_parent_id == true || $scope.chk_stu_grade_name == true || $scope.chk_stu_section_name == true || $scope.chk_health_card_issue_date == true || $scope.chk_health_card_number == true || $scope.chk_health_card_expiry_date == true || $scope.chk_health_card_authority == true || $scope.chk_health_blood_group == true || $scope.chk_health_restriction_status == true || $scope.chk_health_restriction_desc == true || $scope.chk_health_disability_status == true || $scope.chk_health_disability_desc == true || $scope.chk_health_mediciation_status == true || $scope.chk_health_mediciation_desc == true || $scope.chk_health_other_status == true || $scope.chk_health_other_dec == true || $scope.chk_health_hearing_status == true || $scope.chk_health_hearing_desc == true || $scope.chk_health_vision_status == true || $scope.chk_health_vision_desc == true || $scope.chk_health_other_disability_status == true || $scope.chk_health_other_disability_desc == true || $scope.chk_health_height == true || $scope.chk_health_weight == true || $scope.chk_health_teeth == true || $scope.chk_health_dr_name == true || $scope.chk_health_dr_phone == true || $scope.chk_hospital_name == true || $scope.chk_hospital_phone == true
                || $scope.chk_sims_medical_consent_health_records ==true
                || $scope.chk_sims_medical_consent_immunization ==true
                || $scope.chk_sims_medical_consent_emergency_treatment  ==true
                || $scope.chk_sims_medical_consent_medical_treatment  ==true
                || $scope.chk_sims_medical_consent_over_the_counter_medicine ==true
                || $scope.chk_sims_medical_consent_medicine_allowed   ==true
                || $scope.chk_sims_medical_consent_remark  ==true
                || $scope.chk_sims_medical_consent_updated_on  ==true
                || $scope.chk_sims_medical_consent_approved_by  ==true
               ) {
                    $scope.showdisabled = false;
                }
                else {
                    $scope.showdisabled = true;
                }
            }

            $scope.activestudent = function (ca) {
                debugger;
                if (ca) {
                    active = 'A';
                }
                else {
                    active = undefined;
                }
            }

            $scope.inactivestudent = function (cina) {
                debugger;
                if (cina) {
                    inactive = 'I';
                }
                else {
                    inactive = undefined;
                }
            }

        }])
})();