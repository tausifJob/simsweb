﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Health');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('medicalImmunizationGroupCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            $scope.display = false;
            $scope.itemsPerPage = "10";
            $scope.currentPage = 0;
            $scope.items = [];
            $scope.url = ENV.apiUrl + 'Content' + '/' + $http.defaults.headers.common['schoolId'];
            var photofile;

            $http.get(ENV.apiUrl + "api/common/MedicalExamination/GetSims_MedicalImmunizationGroup").then(function (res) {
                debugger;
                $scope.display = false;
                $scope.grid = true;
                $scope.items = res.data;
              
                });


            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;

            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.items.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;
            };

            $scope.edit = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                $scope.edt = str;
                $scope.display = true;
                $scope.grid = false;
                $scope.update1 = true;
                $scope.editflg = true;
                $scope.save1 = false;
                $scope.curChange(str.sims_timetable_cur);
                $scope.academicChange(str.sims_academic_year);
                $scope.gradeChange(str.sims_grade);
                //  $scope.edt = str;
                }
            }

            $scope.Update = function (newEventForm) {
                if (newEventForm.$valid) {
                    var dataupdate = [];
                    var data = $scope.edt;
                    data.opr = 'U';

                    dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/common/MedicalExamination/InsertSims_MedicalImmunizationGroup", dataupdate).then(function (res) {
                        $scope.ins = res.data;
                        if (res.data==true) {
                            swal({  text: "Record Updated Successfully.", imageUrl: "assets/img/check.png" });
                            $http.get(ENV.apiUrl + "api/common/MedicalExamination/GetSims_MedicalImmunizationGroup").then(function (res) {
                                $scope.display = false;
                                $scope.grid = true;
                                $scope.items = res.data;
                                $scope.currentPage = 0;
                            });

                        }
                        else if (res.data==false) {
                            swal({  text: "Record Not Updated .", imageUrl: "assets/img/close.png" });
                        }
                        else {
                            swal("Error-" + $scope.ins)
                        }

                    });




                }
            }

            $scope.Save = function (newEventForm) {

                if (newEventForm.$valid) {
                    var datasave = [];
                    var data = $scope.edt;
                    data.opr = 'I';
                    datasave.push(data);
                    $http.post(ENV.apiUrl + "api/common/MedicalExamination/InsertSims_MedicalImmunizationGroup", datasave).then(function (res) {
                        $scope.ins = res.data;
                        if (res.data==true) {
                            swal({  text: "Record Inserted Successfully.", imageUrl: "assets/img/check.png" });
                            $http.get(ENV.apiUrl + "api/common/MedicalExamination/GetSims_MedicalImmunizationGroup").then(function (res) {
                                $scope.display = false;
                                $scope.grid = true;
                                $scope.items = res.data;
                                $scope.currentPage = 0;
                            });
                        }
                        else if (res.data==false) {
                            swal({ text: "Record Not Inserted.", imageUrl: "assets/img/close.png" });

                        }
                        else {
                            swal("Error-" + $scope.ins)
                        }
                    });

                }
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                $scope.edt = "";
                $scope.grid = false;
                $scope.display = true;
                $scope.save1 = true;
                $scope.editflg = false;
                $scope.edt = {};
                $scope.edt.sims_immunization_age_group_status = true;
                }
            }

            var main, del = '', delvar = [];

            //$scope.chk = function () {
            //    main = document.getElementById('chk_min');
            //    del = [];
            //    if (main.checked == true) {
            //        for (var i = 0; i < $scope.items.length; i++) {
            //            $scope.items[i].sims_immunization_age_group_code1 = true;
            //        }
            //    }
            //    else {
            //        for (var i = 0; i < $scope.items.length; i++) {
            //            $scope.items[i].sims_immunization_age_group_code1 = false;
            //            main.checked = false;
            //            $scope.row1 = '';
            //        }
            //    }
            //}

            $scope.CheckAllChecked = function () {
               
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.items.length; i++) {
                        var v = document.getElementById(i + $scope.items[i].sims_immunization_age_group_code);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.items.length; i++) {
                        var v = document.getElementById(i + $scope.items[i].sims_immunization_age_group_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyoneselect = function () {
                
                $("input[type='checkbox']").change(function (e) {

                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

         

            $scope.OkDelete = function () {

                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                var data1 = [];
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.items.length; i++) {
                    var v = document.getElementById(i + $scope.items[i].sims_immunization_age_group_code);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_immunization_age_group_code': $scope.items[i].sims_immunization_age_group_code,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '', text: "Are you sure you want to Delete?", showCloseButton: true, showCancelButton: true, confirmButtonText: 'Yes', width: 380, cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                           
                            var temp_deleteleave = deleteleave;
                            $http.get(ENV.apiUrl + "api/common/Medical_Immunization/GetSims_MedicalImmunization").then(function (temp_data) {
                                var temp_mappingData = temp_data.data;                                
                                for (var i = 0; i < temp_mappingData.length; i++) {
                                    for (var j = 0; j < deleteleave.length; j++) {
                                        if (temp_mappingData[i].sims_immunization_age_group_code == deleteleave[j].sims_immunization_age_group_code) {
                                            temp_deleteleave.splice(j, 1);
                                        }
                                    }
                                }
                                if (temp_deleteleave.length > 0) {
                                    $http.post(ENV.apiUrl + "api/common/MedicalExamination/InsertSims_MedicalImmunizationGroup", temp_deleteleave).then(function (res) {
                                        if (res.data == true) {
                                            swal({ text: "Record Deleted Successfully.", imageUrl: "assets/img/check.png" });
                                            if (isConfirm) {
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    $scope.row1 = '';
                                                }
                                                $http.get(ENV.apiUrl + "api/common/MedicalExamination/GetSims_MedicalImmunizationGroup").then(function (res) {
                                                    $scope.display = false;
                                                    $scope.grid = true;
                                                    $scope.items = res.data;
                                                    $scope.currentPage = 0;
                                                });
                                            }

                                            $scope.currentPage = true;
                                        }
                                        else if (res.data == false) {
                                            swal({ text: "Record Deleted Successfully.", imageUrl: "assets/img/check.png" });
                                        }
                                        else {
                                            swal("Error-" + $scope.ins)
                                        }


                                    });
                                }
                                else {
                                    swal({ text: "Please Select not mapped records", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                                }
                            });

                            //$http.post(ENV.apiUrl + "api/MedicalStudentSusceptibility/CUDMedicalVisit", deleteleave).then(function (msg) {
                           
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.items.length; i++) {
                                var v = document.getElementById(i + $scope.items[i].sims_immunization_age_group_code);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }

                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }

                //$scope.currentPage = str;
                $scope.row1 = '';
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.update1 = false;
                $scope.save1 = false;
                $scope.editflg = false;
            }

        }]);

})();


