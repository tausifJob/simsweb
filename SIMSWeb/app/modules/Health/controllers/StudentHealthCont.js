﻿(function () {
    'use strict';
    var obj1, temp, opr, comp, edt;
    var main;
    var simsController = angular.module('sims.module.Health');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudentHealthCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            $scope.search_btn = true;
            $scope.clr_btn = true;
            $scope.Update_btn = false;
            $scope.newdisplay = false;
            $scope.table = true;
            var cur;
            var year;
            var grade;
            var section;
            $scope.hide_img = false;
            debugger;
            $scope.imgurl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/';
            //$scope.imgurl = "http://api.mograsys.com/APIERP/Content/adisw" + '/Images/StudentImage/';

            $http.get(ENV.apiUrl + "api/StudentHealth/getBloodGroup").then(function (res) {

                $scope.empstat = true;
                $scope.bloodGroup = res.data;
                console.log($scope.bloodGroup);
                $scope.operation = true;
                $scope.Save_btn = true;
                $scope.Update_btn = false;
                $scope.display = true;

            });

            $scope.getexcus = function () {
                $http.get(ENV.apiUrl + "api/StudentHealth/GetAllExcuses").then(function (GetAllExcuses) {
                    debugger;
                    $scope.GetExce = GetAllExcuses.data;
                });
            }
            //$http.get(ENV.apiUrl + "api/StudentHealth/getAllStudentHealth").then(function (res1) {
            //    $scope.studHealth = res1.data;
            //    $scope.totalItems = $scope.studHealth.length;
            //    $scope.todos = $scope.studHealth;
            //    $scope.makeTodos();
            //});

            $scope.calculateBMI = function (kg, htc) {
                debugger;
                var m = 0, bmi = 0, diff = 0, f_bmi = 0, h2 = 0;
                m = htc / 100;
                h2 = m * m;

                bmi = kg / h2;

                f_bmi = Math.floor(bmi);

                diff = bmi - f_bmi;
                diff = diff * 10;

                diff = Math.round(diff);
                if (diff == 10) {
                    // Need to bump up the whole thing instead
                    f_bmi += 1;
                    diff = 0;
                }
                bmi = f_bmi + "." + diff;
                $scope.temp.sims_health_bmi = bmi;
            }

            $scope.getcur = function () {
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                    $scope.curriculum = AllCurr.data;
                    $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                    $scope.getAccYear($scope.edt.sims_cur_code);
                });
            }

            $scope.getcur();

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                    $scope.getGrade($scope.edt.sims_cur_code, $scope.temp.sims_academic_year);
                    //$scope.health();
                });
            }

            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    console.log($scope.Grade_code);
                });
            }

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    console.log($scope.Section_code);
                });
            }

            //$scope.health = function () {
            //    debugger;
            //    $http.get(ENV.apiUrl + "api/StudentHealth/getAllStudentHealth?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.temp.sims_academic_year).then(function (res1) {
            //        $scope.studHealth = res1.data;
            //        $scope.totalItems = $scope.studHealth.length;
            //        $scope.todos = $scope.studHealth;
            //        $scope.makeTodos();
            //    });
            //}

            
            // table search
            $scope.stud_Details_fun = function (curcode, acYear, gCode, sCode) {
                debugger;
                $scope.filteredTodos = [];
                $scope.Stud_Health_Details = [];
                cur = $scope.edt.sims_cur_code;
                year = $scope.temp.sims_academic_year;
                grade = $scope.temp.sims_grade_code;
                section = $scope.temp.sims_section_code;
                $http.get(ENV.apiUrl + "api/StudentHealth/getStudHealthSearch?sims_cur_code=" + curcode + "&sims_academic_year=" + acYear + "&sims_grade_code=" + gCode + "&sims_section_code=" + sCode).then(function (StudHealthDetails) {
                    $scope.Stud_Health_Details = StudHealthDetails.data;
                    $scope.totalItems = $scope.Stud_Health_Details.length;
                    $scope.todos = $scope.Stud_Health_Details;
                    $scope.makeTodos();
                });
            }

            $rootScope.visible_stud = true;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = false;
            $rootScope.chkMulti = false;

            $scope.SearchStudentWindow = function () {

                // $scope.searchtable = false;
                // $scope.student = '';
                // $('#MyModal').modal('show');
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });

            }

            $scope.$on('global_cancel', function () {
                debugger;

                if ($scope.SelectedUserLst.length > 0) {

                    debugger
                    $scope.temp =
                        {
                            name: $scope.SelectedUserLst[0].name,
                            enroll_number: $scope.SelectedUserLst[0].s_enroll_no,
                        }
                    $http.get(ENV.apiUrl + "api/StudentHealth/getGenderDOB?S_number=" + $scope.temp.enroll_number).then(function (getStudData) {
                        $scope.get_studData = getStudData.data;
                        $('#style_new').html($scope.get_studData[0].sub_class);
                        $scope.hide_img = true;
                    });
                }


            });

            $scope.onlyNumbers1 = function (event) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $scope.SearchSudent = function () {
                debugger;
                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    main.checked = false;
                }
                $scope.searchtable = false;
                $scope.busy = true;
                $scope.sibling_result;
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.temp)).then(function (Allstudent) {

                    $scope.student = Allstudent.data;
                    $scope.totalItems = $scope.student.length;
                    $scope.todos = $scope.student;
                    $scope.makeTodos();
                    $scope.searchtable = true;
                    $scope.busy = false;
                });
            }

            //$scope.DataEnroll = function () {
            //    for (var i = 0; i < $scope.student.length; i++) {
            //        var t = $scope.student[i].s_enroll_no;
            //        var v = document.getElementById(t);
            //        if (v.checked == true) {
            //            $scope.temp = {
            //                enroll_number: $scope.student[i].s_enroll_no
            //            };
            //            $http.get(ENV.apiUrl + "api/StudentHealth/getGenderDOB?S_number=" + $scope.temp.enroll_number).then(function (getStudData) {
            //                $scope.get_studData = getStudData.data;
            //                $('#style_new').html($scope.get_studData[0].sub_class);
            //                $scope.hide_img = true;
            //            });
            //        }
            //    }
            //}

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=0&routeCode=0").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;
                console.log($scope.ComboBoxValues);
            });

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                console.log($scope.curriculum);

            });

            $scope.getacyr = function (str) {
                console.log(str);
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    console.log($scope.Academic_year);
                })
            }

            $http.get(ENV.apiUrl + "api/common/getAllAcademicYear").then(function (get_AllAcademicYear) {
                $scope.ActiveYear = get_AllAcademicYear.data;
            });

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = 'All';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Stud_Health_Details, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Stud_Health_Details;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_student_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_blood_group_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                      item.sims_enrollment_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.New = function () {
                $scope.temp = '';


                $scope.hide_img = false;
                $scope.medication = true;
                $scope.disability = true;
                $scope.restriction = true;
                $scope.hearing = true;
                $scope.vision = true;
                $scope.other = true;
                $scope.student_excuses = true;
                $scope.getexcus();
                $scope.other_disability = true;
                $scope.disabled = false;
                $scope.readonly = true;
                $scope.table = false;
                $scope.newdisplay = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.get_studData = '';
                //$scope.temp = "";
                $scope.Stud_number = false;
                $scope.read = true;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            $scope.showdate = function (date, name1) {

                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //$scope.temp[name1] = year + "/" + month + "/" + day;
                $scope.temp[name1] = date;
            }

            var datasend = []; var data;

            $scope.onlyNumbers2 = function (event) {
                debugger;
                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 45, 'plus': 43,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57

                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $scope.savedata = function (Myform) {//Myform
                debugger
                var data = $scope.temp;
                if ($scope.temp["enroll_number"] != null) {
                    data.opr = "I";
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/StudentHealth/CUDStudentHealth", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });

                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record alredy present Or Please try again. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $scope.getcur();
                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.newdisplay = false;
                    $scope.clear();
                }
                else {
                    swal({ text: "You not enterd reuired data, Please try again. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                }
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.Cancel = function () {
                $scope.temp = '';
                $scope.table = true;
                $scope.newdisplay = false;
                $scope.hide_img = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.getcur();
                //$scope.health = function () {
                //$scope.temp.dest_code = "";
                //$scope.temp.country_code = "";
                //$scope.temp.dest_name = "";
            }

            $scope.clear = function () {
                $scope.temp = '';
                $scope.hide_img = false;
                $scope.getcur();
                //$scope.health = function () {
            }

            $scope.edit = function (str) {
                debugger;
                $scope.medication = true;
                $scope.disability = true;
                $scope.restriction = true;
                $scope.hearing = true;
                $scope.vision = true;
                $scope.other = true;
                $scope.student_excuses = true;
                $scope.getexcus();
                $scope.other_disability = true;
                $scope.read = true;
                $scope.Stud_number = true;
                $scope.table = false;
                $scope.newdisplay = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;

                
                $scope.temp = {
                    sims_cur_code: $scope.edt.sims_cur_code,
                    sims_academic_year: $scope.temp.sims_academic_year,
                    sims_grade_code: $scope.temp.sims_grade_code,
                    sims_section_code:$scope.temp.sims_section_code,
                    enroll_number: str.sims_enrollment_number,
                    sims_blood_group_code: str.sims_blood_group_code,
                    sims_health_card_number: str.sims_health_card_number,
                    sims_health_card_issue_date: str.sims_health_card_issue_date,
                    sims_health_card_expiry_date: str.sims_health_card_expiry_date,
                    sims_health_card_issuing_authority1: str.sims_health_card_issuing_authority1,
                    sims_height: str.sims_height,
                    sims_wieght: str.sims_wieght,
                    sims_teeth: str.sims_teeth,
                    sims_health_bmi: str.sims_health_bmi,
                    sims_medication_status: str.sims_medication_status,
                    sims_medication_desc: str.sims_medication_desc,
                    sims_disability_status: str.sims_disability_status,
                    sims_disability_desc: str.sims_disability_desc,
                    sims_health_restriction_status: str.sims_health_restriction_status,
                    sims_health_restriction_desc: str.sims_health_restriction_desc,
                    sims_health_hearing_status: str.sims_health_hearing_status,
                    sims_health_hearing_desc: str.sims_health_hearing_desc,
                    sims_health_vision_status: str.sims_health_vision_status,
                    sims_health_vision_desc: str.sims_health_vision_desc,
                    sims_health_other_status: str.sims_health_other_status,
                    //sims_student_health_excuses: str.sims_student_health_excuses,
                    sims_health_other_desc: str.sims_health_other_desc,
                    sims_has_your_child_any_other_disability_status: str.sims_has_your_child_any_other_disability_status,
                    sims_has_your_child_any_other_disability: str.sims_has_your_child_any_other_disability,
                    sims_regular_hospital_name: str.sims_regular_hospital_name,
                    sims_regular_hospital_phone: str.sims_regular_hospital_phone,
                    sims_regular_doctor_name: str.sims_regular_doctor_name,
                    sims_regular_doctor_phone: str.sims_regular_doctor_phone, 
                    sims_medical_excuses_name: str.sims_medical_excuses_name,

                };

                $http.get(ENV.apiUrl + "api/StudentHealth/getGenderDOB?S_number=" + $scope.temp.enroll_number).then(function (getStudData) {
                    $scope.get_studData = getStudData.data;
                    $('#style_new').html($scope.get_studData[0].sub_class);
                    $scope.hide_img = true;
                });

            }

            var dataupdate = []; var data;

            $scope.update = function (Myform) //Myform
                //if (Myform)
            {
                debugger
                //var data = $scope.temp;
                var ttt = $scope.temp;
                var data = {
                    'enroll_number': $scope.temp.enroll_number,
                    'sims_blood_group_code': $scope.temp.sims_blood_group_code,
                    'sims_health_card_number': $scope.temp.sims_health_card_number,
                    'sims_health_card_issue_date': $scope.temp.sims_health_card_issue_date,
                    'sims_health_card_expiry_date': $scope.temp.sims_health_card_expiry_date,
                    'sims_health_card_issuing_authority1': $scope.temp.sims_health_card_issuing_authority1,
                    'sims_height': $scope.temp.sims_height,
                    'sims_wieght': $scope.temp.sims_wieght,
                    'sims_teeth': $scope.temp.sims_teeth,
                    'sims_health_bmi': $scope.temp.sims_health_bmi,
                    'sims_medication_status': $scope.temp.sims_medication_status,
                    'sims_medication_desc': $scope.temp.sims_medication_desc,
                    'sims_disability_status': $scope.temp.sims_disability_status,
                    'sims_disability_desc': $scope.temp.sims_disability_desc,
                    'sims_health_restriction_status': $scope.temp.sims_health_restriction_status,
                    'sims_health_restriction_desc': $scope.temp.sims_health_restriction_desc,
                    'sims_health_hearing_status': $scope.temp.sims_health_hearing_status,
                    'sims_health_hearing_desc': $scope.temp.sims_health_hearing_desc,
                    'sims_health_vision_status': $scope.temp.sims_health_vision_status,
                    'sims_health_vision_desc': $scope.temp.sims_health_vision_desc,
                    'sims_health_other_status': $scope.temp.sims_health_other_status,
                    'sims_health_other_desc': $scope.temp.sims_health_other_desc,
                    'sims_has_your_child_any_other_disability_status': $scope.temp.sims_has_your_child_any_other_disability_status,
                    'sims_has_your_child_any_other_disability': $scope.temp.sims_has_your_child_any_other_disability,
                    'sims_regular_hospital_name': $scope.temp.sims_regular_hospital_name,
                    'sims_regular_hospital_phone': $scope.temp.sims_regular_hospital_phone,
                    'sims_regular_doctor_name': $scope.temp.sims_regular_doctor_name,
                    'sims_regular_doctor_phone': $scope.temp.sims_regular_doctor_phone,
                    'sims_medical_excuses_name': $scope.temp.sims_medical_excuses_name,
                   // 'sims_student_health_excuses': $scope.sims_student_health_excuses,
                    'opr': 'U',
                };
                
                    data.opr = "U";
                    dataupdate.push(data);
                     
                    $http.post(ENV.apiUrl + "api/StudentHealth/CUDStudentHealth", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            $scope.stud_Details_fun($scope.temp.sims_cur_code, $scope.temp.sims_academic_year, $scope.temp.sims_grade_code, $scope.temp.sims_section_code)

                        }

                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        //$http.get(ENV.apiUrl + "api/StudentHealth/getAllStudentHealth").then(function (res1) {
                        //    $scope.studHealth = res1.data;
                        //    $scope.totalItems = $scope.studHealth.length;
                        //    $scope.todos = $scope.studHealth;
                        //    $scope.makeTodos();
                        //
                        //    
                        //});

                        debugger;
                        $scope.stud_Details_fun(curcode, acYear, gCode, sCode);
                  
                        //$scope.stud_Details_fun(cur, year, grade, section);
                        //$scope.health();
                    })
                 
                //$http.get(ENV.apiUrl + "api/StudentHealth/getAllStudentHealth").then(function (res1) {
                //    $scope.studHealth = res1.data;
                //    $scope.totalItems = $scope.studHealth.length;
                //    $scope.todos = $scope.studHealth;
                //    $scope.makeTodos();
                //});
                $scope.getcur();
                $scope.table = true;
                $scope.newdisplay = false;
                dataupdate = [];
            }


            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("helt-" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("helt-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }


            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.OkDelete = function () {
                var data1 = [];
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById("helt-" + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'enroll_number': $scope.filteredTodos[i].sims_enrollment_number,
                            'opr': "D"
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/StudentHealth/CUDStudentHealth", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.health();

                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.health();
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById("helt-" + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                data1 = [];
            }

            $scope.StudAdd = function (info) {
                $scope.temp = { comn_user_name: info.s_enroll_no };
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            var dom;
            $scope.flag = true;
            $scope.expand = function (info, $event) {

                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table table table-bordered table-hover table-condensed' cellpadding='5' cellspacing='0' width='100%'>" +
                        "<tbody>" +

                         "<tr style='background-color: #edefef'> <td class='semi-bold'><b>" + "Issuing Authority" + "</b></td> <td class='semi-bold'><b>" + "Teeth" + " </b></td><td class='semi-bold'><b>" + "Medication Status" + "</b></td><td class='semi-bold'><b>" + "Medication Desc" + "</b></td>" +
                         "<td class='semi-bold'><b>" + "Disability Status" + "</b></td> <td class='semi-bold'><b>" + "Disability Desc" + " </b></td><td class='semi-bold'><b>" + "Restriction Status" + "</b></td><td class='semi-bold'><b>" + "Restriction Desc" + "</b></td></tr>" +

                         "<tr><td>" + (info.sims_health_card_issuing_authority1) + "</td> <td>" + (info.sims_teeth) + " </td><td>" + (info.sims_medication_status) + "</td><td>" + (info.sims_medication_desc) + "</td>" +
                        "<td>" + (info.sims_disability_status) + "</td> <td>" + (info.sims_disability_desc) + " </td><td>" + (info.sims_health_restriction_status) + "</td><td>" + (info.sims_health_restriction_desc) + "</td></tr>" +


                         "<tr style='background-color: #edefef'><td class='semi-bold'><b>" + "Hearing Status" + "</b></td> <td class='semi-bold'><b>" + "Hearing Desc" + " </b></td><td class='semi-bold'><b>" + "Vision Status" + "</b></td><td class='semi-bold'><b>" + "Vision Desc" + "</b></td>" +
                         "<td class='semi-bold'><b>" + "Other Status" + "</b></td> <td class='semi-bold'><b>" + "Other Desc" + "</b> </td><td class='semi-bold'><b>" + "Hospital Name" + "</b></td><td class='semi-bold'><b>" + "Hospital Phone No" + "</b></td></tr>" +


                         "<tr><td>" + (info.sims_health_hearing_status) + "</td> <td>" + (info.sims_health_hearing_desc) + " </td><td>" + (info.sims_health_vision_status) + "</td><td>" + (info.sims_health_vision_desc) + "</td>" +
                        "<td>" + (info.sims_health_other_status) + "</td> <td>" + (info.sims_health_other_desc) + " </td><td>" + (info.sims_regular_hospital_name) + "</td><td>" + (info.sims_regular_hospital_phone) + "</td></tr>" +
                         "<tr style='background-color: #edefef'> <td class='semi-bold'><b>" + "Doctor Name" + "</b></td> <td class='semi-bold'><b>" + "Doctor Phone No" + "</b></td><td  colspan='7'></td></tr>" +

                         "<tr><td>" + (info.sims_regular_doctor_name) + "</td> <td>" + (info.sims_regular_doctor_phone) + " </td><td  colspan='7'></td></tr></table>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }
            }

            $scope.CheckedReadonly = function (str) {
                debugger;
                var d = document.getElementById(str);
                if (d.checked == true) {

                    $scope[str] = false;
                }
                else { $scope[str] = true; }
            }
        }])
})();
