﻿/// <reference path="../../HRMS/controller/AttendanceMachine.js" />

(function () {

    'use strict';
    var temp = [];
    var simsController = angular.module('sims.module.Health');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('campTransactionCont',
      ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
          $scope.temp = {};
          $scope.edt = {};
          var insertDetailData = [];
          $scope.teethDetails = [];
          var uname = $rootScope.globals.currentUser.username;
          $scope.expand_object = false;

          var today = new Date();
          today = ('0' + today.getDate()).slice(-2) + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + today.getFullYear();
          $scope.temp["to_date"] = today
          $scope.temp["from_date"] = today
          $scope.temp["issue_creation_date"] = today
          $scope.hide_submit = false;
          $scope.showDetails = false;
          $scope.showStudDetails = false;
          $scope.submitBtnDisabled = false;
          $scope.hide_grid = false;
          $scope.busyindicator = true;

          $scope.getCampDetails = function () {
              $http.get(ENV.apiUrl + "api/CampTransaction/getCampName?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                  $scope.campDetails = res.data;
                  console.log("campDetails", $scope.campDetails);
              });
          }
          $scope.getCampDetails();
          //$scope.onClick = function (value) {

          //    debugger
          //}

          //$scope.getcur = function () {
          //    $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
          //        $scope.curriculum = AllCurr.data;
          //        $scope.temp.sims_cur_code = $scope.curriculum[0].sims_cur_code;
          //        $scope.getAccYear($scope.temp.sims_cur_code);
          //    });
          //}

          //$scope.getcur();

          //$scope.getAccYear = function (curCode) {
          //    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
          //        $scope.Acc_year = Acyear.data;
          //        $scope.temp.sims_academic_year = $scope.Acc_year[0].sims_academic_year ;
          //        $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
          //        //$scope.health();
          //    });
          //}
          $(function () {
              $('#grades').multipleSelect({
                  width: '100%'
              });
          });

          $(function () {
              $('#sections').multipleSelect({
                  width: '100%'
              });
          });

          setTimeout(function () {
              $("#Selectstud").select2();
          }, 100);


          $scope.getGrade = function () {
              $http.get(ENV.apiUrl + "api/CampTransaction/getAllGrades?cur_code=" + $scope.temp.sims_cur_code).then(function (Gradecode) {
                  $scope.Grade_code = Gradecode.data;
                  console.log($scope.Grade_code);
                  $scope.temp.sims_grade_code
                  //$('#grades').multiSelect('select_all');


                  setTimeout(function () {
                      $('#grades').change(function () {
                      }).multipleSelect({
                          width: '100%'
                      });
                  }, 300);
                  //$('#grades').multiSelect('select_all');
                  setTimeout(function () {
                      $('#grades').multipleSelect('checkAll');
                  }, 500);

              });
          }

          $scope.getSection = function () {
              $("#Selectstud").select2("val", "");
              $scope.temp.enroll_number = '';

              $http.get(ENV.apiUrl + "api/CampTransaction/getSectionFromGrade?cur_code=" + $scope.temp.sims_cur_code + "&grade_code=" + $scope.temp.sims_grade_code).then(function (Sectioncode) {
                  $scope.Section_code = Sectioncode.data;
                  console.log($scope.Section_code);
                  setTimeout(function () {
                      $('#sections').change(function () {
                      }).multipleSelect({
                          width: '100%'
                      });
                  }, 300);

                  setTimeout(function () {
                      $('#sections').multipleSelect('checkAll');
                  }, 500);
              });
          }

          $scope.setCampDetails = function (obj) {
              debugger;
              $scope.showDetails = true;
              console.log("obj", obj);
              console.log("json obj", JSON.parse(obj));
              var ob = JSON.parse(obj);
              $scope.edt.sims_cur_short_name_en = ob.sims_cur_short_name_en;
              $scope.edt.sims_health_package_name_en = ob.sims_health_package_name_en;
              $scope.edt.sims_health_camp_date = ob.sims_health_camp_date;
              $scope.edt.sims_health_camp_name_en = ob.sims_health_camp_name_en;
              $scope.edt.sims_health_camp_desc = ob.sims_health_camp_desc;
              $scope.edt.sims_health_camp_code = ob.sims_health_camp_code;
              $scope.edt.sims_health_package_code = ob.sims_health_package_code;
              $scope.edt.sims_health_cur_code = ob.sims_health_cur_code;
              $scope.temp.sims_cur_code = ob.sims_health_cur_code;
              console.log("edt ", $scope.edt);
              $scope.getGrade();
          }

          $scope.getStudents = function () {
              $("#Selectstud").select2("val", "");
              $scope.temp.enroll_number = '';
              $http.get(ENV.apiUrl + "api/CampTransaction/getStudents?cur_code=" + $scope.edt.sims_health_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code
                  + "&enroll_number=" + $scope.temp.enroll_number
                  ).then(function (res) {
                      $scope.students = res.data;
                      console.log("students", $scope.students);
                  });
          }

          $scope.setStudDetails = function (str) {
              var obj = JSON.parse(str);
              console.log("obj", obj);
              $scope.temp.enroll_number = obj.sims_student_enroll_number;
              $scope.edt.stud_name = obj.stud_name;
              $scope.edt.sims_student_dob = obj.sims_student_dob;
              $scope.edt.sims_student_date = obj.sims_student_date;
              $scope.edt.stud_age = obj.stud_age;
              $scope.edt.sims_grade_name_en = obj.sims_grade_name_en;
              $scope.edt.sims_section_name_en = obj.sims_section_name_en;
              $scope.temp.entry_date = today;
          }


          $scope.get_Details = function () {
              if ($scope.temp.enroll_number == "" || $scope.temp.enroll_number == undefined || $scope.temp.enroll_number == null) {
                  swal({ title: "Alert", text: "Please select student", width: 340, showCloseButton: true });
              }
              else {
                  $scope.busyindicator = false;
                  var obj = {
                      sims_health_package_code: $scope.edt.sims_health_package_code,
                      sims_health_enroll_number: $scope.temp.enroll_number,
                      sims_health_camp_code: $scope.edt.sims_health_camp_code
                  };
                  console.log("obj", obj);

                  $http.post(ENV.apiUrl + "api/CampTransaction/CUAllList", obj).then(function (res) {
                      $scope.getDeails = res.data.test_lst;
                      console.log("getDeails ", $scope.getDeails);
                      $scope.busyindicator = true;
                      $scope.showStudDetails = true;
                      $scope.hide_submit = true;
                      $scope.hide_grid = true;
                      $scope.submitBtnDisabled = false;
                      $scope.totalItems = $scope.getDeails.length;
                      $scope.todos = $scope.getDeails;
                      var cmt = '';
                      try {
                          cmt = $scope.getDeails.find(function (cr) { return cr.sims_health_test_code === '0008' || cr.sims_health_test_code === '0011' }).parameter_lst.find(function (cri) { return cri.sims_health_parameter_code === '0044' }).sims_health_camp_parameter_check_comment;
                      }
                      catch (e) {
                      }
                      cmt !== '' && (cmt = cmt.substr(0, cmt.length - 1), cmt = cmt.split(','))

                      for (var i = 11; i <= 85; i++) {
                          if ((i > 18 && i < 21) || (i > 28 && i < 31) || (i > 38 && i < 41) || (i > 48 && i < 51) || (i > 55 && i < 61) || (i > 65 && i < 71) || (i > 75 && i < 81)) {
                          }
                          else {
                              var tempId = '#' + i + ' path';

                              $(tempId).attr('fill', '#FFFFFF');
                          }
                      }

                      //for (var i = 11; i <= 18; i++) {
                      //    $('#' + i + ' path').attr('fill', '#FFFFFF');
                      //}
                      //for (var i = 21; i <= 28; i++) {
                      //    $('#' + i + ' path').attr('fill', '#FFFFFF');
                      //}
                      //for (var i = 31; i <= 38; i++) {
                      //    $('#' + i + ' path').attr('fill', '#FFFFFF');
                      //}
                      //for (var i = 41; i <= 48; i++) {
                      //    $('#' + i + ' path').attr('fill', '#FFFFFF');
                      //}
                      //for (var i = 51; i <= 55; i++) {
                      //    $('#' + i + ' path').attr('fill', '#FFFFFF');
                      //}
                      //for (var i = 61; i <= 65; i++) {
                      //    $('#' + i + ' path').attr('fill', '#FFFFFF');
                      //}
                      //for (var i = 71; i <= 75; i++) {
                      //    $('#' + i + ' path').attr('fill', '#FFFFFF');
                      //}
                      //for (var i = 81; i <= 85; i++) {
                      //    $('#' + i + ' path').attr('fill', '#FFFFFF');
                      //}

                      cmt !== '' && ($timeout(function () {

                          cmt.forEach(function (cr) {
                              var temp = cr.split('-');
                              var tempId = '#' + temp[0] + '' + temp[1] + ' path';
                              switch (temp[0]) {
                                  case 'D': $(tempId).attr('fill', '#FF0000'); break;
                                  case 'F': $(tempId).attr('fill', '#008000'); break;
                                  case 'M': $(tempId).attr('fill', '#FFFF00'); break;
                              }
                              $scope.teethDetails[$scope.teethDetails.length] = { teethNo: temp[1], isFilled: true, teethType: temp[0] };
                          })
                      }, 2000))
                      for (var i = 0; i < $scope.totalItems; i++) {
                          $scope.getDeails[i].sims_icon = "fa fa-plus-circle";
                      }
                  });
              }
          }

          $scope.checkISChange = function (row, para, chk) {
              debugger
              row.testChange = true;
              para.paraChange = true;
              if (chk != undefined) {
                  para.sims_health_camp_parameter_check_comment = chk.sims_health_parameter_check_comment
                  chk.checkChange = true;
              }
          }

          function getLocation() {
              if (navigator.geolocation) {
                  navigator.geolocation.getCurrentPosition(showPosition);
              } else {
                  console.log("Geolocation is not supported by this browser.");
              }
          }
          getLocation();
          function showPosition(position) {
              if (position.coords.latitude == undefined || position.coords.latitude == '')
                  $scope.temp.latitude = '';
              else
                  $scope.temp.latitude = position.coords.latitude;

              if (position.coords.longitude == undefined || position.coords.longitude == '')
                  $scope.temp.longitude = '';
              else
                  $scope.temp.longitude = position.coords.longitude;

              console.log("Latitude: " + $scope.temp.latitude +
              "<br>Longitude: " + $scope.temp.longitude);
          }

          $scope.Save_Data = function () {
              console.log("svae", $scope.getDeails);
              $scope.submitBtnDisabled = true;
              var sendData = [];
              var reportCardNo = '';
              debugger;
              for (var i = 0; i < $scope.getDeails.length; i++) {
                  for (var j = 0; j < $scope.getDeails[i].parameter_lst.length; j++) {
                      for (var k = 0; k < $scope.getDeails[i].parameter_lst[j].check_lst.length; k++) {
                          if ($scope.getDeails[i].parameter_lst[j].check_lst[k].sims_health_camp_report_code == "" || $scope.getDeails[i].parameter_lst[j].check_lst[k].sims_health_camp_report_code == undefined) {
                          }
                          else {
                              var reportCardNo = $scope.getDeails[i].parameter_lst[j].check_lst[k].sims_health_camp_report_code;
                              break;
                          }
                      }
                  }
              }
              console.log("reportCardNo", reportCardNo);

              var object = {
                  sims_health_camp_code: $scope.edt.sims_health_camp_code,
                  sims_health_enroll_number: $scope.temp.enroll_number,
                  sims_health_camp_employee_code: $rootScope.globals.currentUser.username,
                  sims_health_camp_transaction_date: $scope.temp.entry_date,
                  sims_health_camp_report_code: reportCardNo,
                  sims_health_camp_student_absent_status: $scope.temp.stud_absent,
                  comn_audit_user_location: $scope.temp.latitude + ',' + $scope.temp.longitude,
                  comn_audit_ip: $rootScope.ip_add
                  //sims_health_camp_report_exipry_date: $scope.temp.entry_date,
                  //sims_health_camp_report_status:'A'
              }

              console.log("object", object);

              if ($scope.temp.stud_absent == true) {
                  var data = {
                      sims_health_test_code: '',
                      sims_health_parameter_code: '',
                      sims_health_camp_parameter_check_value: '',
                      sims_health_camp_parameter_check_comment: '',
                      sims_health_parameter_check_code: '',
                      sims_health_camp_transaction_date: $scope.temp.entry_date,
                      sims_health_camp_employee_code: $rootScope.globals.currentUser.username,
                      sims_health_camp_transaction_status: 'A',
                      sims_health_camp_code: $scope.edt.sims_health_camp_code,
                      sims_health_enroll_number: $scope.temp.enroll_number
                  }
                  sendData.push(data);
              }
              //else {
              //    for (var i = 0; i < $scope.getDeails.length; i++) {                
              //        for (var j = 0; j < $scope.getDeails[i].parameter_lst.length; j++) {
              //            for (var k = 0; k < $scope.getDeails[i].parameter_lst[j].check_lst.length; k++) {

              //                if ($scope.getDeails[i].testChange == true) {
              //                    if ($scope.getDeails[i].parameter_lst[j].paraChange == true) {
              //                        //if ($scope.getDeails[i].parameter_lst[j].check_lst[k].checkChange == true){

              //                        //var radioValue = $("input[name=" + $scope.getDeails[i].parameter_lst[j].sims_health_parameter_name_en + "]:checked").val();
              //                        //if ($("input[name=" +$scope.getDeails[i].parameter_lst[j].sims_health_parameter_name_en + "][value=" + $scope.getDeails[i].parameter_lst[j].check_lst[k].sims_health_parameter_check_code + "]").prop("checked")) {

              //                        if ($("#" + $scope.getDeails[i].parameter_lst[j].sims_health_parameter_code + '-' + $scope.getDeails[i].parameter_lst[j].check_lst[k].sims_health_parameter_check_code).prop("checked")) {                                      
              //                            var radiochecked = $scope.getDeails[i].parameter_lst[j].check_lst[k].sims_health_parameter_check_code;
              //                                console.log("radiochecked", radiochecked);                                      
              //                                var data = {
              //                                    sims_health_test_code: $scope.getDeails[i].sims_health_test_code,
              //                                    sims_health_parameter_code: $scope.getDeails[i].parameter_lst[j].sims_health_parameter_code,
              //                                    sims_health_camp_parameter_check_value: $scope.getDeails[i].parameter_lst[j].sims_health_camp_parameter_check_value,
              //                                    sims_health_camp_parameter_check_comment: $scope.getDeails[i].parameter_lst[j].sims_health_camp_parameter_check_comment,
              //                                    //sims_health_parameter_check_code: $scope.getDeails[i].parameter_lst[j].check_lst[k].sims_health_parameter_check_code,
              //                                    sims_health_parameter_check_code: radiochecked,//$scope.getDeails[i].parameter_lst[j].check_lst[k].sims_health_camp_parameter_check_code,
              //                                    sims_health_camp_transaction_date: $scope.temp.entry_date,
              //                                    sims_health_camp_employee_code: $rootScope.globals.currentUser.username,
              //                                    sims_health_camp_transaction_status: 'A',
              //                                    sims_health_camp_code: $scope.edt.sims_health_camp_code,
              //                                    sims_health_enroll_number: $scope.temp.enroll_number
              //                                    //sims_health_camp_report_code: reportCardNo
              //                                }
              //                                sendData.push(data);
              //                            }
              //                  // }
              //                }
              //            }

              //           } // for
              //        }// for
              //     }// for4
              //}              
              console.log("sendData ", sendData);

              if (sendData.length > 0 || $scope.temp.stud_absent == true) {
                  $http.post(ENV.apiUrl + "api/CampTransaction/AddCampDetails?data1=" + JSON.stringify(object), sendData).then(function (msg) {
                      $scope.CampMessgae = msg.data;
                      console.log("post", $scope.CampMessgae);
                      if ($scope.CampMessgae == true) {
                          swal({ text: 'Camp Details Added Successfully', imageUrl: "assets/img/check.png", width: 340, showCloseButton: true });
                      }
                      else if ($scope.CampMessgae == false) {
                          swal({ text: 'Camp Details Not Added', imageUrl: "assets/img/check.png", width: 340, showCloseButton: true });
                      }
                      else {
                          swal({ text: $scope.CampMessgae, width: 340, showCloseButton: true });
                      }
                      $scope.resetData();
                  });
              }
              else {
                  swal({ text: 'Please select any one records to update !!', imageUrl: "assets/img/check.png", width: 340, showCloseButton: true });
                  $scope.submitBtnDisabled = false;
              }
          }

          $scope.Update_Data = function (obj, myform) {
              debugger
              //if ($(myform.target.form).hasClass('ng-valid')) {
              console.log("update", $scope.getDeails);
              console.log("obj", obj);
                  console.log("myform events", myform);
              $scope.submitBtnDisabled = true;
              var sendData = [];
              var reportCardNo = '';

              debugger;
              for (var i = 0; i < $scope.getDeails.length; i++) {
                  for (var j = 0; j < $scope.getDeails[i].parameter_lst.length; j++) {
                      for (var k = 0; k < $scope.getDeails[i].parameter_lst[j].check_lst.length; k++) {
                          if ($scope.getDeails[i].parameter_lst[j].check_lst[k].sims_health_camp_report_code == "" || $scope.getDeails[i].parameter_lst[j].check_lst[k].sims_health_camp_report_code == undefined) {
                          }
                          else {
                              var reportCardNo = $scope.getDeails[i].parameter_lst[j].check_lst[k].sims_health_camp_report_code;
                              break;
                          }
                      }
                  }
              }
              console.log("reportCardNo", reportCardNo);

              var object = {
                  sims_health_camp_code: $scope.edt.sims_health_camp_code,
                  sims_health_enroll_number: $scope.temp.enroll_number,
                  sims_health_camp_employee_code: $rootScope.globals.currentUser.username,
                  sims_health_camp_transaction_date: $scope.temp.entry_date,
                  sims_health_camp_report_code: reportCardNo,
                  sims_health_camp_student_absent_status: $scope.temp.stud_absent,
                  comn_audit_user_location: $scope.temp.latitude + ',' + $scope.temp.longitude,
                  comn_audit_ip: $rootScope.ip_add
                  //sims_health_camp_report_exipry_date: $scope.temp.entry_date,
                  //sims_health_camp_report_status:'A'
              }

              console.log("object", object);

                  if ($(myform.target.form).hasClass('ng-valid')) {
                  for (var i = 0; i < $scope.getDeails.length; i++) {
                      for (var j = 0; j < $scope.getDeails[i].parameter_lst.length; j++) {

                          for (var k = 0; k < $scope.getDeails[i].parameter_lst[j].check_lst.length; k++) {
                              if (obj.sims_health_test_code == $scope.getDeails[i].sims_health_test_code) {

                                  if ($scope.getDeails[i].testChange == true) {
                                      if ($scope.getDeails[i].parameter_lst[j].paraChange == true) {
                                          //if ($scope.getDeails[i].parameter_lst[j].check_lst[k].checkChange == true){

                                          //var radioValue = $("input[name=" + $scope.getDeails[i].parameter_lst[j].sims_health_parameter_name_en + "]:checked").val();
                                          //if ($("input[name=" +$scope.getDeails[i].parameter_lst[j].sims_health_parameter_name_en + "][value=" + $scope.getDeails[i].parameter_lst[j].check_lst[k].sims_health_parameter_check_code + "]").prop("checked")) {

                                          if ($("#" + $scope.getDeails[i].parameter_lst[j].sims_health_parameter_code + '-' + $scope.getDeails[i].parameter_lst[j].check_lst[k].sims_health_parameter_check_code).prop("checked")) {
                                              var radiochecked = $scope.getDeails[i].parameter_lst[j].check_lst[k].sims_health_parameter_check_code;
                                              console.log("radiochecked", radiochecked);
                                              var data = {
                                                  sims_health_test_code: $scope.getDeails[i].sims_health_test_code,
                                                  sims_health_parameter_code: $scope.getDeails[i].parameter_lst[j].sims_health_parameter_code,
                                                  sims_health_camp_parameter_check_value: $scope.getDeails[i].parameter_lst[j].sims_health_camp_parameter_check_value,
                                                  sims_health_camp_parameter_check_comment: $scope.getDeails[i].parameter_lst[j].sims_health_camp_parameter_check_comment,
                                                  //sims_health_parameter_check_code: $scope.getDeails[i].parameter_lst[j].check_lst[k].sims_health_parameter_check_code,
                                                  sims_health_parameter_check_code: radiochecked,//$scope.getDeails[i].parameter_lst[j].check_lst[k].sims_health_camp_parameter_check_code,
                                                  sims_health_camp_transaction_date: $scope.temp.entry_date,
                                                  sims_health_camp_employee_code: $rootScope.globals.currentUser.username,
                                                  sims_health_camp_transaction_status: 'A',
                                                  sims_health_camp_code: $scope.edt.sims_health_camp_code,
                                                  sims_health_enroll_number: $scope.temp.enroll_number
                                                  //sims_health_camp_report_code: reportCardNo
                                              }
                                              sendData.push(data);
                                          }
                                          // }
                                      }
                                  }
                              }

                          } // for
                      }// for
                  }// for4
              }
              else {
                  swal({ text: 'Please enter all fields !!', imageUrl: "assets/img/check.png", width: 340, showCloseButton: true });
                  $scope.submitBtnDisabled = false;
              }


              console.log("sendData ", sendData);

                  if (sendData.length > 0 ) {
                  $http.post(ENV.apiUrl + "api/CampTransaction/AddCampDetails?data1=" + JSON.stringify(object), sendData).then(function (msg) {
                      $scope.CampMessgae = msg.data;
                      console.log("post", $scope.CampMessgae);
                      if ($scope.CampMessgae == true) {
                          swal({ text: 'Camp Details Added Successfully', imageUrl: "assets/img/check.png", width: 340, showCloseButton: true });

                      }
                      else if ($scope.CampMessgae == false) {
                          swal({ text: 'Camp Details Not Added', imageUrl: "assets/img/check.png", width: 340, showCloseButton: true });
                      }
                      else {
                          swal({ text: $scope.CampMessgae, width: 340, showCloseButton: true });
                      }
                      $scope.get_Details();
                      //$scope.resetData();
                  });
              }
              
          };

          var dom;
          var count = 0;
          $scope.flag = true;
          var innerTable;

          $scope.expand = function (info, $event) {
              if (info['isexpanded']) {
                  info['isexpanded'] = false;
                  info['sims_icon'] = "fa fa-plus-circle";
              }
              else {
                  info['isexpanded'] = true;
                  info['sims_icon'] = "fa fa-minus-circle";

              }
          }


          $scope.resetData = function () {
              debugger;
              $scope.temp.stud_obj = '';
              $scope.temp.enroll_number = '';
              $scope.hide_submit = false;
              $scope.hide_grid = false;
              $scope.showStudDetails = false;
              $scope.submitBtnDisabled = false;
              //$scope.showDetails = false;
              //$scope.temp.sims_cur_code = $scope.curriculum[0].sims_cur_code;
              //$scope.temp.sims_academic_year = $scope.Acc_year[0].sims_academic_year;

              $("#Selectstud").select2("val", "");

              try {
                  $("#grades").multipleSelect("uncheckAll");
              }
              catch (e) { }
              try {
                  $("#sections").multipleSelect("uncheckAll");
              }
              catch (e) { }
          }

          $scope.openAdmissionForm = function () {
              $state.go('main.ClaRAS');
          };

          $scope.cariesArray = [];


              $scope.onClick = function (number, str, para,row) {
                  debugger;
              var id = '', otherId = '';
              var comment = [];
                  row.testChange = true;
                  para.paraChange = true;
              //if (para.sims_health_camp_parameter_check_comment != undefined) {
              //    comment = para.sims_health_camp_parameter_check_comment;
              //}
              var tempClr = '';
              switch (str) {
                  case 'D': tempClr = '#FF0000'; id = '#D' + number + ' path'; otherId = '#F' + number + ' path, #M' + number + ' path'; break;
                  case 'F': tempClr = '#008000'; id = '#F' + number + ' path'; otherId = '#D' + number + ' path, #M' + number + ' path'; break;
                  case 'M': tempClr = '#FFFF00'; id = '#M' + number + ' path'; otherId = '#D' + number + ' path, #F' + number + ' path'; break;
              }
              var selTeeth = $scope.teethDetails.find(function (cr) { return cr.teethNo === number })
              if (selTeeth === undefined) {
                  $scope.teethDetails[$scope.teethDetails.length] = { teethNo: number, isFilled: true, teethType: str };
                  $(id).attr('fill', tempClr);
              } else {
                  var selTeethTemp = $scope.teethDetails.find(function (cr) { return cr.teethNo === number && cr.teethType === str })
                  if (selTeethTemp !== undefined) {
                      $scope.teethDetails = $scope.teethDetails.filter(function (cr) { return cr.teethNo !== number })
                      $(id).attr('fill', '#FFFFFF');
                      $(otherId).attr('fill', '#FFFFFF');
                  } else {
                      $(id).attr('fill', tempClr);
                      $(otherId).attr('fill', '#FFFFFF');
                      $scope.teethDetails.forEach(function (cr) {
                          cr.teethNo === number && cr.teethType !== str && (cr.teethType = str)
                      })
                      //$scope.teethDetails[selTeeth.ind].teethType = str;
                  }
              }

              //console.log("number", number);
              //if ($scope.cariesArray.length == 0) {
              //    var obj = {
              //        no: number,
              //        carrisValue: str + '-' + number
              //    }
              //    comment += str + '-' + number + ',';
              //    $scope.cariesArray.push(obj);
              //}
              //else {
              //    for (var i = 0; i < $scope.cariesArray.length; i++) {
              //        var flag = 1;
              //        if ($scope.cariesArray[i].no == number) {
              //            flag = 0;
              //            break;
              //        }
              //    }
              //    //if(flag == 0) {
              //    //    swal({ text: 'Please select any one records to update !!', imageUrl: "assets/img/check.png", width: 340, showCloseButton: true });
              //    //}

              //    if (flag == 1) {
              //        var obj = {
              //            no: number,
              //            carrisValue: str + '-' + number
              //        }
              //        comment += str + '-' + number + ',';
              //        $scope.cariesArray.push(obj);
              //    }
              //}
              //console.log('cariesArray', $scope.cariesArray);
              $scope.teethDetails.forEach(function (cr) {
                  comment[comment.length] = cr.teethType + '-' + cr.teethNo
              })
              comment = comment.join(',');
              comment += ','
              para.sims_health_camp_parameter_check_comment = comment;
              //para.sims_health_camp_parameter_check_comment = comment.join(',') //$scope.cariesArray.join(',');
              //console.log('sims_health_camp_parameter_check_comment', comment)
          }
      }])
})();