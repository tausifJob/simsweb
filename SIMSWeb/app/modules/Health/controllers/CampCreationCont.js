﻿(function () {
    'use strict';
    var del = [];
    var main;
    var date1;
    var date3;
    var photo_filename;
    var camp_image1, camp_image2, camp_image3, camp_image4
    var simsController = angular.module('sims.module.Health');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CampCreationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.acadyr_data = [];
            $scope.edit_code = false;
            $scope.edt = {};
            var formdata = new FormData();
            var data1 = [];
            var deletecode = [];
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;


            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                
            });

            if ($http.defaults.headers.common['schoolId'] == 'asd') {
                $scope.showASDFlag = true;
            }

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 5;



            $http.get(ENV.apiUrl + "api/health/Camp/getCampDetails").then(function (res) {
                debugger;
                $scope.display = false;
                $scope.grid = true;
                $scope.camp_details = res.data;
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/';
                //$scope.sims_health_camp_img1 = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/' + $scope.camp_details['sims_health_camp_img1'];
                console.log($scope.sims_health_camp_img1);
                $scope.totalItems = $scope.camp_details.length;
                $scope.todos = $scope.camp_details;
                $scope.makeTodos();
            });
           

            $http.get(ENV.apiUrl + "api/health/Camp/getPackageList").then(function (res) {
                $scope.packageList = res.data;

            });

            $http.get(ENV.apiUrl + "api/health/Camp/getCurriculumData").then(function (res) {
                $scope.CurriculumData = res.data;

            });

            $http.get(ENV.apiUrl + "api/health/Camp/getDoctorsList").then(function (res) {
                $scope.Doctors = res.data;

            });

            $scope.getCampData = function () 
            {
                $http.get(ENV.apiUrl + "api/health/Camp/getCampDetails").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.camp_details = res.data;
                    $scope.totalItems = $scope.camp_details.length;
                    $scope.todos = $scope.camp_details;
                    $scope.makeTodos();
                });
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edit = function (str) {
                debugger;
                $scope.doctorList = [];
                $scope.employee=[];
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $http.get(ENV.apiUrl + "api/health/Camp/getDoctorsData?camp_code=" + str.sims_health_camp_code).then(function (res) {
                        $scope.doctorList = res.data;
                        if ($scope.doctorList.length > 0) {
                            
                            $scope.getSelectedDr();
                        }
                    });
                    for (var j = 0; j < $scope.Doctors.length; j++) {
                        $scope.Doctors[j].emp_check = false;

                    }
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = false;
                    $scope.update1 = true;
                    $scope.delete1 = false;
                   
                    /*$scope.edt =
                        {
                            sims_health_camp_name_en: str.sims_health_camp_name_en,
                            sims_health_camp_name_ot: str.sims_health_camp_name_ot,
                            sims_health_cur_code: str.sims_health_cur_code,
                            sims_health_camp_desc: str.sims_health_camp_desc,
                            sims_health_camp_date: str.sims_health_camp_date,
                            sims_health_camp_start_date: str.sims_health_camp_start_date,
                            sims_health_camp_img1: str.sims_health_camp_img1,
                        }
                    */
                    $scope.edt = str;
                    $scope.sims_health_camp_start_date=str.sims_health_camp_start_date;
                    $scope.sims_health_camp_end_date=str.sims_health_camp_end_date;
                    $scope.sims_health_camp_code = str.sims_health_camp_code;
                    $scope.sims_health_camp_img1 = $scope.url + str.sims_health_camp_img1;
                    $scope.sims_health_camp_img2 = $scope.url + str.sims_health_camp_img2;
                    $scope.sims_health_camp_img3 = $scope.url + str.sims_health_camp_img3;
                    $scope.sims_health_camp_img4 = $scope.url + str.sims_health_camp_img4;
                    $scope.edit_code = true;
                    
                    
                }
            }
            $scope.getSelectedDr = function () {
                debugger;
                for (var j = 0; j < $scope.Doctors.length; j++) {
                    $scope.Doctors[j].emp_check = false;

                }

                if ($scope.doctorList.length > 0) {
                    for (var i = 0; i < $scope.doctorList.length; i++) {
                        for (var j = 0; j < $scope.Doctors.length; j++) {
                            if ($scope.doctorList[i].sims_health_camp_employee_code == $scope.Doctors[j].em_number) {
                                $scope.Doctors[j].emp_check = true;
                                $scope.employee.push($scope.Doctors[j]);
                                for (var k = 0; k < $scope.employee.length; k++) {
                                    $scope.employee[k].sims_health_camp_employee_start_date = $scope.sims_health_camp_start_date;
                                    $scope.employee[k].sims_health_camp_employee_end_date = $scope.sims_health_camp_end_date;

                                }
                            }
                        }
                    }
                }
            }
            $scope.file_changed = function (element, name) {
                debugger;
                var str = '';

                str = element.files[0]['name'].split(".")[0];

                var photofile = element.files[0];
                photo_filename = (photofile.type)
                $scope.edt.sims_health_camp_img1 = str + '.' + photo_filename.split("/")[1];
                camp_image1 = photo_filename.replace(photo_filename, str);

                var reader = new FileReader();

                reader.onload = function (e) {
                    $scope.$apply(function () {

                        $scope.sims_camp_img1 = e.target.result;


                    });
                };
                reader.readAsDataURL(photofile);

                console.log("student_image", camp_image1);

                var request = {
                    method: 'POST',
                    //url: ENV.apiUrl + 'api/file/upload?filename=' + student_image + '&location=StudentImages',
                    url: ENV.apiUrl + '/api/file/upload?filename=' + camp_image1 + "&location=" + "/StudentImages",
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request)
                       .success(function (d) {

                       }, function () {
                           alert("Err");
                       });


            };
            $scope.getTheFiles = function ($files) {

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };
            $scope.file_changed_img2 = function (element, name) {
                debugger;
                var str = '';

                str = element.files[0]['name'].split(".")[0];

                var photofile = element.files[0];
                photo_filename = (photofile.type)
                $scope.edt.sims_health_camp_img2 = str + '.' + photo_filename.split("/")[1];
                camp_image2 = photo_filename.replace(photo_filename, str);

                var reader = new FileReader();

                reader.onload = function (e) {
                    $scope.$apply(function () {

                        $scope.sims_camp_img2 = e.target.result;


                    });
                };
                reader.readAsDataURL(photofile);

                console.log("student_image", camp_image2);

                var request = {
                    method: 'POST',
                    //url: ENV.apiUrl + 'api/file/upload?filename=' + student_image + '&location=StudentImages',
                    url: ENV.apiUrl + '/api/file/upload?filename=' + camp_image2 + "&location=" + "/StudentImages",
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request)
                       .success(function (d) {

                       }, function () {
                           alert("Err");
                       });


            };
            $scope.file_changed_img3 = function (element, name) {
                debugger;
                var str = '';

                str = element.files[0]['name'].split(".")[0];

                var photofile = element.files[0];
                photo_filename = (photofile.type)
                $scope.edt.sims_health_camp_img3 = str + '.' + photo_filename.split("/")[1];
                camp_image3 = photo_filename.replace(photo_filename, str);

                var reader = new FileReader();

                reader.onload = function (e) {
                    $scope.$apply(function () {

                        $scope.sims_camp_img3 = e.target.result;


                    });
                };
                reader.readAsDataURL(photofile);

                console.log("student_image", camp_image3);

                var request = {
                    method: 'POST',
                    //url: ENV.apiUrl + 'api/file/upload?filename=' + student_image + '&location=StudentImages',
                    url: ENV.apiUrl + '/api/file/upload?filename=' + camp_image3 + "&location=" + "/StudentImages",
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request)
                       .success(function (d) {

                       }, function () {
                           alert("Err");
                       });


            };
            $scope.file_changed_img4 = function (element, name) {
                debugger;
                var str = '';

                str = element.files[0]['name'].split(".")[0];

                var photofile = element.files[0];
                photo_filename = (photofile.type)
                $scope.edt.sims_health_camp_img4 = str + '.' + photo_filename.split("/")[1];
                camp_image4 = photo_filename.replace(photo_filename, str);

                var reader = new FileReader();

                reader.onload = function (e) {
                    $scope.$apply(function () {

                        $scope.sims_camp_img4 = e.target.result;


                    });
                };
                reader.readAsDataURL(photofile);

                console.log("student_image", camp_image4);

                var request = {
                    method: 'POST',
                    //url: ENV.apiUrl + 'api/file/upload?filename=' + student_image + '&location=StudentImages',
                    url: ENV.apiUrl + '/api/file/upload?filename=' + camp_image4 + "&location=" + "/StudentImages",
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request)
                       .success(function (d) {

                       }, function () {
                           alert("Err");
                       });


            };
            /*$scope.getImg1 = function ($files) {
                debugger;
                $scope.edt.sims_health_camp_img1 = $files[0].name;

            }*/
           /* $scope.getImg2 = function ($files) {
                debugger;
                $scope.edt.sims_health_camp_img2 = $files[0].name;

            }
            $scope.getImg3 = function ($files) {
                debugger;
                $scope.edt.sims_health_camp_img3 = $files[0].name;

            }
            $scope.getImg4 = function ($files) {
                debugger;
                $scope.edt.sims_health_camp_img4 = $files[0].name;

            }*/
            $scope.employee = [];
            $scope.getEMP = function (obj, index) {
                debugger;
                
                if (obj.emp_check == true)
                {
                    $scope.employee.push(obj);
                    for (var j = 0; j < $scope.employee.length; j++) {
                        $scope.employee[j].sims_health_camp_employee_start_date = $scope.edt.sims_health_camp_start_date;
                        $scope.employee[j].sims_health_camp_employee_end_date = $scope.edt.sims_health_camp_end_date;

                    }
                    console.log($scope.employee);
                    
                } else
                {
                    for (var k = 0; k < $scope.employee.length; k++) {
                        if ($scope.employee[k].em_number == obj.em_number) {
                            $scope.employee.splice(k, 1);
                        }
                    }
                }
                
            }

            var emp, emp_code;
            $scope.Save = function (isvalidate) {
                debugger;
                emp_code = '';
                var list;
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        
                        var data = $scope.edt;
                        data.opr = 'I';
                        data.sims_health_camp_created_by = user;
                      
                        if ($scope.Doctors.length > 0) {
                            for (var j = 0; j < $scope.Doctors.length; j++) {
                                if ($scope.Doctors[j].emp_check == true) {
                                    emp = $scope.Doctors[j].em_number;
                                    emp_code = emp_code + ',' + emp;
                                   
                                }
                            }
                        }
                        
                        
                        $http.post(ENV.apiUrl + "api/health/Camp/CUDInsertCamp", data).then(function (res) {
                            $scope.display = true;
                            $scope.campcode = res.data;
                            
                            if ($scope.campcode) {
                                if ($scope.employee.length > 0) {
                                    for (var k = 0; k < $scope.employee.length; k++) {
                                        $scope.employee[k].sims_health_camp_code = $scope.campcode;
                                        $scope.employee[k].sims_health_camp_user_created_by = user;
                                        $scope.employee[k].opr = 'H';
                                        $scope.employee[k].sims_health_camp_user_status = 'A'
                                    }

                                    $http.post(ENV.apiUrl + "api/health/Camp/CUDEmpDetails", $scope.employee).then(function (result) {
                                        $scope.Message = result.data;
                                        if ($scope.Message == true) {
                                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                                if (isConfirm) {
                                                    $scope.getCampData();

                                                }
                                            });
                                        }
                                        else {
                                            swal({ text: "Record not Inserted ", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                                if (isConfirm) {
                                                    $scope.getCampData();
                                                }
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getCampData();
                                        }
                                    });
                                }
                              }
                            else
                            {
                                swal({ text: "Record not Inserted ", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getCampData();
                                    }
                                });
                            }
                        });

                       
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }

            }

            $scope.getgrid = function () {
                $scope.currentPage = 1;
                $http.get(ENV.apiUrl + "api/common/AcademicYear/getAcademicYear").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.acadyr_data = res.data;
                    $scope.totalItems = $scope.acadyr_data.length;
                    $scope.todos = $scope.acadyr_data;
                    $scope.makeTodos();
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                debugger;
                var data1 = [];
                if (isvalidate) {
                   /* var data = ({
                        sims_cur_code: $scope.edt.sims_cur_code,
                        sims_academic_year: $scope.edt.sims_academic_year,
                        sims_academic_year_start_date: $scope.edt.sims_academic_year_start_date,
                        sims_academic_year_end_date: $scope.edt.sims_academic_year_end_date,
                        sims_academic_year_status: $scope.edt.sims_academic_year_status,
                        sims_academic_year_desc: $scope.edt.sims_academic_year_desc,
                        opr: 'U'
                    });*/
                    var data = $scope.edt;
                    data.opr = 'U';
                    data.sims_health_camp_created_by = user;
                    data.sims_health_camp_code = $scope.sims_health_camp_code;
                    //data1.push(data);
                    
                    $http.post(ENV.apiUrl + "api/health/Camp/CUDInsertCamp", data).then(function (res) {
                        $scope.display = true;
                        $scope.camp_Code = res.data;
                        if ($scope.camp_Code) {
                            if ($scope.employee.length > 0) {
                                for (var k = 0; k < $scope.employee.length; k++) {
                                    $scope.employee[k].sims_health_camp_code = $scope.camp_Code;
                                    $scope.employee[k].sims_health_camp_user_created_by = user;
                                    $scope.employee[k].opr = 'A';
                                    $scope.employee[k].sims_health_camp_user_status = 'A'

                                }

                                $http.post(ENV.apiUrl + "api/health/Camp/CUDEmpDetails", $scope.employee).then(function (result) {
                                    $scope.Message = result.data;
                                    if ($scope.Message == true) {
                                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getCampData();

                                            }
                                        });
                                    }
                                    else {
                                        swal({ text: "Record not Updated ", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getCampData();
                                            }
                                        });
                                    }
                                });
                            }
                            else {
                                swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getCampData();
                                    }
                                });
                            }
                        } else {
                            swal({ text: "Record not Updated ", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getCampData();
                                }
                            });
                        }
                    });
                }
            }

            $scope.New = function () {
               
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = true;
                    $scope.update1 = false;
                    $scope.delete1 = false;
                    $scope.edt = {};
                    $scope.employee = [];
                    if ($scope.Doctors.length > 0) {
                        for (var j = 0; j < $scope.Doctors.length; j++) {
                            if ($scope.Doctors[j].emp_check == true) {
                                $scope.Doctors[j].emp_check = false;

                            }
                        }
                    }
                   
                    $scope.edit_code = false;

                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_health_camp_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_health_camp_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }

            $scope.Delete = function () {
                debugger;
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    var deletecode = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_health_camp_code + i);

                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodercode = ({
                                'sims_health_academic_year': $scope.filteredTodos[i].sims_health_academic_year,
                                'sims_health_cur_code': $scope.filteredTodos[i].sims_health_cur_code,
                                'sims_health_camp_code': $scope.filteredTodos[i].sims_health_camp_code,
                                'opr': 'D'
                            });
                            deletecode.push(deletemodercode);
                        }
                    }

                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {


                                $http.post(ENV.apiUrl + "api/health/Camp/RemoveCamp", deletecode).then(function (res) {
                                    $scope.msg1 = res.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getCampData();
                                            }
                                        });
                                    }
                                    else  {
                                        swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getCampData();
                                            }
                                        });
                                    }
                                    
                                });


                            }
                            else {
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }

                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                   
                                    var v = document.getElementById($scope.filteredTodos[i].sims_health_camp_code + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }

                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                }
            }

            $scope.size = function (str) {
                if (str == 10 || str == 20) {
                    $scope.pager = true;
                }

                else {

                    $scope.pager = false;
                }
                if (str == "*") {
                    $scope.numPerPage = $scope.camp_details.length;
                    $scope.filteredTodos = $scope.camp_details;
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                }
                $scope.makeTodos();

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.camp_details, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.acadyr_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        // var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_health_camp_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }


            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                //return (item.sims_academic_year == toSearch) ? true : false;
                return (item.sims_health_camp_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_health_package_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_cur_full_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_health_camp_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_health_camp_status.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            //sort
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };
        }])
})();