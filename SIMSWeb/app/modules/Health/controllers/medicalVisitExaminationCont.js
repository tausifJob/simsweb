﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Health');
    simsController.controller('medicalVisitExaminationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.pagesize = "10";
            $scope.display = false;
            var main, deletefin = [];
            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.items = [];
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });
            $scope.url = ENV.apiUrl + 'Content' + '/' + $http.defaults.headers.common['schoolId'];
            var photofile;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100)

            $http.get(ENV.apiUrl + "api/common/Medical_Visit_Examination/GetSims_MedicalVisitExamination").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.CreDiv = res.data;
                $scope.totalItems = $scope.CreDiv.length;
                // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                $scope.todos = $scope.CreDiv;
                $scope.makeTodos();

            });

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    //$scope.makeTodos();
                    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_examination_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_examination_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_examination_code == toSearch) ? true : false;
            }

            $scope.edit = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                $scope.edt = str;
                $scope.display = true;
                $scope.grid = false;
                $scope.update1 = true;
                $scope.editflg = true;
                $scope.save1 = false;
                }
            }


            $scope.Update = function (newEventForm) {
                if (newEventForm.$valid) {
                    var datasend1 = [];
                    var data = $scope.edt;
                    data.opr = 'U';

                    datasend1.push(data);
                    $http.post(ENV.apiUrl + "api/common/Medical_Visit_Examination/InsertSims_MedicalVisitExamination", datasend1).then(function (res) {
                        $scope.ins = res.data;
                        if (res.data==true) {
                            swal({  text: "Record Updated Successfully.", imageUrl: "assets/img/check.png" });
                            $http.get(ENV.apiUrl + "api/common/Medical_Visit_Examination/GetSims_MedicalVisitExamination").then(function (res) {
                                $scope.display = false;
                                $scope.grid = true;
                                $scope.items = res.data;
                                $scope.currentPage = 0;
                            });

                        }
                        else if (res.data == false) {
                            swal({  text: "Record Not Updated ." , imageUrl: "assets/img/notification-alert.png" });
                        }
                        else {
                            swal("Error-" + $scope.ins)
                        }
                    });
                }
            }

            $scope.Save = function (newEventForm) {

                if (newEventForm.$valid) {
                    var datasend = [];
                    var data = $scope.edt;
                    data.opr = 'I';
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/common/Medical_Visit_Examination/InsertSims_MedicalVisitExamination", datasend).then(function (res) {
                        $scope.ins = res.data;
                        if (res.data==true) {
                            swal({ title: "Alert", text: "Record Inserted Successfully.", imageUrl: "assets/img/check.png" });
                            $http.get(ENV.apiUrl + "api/common/Medical_Visit_Examination/GetSims_MedicalVisitExamination").then(function (res) {
                                $scope.display = false;
                                $scope.grid = true;
                                $scope.items = res.data;
                                $scope.currentPage = 0;
                                $scope.getgrid();
                            });

                        }
                        else if (res.data == false) {
                            swal({ text: "Record Not Inserted.", imageUrl: "assets/img/close.png" });
                        }
                        else {
                            swal("Error-" + $scope.ins)
                        }
                        });
                }
            }


            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                $scope.edt = "";
                $scope.grid = false;
                $scope.display = true;
                $scope.save1 = true;
                $scope.editflg = false;
                $scope.update1 = false;
                //$scope.edt.sims_reference_status = true;
                }
            }


            var main, del = '', delvar = [];


            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_examination_code + $scope.filteredTodos[i].sims_examination_name + i);
                        v.checked = true;                        
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_examination_code + $scope.filteredTodos[i].sims_examination_name + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }
            

            $scope.Delete = function () {
                debugger;
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_examination_code + $scope.filteredTodos[i].sims_examination_name + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_examination_code': $scope.filteredTodos[i].sims_examination_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/common/Medical_Visit_Examination/InsertSims_MedicalVisitExamination", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            $scope.display = false;
                                            $scope.grid = true;
                                            $scope.items = res.data;
                                            $scope.currentPage = 0;
                                        }
                                        main = document.getElementById('mainchk');
                                        if (main.checked == true) {
                                            main.checked = false;
                                            {
                                                $scope.row1 = '';
                                            }
                                        }                                        
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {                                           
                                            $scope.getgrid();
                                            main.checked = false;
                                            $('tr').removeClass("row_selected");
                                        };
                                        main = document.getElementById('mainchk');
                                        if (main.checked == true) {
                                            main.checked = false;
                                            {
                                                $scope.row1 = '';
                                            }
                                        }                                        
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_examination_code + $scope.filteredTodos[i].sims_examination_name + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    //  $scope.row1 = '';
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.update1 = false;
                $scope.save1 = false;
                $scope.editflg = false;
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/common/Medical_Visit_Examination/GetSims_MedicalVisitExamination").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.CreDiv = res.data;
                    $scope.totalItems = $scope.CreDiv.length;
                    // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                    $scope.todos = $scope.CreDiv;
                    $scope.makeTodos();

                });
            }
        }]);
})();


