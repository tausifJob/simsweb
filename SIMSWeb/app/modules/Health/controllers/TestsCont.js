﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TestsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.user_access = [];
            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $scope.display = false;
            $scope.grid = true;
            $scope.itemsPerPage = "10";
            $scope.currentPage = 0;
            $scope.items = [];

            var photofile;
            var data = [];
           // $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

   
            $scope.New = function () {

                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.grid = false;
                    $scope.display = true;
                    $scope.save1 = true;
                    $scope.update1 = false;
                    $scope.delete1 = false;
                    $scope.edt = {};
                }
            }

            $scope.edit = function (str) {
                debugger;
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = false;
                    $scope.update1 = true;
                    $scope.delete1 = false;

                   
                    //$scope.getparameter = function () {
                    //    $http.get(ENV.apiUrl + "api/health/getAllParameter").then(function (parameter_data) {
                    //        $scope.parameter = parameter_data.data;
                            

                    //    });
                    //}
             
                    $scope.edt =
                        {
                            sims_health_test_sr_no: str.sims_health_test_sr_no,
                            sims_health_test_code: str.sims_health_test_code,
                            sims_health_test_name_en: str.sims_health_test_name_en,
                            sims_health_test_name_ot: str.sims_health_test_name_ot,
                            sims_health_test_price: str.sims_health_test_price,
                            sims_health_test_display_order: str.sims_health_test_display_order,
                            sims_health_test_status: str.sims_health_test_status,
                            sims_health_parameter_code: str.sims_health_parameter_code

                            
                        }

                    $scope.Update = true;
                }
            }

            $scope.getparameter = function () {
                $http.get(ENV.apiUrl + "api/health/getAllParameter").then(function (parameter_data) {
                    $scope.parameter = parameter_data.data;
                   // console.log("parameter = ", $scope.parameter);
                   
                });
            }
            $scope.getparameter();

            $scope.getgrid = function () {

                $http.get(ENV.apiUrl + "api/health/getAllTestDetails").then(function (res) {
                    debugger;
                    $scope.TestMaster = res.data;
                    $scope.totalItems = $scope.TestMaster.length;
                    $scope.todos = $scope.TestMaster;
                    $scope.makeTodos();
                    if (res.data.length > 0) {
                        $scope.Showsave = false;
                        $scope.rpt_list = res.data;
                    }
                    else {
                        swal('No Record Found');
                    }
                })
            }
            $scope.getgrid();

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.TestMaster.length; i++) {

                        var v = document.getElementById("test-" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    } TestMaster
                }
                else {
                    for (var i = 0; i < $scope.TestMaster.length; i++) {

                        var v = document.getElementById("test-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                var main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            //$scope.getPARAM = function (obj, index) {
            //    debugger;

            //    if (obj.parameter_check == true) {
            //        $scope.parameter.push(obj);
                   
            //    } else {
            //        for (var k = 0; k < $scope.parameter.length; k++) {
            //            if ($scope.parameter[k].em_number == obj.sims_health_parameter_code) {
            //                $scope.parameter.splice(k, 1);
            //            }
            //        }
            //    }

            //}

            var param, sims_health_parameter_code;
            $scope.Save = function (isvalidate) {
                debugger;
                sims_health_parameter_code = '';
                var data = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = $scope.edt;
                        data.opr = 'I';
                        data.user = user;
                       
                        if ($scope.parameter.length > 0) {
                            for (var j = 0; j < $scope.parameter.length; j++) {
                                if ($scope.parameter[j].parameter_check == true) {
                                    param = $scope.parameter[j].sims_health_parameter_code;
                                    sims_health_parameter_code = sims_health_parameter_code + ',' + param;

                                }
                            }
                        }
                        data.sims_health_parameter_code = sims_health_parameter_code;
                       // data.today_date = today_date;
                        $http.post(ENV.apiUrl + "api/health/Healthtest01", data).then(function (res) {

                            $scope.grid = true;
                            $scope.display = false;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Test  Details Added Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {

                                       $scope.getgrid();
                                        //$scope.getparameter();
                                       // $scop.getPARAM();
                                    }

                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: " Test Details Record Already Exists. ", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                        $scope.getparameter();
                                    }
                                });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });
                    }
                    else {
                        
                    }
                }
            }
          
            $scope.Update2 = function (isvalidate) {
                debugger;
                var data = [];
                if (isvalidate) {
                    if ($scope.update1 == true) {
                        var data = $scope.edt;
                        data.opr = 'U';
                        data.sims_health_test_sr_no = sims_health_test_sr_no;
                        data.sims_health_test_code = sims_health_test_code;


                        $http.post(ENV.apiUrl + "api/health/Healthtest01", data).then(function (res) {
                            debugger;
                            $scope.grid = true;
                            $scope.display = false;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Test  Detail Updated Successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {

                                        $scope.getgrid();
                                    }
                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ title: "Alert", text: "Only oneTest  Detail  can be set as current at a time.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {

                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            } 
                        });
                    }
                }
            }

     
            $scope.Delete = function () {
              
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    var deletemodercode = [];
                    //$scope.flag = false;
                    for (var i = 0; i < $scope.TestMaster.length; i++) {
                        var v = document.getElementById("test-" + i);
                        if (v.checked == true) {
                            $scope.flag = true;
                          
                            var deletemodercode = ({
                                

                                'sims_health_test_sr_no': $scope.TestMaster[i].sims_health_test_sr_no,
                               
                                'sims_health_test_code': $scope.TestMaster[i].sims_health_test_code,
                                'sims_health_test_name_en': $scope.TestMaster[i].sims_health_test_name_en,
                                'sims_health_test_name_ot': $scope.TestMaster[i].sims_health_test_name_ot,
                                'sims_health_test_price': $scope.TestMaster[i].sims_health_test_price,
                                'sims_health_test_display_order': $scope.TestMaster[i].sims_health_test_display_order,
                                'sims_health_test_status': $scope.TestMaster[i].sims_health_test_status,
                               // 'sims_health_parameter_code': $scope.TestMaster[i].sims_health_parameter_code,


                                
                                opr: 'D'
                            });
                           
                        }
                    }

                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {


                                $http.post(ENV.apiUrl + "api/health/Healthtest01", deletemodercode).then(function (res) {
                                    debugger;
                                    $scope.msg1 = res.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Test  Details Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Record Already Mapped.Can't be Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });


                            }
                            else {
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }

                                for (var i = 0; i < $scope.TestMaster.length; i++) {

                                    var v = document.getElementById("test-" + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }

                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                }
            }

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    //$scope.makeTodos();
                    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.TestMaster = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };
            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };
  
            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;

            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.items.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;
            };


            var main, del = '', delvar = [];



        }]);

})();


