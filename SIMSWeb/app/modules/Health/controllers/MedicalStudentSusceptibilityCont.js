﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Health');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('MedicalStudentSusceptibilityCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = false;
            $scope.table1 = true;
            $scope.enrollno = true;
            $scope.upperPart = true;
            $scope.enroll_number = [];

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.edt = { 'sims_cur_code': $scope.curriculum[0].sims_cur_code }
                $scope.getAccYear($scope.curriculum[0].sims_cur_code);
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    debugger;
                    $scope.edt = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                        'sims_academic_year': $scope.Acc_year[0].sims_academic_year
                    }
                    $scope.getGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                });

            }

            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });

                $scope.getSection($scope.edt.sims_cur_code, $scope.edt.sims_grade_code, $scope.edt.sims_academic_year)
            }

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }

            $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/getSusceptibilityDesc").then(function (res1) {
                $scope.display = false;
                $scope.table = false;
                $scope.table1 = true;
                $scope.suscept = res1.data;
            });

            debugger;
            $scope.Show_Data = function (cur_code, ac_year, grd_code, sec_code) {
                if (cur_code != undefined && ac_year != undefined) {
                    debugger;
                    $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/GetAllMedicalSusceptibilityCode?cur_name=" + cur_code + "&academic_year=" + ac_year + "&grd_code=" + grd_code + "&sec_code=" + sec_code).then(function (res1) {

                        $scope.obj1 = res1.data;
                        $scope.totalItems = $scope.obj1.length;
                        $scope.todos = $scope.obj1;
                        $scope.makeTodos();
                        debugger;
                        if ($scope.suscept.length == 0) {
                            swal({ title: 'Alert', text: "Please Try Again Record Not Found...", showCloseButton: true, width: 450, height: 200 });
                        }
                        else {
                            $scope.display = false;
                            $scope.table = true;
                            $scope.table1 = true;
                        }
                    })
                }
                else {
                    swal({ title: 'Alert', text: "Please Select Curriculum & Academic Year Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                }
            }

            $scope.expand = function (j, $event) {
                if (j.sims_isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.sims_isexpanded = "grid";
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.sims_isexpanded = "none";
                }
            }


            $rootScope.visible_stud = true;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = false;
            $rootScope.chkMulti = true;


            $scope.SearchStudentWindow = function () {
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }


            $scope.$on('global_cancel', function () {
                debugger;
                if ($scope.SelectedUserLst.length > 0) {
                    for (var i = 0; i < $scope.SelectedUserLst.length; i++) {
                        debugger
                        $scope.enroll_number_record =
                            {
                                student_ENrollno: $scope.SelectedUserLst[i].s_enroll_no,
                                student_name: $scope.SelectedUserLst[i].name,
                                s_class: $scope.SelectedUserLst[i].s_class
                            }
                        $scope.enroll_number.push($scope.enroll_number_record);
                    }
                }
                $scope.em_number = $scope.edt.em_number;
            });

            $scope.Reset = function () {
                $scope.temp = '';
            }

            $scope.Reset_Data = function () {
                $scope.edt =
                {
                    sims_cur_code: '',
                    sims_academic_year: '',
                    sims_grade_code: '',
                    sims_section_code: '',
                }
                $scope.searchText = '';
                $scope.temp = '';
                $scope.display = false;
                $scope.table = false;
                $scope.table1 = true;
            }

            $scope.ClearCheckBox = function () {

                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sims_batch_enroll_number;
                        var v = document.getElementById(t);
                        v.checked = false;
                        Student_bacth_code = [];
                        $scope.row1 = '';

                    }
                    main.checked = false;
                }



            }

            $scope.RemoveEnrollMentNo = function ($event, index, str) {

                str.splice(index, 1);

            }

            $scope.MultipleStudentSelect = function () {

                $scope.enroll_number = [];
                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.student.length; i++) {
                        var t = $scope.student[i].s_enroll_no;
                        var v = document.getElementById(t + i);
                        v.checked = true;

                        $scope.enroll_number.push($scope.student[i]);
                    }
                }
                else {
                    for (var i = 0; i < $scope.student.length; i++) {
                        var t = $scope.student[i].s_enroll_no;
                        var v = document.getElementById(t + i);
                        v.checked = false;
                        $scope.enroll_number = [];
                    }
                }


            }

            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str;
            //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    //$scope.makeTodos();
                    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj1, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj1;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                //return (item.sims_student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_susceptibility_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_enrollment_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.gradeSection_Name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.enrollname == toSearch) ? true : false;
                return (item.sims_student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.gradeSection_Name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_enrollment_no == toSearch) ? true : false;
            }

            $scope.New = function () {
                $scope.upperPart = false;
                $scope.searchStud = false;
                $scope.disabled = false;
                $scope.readonly = true;
                $scope.table = false;
                $scope.table1 = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = "";
                $scope.enrollno = true;
                $scope.ClearCheckBox();

                $scope.temp = "";
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }



            var sims_student_enrolls = [], datasend = [];

            $scope.savedata = function (Myform) {
                debugger
                var data = '';
                if ($scope.enroll_number.length > 0) {
                    if (Myform) {

                        console.log("Enroll no:", $scope.enroll_number);

                        if ($scope.enroll_number.length > 0) {
                            for (var i = 0; i < $scope.enroll_number.length; i++) {
                                for (var j = 0; j < $scope.suscept.length; j++) {
                                    if ($scope.enroll_number[i].sims_susceptibility_code == $scope.suscept[j].sims_susceptibility_code) {
                                        datasend.push({
                                            sims_enrollment_no: $scope.enroll_number[i].student_ENrollno,
                                            sims_susceptibility_code:$scope.enroll_number[i].sims_susceptibility_code,
                                             sims_susceptibility_desc: $scope.enroll_number[i].sims_susceptibility_desc,
                                            opr: 'I'
                                        });
                                    }

                                }
                            }
                            debugger
                            $http.post(ENV.apiUrl + "api/MedicalStudentSusceptibility/CUDMedicalStudentSusceptibility", datasend).then(function (MSG1) {
                                datasend = [];
                                $scope.enroll_number = [];
                                $scope.msg1 = MSG1.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                                $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/GetAllMedicalSusceptibilityCode").then(function (res1) {
                                    $scope.obj1 = res1.data;
                                    $scope.totalItems = $scope.obj1.length;
                                    $scope.todos = $scope.obj1;
                                    $scope.makeTodos();
                                });
                            });
                            $scope.upperPart = true;
                            $scope.display = false;
                            $scope.table = false;
                            $scope.table1 = true;
                            $scope.enroll_number = [];
                            sims_student_enrolls = [];
                            datasend = [];
                        }
                    }
                }
                else {
                    swal({ text: "Please select student then try to save.", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
            }

            $scope.Cancel = function () {
                $scope.upperPart = true;
                $scope.temp = "";
                $scope.table = false;
                $scope.table1 = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.sims_susceptibility_code = "";
                $scope.enrollname = "";

            }


        }])

})();
