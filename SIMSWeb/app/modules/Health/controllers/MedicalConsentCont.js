﻿(function () {
    'use strict';
    var obj1, temp, opr, comp, edt;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Health');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('MedicalConsentCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.search_btn = true;
            $scope.clr_btn = true;
            $scope.Update_btn = false;
            $scope.mainPage = false;
            $scope.table = true;
            $scope.txt_read = true;
            var user = $rootScope.globals.currentUser.username;
            $scope.temps = {};

            var cur;
            var year;
            var grade;
            var section;
            $scope.hide_img = false;
           
           
            $http.get(ENV.apiUrl + "api/Medical_Consent/get_MedicalConsent").then(function (GetConsent) {
              
               // $scope.filteredTodos = GetConsent.data;
                $scope.CreDiv = GetConsent.data;
                $scope.totalItems = $scope.CreDiv.length;
                $scope.todos = $scope.CreDiv;
                $scope.makeTodos();
                
            });

            $scope.size = function (str) {
               
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                 $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

              

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_medical_consent_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                        || item.student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }


            $scope.edts = {};
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                
                $scope.curriculum = AllCurr.data;
                $scope.edts = {
                    sims_cur_code: $scope.curriculum[0].sims_cur_code
                };
                $scope.getAccYear($scope.edts.sims_cur_code);
            });

            $scope.getAccYear = function (curCode) {
               
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = {
                        sims_academic_year: $scope.Acc_year[0].sims_academic_year
                    };                    
                });
            }

            $scope.SearchStudentWindow = function () {
                $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                   
                    $scope.ComboBoxValues = AllComboBoxValues.data;
                    //$scope.temp.sims_academic_year = $scope.ComboBoxValues[0].sims_acadmic_year;
                    //$scope.temp.s_cur_code = $scope.ComboBoxValues[0].sims_cur_code;
                    $scope.edt = {
                        sims_academic_year: $scope.ComboBoxValues[0].sims_acadmic_year,
                        s_cur_code: $scope.ComboBoxValues[0].sims_cur_code,
                    };
                });
                $scope.searchtable = false;
                $scope.student = '';
                $('#MyModal').modal('show');
            }

            $scope.searchstudent = function () {

                $scope.busy = true;
                $scope.searchtable = false;

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.edt)).then(function (Allstudent) {

                    $scope.student = Allstudent.data;

                    $scope.busy = false;

                    $scope.searchtable = true;
                });
            }

            var parent_id;
            $scope.editStud = function (info) {
              
                $scope.temps = {};               
                $scope.temps.enroll_number = info.s_enroll_no;
                $http.get(ENV.apiUrl + "api/Medical_Consent/Get_studentlist?enroll_no=" + $scope.temps.enroll_number).then(function (Allstudent) {
                    
                    if (Allstudent.data.length == 0) {
                        swal('', 'No Record Found/Student Status Inactive');
                        $scope.table = true;
                        $scope.mainPage = false;
                        return;
                    }
                    $scope.savedata = Allstudent.data;
                    $scope.stud = $scope.savedata[0];
                    
                });
            }

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
               
                $scope.ComboBoxValues = AllComboBoxValues.data;
                //$scope.temp.sims_academic_year = $scope.ComboBoxValues[0].sims_acadmic_year;
                //$scope.temp.s_cur_code = $scope.ComboBoxValues[0].sims_cur_code;
                $scope.edt = {
                    sims_academic_year: $scope.ComboBoxValues[0].sims_acadmic_year,
                    s_cur_code: $scope.ComboBoxValues[0].sims_cur_code,
                };
            });
           

            $scope.getsectioncode = function (cur, year, grade) {

                $http.get(ENV.apiUrl + "api/common/SectionTerm/getAllSections?cur_code=" + cur + "&ac_year=" + year + "&g_code=" + grade).then(function (res1) {
                    $scope.sections = res1.data;
                });
            }

            $scope.academic_change = function (acd) {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.global_Search.global_curriculum_code + "&academic_year=" + acd).then(function (res) {
                    $scope.All_grade_names = res.data;
                })
            }
                        
            $scope.New = function()
            {
                $scope.table = false;
                $scope.mainPage = true;
                $scope.divcode_readonly = false;
                $scope.btn_se = false;
                //$scope.temp.sims_academic_year = '';
            }

            $scope.edit = function (str) {
               
                $scope.table = false;
                $scope.mainPage = true;
                $scope.Update_btn = true;
                $scope.save_btn = false;
                $scope.divcode_readonly = true;
                $scope.btn_se = true;
                $scope.combos = true;
                $http.get(ENV.apiUrl + "api/Medical_Consent/Get_studentlist?enroll_no=" + str.sims_medical_consent_enroll_number).then(function (Allstudent) {
                  
                    if (Allstudent.data.length == 0) {
                        swal('', 'No Record Found');
                        return;
                    }
                    $scope.savedata = Allstudent.data;
                    $scope.stud = $scope.savedata[0];
                    $scope.stud.student_name = $scope.savedata[0].student_name;
                    $scope.temps.enroll_number = $scope.savedata[0].sims_enroll_no;
                   
                });
                
                $scope.temp =
                    {
                        sims_medical_consent_cur_code: str.sims_medical_consent_cur_code,
                       // enroll_number: str.sims_medical_consent_enroll_number,
                        
                        sims_medical_consent_parent_number: str.sims_medical_consent_parent_number,
                        sims_medical_consent_health_records: str.sims_medical_consent_health_records,
                        sims_medical_consent_immunization: str.sims_medical_consent_immunization,
                        sims_medical_consent_emergency_treatment: str.sims_medical_consent_emergency_treatment,
                        sims_medical_consent_medical_treatment: str.sims_medical_consent_medical_treatment,
                        sims_medical_consent_over_the_counter_medicine: str.sims_medical_consent_over_the_counter_medicine,
                        sims_medical_consent_medicine_allowed: str.sims_medical_consent_medicine_allowed,
                        //'sims_medical_consent_attr1':
                        //'sims_medical_consent_attr2':
                        //'sims_medical_consent_attr3':
                        //'sims_medical_consent_attr4':
                        //'sims_medical_consent_attr5':
                        sims_medical_consent_remark: str.sims_medical_consent_remark,
                        sims_academic_year: str.sims_medical_consent_applicable_from_acad_year,
                        sims_medical_consent_updated_on: str.sims_medical_consent_updated_on,
                        sims_medical_consent_approved_by: str.sims_medical_consent_approved_by
                    }
                
               // $scope.getAccYear(str.sims_medical_consent_cur_code);
               
                if ($scope.temp.sims_medical_consent_health_records == true) {
                    var v = document.getElementById('yes');
                    v.checked = true;
                }
                else {
                    var c = document.getElementById('no');
                    c.checked = true;
                }
                if ($scope.temp.sims_medical_consent_immunization == true) {
                    var v1 = document.getElementById('yes1');
                    v1.checked = true;
                }
                else {
                    var c1 = document.getElementById('no1');
                    c1.checked = true;
                }
                if ($scope.temp.sims_medical_consent_emergency_treatment == true) {
                    var v2 = document.getElementById('yes2');
                    v2.checked = true;
                }
                else {
                    var c2 = document.getElementById('no2');
                    c2.checked = true;
                }
                if ($scope.temp.sims_medical_consent_medical_treatment == true) {
                    var v3 = document.getElementById('yes3');
                    v3.checked = true;
                }
                else {
                    var c3 = document.getElementById('no3');
                    c3.checked = true;
                }
                if ($scope.temp.sims_medical_consent_over_the_counter_medicine == true) {
                    var v4 = document.getElementById('yes4');
                    v4.checked = true;
                }
                else {
                    var c4 = document.getElementById('no4');
                    c4.checked = true;
                }
                if ($scope.temp.sims_medical_consent_medicine_allowed == true) {
                    var v5 = document.getElementById('yes5');
                    v5.checked = true;
                }
                else {
                    var c5 = document.getElementById('no5');
                    c5.checked = true;
                }
                if ($scope.temp.sims_medical_consent_approved_by == 'F') {
                    var pa = document.getElementById('f');
                    pa.checked = true;
                }
                if ($scope.temp.sims_medical_consent_approved_by == 'M') {
                    var mo = document.getElementById('m');
                    mo.checked = true;
                }
                if ($scope.temp.sims_medical_consent_approved_by == 'G') {
                    var ga = document.getElementById('g');
                    ga.checked = true;
                }
                
            }



            var datasend = [];
            var data;
            var p_id;
            $scope.saveconsentdata = function (Myform) {
              
                
                $http.get(ENV.apiUrl + "api/Medical_Consent/getStudentsParent?enroll=" + $scope.temps.enroll_number).then(function (Parentno) {
                   
                    $scope.parent_id = Parentno.data;
                    p_id = $scope.parent_id[0].sims_medical_consent_parent_number;
                    
                
                    if ($scope.temp.sims_medical_consent_health_records == "undefined" || $scope.temp.sims_medical_consent_health_records == null) 
                    {
                        //$scope.temp.sims_medical_consent_health_records = null;
                        swal({ title: "Alert", text: "Please Select Health Records(Y/N)", width: 300, height: 200 })
                        return;
                    }
                    if ($scope.temp.sims_medical_consent_immunization == "undefined" || $scope.temp.sims_medical_consent_immunization == null) {
                        //$scope.temp.sims_medical_consent_immunization = null;
                        swal({ title: "Alert", text: "Please Select Consent Immunization(Y/N)", width: 300, height: 200 })
                        return;
                    }
                    if ($scope.temp.sims_medical_consent_emergency_treatment == "undefined" || $scope.temp.sims_medical_consent_emergency_treatment == null) {
                        //$scope.temp.sims_medical_consent_emergency_treatment = null;
                        swal({ title: "Alert", text: "Please Select Emergency Treatment(Y/N)", width: 300, height: 200 })
                        return;
                    }

                    if ($scope.temp.sims_medical_consent_medical_treatment == "undefined" || $scope.temp.sims_medical_consent_medical_treatment == null) {
                        //$scope.temp.sims_medical_consent_medical_treatment = null;
                        swal({ title: "Alert", text: "Please Select Medical Treatment(Y/N)", width: 300, height: 200 })
                        return;
                    }
                    if ($scope.temp.sims_medical_consent_over_the_counter_medicine == "undefined" || $scope.temp.sims_medical_consent_over_the_counter_medicine == null)
                    {
                        //$scope.temp.sims_medical_consent_over_the_counter_medicine = null;
                        swal({ title: "Alert", text: "Please Select OTC Medicine(Y/N)", width: 300, height: 200 })
                        return;
                    }
                    if ($scope.temp.sims_medical_consent_medicine_allowed == "undefined" || $scope.temp.sims_medical_consent_medicine_allowed == null) 
                    {
                        //$scope.temp.sims_medical_consent_medicine_allowed = null;
                        swal({ title: "Alert", text: "Please Select Medicicne Allowed(Y/N)", width: 300, height: 200 })
                        return;
                    }
                    if ($scope.temp.sims_medical_consent_approved_by == undefined || $scope.temp.sims_medical_consent_approved_by == '') {
                        swal({ title: "Alert", text: "Please Select Approved By", width: 300, height: 200 })
                        return;
                    }

                //var r1 = $scope.temp.sims_medical_consent_health_records;
                //var r2 = $scope.temp.sims_medical_consent_immunization;
                //var r3 = $scope.temp.sims_medical_consent_emergency_treatment;
                //var r4 = $scope.temp.sims_medical_consent_medical_treatment;
                //var r5 = $scope.temp.sims_medical_consent_over_the_counter_medicine;
                //var r6 = $scope.temp.sims_medical_consent_medicine_allowed;
                if (Myform) {
                   
                    var data =
                        {
                            'opr': 'I',
                            'sims_medical_consent_cur_code': $scope.edt.s_cur_code,
                            'sims_medical_consent_enroll_number': $scope.temps.enroll_number,
                            'sims_medical_consent_parent_number': p_id,
                            'sims_medical_consent_health_records': $scope.temp.sims_medical_consent_health_records,
                            'sims_medical_consent_immunization': $scope.temp.sims_medical_consent_immunization,
                            'sims_medical_consent_emergency_treatment': $scope.temp.sims_medical_consent_emergency_treatment,
                            'sims_medical_consent_medical_treatment': $scope.temp.sims_medical_consent_medical_treatment,
                            'sims_medical_consent_over_the_counter_medicine': $scope.temp.sims_medical_consent_over_the_counter_medicine,
                            'sims_medical_consent_medicine_allowed': $scope.temp.sims_medical_consent_medicine_allowed,
                            //'sims_medical_consent_attr1':
                            //'sims_medical_consent_attr2':
                            //'sims_medical_consent_attr3':
                            //'sims_medical_consent_attr4':
                            //'sims_medical_consent_attr5':
                            'sims_medical_consent_remark': $scope.temp.sims_medical_consent_remark,
                            'sims_medical_consent_applicable_from_acad_year': $scope.temp.sims_academic_year,
                            //'sims_medical_consent_updated_on':null,
                            'sims_medical_consent_approved_by': $scope.temp.sims_medical_consent_approved_by
                    }

                

                    $http.post(ENV.apiUrl + "api/Medical_Consent/CUDSims_MedicalConsent", data).then(function (msg) {
                    $scope.msg1 = msg.data;

                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        $scope.clear();
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record cannot be inserted, Record Already Exits. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }

                    //$scope.stud_Details_fun(cur,year,grade,section);

                });
                $scope.table = true;
                $scope.newdisplay = false;
                datasend = [];
                // }
                // }
                //else {

                //    swal({ text: 'Record Already Exist', width: 300, height: 300 });
                //}
                }
                    });
            }


            $scope.update = function (Myform) {
               

                $http.get(ENV.apiUrl + "api/Medical_Consent/getStudentsParent?enroll=" + $scope.temps.enroll_number).then(function (Parentno) {
                   
                    $scope.parent_id = Parentno.data;
                    p_id = $scope.parent_id[0].sims_medical_consent_parent_number;

                   

                    //var r1 = $scope.temp.sims_medical_consent_health_records;
                    //var r2 = $scope.temp.sims_medical_consent_immunization;
                    //var r3 = $scope.temp.sims_medical_consent_emergency_treatment;
                    //var r4 = $scope.temp.sims_medical_consent_medical_treatment;
                    //var r5 = $scope.temp.sims_medical_consent_over_the_counter_medicine;
                    //var r6 = $scope.temp.sims_medical_consent_medicine_allowed;
                    if (Myform) {

                        var data =
                            {
                                'opr': 'U',
                                'sims_medical_consent_cur_code': $scope.edt.s_cur_code,
                                'sims_medical_consent_enroll_number': $scope.temps.enroll_number,
                                'sims_medical_consent_parent_number': p_id,
                                'sims_medical_consent_health_records': $scope.temp.sims_medical_consent_health_records,
                                'sims_medical_consent_immunization': $scope.temp.sims_medical_consent_immunization,
                                'sims_medical_consent_emergency_treatment': $scope.temp.sims_medical_consent_emergency_treatment,
                                'sims_medical_consent_medical_treatment': $scope.temp.sims_medical_consent_medical_treatment,
                                'sims_medical_consent_over_the_counter_medicine': $scope.temp.sims_medical_consent_over_the_counter_medicine,
                                'sims_medical_consent_medicine_allowed': $scope.temp.sims_medical_consent_medicine_allowed,
                                //'sims_medical_consent_attr1':
                                //'sims_medical_consent_attr2':
                                //'sims_medical_consent_attr3':
                                //'sims_medical_consent_attr4':
                                //'sims_medical_consent_attr5':
                                'sims_medical_consent_remark': $scope.temp.sims_medical_consent_remark,
                                'sims_medical_consent_applicable_from_acad_year': $scope.temp.sims_academic_year,
                                'sims_medical_consent_updated_on': null,
                                'sims_medical_consent_approved_by': $scope.temp.sims_medical_consent_approved_by
                            }



                        $http.post(ENV.apiUrl + "api/Medical_Consent/CUDSims_MedicalConsent", data).then(function (msg) {
                            $scope.msg1 = msg.data;

                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                $scope.clear();
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }

                            //$scope.stud_Details_fun(cur,year,grade,section);

                        });
                        $scope.table = true;
                        $scope.newdisplay = false;
                        datasend = [];
                        // }
                        // }
                        //else {

                        //    swal({ text: 'Record Already Exist', width: 300, height: 300 });
                        //}
                    }
                });
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.Cancel = function () {
                $scope.temp = '';
                $scope.table = true;
                $scope.newdisplay = false;
                $scope.hide_img = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $state.go($state.current, {}, { reload: true });
                //$scope.temp.dest_code = "";
                //$scope.temp.country_code = "";
                //$scope.temp.dest_name = "";
                $scope.mainPage = false;
            }

            $scope.clear = function () {
                $scope.temp = '';
                $scope.hide_img = false;
                $state.go($state.current, {}, { reload: true });
            }

            
          
            $scope.StudAdd = function (info) {
                $scope.temp = { comn_user_name: info.s_enroll_no };
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            var dom;
            $scope.flag = true;
            $scope.expand = function (info, $event) {

                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0' width='100%' style='border:solid;border-width:02px'>" +
                        "<tbody>" +

                         "<tr> <td class='semi-bold'>" + "Issuing Authority" + "</td> <td class='semi-bold'>" + "Teeth" + " </td><td class='semi-bold'>" + "Medication Status" + "</td><td class='semi-bold'>" + "Medication Desc" + "</td>" +
                         "<td class='semi-bold'>" + "Disability Status" + "</td> <td class='semi-bold'>" + "Disability Desc" + " </td><td class='semi-bold'>" + "Restriction Status" + "</td><td class='semi-bold'>" + "Restriction Desc" + "</td></tr>" +

                         "<tr><td>" + (info.sims_health_card_issuing_authority) + "</td> <td>" + (info.sims_teeth) + " </td><td>" + (info.sims_medication_status) + "</td><td>" + (info.sims_medication_desc) + "</td>" +
                        "<td>" + (info.sims_disability_status) + "</td> <td>" + (info.sims_disability_desc) + " </td><td>" + (info.sims_health_restriction_status) + "</td><td>" + (info.sims_health_restriction_desc) + "</td></tr>" +


                         "<tr><td class='semi-bold'>" + "Hearing Status" + "</td> <td class='semi-bold'>" + "Hearing Desc" + " </td><td class='semi-bold'>" + "Vision Status" + "</td><td class='semi-bold'>" + "Vision Desc" + "</td>" +
                         "<td class='semi-bold'>" + "Other Status" + "</td> <td class='semi-bold'>" + "Other Desc" + " </td><td class='semi-bold'>" + "Hospital Name" + "</td><td class='semi-bold'>" + "Hospital Phone No" + "</td></tr>" +


                         "<tr><td>" + (info.sims_health_hearing_status) + "</td> <td>" + (info.sims_health_hearing_desc) + " </td><td>" + (info.sims_health_vision_status) + "</td><td>" + (info.sims_health_vision_desc) + "</td>" +
                        "<td>" + (info.sims_health_other_status) + "</td> <td>" + (info.sims_health_other_desc) + " </td><td>" + (info.sims_regular_hospital_name) + "</td><td>" + (info.sims_regular_hospital_phone) + "</td></tr>" +
                         "<tr> <td class='semi-bold'>" + "Doctor Name" + "</td> <td class='semi-bold'>" + "Doctor Phone No" + "</td><td  colspan='7'></td></tr>" +

                         "<tr><td>" + (info.sims_regular_doctor_name) + "</td> <td>" + (info.sims_regular_doctor_phone) + " </td><td  colspan='7'></td></tr></table>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }
            }

            $scope.CheckedReadonly = function (str) {
                var d = document.getElementById(str);
                if (d.checked == true) {

                    $scope[str] = false;
                }
                else { $scope[str] = true; }
            }

            $scope.access_nos = function (event) {
               
                
                //savedata = [];
                if (event.key == "Tab" || event.key == "Enter") {
                    if ($scope.temps.enroll_number == undefined || $scope.temps.enroll_number == "") {
                        swal('', 'Please Enter Enroll No.')
                        return;
                    }
                    $http.get(ENV.apiUrl + "api/StudentHealth/Get_studentlist?enroll_no=" + $scope.temps.enroll_number).then(function (Allstudent) {
                      
                        if (Allstudent.data.length == 0) {
                            swal('', 'No Record Found/Student Status Inactive');
                            $scope.table = true;
                            $scope.mainPage = false;
                            return;
                        }
                        $scope.savedata = Allstudent.data;
                        $scope.stud = $scope.savedata[0];
                       
                    });
                }
            }

            $scope.radio1Click = function () {               
                var v = document.getElementById('yes');                
                v.checked = true;               
                $scope.temp.sims_medical_consent_health_records = true;
            }
            $scope.radio2Click = function () {          
                var c = document.getElementById('no');                
                c.checked = true;
                $scope.temp.sims_medical_consent_health_records = false;
            }
            $scope.radio3Click = function () {
                var v1 = document.getElementById('yes1');                
                v1.checked = true;                
                $scope.temp.sims_medical_consent_immunization = true;
            }
            $scope.radio4Click = function () {                
                var c1 = document.getElementById('no1');               
                c1.checked = true;
                $scope.temp.sims_medical_consent_immunization = false;
            }
            $scope.radio5Click = function () {
                var v2 = document.getElementById('yes2');               
                v2.checked = true;                
                $scope.temp.sims_medical_consent_emergency_treatment = true;
            }
            $scope.radio6Click = function () {                
                var c2 = document.getElementById('no2');                
                c2.checked = true;
                $scope.temp.sims_medical_consent_emergency_treatment = false;
            }
            $scope.radio7Click = function () {
                var v3 = document.getElementById('yes3');
                v3.checked = true;
                $scope.temp.sims_medical_consent_medical_treatment = true;
            }
            $scope.radio8Click = function () {
                var c3 = document.getElementById('no3');
                c3.checked = true;
                $scope.temp.sims_medical_consent_medical_treatment = false;
            }
            $scope.radio9Click = function () {
                var v4 = document.getElementById('yes4');
                v4.checked = true;
                $scope.temp.sims_medical_consent_over_the_counter_medicine = true;
            }
            $scope.radio10Click = function () {
                var c4 = document.getElementById('no4');
                c4.checked = true;
                $scope.temp.sims_medical_consent_over_the_counter_medicine = false;
            }
            $scope.radio11Click = function () {
                var v5 = document.getElementById('yes5');
                v5.checked = true;
                $scope.temp.sims_medical_consent_medicine_allowed = true;
            }
            $scope.radio12Click = function () {
                var c5 = document.getElementById('no5');
                c5.checked = true;
                $scope.temp.sims_medical_consent_medicine_allowed = false;
            }
            $scope.radio13Click = function () {
                var pa = document.getElementById('f');
                pa.checked = true;
                $scope.temp.sims_medical_consent_approved_by = 'F';
            }
            $scope.radio14Click = function () {
                var mo = document.getElementById('m');
                mo.checked = true;
                $scope.temp.sims_medical_consent_approved_by = 'M';
            }
            $scope.radio15Click = function () {
                var ga = document.getElementById('g');
                ga.checked = true;
                $scope.temp.sims_medical_consent_approved_by = 'G';
            }

        }])
})();