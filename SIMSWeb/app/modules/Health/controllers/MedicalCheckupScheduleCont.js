﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Health');
    simsController.controller('MedicalCheckupScheduleCont',
          ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

              //console.clear();
              $scope.vehicle_data = [];
              $scope.table = true;
              $scope.hide_check_detais = false;
              $scope.hide_cc_detais = false;
              $scope.pagesize = "5";
              $scope.pageindex = "1";
              $rootScope.visible_stud = true;
              $rootScope.chkMulti = true;
              $scope.paymentmode = [];
              var year_end, year_start;

              $timeout(function () {
                  $("#table").tableHeadFixer({ 'top': 1 });
              }, 100);

              setTimeout(function () {
                  $("#cmb_vehicle_code,#cmb_bank_code").select2();
              }, 100);

              $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                  $scope.curriculum = AllCurr.data;
                  $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                  $scope.getAccYear($scope.edt.sims_cur_code);
              });

              $scope.getAccYear = function (curCode) {
                  $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/getAcademicYear?cur_code=" + curCode).then(function (Acyear) {
                      $scope.Acc_year = Acyear.data;
                      $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                      $scope.year_end = $scope.Acc_year[0].sims_academic_year_end_date;
                      $scope.year_start = $scope.Acc_year[0].sims_academic_year_start_date;
                      $scope.getGrade(curCode, $scope.temp.sims_academic_year);
                  });
              }

              var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');
              //var day = dateyear.split("-")[0];
              //var month = dateyear.split("-")[1];
              var year = dateyear.split("-")[2];

              $scope.GetGrade = function () {

                  $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                      $scope.Grade_code = Gradecode.data;
                      //setTimeout(function () {
                      //    $('#cmb_grade_code').change(function () {
                      //        console.log($(this).val());
                      //    }).multipleSelect({
                      //        width: '100%'
                      //    });
                      //}, 1000);
                  });
              }

              $scope.getGrade = function (curCode, accYear) {

                  $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                      $scope.Grade_code = Gradecode.data;
                      //setTimeout(function () {
                      //    $('#cmb_grade_code').change(function () {
                      //        console.log($(this).val());
                      //    }).multipleSelect({
                      //        width: '100%'
                      //    });
                      //}, 1000);
                  });
              }

              //$(function () {
              //    $('#cmb_grade_code').multipleSelect({
              //        width: '100%'
              //    });
              //});

              $scope.getSection = function (curCode, gradeCode, accYear) {
                  $http.get(ENV.apiUrl + "api/StudentReport/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                      $scope.Section_code = Sectioncode.data;
                      setTimeout(function () {
                          $('#cmb_section_code').change(function () {
                              console.log($(this).val());
                          }).multipleSelect({
                              width: '100%'
                          });
                      }, 1000);
                  });
              }

              $(function () {
                  $('#cmb_section_code').multipleSelect({
                      width: '100%'
                  });
              });

              $scope.get_ischecked = function (str) {
                  debugger
                  $scope.dt.fvex_cheque = '';
                  $scope.dt.fvex_cheque = str;
                  if (str == "CY") {
                      $scope.hide_check_detais = true;
                      $scope.hide_cc_detais = false;
                      $scope.hide_cheque_detais = true;
                      $scope.hide_dd_detais = false;
                      $scope.hide_cach_detais = false;

                      //$scope.sdt.sims_special_date='';
                      if ($scope.ydt != undefined) {
                          $scope.ydt.sims_special_date = '';
                      }
                      if ($scope.tdt != undefined) {
                          $scope.tdt.sims_special_date = '';
                      }
                      $scope.mdt = '';
                  }
                  else if (str == "CS") {
                      $scope.hide_check_detais = false;
                      $scope.hide_cc_detais = true;
                      $scope.hide_cheque_detais = false;
                      $scope.hide_dd_detais = false;
                      $scope.hide_cach_detais = false;

                     
                      //$scope.sdt.sims_special_date='';
                      if ($scope.sdt != undefined) {
                          $scope.sdt.sims_year_date = '';
                      }
                      if ($scope.tdt != undefined) {
                          $scope.tdt.sims_special_date = '';
                      }
                      $scope.mdt = '';

                      
                  }
                  else if (str == "CM") {
                      $scope.hide_check_detais = false;
                      $scope.hide_cc_detais = false;
                      $scope.hide_cheque_detais = false;
                      $scope.hide_dd_detais = true;
                      $scope.hide_cach_detais = false;

                      if ($scope.ydt != undefined) {
                          $scope.ydt.sims_special_date = '';
                      }
                      if ($scope.sdt != undefined) {
                          $scope.sdt.sims_year_date = '';
                      }
                      if ($scope.tdt != undefined) {
                          $scope.tdt.sims_special_date = '';
                      }
                      year = dateyear.split("-")[2];

                      $scope.mdt = {
                          sims_jan_date: '01-01-' + year,
                          sims_feb_date: '01-02-' + year,
                          sims_mar_date: '01-03-' + year,
                          sims_apr_date: '01-04-' + year,
                          sims_may_date: '01-05-' + year,
                          sims_jun_date: '01-06-' + year,
                          sims_jly_date: '01-07-' + year,
                          sims_aug_date: '01-08-' + year,
                          sims_sep_date: '01-09-' + year,
                          sims_oct_date: '01-10-' + year,
                          sims_nov_date: '01-11-' + year,
                          sims_dec_date: '01-12-' + year,
                      }
                      $(".datepicker").datepicker({ maxDate: '0' });
                      //$("#jandate").data("kendoDatePicker").value($scope.mdt.sims_jan_date);
                      $(".k-datepicker input").bind('click dblclick', function () {
                          return false;
                      });
                  }
                  else if (str == "CT") {
                      $scope.hide_check_detais = false;
                      $scope.hide_cc_detais = false;
                      $scope.hide_cheque_detais = false;
                      $scope.hide_dd_detais = false;
                      $scope.hide_cach_detais = true;

                      if ($scope.ydt != undefined) {
                          $scope.ydt.sims_special_date = '';
                      }
                      if ($scope.sdt != undefined) {
                          $scope.sdt.sims_year_date = '';
                      }
                      $scope.mdt = '';

                      $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/gettermdate?ac_year=" + $scope.temp.sims_academic_year + "&cur_code=" + $scope.edt.sims_cur_code).then(function (res1) {
                          $scope.term_data = res1.data;

                          console.log("$scope.term_data", $scope.term_data);
                          setTimeout(function () {
                              angular.forEach($scope.term_data, function (value, key) {
                                  console.log("value", value);
                                  $('#' + value.sims_term_code).datepicker({
                                      todayBtn: true,
                                      orientation: "top left",
                                      autoclose: true,
                                      todayHighlight: true,
                                      format: 'dd-mm-yyyy'
                                  });
                                  console.log("key", key);
                              });
                          }, 400);

                      });

                  }

                  var t = document.getElementsByName('Recepient');
                  for (var j = 0; j < t.length; j++) {
                      if (t[j].checked === true) {
                          $scope.paymentmode = t[j].defaultValue;
                      }
                  }

              }

              $scope.size = function (str) {
                  $scope.pagesize = str;
                  $scope.currentPage = 1;
                  $scope.numPerPage = str;
                  console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
              }

              $scope.index = function (str) {
                  $scope.pageindex = str;
                  $scope.currentPage = str;
                  console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                  main.checked = false;
                  $scope.CheckAllChecked();
              }

              $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

              $scope.makeTodos = function () {
                  var rem = parseInt($scope.totalItems % $scope.numPerPage);
                  if (rem == '0') {
                      $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                  }
                  else {
                      $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                  }

                  var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                  var end = parseInt(begin) + parseInt($scope.numPerPage);

                  console.log("begin=" + begin); console.log("end=" + end);

                  $scope.filteredTodos = $scope.todos.slice(begin, end);
              };

              $('*[data-datepicker="true"] input[type="text"]').datepicker({
                  todayBtn: true,
                  orientation: "top left",
                  autoclose: true,
                  todayHighlight: true,
                  format: 'dd-mm-yyyy'
              });

              $scope.showdata = function () {
                  $http.get(ENV.apiUrl + "api/TransportStop/getVehicleExpanseDetails").then(function (res1) {
                      $scope.obj = res1.data;
                      $scope.totalItems = $scope.obj.length;
                      $scope.todos = $scope.obj;
                      $scope.makeTodos();
                      if ($scope.obj.length == 0) {
                          swal({ text: "No Record Found...", imageUrl: "assets/img/close.png", showCloseButton: true, width: 450, height: 200 });
                          $scope.tablehide = false;
                      }
                      else {
                          $scope.tablehide = true;
                      }
                  })
              }

              $scope.showdata();
              //term_
              $scope.Show = function () {
                  $http.get(ENV.apiUrl + "api/StudentReport/getTcCertificateRequiestDate?date_wisesearch=" + $scope.dt.sims_tc_certificate_request_date).then(function (res1) {
                      $scope.obj = res1.data;
                      $scope.totalItems = $scope.obj.length;
                      $scope.todos = $scope.obj;
                      $scope.makeTodos();
                      if ($scope.obj.length == 0) {
                          swal({ text: "No Record Found...", imageUrl: "assets/img/close.png", showCloseButton: true, width: 450, height: 200 });
                          $scope.tablehide = false;

                      }
                      else {
                          $scope.tablehide = true;
                      }
                  })
              }

              $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                  $('input[type="text"]', $(this).parent()).focus();
              });

              $scope.Save = function () {
                  debugger;
                  if ($scope.dt.fvex_cheque == "CY") {
                      var user = $rootScope.globals.currentUser.username;
                      var Savedata = [];

                      var obj = {
                          'sims_cur_code': $scope.edt.sims_cur_code,
                          'sims_academic_year': $scope.temp.sims_academic_year,
                          'sims_grade_code': $scope.edt.sims_grade_code + '',
                          'sims_section_code': $scope.edt.sims_section_code + '',
                          'sims_year_date': $scope.sdt.sims_year_date,
                          opr: 'I'
                      };

                      Savedata.push(obj);

                      $http.post(ENV.apiUrl + "api/MedicalStudentSusceptibility/CUDcheckupdetails", Savedata).then(function (msg) {

                          $scope.msg1 = msg.data;
                          if ($scope.msg1.strMessage != undefined) {
                              if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                  swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                              }
                          }
                          else {
                              swal({ text: "Record Not Inserted...", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                          }
                          $scope.showdata();
                      });

                  }

                  else if ($scope.dt.fvex_cc == "CS") {
                      var user = $rootScope.globals.currentUser.username;
                      var Savedata = [];
                    
                          var obj = {
                              'sims_cur_code': $scope.edt.sims_cur_code,
                              'sims_academic_year': $scope.temp.sims_academic_year,
                              'sims_grade_code': $scope.edt.sims_grade_code + '',
                              'sims_section_code': $scope.edt.sims_section_code + '',
                              'sims_special_date': $scope.ydt.sims_special_date,
                              opr: 'A'

                          };
                      
                      Savedata.push(obj);

                      $http.post(ENV.apiUrl + "api/MedicalStudentSusceptibility/CUDcheckupdetails", Savedata).then(function (msg) {

                          $scope.msg1 = msg.data;
                          if ($scope.msg1.strMessage != undefined) {
                              if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                  swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                              }
                          }
                          else {
                              swal({ text: "Record Not Inserted...", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                          }
                          $scope.showdata();
                      });

                  }

                  else if ($scope.dt.fvex_dd == "CM") {

                      var user = $rootScope.globals.currentUser.username;
                      var Savedata = [];

                      var obj = {
                          'sims_cur_code': $scope.edt.sims_cur_code,
                          'sims_academic_year': $scope.temp.sims_academic_year,
                          'sims_grade_code': $scope.edt.sims_grade_code + '',
                          'sims_section_code': $scope.edt.sims_section_code + '',
                          'sims_jan_date': $scope.mdt.sims_jan_date,
                          'sims_feb_date': $scope.mdt.sims_feb_date,
                          'sims_mar_date': $scope.mdt.sims_mar_date,
                          'sims_apr_date': $scope.mdt.sims_apr_date,
                          'sims_may_date': $scope.mdt.sims_may_date,
                          'sims_jun_date': $scope.mdt.sims_jun_date,
                          'sims_jly_date': $scope.mdt.sims_jly_date,
                          'sims_aug_date': $scope.mdt.sims_aug_date,
                          'sims_sep_date': $scope.mdt.sims_sep_date,
                          'sims_oct_date': $scope.mdt.sims_oct_date,
                          'sims_nov_date': $scope.mdt.sims_nov_date,
                          'sims_dec_date': $scope.mdt.sims_dec_date,
                          opr: 'C'

                      };

                      Savedata.push(obj);

                      $http.post(ENV.apiUrl + "api/MedicalStudentSusceptibility/CUDcheckupdetails", Savedata).then(function (msg) {

                          $scope.msg1 = msg.data;
                          if ($scope.msg1.strMessage != undefined) {
                              if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                  swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                              }
                          }
                          else {
                              swal({ text: "Record Not Inserted...", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                          }
                          $scope.showdata();
                      });

                  }

                  else if ($scope.dt.fvex_cach == "CT") {

                      var user = $rootScope.globals.currentUser.username;
                      var Savedata = [];
                      for (var i = 0; i < $scope.term_data.length; i++) {
                          var obj = {
                              'sims_cur_code': $scope.edt.sims_cur_code,
                              'sims_academic_year': $scope.temp.sims_academic_year,
                              'sims_grade_code': $scope.edt.sims_grade_code + '',
                              'sims_section_code': $scope.edt.sims_section_code + '',
                              'sims_special_date': $scope.term_data[i].sims_special_date,
                              opr: 'Q'

                          };

                          Savedata.push(obj);
                      }
                      $http.post(ENV.apiUrl + "api/MedicalStudentSusceptibility/CUDcheckupdetails", Savedata).then(function (msg) {

                          $scope.msg1 = msg.data;
                          if ($scope.msg1.strMessage != undefined) {
                              if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                  swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                              }
                          }
                          else {
                              swal({ text: "Record Not Inserted...", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                          }
                          $scope.showdata();
                      });

                  }

                  $scope.dt.fvex_cheque = '';
              }

              $scope.searched = function (valLists, toSearch) {
                  return _.filter(valLists,
                  function (i) {
                      return searchUtil(i, toSearch);
                  });
              };

              $scope.search = function () {
                  $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                  $scope.totalItems = $scope.todos.length;
                  $scope.currentPage = '1';
                  if ($scope.searchText == '') {
                      $scope.todos = $scope.obj;
                  }
                  $scope.makeTodos();
              }

              function searchUtil(item, toSearch) {
                  return (item.sims_tc_certificate_request_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sager == toSearch) ? true : false;
              }

              $scope.clear = function () {
                  $scope.edt = {
                      sims_enroll_number: '',
                      sims_tc_certificate_reason: '',
                  }
                  $scope.searchText = '';
              }

              $scope.Reset = function () {

                  $scope.clear();
                  $scope.tablehide = false;
              }

              $('*[data-datepicker="true"] input[type="text"]').datepicker({
                  todayBtn: true,
                  orientation: "top left",
                  autoclose: true,
                  todayHighlight: true,
                  format: 'dd-mm-yyyy'
              });

              $('#test-0').datepicker({
                  todayBtn: true,
                  orientation: "top left",
                  autoclose: true,
                  todayHighlight: true,
                  format: 'dd-mm-yyyy'
              });

              $(document).on('touch click', '#test-0', function (e) {
                  $('input[type="text"]', $(this).parent()).focus();
              });

              $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                  $('input[type="text"]', $(this).parent()).focus();
              });

              $scope.checkdatejan = function () {
                  var month = $scope.mdt.sims_jan_date.split("-")[1];
                  if (month == '01') {
                  }
                  else {
                      swal({ title: "Alert", text: 'Please selecte only January date...', width: 380, height: 200 });
                      $scope.mdt.sims_jan_date = '01-01-' + year;
                  }
              }

              $scope.checkdatefeb = function () {
                  var month = $scope.mdt.sims_feb_date.split("-")[1];
                  if (month == '02') {
                  }
                  else {
                      swal({ title: "Alert", text: 'Please selecte only February date...', width: 380, height: 200 });
                      $scope.mdt.sims_feb_date = '01-02-' + year;
                  }
              }

              $scope.checkdatemar = function () {
                  var month = $scope.mdt.sims_mar_date.split("-")[1];
                  if (month == '03') {
                  }
                  else {
                      swal({ title: "Alert", text: 'Please selecte only March date...', width: 380, height: 200 });
                      $scope.mdt.sims_mar_date = '01-03-' + year;
                  }
              }

              $scope.checkdateapr = function () {
                  var month = $scope.mdt.sims_apr_date.split("-")[1];
                  if (month == '04') {
                  }
                  else {
                      swal({ title: "Alert", text: 'Please selecte only April date...', width: 380, height: 200 });
                      $scope.mdt.sims_apr_date = '01-04-' + year;
                  }
              }

              $scope.checkdatemay = function () {
                  var month = $scope.mdt.sims_may_date.split("-")[1];
                  if (month == '05') {
                  }
                  else {
                      swal({ title: "Alert", text: 'Please selecte only May date...', width: 380, height: 200 });
                      $scope.mdt.sims_may_date = '01-05-' + year;
                  }
              }

              $scope.checkdatejun = function () {
                  var month = $scope.mdt.sims_jun_date.split("-")[1];
                  if (month == '06') {
                  }
                  else {
                      swal({ title: "Alert", text: 'Please selecte only June date...', width: 380, height: 200 });
                      $scope.mdt.sims_jun_date = '01-06-' + year;
                  }
              }

              $scope.checkdatejly = function () {
                  var month = $scope.mdt.sims_jly_date.split("-")[1];
                  if (month == '07') {
                  }
                  else {
                      swal({ title: "Alert", text: 'Please selecte only July date...', width: 380, height: 200 });
                      $scope.mdt.sims_jly_date = '01-07-' + year;
                  }
              }

              $scope.checkdateaug = function () {
                  var month = $scope.mdt.sims_aug_date.split("-")[1];
                  if (month == '08') {
                  }
                  else {
                      swal({ title: "Alert", text: 'Please selecte only August date...', width: 380, height: 200 });
                      $scope.mdt.sims_aug_date = '01-08-' + year;
                  }
              }

              $scope.checkdatesep = function () {
                  var month = $scope.mdt.sims_sep_date.split("-")[1];
                  if (month == '09') {
                  }
                  else {
                      swal({ title: "Alert", text: 'Please selecte only September date...', width: 380, height: 200 });
                      $scope.mdt.sims_sep_date = '01-09-' + year;
                  }
              }

              $scope.checkdateoct = function () {
                  var month = $scope.mdt.sims_oct_date.split("-")[1];
                  if (month == '10') {
                  }
                  else {
                      swal({ title: "Alert", text: 'Please selecte only Octomber date...', width: 380, height: 200 });
                      $scope.mdt.sims_oct_date = '01-10-' + year;
                  }
              }

              $scope.checkdatenov = function () {
                  var month = $scope.mdt.sims_nov_date.split("-")[1];
                  if (month == '11') {
                  }
                  else {
                      swal({ title: "Alert", text: 'Please selecte only November date...', width: 380, height: 200 });
                      $scope.mdt.sims_nov_date = '01-11-' + year;
                  }
              }

              $scope.checkdatedec = function () {
                  var month = $scope.mdt.sims_dec_date.split("-")[1];
                  if (month == '12') {
                  }
                  else {
                      swal({ title: "Alert", text: 'Please selecte only December date...', width: 380, height: 200 });
                      $scope.mdt.sims_dec_date = '01-12-' + year;
                  }
              }

          }]);
})();