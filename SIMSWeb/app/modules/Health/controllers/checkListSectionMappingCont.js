﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Health');

    simsController.controller('checkListSectionMappingCont',
         ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
             // name 
             debugger;
             $scope.save_btn = true;
             $scope.Update_btn = true;
             $scope.temp = {};
             
             $scope.tmp = {};
             $scope.tmp.name = '';
             $scope.section_name = '';
             var user= $rootScope.globals.currentUser.username;

             //getSectionList
             //$scope.section = [{ id: '1', name: "Control Room" }, { id: '2', name: "Art & Caught" }, { id: '3', name: "Reporter" }];
             $scope.getSectionList = function () {
                 $http.get(ENV.apiUrl + "api/checkListSectionMapping/get_sectionList").then(function (section_data) {
                     debugger;
                     $scope.section = section_data.data;
                     console.log($scope.section);
                     $scope.save_CheckList
                 });
             
                 // table selction column
                 $scope.changesection = function (str1) {
                     debugger;
                 
                     $scope.section_no = $('select[name=cmb_section] option:selected').text();
                     console.log($scope.section_no);

                     $scope.load_data($scope.temp.sims_checklist_section_number)
                 }
             }

             //getChechbyList
             //$scope.checkby = [{ id: '1', name: "ABCD" }, { id: '2', name: "XYZA" }, { id: '3', name: "MNOP" }, { id: '4', name: "KHJL" }, { id: '5', name: "LJSG" }, { id: '6', name: "WJSD" }, { id: '7', name: "CMVK" }, { id: '8', name: "LSJD" }];
             $scope.getChechbyList = function () {
                 $http.get(ENV.apiUrl + "api/checkListSectionMapping/get_checkbyList").then(function (checklist_data) {
                     debugger;
                     $scope.checkbylist = checklist_data.data;
                     console.log($scope.checkbylist);
                     setTimeout(function () {
                         debugger;
                         $('#Select1').change(function () {
                             console.log($(this).val());

                         }).multipleSelect({
                             width: '100%'
                         });
                     }, 1000);

                 });

                 $(function () {
                     $('#Select1').multipleSelect({
                         width: '100%'
                     });
                 });

                 $scope.myData = function () {
                     console.log("Approve Data ", $scope.Approve);
                 }
             }
             // approveBylist
             //$scope.Approve = [{ id: '0', name: "RAJ" }, { id: '1', name: "MOHAN" }, { id: '2', name: "RAHUL" }, { id: '3', name: "MANOJ" }, { id: '4', name: "SURAJ" }, { id: '5', name: "JIVAN" }, { id: '6', name: "VISHAL" }, { id: '7', name: "SAGAR" }, { id: '8', name: "AVINASH" }];
             $scope.approveBylist = function () {
                 $http.get(ENV.apiUrl + "api/checkListSectionMapping/get_checkbyList").then(function (checklist_data) {
                     debugger;
                     $scope.approveBylist = checklist_data.data;
                     console.log($scope.checkbylist);
                     setTimeout(function () {
                         debugger;
                         $('#Select2').change(function () {
                             console.log($(this).val());

                         }).multipleSelect({
                             width: '100%'
                         });
                     }, 1000);

                 });

                 $(function () {
                     $('#Select2').multipleSelect({
                         width: '100%'
                     });
                 });

                 $scope.myData = function () {
                     console.log("Approve Data ", $scope.Approve);
                 }
             }

             //typelist
             $scope.typelist = function () {
                 $http.get(ENV.apiUrl + "api/checkListSectionMapping/get_typebyList").then(function (typelist_data) {
                     debugger;
                     $scope.typebylist = typelist_data.data;
                     console.log($scope.typebylist);

                 });
             }

// load any single value to get full information
                 $scope.load_data = function (str) {
                     $http.get(ENV.apiUrl + "api/checkListSectionMapping/get_mappingList?section=" + str).then(function (res) {
                         debugger;
                         $scope.list_mapping = res.data;
//get single data     //  var arr = list_mapping.split(', ');
                         $scope.temp['count'] = parseInt( res.data[0].sims_checklist_section_mapping_frequency_count);
                         $scope.temp['TypeList'] = res.data[0].sims_checklist_section_mapping_frequency;
    
                         for (var i = 0; i < $scope.list_mapping.length; i++) {
                             if ($scope.list_mapping[i].sims_checklist_section_mapping_type == 'C') {
                                 $scope.temp['checkDataList'] = res.data[0].sims_checklist_section_mapping_employee_code;
                             }
                             else
                                 $scope.temp['approveDataList'] = res.data[0].sims_checklist_section_mapping_employee_code;
                         }
                         if ($scope.temp.TypeList == 'O') {
                             $scope.flag = true;
                            
                         }
                         else
                             $scope.flag = false;

                         var data_checklist = [];
                         var data_approvelist = [];
//get multiple select date
                         for (var i = 0; i < $scope.list_mapping.length; i++) {
                             if ($scope.list_mapping[i].sims_checklist_section_mapping_type == 'C') {
                                 data_checklist.push( $scope.list_mapping[i].sims_checklist_section_mapping_employee_code);
                             }
                             if ($scope.list_mapping[i].sims_checklist_section_mapping_type == 'A') {

                                 data_approvelist.push($scope.list_mapping[i].sims_checklist_section_mapping_employee_code);
                             }
                         }
                         debugger
                         try{
                             $("#Select1").multipleSelect("setSelects", data_checklist);
                         } catch (ex) { }
                         try{
                             $("#Select2").multipleSelect("setSelects", data_approvelist);
                         } catch (ex) { }

                        // $scope.temp['checkDataList'] = res.data[0].sims_checklist_section_mapping_employee_code;
                         //$scope.section_no = res.data[0].sims_section_name;
                         //console.log($scope.typebylist);

                     });
                 }

             //insert data 
                 $scope.save_CheckList = function (Myform) {
                     debugger
                     if ($scope.temp.checkDataList == undefined && $scope.temp.approveDataList == undefined) {
                         swal('', 'Please select Cheked By or Approved By');
                         return;
                     }
                         var datasend = [];
                         var chk_list1 = [];
                         var chk_list2 = [];
                         var chk_list3 = [];
                         var str = [];
                         if (Myform) {
                             debugger
                             var code = '';
                             chk_list1 = $scope.temp.approveDataList + ','
                             chk_list2 = $scope.temp.checkDataList + ',';
                             //chk_list3 = chk_list1 + chk_list2;
                             //str=chk_list3.substr('/',1);
                             //if ($scope.temp.checkDataList == undefined || $scope.temp.approveDataList == undefined) {
                              
                             for (var i = 0; i < $scope.temp.checkDataList.length; i++) {
                                 debugger
                                 code =
                                    {
                                        'opr': 'I',

                                        'sims_checklist_section_mapping_checklist_section_number': $scope.temp.sims_checklist_section_number,
                                        'sims_checklist_section_mapping_employee_code': $scope.temp.checkDataList[i],
                                        'sims_checklist_section_mapping_type': 'C',
                                        'sims_checklist_section_mapping_created_by': user,
                                        'sims_checklist_section_mapping_frequency': $scope.temp.TypeList,
                                        'sims_checklist_section_mapping_frequency_count': $scope.temp.count,
                                        'sims_checklist_section_mapping_status': 'A'
                                    }
                                 datasend.push(code);
                             }


                             for (var i = 0; i < $scope.temp.approveDataList.length; i++) {
                                 code =
                                    {
                                        'opr': 'I',

                                        'sims_checklist_section_mapping_checklist_section_number': $scope.temp.sims_checklist_section_number,
                                        'sims_checklist_section_mapping_employee_code': $scope.temp.approveDataList[i],
                                        'sims_checklist_section_mapping_type': 'A',
                                        'sims_checklist_section_mapping_created_by': user,
                                        'sims_checklist_section_mapping_frequency': $scope.temp.TypeList,
                                        'sims_checklist_section_mapping_frequency_count': $scope.temp.count,
                                        'sims_checklist_section_mapping_status': 'A'
                                    }
                                 datasend.push(code);
                             }
                             console.log(datasend);
                             $http.post(ENV.apiUrl + "api/checkListSectionMapping/CUDCheckList", datasend).then(function (res) {

                                 $scope.msg1 = res.data;

                                 if ($scope.msg1 == true) {
                                     swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
//reset page
                                     $state.go($state.current, {}, { reload: true });
                                     
                                    // $scope.temp = { };
                                     //$scope.int = {};
                                     //$scope.Cancel();

                                 }
                                 else {
                                     //swal({ title: "Alert", text: "Record NOt Inserted ", showCloseButton: true, width: 300, height: 200 });
                                     swal('', 'Record not inserted');

                                 }
                             });
                         }
                      
                 }

                 $scope.readOnly = function () {
                     debugger
                     if ($scope.temp.TypeList == 'O') {
                         $scope.flag = true;
                         $scope.temp.count = 1;
                     }
                     else
                         $scope.flag = false;
                 }
             //DATA UPDATE
             //var dataforUpdate = [];
             //$scope.update = function (Myform) {
             //    if (Myform) {
             //        debugger;
             //        var data = $scope.temp;
             //        data.opr = "U";
             //        dataforUpdate.push(data);
             //        //dataupdate.push(data);
             //        $http.post(ENV.apiUrl + "api/checkListSectionMapping/CUDCheckList", dataforUpdate).then(function (msg) {
             //            $scope.msg1 = msg.data;
             //            if ($scope.msg1 == true) {
             //                swal({ title: "Alert", text: "Record Updated Successfully", showCloseButton: true, width: 300, height: 200 });
             //            }
             //            else {
             //                swal({ title: "Alert", text: "Record Not Updated", showCloseButton: true, width: 300, height: 200 });
             //            }
                       

             //        });
                      
             //    }
             //}
              
                 $(document).ready(function () {
                     $scope.getSectionList();
                     $scope.getChechbyList();
                     $scope.approveBylist();
                     $scope.typelist();
                     $scope.temp = {};

                 });
         }])
})();