﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Health');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('medicalMedicineCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {


            $scope.display = false;
            $scope.itemsPerPage = "10";
            $scope.currentPage = 0;
            $scope.items = [];

            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });
            $scope.url = ENV.apiUrl + 'Content' + '/' + $http.defaults.headers.common['schoolId'];
            var photofile;

            $http.get(ENV.apiUrl + "api/common/MedicalMedicine/GetSims_MedicalMedicine").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.CreDiv = res.data;
                console.log(res.data);
                $scope.totalItems = $scope.CreDiv.length;
                // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                $scope.todos = $scope.CreDiv;
                $scope.makeTodos();
            });

            $http.get(ENV.apiUrl + "api/common/MedicalMedicine/GetMedicineType").then(function (res) {
                $scope.cmbMedicineType = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/MedicalMedicine/GetMedicineAlert").then(function (res) {
                $scope.cmbAlert = res.data;
            });

            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str;
            //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    //$scope.makeTodos();
                    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                debugger;
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                debugger;
                /* Search Text in all 3 fields */
                return (item.sims_medicine_typename.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_medicine_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_medicine_code == toSearch) ? true : false;
            }

            $scope.edit = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                $scope.edt = str;
                $scope.display = true;
                $scope.grid = false;
                $scope.update1 = true;
                $scope.editflg = true;
                $scope.save1 = false;
                $scope.curChange(str.sims_timetable_cur);
                $scope.academicChange(str.sims_academic_year);
                $scope.gradeChange(str.sims_grade);
                //  $scope.edt = str;
               }

            }

            $scope.Update = function (newEventForm) {
                if (newEventForm.$valid) {




                    var data = $scope.edt;
                    data.opr = 'U';


                    $http.post(ENV.apiUrl + "api/common/MedicalMedicine/InsertUpdateSims_MedicalMedicine", data).then(function (res) {
                        $scope.ins = res.data;
                        if (res.data) {
                            swal({  text: "Record Updated Successfully.", imageUrl: "assets/img/check.png" });
                            $http.get(ENV.apiUrl + "api/common/MedicalMedicine/GetSims_MedicalMedicine").then(function (res) {
                                $scope.display = false;
                                $scope.grid = true;
                                $scope.items = res.data;
                                $scope.currentPage = 0;
                            });
                            $scope.getgrid();
                        }
                        else
                            swal({ text: "Record Not Updated ." + $scope.ins, imageUrl: "assets/img/close.png" });


                    });




                }
            }

            $scope.Save = function (newEventForm) {

                if (newEventForm.$valid) {



                    var data = $scope.edt;
                    data.opr = 'I';

                    $http.post(ENV.apiUrl + "api/common/MedicalMedicine/InsertUpdateSims_MedicalMedicine", data).then(function (res) {
                        $scope.ins = res.data;
                        if (res.data) {
                            swal({  text: "Record Inserted Successfully.", imageUrl: "assets/img/check.png" });
                            $http.get(ENV.apiUrl + "api/common/MedicalMedicine/GetSims_MedicalMedicine").then(function (res) {
                                $scope.display = false;
                                $scope.grid = true;
                                $scope.items = res.data;
                                $scope.currentPage = 0;
                            });
                            $scope.getgrid();
                        }
                        else
                            swal({ text: "Record Not Inserted." + $scope.ins, imageUrl: "assets/img/close.png" });


                    });




                }
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                $scope.edt = "";
                $scope.grid = false;
                $scope.display = true;
                $scope.save1 = true;
                $scope.editflg = false;
                $scope.edt = {};
                $scope.edt = {
                    sims_medicine_production_date: dd + '-' + mm + '-' + yyyy,
                    sims_medicine_expirey_date: dd + '-' + mm + '-' + yyyy
                }
              }
            }
            
            var main, del = '', delvar = [];
            
            $scope.chk = function () {
                main = document.getElementById('chk_min');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_medicine_code + i);

                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_medicine_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.Delete = function () {
                debugger
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_medicine_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_medicine_code': $scope.filteredTodos[i].sims_medicine_code,
                            opr: 'D'
                        });                       
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            debugger;
                            $http.post(ENV.apiUrl + "api/common/MedicalMedicine/dataforDelete", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                            }

                                            $scope.CheckAllChecked();
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else {
                                    swal({  text: "Record Not Deleted. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main.checked = false;
                                            $('tr').removeClass("row_selected");
                                        }
                                    });
                                }

                            });
                            deletefin = [];
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_medicine_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/common/MedicalMedicine/GetSims_MedicalMedicine").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.CreDiv = res.data;
                    console.log(res.data);
                    $scope.totalItems = $scope.CreDiv.length;
                    // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                    $scope.todos = $scope.CreDiv;
                    $scope.makeTodos();
                });
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.update1 = false;
                $scope.save1 = false;
                $scope.editflg = false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.edt = {
            sims_medicine_production_date: dd + '-' + mm + '-' + yyyy,
            sims_medicine_expirey_date: dd + '-' + mm + '-' + yyyy
            }


        }]);



})();


