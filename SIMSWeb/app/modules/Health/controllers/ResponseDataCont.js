﻿(function () {
    'use strict';


    var simsController = angular.module('sims.module.Health');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
  

    simsController.controller('ResponseDataCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
          
            $scope.display = false;
            $scope.table = true;
            $scope.pager1 = false;
            $scope.uploading_doc1 = true;
            $scope.pagesize = "10";
             

            $scope.pager = true;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.CreDiv;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_response_title.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_response_code == toSearch) ? true : false;
            }




            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };
         
    
            var rt_groupcode = '';
            $scope.getgroupcode = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/ResponseData/getResponseGroupCode").then(function (g_code) {
                    debugger;
                    rt_groupcode = g_code.data[0].responsecode;
                    console.log(g_code.data);
                })
            }

            $scope.getgroupcode();
            $scope.edit = function (str) {
                debugger;
                $scope.display = true;
                $scope.table = false;
                $scope.upd_btn = true;
                $scope.save_btn = false;
                $scope.add_btn = true;
                $scope.Table2 = true;
                $scope.Delete = true;
                $scope.rm_edt = false;
                var Response = [];
                var Rating1 = [];
                $http.post(ENV.apiUrl + "api/ResponseData/ResponsDetails?rating=" + str.sims_response_code).then(function (rate_data) {
                    Rating1 = rate_data.data;
                    $scope.Response = {
                        responsecode: Rating1[0].sims_response_code,
                        sims_response_title: Rating1[0].sims_response_title,
                        sims_response_title_ot: Rating1[0].sims_response_title_ot,
                        sims_response_desc: Rating1[0].sims_response_desc,
                        sims_response_desc_ot: Rating1[0].sims_response_desc_ot,
                        sims_response_status: Rating1[0].sims_response_status,
                    }

                    $scope.responseArray = Rating1;
                    $scope.responseArray['responsecode'] = $scope.Response['responsecode'];
                    console.log($scope.Response);
                    console.log($scope.responseArray);
                    setTimeout(function () {
                        angular.forEach($scope.ratingArray, function (value, key) {
                            var num = Math.random();

                        });
                    }, 1000);

                })
            }

            //get data 
                $scope.getData = function ()
                {
                    $http.get(ENV.apiUrl + "api/ResponseData/get_ResponsGrid").then(function (res1) {
                            debugger
                            $scope.CreDiv = res1.data;
                            //console.log(CreDiv);
                            $scope.totalItems = $scope.CreDiv.length;
                            // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                            $scope.todos = $scope.CreDiv;
                            $scope.makeTodos();
                            $scope.responseSetting = res1.data.table;
                            console.log($scope.CreDiv)
                            $scope.totalItems = $scope.CreDiv.length;
                            $scope.todos = $scope.CreDiv;
                            $scope.makeTodos();
                        });
                   
                }
                $scope.getData();


                //Add
            $scope.responseArray = [];
            $scope.AddRating = function () {
                debugger;
                var obj = [];

                if ($scope.response.sims_response_detail == undefined || $scope.response.sims_response_detail == "" || $scope.Response.sims_response_title == undefined) {
                    swal('', 'Please enter Response Detail and Response Title');
                    return;
                }
                if ($scope.Response.responseGroupcode == undefined) {
                    obj = {
                        sims_response_detail: $scope.response.sims_response_detail,
                        sims_response_detail_ot: $scope.response.sims_response_detail_ot,
                        sims_response_detail_status: $scope.response.sims_response_detail_status,
                        sims_response_code: rt_groupcode,
                    }
                }
                else {
                    obj = {
                        sims_response_detail: $scope.response.sims_response_detail,
                        sims_response_detail_ot: $scope.response.sims_response_detail_ot,
                        sims_response_detail_status: $scope.response.sims_response_detail_status,
                        sims_response_code: $scope.Response.responsecode,
                    }
                }

                $scope.responseArray.push(obj);
                $scope.response.sims_response_detail = "";
                $scope.response.sims_response_detail_ot = "";
                $scope.response.sims_response_detail_status = "";

                 console.log($scope.responseArray);
            }

            // remove
            $scope.Remove = function ($event, index, str) {
                str.splice(index, 1);
            }

            //checked all
            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.responseArray.length; i++) {
                        var v = document.getElementById('mainchk' + $scope.responseArray[i].responsecode + i);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                    //$('.sub_checkbox').find('input').prop('checked', true);
                    //$(this).closest('tr').addClass("row_selected");
                    //$scope.color = '#edefef';
                    //for (var i = 0; i < $scope.ratingArray.length; i++) {
                    //    $scope.ratingArray[i].datastatus = true;
                    //}
                }
                else {
                    for (var i = 0; i < $scope.responseArray.length; i++) {
                        var v = document.getElementById('mainchk' + $scope.responseArray[i].responsecode + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                    //$('.sub_checkbox').find('input').prop('checked', false);
                    //$(this).closest('tr').addClass("row_selected");
                    //$scope.color = '#edefef';

                }
            }

            //checked one by one
            var ind;
            var data = [];
            $scope.checkonebyonedelete = function (str, index) {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                          //str.status = true;
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                        //str.status = false;
                    }
                });
                str.status = str.datastatus;
                ind = index;
                //data = str;
            }

            //  new
            $scope.New = function () {
                $scope.display = true;
                $scope.table = false;
                $scope.Table2 = true;
                $scope.save_btn = true;
                $scope.add_btn = true;
                $scope.upd_btn = false;
                $scope.OkDelete = false;
                $scope.Response = {};
                $scope.responseArray = [];
                $scope.Response.sims_response_status = true;
                $scope.Delete = false;
                $scope.rm_edt = true;
                $scope.AddRating;
                //$scope.Rating = {
                //    statusR: true,
                //    statusD: true
                //};

                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            };

            //cancle
            $scope.Cancel = function () {
                $scope.display = false;
                $scope.table = true;
                $scope.Rating = "";
                $scope.ratingArray = [];
                $scope.getData();

            }

            // delete main page data
            $scope.Delete = function () {
                debugger;
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById('checkboxD' + $scope.filteredTodos[i].sims_response_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            opr: 'D',
                            'sims_response_code': $scope.filteredTodos[i].sims_response_code,

                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            debugger;
                            $http.post(ENV.apiUrl + "api/ResponseData/CUDResponseSave", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getData();
                                            main = document.getElementById('checkboxR');
                                            if (main.checked == true) {
                                                main.checked = false;
                                            }

                                            $scope.CheckAllChecked();
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getData();
                                            main.checked = false;
                                            $('tr').removeClass("row_selected");
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                            });
                            deletefin = [];
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById('checkboxR' + $scope.filteredTodos[i].sims_response_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }
            $scope.getData();

            //Save data
            $scope.Save = function (Myform) {
                debugger;

                $scope.Response['response_det'] = [];
                var rt_code = [];
                console.log($scope.Response);
                $scope.Response.opr = 'I';
                $scope.Response['sims_response_title'] = $scope.Response.sims_response_title,
                $scope.Response['sims_response_title_ot'] = $scope.Response.sims_response_title_ot,
                $scope.Response['sims_response_desc'] = $scope.Response.sims_response_desc,
                $scope.Response['sims_response_desc_ot'] = $scope.Response.sims_response_desc_ot,
                $scope.Response['sims_response_status'] = $scope.Response.sims_response_status

                //send.push($scope.Rating);
                // if (Myform) {


                for (var i = 0; i < $scope.responseArray.length; i++) {
                    var v = document.getElementById('mainchk11' + $scope.responseArray[i].sims_response_code + i);
                    if (v.checked == true) {
                         
                        rt_code = {
                            'sims_response_code': $scope.responseArray[i].sims_response_code,
                            'sims_response_detail': $scope.responseArray[i].sims_response_detail,
                            'sims_response_detail_ot': $scope.responseArray[i].sims_response_detail_ot,
                            'sims_response_detail_status': $scope.responseArray[i].sims_response_detail_status,
                            
                        }
                        $scope.Response['response_det'].push(rt_code);
                        $scope.flag = true;
                        //}
                    }

                    //}


                    // $scope.flag = true;


                    if ($scope.flag == false) {
                        swal('', 'Please select the records to Insert');
                        return;
                    }

                }

                $http.post(ENV.apiUrl + "api/ResponseData/CUD_SaveResponseDetails", $scope.Response).then(function (res) {
                    console.log(res);
                    $scope.msg1 = res.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        $scope.getData();
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                });
                $scope.display = false;
                $scope.table = true;
                // $scope.getData();
                // }
            }

            // selected child Delete
            $scope.OkDelete = function () {
                debugger;
                var deleteArray = [];
                $scope.Response['response_det'] = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.responseArray.length; i++) {
                    var v = document.getElementById('mainchk11' + $scope.responseArray[i].sims_response_code + i);

                    if (v.checked == true) {
                        var deletemodulecode = ({
                            opr: 'D',
                            'sims_response_code': $scope.responseArray[i].sims_response_code,
                            'sims_response_detail_code': $scope.responseArray[i].sims_response_detail_code,

                        });
                        deleteArray.push(deletemodulecode);
                    }
                }


                if ($scope.responseArray) {
                    swal({

                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            console.log($scope.response);

                            $http.post(ENV.apiUrl + "api/ResponseData/CUDResponseSave", deleteArray).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Deleted Successfully", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.display = false;
                                            $scope.table = true;
                                            $scope.ratingArray.datastatus == false;
                                            $scope.getData();
                                        }
                                    });

                                }
                                else {
                                    swal({ text: "Record Already Mapped.Can't be Deleted. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {

                                        }
                                    });
                                }

                            });
                            deleteArray = [];
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.responseArray.length; i++) {
                                var v = document.getElementById("test_" + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }

                    });
                    $scope.getData();
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
            }


            $scope.Update = function (Myform) {
                var send = [];
                var sims_response_detail_code = [];
                $scope.flag = false;
                debugger;
                console.log($scope.Response);
                var group_code = $scope.Response.responsecode;
                var group_titl = $scope.Response.sims_response_title;
                var group_titlot = $scope.Response.sims_response_title_ot;
                var group_desc = $scope.Response.sims_response_desc;
                var group_descot = $scope.Response.sims_response_desc_ot;
                var group_status = $scope.Response.sims_response_status;
                if (Myform) {
                    for (var i = 0; i < $scope.responseArray.length; i++) {
                        var v = document.getElementById('mainchk11' + $scope.responseArray[i].sims_response_code + i);

                        if (v.checked == true) {
                            //if ($scope.file_doc.length == 0) {
                            sims_response_detail_code = {
                                    'opr': 'U',
                                    'sims_response_code': group_code,
                                    'sims_response_title': group_titl,
                                    'sims_response_title_ot': group_titlot,
                                    'sims_response_desc': group_desc,
                                    'sims_response_desc_ot': group_descot,
                                    'sims_response_status': group_status,
                                    'sims_response_detail_code': $scope.responseArray[i].sims_response_detail_code,
                                    'sims_response_detail': $scope.responseArray[i].sims_response_detail,
                                    'sims_response_detail_ot': $scope.responseArray[i].sims_response_detail_ot,
                                    'sims_response_detail_status': $scope.responseArray[i].sims_response_detail_status,
                                     
                                //}
                            }
                         
                            $scope.flag = true;
                            send.push(sims_response_detail_code);
                        }
                    }
                    if ($scope.flag == false) {
                        swal('', 'Please select the records to update');
                        return;
                    }
                }
                $http.post(ENV.apiUrl + "api/ResponseData/CUDResponseSave", send).then(function (res) {
                    $scope.msg1 = res.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        $scope.getData();
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                    $scope.getData();

                });
                $scope.display = false;
                $scope.table = true;
                $scope.save_btn = false;
                $scope.getData();
            }
            //$(document).ready(function () {

            //    $scope.getData();
                 
            //});

        }])
})();