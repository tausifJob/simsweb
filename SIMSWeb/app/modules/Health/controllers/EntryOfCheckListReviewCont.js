﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Health');

    simsController.controller('EntryOfCheckListReviewCont', ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

        $scope.table = true;
        $scope.int = {};
        var oDataTiles = [];
        var dt = {};
        var user = $rootScope.globals.currentUser.username;
        $scope.radioTable = true;
        $scope.dropDownTable = false;
        $scope.savebutton = false;


        $scope.pagesize = '10';
        $scope.pageindex = "0";
        $scope.pager = true;

        $timeout(function () {
            $("#fixTable").tableHeadFixer({ 'top': 1 });
        }, 100);

        
        $scope.size = function (str) {
            //console.log(str);
            //$scope.pagesize = str;
            //$scope.currentPage = 1;
            //$scope.numPerPage = str;
            debugger;
            if (str == "All") {
                $scope.currentPage = '1';
                $scope.filteredTodos = $scope.CreDiv;
                $scope.pager = false;
            }
            else {
                $scope.pager = true;
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }
            console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
        }

        $scope.index = function (str) {
            $scope.pageindex = str;
            $scope.currentPage = str;
            console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            main.checked = false;
            $scope.CheckAllChecked();
        }

        $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

        $scope.makeTodos = function () {
            var rem = parseInt($scope.totalItems % $scope.numPerPage);
            if (rem == '0') {
                $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
            }
            else {
                $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
            }

            var begin = (($scope.currentPage - 1) * $scope.numPerPage);
            var end = parseInt(begin) + parseInt($scope.numPerPage);

            console.log("begin=" + begin); console.log("end=" + end);

            $scope.filteredTodos = $scope.todos.slice(begin, end);
        };

        $scope.searched = function (valLists, toSearch) {
            return _.filter(valLists,

            function (i) {
                /* Search Text in all  fields */
                return searchUtil(i, toSearch);
            });
        };




        $scope.propertyName = null;
        $scope.reverse = false;

        $scope.sortBy = function (propertyName) {
            $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        };

        //

        $scope.move = function () {
            var elem = document.getElementById("myBar");
            var width = 10;
            var id = setInterval(frame, 10);
            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                } else {
                    width++;
                    elem.style.width = width + '%';
                    elem.innerHTML = width * 1 + '%';
                }
            }
        }

        // current date 

        $scope.date = moment(new Date()).format('DD/MM/YYYY')
        console.log($scope.date = moment(new Date()).format('DD-MM-YYYY'))

        //get all data grid S
        $scope.typelist = function () {
            debugger;
            $http.get(ENV.apiUrl + "api/EntryOfCheckListReview/get_ListReview").then(function (get_ListReview) {
                debugger;
                $scope.listReview = get_ListReview.data;
                console.log($scope.listReview);
                console.log($scope.listReview);
            });
        }

        //Response Chang data 
        //$scope.changResponsData = function () {
        //    debugger
            
        //    $scope.typelist();
        //}

        //Reponse data select response code 
        //$scope.resptypelist = function () {
        //    debugger;
        //    $http.get(ENV.apiUrl + "api/EntryOfCheckListReview/get_typebyList?str=" + $scope.int.sims_response_code).then(function (typelist_data) {
        //        debugger;
        //        $scope.typebylist = typelist_data.data;
        //        console.log($scope.typebylist);

        //    });
        //}
        

        //get all Parent sectionbyList dropdownlist
        $scope.getAllSection = function () {
            debugger;
            $http.get(ENV.apiUrl + "api/EntryOfCheckListReview/get_sectionbyList").then(function (parentlist_data) {
                $scope.parentsectionBylist = parentlist_data.data;
                console.log("parentsectionBylist = ", $scope.parentsectionBylist);
            });
        }


        //get all response byList dropdownlist
        $scope.getAllrseponse = function () {
            debugger;
            $http.get(ENV.apiUrl + "api/EntryOfCheckListReview/get_responsebyList").then(function (response_data) {
                $scope.responseBylist = response_data.data;
                console.log("responseBylist = ", $scope.responseBylist);
            });
        }
        //get all evidences List dropdownlist with database data
        $scope.getAllEvidences = function () {
            debugger;
            $http.get(ENV.apiUrl + "api/EntryOfCheckListReview/get_evidencesbyList").then(function (evidenceslist_data) {
                $scope.CreDiv = evidenceslist_data.data;
                $($scope.CreDiv).each(function (i, val) {
                    val.sims_checklist_section_review_number = val.sims_checklist_section_review_number == undefined ? 'U' : val.sims_checklist_section_review_number;
                    val.sims_checklist_section_review_date = val.sims_checklist_section_review_date == undefined ? 'U' : val.sims_checklist_section_review_date;
                });
                $scope.totalItems = $scope.CreDiv.length;
                console.log($scope.CreDiv);
                // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                $scope.todos = $scope.CreDiv;
                $scope.makeTodos();

            });
        }

        //DATA UPDATE == Save buttone Action 
        var dataforUpdate = [];
        debugger
        $scope.update = function (Myform) {
            if ($scope.int.sims_checklist_section_number == null || $scope.int.sims_checklist_section_number == "" || $scope.int.sims_checklist_section_number == undefined ||
                $scope.int.sims_checklist_section_review_date == null || $scope.int.sims_checklist_section_review_date == "" || $scope.int.sims_checklist_section_review_date == undefined) {
               // || $scope.int.sims_response_code == null || $scope.int.sims_response_code == "" || $scope.int.sims_response_code == undefined) {
                swal({
                    text: "Please Select Section and Date",
                    imageUrl: "assets/img/notification-alert.png",
                    showCloseButton: true,
                    width: 500,
                    height: 200
                });

            }
            else {
                if (Myform) {
                    debugger;
                    console.log($scope.filteredTodos);
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var data = {
                            'opr': 'U',
                            'sims_checklist_section_review_checklist_section_number': $scope.filteredTodos[i].sims_checklist_section_number,
                            'sims_checklist_section_review_parameter_number': $scope.filteredTodos[i].sims_checklist_section_parameter_number,
                            'sims_checklist_section_review_parameter_response': $scope.filteredTodos[i].sims_checklist_section_review_parameter_response,
                            'sims_checklist_section_review_parameter_action': $scope.filteredTodos[i].sims_checklist_section_review_parameter_action,
                            'sims_checklist_section_review_by': user,
                            'sims_checklist_section_number': $scope.int.sims_checklist_section_number,
                            'sims_checklist_section_review_date': $scope.int.sims_checklist_section_review_date,
                            'sims_checklist_section_review_transaction_date': $scope.date,
                            'sims_checklist_section_review_number': $scope.filteredTodos[i].sims_checklist_section_review_number,
                            'sims_response_code': $scope.filteredTodos[i].sims_response_code
                        }
                        dataforUpdate.push(data);
                    }

                    $http.post(ENV.apiUrl + "api/EntryOfCheckListReview/CUDListReview", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({
                                text: "Record Updated Successfully",
                                imageUrl: "assets/img/check.png",
                                showCloseButton: true,
                                width: 300,
                                height: 200
                            });

                            $scope.searchbutton();
                        } else {
                            swal({
                                text: "Record Not Updated",
                                imageUrl: "assets/img/check.png",
                                showCloseButton: true,
                                width: 300,
                                height: 200
                            });
                        }
                        //show data
                        $scope.searchbutton();

                        

                    });
                    dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }
        }

        $scope.changesection = function () {
            debugger
            $scope.int.sims_checklist_section_review_date = $scope.date;
            console.log($scope.int.sims_checklist_section_number);
            $http.get(ENV.apiUrl + "api/EntryOfCheckListReview/changsectiondata?str=" + $scope.int.sims_checklist_section_number ).then(function (res9) {
                debugger
                $scope.CreDiv = res9.data;
                        $scope.int.sims_response_title = $scope.CreDiv[0].sims_response_title;
       
            });
            }


        // search button
        $scope.searchbutton = function () {
            debugger;
            console.log($scope.int.sims_checklist_section_number);
            console.log($scope.date);
            console.log($scope.int.sims_response_code);
            $scope.savebutton = true;
            //$scope.changResponsData();
            
           // + "&str2=" + $scope.int.sims_response_code
            $http.get(ENV.apiUrl + "api/EntryOfCheckListReview/getsearchlist?str=" + $scope.int.sims_checklist_section_number + "&str1=" + $scope.int.sims_checklist_section_review_date ).then(function (res5) {
                    debugger
                    $scope.CreDiv = res5.data;

                    console.log($scope.CreDiv[0].sims_checklist_section_response_type);
                    console.log($scope.CreDiv[0].sims_response_code);
                    $http.get(ENV.apiUrl + "api/EntryOfCheckListReview/get_typebyList?str=" + $scope.CreDiv[0].sims_response_code).then(function (typelist_data) {
                        debugger;
                        $scope.typebylist = typelist_data.data;
                        console.log($scope.typebylist);

                    });
                    $scope.totalItems = $scope.CreDiv.length;
                    // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                    $scope.todos = $scope.CreDiv;
                    $scope.makeTodos();
                    //$scope.changResponsData();
                    if ($scope.CreDiv[0].sims_checklist_section_response_type == "R") {
                        $scope.radioTable = true;
                        $scope.dropDownTable = false;
                        
                        if ($scope.int.sims_checklist_section_number == null || $scope.int.sims_checklist_section_number == "" || $scope.int.sims_checklist_section_number == undefined) {
                            $scope.int.sims_checklist_section_number = '';
                            $scope.int.sims_checklist_section_review_date = '';
                            $scope.int.sims_response_title = '';
                            $scope.savebutton = false;
                        } else {
                            $scope.int = {
                                sims_checklist_section_number: $scope.CreDiv[0].sims_checklist_section_number,
                                sims_checklist_section_review_date: $scope.int.sims_checklist_section_review_date,
                                sims_response_title: $scope.CreDiv[0].sims_response_title,

                            }
                        } $scope.radiodata = true;
                    }
                    else {
                        $scope.radioTable = false;
                        $scope.dropDownTable = true;
                        
                        if ($scope.int.sims_checklist_section_number == null || $scope.int.sims_checklist_section_number == "" || $scope.int.sims_checklist_section_number == undefined) {
                            $scope.int.sims_checklist_section_number = '';
                            $scope.int.sims_checklist_section_review_date = '';
                            $scope.int.sims_response_title = '';
                            $scope.savebutton = false;
                        } else {
                            $scope.int = {
                                sims_checklist_section_number: $scope.CreDiv[0].sims_checklist_section_number,
                                sims_checklist_section_review_date: $scope.int.sims_checklist_section_review_date,
                                sims_response_title: $scope.CreDiv[0].sims_response_title,

                            }
                        } 
                    }
                   
                    //if ($scope.int.sims_checklist_section_number == null || $scope.int.sims_checklist_section_number == "" || $scope.int.sims_checklist_section_number == undefined) {
                    //    $scope.int.sims_checklist_section_number = '';
                    //    $scope.int.sims_checklist_section_review_date = '';

                    //} else {
                    //    $scope.int = {
                    //        sims_checklist_section_number: $scope.CreDiv[0].sims_checklist_section_number,
                    //    sims_checklist_section_review_date:date,

                             
                    //    }
                    //}

                });
             
             
        }
       
        

        $(document).ready(function () {
            $scope.getAllSection();
            $scope.getAllEvidences();
            $scope.getAllrseponse();
            $scope.typelist();
            //$scope.date();
 
        });
    }])
})();