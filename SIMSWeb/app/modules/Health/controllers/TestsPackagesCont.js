﻿
(function () {
    'use strict';

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TestsPackagesCont',
        
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.user_access = [];
            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $scope.display = false;
            $scope.grid = true;
            $scope.itemsPerPage = "10";
            $scope.currentPage = 0;
            $scope.items = [];

            var photofile;
            var data = [];
            // $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            $scope.getImg1 = function ($files) {
                //debugger;
                $scope.edt.sims_health_package_image = $files[0].name;
            }

            $scope.New = function () {

                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.grid = false;
                    $scope.display = true;
                    $scope.save1 = true;
                    $scope.update1 = false;
                    $scope.delete1 = false;
                    $scope.edt = {};
                }
            }

            //$scope.getSelectedTP = function () {
            //    debugger;
            //    for (var j = 0; j < $scope.parameter.length; j++) {
            //        $scope.parameter[j].parameter_check = false;

            //    }
            //    if ($scope.paramList.length > 0) {
            //        for (var i = 0; i < $scope.paramList.length; i++) {
            //            for (var j = 0; j < $scope.parameter.length; j++) {
            //                if ($scope.paramList[i].sims_health_test_code == $scope.parameter[j].sims_health_test_code) {
            //                    $scope.parameter[j].parameter_check = true;
            //                } 
            //            }
            //        }
            //    }
            //}
            //var paramList, sims_health_test_code;
            $scope.edit = function (str) {
                debugger;
                //sims_health_test_code = '';
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = false;
                    $scope.update1 = true;
                    $scope.delete1 = false;

                    //$http.get(ENV.apiUrl + "api/health/getTestPackagesdata?sims_health_test_code=" + str.sims_health_test_code).then(function (res) {
                    //    $scope.paramList = res.data;
                    //    if ($scope.paramList.length > 0) {
                    //        $scope.getSelectedTP();
                    //    }
                    //});

                    //if ($scope.paramList.length > 0) {
                    //            for (var i = 0; i < $scope.paramList.length; i++) {
                    //                for (var j = 0; j < $scope.parameter.length; j++) {
                    //                    if ($scope.paramList[i].sims_health_test_code == str.sims_health_test_code) {
                    //                        $scope.parameter[j].parameter_check = true;
                    //                   } 
                    //                }
                    //           }
                    //      }
                        
                    
                    $scope.edt =
                        {
                            sims_health_package_code: str.sims_health_package_code,
                            sims_health_package_name_en: str.sims_health_package_name_en,
                            sims_health_package_name_ot: str.sims_health_package_name_ot,
                            sims_health_package_display_order: str.sims_health_package_display_order,
                            sims_health_package_price: str.sims_health_package_price,
                            sims_health_package_status: str.sims_health_package_status,
                            sims_health_package_chargable: str.sims_health_package_chargable,
                            sims_health_package_image: str.sims_health_package_image,
                            sims_health_test_code: str.sims_health_test_code
                            //sims_health_package_updated_by: str.sims_health_package_updated_by,
                            //sims_health_package_updated_datetime: str.sims_health_package_updated_datetime,

                        }
                   

                    $scope.Update = true;
                }
            }

            $scope.getparameter = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/health/getTestPackages").then(function (test_data) {
                    $scope.parameter = test_data.data;
                    // console.log("parameter = ", $scope.parameter);

                });
            }
            $scope.getparameter();


            $scope.file_changed = function (element, name) {
                debugger;
                var str = '';

                str = element.files[0]['name'].split(".")[0];

                var photofile = element.files[0];
                photo_filename = (photofile.type)
                $scope.edt.sims_health_package_image = str + '.' + photo_filename.split("/")[1];
                camp_image1 = photo_filename.replace(photo_filename, str);

                var reader = new FileReader();

                reader.onload = function (e) {
                    $scope.$apply(function () {

                        $scope.sims_package_image = e.target.result;


                    });
                };
                reader.readAsDataURL(photofile);

                console.log("student_image", sims_package_image);

                var request = {
                    method: 'POST',
                 
                    url: ENV.apiUrl + '/api/file/upload?filename=' + sims_package_image + "&location=" + "/StudentImages",
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request)
                       .success(function (d) {

                       }, function () {
                           alert("Err");
                       });


            };


            $scope.getTheFiles = function ($files) {

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.getgrid = function () {

                $http.get(ENV.apiUrl + "api/health/getAllTestPackages").then(function (res) {
                    $scope.TestMaster = res.data;
                    $scope.totalItems = $scope.TestMaster.length;
                    $scope.todos = $scope.TestMaster;
                    $scope.makeTodos();
                    if (res.data.length > 0) {
                        $scope.Showsave = false;
                        $scope.rpt_list = res.data;
                    }
                    else {
                        swal('No Record Found');
                    }
                });
            }
            $scope.getgrid();

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.TestMaster.length; i++) {

                        var v = document.getElementById("test-" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    } TestMaster
                }
                else {
                    for (var i = 0; i < $scope.TestMaster.length; i++) {

                        var v = document.getElementById("test-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                var main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            var param, sims_health_test_code;
            $scope.Save = function (isvalidate) {
                //debugger;
                sims_health_test_code = '';

                var data = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = $scope.edt;
                        data.opr = 'I';
                        data.user = user;

                        if ($scope.parameter.length > 0) {
                            for (var j = 0; j < $scope.parameter.length; j++) {
                                if ($scope.parameter[j].parameter_check == true) {
                                    param = $scope.parameter[j].sims_health_test_code;
                                    sims_health_test_code = sims_health_test_code + ',' + param;

                                }
                            }
                        }
                        data.sims_health_test_code = sims_health_test_code;

                        $http.post(ENV.apiUrl + "api/health/TestPackages01", data).then(function (res) {

                            $scope.grid = true;
                            $scope.display = false;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Test  Details Added Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {

                                        $scope.getgrid();
                                       // $scope.getparameter();
                                    }

                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: " Test Details Record Already Exists. ", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                       //$scope.getparameter();
                                    }
                                });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });
                    }
                    else {

                    }
                }
            }

            var param, sims_health_test_code;
            $scope.Update2 = function (isvalidate) {
                debugger;
                sims_health_test_code = '';
                var data = [];
                if (isvalidate) {
                    if ($scope.update1 == true) {
                        var data = $scope.edt;
                        data.opr = 'U';
                        data.user = user;
                        // data.sims_health_package_code = sims_health_package_code;
                        if ($scope.parameter.length > 0) {
                            for (var j = 0; j < $scope.parameter.length; j++) {
                                if ($scope.parameter[j].parameter_check == true) {
                                    param = $scope.parameter[j].sims_health_test_code;
                                    sims_health_test_code = sims_health_test_code + ',' + param;

                                }
                            }
                        }
                        data.sims_health_test_code = sims_health_test_code;


                        $http.post(ENV.apiUrl + "api/health/TestPackages01", data).then(function (res) {
                            debugger;
                            $scope.grid = true;
                            $scope.display = false;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Test packages  Detail Updated Successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {

                                        $scope.getgrid();
                                    }
                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ title: "Alert", text: "Only oneTest  Detail  can be set as current at a time.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {

                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            } s
                        });
                    }
                }
            }


            $scope.Delete = function () {
                debugger;
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    var deletemodercode = [];
                    //$scope.flag = false;
                    for (var i = 0; i < $scope.TestMaster.length; i++) {
                        var v = document.getElementById("test-" + i);
                        if (v.checked == true) {
                            $scope.flag = true;

                            var deletemodercode = ({


                                'sims_health_package_sr_no': $scope.TestMaster[i].sims_health_package_sr_no,
                                'sims_health_package_code': $scope.TestMaster[i].sims_health_package_code,

                                
                                'sims_health_package_name_en': $scope.TestMaster[i].sims_health_package_name_en,
                                'sims_health_package_name_ot': $scope.TestMaster[i].sims_health_package_name_ot,
                                'sims_health_package_chargable': $scope.TestMaster[i].sims_health_package_chargable,
                                'sims_health_package_price': $scope.TestMaster[i].sims_health_package_price,
                                'sims_health_package_image': $scope.TestMaster[i].sims_health_package_image,

                                'sims_health_package_display_order': $scope.TestMaster[i].sims_health_package_display_order,
                                'sims_health_package_status': $scope.TestMaster[i].sims_health_package_status,
                               
                               
                                opr: 'D'
                            });

                        }
                    }

                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {


                                $http.post(ENV.apiUrl + "api/health/TestPackages01", deletemodercode).then(function (res) {
                                    debugger;
                                    $scope.msg1 = res.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Test Packages  Details Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Record Already Mapped.Can't be Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });


                            }
                            else {
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }

                                for (var i = 0; i < $scope.TestMaster.length; i++) {

                                    var v = document.getElementById("test-" + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }

                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                }
            }

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    //$scope.makeTodos();
                    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.TestMaster = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };
            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;

            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.items.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;
            };


            var main, del = '', delvar = [];



        }]);

})();


