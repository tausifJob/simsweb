﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Common');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('AdmissionclassesCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });
            getdata();
            function getdata() {
                $http.get(ENV.apiUrl + "api/admissionclasses/GetAllAdmissionClasses").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.obj = res.data;
                    for (var i = 0; i < $scope.obj.length; i++) {
                        $scope.obj[i].ischecked = false;
                    }
                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.makeTodos();

                });
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseFloat($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseFloat(begin) + parseFloat($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                main = document.getElementById('mainchk');
                main.checked = false;
                $scope.check();

            };

            $scope.edt = {};

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = true;
                    $scope.update1 = false;
                    $scope.delete1 = false;
                    $scope.edt.simsStatus = true;
                    $scope.readonlySimsAcademicYear = false;
                    $scope.disabledSimsCurCode = false;
                    $scope.disabledGradeCode = false;
                    $scope.disabledSection = false;
                    $scope.disabledTerm = false;

                    if ($scope.Curriculum.length > 0) {
                        $scope.edt.sims_cur_code = $scope.Curriculum[0].sims_cur_code;
                        $scope.getacademicYear($scope.edt.sims_cur_code);
                    }
                    if ($scope.AcademicYear.length > 0) {
                        $scope.edt.sims_academic_year = $scope.AcademicYear[0].sims_academic_year;
                        $scope.getGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                    }
                    var dt = new Date();
                    $scope.edt["sims_academic_year"] = '';
                    $scope.edt["sims_grade_code"] = '';
                    $scope.edt["sims_section_code"] = '';
                    $scope.edt["sims_term_code"] = '';
                    $scope.edt["simsBirthDateFrom"] = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                    $scope.edt["simsBirthDateTo"] = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                    $scope.edt["simsAdmissionStartFrom"] = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                    $scope.edt["simsAdmissionEnd"] = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();

                }
            }

            $scope.edit = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = false;
                    $scope.update1 = true;
                    $scope.delete1 = false;
                    $scope.readonlySimsAcademicYear = true;
                    $scope.disabledSimsCurCode = true;
                    $scope.disabledGradeCode = true;
                    $scope.disabledSection = true;
                    $scope.disabledTerm = true;
                    $scope.edt = angular.copy(str);
                    $scope.getGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year)
                    $scope.getsection($scope.edt.sims_cur_code, $scope.edt.sims_grade_code, $scope.edt.sims_academic_year);
                    $scope.edt.simsBirthDateFrom = str.sims_birth_date_from;
                    $scope.edt.simsBirthDateTo = str.sims_birth_date_to;
                    $scope.edt.simsAdmissionStartFrom = str.sims_admission_start_from;
                    $scope.edt.simsAdmissionEnd = str.sims_admission_end;
                    $scope.edt.sims_section_code = str.sims_section_code;
                    $scope.edt.sims_grade_code = str.sims_grade_code;
                    $scope.edt.sims_term_code = str.sims_term_code;
                    $scope.edt.sims_cur_code = str.sims_cur_code;
                    $scope.edt.sims_academic_year = str.sims_academic_year;


                    if (str.sims_status == true) {
                        $scope.edt.simsStatus = true;
                    }
                    else {
                        $scope.edt.simsStatus = false;
                    }
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //$http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCurriculum) {
            //    $scope.Curriculum = getCurriculum.data;
            //    if (getCurriculum.data.length > 0) {
            //        $scope.edt.sims_cur_code = getCurriculum.data[0].sims_cur_code;
            //        $scope.getacademicYear($scope.edt.sims_cur_code);
            //    }
            //    //$scope.edt['sims_cur_code'] = $scope.Curriculum[0].sims_cur_code
            //    //$scope.getacademicYear($scope.Curriculum[0].sims_cur_code);
            //});

            function getCur(flag, comp_code) {
                if (flag) {
                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        $scope.Curriculum = res.data;
                        if ($scope.Curriculum.length > 0) {
                            $scope.edt.sims_cur_code = $scope.Curriculum[0].sims_cur_code;
                            $scope.getacademicYear($scope.Curriculum[0].sims_cur_code);
                      }
                    });
                }
                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        $scope.Curriculum = res.data;
                        if ($scope.Curriculum.length > 0) {
                            $scope.edt.sims_cur_code = $scope.Curriculum[0].sims_cur_code;
                            $scope.getacademicYear($scope.Curriculum[0].sims_cur_code);
                        }

                    });


                }

            }

            $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.global_count_comp = res.data;

                if ($scope.global_count_comp) {
                    getCur(true, $scope.user_details.comp);


                }
                else {
                    getCur(false, $scope.user_details.comp)
                }
            });







            $scope.getacademicYear = function (str) {
                $scope.AcademicYear = [];
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (getAcademicYear) {
                    $scope.AcademicYear = getAcademicYear.data;
                    if (getAcademicYear.data.length > 0) {
                        $scope.edt.sims_academic_year = getAcademicYear.data[0].sims_academic_year;
                        $scope.getGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                    }
                    //$scope.edt['sims_academic_year'] = $scope.AcademicYear[0].sims_academic_year;                   
                    //$scope.getGrade($scope.edt['sims_cur_code'], $scope.edt['sims_academic_year']);
                });
            }

            $scope.getGrade = function (str1, str2) {
                $scope.AllGrades = [];
                var param = {}
                param.sims_cur_code = str1;
                param.sims_academic_year = str2;

                $http.post(ENV.apiUrl + "api/admissionclasses/AllGrades_p", param).then(function (getAllGrades) {
                    $scope.AllGrades = getAllGrades.data;
                });
                //$http.get(ENV.apiUrl + "api/common/getAllGradesCommon?cur_code=" + str1 + "&academic=" + str2).then(function (getAllGrades) {
                //    $scope.AllGrades = getAllGrades.data;
               
                //})
            }

            $scope.getsection = function (str, str1, str2) {
                $scope.getSectionFromGrade = [];
                var param = {};
                param.sims_cur_code = str;
                param.sims_grade_code = str1;
                param.sims_academic_year = str2;
                $http.post(ENV.apiUrl + "api/admissionclasses/sectionCommon_p", param).then(function (Allsection) {
                    $scope.getSectionFromGrade = Allsection.data;
                    $scope.Getterm1();
                });

                //$http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + str + "&grade_code=" + str1 + "&academic_year=" + str2).then(function (Allsection) {
                //    $scope.getSectionFromGrade = Allsection.data;
                //    $scope.Getterm1();                   
                //})

            };

            $scope.Getterm1 = function () {
                $scope.Termsobject = [];
                $http.get(ENV.apiUrl + "api/admissionclasses/GetAllterm?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Termsobject) {
                    $scope.Termsobject = Termsobject.data;
                });
            }

            $scope.Save = function (Myform) {
                if (Myform) {
                    $scope.edt["opr"] = 'I';
                    $scope.edt.sims_birth_date_from = $scope.edt.simsBirthDateFrom;
                    $scope.edt.sims_birth_date_to = $scope.edt.simsBirthDateTo;
                    $scope.edt.sims_admission_start_from = $scope.edt.simsAdmissionStartFrom;
                    $scope.edt.sims_admission_end = $scope.edt.simsAdmissionEnd;
                    $scope.edt.sims_status = $scope.edt.simsStatus;
                    var data = [];
                    data.push($scope.edt);
                    $http.post(ENV.apiUrl + "api/admissionclasses/CURDAdmissionClasses", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                        if ($scope.msg1 == true) {
                            swal({ text: 'Admission Classes Added Successfully', imageUrl: "assets/img/check.png",width: 320, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                            getdata();
                        }
                        else if ($scope.msg1 == false) {
                                swal({ text: 'Admission Classes Not Added Or Already Exist. ',imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.Update = function (Myform) {
                if (Myform) {
                    $scope.edt["opr"] = 'U';
                    $scope.edt.sims_birth_date_from = $scope.edt.simsBirthDateFrom;
                    $scope.edt.sims_birth_date_to = $scope.edt.simsBirthDateTo;
                    $scope.edt.sims_admission_start_from = $scope.edt.simsAdmissionStartFrom;
                    $scope.edt.sims_admission_end = $scope.edt.simsAdmissionEnd;
                    $scope.edt.sims_status = $scope.edt.simsStatus;
                    var data = [];
                    data.push($scope.edt);
                    $http.post(ENV.apiUrl + "api/admissionclasses/CURDAdmissionClasses", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: 'Admission Classes Updated Successfully',imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                            getdata();
                        }
                        else if ($scope.msg1 == false) {
                                swal({ text: 'Admission Classes Not Updated. ' ,imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                    });
                }
            }

            $scope.Delete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    var data = [];

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        if ($scope.filteredTodos[i].ischecked == true) {
                            $scope.filteredTodos[i].opr = 'D';
                            data.push($scope.filteredTodos[i]);
                        }
                    }

                    if (data.length > 0) {

                        swal({
                            title: '',
                            text: "Are you sure you want to delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $http.post(ENV.apiUrl + "api/admissionclasses/CURDAdmissionClasses", data).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: 'Admission Classes Deleted Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                                        $scope.display = false;
                                        $scope.grid = true;
                                        getdata();
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: 'Admission Classes Not Deleted. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                })

                            }
                            else {
                                main = document.getElementById('mainchk');
                                main.checked = false;
                                $scope.check();
                            }
                        })

                    }
                    else {
                        swal({ text: 'Select Atleast One Record To Delete', imageUrl: "assets/img/notification-alert.png", width: 320, showCloseButton: true });
                    }
                }
            }

            $scope.check = function () {
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if (main.checked == true) {
                        $scope.filteredTodos[i].ischecked = true;
                        $('tr').addClass("row_selected");
                    }

                    else {
                        $scope.filteredTodos[i].ischecked = false;
                        $('tr').removeClass("row_selected");
                    }
                }
            }

            $scope.check1 = function (str) {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

            }

            $scope.size = function (str) {

                //if (str == 'All') {

                //    $scope.totalItems = $scope.obj.length;
                //    $scope.todos = $scope.obj;
                //    $scope.filteredTodos = $scope.GetAllStudentFee;
                //    $scope.numPerPage = $scope.obj.length;
                //    $scope.maxSize = $scope.obj.length
                //    $scope.makeTodos();
                //}
                //else {
                //    $scope.pagesize = str;
                //    $scope.currentPage = 1;
                //    $scope.numPerPage = str;
                //    $scope.makeTodos();
                //}
               
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.obj;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                //return (item.sims_grade_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_section_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
                return (item.sims_grade_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_academic_year.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

        }])
})();