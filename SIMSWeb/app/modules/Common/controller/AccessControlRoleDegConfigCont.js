﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Common');
    var oSlotDayNames = ["XYZ","Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var AllAcademicYear;
    var AllGradesSec;
    var gradeSelected;
    var getSectionFromGrade;
    var data = [];
    var secArr = [];
    var dayLec = [];
    var gradeSecArr = [];
    var settings = { 'on': 'glyphicon glyphicon-check', 'off': 'glyphicon glyphicon-check' };
    var counter = 0;
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    
    simsController.controller('AccessControlRoleDegConfigCont',
        ['$scope', '$compile', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$locale', '$http', 'ENV', function ($scope, $compile, $state, $rootScope, $timeout, gettextCatalog, $locale, $http, ENV) {

            $scope.edt = { sims_role_deg_code: {},sims_access_code:''};
            $scope.edtapp = {};

            $scope.opr = "R";
            $scope.AllGradesSec = [];

            $scope.Allappls = [];

            $(function () {
                $('#cmb_role_desg').multipleSelect({ width: '100%' });
            });

            $scope.preview = function () {
                if ($scope.edt.sims_access_code != '') {
                    $http.get(ENV.apiUrl + "api/AccessCriteria/getUsersAccessCriteriaRoleDesig?flag=" + $scope.opr + "&data=" + $scope.edt.sims_role_deg_code + "&accesscode=" + $scope.edt.sims_access_code).then(function (users) {
                        $scope.users = users.data;
                    });
                }
                else {
                    swal({
                        text: 'Please select Access Criteria',
                        width: 400,
                        height: 300
                    });
                }
            }

            $scope.assign = function (code) {
                $scope.edt['sims_access_code'] = code;
            }

            $scope.getBellData = function () {
                $http.get(ENV.apiUrl + "api/AccessCriteria/getAccessCriteria").then(function (res) {
                    $scope.savedBellData = res.data;
                });
            }
          
            $scope.getData = function (opt) {
                if (opt == "SRL")
                    $scope.opr = "R";
                else
                    $scope.opr = "D";

                $http.get(ENV.apiUrl + "api/AccessCriteria/getAccessCriteriaRoleDesig?opt=" + opt).then(function (res1) {
                    $scope.roleDesigData = res1.data;
                    setTimeout(function () {
                        $('#cmb_role_desg').change(function () {
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }

            $scope.editBell = function (access) {
                $scope.edt = access;
                $scope.opr = "AE";
                //$scope.getGradeSectionData();
                //$scope.getApplicationsData();
            }

            $scope.CheckMultiple = function () {
                var main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.users.length; i++) {
                    var t = $scope.users[i].sims_user_name;
                    var v = document.getElementById('chk'+i);
                    if (main.checked == true) {
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $scope.users['sims_status'] = true;
                    }
                    else {
                        $scope.users['sims_status'] = true;
                        v.checked = false;
                        $scope.row1 = '';
                    }
                }
                console.log($scope.users);
            }

            $scope.check = function (id, app) {
                var v = document.getElementById('chk' + id);
                if (v.checked == true) {
                    v.checked = true;
                    $scope.users[id].sims_status = true;
                }
                else {
                    $scope.users[id].sims_status = false;
                    v.checked = false;
                }
                console.log($scope.users);

            }
            
            $scope.saveconfig = function () {
                $http.post(ENV.apiUrl + "api/AccessCriteria/UsersAccessCriteriaRoleDesigCRUD?flag=" + $scope.opr + "&data=" + $scope.edt.sims_role_deg_code + "&access_code=" + $scope.edt.sims_access_code, $scope.users).then(function (res1) {
                    if (res1.data == true) {
                        swal({
                            text: 'Access Details Saved Sucessfully',
                            imageUrl: "assets/img/check.png",
                            width: 400,
                            height: 300
                        });
                    }
                    else {
                        swal({
                            text: 'Error while Saving Details',
                            imageUrl: "assets/img/notification-alert.png",
                            width: 400,
                            height: 300
                        });
                    }
                    $scope.users = [];
                    
                });
            }
 
            $scope.removeSelectedClass = function (e) {

                if (e.target.tagName == 'i' || e.target.tagName == 'I') {
                    var $checkbox = $(e.target.parentNode).next('input:checkbox');
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.triggerHandler('change');
                    $scope.updateDisplay(e.target.parentNode.parentNode);
                    var id = $(e.target.parentNode).parents('.grade_section').attr("id");
                }
                else {
                    var $checkbox = $(e.target.parentNode).find('input:checkbox');
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.triggerHandler('change');
                    $scope.updateDisplay(e.target.parentNode);
                    var id = $(e.target).parents('.grade_section').attr("id");
                }

                if ($checkbox.is(':checked')) {
                    $("#" + id + " .sectionbyclass .button-checkbox").find('button');
                    $("#" + id + " .sectionbyclass .button-checkbox").find('input:checkbox').prop("checked", true);
                }
                else {
                    $("#" + id + " .sectionbyclass .button-checkbox").find('button');
                    $("#" + id + " .sectionbyclass .button-checkbox").find('input:checkbox').prop("checked", false);
                }


                $("#" + id + " .sectionbyclass .button-checkbox").each(function (k, v) {
                    $scope.updateDisplay(this);
                })
            }

            $scope.selectRemoveSections = function (e) {
                if (e.target.tagName == 'i' || e.target.tagName == 'I') {
                    $checkbox = $(e.target.parentNode).next('input:checkbox');
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.triggerHandler('change');
                    $scope.updateDisplay(e.target.parentNode.parentNode);
                }
                else {
                    var $checkbox = $(e.target.parentNode).find('input:checkbox');
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.triggerHandler('change');
                    $scope.updateDisplay(e.target.parentNode);
                }

                var classSelect = 'I';

                if ($(e.target).parents('.sectionbyclass').find('.active').length > 0) {
                    classSelect = 'A';
                }
                //$(e.target).parents('.sectionbyclass').find('input:checkbox').each(function (k, v) {
                //    if ($(v).is(':checked')) {
                //        classSelect = 'A';
                //    }
                //})

                if (classSelect == 'I') {
                    $(e.target).parents('.grade_section_row').find('.gradeClass').find('.glyphicon').removeClass('glyphicon-check').addClass('glyphicon-unchecked');

                    $(e.target).parents('.grade_section_row').find('.gradeClass').find('.btn').removeClass('btn-danger').addClass('btn-default');
                    $(e.target).parents('.grade_section_row').find('.gradeinput').prop('checked', false);
                }
                else {
                    $(e.target).parents('.grade_section_row').find('.gradeClass').find('.glyphicon').addClass('glyphicon-check').removeClass('glyphicon-unchecked');
                    $(e.target).parents('.grade_section_row').find('.gradeClass').find('.btn').addClass('btn-danger').removeClass('btn-default');
                    $(e.target).parents('.grade_section_row').find('.gradeinput').prop('checked', true);
                }
            }

           
            $scope.removeLec = function(e)
            {
                $(e.target.parentNode).remove();
            }
            
            $scope.updateDisplay = function (obj)
            {
                var $button = $(obj).find('button'), $checkbox = $(obj).find('input:checkbox'), color = $button.attr('data-color');
                
                var isChecked = $checkbox.is(':checked');

                $button.data('state', (isChecked) ? "on" : "off");
                
                if (isChecked)
                {
                    $button.find('.state-icon').removeClass('glyphicon glyphicon-unchecked').addClass('glyphicon glyphicon-check');
                }
                else {
                    $button.find('.state-icon').removeClass('glyphicon glyphicon-check').addClass('glyphicon glyphicon-unchecked');
                }

                
                if (isChecked)
                {
                    $button.removeClass('btn-default').addClass('btn-' + color + ' active');
                }
                else
                {
                    $button.removeClass('btn-' + color + ' active').addClass('btn-default');
                }
            }


            $scope.in_array = function (array, dayid)
            {
                for (var i = 0; i < array.length; i++)
                {
                    if (array[i].day_code === dayid)
                    {
                         return i+1;
                    }
                }
                return false;
            }

            $scope.accordian = function(e)
            {
                if ($(e.target.parentNode).next('.toggle_section').css('display') != 'none') {
                    $(e.target).removeClass('glyphicon-minus').addClass('glyphicon-plus');
                }
                else {
                    $(e.target).removeClass('glyphicon-plus').addClass('glyphicon-minus');
                }
                $(e.target.parentNode).next('.toggle_section').slideToggle();
            }

          
            $scope.selectLectureByDay = function (dayid)
            {
                $("#daytab_" + dayid).addClass('active').siblings('.daytabitem').removeClass('active');
                $("#day_" + dayid).addClass('active').siblings('.days').removeClass('active');
                $(".savelecture").addClass('blink');
                $(".addlecturediv").html('');
                $($scope.AllDayLec).each(function (key, val)
                {
                    if (val.day_code == dayid)
                    {
                        $(val.lecture).each(function (k, v)
                        {
                            var randNum = Math.floor((Math.random() * 100) + 1);
                            var lectid = document.getElementById('lec_' + v.slot_code);
                            var isbreak;
                            if (lectid === null) {
                                if (v.isBreak == 'Y') {
                                    isbreak = '<input type="checkbox" class="k-checkbox" value="' + val.day_code + '_' + v.slot_code + '" checked id="break" name="break" />';
                                }
                                else {
                                    isbreak = '<input type="checkbox" class="k-checkbox" value="' + val.day_code + '_' + v.slot_code + '"  id="break" name="break" />';
                                }
                                var html = '<div class="addlecturefield" id="lec_' + v.slot_code + '"><i class="glyphicon glyphicon-remove removelec" ng-click="removeLec($event)"></i>';
                                html += '<div class="form-group">';
                                html += '<input type="text" class="form-control sujectClass k-textbox" placeholder="Lacture Name" value="' + v.lect_desc + '">';
                                html += '</div>';
                                html += '<div class="form-group">';
                                html += '<label>From</label>';
                                html += '<input type="time" class="form-control startClass k-textbox" value="' + v.from_time + '" placeholder="yy">';
                                html += '</div>';
                                html += '<div class="form-group">';
                                html += '<label>to</label>';
                                html += '<input type="time" class="form-control endClass k-textbox" value="' + v.to_time + '" placeholder="yy">';
                                html += '</div>';
                                html += '<div class="breakDiv">' + isbreak + ' </div><label>  Is Break ?</label></div>';
                                html += '</div>';

                                var temp = $compile(html)($scope);
                                $(".addlecturediv").append(temp);
                            }

                        })
                    }
                })

                
            }

            $(document).ready(function ()
            {
                $scope.getBellData();
               // $scope.getAcademicYear();
                $scope.condenseMenu();
            });

        }]);

})();
