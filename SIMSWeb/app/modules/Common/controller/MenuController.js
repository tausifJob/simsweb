﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Common');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('MenuController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$location', '$anchorScroll', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $location, $anchorScroll) {
            // Pre-Required Functions and Variables
            // Start

            $scope.url = $state.current.url;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1, 'z-index': 1 });
            }, 100);

            var user = $rootScope.globals.currentUser.username;
            //  $scope.lagn = true;

          
            $scope.lagn = gettextCatalog.currentLanguage == "en" ? true : false;


            $scope.$on('lagnuage_chaged', function (str) {
               
                if ($http.defaults.headers.common['schoolId'] == 'siscollege') {
                    $scope.lagn = gettextCatalog.currentLanguage == true;
                    $scope.module_name = $rootScope.module.module_name_en;

                }
                else
                {
                    $scope.lagn = gettextCatalog.currentLanguage == "en" ? true : false;
                    $scope.module_name = gettextCatalog.currentLanguage == "en" ? $rootScope.module.module_name_en : $rootScope.module.module_name_ar;
                }
                $scope.module_help = $rootScope.module.module_desc;
            });

            $http.get(ENV.apiUrl + "api/common/GetMenu?user_code=" + user + "&module_code=" + $rootScope.module.module_code).then(function (menuData) {
                debugger;
                $scope.menuData = menuData.data;
                $scope.tags = $scope.menuData[0];
                $scope.applications = $scope.menuData[1];

                if ($scope.Main_menu_pref == '1') {
                    $('.table-responsive').addClass('default');
                    $('.table-responsive').removeClass('comfort');
                    $('.table-responsive').removeClass('compact');
                }
                else if ($scope.Main_menu_pref == '2') {
                    $('.table-responsive').removeClass('default');
                    $('.table-responsive').addClass('comfort');
                    $('.table-responsive').removeClass('compact');
                }

                else if ($scope.Main_menu_pref == '3') {
                    $('.table-responsive').removeClass('default');
                    $('.table-responsive').removeClass('comfort');
                    $('.table-responsive').addClass('compact');
                }

                //setInterval(function () {
                //    $("#S button").css({ 'background-color': '#BB80ef' });
                //    $("#T button").css({ 'background-color': '#698cee' });
                //    $("#Q button").css({ 'background-color': '#e8a7a0' });
                //    $("#R button").css({ 'background-color': '#59dbd1' });
                //    $("#G button").css({ 'background-color': '#fe46c0' });
                //    $("#A button").css({ 'background-color': '#0090d9' });
                //    $("#M button").css({ 'background-color': '#fdcd00' });
                //    $("#C button").css({ 'background-color': '#999999' });
                //},200);
                
            });

            $scope.$on('module-chage', function () {

                if ($http.defaults.headers.common['schoolId'] == 'ss') {
                    $scope.lagn = gettextCatalog.currentLanguage == true;
                    $scope.module_name = $rootScope.module.module_name_en;

                }
                else {
                    $scope.lagn = gettextCatalog.currentLanguage == "en" ? true : false;
                    $scope.module_name = gettextCatalog.currentLanguage == "en" ? $rootScope.module.module_name_en : $rootScope.module.module_name_ar;
                }
                $scope.module_help = $rootScope.module.module_desc;
                $http.get(ENV.apiUrl + "api/common/GetMenu?user_code=" + user + "&module_code=" + $rootScope.module.module_code).then(function (menuData) {
                    $scope.menuData = menuData.data;
                    $scope.tags = $scope.menuData[0];
                    $scope.applications = $scope.menuData[1];
                    debugger
                    if ($scope.Main_menu_pref == '1') {
                        $('.table-responsive').addClass('default');
                        $('.table-responsive').removeClass('comfort');
                        $('.table-responsive').removeClass('compact');
                    }
                    else if ($scope.Main_menu_pref == '2') {
                        $('.table-responsive').removeClass('default');
                        $('.table-responsive').addClass('comfort');
                        $('.table-responsive').removeClass('compact');
                    }

                    else if ($scope.Main_menu_pref == '3') {
                        $('.table-responsive').removeClass('default');
                        $('.table-responsive').removeClass('comfort');
                        $('.table-responsive').addClass('compact');
                    }
                });
            })
            $scope.gotoapp = function (app) {
                var user = $rootScope.globals.currentUser.username;
                $scope.check_favorite(app.comn_user_appl_code);

                $scope.ar_en_fun(app.comn_user_appl_code);
                $scope.hide_folat_btn = true;

                if (app.comn_appl_type == 'R') {
                    $rootScope.location = app.comn_appl_location;
                    $rootScope.main_appl_code = app.comn_user_appl_code;
                    setTimeout(function () { $state.go('main.ReportCard'); }, 100);
                    $state.go('main');
                    if ($scope.lagn)
                        $scope.menu_list_click('', app.comn_appl_mod_code, app.comn_appl_name_en, app.comn_user_appl_code, app.comn_appl_name_ar)
                  //  else
                     //   $scope.menu_list_click('', app.comn_appl_mod_code, app.comn_appl_name_ar, app.comn_user_appl_code)

                }
                else {
                    if ($scope.lagn)
                        $scope.menu_list_click('', app.comn_appl_mod_code, app.comn_appl_name_en, app.comn_user_appl_code, app.comn_appl_name_ar)
                  //  else
                     //   $scope.menu_list_click('', app.comn_appl_mod_code, app.comn_appl_name_ar, app.comn_user_appl_code)

                 //   $scope.menu_list_click('', app.comn_appl_mod_code, app.comn_appl_name_en, app.comn_user_appl_code)

                    if ($scope.fin == undefined) {
                       

                        if (app.comn_appl_mod_code == '005') {
                            $scope.loader_show = true;
                            $http.get(ENV.apiUrl + "api/PDCSubmission/GetComp_Name?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                                $scope.cmbCompany = res.data;
                                if (res.data.length == 1) {
                                    $scope.companyName = res.data[0].comp_name;

                                    $scope.finnComp = res.data[0].comp_code;

                                    $http.get(ENV.apiUrl + "api/PDCSubmission/GetFinancial_year?comp_code=" + $scope.finnComp + "&username=" + $rootScope.globals.currentUser.username).then(function (res) {

                                        if (res.data.length > 0) {
                                            var data = {
                                                year: res.data[0].financial_year,
                                                company: $scope.finnComp,
                                                companyame: $scope.companyName
                                            }
                                            window.localStorage["Finn_comp"] = JSON.stringify(data)
                                            $state.reload();
                                            setTimeout(function () {
                                                $scope.loader_show = false;
                                            }, 600);

                                            // $.cookie("finnComapny", $scope.finnComp);
                                            // window.localStorage("finnComapny", $scope.finnComp);

                                            //$.cookie("finnYear", res.data[0].financial_year);

                                            //if ($.cookie("finnComapny") != null) {

                                            
                                            //}

                                        }
                                    });

                                    //  AuthenticationService.SetCompany(res.data[0].comp_code, '2017');



                                }
                                else if (res.data.length > 1) {
                                    $scope.financeWindow();
                                }


                            });
                        }

                    }

            
                    if ($http.defaults.headers.common['schoolId'] == 'imert') {
                        if (app.comn_user_appl_code == 'Lms001' || app.comn_user_appl_code == '2') {
                            window.open('http://imert.learntron.net/learntron/index.html', "_new");

                        }
                    }

                  
                  else  if ($http.defaults.headers.common['schoolId'] == 'adis') {
                        if (app.comn_user_appl_code == '2')   //Online Access
                        {
                            window.open('http://oa.mograsys.com/adis/OnlineAdmission.html', "_new");
                        }
                    }

                  else if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {

                        if (app.comn_user_appl_code == 'Lms003')   //Online Access
                        {
                            window.open('http://10.10.200.3:4000/learntron/index.html' + "?u=" + $rootScope.globals.currentUser.username + "&k=kHs6EhhdGGU=", "_new");
                        }
                        if (app.comn_user_appl_code == 'Lms002') //local
                        {
                            window.open('http://dpsmisdohacsms.com:4000/learntronhome/index.html?u=' + $rootScope.globals.currentUser.username + '&k=kHs6EhhdGGU=', "_new");
                        }

                    }

                  else  if ($http.defaults.headers.common['schoolId'] == 'adisw') {


                        if (app.comn_user_appl_code == 'Lms001') {

                            $http.get(ENV.apiUrl + "api/common/getUserDetails_pass?uname=" + $rootScope.globals.currentUser.username).then(function (res) {
                                // $scope.obj = res.data;
                               
                                window.open("http://lms.mograsys.com/mdladisw/autologin.php" + "?u=" + $rootScope.globals.currentUser.username + "&p=" + res.data.table[0].comn_user_password, "_new");
                                //  $state.go('main.dashboard');

                                //$('#load').show();

                                //$scope.uri = "http://www.google.com/";
                                //$scope.url = encodeURI($scope.uri);
                                //$("#load").attr("src", $sce.trustAsResourceUrl($scope.url));

                            });


                        }
                  }
                  else if ($http.defaults.headers.common['schoolId'] == 'asd' || $http.defaults.headers.common['schoolId'] == 'asdportal') {
                      if (app.comn_user_appl_code == 'Lms001') {
                          $state.go('main.lmsAsd')
                      }
                  }
                    else {
                      if (app.comn_user_appl_code == 'Lms001') {

                            //$http.get(ENV.apiUrl + "api/common/getUserDetails_pass?uname=" + $rootScope.globals.currentUser.username).then(function (res) {
                            //    // $scope.obj = res.data;
                            //    console.log($scope.schoolDetails_data.lic_lms_url);
                            //   // window.open($scope.schoolDetails_data.lic_lms_url + "?u=" + $rootScope.globals.currentUser.username + "&p=" + res.data.table[0].comn_user_password, "_new");
                            //    window.open($scope.schoolDetails_data.lic_lms_url + "?u=" + $rootScope.globals.currentUser.username + "&p=" + res.data.table[0].comn_user_password + '&t=t&sc=' + $http.defaults.headers.common['schoolId'], "_new");



                            //});
                          $state.go('main.lmsAll')

                        }

                    }

                    if (app.comn_user_appl_code == 'gblsea') {

                        $scope.searchGlobalClick();


                    }

                    $state.go('main.' + app.comn_user_appl_code);


                }

                if ($scope.oldobj == undefined) {


                    $scope.oldobj = {
                        opr: 'I',
                        comn_audit_user_code: $rootScope.globals.currentUser.username,
                        comn_appl_name_en: app.comn_user_appl_code,
                        comn_audit_start_time: $scope.getDateTime(),
                        comn_audit_ip: $rootScope.ip_add,
                        comn_audit_dns: $rootScope.dns,


                        //comn_audit_end_time: '',
                    }
                    $http.post(ENV.apiUrl + "api/common/InsertUserAudit", $scope.oldobj).then(function (res) {

                    });
                }
                else {

                    var edate = $scope.getDateTime()
                    var data = {
                        opr: 'I',
                        comn_audit_user_code: $rootScope.globals.currentUser.username,
                        comn_appl_name_en: app.comn_user_appl_code,
                        comn_audit_start_time: edate,
                        comn_audit_ip: $rootScope.ip_add,
                        comn_audit_dns: $rootScope.dns,

                    }

                    $http.post(ENV.apiUrl + "api/common/InsertUserAudit", data).then(function (res) {


                        $scope.oldobj['comn_audit_end_time'] = edate
                        $scope.oldobj['opr'] = 'J'

                        $http.post(ENV.apiUrl + "api/common/InsertUserAudit", $scope.oldobj).then(function (res) {

                            $scope.oldobj['comn_audit_start_time'] = edate
                            $scope.oldobj['comn_appl_name_en'] = app.comn_user_appl_code

                        });

                    });

                }
            }


            //if ($http.defaults.headers.common['schoolId'] == 'demo') {
            //    debugger
            //    //$scope.comfortGrid('comfort');

            //    $('.table-responsive').addClass('comfort');
            //    $('.table-responsive').removeClass('default');
            //    $('.table-responsive').removeClass('compact');
            //}
            
          
            //Events End
        }])

})();
