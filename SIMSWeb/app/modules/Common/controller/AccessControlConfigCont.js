﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Common');
    var oSlotDayNames = ["XYZ","Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var AllAcademicYear;
    var AllGradesSec;
    var gradeSelected;
    var getSectionFromGrade;
    var data = [];
    var secArr = [];
    var dayLec = [];
    var gradeSecArr = [];
    var settings = { 'on': 'glyphicon glyphicon-check', 'off': 'glyphicon glyphicon-check' };
    var counter = 0;
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    
        simsController.controller('AccessControlConfigCont',
        ['$scope', '$compile', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$locale', '$http', 'ENV', function ($scope, $compile, $state, $rootScope, $timeout, gettextCatalog, $locale, $http, ENV) {

            $scope.edt = {};
            $scope.edtapp = {};

            $scope.opr = "AI";
            $scope.AllGradesSec = [];
            $scope.Allappls = [];

            $scope.newConfig = function () {
                $scope.edt = {};
                $scope.opr = "AI";
                $scope.getGradeSectionData();
                $scope.getApplicationsData();
            }


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;

                //$scope.sims_cur_code = $scope.curriculum[0].sims_cur_code

               $scope.edt = {
                   'sims_cur_code': $scope.curriculum[0].sims_cur_code
               }
                $scope.getAccYear($scope.edt.sims_cur_code)
            });


            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;

                  //  $scope.sims_cur_code=$scope.curriculum[0].sims_cur_code,
                   // $scope.sims_academic_year = $scope.Acc_year[0].sims_academic_year
                    debugger;
                    $scope.edt2['sims_academic_year'] = $scope.Acc_year[0].sims_academic_year
                    //{
                    //    'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                    //    'sims_academic_year': $scope.Acc_year[0].sims_academic_year
                    //}
                    $scope.getGradeSectionData();

                });
            }





            $scope.getBellData = function () {

                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                    $scope.curriculum = AllCurr.data;
                    $scope.edt2 = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code
                    }
                    //$scope.getAccYear($scope.edt.sims_cur_code)
               
                    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.edt2.sims_cur_code).then(function (Acyear) {
                        $scope.Acc_year = Acyear.data;

                        //  $scope.sims_cur_code=$scope.curriculum[0].sims_cur_code,
                        // $scope.sims_academic_year = $scope.Acc_year[0].sims_academic_year
                        debugger;
                        $scope.edt2 = {
                            'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                            'sims_academic_year': $scope.Acc_year[0].sims_academic_year
                        }
                   


                $http.get(ENV.apiUrl + "api/AccessCriteria/getAccessCriteria").then(function (res) {
                    $scope.savedBellData = res.data;
                    $scope.getGradeSectionData();
                    $scope.getApplicationsData();
                });


                    });

                });

            }

          
            $scope.getGradeSectionData = function () {
                debugger
                if ($scope.edt.sims_access_code == undefined) $scope.edt.sims_access_code = '';
                $http.get(ENV.apiUrl + "api/AccessCriteria/getAllGradesSecAC?accesscode=" + $scope.edt.sims_access_code + "&sims_cur_code=" + $scope.edt2.sims_cur_code + "&sims_academic_year=" + $scope.edt2.sims_academic_year).then(function (res1) {
                    $(".datacontent").removeClass('hide');
                    $scope.AllGradesSec = res1.data;
                    $("#grade_select").prop("disabled", false).removeClass('seldisabled');
                });
            }

            $scope.getApplicationsData = function () {
                if ($scope.edt.sims_access_code == undefined) $scope.edt.sims_access_code = '';
                $http.get(ENV.apiUrl + "api/AccessCriteria/getAllApplicationsAC?accesscode=" + $scope.edt.sims_access_code).then(function (res2) {
                    console.log(res2.data);
                    //   $(".datacontent").removeClass('hide');
                    $scope.AllApplications = res2.data;
                    console.log($scope.AllApplications);
                    //$("#grade_select").prop("disabled", false).removeClass('seldisabled');
                });
            }
            
            $scope.editBell = function (access) {
                $scope.edt = access;
                $scope.opr = "AE";
                $scope.getGradeSectionData();
                $scope.getApplicationsData();
            }

            $scope.deleteBell = function (access) {
                $scope.opr = "AE";
                access['sims_access_status'] = false;
                $http.post(ENV.apiUrl + "api/AccessCriteria/AccessCriteriaCRUD_new?opr=" + $scope.opr, access).then(function (res) {
                    if (res.data == true) {
                        $scope.getBellData();
                    }
                });
            }



            $scope.saveconfig = function () {
                debugger
                var appArr = [];
                gradeSecArr = [];
                $('.grade_section_row').each(function (k, v) {
                    secArr = [];

                    var rowid = $(this).attr('id');
                    var grade_code = $(this).find('.gradeClass').find('input:checkbox').val();
                    var grade_name = $(this).find('.gradeClass').find('input:checkbox').data('name');
                    var issecsele, isgradesele;

                    $(this).find('.sectionbyclass').find('input:checkbox').each(function (key, val) {
                        if (val.checked) {
                            issecsele = 'A';
                        }
                        else {
                            issecsele = 'I';
                        }
                        var sec_code = $(this).val();
                        var sec_namecode = $(this).data('name');
                        if (issecsele == "A")
                            secArr.push({ 'sims_section_code': sec_code, 'sims_section_name': sec_namecode, 'status': issecsele });
                    })
                    gradeSecArr.push({ 'grade_code': grade_code, 'grade_name': grade_name, 'section': secArr });
                })

                var dataarr = [];
                dataarr.push(gradeSecArr);
                $scope.edt['sims_academic_year'] = $scope.edt2.sims_academic_year;
                $scope.edt['sims_cur_code'] = $scope.edt2.sims_cur_code;

                dataarr.push($scope.edt);
                dataarr.push($scope.AllApplications);
                $http.post(ENV.apiUrl + "api/AccessCriteria/AccessCriteriaCRUD?opr=" + $scope.opr, dataarr).then(function (res) {
                    if (res.data == true) {
                        swal({
                            text: 'Configuration Saved Sucessfully',
                            imageUrl: "assets/img/check.png",
                            width: 400,
                            height: 300
                        });
                        $scope.getBellData();
                        $scope.edt = {};
                        $scope.opr = "AI";
                    }
                    else {
                        swal({
                            text: 'Error while saving Configuration',
                            imageUrl: "assets/img/notification-alert.png",
                            width: 400,
                            height: 300
                        });
                    }
                });
            }

            
            $scope.removeSelectedClass = function (e) {

                if (e.target.tagName == 'i' || e.target.tagName == 'I') {
                    var $checkbox = $(e.target.parentNode).next('input:checkbox');
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.triggerHandler('change');
                    $scope.updateDisplay(e.target.parentNode.parentNode);
                    var id = $(e.target.parentNode).parents('.grade_section').attr("id");
                }
                else {
                    var $checkbox = $(e.target.parentNode).find('input:checkbox');
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.triggerHandler('change');
                    $scope.updateDisplay(e.target.parentNode);
                    var id = $(e.target).parents('.grade_section').attr("id");
                }

                if ($checkbox.is(':checked')) {
                    $("#" + id + " .sectionbyclass .button-checkbox").find('button');
                    $("#" + id + " .sectionbyclass .button-checkbox").find('input:checkbox').prop("checked", true);
                }
                else {
                    $("#" + id + " .sectionbyclass .button-checkbox").find('button');
                    $("#" + id + " .sectionbyclass .button-checkbox").find('input:checkbox').prop("checked", false);
                }


                $("#" + id + " .sectionbyclass .button-checkbox").each(function (k, v) {
                    $scope.updateDisplay(this);
                })
            }

            $scope.selectRemoveSections = function (e) {
                if (e.target.tagName == 'i' || e.target.tagName == 'I') {
                    $checkbox = $(e.target.parentNode).next('input:checkbox');
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.triggerHandler('change');
                    $scope.updateDisplay(e.target.parentNode.parentNode);
                }
                else {
                    var $checkbox = $(e.target.parentNode).find('input:checkbox');
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.triggerHandler('change');
                    $scope.updateDisplay(e.target.parentNode);
                }

                var classSelect = 'I';

                if ($(e.target).parents('.sectionbyclass').find('.active').length > 0) {
                    classSelect = 'A';
                }
                //$(e.target).parents('.sectionbyclass').find('input:checkbox').each(function (k, v) {
                //    if ($(v).is(':checked')) {
                //        classSelect = 'A';
                //    }
                //})

                if (classSelect == 'I') {
                    $(e.target).parents('.grade_section_row').find('.gradeClass').find('.glyphicon').removeClass('glyphicon-check').addClass('glyphicon-unchecked');

                    $(e.target).parents('.grade_section_row').find('.gradeClass').find('.btn').removeClass('btn-danger').addClass('btn-default');
                    $(e.target).parents('.grade_section_row').find('.gradeinput').prop('checked', false);
                }
                else {
                    $(e.target).parents('.grade_section_row').find('.gradeClass').find('.glyphicon').addClass('glyphicon-check').removeClass('glyphicon-unchecked');
                    $(e.target).parents('.grade_section_row').find('.gradeClass').find('.btn').addClass('btn-danger').removeClass('btn-default');
                    $(e.target).parents('.grade_section_row').find('.gradeinput').prop('checked', true);
                }
            }

            $scope.removeSelectedClass1 = function (app,data,e) {

                if (e.target.tagName == 'i' || e.target.tagName == 'I') {
                    var $checkbox = $(e.target.parentNode).next('input:checkbox');
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.triggerHandler('change');
                    $scope.updateDisplay(e.target.parentNode.parentNode);
                    var id = $(e.target.parentNode).parents('.grade_section').attr("id");
                }
                else {
                    var $checkbox = $(e.target.parentNode).find('input:checkbox');
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.triggerHandler('change');
                    $scope.updateDisplay(e.target.parentNode);
                    var id = $(e.target).parents('.grade_section').attr("id");
                }

                if ($checkbox.is(':checked')) {
                    $("#" + id + " .sectionbyclass .button-checkbox").find('button');
                    $("#" + id + " .sectionbyclass .button-checkbox").find('input:checkbox').prop("checked", true);
                }
                else {
                    $("#" + id + " .sectionbyclass .button-checkbox").find('button');
                    $("#" + id + " .sectionbyclass .button-checkbox").find('input:checkbox').prop("checked", false);
                }


                $("#" + id + " .sectionbyclass .button-checkbox").each(function (k, v) {
                    $scope.updateDisplay(this);
                })
            }

            $scope.selectRemoveSections1 = function (app, data, e) {
                alert(data);
                if (e.target.tagName == 'i' || e.target.tagName == 'I') {
                    $checkbox = $(e.target.parentNode).next('input:checkbox');
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.triggerHandler('change');
                    $scope.updateDisplay(e.target.parentNode.parentNode);
                }
                else {
                    var $checkbox = $(e.target.parentNode).find('input:checkbox');
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.triggerHandler('change');
                    $scope.updateDisplay(e.target.parentNode);
                }

                var classSelect = 'I';

                if ($(e.target).parents('.sectionbyclass').find('.active').length > 0) {
                    classSelect = 'A';
                }
                //$(e.target).parents('.sectionbyclass').find('input:checkbox').each(function (k, v) {
                //    if ($(v).is(':checked')) {
                //        classSelect = 'A';
                //    }
                //})

                if (classSelect == 'I') {
                    $(e.target).parents('.grade_section_row').find('.gradeClass').find('.glyphicon').removeClass('glyphicon-check').addClass('glyphicon-unchecked');

                    $(e.target).parents('.grade_section_row').find('.gradeClass').find('.btn').removeClass('btn-danger').addClass('btn-default');
                    $(e.target).parents('.grade_section_row').find('.gradeinput').prop('checked', false);
                }
                else {
                    $(e.target).parents('.grade_section_row').find('.gradeClass').find('.glyphicon').addClass('glyphicon-check').removeClass('glyphicon-unchecked');
                    $(e.target).parents('.grade_section_row').find('.gradeClass').find('.btn').addClass('btn-danger').removeClass('btn-default');
                    $(e.target).parents('.grade_section_row').find('.gradeinput').prop('checked', true);
                }
            }

           
            $scope.removeLec = function(e)
            {
                $(e.target.parentNode).remove();
            }
            
            $scope.updateDisplay = function (obj)
            {
                var $button = $(obj).find('button'), $checkbox = $(obj).find('input:checkbox'), color = $button.attr('data-color');
                
                var isChecked = $checkbox.is(':checked');

                $button.data('state', (isChecked) ? "on" : "off");
                
                if (isChecked)
                {
                    $button.find('.state-icon').removeClass('glyphicon glyphicon-unchecked').addClass('glyphicon glyphicon-check');
                }
                else {
                    $button.find('.state-icon').removeClass('glyphicon glyphicon-check').addClass('glyphicon glyphicon-unchecked');
                }

                
                if (isChecked)
                {
                    $button.removeClass('btn-default').addClass('btn-' + color + ' active');
                }
                else
                {
                    $button.removeClass('btn-' + color + ' active').addClass('btn-default');
                }
            }

            $scope.saveData = function()
            {
                $(".savebellconfig").html('<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>');
                data = [];
                gradeSecArr = [];
                var acaDesc = $("#acaYear option:selected").text();
                var acaYear = $("#acaYear").val();
                var bellName = $("#bellName").val();

                $('.grade_section_row').each(function (k, v)
                {
                    secArr = [];

                    var rowid = $(this).attr('id');
                    var grade_code = $(this).find('.gradeClass').find('input:checkbox').val();
                    var grade_name = $(this).find('.gradeClass').find('input:checkbox').data('name');
                    var issecsele, isgradesele;

                    $(this).find('.sectionbyclass').find('input:checkbox').each(function (key, val)
                    {
                        if (val.checked)
                        {
                            issecsele = 'A';
                        }
                        else
                        {
                            issecsele = 'I';
                        }
                        var sec_code = $(this).val();
                        var sec_namecode = $(this).data('name');
                        secArr.push({ 'sims_section_code': sec_code, 'sims_section_name': sec_namecode, 'status': issecsele });
                    })
                    
                    gradeSecArr.push({ 'grade_code': grade_code, 'grade_name': grade_name, 'section': secArr });
                })

                if(acaYear == "" || acaYear == 'undefined')
                {
                    alert("Academic Year Required");
                }
                else if(bellName == "" || bellName == 'undefined')
                {
                    alert("Bell Name Required");
                }
                else if (gradeSecArr.length == 0)
                {
                    alert("Please Select Grades And Section");
                }
                else
                {
                    data.push({ 'aca_year': acaYear, 'bell_name': bellName, 'bellGrade': gradeSecArr, 'bellDay': dayLec });
                }

                console.log("This is final data = ", data);

                if (data.length != 0)
                {
                    var bell_code = null;
                    var cTemp = 'I';
                    if ($('body').hasClass('editmode'))
                    {
                        bell_code = $("body").data('bellcode');
                        cTemp = 'U';
                    }
                    
                    $http.post(ENV.apiUrl + "api/BellConfig/BellCongifDataUpdate?cTemp=" + cTemp + "&bell_code=" + bell_code + "&aca_year=" + acaYear, data).success(function (res) {
                        var html = '<div class="summeryItem " id="bellsum_' + res + '_' + acaYear + '" data-acayear="' + acaYear + '" data-bellcode="' + res + '"><strong class="ng-binding">' + acaDesc + '</strong> <span class="ng-binding">' + bellName + '</span>';
                        html += '<div class="pull-right">';
                        html += '<i data-acayear="' + acaYear + '" data-bellcode="' + res + '" ng-click="deleteBell(' + acaYear + ', ' + res + ')"  class="deleteBell glyphicon glyphicon-remove"></i>';
                        html += '<i data-acayear="' + acaYear + '" data-bellcode="' + res + '" ng-click="editBell(' + acaYear + ', ' + res + ', ' + bellName + ')" class="editBell glyphicon glyphicon-pencil"></i>';
                        html += '</div>';
                        html += '</div>';
                        var ttemp = $compile(html)($scope);

                        if ($('.bellAcaSummery').children('#bellsum_' + res + '_' + acaYear).length > 0)
                        {
                            
                        }
                        else {
                            $(".bellAcaSummery").append(ttemp);
                        }
                        

                        $(".savebellconfig").removeAttr('ng-click').css({'opacity':'0.5'});
                        $(".addnewbell").removeClass('hide');
                        $(".savebellconfig").html('Bell data updated succefully!');
                    });
                }
            }

            $scope.in_array = function (array, dayid)
            {
                for (var i = 0; i < array.length; i++)
                {
                    if (array[i].day_code === dayid)
                    {
                         return i+1;
                    }
                }
                return false;
            }

            $scope.accordian = function(e)
            {
                if ($(e.target.parentNode).next('.toggle_section').css('display') != 'none') {
                    $(e.target).removeClass('glyphicon-minus').addClass('glyphicon-plus');
                }
                else {
                    $(e.target).removeClass('glyphicon-plus').addClass('glyphicon-minus');
                }
                $(e.target.parentNode).next('.toggle_section').slideToggle();
            }

          
            $scope.selectLectureByDay = function (dayid)
            {
                $("#daytab_" + dayid).addClass('active').siblings('.daytabitem').removeClass('active');
                $("#day_" + dayid).addClass('active').siblings('.days').removeClass('active');
                $(".savelecture").addClass('blink');
                $(".addlecturediv").html('');
                $($scope.AllDayLec).each(function (key, val)
                {
                    if (val.day_code == dayid)
                    {
                        $(val.lecture).each(function (k, v)
                        {
                            var randNum = Math.floor((Math.random() * 100) + 1);
                            var lectid = document.getElementById('lec_' + v.slot_code);
                            var isbreak;
                            if (lectid === null) {
                                if (v.isBreak == 'Y') {
                                    isbreak = '<input type="checkbox" class="k-checkbox" value="' + val.day_code + '_' + v.slot_code + '" checked id="break" name="break" />';
                                }
                                else {
                                    isbreak = '<input type="checkbox" class="k-checkbox" value="' + val.day_code + '_' + v.slot_code + '"  id="break" name="break" />';
                                }
                                var html = '<div class="addlecturefield" id="lec_' + v.slot_code + '"><i class="glyphicon glyphicon-remove removelec" ng-click="removeLec($event)"></i>';
                                html += '<div class="form-group">';
                                html += '<input type="text" class="form-control sujectClass k-textbox" placeholder="Lacture Name" value="' + v.lect_desc + '">';
                                html += '</div>';
                                html += '<div class="form-group">';
                                html += '<label>From</label>';
                                html += '<input type="time" class="form-control startClass k-textbox" value="' + v.from_time + '" placeholder="yy">';
                                html += '</div>';
                                html += '<div class="form-group">';
                                html += '<label>to</label>';
                                html += '<input type="time" class="form-control endClass k-textbox" value="' + v.to_time + '" placeholder="yy">';
                                html += '</div>';
                                html += '<div class="breakDiv">' + isbreak + ' </div><label>  Is Break ?</label></div>';
                                html += '</div>';

                                var temp = $compile(html)($scope);
                                $(".addlecturediv").append(temp);
                            }

                        })
                    }
                })

                
            }

            $(document).ready(function ()
            {
                $scope.getBellData();
               // $scope.getAcademicYear();
                $scope.condenseMenu();
            });

        }]);

})();
