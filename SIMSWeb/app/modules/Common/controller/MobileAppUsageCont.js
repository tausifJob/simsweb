﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Common');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('MobileAppUsageCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.pagesize = '50';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.pagesize1 = '50';
            $scope.pageindex1 = "0";
            $scope.pager1 = true;

            $scope.busyindicator = true;
            $scope.nullvalues = true;
            $scope.flagdate = '0';

            $scope.from_date = dd + '-' + mm + '-' + yyyy;
            $scope.to_date = dd + '-' + mm + '-' + yyyy;
            $scope.edt = {
                from_date: dd + '-' + mm + '-' + yyyy,
                to_date: dd + '-' + mm + '-' + yyyy,
            }

            $(function () {
                $("#check_from").click(function () {
                    if ($(this).is(":checked")) {
                        $("#from_date").attr("disabled", "disabled");
                        $scope.edt["from_date"] = '';
                    } else {
                        $("#from_date").removeAttr("disabled");
                        $("#from_date").focus();
                        $scope.edt = {
                            from_date: dd + '-' + mm + '-' + yyyy,
                        }
                    }
                });
            });

            $(function () {
                $("#check_to").click(function () {
                    if ($(this).is(":checked")) {
                        $("#to_date").attr("disabled", "disabled");
                        $scope.edt["to_date"] = '';
                    } else {
                        $("#to_date").removeAttr("disabled");
                        $("#to_date").focus();
                        $scope.edt = {
                            from_date: dd + '-' + mm + '-' + yyyy,
                        }
                    }
                });
            });
            
            $scope.GetMobileUsageDetails = function () {
                debugger
                    $scope.busyindicator = false;
                    $http.get(ENV.apiUrl + "api/LibraryAttribute/getMobileUsageDetails?from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date).then(function (res) {
                    $scope.usage_data = res.data;
                    $scope.busyindicator = true;
                    console.log($scope.usage_data);
                    $scope.totalItems = $scope.usage_data.length;
                    $scope.todos = $scope.usage_data;
                    $scope.makeTodos();
                });
        }
            
            //$scope.exportData = function () {
            //    swal({
            //        title: "Alert",
            //        text: "Do you want to Export in MS-Excel?",
            //        showCloseButton: true,
            //        showCancelButton: true,
            //        confirmButtonText: 'Yes',
            //        width: 380,
            //        cancelButtonText: 'No',
            //    }).then(function (isConfirm) {
            //        if (isConfirm) {
            //            //var blob = new Blob([document.getElementById('Div1').innerHTML], {
            //            //            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
            //            //        });
            //            //        saveAs(blob, "Report.xlsx");
            //            //alasql('Select * INTO XLSX("Report.xlsx",{headers:true}) \ FROM HTML ("#DIV1",{headers:true,skipdisplaynone:true})');
            //            //alasql('SELECT * INTO XLSX("Report.xlsx",{headers:true}) \ FROM HTML("#DIV1",{headers:true,skipdisplaynone:true})');

            //            //    var data1 = alasql('SELECT * FROM HTML("#DIV1",{headers:true})');
            //            //var data = $scope.stud_data1;
            //            alasql('SELECT * INTO XLSX("Mobile Usage Statistics.xlsx",{headers:true}) FROM ?', [$scope.usage_data]);
            //            // alasql('SELECT * INTO XLSX("Fee_ledger.xlsx",{headers:true})\FROM HTML("#printdata3",{headers:true,skipdisplaynone:true})', [$scope.usage_data]);
            //        }
            //    });
            //};


            $scope.exportData = function () {
                var check = true;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            alasql('SELECT * INTO XLSX("Mobile Usage Statistics.xlsx",{headers:true}) \ FROM HTML("#example",{headers:true,skipdisplaynone:true})');
                        }
                    });
                }
                else {
                    swal({  text: "Export Failed",imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, })
                }
            };

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = '1';
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.size1 = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos1 = $scope.todos1;
                    $scope.pager1 = false;
                }
                else {
                    $scope.pager1 = true;
                    $scope.pagesize1 = str;
                    $scope.currentPage = '1';
                    $scope.numPerPage = str;
                    $scope.makeTodos1();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['chk_all'] = false;
                $scope.row1 = '';
            }
            $scope.index1 = function (str) {
                $scope.pageindex1 = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos1();
                $scope.chk = {}
                $scope.chk['chk_all'] = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 50, $scope.maxSize = 50;
            $scope.filteredTodos1 = [], $scope.currentPage = 1, $scope.numPerPage = 50, $scope.maxSize = 50;

            $scope.makeTodos = function () {
                debugger;
                if ($scope.pagesize == 'All') {
                    $scope.currentPage = 1;
                    $scope.showPager = false;
                    $scope.filteredTodos = $scope.usage_data;
                }
                else {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                }
            };
            $scope.makeTodos1 = function () {
                debugger;
                if ($scope.pagesize1 == 'All') {
                    $scope.currentPage = 1;
                    $scope.showPager1 = false;
                    $scope.filteredTodos1 = $scope.usage_data;
                }
                else {
                    var rem = parseInt($scope.totalItems1 % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage) + 1;
                    }
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos1 = $scope.todos1.slice(begin, end);
                }
            };

            $scope.usage_details = function (str) {
                $('#MyModal5').modal('show');
                $scope.busyindicator1 = false;
                $http.get(ENV.apiUrl + "api/LibraryAttribute/getMobileUsageDetailsbyuser?from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date + "&comn_user_name=" + str.comn_user_name).then(function (res) {
                    $scope.usage_details_data = res.data;
                    $scope.busyindicator1 = true;
                    console.log($scope.usage_details_data);
                    $scope.totalItems1 = $scope.usage_details_data.length;
                    $scope.todos1 = $scope.usage_details_data;
                    $scope.makeTodos1();
                    $scope.userid = $scope.filteredTodos1[0].comn_user_name;
                    $scope.usernamed = $scope.filteredTodos1[0].comn_user_full_name;
                    $scope.user_mobile_number = $scope.filteredTodos1[0].user_mobile_number;
                    $scope.user_email_address = $scope.filteredTodos1[0].user_email_address;
                   
                });
            }
        }])
})();



