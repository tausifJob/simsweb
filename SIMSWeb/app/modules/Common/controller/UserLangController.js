﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Common');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('UserLangController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$location', '$anchorScroll', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $location, $anchorScroll) {
           
            console.log($rootScope.current_appcode);
                //Events End
            debugger;
    $scope.display = false;
    $scope.pagesize = '10';
    $scope.pageindex = "0";
    $scope.pager = true;
    $scope.grid = true;
    $scope.edit_data = false;
    $scope.tableupdate = true;
    $scope.sequencedata = [];
    var data1 = [];
    var deletecode = [];
    $scope.edt = [];
    $scope.edt['comn_application_code'] = $rootScope.current_appcode;
    
    
    

    $scope.size = function (str) {
        //console.log(str);
        //$scope.pagesize = str;
        //$scope.currentPage = 1;
        //$scope.numPerPage = str;
        //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
        debugger;
        if (str == "All") {
            $scope.currentPage = '1';
            $scope.filteredTodos = $scope.todos;
            $scope.pager = false;
        }
        else {
            $scope.pager = true;
            $scope.pagesize = str;
            $scope.currentPage = 1;
            $scope.numPerPage = str;
            $scope.makeTodos();
        }
    }

    $scope.index = function (str) {
        $scope.pageindex = str;
        $scope.currentPage = str;
        console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
        main.checked = false;
    }

    $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 50, $scope.maxSize = 50;

    $scope.makeTodos = function () {
        var rem = parseInt($scope.totalItems % $scope.numPerPage);
        if (rem == '0') {
            $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
        }
        else {
            $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
        }

        var begin = (($scope.currentPage - 1) * $scope.numPerPage);
        var end = parseInt(begin) + parseInt($scope.numPerPage);

        console.log("begin=" + begin); console.log("end=" + end);

        $scope.filteredTodos = $scope.todos.slice(begin, end);

        for (var i = 0; i < $scope.filteredTodos.length; i++) {

            $scope.filteredTodos[i].icon = 'fa fa-plus-circle';
        }

        $scope.filteredTodos = $scope.todos.slice(begin, end);
    };

    $scope.searched = function (valLists, toSearch) {
        return _.filter(valLists,
        function (i) {
            return searchUtil(i, toSearch);
        });
    };

    $scope.search = function () {
        $scope.todos = $scope.searched($scope.Stud_Details, $scope.searchText);
        $scope.totalItems = $scope.todos.length;
        $scope.currentPage = '1';
        if ($scope.searchText == '') {
            $scope.todos = $scope.Stud_Details;
        }
        $scope.makeTodos();
    }

    function searchUtil(item, toSearch) {
        /* Search Text in all 3 fields */
        return (item.name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
    }



    //$http.get(ENV.apiUrl + "api/common/UserLangController/getuserLang_show").then(function (res) {
    //    debugger;
    //    $scope.display = false;
    //    $scope.grid = true;
    //    $scope.sequencedata = res.data;
    //    $scope.totalItems = $scope.sequencedata.length;
    //    $scope.todos = $scope.sequencedata;
    //    $scope.makeTodos();
         
    //});



    $http.get(ENV.apiUrl + "api/common/ApplicationMaster/getModules").then(function (res) {
        $scope.module_data = res.data;
        console.log($scope.module_data);
    });

    //$scope.edt = {};
    $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
        $scope.cur_data = res.data;
        if (res.data.length > 0) {
            $scope.edt.comm_cur_code = res.data[0].sims_attendance_cur_code;
            $scope.GetAacd_yr($scope.edt.comm_cur_code);
        }
        console.log($scope.cur_data);
    });

    //$scope.GetAacd_yr = function (cur) {
    //    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur).then(function (res) {
    //        $scope.acad = res.data;
    //        console.log($scope.acad);
    //    });
    //}
    $http.get(ENV.apiUrl + "api/common/UserLangController/getApplication").then(function (res) {
        $scope.appl_name = res.data;
        console.log("appl_name");
        console.log($scope.appl_name);
    });


    $scope.GetAacd_yr = function (cur) {
        $http.get(ENV.apiUrl + "api/common/common_seq/getAllAcademicYear?cur=" + cur).then(function (res) {
            $scope.acad = res.data;
            if (res.data.length > 0) {
                $scope.edt.comn_academic_year = res.data[0].sims_academic_year;
            }
            console.log($scope.acad);
        });
    }

    $scope.GetSelectedApplication_new = function (str) {
        debugger;
        $http.get(ENV.apiUrl + "api/common/UserLangController/getuserLangcode_show?comn_appl_code=" + str).then(function (res) {

            $scope.sequencedata = res.data;
            $scope.totalItems = $scope.sequencedata.length;
            $scope.todos = $scope.sequencedata;
            $scope.makeTodos();

        });
    }
    $scope.GetSelectedApplication = function (str) {
        debugger;
        if (str == undefined) {
            $scope.getgrid();
        }
        else {
            $http.get(ENV.apiUrl + "api/common/UserLangController/getuserLangcode_show?comn_appl_code=" + str).then(function (res) {
                $scope.display = false;
                $scope.sequencedata = res.data;
                $scope.totalItems = $scope.sequencedata.length;
                $scope.todos = $scope.sequencedata;
                $scope.makeTodos();
                $scope.grid = true;

                if ($scope.sequencedata.length == 0) {
                    $scope.ImageView = false;
                    $scope.tableupdate = false;
                }
                else {
                    $scope.ImageView = false;
                    $scope.tableupdate = true;
                }
                 
            });
        }
         
    }
    
    $scope.GetSelectedApplication($rootScope.current_appcode);

    $scope.GetApplication = function () {
        
         
        $http.get(ENV.apiUrl + "api/common/UserLangController/getApplication").then(function (res) {
                $scope.appl_data = res.data;
                console.log($scope.appl_data);

                $scope.edt['sims_appl_code'] = $rootScope.current_appcode;
                $scope.edt['comn_application_code'] = $rootScope.current_appcode;
                $scope.GetSelectedApplication_new($rootScope.current_appcode);
            });
         
    }


    $scope.edit = function (str) {
        debugger;
        $scope.display = true;
        $scope.grid = false;
        $scope.save1 = false;
        $scope.update1 = true;
        $scope.delete1 = false;
        $scope.edit_data = true;
        $scope.appl_data = undefined;

        // $scope.GetAacd_yr(str.comm_cur_code);

        $http.get(ENV.apiUrl + "api/common/ParameterMaster/getApplication?modCode=" + str.comn_mod_code).then(function (res) {
            debugger;
            $scope.appl_data = res.data;
            $scope.appl_name = res.data;
            console.log($scope.appl_name);

            //  $scope.GetApplication(str.comn_mod_code);

            // setTimeout(function () {
            $scope.edt =
               {
                   sims_mod_code: str.comn_mod_code,
                   sims_appl_code: str.comn_application_code,
                   sims_english: str.sims_english,
                   sims_arabic_en: str.sims_arabic_en,
                   sims_english_en: str.sims_english_en
               }
        });
        //  }, 2000);

        $http.get(ENV.apiUrl + "api/common/ApplicationMaster/getModules").then(function (res) {
            $scope.module_data = res.data;
            console.log($scope.module_data);
        });

        //$scope.GetApplication(str.comn_mod_code);
    }


    $scope.New = function () {
        debugger;
        $scope.edit_data = false;
        $scope.display = true;
        $scope.grid = false;
        $scope.save1 = true;
        $scope.update1 = false;
        $scope.delete1 = false;
        $scope.tableupdate = false;
        $scope.edt = [];
        $scope.edt.comn_parameter_ref = true;
        if ($scope.cur_data.length > 0) {
            $scope.edt.comm_cur_code = $scope.cur_data[0].sims_attendance_cur_code;
            $scope.GetAacd_yr($scope.edt.comm_cur_code);
        }
        if ($scope.acad.length > 0) {
            $scope.edt.comn_academic_year = $scope.acad[0].sims_academic_year;
        }
        $scope.GetApplication();
        $scope.edt['sims_appl_code'] = $rootScope.current_appcode;
    }

    $scope.Save = function (isvalidate) {
        debugger;
        var data1 = [];
        //$scope.edt = [];
        if (isvalidate) {
            if ($scope.update1 == false) {
                var data = ({
                    comn_application_code: $scope.edt.sims_appl_code,
                    //comn_mod_code: $scope.edt.sims_mod_code,

                    sims_english: $scope.edt.sims_english,
                    sims_english_en: $scope.edt.sims_english_en,
                    sims_arabic_en: $scope.edt.sims_arabic_en,
                    opr: 'I'
                });

                data1.push(data);

                $http.post(ENV.apiUrl + "api/common/UserLangController/userLang_insert1", data1).then(function (msg) {

                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        // $scope.edt['sims_appl_code'] = str.comn_application_code;
                        $scope.edt = [];
                        $scope.GetApplication();
                        $scope.tableupdate = true;
                        $scope.grid = true;
                        $scope.display = false;
                    }
                    else {
                        swal({ text: "Record already present. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        $scope.getgrid();
                    }


                });
            }
            else {
                $scope.Update(isvalidate);
            }
        }
    }

    $scope.Update = function (isvalidate) {
        debugger;
        var data1 = [];
        if (isvalidate) {
            var data = ({
                comn_application_code: $scope.edt.sims_appl_code,
                comn_mod_code: $scope.edt.sims_mod_code,
                sims_english: $scope.edt.sims_english,
                sims_english_en: $scope.edt.sims_english_en,
                sims_arabic_en: $scope.edt.sims_arabic_en,
                opr: 'U'

            });

            data1.push(data);

            $http.post(ENV.apiUrl + "api/common/UserLangController/userLang_Update", data1).then(function (res) {
                $scope.display = true;
                $scope.msg1 = res.data;
                if ($scope.msg1 == true) {
                    swal({ text: "Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.getgrid();
                        }
                    });
                }
                else if ($scope.msg1 == false) {
                    swal({ text: "Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.getgrid();
                        }
                    });
                }
                else {
                    swal1("Error-" + $scope.msg1)
                }
            });
        }
    }

    $scope.getgrid = function () {
        $http.get(ENV.apiUrl + "api/common/UserLangController/getuserLang_show").then(function (res) {
            debugger;
            $scope.display = false;
            $scope.grid = true;
            $scope.tableupdate = true;
            $scope.sequencedata = res.data;
            $scope.totalItems = $scope.sequencedata.length;
            $scope.todos = $scope.sequencedata;
            $scope.makeTodos();

            //for (var i = 0; i < $scope.totalItems; i++) {
            //    $scope.sequencedata[i].icon = "fa fa-plus-circle";
            //}
        });
        datasend = [];
        $scope.table = true;
        $scope.display = false;
         
    }

    $scope.cancel = function () {
        $scope.grid = true;
        $scope.display = false;
        $scope.tableupdate = true;
        // $scope.appl_data = null;
        $scope.myForm.$setPristine();
        $scope.myForm.$setUntouched();
    }

    $scope.updatetable = function () {
        debugger;
        var send = [];
        //code = [];
        //$scope.flag = false;



        $http.post(ENV.apiUrl + "api/common/UserLangController/userLangTable_Update", $scope.filteredTodos).then(function (msg) {


            $scope.msg1 = msg.data;

            if ($scope.msg1 == true) {
                swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
            }
            else if ($scope.msg1 == false) {
                swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
            }
            else {
                swal("Error-" + $scope.msg1)
            }


        });

    }

    $scope.expand = function (str)
    {
        debugger;
        $scope.edt = [];
        $scope.New();
        $scope.GetApplication();
        $scope.edt['sims_appl_code'] = str.comn_application_code;
    }

    $scope.GetApplication();

        }])

})();
//simsController.controller('UserLangController',
//      ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$location', '$anchorScroll', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $location, $anchorScroll) {

        

  
//      }])
//})();