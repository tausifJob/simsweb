﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Common');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ModuleController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$location', '$anchorScroll', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $location, $anchorScroll) {
            // Pre-Required Functions and Variables
            // Start

            $scope.lagnn = gettextCatalog.currentLanguage == "en" ? true : false;

            $scope.$on('lagnuage_chaged', function (str) {

                    $scope.lagnn = gettextCatalog.currentLanguage == "en" ? true : false;
                });


            $scope.url = ENV.siteUrl;
            $http.get(ENV.apiUrl + "api/common/getAllModulesTiles?username=" + $rootScope.globals.currentUser.username).then(function (res2) {
                $scope.module_list = res2.data;
                console.log('mod', $scope.module_list)
                setTimeout(function () {
                    for (var i = 0; i < $scope.module_list.length; i++) {
                        $scope.test1 = '#mod1' + $scope.obj2[i].module_code;
                        $scope.sd1 = $scope.module_list[i].module_color;
                        $($scope.test1).css({ 'background-color': $scope.sd1 })
                    }
                }, 1000);

            });

            $scope.changeSize = function (str) {
                $('#img' + str.module_code).css({ 'height': '50px', 'width': '50px' })
                $('#text' + str.module_code).css({ 'font-size': '15px !important' })

               
            }

            $scope.changeSize_remain = function (str) {
                $('#img' + str.module_code).css({ 'height': '40px', 'width': '40px' })
                $('#text' + str.module_code).css({ 'font-size': '13px !important' })


            }
            //Events End
        }])

})();
