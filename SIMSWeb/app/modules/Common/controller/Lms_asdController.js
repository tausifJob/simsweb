﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Common');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('Lms_asdController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$location', '$anchorScroll', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $location, $anchorScroll) {
          
         
            $http.get(ENV.apiUrl + "api/common/getUserDetails_pass?uname=" + $rootScope.globals.currentUser.username).then(function (res) {
                //  window.open($scope.schoolDetails_data.lic_lms_url + "?u=" + $rootScope.globals.currentUser.username + "&p=" + res.data.table[0].comn_user_password, "_new");
                $scope.pass = res.data.table[0].comn_user_password;
              $http.get(ENV.apiUrl + "api/common/gettype?uname=" + $rootScope.globals.currentUser.username).then(function (res) {
                  //$scope.url = $scope.schoolDetails_data.lic_lms_url + "?u=" + $rootScope.globals.currentUser.username + "&p=" + res.data.table[0].comn_user_password + '&t=t&sc=' + $http.defaults.headers.common['schoolId'];
                  $scope.uname = $rootScope.globals.currentUser.username;
                  $scope.type1 = res.data;
                  $("#form").submit();
              });

                
            });
        }])

})();
