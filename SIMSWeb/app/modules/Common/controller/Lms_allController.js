﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Common');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('Lms_allController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$location', '$anchorScroll', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $location, $anchorScroll) {
          
         
            $http.get(ENV.apiUrl + "api/common/getUserDetails_pass?uname=" + $rootScope.globals.currentUser.username).then(function (res) {
                //  window.open($scope.schoolDetails_data.lic_lms_url + "?u=" + $rootScope.globals.currentUser.username + "&p=" + res.data.table[0].comn_user_password, "_new");
                $scope.pass = res.data.table[0].comn_user_password;
              $http.get(ENV.apiUrl + "api/common/gettype?uname=" + $rootScope.globals.currentUser.username).then(function (res) {
                  $scope.rurl = $scope.schoolDetails_data.lic_lms_url + "?u=" + $rootScope.globals.currentUser.username + "&p=" + $scope.pass;
                  console.log('url', $scope.rurl)
                  console.log('type', $scope.type1)

                 document.form.action = $scope.rurl;
                  $scope.uname = $rootScope.globals.currentUser.username;
                  $scope.type1 = res.data;
                  $scope.sc_id = $http.defaults.headers.common['schoolId'];
                  $("#form").submit();

              });

                
            });
        }])

})();
