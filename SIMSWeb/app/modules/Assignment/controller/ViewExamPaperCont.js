﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var file_doc = [];
    var file_temp_doc = [];
    var sims_doc_line = 1;
    var simsController = angular.module('sims.module.Assignment');
    simsController.controller('ViewExamPaperCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Update_btn = true;
            $scope.search_btn = false;
            $scope.display = true;
            $scope.table = true;
            $scope.exampaper = false;
            $scope.uploading_doc1 = true;
            $scope.del_btn = false;
            $scope.temp = {};
            var no;
            var srl;
            $scope.temp.sims_exam_paper_status = true;
            $scope.grid1 = false;
            $scope.grid2 = false;
            $scope.grid3 = false;
            $scope.inactive_btn = false;
            $scope.del_btn = false;
            //$scope.username = $rootScope.globals.currentUser.username;
            var user = $rootScope.globals.currentUser.username;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $(function () {
                $('#cmb_Section').multipleSelect({
                    width: '100%'
                });
            });
            $(function () {
                $('#cmb_Section2').multipleSelect({
                    width: '100%'
                });
            });

            $scope.temp.sims_grade_code = '11';
            $scope.temp.sims_section_code = '0001';
           // $scope.temp.sims_exam_code = '';
            

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCurriculum) {
                $scope.Curriculum = getCurriculum.data;
                $scope.temp.sims_cur_code = $scope.Curriculum[0].sims_cur_code
                $scope.getacademicYear($scope.Curriculum[0].sims_cur_code);
            });

            $scope.getacademicYear = function (str) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (getAcademicYear) {
                    $scope.AcademicYear = getAcademicYear.data;

                    $scope.temp.sims_academic_year = $scope.AcademicYear[0].sims_academic_year                    
                    $scope.getsubject();
                    $scope.getExam(str, $scope.temp.sims_academic_year);
                    //$scope.getGrade($scope.Curriculum[0].sims_cur_code, $scope.AcademicYear[0].sims_academic_year);                   
                })
            }

            $scope.getacademicYear($scope.temp.sims_cur_code);
            

            $scope.getExam = function (str,str2) {
                $http.get(ENV.apiUrl + "api/AddExamPaper/getExamCode?cur=" + str + "&year=" + str2).then(function (AllExam) {
                    $scope.Exam = AllExam.data;                    
                });
            }
               

            //$scope.getGrade = function (str1, str2) {
            //    debugger
            //    $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str1 + "&academic_year=" + str2).then(function (getAllGrades) {
            //        debugger
            //        $scope.AllGrades = getAllGrades.data;
            //        $http.get(ENV.apiUrl + "api/AddExamPaper/getExamCode?cur=" + str1 + "&year=" + str2).then(function (AllExam) {
            //            $scope.Exam = AllExam.data;
            //            console.log($scope.Exam);
            //        });
            //    })

            //}

            //$scope.getsection = function (str, str1, str2) {
            //    debugger
            //    $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + str + "&grade_code=" + str1 + "&academic_year=" + str2).then(function (Allsection) {
            //        $scope.getSectionFromGrade = Allsection.data;
            //        setTimeout(function () {
            //            debugger;
            //            $('#cmb_Section').change(function () {
            //                console.log($(this).val());
            //            }).multipleSelect({
            //                width: '100%'
            //            });
            //        }, 1000);

            //        setTimeout(function () {
            //            debugger;
            //            $('#cmb_Section2').change(function () {
            //                console.log($(this).val());
            //            }).multipleSelect({
            //                width: '100%'
            //            });
            //        }, 1000);
            //    })
            //};

            $scope.getsubject = function () {

                $http.get(ENV.apiUrl + "api/AddExamPaper/getSubject?cur=" + $scope.temp.sims_cur_code + "&aca=" + $scope.temp.sims_academic_year + "&grd=" + $scope.temp.sims_grade_code + "&section=" + $scope.temp.sims_section_code).then(function (getSubject) {
                    debugger;
                    $scope.subjectdata = getSubject.data;                   
                });

            }
            

            $scope.getslr = function () {
                $http.get(ENV.apiUrl + "api/AddExamPaper/getOldPapers?cur=" + $scope.temp.sims_cur_code + "&aca=" + $scope.temp.sims_academic_year + "&grd=" + $scope.temp.sims_grade_code + "&sec=" + $scope.temp.sims_section_code + "&exam=" + $scope.temp.sims_exam_code).then(function (AllPapers) {
                    debugger
                    $scope.file_attached = AllPapers.data;

                });
            }

            $scope.reset = function () {
                $scope.temp = {};
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                file_temp_doc = []
                $scope.file_doc = "";
                $scope.inactive_btn = false;
                $scope.del_btn = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $state.go($state.current, {}, { reload: true });
            }

            $scope.downloaddoc1 = function (str) {
                debugger;
                //  $scope.url = "http://localhost:90/SIMSAPI/Content/Assignment/TeacherDoc/" + str;
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/ExamPapers/' + str;
                window.open($scope.url);
            }

            $scope.downloaddoc2 = function (str) {
                debugger;
                //  $scope.url = "http://localhost:90/SIMSAPI/Content/Assignment/TeacherDoc/" + str;
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/ExamPapers/' + str;
                window.open($scope.url);
            }

            $scope.search = function () {
                debugger               
                    var section_code = [];
                    $scope.exampaper = true;
                    $scope.del_btn = true;
                    $scope.grid1 = false;
                    $scope.grid2 = false;
                    $scope.grid3 = true;
                    $scope.inactive_btn = true;
                    $scope.del_btn = true;
                    var j = 1;
                    var section = $scope.temp.sims_section_code;
                    section_code = section_code + ',' + section;
                    var sec = section_code.substr(section_code.indexOf(',') + 1);
                    $http.get(ENV.apiUrl + "api/AddExamPaper/getAllPapers?cur=" + $scope.temp.sims_cur_code + "&aca=" + $scope.temp.sims_academic_year + "&grd=" + $scope.temp.sims_grade_code + "&sec=" + sec + "&exam=" + $scope.temp.sims_exam_code + "&sub=" + $scope.temp.sims_subject_code).then(function (AllPapers) {
                        $scope.papers = AllPapers.data;
                        //for (var i = 0; $scope.papers.length; i++) {
                        //}
                        if ($scope.papers.length == 0) {
                            swal('', 'No Records Found');
                        }                        
                    });
                
            }

            $scope.search();
            
        }])

})();


