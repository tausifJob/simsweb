﻿(function () {
    'use strict';
    var selected_enroll_number = [];
    var opr = '';
    var main;
    var comn_files = [];
    var datasend = [];
    var file_doc = [];
    var file_temp_doc = [];
    var sims_doc_line = 1;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TeacherAssignmentUploadCont', ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
        $scope.display = false;
        $scope.grid = true;
        $scope.pagesize = "10";
        $scope.pageindex = "1";
        var arr_files = [];
        var comp_code = "1"

        var arr_files1 = [];
        $scope.uploading_doc1 = true;
        $scope.images = [];
        $scope.images1 = [];
        $scope.StudentsList_Data = [];
        $scope.student = [];
        $scope.edt = {}
        var comn_files = "";
        $scope.obj = [];
        $scope.enrollno = true;
        var formdata = new FormData();
     
        var user = $rootScope.globals.currentUser.username;
        //$scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
        //var comp_code = $scope.finnDetail.company;
        //var finance_year = $scope.finnDetail.year;

        $timeout(function () {
            $("#fixTable").tableHeadFixer({
                'top': 1
            });
        }, 100);

        $timeout(function () {
            $("#fixTable1").tableHeadFixer({
                'top': 1
            });

        }, 100);

        $http.get(ENV.apiUrl + "api/AssignmentUpload/GetTextFormat").then(function (GetTextFormat) {
            $scope.GetTextFormat = GetTextFormat.data;
            for (var i = 0; i < $scope.GetTextFormat.length; i++) {
                $scope.fileformats = $scope.fileformats + '.' + $scope.GetTextFormat[i].file_format + ',';
            }
        });

        $http.get(ENV.apiUrl + "api/AssignmentUpload/getAssignmentDetail?User_Code=" + user).then(function (getAssignmentDetail_Data) {
            
            $scope.AssignmentDetailData = getAssignmentDetail_Data.data;

            $scope.totalItems = $scope.AssignmentDetailData.length;
            $scope.todos = $scope.AssignmentDetailData;
            $scope.makeTodos();
            
        });

        $http.get(ENV.apiUrl + "api/AssignmentUpload/getAssignmentUpload?User_Code=" + user).then(function (res) {
            if (res.data !== null) {
                $scope.obj = res.data;

            } else {
                var Message = {
                    strMessage: "Sorry Data Not Found"
                }
                $scope.msg1 = Message;
                $('#message').modal('show');
            }
        });

        $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

        $scope.makeTodos = function () {
            var rem = parseInt($scope.totalItems % $scope.numPerPage);
            if (rem == '0') {
                $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
            } else {
                $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
            }
            var begin = (($scope.currentPage - 1) * $scope.numPerPage);
            var end = parseInt(begin) + parseInt($scope.numPerPage);

           

            $scope.filteredTodos = $scope.todos.slice(begin, end);
        };

        $http.get(ENV.apiUrl + "api/common/getAllTerms").then(function (getAllTerms_Data) {

            $scope.AllTerms_Data = getAllTerms_Data.data;
           

        });

       // $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
       //     $scope.curriculum = res.data;
       // })

        function getCur(flag, comp_code) {
            if (flag) {

                $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                    $scope.curriculum = res.data;
                    $scope.edt["sims_cur_code"] = $scope.curriculum[0].sims_cur_code;
                    $scope.getacyr($scope.curriculum[0].sims_cur_code);
                   
                });
            }
            else {

                $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                    $scope.curriculum = res.data;
                    $scope.edt["sims_cur_code"] = $scope.curriculum[0].sims_cur_code;
                    $scope.getacyr($scope.curriculum[0].sims_cur_code);
                });


            }

        }

        $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
            $scope.global_count_comp = res.data;

            if ($scope.global_count_comp) {
                getCur(true, $scope.user_details.comp);


            }
            else {
                getCur(false, $scope.user_details.comp)
            }
        });




        $scope.getacyr = function (str) {

            $scope.cur_code = str;
            $http.get(ENV.apiUrl + "api/AssignmentUpload/GetAcademicYear").then(function (GetAcademicYear) {
                $scope.Academic_year = GetAcademicYear.data;
                
                $scope.edt['sims_academic_year'] = $scope.Academic_year[0].sims_academic_year;
                //////////////////////////

                //   $scope.edt['sims_assignment_freeze_date'] = $scope.Academic_year[0].sims_academic_year_end_date;
                $scope.edt['sims_assignment_status'] = true;
                $scope.getGrade(user, $scope.Academic_year[0].sims_academic_year, $scope.edt.sims_cur_code)
            })

        }

        $scope.getGrade = function (username, acadyear, curcode) {

            $http.get(ENV.apiUrl + "api/AssignmentUpload/getGradeByUsername?username=" + user + "&acadyear=" + $scope.edt.sims_academic_year + "&curcode=" + $scope.edt.sims_cur_code).then(function (getGradeByUsername_Data) {
                
                $scope.GradeByUsername = getGradeByUsername_Data.data;
                setTimeout(function () {
                    $('#cmb_grade').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
               
            });
        }
        var sims_grade_code = '';
        var sims_section_code = '';
        $scope.select_grd_mult = function () {
           
            sims_grade_code = '';
            for (var a = 0; a < $scope.edt.sims_grade_code.length; a++) {
                sims_grade_code = sims_grade_code + $scope.edt.sims_grade_code[a] + ',';
            }

            $http.get(ENV.apiUrl + "api/AssignmentUpload/getSectionByUserGrade?grade=" + sims_grade_code + "&username=" + user + "&acadyear=" + $scope.edt.sims_academic_year + "&curcode=" + $scope.edt.sims_cur_code)
				.then(function (getSectionByUserGrade) {
				    $scope.getSectionByUserGrade = getSectionByUserGrade.data;
				    setTimeout(function () {
				        $('#cmb_section').change(function () { }).multipleSelect({
				            width: '100%'
				        });
				        $("#cmb_section").multipleSelect("checkAll");
				    }, 1000);

				});

        }

        $scope.select_sec_mult = function () {
            var seccoll = [];
            sims_section_code = '';
            for (var a = 0; a < $scope.edt.sims_section_code.length; a++) {

                sims_section_code = sims_section_code + $scope.edt.sims_section_code[a] + ',';
            }

            $http.get(ENV.apiUrl + "api/AssignmentUpload/get_subject_code?teacher=" + user + "&section=" + sims_section_code + "&year=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code).then(function (getSubjectByUseType) {
                $scope.getSubjectByUseType = getSubjectByUseType.data;

            });

        }

        $(function () {
            $('#cmb_grade').multipleSelect({
                width: '100%'
            });
            $('#cmb_section').multipleSelect({
                width: '100%'
            });
        });

        //$scope.getSection = function (curcode, acadyear, grade, username) {
        //    
        //    $http.get(ENV.apiUrl + "api/AssignmentUpload/getSectionByUserGrade?grade=" + $scope.edt.sims_grade_code + "&username=" + user + "&acadyear=" + $scope.edt.sims_academic_year + "&curcode=" + $scope.edt.sims_cur_code).then(function (getSectionByUserGrade_Data) {
        //        $scope.SectionByUserGrade = getSectionByUserGrade_Data.data;
        //        
        //    });
        //}

        //$scope.getSubject = function (section, teacher, year, grade) {
        //    debugger
        //    $http.get(ENV.apiUrl + "api/AssignmentUpload/get_subject_code?teacher=" + user + "&section=" + $scope.edt.sims_section_code + "&year=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code).then(function (getSubjectByUseType) {
        //        $scope.SubjectByUser = getSubjectByUseType.data;
        //       
        //    });
        //}

        $scope.edit = function (str) {
           
            $scope.display = true;
            $scope.grid = false;
            $scope.save1 = false;
            $scope.update1 = true;
            $scope.delete1 = false;
            $scope.edt = str;

            $scope.enrollno = false;
            $scope.ClearCheckBox();
            $scope.student = [];
        }

        $scope.New = function () {
            
            $scope.display = true;
            $scope.grid = false;
            $scope.save1 = true;
            $scope.update1 = false;
            $scope.delete1 = false;
            $scope.edt = [];
            $scope.edt['sims_assignment_status'] = true;
            $scope.enrollno = true;
            // $scope.ClearCheckBox();
            $scope.MyForm.$setPristine();
            $scope.MyForm.$setUntouched();
            $scope.empty = document.getElementById("file1").value = "";


            if ($scope.global_count_comp) {
                getCur(true, $scope.user_details.comp);


            }
            else {
                getCur(false, $scope.user_details.comp)
            }

            //$scope.edt = {
            //    sims_cur_code: $scope.curriculum[0].sims_cur_code
            //}
            //$scope.getacyr($scope.curriculum[0].sims_cur_code);

            var date = new Date();
            var month = (date.getMonth() + 1);
            var day = date.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            
            //$scope.edt.sims_assignment_start_date = date.getFullYear() + '-' + (month) + '-' + (day);
            //$scope.edt.sims_assignment_submission_date = date.getFullYear() + '-' + (month) + '-' + (day);
            $scope.edt.sims_assignment_start_date = (day) + '-' + (month) + '-' + date.getFullYear();
            $scope.edt.sims_assignment_submission_date = (day) + '-' + (month) + '-' + date.getFullYear();

        }

        $scope.CheckAllChecked = function () {
            main = document.getElementById('mainchk');
            $scope.sims_grade_name = [];
            if (main.checked == true) {
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_grade_name;
                    
                    var v = document.getElementById(t);
                    v.checked = true;
                    sims_grade_name1 = sims_grade_name1 + $scope.filteredTodos[i].sims_grade_name + ',';
                    $scope.row1 = 'row_selected';
                }
            } else {
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_grade_name;
                    var v = document.getElementById(t);
                    v.checked = false;
                    main.checked == false;
                    $scope.row1 = '';
                    sims_grade_name1 = [];
                }
            }
        }

        $scope.checkonebyonedelete = function () {

            main = document.getElementById('mainchk');

            if (main.checked == true) {
                main.checked = false;
                $scope.row1 = '';
            } else {
                main.checked = false;
                $scope.row1 = '';
            }

        }

        $scope.cancel = function () {
            
            $scope.display = false;
            $scope.grid = true;
            $scope.MyForm.$setPristine();
            $scope.MyForm.$setUntouched();
            $scope.file_doc = [];
            $scope.student = [];

        }

        // var formdata = new FormData();

        //$scope.file_changed = function (element) {
        //    if ($scope.images.length > 5) {
        //        alert("No more Uploads.");
        //    }
        //};

        var formdata = new FormData();
        $scope.getTheFiles = function ($files) {
            
            angular.forEach($files, function (value, key) {
                formdata.append(key, value);
            });
        };
        var file_name = '';
        var file_name_name = '';
        $scope.file_doc = [];

        $scope.file_changed = function (element) {
            debugger
            file_name = '';
            file_name_name = '';
            var v = new Date();
            if ($scope.file_doc == undefined) {
                $scope.file_doc = '';
            }

            if ($scope.file_doc.length > 4) {
                swal('', 'Upload maximum 5 files.');

            } else {

                file_name = 'Assignment_' + v.getDay() + '_' + v.getMonth() + '_' + v.getYear() + '_' + v.getHours() + '_' + v.getMonth() + '_' + v.getSeconds();
                $scope.photofile = element.files[0];
                file_name_name = $scope.photofile.name;

                //file_name_name = file_name_name.replace(/[^A-Z0-9]/ig, "_");
                var len = 0;
                len = file_name_name.split('.');
                var forName = file_name_name.split('.')[0] + '_sys' +  v.getHours() + v.getSeconds();
                var fortype = file_name_name.split('.')[len.length - 1];
                var formatbool = false;
                for (var i = 0; i < $scope.GetTextFormat.length; i++) {
                    if ($scope.GetTextFormat[i].file_format == fortype)
                        formatbool = true;
                }
                if (formatbool == true) {
                    if ($scope.photofile.size > 2097152) {
                        swal('', 'File size limit not exceed upto 2 MB.')
                    } else {
                        $scope.uploading_doc1 = false;
                        $scope.photo_filename = ($scope.photofile.type);
                        

                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $scope.$apply(function () {
                                $scope.prev_img = e.target.result;
                                var request = {
                                    method: 'POST',
                                    url: ENV.apiUrl + 'api/AssignmentUpload/upload?filename=' + forName  + '.' + fortype + "&location=" + "Images/StudentImage",
                                    data: formdata,
                                    headers: {
                                        'Content-Type': undefined
                                    }
                                }; 
                                $http(request).success(function (d) {

                                    
                                    var t = {
                                        sims508_doc: d,
                                        sims_doc_line: sims_doc_line,
                                        sims508_doc_name: d //file_name_name,
                                    }
                                    $scope.file_doc.push(t);
                                    sims_doc_line++;
                                    $scope.uploading_doc1 = true;
                                    //file_doc = [];
                                    //for (var i = 0; i < $scope.teacher_doc.length; i++) {
                                    //    var c = {
                                    //        sims509_doc: $scope.teacher_doc[i].sims508_doc,
                                    //        sims_doc_line: i + 1
                                    //    }
                                    //    file_doc.push(c);
                                    //}

                                    //$scope.maindata.file = d;

                                });

                            });
                        };
                        reader.readAsDataURL($scope.photofile);
                    }
                } else {
                    swal('', '.' + fortype + ' File format not allowed.');
                }
            }

        }

        $scope.downloaddoc1 = function (str) {
            
            //  $scope.url = "http://localhost:90/SIMSAPI/Content/Assignment/TeacherDoc/" + str;
            $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Assignment/TeacherDoc/' + str;
            window.open($scope.url);
        }

        $scope.CancelFileUpload = function (idx) {
            $scope.images.splice(idx, 1);
           
        };

        var sims_student_enrolls = [];
        $scope.saveUploadedFiles = function (isvalid) {
            if ($scope.edt.sims_assignment_title == undefined || $scope.edt.sims_assignment_title == "") {
                $scope.flag1 = true;
                swal({
                    title: "Alert",
                    text: "Please Enter Assignment Title ",
                    showCloseButton: true,
                    width: 380,
                });

            } else if ($scope.edt.sims_assignment_start_date == undefined || $scope.edt.sims_assignment_start_date == "") {
                $scope.flag1 = true;
                swal({
                    title: "Alert",
                    text: "Please select Start Date",
                    showCloseButton: true,
                    width: 380,
                });
            } else if ($scope.edt.sims_assignment_submission_date == undefined || $scope.edt.sims_assignment_submission_date == "") {
                $scope.flag1 = true;
                swal({
                    title: "Alert",
                    text: "Please select Submission Date",
                    showCloseButton: true,
                    width: 380,
                });
            } else if ($scope.student.length == undefined || $scope.student.length == "") {
                $scope.flag1 = true;
                swal({
                    title: "Alert",
                    text: "Please select Atleast One Student",
                    showCloseButton: true,
                    width: 380,
                });
            } else if ($scope.file_doc.length == undefined || $scope.file_doc.length == "") {
                $scope.flag1 = true;
                swal({
                    title: "Alert",
                    text: "Please select Atleast One File",
                    showCloseButton: true,
                    width: 380,
                });
            } else {
                
                var ass_send = [];
                var doc_send = [];
                var stud_send = [];
                if (isvalid) {

                    $('#loader').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    
                    sims_section_code = '';
                    for (var i = 0; i < $scope.edt.sims_section_code.length; i++) {

                        var data = {
                            sims_academic_year: $scope.edt.sims_academic_year,
                            sims_cur_code: $scope.edt.sims_cur_code,
                            sims_grade_code: $scope.edt.sims_grade_code,
                            sims_section_code: $scope.edt.sims_section_code[i],
                            sims_subject_Code: $scope.edt.sims_subject_code,
                            sims_bell_teacher_code: $rootScope.globals.currentUser.username,
                            sims_assignment_title: $scope.edt.sims_assignment_title,
                            sims_assignment_desc: $scope.edt.sims_assignment_desc,
                            sims_assignment_start_date: $scope.edt.sims_assignment_start_date,
                            sims_assignment_submission_date: $scope.edt.sims_assignment_submission_date,
                            sims_assignment_freeze_date: $scope.edt.sims_assignment_freeze_date,
                            sims_assignment_status: $scope.edt.sims_assignment_status,
                            sims_term_code: $scope.edt.sims_term_code,

                        }
                        ass_send.push(data);
                    }

                    var lst_data = [];
                    lst_data.push(ass_send);
                    lst_data.push(doc_send);
                    lst_data.push(stud_send);

                    if ($scope.file_doc.length > 0) {

                        for (var j = 0; j < $scope.file_doc.length; j++) {
                            var data = {
                                sims_cur_code: $scope.edt.sims_cur_code,
                                sims_academic_year: $scope.edt.sims_academic_year,
                                sims_grade_code: $scope.edt.sims_grade_code,
                                sims_section_code: $scope.edt.sims_section_code,
                                sims_assignment_doc_line_no: $scope.file_doc[j].sims_doc_line,
                                sims_assignment_doc_path: $scope.file_doc[j].sims508_doc,
                                sims_assignment_doc_path_name: $scope.file_doc[j].sims508_doc_name,
                            }

                            doc_send.push(data);

                        }
                    }
                    for (var i = 0; i < $scope.student.length; i++) {
                        stud_send.push($scope.student[i]);
                    }

                    $http.post(ENV.apiUrl + "api/AssignmentUpload/insert_Assignment", lst_data).then(function (res) {
                        $scope.AssignmentData = res.data;
                        
                        swal('', 'Assignment Created successfully');
                        $scope.Clear();
                        $scope.getgridData();

                        $('#loader').modal('hide');
                        $(".modal-backdrop").removeClass("modal-backdrop");
                    });

                }

            }
        }

        $scope.InsertAssignmentStudent = function () {
            
            var dataSend = [];
            for (var i = 0; i < $scope.student.length; i++) {

                
                var data = {

                    sims_cur_code: $scope.edt.sims_cur_code,
                    sims_academic_year: $scope.edt.sims_academic_year,
                    sims_grade_code: $scope.edt.sims_grade_code,
                    sims_section_code: $scope.edt.sims_section_code,
                    sims_student_enroll_number: $scope.student[i].sims_student_enroll_number,

                }

                datasend.push(data);

            }

            $http.post(ENV.apiUrl + "api/AssignmentUpload/insert_Asignment_student", datasend).then(function (res) {
                $scope.AssignmentStudentData = res.data;
                if ($scope.AssignmentStudentData == true) {
                    swal('', 'Assignment Created successfully');
                    $scope.Clear();
                    $scope.getgridData();
                } else {
                    swal('', 'Insert Assignment Student not Created. ');
                }
            });
            datasend = [];
        }

        $scope.Insert_Sims_assignment_doc = function () {
            
            var dataSend = [];

            if ($scope.file_doc.length > 0) {

                for (var j = 0; j < $scope.file_doc.length; j++) {
                    var data = {
                        sims_cur_code: $scope.edt.sims_cur_code,
                        sims_academic_year: $scope.edt.sims_academic_year,
                        sims_grade_code: $scope.edt.sims_grade_code,
                        sims_section_code: $scope.edt.sims_section_code,
                        sims_assignment_doc_line_no: $scope.file_doc[j].sims_doc_line,
                        sims_assignment_doc_path: $scope.file_doc[j].sims508_doc,
                        sims_assignment_doc_path_name: $scope.file_doc[j].sims508_doc_name,

                    }

                    datasend.push(data);

                }

                $http.post(ENV.apiUrl + "api/AssignmentUpload/insert_Assignment_Doc", datasend).then(function (res) {
                    $scope.AssignmentDocData = res.data;
                    if ($scope.AssignmentDocData == true) {
                        $scope.InsertAssignmentStudent();
                    } else {
                        swal('', 'Assignment Not Created');
                    }
                });

            }
        }

        $scope.Clear = function () {
            $scope.edt = [];
            $scope.file_doc = [];
            $scope.student = [];
            $scope.enrollno = false;

        }

        $scope.getgridData = function () {
            $http.get(ENV.apiUrl + "api/AssignmentUpload/getAssignmentDetail?User_Code=" + user).then(function (getAssignmentDetail_Data) {
                
                $scope.AssignmentDetailData = getAssignmentDetail_Data.data;
                $scope.totalItems = $scope.AssignmentDetailData.length;
                $scope.todos = $scope.AssignmentDetailData;
                $scope.makeTodos();
               
                $scope.grid = true;
                $scope.display = false;

            });
        }

        $scope.size = function (str) {
            /*
			$scope.pagesize = str;
			$scope.currentPage = 1;
			$scope.numPerPage = str; */
            main = document.getElementById('mainchk');
            if (main.checked == true) {
                main.checked = false;
                $scope.row1 = '';
                $scope.color = '#edefef';
            }

            if (str == "All") {
                $scope.currentPage = 1;
                $scope.numPerPage = $scope.AssignmentDetailData.length;
                $scope.makeTodos();

            } else {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }
        }

        $scope.index = function (str) {
            $scope.pageindex = str;
            $scope.currentPage = str;
            
            $scope.makeTodos();

            main = document.getElementById('mainchk');
            if (main.checked == true) {
                main.checked = false;
                $scope.row1 = '';
                $scope.color = '#edefef';
            }
        }

        $scope.Update = function () {

            $scope.enroll_number = [];

            var enroll_no = '';

            for (var i = 0; i < $scope.obj.length; i++) {

                if ($scope.obj[i].sims_student_passport_first_name_en == $scope.edt.sims_student_passport_first_name_en) {

                    enroll_no = $scope.obj[i].sims_batch_enroll_number;
                }
            }

            var data = {
                sims_cur_name: $scope.edt.sims_cur_name,
                sims_academic_year: $scope.edt.sims_academic_year,
                sims_batch_enroll_number: enroll_no,
                sims_batch_name: $scope.edt.sims_batch_code,
                sims_batch_status: $scope.edt.sims_batch_status,
                sims_homeroom_code: $scope.edt.sims_homeroom_code,
                opr: 'U'
            }

            $http.post(ENV.apiUrl + "api/homeroom/UHomeroomBatchStudent", data).then(function (MSG1) {

                $scope.msg = MSG1.data;

                $scope.grid = true;
                $scope.display = false;
                if ($scope.msg == true) {

                    swal('', 'Information Updated Successfully');
                    $scope.checkonebyonedelete();

                } else if ($scope.msg == false) {

                    swal('', 'Information Not Updated');
                } else {

                    swal('', 'Error - ' + $scope.msg);

                }

                $http.get(ENV.apiUrl + "api/homeroom/getHomeroomBatchStudent").then(function (res) {
                    if (res.data !== null) {
                        $scope.obj = res.data;
                        $scope.totalItems = $scope.obj.length;
                        $scope.todos = $scope.obj;
                        $scope.makeTodos();
                    } else {
                        swal('', 'Sorry Data Not Found');
                    }
                })
            });
        }

        $scope.SearchStudentWindow = function () {
            if ($scope.edt.sims_cur_code == undefined || $scope.edt.sims_cur_code == "") {
                $scope.flag1 = true;
                swal({
                    title: "Alert",
                    text: "Please select curriculum",
                    showCloseButton: true,
                    width: 380,
                });

            } else if ($scope.edt.sims_academic_year == undefined || $scope.edt.sims_academic_year == "") {
                $scope.flag1 = true;
                swal({
                    title: "Alert",
                    text: "Please select curriculum Year",
                    showCloseButton: true,
                    width: 380,
                });
            } else if ($scope.edt.sims_grade_code == undefined || $scope.edt.sims_grade_code == "") {
                $scope.flag1 = true;
                swal({
                    title: "Alert",
                    text: "Please select Grade",
                    showCloseButton: true,
                    width: 380,
                });
            } else if ($scope.edt.sims_section_code == undefined || $scope.edt.sims_section_code == "") {
                $scope.flag1 = true;
                swal({
                    title: "Alert",
                    text: "Please select Section",
                    showCloseButton: true,
                    width: 380,
                });
            } else if ($scope.edt.sims_subject_code == undefined || $scope.edt.sims_subject_code == "") {
                $scope.flag1 = true;
                swal({
                    title: "Alert",
                    text: "Please select Subject",
                    showCloseButton: true,
                    width: 380,
                });
            } else if ($scope.edt.sims_term_code == undefined || $scope.edt.sims_term_code == "") {
                $scope.flag1 = true;
                swal({
                    title: "Alert",
                    text: "Please select Term",
                    showCloseButton: true,
                    width: 380,
                });
            } else {
                $scope.searchtable = false;
                
                sims_section_code = '';
                for (var a = 0; a < $scope.edt.sims_section_code.length; a++) {

                    sims_section_code = sims_section_code + $scope.edt.sims_section_code[a] + ',';
                }

                $http.get(ENV.apiUrl + "api/AssignmentUpload/getStudentsList?Grade_Code=" + $scope.edt.sims_grade_code + "&Section_Code=" + sims_section_code + "&Sub_Code=" + $scope.edt.sims_subject_code + "&year=" + $scope.edt.sims_academic_year + "&Cur_Code=" + $scope.edt.sims_cur_code).then(function (getStudentsList_Data) {
                    $scope.StudentsList_Data = getStudentsList_Data.data;
                    
                    $scope.searchtable = true;
                    $scope.busy = false;
                    $('#MyModal').modal('show');

                    $scope.chk_all = true;
                    $scope.MultipleStudentSelect($scope.chk_all);

                    $('#MyModal').draggable();
                });
            }
        }

        $scope.searched = function (valLists, toSearch) {

            

            return _.filter(valLists,

				function (i) {
				    /* Search Text in all  fields */
				    return searchUtil(i, toSearch);
				});
        };

        $scope.search = function () {
            $scope.todos = $scope.searched($scope.AssignmentDetailData, $scope.searchText);
            $scope.totalItems = $scope.todos.length;
            $scope.currentPage = '1';
            if ($scope.searchText == '') {
                $scope.todos = $scope.AssignmentDetailData;
            }
            $scope.makeTodos();
        }

        function searchUtil(item, toSearch) {
            /* Search Text in all 3 fields */

            return (
				item.sims_assignment_title.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
				item.sims_subject_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
				item.sims_grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
				item.sims_section_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
				item.sims_grade_name == toSearch) ? true : false;
        }

        $scope.Reset = function () {
            $scope.temp = '';
        }

        $scope.ClearCheckBox = function () {

            main = document.getElementById('mainchk');

            if (main.checked == true) {
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_student_enrolls;
                    var v = document.getElementById(t);
                    v.checked = false;
                    selected_enroll_number = [];
                    $scope.row1 = '';

                }
                main.checked = false;
            }

        }

        $scope.RemoveEnrollMentNo = function ($event, index, str) {

            
            str.splice(index, 1);

        }

        $scope.MultipleStudentSelect = function (str) {
            if (str) {
                for (var i = 0; i < $scope.StudentsList_Data.length; i++) {
                    $scope.StudentsList_Data[i]['stud_chk'] = true;
                }
            } else {
                for (var i = 0; i < $scope.StudentsList_Data.length; i++) {
                    $scope.StudentsList_Data[i]['stud_chk'] = false;
                    $scope.row1 = '';
                }
            }
        }

        $scope.student = [];
        var data1 = [];
        $scope.DataEnroll = function () {
            for (var i = 0; i < $scope.StudentsList_Data.length; i++) {
                var t = $scope.StudentsList_Data[i].sims_student_enroll_number;
                var v = document.getElementById(t + i);
                if (v.checked == true) {
                    data1 = $scope.StudentsList_Data[i];
                    $scope.student.push(data1);
                }
            }
        }

        $('*[data-datepicker="true"] input[type="text"]').datepicker({

            todayBtn: true,
            orientation: "top left",
            autoclose: true,
            todayHighlight: true,
            format: "dd-mm-yyyy"
        });

        $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
            $('input[type="text"]', $(this).parent()).focus();
        });

        //$scope.createdate = function (sims_assignment_freeze_date) {
        //    var month1 = sims_assignment_freeze_date.split("-")[0];
        //    var day1 = sims_assignment_freeze_date.split("-")[1];
        //    var year1 = sims_assignment_freeze_date.split("-")[2];
        //    var freeze_date = year1 + "-" + month1 + "-" + day1;
        //}

        //$scope.createdate = function (end_date, start_date, name) {

        //    var month1 = end_date.split("-")[0];
        //    var day1 = end_date.split("-")[1];
        //    var year1 = end_date.split("-")[2];
        //    var new_end_date = year1 + "-" + month1 + "-" + day1;

        //    var year = start_date.split("-")[0];
        //    var month = start_date.split("-")[1];
        //    var day = start_date.split("-")[2];
        //    var new_start_date = year + "-" + month + "-" + day;

        //    if (new_end_date < new_start_date) {
        //        swal('', 'Please Select Future Date');
        //        //$rootScope.strMessage = "Please Select Future Date";
        //        //$('#message').modal('show');
        //        $scope.edt[name] = '';
        //    }
        //    else {

        //        $scope.edt[name] = new_end_date;
        //    }
        //}

        //$scope.showdate = function (date, name) {
        //    var month = date.split("-")[0];
        //    var day = date.split("-")[1];
        //    var year = date.split("-")[2];
        //    $scope.edt[name] = year + "-" + month + "-" + day;
        //}

        simsController.directive('ngFiles', ['$parse', function ($parse) {

            function fn_link(scope, element, attrs) {
                var onChange = $parse(attrs.ngFiles);
                element.on('change', function (event) {
                    onChange(scope, {
                        $files: event.target.files
                    });
                });
            };

            return {
                link: fn_link
            }
        }])

        $scope.deletedoc = function (str) {
            
            file_temp_doc = [];
            //for (var i = 0; i < $scope.file_doc.length; i++) {
            //    if ($scope.file_doc[i].sims_doc_line == str.sims_doc_line && $scope.file_doc[i].sims508_doc == str.sims508_doc) {
            //        $scope.file_doc.splice(i, 1);
            //        sims_doc_line--;
            //        sims_doc_line: i--;
            //    }
            //}

            for (var i = 0; i < $scope.file_doc.length; i++) {
                if ($scope.file_doc[i].sims_doc_line != str.sims_doc_line) {
                    var x = {
                        sims508_doc: $scope.file_doc[i].sims508_doc,
                        sims508_doc_name: $scope.file_doc[i].sims508_doc_name,
                    }
                    file_temp_doc.push(x);
                }
            }
            $scope.file_doc = [];
           
            var j = 1;
            for (var i = 0; i < file_temp_doc.length; i++) {
                var c = {
                    sims508_doc: file_temp_doc[i].sims508_doc,
                    sims508_doc_name: $scope.file_doc[i].sims508_doc_name,
                    sims_doc_line: i + 1
                }
                $scope.file_doc.push(c);
                //sims_doc_line = i + 1;
                j++;
            }
            sims_doc_line = j;
           

        }

        $timeout(function () {
            $("#fixTable2").tableHeadFixer({
                'top': 1
            });
            $("#fixTable1").tableHeadFixer({
                'top': 1
            });
        }, 100);

    }])
})();