﻿(function () {
    'use strict';
    var demo = [];
    var assign_no;
    var data1 = [];
    var data = [];
    var comn_files = [];
    var main;
    var mainchk;
    var Sub_Code;
    var sims_assignment_enroll_number;
    // var enroll_number = [];
    var simsController = angular.module('sims.module.Assignment');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TeacherAssignmentViewCont', ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
        $scope.pagesize = '10';
        $scope.busyindicator = true;

        var username = $rootScope.globals.currentUser.username;
        var formdata = new FormData();
        $scope.username = username;
        var arr_files = [];
        var arr_files1 = [];
        $scope.images = [];
        $scope.images1 = [];
        $scope.searchtable = false;
        $scope.uploading_doc1 = true;
        var comn_files = "";
        $scope.obj = [];
        $scope.edt = {};

        $http.get(ENV.apiUrl + "api/AssignmentUpload/GetTextFormat").then(function (GetTextFormat) {
            $scope.GetTextFormat = GetTextFormat.data;
            for (var i = 0; i < $scope.GetTextFormat.length; i++) {
                $scope.fileformats = $scope.fileformats + '.' + $scope.GetTextFormat[i].file_format + ',';
            }
        });

        //$http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
        //    $scope.curriculum = AllCurr.data;
        //    $scope.edt = {
        //        sims_cur_code: $scope.curriculum[0].sims_cur_code
        //    }
        //    $scope.getAccYear($scope.curriculum[0].sims_cur_code);
        //    $scope.AssignmentDetails = false;
        //    $scope.assignment_approval = false;
        //});


           function getCur(flag, comp_code) {
            if (flag) {

                $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                    $scope.curriculum = res.data;
                    $scope.edt = {
                        sims_cur_code: $scope.curriculum[0].sims_cur_code
                    }
                    $scope.getAccYear($scope.curriculum[0].sims_cur_code);
                    $scope.AssignmentDetails = false;
                    $scope.assignment_approval = false;
                   
                });
            }
            else {

                $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                    $scope.curriculum = res.data;
                    $scope.edt = {
                        sims_cur_code: $scope.curriculum[0].sims_cur_code
                    }
                    $scope.getAccYear($scope.curriculum[0].sims_cur_code);
                    $scope.AssignmentDetails = false;
                    $scope.assignment_approval = false;
                });


            }

        }

        $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
            $scope.global_count_comp = res.data;

            if ($scope.global_count_comp) {
                getCur(true, $scope.user_details.comp);


            }
            else {
                getCur(false, $scope.user_details.comp)
            }
        });





        $scope.getAccYear = function (curCode) {
            $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                $scope.Acc_year = Acyear.data;
                $scope.edt['sims_academic_year'] = $scope.Acc_year[0].sims_academic_year;
                $scope.select_aca($scope.Acc_year[0].sims_academic_year)
            });
        }

        //$scope.getGrade = function (curCode, accYear) {
        //    $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
        //        $scope.Grade_code = Gradecode.data;
        //    });
        //}

        //$scope.getSection = function (curCode, gradeCode, accYear) {
        //    $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Sectioncode) {
        //        $scope.Section_code = Sectioncode.data;
        //    });
        //}

        $scope.select_aca = function (straca) {

            $http.get(ENV.apiUrl + "api/TeacherAssignmentView/getGradeByUsername?aca=" + straca + "&username=" + username).then(function (getGradeByUsername_Data) {
                $scope.GradeByUsername = getGradeByUsername_Data.data;
               
            });
        }

        $scope.getgrd = function (grade) {
           
            $http.get(ENV.apiUrl + "api/TeacherAssignmentView/getSectionByUserGrade?aca=" + $scope.edt.sims_academic_year + "&grade=" + grade + "&username=" + username).then(function (getSectionByUserGrade_Data) {
                $scope.SectionByUserGrade = getSectionByUserGrade_Data.data;

            });

        }

        $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

        $scope.makeTodos = function () {
            var rem = parseInt($scope.totalItems % $scope.numPerPage);
            if (rem == '0') {
                $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
            } else {
                $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
            }
            var begin = (($scope.currentPage - 1) * $scope.numPerPage);
            var end = parseInt(begin) + parseInt($scope.numPerPage);

            

            $scope.filteredTodos = $scope.todos.slice(begin, end);
        };

        $scope.Show_Data = function () {
            
            if ($scope.edt == undefined || $scope.edt == '' || $scope.edt.sims_grade_code == undefined || $scope.edt.sims_grade_code == '' || $scope.edt.sims_section_code == undefined || $scope.edt.sims_section_code == '') {
                swal('', 'All fields are mandatory.');
            } else {
                $scope.TeacherViewRecord = false;
                $http.get(ENV.apiUrl + "api/TeacherAssignmentView/getAssignmentView?username=" + username + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&ass_name=" + '' + "&academic_yr=" + $scope.edt.sims_academic_year).then(function (getassignmentview_Data) {
                    $scope.assignmentview_Data = getassignmentview_Data.data;
                    $scope.totalItems = $scope.assignmentview_Data.length;
                    $scope.todos = $scope.assignmentview_Data;
                    $scope.makeTodos();
                });

                if ($scope.assignmentview_Data < 1) {
                    swal({
                        title: "Alert",
                        text: "Sorry, There Is No Data Found",
                        width: 300,
                        height: 200
                    });
                    $scope.TeacherViewRecord = false;
                } else {
                    $scope.TeacherViewRecord = true;
                }
            }

        }

        $scope.size = function (str) {

            /*$scope.pagesize = str;
			$scope.currentPage = 1;
			*/

            main = document.getElementById('mainchk');
            if (main.checked == true) {
                main.checked = false;
                $scope.row1 = '';
                $scope.color = '#edefef';
            }

            if (str == "All") {
                $scope.currentPage = 1;
                $scope.numPerPage = $scope.assignmentview_Data.length;
                $scope.makeTodos();

            } else {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }
        }

        $scope.index = function (str) {
            $scope.pageindex = str;
            $scope.currentPage = str;
           
            $scope.makeTodos();
        }

        $scope.Reset = function () {

            $scope.edt = {
                sims_cur_code: '',
                sims_academic_year: '',
                sims_grade_code: '',
                sims_section_code: ''
            }
            $scope.filteredTodos = [];
            $scope.TeacherViewRecord = false;

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.edt = {
                    sims_cur_code: $scope.curriculum[0].sims_cur_code
                }
                $scope.getAccYear($scope.curriculum[0].sims_cur_code);
                $scope.AssignmentDetails = false;
                $scope.assignment_approval = false;
            });
        }

        $scope.OpenAssignment = function (info) {
            
            assign_no = info.sims_assignment_number;

            $scope.temp = {
                sims_subject_code: info.sims_subject_code,
                sims_grade_code: info.sims_grade_code,
                sims_section_code: info.sims_section_code,
                sims_assignment_number: info.sims_assignment_number,
                sims_assignment_title: info.sims_assignment_title,
                sims_assignment_start_date: info.sims_assignment_start_date,
                sims_assignment_freeze_date: info.sims_assignment_freeze_date,
                sims_assignment_submission_date: info.sims_assignment_submission_date,
                sims_assignment_desc: info.sims_assignment_desc
            };
            $('#Model1').modal({
                backdrop: 'static',
                keyboard: true
            });

            $scope.ModelHeader = 'Assignment Details';
            $scope.AssignmentDetails = true;
            $scope.assignment_approval = false;

            $http.get(ENV.apiUrl + "api/TeacherAssignmentView/getStudentAssignment?username=" + username + "&assign_no=" + assign_no).then(function (getStudentAssignment_Data) {
                $scope.getStudentAssignment = getStudentAssignment_Data.data;
               
            });

        }

        $scope.OpenSubmission = function (info) {
            assign_no = info.sims_assignment_number;
            $scope.temp = {
                sims_subject_code: info.sims_subject_code,
                sims_grade_code: info.sims_grade_code,
                sims_section_code: info.sims_section_code,
                sims_assignment_number: info.sims_assignment_number,
                sims_assignment_title: info.sims_assignment_title,
                sims_assignment_start_date: info.sims_assignment_start_date,
                sims_assignment_freeze_date: info.sims_assignment_freeze_date,
                sims_assignment_submission_date: info.sims_assignment_submission_date,
                sims_assignment_desc: info.sims_assignment_desc
            };
            //$scope.temp = { sims_assignment_start_date: info.sims_assignment_start_date };
            //$scope.temp = { sims_assignment_freeze_date: info.sims_assignment_freeze_date };

            $('#MyModalSub').modal('show');
            $scope.SubmissionDetails = true;
            $scope.ModaltableSub = true;
            $http.get(ENV.apiUrl + "api/TeacherAssignmentView/getSubmissionDetails?assign_no=" + assign_no + "&username=" + username).then(function (getSubmissionDetails_Data) {
                $scope.SubmissionDetailsData = getSubmissionDetails_Data.data;

            });
        }

        $scope.ViewDocument = function (info) {

            assign_no = info.sims_assignment_number;
            $scope.temp = {
                sims_subject_code: info.sims_subject_code,
                sims_grade_code: info.sims_grade_code,
                sims_section_code: info.sims_section_code,
                sims_assignment_number: info.sims_assignment_number,
                sims_assignment_title: info.sims_assignment_title,
                sims_assignment_start_date: info.sims_assignment_start_date,
                sims_assignment_freeze_date: info.sims_assignment_freeze_date,
                sims_assignment_submission_date: info.sims_assignment_submission_date,
                sims_assignment_desc: info.sims_assignment_desc
            };
            //$scope.temp = { sims_assignment_start_date: info.sims_assignment_start_date };
            //$scope.temp = { sims_assignment_freeze_date: info.sims_assignment_freeze_date };

            $('#MyModalDoc').modal('show');
            $scope.DocumentDetails = true;
            $scope.ModaltableDoc = true;

            $http.get(ENV.apiUrl + "api/TeacherAssignmentView/getDocumentDetails?assign_no=" + assign_no).then(function (getDocumentDetails_Data) {
                $scope.DocumentDetails_Data = getDocumentDetails_Data.data;
                
            });

        }

        $scope.OpenApproveStatus = function (obj) {
            $scope.ModelHeader = 'Assignment Approval';
            $scope.AssignmentDetails = false;
            $scope.assignment_approval = true;

            $http.get(ENV.apiUrl + "api/TeacherAssignmentView/getStudentDocumentDetails?username=" + username + "&assign_no=" + obj.sims_assignment_number + "&enroll=" + obj.sims_assignment_enroll_number).then(function (getStudentDocumentDetails_Data) {
                $scope.StudentDocumentDetails = getStudentDocumentDetails_Data.data;
                $scope.studdocremark = $scope.StudentDocumentDetails.doc_details;
                $scope.sims_assignment_enroll_number = $scope.StudentDocumentDetails.sims_assignment_enroll_number;
                $scope.StudentName = $scope.StudentDocumentDetails.StudentName;
                $scope.sims_assignment_teacher_remark = $scope.StudentDocumentDetails.sims_assignment_teacher_remark;
                $scope.sims_assignment_student_remark = $scope.StudentDocumentDetails.sims_assignment_student_remark;

            });
            //$scope.temp.sims_assignment_teacher_remark = $scope.StudentDocumentDetails[0].sims_assignment_teacher_remark;
        }

        $scope.Save = function () {
            data1 = [];
            
            if ($scope.studdocremark.length > 0) {
                for (var i = 0; i < $scope.studdocremark.length; i++) {
                    data = {

                        sims_assignment_enroll_number: $scope.StudentDocumentDetails.sims_assignment_enroll_number,
                        sims_assignment_number: $scope.StudentDocumentDetails.sims_assignment_number,
                        sims_doc_line: $scope.studdocremark[i].sims_doc_line,
                        sims509_doc_remark: $scope.studdocremark[i].sims509_doc_remark,
                        sims_assignment_teacher_remark: $scope.sims_assignment_teacher_remark
                    }
                    data1.push(data);

                }
            } else {
                data = {

                    sims_assignment_enroll_number: $scope.StudentDocumentDetails.sims_assignment_enroll_number,
                    sims_assignment_number: $scope.StudentDocumentDetails.sims_assignment_number,
                    sims_assignment_teacher_remark: $scope.sims_assignment_teacher_remark
                }
                data1.push(data);
            }

            
            $http.post(ENV.apiUrl + "api/TeacherAssignmentView/saveTeacherRemark", data1).then(function (msg) {
                $scope.msg1 = msg.data;
                if ($scope.msg1 == true) {
                    swal({
                        text: "Record Updated Successfully",
                        imageUrl: "assets/img/check.png",
                        showCloseButton: true,
                        width: 380,
                    });
                } else if ($scope.msg1 == false) {
                    swal({
                        text: "Record Not Updated. " + $scope.msg1,
                        imageUrl: "assets/img/close.png",
                        showCloseButton: true,
                        width: 380,
                    });
                } else {
                    swal("Error- " + $scope.msg1)
                }

                
                $scope.OpenAssignment($scope.temp);

            });
        }

        $scope.Backtoassignmentlist = function () {
            $scope.OpenAssignment($scope.temp);
        }

        $scope.downloaddoc = function (str) {
           
            //$scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Assignment/StudentDoc/' + str;
            window.open("https://api.mograsys.com/ppapi/Content/" + $http.defaults.headers.common['schoolId'] + "/docs/Assignments/" + str, "_new");

          //  window.open($scope.url);
        }

        $scope.downloaddoc1 = function (str) {
           
           
            $scope.url1 = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Assignment/TeacherDoc/' + str;
            window.open($scope.url1);
        }

        $scope.deletedoc = function (str) {
            
            file_doc = [];
            for (var i = 0; i < $scope.teacher_doc.length; i++) {
                if ($scope.teacher_doc[i].sims_doc_line != str) {
                    var x = {
                        sims509_doc: $scope.teacher_doc[i].sims509_doc,
                        sims509_doc_name: $scope.teacher_doc[i].sims509_doc_name,
                    }
                    file_doc.push(x);
                }
            }
            $scope.teacher_doc = [];
            

            for (var i = 0; i < file_doc.length; i++) {
                var c = {
                    sims509_doc: file_doc[i].sims509_doc,
                    sims509_doc_name: file_doc[i].sims509_doc_name,
                    sims_doc_line: i + 1
                }
                $scope.teacher_doc.push(c);
            }
           

        }

        $scope.Edit = function (info) {
            $scope.assign_no = info.sims_assignment_number;
            $('#MyModalEdit').modal({
                backdrop: 'static',
                keyboard: true
            });

            
            $http.get(ENV.apiUrl + "api/TeacherAssignmentView/getEditAssignmentDetails?assign_no=" + $scope.assign_no).then(function (getEditAssignmentDetails_Data) {

                $scope.EditAssignmentDetails = getEditAssignmentDetails_Data.data;
                $scope.teacher_doc = $scope.EditAssignmentDetails.doc_details;
                $scope.temp = {

                    sims_academic_year: $scope.EditAssignmentDetails.sims_academic_year,
                    sims_cur_code: $scope.EditAssignmentDetails.sims_cur_code,
                    sims_grade_code: $scope.EditAssignmentDetails.sims_grade_code,
                    sims_section_code: $scope.EditAssignmentDetails.sims_section_code,
                    sims_assignment_number: $scope.EditAssignmentDetails.sims_assignment_number,
                    sims_subject_code: $scope.EditAssignmentDetails.sims_subject_code,
                    sims_assignment_title: $scope.EditAssignmentDetails.sims_assignment_title,
                    sims_assignment_desc: $scope.EditAssignmentDetails.sims_assignment_desc,
                    sims_assignment_start_date: $scope.EditAssignmentDetails.sims_assignment_start_date,
                    sims_assignment_submission_date: $scope.EditAssignmentDetails.sims_assignment_submission_date,
                    sims_assignment_freeze_date: $scope.EditAssignmentDetails.sims_assignment_freeze_date,
                    sims_archive_status: $scope.EditAssignmentDetails.sims_archive_status,
                    sims_term_code: $scope.EditAssignmentDetails.sims_term_code
                }

                Sub_Code = $scope.EditAssignmentDetails[0].sims_subject_code;
            });
            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.createdate = function (end_date, start_date, name) {

                
                var month1 = end_date.split("/")[0];
                var day1 = end_date.split("/")[1];
                var year1 = end_date.split("/")[2];
                var new_end_date = year1 + "/" + month1 + "/" + day1;

                var year = start_date.split("/")[0];
                var month = start_date.split("/")[1];
                var day = start_date.split("/")[2];
                var new_start_date = year + "/" + month + "/" + day;

                if (new_end_date < new_start_date) {

                    $rootScope.strMessage = "Please Select Future Date";
                    $('#message').modal('show');

                    $scope.temp[name] = '';
                } else {

                    $scope.temp[name] = new_end_date;
                }
            }

            $scope.showdate = function (date, name) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                $scope.temp[name] = year + "/" + month + "/" + day;
            }

            //$scope.temp = {
            //    sims_assignment_title: info.sims_assignment_title,
            //    sims_assignment_start_date: info.sims_assignment_start_date,
            //    sims_assignment_desc: info.sims_assignment_desc,
            //    sims_assignment_submission_date: info.sims_assignment_submission_date,
            //    sims_assignment_freeze_date: info.sims_assignment_freeze_date
            //};
            $scope.Assignment_Details = true;
            $scope.Search_Student_details = false;
        }

        $scope.Close_edit_Assignment = function () {
            $scope.Assignment_Details = false;
            $scope.Search_Student_details = false;
            $('#MyModalEdit').modal('hide');
        }

        $scope.Submit_Assignment = function () {

            var datadoc = [];

            $http.post(ENV.apiUrl + "api/TeacherAssignmentView/UpdateAssignmentDetails", $scope.temp).then(function (msg) {
                $scope.msg1 = msg.data;
                
                for (var i = 0; i < $scope.teacher_doc.length; i++) {
                    var te = {
                        sims_academic_year: $scope.temp.sims_academic_year,
                        sims_cur_code: $scope.temp.sims_cur_code,
                        sims_grade_code: $scope.temp.sims_grade_code,
                        sims_section_code: $scope.temp.sims_section_code,
                        sims_assignment_number: $scope.temp.sims_assignment_number,
                        sims_doc_line: $scope.teacher_doc[i].sims_doc_line,
                        sims509_desc: $scope.teacher_doc[i].sims509_doc,
                        sims509_desc_name: $scope.teacher_doc[i].sims509_doc_name
                    }
                    datadoc.push(te);
                }

                $http.post(ENV.apiUrl + "api/TeacherAssignmentView/UpdateAssignmentDocDetails", datadoc).then(function (UpdateAssignmentDocDetails) {
                    $scope.UpdateAssignmentDocDetails = UpdateAssignmentDocDetails.data;
                    if ($scope.UpdateAssignmentDocDetails || $scope.msg1) {
                        swal('', 'Record is successfully updated.');

                    } else
                        swal('', 'Record is not updated.' + $scope.msg1);
                    $scope.Assignment_Details = false;
                    $scope.Search_Student_details = false;
                    $('#MyModalEdit').modal('hide');

                });

                $scope.Show_Data();
            });

            //for (var i = 0; i < $scope.StudentDocumentDetails.length; i++) {
            //    data = {

            //        sims_assignment_enroll_number: $scope.StudentDocumentDetails[i].sims_assignment_enroll_number,
            //        sims_assignment_teacher_remark: $scope.temp.sims_assignment_teacher_remark,
            //        assign_no: assign_no,
            //        opr: 'B'
            //    }

            //}
            //data1.push(data);
            //$http.post(ENV.apiUrl + "api/TeacherAssignmentView/UpdateStudentDoc", data1).then(function (msg) {
            //    $scope.msg1 = msg.data;
            //    if ($scope.msg1 == true) {
            //        swal({ title: "Alert", text: "Assignment Submitted Successfully", showCloseButton: true, width: 380, });
            //    }
            //    else {
            //        swal({ title: "Alert", text: "Assignment Not Submitted", showCloseButton: true, width: 380, });
            //    }
            //    $('#Model1').modal('hide');
            //    $('#Model2').modal('hide');
            //    $http.get(ENV.apiUrl + "api/TeacherAssignmentView/getAssignmentView?username=" + username + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&ass_name=" + '').then(function (getassignmentview_Data) {
            //        $scope.assignmentview_Data = getassignmentview_Data.data;
            //        $scope.totalItems = $scope.assignmentview_Data.length;
            //        $scope.todos = $scope.assignmentview_Data;
            //        $scope.makeTodos();
            //    });

            //});

        }

        $scope.Search_Student = function () {

            assign_no = $scope.temp.sims_assignment_number;
            $http.get(ENV.apiUrl + "api/TeacherAssignmentView/getStudentDetails?grade=" + $scope.temp.sims_grade_code + "&section=" + $scope.temp.sims_section_code + "&assign_no=" + assign_no).then(function (getStudentDetails_Data) {
                $scope.StudentDetails = getStudentDetails_Data.data;

            });

            $scope.searchtable = true;
            $scope.Assignment_Details = false;
            $scope.Search_Student_details = true;

            //setTimeout(function () {
            //    $('#Model1Student').modal({ backdrop: 'static', keyboard: true });

            //}, 1000);

        }

        $scope.enroll_number1 = [];
        $scope.enroll_number = [];

        $scope.DataEnroll = function () {
            

            //data1 = [];
            //$scope.enroll_number = [];
            //$scope.student_details = true;
            //for (var i = 0; i < $scope.StudentDetails.length; i++) {
            //    var t = $scope.StudentDetails[i].sims_student_enroll_number;
            //    var v = document.getElementById(t);
            //    //if (v.checked == true) {
            //    sims_assignment_enroll_number = $scope.StudentDetails[i].sims_assignment_enroll_number;
            //        $scope.enroll_number1.push($scope.StudentDetails[i]);
            //    //}
            //}
            //$scope.enroll_number = $scope.enroll_number1;
            
            data1 = [];
            for (var i = 0; i < $scope.StudentDetails.length; i++) {
                data = {

                    sims_academic_year: $scope.temp.sims_academic_year,
                    sims_cur_code: $scope.temp.sims_cur_code,
                    sims_grade_code: $scope.temp.sims_grade_code,
                    sims_section_code: $scope.temp.sims_section_code,
                    sims_assignment_number: $scope.temp.sims_assignment_number,
                    isselected: $scope.StudentDetails[i].isselected,
                    sims_assignment_enroll_number: $scope.StudentDetails[i].sims_assignment_enroll_number,
                    sims_assignment_status: $scope.StudentDetails[i].sims_assignment_status,

                }
                data1.push(data);

            }

            $http.post(ENV.apiUrl + "api/TeacherAssignmentView/UpdateStudentDetails", data1).then(function (msg) {
                $scope.msg1 = msg.data;
                //swal({ title: "Alert", text: "Record Updated Successfully", showCloseButton: true, width: 380, });

            });

            $scope.Assignment_Details = true;
            $scope.Search_Student_details = false;

        }

        $scope.backDataEnroll = function () {
            $scope.Assignment_Details = true;
            $scope.Search_Student_details = false;
        }
        $scope.RemoveEnrollMentNo = function ($event, index, str) {
            str.splice(index, 1);
        }

        $scope.MultipleStudentSelect = function () {

            main = document.getElementById("mainchk2");

            for (var i = 0; i < $scope.StudentDetails.length; i++) {
                //var v = document.getElementById($scope.StudentDetails[i].sims_student_enroll_number);
                $scope.StudentDetails[i].isselected = main.checked;
                $scope.row1 = main.checked ? 'row_selected' : '';
                main.checked ? $('tr').addClass("row_selected") : $('tr').removeClass("row_selected");
            }

        }

        $scope.StudentEnrollNumber = function () {

            $("input[type='checkbox']").change(function (e) {
                if ($(this).is(":checked")) {
                    $(this).closest('tr').addClass("row_selected");
                    $scope.color = '#edefef';
                } else {
                    $(this).closest('tr').removeClass("row_selected");
                    $scope.color = '#edefef';
                }
            });

            $scope.StudentDetails[i].isselected = main.checked;
            if (main.checked == true) {
                main.checked = false;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                    }
                });
            }
        }

        /*$scope.CheckAllChecked = function () {
		    mainchk = document.getElementById("mainchk");
		    if (mainchk.checked == true) {
		        for (var i = 0; i < $scope.filteredTodos.length; i++) {
		            var v = document.getElementById($scope.filteredTodos[i].sims_assignment_title + i);
		            v.checked = true;
		            $scope.row2 = 'row_selected';
		            $('tr').addClass("row_selected");
		        }
		    }
		    else {
		        for (var i = 0; i < $scope.filteredTodos.length; i++) {
		            var v = document.getElementById($scope.filteredTodos[i].sims_assignment_title + i);
		            v.checked = false;
		            mainchk.checked = false;
		            $scope.row2 = '';
		            $('tr').removeClass("row_selected");
		        }
		    }

		}*/

        $scope.CheckAllChecked = function () {
            mainchk = document.getElementById("mainchk");
            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                var v = document.getElementById($scope.filteredTodos[i].sims_assignment_title + i);
                if (mainchk.checked == true) {
                    v.checked = true;
                    $('tr').addClass("row_selected");
                } else {
                    v.checked = false;
                    $('tr').removeClass("row_selected");
                }
            }

        }

        $scope.deleterecord = function () {
            debugger;
            
            var abcd = [];

            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                var v = document.getElementById($scope.filteredTodos[i].sims_assignment_title+i);
                if (v.checked == true) {
                    var m = {
                        sims_assignment_number: $scope.filteredTodos[i].sims_assignment_number
                    }

                    abcd.push(m)
                }
            }


            swal({
                text: 'Are you sure want to delete assignment ? ',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonText: 'Yes',
                width: 380,
                cancelButtonText: 'No',
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $http.post(ENV.apiUrl + "api/TeacherAssignmentView/UpdateDeleteAssignment", abcd).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1) {
                            $http.get(ENV.apiUrl + "api/TeacherAssignmentView/getAssignmentView?username=" + username + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&ass_name=" + '').then(function (getassignmentview_Data) {
                                $scope.assignmentview_Data = getassignmentview_Data.data;
                                $scope.totalItems = $scope.assignmentview_Data.length;
                                $scope.todos = $scope.assignmentview_Data;
                                $scope.makeTodos();
                            });

                        }
                    });
                } else {

                    $http.get(ENV.apiUrl + "api/TeacherAssignmentView/getAssignmentView?username=" + username + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&ass_name=" + '').then(function (getassignmentview_Data) {
                        $scope.assignmentview_Data = getassignmentview_Data.data;
                        $scope.totalItems = $scope.assignmentview_Data.length;
                        $scope.todos = $scope.assignmentview_Data;
                        $scope.makeTodos();
                    });
                }
            });

        }

        $scope.checkonebyonedelete = function () {
            $("input[type='checkbox']").change(function (e) {
                if ($(this).is(":checked")) {
                    $(this).closest('tr').addClass("row_selected");
                    $scope.color = '#edefef';
                } else {
                    $(this).closest('tr').removeClass("row_selected");
                    $scope.color = '#edefef';
                }
            });

            mainchk = document.getElementById('mainchk');
            if (mainchk.checked == true) {
                mainchk.checked = false;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                    }
                });
            }
        }

        $scope.modal_cancel_Student = function () {

            $('body').addClass('grey condense-menu');
            $('#main-menu').addClass('mini');
            $('.page-content').addClass('condensed');
            $scope.isCondensed = true;
            $("body").removeClass("modal-open");
            $("div").removeClass("modal-backdrop in");

        }

        var formdata = new FormData();
        $scope.getTheFiles = function ($files) {
            
            angular.forEach($files, function (value, key) {
                formdata.append(key, value);
            });
        };
        var file_name = '';
        var file_name_en = '';
        var file_doc = [];

        $scope.file_changed = function (element) {
            
            file_name = '';
            var v = new Date();
            if ($scope.teacher_doc.length > 4) {
                swal('', 'Upload maximum 5 files.');

            } else {

                file_name = 'Assignment_' + v.getDay() + '_' + v.getMonth() + '_' + v.getYear() + '_' + v.getHours() + '_' + v.getMonth() + '_' + v.getSeconds();
                $scope.photofile = element.files[0];
                file_name_en = '';

                file_name_en = $scope.photofile.name;
                var len = 0;
                len = file_name_en.split('.');
                var fortype = file_name_en.split('.')[len.length - 1];
                var formatbool = false;
                for (var i = 0; i < $scope.GetTextFormat.length; i++) {
                    if ($scope.GetTextFormat[i].file_format == fortype)
                        formatbool = true;
                }
                if (formatbool == true) {

                    if ($scope.photofile.size > 2097152) {
                        swal('', 'File size limit not exceed upto 2 MB.')
                    } else {
                        $scope.uploading_doc1 = false;
                        $scope.photo_filename = ($scope.photofile.type);
                        

                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $scope.$apply(function () {
                                $scope.prev_img = e.target.result;
                                var request = {
                                    method: 'POST',
                                    url: ENV.apiUrl + 'api/TeacherAssignmentView/upload?filename=' + file_name + '.' + fortype + "&location=" + "Images/StudentImage",
                                    data: formdata,
                                    headers: {
                                        'Content-Type': undefined
                                    }

                                };
                                $http(request).success(function (d) {
                                    
                                    var t = {
                                        sims509_doc: d,
                                        sims509_doc_name: file_name_en
                                    }
                                    $scope.teacher_doc.push(t);
                                    file_doc = [];
                                    for (var i = 0; i < $scope.teacher_doc.length; i++) {
                                        var c = {
                                            sims509_doc: $scope.teacher_doc[i].sims509_doc,
                                            sims509_doc_name: $scope.teacher_doc[i].sims509_doc_name,
                                            sims_doc_line: i + 1
                                        }
                                        file_doc.push(c);
                                    }
                                    $scope.teacher_doc = [];
                                    
                                    $scope.teacher_doc = file_doc;

                                    //$scope.maindata.file = d;

                                    $scope.uploading_doc1 = true;
                                });

                            });
                        };
                        reader.readAsDataURL($scope.photofile);
                    }
                } else {
                    swal('', '.' + fortype + ' File format not allowed.');
                }
            }

        }

        $scope.CancelFileUpload = function (idx) {
            $scope.images.splice(idx, 1);

        };

        $timeout(function () {
            $("#fixTable2").tableHeadFixer({
                'top': 1
            });
            $("#fixTable1").tableHeadFixer({
                'top': 1
            });

        }, 100);

    }])
})();