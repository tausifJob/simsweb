﻿(function () {
    "use strict";
    angular.module('config', [])
   .constant('ENV', {
       name: 'production',
      apiUrl: 'http://localhost:8081/SIMSAPI/', //Always end with '/'
      siteUrl: 'http://localhost:8081/SIMSWeb/',
       //reportUrl: 'http://localhost/SIMSAPI/api/reports/'

     //  apiUrl: 'http://webapi.mograsys.com/APIERP/', //Always end with '/'
//     apiUrl: 'https://api.mograsys.com/kindoapi/', //Always end with '/'
    siteUrl: 'https://main.mograsys.com/',
    reportUrl: 'https://api.mograsys.com/APIERP/api/reports/'

   });
})();