﻿(function () {
    var sims = angular.module('sims');
    sims.filter('studentDataFilter', function () {

        return function (data) {
            var x = data;
            return x.split("|")[0];
        }
    });

    sims.filter('html', ['$sce', function ($sce) {
        return function (text) {
            return $sce.trustAsHtml(text);
        };
    }]);

    sims.filter('studentVisibleFilter', function () {
        return function (data) {
            var visible = data.split("|")[1];
            visible = visible.substring(1, 1);
            return visible === 'Y';
        }
    });
    sims.filter('studentDisableFilter', function () {
        return function (data) {
            var visible = data.split("|")[1];
            visible = visible.substring(0, 1);
            return visible === 'Y';
        }
    });
    sims.filter('offset', function () {
        return function (input, start) {
            start = parseInt(start, 10);
            return input.slice(start);
        };
    });

    sims.filter('capitalize', function () {
        return function (input, scope) {
            if (input != null)
                input = input.toLowerCase();
            return input.substring(0, 1).toUpperCase() + input.substring(1);
        }
    });

    sims.filter('zpad', function () {
        return function (input, n) {
            if (input === undefined)
                input = ""
            if (input.length >= n)
                return input
            var zeros = "0".repeat(n);
            return (zeros + input).slice(-1 * n)
        };
    });
})();