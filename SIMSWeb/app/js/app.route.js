﻿/// <reference path="../modules/Student/views/AllocationRollNo.html" />SimSAM
/// <reference path="../modules/Student/views/AllocationRollNo.html" />
/// <reference path="../modules/Student/views/AllocationRollNo.html" />

(function () {
    'use strict';
    var sims = angular.module('sims');

    sims.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $urlRouterProvider.otherwise('/login');

        $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'app/modules/users/views/login.html',
            controller: 'LoginController'
        })

       .state('login1', {
           url: "/OPAC",
           templateUrl: 'app/modules/Library/views/LibraryItemSearch.html',
           controller: 'LibraryItemSearchCont'
       })
            .state('main.FTLR01', {
                url: "/FreeTeacherListQueryReport",
                templateUrl: 'app/modules/TimeTable/views/FreeTeacherListQueryReport.html',
                controller: 'FreeTeacherListQueryReportCont'
            })


           .state('main.SAR001', {
               url: "/AllocationRollNo",
               templateUrl: 'app/modules/Student/views/AllocationRollNo.html',
               controller: 'AllocationRollNoCont'
           })

       .state('main.SFRNTS', {
           url: "/StudentFeeRefundNew",
           templateUrl: 'app/modules/Fee/views/Sims043RefundNew.html',
           controller: 'Sims043RefundContNew'
       })

    .state('main.Sim545', {
        url: "/UpdateStudentSectionNew",
        templateUrl: 'app/modules/Student/views/UpdateStudentSectionNew.html',
        controller: 'UpdateStudentSectionNewController'
    })

     .state('main.STDINA', {
         url: "/DebitCreditNoteApproval",
         templateUrl: 'app/modules/Fee/views/DebitCreditNoteStudentApproval.html',
         controller: 'DebitCreditNoteStudentApprovalCont'
     })

    .state('main.STDINV', {
        url: "/DebitCreditNoteStudent",
        templateUrl: 'app/modules/Fee/views/DebitCreditNoteStudent.html',
        controller: 'DebitCreditNoteStudentCont'
    })


    .state('main.CCDRR', {
        url: "/CautionDepositCancelReceipt",
        templateUrl: 'app/modules/Fee/views/CautionDepositCancelReceipt.html',
        controller: 'CautionDepositCancelReceiptCont'
    })


      .state('main.SFNTS', {
          url: "/SectionFeeNew",
          templateUrl: 'app/modules/Fee/views/SectionFeeNew.html',
          controller: 'SectionFeeControllerNew'
      })
      .state('main.DSFNTS', {
          url: "/DefineStudentFeeNew",
          templateUrl: 'app/modules/Fee/views/DefineStudentFeeNew.html',
          controller: 'DefineStudentFeeContNew'
      })


        .state('main.Sim712', {
            url: "/SurveyQuestionsMapping",
            templateUrl: 'app/modules/Student/views/Surveyquestionsmapping.html',
            controller: 'SurveyquestionsmappingCont'
        })



        .state('main.CScCfg', {
            url: "/Co-ScholsaticConfig",
            templateUrl: 'app/modules/Gradebook/views/Co-ScholsaticConfig.html',
            controller: 'Co-ScholsaticConfigCont'
        })


            .state('main.SBSA01', {
                url: "/StudentBellowspecifiedAverage",
                templateUrl: 'app/modules/Gradebook/views/StudentBellowspecifiedAverage.html',
                controller: 'StudentBellowspecifiedAverageCont'
            })

        .state('main.ser001', {
            url: "/SurveyRating",
            templateUrl: 'app/modules/Student/views/SurveyRating.html',
            controller: 'SurveyRatingCont'
        })

        .state('main.Sim721', {
            url: "/SurveyRatingEvaluation",
            templateUrl: 'app/modules/Student/views/SurveyRatingEvaluation.html',
            controller: 'SurveyRatingEvaluationCont'
        })


             .state('main.RBAR01', {
                 url: "/RoleBasedAccessReport",
                 templateUrl: 'app/modules/Gradebook/views/RoleBasedAccessReport.html',
                 controller: 'RoleBasedAccessReportCont'
             })



       .state('main.Sim713', {
           url: "/SurveySelection",
           templateUrl: 'app/modules/Student/views/SurveySelection.html',
           controller: 'SurveySelectionCont'
       })


     .state('main.ServeySelection', {
         url: "/ServeySelection",
         templateUrl: 'app/modules/Student/views/ServeySelection.html',
         controller: 'ServeySelectionCont'
     })

        .state('main.RDBOOK', {
            url: "/MinistoryRedbook",
            templateUrl: 'app/modules/Gradebook/views/SOKRedbook.html',
            controller: 'SOKRedbookCont'
        })

                .state('main.Fin235', {
                    url: "/Yearshifting",
                    templateUrl: 'app/modules/Finance/views/Yearshifting.html',
                    controller: 'YearshiftingCont'
                })


				.state('main.CSR001', {
				    url: "/chequeSubmissionReport",
				    templateUrl: 'app/modules/Finance/views/chequeSubmissionReport.html',
				    controller: 'chequeSubmissionReportCont'
				})





        .state('main.COSCSC', {
            url: "/CoScholasticScale",
            templateUrl: 'app/modules/Gradebook/views/coscholasticGroupGradeScale.html',
            controller: 'coscholasticGroupGradeScaleCont'
        })



        .state('main.SBVPP', {
            url: "/SyllabusView",
            templateUrl: 'app/modules/SchoolSetup/views/SyllabusView.html',
            controller: 'SyllabusViewCont'
        })

        .state('main.Mogra02', {
            url: "/mograVersion",
            templateUrl: 'app/modules/SchoolSetup/views/mograVersion.html',
            controller: 'mograVersionCont',
        })


                  .state('main.SESUTM', {
                      url: "/sectiontermmapping",
                      templateUrl: 'app/modules/SchoolSetup/views/SectionSubjectTermMapping.html',
                      controller: 'SectionSubjectTermMappingCont',
                  })



         .state('main.MoMoV', {
             url: "/mograVersionDetail",
             templateUrl: 'app/modules/SchoolSetup/views/mograVersionDetail.html',
             controller: 'mograVersionDetailCont'
         })

                //.state('main.MoMoV1', {
                //    url: "/MograVersionDetailUpdate",
                //    templateUrl: 'app/modules/SchoolSetup/views/MograVersionDetailUpdate.html',
                //    controller: 'MograVersionDetailUpdateCont'
                //})

                 //.state('main.vrgdet', {
                 //    url: "/VersionDetailsUpdate",
                 //    templateUrl: 'app/modules/Setup/views/MograVersionDetailUpdate.html',
                 //    controller: 'MograVersionDetailUpdateCont'
                 //})

                .state('main.MoMoV2', {
                    url: "/MograDeploymentFinalisation",
                    templateUrl: 'app/modules/Setup/views/MograDeploymentFinalisation.html',
                    controller: 'MograDeploymentFinalisationCont'
                })

     .state('main.mom001', {
         url: "/MeetingType",
         templateUrl: 'app/modules/SchoolActivity/views/meetingType.html',
         controller: 'MeetingTypeCont'
     })

    .state('main.mom002', {
        url: "/ScheduleMeeting",
        templateUrl: 'app/modules/SchoolActivity/views/schedulemeeting.html',
        controller: 'ScheduleMeetingCont'
    })

        .state('main.EmrPck', {
            url: "/EmergencyPickup",
            templateUrl: 'app/modules/HRMS/views/EmergencyPickup.html',
            controller: 'EmergencyPickupCont'
        })

    .state('main.mom003', {
        url: "/ApproveMeeting",
        templateUrl: 'app/modules/SchoolActivity/views/approvemeeting.html',
        controller: 'ApproveMeetingCont'
    })

    .state('main.mom004', {
        url: "/ViewMeeting",
        templateUrl: 'app/modules/SchoolActivity/views/viewmeeting.html',
        controller: 'ViewMeetingCont'
    })

    .state('main.mom005', {
        url: "/MinutesofMeeting",
        templateUrl: 'app/modules/SchoolActivity/views/minutesofmeeting.html',
        controller: 'MinutesofMeetingCont'
    })
           .state('main.Att001', {
               url: "/StudentAttendanceNew",
               templateUrl: 'app/modules/Attendance/views/StudentAttendanceNew.html',
               controller: 'StudentAttendanceNewCont'
           })

         .state('main.Att002', {
             url: "/StudentAttendanceMonthlyNew",
             templateUrl: 'app/modules/Attendance/views/StudentAttendanceMonthlyNew.html',
             controller: 'StudentAttendanceMonthlyNewCont'
         })

        .state('main.Att003', {
            url: "/SubjectwisetAttendanceNew",
            templateUrl: 'app/modules/Attendance/views/SubjectwisetAttendanceNew.html',
            controller: 'SubjectwisetAttendanceNewCont'
        })

          .state('main.tchApp', {
              url: "/teacherApplication",
              templateUrl: 'app/modules/Setup/views/UserApplicationsForTeacherApp.html',
              controller: 'UserApplicationsForTeacherAppCont'
          })

        .state('main.SylCrt', {
            url: "/SyllabusCreation",
            templateUrl: 'app/modules/SchoolSetup/views/SyllabusCreation.html',
            controller: 'SyllabusCreationCont'
        })

        .state('main.SimBMF', {
            url: "/BadgeMaster",
            templateUrl: 'app/modules/Setup/views/BadgeMaster.html',
            controller: 'BadgeMasterCont'
        })

        .state('main.SimBAF', {
            url: "/BadgeAllocation",
            templateUrl: 'app/modules/Setup/views/BadgeAllocation.html',
            controller: 'BadgeAllocationCont'
        })

        .state('main.SylCtn', {
            url: "/SyllabusUnitCreation",
            templateUrl: 'app/modules/SchoolSetup/views/SyllabusUnitCreation.html',
            controller: 'SyllabusUnitCreationCont'
        })

        .state('main.EmLPrl', {
            url: "/EmployeeMasterPrl",
            templateUrl: 'app/modules/HRMS/views/EmployeeMasterPearl.html',
            controller: 'EmployeeMasterPearlCont'
        })


        .state('main.TimeTb', {
            url: "/TimeTable",
            templateUrl: 'app/modules/TimeTable/views/SchoolTimeTable.html',
            controller: 'SchoolTimeTableCont'
        })

             .state('main.SLDR01', {
                 url: "/SubstituteLectureDetailsReport",
                 templateUrl: 'app/modules/TimeTable/views/SubstitueLectureDetailsReport.html',
                 controller: 'SubstitueLectureDetailsReportCont'
             })


        .state('main.FreTea', {
            url: "/FreeTeacher",
            templateUrl: 'app/modules/TimeTable/views/FreeTeacher.html',
            controller: 'FreeTeacherCont'
        })

        .state('main.TtClVw', {
            url: "/TimeTableClass",
            templateUrl: 'app/modules/TimeTable/views/TimeTableClassView.html',
            controller: 'TimeTableClassViewCont'
        })
        .state('main.TtTrVw', {
            url: "/TimeTableTeacher",
            templateUrl: 'app/modules/TimeTable/views/TimeTableTeacherView.html',
            controller: 'TimeTableTeacherViewCont'
        })
        .state('main.SutLec', {
            url: "/LectureSubstitution",
            templateUrl: 'app/modules/TimeTable/views/LectureSubstitution.html',
            controller: 'LectureSubstitutionCont'
        })
        .state('main.TechRe', {
            url: "/TeacherReport",
            templateUrl: 'app/modules/TimeTable/views/TeacherReport.html',
            controller: 'TeacherReportCont'
        })
        .state('main.TsCaPr', {
            url: "/TeachingCareerProfile",
            templateUrl: 'app/modules/HRMS/views/TeachingStaffCareerProfile.html',
            controller: 'TeachingStaffCareerProfileCont'
        })
            .state('main.VwAppE', {
                url: "/ViewApplicantProfile",
                templateUrl: 'app/modules/HRMS/views/ViewApplicantProfile.html',
                controller: 'ViewApplicantProfileCont'
            })
        .state('main.SubPrf', {
            url: "/SubjectPreference",
            templateUrl: 'app/modules/TimeTable/views/SubjectPreference.html',
            controller: 'SubjectPreferenceCont'
        })

        .state('main.TeaPrf', {
            url: "/TeacherPreference",
            templateUrl: 'app/modules/TimeTable/views/TeacherPreference.html',
            controller: 'TeacherPreferenceCont'
        })


        .state('main.TeaAll', {
            url: "/TeacherAllocation",
            templateUrl: 'app/modules/TimeTable/views/TimeTable.html',
            controller: 'TimeTableCont'
        })

        .state('main.SubTea', {
            url: "/SubjectTeacher",
            templateUrl: 'app/modules/TimeTable/views/SubjectTeacher.html',
            controller: 'SubjectTeacherCont'
        })

        .state('main.BelCfg', {
            url: "/BellConfig",
            templateUrl: 'app/modules/TimeTable/views/BellConfig.html',
            controller: 'BellConfigCont'
        })

        .state('main.EmpExp', {
            url: "/EmployeeExperience",
            templateUrl: 'app/modules/HRMS/views/EmployeeExperience.html',
            controller: 'EmployeeExperienceCont'
        })

        .state('main.CrENIS', {
            url: "/EmployeeShortFrm",
            templateUrl: 'app/modules/HRMS/views/CreateEditEmployeeForNIS.html',
            controller: 'CreateEditEmployeeForNISCont'
        })

          .state('main.EmProf', {
              url: "/EmployeeProfile",
              templateUrl: 'app/modules/HRMS/views/EmployeeProfile.html',
              controller: 'EmployeeProfileCont'
          })
                    .state('main.UpdOES', {
                        url: "/UpdateEmployeeOES",
                        templateUrl: 'app/modules/HRMS/views/UpdateEmployeeOES.html',
                        controller: 'UpdateEmployeeOESCont'
                    })

          .state('main.TrnEMP', {
              url: "/TransferEmployee",
              templateUrl: 'app/modules/HRMS/views/TransferEmployee.html',
              controller: 'TransferEmployeeCont'
          })

         .state('main.EmpDps', {
             url: "/CreateEditEmpDps",
             templateUrl: 'app/modules/HRMS/views/CreateEditEmployeeDpsmis.html',
             controller: 'CreateEditEmployeeDpsmisCont'
         })
            .state('main.EmpLet', {
                url: "/EmployeeLetterRequest",
                templateUrl: 'app/modules/HRMS/views/EmployeeLetterRequest.html',
                controller: 'EmployeeLetterRequestCont'
            })
            .state('main.LetApp', {
                url: "/EmployeeLetterReqApprove",
                templateUrl: 'app/modules/HRMS/views/EmployeeLetterReqApprove.html',
                controller: 'EmployeeLetterReqApproveCont'
            })
            .state('main.LetIss', {
                url: "/EmployeeLetterReqIssue",
                templateUrl: 'app/modules/HRMS/views/EmployeeLetterReqIssue.html',
                controller: 'EmployeeLetterReqIssueCont'
            })
         .state('main.EmpGrt', {
             url: "/EmpGratuityView",
             templateUrl: 'app/modules/HRMS/views/EmpGratuityView.html',
             controller: 'EmpGratuityViewCont'
         })

         .state('main.UAdisw', {
             url: "/UpdtEmployee",
             templateUrl: 'app/modules/HRMS/views/UpdateEmployeeADISW.html',
             controller: 'UpdateEmployeeADISWCont'
         })


          .state('main.UpEmpM', {
              url: "/UpdateEmployeeMaster",
              templateUrl: 'app/modules/HRMS/views/UpdateEmployeeMasterAdisw.html',
              controller: 'UpdateEmployeeMasterAdiswCont'
          })


        .state('main.EmLgAD', {
            url: "/EmployeeLongForm",
            templateUrl: 'app/modules/HRMS/views/EmployeeLongFormAdisw.html',
            controller: 'EmployeeLongFormAdiswCont'
        })

        .state('main.CdEmpW', {
            url: "/EmployeeShortFrmA",
            templateUrl: 'app/modules/HRMS/views/CreateEditEmployeeAdisw.html',
            controller: 'CreateEditEmployeeAdiswCont'
        })

                .state('main.MMSR20', {
                    url: "/MeetingManagementSchedulReport_Rpt",
                    templateUrl: 'app/modules/HRMS/views/MeetingManagementSchedulReport_Rpt.html',
                    controller: 'MeetingManagementSchedulReport_RptCont'
                })



         .state('main.ATTDAS', {
             url: "/AttendanceDashboard",
             templateUrl: 'app/modules/Attendance/views/AttendanceDashboard.html',
             controller: 'AttendanceDashboardCont'
         })

          .state('main.ADMDAS', {
              url: "/admissionDashboard1",
              templateUrl: 'app/modules/Attendance/views/AdmissionDashboard.html',
              controller: 'AdmissionDashboardCont'
          })

         .state('main.Admcan', {
             url: "/ADMCancellation",
             templateUrl: 'app/modules/Student/views/ADMCencellation.html',
             controller: 'ADMCancellationCont'
         })


         .state('main.Sim765', {
             url: "/studentpromotion",
             templateUrl: 'app/modules/Student/views/studentpromotion.html',
             controller: 'studentpromotionCont'
         })

         .state('main.payApr', {
             url: "/PaymentApproval",
             templateUrl: 'app/modules/Student/views/PaymentApproval.html',
             controller: 'PaymentApprovalCont'
         })

		    .state('main.payIIS', {
		        url: "/PaymentApproval_IIS",
		        templateUrl: 'app/modules/Student/views/PaymentApproval_IIS.html',
		        controller: 'PaymentApproval_IISCont'
		    })

               .state('main.payABQ', {
                   url: "/PaymentApproval_ABQ",
                   templateUrl: 'app/modules/Student/views/PaymentApproval_ABQ.html',
                   controller: 'PaymentApproval_ABQCont'
               })

        .state('main.Grbokn', {
            url: "/GradeBookNewDesign",
            templateUrl: 'app/modules/Gradebook/views/GradeBookNewDesign.html',
            controller: 'GradeBookNewDesignCont'
        })

        .state('main.MrkEty', {
            url: "/MarksEntry",
            templateUrl: 'app/modules/Gradebook/views/MarksEntry.html',
            controller: 'MarksEntryCont'
        })

            .state('main.CosEty', {
                url: "/Co_scholastic_New",
                templateUrl: 'app/modules/Gradebook/views/Co_scholastic_New.html',
                controller: 'Co_scholastic_NewCont'
            })

            .state('main.RptApp', {
                url: "/ReportCardApprove",
                templateUrl: 'app/modules/Gradebook/views/ReportCardApprove.html',
                controller: 'ReportCardApproveCont'
            })


         .state('main.ExRefM', {
             url: "/ExamReferenceMaterial",
             templateUrl: 'app/modules/Gradebook/views/ExamReferenceMaterial.html',
             controller: 'ExamReferenceMaterialCont'
         })


         .state('main.Sim770', {
             url: "/WeeklyAssessment",
             templateUrl: 'app/modules/Student/views/WeeklyAssessment.html',
             controller: 'WeeklyAssessmentCont'
         })


         .state('main.Sim771', {
             url: "/WeeklyAssessmentShow",
             templateUrl: 'app/modules/SchoolSetup/views/WeeklyAssessmentShow.html',
             controller: 'WeeklyAssessmentShowCont'
         })



        .state('main.Sim522', {
            url: "/Admissionclasses",
            templateUrl: 'app/modules/Common/views/Admissionclasses.html',
            controller: 'AdmissionclassesCont'
        })

         .state('main.UserMenu', {
             url: "/menu",
             templateUrl: 'app/modules/Common/views/Menu.html',
             controller: 'MenuController'
         })

             .state('main.menuModule', {
                 url: "/menuModule",
                 templateUrl: 'app/modules/Common/views/Module.html',
                 controller: 'ModuleController'
             })

          .state('main.SIM41R', {
              url: "/AdmissionStatus",
              templateUrl: 'app/modules/Student/views/AdmissionStatusRpt.html',
              controller: 'AdmissionStatusController'
          })
          .state('main.stdImg', {
              url: "/UpdateStudentImage",
              templateUrl: 'app/modules/Student/views/UpdateStudentImage.html',
              controller: 'UpdateStudentImageCont'
          })
         .state('main.Per99V', {
             url: "/VacancyApp",
             templateUrl: 'app/modules/Common/views/VacancyApplication.html',
             controller: 'VacancyApplController'
         })
          .state('main.sim543', {
              url: "/ReportCardComment",
              templateUrl: 'app/modules/Gradebook/views/Comment.html',
              controller: 'CommentController'
          })
         .state('main.sim600', {
             url: "/AgendaApproval",
             templateUrl: 'app/modules/Gradebook/views/AgendaApproval.html',
             controller: 'AgendaApprovalCont'
         })
         .state('main.Per256', {
             url: "/QualificationMaster",
             templateUrl: 'app/modules/HRMS/views/QualificationMaster.html',
             controller: 'QualificationMasterCont'
         })
             .state('main.EHM010', {
                 url: "/EmployeeHobbiesMaster",
                 templateUrl: 'app/modules/HRMS/views/EmployeeHobbiesMaster.html',
                 controller: 'EmployeeHobbiesMasterCont'

             })
          .state('main.Fin017', {
              url: "/FixedAssetDepreciationCalculation",
              templateUrl: 'app/modules/Finance/views/FixedAssetDepreciationCalculation.html',
              controller: 'FixedAssetDepreciationCalculationCont'
          })
        .state('main.Sims617', {
            url: "/LateFeeRules",
            templateUrl: 'app/modules/Fee/views/LateFee.html',
            controller: 'StudentLateFeeController'
        })

         .state('main.LatFee', {
             url: "/LateFeeMaster",
             templateUrl: 'app/modules/Fee/views/LateFeeMaster.html',
             controller: 'LateFeeMasterCont'
         })

         .state('main.StuInv', {
             url: "/StudentInvoice",
             templateUrl: 'app/modules/Fee/views/StudentInvoice.html',
             controller: 'StudentInvoiceCont'
         })

          .state('main.StuInN', {
              url: "/StudentInvoiceNew",
              templateUrl: 'app/modules/Fee/views/StudentInvoiceNew.html',
              controller: 'StudentInvoiceNewCont'
          })



         .state('main.Sims618', {
             url: "/StudentLateFee",
             templateUrl: 'app/modules/Fee/views/latefeerule.html',
             controller: 'LateFeeRuleDefineController'
         })
        .state('main.Sims619', {
            url: "/StudentLateFee",
            templateUrl: 'app/modules/Fee/views/LateRuleFormula.html',
            controller: 'LateRuleFormulaController'
        })
         .state('main.ReportCardParameter', {
             url: "/ReportCardParameter",
             templateUrl: 'app/modules/Setup/views/ReportCardParameter.html',
             controller: 'ReportCardParameterCont'
         })
         .state('main.EmployeeShortForm', {
             url: "/EmployeeShortForm",
             templateUrl: 'app/modules/HRMS/views/EmployeeShortForm.html',
             controller: 'EmployeeShortFormCont'
         })

        .state('main.Com099', {
            url: "/emailtoDefaulters",
            templateUrl: 'app/modules/Collaboration/views/EmailToDefaulters.html',
            controller: 'DefaulterStatusController'
        })

              .state('main.EmaDef', {
                  url: "/emailtoDefaulters_siso",
                  templateUrl: 'app/modules/Collaboration/views/EmailToDefaulterSISO.html',
                  controller: 'DefaulterStatusSISO'
              })

        .state('main.EmlApp', {
            url: "/EmailApproval",
            templateUrl: 'app/modules/Collaboration/views/EmailApproval.html',
            controller: 'EmailApprovalCont'
        })
   .state('main.AppESC', {
       url: "/ApprovalEmailSMSCommunication",
       templateUrl: 'app/modules/Collaboration/views/ApprovalEmailSMSCommunication.html',
       controller: 'ApprovalEmailSMSCommunicationCont'
   })
        .state('main.SmsApp', {
            url: "/SmsApproval",
            templateUrl: 'app/modules/Collaboration/views/SmsApproval.html',
            controller: 'SmsApprovalCont'
        })





         .state('main.ComDef', {
             url: "/CommunicationToDefaulters",
             templateUrl: 'app/modules/Collaboration/views/CommunictaionToDefaulters.html',
             controller: 'CommunicationToDefaultersController'
         })
        .state('main.Sim048', {
            url: "/Workflow",
            templateUrl: 'app/modules/Setup/views/Workflow.html',
            controller: 'WorkflowCont'
        })


        .state('main.Sim507', {
            url: "/MySearch",
            templateUrl: 'app/modules/Setup/views/MySearch.html',
            controller: 'MySearchCont'
        })

         .state('main.Sim510', {
             url: "/SearchUser",
             templateUrl: 'app/modules/Setup/views/SearchUser.html',
             controller: 'SearchUserCont'
         })

           .state('main.Com014', {
               url: "/ResetPassword",
               templateUrl: 'app/modules/Security/views/ResetPassword.html',
               controller: 'ResetPasswordCont'
           })

         .state('main.ReportCard', {
             url: "/ReportCard",
             templateUrl: 'app/modules/Setup/views/ReportCard.html',
             controller: 'ReportCardCont'
         })

          .state('main.Per510', {
              url: "/EmployeeDocumentDetails",
              templateUrl: 'app/modules/HRMS/views/EmployeeDocumentDetails.html',
              controller: 'EmployeeDocumentDetailsCont'
          })

         .state('main.Per78c', {
             url: "/Updatepaysheet",
             templateUrl: 'app/modules/HRMS/views/UpdatePaysheet.html',
             controller: 'UpdatePaysheetCont'
         })

        .state('main.TimeLine', {
            url: "/TimeLine",
            templateUrl: 'app/modules/Setup/views/TimeLine.html',
            controller: 'TimeLineCont'
        })

            .state('main.InvSlD', {
                url: "/SaleDocumentList",
                templateUrl: 'app/modules/Inventory/views/SaleDocumentList.html',
                controller: 'SaleDocumentListCont'
            })
         .state('main.InvSto', {
             url: "/StoreIssue",
             templateUrl: 'app/modules/Inventory/views/StoreIssue.html',
             controller: 'StoreIssueCont'
         })

             .state('main.storeI', {
                 url: "/SMFCStoreIssue",
                 templateUrl: 'app/modules/Inventory/views/SMFCStoreIssue.html',
                 controller: 'SMFCStoreIssueCont'
             })

             .state('main.report', {
                 url: "/SMFCStoreIssueReport",
                 templateUrl: 'app/modules/Inventory/views/SMFCStoreIssueReport.html',
                 controller: 'SMFCStoreIssueReportCont',
                 params: { 'IP': {} }
             })

             .state('main.detail', {
                 url: "/StoreIssueDetails",
                 templateUrl: 'app/modules/Inventory/views/StoreIssueDetails.html',
                 controller: 'StoreIssueDetailsCont',
                 params: { 'IP': {} }
             })

            .state('main.PIDR01', {
                url: "/PendingItemDeliveryReport",
                templateUrl: 'app/modules/Inventory/views/PendingItemDeliveryReport.html',
                controller: 'PendingItemDeliveryReportCont'
            })


         .state('main.deliveryorder', {
             url: "/deliveryorder",
             templateUrl: 'app/modules/Inventory/views/DeliveryOrder.html',
             controller: 'DeliveryOrderCont'
         })


         .state('main.FeePPM', {
             url: "/FeePostingPayModeInvs",
             templateUrl: 'app/modules/Inventory/views/FeePostingPayModeInvs.html',
             controller: 'FeePostingPayModeInvsCont'
         })


       .state('main.ForAgn', {
           url: "/ForwordAgent",
           templateUrl: 'app/modules/Inventory/views/ForwardAgent.html',
           controller: 'ForwardAgentCont'
       })


          .state('main.Sim615', {
              url: "/FeeReceipt",
              templateUrl: 'app/modules/Fee/views/FeeReceipt.html',
              controller: 'FeeReceiptCont'
          })

             .state('main.Sim61A', {
                 url: "/FeeReceiptABQIS",
                 templateUrl: 'app/modules/Fee/views/FeeReceiptABQIS.html',
                 controller: 'FeeReceiptABQISCont'
             })



        .state('main.FeeRec', {
            url: "/FeeReceipt_pearl",
            templateUrl: 'app/modules/Fee/views/FeeReceiptView_pearl.html',
            controller: 'FeeReceiptView_pearl'
        })


        .state('main.FeeLed', {
            url: "/StudentFeeLedger",
            templateUrl: 'app/modules/Fee/views/StudentFeeLedger.html',
            controller: 'StudentFeeLedgerCont'
        })


         .state('main.FeeRece', {
             url: "/FeeReceiptNew",
             templateUrl: 'app/modules/Fee/views/FeeReceiptNew.html',
             controller: 'FeeReceiptNewCont'
         })



         .state('main.SisCla', {
             url: "/ClassWiseFeeReceiptMultiple",
             templateUrl: 'app/modules/Fee/views/ClasswiseFeeReceiptMultiple.html',
             controller: 'ClasswiseFeeReceiptMultipleCont'
         })


          .state('main.SisDfa', {
              url: "/DefaulterList",
              templateUrl: 'app/modules/Fee/views/DefaulterList.html',
              controller: 'DefaulterListCont'
          })


          .state('main.PerDfa', {
              url: "/DefaulterList_pearl",
              templateUrl: 'app/modules/Fee/views/DefaulterList_pearl.html',
              controller: 'DefaulterListCont_pearl'
          })



         .state('main.Sim710', {
             url: "/ClassWiseFeeReceipt",
             templateUrl: 'app/modules/Fee/views/ClassWiseFeeReceipt.html',
             controller: 'ClassWiseFeeReceiptCont'
         })


        .state('main.Sim700', {
            url: "/FeeCollection",
            templateUrl: 'app/modules/Fee/views/FeeCollection.html',
            controller: 'FeeCollectionCont'
        })

         .state('main.Sim501', {
             url: "/PPFieldStatus",
             templateUrl: 'app/modules/Setup/views/PPFieldStatus.html',
             controller: 'PPFieldStatusCont',
         })
          .state('main.Sim135', {
              url: "/libraryTrans",
              templateUrl: 'app/modules/Library/views/LibraryTran.html',
              controller: 'LibraryTranController'
          })
        .state('main.Sim567', {
            url: "/ProspectDashboard",
            templateUrl: 'app/modules/Student/views/ProspectDashboard.html',
            controller: 'ProspectDashboardCont',
            params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
        })

          .state('main.Prdpsd', {
              url: "/ProspectDashboard_dpsd",
              templateUrl: 'app/modules/Student/views/ProspectDashboard_dpsd.html',
              controller: 'ProspectDashboard_dpsdCont',
              params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
          })


             .state('main.Prsiso', {
                 url: "/ProspectDashboard_siso",
                 templateUrl: 'app/modules/Student/views/ProspectDashboard_siso.html',
                 controller: 'ProspectDashboard_sisoCont',
                 params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
             })
			   .state('main.PrsRep', {
			       url: "/ProspectDashboardReport",
			       templateUrl: 'app/modules/Student/views/ProspectDashboardReport.html',
			       controller: 'ProspectDashboardReportCont',

			   })

        .state('main.PrdNIS', {
            url: "/ProspectDashboard_NIS",
            templateUrl: 'app/modules/Student/views/ProspectDashboard_NIS.html',
            controller: 'ProspectDashboard_NISCont',
            params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
        })

        .state('main.Sim034', {
            url: "/ProspectAdmissionNIS",
            templateUrl: 'app/modules/Student/views/ProspectAdmissionNIS.html',
            controller: 'ProspectAdmissionNISCont',
        })

        .state('main.Sim037', {
            url: "/ProspectAdmissionShort",
            templateUrl: 'app/modules/Student/views/ProspectAdmissionShort.html',
            controller: 'ProspectAdmissionShortCont',
            params: { Pros_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
        })

             .state('main.PsSiso', {
                 url: "/ProspectAdmissionShort_siso",
                 templateUrl: 'app/modules/Student/views/ProspectAdmissionShortSiso.html',
                 controller: 'ProspectAdmissionShortSisoCont',
                 params: { Pros_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
             })

             .state('main.PsWalk', {
                 url: "/ProspectAdmissionShortWalkIn",
                 templateUrl: 'app/modules/Student/views/ProspectAdmissionShortWalkIn.html',
                 controller: 'ProspectAdmissionShortWalkInCont',
                 params: { Pros_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
             })


        .state('main.PshNis', {
            url: "/ProspectAdmissionShort_NIS",
            templateUrl: 'app/modules/Student/views/ProspectAdmissionShort_NIS.html',
            controller: 'ProspectAdmissionShort_NISCont',
            params: { Pros_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
        })

          .state('main.Pshdpsd', {
              url: "/ProspectAdmissionShort_dpsd",
              templateUrl: 'app/modules/Student/views/ProspectAdmissionShort_dpsd.html',
              controller: 'ProspectAdmissionShort_dpsdCont',
              params: { Pros_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
          })

        .state('main.ProspectAdmission', {
            url: "/ProspectAdmission",
            templateUrl: 'app/modules/Student/views/ProspectAdmission.html',
            controller: 'ProspectAdmissionCont',
            params: { Pros_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
        })

    .state('main.Munici', {
        url: "/Municipality",
        templateUrl: 'app/modules/Student/views/Municipality.html',
        controller: 'MunicipalityCont',

    })
        .state('main.Munzon', {
            url: "/MunicipalityZone",
            templateUrl: 'app/modules/Student/views/MunicipalityZone.html',
            controller: 'MunicipalityZoneCont',

        })

         .state('main.emailt', {
             url: "/Email",
             templateUrl: 'app/modules/Setup/views/Email.html',
             controller: 'EmailCont'
         })


        .state('main.AuditAlert', {
            url: "/TimeLineAlert",
            templateUrl: 'app/modules/Setup/views/TimeLineAlert.html',
            controller: 'TimeLineAlertCont'
        })


         .state('main.Email', {
             url: "/EmailTransaction",
             templateUrl: 'app/modules/Setup/views/EmailCommunication.html',
             controller: 'EmailCommunicationCont'
         })


        .state('main', {
            url: "/main",
            templateUrl: 'app/modules/main/views/main.html',
            controller: 'MainController'
        })

          .state('main.BankRe', {
              url: "/BankReconciliation",
              templateUrl: 'app/modules/Finance/views/BankReconciliation.html',
              controller: 'BankReconciliationCont'
          })

           .state('main.BankRV', {
               url: "/BankReconciliationView",
               templateUrl: 'app/modules/Finance/views/BankReconciliationView.html',
               controller: 'BankReconciliationViewCont'
           })

        .state('main.Fin006', {
            url: "/AssetLiteral",
            templateUrl: 'app/modules/Finance/views/AssetLiteral.html',
            controller: 'AssetLiteralCont'
        })
        .state('main.Fin155', {
            url: "/FeePosting",
            templateUrl: 'app/modules/Finance/views/FeePosting.html',
            controller: 'FeePostingCont'
        })
        .state('main.Fin021', {
            url: "/AssetMaster",
            templateUrl: 'app/modules/Finance/views/AssetMaster.html',
            controller: 'AssetMasterCont'
        })
        .state('main.AtMtTr', {
            url: "/AssMstTransaction",
            templateUrl: 'app/modules/Finance/views/AssetMasterTransactiondpsd.html',
            controller: 'AssetMasterTransaction_dpsdCont'
        })
        .state('main.AMstTr', {
            url: "/AssetMasterTransaction",
            templateUrl: 'app/modules/Finance/views/AssetMasterTransaction.html',
            controller: 'AssetMasterTransactionCont'
        })
         .state('main.AstMst', {
             url: "/AssetMasterDPSD",
             templateUrl: 'app/modules/Finance/views/AssetMaster_dpsd.html',
             controller: 'AssetMaster_dpsdCont'
         })

         .state('main.AssCus', {
             url: "/AssetCustodian",
             templateUrl: 'app/modules/Finance/views/AssetCustodian.html',
             controller: 'AssetCustodianCont'
         })
         .state('main.Fin013', {
             url: "/ViewEditGLPeriodLedger",
             templateUrl: 'app/modules/Finance/views/ViewEditGLPeriodLedger.html',
             controller: 'ViewEditGLPeriodLedgerCont'
         })
        .state('main.Fin016', {
            url: "/SubledgerMaster",
            templateUrl: 'app/modules/Finance/views/SubledgerMaster.html',
            controller: 'SubledgerMasterCont'
        })
        .state('main.Fin012', {
            url: "/CreateEditFinancialPeriod",
            templateUrl: 'app/modules/Finance/views/CreateEditFinancialPeriod.html',
            controller: 'CreateEditFinancialPeriodCont'
        })
         .state('main.Sim022', {
             url: "/FeeType",
             templateUrl: 'app/modules/Fee/views/FeeType.html',
             controller: 'FeeTypeCont'
         })

        .state('main.ComEMD', {
            url: "/EmailDetails",
            templateUrl: 'app/modules/Collaboration/views/EmailDetails.html',
            controller: 'EmailDetailsCont'
        })

          .state('main.Com018', {
              url: "/ViewCirculars",
              templateUrl: 'app/modules/Collaboration/views/ViewCircular.html',
              controller: 'ViewCircularCont'
          })

            .state('main.CirDet', {
                url: "/CircularDetails",
                templateUrl: 'app/modules/Collaboration/views/CircularDetails.html',
                controller: 'CircularDetailsCont'
            })

			.state('main.CirDAD', {
			    url: "/CircularDetailsAD",
			    templateUrl: 'app/modules/Collaboration/views/CircularDetailsAD.html',
			    controller: 'CircularDetailsADCont'
			})

         .state('main.CirBrs', {
             url: "/ViewCirculars_BRS",
             templateUrl: 'app/modules/Collaboration/views/ViewCircular_BRS.html',
             controller: 'ViewCircularCont'
         })

         .state('main.Sim166', {
             url: "/Alert",
             templateUrl: 'app/modules/Collaboration/views/Alert.html',
             controller: 'AlertCont',
             params: { flag: '', curnum: '' }

         })

////

        .state('main.AlConf', {
            url: "/AlertConfig",
            templateUrl: 'app/modules/Collaboration/views/AlertConfig.html',
            controller: 'AlertConfigCont'
        })


            .state('main.SimTSF', {
                url: "/SFSTransport",
                templateUrl: 'app/modules/Fee/views/SFSTransport.html',
                controller: 'SFCTransportController',
                params: { 'sel': {} }
            })




 .state('main.SimTFC', {
     url: "/TransportFeeTransaction",
     templateUrl: 'app/modules/Fee/views/TransportFeeTransaction.html',
     controller: 'TransportFeeTransactionCont',
     params: { 'IP': {}, 'sel': {} }
 })


        .state('main.Com017', {
            url: "/CreateCirculars",
            templateUrl: 'app/modules/Collaboration/views/CreateCircular.html',
            controller: 'CreateCircularCont'
        })

        .state('main.simapp', {
            url: "/CollaborationDetails",
            templateUrl: 'app/modules/Collaboration/views/CollaborationDetails.html',
            controller: 'CollaborationDetailsCont'
        })


         .state('main.DefineQuota', {
             url: "/DefineAdmissionQuota",
             templateUrl: 'app/modules/Student/views/DefineQuota.html',
             controller: 'DefineQuota'
         })

    .state('main.AR_R20', {
        url: "/AttendanceRuleReport",
        templateUrl: 'app/modules/Student/views/AttendanceRuleReport.html',
        controller: 'AttendanceRuleReportCont'
    })



                .state('main.ASNR01', {
                    url: "/AbsentStudentNotificationReport",
                    templateUrl: 'app/modules/Student/views/AbsentStudentNotificationReport.html',
                    controller: 'AbsentStudentNotificationReportCont'
                })


        .state('main.Sim715', {
            url: "/FeeCatChange",
            templateUrl: 'app/modules/Fee/views/FeeCategoryChange.html',
            controller: 'FeeCategoryChangeController'
        })
        .state('main.Sim566', {
            url: "/UpdateFeeCategory",
            templateUrl: 'app/modules/Student/views/UpdateFeeCategory.html',
            controller: 'UpdateFeeCategoryCont'
        })
        .state('main.stdEA', {
            url: "/StudentEANumber",
            templateUrl: 'app/modules/Student/views/StudentEANumber.html',
            controller: 'StudentEANumberCont'
        })


          .state('main.Sim527', {
              url: "/AgendaCreation",
              templateUrl: 'app/modules/Gradebook/views/AgendaCreation.html',
              controller: 'AgendaCreationCont'
          })

		            .state('main.ActPnt', {
		                url: "/ActivityPointEntrY",
		                templateUrl: 'app/modules/Gradebook/views/ActivityPointEntry.html',
		                controller: 'ActivityPointEntryCont'
		            })

             .state('main.Sim527G', {
                 url: "/AgendaCreationGeis",
                 templateUrl: 'app/modules/Gradebook/views/AgendaCreationGeis.html',
                 controller: 'AgendaCreationGeisCont'
             })

         .state('main.Inv001', {
             url: "/UnitOfMeasurement",
             templateUrl: 'app/modules/Inventory/views/UnitOfMeasurement.html',
             controller: 'UnitOfMeasurementCont'
         })

         .state('main.Inv002', {
             url: "/UomConversions",
             templateUrl: 'app/modules/Inventory/views/UomConversions.html',
             controller: 'UomConversionsCont'
         })

        .state('main.Inv009', {
            url: "/SupplierDescriptionTypes",
            templateUrl: 'app/modules/Inventory/views/SupplierDescriptionTypes.html',
            controller: 'SupplierDescriptionTypesCont'
        })
         .state('main.Inv010', {
             url: "/SupplierDescription",
             templateUrl: 'app/modules/Inventory/views/SupplierDescription.html',
             controller: 'SupplierDescriptionCont'
         })

           .state('main.Inv100', {
               url: "/Bin",
               templateUrl: 'app/modules/Inventory/views/Bins.html',
               controller: 'BinsCont'
           })

         .state('main.Inv126', {
             url: "/SalesType",
             templateUrl: 'app/modules/Inventory/views/SalesType.html',
             controller: 'SalesTypeCont'
         })

          .state('main.Inv046', {
              url: "/AdjustmentReason",
              templateUrl: 'app/modules/Inventory/views/AdjustmentReason.html',
              controller: 'AdjustmentReasonCont'
          })

          .state('main.Inv128', {
              url: "/SaleTypeMarkUp",
              templateUrl: 'app/modules/Inventory/views/SaleTypeMarkUp.html',
              controller: 'SaleTypeMarkUpCont'
          })


        //Inventory Module

            .state('main.Inv020', {
                url: "/CompanyParameter",
                templateUrl: 'app/modules/Inventory/views/CompanyParameter.html',
                controller: 'CompanyParameterCont'
            })

     .state('main.Inv071', {
         url: "/DocumentType",
         templateUrl: 'app/modules/Inventory/views/DocumentType.html',
         controller: 'DocumentTypeCont'
     })

    .state('main.Inv018', {
        url: "/CreateCompany",
        templateUrl: 'app/modules/Inventory/views/CreateCompany.html',
        controller: 'CreateCompanyCont'
    })

    .state('main.Inv131', {
        url: "/ROLSettings",
        templateUrl: 'app/modules/Inventory/views/ROLSettings.html',
        controller: 'ROLSettingsCont'
    })

      //.state('main.Inv058', {
      //    url: "/RequestDetails",
      //    templateUrl: 'app/modules/Inventory/views/RequestDetails.html',
      //    controller: 'RequestDetailsCont'
      //})

    //.state('main.Inv044', {
    //    url: "/CreateOrder",
    //    templateUrl: 'app/modules/Inventory/views/CreateOrder.html',
    //    controller: 'CreateOrderCont'
    //})

        .state('main.InSREQ', {
            url: "/StoresRequest",
            templateUrl: 'app/modules/Inventory/views/StoresRequest.html',
            controller: 'StoresRequestCont'
        })

        .state('main.ApIREQ', {
            url: "/ItemRequestApproval",
            templateUrl: 'app/modules/Inventory/views/ItemRequestApprove.html',
            controller: 'ItemRequestApproveCont'
        })

        .state('main.Inv044', {
            url: "/CreateOrder",
            templateUrl: 'app/modules/Inventory/views/CrOrder.html',
            controller: 'CrOrderCont'
        })

 .state('main.CRDDOC', {
     url: "/CreateOrderDocuments",
     templateUrl: 'app/modules/Inventory/views/CrOrder_docs.html',
     controller: 'CrOrder_docsCont'
 })

         .state('main.ordapp', {
             url: "/ApproveOrder",
             templateUrl: 'app/modules/Inventory/views/OrderApprove.html',
             controller: 'OrderApproveCont'
         })

        .state('main.CrtRFQ', {
            url: "/CreateRfq",
            templateUrl: 'app/modules/Inventory/views/Rfq.html',
            controller: 'RfqCont'
        })


         .state('main.RcvRFQ', {
             url: "/ReceiveRfq",
             templateUrl: 'app/modules/Inventory/views/ReceiveRfq.html',
             controller: 'ReceiveRfqCont'
         })

        .state('main.SupRFQ', {
            url: "/SupplierComparison",
            templateUrl: 'app/modules/Inventory/views/SupplierComparison.html',
            controller: 'SupplierComparisonCont'
        })

         .state('main.DirOrd', {
             url: "/DirectPO",
             templateUrl: 'app/modules/Inventory/views/DirectOrder.html',
             controller: 'DirectOrderCont'
         })


          .state('main.Inv021', {
              url: "/ItemMaster",
              templateUrl: 'app/modules/Inventory/views/ItemMaster.html',
              controller: 'ItemMasterCont'
          })


         .state('main.Inv143', {
             url: "/InterestType",
             templateUrl: 'app/modules/Inventory/views/InterestType.html',
             controller: 'InterestTypeCont'
         })

           .state('main.Inv027', {
               url: "/PurchaseExpensesType",
               templateUrl: 'app/modules/Inventory/views/PurchaseExpensesType.html',
               controller: 'PurchaseExpensesTypeCont'
           })
          .state('main.Inv133', {
              url: "/CustomClearance",
              templateUrl: 'app/modules/Inventory/views/CustomClearance.html',
              controller: 'CustomClearanceCont'
          })
          .state('main.Inv134', {
              url: "/CustomExpenses",
              templateUrl: 'app/modules/Inventory/views/CustomExpenses.html',
              controller: 'CustomExpensesCont'
          })
          .state('main.Inv132_1', {
              url: "/CreateCostingSheet",
              templateUrl: 'app/modules/Inventory/views/CreateCostingSheet.html',
              controller: 'CreateCostingSheetCont'
          })
        .state('main.Inv132', {
            url: "/CostingSheet",
            templateUrl: 'app/modules/Inventory/views/CostingSheet.html',
            controller: 'CostingSheetCont'
        })
         //Inventory Module END
          .state('main.Sim092', {
              url: "/PortalReference",
              templateUrl: 'app/modules/Setup/views/PortalReference.html',
              controller: 'PortalReferenceCont'
          })

        .state('main.LerArb', {
            url: "/LearnArabic",
            templateUrl: 'app/modules/Setup/views/LearnArabic.html',
            controller: 'LearnArabicCont'
        })


         .state('main.Sim526', {
             url: "/UploadDocument",
             templateUrl: 'app/modules/Student/views/UploadRemainingDocument.html',
             controller: 'UploadRemainingDocumentCont'
         })

          .state('main.Sim523', {
              url: "/UploadDocumentNew",
              templateUrl: 'app/modules/Student/views/UploadRemainingDocumentNew.html',
              controller: 'UploadRemainingDocumentNewCont'
          })

             .state('main.AccInt', {
                 url: "/Acceptance",
                 templateUrl: 'app/modules/Student/views/Acceptance.html',
                 controller: 'AcceptanceCont'
             })
          .state('main.Sim184', {
              url: "/BoardExam",
              templateUrl: 'app/modules/SchoolSetup/views/BoardExam.html',
              controller: 'BoardExamCont'
          })

          .state('main.Sim183', {
              url: "/Board",
              templateUrl: 'app/modules/SchoolSetup/views/Board.html',
              controller: 'BoardCont'
          })



          .state('main.Sim185', {
              url: "/StudentBoardExam",
              templateUrl: 'app/modules/Student/views/StudentBoardExam.html',
              controller: 'StudentBoardExamCont'
          })

         //.state('main.Sim525', {
         //    url: "/TeacherAssignmentUpload",
         //    templateUrl: 'app/modules/Setup/views/TeacherAssignmentUpload.html',
         //    controller: 'TeacherAssignmentUploadCont'
         //})


          .state('main.Si509A', {
              url: "/AssignmentViewWithApproval",
              templateUrl: 'app/modules/Assignment/views/AssignmentViewWithApproval.html',
              controller: 'AssignmentViewWithApprovalCont'
          })

        .state('main.Si508A', {
            url: "/AssignmentApprove",
            templateUrl: 'app/modules/Assignment/views/AssignmentApprove.html',
            controller: 'AssignmentApproveCont'
        })


          .state('main.Sim509', {
              url: "/TeacherAssignmentView",
              templateUrl: 'app/modules/Assignment/views/TeacherAssignmentView.html',
              controller: 'TeacherAssignmentViewCont'
          })

          .state('main.Sim508', {
              url: "/TeacherAssignmentUpload",
              templateUrl: 'app/modules/Assignment/views/TeacherAssignmentUpload.html',
              controller: 'TeacherAssignmentUploadCont'
          })

        .state('main.Sim685', {
            url: "/AddExamPaper",
            templateUrl: 'app/modules/Assignment/views/AddExamPaper.html',
            controller: 'AddExamPaperCont'
        })
          .state('main.Per255', {
              url: "/EmployeeImageView",
              templateUrl: 'app/modules/HRMS/views/EmployeeImageView.html',
              controller: 'EmployeeImageViewCont'
          })

          .state('main.Per021', {
              url: "/EmployeeGrade",
              templateUrl: 'app/modules/Attendance/views/EmployeeGrade.html',
              controller: 'EmployeeGradeCont'
          })

        .state('main.Sim612', {
            url: "/HomeRoomAttendance",
            templateUrl: 'app/modules/Attendance/views/HomeRoomAttendance.html',
            controller: 'HomeRoomAttendanceCont'
        })


         .state('main.Per254', {
             url: "/DefineCategory",
             templateUrl: 'app/modules/HRMS/views/DefineCatgory.html',
             controller: 'DefineCategoryCont'
         })


         .state('main.Per061', {
             url: "/OtherLoan",
             templateUrl: 'app/modules/HRMS/views/OtherLoan.html',
             controller: 'OtherLoanCont'
         })

           .state('main.Per090', {
               url: "/SettlementType",
               templateUrl: 'app/modules/HRMS/views/SettlementType.html',
               controller: 'SettlementTypeCont'
           })

         .state('main.Pers312', {
             url: "/EmployeeDocumentUpload",
             templateUrl: 'app/modules/HRMS/views/EmployeeDocumentUpload.html',
             controller: 'EmployeeDocumentUploadCont'
         })

         .state('main.Fin010', {
             url: "/CreateEditDepartment",
             templateUrl: 'app/modules/Finance/views/CreateEditDepartment.html',
             controller: 'CreateEditDepartmentCont'
         })

        .state('main.Fin116', {
            url: "/ScheduleGroupDetails",
            templateUrl: 'app/modules/Finance/views/ScheduleGroupDetails.html',
            controller: 'ScheduleGroupDetailsCont'
        })

         .state('main.Fin117', {
             url: "/ScheduleGroup",
             templateUrl: 'app/modules/Finance/views/ScheduleGroup.html',
             controller: 'ScheduleGroupCont'
         })







             .state('main.Fin239', {
                 url: "/Chartaccount",
                 templateUrl: 'app/modules/Finance/views/Chartaccount.html',
                 controller: 'ChartaccountCont'
             })
          .state('main.Sim175', {
              url: "/StudentImageView",
              templateUrl: 'app/modules/Student/views/StudentImageView.html',
              controller: 'StudentImageViewCont'
          })
         .state('main.Com016', {
             url: "/ViewNews",
             templateUrl: 'app/modules/Collaboration/views/ViewNews.html',
             controller: 'ViewNewsCont'
         })

              .state('main.SimAEH', {
                  url: "/AdmissionEmailHistory",
                  templateUrl: 'app/modules/Student/views/AdmissionEmailHistory.html',
                  controller: 'AdmissionEmailHistoryCont'
              })


        .state('main.NewBrs', {
            url: "/ViewNews_BRS",
            templateUrl: 'app/modules/Collaboration/views/ViewNews_BRS.html',
            controller: 'ViewNewsCont'
        })
        .state('main.Com015', {
            url: "/CreateNews",
            templateUrl: 'app/modules/Collaboration/views/CreateNews.html',
            controller: 'CreateNewsCont'
        })

        .state('main.InvFee', {
            url: "/InventoryFeePosting",
            templateUrl: 'app/modules/Inventory/views/InventoryFeePosting.html',
            controller: 'InventoryFeePostingCont'
        })

         .state('main.NewUpp', {
             url: "/CreateNews1",
             templateUrl: 'app/modules/Collaboration/views/CreateNw.html',
             controller: 'CreateNwCont'
         })

        .state('main.VNewup', {
            url: "/ViewNews_MultipleFiles",
            templateUrl: 'app/modules/Collaboration/views/ViewNews_MultipleFiles.html',
            controller: 'ViewNews_MultipleFilesCont'
        })

           .state('main.Fn102S', {
               url: "/PDCSubmission",
               templateUrl: 'app/modules/Finance/views/PDCSubmission.html',
               controller: 'PDCSubmissionCont'
           })

               .state('main.Fn102V', {
                   url: "/PDCSubmission_ABQIS",
                   templateUrl: 'app/modules/Finance/views/PDCSubmission_ABQIS.html',
                   controller: 'PDCSubmission_ABQISCont'
               })

         .state('main.cir', {
             url: "/Circulars",
             templateUrl: 'app/modules/Collaboration/views/Circular.html',
             controller: 'CircularCont'
         })
             .state('main.news', {
                 url: "/News",
                 templateUrl: 'app/modules/Collaboration/views/News.html',
                 controller: 'NewsCont'
             })

        .state('main.payslp', {
            url: "/EmployeePayslip",
            templateUrl: 'app/modules/HRMS/views/EmailEmployeePayslip.html',
            controller: 'EmailEmployeePayslipCont'
        })

         .state('main.Sim021', {
             url: "/FeeCategory",
             templateUrl: 'app/modules/Fee/views/FeeCategory.html',
             controller: 'FeeCategoryCont'
         })


         .state('main.Sim531', {
             url: "/StudentPayingAgent",
             templateUrl: 'app/modules/Fee/views/StudentPayingAgent.html',
             controller: 'StudentPayingAgentCont'
         })

         .state('main.Sim532', {
             url: "/PayingAgentMapping",
             templateUrl: 'app/modules/Fee/views/PayingAgentMapping.html',
             controller: 'PayingAgentMappingCont'
         })

        .state('main.Per330', {
            url: "/UpdateHoldEmpStatus",
            templateUrl: 'app/modules/HRMS/views/UpdateHoldEmpStatus.html',
            controller: 'UpdateHoldEmpStatusCont'
        })

            .state('main.Per352', {
                url: "/PayrollPubFin",
                templateUrl: 'app/modules/HRMS/views/PayrollPubFinStatus.html',
                controller: 'PayrollPubFinStatusCont'
            })


            .state('main.EPSR01', {
                url: "/EmployeePaySlipReport",
                templateUrl: 'app/modules/HRMS/views/EmployeePaySlipReport.html',
                controller: 'EmployeePaySlipReportCont'


            }).state('main.Per078', {
                url: "/Payroll_Genration",
                templateUrl: 'app/modules/HRMS/views/GeneratePayroll.html',
                controller: 'GeneratePayrollCont'
            }).state('main.Per354', {
                url: "/EmplDependent",
                templateUrl: 'app/modules/HRMS/views/EmplDependent.html',
                controller: 'EmplDependentCont'
            })


          .state('main.Per356', {
              url: "/UEmpDest",
              templateUrl: 'app/modules/HRMS/views/UpdateEmpDestination.html',
              controller: 'UpdateEmpDestinationCont'
          })




         .state('main.Per400', {
             url: "/PayrollPosting",
             templateUrl: 'app/modules/HRMS/views/PayrollPosting.html',
             controller: 'PayrollPostingCont'
         })

         .state('main.Per314', {
             url: "/UpdateLeaveBalance",
             templateUrl: 'app/modules/HRMS/views/UpdateLeaveBal.html',
             controller: 'UpdateLeaveBalCont'
         })

        .state('main.Per340', {
            url: "/UpdatePayrollStatus",
            templateUrl: 'app/modules/HRMS/views/UpdatePayrollStatus.html',
            controller: 'UpdatePayrollStatusCont'
        })

        .state('main.Per072', {
            url: "/PayrollEmployee",
            templateUrl: 'app/modules/HRMS/views/PayrollEmployee.html',
            controller: 'PayrollEmployeeCont'
        })

          .state('main.Pr072A', {
              url: "/PayrollEmployeeDPSD",
              templateUrl: 'app/modules/HRMS/views/PayrollEmployeeDPSD.html',
              controller: 'PayrollEmployee_DPSDCont'
          })

        .state('main.Per351', {
            url: "/PayableIncrement",
            templateUrl: 'app/modules/HRMS/views/PayableIncrement.html',
            controller: 'PayableIncrementCont'
        })

         .state('main.Per350', {
             url: "/PayrollEmployee_1",
             templateUrl: 'app/modules/HRMS/views/PayrollEmployee_1.html',
             controller: 'PayrollEmployee_1Cont'
         })

         .state('main.Sim045', {
             url: "/Subject",
             templateUrl: 'app/modules/Setup/views/Subject.html',
             controller: 'SubjectCont'
         })






       .state('main.Sim046', {
           url: "/SubjectGroup",
           templateUrl: 'app/modules/Setup/views/SubjectGroup.html',
           controller: 'SubjectGroupCont'
       })
      .state('main.Sim047', {
          url: "/SubjectType",
          templateUrl: 'app/modules/Setup/views/SubjectType.html',
          controller: 'SujectTypeCont'
      })


     .state('main.Sim031', {
         url: "/Parameter",
         templateUrl: 'app/modules/Setup/views/Parameter.html',
         controller: 'ParameterCont'
     })


     .state('main.Schpar', {
         url: "/SchoolParameter",
         templateUrl: 'app/modules/Setup/views/SchoolParameter.html',
         controller: 'SchoolParameterCont'
     })


      .state('main.RepoPa', {
          url: "/ReportingParameter",
          templateUrl: 'app/modules/Setup/views/ReportingParameter.html',
          controller: 'ReportingParameterCont'
      })


        .state('main.Sim012', {
            url: "/AdmissionCriteria",
            templateUrl: 'app/modules/Student/views/AdmissionCriteria.html',
            controller: 'AdmissionCriteriaCont'
        })

       .state('main.Ucw241', {
           url: "/Goal",
           templateUrl: 'app/modules/HRMS/views/Goal.html',
           controller: 'GoalCont'
       })

         .state('main.Sim038', {
             url: "/SectionScreening",
             templateUrl: 'app/modules/Student/views/SectionScreening.html',
             controller: 'SectionScreeningCont'
         })


       .state('main.ChgAca', {
           url: "/ChangeAcademicYear",
           templateUrl: 'app/modules/Student/views/ChangeAcademicYear.html',
           controller: 'ChangeAcademicYearCont'
       })


      .state('main.mapStd', {
          url: "/Maping",
          templateUrl: 'app/modules/Student/views/Maping.html',
          controller: 'MapingCont'
      })

.state('main.mapPar', {
    url: "/ParentEmpMaping",
    templateUrl: 'app/modules/Student/views/ParentEmployeeMapping.html',
    controller: 'ParentEmployeeMappingCont'
})

      .state('main.Si010C', {
          url: "/CancelAdmission",
          templateUrl: 'app/modules/Student/views/CancelAdmission.html',
          controller: 'CancelAdmissionCont'
      })

        .state('main.SimSht', {
            url: "/AdmissionShort",
            templateUrl: 'app/modules/Student/views/AdmissionShort.html',
            controller: 'AdmissionShortCont'
        })

         .state('main.AjbSht', {
             url: "/AdmissionShort_ajb",
             templateUrl: 'app/modules/Student/views/AdmissionShort_ajb.html',
             controller: 'AdmissionShort_ajbCont'
         })


        .state('main.QfsSht', {
            url: "/AdmissionShort_qfis",
            templateUrl: 'app/modules/Student/views/AdmissionShort_qfis.html',
            controller: 'AdmissionShort_qfisCont'
        })

           .state('main.SNSht', {
               url: "/AdmissionShort_SN",
               templateUrl: 'app/modules/Student/views/AdmissionShort_SN.html',
               controller: 'AdmissionShort_SNCont'
           })

         .state('main.PeaSht', {
             url: "/AdmissionShort_pearl",
             templateUrl: 'app/modules/Student/views/AdmissionShort_pearl.html',
             controller: 'AdmissionShort_pearlCont'
         })

         .state('main.SbiSht', {
             url: "/AdmissionShort_sbis",
             templateUrl: 'app/modules/Student/views/AdmissionShort_sbis.html',
             controller: 'AdmissionShort_sbisCont'
         })

      .state('main.Sim010', {
          url: "/Admission",
          templateUrl: 'app/modules/Student/views/Admission.html',
          controller: 'AdmissionCont',
          params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
      })



        .state('main.AdSISO', {
            url: "/Admission_SISO",
            templateUrl: 'app/modules/Student/views/Admission_SISO.html',
            controller: 'Admission_SISOCont',
            params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
        })

          .state('main.Admqfs', {
              url: "/Admission_qfis",
              templateUrl: 'app/modules/Student/views/Admission_qfis.html',
              controller: 'Admission_qfisCont',
              params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
          })

          .state('main.Admael', {
              url: "/Admission_aelc",
              templateUrl: 'app/modules/Student/views/Admission_aelc.html',
              controller: 'Admission_aelcCont',
              params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
          })

        .state('main.Admasd', {
            url: "/Admission_asd",
            templateUrl: 'app/modules/Student/views/Admission_asd.html',
            controller: 'Admission_asdCont',
            params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
        })

        .state('main.AdIhis', {
            url: "/Admission_Ihis",
            templateUrl: 'app/modules/Student/views/Admission_Ihis.html',
            controller: 'Admission_IhisCont',
            params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
        })

         .state('main.AdmSN', {
             url: "/Admission_SN",
             templateUrl: 'app/modules/Student/views/Admission_SN.html',
             controller: 'Admission_SNCont',
             params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
         })


        .state('main.OlAdps', {
            url: "/Admission_dpsdOL",
            templateUrl: 'app/modules/Student/views/Admission_dpsdOL.html',
            controller: 'Admission_dpsdOLCont',
            params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
        })

          .state('main.Admdps', {
              url: "/Admission_dpsd",
              templateUrl: 'app/modules/Student/views/Admission_dpsd.html',
              controller: 'Admission_dpsdCont',
              params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
          })
             .state('main.MYAdmd', {
                 url: "/MyAdmission_dpsd",
                 templateUrl: 'app/modules/Student/views/MYAdmission_dpsd.html',
                 controller: 'MYAdmission_dpsdCont',
                 params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
             })

        .state('main.Admzps', {
            url: "/Admission_zps",
            templateUrl: 'app/modules/Student/views/Admission_zps.html',
            controller: 'Admission_zpsCont',
            params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
        })

         .state('main.Admajb', {
             url: "/Admission_ajb",
             templateUrl: 'app/modules/Student/views/Admission_ajb.html',
             controller: 'Admission_ajbCont',
             params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
         })

        .state('main.Admaji', {
            url: "/Admission_aji",
            templateUrl: 'app/modules/Student/views/Admission_aji.html',
            controller: 'Admission_ajiCont',
            params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
        })

        .state('main.AdmNis', {
            url: "/Admission_Nis",
            templateUrl: 'app/modules/Student/views/Admission_Nis.html',
            controller: 'Admission_NisCont',
            params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
        })

         .state('main.Admiis', {
             url: "/Admission_iis",
             templateUrl: 'app/modules/Student/views/Admission_iis.html',
             controller: 'Admission_iisCont',
             params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
         })



         .state('main.Sim000', {
             url: "/AdmissionDashboard",
             templateUrl: 'app/modules/Student/views/AdmissionDashboard.html',
             controller: 'AdmissionDashboardCont',
             params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
         })



        .state('main.DsSISO', {
            url: "/AdmissionDashboard_SISO",
            templateUrl: 'app/modules/Student/views/AdmissionDashboard_SISO.html',
            controller: 'AdmissionDashboard_SISOCont',
            params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
        })


             .state('main.DsDmc', {
                 url: "/AdmissionDashboard_DMC",
                 templateUrl: 'app/modules/Student/views/AdmissionDashboard_dvps.html',
                 controller: 'AdmissionDashboard_dvpsCont',
                 params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
             })


              .state('main.DsDmc.OldmcNew', {
                  url: "/Admission_oldmc",
                  templateUrl: 'app/modules/Student/views/AdmissionDmcOL.html',
                  controller: 'AdmissionDmcOLCont',
                  params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
              })


            .state('main.Dsadm', {
                url: "/AdmissionEntry",
                templateUrl: 'app/modules/Student/views/admission_entry.html',
                controller: 'Admisison_EntryCont',

            })

         .state('main.Dasqfs', {
             url: "/AdmissionDashboard_qfis",
             templateUrl: 'app/modules/Student/views/AdmissionDashboard_qfis.html',
             controller: 'AdmissionDashboard_qfisCont',
             params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
         })



         .state('main.DasNis', {
             url: "/AdmissionDashboard_Nis",
             templateUrl: 'app/modules/Student/views/AdmissionDashboard_Nis.html',
             controller: 'AdmissionDashboard_NisCont',
             params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
         })

         .state('main.Dasasd', {
             url: "/AdmissionDashboard_asd",
             templateUrl: 'app/modules/Student/views/AdmissionDashboard_asd.html',
             controller: 'AdmissionDashboard_asdCont',
             params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
         })

         .state('main.Daszps', {
             url: "/AdmissionDashboard_zps",
             templateUrl: 'app/modules/Student/views/AdmissionDashboard_zps.html',
             controller: 'AdmissionDashboard_zpsCont',
             params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
         })

         .state('main.aelDas', {
             url: "/AdmissionDashboard_aelc",
             templateUrl: 'app/modules/Student/views/AdmissionDashboard_aelc.html',
             controller: 'AdmissionDashboard_aelcCont',
             params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
         })

         .state('main.DasIhi', {
             url: "/AdmissionDashboard_Ihis",
             templateUrl: 'app/modules/Student/views/AdmissionDashboard_Ihis.html',
             controller: 'AdmissionDashboard_IhisCont',
             params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
         })

        .state('main.DasSN', {
            url: "/AdmissionDashboard_SN",
            templateUrl: 'app/modules/Student/views/AdmissionDashboard_SN.html',
            controller: 'AdmissionDashboard_SNCont',
            params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
        })

        .state('main.Dasadi', {
            url: "/AdmissionDashboard_adisw",
            templateUrl: 'app/modules/Student/views/AdmissionDashboard_adisw.html',
            controller: 'AdmissionDashboard_adiswCont',
            params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
        })

         .state('main.Dasajb', {
             url: "/AdmissionDashboard_ajb",
             templateUrl: 'app/modules/Student/views/AdmissionDashboard_ajb.html',
             controller: 'AdmissionDashboard_ajbCont',
             params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
         })

         .state('main.Dasiis', {
             url: "/AdmissionDashboard_iis",
             templateUrl: 'app/modules/Student/views/AdmissionDashboard_iis.html',
             controller: 'AdmissionDashboard_iisCont',
             params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
         })

        .state('main.Dasaji', {
            url: "/AdmissionDashboard_aji",
            templateUrl: 'app/modules/Student/views/AdmissionDashboard_aji.html',
            controller: 'AdmissionDashboard_ajiCont',
            params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
        })


         .state('main.Dasdps', {
             url: "/AdmissionDashboard_dpsd",
             templateUrl: 'app/modules/Student/views/AdmissionDashboard_dpsd.html',
             controller: 'AdmissionDashboard_dpsdCont',
             params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
         })


             .state('main.Dasvac', {
                 url: "/RecruitmentDashboard",
                 templateUrl: 'app/modules/HRMS/views/RecruitmentDashboard.html',
                 controller: 'RecruitmentDashboardCont',
                 params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
             })


         .state('main.Dasiso', {
             url: "/AdmissionDashboards_siso",
             templateUrl: 'app/modules/Student/views/AdmissionDashboard_siso_new.html',
             controller: 'AdmissionDashboard_siso_newCont',
             params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
         })

        .state('main.IISSht', {
            url: "/AdmissionShortIIS",
            templateUrl: 'app/modules/Student/views/AdmissionShort_IIS.html',
            controller: 'AdmissionShort_IISCont'
        })


         .state('main.brsSht', {
             url: "/AdmissionShortBrs",
             templateUrl: 'app/modules/Student/views/AdmissionShort_brs.html',
             controller: 'AdmissionShort_brsCont'
         })

           .state('main.sisSht', {
               url: "/AdmissionShortSISO",
               templateUrl: 'app/modules/Student/views/AdmissionShort_SISO.html',
               controller: 'AdmissionShort_SISOCont'
           })


         .state('main.NisSht', {
             url: "/AdmissionShortNis",
             templateUrl: 'app/modules/Student/views/AdmissionShort_NIS.html',
             controller: 'AdmissionShort_NISCont'
         })

         .state('main.admbrs', {
             url: "/AdmissionBrs",
             templateUrl: 'app/modules/Student/views/Admission_brs.html',
             controller: 'Admission_brsCont',
             params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
         })



          .state('main.Dasbrs', {
              url: "/AdmissionDashboardBrs",
              templateUrl: 'app/modules/Student/views/AdmissionDashboard_brs.html',
              controller: 'AdmissionDashboard_brsCont',
              params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
          })

         .state('main.LPOGRN', {
             url: "/LPOGRN",
             templateUrl: 'app/modules/Inventory/views/LpoGrn.html',
             controller: 'LpoGrnCont'
         })
       .state('main.Sim003', {
           url: "/Curriculum",
           templateUrl: 'app/modules/Setup/views/Curriculum.html',
           controller: 'CurriculumCont'
       })

        .state('main.Sim201', {
            url: "/StudentDevice",
            templateUrl: 'app/modules/Student/views/StudentDevice.html',
            controller: 'StudentDeviceCont'
        })

        .state('main.Sim108', {
            url: "/RequestCertificate",
            templateUrl: 'app/modules/Student/views/RequestCertificate.html',
            controller: 'RequestCertificateCont'
        })

        .state('main.Sim505', {
            url: "/CertificateTCParameter",
            templateUrl: 'app/modules/Student/views/CertificateTCParameter.html',
            controller: 'CertificateTCParameterCont'
        })


         .state('main.cetsis', {
             url: "/CertificateTCParameterTsis",
             templateUrl: 'app/modules/Student/views/CertificateTCParameterTsis.html',
             controller: 'CertificateTCParameterTsisCont'
         })


            .state('main.cetdvp', {
                url: "/CertificateTCParameterDvps",
                templateUrl: 'app/modules/Student/views/CertificateTCParameterDvps.html',
                controller: 'CertificateTCParameterDvpsCont'
            })

       .state('main.cersis', {
           url: "/CertificateTCParameterSisqatar",
           templateUrl: 'app/modules/Student/views/CertificateTCParameterSisqatar.html',
           controller: 'CertificateTCParameterSisqatarCont'
       })

        .state('main.Sim681', {
            url: "/CertificateTCParameterBrs",
            templateUrl: 'app/modules/Student/views/CertificateTCParameter_brs.html',
            controller: 'CertificateTCParameter_brsCont'
        })

        .state('main.Sim684', {
            url: "/CertificateTCApprove_Pearl",
            templateUrl: 'app/modules/Student/views/CertificateTCApprove_Pearl.html',
            controller: 'CertificateTCApprove_PearlCont'
        })

        .state('main.Sim776', {
            url: "/CertificateTCParameter_IIS",
            templateUrl: 'app/modules/Student/views/CertificateTCParameter_IIS.html',
            controller: 'CertificateTCParameter_IISCont'
        })
         .state('main.Sim186', {
             url: "/HeadTeacherMapping",
             templateUrl: 'app/modules/SchoolSetup/views/HeadTeacherMapping.html',
             controller: 'HeadTeacherMappingCont'
         })

        .state('main.Sim187', {
            url: "/SupervisorMapping",
            templateUrl: 'app/modules/SchoolSetup/views/SupervisorMapping.html',
            controller: 'SupervisorMappingCont'
        })

      .state('main.Sim001', {
          url: "/AcademicYear",
          templateUrl: 'app/modules/Setup/views/AcademicYear.html',
          controller: 'AcademicYearCont'
      })

         .state('main.Sim023', {
             url: "/MasterStoredProcedureList",
             templateUrl: 'app/modules/Setup/views/ComnMasterStoredProcedure.html',
             controller: 'ComnMasterStoredProcedureCont'
         })

          .state('main.Sim518', {
              url: "/AttendanceRule",
              templateUrl: 'app/modules/Student/views/AttendanceRule.html',
              controller: 'AttendanceRuleCont'
          })
        .state('main.Per308', {
            url: "/ActiveUser",
            templateUrl: 'app/modules/HRMS/views/ActiveUser.html',
            controller: 'ActiveUserCont'
        })

     .state('main.Sim035', {
         url: "/Section",
         templateUrl: 'app/modules/Setup/views/Section.html',
         controller: 'SectionCont1'
     })
         .state('main.Sim164', {
             url: "/Moderator",
             templateUrl: 'app/modules/Setup/views/Moderator.html',
             controller: 'ModeratorCont'
         })
        .state('main.Com004', {
            url: "/CommonSequence",
            templateUrl: 'app/modules/Setup/views/CommonSequence.html',
            controller: 'CommonSequenceCont'
        })
        .state('main.Sim165', {
            url: "/ModeratorUser",
            templateUrl: 'app/modules/Setup/views/ModeratorUser.html',
            controller: 'ModeratorUserCont'
        })
         .state('main.Sim155', {
             url: "/StudentAttendance",
             templateUrl: 'app/modules/Attendance/views/StudentAttendance.html',
             controller: 'StudentAttendanceCont'
         })

             .state('main.SimRut', {
                 url: "/StudentAttendanceRought",
                 templateUrl: 'app/modules/Attendance/views/StudentAttendanceRought.html',
                 controller: 'StudentAttendanceRoughtCont'
             })

             .state('main.SimGoa', {
                 url: "/StudentAttendance_Goa",
                 templateUrl: 'app/modules/Attendance/views/StudentAttendance.html',
                 controller: 'StudentAttendance_GoaCont'
             })

           .state('main.Sim044', {
               url: "/UpdateStudentSection",
               templateUrl: 'app/modules/Student/views/UpdateStudentSection.html',
               controller: 'UpdateStudentSectionController'
           })

            .state('main.Sim04A', {
                url: "/UpdateStudentSectionA",
                templateUrl: 'app/modules/Student/views/UpdateStudentSectionA.html',
                controller: 'UpdateStudentSectionACont'
            })

            .state('main.Sim008', {
                url: "/Nationality",
                templateUrl: 'app/modules/Setup/views/Nationality.html',
                controller: 'NationalityController'
            })

            .state('main.Sim009', {
                url: "/Religion",
                templateUrl: 'app/modules/Setup/views/Religion.html',
                controller: 'ReligionController'
            })
           .state('main.Sim005', {
               url: "/GradeLevel",
               templateUrl: 'app/modules/Setup/views/GradeLevel.html',
               controller: 'GradeLevelCont'
           })
           .state('main.Sim004', {
               url: "/CurriculumLevel",
               templateUrl: 'app/modules/Setup/views/CurriculumLevel.html',
               controller: 'CurriculumLevelController'
           })

          .state('main.Sim024', {
              url: "/Grade",
              templateUrl: 'app/modules/SchoolSetup/views/Grade.html',
              controller: 'GradeCont'
          })
           .state('main.Sim054', {
               url: "/Term",
               templateUrl: 'app/modules/SchoolSetup/views/Term.html',
               controller: 'TermCont'
           })

        .state('main.Sim036', {
            url: "/SectionFee",
            templateUrl: 'app/modules/Fee/views/SectionFee.html',
            controller: 'SectionFeeController'
        })
       .state('main.Sim017', {
           url: "/Fee Concession",
           templateUrl: 'app/modules/Fee/views/StudentFeeConcession.html',
           controller: 'StudentFeeConcessionCont'
       })
       .state('main.Sim018', {
           url: "/StudentFeeConcessionTransaction",
           templateUrl: 'app/modules/Fee/views/StudentFeeConcessionTransaction.html',
           controller: 'StudentFeeConcessionTransactionCont'
       })

         .state('main.Sim016', {
             url: "/ConcessionCancel",
             templateUrl: 'app/modules/Fee/views/ConcessionCancel.html',
             controller: 'ConcessionCancelCont'
         })

             .state('main.sim707', {
                 url: "/Employee2",
                 templateUrl: 'app/modules/Setup/views/Employee2.html',
                 controller: 'Employee2Cont'
             })


       .state('main.Sim043', {
           url: "/FeeDet",
           templateUrl: 'app/modules/Fee/views/SFS.html',
           controller: 'SFCController',
           params: { 'sel': {} }
       })

       .state('main.VAT043', {
           url: "/FeeDet_VAT",
           templateUrl: 'app/modules/Fee/views/SFS_VAT.html',
           controller: 'SFCController_VAT',
           params: { 'sel': {} }
       })

            .state('main.lmsAsd', {
                url: "/lmsAsd",
                templateUrl: 'app/modules/Common/views/lms_asd.html',
                controller: 'Lms_asdController'
            })


               .state('main.lmsAll', {
                   url: "/lmsAll",
                   templateUrl: 'app/modules/Common/views/lms_all.html',
                   controller: 'Lms_allController'
               })






         .state('main.Sim601', {
             url: "/Sim601",
             templateUrl: 'app/modules/Fee/views/SiblingConcession.html',
             controller: 'SiblingConcessionController',
         })
        .state('main.Sim043SearchSBISMFB', {
            url: "/FeeTransaction1",
            templateUrl: 'app/modules/Fee/views/FeeTransactionSBISMFB.html',
            controller: 'FeeTransactionControllerSBISMFB',
            params: { 'IP': {}, 'sel': {} }
        })
        .state('main.Sim043Search', {
            url: "/FeeTransaction2",
            templateUrl: 'app/modules/Fee/views/FeeTransaction.html',
            controller: 'FeeTransactionController',
            params: { 'IP': {}, 'sel': {} }
        })
        .state('main.Sim043Search_VAT', {
            url: "/FeeTransaction_VAT",
            templateUrl: 'app/modules/Fee/views/FeeTransaction_VAT.html',
            controller: 'FeeTransaction_VATController',
            params: { 'IP': {}, 'sel': {} }
        })
        .state('main.Sim043SBIS', {
            url: "/FeeTransactionn",
            templateUrl: 'app/modules/Fee/views/FeeTransactionSBIS.html',
            controller: 'FeeTransactionControllerSBIS',
            params: { 'IP': {}, 'sel': {} }
        })


        .state('main.Sim043SBISMFB', {
            url: "/FeeTransactionnSMFB",
            templateUrl: 'app/modules/Fee/views/FeeTransactionSBISMFB.html',
            controller: 'FeeTransactionControllerSBISMFB',
            params: { 'IP': {}, 'sel': {} }
        })

         .state('main.Sim043AJB', {
             url: "/FeeTransactionAJB",
             templateUrl: 'app/modules/Fee/views/FeeTransaction.html',
             controller: 'FeeTransactionControllerAJB',
             params: { 'IP': {}, 'sel': {} }
         })
        .state('main.PA', {
            url: "/FeeTransPayingAgent",
            templateUrl: 'app/modules/Fee/views/FeeTransPayingAgent.html',
            controller: 'FeeTransPayingAgent',
            params: { 'sel': {} }
        })
        //.state('main.PATrans', {
        //    url: "/FeeTransPA",
        //    templateUrl: 'app/modules/Fee/views/FeeTransPayingAgent.html',
        //    controller: 'FeeTransPayingAgent',
        //    params: { 'IP': {}, 'sel': {} }
        //})
      .state('main.Sim178', {
          url: "/CopySectionFee",
          templateUrl: 'app/modules/Fee/views/CopySectionFee.html',
          controller: 'CopySectionFeeCont'
      })

      .state('main.Sim059', {
          url: "/SectionTerm",
          templateUrl: 'app/modules/Student/views/SectionTerm.html',
          controller: 'SectionTermCont'
      })

      .state('main.Com003', {
          url: "/ComnModule",
          templateUrl: 'app/modules/Setup/views/ComnModule.html',
          controller: 'CommonModuleCont'
      })

      .state('main.Com001', {
          url: "/CommonApplication",
          templateUrl: 'app/modules/Setup/views/CommonApplication.html',
          controller: 'CommonApplicationCont'
      })

.state('main.Com002', {
    url: "/CommonApplication_rakmps",
    templateUrl: 'app/modules/Setup/views/CommonApplicationRakMps.html',
    controller: 'CommonApplicationRakMpsCont'
})
      .state('main.Sim089', {
          url: "/StudentSectionSubject",
          templateUrl: 'app/modules/Setup/views/StudentSectionSubject.html',
          controller: 'StudentSectionSubjectCont'
      })


             .state('main.Stu356', {
                 url: "/StudentSectionSubject356",
                 templateUrl: 'app/modules/Setup/views/StudentSectionSubject356.html',
                 controller: 'StudentSectionSubject356Cont'
             })




            .state('main.SimIND', {
                url: "/Admission356",
                templateUrl: 'app/modules/Student/views/Admission356.html',
                controller: 'Admission356Cont',
                params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
            })

            .state('main.SimBas', {
                url: "/BaselineMarksEntry",
                templateUrl: 'app/modules/Student/views/BaselineMarksEntry.html',
                controller: 'BaselineMarksEntryCont'
            })

            // .state('main.SimSas', {
            //    url: "/BaselineMarksEntry",
            //    templateUrl: 'app/modules/Student/views/BaselineMarksEntry.html',
            //    controller: 'BaselineMarksEntryCont'
            //})

      .state('main.Mog001', {
          url: "/SchoolLicence",
          templateUrl: 'app/modules/SchoolSetup/views/SchoolLicence.html',
          controller: 'SchoolLicenceCont'
      })

            .state('main.Sim502', {
                url: "/DefineStudentFee",
                templateUrl: 'app/modules/Fee/views/DefineStudentFee.html',
                controller: 'DefineStudentFeeCont'
            })

    //  //END

    //  //kk
                .state('main.EmpHel', {
                    url: "/checkListSectionMapping",
                    templateUrl: 'app/modules/Health/views/checkListSectionMapping.html',
                    controller: 'checkListSectionMappingCont'
                })

                .state('main.ListSe', {
                    url: "/ListSection",
                    templateUrl: 'app/modules/Health/views/ListSection.html',
                    controller: 'ListSectionCont'
                })

                .state('main.CkLiEv', {
                    url: "/ChecklistEvidences",
                    templateUrl: 'app/modules/Health/views/ChecklistEvidences.html',
                    controller: 'ChecklistEvidencesCont'
                })

                .state('main.EChkls', {
                    url: "/EntryOfCheckListReview",
                    templateUrl: 'app/modules/Health/views/EntryOfCheckListReview.html',
                    controller: 'EntryOfCheckListReviewCont'
                })

            .state('main.ResDat', {
                url: "/ResponseData",
                templateUrl: 'app/modules/Health/views/ResponseData.html',
                controller: 'ResponseDataCont'
            })
      .state('main.EmpSht', {
          url: "/CreateEditEmployee",
          templateUrl: 'app/modules/HRMS/views/CreateEditEmployee.html',
          controller: 'CreateEditEmployeeCont'
      })

         .state('main.STerm', {
             url: "/MedicalStudentTermDetail",
             templateUrl: 'app/modules/Health/views/MedicalStudentTermDetail.html',
             controller: 'MedicalStudentTermDetailCont'
         })

           .state('main.UpdateEmployee', {
               url: "/UpdateEmployee",
               templateUrl: 'app/modules/HRMS/views/UpdateEmployee.html',
               controller: 'UpdateEmployeeCont'
           })

        .state('main.UEmASD', {
            url: "/UpdateEmployeeASD",
            templateUrl: 'app/modules/HRMS/views/UpdateEmployeeASD.html',
            controller: 'UpdateEmployeeASDCont'
        })

            .state('main.Sim015', {
                url: "/SchoolCalender",
                templateUrl: 'app/modules/SchoolSetup/views/SchoolCalender.html',
                controller: 'SchoolCalenderController'
            })


             .state('main.SimNis', {
                 url: "/SchoolCalender_niss",
                 templateUrl: 'app/modules/SchoolSetup/views/SchoolCalender_niss.html',
                 controller: 'SchoolCalenderCont_niss'
             })


            .state('main.SchCal', {
                url: "/SchoolCalenderView",
                templateUrl: 'app/modules/SchoolSetup/views/SchoolCalenderview.html',
                controller: 'SchoolCalenderviewCont'
            })


           .state('main.StaffCalendar', {
               url: "/StaffCalendar",
               templateUrl: 'app/modules/SchoolSetup/views/StaffCalendar.html',
               controller: 'StaffCalenderCont'
           })

         .state('main.Sim548', {
             url: "/StudLeaveApprove",
             templateUrl: 'app/modules/SchoolSetup/views/StudLeaveApprove.html',
             controller: 'StudLeaveApproveCont'
         })

        .state('main.result', {
            url: "/GenerateResult",
            templateUrl: 'app/modules/Gradebook/views/GenerateResult.html',
            controller: 'ResultGenerationCont'
        })

            .state('main.RReslt', {
                url: "/RetestResultGeneration",
                templateUrl: 'app/modules/Gradebook/views/GenerateRetestResult.html',
                controller: 'RetestResultGenerationCont'
            })

         .state('main.Sim540', {
             url: "/StaffCalendarNew",
             templateUrl: 'app/modules/SchoolSetup/views/StaffCalenderNew.html',
             controller: 'StaffCalenderNewCont'
         })

             .state('main.Sfview', {
                 url: "/StaffCalendarView",
                 templateUrl: 'app/modules/SchoolSetup/views/StaffCalenderView.html',
                 controller: 'StaffCalenderViewCont'
             })

      .state('main.Per099', {
          url: "/EmployeeMaster",
          templateUrl: 'app/modules/HRMS/views/EmployeeMaster.html',
          controller: 'EmployeeMasterCont'

      })

          .state('main.EmpASD', {
              url: "/EmployeeMasterASD",
              templateUrl: 'app/modules/HRMS/views/EmployeeMasterASD.html',
              controller: 'EmployeeMasterASDCont'

          })

         .state('main.EmpOES', {
             url: "/EmployeeMasterOES",
             templateUrl: 'app/modules/HRMS/views/EmployeeMasterOES.html',
             controller: 'EmployeeMasterOESCont'

         })
           .state('main.Per234', {
               url: "/Designation",
               templateUrl: 'app/modules/HRMS/views/Designation.html',
               controller: 'DesignationCont'
           })
             .state('main.DesigDept', {
                 url: "/DesignationDept",
                 templateUrl: 'app/modules/HRMS/views/DesignationWithDepartment.html',
                 controller: 'DesignationWithDepartmentCont'
             })

      .state('main.Per229', {
          url: "/Destination",
          templateUrl: 'app/modules/HRMS/views/Destination.html',
          controller: 'DestinationCont'
      })

       .state('main.Per258', {
           url: "/QualificationEmployee",
           templateUrl: 'app/modules/HRMS/views/QualificationEmployee.html',
           controller: 'QualificationEmployeeCont'
       })


          .state('main.Sim019', {
              url: "/DocumentDetails",
              templateUrl: 'app/modules/Student/views/DocumentDetails.html',
              controller: 'DocumentDetailsCont'
          })

          .state('main.Sim020', {
              url: "/EmployeeDocumentMaster",
              templateUrl: 'app/modules/HRMS/views/EmployeeDocumentMaster.html',
              controller: 'EmployeeDocumentMasterCont'
          })

    .state('main.Sim539', {
        url: "/AgendaView",
        templateUrl: 'app/modules/Student/views/AgendaView.html',
        controller: 'AgendaViewCont'
    })
              .state('main.Sim535', {
                  url: "/AgendaView_Geis",
                  templateUrl: 'app/modules/Student/views/AgendaView_Geis.html',
                  controller: 'AgendaViewCont_Geis'
              })

        .state('main.Sim195', {
            url: "/AgendaConfiguration",
            templateUrl: 'app/modules/Student/views/AgendaConfiguration.html',
            controller: 'AgendaConfigurationCont'
        })

         .state('main.Sim190', {
             url: "/Behaviour",
             templateUrl: 'app/modules/Student/views/Behaviour.html',
             controller: 'BehaviourCont'
         })

         .state('main.Sim191', {
             url: "/StudentBehaviour",
             templateUrl: 'app/modules/Student/views/StudentBehaviour.html',
             controller: 'StudentBehaviourCont'
         })

        .state('main.Per235', {
            url: "/PaysParameter",
            templateUrl: 'app/modules/HRMS/views/PaysParameter.html',
            controller: 'PaysParameterCont'
        })

          .state('main.Sim099', {
              url: "/MedicalStudentSusceptibility",
              templateUrl: 'app/modules/Health/views/MedicalStudentSusceptibility.html',
              controller: 'MedicalStudentSusceptibilityCont'
          })

            .state('main.Sim098', {
                url: "/StudentHealth",
                templateUrl: 'app/modules/Health/views/StudentHealth.html',
                controller: 'StudentHealthCont'
            })

             .state('main.Sim898', {
                 url: "/StudentHealthCSD",
                 templateUrl: 'app/modules/Health/views/StudentHealthCSD.html',
                 controller: 'StudentHealthCSDCont'
             })

        .state('main.Sim690', {
            url: "/MedicalConsent",
            templateUrl: 'app/modules/Health/views/MedicalConsent.html',
            controller: 'MedicalConsentCont'
        })

        .state('main.Sim692', {
            url: "/Survey",
            templateUrl: 'app/modules/Student/views/Survey.html',
            controller: 'SurveyCont'
        })

        .state('main.MedicalConsentPPN', {
            url: "/MedicalConsentPPN",
            templateUrl: 'app/modules/Health/views/MedicalConsentPPN.html',
            controller: 'MedicalConsentPPNCont'
        })

         .state('main.Fin149', {
             url: "/FinnParameter",
             templateUrl: 'app/modules/Finance/views/FinnParameter.html',
             controller: 'FinnParameterCont'
         })


          .state('main.Fin226', {
              url: "/PettyCashDocument",
              templateUrl: 'app/modules/Finance/views/PettyCashDocument.html',
              controller: 'PettyCashDocumentCont'
          })

        .state('main.Fin227', {
            url: "/PettyCashMasters",
            templateUrl: 'app/modules/Finance/views/PettyCashMasters.html',
            controller: 'PettyCashMastersCont'
        })

          .state('main.Fin229', {
              url: "/PettyCashApprove",
              templateUrl: 'app/modules/Finance/views/PettyCashApprove.html',
              controller: 'PettyCashApproveCont'
          })


        .state('main.Fin123', {
            url: "/SLQuery",
            templateUrl: 'app/modules/Finance/views/SLQuery.html',
            controller: 'SLQueryCont'
        })

        .state('main.Fin123_DPS', {
            url: "/SLQuery_DPS",
            templateUrl: 'app/modules/Finance/views/SLQuery_DPS.html',
            controller: 'SLQuery_DPSCont'
        })


             .state('main.BAPL01', {
                 url: "/BudgetVsActualReportForProfitAndLoss",
                 templateUrl: 'app/modules/Finance/views/BudgetVsActualReportForProfitAndLoss.html',
                 controller: 'BudgetVsActualReportForProfitAndLossCont'
             })


          .state('main.Ucw240', {
              url: "/Achievement",
              templateUrl: 'app/modules/HRMS/views/Achievement.html',
              controller: 'AchievementCont'
          })
             .state('main.MYw260', {
                 url: "/myAchievement",
                 templateUrl: 'app/modules/HRMS/views/myAchievement.html',
                 controller: 'MyAchievementCont'
             })

         .state('main.Ucw242', {
             url: "/GoalTarget",
             templateUrl: 'app/modules/HRMS/views/GoalTarget.html',
             controller: 'GoalTargetCont'
         })

         .state('main.Ucw243', {
             url: "/GoalTargetKPI",
             templateUrl: 'app/modules/HRMS/views/GoalTargetKPI.html',
             controller: 'GoalTargetKPICont'
         })

           .state('main.Ucw244', {
               url: "/GoalTargetKPIMeasure",
               templateUrl: 'app/modules/HRMS/views/GoalTargetKPIMeasure.html',
               controller: 'GoalTargetKPIMeasureCont'
           })

          .state('main.Per305', {
              url: "/GoalTargetKPIMeasureAchievement",
              templateUrl: 'app/modules/HRMS/views/GoalTargetKPIMeasureAchievement.html',
              controller: 'GoalTargetKPIMeasureAchievementCont'
          })

                //This id used in Per301 > GoalTargetEvolution
            .state('main.Per301', {
                url: "/GoalTargetEvaluation",
                templateUrl: 'app/modules/HRMS/views/GoalTargetEvaluation.html',
                controller: 'GoalTargetEvaluationCont'
            })

            .state('main.Ucw245', {
                url: "/GoalTargetGroup",
                templateUrl: 'app/modules/HRMS/views/GoalTargetGroup.html',
                controller: 'GoalTargetGroupCont'
            })

            .state('main.Ucw246', {
                url: "/Reviewer",
                templateUrl: 'app/modules/HRMS/views/Reviewer.html',
                controller: 'ReviewerCont'
            })


              .state('main.Per302', {
                  url: "/SipEvaluationSchedule",
                  templateUrl: 'app/modules/HRMS/views/SipEvaluationSchedule.html',
                  controller: 'SipEvaluationScheduleCont'
              })
    ///end

           .state('main.Com010', {
               url: "/RoleApplication",
               templateUrl: 'app/modules/Setup/views/RoleApplication.html',
               controller: 'RoleApplicationCont'
           })

          .state('main.Com006', {
              url: "/UserApplications",
              templateUrl: 'app/modules/Setup/views/UserApplications.html',
              controller: 'UserApplicationsCont'
          })

        .state('main.modord', {
            url: "/ApplicationModule",
            templateUrl: 'app/modules/Setup/views/ApplicationModule.html',
            controller: 'ApplicationModuleCont'
        })

        .state('main.Sim042', {
            url: "/StudentDatabase",
            templateUrl: 'app/modules/Student/views/StudentDatabase.html',
            controller: 'StudentDatabaseCont'
        })

        .state('main.sdbsis', {
            url: "/StudentSISODB",
            templateUrl: 'app/modules/Student/views/StudentDatabase_SISO.html',
            controller: 'StudentDatabaseCont_DBSISO'
        })

              .state('main.stsiso', {
                  url: "/StudentDBSISO",
                  templateUrl: 'app/modules/Student/views/StudentDatabase_SISO.html',
                  controller: 'StudentDatabaseCont'
              })

             .state('main.stleams', {
                 url: "/StudentDBLeams",
                 templateUrl: 'app/modules/Student/views/StudentDatabase_Leams.html',
                 controller: 'StudentDatabaseLeamsCont'
             })




        .state('main.Sim680', {
            url: "/StudentDatabaseVal",
            templateUrl: 'app/modules/Student/views/StudentDatabaseVal.html',
            controller: 'StudentDatabaseValCont'
        })

          .state('main.Sim039', {
              url: "/SectionSubject",
              templateUrl: 'app/modules/SchoolSetup/views/SectionSubject.html',
              controller: 'SectionSubjectCont'
          })

          .state('main.Sim179', {
              url: "/ClassTeacherMapping",
              templateUrl: 'app/modules/SchoolSetup/views/ClassTeacherMapping.html',
              controller: 'ClassTeacherMappingCont'
          })

		   .state('main.Sim256', {
		       url: "/ClassWiseTeacherMapping",
		       templateUrl: 'app/modules/SchoolSetup/views/ClassWiseTeacherMapping.html',
		       controller: 'ClassWiseTeacherMappingCont'
		   })

          .state('main.Sim173', {
              url: "/EmployeeTeacherCreation",
              templateUrl: 'app/modules/HRMS/views/EmployeeTeacherCreation.html',
              controller: 'EmployeeTeacherCreationCont'
          })

           .state('main.Sim174', {
               url: "/StudentAttendanceMonthly",
               templateUrl: 'app/modules/Attendance/views/StudentAttendanceMonthly.html',
               controller: 'StudentAttendanceMonthlyCont'
           })
		    .state('main.SimGoa1', {
		        url: "/StudentAttendanceMonthly_Goa",
		        templateUrl: 'app/modules/Attendance/views/StudentAttendanceMonthly.html',
		        controller: 'StudentAttendanceMonthly_GoaCont'
		    })

      .state('main.Comn005', {
          url: "/CreateEditUser",
          templateUrl: 'app/modules/Setup/views/CreateEditUser.html',
          controller: 'CreateEditUserCont'
      })

       .state('main.Sim029', {
           url: "/House",
           templateUrl: 'app/modules/SchoolSetup/views/House.html',
           controller: 'HouseCont'
       })

     .state('main.Sim168', {
         url: "/HouseAllocation",
         templateUrl: 'app/modules/Student/views/HouseAllocation.html',
         controller: 'HouseAllocationCont'
     })

      .state('main.Sim533', {
          url: "/FeeTerm",
          templateUrl: 'app/modules/Fee/views/FeeTerm.html',
          controller: 'FeeTermCont'
      })

           .state('main.Sim061', {
               url: "/Bell",
               templateUrl: 'app/modules/Scheduling/views/Bell.html',
               controller: 'BellCont'
           })

         //.state('main.Lrnton', {
         //    url: "/Learntron",
         //    templateUrl: 'app/modules/Scheduling/views/Learntron.html',
         //    controller: 'LearntronCont'
         //})

      .state('main.Sim062', {
          url: "/BellSlotGroup",
          templateUrl: 'app/modules/Scheduling/views/BellSlotGroup.html',
          controller: 'BellSlotGroupCont'
      })

    .state('main.Sim063', {
        url: "/BellSlotRoom",
        templateUrl: 'app/modules/Scheduling/views/BellSlotRoom.html',
        controller: 'BellSlotRoomCont'
    })

     .state('main.Sim064', {
         url: "/BellDay",
         templateUrl: 'app/modules/Scheduling/views/BellDay.html',
         controller: 'BellDayCont'
     })

     .state('main.Sim065', {
         url: "/BellDetails",
         templateUrl: 'app/modules/Scheduling/views/BellDetails.html',
         controller: 'BellDetailsCont'
     })

    .state('main.Sim060', {
        url: "/BellSectionSubjectTeacher",
        templateUrl: 'app/modules/Scheduling/views/BellSectionSubjectTeacher.html',
        controller: 'BellSectionSubjectTeacherCont'
    })
                 .state('main.STLRpt', {
                     url: "/SubjectTeacherList_Rpt",
                     templateUrl: 'app/modules/Student/views/SubjectTeacherListRpt.html',
                     controller: 'SubjectTeacherListRptCont'
                 })

         //.state('main.Sim086', {
         //    url: "/TransportVehicleDetails",
         //    templateUrl: 'app/modules/Fleet/views/TransportVehicleDetails.html',
         //    controller: 'TransportVehicleDetailsCont'
         //})
            .state('main.Sim850', {
                url: "/BellSectionTeacherSubject",
                templateUrl: 'app/modules/Scheduling/views/BellSectionTeacherSubject.html',
                controller: 'BellSectionTeacherSubjectCont'
            })

      //.state('main.Sim083', {
      //    url: "/TransportRouteDetails",
      //    templateUrl: 'app/modules/Fleet/views/TransportRouteDetails.html',
      //    controller: 'TransportRouteDetailsCont'
      //})

           .state('main.Sim083', {
               url: "/TransportRoute",
               templateUrl: 'app/modules/Fleet/views/TransportRoute.html',
               controller: 'TransportRouteCont'
           })

         .state('main.Sim084', {
             url: "/TransportStop",
             templateUrl: 'app/modules/Fleet/views/TransportStop.html',
             controller: 'TransportStopCont'
         })

           .state('main.Sim085', {
               url: "/TransportRouteStop",
               templateUrl: 'app/modules/Fleet/views/TransportRouteStop.html',
               controller: 'TransportRouteStopCont'
           })


           .state('main.vc0001', {
               url: "/VehicleConsumption",
               templateUrl: 'app/modules/Fleet/views/VehicleConsumption.html',
               controller: 'VehicleConsumptionCont'
           })

           .state('main.Sim086', {
               url: "/TransportVehicle",
               templateUrl: 'app/modules/Fleet/views/TransportVehicle.html',
               controller: 'TransportVehicleCont'
           })

         .state('main.Sim087', {
             url: "/TransportCaretaker",
             templateUrl: 'app/modules/Fleet/views/TransportCaretaker.html',
             controller: 'TransportCaretakerCont'
         })

.state('main.SimTra', {
    url: "/TransportEnrollReport",
    templateUrl: 'app/modules/Fleet/views/TransportEnrollReport.html',
    controller: 'TransportEnrollReportCont'
})


         .state('main.Sim088', {
             url: "/TransportDriver",
             templateUrl: 'app/modules/Fleet/views/TransportDriver.html',
             controller: 'TransportDriverCont'
         })

           .state('main.Sim079', {
               url: "/TransportFees",
               templateUrl: 'app/modules/Fleet/views/TransportFees.html',
               controller: 'TransportFeesCont'
           })
           .state('main.Sim519', {
               url: "/ApproveStudentTransport",
               templateUrl: 'app/modules/Fleet/views/ApproveStudentTransport.html',
               controller: 'ApproveStudentTransportCont'
           })

          .state('main.Sim081', {
              url: "/TransportRouteStudent",
              templateUrl: 'app/modules/Fleet/views/TransportRouteStud.html',
              controller: 'TransportRouteStudCont'
          })


         .state('main.Sim766', {
             url: "/MarkEntryView",
             templateUrl: 'app/modules/Student/views/MarkEntryView.html',
             controller: 'MarkEntryViewCont'
         })


         .state('main.Sim525', {
             url: "/TransportCancelUpdate",
             templateUrl: 'app/modules/Fleet/views/TransportCancelUpdate.html',
             controller: 'TransportCancelUpdateCont'
         })

             .state('main.SimRaa', {
                 url: "/TransportCancelUpdateRaakg",
                 templateUrl: 'app/modules/Fleet/views/TransportCancelUpdateRaakg.html',
                 controller: 'TransportCancelUpdateRaakgCont'
             })
            //.state('main.Sim519', {
            //    url: "/ApproveStudentTransport",
            //    templateUrl: 'app/modules/Fleet/views/ApproveStudentTransport.html',
            //    controller: 'ApproveStudentTransportCont'
            //})

          .state('main.Sim182', {
              url: "/CouncilMaster",
              templateUrl: 'app/modules/Setup/views/CouncilMaster.html',
              controller: 'CouncilMasterCont'
          })

           .state('main.Sim189', {
               url: "/CouncilStudentMapping",
               templateUrl: 'app/modules/Setup/views/CouncilStudentMapping.html',
               controller: 'CouncilStudentMappingCont'
           })

     .state('main.Sim056', {
         url: "/CityMaster",
         templateUrl: 'app/modules/Setup/views/CityMaster.html',
         controller: 'CityMasterCont'
     })
        .state('main.otp', {
            url: "/OtpMaster",
            templateUrl: 'app/modules/Setup/views/OtpMaster.html',
            controller: 'OtpMasterCont'
        })

    .state('main.Sim162', {
        url: "/Building",
        templateUrl: 'app/modules/Setup/views/Building.html',
        controller: 'BuildingCont'
    })

     .state('main.Sim163', {
         url: "/ILocation",
         templateUrl: 'app/modules/Setup/views/ILocation.html',
         controller: 'ILocationCont'
     })

        .state('main.Sim568', {
            url: "/GradeScale",
            templateUrl: 'app/modules/Setup/views/GradeScale.html',
            controller: 'GradeScaleCont'
        })

         .state('main.Per244', {
             url: "/CommonCode",
             templateUrl: 'app/modules/HRMS/views/CommonCode.html',
             controller: 'CommonCodeCont'
         })

     .state('main.Per058', {
         url: "/PayCode",
         templateUrl: 'app/modules/HRMS/views/PayCode.html',
         controller: 'PayCodeCont'
     })

        .state('main.Per066', {
            url: "/PayCode_1",
            templateUrl: 'app/modules/HRMS/views/PayCode_1.html',
            controller: 'PayCode_1Cont'
        })

           .state('main.Per130', {
               url: "/LoanAmount",
               templateUrl: 'app/modules/HRMS/views/LoanAmount.html',
               controller: 'LoanAmountCont'
           })

           .state('main.Per131', {
               url: "/LoanCode",
               templateUrl: 'app/modules/HRMS/views/LoanCode.html',
               controller: 'LoanCodeCont'
           })



    .state('main.Per134', {
        url: "/LoanRegister",
        templateUrl: 'app/modules/HRMS/views/LoanRegister.html',
        controller: 'LoanRegisterCont'
    })

     .state('main.Per132', {
         url: "/LoanConfirmation",
         templateUrl: 'app/modules/HRMS/views/LoanConfirmation.html',
         controller: 'LoanConfirmationCont'
     })


     .state('main.Per135', {
         url: "/ManagerConfirmation",
         templateUrl: 'app/modules/HRMS/views/ManagerConfirmation.html',
         controller: 'ManagerConfirmationCont'
     })


    .state('main.Sim055', {
        url: "/State",
        templateUrl: 'app/modules/Setup/views/State.html',
        controller: 'StateCont'
    })

      .state('main.Sim057', {
          url: "/Region",
          templateUrl: 'app/modules/Setup/views/Region.html',
          controller: 'RegionCont'
      })

    .state('main.Sim002', {
        url: "/Country",
        templateUrl: 'app/modules/Setup/views/Country.html',
        controller: 'CountryCont'
    })

    .state('main.Sim006', {
        url: "/Ethnicity",
        templateUrl: 'app/modules/Setup/views/Ethnicity.html',
        controller: 'EthnicityCont'
    })

     .state('main.Sim007', {
         url: "/Language",
         templateUrl: 'app/modules/Setup/views/Language.html',
         controller: 'LanguageCont'
     })


    .state('main.Sim040', {
        url: "/CreateEditSibling",
        templateUrl: 'app/modules/Setup/views/CreateEditSibling.html',
        controller: 'CreateEditSiblingCont'
    })

    .state('main.Sim161', {
        url: "/HomeroomTeacher",
        templateUrl: 'app/modules/Setup/views/HomeroomTeacher.html',
        controller: 'HomeroomTeacherCont'
    })

    .state('main.Sim160', {
        url: "/HomeroomStudent",
        templateUrl: 'app/modules/Setup/views/HomeroomStudent.html',
        controller: 'HomeroomStudentCont'
    })

        .state('main.Sim500', {
            url: "/ClassWiseAttendance",
            templateUrl: 'app/modules/Attendance/views/ClassWiseAttendance.html',
            controller: 'ClassWiseAttendanceCont'
        })

         .state('main.Sim156', {
             url: "/AttendanceCode",
             templateUrl: 'app/modules/Attendance/views/AttendanceCode.html',
             controller: 'AttendanceCodeCont'
         })

         .state('main.Sim025', {
             url: "/CalendarException",
             templateUrl: 'app/modules/SchoolSetup/views/CalendarException.html',
             controller: 'CalendarExceptionCont'
         })

          .state('main.EmpDshBrd', {
              url: "/EmpDashboard",
              templateUrl: 'app/modules/HRMS/views/EmpDashboard.html',
              controller: 'EmpDashboardCont'
          })

    .state('main.EmpDas', {
        url: "/EmpDashboard",
        templateUrl: 'app/modules/HRMS/views/EmpDashboard.html',
        controller: 'EmpDashboardCont'
    })

             .state('main.purDah', {
                 url: "/PurchaseReport",
                 templateUrl: 'app/modules/HRMS/views/PurchaseReport.html',
                 controller: 'PurchaseReportCont'
             })




    .state('main.Sim158', {
        url: "/HomeRoom",
        templateUrl: 'app/modules/Setup/views/HomeRoom.html',
        controller: 'HomeRoomCont'
    })

                .state('main.Sim611', {
                    url: "/MenuDragDrop",
                    templateUrl: 'app/modules/Setup/views/MenuDragDrop.html',
                    controller: 'MenuDragDropCont'
                })

                      .state('main.Sim093', {
                          url: "/medicalExamination",
                          templateUrl: 'app/modules/Health/views/medicalExamination.html',
                          controller: 'medicalExaminationCont'

                      })

           .state('main.Sim094', {
               url: "/medicalImmunization",
               templateUrl: 'app/modules/Health/views/medicalImmunization.html',
               controller: 'medicalImmunizationCont'

           })

         .state('main.Sim095', {
             url: "/medicalImmunizationGroup",
             templateUrl: 'app/modules/Health/views/medicalImmunizationGroup.html',
             controller: 'medicalImmunizationGroupCont'

         })


         .state('main.Sim097', {
             url: "/medicalMedicine",
             templateUrl: 'app/modules/Health/views/medicalMedicine.html',
             controller: 'medicalMedicineCont'

         })

            .state('main.Sim100', {
                url: "/medicalSusceptibility",
                templateUrl: 'app/modules/Health/views/medicalSusceptibility.html',
                controller: 'medicalSusceptibilityCont'

            })

             .state('main.Sim900', {
                 url: "/medicalExcuses",
                 templateUrl: 'app/modules/Health/views/medicalExcuses.html',
                 controller: 'medicalExcusesCont'

             })

          .state('main.Sim102', {
              url: "/medicalVisitExamination",
              templateUrl: 'app/modules/Health/views/medicalVisitExamination.html',
              controller: 'medicalVisitExaminationCont'

          })

        .state('main.Sim103', {
            url: "/medicalVisitMedicine",
            templateUrl: 'app/modules/Health/views/medicalVisitMedicine.html',
            controller: 'medicalVisitMedicineCont'

        })


          .state('main.Sim096', {
              url: "/medicalImmunizationStudent",
              templateUrl: 'app/modules/Health/views/medicalImmunizationStudent.html',
              controller: 'medicalImmunizationStudentCont'

          })

         .state('main.sim536', {
             url: "/SubjectwisetAttendance",
             templateUrl: 'app/modules/Attendance/views/SubjectwisetAttendance.html',
             controller: 'SubjectwisetAttendanceCont'
         })

			.state('main.sim586', {
             url: "/SubjectwisetAttendanceGeneration",
             templateUrl: 'app/modules/Attendance/views/SubjectwisetAttendanceGeneration.html',
             controller: 'SubjectwisetAttendanceGenerationCont'
         })
           .state('main.Com019', {
               url: "/UserApplicationsFins",
               templateUrl: 'app/modules/Setup/views/UserApplicationsFins.html',
               controller: 'UserApplicationsFinsCont'
           })


     .state('main.Sim070', {
         url: "/Gradebook",
         templateUrl: 'app/modules/Gradebook/views/Gradebook.html',
         controller: 'GradebookCont'
     })

     .state('main.GbkCpy', {
         url: "/GradeBookCopyYoY",
         templateUrl: 'app/modules/Gradebook/views/GradeBookCopyYoY.html',
         controller: 'GradeBookCopyYoYCont'
     })


        .state('main.finCom', {
            url: "/finalcommnet",
            templateUrl: 'app/modules/Gradebook/views/FinalComment.html',
            controller: 'FinalCommentCont'
        })


         .state('main.Sim777', {
             url: "/Gradebook_edison",
             templateUrl: 'app/modules/Gradebook/views/Gradebook_edison.html',
             controller: 'Gradebook_edison'
         })


         .state('main.SubTrt', {
             url: "/TraitMaster",
             templateUrl: 'app/modules/Gradebook/views/TraitMaster.html',
             controller: 'TraitMasterCont'
         })

        .state('main.StuTrt', {
            url: "/StudentTrait",
            templateUrl: 'app/modules/Gradebook/views/StudentTrait.html',
            controller: 'StudentTraitCont'
        })



    .state('main.Per060', {
        url: "/Contract",
        templateUrl: 'app/modules/HRMS/views/Contract.html',
        controller: 'ContractCont'
    })

     ///// Incidence ////

        .state('main.Sim151', {
            url: "/ActionCategory",
            templateUrl: 'app/modules/Incidence/views/ActionCategory.html',
            controller: 'ActionCategoryCont'
        })
              .state('main.Simcat', {
                  url: "/ActionCategorynew",
                  templateUrl: 'app/modules/Incidence/views/ActionCategorynew.html',
                  controller: 'ActionCategorynewCont'
              })


             .state('main.DetIon', {
                 url: "/Detention",
                 templateUrl: 'app/modules/Incidence/views/Detention.html',
                 controller: 'DetentionCont'
             })



     .state('main.Sim150', {
         url: "/IncidenceAction",
         templateUrl: 'app/modules/Incidence/views/IncidenceAction.html',
         controller: 'IncidenceActionCont'
     })
    .state('main.Sim152', {
        url: "/IncidenceConsequence",
        templateUrl: 'app/modules/Incidence/views/IncidenceConsequence.html',
        controller: 'IncidenceConsequenceCont'
    })
    .state('main.Sim149', {
        url: "/Incidence",
        templateUrl: 'app/modules/Incidence/views/Incidence.html',
        controller: 'IncidenceCont'
    })

   .state('main.Dtcn01', {
       url: "/Detention",
       templateUrl: 'app/modules/Incidence/views/Detention.html',
       controller: 'DetentionCont'
   })

             .state('main.Dtcn02', {
                 url: "/DetentionA",
                 templateUrl: 'app/modules/Incidence/views/DetentionABQIS.html',
                 controller: 'DetentionABQISCont'
             })

    .state('main.Sim153', {
        url: "/IncidenceTimeline",
        templateUrl: 'app/modules/Incidence/views/IncidenceTimeline.html',
        controller: 'IncidenceTimelineCont'
    })
     .state('main.Sim154', {
         url: "/IncidenceWarning",
         templateUrl: 'app/modules/Incidence/views/IncidenceWarning.html',
         controller: 'IncidenceWarningCont'
     })

     .state('main.Sim772', {
         url: "/ActionAgainstIncidence",
         templateUrl: 'app/modules/Incidence/views/ActionAgainstIncidence.html',
         controller: 'ActionAgainstIncidenceCont'
     })


     .state('main.Per309', {
         url: "/InActiveUser",
         templateUrl: 'app/modules/HRMS/views/InActiveUser.html',
         controller: 'InActiveUserCont'
     })

        .state('main.PerAF', {
            url: "/AirFare",
            templateUrl: 'app/modules/HRMS/views/AirFare.html',
            controller: 'AirFareCont'
        })
          .state('main.PerAFE', {
              url: "/AirFareEmployee",
              templateUrl: 'app/modules/HRMS/views/AirFareEmployee.html',
              controller: 'AirFareEmployeeCont'
          })

             .state('main.AfeOes', {
                 url: "/AirFareEmployee_oes",
                 templateUrl: 'app/modules/HRMS/views/AirFareEmployeeNew.html',
                 controller: 'AirFareEmployeeNewCont'
             })

        .state('main.Per092', {
            url: "/ShiftMaster",
            templateUrl: 'app/modules/HRMS/views/ShiftMaster.html',
            controller: 'ShiftMasterCont'
        })
        .state('main.Per148', {
            url: "/ShiftTemplate",
            templateUrl: 'app/modules/HRMS/views/ShiftTemplate.html',
            controller: 'ShiftTemplateCont'
        })
         .state('main.Per104', {
             url: "/EmployeeShift",
             templateUrl: 'app/modules/HRMS/views/EmployeeShift.html',
             controller: 'EmployeeShiftCont'
         })

        .state('main.Per109', {
            url: "/PaysGradeChange",
            templateUrl: 'app/modules/HRMS/views/PaysGradeChange.html',
            controller: 'PaysGradeChangeCont'
        })

                    .state('main.Per353', {
                        url: "/EmpAttendanceDaily",
                        templateUrl: 'app/modules/HRMS/views/EmpAttendanceDaily.html',
                        controller: 'EmpAttendanceDailyCont'
                    })

                    .state('main.Per322', {
                        url: "/AttendanceMachine",
                        templateUrl: 'app/modules/HRMS/views/AttendanceMachine.html',
                        controller: 'AttendanceMachineCont'
                    })


        /**********************************************/

        .state('main.Per315', {
            url: "/PaysAttendanceCode",
            templateUrl: 'app/modules/HRMS/views/PaysAttendanceCode.html',
            controller: 'PaysAttendanceCodeCont'
        })


         .state('main.Fin151', {
             url: "/AssetMasterView",
             templateUrl: 'app/modules/Finance/views/AssetMasterView.html',
             controller: 'AssetMasterViewCont'
         })

        .state('main.Astcut', {
            url: "/AssetCustodian",
            templateUrl: 'app/modules/Finance/views/AssetCustodian.html',
            controller: 'AssetCustodianCont'
        })


     .state('main.Fin001', {
         url: "/CreateEditAccountCode",
         templateUrl: 'app/modules/Finance/views/CreateEditAccountCode.html',
         controller: 'CreateEditAccountCodeCont'
     })

             .state('main.AVQA01', {
                 url: "/AccountingViewQueryApplication",
                 templateUrl: 'app/modules/Finance/views/AccountingViewQueryApplication.html',
                 controller: 'AccountingViewQueryApplicationCont'
             })

     .state('main.Fin014', {
         url: "/GLAccountsCreateView",
         templateUrl: 'app/modules/Finance/views/GLAccountsCreateView.html',
         controller: 'GLAccountsCreateViewCont'
     })


         .state('main.GlOBal', {
             url: "/GLOBalUpdate",
             templateUrl: 'app/modules/Finance/views/GLOBalUpdate.html',
             controller: 'GLOBalUpdateCont'
         })


        .state('main.Per313', {
            url: "/EmployeeAttendanceMonthly",
            templateUrl: 'app/modules/Attendance/views/EmployeeAttendanceMonthly.html',
            controller: 'EmployeeAttendanceMonthlyCont'
        })

      .state('main.Fin118', {
          url: "/Schedule",
          templateUrl: 'app/modules/Finance/views/Schedule.html',
          controller: 'ScheduleCont'
      })

        .state('main.Fin143', {
            url: "/SubledgerMatching",
            templateUrl: 'app/modules/Finance/views/SubledgerMatching.html',
            controller: 'SubledgerMatchingCont'
        })

       .state('main.Fin108', {
           url: "/PaymentTerm",
           templateUrl: 'app/modules/Finance/views/PaymentTerm.html',
           controller: 'PaymentTermCont'
       })

       .state('main.Sim115', {
           url: "/LibraryAttribute",
           templateUrl: 'app/modules/Library/views/LibraryAttribute.html',
           controller: 'LibraryAttributeCont'
       })

       .state('main.Si0000', {
           url: "/QRCode",
           templateUrl: 'app/modules/Library/views/QRCode.html',
           controller: 'QRCodeCont'
       })

        .state('main.LibDet', {
            url: "/DueBook",
            templateUrl: 'app/modules/Library/views/DueBookDetail.html',
            controller: 'DueBookDetailCont'
        })


       .state('main.Sim117', {
           url: "/LibraryCatalogueAttribute",
           templateUrl: 'app/modules/Library/views/LibraryCatalogueAttribute.html',
           controller: 'LibraryCatalogueAttributeCont'
       })

    .state('main.Sim119', {
        url: "/LibraryCatalogue",
        templateUrl: 'app/modules/Library/views/LibraryCatalogue.html',
        controller: 'LibraryCatalogueCont'
    })

         .state('main.Sim120', {
             url: "/LibraryFee",
             templateUrl: 'app/modules/Library/views/LibraryFee.html',
             controller: 'LibraryFeeCont'
         })

         .state('main.Sim118', {
             url: "/LibraryCategory",
             templateUrl: 'app/modules/Library/views/LibraryCategory.html',
             controller: 'LibraryCategoryCont'
         })

         .state('main.Libvar', {
             url: "/LibraryItemVerificationApprove",
             templateUrl: 'app/modules/Library/views/LibraryItemVerificationApprove.html',
             controller: 'LibraryItemVerificationApproveCont'
         })
         .state('main.Libreq', {
             url: "/LibraryItemVerificationRequest",
             templateUrl: 'app/modules/Library/views/LibraryItemVerificationRequest.html',
             controller: 'LibraryItemVerificationRequestCont'
         })

        .state('main.Sim121', {
            url: "/LibraryItemMaster",
            templateUrl: 'app/modules/Library/views/LibraryItemMaster.html',
            controller: 'LibraryItemMasterCont',
            params: { 'ob': {} }
        })
            .state('main.Sim720', {
                url: "/LibraryItemArrival",
                templateUrl: 'app/modules/Library/views/LibraryItemArrival.html',
                controller: 'LibraryItemArrivalCont',
            })
            .state('main.Sim694', {
                url: "/LibraryItemMaster_GES",
                templateUrl: 'app/modules/Library/views/LibraryItemMaster_GES.html',
                controller: 'LibraryItemMaster_GESCont',
                params: { 'ob': {} }
            })
         .state('main.Sim124', {
             url: "/LibraryParentBookSearch",
             templateUrl: 'app/modules/Library/views/LibraryParentBookSearch.html',
             controller: 'LibraryParentBookSearchCont',
             params: { 'ob': {} }
         })


         .state('main.Libitm', {
             url: "/LibraryItemMaster_DPS",
             templateUrl: 'app/modules/Library/views/LibraryItemMaster_DPS.html',
             controller: 'LibraryItemMasterCont_DPS',
             params: { 'ob': {} }
         })


        .state('main.Sim687', {
            url: "/LibraryItemMaster_Sisqatar",
            templateUrl: 'app/modules/Library/views/LibraryItemMaster_Sisqatar.html',
            controller: 'LibraryItemMaster_SisqatarCont',
            params: { 'ob': {} }
        })

        .state('main.Sim688', {
            url: "/LibraryItemMaster_Edison",
            templateUrl: 'app/modules/Library/views/LibraryItemMaster_Edison.html',
            controller: 'LibraryItemMaster_EdisonCont',
            params: { 'ob': {} }
        })

        .state('main.Sim691', {
            url: "/LibraryItemMaster_Asd",
            templateUrl: 'app/modules/Library/views/LibraryItemMaster_Asd.html',
            controller: 'LibraryItemMaster_AsdCont',
            params: { 'ob': {} }
        })

            .state('main.Sim699', {
                url: "/LibraryItemMaster_SMFC",
                templateUrl: 'app/modules/Library/views/LibraryItemMaster_SMFC.html',
                controller: 'LibraryItemMaster_SMFCCont',
                params: { 'ob': {} }
            })
        .state('main.Sim682', {
            url: "/LibraryItemSearch",
            templateUrl: 'app/modules/Library/views/LibraryItemSearch.html',
            controller: 'LibraryItemSearchCont'
        })

          .state('main.Sim126', {
              url: "/LibraryMembershipType",
              templateUrl: 'app/modules/Library/views/LibraryMembershipType.html',
              controller: 'LibraryMembershipTypeCont'
          })

        .state('main.Sim127', {
            url: "/LibraryPrivilege",
            templateUrl: 'app/modules/Library/views/LibraryPrivilege.html',
            controller: 'LibraryPrivilegeCont'
        })

        .state('main.Sim128', {
            url: "/LibraryItemRequest",
            templateUrl: 'app/modules/Library/views/LibraryItemRequest.html',
            controller: 'LibraryItemRequestCont'
        })

        .state('main.Sim129', {
            url: "/LibraryRequestDetail",
            templateUrl: 'app/modules/Library/views/LibraryRequestDetail.html',
            controller: 'LibraryRequestDetailCont'
        })

         .state('main.Sim123', {
             url: "/LibraryLocation",
             templateUrl: 'app/modules/Library/views/LibraryLocation.html',
             controller: 'LibraryLocationCont'
         })

        .state('main.Sim675', {
            url: "/LibraryTxnHomeroom",
            templateUrl: 'app/modules/Library/views/LibraryTxnHomeroom.html',
            controller: 'LibraryTxnHomeroomCont'
        })

              .state('main.Sim131', {
                  url: "/LibrarySubcategory",
                  templateUrl: 'app/modules/Library/views/LibrarySubcategory.html',
                  controller: 'LibrarySubcategoryCont'
              })

        .state('main.Sim671', {
            url: "/LibraryEmpMembership",
            templateUrl: 'app/modules/Library/views/LibraryEmpMembership.html',
            controller: 'LibraryEmpMembershipCont'
        })

            .state('main.Sim672', {
                url: "/LibraryUserHistory",
                templateUrl: 'app/modules/Library/views/LibraryUserHistory.html',
                controller: 'LibraryUserHistoryCont'
            })


         .state('main.Fin060', {
             url: "/FinancialDocument",
             templateUrl: 'app/modules/Finance/views/FinancialDocument.html',
             controller: 'FinancialDocumentCont'
         })

        .state('main.Fin060_DPS', {
            url: "/FinancialDocument_DPS",
            templateUrl: 'app/modules/Finance/views/FinancialDocument_DPS.html',
            controller: 'FinancialDocument_DPSCont'
        })

          .state('main.Sim515', {
              url: "/FinancialYear",
              templateUrl: 'app/modules/Finance/views/FinancialYear.html',
              controller: 'FinancialYearCont'
          })


              .state('main.Fin138', {
                  url: "/BankMaster",
                  templateUrl: 'app/modules/Finance/views/BankMaster.html',
                  controller: 'BankMasterCont'
              })


           .state('main.PosFee', {
               url: "/FeeTypeFeePosting",
               templateUrl: 'app/modules/Finance/views/FeeTypeFeePostingAccountMapping.html',
               controller: 'FeeTypeFeePostingAccountMappingCont'
           })

             .state('main.FeePos', {
                 url: "/FeePostingGEIS",
                 templateUrl: 'app/modules/Finance/views/FeePosAccMapping_GEIS.html',
                 controller: 'FeePosAccMapping_GEISCont'
             })




            .state('main.TriBal', {
                url: "/TrialBalance",
                templateUrl: 'app/modules/Finance/views/TrialBalanceView.html',
                controller: 'TrialBalanceViewCont'
            })



 .state('main.SimTCA', {
     url: "/TransportConfigure",
     templateUrl: 'app/modules/Fleet/views/TransportConfigure.html',
     controller: 'TransportConfigureCont'
 })


                  .state('main.BankNew', {
                      url: "/BankMasterNew",
                      templateUrl: 'app/modules/Finance/views/BankMasterNew.html',
                      controller: 'BankMasterNew'
                  })


             .state('main.LbBoRe', {
                 url: "/LibraryBookReview",
                 templateUrl: 'app/modules/Library/views/LibraryBookReview.html',
                 controller: 'LibraryBookReviewCont'
             })



             .state('main.LbBoRA', {
                 url: "/LibraryBookReview_Approve",
                 templateUrl: 'app/modules/Library/views/LibraryBookReview_Approve.html',
                 controller: 'LibraryBookReview_ApproveCont'
             })

         .state('main.Fin011', {
             url: "/CreditEditDivision",
             templateUrl: 'app/modules/Finance/views/CreditEditDivision.html',
             controller: 'CreditEditDivisionCont'
         })

             .state('main.Fin205', {
                 url: "/UserAccount",
                 templateUrl: 'app/modules/Finance/views/UserAccount.html',
                 controller: 'UserAccountCont'
             })

              .state('main.Fin207', {
                  url: "/FeePostingPaymentMode",
                  templateUrl: 'app/modules/Finance/views/FeePostingPaymentMode.html',
                  controller: 'FeePostingPaymentModeCont'
              })


       .state('main.Fin069', {
           url: "/LedgerControl",
           templateUrl: 'app/modules/Finance/views/LedgerCntrl.html',
           controller: 'LedgerCntrlCont'
       })


        .state('main.FIN206', {
            url: "/DocumentUserAccount",
            templateUrl: 'app/modules/Finance/views/DocumentUserAccount.html',
            controller: 'DocumentUserAccountCont'
        })

        .state('main.Fin043', {
            url: "/CurrencyMaster",
            templateUrl: 'app/modules/Finance/views/CurrencyMaster.html',
            controller: 'CurrencyMasterCont'
        })

        .state('main.Fin003', {
            url: "/CompanyMaster",
            templateUrl: 'app/modules/Finance/views/CompanyMaster.html',
            controller: 'CompanyMasterCont'
        })

    .state('main.Sim116', {
        url: "/LibraryBin",
        templateUrl: 'app/modules/Library/views/LibraryBin.html',
        controller: 'LibraryBinCont'
    })


        .state('main.Sim11P', {
            url: "/LibraryBin", //LibraryBin_pearl
            templateUrl: 'app/modules/Library/views/LibraryBin_pearl.html',
            controller: 'LibraryBinCont_pearl'
        })

             .state('main.Sim693', {
                 url: "/LibraryBookset",
                 templateUrl: 'app/modules/Library/views/LibraryBookset.html',
                 controller: 'LibraryBooksetCont'
             })

    .state('main.Sim122', {
        url: "/LibraryItemStyle",
        templateUrl: 'app/modules/Library/views/LibraryItemStyle.html',
        controller: 'LibraryItemStyleCont'
    })

        .state('main.Sim130', {
            url: "/LibraryStatus",
            templateUrl: 'app/modules/Library/views/LibraryStatus.html',
            controller: 'LibraryStatusCont'
        })

     .state('main.Sim134', {
         url: "/LibrarySubscriptionType",
         templateUrl: 'app/modules/Library/views/LibrarySubscriptionType.html',
         controller: 'LibrarySubscriptionTypeCont'
     })

     .state('main.Sim137', {
         url: "/LibraryTransactionType",
         templateUrl: 'app/modules/Library/views/LibraryTransactionType.html',
         controller: 'LibraryTransactionTypeCont'
     })

        .state('main.Sim503', {
            url: "/RenewLibrary",
            templateUrl: 'app/modules/Library/views/RenewLibrary.html',
            controller: 'RenewLibraryCont'
        })

        .state('main.Sim767', {
            url: "/MembershipActiveInactive",
            templateUrl: 'app/modules/Library/views/MembershipActiveInactive.html',
            controller: 'MembershipActiveInactiveCont'
        })

        .state('main.Sim136', {
            url: "/LibraryTransactionDetail",
            templateUrl: 'app/modules/Library/views/LibraryTransactionDetail.html',
            controller: 'LibraryTransactionDetailCont'
        })

             .state('main.liblst', {
                 url: "/List",
                 templateUrl: 'app/modules/Library/views/List.html',
                 controller: 'ListCont'
             })

      .state('main.per059', {
          url: "/CompanyMaster",
          templateUrl: 'app/modules/HRMS/views/CompanyMaster.html',
          controller: 'CompanyMasterCont'
      })

      .state('main.Per137', {
          url: "/OvertimeHour",
          templateUrl: 'app/modules/HRMS/views/OvertimeHour.html',
          controller: 'OvertimeHourCont'
      })
          .state('main.Per138', {
              url: "/OvertimeRate",
              templateUrl: 'app/modules/HRMS/views/OvertimeRate.html',
              controller: 'OvertimeRateCont'
          })


      .state('main.Sim143', {
          url: "/Co_Scholastic",
          templateUrl: 'app/modules/Gradebook/views/Co_Scholastic.html',
          controller: 'Co_ScholasticCont'
      })


              .state('main.ExamM', {
                  url: "/ExamMarkApprovalStatus",
                  templateUrl: 'app/modules/Gradebook/views/ExamMarkApprovalStatus.html',
                  controller: 'ExamMarkApprovalStatusCont'
              })

                 .state('main.nblock', {
                     url: "/ReportCardBlock",
                     templateUrl: 'app/modules/Gradebook/views/ReportcardBlock.html',
                     controller: 'ReportcardBlockController'
                 })

            .state('main.rblock', {
                url: "/ReportCardBlock1",
                templateUrl: 'app/modules/Gradebook/views/ReportcardBlockNew.html',
                controller: 'ReportcardBlockNewController'
            })

      .state('main.Sim013', {
          url: "/ConcessionApproval",
          templateUrl: 'app/modules/Fee/views/ConcessionApproval_dpsmis.html',
          controller: 'ConcessionApprovalCont_dpsmis'
      })
     .state('main.Sm043C', {
         url: "/CFR",
         templateUrl: 'app/modules/Fee/views/CFR.html',
         controller: 'CFRCont'
     })

     .state('main.Sim43a', {
         url: "/FeeRefund",
         templateUrl: 'app/modules/Fee/views/FeeRefund.html',
         controller: 'FeeRefundCont'
     })

    .state('main.InvLPO', {
        url: "/InvLPO",
        templateUrl: 'app/modules/Inventory/views/InvLPO.html',
        controller: 'InvLPOCont'
    })

    .state('main.InvCSE', {
        url: "/InvCSE",
        templateUrl: 'app/modules/Inventory/views/CostingSheetEXP.html',
        controller: 'CostingSheetEXPCont'
    })

    .state('main.Inv042', {
        url: "/Bank",
        templateUrl: 'app/modules/Inventory/views/Bank.html',
        controller: 'BankCont'
    })

        .state('main.Inv006', {
            url: "/SupplierMaster",
            templateUrl: 'app/modules/Inventory/views/SupplierMaster.html',
            controller: 'SupplierMasterCont'
        })

        .state('main.Inv052', {
            url: "/InventorySection",
            templateUrl: 'app/modules/Inventory/views/Section.html',
            controller: 'SectionCont'
        })



            .state('main.PERAEN', {
                url: "/AbsentEmployeeNotification",
                templateUrl: 'app/modules/HRMS/views/AbsentEmployeeNotification.html',
                controller: 'AbsentEmployeeNotificationCont'
            })



        .state('main.Inv062', {
            url: "/InventoryLocation",
            templateUrl: 'app/modules/Inventory/views/Location.html',
            controller: 'LocationCont'
        })

        .state('main.Inv064', {
            url: "/TradeTerm",
            templateUrl: 'app/modules/Inventory/views/TradeTerm.html',
            controller: 'TradeTermCont'
        })
         .state('main.Com007', {
             url: "/UserAudit",
             templateUrl: 'app/modules/Security/views/UserAudit.html',
             controller: 'UserAuditCont'
         })

    //.state('main.Per78b', {
    //    url: "/Updatepaysheet",
    //    templateUrl: 'app/modules/HRMS/views/Updatepaysheet.html',
    //    controller: 'UpdatepaysheetCont'
    //})
     .state('main.Sim43b', {
         url: "/Sims43bRefund",
         templateUrl: 'app/modules/Fee/views/Sims043Refund.html',
         controller: 'Sims043RefundCont'
     })

    .state('main.EmpNew', {
        url: "/Employee",
        templateUrl: 'app/modules/Setup/views/Employee.html',
        controller: 'EmployeeCont'
    })

     .state('main.Com005', {
         url: "/CreateEditUser",
         templateUrl: 'app/modules/Setup/views/CreateEditUser.html',
         controller: 'CreateEditUserCont'
     })


        .state('main.MasCon', {
            url: "/MasterConfiguration",
            templateUrl: 'app/modules/Setup/views/MasterConfiguration.html',
            controller: 'MasterConfigurationCont'
        })

              .state('main.SimATF', {
                  url: "/ApproveTransportFee",
                  templateUrl: 'app/modules/Fleet/views/ApproveTransportFee.html',
                  controller: 'ApproveTransportFeeCont'
              })





          .state('main.sim041', {
              url: "/SMTP",
              templateUrl: 'app/modules/Setup/views/SMTP.html',
              controller: 'SMTPCont'
          })

        .state('main.PDCSubmission', {
            url: "/PDCSubmission",
            templateUrl: 'app/modules/Finance/views/PDCSubmission.html',
            controller: 'PDCSubmissionCont'
        })


         .state('main.stupos', {
             url: "/StudentReceiveablePosting",
             templateUrl: 'app/modules/Finance/views/StudentReceiveable.html',
             controller: 'StudentReceiveablePostingCont'
         })

         .state('main.Advpos', {
             url: "/AdvancedMonthPosting",
             templateUrl: 'app/modules/Finance/views/AdvancedMonthPosting.html',
             controller: 'AdvancedMonthPostingCont'
         })



          .state('main.Fn102T', {
              url: "/PDCReSubmission",
              templateUrl: 'app/modules/Finance/views/PDCReSubmission.html',
              controller: 'PDCReSubmissionCont'
          })

               .state('main.Fn102W', {
                   url: "/PDCReSubmission_ABQIS",
                   templateUrl: 'app/modules/Finance/views/PDCReSubmission_ABQIS.html',
                   controller: 'PDCReSubmission_ABQISCont'
               })

            .state('main.Fin150', {
                url: "/JVCreate",
                templateUrl: 'app/modules/Finance/views/JVCreate.html',
                controller: 'JVCreateCont'
            })

             .state('main.JCN150', {
                 url: "/JVCreateNew",
                 templateUrl: 'app/modules/Finance/views/JVCreateNew.html',
                 controller: 'JVCreateNewCont'
             })

          .state('main.Fin150_DPS', {
              url: "/JVCreate_DPS",
              templateUrl: 'app/modules/Finance/views/JVCreate_DPS.html',
              controller: 'JVCreate_DPSCont'
          })



        .state('main.Fin064', {
            url: "/GLQuery",
            templateUrl: 'app/modules/Finance/views/GLQuery.html',
            controller: 'GLQueryCont'
        })

            //.state('main.StudIm', {
            //    url: "/UploadFile",
            //    templateUrl: 'app/modules/Finance/views/stud.html',
            //    controller: 'demoImportCont'
            //})

            .state('main.StudIm', {
                url: "/studDetails",
                templateUrl: 'app/modules/Student/views/studentDetails.html',
                controller: 'studentDetailsCont'
            })

            .state('main.sdfuNe', {
                url: "/sdfuNew",
                templateUrl: 'app/modules/Student/views/sdfuNew.html',
                controller: 'sdfuNewCont'
            })

            .state('main.acesCu', {
                url: "/acesCustomer",
                templateUrl: 'app/modules/Student/views/acesCustomer.html',
                controller: 'acesCustomerCont'
            })

            .state('main.CampRe', {
                url: "/campReportNew",
                templateUrl: 'app/modules/Student/views/campReportNew.html',
                controller: 'campReportNewCont'
            })

            .state('main.TargGr', {
                url: "/targetGrade",
                templateUrl: 'app/modules/Gradebook/views/targetGrade.html',
                controller: 'targetGradeCont'
            })

          .state('main.Fn102R', {
              url: "/PDCReturn",
              templateUrl: 'app/modules/Finance/views/PDCReturn.html',
              controller: 'PDCReturnCont'
          })

             .state('main.Fn102X', {
                 url: "/PDCReturn_ABQIS",
                 templateUrl: 'app/modules/Finance/views/PDCReturn_ABQIS.html',
                 controller: 'PDCReturn_ABQISCont'
             })


        .state('main.Fn102C', {
            url: "/PDCCancel",
            templateUrl: 'app/modules/Finance/views/PDCCancel.html',
            controller: 'PDCCancelCont'
        })

             .state('main.Fn102D', {
                 url: "/PDCCancel_ABQIS",
                 templateUrl: 'app/modules/Finance/views/PDCCancel_ABQIS.html',
                 controller: 'PDCCancel_ABQISCont'
             })


        .state('main.Fn102Z', {
            url: "/PDCRealised",
            templateUrl: 'app/modules/Finance/views/PDCRealised.html',
            controller: 'PDCRealisedCont'
        })

            .state('main.Fn102A', {
                url: "/PDCRealised_ABQIS",
                templateUrl: 'app/modules/Finance/views/PDCRealised_ABQIS.html',
                controller: 'PDCRealised_ABQISCont'
            })






    .state('main.Sim550', {
        url: "/Class Wise Fee Collection",
        templateUrl: 'app/modules/Fee/views/ClassWiseFeeCollection.html',
        controller: 'ClassWiseFeeCollectionCont'
    })
    ////////////////////////END//////////////
    ////////////Ganesh////////////////

          ///////// ////Ganesh/////////////////////////

    .state('main.Sim565', {
        url: "/ProspectFees",
        templateUrl: 'app/modules/Student/views/ProspectFees.html',
        controller: 'ProspectFeesCont'
    })

       .state('main.Sim521', {
           url: "/AdmissionFees",
           templateUrl: 'app/modules/Student/views/AdmissionFees.html',
           controller: 'AdmissionFeesCont'
       })

             .state('main.Sim529', {
                 url: "/AdmissionFeesGeis",
                 templateUrl: 'app/modules/Student/views/AdmissionFeesGeis.html',
                 controller: 'AdmissionFeesGeisCont'
             })

        .state('main.Sim563', {
            url: "/AdmissionFee",
            templateUrl: 'app/modules/Student/views/AdmissionFee.html',
            controller: 'AdmissionFeeCont'
        })

        .state('main.Sim711', {
            url: "/SurveyQuestions",
            templateUrl: 'app/modules/Student/views/SurveyQuestions.html',
            controller: 'SurveyQuestionsCont'
        })

         .state('main.Sim775N', {
             url: "/AdmissionRecord",
             templateUrl: 'app/modules/Student/views/LanguageMappingForStudent.html',
             controller: 'LanguageMappingForStudentCont'
             //templateUrl: 'app/modules/Student/views/AdmissionRecord.html',
             //controller: 'AdmissionRecordCont'
         })

             .state('main.Sim775', {
                 url: "/LanguageMappingForStudentNew",
                 templateUrl: 'app/modules/Student/views/LanguageMappingForStudentNew.html',
                 controller: 'LanguageMappingForStudentNewCont'
                 //templateUrl: 'app/modules/Student/views/AdmissionRecord.html',
                 //controller: 'AdmissionRecordCont'
             })



      .state('main.Sim105', {
          url: "/IssueCertificate",
          templateUrl: 'app/modules/Student/views/IssueCertificate.html',
          controller: 'IssueCertificateCont'
      })

     .state('main.Com012', {
         url: "/Exceptions",
         templateUrl: 'app/modules/Common/views/Exceptions.html',
         controller: 'ExceptionsCont'
     })


    .state('main.Com050', {
        url: "/AlertTransaction",
        templateUrl: 'app/modules/Common/views/AlertTransaction.html',
        controller: 'AlertTransactionCont'
    })

    /////////////END/////////////


    .state('main.Sim078', {
        url: "/album",
        templateUrl: 'app/modules/Setup/views/album.html',
        controller: 'AlbumController'
    })

         .state('main.albmV', {
             url: "/albumView",
             templateUrl: 'app/modules/Setup/views/albumView.html',
             controller: 'AlbumController'
         })
        //.state('main.EmpDocMaster', {
        //    url: "/EmployeeDocumentMaster",
        //    templateUrl: 'app/modules/HRMS/views/EmployeeDocumentMaster.html',
        //    controller: 'EmployeeDocumentMasterCont'
        //})

    .state('main.Sim541', {
        url: "/FeeInvoice",
        templateUrl: 'app/modules/Fee/views/FeeInvoice.html',
        controller: 'FeeInvoiceCont'
    })

     .state('main.FeeCol', {
         url: "/FeeCollectionRpt",
         templateUrl: 'app/modules/Fee/views/FeeCollectionRpt.html',
         controller: 'FeeCollectionRptCont'

     })



     .state('main.Fin141', {
         url: "/BankPayment",
         templateUrl: 'app/modules/Finance/views/BankPayment.html',
         controller: 'BankPaymentCont'
     })

    .state('main.Fin141_DPS', {
        url: "/BankPayment_DPS",
        templateUrl: 'app/modules/Finance/views/BankPayment_DPS.html',
        controller: 'BankPayment_DPSCont'
    })

    ///ganesh///
    .state('main.Fin212', {
        url: "/PDCBills",
        templateUrl: 'app/modules/Finance/views/PDCBills.html',
        controller: 'PDCBillsCont'
    })

        //pooja
          .state('main.Com052', {
              url: "/EmailSmsToParent",
              templateUrl: 'app/modules/Collaboration/views/EmailSmsToParent.html',
              controller: 'EmailSmsToParentCont'
          })

                 .state('main.confor', {
                     url: "/contractform",
                     templateUrl: 'app/modules/fee/views/ContractForm.html',
                     controller: 'ContractFormCont'
                 })


         .state('main.ESDPSD',
         {
             url: "/EmailSmsToParent_dpsd",
             templateUrl: 'app/modules/Collaboration/views/EmailSmsToParent_dpsd.html',
             controller: 'EmailSmsToParent_dpsdCont'
         })

        .state('main.Per306', {
            url: "/MarkEmpResignation",
            templateUrl: 'app/modules/HRMS/views/MarkEmpResignation.html',
            controller: 'MarkEmpResignationCont'
        })

         .state('main.Per323', {
             url: "/GratuityCriteria",
             templateUrl: 'app/modules/HRMS/views/GratuityCriteria.html',
             controller: 'GratuityCriteriaCont'
         })

         .state('main.Per401', {
             url: "/LeaveSalaryCriteria",
             templateUrl: 'app/modules/HRMS/views/LeaveSalaryCriteria.html',
             controller: 'LeaveSalaryCriteriaCont'
         })

         .state('main.Per324', {
             url: "/GratuityCalculation",
             templateUrl: 'app/modules/HRMS/views/GratuityCalculation.html',
             controller: 'GratuityCalculationCont'
         })

         .state('main.Per233', {
             url: "/PaysGrade",
             templateUrl: 'app/modules/HRMS/views/PaysGrade.html',
             controller: 'PaysGradeCont'
         })

    //ganesh
    .state('main.Fin213', {
        url: "/PdcPaymentSubmission",
        templateUrl: 'app/modules/Finance/views/PdcPaymentSubmission.html',
        controller: 'PdcPaymentSubmissionCont'
    })

         .state('main.Fin214', {
             url: "/PdcPaymentReSubmission",
             templateUrl: 'app/modules/Finance/views/PdcPaymentReSubmission.html',
             controller: 'PdcPaymentResubmissionCont'
         })


     .state('main.Inv118', {
         url: "/SupplierDepartments",
         templateUrl: 'app/modules/Inventory/views/SupplierDepartments.html',
         controller: 'SupplierDepartmentsCont'
     })

.state('main.Inv011', {
    url: "/supplierGroup",
    templateUrl: 'app/modules/Inventory/views/supplierGroup.html',
    controller: 'supplierGroupCont'
})



.state('main.Inv017', {
    url: "/deliveryMode",
    templateUrl: 'app/modules/Inventory/views/deliveryMode.html',
    controller: 'deliveryMode'
})


    .state('main.Sim110', {
        url: "/CertificateUserGroup",
        templateUrl: 'app/modules/Student/views/CertificateUserGroup.html',
        controller: 'CertificateUserGroupCont'
    })

    .state('main.admrem', {
        url: "/AdmissionPromoteRemark",
        templateUrl: 'app/modules/Student/views/AdmissionPromoteRemark.html',
        controller: 'AdmissionPromoteRemarkCont'
    })


    .state('main.admre1', {
        url: "/AdmissionPromoteRemark_ABQIS",
        templateUrl: 'app/modules/Student/views/AdmissionPromoteRemark_ABQIS.html',
        controller: 'AdmissionPromoteRemark_ABQISCont'
    })


    .state('main.Sim176', {
        url: "/StudentReport",
        templateUrl: 'app/modules/Student/views/Studentreport.html',
        controller: 'StudentreportCont'
    })

.state('main.Sim899', {
        url: "/StudentReportnew",
        templateUrl: 'app/modules/Student/views/StudentreportASD.html',
        controller: 'StudentreportAsdCont'
    })

.state('main.StuRep', {
    url: "/Studentreport_IND",
    templateUrl: 'app/modules/Student/views/Studentreport_IND.html',
    controller: 'Studentreport_INDCont'
})

.state('main.SR_OBA', {
        url: "/StudentReport_OBA",
        templateUrl: 'app/modules/Student/views/Studentreport_OBA.html',
        controller: 'Studentreport_OBACont'
    }) 
      .state('main.SenAdd', {
          url: "/Sen",
          templateUrl: 'app/modules/Student/views/SEN.html',
          controller: 'SENCont'
      })

             .state('main.SenApr', {
                 url: "/SENApprove",
                 templateUrl: 'app/modules/Student/views/SENApprove.html',
                 controller: 'SENApproveCont'
             })

         .state('main.InvIMS', {
             url: "/ItemMovements",
             templateUrl: 'app/modules/Inventory/views/ItemMovements.html',
             controller: 'ItemMovementsCont'
         })
        .state('main.SimSTT', {
            url: "/StaffTimeTable",
            templateUrl: 'app/modules/TimeTable/views/StaffTimeTable.html',
            controller: 'StaffTimeTableCont'
        })
          .state('main.Sturep', {
              url: "/StudentReportAdisw",
              templateUrl: 'app/modules/Student/views/StudentReportAdisw.html',
              controller: 'StudentreportadiswCont'
          })

        .state('main.StuDps', {
            url: "/StudentReportDpsmis",
            templateUrl: 'app/modules/Student/views/StudentreportDpsmis.html',
            controller: 'StudentreportDpsmisCont'
        })


    //Ganesh
    .state('main.Fin215', {
        url: "/PdcPaymentReturn",
        templateUrl: 'app/modules/Finance/views/PdcPaymentReturn.html',
        controller: 'PdcPaymentReturnCont'
    })
        .state('main.Fin216', {
            url: "/PdcPaymentCancel",
            templateUrl: 'app/modules/Finance/views/PdcPaymentCancel.html',
            controller: 'PdcPaymentCancelCont'
        })
         .state('main.Fin217', {
             url: "/PdcPaymentRealize",
             templateUrl: 'app/modules/Finance/views/PdcPaymentRealize.html',
             controller: 'PdcPaymentRealizeCont'
         })

        .state('main.Fin209', {
            url: "/VoucherReversal",
            templateUrl: 'app/modules/Finance/views/VoucherReversal.html',
            controller: 'VoucherReversalCont'
        })

         .state('main.Fin209_DPS', {
             url: "/VoucherReversal_DPS",
             templateUrl: 'app/modules/Finance/views/VoucherReversal_DPS.html',
             controller: 'VoucherReversal_DPSCont'
         })



         .state('main.Fin231', {
             url: "/PDCReversal",
             templateUrl: 'app/modules/Finance/views/PDCReversal.html',
             controller: 'PDCReversalCont'
         })

     .state('main.Fin099', {
         url: "/PDCRececipt",
         templateUrl: 'app/modules/Finance/views/PDCReceipt(BankReceipt).html',
         controller: 'PDCReceipt(BankReceipt)Cont'
     })

     .state('main.Fin152', {
         url: "/CashPayment",
         templateUrl: 'app/modules/Finance/views/CashPayment.html',
         controller: 'CashPaymentCont'
     })

     .state('main.Fin153', {
         url: "/CashReceipt",
         templateUrl: 'app/modules/Finance/views/CashReceipt.html',
         controller: 'CashReceiptCont'
     })
       .state('main.Fin145', {
           url: "/BankReceipt",
           templateUrl: 'app/modules/Finance/views/BankReceipt.html',
           controller: 'BankReceiptCont'
       })

       .state('main.Fin146', {
           url: "/SupplierPayment",
           templateUrl: 'app/modules/Finance/views/SupplierPayment.html',
           controller: 'SupplierPaymentCont'
       })

         .state('main.Fin152_DPS', {
             url: "/CashPayment_DPS",
             templateUrl: 'app/modules/Finance/views/CashPayment_DPS.html',
             controller: 'CashPayment_DPSCont'
         })

     .state('main.Fin153_DPS', {
         url: "/CashReceipt_DPS",
         templateUrl: 'app/modules/Finance/views/CashReceipt_DPS.html',
         controller: 'CashReceipt_DPSCont'
     })
       .state('main.Fin145_DPS', {
           url: "/BankReceipt_DPS",
           templateUrl: 'app/modules/Finance/views/BankReceipt_DPS.html',
           controller: 'BankReceipt_DPSCont'
       })

        .state('main.Fin157_DPS', {
            url: "/PC_Disbursement_DPS",
            templateUrl: 'app/modules/Finance/views/PC_Disbursement_DPS.html',
            controller: 'PC_Disbursement_DPSCont'
        })

        .state('main.Fin158_DPS', {
            url: "/PC_Reimbursement_DPS",
            templateUrl: 'app/modules/Finance/views/PC_Reimbursement_DPS.html',
            controller: 'PC_Reimbursement_DPSCont'
        })

         .state('main.Fin230', {
             url: "/PettyCash",
             templateUrl: 'app/modules/Finance/views/Petty_Cash_DPS.html',
             controller: 'Petty_Cash_DPSCont'
         })

     .state('main.Sim139', {
         url: "/ReportCardAllocation",
         templateUrl: 'app/modules/ReportCard/views/ReportCardAllocation.html',
         controller: 'ReportCardAllocationCont'
     })
        .state('main.Fin156', {
            url: "/UpdateVoucher",
            templateUrl: 'app/modules/Finance/views/UpdateVoucher.html',
            controller: 'UpdateVoucherCont'
        })
        .state('main.Fin156_DPS', {
            url: "/UpdateVoucher_DPS",
            templateUrl: 'app/modules/Finance/views/UpdateVoucher_DPS.html',
            controller: 'UpdateVoucher_DPSCont'
        })
         .state('main.MO', {
             url: "/ManageOrders",
             templateUrl: 'app/modules/inventory/views/ManageOrder.html',
             controller: 'ManageOrderCont'
         })

         .state('main.Invs005', {
             url: "/DeliveryReasons",
             templateUrl: 'app/modules/Inventory/views/DeliveryReasons.html',
             controller: 'DeliveryReasonsCont'
         })

         .state('main.Inv124', {
             url: "/InvsParameter",
             templateUrl: 'app/modules/Inventory/views/InvsParameter.html',
             controller: 'InvsParameterCont'
         })

         .state('main.Inv063', {
             url: "/ProductCode",
             templateUrl: 'app/modules/Inventory/views/ProductCode.html',
             controller: 'ProductCodeCont'
         })

          .state('main.Inv035', {
              url: "/SalesDoc",
              templateUrl: 'app/modules/Inventory/views/SalesDocument.html',
              controller: 'SalesDocumentCont'
          })


          .state('main.InvExc', {
              url: "/SalesDocExchange",
              templateUrl: 'app/modules/Inventory/views/SalesDocumentExchange.html',
              controller: 'SalesDocumentExchangeCont'
          })

         //Ganesh
            .state('main.Sim146', {
                url: "/ReportCardLevel",
                templateUrl: 'app/modules/ReportCard/views/ReportCardLevel.html',
                controller: 'ReportCardLevelCont'
            })

         .state('main.Sim138', {
             url: "/ReportCardFrm",
             templateUrl: 'app/modules/ReportCard/views/ReportCardFrm.html',
             controller: 'ReportCardFrmCont'
         })

         .state('main.Per329', {
             url: "/CreateAttendanceForEmployee",
             templateUrl: 'app/modules/Attendance/views/CreateAttendanceForEmployee.html',
             controller: 'CreateAttendanceForEmployeeCont'
         })
        .state('main.Sim564', {
            url: "/StudentImmunizationApply",
            templateUrl: 'app/modules/Health/views/StudentImmunizationApply.html',
            controller: 'StudentImmunizationApplyCont'
        })
         .state('main.Inv051', {
             url: "/ShipmentDetails",
             templateUrl: 'app/modules/Inventory/views/ShipmentDetails.html',
             controller: 'ShipmentDetailsCont'
         })
          .state('main.Inv201', {
              url: "/ServiceMaster",
              templateUrl: 'app/modules/Inventory/views/ServiceMaster.html',
              controller: 'ServiceMasterCont'
          })
         .state('main.Inv202', {
             url: "/ServiceSuppliermapping",
             templateUrl: 'app/modules/Inventory/views/ServiceSupplierMapping.html',
             controller: 'ServiceSupplierCont'
         })
        .state('main.Inv059', {
            url: "/Salesman",
            templateUrl: 'app/modules/Inventory/views/Salesman.html',
            controller: 'SalesmanCont'
        })
        .state('main.Inv040', {
            url: "/ItemSupplier",
            templateUrl: 'app/modules/Inventory/views/ItemSupplier.html',
            controller: 'ItemSupplierCont'
        })
         .state('main.Inv036', {
             url: "/InternalCustomer",
             templateUrl: 'app/modules/Inventory/views/InternalCustomer.html',
             controller: 'InternalCustomerCont'
         })

        .state('main.GRN', {
            url: "/GRN",
            templateUrl: 'app/modules/Inventory/views/GRN.html',
            controller: 'GRN'
        })

         .state('main.GRNd', {
             url: "/GRNd",
             templateUrl: 'app/modules/Inventory/views/GRN_Deps.html',
             controller: 'GRNd'
         })



        .state('main.GRNA', {
            url: "/GRNApproval",
            templateUrl: 'app/modules/Inventory/views/GRNApprove.html',
            controller: 'GRNApproveCont'
        })
        .state('main.CostingSheetApproval', {
            url: "/CostingSheetApproval",
            templateUrl: 'app/modules/Inventory/views/CostingSheetApprove.html',
            controller: 'CostingSheetApprovalCont'
        })
        .state('main.CostingSheetAssetization', {
            url: "/CostingSheetAssetization",
            templateUrl: 'app/modules/Inventory/views/CostingSheetAssetization.html',
            controller: 'CostingSheetAssetization'
        })
.state('main.TP0005', {
    url: "/Chat",
    templateUrl: 'app/modules/Common/views/Communication.html',
    controller: 'CommunicationController'
})

      .state('main.communication', {
          url: "/Chat",
          templateUrl: 'app/modules/Common/views/Communication.html',
          controller: 'CommunicationController'
      })


          .state('main.commtn', {
              url: "/Chat",
              templateUrl: 'app/modules/Common/views/Communication.html',
              controller: 'CommunicationController'
          })


    .state('main.Com008', {
        url: "/UserGroupMaster",
        templateUrl: 'app/modules/Security/views/UserGroup.html',
        controller: 'UserGroupCont'
    })


        //.state('main.sim204', {
        //    url: "/EmployeeReport",
        //    templateUrl: 'app/modules/HRMS/views/EmployeeReport.html',
        //    controller: 'EmployeeReportCont'
        //})

                .state('main.sim204ASD', {
                    url: "/EmployeeReportASD",
                    templateUrl: 'app/modules/HRMS/views/EmployeeReportASD.html',
                    controller: 'EmployeeReportASDCont'
                })

                .state('main.sim204DPS', {
                    url: "/EmployeeReportDPS",
                    templateUrl: 'app/modules/HRMS/views/EmployeeReportDpsd.html',
                    controller: 'EmployeeReportDpsdCont'
                })


        .state('main.Per311', {
            url: "/EmployeeReport",
            templateUrl: 'app/modules/HRMS/views/EmployeeReport.html',
            controller: 'EmployeeReportCont'
        })

        .state('main.Com009', {
            url: "/UserRole",
            templateUrl: 'app/modules/Security/views/UserRole.html',
            controller: 'UserRoleCont'
        })

          .state('main.Fin054', {
              url: "/DocumentUser",
              templateUrl: 'app/modules/Finance/views/DocumentUser.html',
              controller: 'DocumentUserCont'
          })

         .state('main.Fin228', {
             url: "/DocumentUser_PC",
             templateUrl: 'app/modules/Finance/views/DocumentUser_PC.html',
             controller: 'DocumentUser_PCCont'
         })


         .state('main.Fin17A', {
             url: "/DepreciationJVPost",
             templateUrl: 'app/modules/Finance/views/DepreciationJVPost.html',
             controller: 'DepreciationJVPostCont'
         })

    .state('main.Fin218', {
        url: "/AssetAllocation",
        templateUrl: 'app/modules/Finance/views/AssetAllocation.html',
        controller: 'AssetAllocationCont'
    })


        .state('main.Cls10T', {
            url: "/ClassListTeacherView",
            templateUrl: 'app/modules/Student/views/ClassListTeacher.html',
            controller: 'ClassListTeacherCont'
        })


    .state('main.Fin221', {
        url: "/CostCentre",
        templateUrl: 'app/modules/Finance/views/CostCentre.html',
        controller: 'CostCentreCont'
    })
    .state('main.Fin220', {
        url: "/GlCostCentreMapping",
        templateUrl: 'app/modules/Finance/views/GlCostCentreMapping.html',
        controller: 'GlCostCentreMappingCont'
    })
    .state('main.Per300', {//Per300
        url: "/EmployeeLeaveApplication",
        templateUrl: 'app/modules/Hrms/views/EmployeeLeaveApplication.html',
        controller: 'EmployeeLeaveApplicationCont',
    })
    .state('main.P300SO', {//Per300
        url: "/EmployeeLeaveApplicationSISO",
        templateUrl: 'app/modules/Hrms/views/EmployeeLeaveApplication_SISO.html',
        controller: 'EmployeeLeaveApplication_SISOCont',
    })
    .state('main.P300NW', {//Per300
        url: "/EmployeeLeaveApplication_Adisw",
        templateUrl: 'app/modules/Hrms/views/EmployeeLeaveApplication_Adisw.html',
        controller: 'EmployeeLeaveApplication_AdiswCont',
    })
     .state('main.Per116', {
         url: "/LeaveType",
         templateUrl: 'app/modules/HRMS/views/LeaveType.html',
         controller: 'LeaveTypeCont'
     })
     .state('main.Per260', {
         url: "/EmployeeLeaveAssign",
         templateUrl: 'app/modules/HRMS/views/EmployeeLeaveAssign.html',
         controller: 'EmployeeLeaveAssignCont'
     })
     .state('main.Per303', {//Per303
         url: "/EmployeeLeaveDetails",
         templateUrl: 'app/modules/Hrms/views/EmployeeLeaveDetails.html',
         controller: 'EmployeeLeaveDetailsCont',
     })
     .state('main.Per318', {//Per318
         url: "/TransLeaveApproval",
         templateUrl: 'app/modules/Hrms/views/TransLeaveApproval.html',
         controller: 'TransLeaveApprovalCont',
     })

        //Cancel Admission
        .state('main.Sim668', {
            url: "/TcCertificateRequiest",
            templateUrl: 'app/modules/Setup/views/TcCertificateRequiest.html',
            controller: 'TcCertificateRequiestCont'
        })

         .state('main.Sim627', {
             url: "/admissionquota",
             templateUrl: 'app/modules/Student/views/AdmisisonQuota.html',
             controller: 'AdmisisonQuotaCont'
         })
        .state('main.Sim628', {
            url: "/sectionelectivesubject",
            templateUrl: 'app/modules/Student/views/SectionElectiveSubjectDetails.html',
            controller: 'SectionElectiveSubjectDetailsCont'
        })

        .state('main.Sim624', {
            url: "/CancelAdmissionApproval",
            templateUrl: 'app/modules/Student/views/CancelAdmissionApproval.html',
            controller: 'CancelAdmissionApprovalCont'
        })

        .state('main.Sim625', {
            url: "/CancelAdmToTC",
            templateUrl: 'app/modules/Student/views/CancelAdmToTC.html',
            controller: 'CancelAdmToTCCont'
        })
        .state('main.Sim626', {
            url: "/TCFromCancelAdm",
            templateUrl: 'app/modules/Student/views/TCFromCancelAdm.html',
            controller: 'TCFromCancelAdmCont',
            params: { edt: '' }
        })

        .state('main.Sim619', {
            url: "/TcToCancelAdm",
            templateUrl: 'app/modules/Student/views/TcToCancelAdm.html',
            controller: 'TcToCancelAdmCont'
        })
        .state('main.Sim620', {
            url: "/CancelAdmFromTc",
            templateUrl: 'app/modules/Student/views/CancelAdmFromTc.html',
            controller: 'CancelAdmFromTcCont',
            params: { tcsend: '' }
        })

        .state('main.Sim623', {
            url: "/TcWithClearance",
            templateUrl: 'app/modules/Student/views/TcWithClearance.html',
            controller: 'TcWithClearanceCont',
            params: { edt: '' }
        })

        .state('main.Sim622', {
            url: "/CancelAdmissionClearFees",
            templateUrl: 'app/modules/Student/views/CancelAdmissionClear.html',
            controller: 'CancelAdmissionClearCont',
            params: { data: '', back: '' },
        })

    //My Change

                .state('main.Sim546', {
                    url: "/AdmissionReport",
                    templateUrl: 'app/modules/Student/views/AdmissionReport.html',
                    controller: 'AdmissionReportCont'
                })


              .state('main.SimRep', {
                  url: "/AdmissionReportSVCC",
                  templateUrl: 'app/modules/Student/views/AdmissionReportSVCC.html',
                  controller: 'AdmissionReportSVCCCont'
              })
             .state('main.dvhRep', {
                 url: "/AdmissionReportDvhss",
                 templateUrl: 'app/modules/Student/views/AdmissionReportDvhss.html',
                 controller: 'AdmissionReportSVCCCont'
             })

         .state('main.SimSAV', {
             url: "/StudentAttendaceView",
             templateUrl: 'app/modules/Attendance/views/StudentAttendaceView.html',
             controller: 'StudentAttendaceViewCont'
         })

         .state('main.InvPRD', {
             url: "/RequestDetails",
             templateUrl: 'app/modules/Inventory/views/RequestDetailsAsd.html',
             controller: 'RequestDetailsAsdCont'
         })

         .state('main.InvPRA', {
             url: "/RequestApprove",
             templateUrl: 'app/modules/Inventory/views/RequestApproveAsd.html',
             controller: 'RequestApproveAsdCont'
         })

        .state('main.SimCMF', {
            url: "/ComplaintMaintenance",
            templateUrl: 'app/modules/Setup/views/ComplaintMaintenance.html',
            controller: 'ComplaintMaintenanceCont'
        })
        .state('main.SimCMP', {
            url: "/ComplaintProcessComplate",
            templateUrl: 'app/modules/Setup/views/ComplaintProcessComplate.html',
            controller: 'ComplaintProcessComplateCont'
        })


        //.state('main.fildts', {
        //    url: "/FieldDetails",
        //    templateUrl: 'app/modules/Setup/views/FieldDetails.html',
        //    controller: 'FieldDetailsCont'
        //})
				.state('main.Test99', {
				    url: "/TestTT",
				    templateUrl: 'app/modules/Health/views/Test.html',
				    controller: 'TestCont'
				})

        .state('main.SimMCS', {
            url: "/MedicalCheckupSchedule",
            templateUrl: 'app/modules/Health/views/MedicalCheckupSchedule.html',
            controller: 'MedicalCheckupScheduleCont'
        })
         .state('main.SimMSC', {
             url: "/MedicalStudentDetail",
             templateUrl: 'app/modules/Health/views/MedicalStudentDetail.html',
             controller: 'MedicalStudentDetailCont'
         })


        .state('main.SimTMS', {
            url: "/TransportStudentFeeorTransportMissing",
            templateUrl: 'app/modules/Fleet/views/TransportStudentFeeorTransportMissing.html',
            controller: 'TransportStudentFeeorTransportMissingCont'
        })

         .state('main.SimASN', {
             url: "/ApproveStudentTransportNew",
             templateUrl: 'app/modules/Fleet/views/ApproveStudentTransportNew.html',
             controller: 'ApproveStudentTransportNewCont'
         })
          .state('main.SimTHS', {
              url: "/TransportStudentHistory",
              templateUrl: 'app/modules/Fleet/views/TransportHistoryOfStudent.html',
              controller: 'TransportHistoryOfStudentCont'
          })

         .state('main.SimTNA', {
             url: "/TransportRouteStudentDpsmis",
             templateUrl: 'app/modules/Fleet/views/TransportRouteStudDpsmis.html',
             controller: 'TransportRouteStudDpsmisCont'
         })
         .state('main.SimTUC', {
             url: "/TransportUpdateCancelDpsmis",
             templateUrl: 'app/modules/Fleet/views/TransportCancelUpdateN.html',
             controller: 'TransportCancelUpdateNCont'
         })
         .state('main.SimTVS', {
             url: "/TransportMaintenance",
             templateUrl: 'app/modules/Fleet/views/TransportMaintenance.html',
             controller: 'TransportMaintenanceCont',
         })

        .state('main.InvSAD', {
            url: "/StockAdjustmentDpsmisAdd",
            templateUrl: 'app/modules/Inventory/views/StockAdjustmentDpsmisAdd.html',
            controller: 'StockAdjustmentDpsmisAddCont',
        })

    .state('main.InvSRD', {
        url: "/StockAdjustmentDpsmisRemove",
        templateUrl: 'app/modules/Inventory/views/StockAdjustmentDpsmisRemove.html',
        controller: 'StockAdjustmentDpsmisRemoveCont',
    })

         .state('main.InvSAA', {
             url: "/StockAdjustmentApproval",
             templateUrl: 'app/modules/Inventory/views/StockAdjustmentApproval.html',
             controller: 'StockAdjustmentApprovalCont',
         })

         .state('main.SimSBT', {
             url: "/Target",
             templateUrl: 'app/modules/Student/views/StudentBehaviourTarget.html',
             controller: 'StudentBehaviourTargetCont'
         })

         .state('main.SimSTD', {
             url: "/StudentBehaviourTargetDetails",
             templateUrl: 'app/modules/Student/views/StudentBehaviourTargetDetails.html',
             controller: 'StudentBehaviourTargetDetailsCont'
         })

    .state('main.sim215', {
        url: "/ManualFeePosting",
        templateUrl: 'app/modules/Fee/views/ManualFeePosting.html',
        controller: 'ManualFeePostingCont',
    })

            .state('main.sim220', {
                url: "/ManualFeePosting_New",
                templateUrl: 'app/modules/Fee/views/ManualFeePosting_New.html',
                controller: 'ManualFeePosting_NewCont',
            })
        .state('main.Inv301', {
            url: "/ManualInventoryFeePosting",
            templateUrl: 'app/modules/Fee/views/ManualInventoryFeePosting.html',
            controller: 'ManualInventoryFeePostingCont',
        })

        .state('main.Per335', {
            url: "/EmployeeReportOES",
            templateUrl: 'app/modules/HRMS/views/EmployeeReportOES.html',
            controller: 'EmployeeReportOesCont'
        })

         .state('main.Inv203', {
             url: "/PendingRequest",
             templateUrl: 'app/modules/Inventory/views/PendingRequest.html',
             controller: 'PendingRequestCont',
         })

         .state('main.Inv204', {
             url: "/PendingOrder",
             templateUrl: 'app/modules/Inventory/views/PendingOrder.html',
             controller: 'PendingOrderCont',
         })

        .state('main.Inv205', {
            url: "/ReprintOrders",
            templateUrl: 'app/modules/Inventory/views/ReprintOrders.html',
            controller: 'ReprintOrdersCont',
        })
         //.state('main.Inv175', {
         //    url: "/RequestApprove",
         //    templateUrl: 'app/modules/Inventory/views/RequestApprove.html',
         //    controller: 'RequestApproveCont'
         //})
         .state('main.Fin234', {
             url: "/VehicleExpense",
             templateUrl: 'app/modules/Finance/views/VehicleExpense.html',
             controller: 'VehicleExpenseCont'
         })

    .state('main.Inv047', {
        url: "/StockAdjustment",
        templateUrl: 'app/modules/Inventory/views/StockAdjustment.html',
        controller: 'StockAdjustmentCont',
    })

        .state('main.Sim561', {
            url: "/StudentDocumentListNew",
            templateUrl: 'app/modules/Student/views/StudentDocumentListNew.html',
            controller: 'StudentDocumentListNewCont'
        })


                 .state('main.SimRCU', {
                     url: "/ReportCardUpload",
                     templateUrl: 'app/modules/Gradebook/views/ReportCardUpload.html',
                     controller: 'ReportCardUploadCont'
                 })



        .state('main.SimFee', {
            url: "/FeesSplit",
            templateUrl: 'app/modules/Fee/views/FeesSplit.html',
            controller: 'FeesSplitCont'
        })

        .state('main.SimIEM', {
            url: "/EmailInvoice",
            templateUrl: 'app/modules/Fee/views/SimsInvoiceEmail.html',
            controller: 'SimsInvoiceEmailCont'
        })

       .state('main.SimTIG', {
           url: "/TransportInvoice",
           templateUrl: 'app/modules/Fee/views/TransportInvoice.html',
           controller: 'TransportInvoiceCont'
       })


        .state('main.SimSEL', {
            url: "/AbsentStudentsNotification",
            templateUrl: 'app/modules/Student/views/AbsentStudentsNotification.html',
            controller: 'AbsentStudentsNotificationCont'
        })

        .state('main.Inv669', {
            url: "/SupplierLocation",
            templateUrl: 'app/modules/Inventory/views/SupplierLocation.html',
            controller: 'SupplierLocationCont'
        })

         .state('main.Sim669', {
             url: "/SupplierLocation",
             templateUrl: 'app/modules/Inventory/views/SupplierLocation.html',
             controller: 'SupplierLocationCont'
         })

          .state('main.Per319', {
              url: "/VacancyMaster",
              templateUrl: 'app/modules/HRMS/views/VacancyMaster.html',
              controller: 'VacancyMasterCont'
          })

         .state('main.Per331', {
             url: "/VacancyView",
             templateUrl: 'app/modules/HRMS/views/VacancyView.html',
             controller: 'VacancyViewCont'
         })

         .state('main.Sim101', {
             url: "/MedicalVisit",
             templateUrl: 'app/modules/Health/views/MedicalVisit.html',
             controller: 'MedicalVisitCont'
         })

            .state('main.Sim106', {
                url: "/MedicalVisitCSD",
                templateUrl: 'app/modules/Health/views/MedicalVisitCSD.html',
                controller: 'MedicalVisitCSDCont'
            })
        .state('main.Clar10', {
            url: "/MedicalParametersClara",
            templateUrl: 'app/modules/Health/views/MedicalParameters.html',
            controller: 'MedicalParametersCont'
        })

            .state('main.FINS15', {
                url: "/PrintVoucher",
                templateUrl: 'app/modules/Finance/views/PrintVoucher.html',
                controller: 'PrintVoucherCont'
            })

         .state('main.FINR15', {
             url: "/PrintVoucher",
             templateUrl: 'app/modules/Finance/views/PrintVoucher.html',
             controller: 'PrintVoucherCont'
         })

                 .state('main.INVREC', {
                     url: "/InvoiceReceiveable",
                     templateUrl: 'app/modules/Finance/views/InvoiceReceiveable.html',
                     controller: 'InvoiceReceiveableCont'
                 })


         .state('main.SFNISS', {
             url: "/DefineStudentFeeNISS",
             templateUrl: 'app/modules/Fee/views/DefineStudentFeeNISS.html',
             controller: 'DefineStudentFeeNISSCont'
         })



        .state('main.InvsIV', {
            url: "/ItemValuation",
            templateUrl: 'app/modules/Inventory/views/ItemValuation.html',
            controller: 'ItemValuationCont'
        })

        .state('main.FINR15_DPS', {
            url: "/PrintVoucher_DPS",
            templateUrl: 'app/modules/Finance/views/PrintVoucher_DPS.html',
            controller: 'PrintVoucher_DPSCont'
        })

     .state('main.Inv148', {
         url: "/InterDepartmentTransfer",
         templateUrl: 'app/modules/Inventory/views/InterDepartmentTransfer.html',
         controller: 'InterDepartmentTransferCont',
     })

     .state('main.InvRes', {
         url: "/CancelSalesReceipt",
         templateUrl: 'app/modules/Inventory/views/CancelSalesReceipt.html',
         controller: 'CancelSalesReceiptCont',
     })

         //New item issue formss
        //Item iisues
         .state('main.InvCan', {
             url: "/CancelSalesReceiptI",
             templateUrl: 'app/modules/Inventory/views/CancelSalesReceipt_internal.html',
             controller: 'CancelSalesReceipt_internalCont',
         })

          .state('main.InvInt', {
              url: "/InterDepartmentTransferI",
              templateUrl: 'app/modules/Inventory/views/InterDepartmentTransfer_internal.html',
              controller: 'InterDepartmentTransfer_internalCont',
          })

         .state('main.InvSII', {
             url: "/SaleDocumentsI",
             templateUrl: 'app/modules/Inventory/views/SalesDocument_internal.html',
             controller: 'SalesDocument_internalCont'
         })

         .state('main.Inv038', {
             url: "/ItemDepartment",
             templateUrl: 'app/modules/Inventory/views/ItemDepartment.html',
             controller: 'ItemDepartmentCont'
         })
         .state('main.Inv160', {
             url: "/SaleDocumentList",
             templateUrl: 'app/modules/Inventory/views/SaleDocumentList.html',
             controller: 'SaleDocumentListCont',
         })

         .state('main.Inv065', {
             url: "/ItemAssembly",
             templateUrl: 'app/modules/Inventory/views/ItemAssembly.html',
             controller: 'ItemAssemblyCont'
         })


         .state('main.Sim616', {
             url: "/ParentRegistration",
             templateUrl: 'app/modules/Student/views/ParentRegistration.html',
             controller: 'ParentRegistrationCont'
         })

        .state('main.Per304', {
            url: "/LeaveDetails",
            templateUrl: 'app/modules/HRMS/views/LeaveDetails.html',
            controller: 'LeaveDetailsCont'
        })

        .state('main.LEAVE', {
            url: "/LeavePolicy",
            templateUrl: 'app/modules/HRMS/views/EmployeeLeavePolicy.html',
            controller: 'EmployeeLeavePolicyCont'
        })

         .state('main.Per312', {
             url: "/EmployeeDocumentUploadNew",
             templateUrl: 'app/modules/HRMS/views/EmployeeDocumentUploadNew.html',
             controller: 'EmployeeDocumentUploadNewCont'
         })

        .state('main.Sim613', {
            url: "/Honour",
            templateUrl: 'app/modules/Setup/views/Honour.html',
            controller: 'HonourCont'
        })

    .state('main.Sim614', {
        url: "/UserHonour",
        templateUrl: 'app/modules/Setup/views/UserHonour.html',
        controller: 'UserHonourCont'
    })


           .state('main.Inv200', {
               url: "/VehicleConsumptionDetails",
               templateUrl: 'app/modules/Inventory/views/VehicleConsumptionDetails.html',
               controller: 'VehicleConsumptionDetailsCont'
           })

         .state('main.Sim171', {
             url: "/timeTableUpload",
             templateUrl: 'app/modules/Scheduling/views/timeTableUpload.html',
             controller: 'timeTableUploadCont'
         })

         .state('main.Sim104', {
             url: "/CertificateMaster",
             templateUrl: 'app/modules/Student/views/CertificateMaster.html',
             controller: 'CertificateMasterCont'
         })

            .state('main.Sim664', {
                url: "/TelephonicConversationDetails",
                templateUrl: 'app/modules/Student/views/TelephonicConversationDetails.html',
                controller: 'TelephonicConversationDetailsCont'
            })
        //Re-Admission
        .state('main.Sim670', {
            url: "/ReAdmission",
            templateUrl: 'app/modules/Student/views/ReAdmission.html',
            controller: 'ReAdmissionCont'
        })


         .state('main.Pe078V', {
             url: "/ViewPayslip",
             templateUrl: 'app/modules/HRMS/views/ViewPayslip.html',
             controller: 'ViewPayslipCont'
         })

          .state('main.aquota', {
              url: "/admissionQuota",
              templateUrl: 'app/modules/Student/views/admissionQuotaMaster.html',
              controller: 'admissionQuotaMasterCont'
          })

        .state('main.uquota', {
            url: "/admissionQuotaUser",
            templateUrl: 'app/modules/Student/views/admissionQuotaUser.html',
            controller: 'admissionQuotaUserCont'
        })

        .state('main.rpts', {
            url: "/Receipt",
            templateUrl: 'app/modules/Inventory/views/receipt.html',
            controller: 'ReceiptCont'
        })

         .state('main.GlbMap', {
             url: "/GlobalSearchMapping",
             templateUrl: 'app/modules/Setup/views/GlobalSearchMapping.html',
             controller: 'GlobalSearchMappingCont'
         })


        .state('main.Inv149', {
            url: "/ApprovedIDT",
            templateUrl: 'app/modules/Inventory/views/ApprovedIDT.html',
            controller: 'ApprovedIDTCont',
        })

        .state('main.inv138', {
            url: "/Inventory",
            templateUrl: 'app/modules/Inventory/views/Inv138.html',
            controller: 'Inv138Controller'
        })
    .state('main.SAA003', {
        url: "/ActivityGroupMaster",
        templateUrl: 'app/modules/Gradebook/views/ActivityGroupMaster.html',
        controller: 'ActivityGroupMasterCont'
    })

    .state('main.SAA005', {
        url: "/ActivityExamMaster",
        templateUrl: 'app/modules/Gradebook/views/ActivityExamMaster.html',
        controller: 'ActivityExamMasterCont'
    })

    .state('main.SAA004', {
        url: "/ActivityIndicatorGroup",
        templateUrl: 'app/modules/Gradebook/views/ActivitiyIndicator.html',
        controller: 'ActivityIndicatorCont'
    }).state('main.SAA001', {
        url: "/SAA001",
        templateUrl: 'app/modules/Gradebook/views/AcitivityConfiguration.html',
        controller: 'ActivityConfigurationController'
    }).state('main.SAA002', {
        url: "/SAA002",
        templateUrl: 'app/modules/Gradebook/views/studentactivityentry.html',
        controller: 'StudentActivityEntryController'
    }).state('main.SAA007', {
        url: "/SAA007",
        templateUrl: 'app/modules/Gradebook/views/CopyAcitivityConfiguration.html',
        controller: 'CopyActivityConfigurationController'
    })

     .state('main.repcat', {
         url: "/ReportCardAttendance",
         templateUrl: 'app/modules/Gradebook/views/ReportCardAttendance.html',
         controller: 'ReportCardAttendanceCont'
     })

    .state('main.quaemp', {
        url: "/EmployeeQualification",
        templateUrl: 'app/modules/HRMS/views/QualificationEmployee_ADISW.html',
        controller: 'QualificationEmployeeADISWCont'
    })

            .state('main.EQR001', {
                url: "/EmployeeQualificationReport",
                templateUrl: 'app/modules/HRMS/views/EmployeeQualificationReport.html',
                controller: 'EmployeeQualificationReportCont'
            })

     .state('main.SEE001', {
         url: "/ScholasticEvalExamMaster",
         templateUrl: 'app/modules/Gradebook/views/ScholasticEvalExamMaster.html',
         controller: 'ScholasticEvalExamMasterCont'
     })

     .state('main.SEE002', {
         url: "/ScholasticEvalGroupMaster",
         templateUrl: 'app/modules/Gradebook/views/ScholasticEvalGroupMaster.html',
         controller: 'ScholasticEvalGroupMasterCont'
     })

    .state('main.SEE003', {
        url: "/ScholasticEvalIndicatorGroupMaster",
        templateUrl: 'app/modules/Gradebook/views/ScholasticEvalIndicatorGroupMaster.html',
        controller: 'ScholasticEvalIndicatorGroupMasterCont'
    }).state('main.SEE004', {
        url: "/SEE004",
        templateUrl: 'app/modules/Gradebook/views/ScholasticEvaluation.html',
        controller: 'ScholasticEvaluationController'
    })
    .state('main.SEE005', {
        url: "/SEE005",
        templateUrl: 'app/modules/Gradebook/views/Studentevaluationentry.html',
        controller: 'StudentEvaluationEntryController'
    })


        .state('main.Lrnton', {
            url: "/LearntronSyc",
            templateUrl: 'app/modules/Scheduling/views/Learntron.html',
            controller: 'LearntronCont'
        })

    .state('main.empa01', {
        url: "/EmployeeAttendanceCode",
        templateUrl: 'app/modules/Attendance/views/EmployeeAttendanceCode.html',
        controller: 'EmployeeAttendanceCodeCont'
    })

    .state('main.home01', {
        url: "/HomeroomTeacherCreation",
        templateUrl: 'app/modules/Setup/views/HomeroomTeacherCreation.html',
        controller: 'HomeRoomTeacherCreationCont'
    })

    .state('main.stcl01', {
        url: "/StudentClearanceStatus",
        templateUrl: 'app/modules/Student/views/StudentClearanceStatus.html',
        controller: 'StudentClearanceStatusCont'
    })

     .state('main.maq256', {
         url: "/MasterQualification",
         templateUrl: 'app/modules/HRMS/views/QualificationMaster_OES.html',
         controller: 'QualificationMasterOESCont'
     })

     .state('main.des234', {
         url: "/DesignationOES",
         templateUrl: 'app/modules/HRMS/views/DesignationOES.html',
         controller: 'DesignationOESCont'
     })

     .state('main.feepar', {
         url: "/FeeTransactionDetailsParent",
         templateUrl: 'app/modules/fee/views/FeeTransactionDetailsParent.html',
         controller: 'FeeTransactionDetailsParentCont'
     })

     .state('main.tectim', {
         url: "/teacherTimeTable",
         templateUrl: 'app/modules/Scheduling/views/teacherTimeTable.html',
         controller: 'teacherTimeTableCont'
     })

        .state('main.tectiv', {
            url: "/teacherTimeTableView",
            templateUrl: 'app/modules/Scheduling/views/teacherTimeTableView.html',
            controller: 'teacherTimeTableViewCont'
        })

        .state('main.feepan', {
            url: "/FeeTransactionDetailsParentNew",
            templateUrl: 'app/modules/fee/views/FeeTransactionDetailsParentNew.html',
            controller: 'FeeTransactionDetailsParentNewCont'
        })

    .state('main.onpyfe', {
        url: "/OnlinePaymentFeeReceipt",
        templateUrl: 'app/modules/Fee/views/OnlinePaymentFeeReceipt.html',
        controller: 'OnlinePaymentFeeReceiptCont'
    })


    .state('main.stuimg', {
        url: "/StudentViewImage",
        templateUrl: 'app/modules/Student/views/StudentOnlyViewImage.html',
        controller: 'StudentOnlyViewImageCont'
    })

     .state('main.studdb', {
         url: "/StudentDB",
         templateUrl: 'app/modules/Student/views/StudentDatabase_DPSMIS.html',
         controller: 'StudentDatabaseDPSMISCont'
     })

     .state('main.edtemp', {
         url: "/EditEmployee",
         templateUrl: 'app/modules/HRMS/views/UpdateEmployee_DPSMIS.html',
         controller: 'UpdateEmployeeDPSMISCont'
     })

    .state('main.empmst', {
        url: "/EmployeeMasterDPS",
        templateUrl: 'app/modules/HRMS/views/EmployeeMaster_DPSMIS.html',
        controller: 'EmployeeMasterDPSMISCont'
    })

     .state('main.pycdup', {
         url: "/PaycodeWiseUpdate",
         templateUrl: 'app/modules/HRMS/views/PayrollEmployee_1_DPSMIS.html',
         controller: 'PayrollEmployee_DPSMISCont'
     })

    .state('main.viwexa', {
        url: "/ViewExamPaper",
        templateUrl: 'app/modules/Assignment/views/ViewExamPaper.html',
        controller: 'ViewExamPaperCont'
    })

         .state('main.BaseMk', {
             url: "/BaselineMarks",
             templateUrl: 'app/modules/Gradebook/views/BaselineMarks.html',
             controller: 'BaselineMarksCont'
         })

         .state('main.TagtMk', {
             url: "/TargetMarks",
             templateUrl: 'app/modules/Gradebook/views/TargetMarks.html',
             controller: 'TargetMarksCont'
         })

         .state('main.BasTar', {
             url: "/BaselineTargetMarks",
             templateUrl: 'app/modules/Gradebook/views/BaselineTargetMarks.html',
             controller: 'BaselineTargetMarksCont'
         })

    .state('main.credem', {
        url: "/CreateEditEMP",
        templateUrl: 'app/modules/HRMS/views/CreateEditEmployee_Pearl.html',
        controller: 'CreateEditEmployee_PearlCont'
    })

    .state('main.Sim442', {
        url: "/StudDatabase",
        templateUrl: 'app/modules/Student/views/StudentDatabase_AELC.html',
        controller: 'StudentDatabaseAELCCont'
    })

     .state('main.Sim446', {
         url: "/StudData",
         templateUrl: 'app/modules/Student/views/StudentDatabase_IIS.html', //StudentDatabase_IIS.html
         controller: 'StudentDatabaseCont'
     })

    .state('main.Sim448', {
        url: "/StudDataASD",
        templateUrl: 'app/modules/Student/views/StudentDatabase_ASD.html',
        controller: 'StudentDatabaseASDCont'
    })

    .state('main.Sim450', {
        url: "/StudDataQFIS",
        templateUrl: 'app/modules/Student/views/StudentDatabase_QFIS.html',
        controller: 'StudentDatabaseQFISCont'
    })

    .state('main.empnis', {
        url: "/EmployeeMasterNIS",
        templateUrl: 'app/modules/HRMS/views/EmployeeMaster_NISS.html',
        controller: 'EmployeeMasterNISSCont'
    })

    .state('main.TrEMPD', {
        url: "/TransferEMP",
        templateUrl: 'app/modules/HRMS/views/TransferEmployee_DPSMIS.html',
        controller: 'TransferEmployeeDPSMISCont'
    })

    .state('main.Per07E', {
        url: "/PayrollEmployeeEdison",
        templateUrl: 'app/modules/HRMS/views/PayrollEmployeeEdison.html',
        controller: 'PayrollEmployeeEdisonCont'
    })

     .state('main.SimAtm', {
         url: "/StudAttendanceMonthly",
         templateUrl: 'app/modules/Attendance/views/StudentAttendanceMonthly.html',
         controller: 'StudentAttendanceMonthlyASDCont'
     })

    .state('main.Simatd', {
        url: "/StudAttendance",
        templateUrl: 'app/modules/Attendance/views/StudentAttendance.html',
        controller: 'StudentAttendanceASDCont'
    })

     .state('main.Survey', {
         url: "/SurveyDetails",
         templateUrl: 'app/modules/Student/views/SurveyDetails.html',
         controller: 'SurveyDetailsCont'
     })

    .state('main.blklst', {
        url: "/BlackList",
        templateUrl: 'app/modules/Fee/views/BlackList.html',
        controller: 'BlackListCont'
    })
                .state('main.Sim774', {
                    url: "/AccessConfig",
                    templateUrl: 'app/modules/Common/views/AccessControlConfig.html',
                    controller: 'AccessControlConfigCont'
                })
    .state('main.Sim773', {
        url: "/AccessConfig1",
        templateUrl: 'app/modules/Common/views/AccessControlRoleDegConfig.html',
        controller: 'AccessControlRoleDegConfigCont'
    })
    .state('main.fildts', {
        url: "/FieldDetails",
        templateUrl: 'app/modules/Setup/views/FieldDetails.html',
        controller: 'FieldDetailsCont'
    })



    .state('main.demo1', {
        url: "/demo",
        templateUrl: 'app/modules/Fee/views/demo.html',
        controller: 'demoCont'
    })

            .state('main.test', {
                url: "/test",
                templateUrl: 'app/modules/Fee/views/test.html',
                controller: 'testCont'
            })

     .state('main.SIBDE1', {
         url: "/sibling_details",
         templateUrl: 'app/modules/Student/views/sibling_details.html',
         controller: 'sibling_detailsCont'
     })

    .state('main.Sim43N', {
        url: "/FeeDet1",
        templateUrl: 'app/modules/Fee/views/SFSNew.html',
        controller: 'SFCControllerNew',
        params: { 'sel': {} }
    })


    .state('main.Sim43S', {
        url: "/Coll_FeeDet",
        templateUrl: 'app/modules/Fee/views/SFSNew_ABQIS.html',
        controller: 'SFCControllerNew_ABQIS',
        params: { 'sel': {} }
    })
             .state('main.feedept', {
                 url: "/Coll_FeeDept",
                 templateUrl: 'app/modules/Fee/views/SFSNew_FeeDept.html',
                 controller: 'SFCControllerNew_FeeDept',
                 params: { 'sel': {} }
             })

       .state('main.SiVATN', {
           url: "/FeeDet1_VAT",
           templateUrl: 'app/modules/Fee/views/SFSNew_VAT.html',
           controller: 'SFCControllerNew_VAT',
           params: { 'sel': {} }
       })


     .state('main.Sim043SearchNew', {
         url: "/FeeTransaction2new",
         templateUrl: 'app/modules/Fee/views/FeeTransactionNew.html',
         controller: 'FeeTransactionControllerNew',
         params: { 'IP': {}, 'sel': {} }
     })

            .state('main.Sim043SearchNew_ABQIS', {
                url: "/Coll_FeeTransaction2new",
                templateUrl: 'app/modules/Fee/views/FeeTransactionNew_ABQIS.html',
                controller: 'FeeTransactionControllerNew_ABQIS',
                params: { 'IP': {}, 'sel': {} }
            })

      .state('main.Sim043SearchNew_VAT', {
          url: "/FeeTransaction2new_VAT",
          templateUrl: 'app/modules/Fee/views/FeeTransactionNew_VAT.html',
          controller: 'FeeTransactionControllerNew_VAT',
          params: { 'IP': {}, 'sel': {} }
      })




      .state('main.SIBD11', {
          url: "/exp",
          templateUrl: 'app/modules/Student/views/exp.html',
          controller: 'expCont'
      })

    .state('main.FEELEG', {
        url: "/student_fee_ledger_report_app",
        templateUrl: 'app/modules/Fee/views/student_fee_ledger_report_app.html',
        controller: 'student_fee_ledger_report_appCont'
    })

     .state('main.Per321', {
         url: "/Reregistration",
         templateUrl: 'app/modules/HRMS/views/EMPReRegistration.html',
         controller: 'EmpReRegistrationCont'
     })
    .state('main.TWARpt', {
        url: "/TermAttendanceReport",
        templateUrl: 'app/modules/Attendance/views/TermWiseAttendanceRpt.html',
        controller: 'TermWiseAttendanceRptCont'
    })
     .state('main.LibraryItemReport', {
         url: "/LibraryItemReport",
         templateUrl: 'app/modules/Library/views/LibraryItemReport.html'
         //controller: 'TermWiseAttendanceRptCont'
     })

    .state('main.lbfrpt', {
        url: "/LibraryForm_report",
        templateUrl: 'app/modules/Library/views/LibraryForm_report.html',
        controller: 'LibraryForm_reportCont'
    })

    .state('main.stdnis', {
        url: "/StudDBNIS",
        templateUrl: 'app/modules/Student/views/StudentDatabase_NISS.html',
        controller: 'StudentDatabaseNISSCont'
    })

    .state('main.SALVI1', {
        url: "/SalaryView",
        templateUrl: 'app/modules/HRMS/views/salary_view.html',
        controller: 'salary_viewCont'
    })

     .state('main.Per257', {
         url: "/LeaveDocumentMaster",
         templateUrl: 'app/modules/HRMS/views/LeaveDocumentMaster.html',
         controller: 'LeaveDocumentMasterCont'
     })

    .state('main.paygen', {
        url: "/GeneratePayroll",
        templateUrl: 'app/modules/HRMS/views/GeneratePayroll_DPSMIS.html',
        controller: 'GeneratePayrollDPSMISCont'
    })

    .state('main.LIBREP', {
        url: "/LibraryReportApp",
        templateUrl: 'app/modules/Library/views/LibraryReportApp.html',
        controller: 'LibraryReportAppCont'
    })

    .state('main.MOBREP', {
        url: "/MobileAppUsage",
        templateUrl: 'app/modules/Common/views/MobileAppUsage.html',
        controller: 'MobileAppUsageCont'
    })

     .state('main.STUDET', {
         url: "/StudentDetentionTransaction",
         templateUrl: 'app/modules/incidence/views/StudentDetentionTransaction.html',
         controller: 'StudentDetentionTransaction'
     })

	.state('main.Studg', {
	    url: "/StudentDetentionTransaction_geis",
	    templateUrl: 'app/modules/incidence/views/StudentDetentionTransaction_geis.html',
	    controller: 'StudentDetentionTransaction_geisCont'
	})
     .state('main.STUDE1', {
         url: "/StudentDetentionTransactionA",
         templateUrl: 'app/modules/incidence/views/StudentDetentionTransactionA.html',
         controller: 'StudentDetentionTransactionA'
     })

    .state('main.DSR001', {
        url: "/DetentionSummaryReport",
        templateUrl: 'app/modules/incidence/views/DetentionSummaryReport.html',
        controller: 'DetentionSummaryReportCont'
    })

    .state('main.ThrSho', {
        url: "/DetentionThreshold",
        templateUrl: 'app/modules/Student/views/DetentionThreshold.html',
        controller: 'DetentionThresholdCont'
    })

    .state('main.DetDes', {
        url: "/DetentionDesc",
        templateUrl: 'app/modules/Student/views/DetentionDesc.html',
        controller: 'DetentionDescCont'
    })

    .state('main.OlAsd', {
        url: "/Admission_olasd",
        templateUrl: 'app/modules/Student/views/Admission_asdOL.html',
        controller: 'Admission_asdOLCont',
        params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
    })

             .state('main.OlAhg', {
                 url: "/Admission_olahg",
                 templateUrl: 'app/modules/Student/views/Admission_ahgsOL.html',
                 controller: 'Admission_ahgsOLCont',
                 params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
             })


               .state('main.OlSvc', {
                   url: "/Admission_olsvc",
                   templateUrl: 'app/modules/Student/views/Admission_SvccOL.html',
                   controller: 'Admission_SvccOLCont',
                   params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
               })


                .state('main.OlChrist', {
                    url: "/Admission_Christ",
                    templateUrl: 'app/modules/Student/views/Admission_ChristOL.html',
                    controller: 'Admission_ChristOLCont',
                    params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
                })

              .state('main.Oldvps', {
                  url: "/Admission_oldvps",
                  templateUrl: 'app/modules/Student/views/Admission_dvpsOL.html',
                  controller: 'Admission_dvpsOLCont',
                  params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
              })


             .state('main.Oldmc', {
                 url: "/Admission_oldmc",
                 templateUrl: 'app/modules/Student/views/AdmissionDmcOL.html',
                 controller: 'AdmissionDmcOLCont',
                 params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
             })

    .state('main.OlSiso', {
        url: "/Admission_olsiso",
        templateUrl: 'app/modules/Student/views/Admission_sisoOL.html',
        controller: 'Admission_sisoOLCont',
        params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
    })

    .state('main.Olgeis', {
        url: "/Admission_olgeis",
        templateUrl: 'app/modules/Student/views/Admission_GeisOL.html',
        controller: 'Admission_asdOLCont',
        params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
    })

    .state('main.AssSch', {
        url: "/StudentAssesmentSchedule",
        templateUrl: 'app/modules/Student/views/StudentAssesmentSchedule.html',
        controller: 'StudentAssesmentScheduleCont'
    })

             .state('main.EmpSch', {
                 url: "/EmployeeAssesmentSchedule",
                 templateUrl: 'app/modules/Student/views/EmployeeAssesmentSchedule.html',
                 controller: 'EmployeeAssesmentScheduleCont'
             })
    .state('main.TeaEnt', {
        url: "/TeacherAssessmentEntry",
        templateUrl: 'app/modules/Student/views/TeacherAssessmentEntry.html',
        controller: 'TeacherAssessmentEntryCont'
    })


            .state('main.EmpEnt', {
                url: "/EmployeeAssessmentEntry",
                templateUrl: 'app/modules/Student/views/EmployeeAssessmentEntry.html',
                controller: 'EmployeeAssessmentEntryCont'
            })
             .state('main.Dfr001', {
                 url: "/FinalDailyReport",
                 templateUrl: 'app/modules/Student/views/FinalDailyReport.html',
                 controller: 'FinalDailyReportCont'
             })


              .state('main.Dfrn01', {
                  url: "/FinalDailyReport_new",
                  templateUrl: 'app/modules/Student/views/FinalDailyReport_new.html',
                  controller: 'FinalDailyReport_newCont'
              })


    .state('main.empsft', {
        url: "/EmployeeShift_NISS",
        templateUrl: 'app/modules/HRMS/views/EmployeeShift_NISS.html',
        controller: 'EmployeeShift_NISSCont'
    })
    .state('main.ForDet', {
        url: "/FormulaDetails",
        templateUrl: 'app/modules/HRMS/views/FormulaDetails.html',
        controller: 'FormulaDetailsCont'
    })

    .state('main.ComSok', {
        url: "/CommunicationToDefaulters_Sok",
        templateUrl: 'app/modules/Collaboration/views/CommunictaionToDefaulters.html',
        controller: 'CommunicationToDefaulters_SokController'
    })

            .state('main.CQRASD', {
                url: "/QueryReport",
                templateUrl: 'app/modules/Collaboration/views/QueryReport.html',
                controller: 'QueryReportCont'
            })

     .state('main.AdmApp', {
         url: "/AdmissionWaitingForApproval_ASD",
         templateUrl: 'app/modules/Student/views/AdmissionwaitingforApproval_ASD.html',
         controller: 'AdmissionwaitingforApproval_ASDCont',
         params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
     })

            .state('main.ILDV01', {
                url: "/InventoryLoanDetailViewReport",
                templateUrl: 'app/modules/Inventory/views/InventoryLoanDetailView_Rpt.html',
                controller: 'InventoryLoanDetailView_RptCont'
            })

      .state('main.DasSis', {
          url: "/AdmissionDashboard_sisqatar",
          templateUrl: 'app/modules/Student/views/AdmissionDashboard_sisqatar.html',
          controller: 'AdmissionDashboard_sisqatarCont',
          params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
      })

     .state('main.PsGeis', {
         url: "/ProspectAdmissionShort_Geis",
         templateUrl: 'app/modules/Student/views/ProspectAdmissionShort_Geis.html',
         controller: 'ProspectAdmissionShort_GeisCont'
     })

     .state('main.OLSvcc', {
         url: "/OLAdmissionReg_svcc",
         templateUrl: 'app/modules/Student/views/OLAdmissionReg_Svcc.html',
         controller: 'OLAdmissionReg_SvccCont'
         //params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
     })
            .state('main.Asiso', {
                url: "/AdmissionReg_siso",
                templateUrl: 'app/modules/Student/views/AdmissionReg_sis.html',
                controller: 'AdmissionReg_sisoCont'
                //params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
            })

     .state('main.AdGeis', {
         url: "/Admission_Geis",
         templateUrl: 'app/modules/Student/views/Admission_Geis.html',
         controller: 'Admission_GeisCont',
         params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
     })

     .state('main.DetHis', {
         url: "/Detention_history_report_app",
         templateUrl: 'app/modules/incidence/views/DetentionHistory.html',
         controller: 'DetentionHistoryRptCont'
     })

    .state('main.tecti2', {
        url: "/TeacherTimeTableByUser",
        templateUrl: 'app/modules/Scheduling/views/TeacherTimeTableByUser.html',
        controller: 'TeacherTimeTableByuserCont'
    })

    .state('main.empfin', {
        url: "/EmployeeFinalSettlement",
        templateUrl: 'app/modules/HRMS/views/EmployeeFinalSettlement.html',
        controller: 'EmployeeFinalSettlementCont'
    })

     .state('main.pubeml', {
         url: "/PublishEmailPayroll",
         templateUrl: 'app/modules/HRMS/views/PublishEmailPayroll.html',
         controller: 'PublishEmailPayrollCont'
     })

    .state('main.SALUSR', {
        url: "/AllPayslipByUser",
        templateUrl: 'app/modules/HRMS/views/AllPayslipByUser.html',
        controller: 'AllPayslipByUserCont'
    })

    .state('main.CSSR01', {
        url: "/CumulativeSubledgerSummaryReport",
        templateUrl: 'app/modules/Finance/views/CumulativeSubledgerSummaryReport.html',
        controller: 'CumulativeSubledgerSummaryReportCont'
    })

     .state('main.anarpt', {
         url: "/analysisReport",
         templateUrl: 'app/modules/Student/views/StudentDashboardAnalysisReport.html',
         controller: 'StudDashAnalysisReportCont'
     })

     .state('main.Dimprt', {
         url: "/DataImport",
         templateUrl: 'app/modules/SchoolSetup/views/DataImport.html',
         controller: 'DataImportCont'
     })

    .state('main.stgeis', {
        url: "/StudentDatabase_geis",
        templateUrl: 'app/modules/Student/views/StudentDatabase_geis.html',
        controller: 'StudentDatabase_geisCont'
    })

     .state('main.stDVHS', {
         url: "/StudentDatabase_dvhs",
         templateUrl: 'app/modules/Student/views/StudentDatabase_dvhs.html',
         controller: 'StudentDatabase_dvhsCont'
     })

     .state('main.AdDvhs', {
         url: "/Admission_dvhs",
         templateUrl: 'app/modules/Student/views/Admission_dvhs.html',
         controller: 'Admission_dvhsCont',
         params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
     })

     .state('main.ShDvhs', {
         url: "/AdmissionShort_dvhs",
         templateUrl: 'app/modules/Student/views/AdmissionShort_dvhs.html',
         controller: 'AdmissionShort_dvhsCont'
     })

     .state('main.lpopro', {
         url: "/LPOProcess",
         templateUrl: 'app/modules/Inventory/views/LPOProcess.html',
         controller: 'LPOProcessCont'
     })

      .state('main.salvat', {
          url: "/SalesDocVAT",
          templateUrl: 'app/modules/Inventory/views/SalesDocument_VAT.html',
          controller: 'SalesDocumentVATCont'
      })

      .state('main.AsAHGS', {
          url: "/StudentAssignRemark",
          templateUrl: 'app/modules/Student/views/StudentAssignRemark_ahgs.html',
          controller: 'StudentAssignRemark_ahgsCont'
      })

     .state('main.HLTREP', {
         url: "/HealthReport",
         templateUrl: 'app/modules/Health/views/HealthReport.html',
         controller: 'HealthReportCont'
     })

    .state('main.empdvs', {
        url: "/CreateEditEmployee_dvps",
        templateUrl: 'app/modules/HRMS/views/CreateEditEmployee_dvps.html',
        controller: 'CreateEditEmployee_dvpsCont'
    })

      .state('main.dpsmst', {
          url: "/EmployeeMaster_dvps",
          templateUrl: 'app/modules/HRMS/views/EmployeeMaster_dvps.html',
          controller: 'EmployeeMaster_dvpsCont'
      })

     .state('main.MARENT', {
         url: "/MarkEntryChangeReport",
         templateUrl: 'app/modules/Gradebook/views/MarkEntryChangeReport.html',
         controller: 'MarkEntryChangeReportCont'
     })

     .state('main.Inv03S', {
         url: "/SalesDocSISO",
         templateUrl: 'app/modules/Inventory/views/SalesDocument_SISO.html',
         controller: 'SalesDocument_SISOCont'
     })


     .state('main.SENLST', {
         url: "/SENApproveList",
         templateUrl: 'app/modules/Student/views/SENApproveList.html',
         controller: 'SENApproveListCont'
     })

     .state('main.SimUSN', {
         url: "/UpdateStudentSectionNew",
         templateUrl: 'app/modules/Student/views/UpdateStudentSectionNew.html',
         controller: 'UpdateStudentSectionNewController'
     })

       .state('main.BILETR', {
           url: "/BillEntry",
           templateUrl: 'app/modules/Finance/views/BillEntry.html',
           controller: 'BillEntryCont'
       })

           .state('main.COMNEW', {
               url: "/CommentsDetailEntry",
               templateUrl: 'app/modules/Gradebook/views/CommentsDetailEntry.html',
               controller: 'CommentsDetailEntryCont'
           })

         .state('main.ATTRPT', {
             url: "/AttendanceAbsentReport",
             templateUrl: 'app/modules/Attendance/views/AttendanceAbsentReport.html',
             controller: 'AttendanceAbsentReportCont'
         })

         .state('main.salper', { ///salper
             url: "/SalesDocPearl",
             templateUrl: 'app/modules/Inventory/views/SalesDocument_Pearl.html',
             controller: 'SalesDocumentPearlCont'
         })

        .state('main.afedem', {
            url: "/AirFareEmpDetails",
            templateUrl: 'app/modules/HRMS/views/AirFareEmployeeDetails.html',
            controller: 'AirFareEmployeeDetailsCont'
        })

       .state('main.apssht', {
           url: "/CreateEditEMPAPS",
           templateUrl: 'app/modules/HRMS/views/CreateEditEmployee_APS.html',
           controller: 'CreateEditEmployee_APSCont'
       })
		.state('main.DSD001', {
		    url: "/DailyStatusDashboard",
		    templateUrl: 'app/modules/Attendance/views/DailyStatusDashboard.html',
		    controller: 'DailyStatusDashboardCont'
		    //params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
		})

         .state('main.apsmst', {
             url: "/EmployeeMasterAPS",
             templateUrl: 'app/modules/HRMS/views/EmployeeMaster_APS.html',
             controller: 'EmployeeMaster_APSCont'
         })

	.state('main.empoff', {
	    url: "/EmployeeOfferLetter",
	    templateUrl: 'app/modules/HRMS/views/EmployeeOfferLetter.html',
	    controller: 'EmployeeOfferLetterCont'
	})

    .state('main.empcon', {
        url: "/EmployeeConfirm",
        templateUrl: 'app/modules/HRMS/views/EmployeeConfirm.html',
        controller: 'EmployeeConfirmCont'
    })

     .state('main.CIRPAR', {
         url: "/CircularStatusByParent",
         templateUrl: 'app/modules/Collaboration/views/CircularStatusByParent.html',
         controller: 'CircularStatusByParentCont'
     })

         .state('main.CIRUSR', {
             url: "/CircularUserStatus",
             templateUrl: 'app/modules/Collaboration/views/CircularUserStatus.html',
             controller: 'CircularUserStatusCont'
         })

             .state('main.COMRPT', {
                 url: "/CommunicationReportNew",
                 templateUrl: 'app/modules/Collaboration/views/CommunicationReportNew.html',
                 controller: 'CommunicationReportNewCont'
             })

           .state('main.EMLSTS', {
               url: "/EmailStatusReport",
               templateUrl: 'app/modules/Collaboration/views/EmailStatusReport.html',
               controller: 'EmailStatusReportCont'
           })

          .state('main.NEWSRP', {
              url: "/NewsReportByUser",
              templateUrl: 'app/modules/Collaboration/views/NewsReportByUser.html',
              controller: 'NewsReportbyUserCont'
          })

          .state('main.STDHOS', {
              url: "/StudentHouseList_VariousAge",
              templateUrl: 'app/modules/Student/views/StudentHouseList_VariousAge.html',
              controller: 'StudHouseList_VariousAgeCont'
          })

         .state('main.ENRTRN', {
             url: "/EnrollStudentsInTransport",
             templateUrl: 'app/modules/Fleet/views/EnrolledStudentsInTransport.html',
             controller: 'EnrolledStudentInTransportCont'
         })

            .state('main.FMLANA', {
                url: "/FamilyAnalysisReport",
                templateUrl: 'app/modules/Fee/views/FamilyAnalysisReport.html',
                controller: 'FamilyAnalysisReportCont'
            })

            .state('main.FMLANB', {
                url: "/FamilyAnalysisReport_new",
                templateUrl: 'app/modules/Fee/views/FamilyAnalysisReport_new.html',
                controller: 'FamilyAnalysisReport_NewCont'
            })

         .state('main.TRSFEE', {
             url: "/TransportFeePaymentDetails",
             templateUrl: 'app/modules/Fleet/views/TransportFeePaymentDetails.html',
             controller: 'TransportFeePaymentDetailsCont'
         })

          .state('main.NXTREG', {
              url: "/NotRegisterNextYr",
              templateUrl: 'app/modules/Student/views/NotRegisterNextYr.html',
              controller: 'NotRegisterNextYrCont'
          })

        .state('main.SBVQ01', {
            url: "/StudentsbyVehicleReport",
            templateUrl: 'app/modules/Fleet/views/StudentsbyVehicleReport.html',
            controller: 'StudentsbyVehicleReportCont'
        })

        .state('main.SBVQ20', {
            url: "/VehicleByRoutes",
            templateUrl: 'app/modules/Fleet/views/VehicleByRoutes.html',
            controller: 'VehicleByRoutesCont'
        })

        .state('main.FINRZ1', {
            url: "/FinanceAudit",
            templateUrl: 'app/modules/Finance/views/FinanceAudit.html',
            controller: 'FinanceAuditCont'
        })

	.state('main.ATRC01', {
	    url: "/AlertTransactionReport",
	    templateUrl: 'app/modules/Common/views/AlertTransactionReport.html',
	    controller: 'AlertTransactionReportCont'
	})

		.state('main.SLRQ01', {
		    url: "/SchoolLeaversReport",
		    templateUrl: 'app/modules/Student/views/SchoolLeaversReport.html',
		    controller: 'SchoolLeaversReportCont'
		})


		.state('main.SPDPQ1', {
		    url: "/StudentPickupDropPointReport",
		    templateUrl: 'app/modules/Fleet/views/StudentPickupDropPointReport.html',
		    controller: 'StudentPickupDropPointReportCont'
		})

        .state('main.TSRQ01', {
            url: "/TransportServiceReport",
            templateUrl: 'app/modules/Fleet/views/TransportServiceReport.html',
            controller: 'TransportServiceReportCont'
        })
        .state('main.SFSQ01', {
            url: "/StudentFamilyStatementReport",
            templateUrl: 'app/modules/Fee/views/StudentFamilyStatementReport.html',
            controller: 'StudentFamilyStatementReportCont'
        })

		.state('main.SFSQ02', {
		    url: "/StudentFamilyStatementReport_dc",
		    templateUrl: 'app/modules/Fee/views/StudentFamilyStatementReport_dc.html',
		    controller: 'StudentFamilyStatementReportCont_dc'
		})

        .state('main.HLTRQ1', {
            url: "/StudentHealthDetailsReport",
            templateUrl: 'app/modules/Health/views/StudentHealthDetailsReport.html',
            controller: 'StudentHealthDetailsReportCont'
        })

        .state('main.FEERQ2', {
            url: "/ClassFeeStructureReport",
            templateUrl: 'app/modules/Fee/views/ClassFeeStructureReport.html',
            controller: 'ClassFeeStructureReportCont'
        })

        .state('main.FEERQ1', {
            url: "/ConcessionDetailsReport",
            templateUrl: 'app/modules/Fee/views/ConcessionDetailsReport.html',
            controller: 'ConcessionDetailsReportCont'
        })

               .state('main.ATFNEW', {
                   url: "/ApproveTransportFeeASIS",
                   templateUrl: 'app/modules/Fleet/views/ApproveTransportFee_ASIS.html',
                   controller: 'ApproveTransportFeeASISCont'
               })





            .state('main.RAQR01', {
                url: "/ReceivableAgingQueryReport",
                templateUrl: 'app/modules/Finance/views/ReceivableAgingQueryReport.html',
                controller: 'ReceivableAgingQueryReportCont'
            })

        .state('main.GBRQ01', {
            url: "/AnalysisAssessmentReport",
            templateUrl: 'app/modules/Gradebook/views/AnalysisAssessmentReport.html',
            controller: 'AnalysisAssessmentReportCont'
        })

        //report
        //supriya
        .state('main.CLR001', {
            url: "/ClassListreport",
            templateUrl: 'app/modules/Student/views/ClassListReportByTeacher.html',
            controller: 'ClassListReportByTeacherCont'
        })

            .state('main.NACQ01', {
                url: "/NewAdmissionCancellationStdList",
                templateUrl: 'app/modules/Student/views/NewAdmissionCancellationStdList.html',
                controller: 'NewAdmissionCancellationStdListCont'
            })
            .state('main.SLGQ01', {
                url: "/StudentListByGrade",
                templateUrl: 'app/modules/Student/views/StudenListByGrade.html',
                controller: 'StudentListByGradeCont'
            })
            .state('main.STLQ02', {
                url: "/SubjectTeacherList",
                templateUrl: 'app/modules/Student/views/SubjectTeacherListReport.html',
                controller: 'SubjectTeacherListReportCont'
            })
             .state('main.OALQ03', {
                 url: "/OnlineAdmissionStdList",
                 templateUrl: 'app/modules/Student/views/OnlineAdmissionStdList.html',
                 controller: 'OnlineAdmissionStdListCont'
             })
            .state('main.ELSQ04', {
                url: "/EmpLeaveSum",
                templateUrl: 'app/modules/HRMS/views/EmpLeaveSum.html',
                controller: 'EmpLeaveSumCont'
            })
            .state('main.EASQ05', {
                url: "/EmpAttSum",
                templateUrl: 'app/modules/HRMS/views/EmpAttendanceSumRep.html',
                controller: 'EmpAttendanceSumRepCont'
            })

            .state('main.EDRQ06', {
                url: "/EmpDocReport",
                templateUrl: 'app/modules/HRMS/views/EmpDocTypeReport.html',
                controller: 'EmpDocTypeReportCont'
            })
                 .state('main.CRDQ07', {
                     url: "/CirRepDate",
                     templateUrl: 'app/modules/Collaboration/views/CircularReportByDate.html',
                     controller: 'CircularReportByDateCont'
                 })

                .state('main.CRUQ08', {
                    url: "/CirRepUser",
                    templateUrl: 'app/modules/Collaboration/views/CircularReportByUser.html',
                    controller: 'CircularReportByUserCont'
                })

                .state('main.IHRQ09', {
                    url: "/ItemHistory",
                    templateUrl: 'app/modules/Library/views/ItemHistoryReport.html',
                    controller: 'ItemHistoryReportCont'
                })

                 .state('main.LTDQ10', {
                     url: "/LibTransaction",
                     templateUrl: 'app/modules/Library/views/LibraryTransactionDetailReport.html',
                     controller: 'LibraryTransactionDetailReportCont'
                 })
                  .state('main.LMUQ11', {
                      url: "/LibMembership",
                      templateUrl: 'app/modules/Library/views/LibMembershipByUserReport.html',
                      controller: 'LibMembershipByUserReportCont'
                  })


               .state('main.UHRQ12', {
                   url: "/UserHistory",
                   templateUrl: 'app/modules/Library/views/UserHistoryReport.html',
                   controller: 'UserHistoryReportCont'
               })

              .state('main.SDRQ13', {
                  url: "/StudentDetails",
                  templateUrl: 'app/modules/Student/views/StudentDetailreport.html',
                  controller: 'StudentDetailreportCont'
              })
                .state('main.CSRQ14', {
                    url: "/CancelledDetails",
                    templateUrl: 'app/modules/Student/views/CancelledListReport.html',
                    controller: 'CancelledListReportCont'
                })
                  .state('main.CASQ15', {
                      url: "/CancelledAdmissionStatus",
                      templateUrl: 'app/modules/Student/views/CancelledAdmissionStatusReport.html',
                      controller: 'CancelledAdmissionStatusReportCont'
                  })
             .state('main.PNAQ16', {
                 url: "/ParentsNotAssigned",
                 templateUrl: 'app/modules/Student/views/ParentsNotAssignedToStudentsReport.html',
                 controller: 'ParentsNotAssignedToStudentsReportCont'
             })
             .state('main.STEQ17', {
                 url: "/StudentListByEsis",
                 templateUrl: 'app/modules/Student/views/StudentListByESISNoReport.html',
                 controller: 'StudentListByESISNoReportCont'
             })
             .state('main.SHRQ18', {
                 url: "/StudentHealthReport",
                 templateUrl: 'app/modules/Health/views/StudentHealthReport.html',
                 controller: 'StudentHealthReportCont'
             })
             .state('main.SDRQ19', {
                 url: "/SiblingReport",
                 templateUrl: 'app/modules/Student/views/SiblingReport.html',
                 controller: 'SiblingReportCont'
             })
             .state('main.SHLQ20', {
                 url: "/StudentHouseListReport",
                 templateUrl: 'app/modules/Student/views/StudentHouseListReport.html',
                 controller: 'StudentHouseListReportCont'
             })
            .state('main.ESTQ21', {
                url: "/EnrolledStudentsTransport",
                templateUrl: 'app/modules/Fleet/views/EnrolledStudentsTransport.html',
                controller: 'EnrolledStudentsTransportCont'

            })
                 .state('main.DDRQ23', {
                     url: "/DriverDetail",
                     templateUrl: 'app/modules/Fleet/views/DriverDetailReport.html',
                     controller: 'DriverDetailReportCont'
                 })
                   .state('main.ARTQ24', {
                       url: "/AttReportForStudentsTransport",
                       templateUrl: 'app/modules/Fleet/views/AttReportForStudentsTransport.html',
                       controller: 'AttReportForStudentsTransportCont'
                   })
                   .state('main.SBBQ25', {
                       url: "/StudentsByBus",
                       templateUrl: 'app/modules/Fleet/views/StudentsByBusReport.html',
                       controller: 'StudentsByBusReportCont'
                   })

            .state('main.FTLQ26', {
                url: "/FreeTeacherList",
                templateUrl: 'app/modules/Scheduling/views/FreeTeacherListReport.html',
                controller: 'FreeTeacherListReportCont'
            })
            .state('main.SLRQ27', {
                url: "/SupervisorList",
                templateUrl: 'app/modules/Scheduling/views/SupervisorListReport.html',
                controller: 'SupervisorListReportCont'
            })
             .state('main.LCRQ28', {
                 url: "/LectureConflict",
                 templateUrl: 'app/modules/Scheduling/views/LectureTimeConflictReport.html',
                 controller: 'LectureTimeConflictReportCont'
             })

             .state('main.RARQ29', {
                 url: "/RoleApplication",
                 templateUrl: 'app/modules/Security/views/RoleApplicationReport.html',
                 controller: 'RoleApplicationReportCont'

             })

            .state('main.RASQ30', {
                url: "/RoleAssigned",
                templateUrl: 'app/modules/Security/views/RoleAssignmentReport.html',
                controller: 'RoleAssignmentReportCont'
            })

             .state('main.USRQ31', {
                 url: "/UserStatus",
                 templateUrl: 'app/modules/Security/views/UserStatusReport.html',
                 controller: 'UserStatusReportCont'
             })

              .state('main.ULDQ32', {
                  url: "/UserLogin",
                  templateUrl: 'app/modules/Security/views/UserLoginDetails.html',
                  controller: 'UserLoginDetailsCont'
              })

              .state('main.SDRQ33', {
                  url: "/StockDetails",
                  templateUrl: 'app/modules/Inventory/views/StockDetailsReport.html',
                  controller: 'StockDetailsReportCont'
              })
              .state('main.RDSQ34', {
                  url: "/RequestDetailsSum",
                  templateUrl: 'app/modules/Inventory/views/RequestDetailSummary.html',
                  controller: 'RequestDetailSummaryCont'
              })

        //ashwariya

         .state('main.AFR01', {
             url: "/AirFareReport",
             templateUrl: 'app/modules/Common/views/AirFareReport.html',
             controller: 'AirFareReportCont'
         })

        .state('main.CTL01', {
            url: "/ClassTeacherList",
            templateUrl: 'app/modules/Student/views/ClassTeacherListRpt.html',
            controller: 'ClassTeacherListRptCont'
        })

         .state('main.SLBGN1', {
             url: "/StudentListByGradeNewReport",
             templateUrl: 'app/modules/Student/views/StudentListByGradeNewRpt.html',
             controller: 'StudentListByGradeNewRptCont'
         })

        .state('main.ALBGS1', {
            url: "/AdmissionListByGradeSection",
            templateUrl: 'app/modules/Student/views/AdmissionListByGradeSectionRpt.html',
            controller: 'AdmissionListByGradeSectionRptCont'
        })



        .state('main.SCR01', {
            url: "/StudentContactReport",
            templateUrl: 'app/modules/Student/views/StudentContactRpt.html',
            controller: 'StudentContactRptCont'
        })

         .state('main.ENMT01', {
             url: "/ExamNumberMappingByteacher",
             templateUrl: 'app/modules/Student/views/ExamNumMappingByTeacherRpt.html',
             controller: 'ExamNumMappingByTeacherRptCont'
         })


        .state('main.SLBT01', {
            url: "/StudentListByTeacher",
            templateUrl: 'app/modules/Student/views/StudentListByTeacher.html',
            controller: 'StudentListByTeacherRptCont'
        })

         .state('main.CASQ01', {
             url: "/CancelAdmissionSummary",
             templateUrl: 'app/modules/Student/views/canceladmissionsummary.html',
             controller: 'canceladmissionsummarycont'
         })

         .state('main.PSL001', {
             url: "/promote_student_list",
             templateUrl: 'app/modules/Student/views/promote_student_list.html',
             controller: 'promote_student_listCont'
         })

         .state('main.RRSR01', {
             url: "/Re-RegisterdStudentReport",
             templateUrl: 'app/modules/Student/views/Re-RegisterdStudRpt.html',
             controller: 'Re-RegisterdStudRptCont'
         })


         .state('main.SID01', {
             url: "/StudentImmigrationDetails",
             templateUrl: 'app/modules/Student/views/StudentImmigrationDetailsRpt.html',
             controller: 'StudentImmigrationDetailsRptCont'
         })

        .state('main.SLBE01', {
            url: "/StudentListByEth",
            templateUrl: 'app/modules/Student/views/StudentListByEthRpt.html',
            controller: 'StudentListByEthRptCont'
        })

         .state('main.awl01', {
             url: "/AgeWiseList",
             templateUrl: 'app/modules/Student/views/agewiselist.html',
             controller: 'agewiselistCont'
         })

         .state('main.EPR01', {
             url: "/EmployeePunchReport",
             templateUrl: 'app/modules/HRMS/views/EmployeePunchReport.html',
             controller: 'EmployeePunchReportCont'
         })

            .state('main.RWUL01', {
                url: "/RoleWiseUserListReport",
                templateUrl: 'app/modules/HRMS/views/RoleWiseUserListRpt.html',
                controller: 'RoleWiseUserListRptCont'
            })

         .state('main.DSDG01', {
             url: "/DistributionOffStafByDesGen",
             templateUrl: 'app/modules/HRMS/views/DistributionByDesGenRpt.html',
             controller: 'DistributionByDesGenRptCont'
         })

          .state('main.DND01', {
              url: "/DistributionByNationalityDesignation",
              templateUrl: 'app/modules/HRMS/views/DistributionNationDesignationRpt.html',
              controller: 'DistributionNationDesignationRptCont'
          })

         .state('main.SSR01', {
             url: "/StaffStrengthReport",
             templateUrl: 'app/modules/HRMS/views/StaffStrengthRpt.html',
             controller: 'StaffStrengthRptCont'
         })


         .state('main.EL01', {
             url: "/EmpListRpt",
             templateUrl: 'app/modules/HRMS/views/EmpListRpt.html',
             controller: 'EmpListRptCont'
         })

         .state('main.ID01', {
             url: "/ImmigrationDocument",
             templateUrl: 'app/modules/HRMS/views/ImmigrationDocRpt.html',
             controller: 'ImmigrationDocRptCont'
         })

         .state('main.PRE01', {
             url: "/ProbationReportForEmployee",
             templateUrl: 'app/modules/HRMS/views/ProbationReportForEmployeeRpt.html',
             controller: 'ProbationReportForEmployeeRptCont'
         })

        .state('main.ABS01', {
            url: "/AvailableBookStock",
            templateUrl: 'app/modules/Library/views/AvailableBookStockRpt.html',
            controller: 'AvailableBookStockRptCont'
        })

        .state('main.LI01', {
            url: "/LibraryItem",
            templateUrl: 'app/modules/Library/views/LibraryItemRpt.html',
            controller: 'LibraryItemRptCont'
        })

          .state('main.LMBG01', {
              url: "/LibraryMembershipByGrade",
              templateUrl: 'app/modules/Library/views/LibMembershipByGradeRpt.html',
              controller: 'LibMembershipByGradeRptCont'
          })

         .state('main.NIB01', {
             url: "/NoIssueBook",
             templateUrl: 'app/modules/Library/views/NoIssueBookListByStudentRpt.html',
             controller: 'NoIssueBookListByStudentCont'
         })

         .state('main.SDL01', {
             url: "/StudentDefaulterList",
             templateUrl: 'app/modules/Library/views/StudentDefaulterListRpt.html',
             controller: 'StudentDefaulterListRptCont'
         })

         .state('main.SBR01', {
             url: "/StopByRoute",
             templateUrl: 'app/modules/Fleet/views/StopByRouteRpt.html',
             controller: 'StopByRouteRptCont'
         })

         .state('main.CA01', {
             url: "/CommonAlert",
             templateUrl: 'app/modules/Common/views/AlertRpt.html',
             controller: 'AlertRptCont'
         })

        .state('main.IDR01', {
            url: "/IncrementDueReport",
            templateUrl: 'app/modules/HRMS/views/IncrementDueRpt.html',
            controller: 'IncrementDueRptCont'
        })

        .state('main.SIR01', {
            url: "/SalaryIncrementRpt",
            templateUrl: 'app/modules/Common/views/SalaryIncRpt.html',
            controller: 'SalaryIncRptCont'
        })

         .state('main.LTR01', {
             url: "/LibraryTransactionReport",
             templateUrl: 'app/modules/Library/views/LibTransactionRpt.html',
             controller: 'LibTransactionRptCont'
         })


        .state('main.ETW01', {
            url: "/ExceedingTeacherWorkloadReport",
            templateUrl: 'app/modules/Scheduling/views/ExceedingTeacherWorkloadRpt.html',
            controller: 'ExceedingTeacherWorkloadRptCont'
        })


         .state('main.IRL01', {
             url: "/ItemReorderListReport",
             templateUrl: 'app/modules/Inventory/views/ItemReorderListRpt.html',
             controller: 'ItemReorderListRptCont'
         })

         .state('main.ISR01', {
             url: "/ItemStockReport",
             templateUrl: 'app/modules/Inventory/views/ItemStockReport.html',
             controller: 'ItemStockReportCont'
         })


         .state('main.AssSchOba', {
             url: "/StudentAssesmentScheduleOba",
             templateUrl: 'app/modules/Student/views/StudentAssesmentScheduleOba.html',
             controller: 'StudentAssesmentScheduleObaCont'
         })


        .state('main.CSDL01', {
            url: "/CancelledSaleDocumentListReport",
            templateUrl: 'app/modules/Inventory/views/CancelledSaleDocumentListRpt.html',
            controller: 'CancelledSaleDocumentListRptCont'
        })

         .state('main.SRBI03', {
             url: "/SalesReportByItemReport",
             templateUrl: 'app/modules/Inventory/views/SalesReportByItemRpt.html',
             controller: 'SalesReportByItemRptCont'
         })

        .state('main.CSS33', {
            url: "/CostingSheetSummaryReport",
            templateUrl: 'app/modules/Inventory/views/CostingSheetSummaryRpt.html',
            controller: 'CostingSheetSummaryRptCont'
        })

        .state('main.SDLQ35', {
            url: "/SalesDocumentListRpt",
            templateUrl: 'app/modules/Inventory/views/SalesDocumentListReport.html',
            controller: 'SalesDocumentListReportCont'
        })

            .state('main.AHRQ36', {
                url: "/HolidayReport",
                templateUrl: 'app/modules/Attendance/views/HolidayRept.html',
                controller: 'HolidayReptCont'
            })
             .state('main.AMRQ37', {
                 url: "/AttendanceNotMarked",
                 templateUrl: 'app/modules/Attendance/views/AttendanceNotMarked.html',
                 controller: 'AttendanceNotMarkedCont'

             })
               .state('main.VARQ38', {
                   url: "/VarianceAttendance",
                   templateUrl: 'app/modules/Attendance/views/VarianceAttendanceTeacherReport.html',
                   controller: 'VarianceAttendanceTeacherReportCont'
               })

        .state('main.SQR01', {
            url: "/StudentQuery",
            templateUrl: 'app/modules/Fee/views/StudentQuery.html',
            controller: 'StudentQueryCont'
        })

 	.state('main.loctrs', {
 	    url: "/ItemTransfer",
 	    templateUrl: 'app/modules/Inventory/views/ItemLocationTransfer.html',
 	    controller: 'ItemLocationTransferCont'
 	})


          .state('main.obaregNew', {
              url: "/AdmissionDashboard_Oba",
              templateUrl: 'app/modules/Student/views/AdmissionDashboard_oba.html',
              controller: 'AdmissionDashboard_obaCont'
          })

        .state('main.SSSQ01', {
            url: "/StudentSectionSubjectQuery",
            templateUrl: 'app/modules/Student/views/StudentSectionSubjectQuery.html',
            controller: 'StudentSectionSubjectQueryCont'
        })

         .state('main.ESTE01', {
             url: "/EmailSmsToEmployee",
             templateUrl: 'app/modules/Collaboration/views/EmailSmsToEmployee.html',
             controller: 'EmailSmsToEmployeeCont'
         })

          .state('main.FRR01', {
              url: "/FireRegisterReport",
              templateUrl: 'app/modules/Attendance/views/FireRegisterRpt.html',
              controller: 'FireRegisterRptCont'
          })

         .state('main.secscr', {
             url: "/SectionScreeningOBA",
             templateUrl: 'app/modules/Student/views/SectionScreening_OBA.html',
             controller: 'SectionScreeningOBACont'
         })

        .state('main.GBRQ02', {
            url: "/StudentMarksList",
            templateUrl: 'app/modules/Gradebook/views/StudentMarksList.html',
            controller: 'StudentMarksListCont'
        })

        .state('main.UFR01', {
            url: "/UserFormApplication",
            templateUrl: 'app/modules/Security/views/UserFormRpt.html',
            controller: 'UserFormRptCont'
        })

         .state('main.sim536Dmc', {
             url: "/SubjectwisetAttendanceDmc",
             templateUrl: 'app/modules/Attendance/views/SubjectwisetAttendanceDmc.html',
             controller: 'SubjectwisetAttendanceDmcCont'
         })

        .state('main.home02', {
            url: "/HomeroomTeacherCreationDMC",
            templateUrl: 'app/modules/Setup/views/HomeroomTeacherCreation_DMC.html',
            controller: 'HomeRoomTeacherCreation_DMCCont'
        })


            .state('main.Sim508New', {
                url: "/TeacherAssignmentUploadNew",
                templateUrl: 'app/modules/Assignment/views/TeacherAssignmentUploadNew.html',
                controller: 'TeacherAssignmentUploadNewCont'
            })

        .state('main.beldmc', {
            url: "/BellSectionSubjectTeacherDMC",
            templateUrl: 'app/modules/Scheduling/views/BellSectionSubjectTeacher_DMC.html',
            controller: 'BellSectionSubjectTeacher_DMCCont'
        })


        .state('main.learn', {
            url: "/Learning",
            templateUrl: 'app/modules/Gradebook/views/Learning.html',
            controller: 'LearningCont'
        })

        .state('main.SLFQ55', {
            url: "/StudentLeaveSlip",
            templateUrl: 'app/modules/Attendance/views/StudentLeaveForm.html',
            controller: 'StudentLeaveFormCont'
        })


         .state('main.Sim539Ahis', {
             url: "/AgendaViewAhis",
             templateUrl: 'app/modules/Student/views/AgendaViewAhis.html',
             controller: 'AgendaViewCont'
         })


          .state('main.Sim888', {
              url: "/AgendaCreationAhis",
              templateUrl: 'app/modules/Gradebook/views/AgendaCreationAhis.html',
              controller: 'AgendaCreationAhisCont'
          })

         .state('main.UserLa', {
             url: "/UserLang",
             templateUrl: 'app/modules/Common/views/UserLang.html',
             controller: 'UserLangController'
         })
       .state('main.Has1', {
           url: "/HouseAllocationSibling",
           templateUrl: 'app/modules/Student/views/HouseAllocationSibling.html',
           controller: 'HouseAllocationSiblingCont'
       })

        .state('main.SAQ01', {
            url: "/StudentMeritDemerit_parent",
            templateUrl: 'app/modules/Attendance/views/StudentMeritDemerit_parent.html',
            controller: 'StudentMeritDemerit_parentCont'
        })


         .state('main.Sim155CSD', {
             url: "/StudentAttendanceCsd",
             templateUrl: 'app/modules/Attendance/views/StudentAttendance.html',
             controller: 'StudentAttendanceCSDCont'
         })

        .state('main.Inav', {
            url: "/InactiveChat",
            templateUrl: 'app/modules/Common/views/InactiveChat.html',
            controller: 'InactiveChatCont'
        })

        .state('main.RepVou', {
            url: "/ReplicateVoucher",
            templateUrl: 'app/modules/Finance/views/ReplicateVoucher.html',
            controller: 'ReplicateVoucherCont'
        })

         .state('main.CRN01', {
             url: "/CommunicationReportNew_ABPS",
             templateUrl: 'app/modules/Collaboration/views/CommRptNew.html',
             controller: 'CommRptNewCont'
         })

        .state('main.RAR01N', {
            url: "/ReceivableAgingQueryReportCont_new",
            templateUrl: 'app/modules/Finance/views/ReceivableAgingQueryReport_new.html',
            controller: 'ReceivableAgingQueryReportCont_new'
        })

        .state('main.atdr01', {
            url: "/AttendanceAnalysisDashboardReport",
            templateUrl: 'app/modules/Student/views/AttendanceAnalysisDashboardReport.html',
            controller: 'AttendanceAnalysisDashboardReportCont'
        })

        .state('main.RejInv', {
            url: "/RejectInvoice",
            templateUrl: 'app/modules/Fee/views/RejectInvoice.html',
            controller: 'RejectInvoiceCont'
        })


        .state('main.CWD01', {
            url: "/ClassWiseDataNew",
            templateUrl: 'app/modules/Fee/views/ClassWiseFeeCollectionDataNew.html',
            controller: 'ClassWiseFeeCollectionDataNewCont'
        })

        .state('main.SA01', {
            url: "/StudentAttendanceSMFC",
            templateUrl: 'app/modules/Attendance/views/StudentAttendance_SMFC.html',
            controller: 'StudentAttendanceCont_SMFC'
        })

        .state('main.TAU01', {
            url: "/TeacherAssignmentUploadHomeRoom_ABQIS",
            templateUrl: 'app/modules/Assignment/views/TeacherAssignmentUploadHomeRoom_ABQIS.html',
            controller: 'TeacherAssignmentUploadHomeRoom_ABQISCont'
        })


	.state('main.atdrkg', {
	    url: "/EmployeeAttendanceRakkag",
	    templateUrl: 'app/modules/Attendance/views/EmployeeAttendanceRakkag.html',
	    controller: 'EmployeeAttendanceRakkagCont'
	})



        .state('main.Fn199', {
            url: "/PDCchequeStatusDetails",
            templateUrl: 'app/modules/Finance/views/PDCchequeStatusDetails.html',
            controller: 'PDCchequeStatusDetailsCont'
        })

        .state('main.SCre01', {
            url: "/SyllabusCreation_pearl",
            templateUrl: 'app/modules/SchoolSetup/views/SyllabusCreation_Pearl.html',
            controller: 'SyllabusCreationCont_Pearl'
        })

          .state('main.EDA355', {
              url: "/EmpAttendance",
              templateUrl: 'app/modules/HRMS/views/EmployeeAttendance.html',
              controller: 'EmployeeAttendanceCont'
          })

         .state('main.learnA', {
             url: "/LearningAsd",
             templateUrl: 'app/modules/Gradebook/views/LearningAsd.html',
             controller: 'LearningAsdCont'
         })


          .state('main.Subatt', {
              url: "/SubjectAttendanceMonthly",
              templateUrl: 'app/modules/Attendance/views/SubjectAttendanceMonthly.html',
              controller: 'SubjectAttendanceMonthlyCont'
          })

        .state('main.TO01', {
            url: "/TeacherObservation_report",
            templateUrl: 'app/modules/Student/views/TeacherObservation_rpt.html',
            controller: 'TeacherObservation_rpt_cont'
        })
                 .state('main.GRMT01', {
                     url: "/GraceMarksTranction",
                     templateUrl: 'app/modules/Gradebook/views/GraceMarksTransaction.html',
                     controller: 'GraceMarksTransactionCont'
                 })

        .state('main.Com017N', {
            url: "/CreateCircularsNew",
            templateUrl: 'app/modules/Collaboration/views/CreateCircularNew.html',
            controller: 'CreateCircularNewCont'
        })

        .state('main.GDA001', {
            url: "/GradebookDashboardAnalysisReport",
            templateUrl: 'app/modules/Gradebook/views/GradebookDashboardAnalysis.html',
            controller: 'GradebookDashboardAnalysisCont'
        })

        .state('main.GMA101', {
            url: "/GraccingMarks",
            templateUrl: 'app/modules/Gradebook/views/GraccingMarksActivity.html',
            controller: 'GraccingMarksActivityCont'
        })

        .state('main.traahg', {
            url: "/TransportRouteStudentAHGS",
            templateUrl: 'app/modules/Fleet/views/TransportRouteStud_AHGS.html',
            controller: 'TransportRouteStudAHGSCont'
        })

	 .state('main.tracan', {
	     url: "/TransportCancelUpdateAHGS",
	     templateUrl: 'app/modules/Fleet/views/TransportCancelUpdate_AHGS.html',
	     controller: 'TransportCancelUpdateAHGSCont'
	 })

	.state('main.crovat', {
	    url: "/CreateOrderVAT",
	    templateUrl: 'app/modules/Inventory/views/CrOrder_VAT.html',
	    controller: 'CrOrderVATCont'
	})

	.state('main.dorvat', {
	    url: "/DirectPOVAT",
	    templateUrl: 'app/modules/Inventory/views/DirectOrder_VAT.html',
	    controller: 'DirectOrder_VATCont'
	})

        .state('main.FeeWU', {
            url: "/FeesWriteup",
            templateUrl: 'app/modules/Fee/views/FeesWriteup.html',
            controller: 'FeesWriteupCont'
        })
         .state('main.demo', {
             url: "/AdmissionDashboardCsd",
             templateUrl: 'app/modules/Student/views/AdmissionDashboardCsd.html',
             controller: 'AdmissionDashboardCsdCont'
         })


            .state('main.dashL', {
                url: "/AdmissionDashboardLeams",
                templateUrl: 'app/modules/Student/views/AdmissionDashboardLeams.html',
                controller: 'AdmissionDashboardLeamsCont'
            })

              .state('main.Sim666', {
                  url: "/SectionSubjectTeacher_leam",
                  templateUrl: 'app/modules/Scheduling/views/SectionSubjectTeacher_leam.html',
                  controller: 'SectionSubjectTeacherCont_leam'
              })

             .state('main.dashL.OLLeams', {
                 url: "/AdmissionDashboardLeams1",
                 templateUrl: 'app/modules/Student/views/Admission_asdOL.html',
                 controller: 'Admission_asdOLCont',
                 params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
             })

        .state('main.COMNEWN', {
            url: "/CommentsDetailEntryN",
            templateUrl: 'app/modules/Gradebook/views/CommentsDetailEntryN.html',
            controller: 'CommentsDetailEntryNCont'
        })

		.state('main.COMNWS', {
		    url: "/CommentsDetailEntryStg",
		    templateUrl: 'app/modules/Gradebook/views/CommentsDetailEntryStag.html',
		    controller: 'CommentsDetailEntryStagCont'
		})

        .state('main.ser123', {
            url: "/ServicematerialStatus",
            templateUrl: 'app/modules/Inventory/views/ServiceandMaterialStatusReport.html',
            controller: 'ServiceandMaterialStatusReportCont'
        })

         .state('main.UVP01', {
             url: "/UpdatePostedVoucher",
             templateUrl: 'app/modules/Finance/views/UpdatePostedVoucher.html',
             controller: 'UpdatePostedVoucherCont'
         })

        .state('main.SSQR01', {
            url: "/SurveyStatisticQueryReportCont",
            templateUrl: 'app/modules/Setup/views/SurveyStatisticQueryReport.html',
            controller: 'SurveyStatisticQueryReportCont'
        })

	.state('main.qunpro', {
	    url: "/QuestionBankProduct",
	    templateUrl: 'app/modules/Student/views/QuestionBankProduct.html',
	    controller: 'QuestionBankProductCont'
	})
        .state('main.QUEBNK', {
            url: "/CreateQuestionBank",
            templateUrl: 'app/modules/Student/views/CreateQuestionBank.html',
            controller: 'CreateQuestionBankCont'
        })
        .state('main.QUEBNM', {
            url: "/QuestionBankQuestionMapping",
            templateUrl: 'app/modules/Student/views/QuestionBankQuestionMapping.html',
            controller: 'QuestionBankQuestionMappingCont'
        })
        .state('main.EER88', {
            url: "/EmployeeExperienceReport",
            templateUrl: 'app/modules/HRMS/views/EmpExpRpt.html',
            controller: 'EmpExpRptCont'
        })

         .state('main.PrCsd', {
             url: "/ProspectDashboard_csd",
             templateUrl: 'app/modules/Student/views/ProspectDashboard_csd.html',
             controller: 'ProspectDashboard_csdCont',
             params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
         })

	.state('main.apoasd', {
	    url: "/ApproveOrderVAT",
	    templateUrl: 'app/modules/Inventory/views/OrderApproveVAT.html',
	    controller: 'OrderApproveVATCont'
	})

	.state('main.tealc', {
	    url: "/TeacherAllocation_DMC",
	    templateUrl: 'app/modules/TimeTable/views/TimeTable_DMC.html',
	    controller: 'TimeTableDMCCont'
	})

         .state('main.SimatN', {
             url: "/StudAttendanceN",
             templateUrl: 'app/modules/Attendance/views/StudentAttendanceN.html',
             controller: 'StudentAttendanceASDCont'
         })

        .state('main.SiAtm', {
            url: "/StudAttendanceMonthlyN",
            templateUrl: 'app/modules/Attendance/views/StudentAttendanceMonthlyN.html',
            controller: 'StudentAttendanceMonthlyN'
        })


  .state('main.ReRegi', {
      url: "/ReregistrationtDashboard",
      templateUrl: 'app/modules/Student/views/ReregistrationtDashboard.html',
      controller: 'ReregistrationtDashboardCont',
      params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
  })
        .state('main.GRMT02', {
            url: "/UpdateGraceMarks",
            templateUrl: 'app/modules/Gradebook/views/UpdateGraceMarks.html',
            controller: 'UpdateGraceMarksCont'
        })
        .state('main.famabq', {
            url: "/FamilyAnalysisABQ",
            templateUrl: 'app/modules/Fee/views/FamilyAnalysisABQ.html',
            controller: 'FamilyAnalysisABQCont'
        })
        .state('main.STDMRK', {
            url: "/StudentMarksList",
            templateUrl: 'app/modules/Gradebook/views/StudentMarksList.html',
            controller: 'StudentMarksListCont'
        })
        .state('main.UV01', {
            url: "/UpdateVoucherNew",
            templateUrl: 'app/modules/Finance/views/UpdateVoucherNew.html',
            controller: 'UpdateVoucherNewCont'
        })
            .state('main.Emplem', {
                url: "/EmpDashboardNew",
                templateUrl: 'app/modules/HRMS/views/EmpDashboardLeams.html',
                controller: 'EmpDashboardCont'
            })




     .state('main.stsstg', {
         url: "/StudentDatabaseStag",
         templateUrl: 'app/modules/Student/views/StudentDatabase_Stag.html',
         controller: 'StudentDatabaseCont_Stag'
     })
         .state('main.UplBdg', {
             url: "/UploadBudget",
             templateUrl: 'app/modules/Finance/views/ImportVoucherData.html',
             controller: 'VoucherDataImportCont'
         })

	.state('main.lemmst', {
	    url: "/EmployeeMaster_lems",
	    templateUrl: 'app/modules/HRMS/views/EmployeeMaster_lems.html',
	    controller: 'EmployeeMaster_lemsCont'
	})

	.state('main.updemp', {
	    url: "/UpdateEmployeeProfile",
	    templateUrl: 'app/modules/HRMS/views/UpdateEmployeeProfile.html',
	    controller: 'UpdateEmployeeProfileCont'
	})

	.state('main.proemp', {
	    url: "/PROEmployee",
	    templateUrl: 'app/modules/HRMS/views/PROEmployee.html',
	    controller: 'PROEmployeeCont'
	})
        .state('main.Appli', {
            url: "/Applicant",
            templateUrl: 'app/modules/HRMS/views/Applicant.html',
            controller: 'ApplicantController'
        })

	.state('main.lemsht', {
	    url: "/CreateEditEmployeeLems",
	    templateUrl: 'app/modules/HRMS/views/CreateEditEmployeeLems.html',
	    controller: 'CreateEditEmployeeLemsCont'
	})
        .state('main.SFRef', {
            url: "/StudFeeRefundNew",
            templateUrl: 'app/modules/Fee/views/StudentFeeRefundNew.html',
            controller: 'StudentFeeRefundNewCont',
            params: { 'IP': {}, 'sel': {} }
        })
        .state('main.RefTra', {
            url: "/RefundTransaction",
            templateUrl: 'app/modules/Fee/views/RefundTransaction.html',
            controller: 'RefundTransactionCont',
            params: { 'IP': {}, 'sel': {} }
        })
        .state('main.FCBD01', {
            url: "/CompanyBankDetails",
            templateUrl: 'app/modules/Finance/views/CompanyBankDetails.html',
            controller: 'CompanyBankDetailsCont'
        })
        .state('main.DigDpt', {
            url: "/DesignationDept",
            templateUrl: 'app/modules/HRMS/views/DesignationWithDepartment.html',
            controller: 'DesignationWithDepartmentCont'
        })
         .state('main.camp01', {
             url: "/CampCreation",
             templateUrl: 'app/modules/Health/views/CampCreation.html',
             controller: 'CampCreationCont'
         })
         .state('main.TEPE01', {
             url: "/Tests_Packages",
             templateUrl: 'app/modules/Health/views/TestsPackages.html',
             controller: 'TestsPackagesCont'
         })
        .state('main.camtra', {
            url: "/campTransaction",
            templateUrl: 'app/modules/Health/views/campTransaction.html',
            controller: 'campTransactionCont'
        })
        .state('main.TEST01', {
            url: "/Tests",
            templateUrl: 'app/modules/Health/views/HealthTest.html',
            controller: 'TestsCont'
        })
            .state('main.ClaRAS', {
                url: "/AdmissionShort_clara",
                templateUrl: 'app/modules/Student/views/AdmissionShort_clara.html',
                controller: 'AdmissionShort_claraCont'
            })
          .state('main.CEclar', {
              url: "/CreateEditEmployee_clara",
              templateUrl: 'app/modules/HRMS/views/CreateEditEmployee_clara.html',
              controller: 'CreateEditEmployee_claraCont'
          })
         .state('main.CURcla', {
             url: "/Curriculum_clara",
             templateUrl: 'app/modules/SchoolSetup/views/Curriculum_clara.html',
             controller: 'Curriculum_claraCont'
         })
        .state('main.SimSAN', {
            url: "/SubjectWiseAbsentStudentsNotification",
            templateUrl: 'app/modules/Student/views/SubjectWiseAbsentStudentsNotification.html',
            controller: 'SubjectWiseAbsentStudentsNotificationCont'
        })
        .state('main.EXMREG', {
            url: "/ExamReRegistration",
            templateUrl: 'app/modules/Gradebook/views/ExamReRegistration.html',
            controller: 'ExamReRegistrationCont'
        })
         .state('main.SimIMT', {
             url: "/Coll_FeeDet_IMERT",
             templateUrl: 'app/modules/Fee/views/SFSNew_ABQIS_IMERT.html',
             controller: 'SFCControllerNew_ABQIS_IMERT',
             params: { 'sel': {} }
         })
        .state('main.ExcLog', {
            url: "/ExceptionLogs",
            templateUrl: 'app/modules/Setup/views/ExceptionLogs.html',
            controller: 'ExceptionLogsCont'
        })
          .state('main.clara0', {
              url: "/CreateCirculars_Clara",
              templateUrl: 'app/modules/Collaboration/views/CreateCircular_clara.html',
              controller: 'CreateCircularCont_clara'
          })

	.state('main.abqmst', {
          url: "/EmployeeMaster_ABQIS",
          templateUrl: 'app/modules/HRMS/views/EmployeeMaster_ABQIS.html',
          controller: 'EmployeeMaster_ABQISCont'
     	 })
		
	 .state('main.campup', {
            url: "/campFileUpload",
            templateUrl: 'app/modules/Health/views/campFileUpload.html',
            controller: 'campFileUploadCont'
	 })
        .state('main.cns01', {
            url: "/ConsistancyCheck1",
            templateUrl: 'app/modules/Student/views/ConsistancyCheck1.html',
            controller: 'ConsistancyCheck1Cont',
        })
         //.state('main.Sim666', {
         //    url: "/SectionSubjectTeacher_leam",
         //    templateUrl: 'app/modules/Scheduling/views/SectionSubjectTeacher_leam.html',
         //    controller: 'SectionSubjectTeacherCont_leam'
         //})
        .state('main.Doc01', {
            url: "/DefineDocNarration",
            templateUrl: 'app/modules/Finance/views/DefineDocNarration.html',
            controller: 'DefineDocNarrationCont',
        })
        .state('main.TEATRK', {
             url: "/TeacherTrackerSystemReport",
             templateUrl: 'app/modules/Gradebook/views/TeacherTrackerSystemReport.html',
             controller: 'TeacherTrackerSystemReportCont'
        })
         .state('main.dasvs', {
             url: "/AdmissionDashboardSVS",
             templateUrl: 'app/modules/Student/views/AdmissionDashboardSVS.html',
             controller: 'AdmissionDashboardSVSCont'
         })
        

 	.state('main.CLSGBR', {
             url: "/ClassWiseMarkEntry",
             templateUrl: 'app/modules/Gradebook/views/ClassWiseMarkEntry.html',
             controller: 'ClassWiseMarkEntryCont'
         })
        .state('main.dacsd', {
            url: "/AdmissionDashboardCsdportal",
            templateUrl: 'app/modules/Student/views/AdmissionDashboardCsdporatl.html',
            controller: 'AdmissionDashboardCsdporatlCont'
        })

	 .state('main.saldoc', {
          url: "/SalesDocVATLeams",
          templateUrl: 'app/modules/Inventory/views/SalesDocument_Leams.html',
          controller: 'SalesDocumentLeamsCont'
	 })
        .state('main.MRKPND', {
            url: "/MarkEntryPendingList",
            templateUrl: 'app/modules/Gradebook/views/MarkEntryPendingList.html',
            controller: 'MarkEntryPendingListCont'
        })
        .state('main.col333', {
            url: "/ColloborationDetails",
            templateUrl: 'app/modules/Setup/views/ColloborationDetails.html',
            controller: 'ColloborationDetailsCont',
        })
        .state('main.BADD01', {
            url: "/BulkAssessmentDataDownload",
            templateUrl: 'app/modules/Gradebook/views/BulkAssessmentDataDownload.html',
            controller: 'BulkAssessmentDataDownloadCont'
        })
        .state('main.sturcb', {
            url: "/StudentReceiveable_StudentwisePosting",            templateUrl: 'app/modules/Finance/views/StudentReceiveable_Studentwise.html',            controller: 'StudentReceiveablePosting_StudentwiseCont'
        })
        .state('main.SimABQ', {
            url: "/AbsentStudentsNotification_abqis",
            templateUrl: 'app/modules/Student/views/AbsentStudentsNotification_abqis.html',
            controller: 'AbsentStudentsNotification_abqisCont'
        })
         .state('main.USD001', {
             url: "/UsageStatisticsDashboard",
             templateUrl: 'app/modules/Security/views/UsageStatisticsDashboard.html',
             controller: 'UsageStatisticsDashboardCont'
         })
	.state('main.STDMET', {
	    url: "/StudentMentor",
	    templateUrl: 'app/modules/Gradebook/views/StudentMentor.html',
	    controller: 'StudentMentorCont'
	})
         .state('main.SSQRD1', {
             url: "/SurveyStatisticQueryReportDetailCont",
             templateUrl: 'app/modules/Setup/views/SurveyStatisticQueryDetailReport.html',
             controller: 'SurveyStatisticQueryReportDetailCont'
         })
        .state('main.IMPDAT', {
            url: "/SchoolRegistrationImport",
            templateUrl: 'app/modules/Setup/views/SchoolRegistrationImport.html',
            controller: 'SchoolRegistrationImportCont'
        })
        .state('main.Inv741', {
            url: "/VatProfile",
            templateUrl: 'app/modules/Inventory/views/VatProfile.html',
            controller: 'VatProfileCont'
        })
        .state('main.surpub', {
            url: "/SurveyPublish",
            templateUrl: 'app/modules/Student/views/SurveyPublish.html',
            controller: 'SurveyPublishCont'
        })
        .state('main.Pos550', {
     url: "/POS_FeePayment",
     templateUrl: 'app/modules/Fee/views/POSFeePayment.html',
     controller: 'POSFeePaymentCont'
 })




        $locationProvider.html5Mode(true)

    })

})();