﻿(function () {
    'use strict';

    var sims = angular.module('sims');

    sims.run(['$rootScope', '$location', '$cookieStore', '$http', '$state', 'gettextCatalog', function ($rootScope, $location, $cookieStore, $http, $state, gettextCatalog) {
      
        $rootScope.locales = {
            'ar': {
                lang: 'ar',
                country: 'AR',
                name: gettextCatalog.getString('Arabic')
            },
            'en': {
                lang: 'en',
                country: 'US',
                name: gettextCatalog.getString('English')
            }
        };
        $cookieStore.put('lang', "en");
        var lang = $cookieStore.get('lang') || navigator.language || navigator.userLanguage;

        $rootScope.locale = $rootScope.locales[lang];
        $rootScope.langDirection = lang == "ar"? "rtl" : "ltr";

        if ($rootScope.locale === undefined) {
            $rootScope.locale = $rootScope.locales[lang];
            if ($rootScope.locale === undefined) {
                $rootScope.locale = $rootScope.locales['en'];
                $cookieStore.put('lang', "en");
            }
        }

        gettextCatalog.setCurrentLanguage($rootScope.locale.lang);
        gettextCatalog.debug = true;

        // keep user logged in after page refresh
       // $cookieStore.remove('globals');
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
            var schoolUrl = window.location.href;
            var conStr = schoolUrl.substring(schoolUrl.indexOf('/') + 2, schoolUrl.indexOf('.'));
            $http.defaults.headers.common['schoolId'] = 'sis';
        }

        $rootScope.bodyStyle = { 'background-color': '' };

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState) {
            var restrictedPage = $.inArray($location.path(), ['/login']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            if (restrictedPage && !loggedIn) {
                $location.path('/login');
            }
            if (toState.name == "login") {
              //  $rootScope.bodyStyle = { "background-color": "grey" };
            } else {
                //$rootScope.bodyStyle = { 'background-color': '' };
            }
            
        });

        $rootScope.showReport = false;

        $rootScope.reportData = {
            //serviceUrl: '',
            //reportSource: {
            //    report: "SimsReports.Sims.SIMR01,SimsReports",
            //    parameters: {}
            //},
            //viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
            //scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
            //scale: 1.0,
            //ready: function () { },
        };
        
    }]);

})();